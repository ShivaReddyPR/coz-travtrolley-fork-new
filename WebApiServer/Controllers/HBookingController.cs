﻿using CT.BookingEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApiServer.Models;
using System;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Net.Http;
using System.Net;
using CT.Core;
using System.Web;

namespace WebApiServer.Controllers
{
    //[EnableCors("*", "*", "*")]
    public class HBookingController : ApiController
    {
        [Authorize]
        [HttpPost]
        public string BookHotel(JObject Data)
        {
            string JSONString = string.Empty;
            try
            {

                
                //var context = HttpRequest.Properties["MS_HttpContext"] as HttpContext;

                //if (HttpContext.Current == null)
                //    Audit.Add(EventType.Email, Severity.Normal, 0, "Valid http current is null", "");
                //if (HttpContext.Current != null)
                //    Audit.Add(EventType.Email, Severity.Normal, 0, "Valid http current", "");

                //if (HttpContext.Current != null && HttpContext.Current.Request != null)
                //    Audit.Add(EventType.Email, Severity.Normal, 0, "Valid http current request", "");

                //if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.Url != null)
                //    Audit.Add(EventType.Email, Severity.Normal, 0, "Valid http current request url", "");

                //if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.Url != null && !string.IsNullOrEmpty(HttpContext.Current.Request.Url.Host))
                //    Audit.Add(EventType.Email, Severity.Normal, 0, "Valid http current request url host - " + HttpContext.Current.Request.Url.Host, "");

                //if (HttpContext.Current != null && HttpContext.Current.Request != null && !string.IsNullOrEmpty(HttpContext.Current.Request.UserHostName))
                //    Audit.Add(EventType.Email, Severity.Normal, 0, "Valid http current request url host - " + HttpContext.Current.Request.UserHostName, "");

                //if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.UrlReferrer != null)
                //    Audit.Add(EventType.Email, Severity.Normal, 0, "Valid http current request UrlReferrer", "");

                //if (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.UrlReferrer != null && !string.IsNullOrEmpty(HttpContext.Current.Request.UrlReferrer.AbsolutePath))
                //    Audit.Add(EventType.Email, Severity.Normal, 0, "Valid http current request UrlReferrer AbsolutePath - " + HttpContext.Current.Request.UrlReferrer.AbsolutePath, "");
                ////var url = HttpContext.Current.Request.UrlReferrer != null ? HttpContext.Current.Request.UrlReferrer.AbsoluteUri.ToUpper() : string.Empty;
                ////Audit.Add(EventType.Email, Severity.Normal, 0, "NEw refer URL : " + url.ToUpper(), "");
                //Audit.Add(EventType.Email, Severity.Normal, 0, "Host : " + HttpContext.Current.Request.UserHostName.ToUpper(), "");
                //Audit.Add(EventType.Email, Severity.Normal, 0, "HOst Name : " +HttpContext.Current.Request.Url.Host.ToUpper(), "");

                //Audit.Add(EventType.Email, Severity.Normal, 0, "end mse: ", "");
                HotelItinerary Prod = Data["Product"].ToObject<HotelItinerary>();
                int AgentId = (Int32)Data["AgentId"];
                long UserId = (Int64)Data["UserId"];
                int iOnBehalfLoc = (Int32)Data["behalfLocation"];
                string SessionId = Data["SessionId"].ToString();
                HotelBooking HB = new HotelBooking();
                var BookingResult = new HotelBooking { bookingResponse = HB.HotelBook(Prod, AgentId, BookingStatus.Ready, UserId, SessionId, iOnBehalfLoc) };
                BookingResult.bookingResponse.ConfirmationNo = Prod.ConfirmationNo;
                //Prod.HotelPolicyDetails = string.Empty;
                //BookingResult.itinerary = Prod;
                JSONString = JsonConvert.SerializeObject(BookingResult);                
            }
            catch (Exception ex)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound) { ReasonPhrase = ex.Message };
                throw new HttpResponseException(resp);
            }
            return JSONString;
        }

        [HttpPost]
        public string GetPaymentPreference(JObject Data)
        {
            string JSONString = string.Empty;
            string paymentPreferences = string.Empty;
            try
            {
                HotelBooking HB = new HotelBooking();
                paymentPreferences = HB.GetPaymentPreference(Data["SessionId"].ToString(), Data["PaymentPrefParams"].ToString());
                JSONString = JsonConvert.SerializeObject(paymentPreferences);
            }
            catch (Exception ex)
            {
                //Audit

            }
            return JSONString;
        }

        [HttpPost]
        public string GetHotelItinerary(JObject Data)
        {
            string JSONString = string.Empty;
            try
            {
                HotelBooking clsHotelBook = new HotelBooking();
                var hotelRequest = Data["request"].ToObject<HotelRequest>();
                var UserId = Convert.ToInt32(Data["userId"]);
                var AgencyId = Convert.ToInt32(Data["agentId"]);                
                var hotelResult = Data["result"].ToObject<HotelSearchResult>();
                var SessionId = Convert.ToString(Data["sessionId"]);
                var RoomDetails = Data["roomdetails"].ToObject<HotelRoomsDetails[]>();
                var OnBehalfLoc = (Int32)Data["behalfLocation"];
                var clsItinerary = clsHotelBook.GetHotelItinerary(hotelRequest, hotelResult, ref RoomDetails, AgencyId, UserId, SessionId, OnBehalfLoc);

                Product prod = clsItinerary;
                prod.ProductId = clsItinerary.ProductId;
                prod.ProductType = clsItinerary.ProductType;
                prod.ProductTypeId = clsItinerary.ProductTypeId;

                object objInput = new { Product = prod, RoomTypes = RoomDetails };                
                JSONString = JsonConvert.SerializeObject(objInput);
            }
            catch (Exception ex)
            {
                
            }
            return JSONString;
        }

        [HttpPost]
        public decimal AddB2CHotelPendingQueue(JObject Data)
        {
            decimal dcPQHotelId = 0;
            try
            {
                HotelBooking clsHotelBook = new HotelBooking();
                HotelItinerary clsItinerary = Data["Product"].ToObject<HotelItinerary>();                
                string SessionId = Data["SessionId"].ToString();
                dcPQHotelId = clsHotelBook.SaveB2CHotelPendingQueue(clsItinerary, clsItinerary.AgencyId, clsItinerary.CreatedBy, SessionId);
            }
            catch (Exception ex)
            {

            }
            return dcPQHotelId;
        }

        [HttpPost]
        public string GetHotelBookingByConfNo(JObject request)
        {
            HotelBooking clsHotelBook = new HotelBooking();
            string sConfNo = request["confirmationNo"].ToObject<string>();
            return JsonConvert.SerializeObject(clsHotelBook.GetHotelBookingByConfNo(sConfNo));
        }
        [HttpPost]
        public string MakeHotelVoucher(JObject request)
        {

            try
            {
                HotelBooking clsHotelBook = new HotelBooking();
                string sConfNo = request["confirmationNo"].ToObject<string>();
                int AgentId = (Int32)request["agentId"];
                long UserId = (Int64)request["userId"];
                return JsonConvert.SerializeObject(clsHotelBook.MakeHotelVoucher(sConfNo, Convert.ToInt32(UserId), AgentId));
            }
            catch (Exception ex)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound) { ReasonPhrase = ex.Message };
                throw new HttpResponseException(resp);
            }
        }
    }
}
