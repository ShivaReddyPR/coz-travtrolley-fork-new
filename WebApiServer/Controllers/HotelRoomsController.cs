﻿using CT.BookingEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApiServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using CT.Core;

namespace WebApiServer.Controllers
{
    //[EnableCorsAttribute("*", "*", "*")]
    [Authorize]
    public class HotelRoomsController : ApiController
    {
        [HttpPost]
        public string GetRoomsResult(JObject Data)
        {
            string JSONString = string.Empty;
            try
            {
                HotelRoomsModel hotelRoomResult = new HotelRoomsModel();
                hotelRoomResult.Request = Data["request"].ToObject<HotelRequest>();
                hotelRoomResult.UserId = Convert.ToInt32(Data["userId"]);
                hotelRoomResult.AgencyId = Convert.ToInt32(Data["agentId"]);
                hotelRoomResult.BehalfAgentLocation = Convert.ToInt32(Data["behalfLocation"]);
                hotelRoomResult.HotelCode = Convert.ToString(Data["hotelCode"]);
                hotelRoomResult.CSessionId = Convert.ToString(Data["sessionId"]);
                hotelRoomResult.sourceId = Convert.ToString(Data["sourceId"]);
                hotelRoomResult.GetHotelRooms();

                var Result = new HotelRoomsModel { ResultObj = hotelRoomResult.ResultObj, CSessionId = hotelRoomResult.CSessionId , decimalValue= hotelRoomResult .decimalValue};
                JSONString = JsonConvert.SerializeObject(Result);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Error in Room GetRoomsResult():" + ex.ToString(), "");

            }
            return JSONString;
        }

        [HttpPost]
        public string GetRoomsCancelPolicy(JObject Data)
        {
            string JSONString = string.Empty;
            try
            {
                HotelRoomsModel hotelRoomResult = new HotelRoomsModel();
                hotelRoomResult.RoomTypeCode = Convert.ToString(Data["roomTypeCode"]);
                hotelRoomResult.ResultObj = Data["resultObj"].ToObject<HotelSearchResult>();
                hotelRoomResult.CSessionId = Convert.ToString(Data["sessionId"]);
                hotelRoomResult.UserId = Convert.ToInt32(Data["userId"]);
                hotelRoomResult.AgencyId = Convert.ToInt32(Data["agentId"]);
                hotelRoomResult.BehalfAgentLocation = Convert.ToInt32(Data["behalfLocation"]);
                hotelRoomResult.Request = Data["hrequest"].ToObject<HotelRequest>();
                hotelRoomResult.RoomNo = Convert.ToString(Data["roomNo"]);
                hotelRoomResult.RoomSource = (HotelBookingSource)Enum.Parse(typeof(HotelBookingSource), Convert.ToString(Data["sourceName"]), true);
                hotelRoomResult.GetRoomsCancelPolicy();

                var Result = new HotelRoomsModel { ResultObj = hotelRoomResult.ResultObj, CSessionId = hotelRoomResult.CSessionId, RoomTypeCode = hotelRoomResult.RoomTypeCode ,CancelPolicy= hotelRoomResult.CancelPolicy};
                JSONString = JsonConvert.SerializeObject(Result);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Error in Room GetRoomsCancelPolicy():" + ex.Message, "");
                //Audit

            }
            return JSONString;
        }

        [HttpPost]
        public string GetTravelPolicy(JObject Data)
        {
            string JSONString = string.Empty;
            try
            {
                HotelRoomsModel hotelRoomResult = new HotelRoomsModel();
                hotelRoomResult.Request = Data["request"].ToObject<HotelRequest>();
                hotelRoomResult.ResultObj = Data["resultObj"].ToObject<HotelSearchResult>();
                hotelRoomResult.RoomTypeCode = Convert.ToString(Data["roomTypeCode"]);
                hotelRoomResult.GetTravelPolicy();

                var Result = new HotelRoomsModel { ResultObj = hotelRoomResult.ResultObj, CSessionId = hotelRoomResult.CSessionId, decimalValue = hotelRoomResult.decimalValue };
                JSONString = JsonConvert.SerializeObject(Result);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Error in Room GetRoomsResult():" + ex.Message, "");

            }
            return JSONString;
        }
    }
}
