﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace RestFullServerAPI.Models
{
    /// <summary>
    /// Compression attribute to compress the response data size after every action method excecution
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CompressFilter : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext context)
        {
            var acceptedEncoding = context.Response.RequestMessage.Headers.AcceptEncoding.First().Value;
            if (!acceptedEncoding.Equals("gzip", StringComparison.InvariantCultureIgnoreCase)
            && !acceptedEncoding.Equals("deflate", StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }
            context.Response.Content = new CompressedContent(context.Response.Content, acceptedEncoding);
        }
    }

    //public class DeflateCompressionAttribute : ActionFilterAttribute
    //{
    //    public override void OnActionExecuted(HttpActionExecutedContext actContext)
    //    {
    //        var content = actContext.Response.Content;
    //        var bytes = content == null ? null : content.ReadAsByteArrayAsync().Result;
    //        var zlibbedContent = bytes == null ? new byte[0] :
    //        CompressionHelper.DeflateByte(bytes);
    //        actContext.Response.Content = new ByteArrayContent(zlibbedContent);
    //        actContext.Response.Content.Headers.Remove("Content-Type");
    //        actContext.Response.Content.Headers.Add("Content-encoding", "deflate");
    //        actContext.Response.Content.Headers.Add("Content-Type", "application/json");
    //        base.OnActionExecuted(actContext);
    //    }

    //}

    /// <summary>
    /// To compress the data using GZip stream
    /// </summary>
    public class CompressedContent : HttpContent
    {
        private readonly string _encodingType;
        private readonly HttpContent _originalContent;

        public CompressedContent(HttpContent content, string encodingType = "gzip")
        {
            _originalContent = content ?? throw new ArgumentNullException("content");
            _encodingType = encodingType.ToLowerInvariant();
            foreach (var header in _originalContent.Headers)
            {
                Headers.TryAddWithoutValidation(header.Key, header.Value);
            }
            Headers.ContentEncoding.Add(encodingType);
        }

        protected override bool TryComputeLength(out long length)
        {
            length = -1;
            return false;
        }

        protected override Task SerializeToStreamAsync(Stream stream, TransportContext context)
        {
            Stream compressedStream = null;
            switch (_encodingType)
            {
                case "gzip":
                    compressedStream = new GZipStream(stream, CompressionMode.Compress, true);
                    break;
                case "deflate":
                    compressedStream = new DeflateStream(stream, CompressionMode.Compress, true);
                    break;
                default:
                    compressedStream = stream;
                    break;
            }

            return _originalContent.CopyToAsync(compressedStream).ContinueWith(tsk =>
            {
                if (compressedStream != null)
                {
                    compressedStream.Dispose();
                }
            });
        }
    }

    //public class CompressionHelper
    //{
    //    public static byte[] DeflateByte(byte[] str)
    //    {
    //        if (str == null)
    //            return null;

    //        using (var output = new MemoryStream())
    //        {
    //            using (var compressor = new Ionic.Zlib.GZipStream(output, Ionic.Zlib.CompressionMode.Compress,Ionic.Zlib.CompressionLevel.BestSpeed))
    //                compressor.Write(str, 0, str.Length);

    //            return output.ToArray();
    //        }
    //    }
    //}
}