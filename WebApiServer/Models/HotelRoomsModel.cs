﻿using CT.BookingEngine;
//using CT.CMS;
using CT.Core;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace WebApiServer.Models
{
    public class HotelRoomsModel
    {

        public HotelRequest Request;
        public string CSessionId;
        public int UserId;
        public int AgencyId;
        public int BehalfAgentLocation;
        public int decimalValue = 2;
        public string HotelCode;
        public  HotelSearchResult ResultObj = new HotelSearchResult();
        public string RoomTypeCode;
        public string CancelPolicy;
        public string RoomNo;
        public HotelBookingSource RoomSource;
        public string sourceId;
        public void GetHotelRooms()
        {
            try
            {
                MetaSearchEngine mse = new MetaSearchEngine();
                //LoginInfo loginInfo = LoginInfo.GetLoginInfo(UserId);
                //if (BehalfAgentLocation != 0)
                //{
                //    loginInfo.IsOnBehalfOfAgent = true;
                //    loginInfo.OnBehalfAgentID = AgencyId;                   
                //    AgentMaster agent = new AgentMaster(loginInfo.OnBehalfAgentID);
                //    loginInfo.OnBehalfAgentCurrency = agent.AgentCurrency;
                //    loginInfo.OnBehalfAgentDecimalValue = agent.DecimalValue;
                //    loginInfo.OnBehalfAgentLocation = BehalfAgentLocation;
                //    LocationMaster locationMaster = new LocationMaster(loginInfo.OnBehalfAgentLocation);                    
                //    Request.LoginCountryCode = locationMaster.CountryCode;
                //    StaticData sd = new StaticData();
                //    sd.BaseCurrency = agent.AgentCurrency;
                //    loginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                //    decimalValue = agent.DecimalValue;
                //}
                //else
                //{
                //    loginInfo.IsOnBehalfOfAgent = false;
                //    loginInfo.OnBehalfAgentID = 0;
                //    decimalValue = loginInfo.DecimalValue;
                //    Request.LoginCountryCode = loginInfo.LocationCountryCode;
                //}

                var clsLogInfo = APIGenericStatic.GetLoginInfo(UserId, BehalfAgentLocation, AgencyId);
                Request.LoginCountryCode = clsLogInfo.IsOnBehalfOfAgent ? APIGenericStatic.GetLoginCountry(BehalfAgentLocation) : clsLogInfo.LocationCountryCode;
                try
                {
                    Request.CountryCode = string.IsNullOrEmpty(Request.CountryCode) ? Country.GetCountryCodeFromCountryName(Request.CountryName) : Request.CountryCode;
                }
                catch { }

                ResultObj = mse.GetApiHotelRoomDetails(Request, AgencyId, BehalfAgentLocation, HotelCode, CSessionId, clsLogInfo,sourceId);
                if (ResultObj != null && ResultObj.RoomDetails != null && ResultObj.RoomDetails.Length > 0)
                {
                    List<HotelSearchResult> hData = new List<HotelSearchResult>();
                    hData.Add(ResultObj);
                    hData[0].Save(CSessionId, hData.ToArray(), Request.NoOfRooms, Request, ResultCacheType.CoreRoomDetails);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Error in Room Model:"+ex.ToString(), "");
                throw ex;
            }
      
        }

        public void GetRoomsCancelPolicy()
        {
            try
            {
                var selectedResult = new HotelSearchResult().Load(CSessionId, ResultCacheType.CoreRoomDetails);

                HotelSearchResult.ValResultPrice(selectedResult[0], ResultObj);

                MetaSearchEngine mse = new MetaSearchEngine();
                mse.SessionId = CSessionId;
                mse.SettingsLoginInfo = APIGenericStatic.GetLoginInfo(UserId, BehalfAgentLocation, AgencyId);
                CancelPolicy = mse.GetApiRoomsCancelPolicy(ResultObj,RoomTypeCode, Request,RoomNo,RoomSource);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Error in GetRoomsCancelPolicy():" + ex.Message, "");
                throw;
            }

        }

        public void GetTravelPolicy()
        {
            try
            {
                MetaSearchEngine mse = new MetaSearchEngine();
                mse.SessionId = CSessionId;
                int iRoomIndx = ResultObj.RoomDetails.ToList().FindIndex(x => x.RoomTypeCode == RoomTypeCode);
                ResultObj.RoomDetails[iRoomIndx].TravelPolicyResult = mse.GetHotelPolicyResult(Request, ResultObj, RoomTypeCode);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Error in GetRoomsCancelPolicy():" + ex.Message, "");
                throw;
            }

        }
    }

     
}
