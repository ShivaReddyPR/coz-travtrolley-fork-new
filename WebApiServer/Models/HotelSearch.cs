﻿using System;
using System.Collections.Generic;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.MetaSearchEngine;
using System.Data;
using System.Linq;

namespace WebApiServer.Models
{
    public class HotelSearch
    {
        // protected HotelRequest request = new HotelRequest();
        protected HotelSearchResult[] result = new HotelSearchResult[0];
        protected string sessionId = string.Empty;
        public int decimalValue = 2;

        public HotelSearchResult[] Result { get => result; set => result = value; }
        public string SessionId { get => sessionId; set => sessionId = value; }

        public void SearchHotels(HotelRequest request,int AgentId,int UserId,int BehalfAgentLocation)
        {    
            try
            {
                //load the member info for the current session
                //Get the hotel results from MSE            
                MetaSearchEngine mse = new MetaSearchEngine();
                //LocationMaster locationMaster = new LocationMaster();
                //LoginInfo loginInfo = LoginInfo.GetLoginInfo(UserId);
                //if (BehalfAgentLocation != 0)//if Condition True then this is IsOnBehalfOfAgent
                //{
                //    loginInfo.IsOnBehalfOfAgent = true;
                //    loginInfo.OnBehalfAgentID = AgentId;

                //    AgentMaster agent = new AgentMaster(loginInfo.OnBehalfAgentID);
                //    loginInfo.OnBehalfAgentCurrency = agent.AgentCurrency;
                //    loginInfo.OnBehalfAgentDecimalValue = agent.DecimalValue;
                //    loginInfo.OnBehalfAgentLocation = BehalfAgentLocation;
                //    StaticData sd = new StaticData();
                //    sd.BaseCurrency = agent.AgentCurrency;
                //    loginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                //    Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(loginInfo.OnBehalfAgentID);

                //    loginInfo.OnBehalfAgentSourceCredentials = AgentCredentials;
                //}
                //else
                //{
                //    loginInfo.IsOnBehalfOfAgent = false;
                //    loginInfo.OnBehalfAgentID = 0;
                //}
                //int agencyId = 0;
                //if (!loginInfo.IsOnBehalfOfAgent)
                //{
                //    agencyId = Convert.ToInt32(loginInfo.AgentId);
                //    decimalValue = loginInfo.DecimalValue;
                //    request.LoginCountryCode = loginInfo.LocationCountryCode;
                //}
                //else
                //{
                //    agencyId = loginInfo.OnBehalfAgentID; //Selected Agency
                //    decimalValue = loginInfo.OnBehalfAgentDecimalValue;
                //    locationMaster = new LocationMaster(loginInfo.OnBehalfAgentLocation);
                //    request.LoginCountryCode = locationMaster.CountryCode;
                //}

                //try
                //{
                //    request.CountryCode = Country.GetCountryCodeFromCountryName(request.CountryName);
                //}
                //catch(Exception extmpEX) { Audit.Add(EventType.Exception, Severity.High, AgentId, "exp gimmo search-> "+extmpEX.ToString(), "");  }              
                mse.SettingsLoginInfo = APIGenericStatic.GetLoginInfo(UserId, BehalfAgentLocation, AgentId);
                request.LoginCountryCode = mse.SettingsLoginInfo.IsOnBehalfOfAgent ? 
                    APIGenericStatic.GetLoginCountry(BehalfAgentLocation) : mse.SettingsLoginInfo.LocationCountryCode;

                
                try
                {
                    request.CountryCode = string.IsNullOrEmpty(request.CountryCode) ? Country.GetCountryCodeFromCountryName(request.CountryName) : request.CountryCode;
                }
                catch (Exception extmpEX) { Audit.Add(EventType.Exception, Severity.High, AgentId, "exp gimmo search-> " + extmpEX.ToString(), ""); }

                /* To get hotel sources for agent if not assigned to request on UI side */
                if (request.Sources == null || request.Sources.Count == 0)
                {
                    
                        DataTable dtSources = AgentMaster.GetAgentSources(AgentId, 2, true);
                        request.Sources = dtSources != null && dtSources.Rows.Count > 0 ? dtSources.AsEnumerable().Select(r => r.Field<string>("Name")).ToList() : request.Sources;
                }
                Result =  mse.GetHotelResults(request, Convert.ToInt64(AgentId));
                

                SessionId = mse.SessionId.ToString();
                HotelSearchResult hData = new HotelSearchResult();
                
                
                if (Result != null && Result.Length > 0)
                {
                    hData.Save(SessionId, Result, request.NoOfRooms, request, ResultCacheType.CoreHotelResult);
                    result.ToList().ForEach(x=> x.RoomDetails=null);
                }
            }
            catch (BookingEngineException ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "exception from GetSearchResult is:" + ex.ToString(), "");

                throw ex;
            }
            catch (Exception excep)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "exception from GetSearchResult is:" + excep.ToString(), "");

            }
        }

    }
}
