﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AgentPaymentRequest
/// </summary>
public class AgentPaymentRequest
{
    #region Variables  
    private string _agentCode;
    private string _receiptNo;
    private DateTime _receiptDate;
    private decimal _amount;
    private string _paymentMode;
    private string _chequeNo;
    private DateTime _chequeDate;
    private string _bankName;
    private string _bankBranch;
    private string _bankAccount;
    private string _remarks;
    private string _depositSlipPath;
    private string _depositSlipType;
    private string _status;
    private long _createdBy;
    private string _createdByName;
    private static decimal _agentBalance;
    private decimal _invBalance;
    private string _agentName;
    private string _currency;
    private decimal _exchRate;
    private string _subAgentCurrency;
    private decimal _subAgentExchRate;
    private decimal _subAgentAmount;
    private string _docType;
    private string _docBase;
    private string _tranxType;
    private string _tranxProvision;
    private int _locationId;
    private int _subAgentId;
    private int _transRefId;
    private string _transRefName;
    private string _transRole;


    #endregion
    #region Properties
    public string AgentCode
    {
        get { return _agentCode; }
        set { _agentCode = value; }
    }
    public string ReceiptNo
    {
        get { return _receiptNo; }
        set { _receiptNo = value; }
    }
    public DateTime ReceiptDate
    {
        get { return _receiptDate; }
        set { _receiptDate = value; }
    }

    public decimal Amount
    {
        get { return _amount; }
        set { _amount = value; }
    }
    public string PaymentMode
    {
        get { return _paymentMode; }
        set { _paymentMode = value; }
    }
    public string ChequeNo
    {
        get { return _chequeNo; }
        set { _chequeNo = value; }
    }
    public DateTime ChequeDate
    {
        get { return _chequeDate; }
        set { _chequeDate = value; }
    }
    public string BankName
    {
        get { return _bankName; }
        set { _bankName = value; }
    }
    public string BankBranch
    {
        get { return _bankBranch; }
        set { _bankBranch = value; }
    }
    public string BankAccount
    {
        get { return _bankAccount; }
        set { _bankAccount = value; }
    }
    public string Remarks
    {
        get { return _remarks; }
        set { _remarks = value; }
    }
    public string DepositSliPath
    {
        get { return _depositSlipPath; }
        set { _depositSlipPath = value; }
    }
    public string DepositSlipType
    {
        get { return _depositSlipType; }
        set { _depositSlipType = value; }
    }

    public string Status
    {
        get { return _status; }
        set { _status = value; }
    }
    public long CreatedBy
    {
        get { return _createdBy; }
        set { _createdBy = value; }
    }

    public string CreatedByName
    {
        get { return _createdByName; }
        set { _createdByName = value; }
    }

    public static decimal AgentBalane
    {
        get { return _agentBalance; }
        set { _agentBalance = value; }

    }
    public decimal InvBalance
    {
        get { return _invBalance; }
        set { _invBalance = value; }

    }

    public string AgentName
    {
        get { return _agentName; }
        set { _agentName = value; }
    }
    public string Currency
    {
        get { return _currency; }
        set { _currency = value; }
    }

    public decimal ExchRate
    {
        get { return _exchRate; }
        set { _exchRate = value; }
    }

    public string DocType
    {
        get { return _docType; }
        set { _docType = value; }
    }
    public string DocBase
    {
        get { return _docBase; }
        set { _docBase = value; }
    }
    public string TranxType
    {
        get { return _tranxType; }
        set { _tranxType = value; }
    }
    public string TranxProvision
    {
        get { return _tranxProvision; }
        set { _tranxProvision = value; }
    }
    public int LocationId
    {
        get { return _locationId; }
        set { _locationId = value; }
    }
    public int SubAgentId
    {
        get { return _subAgentId; }
        set { _subAgentId = value; }
    }
    public int TransReftId
    {
        get { return _transRefId; }
        set { _transRefId = value; }
    }

    public string TransRefName  //agent name or subagent name
    {
        get { return _transRefName; }
        set { _transRefName = value; }
    }

    public string TransRole  //agent or subagent 
    {
        get { return _transRole; }
        set { _transRole = value; }
    }
    public string SubAgentCurrency
    {
        get { return _subAgentCurrency; }
        set { _subAgentCurrency = value; }
    }
    public decimal SubAgentExchangeRate
    {
        get { return _subAgentExchRate; }
        set { _subAgentExchRate = value; }
    }
    public decimal SubAgentAmount
    {
        get { return _subAgentAmount; }
        set { _subAgentAmount = value; }
    }
  
    #endregion
    public AgentPaymentRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}