﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AgentPaymentResponse
/// </summary>
public class AgentPaymentResponse
{
    #region Variables
    decimal _agentBalance;
    string _receiptNo;
    string _errorCode;
    string _errorMessage;
    string _successMessage;
    #endregion
    #region Properties
    public decimal AgentBalance
    {
        get { return _agentBalance; }
        set { _agentBalance = value; }
    }
    public string ReceiptNo
    {
        get { return _receiptNo; }
        set { _receiptNo = value; }
    }
    public string ErrorCode
    {
        get { return _errorCode; }
        set { _errorCode = value; }
    }
    public string ErrorMessage
    {
        get { return _errorMessage; }
        set { _errorMessage = value; }
    }
    public string SuccessMessage
    {
        get { return _successMessage; }
        set { _successMessage = value; }
    }
    #endregion
    public AgentPaymentResponse()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    
}