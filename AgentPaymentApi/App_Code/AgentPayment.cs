﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using System.Transactions;
public class ServiceAuthentication : SoapHeader
{
    public string UserName;
    public string Password;
}
/// <summary>
/// Summary description for AgentPayment
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class AgentPayment : System.Web.Services.WebService
{
    string xmlPath = ConfigurationManager.AppSettings["Path"];
    bool userAuth = false;
    public ServiceAuthentication Credential;
    public AgentPayment()
    {
        LoadCredentials();

    }
    private void LoadCredentials()
    {

        xmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
        try
        {
            if (!System.IO.Directory.Exists(xmlPath))
            {
                System.IO.Directory.CreateDirectory(xmlPath);
            }
        }
        catch { }
    }
    [WebMethod(EnableSession = true)]
    [SoapHeader("Credential")]
    public AgentPaymentResponse SaveApproval(AgentPaymentRequest request)
    {
        AgentPaymentResponse response = new AgentPaymentResponse();
        try
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(AgentPaymentRequest));
            FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_AgentPaymentRequest.xml", FileMode.Create, FileAccess.Write);
            XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            xtw.Formatting = Formatting.Indented;
            serializer.Serialize(xtw, request);
            fs.Close();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Create Path. Error: " + ex.Message, "0");
        }
        try
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Authenticate"]))
            {
                userAuth = true;
            }
            else
            {
                //Audit.Add(EventType.Login, Severity.Low, 1, "(AgentPaymentAPI)AgentTopUp service Before Login  UserName:" + Credential.UserName + ",Password:" + Credential.Password, "");
                if (Credential == null)
                {
                    userAuth = false;
                }
                else if (string.IsNullOrEmpty(Credential.UserName) || Credential.UserName.Length > 50)
                {
                    userAuth = false;
                }
                else if (string.IsNullOrEmpty(Credential.Password) || Credential.Password.Length > 50)
                {
                    userAuth = false;
                }
                else
                {
                    userAuth = UserMaster.IsAuthenticatedUser(Credential.UserName.Trim(), Credential.Password.Trim(), 1);
                   // Audit.Add(EventType.Login, Severity.Low, 1, "(AgentPaymentAPI)AgentTopUp service After Login UserName:" + Credential.UserName + ",Password:" + Credential.Password, "");
                }
            }
            if (userAuth)
            {

                if (string.IsNullOrEmpty(request.AgentCode))
                {
                    response.ErrorCode = "002";
                    response.ErrorMessage = "Agent Code should not be empty ";

                }
                else if (request.AgentCode.Length > 20)
                {
                    response.ErrorCode = "002";
                    response.ErrorMessage = "Agent Code should not be greater than 20 characters ";
                }
                else
                {
                    DataTable agentdetails = AgentMaster.GetAgentIdByCode(request.AgentCode.Trim());
                    if (agentdetails != null && agentdetails.Rows.Count == 1)
                    {
                        DataRow drdetails = agentdetails.Rows[0];
                        int agentid = Utility.ToInteger(drdetails["agent_id"]);
                        string currency = Utility.ToString(drdetails["agent_currency"]);
                        if (agentid <= 0)
                        {
                            response.ErrorMessage = "Agent code Should be greater than 0";
                            response.ErrorCode = "003";
                        }
                        else if (string.IsNullOrEmpty(currency))
                        {
                            response.ErrorMessage = "Currency should not be empty ";
                            response.ErrorCode = "003";
                        }
                        else if (currency.Length > 10)
                        {
                            response.ErrorMessage = "Currency should not be greater than 10";
                            response.ErrorCode = "003";
                        }
                        else if (currency != request.Currency)
                        {
                            response.ErrorMessage = "Currency should be equal to requested currency ";
                            response.ErrorCode = "002";
                        }
                        else if (string.IsNullOrEmpty(request.PaymentMode))
                        {
                            response.ErrorMessage = "Payment Mode should not be empty ";
                            response.ErrorCode = "002";
                        }
                        else if (string.IsNullOrEmpty(request.TranxType) && string.IsNullOrEmpty(request.ReceiptNo))
                        {
                            response.ErrorMessage = "ReceiptNo should not be empty ";
                            response.ErrorCode = "002";
                        }
                        else if (request.ReceiptDate == DateTime.MinValue)
                        {
                            response.ErrorMessage = "ReceiptDate should not be Minimum Date ";
                            response.ErrorCode = "002";
                        }
                        else if (request.ExchRate <= 0)
                        {
                            response.ErrorMessage = "ExchangeRate should be greaterthan 0 ";
                            response.ErrorCode = "002";
                        }
                        else
                        {
                            if (request.PaymentMode == "CHEQUE" || request.PaymentMode == "DRAFT")// if request payment is CHEQUE Or DRAFT, validating cheqno,
                            {
                                if (string.IsNullOrEmpty(request.ChequeNo))
                                {
                                    response.ErrorMessage = "Cheque Number should not be empty";
                                    response.ErrorCode = "002";
                                }
                                else if (request.ChequeNo.Length > 50)
                                {
                                    response.ErrorMessage = "Cheque Number should not be greater than 50";
                                    response.ErrorCode = "002";
                                }
                                else if (request.ChequeDate == DateTime.MinValue)
                                {
                                    response.ErrorMessage = "Cheque Date should not be minimum date";
                                    response.ErrorCode = "002";
                                }
                                //else if (string.IsNullOrEmpty(request.BankName))
                                //{
                                //    response.ErrorMessage = "BankName should not be empty";
                                //    response.ErrorCode = "002";
                                //}
                                else if (request.BankName.Length > 75)
                                {
                                    response.ErrorMessage = "BankName should not be greater than 75";
                                    response.ErrorCode = "002";
                                }
                                else if (string.IsNullOrEmpty(request.BankBranch))
                                {
                                    response.ErrorMessage = "Branch should not be empty";
                                    response.ErrorCode = "002";
                                }
                                else if (request.BankBranch.Length > 75)
                                {
                                    response.ErrorMessage = "Branch should not be greater than 75";
                                    response.ErrorCode = "002";
                                }
                                else if (string.IsNullOrEmpty(request.BankAccount))
                                {
                                    response.ErrorMessage = "Account Number should not be empty";
                                    response.ErrorCode = "002";
                                }
                                else if (request.BankAccount.Length > 50)
                                {
                                    response.ErrorMessage = "Account should not be greater than 50";
                                    response.ErrorCode = "002";
                                }
                                //else if (request.Amount <= 0)
                                //{
                                //    response.ErrorMessage = "Amount should not be empty ";
                                //    response.ErrorCode = "002";
                                //}
                            }
                        }
                        if (string.IsNullOrEmpty(response.ErrorMessage))
                        {
                            using (TransactionScope transactionScope = new TransactionScope())
                            {
                                try
                                {
                                    AgentPaymentDetails payment = new AgentPaymentDetails();
                                    payment.ReceiptNo = request.ReceiptNo;
                                    payment.ReceiptDate = request.ReceiptDate;
                                    payment.AgentId = agentid;
                                    payment.PayMode = request.PaymentMode;
                                    payment.Amount = request.Amount;
                                    payment.TranxType = request.TranxType;
                                    payment.TranxProvision = request.TranxProvision;
                                    payment.ChequeNo = request.ChequeNo;
                                    payment.ChequeDate = request.ChequeDate;
                                    payment.BankName = request.BankName;
                                    payment.BankBranch = request.BankBranch;
                                    payment.BankAccount = request.BankAccount;
                                    payment.DepositSliPath = request.DepositSliPath;
                                    payment.DepositSlipType = request.DepositSlipType;
                                    payment.Remarks = request.Remarks;
                                    payment.Currency = request.Currency;
                                    payment.LocationId = request.LocationId;
                                    payment.ExchRate = request.ExchRate;
                                    payment.Status = Settings.ACTIVE;
                                    payment.CreatedBy = -5;//to do Ziyad -- -5 is to identify the tranx  from VMS Topup
                                    payment.ApprovalStatus = "Y";
                                    payment.Save();

                                    response.ReceiptNo = payment.ReceiptNo;
                                    //Leadger Transaction
                                    CT.AccountingEngine.LedgerTransaction ledgerTxn;
                                    CT.AccountingEngine.NarrationBuilder objNarration = new CT.AccountingEngine.NarrationBuilder();
                                    ledgerTxn = new CT.AccountingEngine.LedgerTransaction();
                                    //ledgerTxn.LedgerId = Utility.ToInteger(agentId);
                                    ledgerTxn.LedgerId = agentid;
                                    ledgerTxn.Amount = Utility.ToDecimal(request.Amount) * -1;
                                    ledgerTxn.IsLCC = false;
                                    ledgerTxn.ReferenceId = Utility.ToInteger(payment.ID);
                                    if (request.TranxType == "DEB")
                                    {
                                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.PaymentReversed;// debit
                                    }
                                    else
                                    {
                                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.PaymentRecieved;// credit
                                    }
                                    objNarration.Remarks = request.TranxProvision;
                                    ledgerTxn.Narration = objNarration;
                                    ledgerTxn.Notes = request.Remarks;
                                    ledgerTxn.Date = DateTime.Now;
                                    //ledgerTxn.CreatedBy = Settings.LoginInfo.AgentId;
                                    ledgerTxn.CreatedBy = (int)request.CreatedBy;
                                    ledgerTxn.Save();
                                    transactionScope.Complete();

                                }
                                catch (Exception ex)
                                {
                                    if (ex.Message.Contains("|"))
                                    {
                                        response.ErrorCode = ex.Message.Split('|')[1];
                                        response.ErrorMessage = ex.Message.Split('|')[0];
                                    }
                                    else
                                    {
                                        response.ErrorMessage = ex.Message;
                                        response.ErrorCode = "004";
                                    }
                                    transactionScope.Dispose();
                                }
                            }
                        }
                    }
                    else
                    {
                        if (agentdetails != null && agentdetails.Rows.Count > 1)
                        {
                            response.ErrorCode = "002";
                            response.ErrorMessage = "Having Duplicate AgentCodes";
                        }
                        else
                        {
                            response.ErrorCode = "002";
                            response.ErrorMessage = "There is No Agent For Requested AgentCode";
                        }
                    }
                }
            }
            else
            {
                response.ErrorMessage = "Invalid Credentials ";
                response.ErrorCode = "001";
            }
            if (string.IsNullOrEmpty(response.ErrorMessage))
            {
                response.ErrorCode = "000";
                response.ErrorMessage = "Success";
                response.SuccessMessage = "AgentPayment added successfully.";
                response.AgentBalance = AgentPaymentDetails.AgentBalane;

            }
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(AgentPaymentResponse));
                FileStream fs = new FileStream(xmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_AgentPaymnetResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        catch (Exception ex)
        {
            response.ErrorMessage = ex.Message;
            response.ErrorCode = "006";
            Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Get Authenticate. Error: " + ex.ToString(), "0");
        }
        return response;
    }
}
