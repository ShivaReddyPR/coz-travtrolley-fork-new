﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using ZEUS = ZeusInsB2B.Zeus;
using System.Xml;
using CT.Configuration;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;
using System.Web;


namespace CT.BookingEngine.Insurance
{

    public class ZEUSIns
    {

        #region Members
        string _userName = string.Empty;
        string _password = string.Empty;
        string _channel = string.Empty;

        int _id;
        string _originCountryCode;
        /// <summary>
        /// Pax Country Code
        /// </summary>
        string _countryCode;
        /// <summary>
        /// Pax Culture code
        /// </summary>
        string _cultureCode;
        string _destinationCountryCode;
        string _origin;
        string _destination;
        string _originAirlineCode;
        string _destinationAirlineCode;
        string _originFlightNo;
        string _destinationFlightNo;
        string _departureDateTime;
        string _returnAirlineCode;
        string _returnDateTime;
        string _returnFlightNo;
        int _adults;
        int _childs;
        int _infants;
        string _pnr;
        string _insurancePlanCode;
        string _ssrFeeCode;
        string _currencyCode;
        double _premiumAmount;
        string _premiumChargeType;
        string _planTitle;
        string _planDescription;
        string _error;
        string _errorDescription;
        /// <summary>
        /// FirstName + LastName of the first passenger
        /// </summary>
        string _contactPersonName;
        /// <summary>
        /// Email address of the Contact Person
        /// </summary>
        string _emailAddress;
        /// <summary>
        /// 
        /// </summary>
        List<PassengerDetail> _passengerDetails;

        public static string agentBaseCurrency;
        public static SerializableDictionary<string, decimal> exchangeRates;
        static double rateOfExchange;
        private string _connectionString;
        #endregion

        #region Properties
        /// <summary>
        /// Logged in User will set for B2B.
        /// </summary>
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string OriginCountryCode
        {
            get { return _originCountryCode; }
            set { _originCountryCode = value; }
        }
        public string DestinationCountryCode
        {
            get { return _destinationCountryCode; }
            set { _destinationCountryCode = value; }
        }
        public string Origin
        {
            get { return _origin; }
            set { _origin = value; }
        }
        public string Destination
        {
            get { return _destination; }
            set { _destination = value; }
        }
        public string OriginAirlineCode
        {
            get { return _originAirlineCode; }
            set { _originAirlineCode = value; }
        }
        public string DestinationAirlineCode
        {
            get { return _destinationAirlineCode; }
            set { _destinationAirlineCode = value; }
        }
        public string OriginFlightNo
        {
            get { return _originFlightNo; }
            set { _originFlightNo = value; }
        }
        public string DestinationFlightNo
        {
            get { return _destinationFlightNo; }
            set { _destinationFlightNo = value; }
        }
        public string DepartureDateTime
        {
            get { return _departureDateTime; }
            set { _departureDateTime = value; }
        }
        public string ReturnFlightNo
        {
            get { return _returnFlightNo; }
            set { _returnFlightNo = value; }
        }
        public string ReturnAirlineCode
        {
            get { return _returnAirlineCode; }
            set { _returnAirlineCode = value; }
        }
        public string ReturnDateTime
        {
            get { return _returnDateTime; }
            set { _returnDateTime = value; }
        }
        public int Adults
        {
            get { return _adults; }
            set { _adults = value; }
        }
        public int Childs
        {
            get { return _childs; }
            set { _childs = value; }
        }
        public int Infants
        {
            get { return _infants; }
            set { _infants = value; }
        }
        public string PNR
        {
            get { return _pnr; }
            set { _pnr = value; }
        }
        public string InsurancePlanCode
        {
            get { return _insurancePlanCode; }
            set { _insurancePlanCode = value; }
        }
        public string SSRFeeCode
        {
            get { return _ssrFeeCode; }
            set { _ssrFeeCode = value; }
        }
        public string CurrencyCode
        {
            get { return _currencyCode; }
            set { _currencyCode = value; }
        }
        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }
        public string CultureCode
        {
            get { return _cultureCode; }
            set { _cultureCode = value; }
        }
        public double PremiumAmount
        {
            get { return _premiumAmount; }
            set { _premiumAmount = value; }
        }
        public string PremiumChargeType
        {
            get { return _premiumChargeType; }
            set { _premiumChargeType = value; }
        }
        public string PlanTitle
        {
            get { return _planTitle; }
            set { _planTitle = value; }
        }
        public string PlanDescription
        {
            get { return _planDescription; }
            set { _planDescription = value; }
        }
        public string Error
        {
            get { return _error; }
            set { _error = value; }
        }
        public string ErrorDescription
        {
            get { return _errorDescription; }
            set { _errorDescription = value; }
        }
        /// <summary>
        /// FirstName + LastName of the first passenger
        /// </summary>
        public string ContactPersonName
        {
            get { return _contactPersonName; }
            set { _contactPersonName = value; }
        }
        /// <summary>
        /// Email address of the Contact Person
        /// </summary>
        public string EmailAddress
        {
            get { return _emailAddress; }
            set { _emailAddress = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public List<PassengerDetail> PassengerDetails
        {
            get { return _passengerDetails; }
            set { _passengerDetails = value; }
        }

        public string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        public string AgentBaseCurrency
        {
            get { return agentBaseCurrency; }
            set { agentBaseCurrency = value; }
        }

        public SerializableDictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        #endregion

        //public ZEUSIns()
        //{
        //    string _userName = ConfigurationSystem.ZeusConfig["UserName"];
        //    string _password = ConfigurationSystem.ZeusConfig["Password"];
        //    string _channel = ConfigurationSystem.ZeusConfig["Channel"];
        //}

        private void Connection()
        {
            _userName = ConfigurationSystem.ZeusConfig["UserName"];
            _password = ConfigurationSystem.ZeusConfig["Password"];
            _channel = ConfigurationSystem.ZeusConfig["Channel"];
            //_channel = "CozmoXBE";

        }

        /// <summary>
        /// This method will only B2B and B2C Markup if transType=True and will be called from BookingAPI.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="agencyId"></param>
        /// <param name="transType"></param>
        /// <returns></returns>
        public List<InsurancePlan> GetAvailablePlans(string username, int agencyId, string transType)
        {
            Connection();

            ZEUS.OutputResponseAvailablePlansOTA availablePlanDetails = null;
            List<InsurancePlan> availablePlans = new List<InsurancePlan>();
            availablePlans.Clear();
            try
            {
                ZEUS.ZEUSTravelInsuranceGateway insGateway = new ZEUS.ZEUSTravelInsuranceGateway();
                ZEUS.InputRequestOTA insPlanRequest = new ZEUS.InputRequestOTA();

                //Authentication details            
                insPlanRequest.Authentication = new ZEUS.ItineraryAuthentication();
                insPlanRequest.Authentication.Username = _userName;
                insPlanRequest.Authentication.Password = _password;

                //Header details
                insPlanRequest.Header = new ZEUS.ItineraryHeaderOTA();
                insPlanRequest.Header.Channel = _channel + username;
                insPlanRequest.Header.Currency = _currencyCode;
                insPlanRequest.Header.CountryCode = _countryCode;
                insPlanRequest.Header.CultureCode = _cultureCode;
                insPlanRequest.Header.TotalAdults = _adults;
                insPlanRequest.Header.TotalChild = _childs;
                insPlanRequest.Header.TotalInfants = _infants;


                //Flight Details
                insPlanRequest.Flights = new ZEUS.ItineraryFlight();
                insPlanRequest.Flights.DepartCountryCode = _originCountryCode;
                insPlanRequest.Flights.DepartStationCode = _origin;
                insPlanRequest.Flights.ArrivalCountryCode = _destinationCountryCode;
                insPlanRequest.Flights.ArrivalStationCode = _destination;
                insPlanRequest.Flights.DepartAirlineCode = _originAirlineCode;
                insPlanRequest.Flights.DepartDateTime = _departureDateTime;
                insPlanRequest.Flights.ReturnAirlineCode = _originAirlineCode;
                insPlanRequest.Flights.ReturnDateTime = _returnDateTime;
                insPlanRequest.Flights.ReturnFlightNo = _returnFlightNo;
                insPlanRequest.Flights.DepartFlightNo = _originFlightNo;

                try
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(insPlanRequest.GetType());
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                    ser.Serialize(writer, insPlanRequest);  // Here Classes are converted to XML String. 
                    // This can be viewed in SB or writer.
                    // Above XML in SB can be loaded in XmlDocument object
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sb.ToString());
                    doc.Save(ConfigurationSystem.ZeusConfig["XmlLogPath"] + "ZeusInsAvailablePlansRequest" + DateTime.Now.ToString("ddMMyyy_hhmmss") + ".xml");
                }
                catch
                {
                }

                availablePlanDetails = insGateway.GetAvailablePlansOTA(insPlanRequest);

                try
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(availablePlanDetails.GetType());
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                    ser.Serialize(writer, availablePlanDetails);  // Here Classes are converted to XML String. 
                    // This can be viewed in SB or writer.
                    // Above XML in SB can be loaded in XmlDocument object
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sb.ToString());
                    doc.Save(ConfigurationSystem.ZeusConfig["XmlLogPath"] + "ZeusInsAvailablePlansResponse" + DateTime.Now.ToString("ddMMyyy_hhmmss") + ".xml");
                }
                catch
                {
                }

                if (availablePlanDetails.ErrorCode != "0")
                {
                    _error = availablePlanDetails.ErrorCode;
                    _errorDescription = availablePlanDetails.ErrorMessage;
                    //InsurancePlan plan = new InsurancePlan();
                    //plan.Error = availablePlanDetails.ErrorCode;
                    //plan.ErrorMessage = availablePlanDetails.ErrorMessage;
                    //availablePlans.Add(plan);
                }
                else
                {
                    string currency = availablePlanDetails.AvailablePlans[0].CurrencyCode;

                    if (ExchangeRates.ContainsKey(currency))
                    {
                        rateOfExchange = Convert.ToDouble(ExchangeRates[currency]);
                    }
                    else
                    {
                        rateOfExchange = 1;
                    }
                    foreach (ZEUS.PlanOTA plan in availablePlanDetails.AvailablePlans)
                    {
                        InsurancePlan insPlan = new InsurancePlan();
                        insPlan.Code = RemoveSpecialCharacters(plan.PlanCode);
                        insPlan.Description = RemoveSpecialCharacters(plan.PlanDesc);
                        insPlan.CurrencyCode = RemoveSpecialCharacters(plan.CurrencyCode);
                        insPlan.AdditionalInfoDesc = RemoveSpecialCharacters(plan.PlanAdditionalInfoDesc);
                        insPlan.AdditionalInfoTitle = RemoveSpecialCharacters(plan.PlanAdditionalInfoTitle);
                        insPlan.IsDefaultPlan = plan.IsDefaultPlan;
                        insPlan.NoConsideration = RemoveSpecialCharacters(plan.PlanNoConsideration);
                        insPlan.Content = RemoveSpecialCharacters(plan.PlanContent);
                        insPlan.NoDesc = RemoveSpecialCharacters(plan.PlanNoDesc);
                        insPlan.PremiumChargeType = plan.PlanPremiumChargeType;
                        insPlan.SSRFeeCode = RemoveSpecialCharacters(plan.SSRFeeCode);
                        insPlan.TermsAndConditions = RemoveSpecialCharacters(plan.PlanTnC);
                        insPlan.Title = RemoveSpecialCharacters(plan.PlanTitle).Replace("Plan Title", "");
                        insPlan.AgencyId = agencyId;
                        insPlan.ROE = rateOfExchange;
                        //if (insPlan.CurrencyCode.ToUpper() != "AED")
                        //{
                        //insPlan.TotalPremiumAmount = (double)GetRateOfExchange(insPlan.CurrencyCode) * plan.TotalPremiumAmount;
                        insPlan.TotalPremiumAmount = plan.TotalPremiumAmount * rateOfExchange;
                        //insPlan.CurrencyCode = "AED";
                        //}
                        //else
                        //{
                        //    insPlan.TotalPremiumAmount = plan.TotalPremiumAmount;
                        //}

                        /* agent Markup  Added by brahmam*/
                        DataTable dtAgentMarkup = new DataTable();
                        if (insPlan.AgencyId > 0)
                        {
                            dtAgentMarkup = CT.BookingEngine.UpdateMarkup.GetAllMarkup(insPlan.AgencyId, 5);
                        }

                        if (dtAgentMarkup != null && dtAgentMarkup.Rows.Count > 0)
                        {
                            dtAgentMarkup.DefaultView.RowFilter = "transType='B2B'";
                            int Totalpax = _adults + _childs;
                            if (dtAgentMarkup.DefaultView != null && dtAgentMarkup.DefaultView.Count > 0)
                            {
                                decimal agtMarkUp = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Markup"]);
                                decimal discount = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Discount"]);

                                if (Convert.ToString(dtAgentMarkup.DefaultView[0]["MarkupType"]) == "P")
                                {
                                    insPlan.Markup = (Convert.ToDecimal(insPlan.TotalPremiumAmount) * agtMarkUp) / 100;
                                }
                                else
                                {
                                    insPlan.Markup = agtMarkUp * Totalpax;
                                }
                                if (Convert.ToString(dtAgentMarkup.DefaultView[0]["DiscountType"]) == "P")
                                {
                                    insPlan.Discount = (Convert.ToDecimal(insPlan.TotalPremiumAmount) * discount) / 100;
                                }
                                else
                                {
                                    insPlan.Discount = discount * Totalpax;
                                }
                            }


                            if (transType == "B2C")
                            {
                                insPlan.TotalAmount = Convert.ToDecimal(insPlan.TotalPremiumAmount) + insPlan.Markup - insPlan.Discount;
                                //Add B2C Markup

                                dtAgentMarkup.DefaultView.RowFilter = "transType='B2C'";
                                if (dtAgentMarkup.DefaultView != null && dtAgentMarkup.DefaultView.Count > 0)
                                {
                                    decimal agtMarkUp = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Markup"]);
                                    decimal discount = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Discount"]);

                                    if (Convert.ToString(dtAgentMarkup.DefaultView[0]["MarkupType"]) == "P")
                                    {
                                        insPlan.B2CMarkup = (Convert.ToDecimal(insPlan.TotalAmount) * agtMarkUp) / 100;
                                    }
                                    else
                                    {
                                        insPlan.B2CMarkup = agtMarkUp * Totalpax;
                                    }
                                    if (Convert.ToString(dtAgentMarkup.DefaultView[0]["DiscountType"]) == "P")
                                    {
                                        insPlan.Discount = (Convert.ToDecimal(insPlan.TotalAmount) * discount) / 100;
                                    }
                                    else
                                    {
                                        insPlan.Discount = discount * Totalpax;
                                    }
                                }
                            }
                        }
                        insPlan.TotalAmount = Convert.ToDecimal(insPlan.TotalPremiumAmount) + insPlan.Markup - insPlan.Discount;
                        insPlan.YesDesc = RemoveSpecialCharacters(plan.PlanYesDesc);

                        List<MarketingPointer> marketingPointers = new List<MarketingPointer>();
                        foreach (ZEUS.PlanMarketingPointer pmp in plan.PlanMarketingPointers)
                        {
                            MarketingPointer mp = new MarketingPointer();
                            mp.PointId = pmp.PointID;
                            mp.Description = pmp.PointDesc;
                            marketingPointers.Add(mp);
                        }
                        insPlan.MarketingPointers = marketingPointers;

                        List<PassengerDetail> qualifiedPasengers = new List<PassengerDetail>();
                        foreach (ZEUS.PlanQualifiedPassenger pqp in plan.PlanQualifiedPassengers)
                        {
                            PassengerDetail pd = new PassengerDetail();
                            pd.FirstName = pqp.FirstName;
                            pd.LastName = pqp.LastName;
                            pd.DOB = Convert.ToDateTime(pqp.DOB);
                            pd.CurrencyCode = pqp.CurrencyCode;
                            pd.DocumentNo = pqp.IdentityNo;
                            pd.IsQualified = pqp.IsPassengerQualified;
                            pd.PremiumAmount = pqp.PassengerPremiumAmount;
                            qualifiedPasengers.Add(pd);
                        }

                        insPlan.QualifiedPassengers = qualifiedPasengers;
                        insPlan.PlanPriceBreakDown = new List<PriceBreakDown>();

                        foreach (ZEUS.PlanPricingBreakdown ppbd in plan.PlanPricingBreakdown)
                        {
                            PriceBreakDown pbd = new PriceBreakDown();
                            pbd.MinAge = ppbd.MinAge;
                            pbd.MaxAge = ppbd.MaxAge;
                            pbd.Gender = ppbd.Gender;
                            pbd.CurrencyCode = ppbd.CurrencyCode;
                            //if (ppbd.CurrencyCode != "AED")
                            //{
                            //pbd.PremiumAmount = (double)GetRateOfExchange(ppbd.CurrencyCode) * ppbd.PremiumAmount; //sai
                            pbd.PremiumAmount = rateOfExchange * ppbd.PremiumAmount;
                            //}
                            //else
                            //{
                            //    pbd.PremiumAmount = ppbd.PremiumAmount;
                            //}
                            pbd.Markup = insPlan.Markup / (_adults + _childs);
                            pbd.Discount = insPlan.Discount / (_adults + _childs);
                            pbd.TotalAmount = insPlan.TotalAmount / (_adults + _childs);
                            pbd.B2CMarkup = insPlan.B2CMarkup / (_adults + _childs);
                            insPlan.PlanPriceBreakDown.Add(pbd);
                        }
                        availablePlans.Add(insPlan);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return availablePlans;
        }

        private string RemoveSpecialCharacters(string value)
        {
            return value.Replace("CDATA", "").Replace("[", "").Replace("]", "").Replace("!", "");
        }

        public void GetPolicyDetails(ref InsurancePlan plan)
        {
            Connection();
            ZEUS.GetPolicyDetailsResponse policyDetailResponse = null;
            try
            {
                ZEUS.ZEUSTravelInsuranceGateway insGateway = new ZEUS.ZEUSTravelInsuranceGateway();

                ZEUS.ItineraryAuthentication authentication = new ZEUS.ItineraryAuthentication();
                authentication.Username = _userName;
                authentication.Password = _password;


                try
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(_pnr.GetType());
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                    ser.Serialize(writer, _pnr);  // Here Classes are converted to XML String. 
                    // This can be viewed in SB or writer.
                    // Above XML in SB can be loaded in XmlDocument object
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    doc.LoadXml(sb.ToString());
                    doc.Save(ConfigurationSystem.ZeusConfig["XmlLogPath"] + "ZeusInsGetPolicyDetailsREQ" + DateTime.Now.ToString("ddMMyyy_hhmmss") + ".xml");
                }
                catch
                {
                }


                policyDetailResponse = insGateway.GetPolicyDetails(authentication, _pnr);


                try
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(policyDetailResponse.GetType());
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                    ser.Serialize(writer, policyDetailResponse);  // Here Classes are converted to XML String. 
                    // This can be viewed in SB or writer.
                    // Above XML in SB can be loaded in XmlDocument object
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    doc.LoadXml(sb.ToString());
                    doc.Save(ConfigurationSystem.ZeusConfig["XmlLogPath"] + "ZeusInsGetPolicyDetailsRES" + DateTime.Now.ToString("ddMMyyy_hhmmss") + ".xml");
                }
                catch
                {
                }

                if (policyDetailResponse.ErrorCode != "0")
                {
                    plan.Error = policyDetailResponse.ErrorCode;
                    plan.ErrorMessage = policyDetailResponse.ErrorMessage;
                }
                else
                {
                    plan.ItineraryId = policyDetailResponse.ItineraryID;
                    plan.PNR = policyDetailResponse.PNR;
                    plan.PolicyNo = policyDetailResponse.PolicyNo;
                    plan.PolicyPurchasedDate = policyDetailResponse.PolicyPurchasedDateTime;

                    foreach (PassengerDetail pax in plan.QualifiedPassengers)
                    {
                        foreach (ZEUS.ConfirmedPassenger cpax in policyDetailResponse.ConfirmedPassengers)
                        {
                            if (cpax.IdentityNo == pax.DocumentNo)
                            {
                                pax.PolicyNo = cpax.PolicyNo;
                                pax.PolicyUrlLink = System.Web.HttpUtility.HtmlDecode(cpax.PolicyURLLink);
                            }
                        }
                    }

                    //Save the insurance Plan along with pax details

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public void ConfirmPolicyPurchase(ref InsurancePlan plan, string username)
        {
            Connection();

            ZEUS.ConfirmPurchaseResponse purchaseResponse = null;
            try
            {
                ZEUS.ZEUSTravelInsuranceGateway insGateway = new ZEUS.ZEUSTravelInsuranceGateway();

                ZEUS.InputRequest insPlanRequest = new ZEUS.InputRequest();

                //Authentication details            
                insPlanRequest.Authentication = new ZEUS.ItineraryAuthentication();
                insPlanRequest.Authentication.Username = _userName;
                insPlanRequest.Authentication.Password = _password;

                //Header details
                insPlanRequest.Header = new ZEUS.ItineraryHeader();
                insPlanRequest.Header.Channel = _channel + username;
                insPlanRequest.Header.ItineraryID = "0";
                insPlanRequest.Header.PNR = _pnr;
                insPlanRequest.Header.PolicyNo = string.Empty;
                insPlanRequest.Header.PurchaseDate = "2013-10-23 00:00:00";// For test access
                insPlanRequest.Header.SSRFeeCode = plan.SSRFeeCode;
                insPlanRequest.Header.FeeDescription = string.Empty;
                insPlanRequest.Header.TotalPremium = plan.TotalPremiumAmount;
                insPlanRequest.Header.Currency = _currencyCode;
                insPlanRequest.Header.CountryCode = _countryCode;
                insPlanRequest.Header.CultureCode = _cultureCode;
                insPlanRequest.Header.TotalAdults = _adults;
                insPlanRequest.Header.TotalChild = _childs;
                insPlanRequest.Header.TotalInfants = _infants;

                //Contact details
                insPlanRequest.ContactDetails = new ZEUS.ItineraryContact();
                insPlanRequest.ContactDetails.ContactPerson = _contactPersonName;
                insPlanRequest.ContactDetails.Address1 = string.Empty;
                insPlanRequest.ContactDetails.Address2 = string.Empty;
                insPlanRequest.ContactDetails.Address3 = string.Empty;
                insPlanRequest.ContactDetails.PostCode = string.Empty;
                insPlanRequest.ContactDetails.City = string.Empty;
                insPlanRequest.ContactDetails.State = string.Empty;
                insPlanRequest.ContactDetails.Country = string.Empty;
                insPlanRequest.ContactDetails.EmailAddress = _emailAddress;
                insPlanRequest.ContactDetails.HomePhoneNum = string.Empty;
                insPlanRequest.ContactDetails.MobilePhoneNum = string.Empty;
                insPlanRequest.ContactDetails.OtherPhoneNum = string.Empty;

                //Flight Details
                ZEUS.ItineraryFlight[] flightDetails = new ZEUS.ItineraryFlight[1];
                ZEUS.ItineraryFlight flightDetail = new ZEUS.ItineraryFlight();

                flightDetail.DepartCountryCode = _originCountryCode;
                flightDetail.DepartStationCode = _origin;
                flightDetail.ArrivalCountryCode = _destinationCountryCode;
                flightDetail.ArrivalStationCode = _destination;
                flightDetail.DepartAirlineCode = _originAirlineCode;
                flightDetail.DepartDateTime = _departureDateTime;
                flightDetail.ReturnAirlineCode = _originAirlineCode;
                flightDetail.ReturnDateTime = _returnDateTime;
                flightDetail.ReturnFlightNo = _returnFlightNo;
                flightDetail.DepartFlightNo = _originFlightNo;

                flightDetails[0] = flightDetail;

                insPlanRequest.Flights = flightDetails;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                ZEUS.ItineraryPassenger[] passengerDetails = new ZEUS.ItineraryPassenger[plan.QualifiedPassengers.Count];
                for (int i = 0; i < plan.QualifiedPassengers.Count; i++)
                {
                    PassengerDetail paxDetail = plan.QualifiedPassengers[i];
                    ZEUS.ItineraryPassenger passengerDetail = new ZEUS.ItineraryPassenger();
                    passengerDetail.FirstName = paxDetail.FirstName;
                    passengerDetail.LastName = paxDetail.LastName;
                    passengerDetail.Gender = paxDetail.Gender;
                    passengerDetail.DOB = paxDetail.DOB.ToString("yyyy-MM-dd 00:00:00");
                    passengerDetail.IsInfant = (paxDetail.IsInfant ? "1" : "0");
                    passengerDetail.Age = (Convert.ToDateTime(_departureDateTime, dateFormat).Subtract(paxDetail.DOB).Days / 365).ToString();
                    passengerDetail.IdentityType = paxDetail.DocumentType;
                    passengerDetail.IdentityNo = paxDetail.DocumentNo;
                    passengerDetail.CountryOfResidence = paxDetail.Country.Split('-')[1];
                    passengerDetail.Nationality = paxDetail.Nationality;
                    passengerDetail.CurrencyCode = plan.CurrencyCode;
                    passengerDetail.SelectedPlanCode = plan.Code;
                    passengerDetail.SelectedSSRFeeCode = plan.SSRFeeCode;
                    passengerDetail.PassengerPremiumAmount = paxDetail.PremiumAmount;
                    passengerDetail.IsQualified = paxDetail.IsQualified;
                    passengerDetails[i] = passengerDetail;
                }
                insPlanRequest.Passengers = passengerDetails;

                try
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(insPlanRequest.GetType());
                    string filePath = ConfigurationSystem.ZeusConfig["XmlLogPath"] + "ZeusInsConfirmPolicyRequest_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + ".xml";
                    System.IO.StreamWriter writer = new System.IO.StreamWriter(filePath);
                    ser.Serialize(writer, insPlanRequest);  // Here Classes are converted to XML String. 
                    writer.Close();
                    ser = null;
                    CT.Core.Audit.Add(CT.Core.EventType.FlyDubaiBooking, CT.Core.Severity.Normal, 0, filePath, "");
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, "Failed to Save Zeus Confirm Policy Request. Reason :" + ex.ToString(), HttpContext.Current.Request["REMOTE_ADDR"]);
                }

                purchaseResponse = insGateway.ConfirmPurchase(insPlanRequest);


                try
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(purchaseResponse.GetType());
                    string filePath = ConfigurationSystem.ZeusConfig["XmlLogPath"] + "ZeusInsConfirmPolicyResponse_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + ".xml";
                    System.IO.StreamWriter writer = new System.IO.StreamWriter(filePath);
                    ser.Serialize(writer, purchaseResponse);  // Here Classes are converted to XML String. 

                    writer.Close();
                    ser = null;
                    CT.Core.Audit.Add(CT.Core.EventType.FlyDubaiBooking, CT.Core.Severity.Normal, 0, filePath, "");
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, "Failed to Save Zeus Confirm Policy Response. Reason :" + ex.ToString(), HttpContext.Current.Request["REMOTE_ADDR"]);
                }



                if (purchaseResponse.ErrorCode != "0")
                {
                    plan.Error = purchaseResponse.ErrorCode;
                    plan.ErrorMessage = purchaseResponse.ErrorMessage;
                }
                else
                {
                    plan.ItineraryId = purchaseResponse.ItineraryID;
                    plan.PNR = purchaseResponse.PNR;
                    plan.PolicyNo = purchaseResponse.PolicyNo;
                    plan.PolicyPurchasedDate = purchaseResponse.PolicyPurchasedDateTime;
                    plan.ProposalState = purchaseResponse.ProposalState;

                    foreach (PassengerDetail pax in plan.QualifiedPassengers)
                    {
                        foreach (ZEUS.ConfirmedPassenger cpax in purchaseResponse.ConfirmedPassengers)
                        {
                            if (cpax.IdentityNo == pax.DocumentNo)
                            {
                                pax.PolicyNo = cpax.PolicyNo;
                                pax.PolicyUrlLink = System.Web.HttpUtility.HtmlDecode(cpax.PolicyURLLink);
                                pax.IsQualified = true;
                            }
                        }
                    }

                    //Save the insurance Plan along with pax details
                    //plan.ConnectionString = _connectionString;
                    plan.Save();
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, ex.Message, "");
                throw ex;
            }

        }


        public void CancelPolicy(ref InsurancePlan plan)
        {
            Connection();
            ZEUS.ZEUSTravelInsuranceGateway insGateway = new ZEUS.ZEUSTravelInsuranceGateway();

            //Authentication details            
            ZEUS.ItineraryAuthentication Authentication = new ZEUS.ItineraryAuthentication();
            Authentication.Username = _userName;
            Authentication.Password = _password;

            ZEUS.PolicyHeader Header = new ZEUS.PolicyHeader();
            Header.ItineraryID = plan.ItineraryId;
            Header.PNR = plan.PNR;
            Header.PolicyNo = plan.PolicyNo;
            Header.PurchaseDate = plan.PolicyPurchasedDate;

            try
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(Header.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, Header);  // Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(ConfigurationSystem.ZeusConfig["XmlLogPath"] + "ZeusInsCancelPolicyREQ" + DateTime.Now.ToString("ddMMyyy_hhmmss") + ".xml");
            }
            catch
            {
            }

            ZEUS.PolicyResponse Response = insGateway.CancelPolicy(Authentication, Header);

            try
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(Response.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, Response);  // Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(ConfigurationSystem.ZeusConfig["XmlLogPath"] + "ZeusInsCancelPolicyRES" + DateTime.Now.ToString("ddMMyyy_hhmmss") + ".xml");
            }
            catch
            {
            }

            if (Response != null)
            {

                if (Response.ErrorCode != "0")
                {
                    plan.Error = Response.ErrorCode;
                    plan.ErrorMessage = Response.ErrorMessage;
                }
                else
                {
                    plan.RefundDateTime = Convert.ToDateTime(Response.PolicyRefundDateTime);
                    plan.PNR = Response.PNR;
                    plan.CancelDateTime = Convert.ToDateTime(Response.PolicyCancelDateTime);
                    plan.ItineraryState = Response.ItineraryState;
                    plan.ItineraryId = Response.ItineraryID;

                    plan.Update();
                }

            }
        }

        public InsurancePlan RetrieveConfirmedPlan(long planId)
        {
            InsurancePlan plan = new InsurancePlan();

            try
            {
                plan.Id = planId;

                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_PlanId", planId);

                DataSet dsPlan = CT.TicketReceipt.DataAccessLayer.DBGateway.FillSP("usp_RetreiveZeusInsurance", paramList);

                if (dsPlan != null && dsPlan.Tables.Count > 0 && dsPlan.Tables[0].Rows.Count > 0)
                {
                    DataTable dtPlan = dsPlan.Tables[0];
                    DataRow row = dtPlan.Rows[0];
                    if (row["Adults"] != DBNull.Value)
                    {
                        plan.Adults = Convert.ToInt32(row["Adults"]);
                    }
                    if (row["Childs"] != DBNull.Value)
                    {
                        plan.Childs = Convert.ToInt32(row["Childs"]);
                    }
                    if (row["Infants"] != DBNull.Value)
                    {
                        plan.Infants = Convert.ToInt32(row["Infants"]);
                    }
                    if (row["PlanTitle"] != DBNull.Value)
                    {
                        plan.Title = row["PlanTitle"].ToString();
                    }
                    if (row["PlanDescription"] != DBNull.Value)
                    {
                        plan.Description = row["PlanDescription"].ToString();
                    }
                    if (row["InsPlanCode"] != DBNull.Value)
                    {
                        plan.Code = row["InsPlanCode"].ToString();
                    }
                    if (row["PolicyNo"] != DBNull.Value)
                    {
                        plan.PolicyNo = row["PolicyNo"].ToString();
                    }
                    if (row["ProposalState"] != DBNull.Value)
                    {
                        switch (row["ProposalState"].ToString())
                        {
                            case "CANCELLED":
                                plan.ProposalState = global::ZeusInsB2B.Zeus.ProposalStatus.CANCELLED;
                                break;
                            case "CONFIRMED":
                                plan.ProposalState = global::ZeusInsB2B.Zeus.ProposalStatus.CONFIRMED;
                                break;
                            case "PROPOSAL":
                                plan.ProposalState = global::ZeusInsB2B.Zeus.ProposalStatus.PROPOSAL;
                                break;
                            case "ONHOLD":
                                plan.ProposalState = global::ZeusInsB2B.Zeus.ProposalStatus.ONHOLD;
                                break;
                        }
                    }
                    if (row["PNR"] != DBNull.Value)
                    {
                        plan.PNR = row["PNR"].ToString();
                    }
                    if (row["Origin"] != DBNull.Value)
                    {
                        plan.Origin = row["Origin"].ToString();
                    }
                    if (row["OriginCountryCode"] != DBNull.Value)
                    {
                        plan.OriginCountryCode = row["OriginCountryCode"].ToString();
                    }
                    if (row["OriginAirlineCode"] != DBNull.Value)
                    {
                        plan.OriginAirlineCode = row["OriginAirlineCode"].ToString();
                    }
                    if (row["OriginFlightNo"] != DBNull.Value)
                    {
                        plan.OriginFlightNo = row["OriginFlightNo"].ToString();
                    }

                    if (row["Destination"] != DBNull.Value)
                    {
                        plan.Destination = row["Destination"].ToString();
                    }
                    if (row["DestinationCountryCode"] != DBNull.Value)
                    {
                        plan.DestinationCountryCode = row["DestinationCountryCode"].ToString();
                    }
                    if (row["DestinationAirlineCode"] != DBNull.Value)
                    {
                        plan.DestinationAirlineCode = row["DestinationAirlineCode"].ToString();
                    }
                    if (row["DestinationFlightNo"] != DBNull.Value)
                    {
                        plan.DestinationFlightNo = row["DestinationFlightNo"].ToString();
                    }
                    if (row["ArrivalDate"] != DBNull.Value)
                    {
                        plan.ReturnDateTime = row["ArrivalDate"].ToString();
                    }
                    if (row["DepartureDate"] != DBNull.Value)
                    {
                        plan.DepartureDateTime = row["DepartureDate"].ToString();
                    }
                    if (row["ItineraryID"] != DBNull.Value)
                    {
                        plan.ItineraryId = row["ItineraryID"].ToString();
                    }
                    if (row["PolicyPurchasedDate"] != DBNull.Value)
                    {
                        plan.PolicyPurchasedDate = row["PolicyPurchasedDate"].ToString();
                    }
                    if (row["SSRFeeCode"] != DBNull.Value)
                    {
                        plan.SSRFeeCode = row["SSRFeeCode"].ToString();
                    }
                    if (row["CurrencyCode"] != DBNull.Value)
                    {
                        plan.CurrencyCode = row["CurrencyCode"].ToString();
                    }
                    if (row["PremiumAmount"] != DBNull.Value)
                    {
                        plan.TotalPremiumAmount = Convert.ToDouble(row["PremiumAmount"]);
                    }
                    if (row["PremiumChargeType"] != DBNull.Value)
                    {
                        if (row["PremiumChargeType"].ToString() == "PerPassenger")
                        {
                            plan.PremiumChargeType = global::ZeusInsB2B.Zeus.PlanOTAChargeType.PerPassenger;
                        }
                        else
                        {
                            plan.PremiumChargeType = global::ZeusInsB2B.Zeus.PlanOTAChargeType.PerBooking;
                        }
                    }
                    if (row["ErrorCode"] != DBNull.Value)
                    {
                        plan.Error = row["ErrorCode"].ToString();
                    }
                    if (row["ErrorDesc"] != DBNull.Value)
                    {
                        plan.ErrorMessage = row["ErrorDesc"].ToString();
                    }
                    if (row["ItineraryState"] != DBNull.Value)
                    {
                        plan.ItineraryState = row["ItineraryState"].ToString();
                    }
                    if (row["RefundDateTime"] != DBNull.Value)
                    {
                        plan.RefundDateTime = Convert.ToDateTime(row["RefundDateTime"]);
                    }
                    if (row["CancelDateTime"] != DBNull.Value)
                    {
                        plan.CancelDateTime = Convert.ToDateTime(row["CancelDateTime"]);
                    }
                    if (row["CreatedBy"] != DBNull.Value)
                    {
                        plan.CreatedBy = Convert.ToInt32(row["CreatedBy"]);
                    }
                    if (row["CreatedOn"] != DBNull.Value)
                    {
                        plan.CreatedOn = Convert.ToDateTime(row["CreatedOn"]);
                    }
                    if (row["agencyId"] != DBNull.Value)
                    {
                        plan.AgencyId = Convert.ToInt32(row["agencyId"]);
                    }
                    if (row["Markup"] != DBNull.Value)
                    {
                        plan.Markup = Convert.ToDecimal(row["Markup"]);
                    }
                    if (row["B2CMarkup"] != DBNull.Value)
                    {
                        plan.B2CMarkup = Convert.ToDecimal(row["B2CMarkup"]);
                    }
                    if (row["Discount"] != DBNull.Value)
                    {
                        plan.Discount = Convert.ToDecimal(row["Discount"]);
                    }
                    if (row["TotalAmount"] != DBNull.Value)
                    {
                        plan.TotalAmount = Convert.ToDecimal(row["TotalAmount"]);
                    }
                    if (row["BookingSource"] != null)
                    {
                        plan.BookingSource = row["BookingSource"].ToString();
                    }


                    DataTable dtPassengers = dsPlan.Tables[1];
                    List<PassengerDetail> passengers = new List<PassengerDetail>();
                    foreach (DataRow paxrow in dtPassengers.Rows)
                    {
                        PassengerDetail pax = new PassengerDetail();
                        if (paxrow["Ins_Det_Id"] != DBNull.Value)
                        {
                            pax.Id = Convert.ToInt32(paxrow["Ins_Det_Id"]);
                        }
                        if (paxrow["FirstName"] != DBNull.Value)
                        {
                            pax.FirstName = paxrow["FirstName"].ToString();
                        }
                        if (paxrow["LastName"] != DBNull.Value)
                        {
                            pax.LastName = paxrow["LastName"].ToString();
                        }
                        if (paxrow["PaxType"] != DBNull.Value)
                        {
                            pax.PaxType = paxrow["PaxType"].ToString();
                        }
                        if (paxrow["Gender"] != DBNull.Value)
                        {
                            if (paxrow["Gender"].ToString() == "Male")
                            {
                                pax.Gender = global::ZeusInsB2B.Zeus.GenderType.Male;
                            }
                            else
                            {
                                pax.Gender = global::ZeusInsB2B.Zeus.GenderType.Female;
                            }
                        }
                        if (paxrow["DOB"] != DBNull.Value)
                        {
                            pax.DOB = Convert.ToDateTime(paxrow["DOB"]);
                        }
                        if (paxrow["City"] != DBNull.Value)
                        {
                            pax.City = paxrow["City"].ToString();
                        }
                        if (paxrow["State"] != DBNull.Value)
                        {
                            pax.State = paxrow["State"].ToString();
                        }
                        if (paxrow["Country"] != DBNull.Value)
                        {
                            pax.Country = paxrow["Country"].ToString();
                        }
                        if (paxrow["Nationality"] != DBNull.Value)
                        {
                            pax.Nationality = paxrow["Nationality"].ToString();
                        }
                        if (paxrow["Email"] != DBNull.Value)
                        {
                            pax.Email = paxrow["Email"].ToString();
                        }
                        if (paxrow["PhoneNumber"] != DBNull.Value)
                        {
                            pax.PhoneNumber = paxrow["PhoneNumber"].ToString();
                        }
                        if (paxrow["DocumentType"] != DBNull.Value)
                        {
                            if (paxrow["DocumentType"].ToString() == "Passport")
                            {
                                pax.DocumentType = global::ZeusInsB2B.Zeus.IdentificationType.Passport;
                            }
                            else
                            {
                                pax.DocumentType = global::ZeusInsB2B.Zeus.IdentificationType.IdentificationCard;
                            }
                        }
                        if (paxrow["DocumentNo"] != DBNull.Value)
                        {
                            pax.DocumentNo = paxrow["DocumentNo"].ToString();
                        }
                        if (paxrow["PolicyNo"] != DBNull.Value)
                        {
                            pax.PolicyNo = paxrow["PolicyNo"].ToString();
                        }
                        if (paxrow["PolicyUrlLinks"] != DBNull.Value)
                        {
                            pax.PolicyUrlLink = paxrow["PolicyUrlLinks"].ToString();
                        }
                        if (paxrow["Remarks"] != DBNull.Value)
                        {
                            pax.Remarks = paxrow["Remarks"].ToString();
                        }
                        if (paxrow["TotalAmount"] != DBNull.Value)
                        {
                            pax.TotalAmount = Convert.ToDecimal(paxrow["TotalAmount"]);
                        }
                        if (paxrow["PremiumAmount"] != DBNull.Value)
                        {
                            pax.PremiumAmount = Convert.ToDouble(paxrow["PremiumAmount"]);
                        }
                        if (paxrow["Markup"] != DBNull.Value)
                        {
                            pax.Markup = Convert.ToDecimal(paxrow["Markup"]);
                        }
                        if (row["B2CMarkup"] != DBNull.Value)
                        {
                            pax.B2CMarkup = Convert.ToDecimal(paxrow["B2CMarkup"]);
                        }
                        if (paxrow["Discount"] != DBNull.Value)
                        {
                            pax.Discount = Convert.ToDecimal(paxrow["Discount"]);
                        }
                        //pax.PremiumAmount = plan.TotalPremiumAmount;

                        passengers.Add(pax);
                    }

                    plan.QualifiedPassengers = passengers;

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }

            return plan;
        }

        private decimal GetRateOfExchange(string currencyCode)
        {
            decimal ROE = 1;

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_CurrencyCode", currencyCode);
                DataTable dtROE = CT.TicketReceipt.DataAccessLayer.DBGateway.FillDataTableSP("usp_RateOfExchange", paramList);

                if (dtROE != null && dtROE.Rows.Count > 0)
                {
                    ROE = Convert.ToDecimal(dtROE.Rows[0]["rateOfExchange"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ROE;
        }
    }


}
