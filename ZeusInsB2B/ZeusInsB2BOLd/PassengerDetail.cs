﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using ZEUS = ZeusInsB2B.Zeus;

namespace CT.BookingEngine.Insurance
{
    public class PassengerDetail
    {
        #region Members
        long _detailId;
        long _headerId;
        string _firstName;
        string _lastName;
        string _paxType;
        ZEUS.GenderType _gender;
        DateTime _dob;
        string _phoneNumber;
        ZEUS.IdentificationType _documentType;
        string _documentNo;
        string _city;
        string _state;
        string _country;
        string _email;
        string _nationality;
        bool _isInfant;
        //Returned from Available Plans response
        bool _isQualified;
        string _currencyCode;
        double _premiumAmount;
        string _policyNo;
        string _policyUrlLink;
        string _remarks;
        private string _connectionString;
        string _tempDOB;
        string _title;
        private decimal _markup;
        private decimal _disount;
        private decimal _totalAmount;
        private int _decimalValue;
        private string _supplierCurrency;
        private double _roe;
        private decimal _supplierAmount;
        private string _eligible;
        private decimal _b2cMarkup;
        #endregion

        #region Properties

        public long Id
        {
            get { return _detailId; }
            set { _detailId = value; }
        }
        public long HeaderId
        {
            get { return _headerId; }
            set { _headerId = value; }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string PaxType
        {
            get { return _paxType; }
            set { _paxType = value; }
        }
        public ZEUS.GenderType Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
        public DateTime DOB
        {
            get { return _dob; }
            set { _dob = value; }
        }
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }
        public ZEUS.IdentificationType DocumentType
        {
            get { return _documentType; }
            set { _documentType = value; }
        }
        public string DocumentNo
        {
            get { return _documentNo; }
            set { _documentNo = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string Nationality
        {
            get { return _nationality; }
            set { _nationality = value; }
        }
        public bool IsInfant
        {
            get { return _isInfant; }
            set { _isInfant = value; }
        }

        /// <summary>
        /// Returned from Available Insurance Plan Response.
        /// </summary>
        public bool IsQualified
        {
            get { return _isQualified; }
            set { _isQualified = value; }
        }

        /// <summary>
        /// Returned from Available Insurance Plan Response.
        /// </summary>
        public string CurrencyCode
        {
            get { return _currencyCode; }
            set { _currencyCode = value; }
        }
        /// <summary>
        /// Returned from Available Insurance Plan Response.
        /// </summary>
        public double PremiumAmount
        {
            get { return _premiumAmount; }
            set { _premiumAmount = value; }
        }
        /// <summary>
        /// Confirmed PolicyNo applicable
        /// </summary>
        public string PolicyNo
        {
            get { return _policyNo; }
            set { _policyNo = value; }
        }
        /// <summary>
        /// URL Link to download Covered Person’s Policy Certificate in PDF or HTML format.
        /// </summary>
        public string PolicyUrlLink
        {
            get { return _policyUrlLink; }
            set { _policyUrlLink = value; }
        }

        public string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        public string Remarks
        {
            get { return _remarks; }
            set { _remarks = value; }
        }
        public string TempDOB
        {
            get { return _tempDOB; }
            set { _tempDOB = value; }
        }
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        public decimal Markup
        {
            get { return _markup; }
            set { _markup = value; }
        }
        public decimal Discount
        {
            get { return _disount; }
            set { _disount = value; }
        }
        public decimal TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }
        public int DecimalValue
        {
            get { return _decimalValue; }
            set { _decimalValue = value; }
        }
        public string SupplierCurrency
        {
            get { return _supplierCurrency; }
            set { _supplierCurrency = value; }
        }
        public double ROE
        {
            get { return _roe; }
            set { _roe = value; }
        }
        public decimal SupplierAmount
        {
            get { return _supplierAmount; }
            set { _supplierAmount = value; }
        }
        public string Eligible
        {
            get { return _eligible; }
            set { _eligible = value; }
        }
        public decimal B2CMarkup
        {
            get { return _b2cMarkup; }
            set { _b2cMarkup = value; }
        }
        #endregion

        public void Save()
        {
            try
            {

                //DBGateway.ConnectionString = _connectionString;
                SqlParameter[] paramList = new SqlParameter[26];
                paramList[0] = new SqlParameter("@P_Ins_Hdr_Id", _headerId);
                paramList[1] = new SqlParameter("@P_FirstName", _firstName);
                paramList[2] = new SqlParameter("@P_LastName", _lastName);
                paramList[3] = new SqlParameter("@P_PaxType", _paxType);
                paramList[4] = new SqlParameter("@P_Gender", _gender.ToString());
                paramList[5] = new SqlParameter("@P_DOB", _dob);
                paramList[6] = new SqlParameter("@P_City", _city);
                paramList[7] = new SqlParameter("@P_State", _state);
                paramList[8] = new SqlParameter("@P_Country", _country);
                paramList[9] = new SqlParameter("@P_Email", _email);
                paramList[10] = new SqlParameter("@P_PhoneNumber", _phoneNumber);
                paramList[11] = new SqlParameter("@P_DocumentType", _documentType.ToString());
                paramList[12] = new SqlParameter("@P_DocumentNo", _documentNo);
                paramList[13] = new SqlParameter("@P_PolicyNo", _policyNo);
                paramList[14] = new SqlParameter("@P_PolicyUrlLinks", _policyUrlLink);
                paramList[15] = new SqlParameter("@P_Nationality", _nationality);
                paramList[16] = new SqlParameter("@P_PremiumAmount", _premiumAmount);
                paramList[17] = new SqlParameter("@P_Remarks", _remarks);
                paramList[18] = new SqlParameter("@P_Markup", Math.Round(_markup, _decimalValue));
                paramList[19] = new SqlParameter("@P_Discount", Math.Round(_disount, _decimalValue));
                paramList[20] = new SqlParameter("@P_TotalAmount", Math.Round(_totalAmount, _decimalValue));
                paramList[21] = new SqlParameter("@P_SupplierCurrency", _supplierCurrency);
                paramList[22] = new SqlParameter("@P_SupplierAmount", _supplierAmount);
                paramList[23] = new SqlParameter("@P_ROE", _roe);
                paramList[24] = new SqlParameter("@P_Eligible", _eligible);
                paramList[25] = new SqlParameter("@P_B2CMarkup", Math.Round(_b2cMarkup, _decimalValue));
                 DBGateway.ExecuteNonQuerySP("usp_AddInsuredPassengers", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
