﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using ZEUS = ZeusInsB2B.Zeus;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine.Insurance
{
    public class InsurancePlan
    {
        #region Members
        private long _Id;
        private string _policyNo;
        private string _code;
        private string _ssrFeeCode;
        private string _currencyCode;
        private ZEUS.PlanOTAChargeType _premiumChargeType;
        private string _title;
        private string _description;
        private double _totalPremiumAmount;
        private string _additionalInfoTitle;
        private string _additionalInfoDesc;
        private string _content;
        private string _yesDesc; //Plan’s Description for Yes Button
        private string _noDesc; //Plan’s Description for No Button
        private string _noConsideration; //Plan’s Description for any Popup Messages upon clicking the No Button
        private string _termsAndConditions;
        private bool _isDefaultPlan; //Whether to display this Insurance Plan as Default or First, when more than 2 Plans are available.
        private ZEUS.ProposalStatus _proposalState;
        private string _itineraryId;
        private string _pnr;
        private string _policyPurchasedDate;
        string _originCountryCode;
        string _destinationCountryCode;
        string _origin;
        string _destination;
        string _originAirlineCode;
        string _destinationAirlineCode;
        string _originFlightNo;
        string _destinationFlightNo;
        string _departureDateTime;
        string _returnAirlineCode;
        string _returnDateTime;
        string _returnFlightNo;
        int _adults;
        int _childs;
        int _infants;
        private string _error;
        private string _errorMessage;
        private string _itineraryState;
        private DateTime _refundDateTime;
        private DateTime _cancelDateTime;

        private List<MarketingPointer> _marketingPointers;
        private List<PassengerDetail> _qualifiedPassengers;
        private List<PriceBreakDown> _planPriceBreakDown;
        private List<PassengerDetail> _unQualifiedPassengers;

        private decimal _markup;
        private decimal _disount;
        private decimal _totalAmount;
        private int _decimalValue;
        private double _roe;
        private string _connectionString;
        private decimal _b2cMarkup;
        /// <summary>
        /// BookingId for Flight/Hotel
        /// </summary>
        long _bookingId;
        /// <summary>
        /// Booking Source B2B/B2C
        /// </summary>
        string _bookingSource;
        /// <summary>
        /// Product (S: Standalone, F: Flight, H: Hotel) 
        /// </summary>
        string _product;

        /// <summary>
        /// 
        /// </summary>
        int _agencyId;
        /// <summary>
        /// 
        /// </summary>
        long _locationId;
        /// <summary>
        /// 
        /// </summary>
        int _createdBy;

        DateTime _createdOn;
        #endregion

        #region Properties

        /// <summary>
        /// Code for the Plan
        /// </summary>
        public long Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string PolicyNo
        {
            get { return _policyNo; }
            set { _policyNo = value; }
        }
        /// <summary>
        /// Code for the Plan
        /// </summary>
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        /// <summary>
        /// Plan’s Special Service Request or Fee Code
        /// </summary>
        public string SSRFeeCode
        {
            get { return _ssrFeeCode; }
            set { _ssrFeeCode = value; }
        }

        /// <summary>
        /// Plan’s Currency Code to Charge.
        /// </summary>
        public string CurrencyCode
        {
            get { return _currencyCode; }
            set { _currencyCode = value; }
        }

        /// <summary>
        /// PerBooking – Total 1 Premium Amount for all passengers
        /// PerPassenger – Each passenger is charged individually
        /// </summary>
        public ZEUS.PlanOTAChargeType PremiumChargeType
        {
            get { return _premiumChargeType; }
            set { _premiumChargeType = value; }
        }

        /// <summary>
        /// Title of the Plan
        /// </summary>
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        /// <summary>
        /// Description
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// HTML Content used to display for the passengers page.
        /// </summary>
        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }

        /// <summary>
        /// Plan’s Total Premium Amount for all passengers.
        /// If PremiumChargeType = 'Per Passenger' then Amount = Amount * Passenger Count.
        /// Otherwise use the same Amount.
        /// </summary>
        public double TotalPremiumAmount
        {
            get { return _totalPremiumAmount; }
            set { _totalPremiumAmount = value; }
        }

        /// <summary>
        /// Additional Info Title Name
        /// </summary>
        public string AdditionalInfoTitle
        {
            get { return _additionalInfoTitle; }
            set { _additionalInfoTitle = value; }
        }

        /// <summary>
        /// Additional Info Description
        /// </summary>
        public string AdditionalInfoDesc
        {
            get { return _additionalInfoDesc; }
            set { _additionalInfoDesc = value; }
        }

        /// <summary>
        /// Plan’s Description for Yes Button
        /// </summary>
        public string YesDesc
        {
            get { return _yesDesc; }
            set { _yesDesc = value; }
        }

        /// <summary>
        /// Plan’s Description for No Button
        /// </summary>
        public string NoDesc
        {
            get { return _noDesc; }
            set { _noDesc = value; }
        }

        /// <summary>
        /// Plan’s Description for any Popup Messages upon clicking the No Button
        /// </summary>
        public string NoConsideration
        {
            get { return _noConsideration; }
            set { _noConsideration = value; }
        }

        /// <summary>
        /// Terms and Conditions for the Plan
        /// </summary>
        public string TermsAndConditions
        {
            get { return _termsAndConditions; }
            set { _termsAndConditions = value; }
        }

        /// <summary>
        /// Whether to display this Insurance Plan as Default or First, when more than 2 Plans are available.
        /// </summary>
        public bool IsDefaultPlan
        {
            get { return _isDefaultPlan; }
            set { _isDefaultPlan = value; }
        }

        public ZEUS.ProposalStatus ProposalState
        {
            get { return _proposalState; }
            set { _proposalState = value; }
        }
        public string ItineraryId
        {
            get { return _itineraryId; }
            set { _itineraryId = value; }
        }
        public string PNR
        {
            get { return _pnr; }
            set { _pnr = value; }
        }
        public string PolicyPurchasedDate
        {
            get { return _policyPurchasedDate; }
            set { _policyPurchasedDate = value; }
        }
        public string OriginCountryCode
        {
            get { return _originCountryCode; }
            set { _originCountryCode = value; }
        }
        public string DestinationCountryCode
        {
            get { return _destinationCountryCode; }
            set { _destinationCountryCode = value; }
        }
        public string Origin
        {
            get { return _origin; }
            set { _origin = value; }
        }
        public string Destination
        {
            get { return _destination; }
            set { _destination = value; }
        }
        public string OriginAirlineCode
        {
            get { return _originAirlineCode; }
            set { _originAirlineCode = value; }
        }
        public string DestinationAirlineCode
        {
            get { return _destinationAirlineCode; }
            set { _destinationAirlineCode = value; }
        }
        public string OriginFlightNo
        {
            get { return _originFlightNo; }
            set { _originFlightNo = value; }
        }
        public string DestinationFlightNo
        {
            get { return _destinationFlightNo; }
            set { _destinationFlightNo = value; }
        }
        public string DepartureDateTime
        {
            get { return _departureDateTime; }
            set { _departureDateTime = value; }
        }
        public string ReturnFlightNo
        {
            get { return _returnFlightNo; }
            set { _returnFlightNo = value; }
        }
        public string ReturnAirlineCode
        {
            get { return _returnAirlineCode; }
            set { _returnAirlineCode = value; }
        }
        public string ReturnDateTime
        {
            get { return _returnDateTime; }
            set { _returnDateTime = value; }
        }
        public int Adults
        {
            get { return _adults; }
            set { _adults = value; }
        }
        public int Childs
        {
            get { return _childs; }
            set { _childs = value; }
        }
        public int Infants
        {
            get { return _infants; }
            set { _infants = value; }
        }
        /// <summary>
        /// Error code returned from the web service
        /// </summary>
        public string Error
        {
            get { return _error; }
            set { _error = value; }
        }

        /// <summary>
        /// Error message returned from the web service
        /// </summary>
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

        public string ItineraryState
        {
            get { return _itineraryState; }
            set { _itineraryState = value; }
        }

        public DateTime RefundDateTime
        {
            get { return _refundDateTime; }
            set { _refundDateTime = value; }
        }

        public DateTime CancelDateTime
        {
            get { return _cancelDateTime; }
            set { _cancelDateTime = value; }
        }

        public List<MarketingPointer> MarketingPointers
        {
            get { return _marketingPointers; }
            set { _marketingPointers = value; }
        }

        public List<PassengerDetail> QualifiedPassengers
        {
            get { return _qualifiedPassengers; }
            set { _qualifiedPassengers = value; }
        }

        public List<PassengerDetail> UnQualifiedPassengers
        {
            get { return _unQualifiedPassengers; }
            set { _unQualifiedPassengers = value; }
        }

        public List<PriceBreakDown> PlanPriceBreakDown
        {
            get { return _planPriceBreakDown; }
            set { _planPriceBreakDown = value; }
        }

        public string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        /// <summary>
        /// BookingId created for Flight/Hotel Booking
        /// </summary>
        public long BookingId
        {
            get { return _bookingId; }
            set { _bookingId = value; }
        }

        /// <summary>
        /// Booking Source (B2C or B2B)
        /// </summary>
        /// <example>
        /// BookingSource = "B2C" or "B2B";
        /// </example>
        public string BookingSource
        {
            get { return _bookingSource; }
            set { _bookingSource = value; }
        }

        /// <summary>
        /// Product Type (S: Standalone, F: Flight, H: Hotel)
        /// </summary>
        public string Product
        {
            get { return _product; }
            set { _product = value; }
        }

        public int AgencyId
        {
            get { return _agencyId; }
            set { _agencyId = value; }
        }

        public long LocationId
        {
            get { return _locationId; }
            set { _locationId = value; }
        }

        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public decimal Markup
        {
            get { return _markup; }
            set { _markup = value; }
        }
        public decimal Discount
        {
            get { return _disount; }
            set { _disount = value; }
        }
        public decimal TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }
        public decimal B2CMarkup
        {
            get { return _b2cMarkup; }
            set { _b2cMarkup = value; }
        }
        public DateTime CreatedOn
        {
            get { return _createdOn; }
            set { _createdOn = value; }
        }

        public int DecimalValue
        {
            get { return _decimalValue; }
            set { _decimalValue = value; }
        }
        public double ROE
        {
            get { return _roe; }
            set { _roe = value; }
        }
        #endregion

        public void Save()
        {
            SqlTransaction trans = null;
            try
            {

                trans = DBGateway.GetConnection().BeginTransaction();

                SqlParameter[] paramList = new SqlParameter[37];
                paramList[0] = new SqlParameter("@P_OriginCountryCode", _originCountryCode);
                paramList[1] = new SqlParameter("@P_DestinationCountryCode", _destinationCountryCode);
                paramList[2] = new SqlParameter("@P_Origin", _origin);
                paramList[3] = new SqlParameter("@P_Destination", _destination);
                paramList[4] = new SqlParameter("@P_OriginAirlineCode", _originAirlineCode);
                paramList[5] = new SqlParameter("@P_DestinationAirlineCode", _destinationAirlineCode);
                paramList[6] = new SqlParameter("@P_OriginFlightNo", _originFlightNo);
                paramList[7] = new SqlParameter("@P_DestinationFlightNo", _destinationFlightNo);
                paramList[8] = new SqlParameter("@P_Adults", _adults);
                paramList[9] = new SqlParameter("@P_Childs", _childs);
                paramList[10] = new SqlParameter("@P_Infants", _infants);
                paramList[11] = new SqlParameter("@P_ItineraryID", _itineraryId);
                paramList[12] = new SqlParameter("@P_PolicyNo", _policyNo);
                paramList[13] = new SqlParameter("@P_PolicyPurchasedDate", _policyPurchasedDate);
                paramList[14] = new SqlParameter("@P_ProposalState", _proposalState.ToString());
                paramList[15] = new SqlParameter("@P_PNR", _pnr);
                paramList[16] = new SqlParameter("@P_InsPlanCode", _code);
                paramList[17] = new SqlParameter("@P_SSRFeeCode", _ssrFeeCode);
                paramList[18] = new SqlParameter("@P_CurrencyCode", _currencyCode);
                paramList[19] = new SqlParameter("@P_PremiumAmount", Math.Round(_totalPremiumAmount, _decimalValue));
                paramList[20] = new SqlParameter("@P_PremiumChargeType", _premiumChargeType.ToString());
                paramList[21] = new SqlParameter("@P_PlanTitle", _title);
                paramList[22] = new SqlParameter("@P_PlanDescription", _description);
                paramList[23] = new SqlParameter("@P_ErrorCode", _error);
                paramList[24] = new SqlParameter("@P_ErrorDesc", _errorMessage);
                paramList[25] = new SqlParameter("@P_DepartureDate", _departureDateTime);
                paramList[26] = new SqlParameter("@P_ArrivalDate", _returnDateTime);
                paramList[27] = new SqlParameter("@P_HeaderId", _Id);
                paramList[27].Direction = ParameterDirection.Output;                
                paramList[28] = new SqlParameter("@P_BookingSource", _bookingSource);
                paramList[29] = new SqlParameter("@P_Product", _product);
                paramList[30] = new SqlParameter("@P_CreatedBy", _createdBy);
                paramList[31] = new SqlParameter("@P_AgencyId", _agencyId);
                paramList[32] = new SqlParameter("@P_locationId", _locationId);
                paramList[33] = new SqlParameter("@P_Markup", Math.Round(_markup, _decimalValue));
                paramList[34] = new SqlParameter("@P_Discount", Math.Round(_disount, _decimalValue));
                paramList[35] = new SqlParameter("@P_TotalAmount", Math.Round(_totalAmount, _decimalValue));
                paramList[36] = new SqlParameter("@P_B2CMarkup", Math.Round(_b2cMarkup, _decimalValue));
                int count = DBGateway.ExecuteNonQuerySP("usp_AddInsurancePlanHeader", paramList);

                if (count > 0 && paramList[27].Value != DBNull.Value)
                {
                    _Id = Convert.ToInt32(paramList[27].Value);
                }
                try
                {
                    foreach (MarketingPointer pointer in _marketingPointers)
                    {
                        pointer.HeaderId = _Id;
                        //pointer.ConnectionString = _connectionString;
                        pointer.Save();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                try
                {
                    foreach (PassengerDetail pax in _qualifiedPassengers)
                    {
                        pax.HeaderId = _Id;
                        pax.Eligible = "Y";
                        pax.DecimalValue = _decimalValue;
                        pax.Save();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                try
                {
                    if (_unQualifiedPassengers.Count > 0)
                    {
                        foreach (PassengerDetail pax in _unQualifiedPassengers)
                        {
                            pax.HeaderId = _Id;
                            pax.Eligible = "N";
                            pax.DecimalValue = _decimalValue;
                            pax.Save();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
        }

        /// <summary>
        /// Updates details when a Policy/Plan is cancelled.
        /// </summary>
        public void Update()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                paramList[0] = new SqlParameter("@P_HeaderId", _Id);
                if (_itineraryState != null && _itineraryState.Length > 0)
                    paramList[1] = new SqlParameter("@P_ItineraryState", _itineraryState);
                if (_refundDateTime != DateTime.MinValue)
                    paramList[2] = new SqlParameter("@P_RefundDateTime", _refundDateTime);
                if (_cancelDateTime != DateTime.MinValue)
                    paramList[3] = new SqlParameter("@P_CancelDateTime", _cancelDateTime);
                if (_proposalState != null)
                    paramList[4] = new SqlParameter("@P_ProposalState", _proposalState.ToString());
                int count = DBGateway.ExecuteNonQuerySP("usp_UpdateTravelInsurancePlan", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    /// <summary>
    /// Insurance Plan’s Key Points
    /// </summary>
    public class MarketingPointer
    {
        private long _pointId;
        private long _headerId;
        private string _description;
        private string _connectionString;

        /// <summary>
        /// Point Number
        /// </summary>
        public long PointId
        {
            get { return _pointId; }
            set { _pointId = value; }
        }

        public long HeaderId
        {
            get { return _headerId; }
            set { _headerId = value; }
        }
        /// <summary>
        /// Plan's Key Point
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_Ins_Key_PlanId", _pointId);
                paramList[1] = new SqlParameter("@P_HeaderId", _headerId);
                paramList[2] = new SqlParameter("@P_Description", _description);

                DBGateway.ExecuteNonQuerySP("usp_AddInsuranceMarketingPoints", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class PriceBreakDown
    {
        int _minAge;
        int _maxAge;
        string _gender;
        string _currecnyCode;
        double _premiumAmount;
        private decimal _markup;
        private decimal _disount;
        private decimal _totalAmount;
        private decimal _b2cMarkup;

        public int MinAge
        {
            get { return _minAge; }
            set { _minAge = value; }
        }

        public int MaxAge
        {
            get { return _maxAge; }
            set { _maxAge = value; }
        }

        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        public string CurrencyCode
        {
            get { return _currecnyCode; }
            set { _currecnyCode = value; }
        }

        public double PremiumAmount
        {
            get { return _premiumAmount; }
            set { _premiumAmount = value; }
        }
        public decimal Markup
        {
            get { return _markup; }
            set { _markup = value; }
        }
        public decimal Discount
        {
            get { return _disount; }
            set { _disount = value; }
        }
        public decimal TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }
        public decimal B2CMarkup
        {
            get { return _b2cMarkup; }
            set { _b2cMarkup = value; }
        }

    }


}
