﻿//using System.Linq;

using System;

namespace CT.BookingEngine.Insurance
{
    [Serializable]
    public class InsuranceRequest
    {
        #region Variables
        string _departureCountryCode;
        string _arrivalCountryCode;
        string _countryCode;
        string _cultureCode;
        string _departureStationCode;
        string _arrivalStationCode;
        string _departureAirlineCode;
        string _returnAirlineCode;
        string _departureDateTime;
        string _returnDateTime;
        string _departureFlightNo;
        string _returnFlightNo;
        int _adults;
        int _childs;
        int _infants;
        string _userName;
        string _transType;
        string _origin;
        string _destination;
        bool isSeniorCitizen=false;
        bool isDomesticSearch = false;//Added to check Domestic Travel for India Agency
        string pseudoCode = string.Empty;
        string currencyCode = string.Empty;
        string locationCountryCode = string.Empty;
        #endregion

        #region Properties
        public string DepartureCountryCode
        {
            get { return _departureCountryCode; }
            set { _departureCountryCode = value; }
        }
        public string ArrivalCountryCode
        {
            get { return _arrivalCountryCode; }
            set { _arrivalCountryCode = value; }
        }
        public string DepartureStationCode
        {
            get { return _departureStationCode; }
            set { _departureStationCode = value; }
        }
        public string ArrivalStationCode
        {
            get { return _arrivalStationCode; }
            set { _arrivalStationCode = value; }
        }
        public string DepartureAirlineCode
        {
            get { return _departureAirlineCode; }
            set { _departureAirlineCode = value; }
        }
        public string ReturnAirlineCode
        {
            get { return _returnAirlineCode; }
            set { _returnAirlineCode = value; }
        }
        public string DepartureFlightNo
        {
            get { return _departureFlightNo; }
            set { _departureFlightNo = value; }
        }
        public string DepartureDateTime
        {
            get { return _departureDateTime; }
            set { _departureDateTime = value; }
        }
        public string ReturnFlightNo
        {
            get { return _returnFlightNo; }
            set { _returnFlightNo = value; }
        }
        public string ReturnDateTime
        {
            get { return _returnDateTime; }
            set { _returnDateTime = value; }
        }
        public int Adults
        {
            get { return _adults; }
            set { _adults = value; }
        }
        public int Childs
        {
            get { return _childs; }
            set { _childs = value; }
        }
        public int Infants
        {
            get { return _infants; }
            set { _infants = value; }
        }
        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }
        public string CultureCode
        {
            get { return _cultureCode; }
            set { _cultureCode = value; }
        }
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        public string TransType
        {
            get { return _transType; }
            set { _transType = value; }
        }
        public string Origin
        {
            get { return _origin; }
            set { _origin = value; }
        }
        public string Destination
        {
            get { return _destination; }
            set { _destination = value; }
        }
        public bool IsSeniorCitizen
        {
            get { return isSeniorCitizen; }
            set { isSeniorCitizen = value; }
        }
        public bool IsDomesticSearch
        {
            get { return isDomesticSearch; }
            set { isDomesticSearch = value; }
        }
        //Added pseudo code & currency code for user level access
        public string PseudoCode
        {
            get { return pseudoCode; }
            set { pseudoCode = value; }
        }
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }     
        }

        public string LocationCountryCode
        {
            get { return locationCountryCode; }
            set { locationCountryCode = value; }
        }
        
        #endregion
    }
}
