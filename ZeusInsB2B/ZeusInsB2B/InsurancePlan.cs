﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine.Insurance
{
    [Serializable]
    public class InsurancePlan
    {
        #region Members
        int _planId;
        int _headerId;
        string _itineraryID;
        string _policyNo;
        DateTime _policyPurchasedDate;
        string _proposalState;
        string _insPlanCode;
        string _ssrFeeCode;
        string _currency;
        decimal _premiumAmount;
        string _premiumChargeType;
        string _planTitle;
        string _planDescription;
        DateTime _refundDateTime;
        DateTime _cancelDateTime;
        string _cancellationId;
        InsuranceBookingStatus _planStatus;
        decimal _netAmount;
        decimal _addFee;
        decimal _markup;
        decimal _markupValue;
        string _markupType;
        decimal _discount;
        decimal _discountValue;
        string _discountType;
        string _sourceCurrency;
        decimal _sourceAmount;
        decimal _roe;
        decimal _b2cMarkup;
        decimal _b2cMarkupValue;
        string _b2cMarkupType;
        string _errorCode;
        string _errorMessage;
        private PriceTaxDetails taxDetail;
        private decimal inputVATAmount;
        private decimal outputVATAmount;
        #endregion

        #region Properties


        public int PlanId
        {
            get { return _planId; }
            set { _planId = value; }
        }
        public int HeaderId
        {
            get { return _headerId; }
            set { _headerId = value; }
        }
        public string ItineraryID
        {
            get { return _itineraryID; }
            set { _itineraryID = value; }
        }
        public string PolicyNo
        {
            get { return _policyNo; }
            set { _policyNo = value; }
        }
        public DateTime PolicyPurchasedDate
        {
            get { return _policyPurchasedDate; }
            set { _policyPurchasedDate = value; }
        }

        public string ProposalState
        {
            get { return _proposalState; }
            set { _proposalState = value; }
        }

        public string InsPlanCode
        {
            get { return _insPlanCode; }
            set { _insPlanCode = value; }
        }
        public string SSRFeeCode
        {
            get { return _ssrFeeCode; }
            set { _ssrFeeCode = value; }
        }

        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
        public decimal PremiumAmount
        {
            get { return _premiumAmount; }
            set { _premiumAmount = value; }
        }

        public string PremiumChargeType
        {
            get { return _premiumChargeType; }
            set { _premiumChargeType = value; }
        }
        public string PlanTitle
        {
            get { return _planTitle; }
            set { _planTitle = value; }
        }

        public string PlanDescription
        {
            get { return _planDescription; }
            set { _planDescription = value; }
        }
        public DateTime RefundDateTime
        {
            get { return _refundDateTime; }
            set { _refundDateTime = value; }
        }
        public DateTime CancelDateTime
        {
            get { return _cancelDateTime; }
            set { _cancelDateTime = value; }
        }
        public string CancellationId
        {
            get { return _cancellationId; }
            set { _cancellationId = value; }
        }
        public InsuranceBookingStatus PlanStatus
        {
            get { return _planStatus; }
            set { _planStatus = value; }
        }

        public decimal NetAmount
        {
            get { return _netAmount; }
            set { _netAmount = value; }
        }
        public decimal AddFee
        {
            get { return _addFee; }
            set { _addFee = value; }
        }
        public decimal Markup
        {
            get { return _markup; }
            set { _markup = value; }
        }
        public decimal MarkupValue
        {
            get { return _markupValue; }
            set { _markupValue = value; }
        }
        public string MarkupType
        {
            get { return _markupType; }
            set { _markupType = value; }
        }
        public decimal Discount
        {
            get { return _discount; ;}
            set { _discount = value; }
        }
        public decimal DiscountValue
        {
            get { return _discountValue; }
            set { _discountValue = value; }
        }
        public string DiscountType
        {
            get { return _discountType; }
            set { _discountType = value; }
        }
        public string SourceCurrency
        {
            get { return _sourceCurrency; }
            set { _sourceCurrency = value; }
        }
        public decimal SourceAmount
        {
            get { return _sourceAmount; }
            set { _sourceAmount = value; }
        }
        public decimal ROE
        {
            get { return _roe; }
            set { _roe = value; }
        }
        public decimal B2CMarkup
        {
            get { return _b2cMarkup; }
            set { _b2cMarkup = value; }
        }
        public decimal B2CMarkupValue
        {
            get { return _b2cMarkupValue; }
            set { _b2cMarkupValue = value; }
        }
        public string B2CMarkupType
        {
            get { return _b2cMarkupType; }
            set { _b2cMarkupType = value; }
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        public string ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }
        public PriceTaxDetails TaxDetail
        {
            get
            {
                return taxDetail;
            }

            set
            {
                taxDetail = value;
            }
        }
        public decimal InputVATAmount
        {
            get
            {
                return inputVATAmount;
            }

            set
            {
                inputVATAmount = value;
            }
        }

        public decimal OutputVATAmount
        {
            get
            {
                return outputVATAmount;
            }

            set
            {
                outputVATAmount = value;
            }
        }

        #endregion

        public void Save(SqlCommand cmd)
        {
            try
            {

                SqlParameter[] paramList = new SqlParameter[32];
                paramList[0] = new SqlParameter("@P_Ins_Id", _headerId);
                paramList[1] = new SqlParameter("@P_ItineraryID", _itineraryID);
                paramList[2] = new SqlParameter("@P_PolicyNo", _policyNo);
                paramList[3] = new SqlParameter("@P_PolicyPurchasedDate", _policyPurchasedDate);
                paramList[4] = new SqlParameter("@P_ProposalState", _proposalState);
                paramList[5] = new SqlParameter("@P_InsPlanCode", _insPlanCode);
                paramList[6] = new SqlParameter("@P_SSRFeeCode", _ssrFeeCode);
                paramList[7] = new SqlParameter("@P_Currency", _currency);
                paramList[8] = new SqlParameter("@P_PremiumAmount", _premiumAmount);
                paramList[9] = new SqlParameter("@P_PremiumChargeType", _premiumChargeType);
                paramList[10] = new SqlParameter("@P_PlanTitle", _planTitle);
                paramList[11] = new SqlParameter("@P_PlanDescription", _planDescription);
                if (_refundDateTime != DateTime.MinValue) paramList[12] = new SqlParameter("@P_RefundDateTime", _refundDateTime);
                if (_cancelDateTime != DateTime.MinValue) paramList[13] = new SqlParameter("@P_CancelDateTime", _cancelDateTime);
                paramList[14] = new SqlParameter("@P_BookingStatus", _planStatus);
                paramList[15] = new SqlParameter("@P_NetAmount", _netAmount);
                paramList[16] = new SqlParameter("@P_AddFee", _addFee);
                paramList[17] = new SqlParameter("@P_Markup", _markup);
                paramList[18] = new SqlParameter("@P_MarkupValue", _markupValue);
                if (!string.IsNullOrEmpty(_markupType)) paramList[19] = new SqlParameter("@P_MarkupType", _markupType);
                paramList[20] = new SqlParameter("@P_Discount", _discount);
                paramList[21] = new SqlParameter("@P_DiscountValue", _discountValue);
                if (!string.IsNullOrEmpty(_discountType)) paramList[22] = new SqlParameter("@P_DiscountType", _discountType);
                paramList[23] = new SqlParameter("@P_SourceCurrency", _sourceCurrency);
                paramList[24] = new SqlParameter("@P_SourceAmount", _sourceAmount);
                paramList[25] = new SqlParameter("@P_ROE", _roe);
                paramList[26] = new SqlParameter("@P_B2CMarkup", _b2cMarkup);
                paramList[27] = new SqlParameter("@P_B2CMarkupValue", _b2cMarkupValue);
                if(!string.IsNullOrEmpty(_b2cMarkupType)) paramList[28] = new SqlParameter("@P_B2CMarkupType", _b2cMarkupType);
                paramList[29] = new SqlParameter("@P_Plan_Id", _planId);
                paramList[29].Direction = ParameterDirection.Output;
                paramList[30] = new SqlParameter("@P_InputVAT", inputVATAmount);
                paramList[31] = new SqlParameter("@P_OutputVAT", outputVATAmount);
                int count = DBGateway.ExecuteNonQueryDetails(cmd, "usp_AddInsurancePlans", paramList);
                if (count > 0 && paramList[29].Value != DBNull.Value)
                {
                    _planId = Convert.ToInt32(paramList[29].Value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<InsurancePlan> Load(int headerId)
        {
            try
            {
                List<InsurancePlan> insPlanList = new List<InsurancePlan>();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_Ins_Id", headerId);
                DataTable dt = DBGateway.FillDataTableSP("usp_GetInsurancePlan", paramList);
                foreach (DataRow dr in dt.Rows)
                {
                    InsurancePlan insPlan = new InsurancePlan();
                    insPlan.PlanId = Convert.ToInt32(dr["PlanId"]);
                    insPlan.HeaderId = Convert.ToInt32(dr["Ins_Id"]);
                    if (dr["ItineraryID"] != DBNull.Value)
                    {
                        insPlan._itineraryID = Convert.ToString(dr["ItineraryID"]);
                    }
                    if (dr["PolicyNo"] != DBNull.Value)
                    {
                        insPlan._policyNo = Convert.ToString(dr["PolicyNo"]);
                    }
                    if (dr["PolicyPurchasedDate"] != DBNull.Value)
                    {
                        insPlan._policyPurchasedDate = Convert.ToDateTime(dr["PolicyPurchasedDate"]);
                    }
                    if (dr["ProposalState"] != DBNull.Value)
                    {
                        insPlan._proposalState = Convert.ToString(dr["ProposalState"]);
                    }
                    if (dr["InsPlanCode"] != DBNull.Value)
                    {
                        insPlan._insPlanCode = Convert.ToString(dr["InsPlanCode"]);
                    }
                    if (dr["SSRFeeCode"] != DBNull.Value)
                    {
                        insPlan._ssrFeeCode = Convert.ToString(dr["SSRFeeCode"]);
                    }
                    if (dr["Currency"] != DBNull.Value)
                    {
                        insPlan._currency = Convert.ToString(dr["Currency"]);
                    }
                    if (dr["PremiumAmount"] != DBNull.Value)
                    {
                        insPlan._premiumAmount = Convert.ToDecimal(dr["PremiumAmount"]);
                    }
                    if (dr["PremiumChargeType"] != DBNull.Value)
                    {
                        insPlan._premiumChargeType = Convert.ToString(dr["PremiumChargeType"]);
                    }
                    if (dr["PlanTitle"] != DBNull.Value)
                    {
                        insPlan._planTitle = Convert.ToString(dr["PlanTitle"]);
                    }
                    if (dr["PlanDescription"] != DBNull.Value)
                    {
                        insPlan._planDescription = Convert.ToString(dr["PlanDescription"]);
                    }
                    if (dr["RefundDateTime"] != DBNull.Value)
                    {
                        insPlan._refundDateTime = Convert.ToDateTime(dr["RefundDateTime"]);
                    }
                    if (dr["CancelDateTime"] != DBNull.Value)
                    {
                        insPlan._cancelDateTime = Convert.ToDateTime(dr["CancelDateTime"]);
                    }
                    if (dr["BookingStatus"] != DBNull.Value)
                    {
                        insPlan._planStatus = (InsuranceBookingStatus)Convert.ToInt32(dr["BookingStatus"]);
                    }
                    if (dr["NetAmount"] != DBNull.Value)
                    {
                        insPlan._netAmount = Convert.ToDecimal(dr["NetAmount"]);
                    }
                    if (dr["AddFee"] != DBNull.Value)
                    {
                        insPlan._addFee = Convert.ToDecimal(dr["AddFee"]);
                    }
                    if (dr["Markup"] != DBNull.Value)
                    {
                        insPlan._markup = Convert.ToDecimal(dr["Markup"]);
                    }
                    if (dr["MarkupValue"] != DBNull.Value)
                    {
                        insPlan._markupValue = Convert.ToDecimal(dr["MarkupValue"]);
                    }
                    if (dr["MarkupType"] != DBNull.Value)
                    {
                        insPlan._markupType = Convert.ToString(dr["MarkupType"]);
                    }
                    if (dr["Discount"] != DBNull.Value)
                    {
                        insPlan._discount = Convert.ToDecimal(dr["Discount"]);
                    }
                    if (dr["DiscountValue"] != DBNull.Value)
                    {
                        insPlan._discountValue = Convert.ToDecimal(dr["DiscountValue"]);
                    }
                    if (dr["DiscountType"] != DBNull.Value)
                    {
                        insPlan._discountType = Convert.ToString(dr["DiscountType"]);
                    }
                    if (dr["B2CMarkup"] != DBNull.Value)
                    {
                        insPlan._b2cMarkup = Convert.ToDecimal(dr["B2CMarkup"]);
                    }
                    if (dr["B2CMarkupValue"] != DBNull.Value)
                    {
                        insPlan._b2cMarkupValue = Convert.ToDecimal(dr["B2CMarkupValue"]);
                    }
                    if (dr["B2CMarkupType"] != DBNull.Value)
                    {
                        insPlan._b2cMarkupType = Convert.ToString(dr["B2CMarkupType"]);
                    }
                    if (dr["SourceCurrency"] != DBNull.Value)
                    {
                        insPlan._sourceCurrency = Convert.ToString(dr["SourceCurrency"]);
                    }
                    if (dr["SourceAmount"] != DBNull.Value)
                    {
                        insPlan._sourceAmount = Convert.ToDecimal(dr["SourceAmount"]);
                    }
                    if (dr["ROE"] != DBNull.Value)
                    {
                        insPlan._roe = Convert.ToDecimal(dr["ROE"]);
                    }
                    if (dr["InputVAT"] != DBNull.Value)
                    {
                        insPlan.inputVATAmount = Convert.ToDecimal(dr["InputVAT"]);
                    }
                    if (dr["OutputVAT"] != DBNull.Value)
                    {
                        insPlan.outputVATAmount = Convert.ToDecimal(dr["OutputVAT"]);
                    }
                    insPlanList.Add(insPlan);
                }
                return insPlanList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


         //<summary>
         //Updates details when a Policy/Plan is cancelled.
         //</summary>
        public static void Update(int planId, int status, string ProState,bool changeRequest)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                paramList[0] = new SqlParameter("@P_PlanId", planId);
                paramList[1] = new SqlParameter("@P_CancelDateTime", DateTime.Now);
                paramList[2] = new SqlParameter("@P_BookingStatus", status);
                paramList[3] = new SqlParameter("@P_ProposalState", ProState);
                if (changeRequest) paramList[4] = new SqlParameter("@P_RefundDateTime", DateTime.Now);
                int count = DBGateway.ExecuteNonQuerySP("usp_UpdateInsurancePlan", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static int GetCancelPlansCount(int hederId)
        {
            int cancelCount = 0;
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_insId", hederId);
                paramList[1] = new SqlParameter("@P_PlansCount", SqlDbType.Int, 200);
                paramList[1].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuerySP("usp_GetCancelInsPlansCount", paramList);
                cancelCount = Convert.ToInt32(paramList[1].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cancelCount;
        }

        /// <summary>
        /// To get insurance plan from plan details object
        /// </summary>
        /// <param name="planDetails"></param>
        /// <returns></returns>
        public static InsurancePlan GetInsurancePlan(InsurancePlanDetails planDetails, int decimalValue)
        {
            InsurancePlan insplan = new InsurancePlan();

            try
            {
                insplan.PlanTitle = planDetails.Title;
                insplan.PlanDescription = planDetails.Description;
                insplan.ItineraryID = insplan.PolicyNo = string.Empty;
                insplan.PolicyPurchasedDate = DateTime.Now;
                insplan.SourceAmount = planDetails.SourceAmount;
                insplan.SourceCurrency = planDetails.SourceCurrency;
                insplan.NetAmount = planDetails.NetFare;
                insplan.Markup = planDetails.Markup;
                insplan.MarkupType = planDetails.MarkupType;
                insplan.MarkupValue = planDetails.MarkupValue;
                insplan.Discount = planDetails.Discount;
                insplan.DiscountType = planDetails.DiscountType;
                insplan.DiscountValue = planDetails.DiscountValue;
                insplan.Currency = planDetails.CurrencyCode;

                // Output VAT Calculation.
                decimal outPutVAT = 0m, totalAmount = insplan.NetAmount + insplan.Markup - insplan.Discount;

                insplan.TaxDetail = planDetails.TaxDetail;
                insplan.InputVATAmount = planDetails.InputVATAmount;

                if (planDetails.TaxDetail != null && planDetails.TaxDetail.OutputVAT != null && planDetails.TaxDetail.OutputVAT.Applied)
                    outPutVAT = planDetails.TaxDetail.OutputVAT.CalculateVatAmount(totalAmount, insplan.Markup, decimalValue);
                                
                insplan.OutputVATAmount = outPutVAT;
                insplan.PremiumAmount = planDetails.PlanPriceBreakDown != null && planDetails.PlanPriceBreakDown.Count > 0 ?
                        Convert.ToDecimal(planDetails.PlanPriceBreakDown[0].PremiumAmount) * planDetails.ROE : insplan.PremiumAmount;
                insplan.PremiumChargeType = planDetails.PlanType == "OTA" ? planDetails.PremiumChargeType.ToString() : planDetails.UPsellPremiumChargeType.ToString();
                insplan.InsPlanCode = planDetails.Code;
                insplan.SSRFeeCode = planDetails.SSRFeeCode;
                insplan.ROE = planDetails.ROE;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return insplan;
        }
    }

    /// <summary>
    /// Insurance Plan’s Key Points
    /// </summary>
    [Serializable]
    public class MarketingPointer
    {
        private long _pointId;
        private long _headerId;
        private string _description;
        private string _connectionString;

        /// <summary>
        /// Point Number
        /// </summary>
        public long PointId
        {
            get { return _pointId; }
            set { _pointId = value; }
        }

        public long HeaderId
        {
            get { return _headerId; }
            set { _headerId = value; }
        }
        /// <summary>
        /// Plan's Key Point
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_Ins_Key_PlanId", _pointId);
                paramList[1] = new SqlParameter("@P_HeaderId", _headerId);
                paramList[2] = new SqlParameter("@P_Description", _description);

                DBGateway.ExecuteNonQuerySP("usp_AddInsuranceMarketingPoints", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    [Serializable]
    public class PriceBreakDown
    {
        int _minAge;
        int _maxAge;
        string _gender;
        string _currecnyCode;
        double _premiumAmount;
     

        public int MinAge
        {
            get { return _minAge; }
            set { _minAge = value; }
        }

        public int MaxAge
        {
            get { return _maxAge; }
            set { _maxAge = value; }
        }

        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        public string CurrencyCode
        {
            get { return _currecnyCode; }
            set { _currecnyCode = value; }
        }

        public double PremiumAmount
        {
            get { return _premiumAmount; }
            set { _premiumAmount = value; }
        }
    }


}
