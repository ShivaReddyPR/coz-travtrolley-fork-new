﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using ZEUS = ZeusInsB2B.Zeus;

namespace CT.BookingEngine.Insurance
{
    [Serializable]
    public class InsurancePassenger
    {
        #region Members
        long _paxId;
        long _headerId;
        string _firstName;
        string _lastName;
        string _paxType;
        ZEUS.GenderType _gender;
        DateTime _dob;
        string _city;
        string _state;
        string _country;
        string _nationality;
        string _email;
        string _phoneNumber;
        string _remarks;
        ZEUS.IdentificationType _documentType;
        string _documentNo;
        bool _isInfant;
        //Returned from Available Plans response
        bool _isQualified;
        string _policyNo;
        string _policyUrlLink;
        string _tempDOB;
        string _title;
        string _eligible;
        string _pnr;
        #endregion

        #region Properties

        public long PaxId
        {
            get { return _paxId; }
            set { _paxId = value; }
        }
        public long HeaderId
        {
            get { return _headerId; }
            set { _headerId = value; }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string PaxType
        {
            get { return _paxType; }
            set { _paxType = value; }
        }
        public ZEUS.GenderType Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
        public DateTime DOB
        {
            get { return _dob; }
            set { _dob = value; }
        }
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }
        public ZEUS.IdentificationType DocumentType
        {
            get { return _documentType; }
            set { _documentType = value; }
        }
        public string DocumentNo
        {
            get { return _documentNo; }
            set { _documentNo = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string Nationality
        {
            get { return _nationality; }
            set { _nationality = value; }
        }
        public bool IsInfant
        {
            get { return _isInfant; }
            set { _isInfant = value; }
        }

        /// <summary>
        /// Returned from Available Insurance Plan Response.
        /// </summary>
        public bool IsQualified
        {
            get { return _isQualified; }
            set { _isQualified = value; }
        }

        /// <summary>
        /// Returned from Available Insurance Plan Response.
        /// </summary>
         
         
        /// <summary>
        /// Confirmed PolicyNo applicable
        /// </summary>
        public string PolicyNo
        {
            get { return _policyNo; }
            set { _policyNo = value; }
        }
        /// <summary>
        /// URL Link to download Covered Person’s Policy Certificate in PDF or HTML format.
        /// </summary>
        public string PolicyUrlLink
        {
            get { return _policyUrlLink; }
            set { _policyUrlLink = value; }
        }
        public string Remarks
        {
            get { return _remarks; }
            set { _remarks = value; }
        }
        public string TempDOB
        {
            get { return _tempDOB; }
            set { _tempDOB = value; }
        }
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        public string Eligible
        {
            get { return _eligible; }
            set { _eligible = value; }
        }
        public string PNR
        {
            get { return _pnr; }
            set { _pnr = value; }
        }
        #endregion

        public void Save(SqlCommand cmd)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[20];
                paramList[0] = new SqlParameter("@P_Ins_Id", _headerId);
                paramList[1] = new SqlParameter("@P_Title", _title);
                paramList[2] = new SqlParameter("@P_FirstName", _firstName);
                paramList[3] = new SqlParameter("@P_LastName", _lastName);
                paramList[4] = new SqlParameter("@P_PaxType", _paxType);
                paramList[5] = new SqlParameter("@P_Gender", _gender.ToString());
                paramList[6] = new SqlParameter("@P_DOB", _dob);
                if (!string.IsNullOrEmpty(_city))  paramList[7] = new SqlParameter("@P_City", _city);
                paramList[8] = new SqlParameter("@P_State", _state);
                paramList[9] = new SqlParameter("@P_Country", _country);
                paramList[10] = new SqlParameter("@P_Nationality", _nationality);
                paramList[11] = new SqlParameter("@P_Email", _email);
                if (!string.IsNullOrEmpty(_phoneNumber)) paramList[12] = new SqlParameter("@P_PhoneNumber", _phoneNumber);
                if(!string.IsNullOrEmpty(_remarks)) paramList[13] = new SqlParameter("@P_Remarks", _remarks);
                if (!string.IsNullOrEmpty(_documentNo))
                {
                    paramList[14] = new SqlParameter("@P_DocumentType", _documentType.ToString());
                    paramList[15] = new SqlParameter("@P_DocumentNo", _documentNo);
                }
                paramList[16] = new SqlParameter("@P_PolicyNo", _policyNo);
                paramList[17] = new SqlParameter("@P_PolicyUrlLinks", _policyUrlLink);
                paramList[18] = new SqlParameter("@P_Eligible", _eligible);
                paramList[19] = new SqlParameter("@P_Paxid", _paxId);
                paramList[19].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQueryDetails(cmd,"usp_AddInsurancePassengers", paramList);
                if (paramList[19].Value != DBNull.Value)
                    _paxId =Convert.ToInt32(paramList[19].Value.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<InsurancePassenger> Load(int headerId)
        {
            try
            {
                List<InsurancePassenger> insPassList = new List<InsurancePassenger>();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_Ins_Id", headerId);
                DataTable dt = DBGateway.FillDataTableSP("usp_GetInsurancePassenger", paramList);
                foreach (DataRow dr in dt.Rows)
                {
                    InsurancePassenger insPassenger = new InsurancePassenger();
                    insPassenger._paxId = Convert.ToInt32(dr["PaxId"]);
                    insPassenger._headerId = Convert.ToInt32(dr["Ins_Id"]);
                    if (dr["Title"] != DBNull.Value)
                    {
                        insPassenger._title = Convert.ToString(dr["Title"]);
                    }
                    if (dr["FirstName"] != DBNull.Value)
                    {
                        insPassenger._firstName = Convert.ToString(dr["FirstName"]);
                    }
                    if (dr["LastName"] != DBNull.Value)
                    {
                        insPassenger._lastName = Convert.ToString(dr["LastName"]);
                    }
                    if (dr["PaxType"] != DBNull.Value)
                    {
                        insPassenger._paxType = Convert.ToString(dr["PaxType"]);
                    }
                    if (dr["Gender"] != DBNull.Value)
                    {
                        if (dr["Gender"].ToString() == "Male")
                        {
                            insPassenger.Gender = global::ZeusInsB2B.Zeus.GenderType.Male;
                        }
                        else
                        {
                            insPassenger.Gender = global::ZeusInsB2B.Zeus.GenderType.Female;
                        }
                    }
                    if (dr["DOB"] != DBNull.Value)
                    {
                        insPassenger._dob = Convert.ToDateTime(dr["DOB"]);
                    }
                    if (dr["City"] != DBNull.Value)
                    {
                        insPassenger._city = Convert.ToString(dr["City"]);
                    }
                    if (dr["State"] != DBNull.Value)
                    {
                        insPassenger._state = Convert.ToString(dr["State"]);
                    }
                    if (dr["Country"] != DBNull.Value)
                    {
                        insPassenger._country = Convert.ToString(dr["Country"]);
                    }
                    if (dr["Nationality"] != DBNull.Value)
                    {
                        insPassenger._nationality = Convert.ToString(dr["Nationality"]);
                    }
                    if (dr["Email"] != DBNull.Value)
                    {
                        insPassenger._email = Convert.ToString(dr["Email"]);
                    }
                    if (dr["PhoneNumber"] != DBNull.Value)
                    {
                        insPassenger._phoneNumber = Convert.ToString(dr["PhoneNumber"]);
                    }
                    if (dr["Remarks"] != DBNull.Value)
                    {
                        insPassenger._remarks = Convert.ToString(dr["Remarks"]);
                    }
                    if (dr["DocumentType"] != DBNull.Value)
                    {
                        if (dr["DocumentType"].ToString() == "Passport")
                        {
                            insPassenger._documentType = global::ZeusInsB2B.Zeus.IdentificationType.Passport;
                        }
                        else
                        {
                            insPassenger._documentType = global::ZeusInsB2B.Zeus.IdentificationType.IdentificationCard;
                        }
                    }
                    if (dr["DocumentNo"] != DBNull.Value)
                    {
                        insPassenger._documentNo = Convert.ToString(dr["DocumentNo"]);
                    }
                    if (dr["PolicyNo"] != DBNull.Value)
                    {
                        insPassenger._policyNo = Convert.ToString(dr["PolicyNo"]);
                    }
                    if (dr["PolicyUrlLinks"] != DBNull.Value)
                    {
                        insPassenger._policyUrlLink = Convert.ToString(dr["PolicyUrlLinks"]);
                    }
                    if (dr["Eligible"] != DBNull.Value)
                    {
                        insPassenger._eligible = Convert.ToString(dr["Eligible"]);
                    }
                    insPassList.Add(insPassenger);
                }
                return insPassList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
