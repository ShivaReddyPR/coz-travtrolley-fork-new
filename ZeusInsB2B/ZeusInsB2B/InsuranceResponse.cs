﻿using System;
using System.Collections.Generic;
//using System.Linq;

namespace CT.BookingEngine.Insurance
{
    [Serializable]
    public class InsuranceResponse
    {
        #region members
        List<InsurancePlanDetails> otaPlans;
        List<InsurancePlanDetails> upsellPlans;
        string _error;
        string _errorMessage;
        string _sessionId;
        #endregion

        #region properities
        public List<InsurancePlanDetails> OTAPlans
        {
            get { return otaPlans; }
            set { otaPlans = value; }
        }
        public List<InsurancePlanDetails> UpsellPlans
        {
            get { return upsellPlans; }
            set { upsellPlans = value; }
        }
        /// <summary>
        /// Error code returned from the web service
        /// </summary>
        public string Error
        {
            get { return _error; }
            set { _error = value; }
        }

        /// <summary>
        /// Error message returned from the web service
        /// </summary>
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        public string SessionId
        {
            get
            {
                return _sessionId;
            }

            set
            {
                _sessionId = value;
            }
        }
        #endregion
    }

    [Serializable]
    public class InsurancePlanDetails
    {
        #region Members
        string _policyNo;
        string _description;
        string _currencyCode;
        string _additionalInfoTitle;
        string _additionalInfoDesc;
        string _code;
        bool _isDefaultPlan; //Whether to display this Insurance Plan as Default or First, when more than 2 Plans are available.
        string _noConsideration; //Plan’s Description for any Popup Messages upon clicking the No Button
        string _noDesc; //Plan’s Description for No Button
        string _content;
        ZeusInsB2B.Zeus.PlanOTAChargeType _premiumChargeType; //normal plans
        ZeusInsB2B.Zeus.UpsellPlanChargeType _upsellPremiumChargeType; //Upcell Plans purpose
        private string _ssrFeeCode;
        string _termsAndConditions;
        string _title;
        decimal _totalAmount;
        string _yesDesc; //Plan’s Description for Yes Button
        //decimal _premiumAmount;
        List<MarketingPointer> _marketingPointers;
        List<InsurancePassenger> _qualifiedPassengers;
        List<PriceBreakDown> _planPriceBreakDown;
        decimal _netFare;
        decimal _markup;
        decimal _markupValue;
        string _markupType;
        decimal _discount;
        decimal _discountValue;
        string _discountType;
        decimal _sourceAmount;
        string _sourceCurrency;
        int _decimalValue;
        decimal _roe;
        decimal _b2cMarkup;
        decimal _b2cMarkupValue;
        string _b2cMarkupType;
        //string _bookingSource;
        int _agencyId;
        string _planType;
        private PriceTaxDetails taxDetail;
        private decimal inputVATAmount;
        private decimal outputVATAmount;
        #endregion

        #region Properties
        public string PolicyNo
        {
            get { return _policyNo; }
            set { _policyNo = value; }
        }
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        /// <summary>
        /// Plan’s Special Service Request or Fee Code
        /// </summary>
        public string SSRFeeCode
        {
            get { return _ssrFeeCode; }
            set { _ssrFeeCode = value; }
        }

        /// <summary>
        ///Agent Currency Currency Code to Charge.
        /// </summary>
        public string CurrencyCode
        {
            get { return _currencyCode; }
            set { _currencyCode = value; }
        }

        /// <summary>
        /// PerBooking – Total 1 Premium Amount for all passengers
        /// PerPassenger – Each passenger is charged individually
        /// </summary>
        public ZeusInsB2B.Zeus.PlanOTAChargeType PremiumChargeType
        {
            get { return _premiumChargeType; }
            set { _premiumChargeType = value; }
        }

        /// <summary>
        /// PerBooking – Total 1 Premium Amount for all passengers
        /// PerPassenger – Each passenger is charged individually
        /// </summary>
        public ZeusInsB2B.Zeus.UpsellPlanChargeType UPsellPremiumChargeType
        {
            get { return _upsellPremiumChargeType; }
            set { _upsellPremiumChargeType = value; }
        }

        /// <summary>
        /// Title of the Plan
        /// </summary>
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        /// <summary>
        /// Description
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// HTML Content used to display for the passengers page.
        /// </summary>
        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }
        /// <summary>
        /// Additional Info Title Name
        /// </summary>
        public string AdditionalInfoTitle
        {
            get { return _additionalInfoTitle; }
            set { _additionalInfoTitle = value; }
        }
        /// <summary>
        /// Additional Info Description
        /// </summary>
        public string AdditionalInfoDesc
        {
            get { return _additionalInfoDesc; }
            set { _additionalInfoDesc = value; }
        }

        /// <summary>
        /// Plan’s Description for Yes Button
        /// </summary>
        public string YesDesc
        {
            get { return _yesDesc; }
            set { _yesDesc = value; }
        }

        /// <summary>
        /// Plan’s Description for No Button
        /// </summary>
        public string NoDesc
        {
            get { return _noDesc; }
            set { _noDesc = value; }
        }

        /// <summary>
        /// Plan’s Description for any Popup Messages upon clicking the No Button
        /// </summary>
        public string NoConsideration
        {
            get { return _noConsideration; }
            set { _noConsideration = value; }
        }

        /// <summary>
        /// Terms and Conditions for the Plan
        /// </summary>
        public string TermsAndConditions
        {
            get { return _termsAndConditions; }
            set { _termsAndConditions = value; }
        }

        /// <summary>
        /// Whether to display this Insurance Plan as Default or First, when more than 2 Plans are available.
        /// </summary>
        public bool IsDefaultPlan
        {
            get { return _isDefaultPlan; }
            set { _isDefaultPlan = value; }
        }
        public List<MarketingPointer> MarketingPointers
        {
            get { return _marketingPointers; }
            set { _marketingPointers = value; }
        }

        public List<InsurancePassenger> QualifiedPassengers
        {
            get { return _qualifiedPassengers; }
            set { _qualifiedPassengers = value; }
        }

        public List<PriceBreakDown> PlanPriceBreakDown
        {
            get { return _planPriceBreakDown; }
            set { _planPriceBreakDown = value; }
        }

        public int AgencyId
        {
            get { return _agencyId; }
            set { _agencyId = value; }
        }

        public decimal Markup
        {
            get { return _markup; }
            set { _markup = value; }
        }
        public decimal MarkupValue
        {
            get { return _markupValue; }
            set { _markupValue = value; }
        }
        public string MarkupType
        {
            get { return _markupType; }
            set { _markupType = value; }
        }
        public decimal Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }
        public decimal DiscountValue
        {
            get { return _discountValue; }
            set { _discountValue = value; }
        }
        public string DiscountType
        {
            get { return _discountType; }
            set { _discountType = value; }
        }
        public decimal TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }
        public decimal NetFare
        {
            get { return _netFare; }
            set { _netFare = value; }
        }
        public decimal B2CMarkup
        {
            get { return _b2cMarkup; }
            set { _b2cMarkup = value; }
        }
        public decimal B2CMarkupValue
        {
            get { return _b2cMarkupValue; }
            set { _b2cMarkupValue = value; }
        }
        public string B2CMarkupType
        {
            get { return _b2cMarkupType; }
            set { _b2cMarkupType = value; }
        }
        public int DecimalValue
        {
            get { return _decimalValue; }
            set { _decimalValue = value; }
        }
        public decimal ROE
        {
            get { return _roe; }
            set { _roe = value; }
        }
        public decimal SourceAmount
        {
            get { return _sourceAmount; }
            set { _sourceAmount = value; }
        }
        public string SourceCurrency
        {
            get { return _sourceCurrency; }
            set { _sourceCurrency = value; }
        }
        public string PlanType
        {
            get { return _planType; }
            set { _planType = value; }
        }
        public PriceTaxDetails TaxDetail
        {
            get
            {
                return taxDetail;
            }

            set
            {
                taxDetail = value;
            }
        }
        public decimal InputVATAmount
        {
            get
            {
                return inputVATAmount;
            }

            set
            {
                inputVATAmount = value;
            }
        }

        public decimal OutputVATAmount
        {
            get
            {
                return outputVATAmount;
            }

            set
            {
                outputVATAmount = value;
            }
        }
        #endregion
    }
}
