﻿using System;
using System.Collections.Generic;
using ZEUS = ZeusInsB2B.Zeus;
using System.Xml;
using CT.Configuration;
using System.Web;
using CT.BookingEngine.Insurance;
using System.Collections;
using System.Linq;
using CT.Core;
using System.Xml.Serialization;
using System.IO;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;

namespace ZeusInsB2B
{
    public class Tunes
    {
        #region Members
        string _userName = string.Empty;
        string _password = string.Empty;
        string _channel = string.Empty;
        decimal rateOfExchange = 1;
        //string _currency = string.Empty;
        string _xmlPath = string.Empty;
        protected ZEUS.ZEUSTravelInsuranceGateway insGateway;
        string endpoint = string.Empty;

        string agentCurrency = "";
        Dictionary<string, decimal> exchangeRates;
        int decimalPoint;
        string sessionId;
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        public Dictionary<string, decimal> AgentExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public int AgentDecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }

        //Added by Lokesh on 9-Jan-18 .Need to pass UserName,Password,Channel Based on AgentDetails i.e through MSE.
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        public string Channel
        {
            get { return _channel; }
            set { _channel = value; }
        }
        public string SessionId
        {
            get
            {
                return sessionId;
            }

            set
            {
                sessionId = value;
            }
        }

        public string Endpoint
        {
            get { return endpoint; }
            set { endpoint = value; }
        }
        #endregion

        //constructor loading all values through config file
        public Tunes()
        {
            //Below are Commented by Lokesh on 9-Jan-18 .Need to pass UserName,Password,Channel Based on AgentDetails i.e through MSE.
            // _userName = ConfigurationSystem.ZeusConfig["UserName"];
            // _password = ConfigurationSystem.ZeusConfig["Password"];
            //_channel = ConfigurationSystem.ZeusConfig["Channel"];
            //_currency = ConfigurationSystem.ZeusConfig["Currency"];
            _xmlPath = ConfigurationSystem.ZeusConfig["XmlLogPath"];
            _xmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            try
            {
                if (!System.IO.Directory.Exists(_xmlPath))
                {
                    System.IO.Directory.CreateDirectory(_xmlPath);
                }
            }
            catch { }

            insGateway = new ZeusInsB2B.Zeus.ZEUSTravelInsuranceGateway();
            insGateway.Url = ConfigurationSystem.ZeusConfig["Url"];
        }

        //Loading Available plans
        public InsuranceResponse GetAvailablePlans(InsuranceRequest request, decimal markup, string markupType, decimal discount, string discountType, string _currency)
        {
            ZEUS.OutputResponseAvailablePlansOTA availablePlanDetails = null;
            insGateway.Url = !string.IsNullOrEmpty(Endpoint) ? Endpoint : insGateway.Url;
            InsuranceResponse availablePlans = new InsuranceResponse();
            try
            {

                ZEUS.InputRequestOTA insPlanRequest = new ZEUS.InputRequestOTA();
                //Authentication details            
                insPlanRequest.Authentication = new ZEUS.ItineraryAuthentication();
                insPlanRequest.Authentication.Username = _userName;
                insPlanRequest.Authentication.Password = _password;

                //Header details
                insPlanRequest.Header = new ZEUS.ItineraryHeaderOTA();
                insPlanRequest.Header.Channel = _channel;
                insPlanRequest.Header.Currency = _currency;
                insPlanRequest.Header.CountryCode = request.CountryCode;
                insPlanRequest.Header.CultureCode = request.CultureCode;
                insPlanRequest.Header.TotalAdults = request.Adults;
                insPlanRequest.Header.TotalChild = request.Childs;
                insPlanRequest.Header.TotalInfants = request.Infants;

                //Flight Details
                insPlanRequest.Flights = new ZEUS.ItineraryFlight();
                insPlanRequest.Flights.DepartCountryCode = request.DepartureCountryCode;
                insPlanRequest.Flights.DepartStationCode = request.DepartureStationCode;
                insPlanRequest.Flights.ArrivalCountryCode = request.ArrivalCountryCode;
                insPlanRequest.Flights.ArrivalStationCode = request.ArrivalStationCode;
                insPlanRequest.Flights.DepartAirlineCode = request.DepartureAirlineCode;
                insPlanRequest.Flights.ReturnAirlineCode = request.ReturnAirlineCode;
                insPlanRequest.Flights.DepartDateTime = request.DepartureDateTime;
                insPlanRequest.Flights.ReturnDateTime = request.ReturnDateTime;
                insPlanRequest.Flights.ReturnFlightNo = request.ReturnFlightNo;
                insPlanRequest.Flights.DepartFlightNo = request.DepartureFlightNo;

                try
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(insPlanRequest.GetType());
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                    ser.Serialize(writer, insPlanRequest);  // Here Classes are converted to XML String. 
                    // This can be viewed in SB or writer.
                    // Above XML in SB can be loaded in XmlDocument object
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sb.ToString());
                    string filePath = @"" + _xmlPath + SessionId + "_"+request.DepartureStationCode+"-"+request.ArrivalStationCode+"-" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_ZeusInsAvailablePlansRequest.xml";
                    doc.Save(filePath);
                    //CT.Core.Audit.Add(CT.Core.EventType.InsuranceBooking, CT.Core.Severity.Normal, 0, "ZeusInsAvailablePlansRequest" + sb.ToString(), "");
                }
                catch
                {
                }

                availablePlanDetails = insGateway.GetAvailablePlansOTAWithRiders(insPlanRequest);
                //XmlSerializer serializer = new XmlSerializer(typeof(ZEUS.OutputResponseAvailablePlansOTA));
                //availablePlanDetails = (ZEUS.OutputResponseAvailablePlansOTA)serializer.Deserialize(new StreamReader(@"C:\XMLlogs\Zeus\32445ca8-6d95-49ed-9a6a-a13257551c38_03062020_112839_ZeusInsAvailablePlansResponse.xml"));

                try

                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(availablePlanDetails.GetType());
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                    ser.Serialize(writer, availablePlanDetails);  // Here Classes are converted to XML String. 
                    // This can be viewed in SB or writer.
                    // Above XML in SB can be loaded in XmlDocument object
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sb.ToString());
                    string filePath = @"" + _xmlPath + SessionId + "_"+request.DepartureStationCode + "-" + request.ArrivalStationCode  + "-" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_ZeusInsAvailablePlansResponse.xml";
                    doc.Save(filePath);
                    //doc.Save(_xmlPath + "ZeusInsAvailablePlansResponse" + DateTime.Now.ToString("ddMMyyy_hhmmss") + ".xml");
                    //CT.Core.Audit.Add(CT.Core.EventType.InsuranceBooking, CT.Core.Severity.Normal, 0, "ZeusInsAvailablePlansResponse"+sb.ToString(), "");
                }
                catch
                {
                }
                //error checking
                if (availablePlanDetails.ErrorCode != "0")
                {
                    availablePlans.Error = availablePlanDetails.ErrorCode;
                    availablePlans.ErrorMessage = availablePlanDetails.ErrorMessage;
                }
                else
                {
                    string currency = availablePlanDetails.AvailablePlans[0].CurrencyCode;
                    //Loading rateofexchange based on the currency
                    if (AgentExchangeRates.ContainsKey(currency))
                    {
                        rateOfExchange = AgentExchangeRates[currency];
                    }
                    else
                    {
                        rateOfExchange = 1;
                    }
                    InsurancePlanDetails insPlan = new InsurancePlanDetails();

                    //Loading OTAPlans Added by brahmam 22.12.2016
                    if (availablePlanDetails.AvailablePlans.Length > 0)
                    {
                        availablePlans.OTAPlans = new List<InsurancePlanDetails>();
                        List<ZEUS.PlanOTA> OTAPlansList = new List<ZEUS.PlanOTA>();
                        //If Domestic travel is false and currency is INR
                        if (!request.IsDomesticSearch && _currency == "INR")
                        {
                            //Filtering plans for senior citizen min age above 60 INR Currency.
                            OTAPlansList = (request.IsSeniorCitizen) ? availablePlanDetails.AvailablePlans.ToList().FindAll(x => x != null && x.PlanPricingBreakdown.ToList().Any(y => y.MinAge > 60)) : availablePlanDetails.AvailablePlans.ToList().FindAll(x => x != null && x.PlanPricingBreakdown.ToList().Any(y => y.MinAge < 60));
                            if (request.IsSeniorCitizen)// if MinAge =2 & MaxAge=75 then we are considering that plan as senior citizen plan 
                            {
                                OTAPlansList.AddRange(availablePlanDetails.AvailablePlans.ToList().FindAll(x => x != null && x.PlanPricingBreakdown.ToList().Any(y => y.MinAge == 2 && y.MaxAge == 75)));
                            }
                        }
                        else if (request.IsDomesticSearch && _currency == "INR")//If Domestic travel is true and currency is INR filtering plans min age <70
                        {
                            OTAPlansList = availablePlanDetails.AvailablePlans.ToList().FindAll(x => x != null && x.PlanPricingBreakdown.ToList().Any(y => y.MinAge < 70));
                        }
                        else
                        {
                            //Filtering plans for senior citizen min age above 75 .
                            OTAPlansList = (request.IsSeniorCitizen) ? availablePlanDetails.AvailablePlans.ToList().FindAll(x => x != null && x.PlanPricingBreakdown.ToList().Any(y => y.MinAge > 75)) : availablePlanDetails.AvailablePlans.ToList().FindAll(x => x != null && x.PlanPricingBreakdown.ToList().Any(y => y.MinAge < 75));
                            if (request.IsSeniorCitizen)// if MinAge =2 & MaxAge=85 then we are considering that plan as senior citizen plan 
                            {
                                OTAPlansList.AddRange(availablePlanDetails.AvailablePlans.ToList().FindAll(x => x != null && x.PlanPricingBreakdown.ToList().Any(y => y.MinAge == 2 && y.MaxAge == 85)));
                            }

                        }
                        if (OTAPlansList != null && OTAPlansList.Count > 0)
                        {
                            OTAPlansList.ToList().ForEach(x =>
                            {
                                insPlan = getOTAPlans(request, x, markup, markupType, discount, discountType);
                                availablePlans.OTAPlans.Add(insPlan);
                            });
                        }
                    }
                    //Loading UpsellPlans Added by brahmam 22.12.2016
                    if (availablePlanDetails.AvailableUpsellPlans.Length > 0)
                    {
                        availablePlans.UpsellPlans = new List<InsurancePlanDetails>();
                        List<ZEUS.UpsellPlanGroup> upsellPlansList = new List<ZEUS.UpsellPlanGroup>();
                        //If Domestic travel is false and currency is INR
                        if (!request.IsDomesticSearch && _currency == "INR")
                        {
                            //Filtering plans for senior citizen min age above 60 for INR currency .
                            upsellPlansList = (request.IsSeniorCitizen) ? availablePlanDetails.AvailableUpsellPlans.ToList().FindAll(x => x.UpsellPlans.ToList().Any(y => y != null && y.PlanPricingBreakdown.ToList().Any(z => z.MinAge > 60))) : availablePlanDetails.AvailableUpsellPlans.ToList().FindAll(x => x.UpsellPlans.ToList().Any(y => y != null && y.PlanPricingBreakdown.ToList().Any(z => z.MinAge < 60)));
                            if (request.IsSeniorCitizen)// if MinAge =2 & MaxAge=75 then we are considering that plan as senior citizen plan 
                            {
                                upsellPlansList.AddRange(availablePlanDetails.AvailableUpsellPlans.ToList().FindAll(x => x != null && x.UpsellPlans.ToList().Any(y => y != null && y.PlanPricingBreakdown.ToList().Any(z => z.MinAge == 2 && z.MaxAge == 75))));
                            }

                        }
                        else if (request.IsDomesticSearch && _currency == "INR")//If Domestic travel is true and currency is INR filtering plans min age <70
                        {
                            upsellPlansList = availablePlanDetails.AvailableUpsellPlans.ToList().FindAll(x => x.UpsellPlans.ToList().Any(y => y != null && y.PlanPricingBreakdown.ToList().Any(z => z.MinAge < 70)));
                        }
                        else
                        {
                            //Filtering plans for senior citizen min age above 75 .
                            upsellPlansList = (request.IsSeniorCitizen) ? availablePlanDetails.AvailableUpsellPlans.ToList().FindAll(x => x.UpsellPlans.ToList().Any(y => y != null && y.PlanPricingBreakdown.ToList().Any(z => z.MinAge > 75))) : availablePlanDetails.AvailableUpsellPlans.ToList().FindAll(x => x.UpsellPlans.ToList().Any(y => y != null && y.PlanPricingBreakdown.ToList().Any(z => z.MinAge < 75)));
                            if (request.IsSeniorCitizen)// if MinAge =2 & MaxAge=85 then we are considering that plan as senior citizen plan 
                            {
                                upsellPlansList.AddRange(availablePlanDetails.AvailableUpsellPlans.ToList().FindAll(x => x != null && x.UpsellPlans.ToList().Any(y => y != null && y.PlanPricingBreakdown.ToList().Any(z => z.MinAge == 2 && z.MaxAge == 85))));
                            }
                        }
                        if (upsellPlansList != null && upsellPlansList.Count > 0)
                        {
                            upsellPlansList.ToList().ForEach(x => x.UpsellPlans.ToList().ForEach(y =>
                            {
                                insPlan = getUPSellPlans(request,y, markup, markupType, discount, discountType);
                                availablePlans.UpsellPlans.Add(insPlan);
                            }));
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return availablePlans;
        }
        private string RemoveSpecialCharacters(string value)
        {
            return value.Replace("CDATA", "").Replace("[", "").Replace("]", "").Replace("!", "");
        }
        private string FormatHtmlTable(string value)
        {
            value = value.Trim();
            if (value.StartsWith("<table") && !value.EndsWith("</table>"))
                value += "</table>";
            if (!value.StartsWith("<table") && value.EndsWith("</table>"))
                value = "<table " + value;

            return value;
        }
        //Loading OTA plans
        private InsurancePlanDetails getOTAPlans(InsuranceRequest request, ZEUS.PlanOTA plan, decimal markup, string markupType, decimal discount, string discountType)
        {
            InsurancePlanDetails insPlan = new InsurancePlanDetails();
            insPlan.Code = RemoveSpecialCharacters(plan.PlanCode);
            insPlan.Description = RemoveSpecialCharacters(plan.PlanDesc);
            insPlan.SourceCurrency = RemoveSpecialCharacters(plan.CurrencyCode);
            insPlan.AdditionalInfoDesc = RemoveSpecialCharacters(plan.PlanAdditionalInfoDesc);
            insPlan.AdditionalInfoTitle = RemoveSpecialCharacters(plan.PlanAdditionalInfoTitle);
            insPlan.IsDefaultPlan = plan.IsDefaultPlan;
            insPlan.NoConsideration = RemoveSpecialCharacters(plan.PlanNoConsideration);
            insPlan.Content = FormatHtmlTable(RemoveSpecialCharacters(plan.PlanContent));
            insPlan.NoDesc = RemoveSpecialCharacters(plan.PlanNoDesc);
            insPlan.PremiumChargeType = plan.PlanPremiumChargeType;
            insPlan.SSRFeeCode = RemoveSpecialCharacters(plan.SSRFeeCode);
            insPlan.TermsAndConditions = RemoveSpecialCharacters(plan.PlanTnC);
            insPlan.Title = RemoveSpecialCharacters(plan.PlanTitle).Replace("Plan Title", "");
            insPlan.ROE = rateOfExchange;
            insPlan.SourceAmount = Convert.ToDecimal(plan.TotalPremiumAmount);
            insPlan.CurrencyCode = AgentCurrency;
            insPlan.NetFare = Math.Round(insPlan.SourceAmount * rateOfExchange, decimalPoint);
            /* agent Markup  Added by brahmam*/
            //calucating price details
            insPlan.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(insPlan.NetFare) * (markup / 100m)));
            insPlan.MarkupType = markupType;
            insPlan.MarkupValue = markup;
            insPlan.Discount = ((discountType == "F") ? discount : (Convert.ToDecimal(insPlan.NetFare) * (discount / 100m)));
            insPlan.DiscountType = discountType;
            insPlan.DiscountValue = discount;
            //INput vat calculation.
            Airport destinationAirport = new Airport(request.ArrivalStationCode);
            Airport originAirport = new Airport(request.DepartureStationCode);
            string countryCode = string.Empty, supplierCountryCode = string.Empty, SupplierCountryCode = string.Empty;
            decimal inputVAT = 0;
            string originCountryCode = originAirport != null ? originAirport.CountryCode : "";
            string destinationCountryCode = destinationAirport != null ? destinationAirport.CountryCode : "";
            if (originCountryCode == "AE" || originCountryCode == "SA")
            {
                countryCode = originCountryCode;
            }
            else if (destinationCountryCode == "AE" || destinationCountryCode == "SA")
            {
                countryCode = destinationCountryCode;
            }
            SupplierCountryCode = "AE";
            
            string destinationType = originCountryCode == destinationCountryCode ? "Domestic" : "International";
            PriceTaxDetails taxDetails = PriceTaxDetails.GetVATCharges(countryCode, request.LocationCountryCode, SupplierCountryCode, (int)ProductType.Insurance, Module.Insurance.ToString(), destinationType);
            insPlan.TotalAmount = Math.Round((insPlan.NetFare + insPlan.Markup - insPlan.Discount), decimalPoint);
            if (taxDetails != null && taxDetails.InputVAT != null)
            {
                taxDetails.InputVAT.CalculateVatAmount(insPlan.TotalAmount, ref inputVAT, decimalPoint);
            }
            insPlan.TaxDetail = taxDetails;
            insPlan.InputVATAmount = inputVAT;
            insPlan.TotalAmount = Math.Round((insPlan.NetFare + inputVAT + insPlan.Markup - insPlan.Discount), decimalPoint);
            insPlan.YesDesc = RemoveSpecialCharacters(plan.PlanYesDesc);
            insPlan.DecimalValue = decimalPoint;
            insPlan.PlanType = "OTA";
            //PlanMarketingPointers loading 
            List<MarketingPointer> marketingPointers = new List<MarketingPointer>();
            foreach (ZEUS.PlanMarketingPointer pmp in plan.PlanMarketingPointers)
            {
                MarketingPointer mp = new MarketingPointer();
                mp.PointId = pmp.PointID;
                mp.Description = pmp.PointDesc;
                marketingPointers.Add(mp);
            }
            insPlan.MarketingPointers = marketingPointers;
            //qualifiedPasengers loading but this coming always Empty
            List<InsurancePassenger> qualifiedPasengers = new List<InsurancePassenger>();
            foreach (ZEUS.PlanQualifiedPassenger pqp in plan.PlanQualifiedPassengers)
            {
                InsurancePassenger pd = new InsurancePassenger();
                pd.FirstName = pqp.FirstName;
                pd.LastName = pqp.LastName;
                pd.DOB = Convert.ToDateTime(pqp.DOB);
                pd.DocumentNo = pqp.IdentityNo;
                pd.IsQualified = pqp.IsPassengerQualified;
                qualifiedPasengers.Add(pd);
            }
            insPlan.QualifiedPassengers = qualifiedPasengers;
            insPlan.PlanPriceBreakDown = new List<PriceBreakDown>();
            //Loading price breakdown
            foreach (ZEUS.PlanPricingBreakdown ppbd in plan.PlanPricingBreakdown)
            {
                PriceBreakDown pbd = new PriceBreakDown();
                pbd.MinAge = ppbd.MinAge;
                pbd.MaxAge = ppbd.MaxAge;
                pbd.Gender = ppbd.Gender;
                pbd.CurrencyCode = ppbd.CurrencyCode;
                pbd.PremiumAmount = ppbd.PremiumAmount;
                insPlan.PlanPriceBreakDown.Add(pbd);
            }
            return insPlan;
        }
        private InsurancePlanDetails getUPSellPlans(InsuranceRequest request, ZEUS.UpsellPlan plan, decimal markup, string markupType, decimal discount, string discountType)
        {
            InsurancePlanDetails insPlan = new InsurancePlanDetails();
            insPlan.Code = RemoveSpecialCharacters(plan.PlanCode);
            insPlan.Description = RemoveSpecialCharacters(plan.PlanDesc);
            insPlan.SourceCurrency = RemoveSpecialCharacters(plan.CurrencyCode);
            insPlan.AdditionalInfoDesc = RemoveSpecialCharacters(plan.PlanAdditionalInfoDesc);
            insPlan.AdditionalInfoTitle = RemoveSpecialCharacters(plan.PlanAdditionalInfoTitle);
            insPlan.IsDefaultPlan = plan.IsDefaultPlan;
            insPlan.NoConsideration = RemoveSpecialCharacters(plan.PlanNoConsideration);
            insPlan.Content = FormatHtmlTable(RemoveSpecialCharacters(plan.PlanContent));
            insPlan.NoDesc = RemoveSpecialCharacters(plan.PlanNoDesc);
            insPlan.UPsellPremiumChargeType = plan.PlanPremiumChargeType;
            insPlan.SSRFeeCode = RemoveSpecialCharacters(plan.SSRFeeCode);
            insPlan.TermsAndConditions = RemoveSpecialCharacters(plan.PlanTnC);
            insPlan.Title = RemoveSpecialCharacters(plan.PlanTitle).Replace("Plan Title", "");
            insPlan.ROE = rateOfExchange;
            insPlan.SourceAmount = Convert.ToDecimal(plan.TotalPremiumAmount);
            insPlan.CurrencyCode = AgentCurrency;
            //without markup
            insPlan.NetFare = Math.Round(insPlan.SourceAmount * rateOfExchange, decimalPoint);
            insPlan.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(insPlan.NetFare) * (markup / 100m)));
            insPlan.MarkupType = markupType;
            insPlan.MarkupValue = markup;
            insPlan.Discount = ((discountType == "F") ? discount : (Convert.ToDecimal(insPlan.NetFare) * (discount / 100m)));
            insPlan.DiscountType = discountType;
            insPlan.DiscountValue = discount;
            insPlan.DecimalValue = decimalPoint;
            //with markup and discount
            //INput vat calculation.
            Airport destinationAirport = new Airport(request.ArrivalStationCode);
            Airport originAirport = new Airport(request.DepartureStationCode);
            string countryCode = string.Empty, supplierCountryCode = string.Empty, SupplierCountryCode = string.Empty;
            decimal inputVAT = 0;
            string originCountryCode = originAirport != null ? originAirport.CountryCode : "";
            string destinationCountryCode = destinationAirport != null ? destinationAirport.CountryCode : "";
            if (originCountryCode == "AE" || originCountryCode == "SA")
            {
                countryCode = originCountryCode;
            }
            else if (destinationCountryCode == "AE" || destinationCountryCode == "SA")
            {
                countryCode = destinationCountryCode;
            }
            SupplierCountryCode ="AE";
            string destinationType = originCountryCode == destinationCountryCode ? "Domestic" : "International";
            PriceTaxDetails taxDetails = PriceTaxDetails.GetVATCharges(countryCode, request.LocationCountryCode, SupplierCountryCode, (int)ProductType.Insurance, Module.Insurance.ToString(), destinationType);
            insPlan.TotalAmount = Math.Round((insPlan.NetFare + insPlan.Markup - insPlan.Discount), decimalPoint);
            if (taxDetails != null && taxDetails.InputVAT != null)
            {
                taxDetails.InputVAT.CalculateVatAmount(insPlan.TotalAmount, ref inputVAT, decimalPoint);
            }
            insPlan.TaxDetail = taxDetails;
            insPlan.InputVATAmount = inputVAT;
            insPlan.TotalAmount = Math.Round((insPlan.NetFare + inputVAT + insPlan.Markup - insPlan.Discount), decimalPoint);
            insPlan.YesDesc = RemoveSpecialCharacters(plan.PlanYesDesc);
            insPlan.PlanType = "UPSELL";
            List<InsurancePassenger> qualifiedPasengers = new List<InsurancePassenger>();
            foreach (ZEUS.PlanQualifiedPassenger pqp in plan.PlanQualifiedPassengers)
            {
                InsurancePassenger pd = new InsurancePassenger();
                pd.FirstName = pqp.FirstName;
                pd.LastName = pqp.LastName;
                pd.DOB = Convert.ToDateTime(pqp.DOB);
                pd.DocumentNo = pqp.IdentityNo;
                pd.IsQualified = pqp.IsPassengerQualified;
                qualifiedPasengers.Add(pd);
            }
            insPlan.QualifiedPassengers = qualifiedPasengers;
            insPlan.PlanPriceBreakDown = new List<PriceBreakDown>();

            //Loading price breakdown
            foreach (ZEUS.PlanPricingBreakdown ppbd in plan.PlanPricingBreakdown)
            {
                PriceBreakDown pbd = new PriceBreakDown();
                pbd.MinAge = ppbd.MinAge;
                pbd.MaxAge = ppbd.MaxAge;
                pbd.Gender = ppbd.Gender;
                pbd.CurrencyCode = ppbd.CurrencyCode;
                pbd.PremiumAmount = ppbd.PremiumAmount;
                insPlan.PlanPriceBreakDown.Add(pbd);
            }
            return insPlan;
        }
        public void ConfirmPolicyPurchase(ref InsuranceHeader header)
        {
            try
            {
                //Check The Pax Details are Exist in DB 
                if(!header.CheckDuplicatePaxDetails(header))  //Pax Details are not Exist in DB 
                {
                    ZEUS.ConfirmPurchaseResponseWithRiders purchaseResponse = null;
                    int bookingFailedCount = 0;
                    int n = 0;
                    //Save the Insurance data before confirm policy call
                    try
                    {
                        header.AgentRefNo = "CZT" + TraceIdGeneration();
                        header.InsPlans.ForEach(x => x.ProposalState = "PROPOSAL");
                        header.Save();//Saving the Insurance Data
                    }
                    catch (Exception ex)
                    {
                        SendFailureMail(header, ex.ToString());
                        throw ex;
                    }

                    foreach (InsurancePlan plan in header.InsPlans)
                    {

                        ZEUS.InputRequestWithRiders insPlanRequest = new ZEUS.InputRequestWithRiders();

                        //Authentication details            
                        insPlanRequest.Authentication = new ZEUS.ItineraryAuthentication();
                        insPlanRequest.Authentication.Username = _userName;
                        insPlanRequest.Authentication.Password = _password;

                        //Header details
                        insPlanRequest.Header = new ZEUS.ItineraryHeaderWithRiders();
                        insPlanRequest.Header.Channel = _channel;
                        insPlanRequest.Header.ItineraryID = "0";
                        insPlanRequest.Header.PNR = header.PNR;
                        insPlanRequest.Header.PolicyNo = string.Empty;
                        insPlanRequest.Header.PurchaseDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"); //"2013-10-23 00:00:00";// For test access

                        //Before Currency We are Passing PurchasesRiders
                        //insPlanRequest.Header.PurchasesRiders = new ZEUS.PurchaseRider[1];
                        //ZEUS.PurchaseRider purchaseRiders = new ZEUS.PurchaseRider();
                        //purchaseRiders.SSRFeeCode = plan.SSRFeeCode;
                        //purchaseRiders.TotalPremium = Convert.ToDouble(plan.SourceAmount);
                        //purchaseRiders.Currency = plan.SourceCurrency;
                        //insPlanRequest.Header.PurchasesRiders[0] = purchaseRiders;

                        //New Implementation
                        insPlanRequest.Header.PurchasesMains = new Zeus.PurchaseMain[1];
                        ZEUS.PurchaseMain purchaseMain = new Zeus.PurchaseMain();
                        purchaseMain.SSRFeeCode = plan.SSRFeeCode;
                        purchaseMain.TotalPremium = Convert.ToDouble(plan.SourceAmount);
                        purchaseMain.Currency = plan.SourceCurrency;
                        insPlanRequest.Header.PurchasesMains[0] = purchaseMain;

                        insPlanRequest.Header.CountryCode = header.CountryCode;
                        insPlanRequest.Header.CultureCode = header.CultureCode;
                        insPlanRequest.Header.TotalAdults = header.Adults;
                        insPlanRequest.Header.TotalChild = header.Childs;
                        insPlanRequest.Header.TotalInfants = header.Infants;

                        //Contact details
                        insPlanRequest.ContactDetails = new ZEUS.ItineraryContact();
                        insPlanRequest.ContactDetails.ContactPerson = header.ContactPersonName;
                        insPlanRequest.ContactDetails.Address1 = string.Empty;
                        insPlanRequest.ContactDetails.Address2 = string.Empty;
                        insPlanRequest.ContactDetails.Address3 = string.Empty;
                        insPlanRequest.ContactDetails.PostCode = string.Empty;
                        insPlanRequest.ContactDetails.City = string.Empty;
                        insPlanRequest.ContactDetails.State = string.Empty;
                        insPlanRequest.ContactDetails.Country = string.Empty;
                        insPlanRequest.ContactDetails.EmailAddress = header.EmailAddress;
                        insPlanRequest.ContactDetails.HomePhoneNum = string.Empty;
                        insPlanRequest.ContactDetails.MobilePhoneNum = string.Empty;
                        insPlanRequest.ContactDetails.OtherPhoneNum = string.Empty;

                        //Flight Details
                        ZEUS.ItineraryFlight[] flightDetails = new ZEUS.ItineraryFlight[1];
                        ZEUS.ItineraryFlight flightDetail = new ZEUS.ItineraryFlight();

                        flightDetail.DepartCountryCode = header.DepartureCountryCode;
                        flightDetail.DepartStationCode = header.DepartureStationCode;
                        flightDetail.ArrivalCountryCode = header.ArrivalCountryCode;
                        flightDetail.ArrivalStationCode = header.ArrivalStationCode;
                        flightDetail.DepartAirlineCode = header.DepartureAirlineCode;
                        flightDetail.DepartDateTime = header.DepartureDateTime;
                        flightDetail.ReturnAirlineCode = header.ReturnAirlineCode;
                        flightDetail.ReturnDateTime = header.ReturnDateTime;
                        flightDetail.ReturnFlightNo = header.DepartureFlightNo;
                        flightDetail.DepartFlightNo = header.ReturnFlightNo;

                        flightDetails[0] = flightDetail;

                        insPlanRequest.Flights = flightDetails;
                        IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                        ZEUS.ItineraryPassenger[] passengerDetails = new ZEUS.ItineraryPassenger[header.InsPassenger.Count];
                        for (int i = 0; i < header.InsPassenger.Count; i++) //assigning passenger details
                        {
                            InsurancePassenger paxDetail = header.InsPassenger[i];
                            ZEUS.ItineraryPassenger passengerDetail = new ZEUS.ItineraryPassenger();
                            passengerDetail.FirstName = paxDetail.FirstName;
                            passengerDetail.LastName = paxDetail.LastName;
                            passengerDetail.Gender = paxDetail.Gender;
                            passengerDetail.DOB = paxDetail.DOB.ToString("yyyy-MM-dd 00:00:00");
                            //passengerDetail.IsInfant = (paxDetail.IsInfant ? "1" : "0");
                            passengerDetail.IsInfant = (paxDetail.PaxType.ToUpper()=="INFANT" ? "1" : "0");
                            
                            passengerDetail.Age = (Convert.ToDateTime(header.DepartureDateTime, dateFormat).Subtract(paxDetail.DOB).Days / 365).ToString();
                            passengerDetail.IdentityType = paxDetail.DocumentType;
                            passengerDetail.IdentityNo = paxDetail.DocumentNo;
                            //passengerDetail.CountryOfResidence = paxDetail.Country.Split('-')[1];
                            passengerDetail.CountryOfResidence = paxDetail.Country.Split('-').Length > 0 ? paxDetail.Country.Split('-')[1] : paxDetail.Country;
                            passengerDetail.Nationality = paxDetail.Nationality;
                            passengerDetail.CurrencyCode = plan.SourceCurrency;
                            passengerDetail.SelectedPlanCode = plan.InsPlanCode;
                            passengerDetail.SelectedSSRFeeCode = plan.SSRFeeCode;
                            if (paxDetail.PaxType.ToUpper() != "INFANT")
                            {
                                passengerDetail.PassengerPremiumAmount = Convert.ToDouble(plan.SourceAmount / (header.Adults + header.Childs));
                            }
                            else
                            {
                                passengerDetail.PassengerPremiumAmount = 0;
                            }
                            passengerDetail.IsQualified = paxDetail.IsQualified;
                            passengerDetails[i] = passengerDetail;
                        }
                        insPlanRequest.Passengers = passengerDetails;
                        try
                        {
                            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(insPlanRequest.GetType());
                            string filePath = _xmlPath + SessionId + "_" + header.DepartureStationCode + "_" + header.ArrivalStationCode + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_ZeusInsConfirmPolicyRequest_" + n + ".xml";
                            //string filePath = _xmlPath + "ZeusInsConfirmPolicyRequest_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_" + n + ".xml";
                            System.IO.StreamWriter writer = new System.IO.StreamWriter(filePath);
                            ser.Serialize(writer, insPlanRequest);  // Here Classes are converted to XML String. 
                            writer.Close();
                            ser = null;
                            //CT.Core.Audit.Add(CT.Core.EventType.InsuranceBooking, CT.Core.Severity.Normal, 0, "ZeusInsConfirmPolicyRequest Genarated", "");
                        }
                        catch (Exception ex)
                        {
                            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, "Failed to Save Zeus Confirm Policy Request. Reason :" + ex.ToString(), HttpContext.Current.Request["REMOTE_ADDR"]);
                        }

                        purchaseResponse = insGateway.ConfirmPurchaseWithRiders((insPlanRequest));


                        try
                        {
                            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(purchaseResponse.GetType());
                            string filePath = _xmlPath + SessionId + "_" + header.DepartureStationCode + "_" + header.ArrivalStationCode + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_ZeusInsConfirmPolicyResponse_" + n + ".xml";
                            //string filePath = _xmlPath + "ZeusInsConfirmPolicyResponse_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_" + n + ".xml";
                            System.IO.StreamWriter writer = new System.IO.StreamWriter(filePath);
                            ser.Serialize(writer, purchaseResponse);  // Here Classes are converted to XML String. 

                            writer.Close();
                            ser = null;
                            // CT.Core.Audit.Add(CT.Core.EventType.InsuranceBooking, CT.Core.Severity.Normal, 0, "ZeusInsConfirmPolicyResponse Genarated", "");
                        }
                        catch (Exception ex)
                        {
                            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, "Failed to Save Zeus Confirm Policy Response. Reason :" + ex.ToString(), HttpContext.Current.Request["REMOTE_ADDR"]);
                        }
                        n++;
                        if (purchaseResponse.ErrorCode != "0") //checking Error
                        {
                            bookingFailedCount++;
                            plan.ErrorCode = purchaseResponse.ErrorCode;
                            plan.ErrorMessage = purchaseResponse.ErrorMessage;
                            plan.ProposalState = purchaseResponse.ProposalState.ToString();
                            header.TotalAmount = header.TotalAmount - (plan.NetAmount + plan.InputVATAmount + plan.OutputVATAmount + plan.Markup - plan.Discount);
                            CT.Core.Audit.Add(CT.Core.EventType.InsuranceBooking, CT.Core.Severity.High, 0, "Insurance Booking Failed for" + plan.PlanTitle, "");
                        }
                        else
                        {
                            //CT.Core.Audit.Add(CT.Core.EventType.InsuranceBooking, CT.Core.Severity.High, 0, "Insurance Booking confirmed for" + plan.PlanTitle, "");
                            //Loading confirmation details
                            plan.ItineraryID = purchaseResponse.ItineraryID;
                            plan.PolicyNo = purchaseResponse.PolicyNo;
                            plan.PolicyPurchasedDate = Convert.ToDateTime(purchaseResponse.PolicyPurchasedDateTime);
                            plan.ProposalState = purchaseResponse.ProposalState.ToString();
                            plan.PlanStatus = InsuranceBookingStatus.Confirmed;
                            foreach (InsurancePassenger pax in header.InsPassenger)
                            {
                                foreach (ZEUS.ConfirmedPassenger cpax in purchaseResponse.ConfirmedPassengers)
                                {
                                    if (cpax.IdentityNo == pax.DocumentNo)  //storing policy no and policyLink pipe separated
                                    {
                                        if (!string.IsNullOrEmpty(pax.PolicyNo))
                                        {
                                            pax.PolicyNo = pax.PolicyNo + "|" + cpax.PolicyNo;
                                        }
                                        else
                                        {
                                            pax.PolicyNo = cpax.PolicyNo;
                                        }
                                        if (!string.IsNullOrEmpty(pax.PolicyUrlLink))
                                        {
                                            pax.PolicyUrlLink = pax.PolicyUrlLink + "|" + System.Web.HttpUtility.HtmlDecode(cpax.PolicyURLLink);
                                        }
                                        else
                                        {
                                            pax.PolicyUrlLink = System.Web.HttpUtility.HtmlDecode(cpax.PolicyURLLink);
                                        }
                                        pax.IsQualified = true;
                                    }
                                }
                            }
                        }
                    }
                    if (header.InsPlans.Count == bookingFailedCount) //checking all booking failed or not
                    {
                        //Return the error message shiva 15 Apr 2019
                        header.Error = purchaseResponse.ErrorMessage;//"InsuranceBookingFailed";

                    }
                    else
                    {
                        try
                        {
                            header.Error = "0";
                            header.Status = InsuranceBookingStatus.Confirmed;
                            //header.AgentRefNo = "CZT" + TraceIdGeneration(); //Genaratine AgentRefNo
                            InsuranceHeader.UpdatePolicyDetails(header);
                        }
                        catch (Exception ex) //Saving faile time sending Email
                        {
                            SendFailureMail(header, ex.ToString());
                            Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, "Insurance Booking is Sucess but not saved" + ex.ToString(), "");
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, ex.ToString(), "");
                throw ex;
            }

        }
        private string TraceIdGeneration()
        {
            string traceid = DateTime.Now.ToString("ddMMyyyyHHmmsss");
            return "CZ" + traceid;
        }

        public Dictionary<string, string> CancelPolicy(string itineraryId, string pnr, string policyNo, DateTime purchaseDate)
        {
            Dictionary<string, string> cancellationData = new Dictionary<string, string>();
            ZEUS.ItineraryAuthentication authentication = new ZEUS.ItineraryAuthentication();
            //Authentication details            
            authentication.Username = _userName;
            authentication.Password = _password;

            ZEUS.PolicyHeader Header = new ZEUS.PolicyHeader();
            Header.ItineraryID = itineraryId;
            Header.PNR = pnr;
            Header.PolicyNo = policyNo;
            Header.PurchaseDate = purchaseDate.ToString("yyyy-MM-dd 00:00:00");

            try
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(Header.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, Header);  // Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(_xmlPath + "ZeusInsCancelPolicyREQ" + DateTime.Now.ToString("ddMMyyy_hhmmss") + ".xml");
                //Audit.Add(EventType.InsuranceBooking, Severity.High, 0, "Insurance CancelPolicy request genarated", "");
            }
            catch
            {
            }

            ZEUS.PolicyResponse Response = insGateway.CancelPolicy(authentication, Header);

            try
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(Response.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, Response);  // Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(_xmlPath + "ZeusInsCancelPolicyRES" + DateTime.Now.ToString("ddMMyyy_hhmmss") + ".xml");
            }
            catch
            {
            }

            if (Response != null)
            {
                if (Response.ErrorCode != "0")
                {
                    cancellationData.Add("Status", "Failed");
                    Audit.Add(EventType.InsuranceBooking, Severity.High, 0, "Insurance CancelPolicy Filed for policy No:" + policyNo + "," + Response.ErrorMessage, "");
                }
                else
                {
                    Audit.Add(EventType.InsuranceBooking, Severity.High, 0, "Insurance CancelPolicy confirmed for policy No:" + policyNo, "");
                    cancellationData.Add("Status", "Cancelled");
                }
            }
            else
            {
                cancellationData.Add("Status", "Failed");
            }
            return cancellationData;
        }

        private void SendFailureMail(InsuranceHeader header, string errorMessage)
        {
            try
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, errorMessage.ToString(), "");
                ConfigurationSystem con = new ConfigurationSystem();
                Hashtable hostPort = con.GetHostPort();
                string fromEmail = hostPort["fromEmail"].ToString();
                string toEmail = hostPort["ErrorNotificationMailingId"].ToString();
                string planCode = string.Empty;
                if (header.InsPlans != null && header.InsPlans.Count > 0)
                {
                    for (int i = 0; i < header.InsPlans.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(planCode))
                        {
                            planCode = planCode + "," + header.InsPlans[0].InsPlanCode;
                        }
                        else
                        {
                            planCode = header.InsPlans[0].InsPlanCode;
                        }
                    }
                }
                string messageText = "Booking might have confirmed but could not able to save for Plans " + planCode + " kindly contact cozmo team before you proceed to book the same";
                header.Error = messageText;
                messageText = messageText + " " + errorMessage.ToString();

                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                Email.Send(fromEmail, toEmail, toArray, "Error while saving Insurance Booking.", messageText, new Hashtable());
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, ex.ToString(), "");
            }
        }
    }
}
