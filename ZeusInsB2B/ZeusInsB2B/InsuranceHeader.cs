﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;

namespace CT.BookingEngine.Insurance
{
    [Serializable]
    public class InsuranceHeader
    {
        #region Members
        int _id;
        string _departureCountryCode;
        string _arrivalCountryCode;
        string _countryCode;
        string _cultureCode;
        string _departureStationCode;
        string _arrivalStationCode;
        string _departureAirlineCode;
        string _returnAirlineCode;
        string _departureDateTime;
        string _returnDateTime;
        string _departureFlightNo;
        string _returnFlightNo;
        int _adults;
        int _childs;
        int _infants;
        string _pnr;
        string _product;
        string _currency;
        decimal _totalAmount;
        string _transType;
        int _agentId;
        int _locationId;
        string _agentRefNo;
        InsuranceBookingStatus _status;
        int _createdBy;
        List<InsurancePassenger> _insPassenger;
        List<InsurancePlan> _insPlans;
        string _contactPersonName;
        string _emailAddress;
        string _error;
        DateTime _cretedOn;
        string _sessionId;
        ModeOfPayment paymentMode;
        bool isSeniorCitizen;
        //added for to save nominee details 
        string nomineeFirstName;
        string nomineeLastName;
        string nomineeMobileNo;
        string nomineeEmailAddress;
        string nomineeCountry;
        string nomineeRemarks;
        string pseudoCode;        
        private decimal outputVATAmount;
        string sourceCurrency;
        #endregion

        #region properities

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }
        public string CultureCode
        {
            get { return _cultureCode; }
            set { _cultureCode = value; }
        }
        public string DepartureCountryCode
        {
            get { return _departureCountryCode; }
            set { _departureCountryCode = value; }
        }
        public string ArrivalCountryCode
        {
            get { return _arrivalCountryCode; }
            set { _arrivalCountryCode = value; }
        }
        public string DepartureStationCode
        {
            get { return _departureStationCode; }
            set { _departureStationCode = value; }
        }
        public string ArrivalStationCode
        {
            get { return _arrivalStationCode; }
            set { _arrivalStationCode = value; }
        }
        public string DepartureAirlineCode
        {
            get { return _departureAirlineCode; }
            set { _departureAirlineCode = value; }
        }
        public string ReturnAirlineCode
        {
            get { return _returnAirlineCode; }
            set { _returnAirlineCode = value; }
        }
        public string DepartureFlightNo
        {
            get { return _departureFlightNo; }
            set { _departureFlightNo = value; }
        }
        public string DepartureDateTime
        {
            get { return _departureDateTime; }
            set { _departureDateTime = value; }
        }
        public string ReturnFlightNo
        {
            get { return _returnFlightNo; }
            set { _returnFlightNo = value; }
        }
        public string ReturnDateTime
        {
            get { return _returnDateTime; }
            set { _returnDateTime = value; }
        }
        public int Adults
        {
            get { return _adults; }
            set { _adults = value; }
        }
        public int Childs
        {
            get { return _childs; }
            set { _childs = value; }
        }
        public int Infants
        {
            get { return _infants; }
            set { _infants = value; }
        }
        public string PNR
        {
            get { return _pnr; }
            set { _pnr = value; }
        }
        public string Product
        {
            get { return _product; }
            set { _product = value; }
        }
        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
        public decimal TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }
        public string TransType
        {
            get { return _transType; }
            set { _transType = value; }
        }
        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public int LocationId
        {
            get { return _locationId; }
            set { _locationId = value; }
        }
        public string AgentRefNo
        {
            get { return _agentRefNo; }
            set { _agentRefNo = value; }
        }
        public InsuranceBookingStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public List<InsurancePassenger> InsPassenger
        {
            get { return _insPassenger; }
            set { _insPassenger = value; }
        }
        public List<InsurancePlan> InsPlans
        {
            get { return _insPlans; }
            set { _insPlans = value; }
        }
        public string ContactPersonName
        {
            get { return _contactPersonName; }
            set { _contactPersonName = value; }
        }
        public string EmailAddress
        {
            get { return _emailAddress; }
            set { _emailAddress = value; }
        }
        public string Error
        {
            get { return _error; }
            set { _error = value; }
        }
        public DateTime CretedOn
        {
            get { return _cretedOn; }
            set { _cretedOn = value; }
        }

        public string SessionId
        {
            get
            {
                return _sessionId;
            }

            set
            {
                _sessionId = value;
            }
        }

        public string PseudoCode
        {
            get { return pseudoCode; }
            set { pseudoCode = value; }
        }

        public ModeOfPayment PaymentMode { get => paymentMode; set => paymentMode = value; }
        public bool IsSeniorCitizen { get => isSeniorCitizen; set => isSeniorCitizen = value; }
        public string NomineeFirstName { get => nomineeFirstName; set => nomineeFirstName = value; }
        public string NomineeLastName { get => nomineeLastName; set => nomineeLastName = value; }
        public string NomineeMobileNo { get => nomineeMobileNo; set => nomineeMobileNo = value; }
        public string NomineeEmailAddress { get => nomineeEmailAddress; set => nomineeEmailAddress = value; }
        public string NomineeCountry { get => nomineeCountry; set => nomineeCountry = value; }
        public string NomineeRemarks { get => nomineeRemarks; set => nomineeRemarks = value; }
        public decimal OutputVATAmount { get => outputVATAmount; set => outputVATAmount = value; }
        public string SourceCurrency { get => sourceCurrency; set => sourceCurrency = value; }
        #endregion
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;  
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                SqlParameter[] paramList = new SqlParameter[25];
                paramList[0] = new SqlParameter("@P_DepartureCountryCode", _departureCountryCode);
                paramList[1] = new SqlParameter("@P_ArrivalCountryCode", _arrivalCountryCode);
                paramList[2] = new SqlParameter("@P_DepartureStationCode", _departureStationCode);
                paramList[3] = new SqlParameter("@P_ArrivalStationCode", _arrivalStationCode);
                paramList[4] = new SqlParameter("@P_DepartureDate", _departureDateTime);
                paramList[5] = new SqlParameter("@P_ReturnDate", _returnDateTime);
                if(!string.IsNullOrEmpty(_departureAirlineCode)) paramList[6] = new SqlParameter("@P_DepartureAirlineCode", _departureAirlineCode);
                if (!string.IsNullOrEmpty(_returnAirlineCode)) paramList[7] = new SqlParameter("@P_ReturnAirlineCode", _returnAirlineCode);
                if(!string.IsNullOrEmpty(_departureFlightNo))  paramList[8] = new SqlParameter("@P_DepartureFlightNo", _departureFlightNo);
                if(!string.IsNullOrEmpty(_returnFlightNo)) paramList[9] = new SqlParameter("@P_ReturnFlightNo", _returnFlightNo);
                paramList[10] = new SqlParameter("@P_Adults", _adults);
                paramList[11] = new SqlParameter("@P_Childs", _childs);
                paramList[12] = new SqlParameter("@P_Infants", _infants);
                paramList[13] = new SqlParameter("@P_PNR", _pnr);
                paramList[14] = new SqlParameter("@P_Product", _product);
                paramList[15] = new SqlParameter("@P_CurrencyCode", _currency);
                paramList[16] = new SqlParameter("@P_TotalAmount", _totalAmount);
                paramList[17] = new SqlParameter("@P_TransType", _transType);
                paramList[18] = new SqlParameter("@P_agencyId", _agentId);
                paramList[19] = new SqlParameter("@P_locationId",_locationId);
                paramList[20] = new SqlParameter("@P_AgetRefNO", _agentRefNo);
                paramList[21] = new SqlParameter("@P_status", _status);
                paramList[22] = new SqlParameter("@P_createdBy", _createdBy);
                paramList[23] = new SqlParameter("@P_Ins_Id", _id);
                paramList[23].Direction = ParameterDirection.Output;
                List<SqlParameter> parameters = new List<SqlParameter>(paramList);
                parameters.Add(new SqlParameter("@P_PaymentMode",paymentMode));
                parameters.Add(new SqlParameter("@P_IsSeniorAdult", isSeniorCitizen ? 'Y' : 'N'));
                parameters.Add(new SqlParameter("@P_NomineeFirstName",  nomineeFirstName));
                parameters.Add(new SqlParameter("@P_NomineeLastName",nomineeLastName));
                parameters.Add(new SqlParameter("@P_NomineeMobileNo", NomineeMobileNo));
                parameters.Add(new SqlParameter("@P_NomineeEmailAddress",nomineeEmailAddress));
                parameters.Add(new SqlParameter("@P_NomineeCountry",nomineeCountry));
                parameters.Add(new SqlParameter("@P_NomineeRemarks",nomineeRemarks));
                
                int count = DBGateway.ExecuteNonQueryDetails(cmd,"usp_AddInsuranceHeader", parameters.ToArray());

                if (count > 0 && paramList[23].Value != DBNull.Value)
                {
                    _id = Convert.ToInt32(paramList[23].Value);
                }
                try
                {
                    foreach (InsurancePlan plan in _insPlans)
                    {
                        plan.HeaderId = _id;
                        plan.Save(cmd);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                try
                {
                    foreach (InsurancePassenger pax in _insPassenger)
                    {
                        pax.HeaderId = _id;
                        pax.Eligible = "Y";
                        pax.Save(cmd);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
        }
        public void RetrieveConfirmedPlan(int headerId)
        {
            if (headerId <= 0)
            {
                throw new ArgumentException("header Id should be positive integer");
            }
            this._id = headerId;
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@P_Ins_Id", headerId);
            try
            {
                //SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetInsuranceHeader", paramList, connection);
                using (DataTable dtInsurance = DBGateway.FillDataTableSP("usp_GetInsuranceHeader", paramList))
                {
                    if (dtInsurance !=null && dtInsurance.Rows.Count > 0)
                    {
                        DataRow data = dtInsurance.Rows[0];
                        _id = Convert.ToInt32(data["Ins_Id"]);
                        if (data["DepartureCountryCode"] != DBNull.Value)
                        {
                            _departureCountryCode = Convert.ToString(data["DepartureCountryCode"]);
                        }
                        if (data["ArrivalCountryCode"] != DBNull.Value)
                        {
                            _arrivalCountryCode = Convert.ToString(data["ArrivalCountryCode"]);
                        }
                        if (data["DepartureStationCode"] != DBNull.Value)
                        {
                            _departureStationCode = Convert.ToString(data["DepartureStationCode"]);
                        }
                        if (data["ArrivalStationCode"] != DBNull.Value)
                        {
                            _arrivalStationCode = Convert.ToString(data["ArrivalStationCode"]);
                        }
                        if (data["DepartureDate"] != DBNull.Value)
                        {
                            _departureDateTime = Convert.ToString(data["DepartureDate"]);
                        }
                        if (data["ReturnDate"] != DBNull.Value)
                        {
                            _returnDateTime = Convert.ToString(data["ReturnDate"]);
                        }
                        if (data["DepartureAirlineCode"] != DBNull.Value)
                        {
                            _departureAirlineCode = Convert.ToString(data["DepartureAirlineCode"]);
                        }
                        if (data["ReturnAirlineCode"] != DBNull.Value)
                        {
                            _returnAirlineCode = Convert.ToString(data["ReturnAirlineCode"]);
                        }
                        if (data["DepartureFlightNo"] != DBNull.Value)
                        {
                            _departureFlightNo = Convert.ToString(data["DepartureFlightNo"]);
                        }
                        if (data["ReturnFlightNo"] != DBNull.Value)
                        {
                            _returnFlightNo = Convert.ToString(data["ReturnFlightNo"]);
                        }
                        if (data["Adults"] != DBNull.Value)
                        {
                            _adults = Convert.ToInt32(data["Adults"]);
                        }
                        if (data["Childs"] != DBNull.Value)
                        {
                            _childs = Convert.ToInt32(data["Childs"]);
                        }
                        if (data["Infants"] != DBNull.Value)
                        {
                            _infants = Convert.ToInt32(data["Infants"]);
                        }
                        if (data["PNR"] != DBNull.Value)
                        {
                            _pnr = Convert.ToString(data["PNR"]);
                        }
                        if (data["PNR"] != DBNull.Value)
                        {
                            _product = Convert.ToString(data["Product"]);
                        }
                        if (data["CurrencyCode"] != DBNull.Value)
                        {
                            _currency = Convert.ToString(data["CurrencyCode"]);
                        }
                        if (data["CurrencyCode"] != DBNull.Value)
                        {
                            _totalAmount = Convert.ToDecimal(data["TotalAmount"]);
                        }
                        if (data["TransType"] != DBNull.Value)
                        {
                            _transType = Convert.ToString(data["TransType"]);
                        }
                        if (data["AgencyId"] != DBNull.Value)
                        {
                            _agentId = Convert.ToInt32(data["AgencyId"]);
                        }
                        if (data["AgencyId"] != DBNull.Value)
                        {
                            _locationId = Convert.ToInt32(data["LocationId"]);
                        }
                        if (data["AgetRefNO"] != DBNull.Value)
                        {
                            _agentRefNo = Convert.ToString(data["AgetRefNO"]);
                        }
                        if (data["status"] != DBNull.Value)
                        {
                            _status = (InsuranceBookingStatus)Convert.ToInt32(data["status"]);
                        }
                        if (data["CreatedBy"] != DBNull.Value)
                        {
                            _createdBy = Convert.ToInt32(data["CreatedBy"]);
                        }
                        if (data["CreatedOn"] != DBNull.Value)
                        {
                            _cretedOn = Convert.ToDateTime(data["CreatedOn"]);
                        }
                        if (data["PaymentMode"] != DBNull.Value)
                        {
                            paymentMode = (ModeOfPayment)Convert.ToInt32(data["paymentMode"]);
                        }
                        if (data["NomineeFirstName"] != DBNull.Value)
                        {
                            nomineeFirstName = Convert.ToString(data["NomineeFirstName"]);
                        }
                        if (data["NomineeLastName"] != DBNull.Value)
                        {
                            nomineeLastName = Convert.ToString(data["NomineeLastName"]);
                        }
                        if (data["NomineeEmailAddress"] != DBNull.Value)
                        {
                            NomineeEmailAddress = Convert.ToString(data["NomineeEmailAddress"]);
                        }
                        if (data["NomineeMobileNo"] != DBNull.Value)
                        {
                            nomineeMobileNo = Convert.ToString(data["NomineeMobileNo"]);
                        }
                        if (data["NomineeCountry"] != DBNull.Value)
                        {
                            nomineeCountry = Convert.ToString(data["NomineeCountry"]);
                        }
                        if (data["NomineeRemarks"] != DBNull.Value)
                        {
                            nomineeRemarks = Convert.ToString(data["NomineeRemarks"]);
                        }
                        if (data["SourceCurrency"] != DBNull.Value)
                        {
                            sourceCurrency = Convert.ToString(data["SourceCurrency"]);
                        }
                        
                        InsurancePlan plan = new InsurancePlan();
                        _insPlans = plan.Load(_id);
                        InsurancePassenger passenger = new InsurancePassenger();
                        _insPassenger = passenger.Load(_id);
                    }
                    //else
                    //{
                    //    data.Close();
                    //    connection.Close();
                    //}
                }
                //data.Close();
                //connection.Close();
            }
            catch (Exception exp)
            {
                //connection.Close();
                throw new ArgumentException("Fleet id does not exist in database exception:"+exp.Message);
            }
            
        }

        public static void UpdateInsuranceHeaderStatus(int insId, int status)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_InsId", insId);
                paramList[1] = new SqlParameter("@P_BookingStatus", status);
                int count = DBGateway.ExecuteNonQuerySP("usp_UpdateInsuranceHeaderStatus", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdatePolicyDetails(InsuranceHeader header)
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                //Update Policy Status in Insurance header

                SqlParameter[] parameter = new SqlParameter[3];
                parameter[0] = new SqlParameter("@InsId", header.InsPlans[0].HeaderId);
                parameter[1] = new SqlParameter("@BookingStatus", InsuranceBookingStatus.Confirmed);
                parameter[2] = new SqlParameter("@TotalAmount", header.TotalAmount);
                DBGateway.ExecuteNonQueryDetails(cmd,"Usp_UpdateInsuranceHeader", parameter);

                //Update Insurance Plan Details
                foreach (InsurancePlan plan in header.InsPlans)
                {
                    SqlParameter[] param = new SqlParameter[7];
                    param[0] = new SqlParameter("@InsId", plan.HeaderId);
                    param[1] = new SqlParameter("@InsPlanCode", plan.InsPlanCode);
                    param[2] = new SqlParameter("@SSRFeeCode", plan.SSRFeeCode);
                    param[3] = new SqlParameter("@ItineraryId", plan.ItineraryID);
                    param[4] = new SqlParameter("@PolicyNo", plan.PolicyNo);
                    param[5] = new SqlParameter("@ProposalState", plan.ProposalState);
                    param[6] = new SqlParameter("@BookingStats", plan.PlanStatus);
                    DBGateway.ExecuteNonQueryDetails(cmd, "usp_UpdateInsurancePlanDetails", param);

                }
                //Update Insurance Details Passenger wise
                foreach (InsurancePassenger passenger in header.InsPassenger)
                {
                    SqlParameter[] sqlParameters = new SqlParameter[3];
                    sqlParameters[0] = new SqlParameter("@paxId", passenger.PaxId);
                    sqlParameters[1] = new SqlParameter("@policyNo", passenger.PolicyNo);
                    sqlParameters[2] = new SqlParameter("@policyUrl", passenger.PolicyUrlLink);
                    DBGateway.ExecuteNonQueryDetails(cmd, "usp_UpdateInsurancePaxPolicyDetails", sqlParameters);
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                throw ex;
            }
        }

        public bool CheckDuplicatePaxDetails(InsuranceHeader header)
        {
            bool paxExist = false;
            try
            {
                foreach (InsurancePassenger passenger in header.InsPassenger)
                {
                    SqlParameter[] sqlParameter = new SqlParameter[7];
                    sqlParameter[0] = new SqlParameter("@departureStationCode", header.DepartureStationCode);
                    sqlParameter[1] = new SqlParameter("@arrivalStationCode", header.ArrivalStationCode);
                    sqlParameter[2] = new SqlParameter("@departureDate", header.DepartureDateTime);
                    sqlParameter[3] = new SqlParameter("@returnDate", header.ReturnDateTime);
                    sqlParameter[4] = new SqlParameter("@firstName", passenger.FirstName);
                    sqlParameter[5] = new SqlParameter("@lastName", passenger.LastName);
                    sqlParameter[6] = new SqlParameter("@documentNo", passenger.DocumentNo);

                    DataSet ds = DBGateway.ExecuteQuery("Usp_CheckPaxDetailsExist", sqlParameter);
                    if (ds != null && ds.Tables[0].Rows.Count > 0 && Convert.ToInt16(ds.Tables[0].Rows[0]["paxCount"].ToString()) > 0)
                    {
                        paxExist = true;
                        header.Error = "Duplicate transaction found, please contact with support team";
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return paxExist;
        }
    }

    public enum InsuranceBookingStatus
    {
        Confirmed = 1,
        Cancelled = 2,
        Failed = 0,
        Pending = 3,
        Error = 4,
        PartialCancelled = 6,
        CancelRequest=7
    }
}
