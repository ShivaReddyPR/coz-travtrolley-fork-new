﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApiServer.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using CT.Core;

namespace WebApiServer.Controllers
{
    //[EnableCors("*", "*", "*")]
    [Authorize]
    public class HotelCancellationController : ApiController
    {    
        [HttpPost]
        public Dictionary<string, string> GetCancelInfo(JObject Data)
        {
           // Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            string JSONString = string.Empty;
            HotelCancellationModel CancelInfoModel = new HotelCancellationModel();
            try
            {
                string source = Convert.ToString(Data["source"]);
                string bookingRefNo = Convert.ToString(Data["bookingRefNo"]);
                int iAgentId = Convert.ToInt32(Data["agentId"]);
                int iUserId = Convert.ToInt32(Data["userId"]);
                CancelInfoModel.HotelCancelInfo(source, bookingRefNo, iAgentId, iUserId);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Error in GetCancelInfo():" + ex.Message, "");
            }
            return CancelInfoModel.cancelInfo;
           
        }
    }
}
