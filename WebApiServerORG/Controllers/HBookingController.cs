﻿using CT.BookingEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApiServer.Models;
using System;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Net.Http;
using System.Net;

namespace WebApiServer.Controllers
{
    //[EnableCors("*", "*", "*")]
    public class HBookingController : ApiController
    {
        [Authorize]
        [HttpPost]
        public string BookHotel(JObject Data)
        {
            string JSONString = string.Empty;
            try
            {
                HotelItinerary Prod = Data["Product"].ToObject<HotelItinerary>();
                int AgentId = (Int32)Data["AgentId"];
                long UserId = (Int64)Data["UserId"];
                int iOnBehalfLoc = (Int32)Data["behalfLocation"];
                string SessionId = Data["SessionId"].ToString();
                HotelBooking HB = new HotelBooking();
                var BookingResult = new HotelBooking { bookingResponse = HB.HotelBook(Prod, AgentId, BookingStatus.Ready, UserId, SessionId, iOnBehalfLoc) };
                BookingResult.bookingResponse.ConfirmationNo = Prod.ConfirmationNo;
                //Prod.HotelPolicyDetails = string.Empty;
                //BookingResult.itinerary = Prod;
                JSONString = JsonConvert.SerializeObject(BookingResult);                
            }
            catch (Exception ex)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound) { ReasonPhrase = ex.Message };
                throw new HttpResponseException(resp);
            }
            return JSONString;
        }

        [HttpPost]
        public string GetPaymentPreference(JObject Data)
        {
            string JSONString = string.Empty;
            string paymentPreferences = string.Empty;
            try
            {
                HotelBooking HB = new HotelBooking();
                paymentPreferences = HB.GetPaymentPreference(Data["SessionId"].ToString(), Data["PaymentPrefParams"].ToString());
                JSONString = JsonConvert.SerializeObject(paymentPreferences);
            }
            catch (Exception ex)
            {
                //Audit

            }
            return JSONString;
        }

        [HttpPost]
        public string GetHotelItinerary(JObject Data)
        {
            string JSONString = string.Empty;
            try
            {
                HotelBooking clsHotelBook = new HotelBooking();
                var hotelRequest = Data["request"].ToObject<HotelRequest>();
                var UserId = Convert.ToInt32(Data["userId"]);
                var AgencyId = Convert.ToInt32(Data["agentId"]);                
                var hotelResult = Data["result"].ToObject<HotelSearchResult>();
                var SessionId = Convert.ToString(Data["sessionId"]);
                var RoomDetails = Data["roomdetails"].ToObject<HotelRoomsDetails[]>();
                var OnBehalfLoc = (Int32)Data["behalfLocation"];
                var clsItinerary = clsHotelBook.GetHotelItinerary(hotelRequest, hotelResult, ref RoomDetails, AgencyId, UserId, SessionId, OnBehalfLoc);

                Product prod = clsItinerary;
                prod.ProductId = clsItinerary.ProductId;
                prod.ProductType = clsItinerary.ProductType;
                prod.ProductTypeId = clsItinerary.ProductTypeId;

                object objInput = new { Product = prod, RoomTypes = RoomDetails };                
                JSONString = JsonConvert.SerializeObject(objInput);
            }
            catch (Exception ex)
            {
                
            }
            return JSONString;
        }

        [HttpPost]
        public decimal AddB2CHotelPendingQueue(JObject Data)
        {
            decimal dcPQHotelId = 0;
            try
            {
                HotelBooking clsHotelBook = new HotelBooking();
                HotelItinerary clsItinerary = Data["Product"].ToObject<HotelItinerary>();                
                string SessionId = Data["SessionId"].ToString();
                dcPQHotelId = clsHotelBook.SaveB2CHotelPendingQueue(clsItinerary, clsItinerary.AgencyId, clsItinerary.CreatedBy, SessionId);
            }
            catch (Exception ex)
            {

            }
            return dcPQHotelId;
        }

        [HttpPost]
        public string GetHotelBookingByConfNo(JObject request)
        {
            HotelBooking clsHotelBook = new HotelBooking();
            string sConfNo = request["confirmationNo"].ToObject<string>();
            return JsonConvert.SerializeObject(clsHotelBook.GetHotelBookingByConfNo(sConfNo));
        }
    }
}
