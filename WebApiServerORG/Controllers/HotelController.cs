﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApiServer.Models;
namespace WebApiServer.Controllers
{
    //[EnableCors("*", "*", "*")]
    public class HotelController : ApiController
    {
        /// <summary>
        /// To get B2C user preferences for hotel product
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public APIUserPreference GetUserPreferences(JObject request)
        {
            AuthenticationData clsCredential = request["Credentials"].ToObject<AuthenticationData>();
            return APIGenericStatic.GetPreferences(clsCredential);            
        }
        [Authorize]
        [HttpPost]
        public string GetSearchResult(JObject Data)
        {
            string JSONString = string.Empty;
            try
            {
               // Audit.Add(EventType.Exception, Severity.High, (int)Data["UserId"], "ziyad inside", "");

                HotelRequest request = Data["request"].ToObject<HotelRequest>();
                int AgentId = (int)Data["AgentId"];
                
                int UserId = (int)Data["UserId"];
                int BehalfAgentLocation = (int)Data["BehalfLocation"];
                
                HotelSearch hotelSearch = new HotelSearch();
                hotelSearch.SearchHotels(request, AgentId, UserId, BehalfAgentLocation);
                var objectDataToPost = new HotelSearch { Result = hotelSearch.Result, SessionId = hotelSearch.SessionId, decimalValue = hotelSearch.decimalValue };
                JSONString = JsonConvert.SerializeObject(objectDataToPost);
            }
            catch (Exception ex )
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Data["UserId"], "exception from GetSearchResult is:" + ex.Message, "");
                throw ex;
            }
            return JSONString;
        }
        [HttpPost]
        public string GetHotelDetails(JObject Data)
        {
            string JSONString = string.Empty;
            try
            {
                HotelSearchResult Results = Data["Results"].ToObject<HotelSearchResult>();
                HotelRequest request = Data["request"].ToObject<HotelRequest>();
                string SessionId = (string)Data["SessionId"];
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "exception from GetHotelDetails is:" + ex.Message, "");
                throw ex;
            }            
            return JSONString;
        }
    }
}
