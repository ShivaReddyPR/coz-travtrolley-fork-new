﻿using System;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace WebApiServer.Infrastructure
{
    //[Authorize]
    //public class LogTracer : ApiController, ITraceWriter
    //{
    //    public void Trace(HttpRequestMessage request, string category, TraceLevel level,
    //        Action<TraceRecord> traceAction)
    //    {
    //        TraceRecord rec = new TraceRecord(request, category, level);
    //        if (rec.Category == "System.Web.Http.Request")
    //        {
    //            //traceAction(rec);
    //            //WriteTrace(rec);
    //        }            
    //    }

    //    protected void WriteTrace(TraceRecord rec)
    //    {
    //        var identity = User.Identity as ClaimsIdentity;
    //        var message = string.Format("{0};{1};{2}",
    //            rec.Operator, rec.Operation, rec.Message);
    //        System.Diagnostics.Trace.WriteLine(message, rec.Category);
    //        string sData = string.IsNullOrEmpty(rec.Category) ? "No Category" : rec.Category;
    //        string ss = Directory.GetCurrentDirectory();
    //        File.AppendAllText("C:\\Developments\\" + "APILogs.txt", "Date Time: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + " .. event: " + sData + "-------" + message + Environment.NewLine);
    //    }
    //}    

    /// <summary>
    /// To log all api request and response messages
    /// </summary>
    public class LogTracer : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            string _TraceId = "Notraceid";

            //var clsIdentity = (ClaimsIdentity)HttpContext.Current.User.Identity;

            //if (clsIdentity != null && clsIdentity.Claims != null && clsIdentity.Claims.Any(x => x.Type.EndsWith("nameidentifier")))
            //    _TraceId = clsIdentity.Claims.Where(x => x.Type.EndsWith("nameidentifier")).Select(x => x.Value).FirstOrDefault();

            if (request != null && request.Headers != null && request.Headers.Authorization != null)
                _TraceId = request.Headers.Authorization.ToString().Replace("Bearer ", string.Empty);

            if (request.RequestUri != null && request.RequestUri.AbsolutePath.Contains("api"))
            {
                var reqUri = request.RequestUri.AbsolutePath.Split('/');
                _TraceId = reqUri[reqUri.Length - 1] + "_" + _TraceId;
            }

            if (request.Content != null)
            {
               
                JavaScriptSerializer js = new JavaScriptSerializer();
                js.MaxJsonLength = Int32.MaxValue;
                // log request body
                var requestBody = await request.Content.ReadAsStringAsync();
                if (!string.IsNullOrEmpty(requestBody.ToString()))
                    ApiGeneral.GenerateLogs(true, false, js.Serialize(requestBody), "ApiRequest_" + _TraceId);
            }

            // let other handlers process the request
            var result = await base.SendAsync(request, cancellationToken);

            if (result.Content != null)
            {
                // once response body is ready, log it
                var responseBody = await result.Content.ReadAsStringAsync();

                if (responseBody.ToString().Contains("Authorization has been denied for this request."))
                    responseBody = responseBody.ToString().Replace("Authorization has been denied for this request.", "Access Token expired." + Environment.NewLine + "Token:" + request.Headers.Authorization.ToString());

                if (!string.IsNullOrEmpty(responseBody.ToString()))
                    ApiGeneral.GenerateLogs(true, false, responseBody, "ApiResponse_" + _TraceId);
            }

            return result;
        }
    }
}