﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;

namespace WebApiServer.Models
{
    public static class APIGenericStatic
    {
        public static LoginInfo GetLoginInfo(int iUserId, int iOnBehalfLoc, int iAgentId)
        {
            LoginInfo clsLInfo = null;
            try
            {
                clsLInfo = LoginInfo.GetLoginInfo(iUserId);
                clsLInfo.IsOnBehalfOfAgent = iOnBehalfLoc > 0 || clsLInfo.AgentId != iAgentId;

                AgentMaster clsAM = new AgentMaster(iAgentId);

                if (clsLInfo.IsOnBehalfOfAgent)
                {
                    clsLInfo.IsOnBehalfOfAgent = true;
                    clsLInfo.OnBehalfAgentID = iAgentId;
                    clsLInfo.OnBehalfAgentLocation = iOnBehalfLoc;
                    clsLInfo.OnBehalfAgentCurrency = clsAM.AgentCurrency;
                    clsLInfo.OnBehalfAgentDecimalValue = clsAM.DecimalValue;
                    StaticData sd = new StaticData();
                    sd.BaseCurrency = clsAM.AgentCurrency;
                    clsLInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                    clsLInfo.OnBehalfAgentSourceCredentials = AgentMaster.GetAirlineCredentials(iAgentId);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to get Login info from Hotel web API:" + ex.Message, "");
            }
            return clsLInfo;
        }

        public static string GetLoginCountry(long iLocId)
        {
            string sLoginCntryCode = string.Empty;
            try
            {
                LocationMaster clsLM = new LocationMaster(iLocId);
                sLoginCntryCode = clsLM.CountryCode;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Failed to get Login country code in Hotel web API:" + ex.Message, "");
            }
            return sLoginCntryCode;
        }

        /// <summary>
        /// To get B2C user preferences
        /// </summary>
        /// <param name="clsCredential"></param>
        /// <returns></returns>
        public static APIUserPreference GetPreferences(AuthenticationData clsCredential)
        {
            LoginInfo clsLoginInfo = new LoginInfo();
            UserPreference clsUserPref = new UserPreference();
            APIUserPreference clsAPIPref = new APIUserPreference();

            try
            {
                //int iUserId = clsUserPref.GetMemberIdBySiteName(clsCredential.SiteName);
                int iUserId = clsUserPref.GetMemberIdBySiteNameForHotel(clsCredential.SiteName);
                LoginInfo AgentLoginInfo = UserMaster.GetB2CUser(iUserId);
                clsAPIPref.Load(iUserId, ItemType.Hotel);
                clsAPIPref.AgentCurrency = AgentLoginInfo.Currency;
                clsAPIPref.AgentDecimalPoint = AgentLoginInfo.DecimalValue;
                clsAPIPref.AgentEmail = AgentLoginInfo.AgentEmail;
                clsAPIPref.AgentID = AgentLoginInfo.AgentId;
                AgentMaster agent = new AgentMaster(AgentLoginInfo.AgentId);
                clsAPIPref.AgentAddress = agent.Address;
                clsAPIPref.AgentPhoneNo = agent.Phone1;
                clsAPIPref.MemberId = Convert.ToInt32(AgentLoginInfo.UserID);
                clsAPIPref.LocationId = Convert.ToInt32(AgentLoginInfo.LocationID);
                clsAPIPref.LocationCountryCode = AgentLoginInfo.LocationCountryCode;

            }
            catch (Exception ex)
            {
                Audit.AddAuditBookingAPI(EventType.Login, Severity.High, 0, "B2C-Site Name is not valid(" + clsCredential.SiteName + "): " + ex.ToString(), "");
                throw new ArgumentException("Site Name is not valid");
            }
            return clsAPIPref;
        }

    }

    public class AuthenticationData
    {
        public string SiteName { get; set; }
        public string AccountCode { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}