﻿
using CT.Core;
using CT.MetaSearchEngine;
using System;
using System.Collections.Generic;


namespace WebApiServer.Models
{
    public class HotelCancellationModel
    {
        public Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
        public void HotelCancelInfo(string source, string bookingRefNo, int iAgentId, int iUserId)
        {
            try
            {
                MetaSearchEngine mse = new MetaSearchEngine();
                mse.SettingsLoginInfo = APIGenericStatic.GetLoginInfo(iUserId, 0, iAgentId);
                cancelInfo = mse.GetBookingCancelInfo(source, bookingRefNo);
            }

            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Error in HotelCancelInfo():" + ex.Message, "");
                throw ex;
            }
        }
    }
}
