﻿using CT.BookingEngine;
using CT.MetaSearchEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiServer.Models
{
    public class HotelBooking
    {
        public HotelItinerary itinerary = new HotelItinerary();
        public BookingResponse bookingResponse = new BookingResponse();
        public BookingResponse HotelBook(Product prod,int agencyId,BookingStatus Status,long UserId,string SessionId, int iOnBehalfLoc)
        {
            BookingResponse bookRes = new BookingResponse();
            MetaSearchEngine mse = new MetaSearchEngine(SessionId);
            mse.SettingsLoginInfo = APIGenericStatic.GetLoginInfo(Convert.ToInt32(UserId), iOnBehalfLoc, agencyId);
                        
            prod.ProductType = ProductType.Hotel;
            bookRes = mse.Book(ref prod, agencyId, Status, UserId);
            return bookRes;
        }

        public string GetPaymentPreference(string SessionId, string PaymentPrefParams)
        {
            string paymentPreferences = string.Empty;
            try
            {
                MetaSearchEngine mse = new MetaSearchEngine(SessionId);
                paymentPreferences = mse.GetPaymentPreference(PaymentPrefParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return paymentPreferences;

        }

        /// <summary>
        /// To create hotel itinerary 
        /// </summary>
        /// <param name="clsHotelReq"></param>
        /// <param name="clsHotelResult"></param>
        /// <param name="RoomDetails"></param>
        /// <param name="iAgencyId"></param>
        /// <param name="Status"></param>
        /// <param name="UserId"></param>
        /// <param name="SessionId"></param>
        /// <param name="iOnBehalfLoc"></param>
        /// <returns></returns>
        public HotelItinerary GetHotelItinerary(HotelRequest clsHotelReq, HotelSearchResult clsHotelResult, ref HotelRoomsDetails[] RoomDetails,
            int iAgencyId, int UserId, string SessionId, int iOnBehalfLoc)
        {            
            MetaSearchEngine mse = new MetaSearchEngine(SessionId);
            mse.SettingsLoginInfo = APIGenericStatic.GetLoginInfo(Convert.ToInt32(UserId), iOnBehalfLoc, iAgencyId);
            itinerary = mse.CreateHotelItinerary(clsHotelResult, clsHotelReq, ref RoomDetails);
            return itinerary;
        }

        public decimal SaveB2CHotelPendingQueue(Product prod, int agencyId, long UserId, string SessionId)
        {            
            MetaSearchEngine mse = new MetaSearchEngine(SessionId);
            mse.SettingsLoginInfo = APIGenericStatic.GetLoginInfo(Convert.ToInt32(UserId), 0, agencyId);
            return mse.SaveB2CHotelPendingQueue(prod, agencyId, UserId, SessionId);
        }

        public HotelItinerary GetHotelBookingByConfNo(string sConfNo)
        {
            return MetaSearchEngine.GetHotelItineraryByConfNo(sConfNo);
        }
    }
}
