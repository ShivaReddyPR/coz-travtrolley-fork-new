﻿using Encryption.AES;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using System.Xml;
using CT.Core;

namespace SafexPay
{
    public class SafexPayService
    {
        #region private Members

        string _xmlPath;//Logs Path.
        Dictionary<string, string> _safexPayConfig;
        string _configPath;
        string _safexPaymentUrl;
        string _txnMerchantKey;

        #region  Txn_Dtails     
        string _ag_id;
        string _me_id;
        string _order_no;
        string _Amount;
        string _Country;
        string _Currency;
        string _txn_type;
        string _success_url;
        string _failure_url;
        string _Channel;
        #endregion
        #region pg_details

        string _pg_id;
        string _Paymode;
        string _Scheme;
        string _emi_months;
        #endregion
        #region card_details
        string _card_no;
        string _exp_month;
        string _exp_year;
        string _cvv2;
        string _card_name;
        #endregion
        #region cust_details
        string _cust_name;
        string _email_id;
        string _mobile_no;
        string _unique_id;
        string _is_logged_in;
        #endregion
        #region Bill_details
        string _bill_address;
        string _bill_city;
        string _bill_state;
        string _bill_country;
        string _bill_zip;
        #endregion
        #region Ship_details
        string _ship_address;
        string _ship_city;
        string _ship_state;
        string _ship_country;
        string _ship_zip;
        string _ship_days;
        string _address_count;
        #endregion
        #region Item_details
        string _item_count;
        string _item_value;
        string _item_category;
        #endregion
        #region Other_details
        string _udf1;
        string _udf2;
        string _udf3;
        string _udf4;
        string _udf5;
        #endregion

        #endregion

        #region public Properties

        ////Marchent Key        
        public string txnMerchantKey
        {
            get { return _txnMerchantKey; }
            set { _txnMerchantKey = value; }
        }

        public string safexPaymentUrl
        {
            get { return _safexPaymentUrl; }
            set { _safexPaymentUrl = value; }
        }

        #region  Txn_Dtails       
        //// Required Max 20 characters. 
        ////Aggregator id is “Paygate” .    
        public string ag_id
        {
            get { return _ag_id; }
            set { _ag_id = value; }
        }

        ////Required Max 20 characters.
        ////Merchant ID issued to you by AvantGarde.
        public string me_id
        {
            get { return _me_id; }
            set { _me_id = value; }
        }

        ////Required Max 70 characters.
        ////This is your Unique Transaction ID for which you want to initiate payment.
        public string order_no
        {
            get { return _order_no; }
            set { _order_no = value; }
        }

        ////Required Max 10 digits
        ////This is your Transaction Amount.You can use
        ////up to 2 decimal places.E.g. “150.00”
        public string Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }

        //// Required
        ////Exact 3 characters
        ////Country code as “IND” for India.
        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        ////Required
        ////Exact 3 characters
        ////Currency code as “INR” for India.
        public string Currency
        {
            get { return _Currency; }
            set { _Currency = value; }
        }

        ////Required
        ////Max 10 characters
        ////Use Txn_Type as “SALE”
        public string txn_type
        {
            get { return _txn_type; }
            set { _txn_type = value; }
        }

        ////Required
        ////Max 100 characters
        ////This will be the return URL where response is
        ////accepted in successful transaction.
        public string success_url
        {
            get { return _success_url; }
            set { _success_url = value; }
        }

        ////Required
        ////Max 100 characters
        ////This will be the return URL where response is
        ////accepted in failed transaction.
        public string failure_url
        {
            get { return _failure_url; }
            set { _failure_url = value; }
        }

        ////Required
        ////Max 10 characters
        ////Use “WEB” in case of website based payment
        ////or “MOBILE” in case of mobile app.
        public string Channel
        {
            get { return _Channel; }
            set { _Channel = value; }
        }

        #endregion
        #region  pg_details

        ////Conditional
        ////Max 3 digits
        ////List will be provided to you.This is only
        ////required if your integration type is “Merchant
        ////Hosted”. E.g. “1” for HDFC Bank.
        public string pg_id
        {
            get { return _pg_id; }
            set { _pg_id = value; }
        }

        ////Conditional
        ////Exact 2 characters
        ////This is only required if your integration type is
        ////“Merchant Hosted”.
        ////NB = Net Banking
        ////CC = Credit Card
        ////DC = Debit Card
        ////PP = Prepaid Card
        ////WA = Wallet
        ////CE = Credit Card EMI
        public string Paymode
        {
            get { return _Paymode; }
            set { _Paymode = value; }
        }

        ////Optional
        ////Max 3 digits
        ////List will be provided to you.This is only
        ////required if your integration type is “Merchant
        ////Hosted”. E.g. “1” for VISA.
        public string Scheme
        {
            get { return _Scheme; }
            set { _Scheme = value; }
        }

        ////Conditional
        ////Max 3 digits
        ////This is only required if your integration type is
        ////“Merchant Hosted” and paymode is Credit
        ////Card EMI.
        ////Values can be 3, 6, 9 or 12.
        public string emi_months
        {
            get { return _emi_months; }
            set { _emi_months = value; }
        }

        #endregion     
        #region  card_details

        ////Conditional
        ////Max 19 digits
        ////Card Number.This is only required if your
        ////paymode is “CC”, “DC”, “PP” or “CE”.
        public string card_no
        {
            get { return _card_no; }
            set { _card_no = value; }
        }

        ////Conditional
        ////Exact 2 characters
        ////Expiry Month on the card in MM format.
        public string exp_month
        {
            get { return _exp_month; }
            set { _exp_month = value; }
        }


        ////Conditional
        ////Exact 2 characters
        ////Expiry Year on the card in YY format.
        public string exp_year
        {
            get { return _exp_year; }
            set { _exp_year = value; }
        }

        ////Conditional
        ////Exact 3 digits
        ////CVV2 on the back of the card.
        public string cvv2
        {
            get { return _cvv2; }
            set { _cvv2 = value; }
        }

        ////Optional
        ////Max 50 characters
        ////Name on the card.
        public string card_name
        {
            get { return _card_name; }
            set { _card_name = value; }
        }

        #endregion
        #region  cust_details

        ////Conditional
        ////Max 50 characters
        ////Customer Name.This is required in case you
        ////required fraud protection.
        public string cust_name
        {
            get { return _cust_name; }
            set { _cust_name = value; }
        }

        ////Conditional
        ////Max 100 characters
        ////Customer’s Email ID.This is required in case
        ////you required fraud protection.
        public string email_id
        {
            get { return _email_id; }
            set { _email_id = value; }
        }

        ////Conditional
        ////Max 15 characters
        ////Customer’s Mobile Number.This is required
        ////in case you required fraud protection.
        public string mobile_no
        {
            get { return _mobile_no; }
            set { _mobile_no = value; }
        }

        ////Conditional
        ////Max 100 characters
        ////Customer’s Unique ID.This is required in case
        ////of stored card feature.This can be user id of
        ////customer in your system or email id which
        ////uniquely identifies the customer.
        public string unique_id
        {
            get { return _unique_id; }
            set { _unique_id = value; }
        }

        ////Optional
        ////Max 1 character
        ////Pass “Y” if user is logged in. Pass “N” for a
        ////guest user.
        public string is_logged_in
        {
            get { return _is_logged_in; }
            set { _is_logged_in = value; }
        }
        #endregion
        #region   Bill_details

        ////        Conditional
        ////Max 200 characters
        ////             Customer’s Billing Address.This is required in
        ////case you required fraud protection.
        public string bill_address
        {
            get { return _bill_address; }
            set { _bill_address = value; }
        }

        ////Conditional
        ////Max 50 characters
        ////Customer’s Billing City.This is required in case
        ////you required fraud protection.
        public string bill_city
        {
            get { return _bill_city; }
            set { _bill_city = value; }
        }

        ////Conditional
        ////Max 50 characters
        ////Customer’s Billing State.This is required in
        ////case you required fraud protection.
        public string bill_state
        {
            get { return _bill_state; }
            set { _bill_state = value; }
        }

        ////Conditional
        ////Max 50 characters
        ////Customer’s Billing Country.This is required in
        ////case you required fraud protection.
        public string bill_country
        {
            get { return _bill_country; }
            set { _bill_country = value; }
        }

        //////Conditional
        //////Max 20 characters
        //////Customer’s Billing Zip/Pin Code. This is
        //////required in case you required fraud
        //////protection.
        public string bill_zip
        {
            get { return _bill_zip; }
            set { _bill_zip = value; }
        }
        #endregion
        #region Ship_details

        ////Conditional
        ////Max 200 characters
        ////Customer’s Shipping Address.This is required
        ////in case of physical delivery of goods.
        public string ship_address
        {
            get { return _ship_address; }
            set { _ship_address = value; }
        }

        ////Conditional
        ////Max 50 characters
        ////Customer’s Shipping City.This is required in
        ////case of physical delivery of goods.
        public string ship_city
        {
            get { return _ship_city; }
            set { _ship_city = value; }
        }

        ////Conditional
        ////Max 50 characters
        ////Customer’s Shipping State.This is required in
        ////case of physical delivery of goods.
        public string ship_state
        {
            get { return _ship_state; }
            set { _ship_state = value; }
        }

        ////Conditional
        ////Max 50 characters
        ////Customer’s Shipping Country.This is required
        ////in case of physical delivery of goods.
        public string ship_country
        {
            get { return _ship_country; }
            set { _ship_country = value; }
        }

        ////Conditional
        ////Max 20 characters
        ////Customer’s Shipping Zip/Pin Code.This is
        ////required in case of physical delivery of goods.
        public string ship_zip
        {
            get { return _ship_zip; }
            set { _ship_zip = value; }
        }

        ////Optional
        ////Max 3 digits
        ////Approximate number of days required for
        ////delivery of goods.This is required in case of
        ////physical delivery of goods.
        public string ship_days
        {
            get { return _ship_days; }
            set { _ship_days = value; }
        }

        ////Optional
        ////Max 3 digits
        ////If customer has more than one addresses
        ////stored in your system.
        public string address_count
        {
            get { return _address_count; }
            set { _address_count = value; }
        }

        #endregion
        #region Item_details

        ////Optional
        ////Max 3 digits
        ////Count of items purchased by customer.
        public string item_count
        {
            get { return _item_count; }
            set { _item_count = value; }
        }

        ////Optional
        ////Max 200 characters
        ////Comma separated item wise cost.E.g.“100.00,200.00”
        public string item_value
        {
            get { return _item_value; }
            set { _item_value = value; }
        }

        ////Optional
        ////Max 200 characters
        ////Comma separated item category.E.g. “E-Book,DVD”
        public string item_category
        {
            get { return _item_category; }
            set { _item_category = value; }
        }
        #endregion
        #region Other_details

        ////Optional
        ////Max 100 characters
        ////This is a user defined field that you can use to
        ////send any additional information about the
        ////transaction.
        public string udf1
        {
            get { return _udf1; }
            set { _udf1 = value; }
        }

        ////Optional
        ////Max 100 characters
        ////This is a user defined field that you can use to
        ////send any additional information about the
        ////transaction.
        public string udf2
        {
            get { return _udf2; }
            set { _udf2 = value; }
        }

        ////Optional
        ////Max 100 characters
        ////This is a user defined field that you can use to
        ////send any additional information about the
        ////transaction.
        public string udf3
        {
            get { return _udf3; }
            set { _udf3 = value; }
        }

        ////Optional
        ////Max 100 characters
        ////This is a user defined field that you can use to
        ////send any additional information about the
        ////transaction.
        public string udf4
        {
            get { return _udf4; }
            set { _udf4 = value; }
        }

        ////Optional
        ////Max 100 characters
        ////This is a user defined field that you can use to
        ////send any additional information about the
        ////transaction.
        public string udf5
        {
            get { return _udf5; }
            set { _udf5 = value; }
        }
        #endregion


        #region Txn_Response 
        public string txn_details;
        public string pg_details;
        public string card_details;
        public string cust_details;
        public string bill_details;
        public string ship_details;
        public string item_details;
        public string other_details;
        public string txn_date;
        public string txn_time;
        public string ag_ref;
        public string pg_ref;
        public string status;
        public string res_code;
        public string res_message;
        #endregion

        #endregion

        #region constructor

        public SafexPayService()
        {
            try
            {
                // configFiles --C:\AppConfig\VMSB2BConfig\
                if (ConfigurationManager.AppSettings["configFiles"] != null)
                    _configPath = ConfigurationManager.AppSettings["configFiles"];

                _safexPayConfig = new Dictionary<string, string>();
                XmlDocument xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.Load(Path.Combine(_configPath, "SafexPay.config.xml"));
                }
                catch (Exception ex)
                {

                    throw ex;
                }

                XmlNode config = null;
                XmlNodeList pagingData = null;
                if (xmlDoc.SelectSingleNode("config") != null)
                {
                    config = xmlDoc.SelectSingleNode("config");
                    pagingData = config.ChildNodes;

                    for (int i = 0; i < pagingData.Count; i++)
                    {
                        _safexPayConfig.Add(pagingData[i].Name, pagingData[i].InnerXml);
                    }
                    _xmlPath = Path.Combine(_safexPayConfig["xmlPath"], DateTime.Now.ToString("ddMMMyyyy"));

                    if (!Directory.Exists(_xmlPath))
                    {
                        Directory.CreateDirectory(_xmlPath);
                    }



                    safexPaymentUrl = _safexPayConfig["safexPaymentUrl"] != null ? _safexPayConfig["safexPaymentUrl"].ToString() : string.Empty;// Payment Gateway URl
                    txnMerchantKey = _safexPayConfig["txnMerchantKey"] != null ? _safexPayConfig["txnMerchantKey"].ToString() : string.Empty; // Transaction Merchant Key
                    ag_id = _safexPayConfig["txnAgentId"] != null ? _safexPayConfig["txnAgentId"].ToString() : string.Empty; //Transaction Agent ID
                    me_id = _safexPayConfig["txnMerchantId"] != null ? _safexPayConfig["txnMerchantId"].ToString() : string.Empty; // Transaction Merchant ID
                    Country = _safexPayConfig["txnCountry"] != null ? _safexPayConfig["txnCountry"].ToString() : string.Empty; //Transaction Country Code 3 Chars
                    Currency = _safexPayConfig["txnCountryCurrency"] != null ? _safexPayConfig["txnCountryCurrency"].ToString() : string.Empty;//Transaction Country Currency   Code 3 Chars
                    txn_type = _safexPayConfig["txnType"] != null ? _safexPayConfig["txnType"].ToString() : string.Empty;//Transaction Type "SALE"
                    Channel = _safexPayConfig["txnChannel"] != null ? _safexPayConfig["txnChannel"].ToString() : string.Empty;// Use in case web based app "WEB" / in case Mobile app "MOBILE"

                }

            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(EventType.Exception, Severity.High, 1, "(SafexPay)Failed initialize config. Reason: " + ex.ToString(), "");
            }
        }


        #endregion

        #region Safexpay Logs

        // User Transaction request before Encryption 
        public void WriteRequest()
        {
            try
            {
                string reqData = "Transaction Details: " + txn_details + "& PG Details: " + pg_details + "& Card Details: " + card_details + "& Customer Details: " + cust_details + "& Billing Details: " + bill_details + "& Shipping Details: " + ship_details + "& Item Details: " + item_details + "& Other Details: " + other_details;
                // For writing of request data as json file    
                string fileName = Path.Combine(_xmlPath, "Request_" + order_no + "_" + DateTime.Now.ToString("dd-MM-yyyy hh-mm-ss") + ".json");//, 
                StreamWriter writer = new StreamWriter(fileName);
                writer.WriteLine(reqData);
                writer.Close();

            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(EventType.Exception, Severity.High, 1, "(SafexPay)Failed to write Payment request. Reason: " + ex.ToString(), "");
            }

        }


        // User Transaction request after Encryption 
        public void WriteEncryptRequest(CryptoClass aes)
        {
            try
            {

                string reqData = aes.enc_txn_details + "&" + aes.enc_pg_details + "&" + aes.enc_card_details + "&" + aes.enc_cust_details + "&" + aes.enc_bill_details + "&" + aes.enc_ship_details + "&" + aes.enc_item_details + "&" + aes.enc_other_details;
                // For writing of request data as json file    
                string fileName = Path.Combine(_xmlPath, "EncrytRequest_" + aes.order_no + "_" + DateTime.Now.ToString("dd-MM-yyyy hh-mm-ss") + ".json");//, 
                StreamWriter writer = new StreamWriter(fileName);
                writer.WriteLine(reqData);
                writer.Close();

            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(EventType.Exception, Severity.High, 1, "(SafexPay)Failed to write Encryption Response. Reason: " + ex.ToString(), "");
            }

        }

        // Transaction response from Payment Gateway in  Decrypt format
        public void WriteResonse(string response)
        {
            try
            {
                // For writing of request data as json file    
                string fileName = Path.Combine(_xmlPath, "Response_" + order_no + "_" + DateTime.Now.ToString("dd-MM-yyyy hh-mm-ss") + ".json");//, 
                StreamWriter writer = new StreamWriter(fileName);
                writer.WriteLine(response);
                writer.Close();

            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(EventType.Exception, Severity.High, 1, "(SafexPay)Failed to write Payment Response. Reason: " + ex.ToString(), "");
            }

        }

        #endregion
    }
}
