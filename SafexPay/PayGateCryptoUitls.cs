﻿using System;
using java.io;
using java.security;
using java.security.spec;
using javax.crypto;
using javax.crypto.spec;


namespace SafexPay
{


    using Base64 = org.apache.commons.codec.binary.Base64;



    /// <summary>
    /// This program provides the following cryptographic functionalities
    /// Encryption and Decryption using AES
    /// 
    /// @author Naresh Podichetty
    /// 
    /// </summary>
    public class PayGateCryptoUtils
    {

        private const string ENCRYPTION_IV = "0123456789abcdef";

        private const string PADDING = "AES/CBC/PKCS5Padding";

        private const string ALGORITHM = "AES";

        private const string CHARTSET = "UTF-8";

        /// <summary>
        /// Encryption
        /// </summary>
        /// <param name="textToEncrypt"> </param>
        /// <param name="key">
        /// @return </param>
        public static string encrypt(string textToEncrypt, string key)
        {
            try
            {
                Cipher cipher = Cipher.getInstance(PADDING);
                cipher.init(Cipher.ENCRYPT_MODE, makeKey(key), makeIv());
                return new string(System.Text.ASCIIEncoding.ASCII.GetString(Base64.encodeBase64(cipher.doFinal(System.Text.ASCIIEncoding.ASCII.GetBytes(textToEncrypt)))).ToCharArray());
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e);
            }
        }

        /// <summary>
        /// Decryption
        /// </summary>
        /// <param name="src"> </param>
        /// <param name="key">
        /// @return </param>
        public static string decrypt(string textToDecrypt, string key)
        {
            try
            {
                Cipher cipher = Cipher.getInstance(PADDING);
                cipher.init(Cipher.DECRYPT_MODE, makeKey(key), makeIv());
                return new string(System.Text.ASCIIEncoding.ASCII.GetString(cipher.doFinal(Base64.decodeBase64(textToDecrypt))).ToCharArray());
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e);
            }
        }

        private static AlgorithmParameterSpec makeIv()
        {
            try
            {
                sbyte[] enc = ENCRYPTION_IV.GetBytes(CHARTSET);
                byte[] bytes = new byte[enc.Length];
                Buffer.BlockCopy(enc, 0, bytes, 0, enc.Length);
                return new IvParameterSpec(bytes);
            }
            catch (UnsupportedEncodingException e)
            {
                throw e;
            }
        }

        private static Key makeKey(string encryptionKey)
        {
            try
            {
                byte[] key = Convert.FromBase64String(encryptionKey);
                return new SecretKeySpec(key, ALGORITHM);
            }
            catch (Exception e)
            {
                System.Console.Write(e);
            }

            return null;
        }

        static PayGateCryptoUtils()
        {
            try
            {
                //Field field = Type.GetType("javax.crypto.JceSecurity").getDeclaredField("isRestricted");
                //field.setAccessible(true);
                //field.set(null, false);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.ToString());
                System.Console.Write(ex.StackTrace);
            }
        }

        public static string generateurl(string requestparameter)
        {
            //requestparameter = AES128Bit.encrypt(requestparameter, "v8sejAgSe+PK9nymyTGL6Q==");
            //requestparameter = requestparameter.replaceAll("\n", "");
            string Url = "https://test.direcpay.com/direcpay/secure/dpTransactionRefund.jsp?requestparams=" + requestparameter + "&merchantId=201105041000002";
            return Url;
        }

        public static string generateMerchantKey()
        {
            string newKey = null;

            try
            {
                // Get the KeyGenerator
                KeyGenerator kgen = KeyGenerator.getInstance("AES");
                kgen.init(256); // 128, 192 and 256 bits available

                // Generate the secret key specs.
                SecretKey skey = kgen.generateKey();
                byte[] raw = skey.getEncoded();

                newKey = new string(System.Text.ASCIIEncoding.ASCII.GetString(raw).ToCharArray());
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.ToString());
                System.Console.Write(ex.StackTrace);
            }

            return newKey;
        }
        public static void Main(string[] args)
        {
            string masterKey = "xKZT6ihzIaYTn0IguVcTfFmuaHV1iz8c45b+/QisRdejDVIG6U9gnPhBGh6PMS1w";
            string requestParam = "725994044441641828|2001604000446840|201105041000002|3175011441641346|1|http://localhost:8085/agcore/demoBankRefund";
            string response = generateurl(requestParam);

        }
    }
}

//-------------------------------------------------------------------------------------------
//	Copyright © 2007 - 2018 Tangible Software Solutions, Inc.
//	This class can be used by anyone provided that the copyright notice remains intact.
//
//	This class is used to convert some aspects of the Java String class.
//-------------------------------------------------------------------------------------------
internal static class StringHelper
{
    //----------------------------------------------------------------------------------
    //	This method replaces the Java String.substring method when 'start' is a
    //	method call or calculated value to ensure that 'start' is obtained just once.
    //----------------------------------------------------------------------------------
    internal static string SubstringSpecial(this string self, int start, int end)
    {
        return self.Substring(start, end - start);
    }

    //------------------------------------------------------------------------------------
    //	This method is used to replace calls to the 2-arg Java String.startsWith method.
    //------------------------------------------------------------------------------------
    internal static bool StartsWith(this string self, string prefix, int toffset)
    {
        return self.IndexOf(prefix, toffset, System.StringComparison.Ordinal) == toffset;
    }

    //------------------------------------------------------------------------------
    //	This method is used to replace most calls to the Java String.split method.
    //------------------------------------------------------------------------------
    internal static string[] Split(this string self, string regexDelimiter, bool trimTrailingEmptyStrings)
    {
        string[] splitArray = System.Text.RegularExpressions.Regex.Split(self, regexDelimiter);

        if (trimTrailingEmptyStrings)
        {
            if (splitArray.Length > 1)
            {
                for (int i = splitArray.Length; i > 0; i--)
                {
                    if (splitArray[i - 1].Length > 0)
                    {
                        if (i < splitArray.Length)
                            System.Array.Resize(ref splitArray, i);

                        break;
                    }
                }
            }
        }

        return splitArray;
    }

    //-----------------------------------------------------------------------------
    //	These methods are used to replace calls to some Java String constructors.
    //-----------------------------------------------------------------------------
    internal static string NewString(sbyte[] bytes)
    {
        return NewString(bytes, 0, bytes.Length);
    }
    internal static string NewString(sbyte[] bytes, int index, int count)
    {
        return System.Text.Encoding.UTF8.GetString((byte[])(object)bytes, index, count);
    }
    internal static string NewString(sbyte[] bytes, string encoding)
    {
        return NewString(bytes, 0, bytes.Length, encoding);
    }
    internal static string NewString(sbyte[] bytes, int index, int count, string encoding)
    {
        return System.Text.Encoding.GetEncoding(encoding).GetString((byte[])(object)bytes, index, count);
    }

    //--------------------------------------------------------------------------------
    //	These methods are used to replace calls to the Java String.getBytes methods.
    //--------------------------------------------------------------------------------
    internal static sbyte[] GetBytes(this string self)
    {
        return GetSBytesForEncoding(System.Text.Encoding.UTF8, self);
    }
    internal static sbyte[] GetBytes(this string self, System.Text.Encoding encoding)
    {
        return GetSBytesForEncoding(encoding, self);
    }
    internal static sbyte[] GetBytes(this string self, string encoding)
    {
        return GetSBytesForEncoding(System.Text.Encoding.GetEncoding(encoding), self);
    }
    private static sbyte[] GetSBytesForEncoding(System.Text.Encoding encoding, string s)
    {
        sbyte[] sbytes = new sbyte[encoding.GetByteCount(s)];
        encoding.GetBytes(s, 0, s.Length, (byte[])(object)sbytes, 0);
        return sbytes;
    }
}
