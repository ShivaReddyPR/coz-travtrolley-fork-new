﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using CT.Configuration;
using Gimmonix.GimmonixService;
using System.Xml.Serialization;
using Newtonsoft.Json;
using CT.Core;
using System.Threading;

namespace CT.BookingEngine.GDS
{

    /// <summary>
    /// This class is used to interact with GimmonixApi.
    /// HotelBookingSource Value for Gimmonix=22    
    /// </summary>
    public class GimmonixApi
    {
        protected int NoofRooms = 1;
        /// <summary>
        ///  This variable is used store the log file path, which is read from api configuration file
        /// </summary>
        string xmlPath = string.Empty;
        /// <summary>
        /// This variable is used, to send the language(response will be returned in same language Ex:en-us) to api, which is read from api configuration file
        /// </summary>
        string language = string.Empty;
        /// <summary>
        /// This variable is used,to send the currency(response will be returned in same currency Ex:AED) to api, which is read from api configuration file
        /// </summary>
        string currency = string.Empty;
        /// <summary>
        /// This variable is used,to store rateOfExchange value 
        /// </summary>
        decimal rateOfExchange = 1;

        public List<HotelStaticData> hotelStaticData { get; set; }

        /// <summary>
        /// To get marketing fee discount % from mark up table
        /// </summary>
        public decimal mfDiscountPercent { get; set; }

        DynamicDataServiceClient client = new DynamicDataServiceClient();

        #region Variables
        /// <summary>
        /// This variable is used,to get exchange rates based on the login agent, which is read from MetaserchEngine
        /// </summary>
        Dictionary<string, decimal> exchangeRates;
        /// <summary>
        /// This variable is used,to be rounded off HotelFare, which is read from MetaserchEngine
        /// </summary>
        int decimalPoint;
        /// <summary>
        /// This variable is used,to get agent currency based on the login agent, which is read from MetaserchEngine  
        /// </summary>
        string agentCurrency;
        /// <summary>
        /// This variable is used,to get apicountry code based on the api, which is read from MetaserchEngine  
        /// </summary>
        string sourceCountryCode;
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        string sessionId;
        /// <summary>
        /// This variable is used,to store the login userid,which is read from MetaserchEngine  
        /// </summary>
        long gimmonixUserId;
        /// <summary>
        /// This variable is used,to store the login username,which is read from MetaserchEngine  
        /// </summary>
        string userName;
        /// <summary>
        /// This variable is used,to store the login passWord,which is read from MetaserchEngine  
        /// </summary>
        string passWord;
        /// <summary>
        /// This variable is used,to store the Cancellation buffer,which is read from AppConfig table  
        /// </summary>
        double cnclBuffer;
        #endregion
        #region Properties
        /// <summary>
        /// Exchange rates to be used for conversion in Agent currency from suplier currency
        /// </summary>
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        /// <summary>
        /// Decimal value to be rounded off
        /// </summary>

        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        /// <summary>
        /// Base currency used by the Agent
        /// </summary>
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        /// <summary>
        /// Api source countryCode
        /// </summary>
        public string SourceCountryCode
        {
            get { return sourceCountryCode; }
            set { sourceCountryCode = value; }
        }
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }
        /// <summary>
        /// this GrnUserId is store user id
        /// </summary>
        public long GimmonixUserId
        {
            get { return gimmonixUserId; }
            set { gimmonixUserId = value; }
        }

        public string UserName { get => userName; set => userName = value; }
        public string PassWord { get => passWord; set => passWord = value; }
        public double CnclBuffer { get => cnclBuffer; set => cnclBuffer = value; }
        #endregion
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public GimmonixApi()
        {
            LoadCredentials();
            //client.Endpoint.Address = "";

        }
        #endregion
        /// <summary>
        /// To loading initial values, which is read from api config
        /// also Creating Day wise folder
        /// </summary>
        private void LoadCredentials()
        {

            //Create Hotel Xml Log path folder per day wise
            xmlPath = ConfigurationSystem.GimmonixConfig["XmlLogPath"];
            xmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            language = ConfigurationSystem.GimmonixConfig["Lang"];
            currency = ConfigurationSystem.GimmonixConfig["Currency"];
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.GimmonixConfig["ServiceUrl"]);
            userName= ConfigurationSystem.GimmonixConfig["username"];
            passWord= ConfigurationSystem.GimmonixConfig["password"];
            try
            {
                if (!System.IO.Directory.Exists(xmlPath))
                {
                    System.IO.Directory.CreateDirectory(xmlPath);
                }
            }
            catch { }

        }
        #region HotelSearch
        /// <summary>
        /// Returns Hotel Results for the Search Criteria.
        /// </summary>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">This is B2B Markup</param>
        /// <param name="markupType">B2B is MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        HotelRequest request = new HotelRequest();
        decimal markup = 0;
        string markupType = string.Empty;
        decimal discount = 0;
        string discounttype = string.Empty;
        //int rowStartIndex = 0, rowEndIndex = 0;
        List<int> hotelid = new List<int>();
        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        List<HotelSearchResult> results = new List<HotelSearchResult>();
        //List<HotelStaticData> hotelStaticData = new List<HotelStaticData>();
        public HotelSearchResult[] GetSearchResults(HotelRequest request, decimal markup, string markupType, decimal discount, string discounttype)
        {
            HotelSearchResult[] searchRes = new HotelSearchResult[0];
            try
            {
                this.request = request;
                this.markup = markup;
                this.markupType = markupType;
                this.discount = discount;
                this.discounttype = discounttype;
                results = new List<HotelSearchResult>();
                XmlDocument xmlDoc = new XmlDocument();
                //  string searchrequest = string.Empty;
                string searchresponse = string.Empty;
                Gimmonix.GimmonixService.ServiceRequest searchrequest = new Gimmonix.GimmonixService.ServiceRequest();
                ServiceRequestResponse serviceRequestResponse = new ServiceRequestResponse();
                DynamicDataServiceRsp rsp;
                HotelStaticData staticInfo = new HotelStaticData();
                try
                {

                    //records means number of hotels passed in each request(this value is hard coded by default with 2999)
                    //maxrecords means number of hotels passed in each request(this value is hard coded by default with 2999)
                    int records = 2999, rowsCount = 0, startIndex = 0, endIndex = 0, maxrecords = 2999;
                    //int records = 999, rowsCount = 0, startIndex = 0, endIndex = 0, maxrecords = 999;// For Testing
                    HotelSearchResult[] result = new HotelSearchResult[0];
                    //ReadySources will hold same source orderings i.e Gimmonix1, Gimmonix12 ....
                    Dictionary<string, int> readySources = new Dictionary<string, int>();
                    //This dictionary will hold what fare search we want to call
                    Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();
                    if (request.Latitude == 0 && request.Longtitude == 0)//  For City search
                    {
                        
                        if (hotelStaticData != null && hotelStaticData.Count > 0)
                        {

                            hotelid = hotelStaticData.Select(i => i.TravolutionaryID).ToList().Select(int.Parse).ToList();
                            //searchrequest = GenerateHotelSearchRequest(this.request, hotelid);
                        }
                        else
                        {
                            Audit.Add(EventType.Exception, Severity.High, 0, "There is No Hotelid's for :" + request.CityName, "");
                            //throw new Exception("No cities found :" + request.CityName);
                        }
                        //ends

                    }
                    else
                    {
                        //if (request.Latitude >= 0 && request.Longtitude >= 0)
                        //{//search with point of intrest (POI) then by default passing with 0's
                        string key = "0- 0 - 0";
                        listOfThreads.Add(key, new WaitCallback(GenarateSearchResults));
                        readySources.Add(key, 10);
                        //}
                    }
                    //Number of hotels for search city
                    rowsCount = hotelid.Count;

                    int j = 0;
                    while (rowsCount > 0)
                    {
                        //records are <= default hotel request count in each request
                        //
                        if (records <= maxrecords)
                        {
                            startIndex = 0;
                            endIndex = (rowsCount <= records) ? rowsCount : records;
                        }
                        //records are > default hotel request count in each request
                        else
                        {
                            startIndex += maxrecords;
                            if ((records) < hotelid.Count)
                            {
                                //each request  need to pass 2999 records only
                                endIndex = maxrecords;
                            }
                            else
                            {
                                endIndex = rowsCount;
                            }
                        }
                        string key = j.ToString() + "-" + startIndex + "-" + endIndex;
                        listOfThreads.Add(key, new WaitCallback(GenarateSearchResults));
                        readySources.Add(key, 10);
                        j++;
                        rowsCount -= maxrecords;
                        if ((records + maxrecords) > hotelid.Count)
                        {
                            records += hotelid.Count - records;
                        }
                        else
                        {
                            records += maxrecords;
                        }

                    }
                    eventFlag = new AutoResetEvent[readySources.Count];
                    int k = 0;
                    //Start each fare search within a Thread which will automatically terminate after the results are received.
                    foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                    {
                        if (readySources.ContainsKey(deThread.Key))
                        {
                            ThreadPool.QueueUserWorkItem(deThread.Value, deThread.Key);
                            eventFlag[k] = new AutoResetEvent(false);
                            k++;
                        }
                    }
                    if (k != 0)
                    {//waiting time for all threads
                        if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 0, 90), true))
                        {
                            //TODO: audit which thread is timed out                
                        }
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GimmonixSearch, Severity.Normal, 1, "Gimmonix.GenerateHotelSearchRequest Err :" + ex.ToString(), "0");
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GimmonixSearch, Severity.Normal, 1, "Gimmonix.GetSearchResults Err :" + ex.ToString(), "0");
                throw ex;
            }

            searchRes = results.ToArray();
            return searchRes;
        }





        private void GenarateSearchResults(object eventNumber)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                string response = string.Empty;
                //get the range values for request
                string[] values = eventNumber.ToString().Split('-');
                Gimmonix.GimmonixService.ServiceRequest searchrequest = new Gimmonix.GimmonixService.ServiceRequest();
                ServiceRequestResponse serviceRequestResponse = new ServiceRequestResponse();
                DynamicDataServiceRsp rsp;

                try
                {
                    searchrequest = GenerateHotelSearchRequest(this.request, Convert.ToInt32(values[1]), Convert.ToInt32(values[2]), hotelid);
                    string filePath = string.Empty;
                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(Gimmonix.GimmonixService.ServiceRequest), new Type[] { typeof(HotelsServiceSearchRequest) });
                        filePath = xmlPath + sessionId + "_SearchRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + "_" + values[0] + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, searchrequest);
                        sw.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    filePath = xmlPath + sessionId + "_SearchRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + "_" + values[0] + ".json";
                    try
                    {
                        using (StreamWriter file = File.CreateText(filePath))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            serializer.Serialize(file, JsonConvert.SerializeObject(searchrequest.rqst));
                        }
                    }
                    catch
                    {

                    }
                }
                catch (Exception ex)
                {
                    //Audit.Add(EventType.GrnSearch, Severity.Normal, 1, "GRN.GenerateHotelSerchRequest Err :" + ex.ToString(), "0");
                    throw ex;
                }
                try
                {
                    //Audit.Add(EventType.GimmonixSearch, Severity.Normal, 1, "GIMMO 1", "0");
                    System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };
                    //Audit.Add(EventType.GimmonixSearch, Severity.Normal, 1, "GIMMO 2", "0");
                    serviceRequestResponse = client.ServiceRequest(searchrequest);
                    //Audit.Add(EventType.GimmonixSearch, Severity.Normal, 1, "GIMMO 3", "0");
                    //searchresponse = PostRequest(searchrequest);
                    rsp = serviceRequestResponse.ServiceRequestResult;
                    if (request.Longtitude > 0 && request.Latitude > 0)
                    {
                        if (serviceRequestResponse.ServiceRequestResult != null)
                        {

                            var HotelIds = (from item in serviceRequestResponse.ServiceRequestResult.HotelsSearchResponse.Result select Convert.ToString(item.ID)).ToArray();
                            if (HotelIds.Count() > 0)
                            {
                                
                                hotelStaticData = hotelStaticData.Where(x => HotelIds.Contains(x.TravolutionaryID)).ToList();

                            }
                            else
                            {
                                Audit.Add(EventType.Exception, Severity.High, 0, "There is No Hotelid's for :" + request.CityName, "");
                            }
                        }
                    }
                    string jsonStr = JsonConvert.SerializeObject(rsp);
                    string jsonStr1 = JsonConvert.SerializeObject(serviceRequestResponse);
                    string filePath = xmlPath + sessionId + "_SearchResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + "_" + values[0] + ".json";
                    using (StreamWriter file = File.CreateText(filePath))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(file, jsonStr);
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GimmonixSearch, Severity.Normal, 1, "Gimmonix.serviceRequestResponse Err :" + ex.ToString(), "0");
                    // throw new BookingEngineException("Error: " + ex.Message);
                }


                #region Generate HotelSearchResult Object
                try
                {
                    if (serviceRequestResponse.ServiceRequestResult != null)
                    {
                        GenerateHotelSearchResult(serviceRequestResponse.ServiceRequestResult, this.request, ref results, hotelStaticData, markup, markupType, discount, discounttype);
                    }

                }
                catch (Exception ex)
                {
                    //Audit.Add(EventType.GimmonixSearch, Severity.Normal, 1, "Gimmonix.GenerateHotelSearchResult  Err :" + ex.ToString(), "0");
                    throw ex;
                }

                eventFlag[Convert.ToInt32(eventNumber.ToString().Split('-')[0])].Set();

            }
            catch (Exception ex)
            {
                eventFlag[Convert.ToInt32(eventNumber.ToString().Split('-')[0])].Set();
                Audit.Add(EventType.GimmonixSearch, Severity.Normal, 1, "Gimmonix.GenerateHotelSearchResult  Err :" + ex.ToString(), "0");
                throw ex;
            }
        }
        #endregion







        /// <summary>
        /// Creating HotelReuest object
        /// </summary>
        private Gimmonix.GimmonixService.ServiceRequest GenerateHotelSearchRequest(HotelRequest req, int startIndex, int endIndex, List<int> hotelid)
        {
            // dtHotels = HotelStaticData.GetStaticHotelIds(req.CityName, HotelBookingSource.GIMMONIX, 0);
            Gimmonix.GimmonixService.ServiceRequest serviceRequest = new Gimmonix.GimmonixService.ServiceRequest();
            try
            {
                var request = string.Empty;
                StringBuilder requestMsg = new StringBuilder();
                HotelsServiceSearchRequest hotelsServiceSearchRequest = new HotelsServiceSearchRequest();

                Credentials credentials = new Credentials();
                credentials.UserName = userName;//ConfigurationSystem.GimmonixConfig["username"];
                credentials.Password = passWord;//ConfigurationSystem.GimmonixConfig["password"];

                DynamicDataServiceRqst dynamicDataServiceRqst = new DynamicDataServiceRqst();
                dynamicDataServiceRqst.Credentials = credentials;
                //hotelsServiceSearchRequest.ClientIP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]!=null ? System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]: System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                hotelsServiceSearchRequest.ClientIP = "185.78.160.133";


                hotelsServiceSearchRequest.DesiredResultCurrency = agentCurrency;
                hotelsServiceSearchRequest.Residency = req.PassengerCountryOfResidence;
                //hotelsServiceSearchRequest.TimeoutSeconds = 0;// 0 - it will take as per the settings panel timeoout, else it will override the timeout
                hotelsServiceSearchRequest.CheckIn = req.StartDate;
                hotelsServiceSearchRequest.CheckOut = req.EndDate;
                hotelsServiceSearchRequest.DetailLevel = SearchDetailLevel.Meta;
                hotelsServiceSearchRequest.ExcludeHotelDetails = true;
                if (req.Latitude > 0 && req.Longtitude > 0)
                {
                    GeoLocation geoLocation = new GeoLocation();
                    geoLocation.Longitude = req.Longtitude;
                    geoLocation.Latitude = req.Latitude;
                    hotelsServiceSearchRequest.GeoLocationInfo = geoLocation;
                    hotelsServiceSearchRequest.RadiusInMeters = req.Radius;
                }
                else
                {
                    //Get the range of hotelids of required range
                    hotelsServiceSearchRequest.HotelIds = hotelid.GetRange(startIndex, endIndex).ToArray();
                }

                hotelsServiceSearchRequest.SupplierIds = req.SupplierIds;
                //hotelsServiceSearchRequest.HotelLocation = 631209;
                hotelsServiceSearchRequest.Nights = (req.EndDate - req.StartDate).Days;

                HotelRoomRequest[] hotelRoomRequest = new HotelRoomRequest[req.NoOfRooms];
                for (int i = 0; i < req.NoOfRooms; i++)
                {
                    HotelRoomRequest hroom = new HotelRoomRequest();
                    hroom.AdultsCount = req.RoomGuest[i].noOfAdults;
                    if (req.RoomGuest[i].childAge != null)
                    {
                        hroom.KidsAges = req.RoomGuest[i].childAge.ToArray();
                    }
                    hotelRoomRequest[i] = hroom;
                }

                hotelsServiceSearchRequest.Rooms = hotelRoomRequest;

                dynamicDataServiceRqst.Request = hotelsServiceSearchRequest;
                dynamicDataServiceRqst.RequestType = ServiceRequestType.Search;
                dynamicDataServiceRqst.TypeOfService = ServiceType.Hotels;
                serviceRequest.rqst = dynamicDataServiceRqst;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "exception in GenerateHotelSearchRequest():" + ex.ToString(), "");
                throw ex;
            }
            return serviceRequest;

        }

        private void GenerateHotelSearchResult(DynamicDataServiceRsp rsp, HotelRequest req, ref List<HotelSearchResult> hotelResults, List<HotelStaticData> staticdata, decimal markup, string markupType, decimal discount, string discountType)
        {


            try
            {
                if (rsp.HotelsSearchResponse != null)
                {
                    foreach (Hotel hotel in rsp.HotelsSearchResponse.Result)
                    {
                        /* Exclude EPS packages for B2C */
                        //if (request.Transtype == "B2C" && hotel.SuppliersLowestPackagePrices != null && hotel.SuppliersLowestPackagePrices.ContainsKey("EPS"))
                        //    hotel.SuppliersLowestPackagePrices.Remove("EPS");

                        if (hotel.SuppliersLowestPackagePrices != null && hotel.SuppliersLowestPackagePrices.Count == 0)
                            continue;

                        HotelSearchResult hotelResult = new HotelSearchResult();
                        hotelResult.HotelCode = hotel.ID.ToString();
                        //HotelStaticData hotelInfo = staticdata.Find(h => h.HotelCode.Trim() == hotelResult.HotelCode);
                        HotelStaticData hotelInfo = staticdata.Find(h => h.TravolutionaryID.Trim() == hotelResult.HotelCode);
                        if (hotelInfo != null)
                        {
                            hotelResult.HotelAddress = (!string.IsNullOrEmpty(hotelInfo.HotelAddress) ? hotelInfo.HotelAddress : string.Empty) + " " + (!string.IsNullOrEmpty(hotelInfo.PhoneNumber) ? hotelInfo.PhoneNumber : string.Empty);
                            hotelResult.HotelContactNo = !string.IsNullOrEmpty(hotelInfo.PhoneNumber) ? hotelInfo.PhoneNumber : string.Empty;
                            hotelResult.HotelDescription = !string.IsNullOrEmpty(hotelInfo.HotelDescription) ? hotelInfo.HotelDescription : string.Empty;
                            hotelResult.HotelName = hotelInfo.HotelName;
                            // hotelResult.HotelPicture = hotel.DefaultImage.FullSize;
                            // if(string.IsNullOrEmpty(hotelResult.HotelPicture))
                            // {
                            ///      hotelResult.HotelPicture = !string.IsNullOrEmpty(hotelInfo.HotelPicture)? hotelInfo.HotelPicture:((hotelInfo.Images!=null? hotelInfo.Images[0]:""));
                            //   } 
                            hotelResult.Rating = hotelInfo.Rating;
                            hotelResult.HotelImages = hotelInfo.Images;
                            hotelResult.HotelStaticID = hotelInfo.HotelStaticID;

                        }
                        else
                        {
                            continue;
                        }

                        string pattern = request.HotelName;
                        #region To get only those satisfy the search conditions
                        if (Convert.ToInt16(hotelResult.Rating) >= (int)request.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)request.MaxRating)
                        {
                            if (!(pattern != null && pattern.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(pattern.ToUpper()) : true))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                        #endregion
                        if (hotelInfo != null)
                        {
                            hotelResult.HotelFacilities = !string.IsNullOrEmpty(hotelInfo.HotelFacilities) ? hotelInfo.HotelFacilities.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries).Select(Convert.ToString).ToList() : null;
                            hotelResult.HotelMap = hotelInfo.HotelMap;
                        }


                        hotelResult.PropertyType = rsp.SessionID;
                        hotelResult.RoomGuest = request.RoomGuest;
                        hotelResult.StartDate = request.StartDate;
                        hotelResult.EndDate = request.EndDate;
                        hotelResult.Currency = agentCurrency;//childRate.Attributes["CurrencyCode"].Value;
                        hotelResult.BookingSource = HotelBookingSource.GIMMONIX;
                        string cc;
                        cc = (!string.IsNullOrEmpty(hotelResult.Currency) ? hotelResult.Currency : "AED");
                        rateOfExchange = (exchangeRates.ContainsKey(cc) ? exchangeRates[cc] : 1);
                        hotelResult.Price = new PriceAccounts();
                        hotelResult.Price.SupplierCurrency = cc;
                        hotelResult.Price.RateOfExchange = rateOfExchange;
                        hotelResult.HotelPicture = hotel.DefaultImage.FullSize;
                       // hotel.SuppliersLowestPackagePrices.
                        decimal totalprice = Convert.ToDecimal(hotel.SuppliersLowestPackagePrices.FirstOrDefault().Value);
                        hotelResult.SourceName = hotel.SuppliersLowestPackagePrices.FirstOrDefault().Key;
                        //VAT Calucation Changes Modified 14.12.2017
                        decimal hotelTotalPrice = 0m;
                        decimal vatAmount = 0m;

                        PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                        hotelTotalPrice = Math.Round((totalprice) * rateOfExchange, decimalPoint);
                        if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                        {
                            hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                        }
                        hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);

                        hotelResult.Price = new PriceAccounts();
                        hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                        hotelResult.Price.SupplierPrice = Math.Round((totalprice));
                        hotelResult.Price.RateOfExchange = rateOfExchange;
                        hotelResult.Price.SupplierPrice = hotelTotalPrice;
                        hotelResult.SellingFare = hotelTotalPrice;
                        hotelResult.TotalPrice = hotelResult.SellingFare + (markupType == "F" ? markup * request.NoOfRooms : hotelResult.SellingFare * (markup / 100));
                        hotelResult.Price.Discount = 0;
                        if (markup > 0 && discount > 0)
                        {
                            hotelResult.Price.Discount = discountType == "F" ? discount * request.NoOfRooms : (hotelResult.TotalPrice - hotelResult.SellingFare) * (discount / 100);
                        }
                        hotelResult.Price.Discount = hotelResult.Price.Discount > 0 ? hotelResult.Price.Discount : 0;
                        hotelResult.Price.NetFare = hotelResult.TotalPrice;
                        hotelResult.Price.AccPriceType = PriceType.NetFare;
                        hotelResult.Price.RateOfExchange = rateOfExchange;
                        hotelResult.Price.Currency = agentCurrency;
                        hotelResult.Price.CurrencyCode = agentCurrency;
                        hotelResult.Transtype = request.Transtype;
                        hotelResult.ApiSessionId = SessionId;

                        hotelResults.Add(hotelResult);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Refetch Hotel Details
        /// <summary>
        /// Complete Room Details For Selected Hotel
        /// </summary>
        public void GetRoomRefetchDetails(ref HotelSearchResult hotelResult, HotelRequest request, decimal markup, string markupType, string sessionid, decimal discount, string discountType)
        {
            try
            {
                SimilarHotelSourceInfo gimmonixHotel = hotelResult.SimilarHotels.Where(f => f.Source == HotelBookingSource.GIMMONIX).FirstOrDefault();
                hotelResult.PropertyType = gimmonixHotel.SourceSessionId;
               // hotelResult.hotelcode = gimmonixHotel[0].SourceSessionId;
                hotelResult.RoomDetails = null;
                string roomFetchrequest = string.Empty;
                string roomFetchresponse = string.Empty;
                XmlDocument docResponse = new XmlDocument();
                NoofRooms = request.NoOfRooms;
                StringBuilder requestMsg = new StringBuilder();

                Gimmonix.GimmonixService.ServiceRequest requests = new Gimmonix.GimmonixService.ServiceRequest();
                ServiceRequestRequest searchrequest = new ServiceRequestRequest();
                ServiceRequestResponse PackagesResponse = new ServiceRequestResponse();

                try
                {

                    requests = GenerateRoomFetchRequest(gimmonixHotel.HotelCode, hotelResult.PropertyType);
                }
                catch (Exception ex)
                {
                    // Audit.Add(EventType.GimmonixRoomDetails, Severity.Normal, 1, "Gimmoinx.GenerateRoomFetchRequest Err :" + ex.ToString(), "0");
                    throw ex;
                }
                try
                {
                    PackagesResponse = client.ServiceRequest(requests);

                    //zi test start
                    ////----------------------Loading previous xml-------------------------------
                    //System.Xml.Serialization.XmlSerializer serLoc = new System.Xml.Serialization.XmlSerializer(typeof(ServiceRequestResponse));
                    //string staticpath = System.Configuration.ConfigurationSettings.AppSettings["TestStaticXmlLoadPath"];
                    //System.IO.StreamReader writerLoc = new StreamReader(@staticpath + "FlightReservationResponse_Test.xml");
                    //PackagesResponse = serLoc.Deserialize(writerLoc) as ServiceRequestResponse;
                    //writerLoc.Close();
                    // ----------------------------- end test ----------------
                    // test end

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(GetPackagesResponse));
                        string filePath = xmlPath + sessionId + "_RoomFetchResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, PackagesResponse.ServiceRequestResult.HotelsGetPackagesResponse);
                        sw.Close();
                    }
                    catch (Exception ex)
                    {

                    }

                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GimmonixRoomDetails, Severity.Normal, 1, "Gimmoinx.PackagesResponse Err :" + ex.ToString(), "0");
                }
                try
                {
                    ReadRefetchRoomDetails(PackagesResponse.ServiceRequestResult.HotelsGetPackagesResponse, ref hotelResult, request, markup, markupType, discount, discountType);

                }
                catch (Exception ex)
                {
                    // Audit.Add(EventType.GimmonixRoomDetails, Severity.Normal, 1, "Gimmoinx.ReadRefetchRoomDetails Err :" + ex.ToString(), "0");
                    throw new BookingEngineException("Error: " + ex.ToString());
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GimmonixRoomDetails, Severity.Normal, 1, "Gimmoinx.ReadRefetchRoomDetails Err :" + ex.ToString(), "0");
                throw ex;
            }
        }
        /// <summary>
        /// Creating HotelReuest object
        /// </summary>
        private Gimmonix.GimmonixService.ServiceRequest GenerateRoomFetchRequest(string hotelcode, string sessionid)
        {
            Gimmonix.GimmonixService.ServiceRequest serviceRequest = new Gimmonix.GimmonixService.ServiceRequest();
            try
            {
                GetPackagesRequest packagesRequest = new GetPackagesRequest();

                packagesRequest.HotelID = Convert.ToInt32(hotelcode);
                packagesRequest.ReturnTaxesAndFees = true;// to get taxbreakup in response
                packagesRequest.ReturnProfit = true;// to return marketing fee
                //packagesRequest.TimeoutSeconds = 0;// 0 - it will take as per the settings panel timeoout, else it will override the timeout
                DynamicDataServiceRqst dynamicDataServiceRqst = new DynamicDataServiceRqst();
                dynamicDataServiceRqst.Request = packagesRequest;
                dynamicDataServiceRqst.RequestType = ServiceRequestType.GetPackages;
                dynamicDataServiceRqst.SessionID = sessionid;
                dynamicDataServiceRqst.TypeOfService = ServiceType.Hotels;
                serviceRequest.rqst = dynamicDataServiceRqst;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(Gimmonix.GimmonixService.ServiceRequest), new Type[] { typeof(GetPackagesRequest) });
                    string filePath = xmlPath + sessionId + "_RoomFetchRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, serviceRequest);
                    sw.Close();
                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return serviceRequest;
        }


        /// <summary>
        ///Read the Complete Room Details For Selected Hotel
        /// </summary>
        private void ReadRefetchRoomDetails(GetPackagesResponse roomfetchreposnse, ref HotelSearchResult hotelResult, HotelRequest request, decimal markup, string markupType, decimal discount, string discountType)
        {
            try
            {
                List<HotelRoomsDetails> rooms = new List<HotelRoomsDetails>();
                foreach (Package package in roomfetchreposnse.Result)
                {
                    /* Exclude EPS packages for B2C */
                    //if (hotelResult.Transtype == "B2C" && package.SupplierName == "EPS")
                    //    continue;

                    decimal totalprice = Convert.ToDecimal(package.SimplePrice);
                    //hotelResult.TotalPrice = 0;
                    decimal roomprice = totalprice / request.NoOfRooms;

                    decimal marketFee = 0, publishedFare = roomprice, mfDiscountAmount = 0;
                    if (package.TotalProfit > 0)
                    {
                        marketFee = Convert.ToDecimal(package.TotalProfit) / request.NoOfRooms;
                        mfDiscountAmount = mfDiscountPercent > 0 ? marketFee * (mfDiscountPercent / 100) : 0;
                        roomprice = mfDiscountAmount > 0 ? roomprice - mfDiscountAmount : roomprice;
                    }

                    List<RoomTaxBreakup> breakup = new List<RoomTaxBreakup>();
                    if (package.PackagePrice != null && package.PackagePrice.TaxesAndFees != null)
                    {
                        foreach (TaxesAndFees taxes in package.PackagePrice.TaxesAndFees)
                        {
                            RoomTaxBreakup roomTaxBreakup = new RoomTaxBreakup();
                            roomTaxBreakup.Currency = taxes.Currency;
                            decimal taxExchangerates = (exchangeRates.ContainsKey(roomTaxBreakup.Currency) ? exchangeRates[roomTaxBreakup.Currency] : 1);
                            //if(request.RegionCode=="EU" && roomTaxBreakup.TaxTitle== "tax_and_service_fee") // IF EU title change 
                            //    roomTaxBreakup.TaxTitle = taxes.FeeTitle = "Tax Recover Charges";
                            roomTaxBreakup.TaxTitle = taxes.FeeTitle;
                            roomTaxBreakup.IsIncluded = taxes.IsIncludedInPrice;
                            roomTaxBreakup.UnitType = taxes.UnitType;
                            roomTaxBreakup.Value = Convert.ToDecimal(taxes.Value) * taxExchangerates;
                            roomTaxBreakup.IsMandatory = taxes.IsMandatory;
                            roomTaxBreakup.FrequencyType = taxes.FrequencyType;
                            breakup.Add(roomTaxBreakup);
                        }
                    }
                    int index = 1;
                    foreach (PackageRoom packageRoom in package.Rooms)
                    {
                        HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                        roomDetail.SequenceNo = index.ToString();
                        roomDetail.TaxBreakups = breakup;
                        //Roomtypecode=sequenceno+packageid+roomid+suppliername+total amount
                        roomDetail.RoomTypeCode = roomDetail.SequenceNo + "||";
                        roomDetail.RoomTypeCode = package.PackageId != null ? roomDetail.RoomTypeCode + package.PackageId + "^" + roomDetail.SequenceNo + "||" : roomDetail.RoomTypeCode + "||";
                        roomDetail.RoomTypeCode = !string.IsNullOrEmpty(packageRoom.Id) ? roomDetail.RoomTypeCode + packageRoom.Id + "||" : roomDetail.RoomTypeCode + "||";
                        roomDetail.RoomTypeCode = !string.IsNullOrEmpty(package.SupplierName) ? roomDetail.RoomTypeCode + package.SupplierName + "||" : roomDetail.RoomTypeCode + "||";

                        //package.PackageId + "||" + packageRoom.Id + "||" + package.SupplierName + "||" + Convert.ToDecimal(package.SimplePrice);
                        roomDetail.mealPlanDesc = packageRoom.RoomBasis;
                        roomDetail.RoomTypeName = packageRoom.RoomName + "-" + package.Refundability.ToString().Replace("Unknown", "as per cancellation policy");
                        string targetRoomkey = packageRoom.TargetRoomKey;
                        RoomContent roomContent = roomfetchreposnse.RoomsContent.FirstOrDefault(r => r.RoomKey == targetRoomkey);
                        if (roomContent != null)
                        {
                            roomDetail.Amenities = roomContent.Amenities.ToList();
                            roomDetail.Images = roomContent.Images.ToList();
                        }



                        decimal hotelTotalPrice = 0m;
                        decimal vatAmount = 0m;

                        rateOfExchange = (exchangeRates.ContainsKey(hotelResult.Currency) ? exchangeRates[hotelResult.Currency] : 1);
                        PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                        hotelTotalPrice = Math.Round((roomprice) * rateOfExchange, decimalPoint);
                        if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                        {
                            hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                        }
                        hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);
                        roomDetail.TotalPrice = hotelTotalPrice;
                        roomDetail.TotalPrice = hotelTotalPrice;
                        roomDetail.Markup = ((markupType == "F") ? markup : ((roomDetail.TotalPrice) * (markup / 100m)));


                        roomDetail.Discount = 0;
                        if (markup > 0 && discount > 0)
                        {
                            roomDetail.Discount = discountType == "F" ? discount : roomDetail.Markup * (discount / 100);
                        }
                        roomDetail.Discount = roomDetail.Discount > 0 ? roomDetail.Discount : 0;
                        roomDetail.MarkupType = markupType;
                        roomDetail.MarkupValue = markup;
                        roomDetail.SellingFare = roomDetail.TotalPrice;
                        roomDetail.supplierPrice = publishedFare; //Math.Round((roomprice));
                        roomDetail.PublishedFare = Math.Round(publishedFare * rateOfExchange, decimalPoint);
                        roomDetail.MarketingFee = Math.Round(marketFee * rateOfExchange, decimalPoint);
                        roomDetail.MFDiscountPercent = mfDiscountPercent;
                        roomDetail.MFDiscountAmount = Math.Round(mfDiscountAmount * rateOfExchange, decimalPoint);

                        //day wise price calucation
                        System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                        RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                        decimal roomAmount = roomDetail.TotalPrice;

                        for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                        {
                            decimal price = roomDetail.TotalPrice / diffResult.Days;
                            if (fareIndex == diffResult.Days - 1)
                            {
                                price = totalprice;
                            }
                            totalprice -= price;
                            hRoomRates[fareIndex].Amount = price;
                            hRoomRates[fareIndex].BaseFare = price;
                            hRoomRates[fareIndex].SellingFare = price;
                            hRoomRates[fareIndex].Totalfare = price;
                            hRoomRates[fareIndex].RateType = RateType.Negotiated;
                            hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);
                        }
                        roomDetail.Rates = hRoomRates;
                        roomDetail.TaxDetail = new PriceTaxDetails();
                        roomDetail.TaxDetail = priceTaxDet;
                        roomDetail.InputVATAmount = vatAmount;
                        roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + Convert.ToDecimal(package.SimplePrice);
                        roomDetail.TotalPrice = roomDetail.TotalPrice + roomDetail.Markup;
                        //hotelResult.TotalPrice = hotelResult.TotalPrice + roomDetail.TotalPrice + roomDetail.Markup;
                        index++;
                        roomDetail.SupplierName = HotelBookingSource.GIMMONIX.ToString();
                        roomDetail.SupplierId =Convert.ToString((int)HotelBookingSource.GIMMONIX);
                        roomDetail.CommonRoomKey =Convert.ToString(package.PackageId);
                        roomDetail.IsIndividualSelection = false;//Here IsIndividualSelection = false means roomselection is combination.
                        rooms.Add(roomDetail);
                    }
                }
                rooms = rooms.OrderBy(h => h.TotalPrice).ToList();
                hotelResult.RoomDetails = rooms.ToArray();
                hotelResult.TotalPrice = 0;
                hotelResult.Price.Discount = 0;
                for (int i = 0; i < request.NoOfRooms; i++)
                {
                    for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                    {
                        if (hotelResult.RoomDetails[j].SequenceNo.Contains((i + 1).ToString()))
                        {
                            hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice;
                            hotelResult.Price.Discount += hotelResult.RoomDetails[j].Discount;
                            hotelResult.Price.NetFare = hotelResult.TotalPrice;
                            hotelResult.Price.AccPriceType = PriceType.NetFare;
                            hotelResult.Price.RateOfExchange = rateOfExchange;
                            hotelResult.Price.Currency = agentCurrency;
                            hotelResult.Price.CurrencyCode = agentCurrency;
                            break;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Fetch cancellation policy Details
        /// <summary>
        /// Complete Room Cancellation policy Details For Selected Hotel
        public void GetCancellationDetails(HotelSearchResult searchResult, string roomtypecode,HotelRequest hotelRequest, string roomNo, decimal markup, string markupType)
        {
            try
            {
                string roomcancellationrequest = string.Empty;
                string roomcancellationresponse = string.Empty;
                ServiceRequestRequest policyrequest = new ServiceRequestRequest();
                ServiceRequestResponse serviceRequestResponse = new ServiceRequestResponse();
                Gimmonix.GimmonixService.ServiceRequest request = new Gimmonix.GimmonixService.ServiceRequest();

                try
                {
                    GenerateCancellationPolicyRequest(ref searchResult, roomtypecode,hotelRequest,roomNo, markup, markupType);
                }
                catch (Exception ex)
                {
                    //Audit.Add(EventType.GimmonicCancelltionPolicy, Severity.Normal, 1, "GIMMONIX.GenerateCancellationPolicyRequest Err :" + ex.ToString(), "0");
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GimmonicCancelltionPolicy, Severity.Normal, 1, "GIMMONIX.GenerateCancellationPolicyRequest Err :" + ex.ToString(), "0");
                throw ex;
            }
        }
        /// <summary>
        /// Creating HotelReuest object
        /// </summary>
        private void GenerateCancellationPolicyRequest(ref HotelSearchResult hotelResult, string roomtypecode, HotelRequest hotelRequest, string roomNo,decimal markup, string markupType)
        {
            try
            {
                SimilarHotelSourceInfo sourceInfo = hotelResult.SimilarHotels.Find(s => s.Source == HotelBookingSource.GIMMONIX);
                Gimmonix.GimmonixService.ServiceRequest serviceRequest = new Gimmonix.GimmonixService.ServiceRequest();
                HotelCancellationPolicyRequest cancellationpolicyRequest = new HotelCancellationPolicyRequest();
                cancellationpolicyRequest.ClientIP = "185.78.160.133";
                cancellationpolicyRequest.HotelID = Convert.ToInt32(sourceInfo.HotelCode);
                string packageid = (hotelResult.RoomDetails.Where(r => r.RoomTypeCode == roomtypecode).ToList())[0].CommonRoomKey; //roomtypecode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[1];
                Guid guid = new Guid(packageid);
                Guid[] guids = new Guid[0];
                cancellationpolicyRequest.PackageID = guid;
                cancellationpolicyRequest.PackagesIDs = guids;
                DynamicDataServiceRqst dynamicDataServiceRqst = new DynamicDataServiceRqst();
                dynamicDataServiceRqst.Request = cancellationpolicyRequest;
                dynamicDataServiceRqst.RequestType = ServiceRequestType.GetCancelationPolicy;
                dynamicDataServiceRqst.SessionID = hotelResult.PropertyType;
                dynamicDataServiceRqst.TypeOfService = ServiceType.Hotels;
                serviceRequest.rqst = dynamicDataServiceRqst;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(Gimmonix.GimmonixService.ServiceRequest), new Type[] { typeof(HotelCancellationPolicyRequest) });
                    string filePath = xmlPath + sessionId + "_CancallationPolicyRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, serviceRequest);
                    sw.Close();
                }
                catch (Exception ex)
                {

                }
                ServiceRequestResponse response = client.ServiceRequest(serviceRequest);

                try
                {
                    string filePath = xmlPath + sessionId + "_CancellationResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    if (response != null)
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(HotelCancellationPolicyResponse));
                        ser.Serialize(sw, response.ServiceRequestResult.HotelCancellationPolicyResponse);
                        sw.Close();
                    }
                    else
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Error));
                        ser.Serialize(sw, response.ServiceRequestResult.Errors[0]);
                        sw.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                string message = string.Empty;
                //Cancel Policy Updates
                List<HotelCancelPolicy> cancelPolicy = new List<HotelCancelPolicy>();
                HotelCancelPolicy cancelPolicyItem = new HotelCancelPolicy();
                if (response != null)
                {
                    CancellationPolicy[] cancellationPolicyItems = response.ServiceRequestResult.HotelCancellationPolicyResponse.CancellationPolicies;
                    foreach (CancellationPolicy cancellationPolicy in cancellationPolicyItems)
                    {
                        decimal chargeAmt = 0m;
                        decimal markupPrice = 0m;
                        rateOfExchange = 1;
                        Price price = new Price();
                        //roomDetail.Markup = ((markupType == "F") ? markup : ((roomDetail.TotalPrice) * (markup / 100m)));
                        if (markup > 0)
                        {
                            markupPrice = ((markupType == "F") ? markup : (Convert.ToDecimal(cancellationPolicy.CancellationFee.FinalPrice) * (markup / 100m))); ;
                            price.FinalPrice = cancellationPolicy.CancellationFee.FinalPrice + Convert.ToDouble(markupPrice);
                        }
                        else
                            price.FinalPrice = cancellationPolicy.CancellationFee.FinalPrice;

                        chargeAmt = Convert.ToDecimal((Convert.ToDecimal(price.FinalPrice / NoofRooms) * rateOfExchange).ToString("N" + hotelResult.Price.DecimalPoint));
                        //int buffer = Convert.ToInt32(ConfigurationSystem.GimmonixConfig["buffer"]);
                        DateTime beforeDate = Convert.ToDateTime(cancellationPolicy.DateFrom);
                        DateTime todate = Convert.ToDateTime(cancellationPolicy.DateTo);
                        //Cancel Policy Updates
                        int bufferDays = 1;
                        // beforeDate = beforeDate.AddDays(-bufferDays);
                        beforeDate = beforeDate.AddDays(-CnclBuffer);
                        if (!string.IsNullOrEmpty(message))
                        {
                            message = message + "|" + "Cancellation is applied between:" + beforeDate.ToString("dd/MMM/yyyy") + "-" + todate.ToString("dd/MMM/yyyy") + ", you will be charged " + hotelResult.Currency + " " + (chargeAmt / hotelResult.RoomGuest.Length).ToString("N" + hotelResult.Price.DecimalPoint);
                        }
                        else
                        {
                            message = "Cancellation is applied between:" + beforeDate.ToString("dd/MMM/yyyy") + "-" + todate.ToString("dd/MMM/yyyy") + ", you will be charged " + hotelResult.Currency + " " + (chargeAmt / hotelResult.RoomGuest.Length).ToString("N" + hotelResult.Price.DecimalPoint);
                        }

                        //Cancel Policy Updates
                        cancelPolicyItem.Amount = Convert.ToDecimal((chargeAmt / hotelResult.RoomGuest.Length).ToString("N" + hotelResult.Price.DecimalPoint));
                        cancelPolicyItem.FromDate = beforeDate;
                        cancelPolicyItem.Todate = todate;
                        cancelPolicyItem.BufferDays = bufferDays;
                        cancelPolicyItem.ChargeType = "Amount";
                        cancelPolicyItem.Currency = hotelResult.Currency;
                        cancelPolicyItem.Remarks = string.IsNullOrEmpty(cancellationPolicy.Description) ? "Cancellation is applied between:" + beforeDate.ToString("dd/MMM/yyyy") + "-" + todate.ToString("dd/MMM/yyyy") + ", you will be charged " + hotelResult.Currency + " " + (chargeAmt / hotelResult.RoomGuest.Length).ToString("N" + hotelResult.Price.DecimalPoint) : cancellationPolicy.Description;
                        cancelPolicy.Add(cancelPolicyItem);
                    }

                }

                for (int i = 0; i < hotelResult.RoomDetails.Length; i++)
                {
                    if (hotelResult.RoomDetails[i].CommonRoomKey== packageid)
                    {
                        // hotelResult.RoomDetails[i].CancellationPolicy = message;
                        hotelResult.RoomDetails[i].CancellationPolicy = message + "^" + "Date and time is calculated based on local time of destination";
                        string sHotelPolicy = response.ServiceRequestResult.HotelCancellationPolicyResponse.BookingRemarks;
                        // sHotelPolicy = sHotelPolicy.Replace(".", "DOTTTTTTT").Replace(",", "COMMMAAAAA").Replace(".", "DOTTTTTTT").
                        //   Replace(":", "TMDIFFFFF").Replace(" ", "SPACEEEEEE").Replace("<ul>", "\n").Replace("</ul>", "\n").
                        // Replace("<li>", "\n").Replace("</li>", "\n").Replace("<u>", "\n").Replace("</u>", "\n").
                        //Replace("<p>", "\n").Replace("</p>", "\n").Replace("<a>", "\a").Replace("</u>", "\n").
                        //Replace("\n", "NEWLINEEEEEEE");
                        //sHotelPolicy = GenericStatic.ReplaceNonASCII(sHotelPolicy);
                        //sHotelPolicy = GenericStatic.FilterAlphaNumeric(sHotelPolicy);
                        //sHotelPolicy = sHotelPolicy.Replace("DOTTTTTTT", ".").Replace("COMMMAAAAA", ",").Replace("DOTTTTTTT", ".").
                        //  Replace("TMDIFFFFF", ":").Replace("SPACEEEEEE", " ").Replace("NEWLINEEEEEEE", ". ");
                        hotelResult.RoomDetails[i].EssentialInformation = sHotelPolicy;
                        //Cancel Policy Updates
                        hotelResult.RoomDetails[i].CancelPolicyList = cancelPolicy;
                        // Display Disclaimer
                        if ((hotelResult.RoomDetails[i].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[3] == "EPS(Hotels)" || hotelResult.RoomDetails[i].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[3] == "EPS") && (hotelResult.RoomGuest[Convert.ToInt32(roomNo) - 1].noOfChild > 0))
                        {
                            hotelResult.RoomDetails[i].EssentialInformation = response.ServiceRequestResult.HotelCancellationPolicyResponse.BookingRemarks + "^" + "Bookings including children will be based on sharing parents bedding and no separate bed for children is provided unless otherwise stated:";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
        #region Fetch payment preference  Details for selected package
        /// <summary>
        /// Complete Room Details For Selected Hotel
        /// </summary>
        public string GetPaymentPreference(string PaymentPrefParams)//(ref HotelItinerary itinerary)
        {
            Gimmonix.GimmonixService.ServiceRequest PaymentPreferencerequest = new Gimmonix.GimmonixService.ServiceRequest();
            string PaymentPreferenceresponse = string.Empty;
            string filePath = string.Empty;
            try
            {
                PaymentPreferencerequest = GeneratePaymentPreferenceRequest(PaymentPrefParams.Split(',')[0], PaymentPrefParams.Split(',')[1], PaymentPrefParams.Split(',')[2]);//(itinerary);
                try
                {
                    filePath = xmlPath + sessionId + "_PaymentPreferenceRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".json";
                    using (StreamWriter file = File.CreateText(filePath))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(file, JsonConvert.SerializeObject(PaymentPreferencerequest.rqst));
                    }
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GimmonixPaymentPreferences, Severity.Normal, 1, "Gimmonix.PaymentPreferencerequest Err :" + ex.ToString(), "0");
                throw ex;
            }
            try
            {
                ServiceRequestResponse serviceRequestResponse = client.ServiceRequest(PaymentPreferencerequest);
                try
                {
                    filePath = xmlPath + sessionId + "_PaymentPreferenceResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    if (serviceRequestResponse.ServiceRequestResult.HotelPaymentPreferences != null)
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(PaymentPreferencesResponse));
                        ser.Serialize(sw, serviceRequestResponse.ServiceRequestResult.HotelPaymentPreferences);
                        sw.Close();
                    }
                    else
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Error));
                        ser.Serialize(sw, serviceRequestResponse.ServiceRequestResult.Errors[0]);
                        sw.Close();
                    }
                }
                catch { }
                PaymentPreferencesResponse paymentPref = serviceRequestResponse.ServiceRequestResult.HotelPaymentPreferences;
                for (int i = 0; i < paymentPref.ApplicablePreferences.Count(); i++)
                {
                    PaymentPreferenceresponse += paymentPref.ApplicablePreferences[i].Type + ",";
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GimmonixPaymentPreferences, Severity.High, 1, "Gimmonix.GetPaymentPreference Err :" + ex.ToString(), "0");

            }
            return PaymentPreferenceresponse.TrimEnd(',');
        }
        /// <summary>
        /// Creating Payment Preference Reuest object
        /// </summary>
        private Gimmonix.GimmonixService.ServiceRequest GeneratePaymentPreferenceRequest(string HotelCode, string PackageID, string PropertyType)//(HotelItinerary itinerary)
        {
            Gimmonix.GimmonixService.ServiceRequest Request = new Gimmonix.GimmonixService.ServiceRequest();
            try
            {
                HotelPaymentPreferencesRequest paymentPreferenceRequest = new HotelPaymentPreferencesRequest();
                paymentPreferenceRequest.ClientIP = "185.78.160.133";
                paymentPreferenceRequest.HotelID = Convert.ToInt32(HotelCode);//Convert.ToInt32(itinerary.HotelCode);
                paymentPreferenceRequest.IncludeCancellationPolicy = true;
                paymentPreferenceRequest.PackageID = Guid.Parse(PackageID);//Guid.Parse(itinerary.Roomtype[0].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[1]);

                DynamicDataServiceRqst dynamicDataServiceRqst = new DynamicDataServiceRqst();
                dynamicDataServiceRqst.Request = paymentPreferenceRequest;
                dynamicDataServiceRqst.RequestType = ServiceRequestType.GetPaymentPreferences;
                dynamicDataServiceRqst.SessionID = PropertyType;//itinerary.PropertyType;
                dynamicDataServiceRqst.TypeOfService = ServiceType.Hotels;
                Request.rqst = dynamicDataServiceRqst;

            }
            catch (Exception ex)
            {

            }
            return Request;
        }
        #endregion

        #region Booking 
        /// <summary>
        /// Booking Method
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>       
        public BookingResponse GetBooking(ref HotelItinerary itinerary)
        {
            BookingResponse bookingResult = new BookingResponse();
            try
            {
                Gimmonix.GimmonixService.ServiceRequest Request = new Gimmonix.GimmonixService.ServiceRequest();
                HotelBookRequest bookRequest = new HotelBookRequest();
                bookRequest.ClientIP = "185.78.160.133";
                //bookRequest.AgentRemarks = "Test Booking";
                bookRequest.AgentRemarks = string.Empty;// itinerary.SpecialRequest;
                bookRequest.Remarks = itinerary.Remarks;
                bookRequest.RoomsRemarks = new Dictionary<string, string>();
                bookRequest.InternalAgentRef1 = itinerary.AgencyReference;
                Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
                //keyValuePairs.Add("My Test Booking", "My Test Booking");
                bookRequest.AgentRoomsRemarks = keyValuePairs;

                //bookRequest.BookingPrice = Convert.ToDouble(itinerary.TotalPrice);
                // Roomwise supplir amount clubbing
                //decimal totSupplierAmount=itinerary.Roomtype.Sum(r => r.Price.SupplierPrice);
               bookRequest.BookingPrice = Convert.ToDouble(itinerary.Roomtype.Sum(x => x.Price.PublishedFare));
                bookRequest.HotelID = Convert.ToInt32(itinerary.HotelCode);
                Guid guid = Guid.NewGuid();
                bookRequest.LeadPaxId = Convert.ToString(guid);
                bookRequest.LeadPaxRoomId = itinerary.Roomtype[0].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[2];
                bookRequest.PackageID = Guid.Parse(itinerary.Roomtype[0].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[1].Split('^')[0]);
                int paxCount = itinerary.Roomtype.Sum(s => s.PassenegerInfo.Count);
                List<CustomerInfo> customerInfos = new List<CustomerInfo>();
                for (int i = 0; i < itinerary.NoOfRooms; i++)
                {
                    //For Room wise Special Request (RoomRemarks will sent to supplier)
                    bookRequest.RoomsRemarks.Add(itinerary.Roomtype[i].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[2], itinerary.SpecialRequest);
                    for (int j = 0; j < itinerary.Roomtype[i].AdultCount + itinerary.Roomtype[i].ChildCount; j++)
                    {
                        CustomerInfo customer = new CustomerInfo();

                        Gimmonix.GimmonixService.Address address = new Gimmonix.GimmonixService.Address();
                        if (i == 0 && j == 0)
                        {
                            address.AddressLine = itinerary.Roomtype[i].PassenegerInfo[j].Addressline1;
                            address.CityName = itinerary.Roomtype[i].PassenegerInfo[j].City;
                            CountryInfo countryInfo = new CountryInfo();
                            countryInfo.Code = itinerary.Roomtype[i].PassenegerInfo[j].Countrycode;
                            address.CountryName = countryInfo;
                            address.PostalCode = "232132";
                            customer.Address = address;
                        }
                        customer.Allocation = itinerary.Roomtype[i].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[2];
                        Gimmonix.GimmonixService.Email email = new Gimmonix.GimmonixService.Email();
                        //email.Value = itinerary.Roomtype[i].PassenegerInfo[j].Email;
                        email.Value = "support@ibyta.com";
                        customer.Email = email;
                        if (i == 0 && j == 0)
                        {
                            customer.Id = bookRequest.LeadPaxId;
                        }
                        else
                        {

                            Guid id = Guid.NewGuid();
                            customer.Id = Convert.ToString(id);
                        }

                        Person person = new Person();
                        PersonName personName = new PersonName();
                        personName.GivenName = itinerary.Roomtype[i].PassenegerInfo[j].Firstname;
                        personName.NamePrefix = itinerary.Roomtype[i].PassenegerInfo[j].Title;
                        personName.Surname = itinerary.Roomtype[i].PassenegerInfo[j].Lastname;
                        person.Name = personName;
                        customer.PersonDetails = person;
                        if (itinerary.Roomtype[i].PassenegerInfo[j].PaxType == HotelPaxType.Adult)
                        {
                            customer.PersonDetails.Type = PersonType.Adult;
                        }
                        else
                        {
                            customer.PersonDetails.Type = PersonType.Child;

                            customer.PersonDetails.Age = itinerary.Roomtype[i].PassenegerInfo[j].Age;
                        }

                        Phone phone = new Phone();
                        phone.PhoneNumber = itinerary.Roomtype[i].PassenegerInfo[j].Phoneno;
                        customer.Telephone = phone;
                        customerInfos.Add(customer);
                    }
                }
                bookRequest.Passengers = customerInfos.ToArray();
                bookRequest.SelectedPaymentMethod = PaymentMethod.Cash;
                bookRequest.ServiceFee = null;
                DynamicDataServiceRqst dynamicDataServiceRqst = new DynamicDataServiceRqst();
                dynamicDataServiceRqst.Request = bookRequest;
                dynamicDataServiceRqst.RequestType = ServiceRequestType.Book;
                dynamicDataServiceRqst.SessionID = itinerary.PropertyType;
                dynamicDataServiceRqst.TypeOfService = ServiceType.Hotels;
                Request.rqst = dynamicDataServiceRqst;

                //string jsonStr = JsonConvert.SerializeObject(Request.rqst);
                //string filePath = xmlPath + sessionId + "_BookRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".json";
                //using (StreamWriter file = File.CreateText(filePath))
                //{
                //    JsonSerializer serializer = new JsonSerializer();
                //    serializer.Serialize(file, jsonStr);
                //}
                //try
                //{
                //    XmlSerializer ser = new XmlSerializer(typeof(Gimmonix.GimmonixService.ServiceRequest), new Type[] { typeof(HotelBookRequest) });
                //    filePath = xmlPath + sessionId + "_BookRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                //    StreamWriter sw = new StreamWriter(filePath);
                //    ser.Serialize(sw, Request);
                //    sw.Close();
                //}
                //catch (Exception ex)
                //{

                //}

                /* Check if total no of passengers in the itinerary has been binded to booking request or not */
                if (bookRequest.Passengers.Length != itinerary.Roomtype.Sum(x => x.AdultCount + x.ChildCount))
                    throw new Exception("Itinerary pasenger count(" + itinerary.Roomtype.Sum(x => x.AdultCount + x.ChildCount).ToString() + ") " +
                        "and booking request passenger count(" + customerInfos.Count() + ") did not match");

                string sData = GenericStatic.SerializeJson(Request.rqst);
                GenericStatic.GenerateLogs(true, true, ref sData, xmlPath + sessionId + "_BookRequest_");

                ServiceRequestResponse serviceRequestResponse = client.ServiceRequest(Request);

                sData = GenericStatic.SerializeJson(serviceRequestResponse);
                GenericStatic.GenerateLogs(true, true, ref sData, xmlPath + sessionId + "_BookResponse_");

                if (serviceRequestResponse.ServiceRequestResult != null && serviceRequestResponse.ServiceRequestResult.Errors != null
                    && serviceRequestResponse.ServiceRequestResult.Errors.Length > 0)
                {
                    string sMsg = "Booking response :";
                    foreach (var err in serviceRequestResponse.ServiceRequestResult.Errors)
                    {
                        sMsg += err.ErrorCode + ", " + err.ErrorText + ", " + (!string.IsNullOrEmpty(err.Message) ? err.Message : string.Empty) + Environment.NewLine;
                    }
                    throw new BookingEngineException(sMsg);
                }

                bookingResult = readBookingResponse(serviceRequestResponse.ServiceRequestResult, itinerary);

                //    try
                //    {
                //        filePath = xmlPath + sessionId + "_BookResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                //        StreamWriter sw = new StreamWriter(filePath);
                // if (serviceRequestResponse.ServiceRequestResult.HotelOrderBookResponse != null && serviceRequestResponse.ServiceRequestResult.HotelOrderBookResponse.HotelSegments != null)
                //    {
                //        XmlSerializer ser = new XmlSerializer(typeof(OrderBookResponse));
                //        ser.Serialize(sw, serviceRequestResponse.ServiceRequestResult.HotelOrderBookResponse);
                //        sw.Close();
                //    }
                //    else
                //    {
                //        XmlSerializer ser = new XmlSerializer(typeof(Error));
                //        ser.Serialize(sw, serviceRequestResponse.ServiceRequestResult.Errors[0]);
                //        sw.Close();
                //    }               
                //}
                //    catch (Exception ex)
                //    {

                //    }
                //    filePath = xmlPath + sessionId + "_BookResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".json";
                //    try
                //    {
                //        if (serviceRequestResponse.ServiceRequestResult.HotelOrderBookResponse != null && serviceRequestResponse.ServiceRequestResult.HotelOrderBookResponse.HotelSegments != null)
                //        {
                //            using (StreamWriter file = File.CreateText(filePath))
                //            {
                //                JsonSerializer serializer = new JsonSerializer();
                //                serializer.Serialize(file, JsonConvert.SerializeObject(serviceRequestResponse.ServiceRequestResult.HotelOrderBookResponse));
                //            }
                //        }
                //        else
                //        {
                //            using (StreamWriter file = File.CreateText(filePath))
                //            {
                //                JsonSerializer serializer = new JsonSerializer();
                //                serializer.Serialize(file, JsonConvert.SerializeObject(serviceRequestResponse.ServiceRequestResult.Errors[0]));
                //            }    
                //        }

                //    }
                //    catch
                //    {

                //    }

                //try
                //{
                //try
                //{
                //    bookingResult = readBookingResponse(serviceRequestResponse.ServiceRequestResult, itinerary);
                //}
                //catch (Exception ex)
                //{
                //    throw new BookingEngineException("Error: " + ex.ToString());
                //}

                //}
                //catch (Exception ex)
                //{
                //     Audit.Add(EventType.GimmonixBooking, Severity.Normal, 1, "Gimmonix.readBookingResponse Err :" + ex.ToString(), "0");
                //    throw ex;
                //}
            }
            catch (WebException ex)
            {
                Audit.Add(EventType.GimmonixBooking, Severity.Normal, 1, "Web service exception :" + ex.ToString(), "0");
                throw ex;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GimmonixBooking, Severity.Normal, 1, "Gimmonix.GetBooking : " + ex.ToString(), "0");
                throw ex;
            }
            return bookingResult;
        }
        /// <summary>
        ///Read the Booking response For Booked Hotel
        /// </summary>
        private BookingResponse readBookingResponse(DynamicDataServiceRsp rsp, HotelItinerary itinerary)
        {
            BookingResponse bookingResponse = new BookingResponse();
            try
            {
                HotelBookSegment[] hotelBookSegments = rsp.HotelOrderBookResponse.HotelSegments;
                foreach (HotelBookSegment segment in hotelBookSegments)
                {
                    bookingResponse.ConfirmationNo = (string.IsNullOrEmpty(segment.BookingID) ? string.Empty : segment.BookingID + "|") +
                        (string.IsNullOrEmpty(segment.BookingReference) ? string.Empty : segment.BookingReference + "|") + segment.SegmentId + "|" + segment.OrderId;
                    //string numericPhone = new String(segment.BookingID.Where(Char.IsDigit).ToArray());
                    //string bookingid = numericPhone;
                    bookingResponse.BookingId = Convert.ToInt32(segment.SegmentId);
                    bookingResponse.Status = BookingResponseStatus.Successful;
                    itinerary.Status = HotelBookingStatus.Confirmed;
                    itinerary.ConfirmationNo = string.IsNullOrEmpty(segment.BookingID) ? segment.BookingReference : segment.BookingID;
                    itinerary.BookingRefNo = (string.IsNullOrEmpty(segment.BookingReference) ? string.Empty : segment.BookingReference + "|") + segment.SegmentId + "|" + segment.OrderId;
                    if (segment.Status.ToUpper() != "OK")
                    {
                        bookingResponse.Status = BookingResponseStatus.Failed;
                        Gimmonix.GimmonixService.ServiceRequest Request = new Gimmonix.GimmonixService.ServiceRequest();
                        HotelBookInfoRequest bookRequest = new HotelBookInfoRequest();
                        bookRequest.ClientIP = "185.78.160.133";
                        bookRequest.SegmentID = segment.SegmentId;
                        Credentials credentials = new Credentials();
                        credentials.UserName = userName; //ConfigurationSystem.GimmonixConfig["username"];
                        credentials.Password =passWord;// ConfigurationSystem.GimmonixConfig["password"];

                        DynamicDataServiceRqst dynamicDataServiceRqst = new DynamicDataServiceRqst();
                        dynamicDataServiceRqst.Credentials = credentials;
                        dynamicDataServiceRqst.Request = bookRequest;
                        dynamicDataServiceRqst.RequestType = ServiceRequestType.CheckBookingStatus;
                        dynamicDataServiceRqst.TypeOfService = ServiceType.Hotels;
                        Request.rqst = dynamicDataServiceRqst;
                        //try
                        //{

                        //    XmlSerializer ser = new XmlSerializer(typeof(Gimmonix.GimmonixService.ServiceRequest), new Type[] { typeof(HotelBookInfoRequest) });
                        //    string filePath = xmlPath + sessionId + "_BookInfoRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                        //    StreamWriter sw = new StreamWriter(filePath);
                        //    ser.Serialize(sw, Request);
                        //    sw.Close();
                        //}
                        //catch (Exception ex)
                        //{

                        //}

                        string sData = GenericStatic.SerializeJson(Request.rqst);
                        GenericStatic.GenerateLogs(true, true, ref sData, xmlPath + sessionId + "_BookInfoRequest_");

                        ServiceRequestResponse serviceRequestResponse = client.ServiceRequest(Request);

                        HotelBookInfoResponse response = serviceRequestResponse.ServiceRequestResult.HotelBookInfoResponse;

                        if (response != null)
                        {

                            sData = GenericStatic.SerializeJson(response);
                            GenericStatic.GenerateLogs(true, true, ref sData, xmlPath + sessionId + "_BookInfoResponse_");
                            if (response.Status.ToUpper() != "OK")
                                throw new Exception(segment.BookingRemarks != null && segment.BookingRemarks.Length > 0 ? segment.BookingRemarks[0] : "Booking failed at suppplier side");
                        }

                        if (response == null && serviceRequestResponse.ServiceRequestResult.Errors != null && serviceRequestResponse.ServiceRequestResult.Errors.Length > 0)
                        {
                            sData = GenericStatic.SerializeJson(serviceRequestResponse.ServiceRequestResult.Errors[0]);
                            GenericStatic.GenerateLogs(true, true, ref sData, xmlPath + sessionId + "_BookInfoResponse_");
                            var Errors = serviceRequestResponse.ServiceRequestResult.Errors;
                            throw new Exception("ErrorCode: " + Errors[0].ErrorCode + ", " + "ErrorText: " + Errors[0].ErrorText + ", " +
                                "ErrorMessage: " + Errors[0].Message);
                        }

                        bookingResponse.Status = BookingResponseStatus.Successful;

                        //    try
                        //    {
                        //        string filePath = xmlPath + sessionId + "_BookResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                        //        StreamWriter sw = new StreamWriter(filePath);
                        //    if (response != null)
                        //    {
                        //        XmlSerializer ser = new XmlSerializer(typeof(HotelBookInfoResponse));
                        //        ser.Serialize(sw, response);
                        //        sw.Close();
                        //    }
                        //    else
                        //    {
                        //        XmlSerializer ser = new XmlSerializer(typeof(Error));
                        //        ser.Serialize(sw, serviceRequestResponse.ServiceRequestResult.Errors[0]);
                        //        sw.Close();
                        //    }

                        //}
                        //    catch (Exception ex)
                        //    {

                        //    }

                        //    if (response != null)
                        //    {
                        //        if (response.Status == "OK")
                        //        {
                        //            bookingResponse.Status = BookingResponseStatus.Successful;
                        //            itinerary.Status = HotelBookingStatus.Confirmed;
                        //        }
                        //    }
                    }
                    //else
                    //{
                    //    bookingResponse.Status = BookingResponseStatus.Successful;
                    //    itinerary.Status = HotelBookingStatus.Confirmed;
                    //}

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bookingResponse;
        }
        /// <summary>
        ///BookingRequest Object 
        /// </summary>
        private string GenerateBookingRequest(HotelItinerary itinerary)
        {
            StringBuilder requestMsg = new StringBuilder();
            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("s", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "s", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteStartElement("s", "Body", null);
                writer.WriteStartElement("ServiceRequest", "http://tempuri.org/");
                writer.WriteStartElement("rqst", null);
                writer.WriteAttributeString("xmlns", "i", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteStartElement("Request", "");
                writer.WriteAttributeString("i", "type", null, "HotelBookRequest");
                writer.WriteStartElement("ClientIP", "185.78.160.133");
                writer.WriteAttributeString("i", "nil", null, "true");
                writer.WriteEndElement();//For ClientIP 


                writer.WriteElementString("AgentRemarks", null, "Test Booking");
                writer.WriteStartElement("AgentRoomsRemarks", null);
                writer.WriteAttributeString("xmlns", "a", null, "http://schemas.microsoft.com/2003/10/Serialization/Arrays");
                writer.WriteStartElement("a", "KeyValueOfstringstring", null);
                writer.WriteElementString("a", "key", null, "Test Booking");
                writer.WriteElementString("a", "Value", null, "Test Booking");
                writer.WriteEndElement();//For KeyValueOfstringstring   
                writer.WriteEndElement();//For AgentRoomsRemarks              
                writer.WriteElementString("BookingPrice", null, "546.17");
                writer.WriteElementString("HotelID", null, "4178589");
                writer.WriteElementString("LeadPaxId", null, "8c0c4b3e-afb2-4b31-943b-f314f4ee7eaf");
                writer.WriteElementString("LeadPaxRoomId", null, "5559d3d4-0a16-4dd3-9e5d-164b6ed09274");
                writer.WriteElementString("PackageID", null, "793767cb-f750-4d9e-aa4e-48aca5c98df0");
                writer.WriteStartElement("Passengers", null);
                writer.WriteStartElement("CustomerInfo", null);
                writer.WriteStartElement("Address", null);
                writer.WriteElementString("AddressLine", null, "Address");
                writer.WriteElementString("CityName", null, "City");
                writer.WriteStartElement("CountryName", null);
                writer.WriteElementString("Code", null, "US");
                writer.WriteEndElement();//For CountryName
                writer.WriteElementString("PostalCode", null, "12121231321");
                writer.WriteEndElement();//For Address
                writer.WriteElementString("Allocation", null, "5559d3d4-0a16-4dd3-9e5d-164b6ed09274");
                writer.WriteStartElement("Email", null);
                writer.WriteElementString("Value", null, "qa@travolutionary.com");
                writer.WriteEndElement();//For Email
                writer.WriteElementString("Id", null, "5559d3d4-0a16-4dd3-9e5d-164b6ed09274");
                writer.WriteStartElement("PersonDetails", null);
                writer.WriteStartElement("Name", null);
                writer.WriteElementString("GivenName", null, "FirstName");
                writer.WriteElementString("NamePrefix", null, "Mr");
                writer.WriteElementString("Surname", null, "LastName");
                writer.WriteEndElement();//For Name
                writer.WriteElementString("Type", null, "Adult");
                writer.WriteEndElement();//For PersonDetails
                writer.WriteStartElement("Telephone", null);
                writer.WriteElementString("PhoneNumber", null, "972 50 3301234");
                writer.WriteEndElement();//For Telephone
                writer.WriteEndElement();//For CustomerInfo
                writer.WriteEndElement();//For Telephone
                writer.WriteEndElement();//For Passengers
                writer.WriteElementString("SelectedPaymentMethod", null, "Cash");
                writer.WriteStartElement("ServiceFee", null);
                writer.WriteAttributeString("i", "nil", null, "true");
                writer.WriteEndElement();//For Request               


                writer.WriteStartElement("RequestType", "");
                writer.WriteString("Book");
                writer.WriteEndElement();//For RequestType 
                writer.WriteStartElement("SessionID", "");
                writer.WriteString("/256/128158/D20190401T123607/10d09bcd6e414f3399130a0304c51685");
                writer.WriteEndElement();//For SessionID
                writer.WriteStartElement("TypeOfService", "");
                writer.WriteString("Hotels");
                writer.WriteEndElement();//For TypeOfService
                writer.WriteEndElement();//For rqst 
                writer.WriteEndElement();//For ServiceRequest 
                writer.WriteEndElement();//For s:Body 
                writer.WriteEndElement();//For s:Envelope 
                writer.Flush();
                writer.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return requestMsg.ToString();
        }
        #endregion

        #region Booking Cancel
        /// <summary>
        ///Booking Cancellation 
        /// </summary>
        public Dictionary<string, string> CancelHotelBooking(string segmentid)
        {
            Dictionary<string, string> cancelRes = new Dictionary<string, string>();
            try
            {
                Gimmonix.GimmonixService.ServiceRequest Request = new Gimmonix.GimmonixService.ServiceRequest();
                HotelBookCancelRequest cancelRequest = new HotelBookCancelRequest();
                cancelRequest.ClientIP = "185.78.160.133";
                cancelRequest.SegmentID = Convert.ToInt64(segmentid);
                Credentials credentials = new Credentials();
                credentials.UserName = userName;// ConfigurationSystem.GimmonixConfig["username"];
                credentials.Password = passWord;//ConfigurationSystem.GimmonixConfig["password"];

                DynamicDataServiceRqst dynamicDataServiceRqst = new DynamicDataServiceRqst();
                dynamicDataServiceRqst.Credentials = credentials;
                dynamicDataServiceRqst.Request = cancelRequest;
                dynamicDataServiceRqst.RequestType = ServiceRequestType.CancelBooking;
                dynamicDataServiceRqst.TypeOfService = ServiceType.Hotels;
                Request.rqst = dynamicDataServiceRqst;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(Gimmonix.GimmonixService.ServiceRequest), new Type[] { typeof(HotelBookCancelRequest) });
                    string filePath = xmlPath + segmentid + "_BookingCancelRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, Request);
                    sw.Close();
                }
                catch (Exception ex)
                {

                }
                ServiceRequestResponse serviceRequestResponse = client.ServiceRequest(Request);
                HotelBookCancelResponse response = serviceRequestResponse.ServiceRequestResult.HotelBookCancelResponse;
                try
                {

                    string filePath = xmlPath + segmentid + "_BookingCancelResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    if (response != null)
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(HotelBookCancelResponse));
                        ser.Serialize(sw, response);
                        sw.Close();
                    }
                    else
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Error));
                        ser.Serialize(sw, serviceRequestResponse.ServiceRequestResult.Errors[0]);
                        sw.Close();
                    }

                }
                catch (Exception ex)
                {

                }
                if (response != null)
                {
                    if (response.Status == "CX")
                    {
                        response.BookCancelID = !string.IsNullOrEmpty(response.BookCancelID) ? response.BookCancelID : response.Status + segmentid;
                        cancelRes.Add("Status", "Cancelled");
                        cancelRes.Add("ID", response.BookCancelID);
                        cancelRes.Add("Amount", Convert.ToString(response.CancellationFee));
                        cancelRes.Add("Currency", response.Currency);
                    }
                    else
                    {
                        cancelRes.Add("Status", response.Status);
                    }

                }
                else
                {
                    cancelRes.Add("Status", "FAILED");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GimmonixBookCancel, Severity.Normal, 1, "Gimmoinix.CancelHotelBooking Err :" + ex.ToString(), "0");
                throw ex;
            }

            return cancelRes;
        }
        #endregion
    }
}
