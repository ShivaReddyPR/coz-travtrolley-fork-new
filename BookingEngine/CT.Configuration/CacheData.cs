using System.Collections;
using System.Collections.Generic;


namespace CT.Configuration
{
    /// <summary>
    /// This class is used for Cacheing Data to reduce number of hits to Data base
    /// </summary>
    public class CacheData
    {
     // These variables are for Cacheing the data.
        public static Dictionary<string, List<string>> AirportList = new Dictionary<string, List<string>>();
        //string contains airline code list contians 1. airport Name 2.city code 3.country code.
        public static Dictionary<string, string> CityList = new Dictionary<string, string>();
        //it contains city code and city name.
        public static Dictionary<string, string> SeatList = new Dictionary<string, string>();
        //It contains seat Code and Seat description
        public static Dictionary<string, string> CountryList = new Dictionary<string, string>();
        //It contains country code and country Name
        public static Dictionary<string, ArrayList> AirlineList = new Dictionary<string, ArrayList>();
        public static Dictionary<string, List<string>> CityCodeList = new Dictionary<string, List<string>>();
        //string contains city code and List contains 1.airline code 
        public static Dictionary<int, Dictionary<string, string>> MealList = new Dictionary<int, Dictionary<string, string>>();
        public static Dictionary<int, string> MemberList = null;
        public static SortedList ServiceRequestStatus = null;

        public static Dictionary<string, string> SectorsList = new Dictionary<string, string>();//Added By Suresh V
        //It contains meal code and Meal description.
         /// <summary>
        /// checks whether the Cache contains airportcode.
        /// </summary>
        /// <param name="airportCode"></param>
        /// <returns></returns>
        public static  bool CheckAirportLoad(string airportCode)
        {
           if(AirportList.ContainsKey(airportCode.ToUpper()))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Check whether the Cache contains member.
        /// </summary>
        /// <returns></returns>
        public static bool CheckMemberList()
        {
            if (MemberList.Count>0 && MemberList!=null)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check whether the Cache contains servic request status.
        /// </summary>
        /// <returns></returns>
        public static bool CheckServiceRequestStatus()
        {
            if(ServiceRequestStatus!=null)
            {
                return true;
            }
            return false;
        }
       
        /// <summary>
        /// checks whether the Cache contains citycode in cityList.
        /// </summary>
        /// <param name="citycode"></param>
        /// <returns></returns>
        public static bool CheckGetCityName(string citycode)
        {
            if (CityList.ContainsKey(citycode.ToUpper()))
            {
                return true;
            }
            return false;
        }
   
        /// <summary>
        /// checks whether the Cache contains citycode in cityCode List.
        /// </summary>
        /// <param name="citycode"></param>
        /// <returns></returns>
        public static bool CheckGetAirportCode(string citycode)
        {
            if (CityCodeList.ContainsKey(citycode.ToUpper()))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// checks  whether the Cache contains airlinecode in airlineList.
        /// </summary>
        /// <param name="airlineCode"></param>
        /// <returns></returns>

        public static bool CheckAirlineLoad(string airlineCode)
        {
            if (AirlineList.ContainsKey(airlineCode.ToUpper()))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        ///  checks whether the Cache contains meal Details or not.
        /// </summary>
        /// <returns></returns>

        public static bool CheckGetMealPreferenceList()
        {
            if (MealList.Count > 0)
            {
                return true;
            }
            return false;
        }

        public static bool CheckSectorList() //Added By Suresh V
        {
            if (SectorsList.Count > 0)
                return true;
            else
                return false; 
        }
        /// <summary>
        /// checks  whether  the Cache contains Seat details or not.
        /// </summary>
        /// <returns></returns>

        public static bool CheckGetSeatPreferenceList()
        {
            if (SeatList.Count > 0)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// checks  whether  the Cache contains countrycode in countryList.
        /// </summary>
        /// <param name="countrycode"></param>
        /// <returns></returns>
        public static bool CheckCountryName(string countrycode)
        {
            if (CountryList.ContainsKey(countrycode.ToUpper()))
            {
                return true;
            }
            return false;

        }
        
    }
}
