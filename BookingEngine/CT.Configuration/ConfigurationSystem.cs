using System;
using System.Collections.Generic;
using System.Xml;
using System.Collections;
using CT.MemCache;

namespace CT.Configuration
{
    //TODO: Make a static hash table.
    public class ConfigurationSystem
    {
        // path of config files
        static string configPath = System.Configuration.ConfigurationManager.AppSettings["configFiles"];
        static bool isMemCacheEnabled = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["MemCacheEnabled"]);
        private static string xslconfigPath = System.Configuration.ConfigurationManager.AppSettings["xslFiles"];

        public static string XslconfigPath { get { return xslconfigPath; } }
        //for test - configFilesTest (TODO: Remove if not for test release)
        //static string configPath = "C:\\configFiles\\";
        //for test - configFilesTest (TODO: Remove if not for test release)
        //static string configPath = "C:\\configFiles\\";

        public Hashtable NetWorkConncetion()// for Tempporary
        {
            string applicationName = Convert.ToString("DataAccess");

            string applicationPath = configPath + applicationName;

            XmlDocument xmlDoc = new XmlDocument();
            Hashtable myHash = new Hashtable();

            xmlDoc.Load(applicationPath + ".config.xml");
            XmlNode node = xmlDoc.ChildNodes[1];

            if (node.HasChildNodes)
            {
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    myHash.Add(node.ChildNodes[i].Name, node.ChildNodes[i].InnerText);
                }
            }

            return myHash;
        }

        public Hashtable getApplicationPath()
        {
            string applicationName = Convert.ToString(System.Reflection.Assembly.GetCallingAssembly().GetName().Name);

            string applicationPath = configPath + applicationName;

            XmlDocument xmlDoc = new XmlDocument();
            Hashtable myHash = new Hashtable();

            xmlDoc.Load(applicationPath + ".config.xml");
            XmlNode node = xmlDoc.ChildNodes[1];

            if (node.HasChildNodes)
            {
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    myHash.Add(node.ChildNodes[i].Name, node.ChildNodes[i].InnerText);
                }
            }

            return myHash;
        }
        /// <summary>
        /// Method return Host and port from Config files in C:\\ConfigFiles HostPort.config.xml
        /// </summary>
        /// <returns></returns>
        public Hashtable GetHostPort()
        {
            XmlDocument xmlDoc = new XmlDocument();
            Hashtable myHash = new Hashtable();
            xmlDoc.Load(configPath + "HostPort.config.xml");
            XmlNode node = xmlDoc.ChildNodes[1];
            if (node.HasChildNodes)
            {
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    myHash.Add(node.ChildNodes[i].Name, node.ChildNodes[i].InnerText);
                }
            }
            return myHash;
        }
        public Hashtable GetSelfAgencyId()
        {
            XmlDocument xmlDoc = new XmlDocument();
            Hashtable myHash = new Hashtable();
            xmlDoc.Load(configPath + "AccountingEngine.config.xml");
            XmlNode node = xmlDoc.ChildNodes[1];
            if (node.HasChildNodes)
            {
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    myHash.Add(node.ChildNodes[i].Name, node.ChildNodes[i].InnerText);
                }
            }
            return myHash;
        }

        //TODO: The config system is to be made generic accordingly.
        /// <summary>
        /// bookingEngine configuration entries.
        /// </summary>
        private static Dictionary<string, string> bookingEngineConfig = null;
        /// <summary>
        /// Gets the dictionary containing bookingEngineConfig configuration.
        /// </summary>
        public static Dictionary<string, string> BookingEngineConfig
        {
            get
            {
                if (!ShouldLoadFromFile(bookingEngineConfig, MemCacheKeys.BookingEngineConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(bookingEngineConfig, MemCacheKeys.BookingEngineConfig);
                }
                else
                {
                    bookingEngineConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "BookingEngine.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList beData = config.ChildNodes;
                    for (int i = 0; i < beData.Count; i++)
                    {
                        bookingEngineConfig.Add(beData[i].Name, beData[i].InnerText);
                    }
                    AddInMemCache(bookingEngineConfig, MemCacheKeys.BookingEngineConfig);
                    return bookingEngineConfig;
                }
            }
        }



        //TODO: The config system is to be made generic accordingly.
        /// <summary>
        /// Worldspan configuration entries.
        /// </summary>
        private static Dictionary<string, string> worldspanConfig = null;
        /// <summary>
        /// Gets the dictionary containing worldspan configuration.
        /// </summary>
        public static Dictionary<string, string> WorldspanConfig
        {
            get
            {
                if (!ShouldLoadFromFile(worldspanConfig, MemCacheKeys.WorldspanConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(worldspanConfig, MemCacheKeys.WorldspanConfig);
                }
                else
                {
                    worldspanConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Worldspan.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList wspanData = config.ChildNodes;
                    for (int i = 0; i < wspanData.Count; i++)
                    {
                        worldspanConfig.Add(wspanData[i].Name, wspanData[i].InnerXml);
                    }
                    AddInMemCache(worldspanConfig, MemCacheKeys.WorldspanConfig);
                    return worldspanConfig;
                }
            }
        }

        //TODO: The config system is to be made generic accordingly.
        /// <summary>
        /// Worldspan configuration entries.
        /// </summary>
        private static Dictionary<string, string> amadeusConfig = null;
        /// <summary>
        /// Gets the dictionary containing Amadeus configuration.
        /// </summary>
        public static Dictionary<string, string> AmadeusConfig
        {
            get
            {
                if (!ShouldLoadFromFile(amadeusConfig, MemCacheKeys.AmadeusConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(amadeusConfig, MemCacheKeys.AmadeusConfig);
                }
                else
                {
                    amadeusConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Amadeus.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList amData = config.ChildNodes;
                    for (int i = 0; i < amData.Count; i++)
                    {
                        amadeusConfig.Add(amData[i].Name, amData[i].InnerXml);
                    }
                    AddInMemCache(amadeusConfig, MemCacheKeys.AmadeusConfig);
                    return amadeusConfig;
                }
            }
        }

        /// <summary>
        /// Method return records per page for booking queue from Config files in C:\\ConfigFiles recordsPerPageForBookingQueue.config.xml
        /// </summary>
        /// <returns></returns>
        public Hashtable GetRecordsPerPageForBookingQueue()
        {
            XmlDocument xmlDoc = new XmlDocument();
            Hashtable myHash = new Hashtable();
            xmlDoc.Load(configPath + "recordsPerPageForBookingQueue.config.xml");
            XmlNode node = xmlDoc.ChildNodes[1];
            if (node.HasChildNodes)
            {
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    myHash.Add(node.Name, node.ChildNodes[i].InnerText);
                }
            }
            return myHash;
        }

        /// <summary>
        /// Method returns show how many page nos on a page from Config files in C:\\ConfigFiles showPageNos.config.xml
        /// </summary>
        /// <returns></returns>
        public Hashtable GetShowPageNos()
        {
            XmlDocument xmlDoc = new XmlDocument();
            Hashtable myHash = new Hashtable();
            xmlDoc.Load(configPath + "showPageNos.config.xml");
            XmlNode node = xmlDoc.ChildNodes[1];
            if (node.HasChildNodes)
            {
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    myHash.Add(node.Name, node.ChildNodes[i].InnerText);
                }
            }
            return myHash;
        }


        /// <summary>
        /// Paging configuration entries.
        /// </summary>
        private static Dictionary<string, string> pagingConfig = null;
        /// <summary>
        /// Gets the dictionary containing worldspan configuration.
        /// </summary>
        public static Dictionary<string, string> PagingConfig
        {
            get
            {
                if (!ShouldLoadFromFile(pagingConfig, MemCacheKeys.PagingConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(pagingConfig, MemCacheKeys.PagingConfig);
                }
                else
                {
                    pagingConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "paging.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList pagingData = config.ChildNodes;
                    for (int i = 0; i < pagingData.Count; i++)
                    {
                        pagingConfig.Add(pagingData[i].Name, pagingData[i].InnerXml);
                    }
                    AddInMemCache(pagingConfig, MemCacheKeys.PagingConfig);
                    //MemChached.Cache.Add("pagingConfig", pagingConfig);
                    return pagingConfig;
                }
            }
        }

        private static Dictionary<string, string> core = null;

        public static Dictionary<string, string> Core
        {
            get
            {
                if (!ShouldLoadFromFile(core, MemCacheKeys.CoreConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(core, MemCacheKeys.CoreConfig);
                }
                else
                {
                    core = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "core.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList data = config.ChildNodes;
                    for (int i = 0; i < data.Count; i++)
                    {
                        core.Add(data[i].Name, data[i].InnerXml);
                    }
                    AddInMemCache(core, MemCacheKeys.CoreConfig);
                    return ConfigurationSystem.core;
                }
            }
        }
        private static Dictionary<string, string> GetNoOfRecord = null;

        public static Dictionary<string, string> MobileConfig
        {
            get
            {
                if (!ShouldLoadFromFile(GetNoOfRecord, MemCacheKeys.MobileConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(GetNoOfRecord, MemCacheKeys.MobileConfig);
                }
                else
                {
                    GetNoOfRecord = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Mobile.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList data = config.ChildNodes;
                    for (int i = 0; i < data.Count; i++)
                    {
                        GetNoOfRecord.Add(data[i].Name, data[i].InnerXml);
                    }
                    AddInMemCache(GetNoOfRecord, MemCacheKeys.MobileConfig);
                    return ConfigurationSystem.MobileConfig;
                }
            }
        }

        /// <summary>
        /// Paging configuration entries.
        /// </summary>
        private static Dictionary<string, string> activeSources = null;

        /// <summary>
        /// Get the active sources with their time out
        /// </summary>
        public static Dictionary<string, string> ActiveSources
        {
            get
            {
                if (!ShouldLoadFromFile(activeSources, MemCacheKeys.ActiveSourcesConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(activeSources, MemCacheKeys.ActiveSourcesConfig);
                }
                else
                {
                    XmlNode node;
                    activeSources = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "activeSources.config.xml");
                    XmlNodeList sourceList = xmlDoc.SelectNodes("config/source");
                    int defaultTimeOut = Convert.ToInt32(xmlDoc.SelectSingleNode("config/defaulttimeout").InnerText);
                    foreach (XmlNode source in sourceList)
                    {
                        if (Convert.ToInt32(source.SelectSingleNode("active").InnerText) != 0)
                        {
                            node = source.SelectSingleNode("name");
                            if (node != null && node.InnerText.Length != 0)
                            {
                                if (source.SelectSingleNode("timeout") != null && source.SelectSingleNode("timeout").InnerText.Length != 0)
                                {
                                    activeSources.Add(node.InnerText, source.SelectSingleNode("type").InnerText + "," + source.SelectSingleNode("timeout").InnerText);
                                }
                                else
                                {
                                    activeSources.Add(node.InnerText, source.SelectSingleNode("type").InnerText + "," + defaultTimeOut);
                                }
                            }
                        }
                    }
                    AddInMemCache(activeSources, MemCacheKeys.ActiveSourcesConfig);
                    return activeSources;
                }
            }
        }
        /// <summary>
        /// Paging configuration entries.
        /// </summary>
        private static int mailingTime;

        /// <summary>
        /// Get the active sources with their time out
        /// </summary>
        public static int MailingTime
        {
            get
            {
                if (mailingTime != 0)
                {
                    return mailingTime;
                }
                else
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "BookingAPI.config.xml");
                    mailingTime = Convert.ToInt32(xmlDoc.SelectSingleNode("config/mailingTime").InnerText);
                    return mailingTime;
                }
            }
        }


        private static Dictionary<string, int> resultCountBySources = null;

        /// <summary>
        /// Get the no of result needed
        /// </summary>
        public static Dictionary<string, int> ResultCountBySources
        {
            get
            {
                if (!ShouldLoadFromFile(resultCountBySources, MemCacheKeys.ResultCountBySourcesConfig))
                {
                    return (Dictionary<string, int>)GetStaticData(resultCountBySources, MemCacheKeys.ResultCountBySourcesConfig);
                }
                else
                {
                    XmlNode node;
                    resultCountBySources = new Dictionary<string, int>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "activeSources.config.xml");
                    XmlNodeList sourceList = xmlDoc.SelectNodes("config/source");
                    int showResultCount = Convert.ToInt32(xmlDoc.SelectSingleNode("config/showresultcount").InnerText);
                    foreach (XmlNode source in sourceList)
                    {
                        if (Convert.ToInt32(source.SelectSingleNode("active").InnerText) != 0)
                        {
                            node = source.SelectSingleNode("name");
                            if (node != null && node.InnerText.Length != 0)
                            {
                                if (source.SelectSingleNode("showResCount") != null && source.SelectSingleNode("showResCount").InnerText.Length != 0)
                                {
                                    resultCountBySources.Add(node.InnerText, Convert.ToInt32(source.SelectSingleNode("showResCount").InnerText));
                                }
                                else
                                {
                                    resultCountBySources.Add(node.InnerText, showResultCount);
                                }
                            }
                        }
                    }
                    AddInMemCache(resultCountBySources, MemCacheKeys.ResultCountBySourcesConfig);
                    return resultCountBySources;
                }
            }
        }

        private static Dictionary<string, string> getImagePath = null;
        /// <summary>
        /// To retrieve the path for storing images in CMS
        /// </summary>
        public static Dictionary<string, string> CMSImageConfig
        {
            get
            {
                if (!ShouldLoadFromFile(getImagePath, MemCacheKeys.CMSImageConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(getImagePath, MemCacheKeys.CMSImageConfig);
                }
                else
                {
                    getImagePath = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "CMSImage.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    if (config != null)
                    {
                        XmlNodeList data = config.ChildNodes;
                        for (int i = 0; i < data.Count; i++)
                        {
                            getImagePath.Add(data[i].Name, data[i].InnerXml);
                        }
                        AddInMemCache(getImagePath, MemCacheKeys.CMSImageConfig);
                        return ConfigurationSystem.getImagePath;

                    }
                    else
                    {
                        return ConfigurationSystem.getImagePath;
                    }

                }
            }
        }

        /// <summary>
        /// Total result count showing on search result page
        /// </summary>
        private static int resultCount = 0;

        /// <summary>
        /// Total result count that should be shown on search result page
        ///  
        /// </summary>
        public static int ResultCount
        {
            get
            {
                if (resultCount != 0)
                {
                    return resultCount;
                }
                else
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "activeSources.config.xml");
                    resultCount = Convert.ToInt32(xmlDoc.SelectSingleNode("config/showresultcount").InnerText);
                    return resultCount;
                }
            }
        }

        /// <summary>
        /// Spice Jet configuration entries.
        /// </summary>
        private static Dictionary<string, string> spiceJetConfig = null;

        /// <summary>
        /// Gets the dictionary containing Spice Jet configuration.
        /// </summary>
        public static Dictionary<string, string> SpiceJetConfig
        {
            get
            {
                if (!ShouldLoadFromFile(spiceJetConfig, MemCacheKeys.SpiceJetConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(spiceJetConfig, MemCacheKeys.SpiceJetConfig);
                }
                else
                {
                    spiceJetConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "SpiceJet.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList sjData = config.ChildNodes;
                    for (int i = 0; i < sjData.Count; i++)
                    {
                        spiceJetConfig.Add(sjData[i].Name, sjData[i].InnerXml);
                    }
                    AddInMemCache(spiceJetConfig, MemCacheKeys.SpiceJetConfig);
                    return spiceJetConfig;
                }
            }
        }

        /// <summary>
        ///Configuration entries for switching on/off logging.
        /// </summary>
        private static Dictionary<string, string> auditConfig = null;

        /// <summary>
        /// Gets the dictionary containing Audit configuration.
        /// </summary>
        public static Dictionary<string, string> AuditConfig
        {
            get
            {
                if (!ShouldLoadFromFile(auditConfig, MemCacheKeys.AuditConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(auditConfig, MemCacheKeys.AuditConfig);
                }
                else
                {
                    auditConfig = new Dictionary<string, string>();
                    lock (auditConfig)
                    {

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.Load(configPath + "Audit.config.xml");
                        XmlNode config = xmlDoc.SelectSingleNode("config");
                        XmlNodeList data = config.ChildNodes;
                        for (int i = 0; i < data.Count; i++)
                        {
                            auditConfig.Add(data[i].Name, data[i].InnerXml);
                        }
                        AddInMemCache(auditConfig, MemCacheKeys.AuditConfig);
                    }
                    return auditConfig;
                }
            }
        }

        /// <summary>
        /// Navitaire configuration entries.
        /// </summary>
        private static Dictionary<string, Dictionary<string, string>> navitaireConfig = null;

        /// <summary>
        /// Gets the dictionary containing Navitaire configuration.
        /// </summary>
        public static Dictionary<string, Dictionary<string, string>> NavitaireConfig
        {
            get
            {
                if (!ShouldLoadFromFile(navitaireConfig, MemCacheKeys.NavitaireConfig))
                {
                    return (Dictionary<string, Dictionary<string, string>>)GetStaticData(navitaireConfig, MemCacheKeys.NavitaireConfig);
                }
                else
                {
                    navitaireConfig = new Dictionary<string, Dictionary<string, string>>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Navitaire.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList sjData = config.ChildNodes;
                    for (int i = 0; i < sjData.Count; i++)
                    {
                        Dictionary<string, string> sourceWiseData = new Dictionary<string, string>();
                        XmlNodeList sourceData = sjData[i].ChildNodes;
                        for (int j = 0; j < sourceData.Count; j++)
                        {
                            sourceWiseData.Add(sourceData[j].Name, sourceData[j].InnerXml);
                        }
                        navitaireConfig.Add((sjData[i].Name.Split('-'))[1], sourceWiseData);
                    }
                    AddInMemCache(navitaireConfig, MemCacheKeys.NavitaireConfig);
                    return navitaireConfig;
                }
            }
        }


        /// <summary>
        /// Active sources configuration entries. -TODO: remove if not in use
        /// </summary>
        private static string testModeConfig = null;

        /// <summary>
        /// Gets the dictionary containing worldspan configuration.
        /// </summary>
        public static string TestModeConfig
        {
            get
            {
                if (!ShouldLoadFromFile(testModeConfig, MemCacheKeys.TestModeConfig))
                {
                    return (string)GetStaticData(testModeConfig, MemCacheKeys.TestModeConfig);
                }
                else
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "activeSources.config.xml");
                    testModeConfig = xmlDoc.SelectSingleNode("config/testMode").InnerText;
                    AddInMemCache(testModeConfig, MemCacheKeys.TestModeConfig);
                    return testModeConfig;
                }
            }
        }
        private static Dictionary<string, string> robotId = null;

        public static Dictionary<string, string> RobotId
        {
            get
            {
                if (!ShouldLoadFromFile(robotId, MemCacheKeys.RobotIdConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(robotId, MemCacheKeys.RobotIdConfig);
                }
                else
                {
                    robotId = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Robot.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList data = config.ChildNodes;
                    for (int i = 0; i < data.Count; i++)
                    {
                        robotId.Add(data[i].Name, data[i].InnerXml);
                    }
                    AddInMemCache(robotId, MemCacheKeys.RobotIdConfig);
                    return robotId;
                }
            }
        }

        /// <summary>
        /// Worldspan configuration entries.
        /// </summary>
        private static Dictionary<string, string> email = null;
        /// <summary>
        /// Gets the dictionary containing worldspan configuration.
        /// </summary>
        public static Dictionary<string, string> Email
        {
            get
            {
                if (!ShouldLoadFromFile(email, MemCacheKeys.EmailConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(email, MemCacheKeys.EmailConfig);
                }
                else
                {
                    email = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Email.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList data = config.ChildNodes;
                    for (int i = 0; i < data.Count; i++)
                    {
                        if (!email.ContainsKey(data[i].Name))
                        {
                            email.Add(data[i].Name, data[i].InnerXml);
                        }
                    }
                    AddInMemCache(email, MemCacheKeys.EmailConfig);
                    return email;
                }
            }
        }

        /// <summary>
        /// Worldspan configuration entries.
        /// </summary>
        private static Dictionary<string, bool> apiSources = null;
        /// <summary>
        /// Gets the dictionary containing worldspan configuration.
        /// </summary>
        public static Dictionary<string, bool> ApiSources
        {
            get
            {
                if (!ShouldLoadFromFile(apiSources, MemCacheKeys.ApiSourcesConfig))
                {
                    return (Dictionary<string, bool>)GetStaticData(apiSources, MemCacheKeys.ApiSourcesConfig);
                }
                else
                {
                    apiSources = new Dictionary<string, bool>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "BookingAPI.config.xml");
                    XmlNodeList data = xmlDoc.SelectNodes("/config/source");
                    for (int i = 0; i < data.Count; i++)
                    {
                        XmlNode nameNode = data[i].SelectSingleNode("name");
                        XmlNode activeNode = data[i].SelectSingleNode("active");
                        if (nameNode != null && activeNode != null)
                        {
                            apiSources.Add(nameNode.InnerText, Convert.ToBoolean(activeNode.InnerText));
                        }
                    }
                    AddInMemCache(apiSources, MemCacheKeys.ApiSourcesConfig);
                    return apiSources;
                }
            }
        }

        /// <summary>
        /// Indigo configuration entries.
        /// </summary>
        private static Dictionary<string, string> indigoConfig = null;

        /// <summary>
        /// Gets the dictionary containing Indigo configuration.
        /// </summary>
        public static Dictionary<string, string> IndigoConfig
        {
            get
            {
                if (!ShouldLoadFromFile(indigoConfig, MemCacheKeys.IndigoConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(indigoConfig, MemCacheKeys.IndigoConfig);
                }
                else
                {
                    indigoConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Indigo.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList inData = config.ChildNodes;
                    for (int i = 0; i < inData.Count; i++)
                    {
                        indigoConfig.Add(inData[i].Name, inData[i].InnerXml);
                    }
                    AddInMemCache(indigoConfig, MemCacheKeys.IndigoConfig);
                    return indigoConfig;
                }
            }
        }

        /// <summary>
        /// Paramount configuration entries.
        /// </summary>
        private static Dictionary<string, string> paramountConfig = null;

        /// <summary>
        /// Gets the dictionary containing Indigo configuration.
        /// </summary>
        public static Dictionary<string, string> ParamountConfig
        {
            get
            {
                if (!ShouldLoadFromFile(paramountConfig, MemCacheKeys.ParamountConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(paramountConfig, MemCacheKeys.ParamountConfig);
                }
                else
                {
                    paramountConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Paramount.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList inData = config.ChildNodes;
                    for (int i = 0; i < inData.Count; i++)
                    {
                        paramountConfig.Add(inData[i].Name, inData[i].InnerXml);
                    }
                    AddInMemCache(paramountConfig, MemCacheKeys.ParamountConfig);
                    return paramountConfig;
                }
            }
        }
        /// <summary>
        /// Update given XML File
        /// </summary>
        /// <param name="fileName"></param>
        public static void UpdateVariable(string fileName)
        {
            switch (fileName)
            {
                case "Worldspan.config.xml":
                    worldspanConfig = null;
                    RemoveFromMemCache(MemCacheKeys.WorldspanConfig);
                    break;
                case "Paging.config.xml":
                    pagingConfig = null;
                    RemoveFromMemCache(MemCacheKeys.PagingConfig);
                    break;
                case "Core.config.xml":
                    core = null;
                    defaultCategory = null;
                    RemoveFromMemCache(MemCacheKeys.CoreConfig);
                    RemoveFromMemCache(MemCacheKeys.DefaultCategoryConfig);
                    //core = null;
                    //defaultCategory = null;
                    break;
                case "Email.config.xml":
                    email = null;
                    RemoveFromMemCache(MemCacheKeys.EmailConfig);
                    //email = null;
                    break;
                case "Mobile.config.xml":
                    GetNoOfRecord = null;
                    RemoveFromMemCache(MemCacheKeys.MobileConfig);
                    //GetNoOfRecord = null;
                    break;
                case "ActiveSources.config.xml":
                    activeSources = null;
                    resultCountBySources = null;
                    resultCount = 0;
                    testModeConfig = null;
                    RemoveFromMemCache(MemCacheKeys.ActiveSourcesConfig);
                    RemoveFromMemCache(MemCacheKeys.ResultCountBySourcesConfig);
                    RemoveFromMemCache(MemCacheKeys.ResultCountBySourcesConfig);
                    RemoveFromMemCache(MemCacheKeys.TestModeConfig);
                    //activeSources = null;
                    //resultCountBySources = null;
                    //resultCount = 0;
                    //testModeConfig = null;
                    break;
                case "CMSImage.config.xml":
                    getImagePath = null;
                    RemoveFromMemCache(MemCacheKeys.CMSImageConfig);
                    //getImagePath = null;
                    break;
                case "SpiceJet.config.xml":
                    spiceJetConfig = null;
                    RemoveFromMemCache(MemCacheKeys.SpiceJetConfig);
                    //spiceJetConfig = null;
                    break;
                case "Audit.config.xml":
                    auditConfig = null;
                    RemoveFromMemCache(MemCacheKeys.AuditConfig);
                    //auditConfig = null;
                    break;
                case "Navitaire.config.xml":
                    navitaireConfig = null;
                    RemoveFromMemCache(MemCacheKeys.NavitaireConfig);
                    //navitaireConfig = null;
                    break;
                case "Robot.config.xml":
                    robotId = null;
                    RemoveFromMemCache(MemCacheKeys.RobotIdConfig);
                    //robotId = null;
                    break;
                case "Indigo.config.xml":
                    indigoConfig = null;
                    RemoveFromMemCache(MemCacheKeys.IndigoConfig);
                    //indigoConfig = null;
                    break;
                case "Paramount.config.xml":
                    paramountConfig = null;
                    RemoveFromMemCache(MemCacheKeys.ParamountConfig);
                    //paramountConfig = null;
                    break;
                case "AirDeccan.config.xml":
                    airDeccanConfig = null;
                    RemoveFromMemCache(MemCacheKeys.AirDeccanConfig);
                    //airDeccanConfig = null;
                    break;
                case "Mdlr.config.xml":
                    mdlrConfig = null;
                    RemoveFromMemCache(MemCacheKeys.MdlrConfig);
                    //mdlrConfig = null;
                    break;
                case "BookingEngine.config.xml":
                    allowedTitles = null;
                    bookingEngineConfig = null;
                    nonCommisionableTaxConfig = null;
                    RemoveFromMemCache(MemCacheKeys.AllowedTitlesConfig);
                    RemoveFromMemCache(MemCacheKeys.BookingEngineConfig);
                    RemoveFromMemCache(MemCacheKeys.NonCommisionableTaxConfig);
                    //allowedTitles = null;
                    //bookingEngineConfig = null;
                    //nonCommisionableTaxConfig = null;
                    break;
                case "GoAir.config.xml":
                    goAirConfig = null;
                    RemoveFromMemCache(MemCacheKeys.GoAirConfig);
                    //goAirConfig = null;
                    break;
                case "ICICI.config.xml":
                    iciciConfig = null;
                    RemoveFromMemCache(MemCacheKeys.ICICIConfig);
                    //iciciConfig = null;
                    break;
                case "Desiya.config.xml":
                    desiyaConfig = null;
                    RemoveFromMemCache(MemCacheKeys.DesiyaConfig);
                    //desiyaConfig = null;
                    break;
                case "DesiyaCreditCard.config.xml":
                    desiyaCreditConfig = null;
                    RemoveFromMemCache(MemCacheKeys.DesiyaCreditConfig);
                    //desiyaCreditConfig = null;
                    break;
                case "GTA.config.xml":
                    GTAconfig = null;
                    RemoveFromMemCache(MemCacheKeys.GTAConfig);
                    //GTAconfig = null;
                    break;
                case "TBOSpecial.config.xml":
                    TBOHotelconfig = null;
                    RemoveFromMemCache(MemCacheKeys.TBOHotelConfig);
                    //TBOSplconfig = null;
                    break;
                case "HotelBeds.config.xml":
                    hotelBedsConfig = null;
                    RemoveFromMemCache(MemCacheKeys.HotelBedsConfig);
                    //hotelBedsConfig = null;
                    break;
                case "Galileo.config.xml":
                    galileoConfig = null;
                    RemoveFromMemCache(MemCacheKeys.GalileoConfig);
                    //galileoConfig = null;
                    break;
                case "UAPI.config.xml":
                    uapiConfig = null;
                    RemoveFromMemCache(MemCacheKeys.UAPIConfig);
                    //galileoConfig = null;
                    break;
                case "Amadeus.config.xml":
                    amadeusConfig = null;
                    RemoveFromMemCache(MemCacheKeys.AmadeusConfig);
                    //amadeusConfig = null;
                    break;
                case "BookingAPI.config.xml":
                    apiSources = null;
                    RemoveFromMemCache(MemCacheKeys.ApiSourcesConfig);
                    //apiSources = null;
                    //mailingTime = 0;
                    break;
                case "Promotion.config.xml":
                    promotionConfig = null;
                    RemoveFromMemCache(MemCacheKeys.PromotionConfig);
                    //promotionConfig = null;
                    break;
                case "Sama.config.xml":
                    samaConfig = null;
                    RemoveFromMemCache(MemCacheKeys.SamaConfig);
                    //samaConfig = null;
                    break;
                case "SamaCity.config.xml":
                    samaCityConfig = null;
                    RemoveFromMemCache(MemCacheKeys.SamaCityConfig);
                    //samaCityConfig = null;
                    break;
                case "Tourico.config.xml":
                    touricoConfig = null;
                    RemoveFromMemCache(MemCacheKeys.TouricoConfig);
                    //touricoConfig = null;
                    break;
                case "IAN.config.xml":
                    ianConfig = null;
                    RemoveFromMemCache(MemCacheKeys.IANConfig);
                    //ianConfig = null;
                    break;
                case "IndiaTimes.config.xml":
                    b2cSettingsConfig = null;
                    RemoveFromMemCache(MemCacheKeys.B2CSettingsConfig);
                    //indiaTimesConfig = null;
                    break;
                case "Hermes.config.xml":
                    hermesConfig = null;
                    RemoveFromMemCache(MemCacheKeys.HermesConfig);
                    //hermesConfig = null;
                    break;
                case "SectorList.config.xml":
                    sectorListConfig = null;
                    RemoveFromMemCache(MemCacheKeys.SectorListConfig);
                    //sectorListConfig = null;
                    break;
                case "SMSConfig.xml":
                    smsConfig = null;
                    RemoveFromMemCache(MemCacheKeys.SmsConfig);
                    //smsConfig = null;
                    break;
                case "StatusList.config.xml":
                    statusListConfig = null;
                    RemoveFromMemCache(MemCacheKeys.StatusListConfig);
                    //statusListConfig = null;
                    break;
                case "IRCTCConfig.xml":
                    irctcConfig = null;
                    RemoveFromMemCache(MemCacheKeys.IRCTCConfig);
                    //irctcConfig = null;
                    break;
                case "CreditCard.config.xml":
                    creditCardConfig = null;
                    RemoveFromMemCache(MemCacheKeys.CreditCardConfig);
                    //creditCardConfig = null;
                    break;
                case "ITRed.config.xml":
                    itRedConfig = null;
                    RemoveFromMemCache(MemCacheKeys.ITRedConfig);
                    //itRedConfig = null;
                    break;
                case "Miki.config.xml":
                    Mikiconfig = null;
                    RemoveFromMemCache(MemCacheKeys.MikiConfig);
                    //Mikiconfig= null;
                    break;
                case "Travco.config.xml":
                    Travcoconfig = null;
                    RemoveFromMemCache(MemCacheKeys.TravcoConfig);
                    //Travcoconfig = null;
                    break;
                case "ReportingServicesConfig.config.xml":
                    reportingServicesConfig = null;
                    RemoveFromMemCache(MemCacheKeys.ReportingServicesConfig);
                    //Travcoconfig = null;
                    break;
                case "IffcoInsurance.config.xml":
                    RemoveFromMemCache(MemCacheKeys.IffcoInsuranceConfig);
                    break;
                case "HotelConnect.config.xml":
                    hotelConnectConfig = null;
                    RemoveFromMemCache(MemCacheKeys.HotelConnectConfig);
                    break;
                case "DOTW.config.xml":
                    DOTWconfig = null;
                    RemoveFromMemCache(MemCacheKeys.DOTWConfig);
                    break;
                case "WST.config.xml":
                    WSTconfig = null;
                    RemoveFromMemCache(MemCacheKeys.WSTConfig);
                    break;
                case "PayworldConfig.config.xml":
                    payWorldConfig = null;
                    RemoveFromMemCache(MemCacheKeys.PayWorldConfig);
                    break;
                case "Locale.config.xml":
                    localeConfig = null;
                    RemoveFromMemCache(MemCacheKeys.LocaleConfig);
                    break;
            }
        }

        /// <summary>
        /// Air Deccan configuration entries.
        /// </summary>
        private static Dictionary<string, string> airDeccanConfig = null;

        /// <summary>
        /// Gets the dictionary containing Air Deccan configuration.
        /// </summary>
        public static Dictionary<string, string> AirDeccanConfig
        {
            get
            {
                if (!ShouldLoadFromFile(airDeccanConfig, MemCacheKeys.AirDeccanConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(airDeccanConfig, MemCacheKeys.AirDeccanConfig);
                }
                else
                {
                    airDeccanConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "AirDeccan.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList deccanData = config.ChildNodes;
                    for (int i = 0; i < deccanData.Count; i++)
                    {
                        airDeccanConfig.Add(deccanData[i].Name, deccanData[i].InnerXml);
                    }
                    AddInMemCache(airDeccanConfig, MemCacheKeys.AirDeccanConfig);
                    return airDeccanConfig;
                }
            }
        }

        /// <summary>
        /// MDLR configuration entries.
        /// </summary>
        private static Dictionary<string, string> mdlrConfig = null;

        /// <summary>
        /// Gets the dictionary containing MDLR configuration.
        /// </summary>
        public static Dictionary<string, string> MdlrConfig
        {
            get
            {
                if (!ShouldLoadFromFile(mdlrConfig, MemCacheKeys.MdlrConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(mdlrConfig, MemCacheKeys.MdlrConfig);
                }
                else
                {
                    mdlrConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Mdlr.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList inData = config.ChildNodes;
                    for (int i = 0; i < inData.Count; i++)
                    {
                        mdlrConfig.Add(inData[i].Name, inData[i].InnerXml);
                    }
                    AddInMemCache(mdlrConfig, MemCacheKeys.MdlrConfig);
                    return mdlrConfig;
                }
            }
        }

        /// <summary>
        /// MDLR configuration entries.
        /// </summary>
        private static Dictionary<string, string> wlAdConfig = null;

        /// <summary>
        /// Gets the dictionary containing MDLR configuration.
        /// </summary>
        public static Dictionary<string, string> WLAdConfig
        {
            get
            {
                if (!ShouldLoadFromFile(wlAdConfig, MemCacheKeys.WLAdConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(wlAdConfig, MemCacheKeys.WLAdConfig);
                }
                else
                {
                    wlAdConfig = new Dictionary<string, string>();
                    lock (wlAdConfig)
                    {
                        wlAdConfig = new Dictionary<string, string>();
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.Load(configPath + "WLAdvertisement.config.xml");
                        XmlNode config = xmlDoc.SelectSingleNode("config");
                        XmlNodeList inData = config.ChildNodes;
                        for (int i = 0; i < inData.Count; i++)
                        {
                            if (!wlAdConfig.ContainsKey(inData[i].Name))
                            {
                                wlAdConfig.Add(inData[i].Name, inData[i].InnerXml);
                            }
                        }
                        AddInMemCache(wlAdConfig, MemCacheKeys.WLAdConfig);
                    }
                    return wlAdConfig;
                }
            }
        }

        /// <summary>
        /// Desiya configuration entries.
        /// </summary>
        private static Dictionary<string, string> desiyaConfig = null;

        /// <summary>
        /// Gets the dictionary containing Desiya configuration.
        /// </summary>
        public static Dictionary<string, string> DesiyaConfig
        {
            get
            {
                if (!ShouldLoadFromFile(desiyaConfig, MemCacheKeys.DesiyaConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(desiyaConfig, MemCacheKeys.DesiyaConfig);
                }
                else
                {
                    desiyaConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Desiya.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList desiyaData = config.ChildNodes;
                    for (int i = 0; i < desiyaData.Count; i++)
                    {
                        desiyaConfig.Add(desiyaData[i].Name, desiyaData[i].InnerXml);
                    }
                    AddInMemCache(desiyaConfig, MemCacheKeys.DesiyaConfig);
                    return desiyaConfig;
                }
            }
        }
        /// <summary>
        /// Desiya Credit Card configuration entries.
        /// </summary>
        private static Dictionary<string, string> desiyaCreditConfig = null;

        /// <summary>
        /// Gets the dictionary containing Desiya configuration.
        /// </summary>
        public static Dictionary<string, string> DesiyaCreditConfig
        {
            get
            {
                if (!ShouldLoadFromFile(desiyaCreditConfig, MemCacheKeys.DesiyaCreditConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(desiyaCreditConfig, MemCacheKeys.DesiyaCreditConfig);
                }
                else
                {
                    desiyaCreditConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "DesiyaCreditCard.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList desiyaData = config.ChildNodes;
                    for (int i = 0; i < desiyaData.Count; i++)
                    {
                        desiyaCreditConfig.Add(desiyaData[i].Name, desiyaData[i].InnerXml);
                    }
                    AddInMemCache(desiyaCreditConfig, MemCacheKeys.DesiyaCreditConfig);
                    return desiyaCreditConfig;
                }
            }
        }

        /// <summary>
        /// Switching configuration entries.
        /// </summary>
        private static Dictionary<string, string> switchConfig = null;
        /// <summary>
        /// Gets the dictionary containing Switch configuration.
        /// </summary>
        public static Dictionary<string, string> SwitchConfig
        {
            get
            {
                if (!ShouldLoadFromFile(switchConfig, MemCacheKeys.SwitchConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(switchConfig, MemCacheKeys.SwitchConfig);
                }
                else
                {
                    switchConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "SwitchConfig.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList switchData = config.ChildNodes;
                    for (int i = 0; i < switchData.Count; i++)
                    {
                        switchConfig.Add(switchData[i].Name, switchData[i].InnerXml);
                    }
                    AddInMemCache(switchConfig, MemCacheKeys.SwitchConfig);
                    return switchConfig;
                }
            }
        }

        /// <summary>
        /// Desiya configuration entries.
        /// </summary>
        private static Dictionary<string, string> GTAconfig = null;

        /// <summary>
        /// Gets the dictionary containing Desiya configuration.
        /// </summary>
        public static Dictionary<string, string> GTAConfig
        {
            get
            {
                if (!ShouldLoadFromFile(GTAconfig, MemCacheKeys.GTAConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(GTAconfig, MemCacheKeys.GTAConfig);
                }
                else
                {
                    GTAconfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "GTA.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList GTAData = config.ChildNodes;
                    for (int i = 0; i < GTAData.Count; i++)
                    {
                        GTAconfig.Add(GTAData[i].Name, GTAData[i].InnerXml);
                    }
                    AddInMemCache(GTAconfig, MemCacheKeys.GTAConfig);
                    return GTAconfig;
                }
            }
        }
        /// <summary>
        /// Miki configuration entries.
        /// </summary>
        private static Dictionary<string, string> Mikiconfig = null;

        /// <summary>
        /// Gets the dictionary containing Miki configuration.
        /// </summary>
        public static Dictionary<string, string> MikiConfig
        {
            get
            {
                if (!ShouldLoadFromFile(Mikiconfig, MemCacheKeys.MikiConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(Mikiconfig, MemCacheKeys.MikiConfig);
                }
                else
                {
                    Mikiconfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Miki.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList MikiData = config.ChildNodes;
                    for (int i = 0; i < MikiData.Count; i++)
                    {
                        Mikiconfig.Add(MikiData[i].Name, MikiData[i].InnerXml);
                    }
                    AddInMemCache(Mikiconfig, MemCacheKeys.MikiConfig);
                    return Mikiconfig;
                }
            }
        }

        /// <summary>
        /// Travco configuration entries.
        /// </summary>
        private static Dictionary<string, string> Travcoconfig = null;

        /// <summary>
        /// Gets the dictionary containing Travco configuration.
        /// </summary>
        public static Dictionary<string, string> TravcoConfig
        {
            get
            {
                if (!ShouldLoadFromFile(Travcoconfig, MemCacheKeys.TravcoConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(Travcoconfig, MemCacheKeys.TravcoConfig);
                }
                else
                {
                    Travcoconfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Travco.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList TravcoData = config.ChildNodes;
                    for (int i = 0; i < TravcoData.Count; i++)
                    {
                        Travcoconfig.Add(TravcoData[i].Name, TravcoData[i].InnerXml);
                    }
                    AddInMemCache(Travcoconfig, MemCacheKeys.TravcoConfig);
                    return Travcoconfig;
                }
            }
        }

        /// <summary>
        /// DOTW configuration entries.
        /// </summary>
        private static Dictionary<string, string> DOTWconfig = null;

        /// <summary>
        /// Gets the dictionary containing DOTW configuration.
        /// </summary>
        public static Dictionary<string, string> DOTWConfig
        {
            get
            {
                if (!ShouldLoadFromFile(DOTWconfig, MemCacheKeys.DOTWConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(DOTWconfig, MemCacheKeys.DOTWConfig);
                }
                else
                {
                    DOTWconfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "DOTW.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList DOTWData = config.ChildNodes;
                    for (int i = 0; i < DOTWData.Count; i++)
                    {
                        DOTWconfig.Add(DOTWData[i].Name, DOTWData[i].InnerXml);
                    }
                    AddInMemCache(DOTWconfig, MemCacheKeys.DOTWConfig);
                    return DOTWconfig;
                }
            }
        }

        /// <summary>
        /// WST configuration entries.
        /// </summary>
        private static Dictionary<string, string> WSTconfig = null;

        /// <summary>
        /// Gets the dictionary containing WST configuration.
        /// </summary>
        public static Dictionary<string, string> WSTConfig
        {
            get
            {
                if (!ShouldLoadFromFile(WSTconfig, MemCacheKeys.WSTConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(WSTconfig, MemCacheKeys.WSTConfig);
                }
                else
                {
                    WSTconfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "WST.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList WSTData = config.ChildNodes;
                    for (int i = 0; i < WSTData.Count; i++)
                    {
                        WSTconfig.Add(WSTData[i].Name, WSTData[i].InnerXml);
                    }
                    AddInMemCache(WSTconfig, MemCacheKeys.WSTConfig);
                    return WSTconfig;
                }
            }
        }
        /// Regex for allowed title while retrieving the PNR.
        /// </summary>
        private static string allowedTitles = null;

        /// <summary>
        /// Regex for allowed title while retrieving the PNR.
        /// </summary>
        public static string AllowedTitles
        {
            get
            {
                if (!ShouldLoadFromFile(allowedTitles, MemCacheKeys.AllowedTitlesConfig))
                {
                    return (string)GetStaticData(allowedTitles, MemCacheKeys.AllowedTitlesConfig);
                }
                else
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "BookingEngine.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config/allowedTitles");
                    string titles = config.InnerText;
                    string[] titlesCsv = titles.Split(',');
                    allowedTitles = "[ .]?(";
                    for (int i = 0; i < titlesCsv.Length; i++)
                    {
                        if (i == 0)
                        {
                            allowedTitles += "(" + titlesCsv[i] + ")";
                        }
                        else
                        {
                            allowedTitles += "|(" + titlesCsv[i] + ")";
                        }
                    }
                    allowedTitles += ")$";
                    AddInMemCache(allowedTitles, MemCacheKeys.AllowedTitlesConfig);
                    return allowedTitles;
                }
            }
        }

        #region Yatra Source  --  Added by somasekhar on 24/08/2018 

        /// <summary>
        /// Yatra configuration entries.
        /// </summary>
        private static Dictionary<string, string> yatraConfig = null;

        /// <summary>
        /// Gets the dictionary containing Yatra configuration.
        /// </summary>
        public static Dictionary<string, string> YatraConfig
        {
            get
            {
                if (!ShouldLoadFromFile(yatraConfig, MemCacheKeys.YatraConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(yatraConfig, MemCacheKeys.YatraConfig);
                }
                else
                {
                    yatraConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Yatra.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList yatraData = config.ChildNodes;
                    for (int i = 0; i < yatraData.Count; i++)
                    {
                        yatraConfig.Add(yatraData[i].Name, yatraData[i].InnerXml);
                    }
                    AddInMemCache(yatraConfig, MemCacheKeys.DesiyaConfig);
                    return yatraConfig;
                }
            }
        }

        #endregion

        /// <summary>
        /// Go Air configuration entries.
        /// </summary>
        private static Dictionary<string, string> goAirConfig = null;

        /// <summary>
        /// Gets the dictionary containing Go Air configuration.
        /// </summary>
        public static Dictionary<string, string> GoAirConfig
        {
            get
            {
                if (!ShouldLoadFromFile(goAirConfig, MemCacheKeys.GoAirConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(goAirConfig, MemCacheKeys.GoAirConfig);
                }
                else
                {
                    goAirConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "GoAir.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList goAirData = config.ChildNodes;
                    for (int i = 0; i < goAirData.Count; i++)
                    {
                        goAirConfig.Add(goAirData[i].Name, goAirData[i].InnerXml);
                    }
                    AddInMemCache(goAirConfig, MemCacheKeys.GoAirConfig);
                    return goAirConfig;
                }
            }
        }

        public static Dictionary<string, string> faq;
        /// <summary>
        /// Getting default FAQ's
        /// </summary>
        //WhiteLabelFaqs faq = new WhiteLabelFaqs();       
        public static Dictionary<string, string> Faq
        {
            get
            {
                if (!ShouldLoadFromFile(faq, MemCacheKeys.FaqConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(faq, MemCacheKeys.FaqConfig);
                }
                else
                {
                    faq = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "FAQ.config.xml");
                    XmlNodeList faqList = xmlDoc.SelectNodes("config/faq");
                    foreach (XmlNode quest in faqList)
                    {
                        XmlNode question = quest.SelectSingleNode("question");
                        XmlNode answer = quest.SelectSingleNode("answer");
                        faq.Add(question.InnerText, answer.InnerText);
                    }
                    AddInMemCache(faq, MemCacheKeys.FaqConfig);
                    return faq;
                }
            }
        }

        public static Dictionary<string, string> holidayFAQ;
        /// <summary>
        /// Getting default HolidayFAQ's
        /// </summary>
        public static Dictionary<string, string> HolidayFAQ
        {
            get
            {
                if (!ShouldLoadFromFile(holidayFAQ, MemCacheKeys.HolidayFaqConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(holidayFAQ, MemCacheKeys.HolidayFaqConfig);
                }
                else
                {
                    holidayFAQ = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "HolidayFAQ.config.xml");
                    XmlNodeList faqList = xmlDoc.SelectNodes("config/faq");
                    foreach (XmlNode quest in faqList)
                    {
                        XmlNode question = quest.SelectSingleNode("question");
                        XmlNode answer = quest.SelectSingleNode("answer");
                        holidayFAQ.Add(question.InnerText, answer.InnerText);
                    }
                    AddInMemCache(holidayFAQ, MemCacheKeys.HolidayFaqConfig);
                    return holidayFAQ;
                }
            }
        }

        public static Dictionary<string, string> hotelFaq;
        /// <summary>
        /// Getting default FAQ's
        /// </summary>
        //WhiteLabelFaqs faq = new WhiteLabelFaqs();       
        public static Dictionary<string, string> HotelFaq
        {
            get
            {
                if (!ShouldLoadFromFile(hotelFaq, MemCacheKeys.HotelFaqConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(hotelFaq, MemCacheKeys.HotelFaqConfig);
                }
                else
                {
                    hotelFaq = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "FAQ.config.xml");
                    XmlNodeList faqList = xmlDoc.SelectNodes("config/hotelFaq");
                    foreach (XmlNode quest in faqList)
                    {
                        XmlNode question = quest.SelectSingleNode("question");
                        XmlNode answer = quest.SelectSingleNode("answer");
                        hotelFaq.Add(question.InnerText, answer.InnerText);
                    }
                    AddInMemCache(hotelFaq, MemCacheKeys.HotelFaqConfig);
                    return hotelFaq;
                }
            }
        }

        private static Dictionary<string, string> TBOHotelconfig = null;
        /// <summary>
        /// This is for TBO Special Config File.
        /// </summary>
        public static Dictionary<string, string> TBOHotelConfig
        {
            get
            {
                if (!ShouldLoadFromFile(TBOHotelconfig, MemCacheKeys.TBOHotelConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(TBOHotelconfig, MemCacheKeys.TBOHotelConfig);
                }
                else
                {
                    TBOHotelconfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "TBOHotel.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList TBOData = config.ChildNodes;
                    for (int i = 0; i < TBOData.Count; i++)
                    {
                        TBOHotelconfig.Add(TBOData[i].Name, TBOData[i].InnerXml);
                    }
                    AddInMemCache(TBOHotelconfig, MemCacheKeys.TBOHotelConfig);
                    return TBOHotelconfig;
                }
            }
        }

        /// <summary>
        /// Show Promotion configuration entries.
        /// </summary>
        private static Dictionary<string, int> promotionConfig = null;

        /// <summary>
        /// Gets the dictionary containing Promotional Deal configuration.
        /// </summary>
        public static Dictionary<string, int> PromotionConfig
        {
            get
            {
                if (!ShouldLoadFromFile(promotionConfig, MemCacheKeys.PromotionConfig))
                {
                    return (Dictionary<string, int>)GetStaticData(promotionConfig, MemCacheKeys.PromotionConfig);
                }
                else
                {
                    promotionConfig = new Dictionary<string, int>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Promotion.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList proConfigData = config.ChildNodes;
                    for (int i = 0; i < proConfigData.Count; i++)
                    {
                        XmlNode tempNode = proConfigData[i].SelectSingleNode("PageName");
                        string page = tempNode.InnerText;
                        tempNode = proConfigData[i].SelectSingleNode("Active");
                        bool isActive = false;
                        if (tempNode != null)
                        {
                            isActive = Convert.ToBoolean(tempNode.InnerText);
                            if (isActive)
                            {
                                tempNode = proConfigData[i].SelectSingleNode("NoOfDeal");
                                if (tempNode != null && tempNode.InnerText.Length > 0)
                                {
                                    int noOfDeal = Convert.ToInt32(tempNode.InnerText);
                                    promotionConfig.Add(page, noOfDeal);
                                }
                            }
                        }
                    }
                    AddInMemCache(promotionConfig, MemCacheKeys.PromotionConfig);
                    return promotionConfig;
                }
            }
        }
        /// <summary>
        /// This is for Hotel Auto Cancellation.
        /// </summary>
        private static Dictionary<string, Dictionary<string, string>> hotelAutoCancel = null;
        public static Dictionary<string, Dictionary<string, string>> HotelAutoCancel
        {
            get
            {
                if (!ShouldLoadFromFile(hotelAutoCancel, MemCacheKeys.HotelAutoCancelConfig))
                {
                    return (Dictionary<string, Dictionary<string, string>>)GetStaticData(hotelAutoCancel, MemCacheKeys.HotelAutoCancelConfig);
                }
                else
                {
                    hotelAutoCancel = new Dictionary<string, Dictionary<string, string>>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "hotelAutoCancellation.config.xml");
                    XmlNodeList data = xmlDoc.SelectNodes("/config/source");
                    for (int i = 0; i < data.Count; i++)
                    {
                        XmlNode nameNode = data[i].SelectSingleNode("Name");
                        XmlNode activeNode = data[i].SelectSingleNode("Active");
                        if (nameNode != null && activeNode != null && activeNode.InnerText == "1")
                        {
                            Dictionary<string, string> autoCancel = new Dictionary<string, string>();
                            XmlNode tempNode = data[i].SelectSingleNode("CancelRequested");
                            if (tempNode != null)
                            {
                                autoCancel.Add("CancelRequested", tempNode.InnerText);
                            }
                            tempNode = data[i].SelectSingleNode("NonRequestedButNotVouchered");
                            if (tempNode != null)
                            {
                                autoCancel.Add("NonRequestedButNotVouchered", tempNode.InnerText);
                            }
                            hotelAutoCancel.Add(nameNode.InnerText, autoCancel);
                        }
                    }
                    AddInMemCache(hotelAutoCancel, MemCacheKeys.HotelAutoCancelConfig);
                    return hotelAutoCancel;
                }
            }
        }
        private static Dictionary<string, string> iciciConfig = null;
        /// <summary>
        /// This is for TBO Special Config File.
        /// </summary>
        public static Dictionary<string, string> ICICIConfig
        {
            get
            {
                if (!ShouldLoadFromFile(iciciConfig, MemCacheKeys.ICICIConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(iciciConfig, MemCacheKeys.ICICIConfig);
                }
                else
                {
                    iciciConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "ICICI.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList iciciData = config.ChildNodes;
                    for (int i = 0; i < iciciData.Count; i++)
                    {
                        iciciConfig.Add(iciciData[i].Name, iciciData[i].InnerXml);
                    }
                    AddInMemCache(iciciConfig, MemCacheKeys.ICICIConfig);
                    return iciciConfig;
                }
            }
        }

        /// <summary>
        /// HotelBeds configuration entries.
        /// </summary>
        private static Dictionary<string, string> hotelBedsConfig = null;

        /// <summary>
        /// Gets the dictionary containing HotelBeds configuration.
        /// </summary>
        public static Dictionary<string, string> HotelBedsConfig
        {
            get
            {
                if (!ShouldLoadFromFile(hotelBedsConfig, MemCacheKeys.HotelBedsConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(hotelBedsConfig, MemCacheKeys.HotelBedsConfig);
                }
                else
                {
                    hotelBedsConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "HotelBeds.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList hotelBedsData = config.ChildNodes;
                    for (int i = 0; i < hotelBedsData.Count; i++)
                    {
                        hotelBedsConfig.Add(hotelBedsData[i].Name, hotelBedsData[i].InnerXml);
                    }
                    AddInMemCache(hotelBedsConfig, MemCacheKeys.HotelBedsConfig);
                    return hotelBedsConfig;
                }
            }
        }

        /// <summary>
        /// Galileo configuration entries.
        /// </summary>
        private static Dictionary<string, string> galileoConfig = null;

        /// <summary>
        /// Gets the dictionary containing HotelBeds configuration.
        /// </summary>
        public static Dictionary<string, string> GalileoConfig
        {
            get
            {
                if (!ShouldLoadFromFile(galileoConfig, MemCacheKeys.GalileoConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(galileoConfig, MemCacheKeys.GalileoConfig);
                }
                else
                {
                    galileoConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Galileo.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList galileoData = config.ChildNodes;
                    for (int i = 0; i < galileoData.Count; i++)
                    {
                        galileoConfig.Add(galileoData[i].Name, galileoData[i].InnerXml);
                    }
                    AddInMemCache(galileoConfig, MemCacheKeys.GalileoConfig);
                    return galileoConfig;
                }
            }
        }

        /// <summary>
        /// UAPI configuration entries.
        /// </summary>
        private static Dictionary<string, string> uapiConfig = null;

        /// <summary>
        /// Gets the dictionary containing HotelBeds configuration.
        /// </summary>
        public static Dictionary<string, string> UAPIConfig
        {
            get
            {
                if (!ShouldLoadFromFile(uapiConfig, MemCacheKeys.UAPIConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(uapiConfig, MemCacheKeys.UAPIConfig);
                }
                else
                {
                    uapiConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "UAPI.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList uapiData = config.ChildNodes;
                    for (int i = 0; i < uapiData.Count; i++)
                    {
                        uapiConfig.Add(uapiData[i].Name, uapiData[i].InnerXml);
                    }
                    AddInMemCache(uapiConfig, MemCacheKeys.UAPIConfig);
                    return uapiConfig;
                }
            }
        }

        /// <summary>
        /// Tourico configuration entries.
        /// </summary>
        private static Dictionary<string, string> touricoConfig = null;

        /// <summary>
        /// Gets the dictionary containing Tourico configuration.
        /// </summary>
        public static Dictionary<string, string> TouricoConfig
        {
            get
            {
                if (!ShouldLoadFromFile(touricoConfig, MemCacheKeys.TouricoConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(touricoConfig, MemCacheKeys.TouricoConfig);
                }
                else
                {
                    touricoConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Tourico.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList touricoData = config.ChildNodes;
                    for (int i = 0; i < touricoData.Count; i++)
                    {
                        touricoConfig.Add(touricoData[i].Name, touricoData[i].InnerXml);
                    }
                    AddInMemCache(touricoConfig, MemCacheKeys.TouricoConfig);
                    return touricoConfig;
                }
            }
        }

        private static Dictionary<string, string> cacheConfig = null;
        /// <summary>
        /// Gets config data from Cache.config.xml file.
        /// </summary>
        public static Dictionary<string, string> CacheConfig
        {
            get
            {
                if (cacheConfig == null)
                {
                    cacheConfig = ReadConfig("Cache");
                    // Creating Server list.
                    string servers = string.Empty;
                    XmlDocument xml = new XmlDocument();
                    xml.LoadXml(cacheConfig["servers"]);
                    XmlNodeList serverList = xml.SelectNodes("server");
                    XmlNode node;
                    foreach (XmlNode serverNode in serverList)
                    {
                        node = serverNode.SelectSingleNode("ip");
                        if (node != null && !string.IsNullOrEmpty(node.InnerText))
                        {
                            servers += node.InnerText;
                            node = serverNode.SelectSingleNode("port");
                            if (node != null && !string.IsNullOrEmpty(node.InnerText))
                            {
                                servers += ":" + node.InnerText;
                            }
                            servers += ";";
                        }
                    }
                    cacheConfig.Add("serverList", servers);
                    //AddInMemCache(pagingConfig, MemCacheKeys.PagingConfig);
                }

                return cacheConfig;
            }
        }

        /// <summary>
        /// Reads config file of given module.
        /// </summary>
        /// <param name="moduleName">Name of the module.</param>
        /// <returns>Dictionary containing node name and values from config file.
        /// Only the child nodes of node 'config' are read.</returns>
        private static Dictionary<string, string> ReadConfig(string moduleName)
        {
            Dictionary<string, string> configList = new Dictionary<string, string>();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(configPath + moduleName + ".config.xml");
            XmlNode config = xmlDoc.SelectSingleNode("config");
            XmlNodeList configData = config.ChildNodes;
            for (int i = 0; i < configData.Count; i++)
            {
                configList.Add(configData[i].Name, configData[i].InnerXml);
            }
            return configList;
        }
        private static Dictionary<string, string> samaConfig = null;

        public static Dictionary<string, string> SamaConfig
        {
            get
            {
                if (!ShouldLoadFromFile(samaConfig, MemCacheKeys.SamaConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(samaConfig, MemCacheKeys.SamaConfig);
                }
                else
                {
                    samaConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Sama.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList inData = config.ChildNodes;
                    for (int i = 0; i < inData.Count; i++)
                    {
                        samaConfig.Add(inData[i].Name, inData[i].InnerXml);
                    }
                    AddInMemCache(samaConfig, MemCacheKeys.SamaConfig);
                    return samaConfig;
                }
            }
        }

        private static Dictionary<string, string> samaCityConfig = null;

        public static Dictionary<string, string> SamaCityConfig
        {
            get
            {
                if (!ShouldLoadFromFile(samaCityConfig, MemCacheKeys.SamaCityConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(samaCityConfig, MemCacheKeys.SamaCityConfig);
                }
                else
                {
                    samaCityConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "SamaCity.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("root");
                    XmlNodeList inData = config.SelectNodes("city");
                    for (int i = 0; i < inData.Count; i++)
                    {
                        XmlNodeList city = inData[i].ChildNodes;
                        samaCityConfig.Add(city[1].InnerXml, city[0].InnerXml);
                    }
                    AddInMemCache(samaCityConfig, MemCacheKeys.SamaCityConfig);
                    return samaCityConfig;
                }
            }
        }


        private static Dictionary<string, string> samaTimeZoneConfig = null;

        public static Dictionary<string, string> SamaTimeZoneConfig
        {
            get
            {
                if (!ShouldLoadFromFile(samaTimeZoneConfig, MemCacheKeys.SamaTimeZoneConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(samaTimeZoneConfig, MemCacheKeys.SamaTimeZoneConfig);
                }
                else
                {
                    samaTimeZoneConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "SamaCity.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("root");
                    XmlNodeList inData = config.SelectNodes("city");
                    for (int i = 0; i < inData.Count; i++)
                    {
                        XmlNodeList city = inData[i].ChildNodes;
                        samaTimeZoneConfig.Add(city[0].InnerXml, city[2].InnerXml);
                    }
                    AddInMemCache(samaTimeZoneConfig, MemCacheKeys.SamaTimeZoneConfig);
                    return samaTimeZoneConfig;
                }
            }
        }
        /// <summary>
        /// Tourico configuration entries.
        /// </summary>
        private static Dictionary<string, string> irctcConfig = null;

        /// <summary>
        /// Gets the dictionary containing Tourico configuration.
        /// </summary>
        public static Dictionary<string, string> IRCTCConfig
        {
            get
            {
                if (!ShouldLoadFromFile(irctcConfig, MemCacheKeys.IRCTCConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(irctcConfig, MemCacheKeys.IRCTCConfig);
                }
                else
                {
                    irctcConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "IRCTCConfig.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("IRCTC");
                    XmlNodeList irctcData = config.ChildNodes;
                    for (int i = 0; i < irctcData.Count; i++)
                    {
                        irctcConfig.Add(irctcData[i].Name, irctcData[i].InnerXml);
                    }
                    AddInMemCache(irctcConfig, MemCacheKeys.IRCTCConfig);
                    return irctcConfig;
                }
            }
        }
        /// <summary>
        /// IAN configuration entries.
        /// </summary>
        private static Dictionary<string, string> ianConfig = null;

        /// <summary>
        /// Gets the dictionary containing IAN configuration.
        /// </summary>
        public static Dictionary<string, string> IANConfig
        {
            get
            {
                if (!ShouldLoadFromFile(ianConfig, MemCacheKeys.IANConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(ianConfig, MemCacheKeys.IANConfig);
                }
                else
                {
                    ianConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "IAN.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList ianData = config.ChildNodes;
                    for (int i = 0; i < ianData.Count; i++)
                    {
                        ianConfig.Add(ianData[i].Name, ianData[i].InnerXml);
                    }
                    AddInMemCache(ianConfig, MemCacheKeys.IANConfig);
                    return ianConfig;
                }
            }
        }

        /// <summary>
        /// India Times configuration entries
        /// </summary>
        private static Dictionary<string, string> b2cSettingsConfig;

        public static Dictionary<string, string> B2CSettingsConfig
        {
            get
            {
                if (!ShouldLoadFromFile(b2cSettingsConfig, MemCacheKeys.B2CSettingsConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(b2cSettingsConfig, MemCacheKeys.B2CSettingsConfig);
                }
                else
                {
                    b2cSettingsConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    //xmlDoc.Load(configPath + "IndiaTimes.config.xml");
                    xmlDoc.Load(configPath + "B2CSettings.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList indiaTimesData = config.ChildNodes;
                    for (int i = 0; i < indiaTimesData.Count; i++)
                    {
                        b2cSettingsConfig.Add(indiaTimesData[i].Name, indiaTimesData[i].InnerXml);
                    }
                    AddInMemCache(b2cSettingsConfig, MemCacheKeys.B2CSettingsConfig);
                    return b2cSettingsConfig;
                }
            }
        }

        /// <summary>
        /// AirlineCommission configuration entries.
        /// </summary>
        private static Dictionary<string, decimal> nonCommisionableTaxConfig = null;

        /// <summary>
        /// Gets the dictionary containing AirlineCommission configuration.
        /// The key for the dictionary is the airline code, and the key value pair the key is the nonCommissionable tax and the value is the commission to be given out
        /// </summary>
        public static Dictionary<string, decimal> NonCommisionableTaxConfig
        {
            get
            {
                if (!ShouldLoadFromFile(nonCommisionableTaxConfig, MemCacheKeys.NonCommisionableTaxConfig))
                {
                    return (Dictionary<string, decimal>)GetStaticData(nonCommisionableTaxConfig, MemCacheKeys.NonCommisionableTaxConfig);
                }
                else
                {
                    nonCommisionableTaxConfig = new Dictionary<string, decimal>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "BookingEngine.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config/NonCommisionableTax");
                    string delimittedString = string.Empty;
                    if (config != null)
                    {
                        delimittedString = config.InnerText;
                    }
                    string[] airlinesList = delimittedString.Split(',');
                    for (int i = 0; i < airlinesList.Length; i++)
                    {
                        string[] airlineCommision = airlinesList[i].Split('#');
                        nonCommisionableTaxConfig.Add(airlineCommision[0], Convert.ToDecimal(airlineCommision[1]));
                    }
                    AddInMemCache(nonCommisionableTaxConfig, MemCacheKeys.NonCommisionableTaxConfig);
                    return nonCommisionableTaxConfig;
                }
            }
        }

        /// <summary>
        /// AirlineCashBack configuration entries.
        /// </summary>
        private static Dictionary<string, decimal> airlineCashBackConfig = null;

        /// <summary>
        /// Gets the dictionary containing AirlineCommission configuration.
        /// The key for the dictionary is the airline code, and the key value pair the key is the nonCommissionable tax and the value is the commission to be given out
        /// </summary>
        public static Dictionary<string, decimal> AirlineCashBackConfig
        {
            get
            {
                if (!ShouldLoadFromFile(airlineCashBackConfig, MemCacheKeys.AirlineCashBackConfig))
                {
                    return (Dictionary<string, decimal>)GetStaticData(airlineCashBackConfig, MemCacheKeys.AirlineCashBackConfig);
                }
                else
                {
                    airlineCashBackConfig = new Dictionary<string, decimal>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "BookingEngine.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config/CashBack");
                    string delimittedString = string.Empty;
                    if (config != null)
                    {
                        delimittedString = config.InnerText;
                    }
                    string[] airlinesList = delimittedString.Split(',');
                    for (int i = 0; i < airlinesList.Length; i++)
                    {
                        string[] airlineCommision = airlinesList[i].Split('#');
                        decimal cashBack = Convert.ToDecimal(airlineCommision[1]);
                        string airline = airlineCommision[0];
                        airlineCashBackConfig.Add(airline, cashBack);
                    }
                    AddInMemCache(airlineCashBackConfig, MemCacheKeys.AirlineCashBackConfig);
                    return airlineCashBackConfig;
                }
            }
        }

        /// <summary>
        /// Hermes configuration entries.
        /// </summary>
        private static Dictionary<string, string> hermesConfig = null;
        /// <summary>
        /// Gets the dictionary containing Hermes configuration.
        /// </summary>
        public static Dictionary<string, string> HermesConfig
        {
            get
            {
                if (!ShouldLoadFromFile(hermesConfig, MemCacheKeys.HermesConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(hermesConfig, MemCacheKeys.HermesConfig);
                }
                else
                {
                    hermesConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Hermes.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList hermesData = config.ChildNodes;
                    for (int i = 0; i < hermesData.Count; i++)
                    {
                        hermesConfig.Add(hermesData[i].Name, hermesData[i].InnerXml);
                    }
                    AddInMemCache(hermesConfig, MemCacheKeys.HermesConfig);
                    return hermesConfig;
                }
            }
        }


        /// <summary>
        /// SectorList configuration entries.
        /// </summary>
        private static Dictionary<string, Dictionary<string, string>> sectorListConfig = null;
        /// <summary>
        /// Gets the dictionary containing SectorList configuration.
        /// </summary>
        public static Dictionary<string, Dictionary<string, string>> SectorListConfig
        {
            get
            {
                if (!ShouldLoadFromFile(sectorListConfig, MemCacheKeys.SectorListConfig))
                {
                    return (Dictionary<string, Dictionary<string, string>>)GetStaticData(sectorListConfig, MemCacheKeys.SectorListConfig);
                }
                else
                {
                    sectorListConfig = new Dictionary<string, Dictionary<string, string>>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "SectorList.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList sectorSource = config.ChildNodes;
                    for (int i = 0; i < sectorSource.Count; i++)
                    {
                        XmlNodeList sectorListData = sectorSource[i].ChildNodes;
                        Dictionary<string, string> sectorData = new Dictionary<string, string>();
                        for (int j = 0; j < sectorListData.Count; j++)
                        {
                            sectorData.Add(sectorListData[j].Name, sectorListData[j].InnerXml);
                        }
                        sectorListConfig.Add(sectorSource[i].Name, sectorData);
                    }
                    AddInMemCache(sectorListConfig, MemCacheKeys.SectorListConfig);
                    return sectorListConfig;
                }
            }
        }

        /// <summary>
        /// AirlineCommission configuration entries.
        /// </summary>
        private static Dictionary<string, short> defaultCategory = null;

        /// <summary>
        /// Gets the dictionary containing AirlineCommission configuration.
        /// The key for the dictionary is the airline code, and the key value pair the key is the nonCommissionable tax and the value is the commission to be given out
        /// </summary>
        public static Dictionary<string, short> DefaultCategory
        {
            get
            {
                if (!ShouldLoadFromFile(defaultCategory, MemCacheKeys.DefaultCategoryConfig))
                {
                    return (Dictionary<string, short>)GetStaticData(defaultCategory, MemCacheKeys.DefaultCategoryConfig);
                }
                else
                {
                    defaultCategory = new Dictionary<string, short>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Core.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config/defaultCategory");
                    string delimittedString = string.Empty;
                    if (config != null)
                    {
                        delimittedString = config.InnerText;
                    }
                    string[] airlinesList = delimittedString.Split(',');
                    for (int i = 0; i < airlinesList.Length; i++)
                    {
                        string[] category = airlinesList[i].Split('#');
                        defaultCategory.Add(category[0], Convert.ToInt16(category[1]));
                    }
                    AddInMemCache(defaultCategory, MemCacheKeys.DefaultCategoryConfig);
                    return defaultCategory;
                }
            }
        }

        /// <summary>
        /// Reporting configuration entries.
        /// </summary>
        private static Dictionary<string, string> reportingServicesConfig = null;
        /// <summary>
        /// Gets the dictionary containing reporting services configuration.
        /// </summary>
        public static Dictionary<string, string> ReportingServicesConfig
        {
            get
            {
                if (!ShouldLoadFromFile(reportingServicesConfig, MemCacheKeys.ReportingServicesConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(reportingServicesConfig, MemCacheKeys.ReportingServicesConfig);
                }
                else
                {
                    reportingServicesConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "ReportingServicesConfig.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("ReportingServices");
                    XmlNodeList reportingServicesData = config.ChildNodes;
                    for (int i = 0; i < reportingServicesData.Count; i++)
                    {
                        reportingServicesConfig.Add(reportingServicesData[i].Name, reportingServicesData[i].InnerXml);
                    }
                    AddInMemCache(reportingServicesConfig, MemCacheKeys.ReportingServicesConfig);
                    return reportingServicesConfig;
                }
            }
        }
        /// <summary>
        /// SMS configuration entries.
        /// </summary>
        private static Dictionary<string, string> smsConfig = null;
        /// <summary>
        /// Gets the dictionary containing SMS services configuration.
        /// </summary>
        public static Dictionary<string, string> SmsConfig
        {
            get
            {
                if (!ShouldLoadFromFile(smsConfig, MemCacheKeys.SmsConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(smsConfig, MemCacheKeys.SmsConfig);
                }
                else
                {
                    smsConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "SMSConfig.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList smsConfigData = config.ChildNodes;
                    for (int i = 0; i < smsConfigData.Count; i++)
                    {
                        smsConfig.Add(smsConfigData[i].Name, smsConfigData[i].InnerXml);
                    }
                    AddInMemCache(smsConfig, MemCacheKeys.SmsConfig);
                    return smsConfig;
                }
            }
        }
        /// <summary>
        /// SectorList configuration entries.
        /// </summary>
        private static Dictionary<string, List<string>> statusListConfig = null;
        /// <summary>
        /// Gets the dictionary containing SectorList configuration.
        /// </summary>
        public static Dictionary<string, List<string>> StatusListConfig
        {
            get
            {
                if (!ShouldLoadFromFile(statusListConfig, MemCacheKeys.StatusListConfig))
                {
                    return (Dictionary<string, List<string>>)GetStaticData(statusListConfig, MemCacheKeys.StatusListConfig);
                }
                else
                {
                    statusListConfig = new Dictionary<string, List<string>>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "StatusList.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList sectorSource = config.ChildNodes;
                    for (int i = 0; i < sectorSource.Count; i++)
                    {
                        List<string> status = new List<string>(sectorSource[i].InnerText.Split(','));
                        statusListConfig.Add(sectorSource[i].Name, status);
                    }
                    AddInMemCache(statusListConfig, MemCacheKeys.StatusListConfig);
                    return statusListConfig;
                }
            }
        }
        /// <summary>
        /// Config for Hotel Connect Integration
        /// </summary>
        private static Dictionary<string, string> hotelConnectConfig = null;
        public static Dictionary<string, string> HotelConnectConfig
        {
            get
            {
                if (!ShouldLoadFromFile(hotelConnectConfig, MemCacheKeys.HotelConnectConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(hotelConnectConfig, MemCacheKeys.HotelConnectConfig);
                }
                else
                {
                    hotelConnectConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "HIS.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList hotelConnectData = config.ChildNodes;
                    for (int i = 0; i < hotelConnectData.Count; i++)
                    {
                        hotelConnectConfig.Add(hotelConnectData[i].Name, hotelConnectData[i].InnerXml);
                    }
                    AddInMemCache(hotelConnectConfig, MemCacheKeys.HotelConnectConfig);
                    return hotelConnectConfig;
                }
            }
        }


        /// <summary>
        ///Credit Card configuration entries.
        /// </summary>
        private static Dictionary<string, string> creditCardConfig = null;
        /// <summary>
        /// Gets the dictionary containing Credit Card configuration.
        /// </summary>
        public static Dictionary<string, string> CreditCardConfig
        {
            get
            {
                if (!ShouldLoadFromFile(creditCardConfig, MemCacheKeys.CreditCardConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(creditCardConfig, MemCacheKeys.CreditCardConfig);
                }
                else
                {
                    creditCardConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "CreditCard.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList inData = config.ChildNodes;
                    for (int i = 0; i < inData.Count; i++)
                    {
                        creditCardConfig.Add(inData[i].Name, inData[i].InnerXml);
                    }
                    AddInMemCache(creditCardConfig, MemCacheKeys.CreditCardConfig);
                    return creditCardConfig;
                }
            }
        }
        /// <summary>
        ///ITRed configuration entries.
        /// </summary>
        private static SortedDictionary<int, int> itRedConfig = null;
        /// <summary>
        /// Gets the SortedDictionary containing FlightNumbers of ITRed.
        /// </summary>
        public static SortedDictionary<int, int> ITRedConfig
        {
            get
            {
                if (!ShouldLoadFromFile(itRedConfig, MemCacheKeys.ITRedConfig))
                {
                    return (SortedDictionary<int, int>)GetStaticData(itRedConfig, MemCacheKeys.ITRedConfig);
                }
                else
                {
                    itRedConfig = new SortedDictionary<int, int>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "ITRed.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList inData = config.ChildNodes;
                    List<string> range = new List<string>();
                    range.AddRange(inData[0].InnerXml.Split(','));
                    foreach (string s in range)
                    {
                        int value;
                        string[] splitValues = s.Split('-');
                        if (splitValues.Length >= 2)
                        {
                            value = Convert.ToInt16(splitValues[1]);
                        }
                        else
                        {
                            value = Convert.ToInt16(splitValues[0]);
                        }
                        itRedConfig.Add(Convert.ToInt16(splitValues[0]), value);
                    }
                    AddInMemCache(itRedConfig, MemCacheKeys.ITRedConfig);
                    return itRedConfig;
                }
            }
        }

        /// <summary>
        ///Credit Card configuration entries.
        /// </summary>
        private static Dictionary<string, string> flyDubaiConfig = null;
        /// <summary>
        /// Gets the dictionary containing Credit Card configuration.
        /// </summary>
        public static Dictionary<string, string> FlyDubaiConfig
        {
            get
            {
                if (!ShouldLoadFromFile(flyDubaiConfig, MemCacheKeys.FlyDubaiConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(flyDubaiConfig, MemCacheKeys.FlyDubaiConfig);
                }
                else
                {
                    flyDubaiConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "FlyDubai.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList inData = config.ChildNodes;
                    for (int i = 0; i < inData.Count; i++)
                    {
                        flyDubaiConfig.Add(inData[i].Name, inData[i].InnerXml);
                    }
                    AddInMemCache(flyDubaiConfig, MemCacheKeys.FlyDubaiConfig);
                    return flyDubaiConfig;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private static Dictionary<string, string> holidayDealConfig;

        public static Dictionary<string, string> HolidayDealConfig
        {
            get
            {
                if (!ShouldLoadFromFile(holidayDealConfig, MemCacheKeys.HolidayDealConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(holidayDealConfig, MemCacheKeys.HolidayDealConfig);
                }
                else
                {
                    holidayDealConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "HolidayDeal.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList holidayDealData = config.ChildNodes;
                    for (int i = 0; i < holidayDealData.Count; i++)
                    {
                        holidayDealConfig.Add(holidayDealData[i].Name, holidayDealData[i].InnerXml);
                    }
                    AddInMemCache(holidayDealConfig, MemCacheKeys.HolidayDealConfig);
                    return holidayDealConfig;
                }
            }
        }




        /// <summary>
        /// Visa Configuration Entries
        /// </summary>
        private static Dictionary<string, string> visaConfig;

        public static Dictionary<string, string> VisaConfig
        {
            get
            {
                //if (!ShouldLoadFromFile(VisaConfig, MemCacheKeys.VisaConfig))
                //{
                //    return (Dictionary<string, string>)GetStaticData(visaConfig, MemCacheKeys.VisaConfig);
                //}
                //else
                //{
                visaConfig = new Dictionary<string, string>();
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(configPath + "Visa.config.xml");
                XmlNode config = xmlDoc.SelectSingleNode("config");
                XmlNodeList visaData = config.ChildNodes;
                for (int i = 0; i < visaData.Count; i++)
                {
                    visaConfig.Add(visaData[i].Name, visaData[i].InnerXml);
                }
                AddInMemCache(visaConfig, MemCacheKeys.VisaConfig);
                return visaConfig;
                //}
            }
        }


        private static Dictionary<string, string> airArabiaConfig = null;
        /// <summary>
        /// Gets the dictionary containing Go Air configuration.
        /// </summary>
        public static Dictionary<string, string> AirArabiaConfig
        {
            get
            {
                if (!ShouldLoadFromFile(airArabiaConfig, MemCacheKeys.AirArabia))
                {
                    return (Dictionary<string, string>)GetStaticData(airArabiaConfig, MemCacheKeys.AirArabia);
                }
                else
                {
                    airArabiaConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "AirArabia.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList airArabiaData = config.ChildNodes;
                    for (int i = 0; i < airArabiaData.Count; i++)
                    {
                        airArabiaConfig.Add(airArabiaData[i].Name, airArabiaData[i].InnerXml);
                    }
                    AddInMemCache(airArabiaConfig, MemCacheKeys.AirArabia);
                    return airArabiaConfig;
                }
            }
        }

        /// <summary>
        ///PayWorld configuration entries.
        /// </summary>
        private static Dictionary<string, string> payWorldConfig = null;
        /// <summary>
        /// Gets the dictionary containing PayWorld configuration.
        /// </summary>
        public static Dictionary<string, string> PayWorldConfig
        {
            get
            {
                if (!ShouldLoadFromFile(payWorldConfig, MemCacheKeys.PayWorldConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(payWorldConfig, MemCacheKeys.PayWorldConfig);
                }
                else
                {
                    payWorldConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "PayWorldConfig.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList inData = config.ChildNodes;
                    for (int i = 0; i < inData.Count; i++)
                    {
                        payWorldConfig.Add(inData[i].Name, inData[i].InnerXml);
                    }
                    AddInMemCache(payWorldConfig, MemCacheKeys.PayWorldConfig);
                    return payWorldConfig;
                }
            }
        }

        /// <summary>
        ///Credit Card configuration entries.
        /// </summary>
        private static Dictionary<string, string> iffcoInsuranceConfig = null;
        /// <summary>
        /// Gets the dictionary containing Credit Card configuration.
        /// </summary>
        public static Dictionary<string, string> IffcoInsuranceConfig
        {
            get
            {
                if (!ShouldLoadFromFile(iffcoInsuranceConfig, MemCacheKeys.IffcoInsuranceConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(iffcoInsuranceConfig, MemCacheKeys.IffcoInsuranceConfig);
                }
                else
                {
                    iffcoInsuranceConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "IffcoInsurance.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList inData = config.ChildNodes;
                    for (int i = 0; i < inData.Count; i++)
                    {
                        iffcoInsuranceConfig.Add(inData[i].Name, inData[i].InnerXml);
                    }
                    AddInMemCache(iffcoInsuranceConfig, MemCacheKeys.IffcoInsuranceConfig);
                    return iffcoInsuranceConfig;
                }
            }
        }

        private static Dictionary<string, string> zeusConfig = null;
        /// <summary>
        /// Gets the dictionary containing ZEUS insurance configuration.
        /// </summary>
        public static Dictionary<string, string> ZeusConfig
        {
            get
            {
                if (!ShouldLoadFromFile(zeusConfig, MemCacheKeys.Zeus))
                {
                    return (Dictionary<string, string>)GetStaticData(zeusConfig, MemCacheKeys.Zeus);
                }
                else
                {
                    zeusConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "ZEUS.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList zeusData = config.ChildNodes;
                    for (int i = 0; i < zeusData.Count; i++)
                    {
                        zeusConfig.Add(zeusData[i].Name, zeusData[i].InnerXml);
                    }
                    AddInMemCache(zeusConfig, MemCacheKeys.Zeus);
                    return zeusConfig;
                }
            }
        }


        //TODO: The config system is to be made generic accordingly.
        /// <summary>
        /// bookingEngine configuration entries.
        /// </summary>
        private static Dictionary<string, string> localeConfig = null;
        /// <summary>
        /// Gets the dictionary containing bookingEngineConfig configuration.
        /// </summary>
        public static Dictionary<string, string> LocaleConfig
        {
            get
            {
                if (!ShouldLoadFromFile(localeConfig, MemCacheKeys.BookingEngineConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(localeConfig, MemCacheKeys.LocaleConfig);
                }
                else
                {
                    localeConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Locale.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList beData = config.ChildNodes;
                    for (int i = 0; i < beData.Count; i++)
                    {
                        localeConfig.Add(beData[i].Name, beData[i].InnerText);
                    }
                    AddInMemCache(localeConfig, MemCacheKeys.LocaleConfig);
                    return localeConfig;
                }
            }
        }

        /// <summary>
        /// RezLive configuration entries.
        /// </summary>
        private static Dictionary<string, string> rezLiveConfig = null;

        /// <summary>
        /// Gets the dictionary containing RezLive configuration.
        /// </summary>
        public static Dictionary<string, string> RezLiveConfig
        {
            get
            {
                if (!ShouldLoadFromFile(rezLiveConfig, MemCacheKeys.RezLiveConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(rezLiveConfig, MemCacheKeys.RezLiveConfig);
                }
                else
                {
                    rezLiveConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "RezLive.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList RezLiveData = config.ChildNodes;
                    for (int i = 0; i < RezLiveData.Count; i++)
                    {
                        RezLiveConfig.Add(RezLiveData[i].Name, RezLiveData[i].InnerXml);
                    }
                    AddInMemCache(rezLiveConfig, MemCacheKeys.RezLiveConfig);
                    return rezLiveConfig;
                }
            }
        }


        /// LotsOfHotels configuration entries.
        /// </summary>
        private static Dictionary<string, string> lohConfig = null;

        /// <summary>
        /// Gets the dictionary containing LOH configuration.
        /// </summary>
        public static Dictionary<string, string> LOHConfig
        {
            get
            {
                if (!ShouldLoadFromFile(lohConfig, MemCacheKeys.LOHConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(lohConfig, MemCacheKeys.LOHConfig);
                }
                else
                {
                    lohConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "LOH.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList LOHData = config.ChildNodes;
                    for (int i = 0; i < LOHData.Count; i++)
                    {
                        lohConfig.Add(LOHData[i].Name, LOHData[i].InnerXml);
                    }
                    AddInMemCache(lohConfig, MemCacheKeys.LOHConfig);
                    return lohConfig;
                }
            }
        }

        private static Dictionary<string, string> tboAirconfig = null;
        /// <summary>
        /// This is for TBO Special Config File.
        /// </summary>
        public static Dictionary<string, string> TBOAirConfig
        {
            get
            {
                if (!ShouldLoadFromFile(tboAirconfig, MemCacheKeys.TBOAirConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(tboAirconfig, MemCacheKeys.TBOAirConfig);
                }
                else
                {
                    tboAirconfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "TBOAir.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList TBOData = config.ChildNodes;
                    for (int i = 0; i < TBOData.Count; i++)
                    {
                        tboAirconfig.Add(TBOData[i].Name, TBOData[i].InnerXml);
                    }
                    AddInMemCache(tboAirconfig, MemCacheKeys.TBOAirConfig);
                    return tboAirconfig;
                }
            }
        }

        //Added by brahmam 29.06.2016
        private static Dictionary<string, string> jacConfig = null;
        /// <summary>
        /// Gets the dictionary containing jac configuration
        /// </summary>
        public static Dictionary<string, string> JACConfig
        {
            get
            {
                if (!ShouldLoadFromFile(jacConfig, MemCacheKeys.JACConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(jacConfig, MemCacheKeys.JACConfig);
                }
                else
                {
                    jacConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "JAC.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList JACData = config.ChildNodes;
                    for (int i = 0; i < JACData.Count; i++)
                    {
                        jacConfig.Add(JACData[i].Name, JACData[i].InnerXml);
                    }
                    AddInMemCache(jacConfig, MemCacheKeys.JACConfig);
                    return jacConfig;
                }
            }
        }

        //Added by brahmam 03-OCT-2016
        /// EgypeExpressTravel configuration entries.
        /// </summary>
        private static Dictionary<string, string> eetConfig = null;

        /// <summary>
        /// Gets the dictionary containing EET configuration.
        /// </summary>
        public static Dictionary<string, string> EETConfig
        {
            get
            {
                if (!ShouldLoadFromFile(eetConfig, MemCacheKeys.EETConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(eetConfig, MemCacheKeys.EETConfig);
                }
                else
                {
                    eetConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "EET.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList EETData = config.ChildNodes;
                    for (int i = 0; i < EETData.Count; i++)
                    {
                        eetConfig.Add(EETData[i].Name, EETData[i].InnerXml);
                    }
                    AddInMemCache(eetConfig, MemCacheKeys.EETConfig);
                    return eetConfig;
                }
            }
        }


        //Added by brahmam 29.06.2016
        private static Dictionary<string, string> sayaraConfig = null;
        /// <summary>
        /// Gets the dictionary containing Sayara configuration
        /// </summary>
        public static Dictionary<string, string> SAYARAConfig
        {
            get
            {
                if (!ShouldLoadFromFile(sayaraConfig, MemCacheKeys.SayaraConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(sayaraConfig, MemCacheKeys.SayaraConfig);
                }
                else
                {
                    sayaraConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Sayara.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList SayaraData = config.ChildNodes;
                    for (int i = 0; i < SayaraData.Count; i++)
                    {
                        sayaraConfig.Add(SayaraData[i].Name, SayaraData[i].InnerXml);
                    }
                    AddInMemCache(sayaraConfig, MemCacheKeys.SayaraConfig);
                    return sayaraConfig;
                }
            }
        }


        /// <summary>
        ///Air India Express Configuration Entries
        /// </summary>
        private static Dictionary<string, string> airIndiaExpressConfig = null;
        /// <summary>
        /// Air India Express Configuration Entries
        /// </summary>
        public static Dictionary<string, string> AirIndiaExpressConfig
        {
            get
            {
                if (!ShouldLoadFromFile(airIndiaExpressConfig, MemCacheKeys.AirIndiaExpressConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(airIndiaExpressConfig, MemCacheKeys.AirIndiaExpressConfig);
                }
                else
                {
                    airIndiaExpressConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "AirIndiaExpress.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList inData = config.ChildNodes;
                    for (int i = 0; i < inData.Count; i++)
                    {
                        airIndiaExpressConfig.Add(inData[i].Name, inData[i].InnerXml);
                    }
                    AddInMemCache(airIndiaExpressConfig, MemCacheKeys.AirIndiaExpressConfig);
                    return airIndiaExpressConfig;
                }
            }
        }


        //added by Venkatesh for FlightInventory Services on 05-05-2017.
        private static Dictionary<string, string> flightInvConfig = null;
        /// <summary>
        /// Gets the dictionary containing AirAPI configuration
        /// </summary>
        public static Dictionary<string, string> FlightInvConfig
        {
            get
            {
                if (!ShouldLoadFromFile(flightInvConfig, MemCacheKeys.FlightInvConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(flightInvConfig, MemCacheKeys.FlightInvConfig);
                }
                else
                {
                    flightInvConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "FlightInventory.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList FlightInvData = config.ChildNodes;
                    for (int i = 0; i < FlightInvData.Count; i++)
                    {
                        flightInvConfig.Add(FlightInvData[i].Name, FlightInvData[i].InnerXml);
                    }
                    AddInMemCache(flightInvConfig, MemCacheKeys.FlightInvConfig);
                    return flightInvConfig;
                }
            }
        }

        /// <summary>
        /// Network International Configuration
        /// </summary>
        private static Dictionary<string, string> networkInternationalPayConfig = null;
        /// <summary>
        /// Gets the dictionary containing NDTV Travel configuration.
        /// </summary>
        public static Dictionary<string, string> NetworkInternationalPayConfig
        {
            get
            {
                if (networkInternationalPayConfig != null)
                {
                    return networkInternationalPayConfig;
                }
                else
                {
                    networkInternationalPayConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "NetworkInternationalPay.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList pagingData = config.ChildNodes;
                    for (int i = 0; i < pagingData.Count; i++)
                    {
                        networkInternationalPayConfig.Add(pagingData[i].Name, pagingData[i].InnerXml);
                    }
                    return networkInternationalPayConfig;
                }
            }
        }


        //Added by Brahmam 06.02.2018

        /// <summary>
        /// Agoda Configuration
        /// </summary>
        private static Dictionary<string, string> agodaConfig = null;
        /// <summary>
        /// Gets the dictionary containing Agoda configuration.
        /// </summary>
        public static Dictionary<string, string> AgodaConfig
        {
            get
            {
                if (agodaConfig != null)
                {
                    return agodaConfig;
                }
                else
                {
                    agodaConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Agoda.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList AgodaData = config.ChildNodes;
                    for (int i = 0; i < AgodaData.Count; i++)
                    {
                        agodaConfig.Add(AgodaData[i].Name, AgodaData[i].InnerXml);
                    }
                    return agodaConfig;
                }
            }
        }

        /// <summary>
        /// PK FARES Configuration
        /// </summary>
        private static Dictionary<string, string> pkFaresConfig = null;
        /// <summary>
        /// Gets the dictionary containing PK FARES configuration.
        /// </summary>
        public static Dictionary<string, string> PKFaresConfig
        {
            get
            {
                if (pkFaresConfig != null)
                {
                    return pkFaresConfig;
                }
                else
                {
                    pkFaresConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "PKFares.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList pagingData = config.ChildNodes;
                    for (int i = 0; i < pagingData.Count; i++)
                    {
                        pkFaresConfig.Add(pagingData[i].Name, pagingData[i].InnerXml);
                    }
                    return pkFaresConfig;
                }
            }
        }

        /// <summary>
        /// Sabre Configuration
        /// </summary>
        private static Dictionary<string, string> sabreConfig = null;
        /// <summary>
        /// Gets the dictionary containing Sabre configuration.
        /// </summary>
        public static Dictionary<string, string> SabreConfig
        {
            get
            {
                if (sabreConfig != null)
                {
                    return sabreConfig;
                }
                else
                {
                    sabreConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Sabre.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList pagingData = config.ChildNodes;
                    for (int i = 0; i < pagingData.Count; i++)
                    {
                        sabreConfig.Add(pagingData[i].Name, pagingData[i].InnerXml);
                    }
                    return sabreConfig;
                }
            }
        }

        /// <summary>

        /// This method checks whether data should be loaded from config files or is it already there in memcache or memory
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static bool ShouldLoadFromFile(object dataObject, string key)
        {
            bool response = false;
            if (isMemCacheEnabled)
            {
                response = !Cache.ContainsKey(key);
            }
            else
            {
                response = (dataObject == null);
            }
            return response;
        }
        /// <summary>
        /// This method gets the actual data either from memcache or from memory
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static object GetStaticData(object dataObject, string key)
        {
            object staticData;
            if (isMemCacheEnabled)
            {
                staticData = Cache.Get(key);
            }
            else
            {
                staticData = dataObject;
            }
            return staticData;
        }
        /// <summary>
        /// This method is used to add data in memcache
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="key"></param>
        private static void AddInMemCache(object dataObject, string key)
        {
            if (isMemCacheEnabled)
            {
                Cache.Add(key, dataObject);
            }
        }
        /// <summary>
        /// This method is used to remove data from memcache
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="key"></param>
        private static void RemoveFromMemCache(string key)
        {
            if (isMemCacheEnabled)
            {
                Cache.Delete(key);
            }
        }
        /// <summary>
        /// GRN Configuration
        /// </summary>
        private static Dictionary<string, string> grnconfig = null;
        /// <summary>
        /// Gets the dictionary containing GRN configuration.
        /// </summary>
        public static Dictionary<string, string> GrnConfig
        {
            get
            {
                if (grnconfig != null)
                {
                    return grnconfig;
                }
                else
                {
                    grnconfig = new Dictionary<string, string>();
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(configPath + "GRN.config.xml");
                    XmlNode config = xmldoc.SelectSingleNode("config");
                    XmlNodeList grnData = config.ChildNodes;
                    for (int i = 0; i < grnData.Count; i++)
                    {
                        grnconfig.Add(grnData[i].Name, grnData[i].InnerXml);
                    }
                    return grnconfig;
                }
            }
        }

        //Added By Hari Malla

        private static Dictionary<string, string> religareConfig = null;


        public static Dictionary<string, string> ReligareConfig
        {
            get
            {
                if (religareConfig != null)
                {
                    return religareConfig;
                }
                else
                {
                    religareConfig = new Dictionary<string, string>();
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(configPath + "Religare.config.xml");
                    XmlNode config = xmldoc.SelectSingleNode("config");
                    XmlNodeList religareData = config.ChildNodes;
                    for (int i = 0; i < religareData.Count; i++)
                    {
                        religareConfig.Add(religareData[i].Name, religareData[i].InnerXml);
                    }
                    return religareConfig;
                }
            }
        }

        #region OYO Source  --  Added by somasekhar on 04/12/2018 
        /// <summary>
        /// OYO configuration entries.
        /// </summary>
        private static Dictionary<string, string> oyoConfig = null;
        /// <summary>
        /// Gets the dictionary containing OYO configuration.
        /// </summary>
        public static Dictionary<string, string> OYOConfig
        {
            get
            {
                if (!ShouldLoadFromFile(oyoConfig, MemCacheKeys.OYOConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(oyoConfig, MemCacheKeys.OYOConfig);
                }
                else
                {
                    oyoConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "OYO.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList oyoData = config.ChildNodes;
                    for (int i = 0; i < oyoData.Count; i++)
                    {
                        oyoConfig.Add(oyoData[i].Name, oyoData[i].InnerXml);
                    }
                    AddInMemCache(oyoConfig, MemCacheKeys.DesiyaConfig);
                    return oyoConfig;
                }
            }
        }
        #endregion
        /// <summary>
        /// Jazeera configuration entries.
        /// </summary>
        private static Dictionary<string, string> jazeeraConfig = null;

        /// <summary>
        /// Gets the dictionary containing Jazeera configuration.
        /// </summary>
        public static Dictionary<string, string> JazeeraConfig
        {
            get
            {
                if (!ShouldLoadFromFile(jazeeraConfig, MemCacheKeys.JazeeraConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(jazeeraConfig, MemCacheKeys.JazeeraConfig);
                }
                else
                {
                    jazeeraConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Jazeera.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList jazeeraData = config.ChildNodes;
                    for (int i = 0; i < jazeeraData.Count; i++)
                    {
                        jazeeraConfig.Add(jazeeraData[i].Name, jazeeraData[i].InnerXml);
                    }
                    AddInMemCache(jazeeraConfig, MemCacheKeys.JazeeraConfig);
                    return jazeeraConfig;
                }
            }
        }

        /// <summary>
        /// Jazeera configuration entries.
        /// </summary>
        private static Dictionary<string, string> salamAirConfig = null;

        /// <summary>
        /// Gets the dictionary containing Jazeera configuration.
        /// </summary>
        public static Dictionary<string, string> SalamAirConfig
        {
            get
            {
                if (!ShouldLoadFromFile(salamAirConfig, MemCacheKeys.SalamAirConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(salamAirConfig, MemCacheKeys.SalamAirConfig);
                }
                else
                {
                    salamAirConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "SalamAir.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList SalamData = config.ChildNodes;
                    for (int i = 0; i < SalamData.Count; i++)
                    {
                        salamAirConfig.Add(SalamData[i].Name, SalamData[i].InnerXml);
                    }
                    AddInMemCache(salamAirConfig, MemCacheKeys.SalamAirConfig);
                    return salamAirConfig;
                }
            }
        }

        #region Gimmonix Source  --  Added by Harish on 01/Apr/2019
        /// <summary>
        /// Gimmonix configuration entries.
        /// </summary>
        private static Dictionary<string, string> gimmonixConfig = null;
        /// <summary>
        /// Gets the dictionary containing OYO configuration.
        /// </summary>
        public static Dictionary<string, string> GimmonixConfig
        {
            get
            {
                if (!ShouldLoadFromFile(gimmonixConfig, MemCacheKeys.GIMMONIXConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(gimmonixConfig, MemCacheKeys.GIMMONIXConfig);
                }
                else
                {
                    gimmonixConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "Gimmonix.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList gimmonixConfiguration = config.ChildNodes;
                    for (int i = 0; i < gimmonixConfiguration.Count; i++)
                    {
                        gimmonixConfig.Add(gimmonixConfiguration[i].Name, gimmonixConfiguration[i].InnerXml);
                    }
                    AddInMemCache(gimmonixConfig, MemCacheKeys.GIMMONIXConfig);
                    return gimmonixConfig;
                }
            }
        }
        #endregion

        #region UAPI Hotel Source  --  Added by Praveen on 07/Sep/2019
        /// <summary>
        /// Gimmonix configuration entries.
        /// </summary>
        private static Dictionary<string, string> _UAPIHotelConfig = null;
        /// <summary>
        /// Gets the dictionary containing UAPI configuration.
        /// </summary>
        public static Dictionary<string, string> UAPIHotelConfig
        {
            get
            {
                if (!ShouldLoadFromFile(_UAPIHotelConfig, MemCacheKeys.UAPIHotelConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(_UAPIHotelConfig, MemCacheKeys.UAPIHotelConfig);
                }
                else
                {
                    _UAPIHotelConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "UAPIHotel.config.xml");
                    XmlNode xmlNode = xmlDoc.SelectSingleNode("config");
                    XmlNodeList xmlNodeList = xmlNode.ChildNodes;
                    for (int i = 0; i < xmlNodeList.Count; i++)
                    {
                        _UAPIHotelConfig.Add(xmlNodeList[i].Name, xmlNodeList[i].InnerXml);
                    }
                    AddInMemCache(_UAPIHotelConfig, MemCacheKeys.UAPIHotelConfig);
                    return _UAPIHotelConfig;
                }
            }
        }
        #endregion
 #region CZA Activity --  Added by Somasekhar 
        private static Dictionary<string, string> activityServiceConfig = null;
        /// <summary>
        /// Gets the dictionary containing ActivityService configuration details.
        /// </summary>
        public static Dictionary<string, string> ActivityServiceConfig
        {
            get
            {
                if (!ShouldLoadFromFile(activityServiceConfig, MemCacheKeys.ActivityServiceConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(activityServiceConfig, MemCacheKeys.ActivityServiceConfig);
                }
                else
                {
                    activityServiceConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "ActivityService.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList activityServiceData = config.ChildNodes;
                    for (int i = 0; i < activityServiceData.Count; i++)
                    {
                        activityServiceConfig.Add(activityServiceData[i].Name, activityServiceData[i].InnerXml);
                    }
                    AddInMemCache(activityServiceConfig, MemCacheKeys.ActivityServiceConfig);
                    return activityServiceConfig;
                }
            }

        }
        #endregion

    



        /// <summary>
        ///Babylon Configuration Entries
        /// </summary>
        private static Dictionary<string, string> babylonConfig = null;
        /// <summary>
        /// Babylon Configuration Entries
        /// </summary>
        public static Dictionary<string, string> BabylonConfig
        {
            get
            {
                if (!ShouldLoadFromFile(babylonConfig, MemCacheKeys.BabylonConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(babylonConfig, MemCacheKeys.BabylonConfig);
                }
                else
                {
                    babylonConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "BabylonFares.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList inData = config.ChildNodes;
                    for (int i = 0; i < inData.Count; i++)
                    {
                        babylonConfig.Add(inData[i].Name, inData[i].InnerXml);
                    }
                    AddInMemCache(babylonConfig, MemCacheKeys.BabylonConfig);
                    return babylonConfig;
                }
            }
        }

        #region Talixo Configuration Entries
        private static Dictionary<string, string> talixoConfig = null;
        public static Dictionary<string, string> TalixoConfig
        {
            get
            {
                if (!ShouldLoadFromFile(talixoConfig, MemCacheKeys.GTAConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(talixoConfig, MemCacheKeys.GTAConfig);
                }
                else
                {
                    talixoConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "TalixoConfig.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList TalixoData = config.ChildNodes;
                    for (int i = 0; i < TalixoData.Count; i++)
                    {
                        talixoConfig.Add(TalixoData[i].Name, TalixoData[i].InnerXml);
                    }
                    AddInMemCache(talixoConfig, MemCacheKeys.TalixoConfig);
                    return talixoConfig;
                }
            }
        }
        #endregion
        #region Illusions Hotel Source  --  Added by Krishna Phani on 14/Apr/2020
        /// <summary>
        /// Illusion configuration entries.
        /// </summary>
        private static Dictionary<string, string> _IllusionsHotelConfig = null;
        /// <summary>
        /// Gets the dictionary containing UAPI configuration.
        /// </summary>
        public static Dictionary<string, string> IllusionsHotelConfig
        {
            get
            {
                if (!ShouldLoadFromFile(_IllusionsHotelConfig, MemCacheKeys.IllusionsHotelConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(_IllusionsHotelConfig, MemCacheKeys.IllusionsHotelConfig);
                }
                else
                {
                    _IllusionsHotelConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "IllusionsHotel.config.xml");
                    XmlNode xmlNode = xmlDoc.SelectSingleNode("config");
                    XmlNodeList xmlNodeList = xmlNode.ChildNodes;
                    for (int i = 0; i < xmlNodeList.Count; i++)
                    {
                        _IllusionsHotelConfig.Add(xmlNodeList[i].Name, xmlNodeList[i].InnerXml);
                    }
                    AddInMemCache(_IllusionsHotelConfig, MemCacheKeys.IllusionsHotelConfig);
                    return _IllusionsHotelConfig;
                }
            }
        }
        #endregion
        #region for HotelExtraNet
        /// <summary>
        /// Config for Hotel Connect Integration
        /// </summary>
        private static Dictionary<string, string> hotelExtranetConfig = null;
        public static Dictionary<string, string> HotelExtranetConfig
        {
            get
            {
                if (!ShouldLoadFromFile(hotelExtranetConfig, MemCacheKeys.HotelExtranetConfig))
                {
                    return (Dictionary<string, string>)GetStaticData(hotelExtranetConfig, MemCacheKeys.HotelExtranetConfig);
                }
                else
                {
                    hotelExtranetConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "HotelExtranet.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList hotelConnectData = config.ChildNodes;
                    for (int i = 0; i < hotelConnectData.Count; i++)
                    {
                        hotelExtranetConfig.Add(hotelConnectData[i].Name, hotelConnectData[i].InnerXml);
                    }
                    AddInMemCache(hotelExtranetConfig, MemCacheKeys.HotelExtranetConfig);
                    return hotelExtranetConfig;
                }
            }
        }
        #endregion

        /// <summary>
        /// RazorPay Configuration
        /// </summary>
        private static Dictionary<string, string> razorPayConfig = null;
        /// <summary>
        /// Gets the dictionary containing RazorPay configuration.
        /// </summary>
        public static Dictionary<string, string> RazorPayConfig
        {
            get
            {
                if (razorPayConfig != null)
                {
                    return razorPayConfig;
                }
                else
                {
                    razorPayConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "RazorPay.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList pagingData = config.ChildNodes;
                    for (int i = 0; i < pagingData.Count; i++)
                    {
                        razorPayConfig.Add(pagingData[i].Name, pagingData[i].InnerXml);
                    }
                    return razorPayConfig;
                }
            }
        }


        /// <summary>
        /// PayFort Configuration
        /// </summary>
        private static Dictionary<string, string> payFortPayConfig = null;
        /// <summary>
        /// Gets the dictionary containing PayFort configuration.
        /// </summary>
        public static Dictionary<string, string> PayFortlPayConfig
        {
            get
            {
                if (payFortPayConfig != null)
                {
                    return payFortPayConfig;
                }
                else
                {
                    payFortPayConfig = new Dictionary<string, string>();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configPath + "PayFortPay.config.xml");
                    XmlNode config = xmlDoc.SelectSingleNode("config");
                    XmlNodeList pagingData = config.ChildNodes;
                    for (int i = 0; i < pagingData.Count; i++)
                    {
                        payFortPayConfig.Add(pagingData[i].Name, pagingData[i].InnerXml);
                    }
                    return payFortPayConfig;
                }
            }
        }


    }
}

