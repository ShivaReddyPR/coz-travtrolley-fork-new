﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Xml;
using CT.Configuration;
using CT.Core;
using CT.BookingEngine;
using System.Data;
using System.IO.Compression;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net;
using static CT.BookingEngine.TransferSearchResult;
using Newtonsoft.Json.Linq;
using System.Globalization;

namespace Talixo
{
    public class TransferTalixo : IDisposable
    {
        private string JsonLogPath = string.Empty; //Json Data Log

        private string ApiKey = string.Empty; //Talixo Api Key

        private string VehicleListPath = string.Empty;

        private string CreateBookingPath = string.Empty;

        private string RetriveBookingPath = string.Empty;
       
        Dictionary<string, decimal> exchangeRates;
       
        int decimalPoint;
      
        string agentCurrency;
       
        string sourceCountryCode;

        string sessionId = string.Empty;
        
        #region Properties
        /// <summary>
        /// Exchange rates to be used for conversion in Agent currency from suplier currency
        /// </summary>
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        /// <summary>
        /// Decimal value to be rounded off
        /// </summary>

        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        /// <summary>
        /// Base currency used by the Agent
        /// </summary>
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        /// <summary>
        /// Api source countryCode
        /// </summary>
        public string SourceCountryCode
        {
            get { return sourceCountryCode; }
            set { sourceCountryCode = value; }
        }

        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }

        decimal rateOfExchange = 1;
        #endregion
        // Talixo API calling Methods
        private async Task<string> SendPostRequest(string url, object apiParam, string apiKey)
        {
            string result = string.Empty;
            try
            {
                using (var client = new HttpClient())                    
                using (var request = new HttpRequestMessage(HttpMethod.Post, url))
                {
                    client.Timeout = TimeSpan.FromMinutes(30);
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                    client.DefaultRequestHeaders.Add("Partner", apiKey);
                    var json = JsonConvert.SerializeObject(apiParam);
                    using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                    {
                        var response = await client.PostAsync(url, stringContent);
                        // response.EnsureSuccessStatusCode();

                        using (HttpContent content = response.Content)
                        {
                            result = content.ReadAsStringAsync().Result;
                            return result;
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Audit.Add(EventType.TransferSearch, Severity.High, 0, "Exception returned from Talixo. Error Message:" + err.Message + " | " + DateTime.Now, "");
                return result;
            }
        }

        private async Task<CancellationResponse> SendDeleteRequest(string url, object apiParam, string apiKey)
        {
            string result = string.Empty;
            CancellationResponse cancellation = new CancellationResponse();
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromMinutes(30);
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                    client.DefaultRequestHeaders.Add("Partner", apiKey);
                    var response = await client.DeleteAsync(url+ apiParam.ToString());
                    if (response.IsSuccessStatusCode)
                    {
                        cancellation.status = true;
                        cancellation.message = "Cancellation success";
                        
                        return cancellation;
                    }
                    else
                    {
                        using (HttpContent content = response.Content)
                        {
                            string responseResult = content.ReadAsStringAsync().Result;
                            cancellation.status = false;
                            cancellation.message = responseResult;

                            return cancellation; ;
                        }
                    }                    
                }
            }
            catch (Exception err)
            {
                Audit.Add(EventType.TransferSearch, Severity.High, 0, "Exception returned from Talixo. Error Message:" + err.Message + " | " + DateTime.Now, "");
                
                cancellation.status = false;
                cancellation.message = "Cancellation Failed";

                return cancellation; ;
               
            }
        }

        private async Task<string> SendGetRequest(string url, object apiParam, string apiKey)
        {

            string result = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromMinutes(30);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("Partner", apiKey);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //GET Method  
                    HttpResponseMessage response = await client.GetAsync(url+ apiParam);
                    if (response.IsSuccessStatusCode)
                    {
                        result =await response.Content.ReadAsStringAsync();
                        return result;
                    }
                    else
                    {                        
                        return result;
                    }
                }
            }
            catch (Exception err)
            {
                Audit.Add(EventType.TransferSearch, Severity.High, 0, "Exception returned from Talixo. Error Message:" + err.Message + " | " + DateTime.Now, "");
                return result;
            }
        }


        public TransferSearchResult[] GetTransferAvailability(TransferRequest req, decimal markup, string markupType, decimal discount, string discounttype,long LocationId,int UserID)
        {
            List<TransferSearchResult> searchRes = new List<TransferSearchResult>();
            try
            {
                object ApiParameters = RequestParameters(req);
                string RequestFilePath = @"" + JsonLogPath + "_TalixoAvailableVehicleRequest_"+ sessionId + ".txt";
                string WriteJsonRequest = JsonConvert.SerializeObject(ApiParameters);
                System.IO.File.WriteAllText(RequestFilePath, WriteJsonRequest);
                //write string to file
                
                if (ApiParameters!=null)
                {
                    var SendReq = System.Threading.Tasks.Task.Run(() => SendPostRequest(VehicleListPath, ApiParameters, ApiKey));
                    SendReq.Wait();
                    string RequestResponse = SendReq.Result;
                    JObject RequestResponseJson = JObject.Parse(RequestResponse);
                    if (RequestResponseJson["errors"] == null)
                    {
                        SearchRequestAPIResponseModels ApiResponseObj = JsonConvert.DeserializeObject<SearchRequestAPIResponseModels>(RequestResponse);
                        string ResponseFilePath = @"" + JsonLogPath + "_TalixoAvailableVehicleResponse_" + sessionId + ".txt";
                        string WriteJsonResponse = JsonConvert.SerializeObject(ApiResponseObj);
                        System.IO.File.WriteAllText(ResponseFilePath, WriteJsonResponse);
                        searchRes.Add(ApiResonseToTranferSearch(ApiResponseObj, req, markup, markupType, discount, discounttype,LocationId,UserID));
                    }
                    else
                    {
                      Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                      {
                        " Talixo:GetTransferAvailability,Error Message:",
                        "Available list Failed : ",RequestResponseJson["errors"][0]["errors"].ToString()
                      }), "");
                    }
                }               
                return searchRes.ToArray();
            }
            catch(Exception e)
            {
                Audit.Add(EventType.TransferSearch, Severity.High, 0, "Exception returned from Transfer Talixo dll. Error Message:" + e.Message + " | " + DateTime.Now, "");
                return searchRes.ToArray();
            }
           
        }

        private object RequestParameters(TransferRequest requestObject)
        {
            try
            {
                var ReqParam = new
                {
                    //start_iata_code = requestObject.PickUpPoint.PickUpIATACode,
                    start_point_lat = requestObject.PickUpPoint.PickUpLatitude,
                    start_point_lng = requestObject.PickUpPoint.PickUpLongitude,
                    //end_iata_code = requestObject.DropOffPoint.DropOffIATACode,
                    end_point_lat = requestObject.DropOffPoint.DropOffLatitude,
                    end_point_lng = requestObject.DropOffPoint.DropOffLongitude,
                    start_time_date = requestObject.TrasnferDate.ToString("yyyy-MM-dd"),
                    start_time_time = requestObject.PickUpTime,
                    passengers = requestObject.NumberOfPassengers,
                    sport_luggage = requestObject.SportLuggage,
                    animals = requestObject.AnimalLuggage,
                    luggage = requestObject.Luggage,
                    children_seat_1 = requestObject.ChildSeat1 > 0 ? requestObject.ChildSeat1 : (int?)null,
                    children_seat_2 = requestObject.ChildSeat2 > 0 ? requestObject.ChildSeat2 : (int?)null,
                    promo_code = requestObject.VoucherCode
                };
                return ReqParam;
            }
            catch(Exception e)
            {
                Audit.Add(EventType.TransferSearch, Severity.High, 0, "Exception returned from Transfer Talixo dll. Error Message:" + e.Message + " | " + DateTime.Now, "");
                var ReqParam = new { };
                return ReqParam;
            }
            
        }

        private TransferSearchResult ApiResonseToTranferSearch(SearchRequestAPIResponseModels apiResponse, TransferRequest requestObject, decimal markup, string markupType, decimal discount, string discounttype,long LocationId,int UserID)
        {
            TransferSearchResult transferSearchResults = new TransferSearchResult();
            List<TransferVehicleDetails> transferVehicels = new List<TransferVehicleDetails>();
            try
            {
                string[] NationalityDetails = requestObject.NationalityCode.Split('|');
                foreach (var taxi in apiResponse.taxis)
                {
                    TransferVehicleDetails transferVehicleDetails = new TransferVehicleDetails();
                    transferVehicleDetails.ImageUrl = taxi.image_url;
                    transferVehicleDetails.Vehicle = taxi.car_model;
                    transferVehicleDetails.VehicleCode = taxi.id;
                    transferVehicleDetails.BufferTime = taxi.included_waiting_time.ToString();
                    transferVehicleDetails.Category = taxi.booking_category;
                    transferVehicleDetails.ItemPriceCurrency = taxi.currency_code;
                    transferVehicleDetails.ItemPrice = taxi.regular_price;
                    transferVehicleDetails.DiscountPrice = taxi.discount_price;
                    transferVehicleDetails.Seats = taxi.seats;
                    transferVehicleDetails.Luggage = taxi.luggage;
                    transferVehicleDetails.Animal = false;
                    transferVehicleDetails.SportsLuggage = 0;
                    transferVehicleDetails.WheelChair = false;
                    transferVehicleDetails.ChildrenSeat1 = string.Empty;
                    transferVehicleDetails.ChildrenSeat2 = string.Empty;
                    if (taxi.regular_price != taxi.discount_price)
                    {
                        transferVehicleDetails.VoucherStatus = true;
                    }
                    else
                    {
                        transferVehicleDetails.VoucherStatus = false;
                    }
                    string cc;
                    cc = (!string.IsNullOrEmpty(transferVehicleDetails.ItemPriceCurrency) ? transferVehicleDetails.ItemPriceCurrency : "AED");
                    rateOfExchange = (exchangeRates.ContainsKey(cc) ? exchangeRates[cc] : 1);
                    decimal totalprice = Convert.ToDecimal(taxi.discount_price);
                    decimal transferTotalPrice = 0m;
                    transferTotalPrice = Math.Round((totalprice) * rateOfExchange, decimalPoint);
                    transferVehicleDetails.PriceInfo = new PriceAccounts();
                    transferVehicleDetails.PriceInfo.SupplierCurrency = transferVehicleDetails.ItemPriceCurrency;
                    transferVehicleDetails.PriceInfo.SupplierPrice = Math.Round(totalprice, decimalPoint);
                    transferVehicleDetails.PriceInfo.RateOfExchange = rateOfExchange;
                    transferVehicleDetails.SellingFare = transferTotalPrice;
                    transferVehicleDetails.TotalPrice = transferVehicleDetails.SellingFare + (markupType == "F" ? markup : transferVehicleDetails.SellingFare * (markup / 100));
                    transferVehicleDetails.PriceInfo.Discount = 0;
                    if (discounttype == "F")
                    {
                        transferVehicleDetails.PriceInfo.Discount = discount;
                    }
                    else
                    {
                        transferVehicleDetails.PriceInfo.Discount = Math.Round(transferVehicleDetails.TotalPrice * (discount / 100), decimalPoint);
                    }
                    transferVehicleDetails.PriceInfo.Discount = transferVehicleDetails.PriceInfo.Discount > 0 ? transferVehicleDetails.PriceInfo.Discount : 0;
                    transferVehicleDetails.PriceInfo.NetFare = Math.Round((totalprice) * rateOfExchange, decimalPoint);

                    transferVehicleDetails.PriceInfo.AccPriceType = PriceType.NetFare;
                    transferVehicleDetails.PriceInfo.RateOfExchange = rateOfExchange;
                    transferVehicleDetails.PriceInfo.Currency = agentCurrency;
                    transferVehicleDetails.PriceInfo.CurrencyCode = agentCurrency;
                    transferVehicleDetails.PriceInfo.DiscountType = discounttype;
                    transferVehicleDetails.PriceInfo.DiscountValue = discount;
                    transferVehicleDetails.PriceInfo.MarkupValue = markup;
                    decimal TotalMarkup = 0;
                    if (markupType == "F")
                    {
                        TotalMarkup = markup;
                    }
                    else
                    {
                        TotalMarkup = Math.Round(transferVehicleDetails.SellingFare * (markup / 100), decimalPoint);
                    }
                    decimal OutPutVat = 0;
                    decimal InPutVat = 0;
                    if (requestObject.LoginCountryCode == "IN")
                    {
                        List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                        decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, TotalMarkup, LocationId, UserID);
                        transferVehicleDetails.PriceInfo.GSTDetailList = gstTaxList;
                        OutPutVat += Math.Round(gstAmount, decimalPoint);
                        transferVehicleDetails.PriceInfo.OutputVATAmount = OutPutVat;

                    }
                    else
                    {
                        PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(NationalityDetails[1].ToString(), requestObject.LoginCountryCode, sourceCountryCode, (int)ProductType.Transfers, Module.Transfer.ToString(), DestinationType.Domestic.ToString());
                        if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                        {

                            decimal toatalInPutVat = priceTaxDet.InputVAT.CalculateVatAmount(transferTotalPrice + TotalMarkup - transferVehicleDetails.PriceInfo.Discount, ref InPutVat, decimalPoint);
                            transferVehicleDetails.PriceInfo.InputVATAmount = InPutVat;
                        }
                        if (priceTaxDet != null && priceTaxDet.OutputVAT != null)
                        {

                            OutPutVat = priceTaxDet.OutputVAT.CalculateVatAmount(transferTotalPrice + TotalMarkup - transferVehicleDetails.PriceInfo.Discount, TotalMarkup, decimalPoint);
                            transferVehicleDetails.PriceInfo.OutputVATAmount = OutPutVat;
                        }

                    }
                    transferVehicleDetails.TotalPrice = transferVehicleDetails.TotalPrice - transferVehicleDetails.PriceInfo.Discount;
                    transferVehicleDetails.PriceInfo.PublishedFare = transferVehicleDetails.TotalPrice;
                    //transferVehicleDetails.PriceInfo.Tax = OutPutVat + InPutVat;
                    transferVehicleDetails.TotalPrice = Math.Ceiling(Math.Round(transferVehicleDetails.TotalPrice + OutPutVat + InPutVat, decimalPoint));

                    transferVehicleDetails.PriceInfo.Markup = Math.Round(TotalMarkup, decimalPoint);
                    transferVehicleDetails.PriceInfo.MarkupType = markupType;
                    transferVehicels.Add(transferVehicleDetails);
                }
                foreach (var limousine in apiResponse.limousines)
                {
                    TransferVehicleDetails transferVehicleDetails = new TransferVehicleDetails();
                    transferVehicleDetails.ImageUrl = limousine.image_url;
                    transferVehicleDetails.Vehicle = limousine.car_model;
                    transferVehicleDetails.VehicleCode = limousine.id;
                    transferVehicleDetails.BufferTime = limousine.included_waiting_time.ToString();
                    transferVehicleDetails.Category = limousine.booking_category;
                    transferVehicleDetails.ItemPriceCurrency = limousine.currency_code;
                    transferVehicleDetails.ItemPrice = limousine.regular_price;
                    transferVehicleDetails.DiscountPrice = limousine.discount_price;
                    transferVehicleDetails.Seats = limousine.seats;
                    transferVehicleDetails.Luggage = limousine.luggage;
                    transferVehicleDetails.Animal = limousine.animals;
                    transferVehicleDetails.SportsLuggage = limousine.sport_luggage;
                    transferVehicleDetails.WheelChair = limousine.wheelchairs;
                    transferVehicleDetails.ChildrenSeat1 = limousine.children_seat_1;
                    transferVehicleDetails.ChildrenSeat2= limousine.children_seat_2;
                    if (limousine.regular_price != limousine.discount_price)
                    {
                        transferVehicleDetails.VoucherStatus = true;
                    }
                    else
                    {
                        transferVehicleDetails.VoucherStatus = false;
                    }
                    string cc;
                    cc = (!string.IsNullOrEmpty(transferVehicleDetails.ItemPriceCurrency) ? transferVehicleDetails.ItemPriceCurrency : "AED");
                    rateOfExchange = (exchangeRates.ContainsKey(cc) ? exchangeRates[cc] : 1);
                    decimal totalprice = Convert.ToDecimal(limousine.discount_price);
                    decimal transferTotalPrice = 0m; 
                    transferTotalPrice = Math.Round((totalprice) * rateOfExchange, decimalPoint); 
                    transferVehicleDetails.PriceInfo = new PriceAccounts();
                    transferVehicleDetails.PriceInfo.SupplierCurrency = transferVehicleDetails.ItemPriceCurrency;
                    transferVehicleDetails.PriceInfo.SupplierPrice = Math.Round(totalprice, decimalPoint);
                    transferVehicleDetails.PriceInfo.RateOfExchange = rateOfExchange;                   
                    transferVehicleDetails.SellingFare = transferTotalPrice;
                    transferVehicleDetails.TotalPrice = transferVehicleDetails.SellingFare + (markupType == "F" ? markup : transferVehicleDetails.SellingFare * (markup / 100));
                    transferVehicleDetails.PriceInfo.Discount = 0;                                      
                    if (discounttype == "F")
                    {
                        transferVehicleDetails.PriceInfo.Discount = discount;
                    }
                    else
                    {
                        transferVehicleDetails.PriceInfo.Discount = Math.Round(transferVehicleDetails.TotalPrice * (discount / 100), decimalPoint);
                    }
                    transferVehicleDetails.PriceInfo.Discount = transferVehicleDetails.PriceInfo.Discount > 0 ? transferVehicleDetails.PriceInfo.Discount : 0;
                    transferVehicleDetails.PriceInfo.NetFare = Math.Round((totalprice) * rateOfExchange, decimalPoint);
                    
                    transferVehicleDetails.PriceInfo.AccPriceType = PriceType.NetFare;
                    transferVehicleDetails.PriceInfo.RateOfExchange = rateOfExchange;
                    transferVehicleDetails.PriceInfo.Currency = agentCurrency;
                    transferVehicleDetails.PriceInfo.CurrencyCode = agentCurrency;
                    transferVehicleDetails.PriceInfo.DiscountType = discounttype;
                    transferVehicleDetails.PriceInfo.DiscountValue = discount;
                    transferVehicleDetails.PriceInfo.MarkupValue = markup;
                    decimal TotalMarkup = 0;
                    if (markupType == "F")
                    {
                        TotalMarkup = markup;
                    }
                    else
                    {
                        TotalMarkup = Math.Round(transferVehicleDetails.SellingFare * (markup / 100),decimalPoint);
                    }
                    decimal OutPutVat = 0;
                    decimal InPutVat = 0;                    
                    if (requestObject.LoginCountryCode == "IN")
                    {
                        List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                        decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, TotalMarkup, LocationId, UserID);
                        transferVehicleDetails.PriceInfo.GSTDetailList = gstTaxList;
                        OutPutVat += Math.Round(gstAmount, decimalPoint);
                        transferVehicleDetails.PriceInfo.OutputVATAmount = OutPutVat;

                    }
                    else
                    {
                        PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(NationalityDetails[1].ToString(), requestObject.LoginCountryCode, sourceCountryCode, (int)ProductType.Transfers, Module.Transfer.ToString(), DestinationType.Domestic.ToString());
                        if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                        {
                                                      
                            decimal toatalInPutVat = priceTaxDet.InputVAT.CalculateVatAmount(transferTotalPrice+ TotalMarkup- transferVehicleDetails.PriceInfo.Discount, ref InPutVat, decimalPoint);
                            transferVehicleDetails.PriceInfo.InputVATAmount = InPutVat;
                        }
                        if (priceTaxDet != null && priceTaxDet.OutputVAT != null)
                        {                          
                                                       
                            OutPutVat = priceTaxDet.OutputVAT.CalculateVatAmount(transferTotalPrice + TotalMarkup - transferVehicleDetails.PriceInfo.Discount, TotalMarkup, decimalPoint);
                            transferVehicleDetails.PriceInfo.OutputVATAmount = OutPutVat;
                        }

                    }
                    transferVehicleDetails.TotalPrice = transferVehicleDetails.TotalPrice - transferVehicleDetails.PriceInfo.Discount;
                    transferVehicleDetails.PriceInfo.PublishedFare = transferVehicleDetails.TotalPrice;
                    //transferVehicleDetails.PriceInfo.Tax = OutPutVat + InPutVat;
                    transferVehicleDetails.TotalPrice = Math.Ceiling(Math.Round(transferVehicleDetails.TotalPrice + OutPutVat + InPutVat,decimalPoint));
                    
                    transferVehicleDetails.PriceInfo.Markup = Math.Round(TotalMarkup, decimalPoint);
                    transferVehicleDetails.PriceInfo.MarkupType = markupType;
                    transferVehicels.Add(transferVehicleDetails);
                }
                DateTime startDate = DateTime.Parse(apiResponse.start_time);
                DateTime endDate = DateTime.Parse(apiResponse.end_time);
                TimeSpan timeDiff = endDate.Subtract(startDate);
                transferSearchResults.Distance = apiResponse.distance.ToString();
                transferSearchResults.ApproximateTransferTime = (decimal)timeDiff.TotalMinutes;
                transferSearchResults.Vehicles = transferVehicels.ToArray();
                transferSearchResults.Source = TransferBookingSource.TALIXO;                
                transferSearchResults.StartTime = apiResponse.start_time;
                transferSearchResults.EndTime = apiResponse.end_time;

                transferSearchResults.StrStartTime = DateTimeOffset.ParseExact(apiResponse.start_time, "yyyy-MM-dd'T'HH:mm:sszzz", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd'T'HH:mm");
                transferSearchResults.StrEndTime = DateTimeOffset.ParseExact(apiResponse.end_time, "yyyy-MM-dd'T'HH:mm:sszzz", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd'T'HH:mm");
                return transferSearchResults;
            }
            catch(Exception e)
            {
                Audit.Add(EventType.TransferSearch, Severity.High, 0, "Exception returned from Transfer Talixo dll. Error Message:" + e.Message + " | " + DateTime.Now, "");
                return transferSearchResults;
            }
            
        }

        public BookingResponse GetTransferBooking(ref TransferItinerary itineary)
        {
            BookingResponse bookingRes = default(BookingResponse);
            try
            {
                string bookingReference = "BookTRef" + TraceIdGeneration();
                object ApiParameters = BookingRequestParameter(itineary, bookingReference);
                string RequestFilePath = @"" + JsonLogPath + "_TalixoBookingRequest_" + sessionId + ".txt";
                string WriteJsonRequest = JsonConvert.SerializeObject(ApiParameters);
                System.IO.File.WriteAllText(RequestFilePath, WriteJsonRequest);
                if (ApiParameters != null)
                {
                    var SendReq = System.Threading.Tasks.Task.Run(() => SendPostRequest(CreateBookingPath, ApiParameters, ApiKey));
                    SendReq.Wait();
                    string RequestResponse = SendReq.Result;
                    JObject RequestResponseJson = JObject.Parse(RequestResponse);
                    string ResponseFilePath = @"" + JsonLogPath + "_TalixoBookingResponse_" + sessionId + ".txt";
                    string WriteJsonResponse = JsonConvert.SerializeObject(ApiParameters);
                    System.IO.File.WriteAllText(ResponseFilePath, WriteJsonResponse);
                    if (RequestResponseJson["errors"]!=null)
                    {                       
                        Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                        {
                        " Talixo:BookingAPIToBookingResponse,Error Message:",
                        "Booking Failed : ",RequestResponseJson["errors"][0]["errors"].ToString()
                        }), "");
                        bookingRes.Status = BookingResponseStatus.Failed;
                        itineary.BookingStatus = TransferBookingStatus.Error;
                    }
                    else
                    {
                        TalixoBookingResponseObject ApiResponseObj = JsonConvert.DeserializeObject<TalixoBookingResponseObject>(RequestResponse);
                        bookingRes = BookingAPIToBookingResponse(ApiResponseObj, itineary);                       
                    }
                }
            }
            catch(Exception e)
            {
                Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                        {
                        " Talixo:BookingAPIToBookingResponse,Error Message:",
                        e.Message,
                        }), "");
            }
            
            return bookingRes;
        }

        public BookingResponse BookingAPIToBookingResponse(TalixoBookingResponseObject responseObj, TransferItinerary itineary)
        {
            BookingResponse bookingRes = default(BookingResponse);
            try
            {
                if (responseObj.booking_number != null)
                {
                    bookingRes.ProdType = ProductType.Transfers;
                    itineary.BookingReference = responseObj.booking_number;
                    itineary.ConfirmationNo= responseObj.booking_number;
                    bookingRes.ConfirmationNo = responseObj.booking_number;
                    itineary.BookingStatus = TransferBookingStatus.Confirmed;
                    bookingRes.Status = BookingResponseStatus.Successful;
                }
                else
                {
                    Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                        {
                        " Talixo:BookingAPIToBookingResponse,Error Message:",
                        "Booking Failed",
                        }), "");
                    bookingRes.Status = BookingResponseStatus.Failed;
                    itineary.BookingStatus = TransferBookingStatus.Error;
                    
                }
                return bookingRes;
            }
            catch
            {
                Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                        {
                        " Talixo:BookingAPIToBookingResponse,Error Message:",
                        "Booking Failed",
                        }), "");
                bookingRes.Status = BookingResponseStatus.Failed;
                itineary.BookingStatus = TransferBookingStatus.Error;
                return bookingRes;
            }
            
        }

        private object BookingRequestParameter(TransferItinerary itineary,string BookingRef)
        {
            try
            {
                var BookingParam = new
                {
                    start_point_lat = itineary.PickUpLatitude,
                    start_point_lng = itineary.PickUpLongitude,
                    end_point_lat = itineary.DropOffLatitude,
                    end_point_lng = itineary.DropOffLongitude,
                    start_time_date = itineary.TransferDate.ToString("yyyy-MM-dd"),
                    start_time_time = itineary.TransferTime,
                    passengers = itineary.NumOfPax,
                    sport_luggage = itineary.SportLuggage,
                    animals = itineary.AnimalLuggage,
                    luggage = itineary.Luggage,
                    airport_pickup_buffer = itineary.PickUpBuffer,
                    first_name = itineary.PassengerDetails.PassengerFirstName,
                    last_name = itineary.PassengerDetails.PassengerLastName,
                    mobile = itineary.PassengerDetails.PassengerMobile,
                    email = itineary.PassengerDetails.PassengerEmail,
                    vehicle = itineary.ItemCode,
                    payment_method = "default",
                    talixo_shield_text = itineary.PickUpRemarks,
                    special_wishes = itineary.DropOffRemarks,
                    flight_number = itineary.TransferDetails[0].AirlineCode + itineary.TransferDetails[0].FlightNumber
                };
                return BookingParam;
            }
            catch (Exception e)
            {
                Audit.Add(EventType.TransferSearch, Severity.High, 0, "Exception returned from Transfer Talixo dll. Error Message:" + e.Message + " | " + DateTime.Now, "");
                var ReqParam = new { };
                return ReqParam;
            }
        }

        public Dictionary<string, string> CancelBookingBooking(string refernceCode)
        {
            Dictionary<string, string> CancelInfo = new Dictionary<string, string>();
            CancellationResponse ApiResponse = new CancellationResponse();
            try
            {
                var CancellationData = new { refernceId = refernceCode };
                string RequestFilePath = @"" + JsonLogPath + "_TalixoCancelBookingRequest_"+ sessionId + "_" + refernceCode + ".txt";
                string WriteJsonRequest = JsonConvert.SerializeObject(CancellationData);
                System.IO.File.WriteAllText(RequestFilePath, WriteJsonRequest);
                if (refernceCode != "" || refernceCode != null)
                {
                    var SendReq = System.Threading.Tasks.Task.Run(() => SendDeleteRequest(CreateBookingPath, refernceCode, ApiKey));
                    SendReq.Wait();
                    ApiResponse = SendReq.Result;
                    string ResponseFilePath = @"" + JsonLogPath + "_TalixoCancelBookingResponse_" + sessionId + "_" + refernceCode + ".txt";
                    string WriteJsonResponse = JsonConvert.SerializeObject(ApiResponse);
                    System.IO.File.WriteAllText(ResponseFilePath, WriteJsonResponse);
                    if (ApiResponse.status==true)
                    {  
                        CancelInfo["ID"]="CTW-"+ refernceCode;
                        CancelInfo["Status"] = "CANCELLED";
                    }
                    else
                    {                       
                        Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                        {
                        " Talixo:Cancel Booking,Error Message:",
                        "Cancellation Failed : ",JsonConvert.SerializeObject(ApiResponse)
                        }), "");                       
                        
                    }
                    return CancelInfo;
                }
                else
                {
                    var successObj = new
                    {
                        status = false,
                        message = "Cancellation Faile: Missing Confirmation Number"
                    };
                    Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                        {
                        " Talixo:Cancel Booking,Error Message:",
                        "Cancellation Failed : ",successObj.ToString()
                        }), "");
                    return CancelInfo;
                }
               
            }
            catch(Exception e)
            {
                Audit.Add(EventType.TransferBooking, Severity.High, 0, string.Concat(new object[]
                        {
                        " Talixo:Cancel Booking,Error Message:",
                        "Cancellation Failed : ",e.Message
                        }), "");
                return CancelInfo;

            }
            
        }

       

        public TransferTalixo()
        {
            try
            {
                JsonLogPath = ConfigurationSystem.TalixoConfig["XmlLogPath"];
                JsonLogPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
                try
                {
                    if (!System.IO.Directory.Exists(JsonLogPath))
                    {
                        System.IO.Directory.CreateDirectory(JsonLogPath);
                    }
                }
                catch { }
                ApiKey = ConfigurationSystem.TalixoConfig["ApiKey"];
                VehicleListPath = ConfigurationSystem.TalixoConfig["VehicleList"];
                CreateBookingPath = ConfigurationSystem.TalixoConfig["CreateBooking"];
                RetriveBookingPath = ConfigurationSystem.TalixoConfig["RetriveBooking"];

            }
            catch(Exception e)
            {
                Audit.Add(EventType.TransferSearch, Severity.High, 0, "Exception returned from Transfer Talixo config file. Error Message:" + e.Message + " | " + DateTime.Now, "");
            }
            
        }

        private string TraceIdGeneration()
        {
            string traceid = DateTime.Now.ToString("ddMMyyyyHHmmsss");
            return "CZ" + traceid;
        }

        #region IDisposable Support
        /// <summary>
        /// To detect redundant calls
        /// </summary>
        private bool disposedValue = false; // To detect redundant calls

        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing">disposing(true/false)</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        /// <summary>
        /// ~GTA
        /// </summary>
        ~TransferTalixo()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        /// <summary>
        /// This code added to correctly implement the disposable pattern.
        /// </summary>
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

        #region Vehicle list Api response  object
        public class SearchRequestAPIResponseModels
        {
            public List<Taxi> taxis { get; set; }
            public List<Limousine> limousines { get; set; }
            public List<AvailabilityError> availability_errors { get; set; }
            public string start_time { get; set; }
            public string end_time { get; set; }
            public double distance { get; set; }
            public List<AvailableBookingClass> available_booking_classes { get; set; }
        }
        #endregion

        #region Limousine data object
        public class Limousine
        {
            public int luggage { get; set; }
            public int seats { get; set; }
            public bool animals { get; set; }
            public bool wheelchairs { get; set; }
            public int sport_luggage { get; set; }
            public string car_model { get; set; }
            public string children_seat_1 { get; set; }
            public string children_seat_2 { get; set; }
            public string id { get; set; }
            public string color { get; set; }
            public string booking_category { get; set; }
            public string extended_booking_category { get; set; }
            public string currency_symbol { get; set; }
            public string currency_code { get; set; }
            public int talixo_points { get; set; }
            public string vehicle_type { get; set; }
            public int included_waiting_time { get; set; }
            public decimal regular_price { get; set; }
            public decimal discount_price { get; set; }
            public string local_price { get; set; }
            public string frame_contract { get; set; }
            public bool tolls_included { get; set; }
            public bool gratuity_included { get; set; }
            public string search_url { get; set; }
            public string image_url { get; set; }
        }
        #endregion

        #region Availability error data object
        public class AvailabilityError
        {
            public string id { get; set; }
            public List<string> errors { get; set; }
        }
        #endregion

        #region Available booking class data object
        public class AvailableBookingClass
        {
            public string id { get; set; }
            public string name { get; set; }
            public string image_url { get; set; }
        }
        #endregion

        #region Taxi data object
        public class Taxi
        {
            public int included_waiting_time { get; set; }
            public string id { get; set; }
            public string car_model { get; set; }
            public int seats { get; set; }
            public int luggage { get; set; }
            public decimal regular_price { get; set; }
            public decimal discount_price { get; set; }
            public string currency_symbol { get; set; }
            public string currency_code { get; set; }
            public bool tolls_included { get; set; }
            public int talixo_points { get; set; }
            public decimal vat_amount { get; set; }
            public string local_price { get; set; }
            public bool gratuity_included { get; set; }
            public string image_url { get; set; }
            public string search_url { get; set; }
            public int vat_rate { get; set; }
            public string booking_category { get; set; }
            public string extended_booking_category { get; set; }           
        }
        #endregion

        #region BookingResponseObject
        public class TalixoBookingResponseObject
        {
            public string url { get; set; }
            public string booking_number { get; set; }
            public StartPoint start_point { get; set; }
            public EndPoint end_point { get; set; }
            public string start_time { get; set; }
            public string end_time { get; set; }
            public string duration { get; set; }
            public string passengers { get; set; }
            public string luggage { get; set; }
            public string sport_luggage { get; set; }
            public string animals { get; set; }
            public string wheelchairs { get; set; }
            public string children_seat_1 { get; set; }
            public string children_seat_2 { get; set; }
            public string cost_center { get; set; }
            public string special_wishes { get; set; }
            public string pin { get; set; }
            public string profile { get; set; }
            public PassengerData passenger { get; set; }
            public string flight_number { get; set; }
            public string included_waiting_time { get; set; }
            public object ad_hoc_data { get; set; }
            public string rating_data { get; set; }
            public bool is_past { get; set; }
            public bool is_cancelled { get; set; }
            public string booking_category { get; set; }
            public decimal price { get; set; }
            public string driver { get; set; }
            public string vehicle { get; set; }
            public decimal distance { get; set; }
            public bool can_be_cancelled { get; set; }
            public bool can_be_modified { get; set; }
            public string booking_type { get; set; }
            public string promo_code { get; set; }
            public string currency_symbol { get; set; }
            public string currency_code { get; set; }
            public object custom_fields { get; set; }
            public bool has_final_price { get; set; }
            public string booking_origin { get; set; }
            public string booked_by { get; set; }
            public string executor_description { get; set; }
            public PromotionUrl promotion_info { get; set; }
            public PaymentMethod payment_method { get; set; }
        }
        #endregion
        public class StartPoint
        {
            public string talixo_id { get; set; }
            public string latitude { get; set; }
            public string longitude { get; set; }
            public string types { get; set; }
            public string address { get; set; }
        }
        public class EndPoint
        {
            public string talixo_id { get; set; }
            public string latitude { get; set; }
            public string longitude { get; set; }
            public string types { get; set; }
            public string address { get; set; }
        }
        public class PasengerData
        {
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string mobile_0 { get; set; }
            public string mobile_1 { get; set; }
            public string mobile { get; set; }
            public string email { get; set; }
            public string cost_center { get; set; }
        }
        public class PromotionUrl
        {
            public string learn_more_url { get; set; }
            public string learn_more_text { get; set; }
        }
        public class PaymentMethod
        {
            public string label { get; set; }
            public string url { get; set; }
            public bool active { get; set; }
            public List<string> views { get; set; }
            public string token { get; set; }
            public bool can_be_deleted { get; set; }
            public string variant { get; set; }
            //public bool default {get;set;}
        }

        public class CancellationResponse
        {
            public bool status { get; set; }
            public string message { get; set; }
        }
    }

}
