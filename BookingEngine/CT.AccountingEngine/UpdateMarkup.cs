﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

namespace CT.AccountingEngine
{
    public class UpdateMarkupZ
    {
        #region Member Variables
        private const long NEW_RECORD = -1;
        private int _id;
        private int _mrDId;
        private int _productId;
        private string _sourceId;
        private int _createdBy;

        private int _agentId;
        private decimal _markup;
        private string _markupType;
        private decimal _discount;
        private string _discountType;

        private int _retMRId;


        #endregion

        #region properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
         public int MrDId
        {
            get { return _mrDId; }
            set { _mrDId = value; }
         }
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }
        public string SourceId
        {
            get { return _sourceId; }
            set { _sourceId = value; }
        }
         
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public decimal Markup
        {
            get { return _markup; }
            set { _markup = value; }
        }

        public string MarkupType
        {
            get { return _markupType; }
            set { _markupType = value; }
        }

        public decimal Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }

        public string DiscountType
        {
            get { return _discountType; }
            set { _discountType = value; }
        }

        
        #endregion
        public UpdateMarkupZ()
        {
            _id = -1;
            _mrDId = -1;
        }


        public static DataTable ProductGetList(ListStatus status, RecordStatus recordStatus)
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[2];
                    paramList[0] = new SqlParameter("@P_LIST_STATUS", status);
                    paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString<char>((char)recordStatus));
                    data = DBGateway.ExecuteReaderSP(SPNames.ProductTypeGetList, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }


        public static DataTable GetMarkupList()
        {
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[0];
                    data = DBGateway.ExecuteReaderSP(SPNames.GetMarkupList, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch(Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
                }
            }
            return dt;
        }



        #region Public Methods
        public void Save()
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;

            try
            {
                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;


                SqlParameter[] paramList = new SqlParameter[7];
                paramList[0] = new SqlParameter("@MRId", _id);
                paramList[1] = new SqlParameter("@ProductId", _productId);
                paramList[2] = new SqlParameter("@SourceId",_sourceId);
                paramList[3] = new SqlParameter("@CreatedBy", _createdBy);
                paramList[4] = new SqlParameter("@MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[4].Direction = ParameterDirection.Output;
                paramList[5] = new SqlParameter("@MSG_TEXT", SqlDbType.VarChar, 200);
                paramList[5].Direction = ParameterDirection.Output;
                paramList[6] = new SqlParameter("@MRId_RETURN", SqlDbType.BigInt);
                paramList[6].Direction = ParameterDirection.Output;

                DBGateway.ExecuteNonQuerySP(SPNames.MarkupRulesAddUpdate, paramList);
                string messageType = Utility.ToString(paramList[4].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[5].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                _retMRId = Utility.ToInteger(paramList[6].Value);

                SaveMarkupDeatils(cmd,_retMRId);
                trans.Commit();
            }
            catch(Exception ex)
            {
                trans.Rollback();
                Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
            }
            finally
            {
                cmd.Connection.Close();
            }
            
        }

        private void SaveMarkupDeatils(SqlCommand cmd, int mrId)
        {
            SqlParameter[] paramArr = new SqlParameter[10];
            paramArr[0] = new SqlParameter("@MRDId", _mrDId);
            paramArr[1] = new SqlParameter("@MRId", mrId);
            if (_agentId > 0)
            {
                paramArr[2] = new SqlParameter("@AgentId", AgentId);
            }
            paramArr[3] = new SqlParameter("@Markup", Markup);
            paramArr[4] = new SqlParameter("@MarkupType", MarkupType);
            paramArr[5] = new SqlParameter("@Discount", Discount);
            paramArr[6] = new SqlParameter("@DiscountType", DiscountType);
            paramArr[7] = new SqlParameter("@CreatedBy", CreatedBy);
            SqlParameter paramMsgType = new SqlParameter("@MSG_TYPE", SqlDbType.NVarChar);
            paramMsgType.Size = 10;
            paramMsgType.Direction = ParameterDirection.Output;
            paramArr[8] = paramMsgType;

            SqlParameter paramMsgText = new SqlParameter("@MSG_TEXT", SqlDbType.NVarChar);
            paramMsgText.Size = 100;
            paramMsgText.Direction = ParameterDirection.Output;
            paramArr[9] = paramMsgText;

            DBGateway.ExecuteNonQuerySP(SPNames.MarkupRuleDetailsAddUpdate, paramArr);
            if (Utility.ToString(paramMsgType.Value) == "E")
                throw new Exception(Utility.ToString(paramMsgText.Value));
        }
        #endregion

        public static DataTable Load(int agentId, string source, int prodId)
        {
            UpdateMarkup objUpdateMarkup = new UpdateMarkup();
            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {
                    paramList = new SqlParameter[3];
                    if (agentId > 1)
                    {
                        paramList[0] = new SqlParameter("@AgentId", agentId);
                    }
                    paramList[1] = new SqlParameter("@Source", source);
                    paramList[2] = new SqlParameter("@ProductId", prodId);
                    data = DBGateway.ExecuteReaderSP(SPNames.GetAgentMarkup, paramList, connection);
                    if (data !=null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
    }


}
