﻿using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CT.AccountingEngine
{
    /// <summary>
    /// Ledger transaction Class contains the data regarding each transaction
    /// </summary>
    public class LedgerTransaction
    {
        /// <summary>
        /// Unique Field txnId
        /// </summary>
        private int txnId;
        /// <summary>
        /// LedgerId field
        /// </summary>
        private int ledgerId;
        /// <summary>
        /// Amount of a transaction
        /// </summary>
        private decimal amount;
        /// <summary>
        /// narration of a transaction
        /// </summary>
        private string narration;
        /// <summary>
        /// Notes of a transaction
        /// </summary>
        private string notes;
        /// <summary>
        /// Date of transaction
        /// </summary>
        private DateTime date;
        /// <summary>
        /// agencyId field
        /// </summary>
        private int agencyId;
        /// <summary>
        /// Unique ID of the member who created this record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Date when the record was created
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// Unique ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;
        /// <summary>
        /// Date when the record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// Field tells whether this transaction is for LCCs or not
        /// </summary>
        private bool isLCC;
        /// <summary>
        /// referenceId field contains TicketId ot paymentDetailId(as for the reference Type)
        /// </summary>
        private int referenceId;
        /// <summary>
        /// referenceType(like TicketRefund,Ticket Created etc) is the for Transaction
        /// </summary>        
        private ReferenceType referenceType;
        /// <summary>
        /// paxName field contains Passenger First Name for transaction
        /// </summary>
        private string paxFirstName = "-";
        /// <summary>
        /// paxName field contains Passenger Last Name for transaction
        /// </summary>
        private string paxLastName = string.Empty;
        /// <summary>
        /// paxName field contains Sector for transaction
        /// </summary>
        private string sector = "-";
        /// <summary>
        /// paxName field contains TicketNo for transaction
        /// </summary>
        private string ticketNo = "-";
        /// <summary>
        /// paxName field contains Check No for transaction
        /// </summary>
        private string checkNo = "-";
        /// <summary>
        /// UniqueId Property txnId
        /// </summary>
        private decimal runningTotal;

        private string transType = "B2B";
        //Added Brahmam 14.04.2018
        //AdminLedger Page we are showing HotelPax count 
        private int paxCount;
        //Showing agent wise Currency
        string agentCurrency;
        //Showing agent wise agentCurrentBalance
        decimal agentCurrentBalance;
        //Showing agent wise agentDecimal
        int agentDecimal;
        //Showing agent wise agentName
        string agentName;
        //Showing agent wise invoiceNumber
        string invoiceNumber;
        //Celling total amount TBOFlight source only
        int flightSource;
        //Balance TillDate
        decimal balanceTillDate;
        /// <summary>
        /// PaymentMode for agent credit transactions
        /// </summary>
        int paymentMode;
        /// <summary>
        /// Agent_Credit_Limit for credit limit.
        /// </summary>
        decimal agent_Credit_Limit;
        /// <summary>
        /// agent_Credit_Days for credit days.
        /// </summary>
        int agent_Credit_Days;
        /// <summary>
        /// agent_Credit_Buffer_Amount for credit buffer Amount.
        /// </summary>
        decimal agent_Credit_Buffer_Amount;
        /// <summary>
        /// agent_Payment_Mode for Payment Mode.
        /// </summary>
        string agent_Payment_Mode;
        public int TxnId
        {
            get
            {
                return this.txnId;
            }
            set
            {
                this.txnId = value;
            }
        }
        /// <summary>
        /// Ledger Id property of a transaction
        /// </summary>
        public int LedgerId
        {
            get
            {
                return this.ledgerId;
            }
            set
            {
                this.ledgerId = value;
            }
        }
        /// <summary>
        /// Gets or Set the amount of a transaction
        /// </summary>
        public decimal Amount
        {
            get
            {
                return this.amount;
            }
            set
            {
                this.amount = value;
            }
        }
        /// <summary>
        /// Get or Sets the narration of a transaction
        /// </summary>
        public NarrationBuilder Narration
        {
            get
            {
                NarrationBuilder obj = new NarrationBuilder();
                string[] arr = this.narration.Split(new char[]
				{
					'|'
				});
                obj.Remarks = arr[0];
                if (arr.GetUpperBound(0) > 1)
                {
                    obj.PaxName = arr[1];
                    obj.Sector = arr[2];
                    obj.TicketNo = arr[3];
                    obj.DocNo = arr[4];
                    obj.TravelDate = arr[5];
                    obj.ChequeNo = ((arr[6] != "") ? ("-" + arr[6]) : "");
                    obj.FlightNo = arr[7];
                    obj.HotelConfirmationNo = arr[8];
                    obj.DestinationCity = arr[9];
                    obj.PolicyNo = arr[10];
                    obj.PaymentMode = arr[11];
                }
                return obj;
            }
            set
            {
                this.narration = string.Concat(new string[]
				{
					value.Remarks,
					"|",
					value.PaxName,
					"|",
					value.Sector,
					"|",
					value.TicketNo,
					"|",
					value.DocNo,
					"|",
					value.TravelDate,
					"|",
					value.ChequeNo,
					"|",
					value.FlightNo,
					"|",
					value.HotelConfirmationNo,
					"|",
					value.DestinationCity,
					"|",
					value.PolicyNo,
					"|",
					value.PaymentMode
				});
            }
        }
        public string NarrationString
        {
            get
            {
                return this.narration;
            }
        }
        /// <summary>
        /// Gets or sets the notes of a transaction
        /// </summary>
        public string Notes
        {
            get
            {
                return this.notes;
            }
            set
            {
                this.notes = value;
            }
        }
        /// <summary>
        /// Gets or sets the date of a transaction
        /// </summary>
        public DateTime Date
        {
            get
            {
                return this.date;
            }
            set
            {
                this.date = value;
            }
        }
        /// <summary>
        /// Agency id property of a transaction
        /// </summary>
        public int AgencyId
        {
            get
            {
                return this.agencyId;
            }
            set
            {
                this.agencyId = value;
            }
        }
        /// <summary>
        /// Gets Id of member who created the record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return this.createdBy;
            }
            set
            {
                this.createdBy = value;
            }
        }
        /// <summary>
        /// Gets the date when the record was created on
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return this.createdOn;
            }
            set
            {
                this.createdOn = value;
            }
        }
        /// <summary>
        /// Gets Id of member who modified the record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return this.lastModifiedBy;
            }
            set
            {
                this.lastModifiedBy = value;
            }
        }
        /// <summary>
        /// Gets the date when the record was last Modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return this.lastModifiedOn;
            }
            set
            {
                this.lastModifiedOn = value;
            }
        }
        /// <summary>
        /// Gets or sets isLCC flag
        /// </summary>
        public bool IsLCC
        {
            get
            {
                return this.isLCC;
            }
            set
            {
                this.isLCC = value;
            }
        }
        /// <summary>
        /// Gets and Sets the referenceId field
        /// </summary>
        public int ReferenceId
        {
            get
            {
                return this.referenceId;
            }
            set
            {
                this.referenceId = value;
            }
        }
        /// <summary>
        /// Gets and sets the referenceType field
        /// </summary>
        public ReferenceType ReferenceType
        {
            get
            {
                return this.referenceType;
            }
            set
            {
                this.referenceType = value;
            }
        }
        /// <summary>
        /// Gets and sets the paxFirstName field
        /// </summary>
        public string PaxFirstName
        {
            get
            {
                return this.paxFirstName;
            }
            set
            {
                this.paxFirstName = value;
            }
        }
        /// <summary>
        /// Gets the PaxFullName
        /// </summary>
        public string PaxFullName
        {
            get
            {
                return string.Format("{0} {1}", this.paxFirstName, this.paxLastName);
            }
        }
        /// <summary>
        /// Gets and sets the paxFirstName field
        /// </summary>
        public string PaxLastName
        {
            get
            {
                return this.paxLastName;
            }
            set
            {
                this.paxLastName = value;
            }
        }
        /// <summary>
        /// Gets and sets the sector field
        /// </summary>
        public string Sector
        {
            get
            {
                return this.sector;
            }
            set
            {
                this.sector = value;
            }
        }
        /// <summary>
        /// Gets and sets the ticketNo field
        /// </summary>
        public string TicketNo
        {
            get
            {
                return this.ticketNo;
            }
            set
            {
                this.ticketNo = value;
            }
        }
        /// <summary>
        /// Gets and sets the ticketNo field
        /// </summary>
        public string CheckNo
        {
            get
            {
                return this.checkNo;
            }
            set
            {
                this.checkNo = value;
            }
        }

        public decimal RunningTotal
        {
            get
            {
                return runningTotal;
            }
            set
            {
                runningTotal = value;
            }
        }

        public string TransType
        {
            get { return transType; }
            set{transType=value;}
        }

        public int PaxCount
        {
            get
            {
                return paxCount;
            }

            set
            {
                paxCount = value;
            }
        }

        public string AgentCurrency
        {
            get
            {
                return agentCurrency;
            }

            set
            {
                agentCurrency = value;
            }
        }

        public decimal AgentCurrentBalance
        {
            get
            {
                return agentCurrentBalance;
            }

            set
            {
                agentCurrentBalance = value;
            }
        }

        public int AgentDecimal
        {
            get
            {
                return agentDecimal;
            }

            set
            {
                agentDecimal = value;
            }
        }

        public string AgentName
        {
            get
            {
                return agentName;
            }

            set
            {
                agentName = value;
            }
        }

        public string InvoiceNumber
        {
            get
            {
                return invoiceNumber;
            }

            set
            {
                invoiceNumber = value;
            }
        }

        public int FlightSource
        {
            get
            {
                return flightSource;
            }

            set
            {
                flightSource = value;
            }
        }

        public decimal BalanceTillDate
        {
            get
            {
                return balanceTillDate;
            }

            set
            {
                balanceTillDate = value;
            }
        }

        public int PaymentMode { get => paymentMode; set => paymentMode = value; }
        public decimal Agent_Credit_Limit { get => agent_Credit_Limit; set => agent_Credit_Limit = value; }
        public int Agent_Credit_Days { get => agent_Credit_Days; set => agent_Credit_Days = value; }
        public decimal Agent_Credit_Buffer_Amount { get => agent_Credit_Buffer_Amount; set => agent_Credit_Buffer_Amount = value; }
        public string Agent_Payment_Mode { get => agent_Payment_Mode; set => agent_Payment_Mode = value; }
        /// <summary>
        /// Methos saves the Transaction
        /// </summary>
        public void Save()
        {
            if (this.txnId > 0)
            {
                //Trace.TraceInformation("LedgerTransaction.Update entered : ");
                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_UpdateLedgerTransaction", new SqlParameter[]
				{
					new SqlParameter("@txnId", this.txnId),
					new SqlParameter("@ledgerId", this.ledgerId),
					new SqlParameter("@amount", this.amount),
					new SqlParameter("@narration", this.narration),
					new SqlParameter("@notes", this.notes),
					new SqlParameter("@referenceId", this.referenceId),
					new SqlParameter("@referenceTypeId", (int)this.referenceType),
					new SqlParameter("@isLCC", this.isLCC),
					new SqlParameter("@lastModifiedOn", this.lastModifiedOn),
					new SqlParameter("@lastModifiedBy", this.lastModifiedBy),
                    new SqlParameter("@paymentMode", this.paymentMode)
                });
                //Trace.TraceInformation("LedgerTransaction.Update exiting : rowsAffected = " + rowsAffected);
            }
            else
            {
                //Trace.TraceInformation("LedgerTransaction.Save entered : ");
                SqlParameter[] paramList = new SqlParameter[13];
                paramList[0] = new SqlParameter("@txnId", this.txnId);
                paramList[0].Direction = ParameterDirection.Output;
                paramList[1] = new SqlParameter("@ledgerId", this.ledgerId);
                paramList[2] = new SqlParameter("@amount", this.amount);
                paramList[3] = new SqlParameter("@narration", this.narration);
                paramList[4] = new SqlParameter("@notes", this.notes);
                paramList[5] = new SqlParameter("@date", this.date);
                paramList[6] = new SqlParameter("@referenceId", this.referenceId);
                paramList[7] = new SqlParameter("@referenceTypeId", (int)this.referenceType);
                paramList[8] = new SqlParameter("@isLCC", this.isLCC);
                paramList[9] = new SqlParameter("@createdBy", this.createdBy);
                if (this.CreatedOn != new DateTime(1, 1, 1))
                {
                    paramList[10] = new SqlParameter("@createdOn", this.createdOn);
                }
                else
                {
                    paramList[10] = new SqlParameter("@createdOn", DateTime.UtcNow);
                }
                paramList[11] = new SqlParameter("@transType", this.transType);
                paramList[12] = new SqlParameter("@paymentMode", this.paymentMode);
                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddLedgerTransaction", paramList);
                this.txnId = Convert.ToInt32(paramList[0].Value);
                //Trace.TraceInformation("LedgerTransaction.Save exiting : rowsAffected = " + rowsAffected);
            }
        }
        /// <param name="ledgerId">Ledger Id</param>
        /// <param name="startingDate">Starting Date</param>
        /// <param name="endingDate">Ending Date</param>
        /// <returns>Generic List of LedgerTransaction objects</returns>
        public static List<LedgerTransaction> GetAllTransactions(int ledgerId, DateTime startingDate, DateTime endingDate, string agentType, string transType, string paymentMode)
        {
           
                
                //Trace.TraceInformation("LedgerTransaction.GetAllTransactions entered : ledgerId = " + ledgerId);
                List<LedgerTransaction> tempList = new List<LedgerTransaction>();
                try
                {
                    if (ledgerId < 0)
                    {
                        throw new ArgumentException("Ledger Id should be positive integer ledgerId=" + ledgerId);
                    }
                    DateTime startDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
                    DateTime endDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
                    //SqlConnection connection = DBGateway.GetConnection();
                    //SqlDataReader data;
                    SqlParameter[] paramList = new SqlParameter[6];
                    if (ledgerId > 0) paramList[0] = new SqlParameter("@ledgerId", ledgerId);
                    paramList[1] = new SqlParameter("@startDate", startDate);
                    paramList[2] = new SqlParameter("@endDate", endDate);
                    paramList[3] = new SqlParameter("@P_AGENT_TYPE", agentType);
                    if (!string.IsNullOrEmpty(transType)) paramList[4] = new SqlParameter("@P_TransType", transType);
                    paramList[5] = new SqlParameter("@P_PaymentType", paymentMode);
                   // Audit.Add(EventType.Exception, Severity.High, 1, " 1 " + ledgerId.ToString(), "0");
                   DataTable dtLedger = DBGateway.ExecuteQuery("usp_GetAdminLedger", paramList).Tables[0];
                    if (dtLedger != null && dtLedger.Rows.Count > 0)
                    {
                        tempList = LedgerTransaction.GetTransactionList(dtLedger);
                    }
                }
                catch(Exception ex) { throw ex; }
                return tempList;
            
        }
        /// <param name="ledgerId">Ledger Id</param>
        /// <param name="startingDate">Starting Date</param>
        /// <param name="endingDate">Ending Date</param>
        /// <returns>Generic List of Agents LedgerTransaction objects</returns>
        public static List<LedgerTransaction> GetAllAgentLedgers(int ledgerId, DateTime startingDate, DateTime endingDate, string agentType, string transType, string paymentMode)
        {
            List<LedgerTransaction> tempList = new List<LedgerTransaction>();
            try
            {
                if (ledgerId < 0)
                {
                    throw new ArgumentException("Ledger Id should be positive integer ledgerId=" + ledgerId);
                }
                DateTime startDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
                DateTime endDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
                List<SqlParameter> paramList = new List<SqlParameter>();
                if (ledgerId > 0) paramList.Add(new SqlParameter("@ledgerId", ledgerId));
                paramList.Add(new SqlParameter("@startDate", startDate));
                paramList.Add(new SqlParameter("@endDate", endDate));
                paramList.Add(new SqlParameter("@P_AGENT_TYPE", agentType));
                if (!string.IsNullOrEmpty(transType)) paramList.Add(new SqlParameter("@P_TransType", transType));
                paramList.Add(new SqlParameter("@P_PaymentType", paymentMode));
                //DataTable dtLedger = DBGateway.FillDataTableSP("usp_GetAgentLedger", paramList);
                SqlConnection connection = DBGateway.GetConnection();
                DataTable dtLedger = new DataTable();

                SqlDataAdapter dataAdapter = new SqlDataAdapter("SET ARITHABORT ON exec usp_GetAgentLedger @ledgerId =" + ledgerId + " ,@startDate = '" + startDate.ToString("yyyy-MM-dd hh:mm:ss tt") + "', @endDate = '" + endDate.ToString("yyyy-MM-dd hh:mm:ss tt") + "', @P_AGENT_TYPE = '" + agentType + "', @P_TransType = " + (!string.IsNullOrEmpty(transType) ? "'" + transType + "'" : "NULL") + ", @P_PaymentType = '" + paymentMode + "'", connection);
                dataAdapter.SelectCommand.CommandTimeout = 950;
                dataAdapter.SelectCommand.CommandType = CommandType.Text;
                dataAdapter.SelectCommand.Parameters.AddRange(paramList.ToArray());                
                dataAdapter.Fill(dtLedger);
                if (dtLedger != null && dtLedger.Rows.Count > 0)
                {
                    tempList = GetTransactionList(dtLedger);
                }
            }
            catch (Exception ex) { throw ex; }
            return tempList;
        }
        //public static List<LedgerTransaction> GetAllAgentLedgers(int ledgerId, DateTime startingDate, DateTime endingDate, string agentType, string transType, string paymentMode)
        //{
        //    List<LedgerTransaction> tempList = new List<LedgerTransaction>();
        //    try
        //    {
        //        if (ledgerId < 0)
        //        {
        //            throw new ArgumentException("Ledger Id should be positive integer ledgerId=" + ledgerId);
        //        }
        //        DateTime startDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
        //        DateTime endDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
        //        SqlParameter[] paramList = new SqlParameter[6];
        //        if (ledgerId > 0) paramList[0] = new SqlParameter("@ledgerId", ledgerId);
        //        paramList[1] = new SqlParameter("@startDate", startDate);
        //        paramList[2] = new SqlParameter("@endDate", endDate);
        //        paramList[3] = new SqlParameter("@P_AGENT_TYPE", agentType);
        //        if (!string.IsNullOrEmpty(transType)) paramList[4] = new SqlParameter("@P_TransType", transType);
        //        paramList[5] = new SqlParameter("@P_PaymentType", paymentMode);
        //        DataTable dtLedger = DBGateway.ExecuteQuery("usp_GetAgentLedger", paramList).Tables[0];
        //        if (dtLedger != null && dtLedger.Rows.Count > 0)
        //        {
        //            tempList = GetTransactionList(dtLedger);
        //        }
        //    }
        //    catch (Exception ex) { throw ex; }
        //    return tempList;
        //}

        /// <summary>
        /// Method adds entry in Invoice transactiion 
        /// </summary>
        /// <param name="invoiceNumber">invoice Number</param>
        /// <param name="ledgerTxnId">txnId</param>
        public static void AddInvoiceTxn(int invoiceNumber, int ledgerTxnId)
        {
            //Trace.TraceInformation("LedgerTransaction.AddInvoiceTxn entered : ");
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("Invoice Number Id should be positive integer invoiceNumber=" + invoiceNumber);
            }
            int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddInvoiceTxn", new SqlParameter[]
			{
				new SqlParameter("@invoiceNumber", invoiceNumber),
				new SqlParameter("@ledgerTxnId", ledgerTxnId)
			});
            //Trace.TraceInformation("LedgerTransaction.AddInvoiceTxn exiting : rowsAffected = " + rowsAffected);
        }
        /// <summary>
        /// Private Method loads List from DataReader
        /// </summary>
        /// <param name="data">DataReader</param>
        /// <returns>List of Ledger Transactions</returns>
        private static List<LedgerTransaction> GetTransactionList(DataTable dtLedger)
        {

            List<LedgerTransaction> tempList = new List<LedgerTransaction>();
            try
            {

                if (dtLedger !=null)
                {
                    foreach (DataRow data in dtLedger.Rows)
                    {
                        LedgerTransaction instance = new LedgerTransaction();
                        if (data["txnId"] != DBNull.Value)
                        {
                            instance.txnId = Convert.ToInt32(data["txnId"]);
                        }
                        if (data["amount"] !=DBNull.Value)
                        {
                            instance.amount = Convert.ToDecimal(data["amount"]);
                        }
                        if (data["narration"] != DBNull.Value)
                        {
                            instance.narration = data["narration"].ToString();
                        }
                        else
                        {
                            instance.narration = string.Empty;
                        }
                        if (data["notes"] != DBNull.Value)
                        {
                            instance.notes =Convert.ToString(data["notes"]);
                        }
                        else
                        {
                            instance.notes = string.Empty;
                        }
                        if (data["date"] != DBNull.Value)
                        {
                            instance.date = Convert.ToDateTime(data["date"]);
                        }
                        if (data["referenceId"] != DBNull.Value)
                        {
                            instance.referenceId = Convert.ToInt32(data["referenceId"]);
                        }
                        if (data["referenceTypeId"] != DBNull.Value)
                        {
                            instance.referenceType = (ReferenceType)Enum.Parse(typeof(ReferenceType),Convert.ToString(data["referenceTypeId"]));
                        }
                        else
                        {
                            instance.referenceType = ReferenceType.TicketCreated;
                        }
                        if (instance.referenceType == ReferenceType.TicketCreated || instance.referenceType == ReferenceType.TicketRefund || instance.referenceType == ReferenceType.OfflineTicket || instance.referenceType == ReferenceType.OfflineRefund)
                        {
                            //Audit.Add(EventType.Exception, Severity.High, 1, " 3", "0");
                            if (data["TicketDetail"] != DBNull.Value)
                            {
                                string[] tmp =Convert.ToString(data["TicketDetail"]).Split(new char[]
                            {
                                '|'
                            });
                                instance.Sector = tmp[0];
                                instance.TicketNo = tmp[1];
                                instance.PaxFirstName = tmp[2];
                                instance.PaxLastName = tmp[3];
                            }
                        }
                        if (instance.referenceType == ReferenceType.PaymentRecieved || instance.referenceType == ReferenceType.PaymentReversed)
                        {
                            if (data["TicketDetail"] != DBNull.Value)
                            {
                                instance.CheckNo =Convert.ToString(data["TicketDetail"]);

                            }
                        }
                        if (data["isLCC"] != DBNull.Value)
                        {
                            instance.isLCC = Convert.ToBoolean(data["isLCC"]);
                        }
                        if (data["agencyId"] != DBNull.Value && data["agencyId"] != null)
                        {
                            instance.agencyId = Convert.ToInt32(data["agencyId"]);
                        }
                        if (data["createdOn"] != DBNull.Value)
                        {
                            instance.createdOn = Convert.ToDateTime(data["createdOn"]);
                        }
                        if (data["createdBy"] != DBNull.Value)
                        {
                            instance.createdBy = Convert.ToInt32(data["createdBy"]);
                        }
                        if (data["lastModifiedOn"] != DBNull.Value)
                        {
                            instance.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                        }
                        if (data["lastModifiedBy"] != DBNull.Value)
                        {
                            instance.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                        }
                        if (data["runningTotal"] != DBNull.Value)
                        {
                            instance.runningTotal = Convert.ToDecimal(data["runningTotal"]);
                        }
                        if (data["TransType"] != DBNull.Value)
                        {
                            instance.transType = Convert.ToString(data["TransType"]);
                        }
                        if (data["paxCount"] != DBNull.Value)
                        {
                            instance.paxCount = Convert.ToInt32(data["paxCount"]);
                        }
                        if (data["agent_decimal"] != DBNull.Value)
                        {
                            instance.agentDecimal = Convert.ToInt32(data["agent_decimal"]);
                        }
                        if (data["agent_currency"] != DBNull.Value)
                        {
                            instance.agentCurrency = Convert.ToString(data["agent_currency"]);
                        }
                        if (data["agent_current_balance"] != DBNull.Value)
                        {
                            instance.agentCurrentBalance = Convert.ToDecimal(data["agent_current_balance"]);
                        }
                        if (data["agent_name"] != DBNull.Value)
                        {
                            instance.agentName = Convert.ToString(data["agent_name"]);
                        }
                        if (data["invoiceNumber"] != DBNull.Value)
                        {
                            instance.invoiceNumber = Convert.ToString(data["invoiceNumber"]);
                        }
                        if (data["SOURCE"] != DBNull.Value)
                        {
                            instance.flightSource = Convert.ToInt32(data["SOURCE"]);
                        }
                        if (data["balanceTillDate"] != DBNull.Value)
                        {
                            instance.balanceTillDate = Convert.ToDecimal(data["balanceTillDate"]);
                        }
                        if (data["agencyId"] != DBNull.Value)
                        {
                            instance.agencyId = Convert.ToInt32(data["agencyId"]);
                        }
                        if (data["PaymentMode"] != DBNull.Value)
                        {
                            instance.paymentMode = Convert.ToInt16(data["PaymentMode"]);
                        }
                        if (data["agent_Payment_Mode"] != DBNull.Value)
                        {
                            instance.agent_Payment_Mode = Convert.ToString(data["agent_Payment_Mode"]);
                        }
                        if (data["agent_Credit_Days"] != DBNull.Value)
                        {
                            instance.agent_Credit_Days = Convert.ToInt16(data["agent_Credit_Days"]);
                        }
                        if (data["agent_Credit_Buffer_Amount"] != DBNull.Value)
                        {
                            instance.agent_Credit_Buffer_Amount = Convert.ToDecimal(data["agent_Credit_Buffer_Amount"]);
                        }
                        if (data["agent_Credit_Limit"] != DBNull.Value)
                        {
                            instance.agent_Credit_Limit = Convert.ToDecimal(data["agent_Credit_Limit"]);
                        }
                        //Combining all ticket numbers (,) seperated like 123665333,123665334
                        instance.ticketNo = data["ticketNumbers"] != DBNull.Value ? data["ticketNumbers"].ToString() : string.Empty;
                        tempList.Add(instance);
                    }
                }
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, " Error: "+ex.ToString(), "0");
                throw;
            }
            return tempList;
        }

        public static decimal GetOpeningBalanceTillDate(int agencyId, DateTime date,string paymentMode)
        {
            //Trace.TraceInformation("Ledger.GetOpeningBalanceTillDate entered : AgencyId " + agencyId);
            decimal openingBalanceTillDate = 0m;
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader reader = DBGateway.ExecuteReaderSP("usp_GetOpeningBalanceTillDate", new SqlParameter[]
			{
				new SqlParameter("@agencyId", agencyId),
				new SqlParameter("@date", date),
                new SqlParameter("@P_PaymentType",paymentMode)
			}, connection);
            while (reader.Read())
            {
                if (reader["Amount"] != DBNull.Value)
                {
                    openingBalanceTillDate = Convert.ToDecimal(reader["Amount"]);
                }
            }
            reader.Close();
            connection.Close();
            //Trace.TraceInformation("Ledger.GetOpeningBalanceTillDate exited : Opening balance till date " + openingBalanceTillDate);
            return openingBalanceTillDate;
        }

        public static int GetHotelPaxCount(int referenceId)
        {
            int count=0;
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader reader = DBGateway.ExecuteReaderSP("usp_Get_Hotel_Pax_count", new SqlParameter[]
			{
				new SqlParameter("@P_ROOM_ID", referenceId),
			}, connection);
            while (reader.Read())
            {
                if (reader["paxcount"] != DBNull.Value)
                {
                    count = Convert.ToInt32(reader["paxcount"]);
                }
            }
            reader.Close();
            connection.Close();
            return count;
        }
        
    }
}
