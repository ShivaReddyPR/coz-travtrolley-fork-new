namespace CT.AccountingEngine
{
    /// <summary>
    /// This Enum contains the Reference Type For Ledger Transaction
    /// </summary>
    public enum ReferenceType
    {
        /// <summary>
        /// ReferenceId -- TicketId
        /// </summary>
        TicketCreated = 1,
        /// <summary>
        /// ReferenceId -- PaymentDetailId
        /// </summary>
        PaymentRecieved = 2,
        /// <summary>
        /// ReferenceId -- PaymentDetailId
        /// </summary>
        PaymentReversed = 3,
        /// <summary>
        /// ReferenceId -- PaymentDetailId
        /// </summary>
        TicketRefund = 4,
        /// <summary>
        /// ReferenceId -- 0
        /// </summary>
        ManualEntry = 5,
        /// <summary>
        /// ReferenceId -- PaymentDetailId
        /// </summary>
        TicketCancellationCharge = 6,
        /// <summary>
        /// ReferenceId -- OfflineBookingId
        /// </summary>
        OfflineTicket = 7,
        /// <summary>
        /// ReferenceId -- PaymentDetailId
        /// </summary>
        OfflineRefund = 8,
        /// <summary>
        /// ReferenceId -- PaymentDetailId
        /// </summary>
        OfflineCancellationCharge = 9,
        /// <summary>
        /// ReferenceId -- TicketId
        /// </summary>
        TicketReverted = 10,
        /// <summary>
        /// PLB Revereted for earlier tickets
        /// </summary>
        PLBReverted = 11,
        /// <summary>
        /// PLB for Earlier entries in DB
        /// </summary>
        PLB = 12,
        /// <summary>
        /// Hotel Booking successful
        /// </summary>
        HotelBooked = 13,
        /// <summary>
        /// Hotel Refund
        /// </summary>
        HotelRefund = 14,
        /// <summary>
        /// Hotel Cancellation Charge
        /// </summary>
        HotelCancellationCharge = 15,
        /// <summary>
        /// Insurance Creation Successful 
        /// </summary>
        InsuranceCreated = 16,
        /// <summary>
        /// Insurance Creation Successful 
        /// </summary>
        InvoiceEdited = 17,
        /// <summary>
        /// Transfer Booking Successful
        /// </summary>
        TransferBooked = 18,
        /// <summary>
        /// Sightseeing Booking successful
        /// </summary>
        SightseeingBooked = 19,
        /// <summary>
        /// Train ticket created
        /// </summary>
        TrainTicket = 20,
        /// <summary>
        /// Train ticket refunded
        /// </summary>
        TrainTicketRefund = 21,
        /// <summary>
        /// Cash Discount recieved 
        /// </summary>
        CashDiscount = 22,
        /// <summary>
        /// Cash Discount refunded
        /// </summary>
        CashDiscountReversal = 23,
        /// <summary>
        /// For Misc Invoice
        /// </summary>
        MiscItem = 24,
        ///<summary>
        ///For insurance Refund
        ///<summary>
        InsuranceRefund = 25,
        ///<summary>
        ///Insurance Cancellation Charge
        ///<summary>
        InsuranceCancellationCharge = 26,
        /// <summary>
        /// for Mobile Recharge
        /// </summary>
        MobileRecharge=27,
        /// <summary>
        /// for Activity Booking
        /// </summary>
        ActivityBooked = 28,

        /// <summary>
        /// TicketRefund -- PaymentDetailId
        /// </summary>
        TicketVoid= 29,

        /// <summary>
        /// TicketModification -- PaymentDetailId
        /// </summary>
        TicketModification = 30,


        /// <summary>
        /// TicketModification -- PaymentDetailId
        /// </summary>
        FixedDepartureBooked = 31,


        SightSeeingRefund = 32,
        SightseeingCancellationCharge=33,
        CardHotelBooked = 34,
        CardHotelRefund = 35,
        CardHotelCancellationCharge = 36,
        CardTicketCreated = 37,
        CardTicketCancellationCharge = 38,
        CardTicketVoid = 39,
        CardTicketRefund = 40,
        CardTicketModification = 41,
                /// <summary>
        /// Transfer Refund
        /// </summary>
        TransferRefund = 42,
        TransferCancellationCharge = 43, //added by chandan on 18032016
        CarBooked=44,
        CarCancellationCharge=45,
        CarRefund = 46,

        VisaReceipt = 47,
        OK2BReceipt = 48,
        AdditionalService = 49,
        PackageBooked = 50,
        PackageCancellationCharges=51,
        PackageRefund=52,
        GlobalVisaBooked = 53,
        CTWGVBooked = 54   //Added by Harimalla 30-04-2019 for GlobalVisa

        // Need to check B2C as well(Accounting Engine)

    }
    /// <summary>
    /// This Enum defines the different types of Accounts for making payment
    /// </summary>
    public enum AccountType
    {
        ///<summary>
        ///LCC
        ///</summary>
        LCC = 1,
        ///<summary>
        ///Non LCC
        ///</summary>
        NonLCC = 2,
        ///<summary>
        ///Both
        ///</summary>
        Both = 3,
        /// <summary>
        /// Hotel account
        /// </summary>
        Hotel = 4
    }

    public enum ReferenceIdType
    {
        /// <summary>
        /// payment by agent
        /// </summary>
        Payment = 1,
        /// <summary>
        /// invoice for the ticket
        /// </summary>
        Invoice = 2,

    }

    public enum CreditCardPaymentStatus
    {
        /// <summary>
        /// The request has been sent to PG
        /// </summary>
        Sent=1,
        /// <summary>
        /// The payment was successfull
        /// </summary>
        Successfull = 2,
        /// <summary>
        /// The payment was successfull but we were not able to save because of session expiry or some exception
        /// </summary>
        SuccessfullButNotSaved =3,        
        /// <summary>
        /// The payment was unsuccessfull
        /// </summary>
        Unsuccessfull = 4,
        /// <summary>
        /// The payment was successfull.And Entries were done Manually.
        /// </summary>
        SuccessfulManually = 5,
        /// <summary>
        /// The payment was Unsuccessfull.And Entries were done Manually.
        /// </summary>
        UnsuccessfulManually = 6
    }


}
