using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.BookingEngine;

namespace CT.AccountingEngine
{
    /// <summary>
    /// It just hold the data of accounts detail
    /// </summary>
    public class AccountsDetail
    {
        
        /// <summary>
        /// UniqueId field for the object
        /// </summary>
        private int detailId;
        /// <summary>
        /// MarkUp field is the percentage we taake on fare
        /// </summary>
        private float markup;
        /// <summary>
        /// ServiceTaxDom field contains service tax(in percentage) for domestic fare
        /// </summary>
        private float serviceTaxDom;
        /// <summary>
        /// ServiceTaxIntl field contains service tax(in percentage) for international
        /// fare
        /// </summary>
        private float serviceTaxIntl;
        /// <summary>
        /// TDS default field contains default TDS(in percentage) 
        /// </summary>
        private float tdsDefault;
        /// <summary>
        /// This property field the starting time of financial year start
        /// </summary>
        private DateTime financialYearStart;
        /// <summary>
        /// UniqueId property for the object
        /// </summary>
        public int DetailId
        {
            get
            {
                return this.detailId;
            }
            set 
            {
                this.detailId = value;
            }
        }
        /// <summary>
        /// MarkUp property is the percentage we taake on fare
        /// </summary>
        public float Markup
        {
            get
            {
                return this.markup;
            }
            set
            {
                this.markup = value;
            }
        }
        /// <summary>
        /// ServiceTaxDom property contains service tax(in percentage) for domestic fare
        /// </summary>
        public float ServiceTaxDom
        {
            get
            {
                return this.serviceTaxDom;
            }
            set
            {
                this.serviceTaxDom = value;
            }
        }
        /// <summary>
        /// ServiceTaxIntl property contains service tax(in percentage) for international
        /// fare
        /// </summary>
        public float ServiceTaxIntl
        {
            get
            {
                return this.serviceTaxIntl;
            }
            set
            {
                this.serviceTaxIntl = value;
            }
        }
        /// <summary>
        /// TDS default property contains default TDS(in percentage) 
        /// </summary>
        public float TDSDefault
        {
            get
            {
                return this.tdsDefault;
            }
            set
            {
                this.tdsDefault = value;
            }
        }
        /// <summary>
        /// This property contains the starting time of financial year start
        /// </summary>
        public DateTime FinancialYearStart
        {
            get 
            {
                return this.financialYearStart;
            }
            set
            {
                this.financialYearStart = value;
            }
        }
        /// <summary>
        /// Method gets default tds
        /// </summary>
        /// <param name="datetime">DateTime</param>
        /// <returns></returns>
        public static float GetTDSDefault(DateTime datetime, bool isDomestic)
        {
            float tds = 0.0f;
            //Trace.TraceInformation("AccountsDetail.GetTDSDefault entered : dateTime = " + datetime.ToString("dd-mm-yyyy"));
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@dateTime", datetime);
            paramList[1] = new SqlParameter("@isDomestic", isDomestic);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetTDSDefault , paramList,connection);
            if (dataReader.Read())
            {
                tds = Convert.ToSingle(dataReader["TDSDefault"]);
            }
            dataReader.Close();
            connection.Close();
            return tds;
        }

        /// <summary>
        /// Get product wise service tax Rate
        /// </summary>
        /// <param name="prodType"></param>
        /// <param name="isDomestic"></param>
        /// <returns></returns>

        public static decimal GetProductWiseServiceTaxRate(ProductType prodType, bool isDomestic)
        {
            //Trace.TraceInformation("AccountingEngine.GetProductWiseServiceTaxRate entered :");
            float serviceTax = 0.0f;
            try
            {
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList;
                if (prodType == ProductType.Hotel)
                {
                    paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@isDomestic", isDomestic);
                    SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetHotelServiceTax, paramList,connection);
                    if (dataReader.Read())
                    {
                        serviceTax = Convert.ToSingle(dataReader["serviceTax"]);
                    }
                    dataReader.Close();
                }
                else if (prodType == ProductType.Train)
                {
                    paramList = new SqlParameter[0];
                    SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetTrainServiceTaxRate, paramList,connection);
                    if (dataReader.Read())
                    {
                        serviceTax = Convert.ToSingle(dataReader["serviceTax"]);
                    }
                    dataReader.Close();
                }

                else if (prodType == ProductType.Insurance)
                {
                    paramList = new SqlParameter[0];
                    SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetInsuranceServiceTaxRate, paramList,connection);
                    if (dataReader.Read())
                    {
                        serviceTax = Convert.ToSingle(dataReader["serviceTax"]);
                    }
                    dataReader.Close();
                }

                connection.Close();
            }
            catch (SqlException exception)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
            }
            //Trace.TraceInformation("AccountingEngine.GetProductWiseServiceTaxRate exited : serviceTax = " + serviceTax);
            return Convert.ToDecimal(serviceTax);
        }
        
        /// <summary>
        /// Gets the percentage to servicetax
        /// </summary>
        /// <returns></returns>

        public static float GetServiceTaxDefault(bool isDomestic)
        {
            float serviceTax = 0.0f;
            //Trace.TraceInformation("AccountsDetail.GetServiceTaxDefault entered : isDomestic = " +isDomestic);
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@isDomestic", isDomestic);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetServiceTax, paramList,connection);
            if (dataReader.Read())
            {
                serviceTax = Convert.ToSingle(dataReader["serviceTax"]);
            }
            dataReader.Close();
            connection.Close();
            return serviceTax;
        }


        /// <summary>
        /// Gets the servicetax percentage
        /// </summary>
        /// <returns></returns>

        public static float GetServiceTaxPercent(bool isDomestic)
        {
            float serviceTax = 0.0f;
            //Trace.TraceInformation("AccountsDetail.GetServiceTaxDefault entered : isDomestic = " + isDomestic);
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@isDomestic", isDomestic);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetServiceTaxPercent, paramList,connection);
            if (dataReader.Read())
            {
                serviceTax = Convert.ToSingle(dataReader["serviceTax"]);
            }
            dataReader.Close();
            connection.Close();
            return serviceTax;
        }

        /// <summary>
        /// Gets the percentage to Cess
        /// </summary>
        /// <returns></returns>

        public static float GetCessDefault()
        {
            float cess = 0.0f;
            //Trace.TraceInformation("AccountsDetail.GetCessDefault entered");
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetCess, paramList,connection);
            if (dataReader.Read())
            {
                cess = Convert.ToSingle(dataReader["cess"]);
            }
            dataReader.Close();
            connection.Close();
            return cess;
        }


        /// <summary>
        /// Gets the percentage to charge on credit card
        /// </summary>
        /// <returns></returns>
        public static float GetCreditCardCharge()
        {
            float ccCharge = 0.0f;
            //Trace.TraceInformation("AccountsDetail.GetCreditCardCharge entered : ");
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetCreditCardCharge, paramList,connection);
            if (dataReader.Read())
            {
                ccCharge = Convert.ToSingle(dataReader["creditCardCharge"]);
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("AccountsDetail.GetCreditCardCharge exited : ");
            return ccCharge;
        }

        /// <summary>
        /// Get ourBankdetailId on the basis of PaymentSourceId.
        /// </summary>
        /// <returns></returns>
        public static int GetOurBankDetailIdBySource(int sourceId)
        {
            int ourBankDetailId=0;
            //Trace.TraceInformation("AccountsDetail.GetOurBankDetailIdBySource entered : ");
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@sourceid", sourceId);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetOurBankDetailIdBySource, paramList,connection);
            if (dataReader.Read())
            {
                ourBankDetailId = Convert.ToInt32(dataReader["ourBankDetailid"]);
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("AccountsDetail.GetOurBankDetailIdBySource exited : ");
            return ourBankDetailId;
        }

        ///<summary>
        /// Get the max document sequenceNumber on the basis of docTypeCode
        /// <return> return the max. doc sequence Number</return>
        ///</summary> 

        public static string GetMaxDocumentSequenceNumber(string docTypeCode)
        {
            string maxSequenceNo = string.Empty;
            //Trace.TraceInformation("AccountingDetails.GetMaxDocumentSequenceNumber entered : ");
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@docTypeCode", docTypeCode);
                using (SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetMaxDocumentSequenceNumber, paramList,connection))
                {
                    if (dataReader.Read())
                    {
                        maxSequenceNo = dataReader["MaxDocSeqNumber"].ToString();
                    }
                    dataReader.Close();
                }
            }
            //Trace.TraceInformation("AccountingDetails.GetMaxDocumentSequenceNumber exited: ");
            return maxSequenceNo;
        }

    
        /// <summary>
        /// Gets the percentage to charge on credit card
        /// </summary>
        /// <returns></returns>
        public static float GetCreditCardCharge(PaymentGatewaySource source)
        {
            String Source = string.Empty;
            if (source == PaymentGatewaySource.HDFC)
            {
                Source = "HDFCCharge";
            }
            else if (source == PaymentGatewaySource.AMEX)
            {
                Source = "AMEXCharge";
            }
            else if (source == PaymentGatewaySource.CCAvenue)
            {
                Source = "CCAvenueCharge";
            }
            else if (source == PaymentGatewaySource.ICICI)
            {
                Source = "ICICICharge";
            }
            float ccCharge = 0.0f;
            //Trace.TraceInformation("AccountsDetail.GetCreditCardCharge entered : ");
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@keyVariable", Source);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetCreditCardCharge, paramList,connection);
            if (dataReader.Read())
            {
                ccCharge = Convert.ToSingle(dataReader["creditCardCharge"]);
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("AccountsDetail.GetCreditCardCharge exited : ");
            return ccCharge;
        }

        public static List<KeyValuePair<PaymentGatewaySource, float>> GetCreditCardChargesFor(params PaymentGatewaySource[] pGatways)
        {
            string csv = "";
            foreach (PaymentGatewaySource src in pGatways)
            {
                if(src== PaymentGatewaySource.ICICI)
                {
                    csv += "ICICICharge,";
                }
                else if (src == PaymentGatewaySource.HDFC)
                {
                    csv += "HDFCCharge,";
                }
                else if (src == PaymentGatewaySource.AMEX)
                {
                    csv += "AMEXCharge,";
                }
                else if (src == PaymentGatewaySource.CCAvenue)
                {
                    csv += "CCAvenueCharge,";
                }                
            }
            csv.TrimEnd(',');
            List<KeyValuePair<PaymentGatewaySource, float>> chargesList = new List<KeyValuePair<PaymentGatewaySource, float>>();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@keyVariableCSV", csv);
                using (SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetAllAccountsDetailCharges, paramList,connection))
                {
                    while(dataReader.Read())
                    {
                        string ccSource = Convert.ToString(dataReader["creditCardSource"]).Trim().ToUpper();
                        float ccCharge = Convert.ToSingle(dataReader["creditCardCharge"]);
                        KeyValuePair<PaymentGatewaySource, float> obj;
                        if (ccSource == "HDFCCHARGE")
                        {
                            obj = new KeyValuePair<PaymentGatewaySource, float>(PaymentGatewaySource.HDFC, ccCharge);
                            chargesList.Add(obj);
                        }
                        else if (ccSource == "ICICICHARGE")
                        {
                            obj = new KeyValuePair<PaymentGatewaySource, float>(PaymentGatewaySource.ICICI, ccCharge);
                            chargesList.Add(obj);
                        }
                        else if (ccSource == "CCAVENUECHARGE")
                        {
                            obj = new KeyValuePair<PaymentGatewaySource, float>(PaymentGatewaySource.CCAvenue, ccCharge);
                            chargesList.Add(obj);
                        }
                        else if (ccSource == "AMEXCHARGE")
                        {
                            obj = new KeyValuePair<PaymentGatewaySource, float>(PaymentGatewaySource.AMEX, ccCharge);
                            chargesList.Add(obj);
                        }                     
                    }    
                    dataReader.Close();
                }
                connection.Close();
            }
            return chargesList;
        }
    }
}
