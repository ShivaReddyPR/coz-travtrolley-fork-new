using System;
using System.Collections.Generic;
using CT.BookingEngine;
using System.Transactions;
using System.Data;
using CT.BookingEngine.Insurance;
using ReligareInsurance;
using CT.TicketReceipt.BusinessLayer;
//using InsuranceBookingEngine;

namespace CT.AccountingEngine
{
    /// <summary>
    /// Summary description for Util
    /// </summary>
    public class AccountUtility

    {
        #region Invoice Creation

        /// <summary>
        /// Raises invoice for the given itinerary.
        /// </summary>
        /// <param name="itinerary">itinerary for which invoice is to be raised.</param>
        /// <param name="remarks">remarks if any.</param>
        /// <param name="memberId">memberId of the member who is currently logged in.</param>
        /// <returns>Invoice number generated. If invoice for itinerary is already generated then it will give the existing invoice number.</returns>
        public static int RaiseInvoicePart2(HotelItinerary itinerary, string remarks, int memberId, decimal rateOfExchange)
        {
            BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(itinerary.HotelId, ProductType.Hotel));
            Invoice invoice = new Invoice();

            if (itinerary.Status == HotelBookingStatus.Confirmed || itinerary.Status==HotelBookingStatus.AwaitingForPayment)
            {
                UserMaster clsUM = new UserMaster(itinerary.CreatedBy);
                bool bOnBehalf = clsUM.AgentId != itinerary.AgencyId;

                if (invoice.InvoiceRaised(itinerary.HotelId, ProductType.Hotel) == 0)
                {
                    decimal totalAmount = 0;
                    invoice.PaymentMode = string.Empty;
                    invoice.Remarks = remarks;
                    invoice.AgencyId = booking.AgencyId;
                    invoice.Status = InvoiceStatus.Paid;
                    invoice.CreatedBy = memberId;
                    invoice.CreatedOn = DateTime.UtcNow;
                    invoice.TranxHeaderId = itinerary.HotelId; // Newly added on Sep-09-2014 sainadh
                    invoice.PaymentMode = ((int)itinerary.PaymentMode).ToString();
                    // Generating Invoice line items.
                    invoice.LineItem = new List<InvoiceLineItem>();
                    //PriceAccounts priceInfo = new PriceAccounts();
                    for (int i = 0; i < itinerary.Roomtype.Length; i++)
                    {
                        InvoiceLineItem line = new InvoiceLineItem();
                        line.ItemDescription = string.Empty;
                        line.ItemReferenceNumber = itinerary.Roomtype[i].RoomId;
                        //TODO: line item type Id. should not be hardcoded 3.
                        line.ItemTypeId = (int)InvoiceItemTypeId.HotelBooking;
                        if (!itinerary.IsDomestic)
                        {
                            //priceInfo = itinerary.Roomtype[i].Price;
                            //priceInfo.RateOfExchange = rateOfExchange;
                            //priceInfo.Save();
                        }
                        line.Price = itinerary.Roomtype[i].Price;
                        invoice.LineItem.Add(line);
                    }
                    using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                    {
                        //Audit.Add(EventType.GTABooking, Severity.Normal, 1, "Saving Inovoice now. IsDomestic:" + itinerary.IsDomestic, "");
                        invoice.Save(itinerary.IsDomestic, 0, ProductType.Hotel);
                        BookingHistory bhInvoice = new BookingHistory();
                        bhInvoice.BookingId = booking.BookingId;
                        bhInvoice.EventCategory = EventCategory.Payment;
                        bhInvoice.Remarks = "Invoice generated";
                        bhInvoice.CreatedBy = memberId;
                        bhInvoice.Save();
                        //invoice.Load(invoice.InvoiceNumber);
                        //if (itinerary.IsDomestic && itinerary.Source != HotelBookingSource.IAN)
                        {
                            NarrationBuilder objNarration = new NarrationBuilder();
                            //Ledger ledger = new Ledger();
                            //ledger.Load(booking.AgencyId, DateTime.Now, DateTime.Now);
                            decimal outVatAmount = 0;
                            for (int i = 0; i < itinerary.Roomtype.Length; i++)
                            {
                                totalAmount += itinerary.Roomtype[i].Price.GetAgentPrice();
                                outVatAmount += itinerary.Roomtype[i].Price.OutputVATAmount;
                                totalAmount += bOnBehalf ? itinerary.Roomtype[i].Price.AsvAmount : 0;
                            }
                            totalAmount = Math.Ceiling(totalAmount) + Math.Ceiling(outVatAmount);
                            LedgerTransaction ledgerTxn = new LedgerTransaction();

                            //To setup narration
                            objNarration.PaxName = string.Format("{0} {1}", itinerary.HotelPassenger.Firstname, itinerary.HotelPassenger.Lastname);
                            if (itinerary.PaymentMode == ModeOfPayment.CreditCard) //checking card or payment added by brahmam
                            {
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CardHotelBooked;
                                objNarration.Remarks = "Card Hotel Booking Created";
                            }
                            else
                            {
                                ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.HotelBooked;
                                objNarration.Remarks = "Hotel Booking Created";
                            }
                            objNarration.DocNo = invoice.CompleteInvoiceNumber;
                            objNarration.HotelConfirmationNo = itinerary.ConfirmationNo.Replace('|', '@');   //for miki purpose
                            objNarration.TravelDate = itinerary.StartDate.ToShortDateString();
                            ledgerTxn.LedgerId = booking.AgencyId;
                            ledgerTxn.Amount = -Math.Ceiling(totalAmount);
                            ledgerTxn.Narration = objNarration;
                            ledgerTxn.ReferenceId = itinerary.Roomtype[0].RoomId;
                            ledgerTxn.PaymentMode = Convert.ToInt32(itinerary.PaymentMode);

                            ledgerTxn.IsLCC = true;
                            ledgerTxn.Notes = "Confirmation NO :" + itinerary.ConfirmationNo;//for miki purpose
                            ledgerTxn.Date = DateTime.UtcNow;
                            ledgerTxn.CreatedBy = memberId;
                            ledgerTxn.TransType = itinerary.TransType;
                            ledgerTxn.Save();
                            LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);

                            //Agency.UpdateLCCBalance(invoice.AgencyId, -(totalAmount), memberId);
                        }
                        //TODO: White Label will be done later
                        //if (itinerary.BookingMode == BookingMode.WhiteLabel)
                        //{
                        //    SaveWlInvoice(invoice, itinerary, ticket);
                        //}
                        updateTransaction.Complete();
                    }
                }
                else if (invoice.InvoiceRaised(itinerary.HotelId, ProductType.Hotel) == itinerary.Roomtype.Length)
                {
                    invoice.InvoiceNumber = Invoice.isInvoiceGenerated(itinerary.Roomtype[0].RoomId, ProductType.Hotel);
                }
                else
                {
                    //TODO: serious error log it and send mail to admin with detail.
                    throw new BookingEngineException("Invoice Error");
                }
            }
            return invoice.InvoiceNumber;
        }

        /// <summary>
        /// Raises invoice for the given itinerary.
        /// </summary>
        /// <param name="itinerary">itinerary for which invoice is to be raised.</param>
        /// <param name="remarks">remarks if any.</param>
        /// <param name="memberId">memberId of the member who is currently logged in.</param>
        /// <returns>Invoice number generated. If invoice for itinerary is already generated then it will give the existing invoice number.</returns>
        public static int RaiseInvoicePart2(FlightItinerary itinerary, string remarks, int memberId, decimal rateOfExchange)
        {
            BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(itinerary.FlightId, ProductType.Flight));
            Invoice invoice = new Invoice();

            UserMaster clsUM = new UserMaster(itinerary.CreatedBy);
            bool bOnBehalf = clsUM.AgentId != itinerary.AgencyId;

            //if (itinerary.ETicketHit == 1)
            //{
            if (invoice.InvoiceRaised(itinerary.FlightId, ProductType.Flight) == 0)
            {
                decimal totalAmount = 0;
                invoice.PaymentMode = string.Empty;
                invoice.Remarks = remarks;
                invoice.AgencyId = booking.AgencyId;
                invoice.Status = InvoiceStatus.Paid;
                invoice.CreatedBy = memberId;
                invoice.CreatedOn = DateTime.UtcNow;
                invoice.TranxHeaderId = itinerary.FlightId; //Newly added on Sep-09-2014 sainadh
                                                            // Generating Invoice line items.
                invoice.LineItem = new List<InvoiceLineItem>();
                PriceAccounts priceInfo = new PriceAccounts();
                List<Ticket> tickets = Ticket.GetTicketList(itinerary.FlightId);
                for (int i = 0; i < tickets.Count; i++)
                {
                    InvoiceLineItem line = new InvoiceLineItem();
                    line.ItemDescription = string.Empty;
                    line.ItemReferenceNumber = tickets[i].TicketId;
                    //TODO: line item type Id. should not be hardcoded 3.
                    line.ItemTypeId = (int)InvoiceItemTypeId.Ticketed;
                    if (!itinerary.IsDomestic)
                    {
                        //priceInfo = tickets[i].Price;
                        //priceInfo.RateOfExchange = rateOfExchange;
                        //priceInfo.Save();
                    }
                    line.Price = tickets[i].Price;
                    invoice.LineItem.Add(line);
                }
                BookingHistory bhInvoice = new BookingHistory();
                bhInvoice.BookingId = booking.BookingId;
                bhInvoice.EventCategory = EventCategory.Payment;
                bhInvoice.Remarks = "Invoice generated";
                bhInvoice.CreatedBy = memberId;

                //Audit.Add(EventType.GTABooking, Severity.Normal, 1, "Saving Inovoice now. IsDomestic:" + itinerary.IsAccounted, "");
                List<LedgerTransaction> ledgerTransactionList = new List<LedgerTransaction>();

                for (int i = 0; i < tickets.Count; i++)
                {
                    decimal ledgerAmount = 0;
                    NarrationBuilder objNarration = new NarrationBuilder();
                    LedgerTransaction ledgerTxn = new LedgerTransaction();
                    ledgerTxn.LedgerId = booking.AgencyId;
                    if (itinerary.FlightBookingSource == BookingSource.OffLine)// For Offline Entry
                    {
                        ledgerAmount = tickets[i].Price.PublishedFare + tickets[i].Price.Tax + tickets[i].Price.Markup + tickets[i].Price.AsvAmount - tickets[i].Price.Discount - tickets[i].Price.TransactionFee - tickets[i].Price.AdditionalTxnFee + tickets[i].Price.OutputVATAmount;
                    }
                    else
                    {
                        ledgerAmount = tickets[i].Price.GetAgentPrice() + tickets[i].Price.Markup + tickets[i].Price.BaggageCharge + tickets[i].Price.MealCharge - tickets[i].Price.Discount + tickets[i].Price.OutputVATAmount + tickets[i].Price.HandlingFeeAmount + tickets[i].Price.SeatPrice; // table level mark up also to be added in GetAgentPrice
                        ledgerAmount += bOnBehalf ? tickets[i].Price.AsvAmount : 0;
                        if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                        {
                            ledgerAmount += tickets[i].Price.AdditionalTxnFee + tickets[i].Price.OtherCharges + tickets[i].Price.SServiceFee + tickets[i].Price.TransactionFee;
                        }
                    }
                    ledgerTxn.Amount = -ledgerAmount;
                    totalAmount += ledgerAmount;
                    objNarration.PaxName = string.Format("{0} {1}", tickets[i].PaxFirstName, tickets[i].PaxLastName);

                    if (itinerary.PaymentMode == ModeOfPayment.Credit || itinerary.PaymentMode == ModeOfPayment.CreditLimit)
                    {
                        objNarration.Remarks = "Ticket Created";
                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.TicketCreated;
                    }
                    else
                    {
                        objNarration.Remarks = "Card Ticket Created";
                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CardTicketCreated;
                    }
                    objNarration.TicketNo = tickets[i].TicketNumber.TrimEnd('|').Replace("|", "-");
                    objNarration.TravelDate = itinerary.TravelDate.ToShortDateString();
                    objNarration.FlightNo = itinerary.ValidatingAirlineCode + itinerary.Segments[0].FlightNumber;
                    objNarration.Sector = Ticket.GetItineraryString(itinerary); // Ticket.GetItineraryString(itinerary);
                    ledgerTxn.Narration = objNarration;
                    ledgerTxn.ReferenceId = tickets[i].TicketId;
                    ledgerTxn.PaymentMode = (int)itinerary.PaymentMode;
                    ledgerTxn.IsLCC = true;
                    if (itinerary.PNR.Contains("|"))//Check for Multiple PNR's for TBOAir bcoz Ledger Narration splits Notes based on '|' symbol
                    {
                        ledgerTxn.Notes = "PNR:" + itinerary.PNR.TrimEnd('|').Replace("|", "-");
                    }
                    else
                    {
                        ledgerTxn.Notes = "PNR:" + itinerary.PNR;
                    }
                    ledgerTxn.Date = DateTime.UtcNow;
                    ledgerTxn.CreatedBy = memberId;
                    ledgerTxn.TransType = itinerary.TransactionType;
                    ledgerTransactionList.Add(ledgerTxn);
                }
                invoice.Status = InvoiceStatus.Paid;
                invoice.PaymentMode = ((int)itinerary.PaymentMode).ToString();
                using (TransactionScope updateTransaction = new TransactionScope())
                {
                    //Audit.Add(EventType.GTABooking, Severity.Normal, 1, "Saving Inovoice now. IsDomestic:" + itinerary.IsDomestic, "");
                    invoice.Save(itinerary.IsDomestic, 0, ProductType.Flight);
                    bhInvoice.Save();
                    foreach (LedgerTransaction ledgerTransaction in ledgerTransactionList)
                    {
                        ledgerTransaction.Narration.DocNo = Convert.ToString(invoice.DocTypeCode + Convert.ToString(invoice.DocumentNumber));
                        ledgerTransaction.CreatedOn = invoice.CreatedOn;
                        ledgerTransaction.Save();
                        LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTransaction.TxnId);
                    }

                    updateTransaction.Complete();
                }
                //using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                //{
                //    Audit.Add(EventType.GTABooking, Severity.Normal, 1, "Saving Inovoice now. IsDomestic:" + itinerary.IsDomestic, "");
                //    invoice.Save(itinerary.IsDomestic, 0, ProductType.Flight);
                //    //invoice.Load(invoice.InvoiceNumber);
                //    //if (itinerary.IsDomestic && itinerary.Source != HotelBookingSource.IAN)
                //    //{
                //    //    NarrationBuilder objNarration = new NarrationBuilder();
                //    //    Ledger ledger = new Ledger();
                //    //    ledger.Load(booking.AgencyId, DateTime.Now, DateTime.Now);
                //    //    for (int i = 0; i < itinerary.Roomtype.Length; i++)
                //    //    {
                //    //        totalAmount = totalAmount + itinerary.Roomtype[i].Price.GetAgentPrice();
                //    //        LedgerTransaction ledgerTxn = new LedgerTransaction();

                //    //        //To setup narration
                //    //        objNarration.PaxName = string.Format("{0} {1}", itinerary.HotelPassenger.Firstname, itinerary.HotelPassenger.Lastname);
                //    //        objNarration.Remarks = "Hotel Booking Created";
                //    //        objNarration.DocNo = invoice.CompleteInvoiceNumber;
                //    //        objNarration.HotelConfirmationNo = itinerary.ConfirmationNo;
                //    //        objNarration.TravelDate = itinerary.StartDate.ToShortDateString();
                //    //        ledgerTxn.LedgerId = ledger.LedgerId;
                //    //        ledgerTxn.Amount = -(itinerary.Roomtype[i].Price.GetAgentPrice());
                //    //        ledgerTxn.Narration = objNarration;
                //    //        ledgerTxn.ReferenceId = itinerary.Roomtype[i].RoomId;
                //    //        ledgerTxn.ReferenceType = ReferenceType.HotelBooked;
                //    //        ledgerTxn.IsLCC = true; //For Hotel the ledger transaction will be like LCC
                //    //        ledgerTxn.Notes = "";
                //    //        ledgerTxn.Date = DateTime.UtcNow;
                //    //        ledgerTxn.CreatedBy = memberId;
                //    //        ledgerTxn.Save();
                //    //        LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);
                //    //    }
                //    //    Agency.UpdateLCCBalance(invoice.AgencyId, -(totalAmount), memberId);
                //    //}
                //    //TODO: White Label will be done later
                //    //if (itinerary.BookingMode == BookingMode.WhiteLabel)
                //    //{
                //    //    SaveWlInvoice(invoice, itinerary, ticket);
                //    //}
                //    updateTransaction.Complete();
                //}
            }
            else if (invoice.InvoiceRaised(itinerary.FlightId, ProductType.Flight) == itinerary.Passenger.Length)
            {
                invoice.InvoiceNumber = Invoice.isInvoiceGenerated(itinerary.Passenger[0].PaxId, ProductType.Flight);
            }
            else
            {
                //TODO: serious error log it and send mail to admin with detail.
                throw new BookingEngineException("Invoice Error");
            }
            //}
            return invoice.InvoiceNumber;
        }

        /// <summary>
        /// Raise invoice for Insurance
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="rateOfExchange"></param>
        /// <returns></returns>
        public static int RaiseInvoicePart2(InsuranceHeader insHeader, string remarks, int memberId, decimal rateOfExchange)
        {
            Invoice invoice = new Invoice();

            if (invoice.InvoiceRaised(insHeader.Id, ProductType.Insurance) == 0)
            {
                decimal totalAmount = 0;
                invoice.PaymentMode = string.Empty;
                invoice.Remarks = remarks;
                invoice.AgencyId = insHeader.AgentId;
                invoice.Status = InvoiceStatus.Paid;
                invoice.CreatedBy = memberId;
                invoice.CreatedOn = DateTime.UtcNow;
                invoice.TranxHeaderId = insHeader.Id;
                invoice.TotalPrice = insHeader.TotalAmount;
                invoice.PaymentMode = ((int)insHeader.PaymentMode).ToString();
                // Generating Invoice line items.
                invoice.LineItem = new List<InvoiceLineItem>();                

                InvoiceLineItem line = new InvoiceLineItem();
                line.ItemDescription = string.Empty;
                line.ItemReferenceNumber = insHeader.Id;
                line.ItemTypeId = (int)InvoiceItemTypeId.InsuranceBooking;
                line.Price = new PriceAccounts();
                line.Price.PriceId = (int)insHeader.Id;
                invoice.LineItem.Add(line);

                List<LedgerTransaction> ledgerTransactionList = new List<LedgerTransaction>();
                using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                {
                    //Audit.Add(EventType.InsuranceBooking, Severity.Normal, 1, "Saving Inovoice now.", "");
                    invoice.Save(false, 0, ProductType.Insurance);

                    NarrationBuilder objNarration = new NarrationBuilder();
                    LedgerTransaction ledgerTxn = new LedgerTransaction();
                    ledgerTxn.LedgerId = insHeader.AgentId;
                    ledgerTxn.PaymentMode = (int)insHeader.PaymentMode;
                    decimal ledgerAmount = insHeader.TotalAmount;
                    ledgerTxn.Amount = -ledgerAmount;
                    totalAmount += ledgerAmount;
                    objNarration.PaxName = string.Format("{0} {1}", insHeader.InsPassenger[0].FirstName, insHeader.InsPassenger[0].LastName);
                    objNarration.Remarks = "Insurance Booking";
                    objNarration.DocNo = invoice.CompleteInvoiceNumber;
                    string policyNo = string.Empty;
                    for (int k = 0; k < insHeader.InsPlans.Count; k++)
                    {
                        if (!string.IsNullOrEmpty(policyNo))
                        {
                            policyNo = policyNo + "," + insHeader.InsPlans[k].PolicyNo;
                        }
                        else
                        {
                            policyNo = insHeader.InsPlans[k].PolicyNo;
                        }
                    }

                    objNarration.PolicyNo = policyNo;
                    objNarration.TravelDate = insHeader.DepartureDateTime;
                    ledgerTxn.Narration = objNarration;
                    ledgerTxn.ReferenceId = (int)insHeader.Id;
                    ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.InsuranceCreated;
                    ledgerTxn.Notes = insHeader.PNR;
                    ledgerTxn.Date = DateTime.UtcNow;
                    ledgerTxn.CreatedBy = memberId;
                    ledgerTxn.TransType = insHeader.TransType;
                    ledgerTransactionList.Add(ledgerTxn);
                    ledgerTxn.Save();
                    LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);
                    updateTransaction.Complete();
                }
            }
            else if (Convert.ToBoolean(invoice.InvoiceRaised(insHeader.Id, ProductType.Insurance)))
            {
                invoice.InvoiceNumber = Invoice.isInvoiceGenerated(insHeader.Id, ProductType.Insurance);
            }
            else
            {
                //TODO: serious error log it and send mail to admin with detail.
                throw new BookingEngineException("Invoice Error");
            }

            return invoice.InvoiceNumber;
        }


        /// <summary>
        /// Raise invoice for Activity
        /// </summary>
        /// <param name="Activities"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="rateOfExchange"></param>
        /// <returns></returns>
        public static int RaiseInvoicePart2(Activity Activities, string remarks, int memberId, decimal rateOfExchange, ProductType prodType)
        {
            Invoice invoice = new Invoice();
            int invoiceCount = 0;
            if (prodType == ProductType.Activity)
            {
                invoiceCount = invoice.InvoiceRaised((int)Activities.Id, ProductType.Activity);
            }
            else
            {
                invoiceCount = invoice.InvoiceRaised((int)Activities.Id, ProductType.FixedDeparture);
            }
            if (invoiceCount == 0)
            {
                decimal totalAmount = 0;
                invoice.PaymentMode = string.Empty;
                invoice.Remarks = remarks;
                invoice.AgencyId = Convert.ToInt32(Activities.TransactionHeader.Rows[0]["AgencyId"]);
                invoice.Status = InvoiceStatus.Paid;
                invoice.CreatedBy = memberId;
                invoice.CreatedOn = DateTime.UtcNow;
                invoice.TranxHeaderId = Activities.Id;
                invoice.TotalPrice = Convert.ToDecimal(Activities.TransactionHeader.Rows[0]["TotalPrice"]);
                // Generating Invoice line items.
                invoice.LineItem = new List<InvoiceLineItem>();
                PriceAccounts priceInfo = new PriceAccounts();
                DataTable passengers = Activities.TransactionDetail;
                for (int i = 0; i < passengers.Rows.Count; i++)
                {
                    InvoiceLineItem line = new InvoiceLineItem();
                    line.ItemDescription = string.Empty;
                    //line.ItemReferenceNumber = Convert.ToInt32(passengers.Rows[i]["ATDId"]);
                    line.ItemReferenceNumber = Convert.ToInt32(passengers.Rows[i]["ATHDId"]);
                    if (prodType == ProductType.Activity)
                    {
                        line.ItemTypeId = (int)InvoiceItemTypeId.ActivityBooking;
                    }
                    else if (prodType == ProductType.FixedDeparture)
                    {
                        line.ItemTypeId = (int)InvoiceItemTypeId.FixedDepartureBooking;
                    }
                    line.Price = new PriceAccounts();
                    line.Price.PriceId = Convert.ToInt32(passengers.Rows[i]["ATDId"]);
                    invoice.LineItem.Add(line);
                }

                List<LedgerTransaction> ledgerTransactionList = new List<LedgerTransaction>();
                for (int i = 0; i < passengers.Rows.Count; i++)
                {
                    NarrationBuilder objNarration = new NarrationBuilder();
                    LedgerTransaction ledgerTxn = new LedgerTransaction();
                    ledgerTxn.LedgerId = (int)Activities.TransactionHeader.Rows[0]["AgencyId"];
                    decimal ledgerAmount = Convert.ToDecimal(Activities.TransactionHeader.Rows[0]["TotalPrice"]);
                    ledgerTxn.Amount = -ledgerAmount;
                    totalAmount += ledgerAmount;
                    objNarration.PaxName = string.Format("{0} {1}", passengers.Rows[i]["FirstName"].ToString(), passengers.Rows[i]["LastName"].ToString());
                    objNarration.PolicyNo = Activities.TransactionHeader.Rows[0]["TripId"].ToString();
                    objNarration.TravelDate = Convert.ToDateTime(Activities.TransactionHeader.Rows[0]["Booking"]).ToString("MM/dd/yyyy");
                    if (prodType == ProductType.Activity)
                    {
                        objNarration.Remarks = "Activity Booking";
                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.ActivityBooked;
                    }
                    else if (prodType == ProductType.FixedDeparture)
                    {
                        objNarration.Remarks = "FixedDeparture Booking";
                        ledgerTxn.ReferenceType = ReferenceType.FixedDepartureBooked;
                    }
                    ledgerTxn.Narration = objNarration;
                    ledgerTxn.ReferenceId = Convert.ToInt32(passengers.Rows[i]["ATDId"]);
                    ledgerTxn.Notes = "";
                    ledgerTxn.Date = DateTime.UtcNow;
                    ledgerTxn.CreatedBy = memberId;
                    ledgerTransactionList.Add(ledgerTxn);
                }
                invoice.Status = InvoiceStatus.Paid;
                using (TransactionScope updateTransaction = new TransactionScope())
                {
                    if (prodType == ProductType.Activity)
                    {
                        invoice.Save(true, 0, ProductType.Activity);
                    }
                    else if (prodType == ProductType.FixedDeparture)
                    {
                        invoice.Save(true, 0, ProductType.FixedDeparture);
                    }
                    foreach (LedgerTransaction ledgerTransaction in ledgerTransactionList)
                    {
                        ledgerTransaction.Narration.DocNo = Convert.ToString(invoice.DocTypeCode + Convert.ToString(invoice.DocumentNumber));
                        ledgerTransaction.CreatedOn = invoice.CreatedOn;
                        if (prodType == ProductType.Activity)
                        {
                            ledgerTransaction.Save();
                            LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTransaction.TxnId);
                        }
                    }

                    updateTransaction.Complete();
                }
            }
            else if (invoiceCount == Activities.TransactionDetail.Rows.Count)
            {
                if (prodType == ProductType.Activity)
                {
                    invoice.InvoiceNumber = Invoice.isInvoiceGenerated(Convert.ToInt32(Activities.TransactionDetail.Rows[0]["ATDId"]), ProductType.Activity);
                }
                else if (prodType == ProductType.FixedDeparture)
                {
                    invoice.InvoiceNumber = Invoice.isInvoiceGenerated(Convert.ToInt32(Activities.TransactionDetail.Rows[0]["ATDId"]), ProductType.FixedDeparture);
                }
            }
            else
            {
                throw new BookingEngineException("Invoice Error");
            }
            //}
            return invoice.InvoiceNumber;
        }

        /// <summary>
        /// Raises invoice for the given itinerary.
        /// </summary>
        /// <param name="itinerary">itinerary for which invoice is to be raised.</param>
        /// <param name="remarks">remarks if any.</param>
        /// <param name="memberId">memberId of the member who is currently logged in.</param>
        /// <returns>Invoice number generated. If invoice for itinerary is already generated then it will give the existing invoice number.</returns>
        public static int RaiseInvoice(HotelItinerary itinerary, string remarks, int memberId, decimal rateOfExchange)
        {
            //GTADBG
            //Audit.Add(EventType.GTABooking, Severity.Normal, 1, "Hotel Raise Invoice entered. conf no : " + itinerary.ConfirmationNo + " hotelId : " + itinerary.HotelId, "");
            if (itinerary.Roomtype.Length == 0)
            {
                HotelRoom hRoom = new HotelRoom();
                itinerary.Roomtype = hRoom.Load(itinerary.HotelId);
            }
            return RaiseInvoicePart2(itinerary, remarks, memberId, rateOfExchange);
        }

        /// <summary>
        /// Raises invoice for the given itinerary.
        /// </summary>
        /// <param name="itinerary">itinerary for which invoice is to be raised.</param>
        /// <param name="remarks">remarks if any.</param>
        /// <param name="memberId">memberId of the member who is currently logged in.</param>
        /// <returns>Invoice number generated. If invoice for itinerary is already generated then it will give the existing invoice number.</returns>
        public static int RaiseInvoice(FlightItinerary itinerary, string remarks, int memberId, decimal rateOfExchange)
        {
            //GTADBG
            //Audit.Add(EventType.GTABooking, Severity.Normal, 1, "Flight Raise Invoice entered. conf no : " + itinerary.PNR + " flightId : " + itinerary.FlightId, "");

            return RaiseInvoicePart2(itinerary, remarks, memberId, rateOfExchange);
        }

        /// <summary>
        /// Raise invoice for Insurance.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="rateOfExchange"></param>
        /// <returns></returns>
        public static int RaiseInvoice(InsuranceHeader plan, string remarks, int memberId, decimal rateOfExchange)
        {
            //Audit.Add(EventType.InsuranceBooking, Severity.Normal, 1, "Insurance Raise Invoice entered. Agent Ref no : " + plan.AgentRefNo + " Insurance Id : " + plan.Id, "");
            return RaiseInvoicePart2(plan, remarks, memberId, rateOfExchange);
        }

        //Added by Suresh V for Baggage Insurance
        public static int RaiseInvoice(BaggageInsuranceHeader plan, string remarks, int memberId, decimal rateOfExchange)
        {
            //Audit.Add(EventType.InsuranceBooking, Severity.Normal, 1, "Insurance Raise Invoice entered. Agent Ref no : " + plan.AgentRefNo + " Insurance Id : " + plan.Id, "");
            return RaiseInvoicePart2(plan, remarks, memberId, rateOfExchange);
        }

        public static int RaiseInvoicePart2(BaggageInsuranceHeader insHeader, string remarks, int memberId, decimal rateOfExchange)
        {
            Invoice invoice = new Invoice();

            if (invoice.InvoiceRaised(insHeader.Bid, ProductType.BaggageInsurance) == 0)
            {
                decimal totalAmount = 0;
                invoice.PaymentMode = string.Empty;
                invoice.Remarks = remarks;
                invoice.AgencyId = insHeader.AgentID;
                invoice.Status = InvoiceStatus.Paid;
                invoice.CreatedBy = memberId;
                invoice.CreatedOn = DateTime.UtcNow;
                invoice.TranxHeaderId = insHeader.Bid;
                invoice.TotalPrice = insHeader.TotalAmount;
                // Generating Invoice line items.
                invoice.LineItem = new List<InvoiceLineItem>();


                InvoiceLineItem line = new InvoiceLineItem();
                line.ItemDescription = string.Empty;
                line.ItemReferenceNumber = insHeader.Bid;
                line.ItemTypeId = (int)InvoiceItemTypeId.BaggageInsuranceBooking;
                line.Price = new PriceAccounts();
                line.Price.PriceId = (int)insHeader.Bid;
                invoice.LineItem.Add(line);

                List<LedgerTransaction> ledgerTransactionList = new List<LedgerTransaction>();
                using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                {
                    //Audit.Add(EventType.InsuranceBooking, Severity.Normal, 1, "Saving Inovoice now.", "");
                    invoice.Save(false, 0, ProductType.BaggageInsurance);

                    NarrationBuilder objNarration = new NarrationBuilder();
                    LedgerTransaction ledgerTxn = new LedgerTransaction();
                    ledgerTxn.LedgerId = insHeader.AgentID;
                    decimal ledgerAmount = insHeader.TotalAmount;
                    ledgerTxn.Amount = -ledgerAmount;
                    totalAmount += ledgerAmount;
                    objNarration.PaxName = string.Format("{0} {1}", insHeader.FirstName, insHeader.LastName);
                    objNarration.Remarks = "Baggage Insurance Booking";
                    objNarration.DocNo = invoice.CompleteInvoiceNumber;
                    objNarration.PolicyNo = insHeader.Policy;
                    //objNarration.TravelDate = insHeader.DepartureDateTime;
                    ledgerTxn.Narration = objNarration;
                    ledgerTxn.ReferenceId = (int)insHeader.Bid;
                    ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.InsuranceCreated;
                    ledgerTxn.Notes = insHeader.PNR;
                    ledgerTxn.Date = DateTime.UtcNow;
                    ledgerTxn.CreatedBy = memberId;
                    ledgerTxn.TransType = "B2B";
                    ledgerTransactionList.Add(ledgerTxn);
                    ledgerTxn.Save();
                    LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);
                    updateTransaction.Complete();
                }
            }
            else if (Convert.ToBoolean(invoice.InvoiceRaised(insHeader.Bid, ProductType.BaggageInsurance)))
            {
                invoice.InvoiceNumber = Invoice.isInvoiceGenerated(insHeader.Bid, ProductType.BaggageInsurance);
            }
            else
            {
                //TODO: serious error log it and send mail to admin with detail.
                throw new BookingEngineException("Invoice Error");
            }

            return invoice.InvoiceNumber;
        }
        // Added By Hari Malla on 04-02-2019
        public static int RaiseReligareInvoice(ReligareHeader header, string remarks, int memberId, decimal rateOfExchange)
        {
            return RaiseInvoicePart2(header, remarks, memberId, rateOfExchange);
        }
        // Added By Hari Malla on 04-02-2019
        public static int RaiseInvoicePart2(ReligareHeader header, string remarks, int memberId, decimal rateOfExchange)
        {
            Invoice invoice = new Invoice();
            if (Invoice.isReligareInvoiceGenerated(header.Header_id, ProductType.Insurance, remarks) == 0)
            {
                decimal totalAmount = 0;
                invoice.PaymentMode = string.Empty;
                invoice.Remarks = "Religare";
                invoice.StaffRemarks = "Religare";
                invoice.AgencyId = header.Agent_id;
                invoice.Status = InvoiceStatus.PartiallyPaid;
                invoice.CreatedBy = memberId;
                invoice.CreatedOn = DateTime.UtcNow;
                invoice.TranxHeaderId = header.Header_id;
                invoice.TotalPrice = Convert.ToDecimal(header.Premium_Amount);
                invoice.DocTypeCode = "RG";
                invoice.InvoiceDueDate = DateTime.Now.AddDays(1);
                // Generating Invoice line items.
                List<InvoiceLineItem> li_lineItem = new List<InvoiceLineItem>();
                InvoiceLineItem lineItem;
                foreach (ReligarePassengers pax in header.ReligarePassengers)
                {
                    lineItem = new InvoiceLineItem();
                    PriceAccounts accounts = new PriceAccounts();
                    accounts.PriceId = Convert.ToInt32(header.Header_id);
                    lineItem.Price = accounts;
                    lineItem.ItemReferenceNumber = header.Header_id;
                    lineItem.ItemTypeId = (int)ProductType.Insurance;
                    lineItem.ItemDescription = "RG";
                    li_lineItem.Add(lineItem);
                }
                invoice.LineItem = li_lineItem;
                List<LedgerTransaction> ledgerTransactionList = new List<LedgerTransaction>();
                using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                {
                    invoice.Save(ProductType.Insurance, false);
                    NarrationBuilder objNarration = new NarrationBuilder();
                    LedgerTransaction ledgerTxn = new LedgerTransaction();
                    ledgerTxn.LedgerId = header.Agent_id;
                    decimal ledgerAmount = Convert.ToDecimal(header.Premium_Amount);
                    ledgerTxn.Amount = -ledgerAmount;
                    totalAmount += ledgerAmount;
                    objNarration.PaxName = string.Format("{0} {1}", header.ReligarePassengers[0].FirstName, header.ReligarePassengers[0].LastName);
                    objNarration.Remarks = "Religare Insurance Booking Policy No " + header.Policy_no + " created.";
                    objNarration.DocNo = "RG" + Convert.ToString(invoice.InvoiceNumber);
                    objNarration.PolicyNo = header.Policy_no;
                    objNarration.TravelDate = header.Startdate.ToString();
                    ledgerTxn.Narration = objNarration;
                    ledgerTxn.ReferenceId = (int)header.Header_id;
                    ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.InsuranceCreated;
                    ledgerTxn.Notes = "";
                    ledgerTxn.Date = DateTime.UtcNow;
                    ledgerTxn.CreatedBy = memberId;
                    ledgerTxn.TransType = header.Trans_type;
                    ledgerTransactionList.Add(ledgerTxn);
                    ledgerTxn.Save();
                    LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);
                    updateTransaction.Complete();
                }
            }
            else if (Convert.ToBoolean(invoice.InvoiceRaised(header.Header_id, ProductType.Insurance)))
            {
                invoice.InvoiceNumber = Invoice.isReligareInvoiceGenerated(header.Header_id, ProductType.Insurance, "RG");
            }
            else
            {
                //TODO: serious error log it and send mail to admin with detail.
                throw new BookingEngineException("Invoice Error");
            }
            return invoice.InvoiceNumber;
        }

        /// <summary>
        /// Raise invoice for Activity.
        /// </summary>
        /// <param name="Activity"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="rateOfExchange"></param>
        /// <returns></returns>
        public static int RaiseInvoice(Activity activities, string remarks, int memberId, decimal rateOfExchange, ProductType prodType)
        {
            //Audit.Add(EventType.Book, Severity.Normal, 1, "Activity Raise Invoice entered. ActivityId : " + activities.Id, "");
            return RaiseInvoicePart2(activities, remarks, memberId, rateOfExchange, prodType);
        }


        /// This Overload method is used to Raise Invoice with extra parameter Rate of Exchange.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="prodType"></param>
        /// <param name="rateOfExchange"></param>
        /// <returns></returns>
        public static int RaiseInvoice(int productId, string remarks, int memberId, ProductType prodType, decimal rateOfExchange)
        {
            int raiseInvoice = 0;
            if (prodType == ProductType.Flight)
            {
                FlightItinerary itinerary = new FlightItinerary(productId);
                raiseInvoice = RaiseInvoice(itinerary, remarks, memberId, rateOfExchange);
            }
            else if (prodType == ProductType.Hotel)
            {
                HotelItinerary itinerary = new HotelItinerary();
                itinerary.Load(productId);
                HotelRoom room = new HotelRoom();
                HotelPassenger passInfo = new HotelPassenger();
                passInfo.Load(productId);
                itinerary.Roomtype = room.Load(productId);
                itinerary.HotelPassenger = passInfo;
                raiseInvoice = RaiseInvoice(itinerary, remarks, memberId, rateOfExchange);
            }
            else if (prodType == ProductType.BaggageInsurance)
            {
                BaggageInsuranceHeader baggageheader = new BaggageInsuranceHeader();
                baggageheader.LoadBaggageHeader(productId);
                raiseInvoice = RaiseInvoice(baggageheader, remarks, memberId, rateOfExchange);
            }

            else if (prodType == ProductType.Insurance)
            {
                // Added By Hari Malla on 04-02-2019 for Religare Insurance.
                if (remarks == "RG")
                {
                    ReligareHeader header = new ReligareHeader();
                    header.Load(productId);
                    raiseInvoice = RaiseReligareInvoice(header, remarks, memberId, rateOfExchange);
                }
                else
                {
                    InsuranceHeader header = new InsuranceHeader();
                    header.RetrieveConfirmedPlan(productId);
                    raiseInvoice = RaiseInvoice(header, remarks, memberId, rateOfExchange);
                }
            }
            else if (prodType == ProductType.Activity)
            {
                Activity activity = new Activity();
                activity.GetActivityForQueue(productId);
                raiseInvoice = RaiseInvoice(activity, remarks, memberId, rateOfExchange, ProductType.Activity);
            }
            else if (prodType == ProductType.Transfers)
            {
                TransferItinerary itinerary = new TransferItinerary();
                itinerary.Load(productId);
                raiseInvoice = RaiseInvoice(itinerary, remarks, memberId, rateOfExchange, ProductType.Transfers);
            }
            else if (prodType == ProductType.SightSeeing)
            {
                SightseeingItinerary itinerary = new SightseeingItinerary();
                itinerary.Load(productId);
                raiseInvoice = RaiseInvoice(itinerary, remarks, memberId, rateOfExchange);
            }
            else if (prodType == ProductType.Car)
            {
                FleetItinerary itinerary = new FleetItinerary();
                itinerary.Load(productId);
                raiseInvoice = RaiseInvoice(itinerary, remarks, memberId, rateOfExchange, ProductType.Car);
            }
            else if (prodType == ProductType.MobileRecharge)
            {
                //MobileRechargeItinerary itinerary = new MobileRechargeItinerary();
                //itinerary.Load(productId);
                //raiseInvoice = RaiseMobileRechargeInvoice(itinerary, remarks, memberId);
            }
            else if (prodType == ProductType.FixedDeparture)
            {
                Activity activity = new Activity();
                activity.GetActivityForQueue(productId);
                raiseInvoice = RaiseInvoice(activity, remarks, memberId, rateOfExchange, ProductType.FixedDeparture);
            }
            return raiseInvoice;
        }

        #region sightSeeing
        /// <summary>
        /// This Method is used to Raise the Sightseeings Invoice
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="rateOfExchange"></param>
        /// <returns></returns>
        public static int RaiseInvoice(SightseeingItinerary itinerary, string remarks, int memberId, decimal rateOfExchange)
        {
            //GTADBG
            //Audit.Add(EventType.SightseeingBooking, Severity.Normal, 1, "Sightseeing Raise Invoice entered. conf no : " + itinerary.ConfirmationNo + " Id : " + itinerary.SightseeingId, "");
            BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(itinerary.SightseeingId, ProductType.SightSeeing));
            Invoice invoice = new Invoice();

            if (itinerary.BookingStatus == SightseeingBookingStatus.Confirmed)
            {
                if (invoice.InvoiceRaised(itinerary.SightseeingId, ProductType.SightSeeing) == 0)
                {

                    decimal totalAmount = 0;
                    invoice.PaymentMode = string.Empty;
                    invoice.Remarks = remarks;
                    invoice.AgencyId = booking.AgencyId;
                    invoice.Status = InvoiceStatus.Paid;
                    invoice.CreatedBy = memberId;
                    invoice.CreatedOn = DateTime.UtcNow;
                    invoice.TranxHeaderId = itinerary.SightseeingId;

                    // Generating Invoice line items.
                    invoice.LineItem = new List<InvoiceLineItem>();
                    //PriceAccounts priceInfo = new PriceAccounts();
                    InvoiceLineItem line = new InvoiceLineItem();
                    line.ItemDescription = string.Empty;
                    line.ItemReferenceNumber = itinerary.SightseeingId;
                    line.ItemTypeId = (int)InvoiceItemTypeId.SightseeingBooking;
                    line.Price = itinerary.Price;
                    invoice.LineItem.Add(line);
                    using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                    {
                        // Audit.Add(EventType.SightseeingBooking, Severity.Normal, 1, "Saving Inovoice now.", "");
                        invoice.Save(false, 0, ProductType.SightSeeing);

                        NarrationBuilder objNarration = new NarrationBuilder();
                        //Ledger ledger = new Ledger();
                        //ledger.Load(booking.AgencyId, DateTime.Now, DateTime.Now);

                        totalAmount = itinerary.Price.GetAgentPrice() + Math.Ceiling(itinerary.Price.OutputVATAmount);
                        LedgerTransaction ledgerTxn = new LedgerTransaction();

                        //To setup narration
                        objNarration.PaxName = itinerary.PaxNames[0].ToString(); ;
                        objNarration.Remarks = "SightSeeing Booking Created";
                        objNarration.DocNo = invoice.CompleteInvoiceNumber;
                        objNarration.TourConfirmationNo = itinerary.ConfirmationNo;
                        objNarration.TravelDate = itinerary.TourDate.ToShortDateString();
                        ledgerTxn.LedgerId = booking.AgencyId;
                        //ledgerTxn.Amount = -Math.Ceiling(totalAmount);
                        ledgerTxn.Amount = -Math.Ceiling(totalAmount); //modified on 08082016 by chandan
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.ReferenceId = itinerary.SightseeingId;
                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.SightseeingBooked;
                        ledgerTxn.IsLCC = true;
                        ledgerTxn.Notes = "Confirmation NO :" + itinerary.ConfirmationNo;
                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = memberId;
                        ledgerTxn.TransType = itinerary.TransType;
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);
                        updateTransaction.Complete();

                    }
                }
                else if (Convert.ToBoolean(invoice.InvoiceRaised(itinerary.SightseeingId, ProductType.SightSeeing)))
                {
                    invoice.InvoiceNumber = Invoice.isInvoiceGenerated(itinerary.SightseeingId, ProductType.SightSeeing);
                }
                else
                {
                    //TODO: serious error log it and send mail to admin with detail.
                    throw new BookingEngineException("Invoice Error");
                }
            }
            return invoice.InvoiceNumber;
        }
        public static int RaiseInvoice(TransferItinerary itinerary, string remarks, int memberId, decimal rateOfExchange, ProductType prodType)
        {
            //Audit.Add(EventType.Book, Severity.Normal, 1, "Transfer Raise Invoice entered. TransferId : " + itinerary.TransferId, "");
            return RaiseInvoicePart2(itinerary, remarks, memberId, rateOfExchange, prodType);
        }
        public static int RaiseInvoicePart2(TransferItinerary itinerary, string remarks, int memberId, decimal rateOfExchange, ProductType prodType)
        {

            //Audit.Add(EventType.TransferBooking, Severity.Normal, 1, "Transfer Raise Invoice entered. conf no : " + itinerary.ConfirmationNo + " Id : " + itinerary.TransferId, "");
            BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(itinerary.TransferId, ProductType.Transfers));
            Invoice invoice = new Invoice();

            if (itinerary.BookingStatus == TransferBookingStatus.Confirmed)
            {
                if (invoice.InvoiceRaised(itinerary.TransferId, ProductType.Transfers) == 0)
                {
                    decimal totalAmount = 0;
                    invoice.PaymentMode = string.Empty;
                    invoice.Remarks = remarks;
                    invoice.AgencyId = booking.AgencyId;
                    invoice.Status = InvoiceStatus.Paid;
                    invoice.CreatedBy = memberId;
                    invoice.CreatedOn = DateTime.UtcNow;
                    invoice.TranxHeaderId = itinerary.TransferId;
                    // Generating Invoice line items.
                    invoice.LineItem = new List<InvoiceLineItem>();
                    for (int i = 0; i < itinerary.TransferDetails.Count; i++)
                    {
                        InvoiceLineItem line = new InvoiceLineItem();
                        line.ItemDescription = string.Empty;
                        line.ItemReferenceNumber = itinerary.TransferId;
                        line.ItemTypeId = (int)InvoiceItemTypeId.TransferBooking;
                        line.Price = itinerary.TransferDetails[i].ItemPrice;
                        invoice.LineItem.Add(line);
                    }

                    // Generating Invoice line items.
                    PriceAccounts priceInfo = new PriceAccounts();
                    using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                    {
                        //Audit.Add(EventType.TransferBooking, Severity.Normal, 1, "Saving Inovoice now.", "");
                        invoice.Save(false, 0, ProductType.Transfers);

                        NarrationBuilder objNarration = new NarrationBuilder();
                        //Ledger ledger = new Ledger();
                        //ledger.Load(booking.AgencyId, DateTime.Now, DateTime.Now);
                        for (int i = 0; i < itinerary.TransferDetails.Count; i++)
                        {
                            totalAmount += itinerary.TransferDetails[i].ItemPrice.GetAgentPrice();
                        }
                        //totalAmount = itinerary.Price.GetAgentPrice();

                        LedgerTransaction ledgerTxn = new LedgerTransaction();

                        //To setup narration
                        if (!string.IsNullOrEmpty(itinerary.PassengerInfo))
                        {
                            objNarration.PaxName = itinerary.PassengerInfo.Split('|')[0];
                        }
                        objNarration.Remarks = "Transfers Booking Created";
                        objNarration.DocNo = invoice.CompleteInvoiceNumber;
                        objNarration.TourConfirmationNo = itinerary.ConfirmationNo;
                        objNarration.TravelDate = itinerary.TransferDate.ToShortDateString();
                        ledgerTxn.LedgerId = booking.AgencyId;
                        //ledgerTxn.Amount = -Math.Ceiling(totalAmount);
                        ledgerTxn.Amount = -Math.Ceiling(Math.Round(totalAmount+itinerary.TransferDetails[0].ItemPrice.OutputVATAmount + itinerary.TransferDetails[0].ItemPrice.InputVATAmount, itinerary.TransferDetails[0].ItemPrice.DecimalPoint)); //modified on 01042016 by chandan| Math.Round method using for transfer
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.ReferenceId = itinerary.TransferId;
                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.TransferBooked;
                        ledgerTxn.IsLCC = true;
                        ledgerTxn.Notes = "Confirmation NO :" + itinerary.ConfirmationNo;
                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = memberId;
                        ledgerTxn.TransType = itinerary.TransactionType;
                        ledgerTxn.PaymentMode = (int)itinerary.PaymentMode;
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);
                        updateTransaction.Complete();
                    }
                }
                else if (Convert.ToBoolean(invoice.InvoiceRaised(itinerary.TransferId, ProductType.Transfers)))
                {
                    invoice.InvoiceNumber = Invoice.isInvoiceGenerated(itinerary.TransferId, ProductType.Transfers);
                }
                else
                {
                    //TODO: serious error log it and send mail to admin with detail.
                    throw new BookingEngineException("Invoice Error");
                }
            }
            return invoice.InvoiceNumber;
        }
        #endregion
        #region Fleet
        public static int RaiseInvoice(FleetItinerary itinerary, string remarks, int memberId, decimal rateOfExchange, ProductType prodType)
        {
            //Audit.Add(EventType.Book, Severity.Normal, 1, "Fleet Raise Invoice entered. FleetId : " + itinerary.FleetId, "");
            return RaiseInvoicePart2(itinerary, remarks, memberId, rateOfExchange, prodType);
        }
        public static int RaiseInvoicePart2(FleetItinerary itinerary, string remarks, int memberId, decimal rateOfExchange, ProductType prodType)
        {
            //Audit.Add(EventType.FleetBooking, Severity.Normal, 1, "Fleet Raise Invoice entered. conf no : " + itinerary.BookingRefNo + " Id : " + itinerary.FleetId, "");
            BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(itinerary.FleetId, ProductType.Car));
            Invoice invoice = new Invoice();

            if (itinerary.BookingStatus == CarBookingStatus.Confirmed)
            {
                if (invoice.InvoiceRaised(itinerary.FleetId, ProductType.Car) == 0)
                {
                    decimal totalAmount = 0;
                    invoice.PaymentMode = string.Empty;
                    invoice.Remarks = remarks;
                    invoice.AgencyId = booking.AgencyId;
                    invoice.Status = InvoiceStatus.Paid;
                    invoice.CreatedBy = memberId;
                    invoice.CreatedOn = DateTime.UtcNow;
                    invoice.TranxHeaderId = itinerary.FleetId;
                    // Generating Invoice line items.
                    invoice.LineItem = new List<InvoiceLineItem>();
                    //PriceAccounts priceInfo = new PriceAccounts();
                    InvoiceLineItem line = new InvoiceLineItem();
                    line.ItemDescription = string.Empty;
                    line.ItemReferenceNumber = itinerary.FleetId;
                    line.ItemTypeId = (int)InvoiceItemTypeId.CarBooking;
                    line.Price = itinerary.Price;
                    invoice.LineItem.Add(line);

                    // Generating Invoice line items.
                    PriceAccounts priceInfo = new PriceAccounts();
                    using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                    {
                        //Audit.Add(EventType.FleetBooking, Severity.Normal, 1, "Saving Inovoice now.", "");
                        invoice.Save(false, 0, ProductType.Car);

                        NarrationBuilder objNarration = new NarrationBuilder();

                        totalAmount = itinerary.Price.GetAgentFleetPrice();

                        LedgerTransaction ledgerTxn = new LedgerTransaction();

                        //To setup narration
                        try
                        {
                            objNarration.PaxName = string.Format("{0} {1}", itinerary.PassengerInfo[0].FirstName, itinerary.PassengerInfo[0].LastName);
                        }
                        catch { }
                        objNarration.Remarks = "Fleet Booking Created";
                        objNarration.DocNo = invoice.CompleteInvoiceNumber;
                        objNarration.TourConfirmationNo = itinerary.BookingRefNo;
                        objNarration.TravelDate = itinerary.FromDate.ToShortDateString();
                        ledgerTxn.LedgerId = booking.AgencyId;
                        ledgerTxn.Amount = -Math.Ceiling(totalAmount);
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.ReferenceId = itinerary.FleetId;
                        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CarBooked;
                        ledgerTxn.IsLCC = true;
                        ledgerTxn.Notes = "Confirmation NO :" + itinerary.BookingRefNo;
                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = memberId;
                        ledgerTxn.TransType = itinerary.TransType;
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);
                        updateTransaction.Complete();
                    }
                }
                else if (Convert.ToBoolean(invoice.InvoiceRaised(itinerary.FleetId, ProductType.Car)))
                {
                    invoice.InvoiceNumber = Invoice.isInvoiceGenerated(itinerary.FleetId, ProductType.Car);
                }
                else
                {
                    //TODO: serious error log it and send mail to admin with detail.
                    throw new BookingEngineException("Invoice Error");
                }
            }
            return invoice.InvoiceNumber;
        }
        #endregion

        //Added by Lokesh on 30-Aug-2017 for Flight Additional Services
        public static int RaiseInvoice(ItineraryAddServiceDetails detail, string remarks, int memberId, decimal rateOfExchange, ProductType prodType, int agentId)
        {
            //Audit.Add(EventType.Book, Severity.Normal, 1, "Flight Additional services Raise Invoice entered. Service Id : " + detail.Add_id, "");
            return RaiseInvoicePart2(detail, remarks, memberId, rateOfExchange, agentId);

        }

        public static int RaiseInvoicePart2(ItineraryAddServiceDetails itinerary, string remarks, int memberId, decimal rateOfExchange, int agentId)
        {

            Invoice invoice = new Invoice();

            invoice.PaymentMode = string.Empty;
            invoice.Remarks = remarks;
            invoice.AgencyId = agentId;
            invoice.Status = InvoiceStatus.Paid;
            invoice.CreatedBy = memberId;
            invoice.CreatedOn = DateTime.UtcNow;
            invoice.TranxHeaderId = itinerary.Add_id;
            invoice.TotalPrice = itinerary.Amount;

            invoice.LineItem = new List<InvoiceLineItem>();
            PriceAccounts priceInfo = new PriceAccounts();
            InvoiceLineItem line = new InvoiceLineItem();
            line.ItemDescription = string.Empty;
            line.ItemReferenceNumber = (int)itinerary.Add_id;
            line.ItemTypeId = (int)InvoiceItemTypeId.AdditionalServiceBooking;
            line.Price = new PriceAccounts();
            line.Price.PriceId = (int)itinerary.Add_id;
            invoice.LineItem.Add(line);

            using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
            {

                invoice.Save(true, 0, ProductType.ItineraryAddService);
                NarrationBuilder objNarration = new NarrationBuilder();
                LedgerTransaction ledgerTxn = new LedgerTransaction();

                //setup narration
                objNarration.PaxName = itinerary.Pax_name;
                objNarration.Remarks = "Flight Additional Service Created";
                objNarration.DocNo = invoice.CompleteInvoiceNumber;

                ledgerTxn.LedgerId = agentId;
                ledgerTxn.Amount = -Math.Ceiling(itinerary.Amount);
                ledgerTxn.Narration = objNarration;
                ledgerTxn.ReferenceId = itinerary.Add_id;
                ledgerTxn.Notes = "Confirmation NO :" + invoice.CompleteInvoiceNumber;
                ledgerTxn.Date = DateTime.UtcNow;
                ledgerTxn.CreatedBy = memberId;
                ledgerTxn.TransType = "B2B";
                ledgerTxn.ReferenceType = ReferenceType.AdditionalService;
                ledgerTxn.Save();
                LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);
                updateTransaction.Complete();
            }
            return invoice.InvoiceNumber;
        }



        #endregion
    }
}
