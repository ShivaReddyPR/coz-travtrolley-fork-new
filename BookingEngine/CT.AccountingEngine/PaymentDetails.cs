using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using CT.BookingEngine;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;

namespace CT.AccountingEngine
{
    /// <summary>
    /// Payment Details
    /// </summary>
    public class PaymentDetails
    {
        #region Private Fields
        /// <summary>
        /// UniqueIdField paymentDetailid
        /// </summary>
        private int paymentDetailId;

        /// <summary>
        /// agencyId Field(payment given by this agency)
        /// </summary>
        private int agencyId;

        /// <summary>
        /// Amount of payment
        /// </summary>
        private decimal amount;

        /// <summary>
        /// Remaining Amount of any payment
        /// </summary>
        private decimal remainingAmount;

        /// <summary>
        /// Payment date field
        /// </summary>
        private DateTime paymentDate;

        /// <summary>
        /// Mode of Payment whethert its cheque,draft or cash
        /// </summary>
        private PaymentMode modeOfPayment;

        /// <summary>
        /// Reference No field(Cheque or draft no)
        /// </summary>
        private int referenceNo;

        /// <summary>
        /// Bank name field
        /// </summary>
        private string bankName;

        /// <summary>
        /// Branch of the bank
        /// </summary>
        private string branch;

        /// <summary>
        /// Status whethee it is assigned,rejected and so on
        /// </summary>
        private PaymentStatus status;

        /// <summary>
        /// Notes against a payment
        /// </summary>
        private string notes;

        /// <summary>
        /// Remarks against a payment
        /// </summary>
        private string remarks;

        /// <summary>
        /// Collection of invoiceCollection struct
        /// </summary>
        private List<InvoiceCollection> invoices;

        /// <summary>
        /// Date when the record was created
        /// </summary>
        private DateTime createdOn;
        
        /// <summary>
        /// Unique ID of the member who created this record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Unique ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;
        /// <summary>
        /// Date when the record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// Unique ID of the bank Where Amount is being paid 
        /// </summary>
        private int ourBankDetailId;
        /// <summary>
        /// Field tells whether this payment is for LCCs or not
        /// </summary>
        private bool isLCC;
        /// <summary>
        /// Field stores the Receipt Numbers for the payments accepted by admin
        /// </summary>
        private string receiptNumber;

        /// <summary>
        /// To store collection of invoices settle by this payment as XML.
        /// </summary>
        private StringBuilder invoiceCollectionXML = new StringBuilder("<RECORD>");

        /// <summary>
        /// Field stores Mode of Payment(auto/manual)
        /// </summary>
        private int autoPayment;


        #endregion

        #region Public Properties

        /// <summary>
        /// UniqueId property paymentDetailId
        /// </summary>
        public int PaymentDetailId
        {
            get
            {
                return this.paymentDetailId;
            }
            set
            {
                this.paymentDetailId = value;
            }
        }

        /// <summary>
        /// Get or sets the agencyId property
        /// </summary>
        public int AgencyId
        {
            get
            {
                return this.agencyId;
            }
            set
            {
                this.agencyId = value;
            }
        }

        /// <summary>
        /// Gets or sets the Amount property
        /// </summary>
        public decimal Amount
        {
            get
            {
                return this.amount;
            }
            set
            {
                this.amount = value;
            }
        }

        /// <summary>
        /// Gets or sets the Remaining Amount property
        /// </summary>
        public decimal RemainingAmount
        {
            get
            {
                return this.remainingAmount;
            }
            set
            {
                this.remainingAmount = value;
            }
        }

        /// <summary>
        /// payment date property
        /// </summary>
        public DateTime PaymentDate
        {
            get
            {
                return this.paymentDate;
            }
            set
            {
                this.paymentDate = value;
            }
        }

        /// <summary>
        /// Payment Mode property
        /// </summary>
        public PaymentMode ModeOfPayment
        {
            get
            {
                return this.modeOfPayment;
            }
            set
            {
                this.modeOfPayment = value;
            }
        }

        /// <summary>
        /// ReferenceNo(Cheque or draft no when payment mode is cheque or draft)
        /// </summary>
        public int ReferenceNo
        {
            get
            {
                return this.referenceNo;
            }
            set
            {
                this.referenceNo = value;
            }
        }

        /// <summary>
        /// Bank name property
        /// </summary>
        public string BankName
        {
            get
            {
                return this.bankName;
            }
            set
            {
                this.bankName = value;
            }
        }

        /// <summary>
        /// Branch property 
        /// </summary>
        public string Branch
        {
            get
            {
                return this.branch;
            }
            set
            {
                this.branch = value;
            }
        }

        /// <summary>
        /// Payment Status
        /// </summary>
        public PaymentStatus Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.status = value;
            }
        }

        /// <summary>
        /// Notes
        /// </summary>
        public string Notes
        {
            get
            {
                return this.notes;
            }
            set
            {
                this.notes = value;
            }
        }

        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks
        {
            get
            {
                return this.remarks;
            }
            set
            {
                this.remarks = value;
            }
        }

        /// <summary>
        /// List of Invoices
        /// </summary>
        public List<InvoiceCollection> Invoices
        {
            get
            {
                return this.invoices;
            }
            set
            {
                this.invoices = value;
            }
        }
        /// <summary>
        /// Created On - Date
        /// </summary>
        public DateTime CreatedOn
        {
            get 
            {
                return this.createdOn;
            }
            set
            {
                this.createdOn = value;
            }
        }
        /// <summary>
        /// Gets the ID of member who created this record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets the datetime when the record was last modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
        }

        /// <summary>
        /// Gets the ID of member who modified this record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets and set the ID of Bank
        /// </summary>
        public int OurBankDetailId
        {
            get
            {
                return ourBankDetailId;
            }
            set
            {
                ourBankDetailId = value;
            }
        }
        /// <summary>
        /// Gets or sets isLCC flag
        /// </summary>
        public bool IsLCC
        {
            get
            {
                return this.isLCC;
            }
            set
            {
                this.isLCC = value;
            }
        }
        /// <summary>
        /// Gets or sets the receipt number 
        /// </summary>
        public string ReceiptNumber 
        {
            get 
            {
                return receiptNumber;
            }
            set 
            {
                receiptNumber = value;
            }
        }

        /// <summary>
        /// auto payment status
        /// </summary>
        public int AutoPayment
        {
            get
            {
                return autoPayment;
            }
            set
            {
                autoPayment = value;
            }
        }

        #endregion

        #region Class Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public PaymentDetails()
        {

        }
        #endregion

        #region Class Methods

        /// <summary>
        /// Load method loads the paymentDetail of a given id
        /// </summary>
        /// <param name="paymentDetailId"></param>
        public void Load(int paymentDetailId)
        { 
             
            //Trace.TraceInformation("PaymentDetail.Load entered : paymentDetailId =" + paymentDetailId);
            if (paymentDetailId <= 0)
            {
                throw new ArgumentException("Payment Detail Id should be positive integer", "PaymentDetailId");
            }
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paymentDetailId", paymentDetailId);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetPaymentDetail, paramList, connection);
            if (data != null && data.Read())
            {
                this.paymentDetailId = paymentDetailId;
                this.agencyId = Convert.ToInt32(data["agencyId"]);
                this.amount = Convert.ToDecimal(data["amount"]);
                this.remainingAmount = Convert.ToDecimal(data["remainingAmount"]);
                if (data["bankName"] != DBNull.Value)
                {
                    this.bankName = data["bankName"].ToString();
                }
                else
                {
                    this.bankName = string.Empty;
                }
                if (data["branch"] != DBNull.Value)
                {
                    this.branch = data["branch"].ToString();
                }
                else
                {
                    this.branch = string.Empty;
                }
                
                this.modeOfPayment = (PaymentMode)(Enum.Parse(typeof(PaymentMode), data["modeId"].ToString()));
                if (data["notes"] != DBNull.Value)
                {
                    this.notes = data["notes"].ToString();
                }
                else
                {
                    this.notes = string.Empty;
                }
                this.createdOn = Convert.ToDateTime(data["createdOn"]);
                this.paymentDate = Convert.ToDateTime(data["paymentDate"]);
                this.referenceNo = Convert.ToInt32(data["refNo"]);
                this.remarks = data["remarks"].ToString();
                this.status = (PaymentStatus)(Enum.Parse(typeof(PaymentStatus), data["paymentStatusId"].ToString()));
                if (data["createdBy"] != DBNull.Value)
                {
                    this.createdBy = Convert.ToInt32(data["createdBy"]);
                }
                if (data["lastModifiedOn"] != DBNull.Value)
                {
                    this.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                }
                if (data["lastModifiedBy"] != DBNull.Value)
                {
                    this.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                }
                if (data["receiptNumber"] != DBNull.Value)
                {
                    this.receiptNumber = Convert.ToString(data["receiptNumber"]);
                }
                this.ourBankDetailId = Convert.ToInt32(data["ourBankDetailId"]);
                this.isLCC = Convert.ToBoolean(data["isLCC"]);
                this.autoPayment = Convert.ToInt32(data["autoPayment"]);
                data.Close();
                connection.Close();

                //Loads the collection of invoices which checked off against payment
                LoadPaymentInvoice(this.paymentDetailId);

                //Trace.TraceInformation("PaymentDetail.Save exiting : paymentDetailId " + paymentDetailId);
            }
            else
            {
                data.Close();
                connection.Close();
                //Trace.TraceInformation("PaymentDetail.Save exiting : ");
                throw new ArgumentException("PaymentDetail id does not exist in database");
            }
        }
        /// <summary>
        /// Saves the detail of payment
        /// </summary>
        public void Save()
        {
            //Trace.TraceInformation("PaymentDetail.Save entered : ");

            SqlParameter[] paramList = new SqlParameter[15];
            paramList[0] = new SqlParameter("@paymentDetailId", paymentDetailId);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@amount", amount);
            paramList[3] = new SqlParameter("@remainingAmount", remainingAmount);
            paramList[4] = new SqlParameter("@paymentDate", paymentDate);
            paramList[5] = new SqlParameter("@modeId", (int)modeOfPayment);
            paramList[6] = new SqlParameter("@refNo", referenceNo);
            paramList[7] = new SqlParameter("@bankName", bankName);
            paramList[8] = new SqlParameter("@branch", branch);
            paramList[9] = new SqlParameter("@ourBankDetailId", ourBankDetailId);
            paramList[10] = new SqlParameter("@paymentStatusId", (int)status);
            paramList[11] = new SqlParameter("@notes", notes);
            paramList[12] = new SqlParameter("@remarks", remarks);            
            paramList[13] = new SqlParameter("@isLCC", isLCC); 
            paramList[14] = new SqlParameter("@createdBy", createdBy);

            
            int rowsAffected;
            if (paymentDetailId > 0)
            {
                paramList[14] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdatePaymentDetail, paramList);
                UpdateEntryInPaymentInvoice();
            }
            else
            {
                paramList[0].Direction = ParameterDirection.Output;
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddPaymentDetail, paramList);
                paymentDetailId = Convert.ToInt32(paramList[0].Value);
                AddEntryInPaymentInvoice();
            }
            //Trace.TraceInformation("PaymentDetail.Save exiting : rowsAffected = " + rowsAffected);
        }
        public void PaymentAccept(string narration,int ledgerId)
        {
            //Trace.TraceInformation("PaymentDetail.PaymentAccept entered : ");

            invoiceCollectionXML.Append("</RECORD>");
            //CT.Core.Audit.Add(CT.Core.EventType.Account, CT.Core.Severity.Low, lastModifiedBy, "PaymentDetail.PaymentAccept entered with XML: " + invoiceCollectionXML, "0");
            int rowsAffected=0;

            try
            {
                if (paymentDetailId > 0)
                {
                    SqlParameter[] paramList = new SqlParameter[18];
                    paramList[0] = new SqlParameter("@paymentDetailId", paymentDetailId);
                    paramList[1] = new SqlParameter("@agencyId", agencyId);
                    paramList[2] = new SqlParameter("@amount", amount);
                    paramList[3] = new SqlParameter("@remainingAmount", remainingAmount);
                    paramList[4] = new SqlParameter("@paymentDate", paymentDate);
                    paramList[5] = new SqlParameter("@modeId", (int)modeOfPayment);
                    paramList[6] = new SqlParameter("@refNo", referenceNo);
                    paramList[7] = new SqlParameter("@bankName", bankName);
                    paramList[8] = new SqlParameter("@branch", branch);
                    paramList[9] = new SqlParameter("@ourBankDetailId", ourBankDetailId);
                    paramList[10] = new SqlParameter("@paymentStatusId", (int)status);
                    paramList[11] = new SqlParameter("@paymentInvoiceXML", invoiceCollectionXML.ToString());
                    paramList[12] = new SqlParameter("@notes", notes);
                    paramList[13] = new SqlParameter("@remarks", remarks);
                    paramList[14] = new SqlParameter("@isLCC", isLCC);
                    paramList[15] = new SqlParameter("@ledgerId", ledgerId);
                    paramList[16] = new SqlParameter("@narration", narration);
                    paramList[17] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                    rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.PaymentAccept, paramList);
                }
            }
            catch (SqlException ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Account, CT.Core.Severity.High, lastModifiedBy, "PaymentDetail.PaymentAccept error occured: " + ex.Message, "0");
            }
            //Trace.TraceInformation("PaymentDetail.PaymentAccept exiting : rowsAffected = " + rowsAffected);
        }
        public void SavePaymentProcessingForQueue(CT.Core.Queue queue)
        {
            //Trace.TraceInformation("PaymentDetail.SavePaymentProcessingForQueue entered : ");

            invoiceCollectionXML.Append("</RECORD>");
            //CT.Core.Audit.Add(CT.Core.EventType.Account, CT.Core.Severity.Low, lastModifiedBy, "PaymentDetail.SavePaymentProcessingForQueue entered with XML: " + invoiceCollectionXML, "0");
            int rowsAffected = 0;
            try
            {
                SqlParameter[] paramList = new SqlParameter[19];                
                if (paymentDetailId == 0)
                {
                    paramList[0] = new SqlParameter("@paymentDetailId", paymentDetailId);
                    paramList[1] = new SqlParameter("@agencyId", agencyId);
                    paramList[2] = new SqlParameter("@amount", amount);
                    paramList[3] = new SqlParameter("@remainingAmount", remainingAmount);
                    paramList[4] = new SqlParameter("@paymentDate", paymentDate);
                    paramList[5] = new SqlParameter("@modeId", (int)modeOfPayment);
                    paramList[6] = new SqlParameter("@refNo", referenceNo);
                    paramList[7] = new SqlParameter("@bankName", bankName);
                    paramList[8] = new SqlParameter("@branch", branch);
                    paramList[9] = new SqlParameter("@ourBankDetailId", ourBankDetailId);
                    paramList[10] = new SqlParameter("@paymentStatusId", (int)status);
                    paramList[11] = new SqlParameter("@paymentInvoiceXML", invoiceCollectionXML.ToString());
                    paramList[12] = new SqlParameter("@notes", notes);
                    paramList[13] = new SqlParameter("@remarks", remarks);
                    paramList[14] = new SqlParameter("@isLCC", isLCC);
                    paramList[15] = new SqlParameter("@createdBy", createdBy);
                    paramList[16] = new SqlParameter("@statusId", queue.StatusId);
                    paramList[17] = new SqlParameter("@completionDate", queue.CompletionDate);
                    paramList[18] = new SqlParameter("@queueTypeId", queue.QueueTypeId);
                    paramList[0].Direction = ParameterDirection.Output;
                    rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddPaymentDetailPaymentProcessing, paramList);
                    paymentDetailId = Convert.ToInt32(paramList[0].Value);
                    //AddEntryInPaymentInvoice();
                }
            }
            catch (SqlException ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Account, CT.Core.Severity.High, lastModifiedBy, "PaymentDetail.SavePaymentProcessingForQueue error occured: " + ex.Message, "0");
            }
            //Trace.TraceInformation("PaymentDetail.SavePaymentProcessingForQueue exiting : rowsAffected = " + rowsAffected);
        }
        
        /// <summary>
        /// Method to update payment against invoice
        /// </summary>
        /// <param name="paymentDetailId"></param>
        /// <param name="invoiceNumber"></param>
        /// <param name="amount"></param>
        /// <param name="ispartial"></param>
        
        public static void UpdatePaymentAgainstInvoice(int paymentDetailId, int invoiceNumber, decimal amount, bool ispartial)
        {
            ////Trace.TraceInformation("PaymentDetail.UpdatePaymentAgainstInvoice entered:");
            SqlParameter[] paramlist = new SqlParameter[4];
            paramlist[0] = new SqlParameter("@paymentDetailId", paymentDetailId);
            paramlist[1] = new SqlParameter("@invoiceNumber",invoiceNumber);
            paramlist[2] = new SqlParameter("@amount",amount);
            paramlist[3] = new SqlParameter("@isPartial",ispartial);
            int rowAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdatePaymentInvoice, paramlist);
            //Trace.TraceInformation("PaymentDetail.UpdatePaymentInvoice exiting");
        }

        /// <summary>
        /// Adds entry in payment Invoice
        /// </summary>
        private void AddEntryInPaymentInvoice()
        {
            //Trace.TraceInformation("PaymentDetail.AddEntryInPaymentInvoice entered : ");
            foreach (InvoiceCollection invoice in invoices)
            {
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@paymentDetailId", paymentDetailId);
                paramList[1] = new SqlParameter("@invoiceNumber", invoice.InvoiceNumber);
                paramList[2] = new SqlParameter("@amount", invoice.Amount);
                paramList[3] = new SqlParameter("@isPartial", invoice.IsPartial);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddPaymentInvoice, paramList);
            }
            //Trace.TraceInformation("PaymentDetail.AddEntryInPaymentInvoice exiting ");
        }
        /// <summary>
        /// Updates entry in payment Invoice
        /// </summary>
        private void UpdateEntryInPaymentInvoice()
        {
            //Trace.TraceInformation("PaymentDetail.UpdateEntryInPaymentInvoice entered : ");
            DeleteInvoiceAgainstPayment();
            AddEntryInPaymentInvoice();
            //Trace.TraceInformation("PaymentDetail.UpdateEntryInPaymentInvoice exiting  ");
        }
        public void DeleteInvoiceAgainstPayment()
        {
            //Trace.TraceInformation("PaymentDetail.DeleteInvoiceAgainstPayment entered : ");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paymentDetailId", paymentDetailId);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.DeleteInvoiceAgainstPayment, paramList);
            //Trace.TraceInformation("PaymentDetail.DeleteInvoiceAgainstPayment exited : ");
        }
        /// <summary>
        /// Load method for payment Invoice
        /// </summary>
        /// <param name="paymentDetailId">paymentDetailId</param>
        private void LoadPaymentInvoice(int paymentDetailId)
        {
            //Trace.TraceInformation("PaymentDetail.LoadPaymentInvoice entered : ");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paymentDetailId", paymentDetailId);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetPaymentInvoice, paramList, connection);
            this.invoices = new List<InvoiceCollection>();
            while (data != null && data.Read())
            {
                InvoiceCollection tempIc = new InvoiceCollection();
                tempIc.PaymentDetailId = paymentDetailId;
                tempIc.InvoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
                tempIc.Amount = Convert.ToDecimal(data["amount"]);
                tempIc.IsPartial = Convert.ToBoolean(data["isPartial"]);
                this.invoices.Add(tempIc);
            }

            data.Close();
            connection.Close();
            //Trace.TraceInformation("PaymentDetail.LoadPaymentInvoice exiting : rowsAffected = ");
        }
        /// <summary>
        /// Function gets all the payments without loading its invoices because it is not 
        /// required at this moment
        /// </summary>
        /// <param name="agencyId">agencyId</param>
        /// <param name="startingDate">Starting Date</param>
        /// <param name="endingDate">Ending Date</param>
        /// <returns>List of payments dont having its invoices </returns>
        public static List<PaymentDetails> GetPayments(int agencyId, DateTime startingDate, DateTime endingDate)
        {
            //Trace.TraceInformation("PaymentDetail.GetPayments entered : ");
            List<PaymentDetails> tempList = new List<PaymentDetails>();
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
            startingDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
            endingDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
            startingDate = startingDate.ToUniversalTime();
            endingDate = endingDate.ToUniversalTime();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@startingDate", startingDate);
            paramList[2] = new SqlParameter("@endingDate", endingDate);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetPayments, paramList, connection);
            if (data.HasRows)
            {
                tempList = GetPaymentList(data);
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("PaymentDetail.GetPayments exiting : rowsAffected = ");
            return tempList;
        }
        /// <summary>
        /// Function gets all the payments without loading its invoices because it is not  
        /// required at this moment of certain status
        /// </summary>
        /// <param name="agencyId">agencyId</param>
        /// <param name="status">payment Status</param>
        /// <returns>List of payments dont having its invoices </returns>
        public static List<PaymentDetails> GetPayments(int agencyId,PaymentStatus status)
        {
            //Trace.TraceInformation("PaymentDetail.GetPayments entered : ");
            List<PaymentDetails> tempList = new List<PaymentDetails>();
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@paymentStatusId", (int)status);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetPaymentsOfStatus, paramList, connection);
            if (data.HasRows)
            {
                tempList = GetPaymentList(data);
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("PaymentDetail.GetPayments exiting : rowsAffected = ");
            return tempList;
        }

        /// <summary>
        /// Gets the detail of payment queue for making csv
        /// </summary>        
        /// <returns>Datatable contains all the column required for csv</returns>
        public static DataTable GetPaymentQueueForCSV()
        {
            DataTable paymentQueueData = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[0];
                SqlConnection sqlConn = DBGateway.GetConnection();
                paymentQueueData.Columns.Add("Agency Name");
                paymentQueueData.Columns.Add("Payment Date");
                paymentQueueData.Columns.Add("Amount");
                paymentQueueData.Columns.Add("Payment Mode");
                paymentQueueData.Columns.Add("Our Bank Account Detail");
                paymentQueueData.Columns.Add("Agency Type");


                SqlDataReader reader = DBGateway.ExecuteReaderSP(SPNames.GetPaymentQueueForCSV, paramList, sqlConn);
                while (reader.Read())
                {
                    string paymentMode = string.Empty;
                    string agencyType = string.Empty;
                    int modeId = Convert.ToInt32(reader["modeId"]);
                    paymentMode = ((PaymentMode)modeId).ToString();
                    int agencyTypeId = Convert.ToInt32(reader["agencyTypeId"]);
                    agencyType = ((Agencytype)agencyTypeId).ToString();

                    paymentQueueData.Rows.Add(new object[] { reader["agencyName"], reader["paymentDate"], reader["amount"], paymentMode, reader["ourBankDetail"],agencyType });
                }
                reader.Close();
                sqlConn.Close();
            }
            catch (SqlException ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Account, CT.Core.Severity.High, 0, "An error occured during extracting data from database for csv of payment queue error: " + ex.Message, "");
            }
            //Trace.TraceInformation("Agency.GetAgencyDetailByName exited");
            return paymentQueueData;
        }

        /// <summary>
        /// Sets the status of the payment
        /// </summary>
        /// <param name="paymentDetailId">paymentDetailId</param>
        /// <param name="status">status</param>
        /// <param name="remarks">remarks</param>
        /// <param name="lastModifiedBy">lastModifiedBy</param>
        public static void SetStatus(int paymentDetailId, PaymentStatus status, string remarks, int lastModifiedBy)
        {
            //Trace.TraceInformation("PaymentDetails.SetStatus entered");
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@paymentDetailId", paymentDetailId);
            paramList[1] = new SqlParameter("@statusId", (int)status);
            paramList[2] = new SqlParameter("@remarks", remarks);
            paramList[3] = new SqlParameter("@lastModifiedBy",lastModifiedBy);
            int rowsEffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdatePaymentStatusId, paramList);
            //Trace.TraceInformation("PaymentDetails.SetStatus exited");
        }

        /// <summary>
        /// Method Gets the amount of payment done in this fortnight
        /// </summary>
        /// <param name="agencyId">agencyId</param> 
        /// <returns></returns>
        public static decimal GetPaymentsThisFortnight(int agencyId)
        {
            //Trace.TraceInformation("PaymentDetail.GetPaymentsThisFortnight entered : agencyId = "+agencyId);
            decimal paymentThisFortnight = 0;
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlDataReader reader = DBGateway.ExecuteReaderSP(SPNames.GetPaymentsThisFortnight, paramList, connection);
            if (reader != null && reader.Read())
            {
                if (reader["amount"] != DBNull.Value)
                {
                    paymentThisFortnight = Convert.ToDecimal(reader["amount"]);
                }
            }
            reader.Close();
            connection.Close();
            //Trace.TraceInformation("Ledger.GetPaymentsThisFortnight exited :paymentThisFortnight = "+paymentThisFortnight);
            return (paymentThisFortnight);
        }

        /// <summary>
        /// To Retrieve Top 5 recent Payment
        /// </summary>
        /// <param name="agencyId">Unique Id of Agent </param>
        /// <returns>Retrun List of Top 5 Payments</returns>
        public static List<PaymentDetails> GetRecentPayments(int agencyId)
        {
            //Trace.TraceInformation("PaymentDetail.GetPayments entered : ");
            List<PaymentDetails> tempList = new List<PaymentDetails>();
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetRecentPayments, paramList, connection);            
            if (data.HasRows)
            {
                tempList = GetPaymentList(data);
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("PaymentDetail.GetRecentPayments exiting : retrieve Record = " + tempList.Count);
            return tempList;
        }
        /// <summary>
        /// Return List Of Payment
        /// </summary>
        /// <param name="data">DataReader that contains rows retrun by stored procedure</param>
        /// <returns>List of Payment Class</returns>
        private static List<PaymentDetails> GetPaymentList(SqlDataReader data)
        {
            List<PaymentDetails> tempList = new List<PaymentDetails>();
            if (data.HasRows)
            {
                while (data.Read())
                {
                    PaymentDetails pd = new PaymentDetails();
                    pd.paymentDetailId = Convert.ToInt32(data["paymentDetailId"]);
                    pd.agencyId = Convert.ToInt32(data["agencyId"]);
                    pd.amount = Convert.ToDecimal(data["amount"]);
                    pd.remainingAmount = Convert.ToDecimal(data["remainingAmount"]);
                    if (data["bankName"] != DBNull.Value)
                    {
                        pd.bankName = data["bankName"].ToString();
                    }
                    else
                    {
                        pd.bankName = string.Empty;
                    }
                    if (data["branch"] != DBNull.Value)
                    {
                        pd.branch = data["branch"].ToString();
                    }
                    else
                    {
                        pd.branch = string.Empty;
                    }
                    pd.modeOfPayment = (PaymentMode)(Enum.Parse(typeof(PaymentMode), data["modeId"].ToString()));
                    if (data["notes"] != DBNull.Value)
                    {
                        pd.notes = data["notes"].ToString();
                    }
                    else
                    {
                        pd.notes = string.Empty;
                    }
                    pd.createdOn = Convert.ToDateTime(data["createdOn"]);
                    pd.paymentDate = Convert.ToDateTime(data["paymentDate"]);
                    pd.referenceNo = Convert.ToInt32(data["refNo"]);
                    pd.receiptNumber = Convert.ToString(data["receiptNumber"]);
                    pd.remarks = data["remarks"].ToString();
                    pd.isLCC = Convert.ToBoolean(data["isLCC"]);
                    pd.status = (PaymentStatus)(Enum.Parse(typeof(PaymentStatus), data["paymentStatusId"].ToString()));
                    pd.ourBankDetailId = Convert.ToInt32(data["ourBankDetailId"]);
                    if (data["createdBy"] != DBNull.Value)
                    {
                        pd.createdBy = Convert.ToInt32(data["createdBy"]);
                    }
                    if (data["lastModifiedOn"] != DBNull.Value)
                    {
                        pd.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                    }
                    if (data["lastModifiedBy"] != DBNull.Value)
                    {
                        pd.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    }
                    tempList.Add(pd);
                }
            }
            data.Close();            
            return (tempList);
            
        }
        /// <summary>
        /// Gets the list of lcc Bank Account  or non LCC bank account
        /// </summary>        
        /// <returns>Sorted list of all Bank Account. bankDetailId as key and bank name+ Account Number as value</returns>
        public static SortedList GetBankDetail(AccountType accountType, int ourBankDetailId)
        {
            //Trace.TraceInformation("PaymentDetail.GetBankDetail entered");
            SortedList bankList = new SortedList();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@accountType", accountType);
            paramList[1] = new SqlParameter("@ourBankDetailId", ourBankDetailId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetBankDetail, paramList, connection);
            while (data.Read())
            {
                bankList.Add(data["bankDetail"].ToString(), Convert.ToInt32(data["ourBankDetailId"]));
            }
            data.Close();
            connection.Close();
            bankList.TrimToSize();
            //Trace.TraceInformation("PaymentDetail.GetBankDetail exited retrieve Record = " + bankList.Count);
            return bankList;
        }
        /// <summary>
        /// Gets the list of lcc Bank Account  or non LCC bank account.This is an overload method taking 3 arguments taking an extra parameter agencyid  
        /// </summary>        
        /// <returns>Sorted list of all Bank Account. bankDetailId as key and bank name+ Account Number as value</returns>
        public static SortedList GetBankDetail(AccountType accountType, int ourBankDetailId,int agencyId)
        {
            //Trace.TraceInformation("PaymentDetail.GetBankDetail entered");
            SortedList bankList = new SortedList();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@accountType", accountType);
            paramList[1] = new SqlParameter("@ourBankDetailId", ourBankDetailId);
            paramList[2] = new SqlParameter("@agencyId", agencyId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetBankDetail, paramList, connection);
            while (data.Read())
            {
                bankList.Add(data["bankDetail"].ToString(), Convert.ToInt32(data["ourBankDetailId"]));
            }
            data.Close();
            connection.Close();
            bankList.TrimToSize();
            //Trace.TraceInformation("PaymentDetail.GetBankDetail exited retrieve Record = " + bankList.Count);
            return bankList;
        }

        /// <summary>
        /// Gets the list of all Bank Account
        /// </summary>        
        /// <returns>Sorted list of all Bank Account. bankDetailId as key and bank name+ Account Number as value</returns>
        public static SortedList GetAllBankDetail()
        {
            //Trace.TraceInformation("PaymentDetail.GetAllBankDetail entered");
            SortedList bankList = new SortedList();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAllBankDetail, paramList, connection);
            while (data.Read())
            {
                bankList.Add(data["bankDetail"].ToString(), Convert.ToInt32(data["ourBankDetailId"]));
            }
            data.Close();
            connection.Close();
            bankList.TrimToSize();
            //Trace.TraceInformation("PaymentDetail.GetAllBankDetail exited retrieve Record = " + bankList.Count);
            return bankList;
        }
        /// <summary>
        /// Get the receipt number against paymentId
        /// </summary>
        /// <returns></returns>
        public static string GetReceiptNumberAgainstPaymentDetailId(int paymentId)
        {
            //Trace.TraceInformation("PaymentDetail.GetReceiptNumberAgainstPaymentDetailId entered");
            string receiptNumber = string.Empty;
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paymentDetailId",paymentId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetReceiptNumberAgainstPaymentDetailId, paramList, connection);
            while (data.Read())
            {
                if (data["receiptnumber"] != DBNull.Value)
                {
                    receiptNumber =Convert.ToString(data["receiptnumber"]);
                }
            }
           
            data.Close();
            connection.Close();
            //Trace.TraceInformation("PaymentDetail.GetReceiptNumberAgainstPaymentDetailId exited retrieve receiptNumber = " +receiptNumber.ToString());
            return receiptNumber;
        }


        /// <summary>
        /// Get the List Of Available Balance against Advance Payment of a agent
        /// </summary>
        /// <param name="agencyId">Unique Id of Agent</param>
        /// <returns>List Of Available Balance against Advance Payment and coreesponding PaymentDetailId</returns>
        public static List<KeyValuePair<int, decimal>> GetBalanceLeftofAdvancePayments(int agencyId)
        {
            //Trace.TraceInformation("PaymentDetail.GetBalanceLeft entered");
            List<KeyValuePair<int, decimal>> balanceLeftList = new List<KeyValuePair<int, decimal>>();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetBalanceLeftofAdvancePayments, paramList, connection);
            while (dataReader.Read())
            {
                balanceLeftList.Add(new KeyValuePair<int, decimal>(Convert.ToInt32(dataReader["paymentDetailId"]),Convert.ToDecimal(dataReader["remainingAmount"])));
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("PaymentDetail.GetBalanceLeftofAdvancePayments exiting : balanceLeftList.Count = " + balanceLeftList.Count);
            return balanceLeftList;
        }

        /// <summary>
        /// Get the List Of Available Balance against Advance Payment along with paymentmode of a agent
        /// </summary>
        /// <param name="agencyId">Unique Id of Agent</param>
        /// <returns>Table Of Available Balance against Advance Payment and paymentDetail,paymentMode and referenceNumber</returns>
        public static DataTable GetBalanceLeftAndPaymentModeofAdvancePayments(int agencyId)
        {
            //Trace.TraceInformation("PaymentDetail.GetBalanceLeftAndPaymentModeofAdvancePayments entered");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            DataTable table = DBGateway.FillDataTableSP(SPNames.GetBalanceLeftAndPaymentModeofAdvancePayments, paramList);
            //Trace.TraceInformation("PaymentDetail.GetBalanceLeftAndPaymentModeofAdvancePayments exiting : balanceLeftList.Count = " + table.Rows.Count);
            return table;
        }

        /// <summary>
        /// Get the List Of Total Paid amount against Advance Payment of a agent
        /// </summary>
        /// <param name="agencyId">Unique Id of Agent</param>
        /// <returns>List Of Total Paid amount against Advance Payment and coreesponding PaymentDetailId</returns>
        public static List<KeyValuePair<int, decimal>> GetTotalAdvancePayments(int agencyId)
        {
            //Trace.TraceInformation("PaymentDetail.GetTotalAdvancePayments entered: agencyId" + agencyId);
            List<KeyValuePair<int, decimal>> totalAmountList = new List<KeyValuePair<int, decimal>>();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetTotalAdvancePayments, paramList, connection);
            while (dataReader.Read())
            {
                totalAmountList.Add(new KeyValuePair<int, decimal>(Convert.ToInt32(dataReader["paymentDetailId"]), Convert.ToDecimal(dataReader["amount"])));
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("PaymentDetail.GetTotalAdvancePayments exiting : totalAmountList.Count = " + totalAmountList.Count);
            return totalAmountList;
        }
        /// <summary>
        /// Add entry in Payment Invoice table
        /// </summary>
        /// <param name="paymentDetailId">Id of Payment</param>
        /// <param name="invoiceNumber">Invoice Number</param>
        /// <param name="amount">Amount given in a payment</param>
        /// <param name="isPartial">boolean value for partial payments</param>
        public static void AddPaymentAgainstInvoice(int paymentDetailId, int invoiceNumber, decimal amount, bool isPartial)
        {
            //Trace.TraceInformation("PaymentDetail.AddPaymentAgainstInvoice entered : ");
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@paymentDetailId", paymentDetailId);
            paramList[1] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[2] = new SqlParameter("@amount", amount);
            paramList[3] = new SqlParameter("@isPartial", isPartial);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddPaymentInvoice, paramList);
            //Trace.TraceInformation("PaymentDetail.AddPaymentAgainstInvoice exiting ");
        }
        /// <summary>
        /// To Update Remaining Balance
        /// </summary>
        /// <param name="paymentDetailId"> unique Id of Payments</param>
        /// <param name="amount">amount of payment</param>
        public static void UpdateRemainingBalance(int paymentDetailId, decimal amount)
        {
            //Trace.TraceInformation("PaymentDetail.UpdateRemainingBalance entered : paymentDetailId"+paymentDetailId);
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@paymentDetailId", paymentDetailId);            
            paramList[1] = new SqlParameter("@amount", amount);            
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateRemainingBalance, paramList);
            //Trace.TraceInformation("PaymentDetail.UpdateRemainingBalance exiting  rowsAffected = " + rowsAffected);
        }

        /// <summary>
        /// Function to update invoice payment status
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <param name="isPartial"></param>
        public static void UpdateInvoicePaymentStatus(int invoiceNumber, bool isPartial)
        {
            ////Trace.TraceInformation("PaymentDetailoId.UpdatePaymentInvoiceStatus entered: InvoiceNumber =" + invoiceNumber);
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@invoiceNumber",invoiceNumber);
            paramList[1] = new SqlParameter("@isPartial",isPartial);
            int rowsaffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateInvoicePaymentStatus, paramList);
            //Trace.TraceInformation("PaymentDetail.UpdateInvoicePaymentStatus exiting rowsAffected = "+ rowsaffected);
        }
        /// <summary>
        /// To Get Received Amount of All selected agent
        /// </summary>
        /// <param name="ourBankDetailId">Unique Id of a bank</param>
        /// <param name="startingDate">From Date</param>
        /// <param name="endingDate">To Date</param>
        /// <returns></returns>
        public static DataTable GetReceivedAmountOnBank(int ourBankDetailId, DateTime startingDate, DateTime endingDate, PaymentMode paymentMode)
        {
            //Trace.TraceInformation("Payment.GetReceivedAmountOnBank entered ourBankDetailId =" + ourBankDetailId);
            DataTable totalSubmittedAmount = new DataTable();
            SqlParameter[] paramList = new SqlParameter[4];
            startingDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
            endingDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
            startingDate = startingDate.ToUniversalTime();
            endingDate = endingDate.ToUniversalTime();            
            paramList[0] = new SqlParameter("@ourBankDetailId", ourBankDetailId);
            paramList[1] = new SqlParameter("@startingDate", startingDate);
            paramList[2] = new SqlParameter("@endingDate", endingDate);
            paramList[3] = new SqlParameter("@paymentMode", (int)paymentMode);
            totalSubmittedAmount = DBGateway.FillDataTableSP(SPNames.GetReceivedAmountOnBank, paramList);
            //Trace.TraceInformation("Payment.GetReceivedAmountOnBank exited count = " + totalSubmittedAmount.Rows.Count);
            return totalSubmittedAmount;
        }

        /// <summary>
        /// To Get Received Amount of All selected agent from report server
        /// </summary>
        /// <param name="ourBankDetailId">Unique Id of a bank</param>
        /// <param name="startingDate">From Date</param>
        /// <param name="endingDate">To Date</param>
        /// <returns></returns>
        public static DataTable GetReceivedAmountOnBankFromReportServer(int ourBankDetailId, DateTime startingDate, DateTime endingDate, PaymentMode paymentMode)
        {
            ///Trace.TraceInformation("Payment.GetReceivedAmountOnBankFromReportServer entered ourBankDetailId =" + ourBankDetailId);
            DataTable totalSubmittedAmount = new DataTable();
            SqlParameter[] paramList = new SqlParameter[4];
            startingDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
            endingDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
            startingDate = startingDate.ToUniversalTime();
            endingDate = endingDate.ToUniversalTime();
            paramList[0] = new SqlParameter("@ourBankDetailId", ourBankDetailId);
            paramList[1] = new SqlParameter("@startingDate", startingDate);
            paramList[2] = new SqlParameter("@endingDate", endingDate);
            paramList[3] = new SqlParameter("@paymentMode", (int)paymentMode);
            //totalSubmittedAmount = DBGateway.FillDataTableSP(SPNames.GetReceivedAmountOnBank, paramList,ConncetionStringUsingModule.ReportingServer);
            totalSubmittedAmount = DBGateway.FillDataTableSP(SPNames.GetReceivedAmountOnBank, paramList);
            ////Trace.TraceInformation("Payment.GetReceivedAmountOnBankFromReportServer exited count = " + totalSubmittedAmount.Rows.Count);
            return totalSubmittedAmount;
        }

        public void AddInvoice(InvoiceCollection invoice)
        {
            invoiceCollectionXML.Append("<PI>");
            invoiceCollectionXML.Append("<INVNO>" + invoice.InvoiceNumber.ToString() + "</INVNO>");
            invoiceCollectionXML.Append("<PDID>" + invoice.PaymentDetailId.ToString() + "</PDID>");
            invoiceCollectionXML.Append("<AMT>" + invoice.Amount.ToString() + "</AMT>");
            if (invoice.IsPartial)
            {
                invoiceCollectionXML.Append("<ISP>1</ISP>");
            }
            else
            {
                invoiceCollectionXML.Append("<ISP>0</ISP>");
            }
            invoiceCollectionXML.Append("</PI>");                     
        }
        /// <summary>
        /// To Get Received Amount of any selected agent
        /// </summary>
        /// <param name="csvAgencyId" >Selected Agent Id</param>
        /// <param name="ourBankDetailId">Unique Id of a bank</param>
        /// <param name="startingDate">From Date</param>
        /// <param name="endingDate">To Date</param>
        /// <returns></returns>
        public static DataTable GetReceivedAmountOnBank(string csvAgencyId, int ourBankDetailId, DateTime startingDate, DateTime endingDate, PaymentMode paymentMode)
        {
            //Trace.TraceInformation("Payment.GetReceivedAmountOnBank entered ourBankDetailId =" + ourBankDetailId);
            DataTable totalSubmittedAmount = new DataTable();
            SqlParameter[] paramList = new SqlParameter[5];
            startingDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
            endingDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
            startingDate = startingDate.ToUniversalTime();
            endingDate = endingDate.ToUniversalTime();
            paramList[0] = new SqlParameter("@csvAgencyId", csvAgencyId);
            paramList[1] = new SqlParameter("@ourBankDetailId", ourBankDetailId);
            paramList[2] = new SqlParameter("@startingDate", startingDate);
            paramList[3] = new SqlParameter("@endingDate", endingDate);
            paramList[4] = new SqlParameter("@paymentMode", (int)paymentMode);
            totalSubmittedAmount = DBGateway.FillDataTableSP(SPNames.GetReceivedAmountOnBankForAgent, paramList);
            //Trace.TraceInformation("Payment.GetReceivedAmountOnBank exited count = " + totalSubmittedAmount.Rows.Count);
            return totalSubmittedAmount;
        }

        /// <summary>
        /// To Get Received Amount of any selected agent from report server
        /// </summary>
        /// <param name="csvAgencyId" >Selected Agent Id</param>
        /// <param name="ourBankDetailId">Unique Id of a bank</param>
        /// <param name="startingDate">From Date</param>
        /// <param name="endingDate">To Date</param>
        /// <returns></returns>
        public static DataTable GetReceivedAmountOnBankFromReportServer(string csvAgencyId, int ourBankDetailId, DateTime startingDate, DateTime endingDate, PaymentMode paymentMode)
        {
            //Trace.TraceInformation("Payment.GetReceivedAmountOnBankFromReportServer entered ourBankDetailId =" + ourBankDetailId);
            DataTable totalSubmittedAmount = new DataTable();
            SqlParameter[] paramList = new SqlParameter[5]; 
            startingDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
            endingDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
            startingDate = startingDate.ToUniversalTime();
            endingDate = endingDate.ToUniversalTime();
            paramList[0] = new SqlParameter("@csvAgencyId", csvAgencyId);
            paramList[1] = new SqlParameter("@ourBankDetailId", ourBankDetailId);
            paramList[2] = new SqlParameter("@startingDate", startingDate);
            paramList[3] = new SqlParameter("@endingDate", endingDate);
            paramList[4] = new SqlParameter("@paymentMode", (int)paymentMode);
            //totalSubmittedAmount = DBGateway.FillDataTableSP(SPNames.GetReceivedAmountOnBankForAgent, paramList,ConncetionStringUsingModule.ReportingServer); commented by ziyad
            totalSubmittedAmount = DBGateway.FillDataTableSP(SPNames.GetReceivedAmountOnBankForAgent, paramList);
            //Trace.TraceInformation("Payment.GetReceivedAmountOnBankFromReportServer exited count = " + totalSubmittedAmount.Rows.Count);
            return totalSubmittedAmount;
        }
        /// <summary>
        /// To get payment receipt report 
        /// </summary>
        /// <param name="startDate" >startDate</param>
        /// <param name="endDate">endDate</param>
        /// <returns></returns>
        public static DataTable GetAcceptedPaymentsReport(DateTime startDate,DateTime endDate) 
        {
            //Trace.TraceInformation("Payment.GetAcceptedPaymentsReport entered");
            DataTable getReceiptReport = new DataTable();
            SqlParameter[] paramList=new SqlParameter[2];
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
            startDate = Util.ISTToUTC(startDate);
            endDate = Util.ISTToUTC(endDate);
            paramList[0] = new SqlParameter("@startDate", startDate);
            paramList[1] = new SqlParameter("@endDate", endDate);
            getReceiptReport = DBGateway.FillDataTableSP(SPNames.GetReceiptReport, paramList);
            //Trace.TraceInformation("Payment.GetAcceptedPaymentsReport exited");
            return getReceiptReport;
        }

        /// <summary>
        /// To get payment receipt report from report server
        /// </summary>
        /// <param name="startDate" >startDate</param>
        /// <param name="endDate">endDate</param>
        /// <returns></returns>
        public static DataTable GetAcceptedPaymentsReportFromReportServer(DateTime startDate, DateTime endDate)
        {
            //Trace.TraceInformation("Payment.GetAcceptedPaymentsReportFromReportServer entered");
            DataTable getReceiptReport = new DataTable();
            SqlParameter[] paramList = new SqlParameter[2];
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
            startDate = Util.ISTToUTC(startDate);
            endDate = Util.ISTToUTC(endDate);
            paramList[0] = new SqlParameter("@startDate", startDate);
            paramList[1] = new SqlParameter("@endDate", endDate);
            //getReceiptReport = DBGateway.FillDataTableSP(SPNames.GetReceiptReport, paramList,ConncetionStringUsingModule.ReportingServer);
            getReceiptReport = DBGateway.FillDataTableSP(SPNames.GetReceiptReport, paramList);
            //Trace.TraceInformation("Payment.GetAcceptedPaymentsReportFromReportServer exited");
            return getReceiptReport;
        }
        // Commentd By ziyad
        
        //public void AddAcceptedPayment(int loggedMemberId)
        //{
        //    status = PaymentStatus.Accepted;
        //    Save();
        //    #region Save Ledger
        //    LedgerTransaction ledgerTxn = new LedgerTransaction();
        //    Ledger ledger = new Ledger();
        //    ledger.Load(agencyId);
        //    ledgerTxn.LedgerId = ledger.LedgerId;
        //    ledgerTxn.ReferenceId = paymentDetailId;
        //    ledgerTxn.ReferenceType = ReferenceType.PaymentRecieved;
        //    NarrationBuilder narrationBuilder = new NarrationBuilder();
        //    ledgerTxn.Amount = amount;
        //    ledgerTxn.IsLCC = isLCC;
        //    if (remarks.Trim().Length > 0)
        //    {
        //        narrationBuilder.Remarks = remarks;
        //    }
        //    else
        //    {
        //        narrationBuilder.Remarks = "Payment Done";
        //    }
        //    narrationBuilder.ChequeNo = referenceNo.ToString(); ;
        //    narrationBuilder.PaymentMode = modeOfPayment.ToString();
        //    ledgerTxn.Narration = narrationBuilder;
        //    ledgerTxn.Notes = "Payment Done";
        //    ledgerTxn.Date = DateTime.Now;
        //    ledgerTxn.CreatedBy = loggedMemberId;
        //    ledgerTxn.Save();
        //    #endregion
        //    Agency agency = new Agency(agencyId);
        //    if (!isLCC && agency.AgencyTypeId != 1 && agency.AgencyTypeId != (int)Agencytype.Service )
        //    {
        //        Ledger.UpdateCurrentBalance(agencyId, amount, loggedMemberId);
        //    }
        //    else
        //    {
        //        Agency.UpdateLCCBalance(agencyId, amount, loggedMemberId);
        //    }
        //    CT.Core.Queue.SetStatus(QueueType.Payment, paymentDetailId, QueueStatus.Completed, loggedMemberId);
                    
        //}


        public static List<int> CheckPaymentEntry(DateTime date, int agencyId, decimal amount,int paymentMode)
        {
           // CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 1, "AccountingEngine.PaymentDetail.checkpayment entry started...", "0");
            SqlConnection sqlConnection=DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@depositAmount", amount);
            paramList[2] = new SqlParameter("@depositDate", date);
            paramList[3] = new SqlParameter("@paymentMode", paymentMode);
            SqlDataReader sqlDataReader=DBGateway.ExecuteReaderSP(SPNames.GetPaymentEntry, paramList, sqlConnection);
            List<int> pdList = new List<int>();
            while (sqlDataReader.Read())
            {
                pdList.Add(Convert.ToInt32(sqlDataReader["paymentDetailId"]));
            }
            sqlDataReader.Close();
            sqlConnection.Close();
            //Trace.TraceInformation("Payment.checkPaymentEntry exited");
            //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 1, "AccountingEngine.PaymentDetail.checkpayment entry exit...", "0");
            return pdList;
        }

        public static List<int> CheckPreviusPaymentEntry(DateTime date, int agencyId, decimal amount, int paymentMode)
        {
            //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 1, "AccountingEngine.PaymentDetail.CheckPreviusPaymentEntry entry started...", "0");
            SqlConnection sqlConnection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@depositAmount", amount);
            paramList[2] = new SqlParameter("@depositDate", date);
            paramList[3] = new SqlParameter("@paymentMode", paymentMode);
            SqlDataReader sqlDataReader = DBGateway.ExecuteReaderSP(SPNames.GetPreviousPaymentEntry, paramList, sqlConnection);
            List<int> pdList = new List<int>();
            while (sqlDataReader.Read())
            {
                pdList.Add(Convert.ToInt32(sqlDataReader["paymentDetailId"]));
            }
            //Trace.TraceInformation("Payment.checkPaymentEntry exited");
            //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 1, "AccountingEngine.PaymentDetail.CheckPreviusPaymentEntry entry exit...", "0");
            return pdList;
        }


        public static void updateAutoPaymentStatus(int paymentDetailId,int lastModifiedBy)
        {
           // CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 1, "AccountingEngine.PaymentDetail.updateautoPayment entry started...", "0");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@paymentDetailId", paymentDetailId);
            paramList[1] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            try
            {
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateAutoPaymentStatus, paramList);
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 1, "Exception occurred PaymentDetailId("+paymentDetailId+") status is not updated.Now exit.."+ex.Message, "0");
                return;
            }
            
            //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 1, "AccountingEngine.PaymentDetail.updateautopayment exit...", "0");
        }


        #endregion



        
    }

    /// <summary>
    /// Mode of Payment
    /// </summary>
    public enum PaymentMode
    {
        /// <summary>
        /// Payment is done through cheque
        /// </summary>
        Cheque = 1,
        /// <summary>
        /// Payment is done through Draft
        /// </summary>
        Draft = 2,
        /// <summary>
        /// Payment is done through Cash
        /// </summary>
        Cash = 3,
        /// <summary>
        /// Payment is done through none of the above mode(like credit card)
        /// </summary>
        Other = 4,
        /// <summary>
        /// Payment Mode added to his account for ticket refund
        /// </summary>
        Credit = 5,
        /// <summary>
        /// When Payment done by credit cards
        /// </summary>
        CreditCard =6,
        /// <summary>
        /// Payment Mode When we do Invoice Edition
        /// </summary>
        ManualAdjustment=7,
                /// Payment made by credit card through GDS
        /// </summary>
        CreditCardGDS = 8,
        /// <summary>
        /// Payment Settle against Credit Note
        /// </summary>
        CreditNote=10,
        /// <summary>
        /// Payment made  through CCAvenue
        /// </summary>        
        CCAvenue = 9,
        /// <summary>
        /// Refund for train
        /// </summary>
        TrainRefund = 11,
        ///<summary>
        /// Payment made if invoice amount decrease in case of edit invoice
        ///</summary>
        EditInvoice = 12,
        ///<summary>
        /// Payment made if credit note amount increase in case of edit invoice
        ///</summary>
        EditCreditNote = 13,

        ///<summary>
        /// Payment made by Manual Credit entry.
        ///</summary>
        ManualCredit=14,

        ///<summary>
        /// Payment made by RTGS.
        ///</summary>
        RTGS = 15

    }

    /// <summary>
    /// Payment Status
    /// </summary>
    public enum PaymentStatus
    {
        /// <summary>
        /// Payment is in Submitted State(not processed by admin yet)
        /// </summary>
        Submitted = 1,
        /// <summary>
        /// Payment has been accepted
        /// </summary>
        Accepted = 2,
        /// <summary>
        /// Payment has been rejected
        /// </summary>
        Rejected = 3,
        /// <summary>
        /// Payment has been reversed
        /// </summary>
        PaymentReversed=4
    }

    /// <summary>
    /// Invoice Collection
    /// </summary>
    public struct InvoiceCollection
    {
        /// <summary>
        /// Unique Id of Payments
        /// </summary>
        public int PaymentDetailId;
        /// <summary>
        /// Uinique of a invoice
        /// </summary>
        public int InvoiceNumber;
        /// <summary>
        /// Is Invoice is Fully paid or partially paid
        /// </summary>
        public bool IsPartial;
        /// <summary>
        /// Amount paid to that Invoice
        /// </summary>
        public decimal Amount;
    }
}
