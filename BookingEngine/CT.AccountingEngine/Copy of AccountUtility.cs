using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using CT.BookingEngine;
using System.Transactions;
using CT.Core;
using CT.Configuration;
using System.Globalization;
using System.Diagnostics;
using System.Data;
//using InsuranceBookingEngine;

namespace CT.AccountingEngine
{
    /// <summary>
    /// Summary description for Util
    /// </summary>
    public class AccountUtility
    {
        /// <summary>
        /// Will raise invoice for the itinerary corresponding to flightId.
        /// </summary>
        /// <param name="flightId">flightId of the itinerary</param>
        /// <param name="remarks">remarks if any</param>
        /// <param name="memberId">memberId of the member who is currently logged in.</param>
        /// <returns>Invoice number generated. If invoice for itinerary is already generated then it will give the existing invoice number.</returns>
        public static int RaiseInvoice(int flightId, string remarks, int memberId)
        {
            FlightItinerary itinerary = new FlightItinerary(flightId);
            return RaiseInvoice(itinerary, remarks, memberId);
        }

        /// <summary>
        /// Raises invoice for the given itinerary.
        /// </summary>
        /// <param name="itinerary">itinerary for which invoice is to be raised.</param>
        /// <param name="remarks">remarks if any.</param>
        /// <param name="memberId">memberId of the member who is currently logged in.</param>
        /// <returns>Invoice number generated. If invoice for itinerary is already generated then it will give the existing invoice number.</returns>
        public static int RaiseInvoice(FlightItinerary itinerary, string remarks, int memberId)
        {
            List<Ticket> ticket = Ticket.GetTicketList(itinerary.FlightId);
            return RaiseInvoice(itinerary, ticket, remarks, memberId);
        }

        /// <summary>
        /// Raises invoice for the given itinerary for Manual Invoice section.
        /// </summary>
        /// <param name="itinerary">itinerary for which invoice is to be raised.</param>
        /// <param name="tickets">ticket list for the itinerary</param>
        /// <param name="table">table containing hash values</param>
        /// <param name="memberId">memberId of the member who is currently logged in.</param>
        /// <param name="booking"> booking object is having booking details.</param>
        /// <returns>Invoice number generated. If invoice for itinerary is already generated then it will give the existing invoice number.</returns>

        public static int RaiseInvoiceForManualBooking(FlightItinerary itinerary, Ticket[] tickets, Hashtable table, int memberId, BookingDetail booking)
        {
            List<Ticket> ticket = new List<Ticket>();
            ticket.AddRange(tickets);
            Invoice invoice = new Invoice();
            bool isLCC = false;

            for (int counter = 0; counter < itinerary.Segments.Length; counter++)
            {
                Airline airline = new Airline();
                airline.Load(itinerary.Segments[counter].Airline);
                if (airline.IsLCC)
                {
                    isLCC = true;
                    break;
                }
            }

            if (invoice.InvoiceRaised(itinerary.FlightId) == 0)
            {
                Agency agency = new Agency(booking.AgencyId);
                bool isServiceAgency = agency.AgencyTypeId == Convert.ToInt32(ConfigurationSystem.Core["ServiceAgencyTypeId"]);
                decimal totalAmount = 0;
                invoice.PaymentMode = string.Empty;
                invoice.Remarks = table["AgentRemarks"].ToString();
                invoice.AgencyId = booking.AgencyId;
                decimal netAmount = Convert.ToDecimal(0.00);

                for (int i = 0; i < ticket.Count; i++)
                {
                    netAmount += isServiceAgency ? ticket[i].Price.GetServiceAgentPrice() : ticket[i].Price.GetAgentPrice();
                }

                invoice.CreatedBy = memberId;
                decimal currentBalance = Convert.ToDecimal(0.00);

                invoice.CreatedOn = Convert.ToDateTime(table["InvoiceDate"]);
                invoice.StaffRemarks = table["StaffRemarks"].ToString();
                invoice.InvoiceDueDate = Convert.ToDateTime(table["InvoiceDueDate"]);

                invoice.XONumber = table["XoNumber"].ToString();
                invoice.DocTypeCode = "HD";

                // Generating Invoice line items.
                invoice.LineItem = new List<InvoiceLineItem>();
                for (int i = 0; i < ticket.Count; i++)
                {
                    InvoiceLineItem line = new InvoiceLineItem();
                    line.ItemDescription = string.Empty;
                    line.ItemReferenceNumber = ticket[i].TicketId;
                    line.ItemTypeId = 1;
                    line.Price = ticket[i].Price;
                    invoice.LineItem.Add(line);
                }
                BookingHistory bhInvoice = new BookingHistory();
                bhInvoice.BookingId = booking.BookingId;
                bhInvoice.EventCategory = EventCategory.Payment;
                if (isLCC)
                {
                    bhInvoice.Remarks = "Invoice generated and settled";
                }
                else
                {
                    bhInvoice.Remarks = "Invoice generated";
                }
                bhInvoice.CreatedBy = memberId;
                Ledger ledger = new Ledger();
                ledger.Load(booking.AgencyId, DateTime.Now, DateTime.Now);
                List<LedgerTransaction> ledgerTransactionList = new List<LedgerTransaction>();
                for (int i = 0; i < ticket.Count; i++)
                {
                    LedgerTransaction ledgerTxn = new LedgerTransaction();
                    ledgerTxn.LedgerId = ledger.LedgerId;
                    decimal ledgerAmount = isServiceAgency ? ticket[i].Price.GetServiceAgentPrice() : ticket[i].Price.GetAgentPrice();
                    ledgerTxn.Amount = -ledgerAmount;
                    totalAmount += ledgerAmount;
                    NarrationBuilder objNarration = new NarrationBuilder();
                    objNarration.Remarks = "Manual Ticket Created";
                    objNarration.PaxName = string.Format("{0} {1}", ticket[i].PaxFirstName, ticket[i].PaxLastName);
                    objNarration.Remarks = "Ticket Created";
                    objNarration.TicketNo = ticket[i].TicketNumber;
                    objNarration.TravelDate = itinerary.TravelDate.ToShortDateString();
                    objNarration.FlightNo = itinerary.ValidatingAirlineCode + itinerary.Segments[0].FlightNumber;
                    objNarration.Sector = Ticket.GetItineraryString(itinerary);
                    ledgerTxn.Narration = objNarration;
                    ledgerTxn.ReferenceId = ticket[i].TicketId;
                    ledgerTxn.ReferenceType = ReferenceType.TicketCreated;
                    if (agency.AgencyTypeId == (int)Agencytype.Cash || agency.AgencyTypeId == (int)Agencytype.Service)
                    {
                        ledgerTxn.IsLCC = true;
                    }
                    else
                    {
                        ledgerTxn.IsLCC = isLCC;
                    }
                    ledgerTxn.Notes = "";
                    ledgerTxn.Date = DateTime.UtcNow;
                    ledgerTxn.CreatedBy = memberId;
                    ledgerTxn.CreatedOn = Convert.ToDateTime(table["InvoiceDate"]);
                    ledgerTransactionList.Add(ledgerTxn);
                }
                InvoiceStatus invoiceStatus = InvoiceStatus.Raised;

                if (agency.AgencyTypeId != (int)Agencytype.Cash && agency.AgencyTypeId != (int)Agencytype.Service)
                {
                    if (isLCC)
                    {
                        invoiceStatus = InvoiceStatus.Paid;
                    }
                    else
                    {
                        invoiceStatus = InvoiceStatus.Raised;
                    }
                }
                else
                {
                    invoiceStatus = InvoiceStatus.Paid;
                }

                List<PaymentsAgainstInvoice> listOfPaymentsAgainstInvoice = new List<PaymentsAgainstInvoice>();
                //The below code changes the status of the invoice to paid or partially for non lcc booking of credit agent, if agent has advance payments.
                if (!isLCC && agency.AgencyTypeId == (int)Agencytype.Credit && itinerary.PaymentMode != ModeOfPayment.CreditCard)
                {
                    decimal amountLeft = totalAmount;
                    //table of advance payment, remaining balance, paymentmode and reference number.

                    DataTable balanceLeftInfoTable = PaymentDetails.GetBalanceLeftAndPaymentModeofAdvancePayments(agency.AgencyId);

                    string Remarks = string.Empty;
                    string paymentType = string.Empty;
                    foreach (DataRow dr in balanceLeftInfoTable.Rows)
                    {
                        PaymentsAgainstInvoice paymentsAgainstInvoice = new PaymentsAgainstInvoice();
                        decimal balanceLeft = Convert.ToDecimal(dr["remainingAmount"]);

                        PaymentMode mode = (PaymentMode)(Enum.Parse(typeof(PaymentMode), dr["PaymentMode"].ToString()));

                        if (balanceLeft >= amountLeft)
                        {
                            invoiceStatus = InvoiceStatus.Paid;
                            paymentsAgainstInvoice.PaymentDetailId = Convert.ToInt32(dr["paymentDetailId"]);
                            paymentsAgainstInvoice.Amount = amountLeft;
                            paymentsAgainstInvoice.IsPartial = false;
                            listOfPaymentsAgainstInvoice.Add(paymentsAgainstInvoice);
                            if (mode == PaymentMode.Cash)
                            {
                                paymentType = mode.ToString() + " Amount( " + Math.Round(amountLeft,Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )) + " )";
                            }
                            else
                            {
                                paymentType = mode.ToString() + "-" + dr["Referencenumber"] + " Amount( " + Math.Round(amountLeft,Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )) + " )";
                            }

                            if (Remarks == string.Empty)
                            {
                                Remarks = "Invoice is fully settled against " + paymentType;
                            }
                            else
                            {
                                Remarks = Remarks + " and is fully settled against " + paymentType;
                            }
                            break;
                        }
                        else
                        {
                            invoiceStatus = InvoiceStatus.PartiallyPaid;
                            paymentsAgainstInvoice.PaymentDetailId = Convert.ToInt32(dr["paymentDetailId"]);
                            paymentsAgainstInvoice.Amount = balanceLeft;
                            paymentsAgainstInvoice.IsPartial = true;
                            listOfPaymentsAgainstInvoice.Add(paymentsAgainstInvoice);
                            amountLeft = amountLeft - balanceLeft;
                            if (mode == PaymentMode.Cash)
                            {
                                paymentType = mode.ToString() + " Amount(" + Math.Round(balanceLeft,Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )) + ")";
                            }
                            else
                            {
                                paymentType = mode.ToString() + "-" + dr["Referencenumber"] + " Amount( " +Math.Round(balanceLeft,Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )) + " )";
                            }

                            if (Remarks == string.Empty)
                            {

                                Remarks = "Invoice is partially settled against " + paymentType;
                            }
                            else
                            {
                                Remarks = Remarks + " , " + paymentType;
                            }
                        }


                    }
                    if (Remarks != string.Empty)
                    {
                        bhInvoice.Remarks = Remarks;
                        invoice.Remarks = invoice.Remarks + " " + Remarks;
                    }
                }

                invoice.Status = invoiceStatus;
                using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                {
                    //Save Invoice
                invoice.Save(itinerary.CheckDomestic("" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + ""));
                    foreach (PaymentsAgainstInvoice paymentAgainstInvoice in listOfPaymentsAgainstInvoice)
                    {
                        PaymentDetails.UpdateRemainingBalance(paymentAgainstInvoice.PaymentDetailId, paymentAgainstInvoice.Amount);
                        PaymentDetails.AddPaymentAgainstInvoice(paymentAgainstInvoice.PaymentDetailId, invoice.InvoiceNumber, paymentAgainstInvoice.Amount, paymentAgainstInvoice.IsPartial);
                    }
                    //Add entry in booking history
                    bhInvoice.Save();
                    //Add entries in ledger transaction
                    foreach (LedgerTransaction ledgerTransaction in ledgerTransactionList)
                    {
                        ledgerTransaction.Save();
                        LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTransaction.TxnId);
                    }
                    //Update balance of the agent
                    if (!isLCC)
                    {
                        if (agency.AgencyTypeId != 1 && agency.AgencyTypeId != (int)Agencytype.Service)
                        {
                            Ledger.UpdateCurrentBalance(invoice.AgencyId, -(Math.Round(totalAmount)), memberId);
                        }
                        else
                        {
                            Agency.UpdateLCCBalance(invoice.AgencyId, -(Math.Round(totalAmount)), memberId);
                        }
                    }
                    else
                    {
                        Agency.UpdateLCCBalance(invoice.AgencyId, -(Math.Round(totalAmount)), memberId);
                    }


                    updateTransaction.Complete();

                }
            }
            else if (invoice.InvoiceRaised(itinerary.FlightId) == ticket.Count)
            {
                invoice.InvoiceNumber = Invoice.isInvoiceGenerated(ticket[0].TicketId);
            }
            else
            {
                //TODO: serious error log it and send mail to admin with detail.
                throw new BookingEngineException("Invoice Error");
            }

            return invoice.InvoiceNumber;
        }
        /*public static int RaiseInvoiceMisc(MiscBooking item, PriceAccounts price, DateTime invoiceCreatedOn, DateTime invoiceDueDate, bool isDomestic)
        {
            Trace.TraceInformation("Entering Invoice.RaiseInvoice");
            int invoiceNumber = 0;
            bool isServiceAgency = false;
            if (Agency.GetAgencyTypeId(item.AgencyId) == Convert.ToInt32(ConfigurationSystem.Core["ServiceAgencyTypeId"]))
            {
                isServiceAgency = true;
            }
            Invoice invoice = new Invoice();
            invoice.AgencyId = item.AgencyId;
            //invoice.TotalPrice = isServiceAgency ? price.GetServiceAgentPrice() : price.GetAgentPrice();
            invoice.InvoiceDueDate = invoiceDueDate;
            invoice.Status = InvoiceStatus.Paid;
            invoice.CreatedBy = item.CreatedBy;
            invoice.CreatedOn = invoiceCreatedOn;
            invoice.PaymentMode = String.Empty;
            invoice.XONumber = "";
            invoice.ManuallyUpdated = false;
            invoice.Remarks = "Misc invoice generated";
            invoice.LineItem = new List<InvoiceLineItem>();

            InvoiceLineItem line = new InvoiceLineItem();
            line.ItemTypeId = (int)InvoiceItemTypeId.Misc;

            Ledger ledger = new Ledger();
            ledger.Load(item.AgencyId, DateTime.Now, DateTime.Now);
            NarrationBuilder objNarration = null;
            objNarration = new NarrationBuilder();
            LedgerTransaction ledgerTxn = new LedgerTransaction();
            ledgerTxn.LedgerId = ledger.LedgerId;
            decimal ledgerAmount = isServiceAgency ? price.GetServiceAgentPrice() : price.GetAgentPrice();
            ledgerTxn.Amount = -ledgerAmount;
            //To setup narration
            objNarration.PaxName = item.PaxDetail;

            ProductType prodType = new ProductType();
            if (item.ProductTypeId == (int)ProductType.Train)
            {
                objNarration.Remarks = "Train Ticket Created";
                prodType = ProductType.Train;
            }
            else if (item.ProductTypeId == (int)ProductType.Hotel)
            {
                objNarration.Remarks = "Hotel Booked";
                prodType = ProductType.Hotel;
            }
            else if (item.ProductTypeId == (int)ProductType.Insurance)
            {
                objNarration.Remarks = "Insurance Policy Created";
                prodType = ProductType.Insurance;
            }
            else if (item.ProductTypeId == (int)ProductType.SMSPack)
            {
                objNarration.Remarks = "SMS Pack Activated.";
                prodType = ProductType.SMSPack;
            }
            line.ItemDescription = objNarration.Remarks;
            line.CreatedBy = item.CreatedBy;
            line.CreatedOn = invoiceCreatedOn;

            ledgerTxn.Narration = objNarration;
            ledgerTxn.ReferenceType = ReferenceType.MiscItem;
            ledgerTxn.IsLCC = true;
            ledgerTxn.Notes = "";
            ledgerTxn.Date = invoiceCreatedOn;
            ledgerTxn.CreatedBy = item.CreatedBy;

            CoreLogic.Queue queue = new CoreLogic.Queue();
            queue.QueueTypeId = (int)QueueType.MiscInvoice;
            queue.StatusId = (int)QueueStatus.Completed;
            queue.AssignedTo = item.CreatedBy;
            queue.AssignedBy = item.CreatedBy;
            queue.AssignedDate = invoiceCreatedOn;
            queue.CompletionDate = invoiceCreatedOn;
            queue.CreatedBy = item.CreatedBy;

            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    price.Save();
                    item.PriceId = price.PriceId;
                    item.Save();
                    line.Price = price;
                    line.ItemReferenceNumber = item.MiscBookingId;
                    invoice.LineItem.Add(line);
                    queue.Save();

                    invoice.Save(prodType, isDomestic);
                    ledgerTxn.ReferenceId = item.MiscBookingId;
                    ledgerTxn.CreatedOn = invoice.CreatedOn;
                    ledgerTxn.Save();

                    LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);
                    if (isDomestic)
                    {
                        Agency.UpdateLCCBalance(invoice.AgencyId, -(ledgerAmount), item.CreatedBy);
                    }
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Account, Severity.High, 0, ex.Message, "");
                throw ex;
            }
            Trace.TraceInformation("Exiting Invoice.RaiseInvoice");
            return invoiceNumber;
        }
        */
        /// <summary>
        /// Raises invoice for the given itinerary.
        /// </summary>
        /// <param name="itinerary">itinerary for which invoice is to be raised.</param>
        /// <param name="ticket">ticket list for the itinerary</param>
        /// <param name="remarks">remarks if any.</param>
        /// <param name="memberId">memberId of the member who is currently logged in.</param>
        /// <returns>Invoice number generated. If invoice for itinerary is already generated then it will give the existing invoice number.</returns>
        public static int RaiseInvoice(FlightItinerary itinerary, List<Ticket> ticket, string remarks, int memberId)
        {
            BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(itinerary.FlightId));

            Invoice invoice = new Invoice();
            bool isLCC = false;
            for (int counter = 0; counter < itinerary.Segments.Length; counter++)
            {
                Airline airline = new Airline();
                airline.Load(itinerary.Segments[counter].Airline);
                if (airline.IsLCC)
                {
                    isLCC = true;
                    break;
                }
            }
            if (FlightItinerary.IsTicketed(itinerary))
            {
                int invoiceRaisedResult = invoice.InvoiceRaised(itinerary.FlightId);
                if (invoiceRaisedResult == 0)
                {
                    Agency agency = new Agency(booking.AgencyId);
                    bool isServiceAgency = agency.AgencyTypeId == Convert.ToInt32(ConfigurationSystem.Core["ServiceAgencyTypeId"]);
                    decimal totalAmount = 0;
                    invoice.PaymentMode = string.Empty;
                    invoice.Remarks = remarks;
                    invoice.AgencyId = booking.AgencyId;
                    invoice.CreatedBy = memberId;

                    invoice.CreatedOn = ticket[0].IssueDate;
                    // Generating Invoice line items.
                    invoice.LineItem = new List<InvoiceLineItem>();
                    for (int i = 0; i < ticket.Count; i++)
                    {
                        InvoiceLineItem line = new InvoiceLineItem();
                        line.ItemDescription = string.Empty;
                        line.ItemReferenceNumber = ticket[i].TicketId;
                        //TODO: line item type Id. should not be hardcoded 1.
                        line.ItemTypeId = 1;
                        line.Price = ticket[i].Price;
                        invoice.LineItem.Add(line);
                    }
                    BookingHistory bhInvoice = new BookingHistory();
                    bhInvoice.BookingId = booking.BookingId;
                    bhInvoice.EventCategory = EventCategory.Payment;
                    if (isLCC)
                    {
                        bhInvoice.Remarks = "Invoice generated and settled";
                    }
                    else
                    {
                        bhInvoice.Remarks = "Invoice generated";
                    }
                    bhInvoice.CreatedBy = memberId;
                    Ledger ledger = new Ledger();
                    ledger.Load(booking.AgencyId, DateTime.Now, DateTime.Now);
                    NarrationBuilder objNarration = null;
                    List<LedgerTransaction> ledgerTransactionList = new List<LedgerTransaction>();
                    for (int i = 0; i < ticket.Count; i++)
                    {
                        objNarration = new NarrationBuilder();
                        LedgerTransaction ledgerTxn = new LedgerTransaction();
                        ledgerTxn.LedgerId = ledger.LedgerId;
                        decimal ledgerAmount = isServiceAgency ? ticket[i].Price.GetServiceAgentPrice() : ticket[i].Price.GetAgentPrice();
                        ledgerTxn.Amount = -ledgerAmount;
                        totalAmount += ledgerAmount;
                        //To setup narration
                        objNarration.PaxName = string.Format("{0} {1}", ticket[i].PaxFirstName, ticket[i].PaxLastName);
                        objNarration.Remarks = "Ticket Created";
                        objNarration.TicketNo = ticket[i].TicketNumber;
                        objNarration.TravelDate = itinerary.TravelDate.ToShortDateString();
                        objNarration.FlightNo = itinerary.ValidatingAirlineCode + itinerary.Segments[0].FlightNumber;
                        objNarration.Sector = Ticket.GetItineraryString(itinerary);
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.ReferenceId = ticket[i].TicketId;
                        ledgerTxn.ReferenceType = ReferenceType.TicketCreated;
                        if (agency.AgencyTypeId == (int)Agencytype.Cash || agency.AgencyTypeId == (int)Agencytype.Service)
                        {
                            ledgerTxn.IsLCC = true;
                        }
                        else
                        {
                            ledgerTxn.IsLCC = isLCC;
                        }
                        ledgerTxn.Notes = "";
                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = memberId;
                        ledgerTransactionList.Add(ledgerTxn);
                        if (ticket[i].Price.Discount > 0)
                        {
                            objNarration = new NarrationBuilder();
                            ledgerTxn = new LedgerTransaction();
                            ledgerTxn.LedgerId = ledger.LedgerId;
                            ledgerAmount = ticket[i].Price.Discount;
                            ledgerTxn.Amount = ledgerAmount;
                            totalAmount -= ledgerAmount;
                            //To setup narration
                            objNarration.PaxName = string.Format("{0} {1}", ticket[i].PaxFirstName, ticket[i].PaxLastName);
                            objNarration.Remarks = "Cash Discount";
                            objNarration.TicketNo = ticket[i].TicketNumber;
                            objNarration.TravelDate = itinerary.TravelDate.ToShortDateString();
                            objNarration.FlightNo = itinerary.AirlineCode + itinerary.Segments[0].FlightNumber;
                            objNarration.Sector = Ticket.GetItineraryString(itinerary);
                            ledgerTxn.Narration = objNarration;
                            ledgerTxn.ReferenceId = ticket[i].TicketId;
                            ledgerTxn.ReferenceType = ReferenceType.CashDiscount;
                            if (agency.AgencyTypeId == (int)Agencytype.Cash || agency.AgencyTypeId == (int)Agencytype.Service)
                            {
                                ledgerTxn.IsLCC = true;
                            }
                            else
                            {
                                ledgerTxn.IsLCC = isLCC;
                            }
                            ledgerTxn.Notes = "";
                            ledgerTxn.Date = DateTime.UtcNow;
                            ledgerTxn.CreatedBy = memberId;
                            ledgerTransactionList.Add(ledgerTxn);
                        }
                    }
                    InvoiceStatus invoiceStatus = InvoiceStatus.Raised;
                    List<PaymentsAgainstInvoice> listOfPaymentsAgainstInvoice = new List<PaymentsAgainstInvoice>();
                    //The below code changes the status of the invoice to paid or partially for non lcc booking of credit agent, if agent has advance payments.
                    if (!isLCC && agency.AgencyTypeId == (int)Agencytype.Credit && itinerary.PaymentMode != ModeOfPayment.CreditCard)
                    {
                        decimal amountLeft = totalAmount;
                        //table of advance payment, remaining balance, paymentmode and reference number.
                        DataTable balanceLeftInfoTable = PaymentDetails.GetBalanceLeftAndPaymentModeofAdvancePayments(agency.AgencyId);
                        string Remarks = string.Empty;
                        string paymentType = string.Empty;
                        foreach (DataRow dr in balanceLeftInfoTable.Rows)
                        {
                            PaymentsAgainstInvoice paymentsAgainstInvoice = new PaymentsAgainstInvoice();
                            decimal balanceLeft = Convert.ToDecimal(dr["remainingAmount"]);
                            PaymentMode mode = (PaymentMode)(Enum.Parse(typeof(PaymentMode), dr["PaymentMode"].ToString()));

                            if (balanceLeft >= amountLeft)
                            {
                                invoiceStatus = InvoiceStatus.Paid;
                                paymentsAgainstInvoice.PaymentDetailId = Convert.ToInt32(dr["paymentDetailId"]);
                                paymentsAgainstInvoice.Amount = amountLeft;
                                paymentsAgainstInvoice.IsPartial = false;
                                listOfPaymentsAgainstInvoice.Add(paymentsAgainstInvoice);

                                if (mode == PaymentMode.Cash)
                                {
                                    paymentType = mode.ToString() + " Amount( " + Math.Round(amountLeft,Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )) + " )";
                                }
                                else
                                {
                                    paymentType = mode.ToString() + "-" + dr["Referencenumber"] + " Amount( " + Math.Round(amountLeft,Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )) + " )";
                                }

                                if (Remarks == string.Empty)
                                {
                                    Remarks = "Invoice is fully settled against " + paymentType;
                                }
                                else
                                {
                                    Remarks = Remarks + " and is fully settled against " + paymentType;
                                }
                                break;
                            }
                            else
                            {
                                invoiceStatus = InvoiceStatus.PartiallyPaid;
                                paymentsAgainstInvoice.PaymentDetailId = Convert.ToInt32(dr["paymentDetailId"]);
                                paymentsAgainstInvoice.Amount = balanceLeft;
                                paymentsAgainstInvoice.IsPartial = true;
                                listOfPaymentsAgainstInvoice.Add(paymentsAgainstInvoice);
                                amountLeft = amountLeft - balanceLeft;

                                if (mode == PaymentMode.Cash)
                                {
                                    paymentType = mode.ToString() + " Amount( " + Math.Round(balanceLeft,Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )) + " )";
                                }
                                else
                                {
                                    paymentType = mode.ToString() + "-" + dr["Referencenumber"] + " Amount( " + Math.Round(balanceLeft,Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )) + " )";
                                }

                                if (Remarks == string.Empty)
                                {
                                    Remarks = "Invoice is partially settled against " + paymentType;
                                }
                                else
                                {
                                    Remarks = Remarks + " , " + paymentType;
                                }
                            }
                        }
                        if (Remarks != string.Empty)
                        {
                            bhInvoice.Remarks = Remarks;
                            invoice.Remarks = invoice.Remarks + " " + Remarks;
                        }
                    }
                    else if (isLCC || agency.AgencyTypeId == (int)Agencytype.Cash || agency.AgencyTypeId == (int)Agencytype.Service)
                    {
                        invoiceStatus = InvoiceStatus.Paid;
                    }
                    invoice.Status = invoiceStatus;
                    using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                    {
                        //Save Invoice
                    invoice.Save(itinerary.CheckDomestic("" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + ""), 0);
                        //invoice.Load(invoice.InvoiceNumber);

                        foreach (PaymentsAgainstInvoice paymentAgainstInvoice in listOfPaymentsAgainstInvoice)
                        {
                            PaymentDetails.UpdateRemainingBalance(paymentAgainstInvoice.PaymentDetailId, paymentAgainstInvoice.Amount);
                            PaymentDetails.AddPaymentAgainstInvoice(paymentAgainstInvoice.PaymentDetailId, invoice.InvoiceNumber, paymentAgainstInvoice.Amount, paymentAgainstInvoice.IsPartial);
                        }
                        //Add entry in booking history
                        bhInvoice.Save();
                        //Add entries in ledger transaction
                        foreach (LedgerTransaction ledgerTransaction in ledgerTransactionList)
                        {
                            ledgerTransaction.Narration.DocNo = Convert.ToString(invoice.DocTypeCode + Convert.ToString(invoice.DocumentNumber));
                            ledgerTransaction.CreatedOn = invoice.CreatedOn;
                            ledgerTransaction.Save();
                            LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTransaction.TxnId);
                        }
                        //Update balance of the agent                                                
                        if (!isLCC)
                        {
                            if (agency.AgencyTypeId != 1 && agency.AgencyTypeId != (int)Agencytype.Service)
                            {
                                Ledger.UpdateCurrentBalance(invoice.AgencyId, -(Math.Round(totalAmount)), memberId);
                            }
                            else
                            {
                                Agency.UpdateLCCBalance(invoice.AgencyId, -(Math.Round(totalAmount)), memberId);
                            }
                        }
                        else
                        {
                            Agency.UpdateLCCBalance(invoice.AgencyId, -(Math.Round(totalAmount)), memberId);
                        }

                        if (itinerary.BookingMode == BookingMode.WhiteLabel || itinerary.BookingMode == BookingMode.BookingAPI)
                        {
                            SaveWlInvoice(invoice, itinerary, ticket);
                        }
                        updateTransaction.Complete();
                    }
                }
                else if (invoiceRaisedResult == ticket.Count)
                {
                    invoice.InvoiceNumber = Invoice.isInvoiceGenerated(ticket[0].TicketId);
                }
                else
                {
                    //TODO: serious error log it and send mail to admin with detail.
                    throw new BookingEngineException("Invoice Error");
                }
            }
            return invoice.InvoiceNumber;
        }
        private static void SaveWlInvoice(Invoice invoice, FlightItinerary itinerary, List<Ticket> ticket)
        {
            WLInvoice wlInvoice = new WLInvoice();
            string bookingClass = string.Empty;
            string routing = string.Empty;
            wlInvoice.InvoiceNumber = invoice.InvoiceNumber;
            wlInvoice.AgencyId = invoice.AgencyId;
            Agency agency = new Agency();
            agency.Load(invoice.AgencyId);
            wlInvoice.AccountCode = agency.AccountCode;
            wlInvoice.Pnr = itinerary.PNR;
            wlInvoice.LineItem = new List<WLInvoiceLineItem>();
            foreach (FlightInfo temp in itinerary.Segments)
            {
                bookingClass = bookingClass + temp.BookingClass;

            }
            FlightInfo[] segment = FlightInfo.GetSegments(itinerary.FlightId);
            routing = segment[0].Origin.CityCode;
            for (int i = 0; i < segment.Length; i++)
            {
                routing += "-" + segment[i].Destination.CityCode;
            }
            for (int i = 0; i < ticket.Count; i++)
            {
                WLInvoiceLineItem lineItem = new WLInvoiceLineItem();
                lineItem.BookingClass = bookingClass;
                lineItem.PaxName = FlightPassenger.GetPaxFullName(ticket[i].PaxId);
                lineItem.PaxType = FlightPassenger.GetPTC(ticket[i].PaxType);
                lineItem.ProductType = ProductType.Flight;
                lineItem.ReferenceId = ticket[i].TicketId;
                lineItem.ReferenceValue = ticket[i].ValidatingAriline + ticket[i].TicketNumber;
                lineItem.Sectors = routing;
                lineItem.Price = new PriceAccounts();
                lineItem.Price.PriceId = ticket[i].Price.PriceId;
                wlInvoice.LineItem.Add(lineItem);
            }
            wlInvoice.Save();
        }

        /// <summary>    
        /// Will raise invoice for the itinerary corresponding to flightId.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId">memberId of the member who is currently logged in.</param>
        /// <param name="prodType"></param>
        /// <returns>Invoice number generated. If invoice for itinerary is already generated then it will give the existing invoice number.</returns>
        ///         
        public static int RaiseInvoice(int productId, string remarks, int memberId, ProductType prodType)
        {
            int raiseInvoice = 0;
            if (prodType == ProductType.Flight)
            {
                FlightItinerary itinerary = new FlightItinerary(productId);
                raiseInvoice = RaiseInvoice(itinerary, remarks, memberId);
            }
            else if (prodType == ProductType.Hotel)
            {
                HotelItinerary itinerary = new HotelItinerary();
                itinerary.Load(productId);
                HotelRoom room = new HotelRoom();
                HotelPassenger passInfo = new HotelPassenger();
                passInfo.Load(productId);
                itinerary.Roomtype = room.Load(productId);
                itinerary.HotelPassenger = passInfo;
                raiseInvoice = RaiseInvoice(itinerary, remarks, memberId, 1);
            }
            else if (prodType == ProductType.Insurance)
            {
                Insurance insurance = new Insurance(productId);
                raiseInvoice = RaiseInvoice(insurance, remarks, memberId);
            }
            else if (prodType == ProductType.MobileRecharge)
            {
                MobileRechargeItinerary itinerary = new MobileRechargeItinerary();
                itinerary.Load(productId);
                raiseInvoice = RaiseMobileRechargeInvoice(itinerary, remarks, memberId);
            }
            return raiseInvoice;
        }
        /// <summary>
        /// This Overload method is used to Raise Invoice with extra parameter Rate of Exchange.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="prodType"></param>
        /// <param name="rateOfExchange"></param>
        /// <returns></returns>
        public static int RaiseInvoice(int productId, string remarks, int memberId, ProductType prodType, decimal rateOfExchange)
        {
            int raiseInvoice = 0;
            if (prodType == ProductType.Flight)
            {
                FlightItinerary itinerary = new FlightItinerary(productId);
                raiseInvoice = RaiseInvoice(itinerary, remarks, memberId);
            }
            else if (prodType == ProductType.Hotel)
            {
                HotelItinerary itinerary = new HotelItinerary();
                itinerary.Load(productId);
                HotelRoom room = new HotelRoom();
                HotelPassenger passInfo = new HotelPassenger();
                passInfo.Load(productId);
                itinerary.Roomtype = room.Load(productId);
                itinerary.HotelPassenger = passInfo;
                raiseInvoice = RaiseInvoice(itinerary, remarks, memberId, rateOfExchange);
            }
            else if (prodType == ProductType.Insurance)
            {
                Insurance insurance = new Insurance(productId);
                raiseInvoice = RaiseInvoice(insurance, remarks, memberId);
            }
            else if (prodType == ProductType.Transfers)
            {
                TransferItinerary itinerary = new TransferItinerary();
                itinerary.Load(productId);
                raiseInvoice = RaiseInvoice(itinerary, remarks, memberId, rateOfExchange);
            }
            else if (prodType == ProductType.SightSeeing)
            {
                SightseeingItinerary itinerary = new SightseeingItinerary();
                itinerary.Load(productId);
                raiseInvoice = RaiseInvoice(itinerary, remarks, memberId, rateOfExchange);
            }
            else if (prodType == ProductType.MobileRecharge)
            {
                MobileRechargeItinerary itinerary = new MobileRechargeItinerary();
                itinerary.Load(productId);
                raiseInvoice = RaiseMobileRechargeInvoice(itinerary, remarks, memberId);
            }
            return raiseInvoice;
        }
        /// <summary>
        /// Raises invoice for the given itinerary.
        /// </summary>
        /// <param name="itinerary">itinerary for which invoice is to be raised.</param>
        /// <param name="remarks">remarks if any.</param>
        /// <param name="memberId">memberId of the member who is currently logged in.</param>
        /// <returns>Invoice number generated. If invoice for itinerary is already generated then it will give the existing invoice number.</returns>
        public static int RaiseInvoice(HotelItinerary itinerary, string remarks, int memberId, decimal rateOfExchange)
        {
            //GTADBG
            Audit.Add(EventType.GTABooking, Severity.Normal, 1, "Hotel Raise Invoice entered. conf no : " + itinerary.ConfirmationNo + " hotelId : " + itinerary.HotelId, "");
            if (itinerary.Roomtype.Length == 0)
            {
                HotelRoom hRoom = new HotelRoom();
                itinerary.Roomtype = hRoom.Load(itinerary.HotelId);
            }
            return RaiseInvoicePart2(itinerary, remarks, memberId, rateOfExchange);
        }
        /// <summary>
        /// This Method is used to Raise the Transfer Invoice
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="rateOfExchange"></param>
        /// <returns></returns>
        public static int RaiseInvoice(TransferItinerary itinerary, string remarks, int memberId, decimal rateOfExchange)
        {
            //GTADBG
            Audit.Add(EventType.TransferBooking, Severity.Normal, 1, "Transfer Raise Invoice entered. conf no : " + itinerary.ConfirmationNo + " transferId : " + itinerary.TransferId, "");
            BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(itinerary.TransferId, ProductType.Transfers));
            Invoice invoice = new Invoice();

            if (itinerary.BookingStatus == TransferBookingStatus.Confirmed)
            {
                if (invoice.InvoiceRaised(itinerary.TransferId, ProductType.Transfers) == 0)
                {
                    decimal totalAmount = 0;
                    invoice.PaymentMode = string.Empty;
                    invoice.Remarks = remarks;
                    invoice.AgencyId = booking.AgencyId;
                    invoice.Status = InvoiceStatus.Paid;
                    invoice.CreatedBy = memberId;
                    invoice.CreatedOn = DateTime.UtcNow;

                    // Generating Invoice line items.
                    TransferVehicle vehInfo = new TransferVehicle();
                    vehInfo = itinerary.TransferDetails[0];
                    invoice.LineItem = new List<InvoiceLineItem>();
                    PriceAccounts priceInfo = new PriceAccounts();
                    InvoiceLineItem line = new InvoiceLineItem();
                    line.ItemDescription = string.Empty;
                    line.ItemReferenceNumber = itinerary.TransferId;
                    line.ItemTypeId = (int)InvoiceItemTypeId.TransferBooking;

                    priceInfo = vehInfo.ItemPrice;
                    priceInfo.RateOfExchange = rateOfExchange;
                    priceInfo.Currency = vehInfo.Currency;
                    priceInfo.Save();
                    line.Price = vehInfo.ItemPrice;
                    invoice.LineItem.Add(line);

                    using (System.Transactions.TransactionScope updateTransaction =
            new System.Transactions.TransactionScope())
                    {
                        Audit.Add(EventType.TransferBooking, Severity.Normal, 1, "Saving Inovoice now.", "");
                        invoice.Save(false, 0, ProductType.Transfers);
                        updateTransaction.Complete();
                    }
                }
                else if (Convert.ToBoolean(invoice.InvoiceRaised(itinerary.TransferId, ProductType.Transfers)))
                {
                    invoice.InvoiceNumber = Invoice.isInvoiceGenerated(itinerary.TransferId, ProductType.Transfers);
                }
                else
                {
                    //TODO: serious error log it and send mail to admin with detail.
                    throw new BookingEngineException("Invoice Error");
                }
            }
            return invoice.InvoiceNumber;
        }
        /// <summary>
        /// This Method is used to Raise the Sightseeings Invoice
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="rateOfExchange"></param>
        /// <returns></returns>
       /* public static int RaiseInvoice(SightseeingItinerary itinerary, string remarks, int memberId, decimal rateOfExchange)
        {
            //GTADBG
            Audit.Add(EventType.TransferBooking, Severity.Normal, 1, "Transfer Raise Invoice entered. conf no : " + itinerary.ConfirmationNo + " Id : " + itinerary.SightseeingId, "");
            BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(itinerary.SightseeingId, ProductType.SightSeeing));
            Invoice invoice = new Invoice();

            if (itinerary.BookingStatus == SightseeingBookingStatus.Confirmed)
            {
                if (invoice.InvoiceRaised(itinerary.SightseeingId, ProductType.SightSeeing) == 0)
                {
                    decimal totalAmount = 0;
                    invoice.PaymentMode = string.Empty;
                    invoice.Remarks = remarks;
                    invoice.AgencyId = booking.AgencyId;
                    invoice.Status = InvoiceStatus.Paid;
                    invoice.CreatedBy = memberId;
                    invoice.CreatedOn = DateTime.UtcNow;

                    // Generating Invoice line items.
                    invoice.LineItem = new List<InvoiceLineItem>();
                    PriceAccounts priceInfo = new PriceAccounts();
                    InvoiceLineItem line = new InvoiceLineItem();
                    line.ItemDescription = string.Empty;
                    line.ItemReferenceNumber = itinerary.SightseeingId;
                    line.ItemTypeId = (int)InvoiceItemTypeId.SightseeingBooking;

                    priceInfo = itinerary.Price;
                    priceInfo.RateOfExchange = rateOfExchange;
                    priceInfo.Currency = itinerary.Currency;
                    priceInfo.Save();
                    line.Price = priceInfo;
                    invoice.LineItem.Add(line);

                    using (System.Transactions.TransactionScope updateTransaction =
            new System.Transactions.TransactionScope())
                    {
                        Audit.Add(EventType.SightseeingBooking, Severity.Normal, 1, "Saving Inovoice now.", "");
                        invoice.Save(false, 0, ProductType.SightSeeing);
                        updateTransaction.Complete();
                    }
                }
                else if (Convert.ToBoolean(invoice.InvoiceRaised(itinerary.SightseeingId, ProductType.SightSeeing)))
                {
                    invoice.InvoiceNumber = Invoice.isInvoiceGenerated(itinerary.SightseeingId, ProductType.SightSeeing);
                }
                else
                {
                    //TODO: serious error log it and send mail to admin with detail.
                    throw new BookingEngineException("Invoice Error");
                }
            }
            return invoice.InvoiceNumber;
        }*/
        /// <summary>
        /// Raises invoice for the given itinerary.
        /// </summary>
        /// <param name="itinerary">itinerary for which invoice is to be raised.</param>
        /// <param name="remarks">remarks if any.</param>
        /// <param name="memberId">memberId of the member who is currently logged in.</param>
        /// <returns>Invoice number generated. If invoice for itinerary is already generated then it will give the existing invoice number.</returns>
        public static int RaiseInvoicePart2(HotelItinerary itinerary, string remarks, int memberId, decimal rateOfExchange)
        {
            BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(itinerary.HotelId, ProductType.Hotel));
            Invoice invoice = new Invoice();

            if (itinerary.Status == HotelBookingStatus.Confirmed)
            {
                if (invoice.InvoiceRaised(itinerary.HotelId, ProductType.Hotel) == 0)
                {
                    decimal totalAmount = 0;
                    invoice.PaymentMode = string.Empty;
                    invoice.Remarks = remarks;
                    invoice.AgencyId = booking.AgencyId;
                    invoice.Status = InvoiceStatus.Paid;
                    invoice.CreatedBy = memberId;
                    invoice.CreatedOn = DateTime.UtcNow;

                    // Generating Invoice line items.
                    invoice.LineItem = new List<InvoiceLineItem>();
                    PriceAccounts priceInfo = new PriceAccounts();
                    for (int i = 0; i < itinerary.Roomtype.Length; i++)
                    {
                        InvoiceLineItem line = new InvoiceLineItem();
                        line.ItemDescription = string.Empty;
                        line.ItemReferenceNumber = itinerary.Roomtype[i].RoomId;
                        //TODO: line item type Id. should not be hardcoded 3.
                        line.ItemTypeId = (int)InvoiceItemTypeId.HotelBooking;
                        if (!itinerary.IsDomestic)
                        {
                            priceInfo = itinerary.Roomtype[i].Price;
                            priceInfo.RateOfExchange = rateOfExchange;
                            priceInfo.Save();
                        }
                        line.Price = itinerary.Roomtype[i].Price;
                        invoice.LineItem.Add(line);
                    }
                    using (System.Transactions.TransactionScope updateTransaction =
            new System.Transactions.TransactionScope())
                    {
                        Audit.Add(EventType.GTABooking, Severity.Normal, 1, "Saving Inovoice now. IsDomestic:" + itinerary.IsDomestic, "");
                        invoice.Save(itinerary.IsDomestic, 0, ProductType.Hotel);
                        invoice.Load(invoice.InvoiceNumber);
                        if (itinerary.IsDomestic && itinerary.Source != HotelBookingSource.IAN)
                        {
                            NarrationBuilder objNarration = new NarrationBuilder();
                            Ledger ledger = new Ledger();
                            ledger.Load(booking.AgencyId, DateTime.Now, DateTime.Now);
                            for (int i = 0; i < itinerary.Roomtype.Length; i++)
                            {
                                totalAmount = totalAmount + itinerary.Roomtype[i].Price.GetAgentPrice();
                                LedgerTransaction ledgerTxn = new LedgerTransaction();

                                //To setup narration
                                objNarration.PaxName = string.Format("{0} {1}", itinerary.HotelPassenger.Firstname, itinerary.HotelPassenger.Lastname);
                                objNarration.Remarks = "Hotel Booking Created";
                                objNarration.DocNo = invoice.CompleteInvoiceNumber;
                                objNarration.HotelConfirmationNo = itinerary.ConfirmationNo;
                                objNarration.TravelDate = itinerary.StartDate.ToShortDateString();
                                ledgerTxn.LedgerId = ledger.LedgerId;
                                ledgerTxn.Amount = -(itinerary.Roomtype[i].Price.GetAgentPrice());
                                ledgerTxn.Narration = objNarration;
                                ledgerTxn.ReferenceId = itinerary.Roomtype[i].RoomId;
                                ledgerTxn.ReferenceType = ReferenceType.HotelBooked;
                                ledgerTxn.IsLCC = true; //For Hotel the ledger transaction will be like LCC
                                ledgerTxn.Notes = "";
                                ledgerTxn.Date = DateTime.UtcNow;
                                ledgerTxn.CreatedBy = memberId;
                                ledgerTxn.Save();
                                LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);
                            }
                            Agency.UpdateLCCBalance(invoice.AgencyId, -(totalAmount), memberId);
                        }
                        //TODO: White Label will be done later
                        //if (itinerary.BookingMode == BookingMode.WhiteLabel)
                        //{
                        //    SaveWlInvoice(invoice, itinerary, ticket);
                        //}
                        updateTransaction.Complete();
                    }
                }
                else if (invoice.InvoiceRaised(itinerary.HotelId, ProductType.Hotel) == itinerary.Roomtype.Length)
                {
                    invoice.InvoiceNumber = Invoice.isInvoiceGenerated(itinerary.Roomtype[0].RoomId, ProductType.Hotel);
                }
                else
                {
                    //TODO: serious error log it and send mail to admin with detail.
                    throw new BookingEngineException("Invoice Error");
                }
            }
            return invoice.InvoiceNumber;
        }
        /// <summary>
        /// Raises Invoice for Mobile recharge
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <returns></returns>
       /* public static int RaiseMobileRechargeInvoice(MobileRechargeItinerary itinerary, string remarks, int memberId)
        {
            BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(itinerary.RechargeId, ProductType.MobileRecharge));
            Invoice invoice = new Invoice();
            if (invoice.InvoiceRaised(itinerary.RechargeId, ProductType.MobileRecharge) == 0)
            {
                decimal totalAmount = 0;
                invoice.PaymentMode = string.Empty;
                invoice.Remarks = remarks;
                invoice.AgencyId = booking.AgencyId;
                invoice.Status = InvoiceStatus.Paid;
                invoice.CreatedBy = memberId;
                invoice.CreatedOn = DateTime.UtcNow;

                // Generating Invoice line items.
                invoice.LineItem = new List<InvoiceLineItem>();
                InvoiceLineItem line = new InvoiceLineItem();
                line.ItemDescription = string.Empty;
                line.ItemReferenceNumber = itinerary.RechargeId;
                //TODO: line item type Id. should not be hardcoded 3.
                line.ItemTypeId = (int)InvoiceItemTypeId.MobileRecharge;
                PriceAccounts price = new PriceAccounts();
                price.Load(itinerary.PriceId);
                line.Price = price;
                invoice.LineItem.Add(line);
                using (System.Transactions.TransactionScope updateTransaction =
       new System.Transactions.TransactionScope())
                {
                    Audit.Add(EventType.MobileRecharge, Severity.High, 1, "Saving Inovoice now. MobileNum" + itinerary.MobileNumber, "");
                    invoice.Save(true, 0, ProductType.MobileRecharge);
                    invoice.Load(invoice.InvoiceNumber);

                    NarrationBuilder objNarration = new NarrationBuilder();
                    Ledger ledger = new Ledger();
                    ledger.Load(booking.AgencyId, DateTime.Now, DateTime.Now);

                    totalAmount = Convert.ToDecimal(itinerary.Amount) - itinerary.AgentCommission;
                    LedgerTransaction ledgerTxn = new LedgerTransaction();

                    //To setup narration
                    objNarration.PaxName = "";
                    objNarration.Remarks = "Mobile Recharge for " + itinerary.MobileNumber;
                    objNarration.DocNo = invoice.CompleteInvoiceNumber;
                    objNarration.TravelDate = DateTime.Now.ToString();
                    ledgerTxn.LedgerId = ledger.LedgerId;
                    ledgerTxn.Amount = -(Convert.ToDecimal(itinerary.Amount) - itinerary.AgentCommission);
                    ledgerTxn.Narration = objNarration;
                    ledgerTxn.ReferenceId = itinerary.RechargeId;
                    ledgerTxn.ReferenceType = ReferenceType.MobileRecharge;
                    ledgerTxn.IsLCC = true; //For Hotel the ledger transaction will be like LCC
                    ledgerTxn.Notes = "";
                    ledgerTxn.Date = DateTime.UtcNow;
                    ledgerTxn.CreatedBy = memberId;
                    ledgerTxn.Save();
                    LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);

                    Agency.UpdateLCCBalanceWithoutRndoff(invoice.AgencyId, -(totalAmount), memberId);

                    updateTransaction.Complete();
                }
            }
            else if (invoice.InvoiceRaised(itinerary.RechargeId, ProductType.MobileRecharge) > 0)
            {
                invoice.InvoiceNumber = Invoice.isInvoiceGenerated(itinerary.RechargeId, ProductType.MobileRecharge);
            }
            else
            {
                //TODO: serious error log it and send mail to admin with detail.
                throw new BookingEngineException("Invoice Error");
            }
            return invoice.InvoiceNumber;
        }*/
        /// Raises invoice for the given insurance.
        /// </summary>
        /// <param name="insurance">Insurance for which invoice has to be raised</param>
        /// <param name="remarks">remarks if any.</param>
        /// <param name="memberId">memberId of the member who is currently logged in.</param>
        /// <returns></returns>
       /* public static int RaiseInvoice(Insurance insurance, string remarks, int memberId)
        {
            BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(insurance.InsuranceId, ProductType.Insurance));
            Invoice invoice = new Invoice();
            int activePassenger = 0;
            for (int i = 0; i < insurance.Passenger.Length; i++)
            {
                if (insurance.Passenger[i].IsActive || insurance.Passenger[i].Status == InsurancePassengerStatus.Cancelled)
                {
                    activePassenger++;
                }
            }
            if (insurance.Status == InsuranceStatus.Created)
            {
                if (invoice.InvoiceRaised(insurance.InsuranceId, ProductType.Insurance) == 0)
                {
                    decimal totalAmount = 0;
                    invoice.PaymentMode = string.Empty;
                    invoice.Remarks = remarks;
                    invoice.AgencyId = booking.AgencyId;
                    invoice.Status = InvoiceStatus.Paid;
                    invoice.CreatedBy = memberId;
                    invoice.CreatedOn = DateTime.UtcNow;

                    // Generating Invoice line items.
                    invoice.LineItem = new List<InvoiceLineItem>();
                    for (int i = 0; i < insurance.Passenger.Length; i++)
                    {
                        InvoiceLineItem line = new InvoiceLineItem();
                        line.ItemDescription = string.Empty;
                        line.ItemReferenceNumber = insurance.Passenger[i].PaxId;
                        line.ItemTypeId = (int)InvoiceItemTypeId.InsuranceBooking;
                        line.Price = insurance.Passenger[i].Price;
                        invoice.LineItem.Add(line);
                    }
                    using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                    {
                        Audit.Add(EventType.InsuranceBooking, Severity.Normal, 1, "Saving Invoice now. Insurance:" + insurance.InsuranceId, "");
                        invoice.Save(insurance.IsDomestic, 0, ProductType.Insurance);
                        invoice.Load(invoice.InvoiceNumber);
                        Ledger ledger = new Ledger();
                        NarrationBuilder objNarration = new NarrationBuilder();
                        ledger.Load(booking.AgencyId, DateTime.Now, DateTime.Now);
                        for (int i = 0; i < insurance.Passenger.Length; i++)
                        {
                            totalAmount = totalAmount + insurance.Passenger[i].Price.GetAgentInsurancePrice();
                            LedgerTransaction ledgerTxn = new LedgerTransaction();
                            ledgerTxn.LedgerId = ledger.LedgerId;
                            ledgerTxn.Amount = -(insurance.Passenger[i].Price.GetAgentInsurancePrice());
                            objNarration.Remarks = "Insurance Policy Created";
                            objNarration.PolicyNo = Convert.ToString(insurance.Passenger[i].PolicyNumber);
                            objNarration.PaxName = insurance.Passenger[i].Title + insurance.Passenger[i].FirstName + insurance.Passenger[i].LastName;
                            objNarration.DocNo = invoice.CompleteInvoiceNumber;
                            ledgerTxn.Narration = objNarration;
                            ledgerTxn.ReferenceId = insurance.Passenger[i].PaxId;
                            ledgerTxn.ReferenceType = ReferenceType.InsuranceCreated;
                            ledgerTxn.IsLCC = true; //For Insurance the ledger transaction will be like LCC
                            ledgerTxn.Notes = "";
                            ledgerTxn.Date = DateTime.UtcNow;
                            ledgerTxn.CreatedBy = memberId;
                            ledgerTxn.Save();
                            LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);
                        }
                        Agency.UpdateLCCBalance(invoice.AgencyId, -(totalAmount), memberId);

                        //TODO: White Label will be done later
                        //if (itinerary.BookingMode == BookingMode.WhiteLabel)
                        //{
                        //    SaveWlInvoice(invoice, itinerary, ticket);
                        //}
                        updateTransaction.Complete();
                    }
                }
                else if (invoice.InvoiceRaised(insurance.InsuranceId, ProductType.Insurance) == activePassenger)
                {
                    for (int i = 0; i < insurance.Passenger.Length; i++)
                    {
                        if (insurance.Passenger[i].IsActive || insurance.Passenger[i].Status == InsurancePassengerStatus.Cancelled)
                        {
                            invoice.InvoiceNumber = Invoice.isInvoiceGenerated(insurance.Passenger[i].PaxId, ProductType.Insurance);
                            break;
                        }
                    }
                }
                else
                {
                    //TODO: serious error log it and send mail to admin with detail.
                    throw new BookingEngineException("Invoice Error");
                }
            }
            return invoice.InvoiceNumber;
        }*/
        private struct PaymentsAgainstInvoice
        {
            public int PaymentDetailId;
            public decimal Amount;
            public bool IsPartial;
        }

        
        /// <summary>
        /// This function will settle credit note with due invoice
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="cancellationCharge"></param>
        /// <param name="creditNoteNumber"></param>
        /// <param name="paymentDetailId"></param>
        /// <param name="isLcc"></param>
        public static void KnockOffInvoiceWithCreditNote(int ticketId, decimal ticketAmount, decimal cancellationCharge, int creditNoteNumber, int paymentDetailId, bool isLcc, int loggedMemberId, bool isOfflineBooking)
        {
            int invoiceNumber = 0;
            if (!isOfflineBooking)
            {
                invoiceNumber = Invoice.GetInvoiceNumberByTicketId(ticketId);
            }
            else
            {
                invoiceNumber = Invoice.GetInvoiceNumberByOfflineBookingId(ticketId);
            }
            Invoice invoice = new Invoice();
            invoice.Load(invoiceNumber);
            decimal totalPrice = 0;
            decimal amountToBeKnockOff = 0;
            //To Calculate total bill amount
            int agencyTypeId = Agency.GetAgencyTypeId(invoice.AgencyId);
            foreach (InvoiceLineItem lineItem in invoice.LineItem)
            {
                if ((int)Agencytype.Service == agencyTypeId)
                {
                    totalPrice = totalPrice + Math.Round(lineItem.Price.GetServiceAgentPrice(), Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                }
                else
                {
                    totalPrice = totalPrice + Math.Round(lineItem.Price.GetAgentPrice(), Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                }
            }
            decimal invoiceDueAmount = 0;
            // To calculate due amount(Amount to be paid against Unpaid/Partialy paid invoice)
            if (invoice.Status != InvoiceStatus.Paid)
            {
                invoiceDueAmount = totalPrice - Invoice.GetAcceptedPaymentsAgainstInvoice(invoiceNumber);
            }
            //To calculate amount to be KnockOff(Balance amount to be refund against refunded/voided invoces)            
            amountToBeKnockOff = (ticketAmount - cancellationCharge) - invoiceDueAmount;
            //Payment Detail to settle invoice
            PaymentDetails pd = new PaymentDetails();
            // Payment detail to make payment             
            InvoiceCollection ic = new InvoiceCollection();
            pd.Load(paymentDetailId);
            pd.ReferenceNo = creditNoteNumber;
            pd.Invoices = new List<InvoiceCollection>();
            //If amount to be refund>due amount then invoice(against that credit note generated)
            //will be settled and a payment will be created equal to balance amount(amount to be refund-due amount)            
            if (amountToBeKnockOff > 0)
            {
                //To settle Invoice
                ic.InvoiceNumber = invoiceNumber;
                ic.Amount = invoiceDueAmount;
                invoice.Status = InvoiceStatus.Paid;
                ic.IsPartial = false;
                pd.Invoices.Add(ic);
                pd.RemainingAmount = amountToBeKnockOff;//pd.Amount-invoiceDueAmount;                
            }

            //If amount to be refund<due amount then due amount of the invoice(against that credit note generated)
            //will be equal to balance amount(due amount-amount to be refund)
            else if (amountToBeKnockOff < 0)
            {
                //To settle Invoice                
                ic.InvoiceNumber = invoiceNumber;
                ic.Amount = invoiceDueAmount + amountToBeKnockOff;// +invoiceDueAmount;
                ic.IsPartial = true;
                invoice.Status = InvoiceStatus.PartiallyPaid;
                pd.RemainingAmount = 0;// -amountToBeKnockOff;// pd.Amount - (amountToBeKnockOff + invoiceDueAmount); 
                pd.Invoices.Add(ic);
            }

            //If amount to be refund=due amount then invoice(against that credit note generated)
            //will be settled
            else
            {
                //To settle Invoice
                ic.InvoiceNumber = invoiceNumber;
                ic.Amount = invoiceDueAmount;
                ic.IsPartial = false;
                invoice.Status = InvoiceStatus.Paid;
                pd.RemainingAmount = 0;
                pd.Invoices.Add(ic);
            }
            pd.ModeOfPayment = PaymentMode.CreditNote;
            pd.Save();
            Invoice.UpdateInvoiceStatus(invoiceNumber, invoice.Status, loggedMemberId);
        }

        /// <summary>
        /// This function is used to generate credit note
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="ticket"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="cancellationCharge"></param>
        /// <param name="paymentDetailId"></param>
        /// <param name="prodType"></param>
        /// <returns></returns>
        public static string RaiseCreditNote(FlightItinerary itinerary, Ticket ticket, string remarks, int memberId, decimal cancellationCharge, int paymentDetailId, ProductType prodType, BookingStatus bookingStatus)
        {
            CoreLogic.Audit.Add(CoreLogic.EventType.Account, CoreLogic.Severity.Normal, 0,
                "AccountUtility.RaiseCreditNote entered.", "");

            BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(itinerary.FlightId));
            Invoice invoice = new Invoice();
            bool isLCC = false;


            decimal ticketAmount = ((int)Agencytype.Service == Agency.GetAgencyTypeId(itinerary.AgencyId)) ? ticket.Price.GetServiceAgentPrice() - ticket.Price.OtherCharges : ticket.Price.GetAgentPrice() - ticket.Price.OtherCharges;// ; ticket.Price.PublishedFare + ticket.Price.Tax + ticket.Price.OtherCharges;           
            PriceAccounts refundPrice = ticket.Price;
            refundPrice.PriceId = 0;
            for (int counter = 0; counter < itinerary.Segments.Length; counter++)
            {
                Airline airline = new Airline();
                airline.Load(itinerary.Segments[counter].Airline);
                if (airline.IsLCC)
                {
                    isLCC = true;
                    break;
                }
            }
            if (FlightItinerary.IsTicketed(itinerary))
            {
                if (invoice.CreditNoteRaisedForTicketId(ticket.TicketId) == 0)
                {
                    Agency agency = new Agency(booking.AgencyId);
                    // bool isServiceAgency = agency.AgencyTypeId == Convert.ToInt32(ConfigurationSystem.Core["ServiceAgencyTypeId"]);
                    invoice.PaymentMode = string.Empty;
                    invoice.Remarks = remarks;
                    invoice.AgencyId = booking.AgencyId;
                    invoice.CreatedBy = memberId;
                    invoice.CreatedOn = DateTime.UtcNow;
                    // Generating Invoice line items.
                    invoice.LineItem = new List<InvoiceLineItem>();
                    InvoiceLineItem line = new InvoiceLineItem();
                    line.ItemDescription = string.Empty;
                    line.ItemReferenceNumber = ticket.TicketId;

                    //TODO: line item type Id. should not be hardcoded 1.
                    line.ItemTypeId = 1;
                    invoice.Status = InvoiceStatus.CreditNote;
                    try
                    {
                        refundPrice.Save();
                        line.Price = refundPrice;
                        invoice.LineItem.Add(line);
                        invoice.Save(itinerary.CheckDomestic("" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + ""), 0, prodType, bookingStatus);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
                else if (invoice.CreditNoteRaisedForTicketId(ticket.TicketId) == 1)
                {
                    invoice.InvoiceNumber = Invoice.isCreditNoteGenerated(ticket.TicketId);
                }
                else
                {
                    //TODO: serious error log it and send mail to admin with detail.
                    throw new BookingEngineException("Invoice Error");
                }
            }
            // To settle due invoice with credit note
            if (!isLCC)
            {
                KnockOffInvoiceWithCreditNote(ticket.TicketId, ticketAmount, cancellationCharge, invoice.InvoiceNumber, paymentDetailId, isLCC, memberId, false);
            }
            string[] tmp = Invoice.GetCreditNoteNumberByTicketId(ticket.TicketId);
            CoreLogic.Audit.Add(CoreLogic.EventType.Account, CoreLogic.Severity.Normal, 0, "AccountUtility.RaiseOffLineCreditNote exited with credit note number:" + tmp[0] + tmp[1], "");
            return tmp[0] + tmp[1];
        }
        /// <summary>
        /// This function will generate credit note for OffLineBooking
        /// </summary>
        /// <param name="offBookingId"></param>
        /// <param name="cancellationCharge"></param>
        /// <param name="price"></param>
        /// <param name="agencyId"></param>
        /// <param name="isLcc"></param>
        /// <param name="isDomestic"></param>
        /// <param name="remarks"></param>
        /// <param name="memberId"></param>
        /// <param name="paymentDetailId"></param>
        /// <param name="prodType"></param>
        /// <returns></returns>
        public static string RaiseOffLineCreditNote(int offBookingId, decimal cancellationCharge, PriceAccounts price, int agencyId, bool isLcc, bool isDomestic, string remarks, int memberId, int paymentDetailId, ProductType prodType, BookingStatus bookingStatus)
        {
            CoreLogic.Audit.Add(CoreLogic.EventType.Account, CoreLogic.Severity.Normal, 0,
                "AccountUtility.RaiseOffLineCreditNote entered with Price: " + price + ", AgencyId:" + agencyId +
                ", isLCC:" + isLcc + ", isDomestic:" + isDomestic + ", remarks:" + remarks + ", memberId:" + memberId +
                ", paymentDetailId:" + paymentDetailId + ", ProductType:" + prodType + ", BookingStatus:" + bookingStatus, "");
            Invoice invoice = new Invoice();

            PriceAccounts refundPrice = price;
            decimal ticketAmount = 0;
            refundPrice.PriceId = 0;
            //refundPrice.Save(true);
            if (Invoice.CreditNoteRaisedForItem(offBookingId, (int)InvoiceItemTypeId.Offline) == 0)
            {
                Agency agency = new Agency(agencyId);
                //bool isServiceAgency = agency.AgencyTypeId == Convert.ToInt32(ConfigurationSystem.Core["ServiceAgencyTypeId"]);                
                ticketAmount = ((int)Agencytype.Service == agency.AgencyTypeId) ? price.GetServiceAgentPrice() - price.OtherCharges : price.GetAgentPrice() - price.OtherCharges;//priceAccount.PublishedFare + priceAccount.Tax + priceAccount.OtherCharges;          
                invoice.PaymentMode = string.Empty;
                invoice.Remarks = remarks;
                invoice.AgencyId = agencyId;
                invoice.CreatedBy = memberId;
                invoice.CreatedOn = DateTime.UtcNow;
                // Generating Invoice line items.
                invoice.LineItem = new List<InvoiceLineItem>();
                InvoiceLineItem line = new InvoiceLineItem();
                line.ItemDescription = string.Empty;
                line.ItemReferenceNumber = offBookingId;
                line.ItemTypeId = (int)InvoiceItemTypeId.Offline;
                invoice.Status = InvoiceStatus.CreditNote;
                try
                {
                    refundPrice.Save();
                    line.Price = refundPrice;
                    invoice.LineItem.Add(line);
                    invoice.Save(isDomestic, 0, prodType, bookingStatus);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                //TODO: serious error log it and send mail to admin with detail.
                throw new BookingEngineException("Invoice Error");
            }
            // To settle due invoice with credit note
            if (!isLcc)
            {
                KnockOffInvoiceWithCreditNote(offBookingId, ticketAmount, cancellationCharge, invoice.InvoiceNumber, paymentDetailId, isLcc, memberId, true);
            }
            string[] tmp = Invoice.GetCreditNoteNumberStringByOfflineBookingId(offBookingId);
            CoreLogic.Audit.Add(CoreLogic.EventType.Account, CoreLogic.Severity.Normal, 0, "AccountUtility.RaiseOffLineCreditNote exited with credit note number:" + tmp[0] + tmp[1], "");
            return tmp[0] + tmp[1];
        }
        /// <summary>
        /// Raise invoice for train bookings
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="listOfPrice"></param>
        /// <returns></returns>
       /* public static bool RaiseInvoiceForTrain(TrainItinerary itinerary, List<PriceAccounts> listOfPrice)
        {
            Trace.TraceInformation("AccountUtility.RaiseInvoiceForTrain entered");
            decimal totalAmount = 0;
            Ledger ledger = new Ledger();
            Invoice invoice = new Invoice();
            ledger.Load(itinerary.AgencyId);
            List<LedgerTransaction> ledgerTransactionList = new List<LedgerTransaction>();
            for (int i = 0; i < itinerary.PassengerList.Count; i++)
            {
                LedgerTransaction ledgerTxn = new LedgerTransaction();
                ledgerTxn.LedgerId = ledger.LedgerId;
                decimal ledgerAmount = listOfPrice[i].GetTrainTicketPrice();
                ledgerTxn.Amount = -ledgerAmount;
                totalAmount += ledgerAmount;
                NarrationBuilder objNarration = new NarrationBuilder();
                objNarration.Remarks = "Train Ticket Created with PNR = " + itinerary.Pnr;
                objNarration.PaxName = itinerary.PassengerList[i].PaxName;
                objNarration.TicketNo = itinerary.TicketNumber;
                objNarration.TravelDate = itinerary.TravelDate.ToShortDateString();
                objNarration.FlightNo = itinerary.TrainNumber;
                objNarration.Sector = itinerary.Origin + "-" + itinerary.Destination;
                ledgerTxn.Narration = objNarration;
                ledgerTxn.ReferenceType = ReferenceType.TrainTicket;
                ledgerTxn.IsLCC = true;
                ledgerTxn.Notes = "";
                ledgerTxn.Date = DateTime.UtcNow;
                ledgerTxn.CreatedOn = itinerary.CreatedOn;
                ledgerTxn.CreatedBy = itinerary.CreatedBy;
                ledgerTxn.ReferenceId = itinerary.PassengerList[i].PaxId;
                ledgerTransactionList.Add(ledgerTxn);
            }
            invoice.PaymentMode = string.Empty;
            invoice.Remarks = "Train Ticket Created";
            invoice.AgencyId = itinerary.AgencyId;
            invoice.CreatedBy = itinerary.CreatedBy;
            invoice.CreatedOn = DateTime.UtcNow;
            invoice.Status = InvoiceStatus.Paid;
            // Generating Invoice line items.
            invoice.LineItem = new List<InvoiceLineItem>();
            for (int i = 0; i < itinerary.PassengerList.Count; i++)
            {
                InvoiceLineItem line = new InvoiceLineItem();
                line.ItemDescription = string.Empty;
                line.ItemReferenceNumber = itinerary.PassengerList[i].PaxId;
                //TODO: line item type Id. should not be hardcoded 1.
                line.ItemTypeId = (int)InvoiceItemTypeId.TrainBooking;
                line.Price = listOfPrice[i];
                invoice.LineItem.Add(line);
            }
            using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
            {
                //Save Invoice
                invoice.Save(true, 0, ProductType.Train);
                //Add entries in ledger transaction
                invoice.Load(invoice.InvoiceNumber);
                foreach (LedgerTransaction ledgerTransaction in ledgerTransactionList)
                {
                    ledgerTransaction.Narration.DocNo = invoice.CompleteInvoiceNumber;
                    ledgerTransaction.Save();
                    LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTransaction.TxnId);
                }
                Agency.UpdateLCCBalance(invoice.AgencyId, -(totalAmount), invoice.CreatedBy);
                updateTransaction.Complete();
            }
            Trace.TraceInformation("AccountUtility.RaiseInvoiceForTrain exited");
            return true;
        }
*/
        /// <summary>
        /// To edit containts of credit note
        /// </summary>
        /// <param name="creditNotePriceNew"></param>
        /// <param name="adminFee"></param>
        /// <param name="suppFee"></param>
        /// <param name="ticketId"></param>
        /// <param name="invoiceNumber"></param>
        public static void EditCreditNote(PriceAccounts creditNotePriceNew, decimal adminFee, decimal suppFee, int ticketId, int creditNoteNumber, int loggedMemberId, string remarks)
        {
            Ticket ticket = new Ticket();
            ticket.Load(ticketId);
            InvoiceLineItem creditNoteLineItem = new InvoiceLineItem().GetLineItems(creditNoteNumber)[0];
            creditNoteLineItem.CreatedBy = loggedMemberId;
            CancellationCharges cCharge = new CancellationCharges();
            cCharge.Load(ticketId, ProductType.Flight, InvoiceItemTypeId.Ticketed);
            Member member = new Member(loggedMemberId);
            int bookingId = new FlightItinerary(ticket.FlightId).BookingId;

            BookingHistory bhRefund = new BookingHistory();
            BookingHistory bhCancellCharge = new BookingHistory();
            FlightItinerary itinerary = new FlightItinerary(ticket.FlightId);

            bhRefund.BookingId = bookingId;
            bhRefund.CreatedBy = loggedMemberId;
            bhRefund.EventCategory = EventCategory.Refund;

            bhCancellCharge.BookingId = bookingId;
            bhCancellCharge.CreatedBy = loggedMemberId;
            bhCancellCharge.EventCategory = EventCategory.Refund;

            LedgerTransaction ledgerTxnRefund = new LedgerTransaction();
            ledgerTxnRefund.LoadByReferenceId(cCharge.PaymentDetailId, (int)ReferenceType.TicketRefund);

            LedgerTransaction ledgerTxnCancelCharge = new LedgerTransaction();
            ledgerTxnCancelCharge.LoadByReferenceId(cCharge.PaymentDetailId, (int)ReferenceType.TicketCancellationCharge);

            ledgerTxnCancelCharge.Amount = -(adminFee + suppFee);

            decimal originalAmount = cCharge.AdminFee + cCharge.SupplierFee;
            decimal editedAmount = adminFee + suppFee;
            bhCancellCharge.Remarks = "Cancellation charge edited on " + DateTime.Now.ToString("dd/MM/yyyy") +
                    " by " + member.FullName + " from " + originalAmount.ToString("N2") + " to " + editedAmount.ToString("N2") + ": " + remarks;

            originalAmount = ledgerTxnRefund.Amount + creditNotePriceNew.OtherCharges;

            editedAmount = ((int)Agencytype.Service == Agency.GetAgencyTypeId(itinerary.AgencyId)) ? creditNotePriceNew.GetServiceAgentPrice() : creditNotePriceNew.GetAgentPrice();
            bhRefund.Remarks = "Ticket amount for refund edited on " + DateTime.Now.ToString("dd/MM/yyyy") +
                " by " + member.FullName + " from " + originalAmount.ToString("N2") + " to " + editedAmount.ToString("N2") + ": " + remarks;

            bool isLCC = false;
            // FlightItinerary itinerary = new FlightItinerary(ticket.FlightId);
            for (int counter = 0; counter < itinerary.Segments.Length; counter++)
            {
                Airline airline = new Airline();
                airline.Load(itinerary.Segments[counter].Airline);
                if (airline.IsLCC)
                {
                    isLCC = true;
                    break;
                }
            }
            int agencyTypeId = Agency.GetAgencyTypeId(itinerary.AgencyId);
            decimal amountToBeRefund = ((int)Agencytype.Service == agencyTypeId) ? creditNotePriceNew.GetServiceAgentPrice() - (adminFee + suppFee) : creditNotePriceNew.GetAgentPrice() - (adminFee + suppFee);
            decimal refundedAmount = ((int)Agencytype.Service == agencyTypeId) ? creditNoteLineItem.Price.GetServiceAgentPrice() - (cCharge.AdminFee + cCharge.SupplierFee) : creditNoteLineItem.Price.GetAgentPrice() - (cCharge.AdminFee + cCharge.SupplierFee);
            decimal invoiceDueAmount = 0;
            //decimal newCreditNotePrice = creditNotePriceNew.GetAgentPrice();

            //To check that is creditnote using ticket price or its own price
            if (ticket.Price.PriceId != creditNoteLineItem.Price.PriceId)
            {
                creditNotePriceNew.PriceId = creditNoteLineItem.Price.PriceId;
            }
            creditNotePriceNew.CommissionType = ticket.Price.CommissionType;
            creditNotePriceNew.YQTax = ticket.Price.YQTax;
            creditNotePriceNew.OurCommission = ticket.Price.OurCommission;
            creditNotePriceNew.OurPLB = ticket.Price.OurPLB;

            Invoice invoice = new Invoice();
            PaymentDetails pd = new PaymentDetails();
            InvoiceCollection ic = new InvoiceCollection();
            string[] crNo = Invoice.GetCreditNoteNumberByTicketId(ticketId);

            pd.Remarks = "Payment from edit creditNote. CreditNoteNo: " + crNo[0] + crNo[1];
            pd.Status = PaymentStatus.Accepted;
            pd.ModeOfPayment = PaymentMode.EditCreditNote;
            pd.ReferenceNo = creditNoteNumber;
            pd.CreatedBy = loggedMemberId;
            pd.PaymentDate = DateTime.Now.ToUniversalTime();

            invoice.Load(Invoice.GetInvoiceNumberByTicketId(ticketId));
            // int agencyTypeId = Agency.GetAgencyTypeId(invoice.AgencyId);
            if (agencyTypeId == (int)Agencytype.Cash || agencyTypeId == (int)Agencytype.Service)
            {
                isLCC = true;
            }

            pd.IsLCC = isLCC;
            pd.AgencyId = invoice.AgencyId;

            if (amountToBeRefund > refundedAmount)
            {
                pd.Amount = amountToBeRefund - refundedAmount;
                if (!isLCC)
                {
                    if (invoice.Status == InvoiceStatus.Paid)
                    {
                        pd.RemainingAmount = pd.Amount;
                        pd.Remarks = "Payment from edit creditNote. CreditNoteNo: " + crNo[0] + crNo[1];
                    }
                    else
                    {
                        invoiceDueAmount = invoice.TotalPrice - Invoice.GetPaymentDoneAgainstInvoice(invoice.InvoiceNumber);

                        if ((amountToBeRefund - refundedAmount) >= invoiceDueAmount)
                        {
                            invoice.Status = InvoiceStatus.Paid;
                            pd.Amount = amountToBeRefund - refundedAmount;
                            pd.RemainingAmount = pd.Amount - invoiceDueAmount;

                            ic.Amount = invoiceDueAmount;
                            ic.InvoiceNumber = invoice.InvoiceNumber;
                            ic.IsPartial = false;
                        }
                        else if ((amountToBeRefund - refundedAmount) < invoiceDueAmount)
                        {
                            invoice.Status = InvoiceStatus.PartiallyPaid;
                            pd.RemainingAmount = 0;

                            ic.Amount = amountToBeRefund - refundedAmount;
                            ic.InvoiceNumber = invoice.InvoiceNumber;
                            ic.IsPartial = true;
                        }
                    }
                }
                else
                {
                    pd.RemainingAmount = pd.Amount;
                    pd.Remarks = "Payment from edit creditNote. CreditNoteNo: " + crNo[0] + crNo[1];
                }
            }
            else if (amountToBeRefund < refundedAmount)
            {
                if (!isLCC)
                {
                    pd.Amount = amountToBeRefund - refundedAmount;
                    invoice.Status = InvoiceStatus.PartiallyPaid;
                    pd.RemainingAmount = 0;

                    ic.Amount = amountToBeRefund - refundedAmount;
                    ic.InvoiceNumber = invoice.InvoiceNumber;
                    ic.IsPartial = true;
                }
                else
                {
                    pd.Amount = amountToBeRefund - refundedAmount;
                    pd.RemainingAmount = amountToBeRefund - refundedAmount;
                }
            }

            pd.Invoices = new List<InvoiceCollection>();

            if (ic.Amount != 0)
            {
                pd.Invoices.Add(ic);
            }
            cCharge.AdminFee = adminFee;
            cCharge.SupplierFee = suppFee;
            ledgerTxnRefund.Amount = ((int)Agencytype.Service == agencyTypeId) ? creditNotePriceNew.GetServiceAgentPrice() - creditNotePriceNew.OtherCharges : creditNotePriceNew.GetAgentPrice() - creditNotePriceNew.OtherCharges;

            //invoi
            //ledgerTxnRefund.Narration = nbRefund;
            //ledgerTxnCancelCharge.Narration = nbCancellCharge;

            using (System.Transactions.TransactionScope tran = new TransactionScope())
            {
                try
                {
                    creditNotePriceNew.Save();
                    invoice.UpdateInvoice();

                    bhCancellCharge.Save();
                    bhRefund.Save();

                    creditNoteLineItem.Price = creditNotePriceNew;
                    creditNoteLineItem.ItemDescription = remarks;
                    creditNoteLineItem.Update();
                    cCharge.Update();

                    #region Making entry in payment and paymentInvoiceTable and update ledgerTransaction
                    pd.Save();
                    if (ic.Amount < 0)
                    {
                        PaymentDetails.UpdateInvoicePaymentStatus(invoice.InvoiceNumber, true);
                    }
                    ledgerTxnRefund.Save();
                    ledgerTxnCancelCharge.Save();
                    #endregion

                    #region Update Ledger's Current Balance
                    if (!isLCC)
                    {
                        Ledger.UpdateCurrentBalance(invoice.AgencyId, amountToBeRefund - refundedAmount, loggedMemberId);
                    }
                    else
                    {
                        Agency.UpdateLCCBalance(invoice.AgencyId, amountToBeRefund - refundedAmount, loggedMemberId);
                    }
                    ServiceRequest.UpdateCancellationCharge(ticketId, adminFee, suppFee);
                    #endregion
                    tran.Complete();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Account, Severity.High, 0, "Fail to process AccountUtility.EditCreditNote: " + ex.Message, "");
                }
            }
        }
        public static void EditOfflineCreditNote(PriceAccounts creditNotePriceNew, decimal adminFee, decimal suppFee, int offlineBookingId, int offlineBookingPriceId, bool isLCC, int creditNoteNumber, InvoiceLineItem creditNoteLineItem, int loggedMemberId, string remarks)
        {
            Member member = new Member(loggedMemberId);
            creditNoteLineItem.CreatedBy = loggedMemberId;
            CancellationCharges cCharge = new CancellationCharges();
            cCharge.Load(offlineBookingId, ProductType.Flight, InvoiceItemTypeId.Offline);

            BookingHistory bhRefund = new BookingHistory();
            BookingHistory bhCancellCharge = new BookingHistory();

            bhRefund.BookingId = offlineBookingId;
            bhRefund.CreatedBy = loggedMemberId;
            bhRefund.EventCategory = EventCategory.Refund;

            bhCancellCharge.BookingId = offlineBookingId;
            bhCancellCharge.CreatedBy = loggedMemberId;
            bhCancellCharge.EventCategory = EventCategory.Refund;

            LedgerTransaction ledgerTxnRefund = new LedgerTransaction();
            ledgerTxnRefund.LoadByReferenceId(cCharge.PaymentDetailId, (int)ReferenceType.OfflineRefund);
            LedgerTransaction ledgerTxnCancelCharge = new LedgerTransaction();
            ledgerTxnCancelCharge.LoadByReferenceId(cCharge.PaymentDetailId, (int)ReferenceType.OfflineCancellationCharge);

            ledgerTxnCancelCharge.Amount = -(adminFee + suppFee);
            //ledgerTxnRefund.Narration.
            Invoice invoice = new Invoice();
            invoice.Load(Invoice.GetInvoiceNumberByOfflineBookingId(offlineBookingId));
            int agencyTypeId = Agency.GetAgencyTypeId(invoice.AgencyId);
            decimal amountToBeRefund = ((int)Agencytype.Service == agencyTypeId) ? creditNotePriceNew.GetServiceAgentPrice() - (adminFee + suppFee) : creditNotePriceNew.GetAgentPrice() - (adminFee + suppFee);
            decimal refundedAmount = ((int)Agencytype.Service == agencyTypeId) ? creditNoteLineItem.Price.GetServiceAgentPrice() - (cCharge.AdminFee + cCharge.SupplierFee) : creditNoteLineItem.Price.GetAgentPrice() - (cCharge.AdminFee + cCharge.SupplierFee);
            decimal invoiceDueAmount = 0;


            //To check that is creditnote using ticket price or its own price
            if (offlineBookingPriceId != creditNoteLineItem.Price.PriceId)
            {
                creditNotePriceNew.PriceId = creditNoteLineItem.Price.PriceId;
            }

            decimal originalAmount = cCharge.AdminFee + cCharge.SupplierFee;
            decimal editedAmount = adminFee + suppFee;
            bhCancellCharge.Remarks = "Cancellation charge edited on " + DateTime.Now.ToString("dd/MM/yyyy") +
                " by " + member.FullName + " from " + originalAmount.ToString("N2") + " to " + editedAmount.ToString("N2") + ": " + remarks;

            originalAmount = ledgerTxnRefund.Amount + creditNotePriceNew.OtherCharges;
            editedAmount = ((int)Agencytype.Service == agencyTypeId) ? creditNotePriceNew.GetServiceAgentPrice() : creditNotePriceNew.GetAgentPrice();
            bhRefund.Remarks = "Ticket amount for refund edited on " + DateTime.Now.ToString("dd/MM/yyyy") +
                " by " + member.FullName + " from " + originalAmount.ToString("N2") + " to " + editedAmount.ToString("N2") + ": " + remarks;

            //Invoice invoice = new Invoice();
            PaymentDetails pd = new PaymentDetails();
            InvoiceCollection ic = new InvoiceCollection();
            string[] crNo = Invoice.GetCreditNoteNumberStringByOfflineBookingId(offlineBookingId);

            pd.Remarks = "Payment from edit creditNote. CreditNoteNo: " + crNo[0] + crNo[1];
            pd.Status = PaymentStatus.Accepted;
            pd.ModeOfPayment = PaymentMode.EditCreditNote;
            pd.ReferenceNo = creditNoteNumber;
            pd.CreatedBy = loggedMemberId;
            pd.PaymentDate = DateTime.Now.ToUniversalTime();

            // invoice.Load(Invoice.GetInvoiceNumberByOfflineBookingId(offlineBookingId));
            pd.AgencyId = invoice.AgencyId;
            if (Agency.GetAgencyTypeId(invoice.AgencyId) == (int)Agencytype.Cash || Agency.GetAgencyTypeId(invoice.AgencyId) == (int)Agencytype.Service)
            {
                isLCC = true;
            }

            pd.IsLCC = isLCC;

            if (amountToBeRefund > refundedAmount)
            {
                pd.Amount = amountToBeRefund - refundedAmount;
                if (invoice.Status == InvoiceStatus.Paid || isLCC)
                {
                    pd.RemainingAmount = pd.Amount;
                    pd.Remarks = "Payment from edit creditNote. CreditNoteNo: " + crNo[0] + crNo[1];
                }
                else
                {
                    invoiceDueAmount = invoice.TotalPrice - Invoice.GetPaymentDoneAgainstInvoice(invoice.InvoiceNumber);

                    if ((amountToBeRefund - refundedAmount) >= invoiceDueAmount)
                    {
                        invoice.Status = InvoiceStatus.Paid;
                        pd.Amount = amountToBeRefund - refundedAmount;
                        pd.RemainingAmount = pd.Amount - invoiceDueAmount;

                        ic.Amount = invoiceDueAmount;
                        ic.InvoiceNumber = invoice.InvoiceNumber;
                        ic.IsPartial = false;
                    }
                    else if ((amountToBeRefund - refundedAmount) < invoiceDueAmount)
                    {
                        invoice.Status = InvoiceStatus.PartiallyPaid;
                        pd.RemainingAmount = 0;

                        ic.Amount = amountToBeRefund - refundedAmount;
                        ic.InvoiceNumber = invoice.InvoiceNumber;
                        ic.IsPartial = true;
                    }
                }
            }
            else if (amountToBeRefund < refundedAmount)
            {
                pd.Amount = amountToBeRefund - refundedAmount;
                if (!isLCC)
                {
                    invoice.Status = InvoiceStatus.PartiallyPaid;
                    pd.RemainingAmount = 0;

                    ic.Amount = amountToBeRefund - refundedAmount;
                    ic.InvoiceNumber = invoice.InvoiceNumber;
                    ic.IsPartial = true;
                }
                else
                {
                    pd.RemainingAmount = amountToBeRefund - refundedAmount;
                }
            }

            pd.Invoices = new List<InvoiceCollection>();
            if (ic.Amount != 0)
            {
                pd.Invoices.Add(ic);
            }
            cCharge.AdminFee = adminFee;
            cCharge.SupplierFee = suppFee;
            ledgerTxnRefund.Amount = ((int)Agencytype.Service == Agency.GetAgencyTypeId(invoice.AgencyId)) ? creditNotePriceNew.GetServiceAgentPrice() - creditNotePriceNew.OtherCharges : creditNotePriceNew.GetAgentPrice() - creditNotePriceNew.OtherCharges;

            //ledgerTxnRefund.Narration = nbRefund;
            //ledgerTxnCancelCharge.Narration = nbCancellCharge;

            using (System.Transactions.TransactionScope tran = new TransactionScope())
            {
                try
                {
                    creditNotePriceNew.Save();
                    invoice.UpdateInvoice();
                    creditNoteLineItem.Price = creditNotePriceNew;
                    creditNoteLineItem.ItemDescription = remarks;
                    creditNoteLineItem.Update();
                    cCharge.Update();

                    bhCancellCharge.Save();
                    bhRefund.Save();

                    #region Making entry in payment and paymentInvoiceTable and update ledgerTransaction
                    pd.Save();
                    ledgerTxnRefund.Save();
                    ledgerTxnCancelCharge.Save();
                    #endregion

                    #region Update Ledger's Current Balance
                    if (!isLCC)
                    {
                        Ledger.UpdateCurrentBalance(invoice.AgencyId, amountToBeRefund - refundedAmount, loggedMemberId);
                    }
                    else
                    {
                        Agency.UpdateLCCBalance(invoice.AgencyId, amountToBeRefund - refundedAmount, loggedMemberId);
                    }
                    #endregion
                    tran.Complete();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Account, Severity.High, 0, "Fail to process AccountUtility.EditCreditNote: " + ex.Message, "");
                }
            }
        }
    }
}