using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using CT.Configuration;
using System.Collections;
using CT.TicketReceipt.BusinessLayer;
using System.Data;


namespace CT.AccountingEngine
{
    /// <summary>
    /// It just calculate the data of accounts detail
    /// </summary>
    public class AccountingEngine
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public AccountingEngine()
        {
        }
        /// <summary>
        /// Method get the price object contains everything from itinerary
        /// </summary>
        /// <param name="itinerary">itineray</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="paxIndex">paxIndex i.e. which passenger is being processed currently</param>
        /// <param name="otherCharges">other charges</param>
        /// <returns>Price object contains all the values of properties</returns>
        public static PriceAccounts GetPrice(FlightItinerary itinerary, int agencyId, int paxIndex, decimal otherCharges) 
        {
            #region Gets isDomestic from itinerary
            PriceAccounts price = new PriceAccounts();
            price.CommissionType = "RB";
            bool isDomestic = true;
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {

                if (itinerary.Segments[i] != null)
                {
                    if (itinerary.Segments[i].Origin != null && itinerary.Segments[i].Destination != null)
                    {
                    if (itinerary.Segments[i].Origin.CountryCode != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "" || itinerary.Segments[i].Destination.CountryCode != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "")
                        {
                            isDomestic = false;
                            break;
                        }
                    }
                }
            }
            string bookingClass = itinerary.Segments[0].BookingClass;
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                if (bookingClass.IndexOf(itinerary.Segments[i].BookingClass) < 0)
                {
                    bookingClass += "," + itinerary.Segments[i].BookingClass;
                }
            }

            #endregion
            if (isDomestic)
            {
                //price = DomesticPricing.GetPrice(itinerary, agencyId, paxIndex, otherCharges);
            }
            else
            {
                UserPreference preference = new UserPreference();
                //int memberid = Member.GetPrimaryMemberId(agencyId);
                int memberid = agencyId;// (int)Settings.LoginInfo.AgentId;
                preference = preference.GetPreference(memberid, preference.CurrencyPref, preference.SearchResult);
                Dictionary<string, string> prefPair = new Dictionary<string, string>();
                prefPair = UserPreference.GetPreferenceList(memberid, ItemType.Flight);
                Dictionary<string, decimal> rateList = new Dictionary<string, decimal>();
                string currencyPref = "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "";
                if (prefPair.ContainsKey(preference.CurrencyPref))
                {
                    currencyPref = prefPair[preference.CurrencyPref];
                    if (currencyPref.Equals("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "", StringComparison.OrdinalIgnoreCase) != true)
                    {
                        StaticData staticInfo = new StaticData();
                        rateList = staticInfo.CurrencyROE;
                        price.RateOfExchange = rateList[currencyPref];
                    }
                }
                price.Currency = currencyPref;

                decimal grossFare = itinerary.Passenger[paxIndex].Price.PublishedFare;
                decimal sotoOtherCharge = 0;
                bool isGross = true;
                bool isReturn = false;

                #region Gets AirlineCode from itinerary
                string airlineCode = itinerary.Segments[0].Airline;
                int dnSegements = 0;
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    if (itinerary.Segments[i] != null)
                    {
                        if (airlineCode != itinerary.Segments[i].Airline)
                        {
                            //## denotes multiple airlines
                            airlineCode = "##";
                            break;
                        }
                        if ((airlineCode == "IT") && ((Convert.ToInt32(itinerary.Segments[i].FlightNumber) >= Convert.ToInt32(ConfigurationSystem.AirDeccanConfig["StartRangeDnIt"])) && (Convert.ToInt32(itinerary.Segments[i].FlightNumber) <= Convert.ToInt32(ConfigurationSystem.AirDeccanConfig["EndRangeDnIt"]))))
                        {
                            dnSegements++;
                        }
                    }
                }
                if (dnSegements == itinerary.Segments.Length)
                {
                    airlineCode = "DN";
                }
                #endregion

                if (itinerary.Origin == itinerary.Destination)
                {
                    isReturn = false;
                }
                else
                {
                    if (itinerary.Segments[0].Origin.CityCode == itinerary.Segments[itinerary.Segments.Length - 1].Destination.CityCode)
                    {
                        isReturn = true;
                    }
                }


                #region Get origin and destination
                string origin = itinerary.Segments[0].Origin.AirportCode;
                int len = itinerary.Segments.Length - 1;
                string destAirport = string.Empty;
                destAirport = itinerary.Segments[len].Destination.AirportCode;

                #endregion
                bool isSoto = IsSOTO(itinerary.Segments[0].Origin.CountryCode, itinerary.Segments[len].Destination.CountryCode, itinerary);
                if (isSoto)
                {
                    ConfigurationSystem con = new ConfigurationSystem();
                    Hashtable accountingConfig = con.GetSelfAgencyId();
                    sotoOtherCharge = Convert.ToDecimal(accountingConfig["sotoOtherCharge"]);
                }
                if (isGross)
                {
                    #region When Gross Fare is given
                    //TODO: add detailed commment with examples here
                    price.PublishedFare = grossFare;
                    price.NetFare = 0;
                    price.Markup = 0;

                    #region Get Airline Transaction Fee - using Itinerary
                    price.AirlineTransFee = itinerary.Passenger[paxIndex].Price.AirlineTransFee;
                    #endregion

                    if (airlineCode != "##")
                    {
                        Airline airline = new Airline();
                        airline.Load(airlineCode);
                        price.OtherCharges = Math.Round((otherCharges + airline.Surcharge + sotoOtherCharge),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                        decimal fuelSurcharge = 0; // Util.GetFuelSurcharge(airline.AirlineCode, "", "");
                        if (isReturn)
                        {
                            fuelSurcharge = fuelSurcharge * 2;
                        }
                        price.TransactionFee = Math.Round((((grossFare + fuelSurcharge) * Convert.ToDecimal(airline.TransactionFee)) / 100),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    }
                    else
                    {
                        price.OtherCharges = Math.Round((otherCharges + sotoOtherCharge),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    }
                    //price.OtherCharges += Convert.ToDecimal(ConfigurationSystem.BookingEngineConfig["InternationalMiscCharges"]); 
                    if (isSoto)
                    {
                        //Agent commmission
                        price.AgentCommission = 0;
                        //Our Commission
                        price.OurCommission = 0;
                        price.AgentPLB = 0;
                        price.OurPLB = 0;
                    }
                    else
                    {
                        //Agent commmission
                        AgentMaster agency = new AgentMaster(agencyId);
                        //if (!agency.IsCorporate)
                        bool IsCorporate = false;
                        if (IsCorporate)
                        {
                            price.AgentCommission = CalculateCommissionAgent(grossFare, agencyId, airlineCode);
                            if (airlineCode == "IX")
                            {
                                if (isReturn)
                                {
                                    price.AgentCommission = 200;
                                }
                                else
                                {
                                    price.AgentCommission = 100;
                                }
                            }
                        }
                        if (airlineCode == "G9")
                        {
                            if (isReturn)
                            {
                                price.OtherCharges = Convert.ToDecimal(CT.Configuration.ConfigurationSystem.AirArabiaConfig["ReturnServiceCharge"]);
                            }
                            else
                            {
                                price.OtherCharges = Convert.ToDecimal(CT.Configuration.ConfigurationSystem.AirArabiaConfig["OneWayServiceCharge"]);
                            }
                            price.AgentCommission = 0;
                        }
                        //Our Commission
                        price.OurCommission = GetOurCommission(grossFare, airlineCode);
                        //Net Fare
                        decimal nFare = price.PublishedFare - price.OurCommission;
                        //PLB Agent
                        //if (!agency.IsCorporate)
                        if (IsCorporate)
                        {
                            price.AgentPLB = CalculatePLBAgent(price.PublishedFare - price.AgentCommission, agencyId, airlineCode, FlightPassenger.GetPTC(itinerary.Passenger[paxIndex].Type), bookingClass, itinerary.Segments[0].DepartureTime, origin, destAirport);
                        }
                        price.OurPLB = CalculateOurPLB(nFare, airlineCode, FlightPassenger.GetPTC(itinerary.Passenger[paxIndex].Type), Convert.ToString(itinerary.Segments[0].BookingClass), itinerary.Segments[0].DepartureTime, origin, destAirport);
                    }
                    decimal tdsrate = 0.0M;
                    //price.TdsCommission =DomesticPricing.CalculateTDSAndTDSRate(price.AgentCommission, agencyId, isDomestic,out tdsrate);
                    price.TdsRate = tdsrate;
                    price.TDSPLB = 0;
                  
                    //price.TDSPLB = CalculateTDS(price.AgentPLB, agencyId, isDomestic);

                    #endregion
                }
                else
                {
                    //TODO: Securate case is not correctly dealt bcoz of no example
                    #region Securate Case
                    price.PublishedFare = grossFare;
                    price.NetFare = grossFare;
                    price.Markup = GetMarkup(grossFare);
                    price.OurCommission = 0;
                    price.OtherCharges = otherCharges;
                    price.AgentPLB = CalculatePLBAgent(grossFare, agencyId, airlineCode);
                    price.OurPLB = CalculateOurPLB(grossFare, airlineCode);

                    price.AgentCommission = 0;
                    decimal tdsrate = 0.0M;
                    //price.TdsCommission = DomesticPricing.CalculateTDSAndTDSRate(price.AgentCommission, agencyId, isDomestic,out tdsrate);
                    price.TdsRate = tdsrate;
                    price.TDSPLB = 0;
                    //price.TDSPLB = CalculateTDS(price.AgentPLB, agencyId, isDomestic);

                    #endregion
                }

                ////Service Tax on gross Fare
                //price.SeviceTax = CalculateServiceTax(grossFare, isDomestic);
                //price.AdditionalTxnFee = itinerary.Passenger[paxIndex].Price.AdditionalTxnFee;
                //price.Tax = itinerary.Passenger[paxIndex].Price.Tax;
                ////price.Currency = itinerary.Passenger[paxIndex].Price.Currency;
                ////Region For Charge Break up of Other Charges
                //if (price.OtherCharges > 0)
                //{
                //    ChargeBreakUp cbu = new ChargeBreakUp();
                //    cbu.PriceId = price.PriceId;
                //    cbu.ChargeType = ChargeType.OtherCharges;
                //    cbu.Amount = price.OtherCharges;
                //    price.ChargeBU = new List<ChargeBreakUp>();
                //    price.ChargeBU.Add(cbu);

                //}
            }
            return price;
        }

        /// <summary>
        /// Method get the price object contains everything from result object
        /// </summary>
        /// <param name="result">Result object</param>
        /// <param name="fareBreakdownIndex">fareBreakdown index means which passenger is being processed(-1 when all are being processed)</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="otherCharges">other charges</param>
        /// <returns>Price object contains all the values of properties</returns>
        public static PriceAccounts GetPrice(SearchResult result, int fareBreakdownIndex, int agencyId, decimal otherCharges)
        {
            PriceAccounts price = new PriceAccounts();
            price.CommissionType = "RB";
            #region Gets isDomestic from result
            bool isDomestic = true;
            string origin = result.Flights[0][0].Origin.CountryCode;
            int len = result.Flights[0].Length - 1;
            string dest = string.Empty;
            string destAirport = string.Empty;
            dest = result.Flights[0][len].Destination.CountryCode;
            destAirport = result.Flights[0][len].Destination.AirportCode;
            if (origin != dest || origin != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "")
            {
                isDomestic = false;
            }
            string bookingClass = result.Flights[0][0].BookingClass;
            for (int g = 0; g < result.Flights.Length; g++)
            {
                for (int f = 0; f < result.Flights[g].Length; f++)
                {
                    if (bookingClass.IndexOf(result.Flights[g][f].BookingClass) < 0)
                    {
                        bookingClass += "," + result.Flights[g][f].BookingClass;
                    }
                }
            }
            #endregion

            if (isDomestic)
            {
               // price = DomesticPricing.GetPrice(result, fareBreakdownIndex, agencyId, otherCharges);
            }
            else
            {
                decimal grossFare;
                decimal airlineTxnFee;
                int paxCount = 1;
                int seatCount = 1;
                decimal sotoOtherCharge = 0;
                bool isReturn = false;
                //Agency agency = new Agency(agencyId);
                AgentMaster agency = new AgentMaster(agencyId);
               
                UserPreference preference = new UserPreference();
                //int memberid = Member.GetPrimaryMemberId(agencyId);
                int memberid = agencyId; //(int)Settings.LoginInfo.AgentId;
                preference = preference.GetPreference(memberid, preference.CurrencyPref, preference.SearchResult);
                Dictionary<string, string> prefPair = new Dictionary<string, string>();
                prefPair = UserPreference.GetPreferenceList(memberid, ItemType.Flight);
                Dictionary<string, decimal> rateList = new Dictionary<string, decimal>();
                string currencyPref = "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "";
                if (prefPair.ContainsKey(preference.CurrencyPref))
                {
                    currencyPref = prefPair[preference.CurrencyPref];
                    if (currencyPref.Equals("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "", StringComparison.OrdinalIgnoreCase) != true)
                    {
                        StaticData staticInfo = new StaticData();
                        rateList = staticInfo.CurrencyROE;
                        price.RateOfExchange = rateList[currencyPref];
                    }
                }
                price.Currency = currencyPref;

                if (result.Flights.Length > 1)
                {
                    isReturn = true;
                }
                //this case is when we calculate full fare in place of individual farebreakdown
                if (fareBreakdownIndex == -1)
                {
                    grossFare = Convert.ToDecimal(result.BaseFare);
                    paxCount = 0;
                    seatCount = 0;
                    airlineTxnFee = 0;
                    for (int i = 0; i < result.FareBreakdown.Length; i++)
                    {
                        if (result.FareBreakdown[i].PassengerType != PassengerType.Infant)
                        {
                            seatCount += result.FareBreakdown[i].PassengerCount;
                        }
                        paxCount += result.FareBreakdown[i].PassengerCount;
                        airlineTxnFee += result.FareBreakdown[i].AirlineTransFee;
                        price.AdditionalTxnFee += result.FareBreakdown[i].AdditionalTxnFee;
                    }
                }
                else
                {
                    grossFare = Convert.ToDecimal(result.FareBreakdown[fareBreakdownIndex].BaseFare / result.FareBreakdown[fareBreakdownIndex].PassengerCount);
                    airlineTxnFee = result.FareBreakdown[fareBreakdownIndex].AirlineTransFee / result.FareBreakdown[fareBreakdownIndex].PassengerCount;
                    price.AdditionalTxnFee = result.FareBreakdown[fareBreakdownIndex].AdditionalTxnFee / result.FareBreakdown[fareBreakdownIndex].PassengerCount;
                }
                bool isGross = true;

                #region Gets AirlineCode from itinerary
                string airlineCode = result.Flights[0][0].Airline;
                int dnSegements = 0;
                int noOfSegements = 0;
                for (int g = 0; g < result.Flights.Length; g++)
                {
                    for (int f = 0; f < result.Flights[g].Length; f++)
                    {
                        if (airlineCode != result.Flights[g][f].Airline)
                        {
                            airlineCode = "##";
                            break;
                        }
                        if ((airlineCode == "IT") && ((Convert.ToInt32(result.Flights[g][f].FlightNumber) >= Convert.ToInt32(ConfigurationSystem.AirDeccanConfig["StartRangeDnIt"])) && (Convert.ToInt32(result.Flights[g][f].FlightNumber) <= Convert.ToInt32(ConfigurationSystem.AirDeccanConfig["EndRangeDnIt"]))))
                        {
                            dnSegements++;
                        }
                    }
                    noOfSegements += result.Flights[g].Length;
                }
                if (dnSegements == noOfSegements)
                {
                    airlineCode = "DN";
                }
                #endregion


               // bool isSoto = IsSOTO(origin, dest, result);
                bool isSoto = false;
                if (isSoto)
                {
                    ConfigurationSystem con = new ConfigurationSystem();
                    Hashtable accountingConfig = con.GetSelfAgencyId();
                    sotoOtherCharge = Convert.ToDecimal(accountingConfig["sotoOtherCharge"]);
                }
                if (isGross)
                {
                    #region When Gross Fare is given
                    //TODO: add detailed commment with examples here
                    // grossFare = Math.Round(grossFare);
                    price.PublishedFare = grossFare;
                    price.NetFare = 0;
                    price.Markup = 0;

                    #region Get Airline Transaction Fee - Using SearchResult
                    if (fareBreakdownIndex == -1)
                    {
                        decimal transFee = 0;
                        for (int i = 0; i < result.FareBreakdown.Length; i++)
                        {
                            transFee += result.FareBreakdown[i].AirlineTransFee;
                        }
                        price.AirlineTransFee = transFee;
                    }
                    else
                    {
                        price.AirlineTransFee = result.FareBreakdown[fareBreakdownIndex].AirlineTransFee / result.FareBreakdown[fareBreakdownIndex].PassengerCount;
                    }
                    #endregion

                    if (airlineCode != "##")
                    {
                        Airline airline = new Airline();
                        airline.Load(airlineCode);
                        price.OtherCharges = Math.Round(((otherCharges + airline.Surcharge + sotoOtherCharge)),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )) * paxCount;
                        decimal fuelSurcharge = Util.GetFuelSurcharge(airline.AirlineCode, "", "") * paxCount;
                        if (isReturn)
                        {
                            fuelSurcharge = fuelSurcharge * 2;
                        }
                        price.TransactionFee = Math.Round((((grossFare + fuelSurcharge) * Convert.ToDecimal(airline.TransactionFee)) / 100),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    }
                    else
                    {
                        price.OtherCharges = Math.Round((otherCharges + sotoOtherCharge),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )) * paxCount;
                    }
                    if (isSoto)
                    {
                        //Agent commmission
                        price.AgentCommission = 0;
                        //Our Commission
                        price.OurCommission = 0;
                    }
                    else
                    {
                        //if (!agency.IsCorporate)
                        //{
                        //    price.AgentCommission = CalculateCommissionAgent(grossFare, agencyId, airlineCode);

                        //    if (airlineCode == "IX")
                        //    {
                        //        if (isReturn)
                        //        {
                        //            price.AgentCommission = 200 * seatCount;
                        //        }
                        //        else
                        //        {
                        //            price.AgentCommission = 100 * seatCount;
                        //        }
                        //    }
                        //}
                        if (airlineCode == "G9")
                        {
                            if (isReturn)
                            {
                                price.OtherCharges = Convert.ToDecimal(CT.Configuration.ConfigurationSystem.AirArabiaConfig["ReturnServiceCharge"]) * paxCount;
                            }
                            else
                            {
                                price.OtherCharges = Convert.ToDecimal(CT.Configuration.ConfigurationSystem.AirArabiaConfig["OneWayServiceCharge"]) * paxCount;
                            }
                            price.AgentCommission = 0;
                        }
                        //Our Commission
                        price.OurCommission = GetOurCommission(grossFare, airlineCode);
                    }
                    //Net Fare
                    decimal nFare = Math.Round((price.PublishedFare - price.OurCommission),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    if (isSoto)
                    {
                        //Agent PLB
                        price.AgentPLB = 0;
                        //Our PLB
                        price.OurPLB = 0;
                    }
                    else
                    {
                        //PLB Agent
                        //fareBreakdownIndex = -1 means when we calculate plb on full price(for all passengers)
                        if (fareBreakdownIndex == -1)
                        {
                            for (int i = 0; i < result.FareBreakdown.Length; i++)
                            {
                                //if (!agency.IsCorporate)
                                //{
                                //    price.AgentPLB += CalculatePLBAgent(price.PublishedFare - price.AgentCommission, agencyId, airlineCode, FlightPassenger.GetPTC(result.FareBreakdown[i].PassengerType), bookingClass, result.Flights[0][0].DepartureTime, result.Flights[0][0].Origin.AirportCode, destAirport);
                                //}
                                price.OurPLB += CalculateOurPLB(nFare, airlineCode, FlightPassenger.GetPTC(result.FareBreakdown[i].PassengerType), Convert.ToString(result.Flights[0][0].BookingClass), result.Flights[0][0].DepartureTime, result.Flights[0][0].Origin.AirportCode, destAirport);
                            }
                            price.AgentPLB = Math.Round((price.AgentPLB / (result.FareBreakdown.Length)), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                            price.OurPLB = Math.Round((price.OurPLB / (result.FareBreakdown.Length)), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                        }
                        else
                        {
                            //if (!agency.IsCorporate)
                            //{
                            //    price.AgentPLB = CalculatePLBAgent(price.PublishedFare - price.AgentCommission, agencyId, airlineCode, FlightPassenger.GetPTC(result.FareBreakdown[fareBreakdownIndex].PassengerType), bookingClass, result.Flights[0][0].DepartureTime, result.Flights[0][0].Origin.AirportCode, destAirport);
                            //}
                            price.OurPLB = CalculateOurPLB(nFare, airlineCode, FlightPassenger.GetPTC(result.FareBreakdown[fareBreakdownIndex].PassengerType), Convert.ToString(result.Flights[0][0].BookingClass), result.Flights[0][0].DepartureTime, result.Flights[0][0].Origin.AirportCode, destAirport);
                        }
                    }
                    decimal tdsrate = 0.0M;
                   // price.TdsCommission = DomesticPricing.CalculateTDSAndTDSRate(price.AgentCommission, agencyId, isDomestic,out tdsrate);
                    price.TdsCommission = 0;
                    price.TdsRate = tdsrate;
                    price.TDSPLB = 0;
                    //price.TDSPLB = CalculateTDS(price.AgentPLB, agencyId, isDomestic);
                    #endregion
                }
                else
                {
                    //TODO: Securate case is not correctly dealt bcoz of no example
                    #region Securate Case
                    price.PublishedFare = grossFare;
                    price.NetFare = grossFare;
                    price.Markup = GetMarkup(grossFare);
                    price.OurCommission = 0;
                    price.OtherCharges = otherCharges;
                    price.AgentPLB = CalculatePLBAgent(grossFare, agencyId, airlineCode);
                    price.OurPLB = CalculateOurPLB(grossFare, airlineCode);
                    price.AgentCommission = 0;
                    decimal tdsRate = 0.0M;
                    //price.TdsCommission = DomesticPricing.CalculateTDSAndTDSRate(price.AgentCommission, agencyId, isDomestic,out tdsRate);
                    price.TdsCommission = 0;
                    price.TdsRate = tdsRate;
                    price.TDSPLB = 0;
                    //price.TDSPLB = CalculateTDS(price.AgentPLB, agencyId, isDomestic);

                    #endregion
                }

                //Service Tax on gross Fare
                price.SeviceTax = CalculateServiceTax(grossFare, isDomestic);

                if (fareBreakdownIndex == -1)
                {
                    price.Tax = Convert.ToDecimal(result.Tax);
                }
                else
                {
                    price.Tax = (Convert.ToDecimal(result.FareBreakdown[fareBreakdownIndex].TotalFare) - Convert.ToDecimal(result.FareBreakdown[fareBreakdownIndex].BaseFare)) / result.FareBreakdown[fareBreakdownIndex].PassengerCount;
                }
               // price.Currency = result.Currency;  // Commented as currency is taken from user preference

                //Region for charge break up of other charges
                if (price.OtherCharges > 0)
                {
                    ChargeBreakUp cbu = new ChargeBreakUp();
                    cbu.PriceId = price.PriceId;
                    cbu.ChargeType = ChargeType.OtherCharges;
                    cbu.Amount = price.OtherCharges;
                    price.ChargeBU = new List<ChargeBreakUp>();
                    price.ChargeBU.Add(cbu);
                }

                // Agent Markup calculation.
                
            }
            return price;
        }

        /// <summary>
        /// This method will only B2B and B2C Markup if transType=B2C  and will be called from BookingAPI.
        /// </summary>
        /// <param name="result">Result object</param>
        /// <param name="fareBreakdownIndex">fareBreakdown index means which passenger is being processed(-1 when all are being processed)</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="otherCharges">other charges</param>
        /// <param name="productType">productType</param>
        /// <param name="transType">transType(B2C/B2B)</param>
        /// <returns>Price object contains all the values of properties</returns>
        public static PriceAccounts GetPrice(SearchResult result, int fareBreakdownIndex, int agencyId, decimal otherCharges, int productType, string transType)
        {
            PriceAccounts price = result.Price;
            price.CommissionType = "RB";
            #region Gets isDomestic from result
            //bool isDomestic = true;
            string origin = result.Flights[0][0].Origin.CountryCode;
            int len = result.Flights[0].Length - 1;
            string dest = string.Empty;
            string destAirport = string.Empty;
            dest = result.Flights[0][len].Destination.CountryCode;
            destAirport = result.Flights[0][len].Destination.AirportCode;
            //if (origin != dest || origin != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "")
            //{
            //    isDomestic = false;
            //}
            string bookingClass = result.Flights[0][0].BookingClass;
            for (int g = 0; g < result.Flights.Length; g++)
            {
                for (int f = 0; f < result.Flights[g].Length; f++)
                {
                    if (bookingClass.IndexOf(result.Flights[g][f].BookingClass) < 0)
                    {
                        bookingClass += "," + result.Flights[g][f].BookingClass;
                    }
                }
            }
            #endregion

            //if (isDomestic)
            //{
            //    // price = DomesticPricing.GetPrice(result, fareBreakdownIndex, agencyId, otherCharges);
            //}
            //else
            // {
            decimal grossFare;
            decimal airlineTxnFee;
            int paxCount = 1;
            int seatCount = 1;
            decimal sotoOtherCharge = 0;
            //bool isReturn = false;
            //Agency agency = new Agency(agencyId);
            AgentMaster agency = new AgentMaster(agencyId);

            UserPreference preference = new UserPreference();
            //int memberid = Member.GetPrimaryMemberId(agencyId);
            int memberid = agencyId;
            //preference = preference.GetPreference(memberid, preference.CurrencyPref, preference.SearchResult);
            //Dictionary<string, string> prefPair = new Dictionary<string, string>();
            //prefPair = UserPreference.GetPreferenceList(memberid, ItemType.Flight);
            //Dictionary<string, decimal> rateList = new Dictionary<string, decimal>();
            //string currencyPref = "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "";
            //if (prefPair.ContainsKey(preference.CurrencyPref))
            //{
            //    currencyPref = prefPair[preference.CurrencyPref];
            //    if (currencyPref.Equals("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "", StringComparison.OrdinalIgnoreCase) != true)
            //    {
            //        StaticData staticInfo = new StaticData();
            //        rateList = staticInfo.CurrencyROE;
            //        price.RateOfExchange = rateList[currencyPref];
            //    }
            //}
            price.Currency = result.Currency;//Agent Currency will be applied from MSE

            //if (result.Flights.Length > 1)
            //{
            //    isReturn = true;
            //}
            //this case is when we calculate full fare in place of individual farebreakdown
            if (fareBreakdownIndex == -1)
            {
                grossFare = Convert.ToDecimal(result.BaseFare);
                paxCount = 0;
                seatCount = 0;
                airlineTxnFee = 0;
                for (int i = 0; i < result.FareBreakdown.Length; i++)
                {
                    if (result.FareBreakdown[i].PassengerType != PassengerType.Infant)
                    {
                        seatCount += result.FareBreakdown[i].PassengerCount;
                    }
                    paxCount += result.FareBreakdown[i].PassengerCount;
                    airlineTxnFee += result.FareBreakdown[i].AirlineTransFee;
                    price.AdditionalTxnFee += result.FareBreakdown[i].AdditionalTxnFee;
                }
            }
            else
            {
                grossFare = Convert.ToDecimal(result.FareBreakdown[fareBreakdownIndex].BaseFare / result.FareBreakdown[fareBreakdownIndex].PassengerCount);
                airlineTxnFee = result.FareBreakdown[fareBreakdownIndex].AirlineTransFee / result.FareBreakdown[fareBreakdownIndex].PassengerCount;
                //price.AdditionalTxnFee = result.FareBreakdown[fareBreakdownIndex].AdditionalTxnFee / result.FareBreakdown[fareBreakdownIndex].PassengerCount;
            }
            bool isGross = true;

            #region Gets AirlineCode from itinerary
            string airlineCode = result.Flights[0][0].Airline;
            int dnSegements = 0;
            int noOfSegements = 0;
            for (int g = 0; g < result.Flights.Length; g++)
            {
                for (int f = 0; f < result.Flights[g].Length; f++)
                {
                    if (airlineCode != result.Flights[g][f].Airline)
                    {
                        airlineCode = "##";
                        break;
                    }
                    if ((airlineCode == "IT") && ((Convert.ToInt32(result.Flights[g][f].FlightNumber) >= Convert.ToInt32(ConfigurationSystem.AirDeccanConfig["StartRangeDnIt"])) && (Convert.ToInt32(result.Flights[g][f].FlightNumber) <= Convert.ToInt32(ConfigurationSystem.AirDeccanConfig["EndRangeDnIt"]))))
                    {
                        dnSegements++;
                    }
                }
                noOfSegements += result.Flights[g].Length;
            }
            if (dnSegements == noOfSegements)
            {
                airlineCode = "DN";
            }
            #endregion


            // bool isSoto = IsSOTO(origin, dest, result);
            bool isSoto = false;
            if (isSoto)
            {
                ConfigurationSystem con = new ConfigurationSystem();
                Hashtable accountingConfig = con.GetSelfAgencyId();
                sotoOtherCharge = Convert.ToDecimal(accountingConfig["sotoOtherCharge"]);
            }
            if (isGross)
            {
                #region When Gross Fare is given
                //TODO: add detailed commment with examples here
                // grossFare = Math.Round(grossFare);
                price.PublishedFare = grossFare;
                price.NetFare = 0;
                price.Markup = 0;

                #region Get Airline Transaction Fee - Using SearchResult
                if (fareBreakdownIndex == -1)
                {
                    decimal transFee = 0;
                    for (int i = 0; i < result.FareBreakdown.Length; i++)
                    {
                        transFee += result.FareBreakdown[i].AirlineTransFee;
                    }
                    price.AirlineTransFee = transFee;
                }
                else
                {
                    //price.AirlineTransFee = result.FareBreakdown[fareBreakdownIndex].AirlineTransFee / result.FareBreakdown[fareBreakdownIndex].PassengerCount;
                }
                #endregion



                //if (airlineCode != "##")
                //{
                //    Airline airline = new Airline();
                //    airline.Load(airlineCode);
                //    price.OtherCharges = Math.Round(((otherCharges + airline.Surcharge + sotoOtherCharge)), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])) * paxCount;
                //    decimal fuelSurcharge = Util.GetFuelSurcharge(airline.AirlineCode, "", "") * paxCount;
                //    if (isReturn)
                //    {
                //        fuelSurcharge = fuelSurcharge * 2;
                //    }
                //    price.TransactionFee = Math.Round((((grossFare + fuelSurcharge) * Convert.ToDecimal(airline.TransactionFee)) / 100), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                //}
                //else
                //{
                //    price.OtherCharges = Math.Round((otherCharges + sotoOtherCharge), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])) * paxCount;
                //}


                if (isSoto)
                {
                    //Agent commmission
                    price.AgentCommission = 0;
                    //Our Commission
                    price.OurCommission = 0;
                }
                else
                {
                    //For TBOAir these charges will come from xml so we are commenting here
                    //if (!agency.IsCorporate)
                    //{
                    //    price.AgentCommission = CalculateCommissionAgent(grossFare, agencyId, airlineCode);

                    //    if (airlineCode == "IX")
                    //    {
                    //        if (isReturn)
                    //        {
                    //            price.AgentCommission = 200 * seatCount;
                    //        }
                    //        else
                    //        {
                    //            price.AgentCommission = 100 * seatCount;
                    //        }
                    //    }
                    //}
                    //Commented by Shiva bcoz of TBO Issue Apr 11 2017
                    //if (airlineCode == "G9")
                    //{
                    //    if (isReturn)
                    //    {
                    //        price.OtherCharges = Convert.ToDecimal(CT.Configuration.ConfigurationSystem.AirArabiaConfig["ReturnServiceCharge"]) * paxCount;
                    //    }
                    //    else
                    //    {
                    //        price.OtherCharges = Convert.ToDecimal(CT.Configuration.ConfigurationSystem.AirArabiaConfig["OneWayServiceCharge"]) * paxCount;
                    //    }
                    //    price.AgentCommission = 0;
                    //}
                    //Our Commission
                    //price.OurCommission = GetOurCommission(grossFare, airlineCode);
                }
                //Net Fare
                decimal nFare = Math.Round((price.PublishedFare - price.OurCommission), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                if (isSoto)
                {
                    //Agent PLB
                    price.AgentPLB = 0;
                    //Our PLB
                    price.OurPLB = 0;
                }
                else
                {
                    //PLB Agent
                    //fareBreakdownIndex = -1 means when we calculate plb on full price(for all passengers)
                    if (fareBreakdownIndex == -1)
                    {
                        for (int i = 0; i < result.FareBreakdown.Length; i++)
                        {
                            //if (!agency.IsCorporate)
                            //{
                            //    price.AgentPLB += CalculatePLBAgent(price.PublishedFare - price.AgentCommission, agencyId, airlineCode, FlightPassenger.GetPTC(result.FareBreakdown[i].PassengerType), bookingClass, result.Flights[0][0].DepartureTime, result.Flights[0][0].Origin.AirportCode, destAirport);
                            //}
                            //price.OurPLB += CalculateOurPLB(nFare, airlineCode, FlightPassenger.GetPTC(result.FareBreakdown[i].PassengerType), Convert.ToString(result.Flights[0][0].BookingClass), result.Flights[0][0].DepartureTime, result.Flights[0][0].Origin.AirportCode, destAirport); // commented temp
                        }
                        //price.AgentPLB = Math.Round((price.AgentPLB / (result.FareBreakdown.Length)), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                        //price.OurPLB = Math.Round((price.OurPLB / (result.FareBreakdown.Length)), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                    }
                    else
                    {
                        //if (!agency.IsCorporate)
                        //{
                        //    price.AgentPLB = CalculatePLBAgent(price.PublishedFare - price.AgentCommission, agencyId, airlineCode, FlightPassenger.GetPTC(result.FareBreakdown[fareBreakdownIndex].PassengerType), bookingClass, result.Flights[0][0].DepartureTime, result.Flights[0][0].Origin.AirportCode, destAirport);
                        //}
                        // price.OurPLB = CalculateOurPLB(nFare, airlineCode, FlightPassenger.GetPTC(result.FareBreakdown[fareBreakdownIndex].PassengerType), Convert.ToString(result.Flights[0][0].BookingClass), result.Flights[0][0].DepartureTime, result.Flights[0][0].Origin.AirportCode, destAirport);
                    }
                }
                //decimal tdsrate = 0.0M;
                // price.TdsCommission = DomesticPricing.CalculateTDSAndTDSRate(price.AgentCommission, agencyId, isDomestic,out tdsrate);
                //price.TdsCommission = 0;
                //price.TdsRate = tdsrate;
                //price.TDSPLB = 0;
                //price.TDSPLB = CalculateTDS(price.AgentPLB, agencyId, isDomestic);
                #endregion
            }
            else
            {
                //TODO: Securate case is not correctly dealt bcoz of no example
                #region Securate Case
                //price.PublishedFare = grossFare;
                //price.NetFare = grossFare;
                //price.Markup = GetMarkup(grossFare);
                //price.OurCommission = 0;
                ////price.OtherCharges = otherCharges;
                //price.AgentPLB = CalculatePLBAgent(grossFare, agencyId, airlineCode);
                //price.OurPLB = CalculateOurPLB(grossFare, airlineCode);
                ////price.AgentCommission = 0;
                //decimal tdsRate = 0.0M;
                ////price.TdsCommission = DomesticPricing.CalculateTDSAndTDSRate(price.AgentCommission, agencyId, isDomestic,out tdsRate);
                ////price.TdsCommission = 0;
                //price.TdsRate = tdsRate;
                //price.TDSPLB = 0;
                //price.TDSPLB = CalculateTDS(price.AgentPLB, agencyId, isDomestic);

                #endregion
            }

            //Service Tax on gross Fare
            //price.SeviceTax = CalculateServiceTax(grossFare, isDomestic);

            if (fareBreakdownIndex == -1)
            {
                price.Tax = Convert.ToDecimal(result.Tax);
            }
            else
            {
                price.Tax = (Convert.ToDecimal(result.FareBreakdown[fareBreakdownIndex].TotalFare) - Convert.ToDecimal(result.FareBreakdown[fareBreakdownIndex].BaseFare)) / result.FareBreakdown[fareBreakdownIndex].PassengerCount;
            }
            // price.Currency = result.Currency;  // Commented as currency is taken from user preference

            //Region for charge break up of other charges
            if (price.OtherCharges > 0)
            {
                ChargeBreakUp cbu = new ChargeBreakUp();
                cbu.PriceId = price.PriceId;
                cbu.ChargeType = ChargeType.OtherCharges;
                cbu.Amount = price.OtherCharges;
                price.ChargeBU = new List<ChargeBreakUp>();
                price.ChargeBU.Add(cbu);
            }

            // Agent Markup calculation.
            #region AgentMarkup calculations
            DataTable dtAgentMarkup = new DataTable();
            List<FlightInfo> flightInfos = new List<FlightInfo>();
            flightInfos.AddRange(result.Flights[0]);
            if (result.Flights.Length > 1)
                flightInfos.AddRange(result.Flights[1]);

            bool isAirlineLCC = Airline.IsAirlineLCC(result.Flights[0][0].Airline);
            bool isInternational = flightInfos.Exists(delegate (FlightInfo fi) { return fi.Origin.CountryCode != fi.Destination.CountryCode; });

            dtAgentMarkup = UpdateMarkup.GetAllMarkup(agencyId, 1, flightInfos[0].DepartureTime);

            //string FlightType=(isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')"

            //if (agencyId > 0)
            //{
            //    dtAgentMarkup = UpdateMarkup.GetAllMarkup(agencyId, 1, (isInternational ? "INT" : "DOM"), (result.Flights.Length > 1 ? "RET" : "ONW"), (isAirlineLCC ? "LCC" : "GDS"));
            //}


            int Count = 0;

            foreach (Fare fare in result.FareBreakdown)
            {
                Count += fare.PassengerCount;
            }

            string source = "";
            switch (result.ResultBookingSource)
            {
                case BookingSource.AirArabia:
                    source = "'G9'";
                    break;
                case BookingSource.FlyDubai:
                    source = "'FZ'";
                    break;
                case BookingSource.UAPI:
                    source = "'UA'";
                    break;
                case BookingSource.SpiceJet:
                    source = "'SG'";
                    break;
                case BookingSource.TBOAir:
                    source = "'TA'";
                    break;
                case BookingSource.Indigo:
                    source = "'6E'";
                    break;
                case BookingSource.AirIndiaExpressIntl:
                    source = "'IX'";
                    break;
                case BookingSource.GoAir:
                    source = "'G8'";
                    break;
                case BookingSource.FlightInventory:
                    source = "'FI'";
                    break;
                case BookingSource.PKFares:
                    source = "'PK'";
                    break;
                case BookingSource.Amadeus:
                    source = "'1A'";
                    break;
                case BookingSource.SpiceJetCorp:
                    source = "'SGCORP'";
                    break;
                case BookingSource.IndigoCorp:
                    source = "'6ECORP'";
                    break;
                case BookingSource.Jazeera:
                    source = "'J9'";
                    break;
                case BookingSource.GoAirCorp:
                    source = "'G8CORP'";
                    break;
                case BookingSource.Babylon:
                    source = "'BABYLON'";
                    break;
                case BookingSource.Sabre:
                    source = "'SABRE'";
                    break;
            }
            string b2bRowFilter = string.Empty;
            if (dtAgentMarkup != null && dtAgentMarkup.Rows.Count > 0 )
            {

                dtAgentMarkup.DefaultView.RowFilter = "(AirlineCode='" + flightInfos[0].Airline + "' OR AirlineCode is NULL OR AirlineCode='All') AND Origin='" + flightInfos[0].Origin.AirportCode + "' AND Destination='" + flightInfos.FindLast(x => x.Group == 0 || x.Group == 1).Destination.AirportCode + "' AND Currency='" + result.Currency + "'";

                if (dtAgentMarkup.DefaultView.Count == 0)
                {
                    dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND FlightType='" + (isInternational ? "INT" : "DOM") + "' AND JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "' AND CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "'";
                    b2bRowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND FlightType='" + (isInternational ? "INT" : "DOM") + "' AND JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "' AND CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "'";
                }
                if (dtAgentMarkup.DefaultView.Count == 0)//FlightType not found FlightType=All, JourneyType=ONW,RET, CarrierType=GDS,LCC
                {
                    dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "')";
                    b2bRowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "')";
                }

                if (dtAgentMarkup.DefaultView.Count == 0)//FlightType, JourneyType not found FlightType=All, JourneyType=All, CarrierType=GDS,LCC
                {
                    dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "')";
                    b2bRowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "')";
                }

                if (dtAgentMarkup.DefaultView.Count == 0)//CarrierType, JourneyType not found FlightType=INT,DOM, JourneyType=All, CarrierType=All
                {
                    dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "') AND (JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                    b2bRowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "') AND (JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                }

                if (dtAgentMarkup.DefaultView.Count == 0)//FlightType, CarrierType not found FlightType=All, JourneyType=ONW,RET, CarrierType=All
                {
                    dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                    b2bRowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                }

                if (dtAgentMarkup.DefaultView.Count == 0)//FlightType, JourneyType, CarrierType not found FlightType=All, JourneyType=All, CarrierType=All
                {
                    dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                    b2bRowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                }   
            }

            //if ((agencyId > 1 && transType == "B2B") || transType == "B2C")
            if ((transType == "B2B") || transType == "B2C")
            {
                if (dtAgentMarkup.DefaultView != null && dtAgentMarkup.DefaultView.Count > 0)
                {
                    decimal agtMarkUp = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Markup"]);
                    price.MarkupType = Convert.ToString(dtAgentMarkup.DefaultView[0]["MarkupType"]);
                    price.MarkupValue = agtMarkUp;                    

                    if (Convert.ToString(dtAgentMarkup.DefaultView[0]["MarkupType"]) == "P")
                    {
                        if (agency.AgentAirMarkupType == "BF")
                        {
                            price.Markup = (Convert.ToDecimal(result.FareBreakdown[fareBreakdownIndex].BaseFare) * agtMarkUp) / 100;// TODO- should be configurable Basefare/totalFare
                        }
                        else if (agency.AgentAirMarkupType == "TX")
                        {
                            if (source == "'TA'") // For TBOAir , adding markup for AdditionalTxnFee + OtherCharges + SServiceFee + TransactionFee) / paxcount
                            {
                                price.Markup = (Convert.ToDecimal((result.FareBreakdown[fareBreakdownIndex].Tax / result.FareBreakdown[fareBreakdownIndex].PassengerCount) + ((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / Count)) * agtMarkUp) / 100;
                            }
                            else
                            {
                                price.Markup = (Convert.ToDecimal(result.FareBreakdown[fareBreakdownIndex].Tax / result.FareBreakdown[fareBreakdownIndex].PassengerCount) * agtMarkUp) / 100;
                            }
                        }
                        else if (agency.AgentAirMarkupType == "TF")
                        {
                            if (source == "'TA'") // For TBOAir , adding markup for AdditionalTxnFee + OtherCharges + SServiceFee + TransactionFee) / paxcount
                            {
                                price.Markup = (Convert.ToDecimal(((decimal)result.FareBreakdown[fareBreakdownIndex].TotalFare / result.FareBreakdown[fareBreakdownIndex].PassengerCount) + ((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / Count)) * agtMarkUp) / 100;
                            }
                            else
                            {
                                price.Markup = (Convert.ToDecimal(result.FareBreakdown[fareBreakdownIndex].TotalFare / result.FareBreakdown[fareBreakdownIndex].PassengerCount) * agtMarkUp) / 100;
                            }
                        }
                    }
                    else
                    {
                        price.Markup = agtMarkUp;
                    }
                    if (dtAgentMarkup.Columns.Contains("Discount") && dtAgentMarkup.Columns.Contains("DiscountType"))
                    {
                        decimal discount = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Discount"]);
                        price.DiscountType = Convert.ToString(dtAgentMarkup.DefaultView[0]["DiscountType"]);
                        price.DiscountValue = discount;
                        if (Convert.ToString(dtAgentMarkup.DefaultView[0]["DiscountType"]) == "P")
                        {
                            if (agency.AgentAirMarkupType == "BF") // Base Fare
                            {
                                price.Discount = (Convert.ToDecimal(result.FareBreakdown[fareBreakdownIndex].BaseFare) * discount) / 100;
                            }
                            else if (agency.AgentAirMarkupType == "TX")  // Tax
                            {
                                if (result.ResultBookingSource == BookingSource.TBOAir)
                                {
                                    price.Discount = (Convert.ToDecimal((result.FareBreakdown[fareBreakdownIndex].Tax / result.FareBreakdown[fareBreakdownIndex].PassengerCount) + ((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / Count)) * discount) / 100;
                                }
                                else
                                {
                                    price.Discount = (Convert.ToDecimal((result.FareBreakdown[fareBreakdownIndex].Tax / result.FareBreakdown[fareBreakdownIndex].PassengerCount)) * discount) / 100;
                                }
                            }
                            else if (agency.AgentAirMarkupType == "TF") // Total Fare = BaseFare + Tax
                            {
                                if (result.ResultBookingSource == BookingSource.TBOAir)
                                {
                                    price.Discount = (Convert.ToDecimal((result.FareBreakdown[fareBreakdownIndex].TotalFare / result.FareBreakdown[fareBreakdownIndex].PassengerCount) + (double)((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / Count)) * discount) / 100;
                                }
                                else
                                {
                                    price.Discount = (Convert.ToDecimal((result.FareBreakdown[fareBreakdownIndex].TotalFare / result.FareBreakdown[fareBreakdownIndex].PassengerCount)) * discount) / 100;
                                }
                            }
                        }
                        else
                        {
                            price.Discount = discount;
                        }
                    }

                    //Calculate Handling Fee only if Login user is from India
                    if (result.LoginCountryCode == "IN" && dtAgentMarkup.DefaultView.Count > 0 && dtAgentMarkup.Columns.Contains("HandlingFeeValue") && dtAgentMarkup.Columns.Contains("HandlingFeeType") && (dtAgentMarkup.DefaultView[0]["HandlingFeeType"]) != DBNull.Value)
                    {
                        decimal handlingFee = 0m, handlingFeeAmount = 0m;
                        handlingFee = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["HandlingFeeValue"]);

                        price.HandlingFeeType = Convert.ToString(dtAgentMarkup.DefaultView[0]["HandlingFeeType"]);
                        price.HandlingFeeValue = handlingFee;

                        decimal totalFare = (decimal)(result.FareBreakdown[fareBreakdownIndex].TotalFare / result.FareBreakdown[fareBreakdownIndex].PassengerCount) + (price.Markup) + (price.B2CMarkup);
                        if (price.HandlingFeeType == "P")
                        {
                            if (result.FareBreakdown[fareBreakdownIndex].PassengerType == PassengerType.Adult && result.ResultBookingSource == BookingSource.TBOAir)//Add OtherCharges for Adult only
                            {
                                handlingFeeAmount = (totalFare + ((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / result.FareBreakdown[fareBreakdownIndex].PassengerCount)) * handlingFee / 100;
                            }
                            else
                            {
                                handlingFeeAmount = (totalFare * handlingFee / 100);
                            }
                            handlingFeeAmount = handlingFeeAmount * result.FareBreakdown[fareBreakdownIndex].PassengerCount;
                        }
                        else
                        {
                            handlingFeeAmount = handlingFee * result.FareBreakdown[fareBreakdownIndex].PassengerCount;
                        }
                        handlingFeeAmount = Math.Round(handlingFeeAmount, agency.DecimalValue); //Rounding to agent decimal value to avoid decimal issues for handling fee
                        price.HandlingFeeAmount += handlingFeeAmount;
                        result.BaseFare += (double)handlingFeeAmount;
                        result.FareBreakdown[fareBreakdownIndex].HandlingFee = handlingFeeAmount;
                        result.TotalFare += (double)handlingFeeAmount;
                    }
                }


                if (transType == "B2C")
                {
                    //Get B2C Markup
                    dtAgentMarkup.DefaultView.RowFilter = "TransType='B2C' AND (AirlineCode='" + flightInfos[0].Airline + "' OR AirlineCode is NULL) AND SourceId=" + source + " AND ProductId=" + productType + "AND FlightType='" + (isInternational ? "INT" : "DOM") + "' AND JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "' AND CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "'";
                    if (dtAgentMarkup.DefaultView.Count == 0)
                    {
                        dtAgentMarkup.DefaultView.RowFilter = "TransType='B2C' AND (AirlineCode='" + flightInfos[0].Airline + "' OR AirlineCode is NULL OR AirlineCode='All') AND SourceId=" + source + " AND ProductId=" + productType + " AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (result.Flights.Length > 1 ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                    }       
                    if (dtAgentMarkup.DefaultView != null && dtAgentMarkup.DefaultView.Count > 0)
                    {
                        if (dtAgentMarkup.DefaultView[0]["MarkupType"] != DBNull.Value)
                        {
                            decimal agtB2CMarkUp = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Markup"]);
                            price.B2CMarkupType = dtAgentMarkup.DefaultView[0]["MarkupType"].ToString();
                            price.B2CMarkupValue = agtB2CMarkUp;

                            if (Convert.ToString(dtAgentMarkup.DefaultView[0]["MarkupType"]) == "P")
                            {
                                if (agency.AgentAirMarkupType == "BF")
                                {
                                    price.B2CMarkup = ((Convert.ToDecimal(result.FareBreakdown[fareBreakdownIndex].BaseFare) + price.Markup) * agtB2CMarkUp) / 100;// TODO- should be configurable Basefare/totalFare
                                }
                                else if (agency.AgentAirMarkupType == "TX")
                                {
                                    if (source == "'TA'") // For TBOAir , adding markup for AdditionalTxnFee + OtherCharges + SServiceFee + TransactionFee) / paxcount
                                    {
                                        price.B2CMarkup = (Convert.ToDecimal(price.Markup + (result.FareBreakdown[fareBreakdownIndex].Tax / result.FareBreakdown[fareBreakdownIndex].PassengerCount) + ((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / Count)) * agtB2CMarkUp) / 100;
                                    }
                                    else
                                    {
                                        price.B2CMarkup = (Convert.ToDecimal(price.Markup + (result.FareBreakdown[fareBreakdownIndex].Tax / result.FareBreakdown[fareBreakdownIndex].PassengerCount)) * agtB2CMarkUp) / 100;
                                    }
                                }
                                else if (agency.AgentAirMarkupType == "TF")
                                {
                                    if (source == "'TA'") // For TBOAir , adding markup for AdditionalTxnFee + OtherCharges + SServiceFee + TransactionFee) / paxcount
                                    {
                                        price.B2CMarkup = (Convert.ToDecimal(price.Markup + ((decimal)result.FareBreakdown[fareBreakdownIndex].TotalFare / result.FareBreakdown[fareBreakdownIndex].PassengerCount) + ((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / Count)) * agtB2CMarkUp) / 100;
                                    }
                                    else
                                    {
                                        price.B2CMarkup = (Convert.ToDecimal(price.Markup + (decimal)(result.FareBreakdown[fareBreakdownIndex].TotalFare / result.FareBreakdown[fareBreakdownIndex].PassengerCount)) * agtB2CMarkUp) / 100;
                                    }
                                }
                            }
                            else
                            {
                                price.B2CMarkup = agtB2CMarkUp;
                            }
                        }
                        if (dtAgentMarkup.Columns.Contains("Discount") && dtAgentMarkup.Columns.Contains("DiscountType"))
                        {
                            if (dtAgentMarkup.DefaultView[0]["DiscountType"] != DBNull.Value)
                            {
                                decimal discount = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Discount"]);

                                if (Convert.ToString(dtAgentMarkup.DefaultView[0]["DiscountType"]) == "P")
                                {
                                    if (agency.AgentAirMarkupType == "BF")
                                    {
                                        price.WhiteLabelDiscount = (Convert.ToDecimal(result.FareBreakdown[fareBreakdownIndex].BaseFare) * discount) / 100;// TODO- should be configurable Basefare/totalFare
                                    }
                                    else if (agency.AgentAirMarkupType == "TX")  // Tax
                                    {
                                        if (result.ResultBookingSource == BookingSource.TBOAir)
                                        {
                                            price.WhiteLabelDiscount = (Convert.ToDecimal((result.FareBreakdown[fareBreakdownIndex].Tax / result.FareBreakdown[fareBreakdownIndex].PassengerCount) + ((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / Count)) * discount) / 100;
                                        }
                                        else
                                        {
                                            price.WhiteLabelDiscount = (Convert.ToDecimal((result.FareBreakdown[fareBreakdownIndex].Tax / result.FareBreakdown[fareBreakdownIndex].PassengerCount)) * discount) / 100;
                                        }
                                    }
                                    else if (agency.AgentAirMarkupType == "TF") // Total Fare = BaseFare + Tax
                                    {
                                        if (result.ResultBookingSource == BookingSource.TBOAir)
                                        {
                                            price.WhiteLabelDiscount = (Convert.ToDecimal((result.FareBreakdown[fareBreakdownIndex].TotalFare / result.FareBreakdown[fareBreakdownIndex].PassengerCount) + (double)((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / Count)) * discount) / 100;
                                        }
                                        else
                                        {
                                            price.WhiteLabelDiscount = (Convert.ToDecimal((result.FareBreakdown[fareBreakdownIndex].TotalFare / result.FareBreakdown[fareBreakdownIndex].PassengerCount)) * discount) / 100;
                                        }
                                    }
                                }
                                else
                                {
                                    price.WhiteLabelDiscount = discount;
                                }
                            }
                        }

                        //Calculate Handling Fee only if Login user is from India
                        if (result.LoginCountryCode == "IN" && dtAgentMarkup.DefaultView.Count > 0 && dtAgentMarkup.Columns.Contains("HandlingFeeValue") && dtAgentMarkup.Columns.Contains("HandlingFeeType") && (dtAgentMarkup.DefaultView[0]["HandlingFeeType"]) != DBNull.Value)
                        {
                            decimal handlingFee = 0m, handlingFeeAmount = 0m;
                            handlingFee = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["HandlingFeeValue"]);

                            price.HandlingFeeType = Convert.ToString(dtAgentMarkup.DefaultView[0]["HandlingFeeType"]);
                            price.HandlingFeeValue = handlingFee;

                            decimal totalFare = (decimal)(result.FareBreakdown[fareBreakdownIndex].TotalFare / result.FareBreakdown[fareBreakdownIndex].PassengerCount) + (price.Markup) + (price.B2CMarkup);
                            if (price.HandlingFeeType == "P")
                            {
                                if (result.FareBreakdown[fareBreakdownIndex].PassengerType == PassengerType.Adult && result.ResultBookingSource == BookingSource.TBOAir)//Add OtherCharges for Adult only
                                {
                                    handlingFeeAmount = (totalFare + ((result.Price.AdditionalTxnFee + result.Price.OtherCharges + result.Price.SServiceFee + result.Price.TransactionFee) / result.FareBreakdown[fareBreakdownIndex].PassengerCount)) * handlingFee / 100;
                                }
                                else
                                {
                                    handlingFeeAmount = (totalFare * handlingFee / 100);
                                }
                                handlingFeeAmount = handlingFeeAmount * result.FareBreakdown[fareBreakdownIndex].PassengerCount;
                            }
                            else
                            {
                                handlingFeeAmount = handlingFee * result.FareBreakdown[fareBreakdownIndex].PassengerCount;
                            }
                            handlingFeeAmount = Math.Round(handlingFeeAmount, agency.DecimalValue); //Rounding to agent decimal value to avoid decimal issues for handling fee
                            price.HandlingFeeAmount += handlingFeeAmount;
                            result.BaseFare += (double)handlingFeeAmount;
                            result.FareBreakdown[fareBreakdownIndex].HandlingFee += handlingFeeAmount;
                            result.TotalFare += (double)handlingFeeAmount;
                        }
                    }
                }
                
                
            }


            #endregion

            //}
            return price;
        }

        /// <summary>
        /// Method get the price object contains everything from result object - For Hotel
        /// </summary>
        /// <param name="result">Result object</param>
        /// <param name="hotelRoomIndex">hotelRoomIndex</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="otherCharges">other charges</param>
        /// <param name="pType">pType</param>
        /// <param name="nights">nights</param>
        /// <param name="noOfRooms">noOfRooms</param>
        /// <param name="dayIndex">dayIndex</param>
        /// <returns>Price object contains all the values of properties</returns>

        public static PriceAccounts GetPrice(HotelSearchResult result, int hotelRoomIndex, int agencyId, decimal otherCharges, int dayIndex, int noOfRooms, int nights, PriceType pType)
        {
            decimal grossFare = 0;
            PriceAccounts price = new PriceAccounts();

            //this case is when we calculate full fare in place of individual farebreakdown
            if (hotelRoomIndex == -1 && dayIndex == -1)
            {
                grossFare = result.TotalPrice - result.TotalTax;
            }
            else if (hotelRoomIndex != -1 && dayIndex == -1) // In case of extra Guest Charge
            {
                grossFare = result.RoomDetails[hotelRoomIndex].PubExtraGuestCharges;
            }
            else
            {
                if (result.BookingSource == HotelBookingSource.DOTW)
                {
                    grossFare = result.RoomDetails[hotelRoomIndex].TotalPrice;
                }
                else if(result.BookingSource == HotelBookingSource.RezLive)
                {
                    grossFare = result.RoomDetails[hotelRoomIndex].Rates[dayIndex].Amount;
                }
            }
            bool isGross = true;
            //TODO: Check IsDomestic 
            bool isDomestic = true;
            CT.Core.StaticData staticInfo = new CT.Core.StaticData();
            Dictionary<string, decimal> rateOfEx = staticInfo.CurrencyROE;
            decimal rateofExchange = 1;
            if (result.BookingSource == HotelBookingSource.Desiya)
            {
                isDomestic = true;
            }
            else
            {
                isDomestic = false;
                if (result.Currency != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
                {
                    rateofExchange = rateOfEx[result.Currency];
                }
            }

            if (hotelRoomIndex == -1)
            {
                price.Tax = result.TotalTax;
            }
            //checking whether markup need to change in gross fare currency - In case of mark up fixed
            // no need to change fare currency - In case of percentage




            if (isGross)
            {
                if (pType == PriceType.PublishedFare)
                {
                    #region When Gross Fare is given
                    //TODO: add detailed commment with examples here
                    if (isDomestic)
                    {
                        grossFare = Math.Round(grossFare,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    }
                    price.PublishedFare = grossFare;
                    price.NetFare = 0;
                    price.Markup = 0;
                    price.AccPriceType = PriceType.PublishedFare;
                    price.OtherCharges = Math.Round(otherCharges,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    Dictionary<string, decimal> commList = CalculateCommission(grossFare, agencyId, result.BookingSource, noOfRooms, nights, rateofExchange,ProductType.Hotel);
                    ////Agent commmission
                    //price.AgentCommission = commList["AgentCommission"];
                    ////Our Commission
                    //price.OurCommission = commList["OurCommission"];

                    //Markup
                    if (commList.ContainsKey("Markup"))
                    {
                        price.Markup = commList["Markup"];
                        price.Discount = commList["Discount"];
                    }
                    
                    
                    //price.TdsCommission = CalculateTDS(price.AgentCommission, agencyId, isDomestic);
                    price.TdsCommission = 0;

                    // For hotel service tax - it should be cqlculated on (basic + Taxes - commission)
                    price.SeviceTax = CalculateProductServiceTax(grossFare + price.Tax, isDomestic, ProductType.Hotel);

                    //Net Fare
                    decimal nFare = Math.Round((price.PublishedFare - price.OurCommission),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));

                    //TODO: Calculate TDS               
                    #endregion
                }
                else if (pType == PriceType.NetFare)
                {
                    #region When Net Fare is given
                    if (isDomestic)
                    {
                        grossFare = Math.Round(grossFare,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    }
                    price.AccPriceType = PriceType.NetFare;
                    price.PublishedFare = 0;
                    price.NetFare = grossFare;
                    price.AgentCommission = 0;
                    price.OurCommission = 0;

                    price.OtherCharges = Math.Round(otherCharges,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));

                    Dictionary<string, decimal> commList = CalculateCommission(grossFare, agencyId, result.BookingSource, noOfRooms, nights, rateofExchange,ProductType.Hotel);
                    //Agent commmission
                    if (commList.ContainsKey("Markup"))
                    {
                        price.Markup = commList["Markup"];
                        price.Discount = commList["Discount"];
                    }

                    // For hotel service tax - it should be cqlculated on (basic + Taxes - commission)
                    if (isDomestic)
                    {
                        price.SeviceTax = CalculateProductServiceTax(grossFare + price.Tax, isDomestic, ProductType.Hotel);
                    }
                    else
                    {
                        price.SeviceTax = 0;
                    }

                    //Net Fare
                    decimal nFare = Math.Round((price.NetFare + price.Markup),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    #endregion
                }
            }

            //NO Individual day wise tax found

            price.Currency = result.Currency;
            //Region for charge break up of other charges
            if (price.OtherCharges > 0)
            {
                ChargeBreakUp cbu = new ChargeBreakUp();
                cbu.PriceId = price.PriceId;
                cbu.ChargeType = ChargeType.OtherCharges;
                cbu.Amount = price.OtherCharges;
                price.ChargeBU = new List<ChargeBreakUp>();
                price.ChargeBU.Add(cbu);
            }
            return price;
        }

        /// <summary>
        /// Method get the price object contains everything from result object - For Transfers
        /// </summary>
        /// <param name="result">Result object</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="otherCharges">other charges</param>
        /// <param name="dayIndex">dayIndex</param>
        /// <param name="pType">pType</param>
        /// <returns>Price object contains all the values of properties</returns>

        public static PriceAccounts GetPrice(TransferSearchResult result, int agencyId, decimal otherCharges, int dayIndex, PriceType pType) 
        {
            decimal grossFare;
            PriceAccounts price = new PriceAccounts();
            //this case is when we calculate full fare in place of individual farebreakdown
            grossFare = result.Vehicles[dayIndex].ItemPrice;
            bool isGross = true;
            //TODO: Check IsDomestic 
            bool isDomestic = true;
            CT.Core.StaticData staticInfo = new CT.Core.StaticData();
            staticInfo.BaseCurrency = new AgentMaster(agencyId).AgentCurrency;
            Dictionary<string, decimal> rateOfEx = staticInfo.CurrencyROE;
            decimal rateofExchange = 1;
            if (result.Source == TransferBookingSource.GTA)
            {
                isDomestic = false;
                if (result.Currency != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
                {
                    rateofExchange = rateOfEx[result.Currency];
                }
            }
            if (isGross)
            {
                if (pType == PriceType.PublishedFare)
                {
                    #region When Gross Fare is given
                    //TODO: add detailed commment with examples here
                    if (isDomestic)
                    {
                        grossFare = Math.Round(grossFare,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    }
                    price.AccPriceType = PriceType.PublishedFare;
                    price.PublishedFare = grossFare;
                    price.NetFare = 0;
                    price.Markup = 0;

                    price.OtherCharges = Math.Round(otherCharges,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    Dictionary<string, decimal> commList = CalculateCommission(grossFare, agencyId, result.Source, rateofExchange);
                    //Agent commmission
                    price.AgentCommission = commList["AgentCommission"];
                    //Our Commission
                    price.OurCommission = commList["OurCommission"];

                    price.TdsCommission = CalculateTDS(price.AgentCommission, agencyId, isDomestic);

                    // For hotel service tax - it should be cqlculated on (basic + Taxes - commission)
                    price.SeviceTax = CalculateProductServiceTax(grossFare + price.Tax, isDomestic, ProductType.Transfers);

                    //Net Fare
                    decimal nFare = Math.Round((price.PublishedFare - price.OurCommission),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));

                    //TODO: Calculate TDS               
                    #endregion
                }
                else if (pType == PriceType.NetFare)
                {
                    #region When Net Fare is given
                    if (isDomestic)
                    {
                        grossFare = Math.Round(grossFare,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    }
                    price.AccPriceType = PriceType.NetFare;
                    price.PublishedFare = 0;
                    price.NetFare = grossFare;
                    price.AgentCommission = 0;
                    price.OurCommission = 0;

                    price.OtherCharges = Math.Round(otherCharges,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));

                    Dictionary<string, decimal> commList = CalculateCommission(grossFare, agencyId, result.Source, rateofExchange);
                    //Agent commmission
                    price.Markup = commList["OurCommission"];

                    // For hotel service tax - it should be cqlculated on (basic + Taxes - commission)
                    if (isDomestic)
                    {
                        price.SeviceTax = CalculateProductServiceTax(grossFare + price.Tax, isDomestic, ProductType.Transfers);
                    }
                    else
                    {
                        price.SeviceTax = 0;
                    }

                    //Net Fare
                    decimal nFare = Math.Round((price.NetFare + price.Markup),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    #endregion
                }
            }

            //NO Individual day wise tax found

            price.Currency = result.Currency;
            price.RateOfExchange = rateofExchange;
            //Region for charge break up of other charges
            if (price.OtherCharges > 0)
            {
                ChargeBreakUp cbu = new ChargeBreakUp();
                cbu.PriceId = price.PriceId;
                cbu.ChargeType = ChargeType.OtherCharges;
                cbu.Amount = price.OtherCharges;
                price.ChargeBU = new List<ChargeBreakUp>();
                price.ChargeBU.Add(cbu);
            }
            return price;
        }
        /// <summary>
        /// This Method is used to get the PriceInfo for Sightseeing results
        /// </summary>
        /// <param name="result">Result object</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="otherCharges">other charges</param>
        /// <param name="dayIndex">dayIndex</param>
        /// <param name="pType">pType</param>
        /// <returns>PriceAccounts</returns>
        /*public static PriceAccounts GetPrice(SightseeingSearchResult result, int agencyId, decimal otherCharges, int dayIndex, PriceType pType)
        {
            decimal grossFare;
            PriceAccounts price = new PriceAccounts();
            //this case is when we calculate full fare in place of individual farebreakdown
            grossFare = result.TourOperationList[dayIndex].ItemPrice;
            bool isGross = true;
            //TODO: Check IsDomestic 
            bool isDomestic = true;
            CT.Core.StaticData staticInfo = new CT.Core.StaticData();
            Dictionary<string, decimal> rateOfEx = staticInfo.CurrencyROE;
            decimal rateofExchange = 1;
            if (result.Source == SightseeingBookingSource.GTA)
            {
                isDomestic = false;
                if (result.TourOperationList[dayIndex].Currency != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
                {
                    rateofExchange = rateOfEx[result.TourOperationList[dayIndex].Currency];
                }
            }
            if (isGross)
            {
                if (pType == PriceType.PublishedFare)
                {
                    #region When Gross Fare is given
                    //TODO: add detailed commment with examples here
                    if (isDomestic)
                    {
                        grossFare = Math.Round(grossFare,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    }
                    price.AccPriceType = PriceType.PublishedFare;
                    price.PublishedFare = grossFare;
                    price.NetFare = 0;
                    price.Markup = 0;

                    price.OtherCharges = Math.Round(otherCharges,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    Dictionary<string, decimal> commList = CalculateCommission(grossFare, agencyId, result.Source, rateofExchange);
                    //Agent commmission
                    price.AgentCommission = commList["AgentCommission"];
                    //Our Commission
                    price.OurCommission = commList["OurCommission"];

                    price.TdsCommission = CalculateTDS(price.AgentCommission, agencyId, isDomestic);

                    // For hotel service tax - it should be cqlculated on (basic + Taxes - commission)
                    price.SeviceTax = CalculateProductServiceTax(grossFare + price.Tax, isDomestic, ProductType.Transfers);

                    //Net Fare
                    decimal nFare = Math.Round((price.PublishedFare - price.OurCommission),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));

                    //TODO: Calculate TDS               
                    #endregion
                }
                else if (pType == PriceType.NetFare)
                {
                    #region When Net Fare is given
                    if (isDomestic)
                    {
                        grossFare = Math.Round(grossFare,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    }
                    price.AccPriceType = PriceType.NetFare;
                    price.PublishedFare = 0;
                    price.NetFare = grossFare;
                    price.AgentCommission = 0;
                    price.OurCommission = 0;

                    price.OtherCharges = Math.Round(otherCharges,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));

                    Dictionary<string, decimal> commList = CalculateCommission(grossFare, agencyId, result.Source, rateofExchange);
                    //Agent commmission
                    price.Markup = commList["OurCommission"];

                    // For hotel service tax - it should be cqlculated on (basic + Taxes - commission)
                    if (isDomestic)
                    {
                        price.SeviceTax = CalculateProductServiceTax(grossFare + price.Tax, isDomestic, ProductType.Transfers);
                    }
                    else
                    {
                        price.SeviceTax = 0;
                    }

                    //Net Fare
                    decimal nFare = Math.Round((price.NetFare + price.Markup),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    #endregion
                }
            }

            //NO Individual day wise tax found

            price.Currency = result.TourOperationList[0].Currency;
            price.RateOfExchange = rateofExchange;
            //Region for charge break up of other charges
            if (price.OtherCharges > 0)
            {
                ChargeBreakUp cbu = new ChargeBreakUp();
                cbu.PriceId = price.PriceId;
                cbu.ChargeType = ChargeType.OtherCharges;
                cbu.Amount = price.OtherCharges;
                price.ChargeBU = new List<ChargeBreakUp>();
                price.ChargeBU.Add(cbu);
            }
            return price;
        }*/

        /// <summary>
        /// Method get the price object contains everything from itinerary - For Hotel
        /// </summary>
        /// <param name="nights">nights</param>
        /// <param name="pType">pType</param>
        /// <param name="roomTypeCode">roomTypeCode</param>
        /// <param name="noofRooms">noofRooms</param>
        /// <param name="result">result</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="otherCharges">other charges</param>
        /// <returns>Price object contains all the values of properties</returns>
        public static PriceAccounts GetPrice(HotelSearchResult result, string roomTypeCode, int agencyId, decimal otherCharges, int noofRooms, int nights, PriceType pType)
        {
            PriceAccounts price = new PriceAccounts();
            decimal grossFare = 0;
            decimal totalTax = 0;
            for (int j = 0; j < result.RoomDetails.Length; j++)
            {
                if (result.RoomDetails[j].RoomTypeCode.Equals(roomTypeCode))
                {
                    HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[result.RoomDetails[j].Rates.Length];
                    for (int k = 0; k < result.RoomDetails[j].Rates.Length; k++)
                    {
                        grossFare += result.RoomDetails[j].Rates[k].Amount + result.RoomDetails[j].PubExtraGuestCharges;
                        totalTax += result.RoomDetails[j].Rates[k].Totalfare - result.RoomDetails[j].Rates[k].BaseFare;
                    }
                    if (result.BookingSource == HotelBookingSource.IAN)
                    {
                        totalTax = result.RoomDetails[j].TotalTax;
                    }
                    break;
                }
            }
            grossFare = grossFare / noofRooms;
            totalTax = totalTax / noofRooms;
            // = itinerary.Passenger[paxIndex].Price.PublishedFare;
            bool isGross = true;
            //TODO: Check IsDomestic 
            bool isDomestic = true;
            CT.Core.StaticData staticInfo = new CT.Core.StaticData();
            Dictionary<string, decimal> rateOfEx = staticInfo.CurrencyROE;
            decimal rateofExchange = 1;
            if (result.BookingSource == HotelBookingSource.Desiya)
            {
                isDomestic = true;
            }
            else
            {
                isDomestic = false;
                if (result.Currency != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
                {
                    rateofExchange = rateOfEx[result.Currency];
                }
            }

            price.Tax = totalTax;
            //checking whether markup need to change in gross fare currency - In case of mark up fixed
            // no need to change fare currency - In case of percentage


            if (isGross)
            {
                if (pType == PriceType.PublishedFare)
                {
                    #region When Gross Fare is given
                    //TODO: add detailed commment with examples here
                    if (isDomestic)
                    {
                        grossFare = Math.Round(grossFare,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    }
                    price.AccPriceType = PriceType.PublishedFare;
                    price.PublishedFare = grossFare;
                    price.NetFare = 0;
                    price.Markup = 0;
                    price.OtherCharges = Math.Round(otherCharges,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    Dictionary<string, decimal> commList = CalculateCommission(grossFare, agencyId, result.BookingSource, noofRooms, nights, rateofExchange);
                    //Agent commmission
                    price.AgentCommission = commList["AgentCommission"];
                    //Our Commission
                    price.OurCommission = commList["OurCommission"];

                    price.TdsCommission = 0;
                    //price.TdsCommission = CalculateTDS(price.AgentCommission, agencyId, isDomestic);

                    // For hotel service tax - it should be cqlculated on (basic + Taxes - commission)
                    price.SeviceTax = CalculateProductServiceTax(grossFare + price.Tax, isDomestic, ProductType.Hotel);

                    //Net Fare
                    decimal nFare = price.PublishedFare - price.OurCommission;
                    //TODO: Calculate TDS    
                    #endregion
                }
                else if (pType == PriceType.NetFare)
                {
                    #region When Net Fare is given

                    if (isDomestic)
                    {
                        grossFare = Math.Round(grossFare,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    }
                    price.AccPriceType = PriceType.NetFare;
                    price.PublishedFare = 0;
                    price.NetFare = grossFare;
                    price.AgentCommission = 0;
                    price.OurCommission = 0;

                    price.OtherCharges = Math.Round(otherCharges,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    Dictionary<string, decimal> commList = CalculateCommission(grossFare, agencyId, result.BookingSource, noofRooms, nights, rateofExchange);
                    //Agent commmission
                    price.Markup = commList["OurCommission"];

                    if (isDomestic)
                    {
                        // For hotel service tax - it should be cqlculated on (basic + Taxes - commission)
                        price.SeviceTax = CalculateProductServiceTax(grossFare + price.Tax, isDomestic, ProductType.Hotel);
                    }
                    else
                    {
                        price.SeviceTax = 0;
                    }

                    //Net Fare
                    decimal nFare = Math.Round((price.NetFare + price.Markup),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    #endregion
                }
            }

            price.Currency = result.Currency;
            //Region for charge break up of other charges
            if (price.OtherCharges > 0)
            {
                ChargeBreakUp cbu = new ChargeBreakUp();
                cbu.PriceId = price.PriceId;
                cbu.ChargeType = ChargeType.OtherCharges;
                cbu.Amount = price.OtherCharges;
                price.ChargeBU = new List<ChargeBreakUp>();
                price.ChargeBU.Add(cbu);
            }
            return price;
        }

        /// <summary>
        /// Method get the price object contains everything from Hotel Itinerary object - For Hotel
        /// </summary>
        /// <param name="dayIndex">dayIndex</param>
        /// <param name="pType">pType</param>
        /// <param name="nights">nights</param>
        /// <param name="noOfRooms">noOfRooms</param>
        /// <param name="itinerary">itinerary</param>
        /// <param name="hotelRoomIndex">hotelRoomIndex</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="otherCharges">other charges</param>
        /// <returns>Price object contains all the values of properties</returns>
        public static PriceAccounts GetPrice(HotelItinerary itinerary, int hotelRoomIndex, int agencyId, decimal otherCharges, int dayIndex, int noOfRooms, int nights, PriceType pType)
        {
            decimal grossFare;
            PriceAccounts price = new PriceAccounts();
            //setting currency for the country
            GTACity gtaCity = new GTACity();
            string countryName = gtaCity.GetCountryNameforCityCode(itinerary.CityCode);
            price.Currency = gtaCity.GetCurrencyForCountry(countryName);
            //this case is when we calculate full fare for a sigle Hotel Room 
            if (hotelRoomIndex != -1 && dayIndex == -2)
            {
                if (pType == PriceType.NetFare)
                {
                    grossFare = itinerary.Roomtype[hotelRoomIndex].Price.NetFare;
                }
                else
                {
                    grossFare = itinerary.Roomtype[hotelRoomIndex].Price.PublishedFare;
                }
            }
            else if (hotelRoomIndex != -1 && dayIndex == -1) // In case of extra Guest Charge
            {
                grossFare = itinerary.Roomtype[hotelRoomIndex].ExtraGuestCharge;
            }
            else
            {
                grossFare = itinerary.Roomtype[hotelRoomIndex].RoomFareBreakDown[dayIndex].RoomPrice;
            }
            bool isGross = true;
            //TODO: Check IsDomestic 
            bool isDomestic = true;
            CT.Core.StaticData staticInfo = new CT.Core.StaticData();
            Dictionary<string, decimal> rateOfEx = staticInfo.CurrencyROE;
            decimal rateofExchange = 1;
            if (itinerary.Source == HotelBookingSource.Desiya)
            {
                isDomestic = true;
            }
            else
            {
                isDomestic = false;
                if (itinerary.Roomtype[0].Price.Currency != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
                {
                    rateofExchange = rateOfEx[itinerary.Roomtype[0].Price.Currency];
                }
            }

            //checking whether markup need to change in gross fare currency - In case of mark up fixed
            // no need to change fare currency - In case of percentage
            if (isGross)
            {
                if (pType == PriceType.PublishedFare)
                {
                    #region When Gross Fare is given
                    //TODO: add detailed commment with examples here
                    if (isDomestic)
                    {
                        grossFare = Math.Round(grossFare,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    }
                    price.AccPriceType = PriceType.PublishedFare;
                    price.PublishedFare = grossFare;
                    price.NetFare = 0;
                    price.Markup = 0;

                    price.OtherCharges = Math.Round(otherCharges,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    Dictionary<string, decimal> commList = CalculateCommission(grossFare, agencyId, itinerary.Source, noOfRooms, nights, rateofExchange);
                    ////Agent commmission
                    //price.AgentCommission = commList["AgentCommission"];
                    ////Our Commission
                    //price.OurCommission = commList["OurCommission"];
                    price.Markup = commList["Markup"];
                    price.Discount = commList["Discount"];

                    price.TdsCommission = 0;
                    //price.TdsCommission = CalculateTDS(price.AgentCommission, agencyId, isDomestic);

                    // For hotel service tax - it should be cqlculated on (basic + Taxes - commission)
                    price.SeviceTax = CalculateProductServiceTax(grossFare + price.Tax, isDomestic, ProductType.Hotel);

                    //Net Fare
                    decimal nFare = Math.Round((price.PublishedFare - price.OurCommission),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));

                    //TODO: Calculate TDS               
                    #endregion
                }
                else if (pType == PriceType.NetFare)
                {
                    #region When Net Fare is given
                    if (isDomestic)
                    {
                        grossFare = Math.Round(grossFare,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    }
                    price.AccPriceType = PriceType.NetFare;
                    price.PublishedFare = 0;
                    price.NetFare = grossFare;
                    price.AgentCommission = 0;
                    price.OurCommission = 0;

                    price.OtherCharges = Math.Round(otherCharges,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));

                    Dictionary<string, decimal> commList = CalculateCommission(grossFare, agencyId, itinerary.Source, noOfRooms, nights, rateofExchange,ProductType.Hotel);
                    //Agent commmission
                    price.Markup = commList["OurCommission"];

                    // For hotel service tax - it should be cqlculated on (basic + Taxes - commission)
                    if (isDomestic)
                    {
                        price.SeviceTax = CalculateProductServiceTax(grossFare + price.Tax, isDomestic, ProductType.Hotel);
                    }
                    else
                    {
                        price.SeviceTax = 0;
                    }

                    //Net Fare
                    decimal nFare = Math.Round((price.NetFare + price.Markup),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                    #endregion
                }
            }

            //NO Individual day wise tax found

            price.Currency = itinerary.Roomtype[0].Price.Currency;
            //Region for charge break up of other charges
            if (price.OtherCharges > 0)
            {
                ChargeBreakUp cbu = new ChargeBreakUp();
                cbu.PriceId = price.PriceId;
                cbu.ChargeType = ChargeType.OtherCharges;
                cbu.Amount = price.OtherCharges;
                price.ChargeBU = new List<ChargeBreakUp>();
                price.ChargeBU.Add(cbu);
            }
            return price;
        }


        /// <summary>
        /// Method add the markUp in case of securate
        /// </summary>
        /// <param name="netFare">Net Fare</param>
        /// <returns>netFare+markup</returns>
        private static decimal GetMarkup(decimal netFare)
        {
            float markup = 0.0f;
            //Trace.TraceInformation("AccountingEngine.GetMarkup entered : netFare = " + netFare);
            try
            {
                //SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[0];
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetMarkup, paramList,connection);
                using (DataTable dtMarkup = DBGateway.FillDataTableSP(SPNames.GetMarkup, paramList))
                {
                    if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                    {
                        DataRow dataReader = dtMarkup.Rows[0];
                        if (dataReader != null && dataReader["markup"] != DBNull.Value)
                        {
                            markup = Convert.ToSingle(dataReader["markup"]);
                        }
                    }
                }
                //dataReader.Close();
                //connection.Close();
            }
            catch (SqlException exception)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
            }
            //Trace.TraceInformation("AccountingEngine.GetMarkup exited : markup = " + markup);
            return Math.Round((Convert.ToDecimal(markup) * netFare / 100), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
        }
        /// <summary>
        /// Method calculates the PLB of the agent
        /// </summary>
        /// <param name="netFare">netFare</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="airlineCode">airlineCode</param>
        /// <returns>decimal</returns>
        private static decimal CalculatePLBAgent(decimal netFare, int agencyId, string airlineCode)
        {
            //Trace.TraceInformation("AccountingEngine.CalculatePLBAgent entered : netFare = " + netFare);
            float plb = 0.0f;
            if (agencyId == 0)
            {
                return Convert.ToDecimal(plb);
            }
            try
            {
               // SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@agencyId", agencyId);
                paramList[1] = new SqlParameter("@airlineCode", airlineCode);
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetPLB, paramList,connection);
                using (DataTable dtPlb = DBGateway.FillDataTableSP(SPNames.GetPLB, paramList))
                {
                    if (dtPlb != null && dtPlb.Rows.Count > 0)
                    {
                        DataRow dataReader = dtPlb.Rows[0];
                        if (dataReader !=null && dataReader["plb"] !=DBNull.Value)
                        {
                            plb = Convert.ToSingle(dataReader["plb"]);
                        }
                    }
                }
                //dataReader.Close();
                //connection.Close();
            }
            catch (SqlException exception)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
            }
            //Trace.TraceInformation("AccountingEngine.CalculatePLBAgent exited : plb = " + plb);
            return (Convert.ToDecimal(plb) * netFare / 100);
        }
        /// <summary>
        /// Method calculates the TDS of the agent
        /// </summary>
        /// <param name="income">income</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="isDomestic">isDomestic</param>
        /// <returns>decimal</returns>
        public static decimal CalculateTDS(decimal income, int agencyId, bool isDomestic)
        {
            //Trace.TraceInformation("AccountingEngine.CalculateTDS entered : income = " + income);
            float tds = 0.0f;
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable accountingConfig = con.GetSelfAgencyId();
            int selfAgencyId = Convert.ToInt32(accountingConfig["selfAgencyId"]);
            if (agencyId == selfAgencyId)
            {
                return Convert.ToDecimal(tds);
            }
            //Agency agency = new Agency(agencyId); todo-ziya
            //AgentMaster agency = new AgentMaster(CT.TicketReceipt.Common.Utility.ToLong(agencyId));
            //if (agency.ExemptionLimit <= 0)
            if (Settings.LoginInfo.AgentBalance <= 0)
            {
                tds = AccountsDetail.GetTDSDefault(DateTime.Now, isDomestic);
            }
            else
            {
                //decimal agentEarning = Ledger.GetAgentEarningThisFinancialYear(agencyId); ziya-todo
                decimal agentEarning = 0;
                //if (agentEarning >= agency.ExemptionLimit)
                if (agentEarning >= Settings.LoginInfo.AgentBalance)
                        

                {
                    tds = AccountsDetail.GetTDSDefault(DateTime.Now, isDomestic);
                }
                else
                {
                    //Transition Phase When Income on cuerrrent booking and current earned less than Exemption Limit
                    //if ((income + agentEarning <= agency.ExemptionLimit))
                    if ((income + agentEarning <= Settings.LoginInfo.AgentBalance))
                    {
                        //tds = agency.ExemptionPercentage;
                        tds = ((float)Settings.LoginInfo.AgentBalance);// ziya-todo
                    }
                    else
                    {
                        tds = AccountsDetail.GetTDSDefault(DateTime.Now, isDomestic);
                    }
                }
            }
            decimal tdsAmount = Math.Round((income * Convert.ToDecimal(tds) / 100), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
                      
            //Trace.TraceInformation("AccountingEngine.CalculateTDS exited : tds = " + tdsAmount);
            return tdsAmount;
            //Trace.TraceInformation("AccountingEngine.CalculateTDS exited : tds = " + tds);
            //return Math.Round((income * Convert.ToDecimal(tds) / 100),2);
        }
        /// <summary>
        /// Method calculate service tax on that Fare
        /// </summary>
        /// <param name="grossFare">Gross Fare</param>
        /// <param name="isDomestic">flag(whether flight is domestic or not)</param>
        /// <returns>Service Tax</returns>
        public static decimal CalculateServiceTax(decimal grossFare, bool isDomestic)
        {
            //Trace.TraceInformation("AccountingEngine.CalculateServiceTax entered : grossFare = " + grossFare);
            float serviceTax = 0.0f;
            try
            {
                //SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@isDomestic", isDomestic);
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetServiceTax, paramList,connection);
                using (DataTable dtService = DBGateway.FillDataTableSP(SPNames.GetServiceTax, paramList))
                {
                    if (dtService != null && dtService.Rows.Count > 0)
                    {
                        DataRow dataReader = dtService.Rows[0];
                        if (dataReader != null && dataReader["serviceTax"] != DBNull.Value)
                        {
                            serviceTax = Convert.ToSingle(dataReader["serviceTax"]);
                        }
                    }
                }
                //dataReader.Close();
                //connection.Close();
            }
            catch (SqlException exception)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
            }
            decimal serviceTaxAmount = grossFare * Convert.ToDecimal(serviceTax) / 100; 
            float cess = AccountsDetail.GetCessDefault();
            decimal cessAmount = serviceTaxAmount * Convert.ToDecimal(cess) / 100;
            decimal totalServiceTaxAmount = Math.Round(serviceTaxAmount+cessAmount,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
            //Trace.TraceInformation("AccountingEngine.CalculateServiceTax exited : serviceTax = " + totalServiceTaxAmount);
            return totalServiceTaxAmount;
        }

        /// <summary>
        /// Calculate product service tax
        /// </summary>
        /// <param name="grossFare">Gross Fare</param>
        /// <param name="isDomestic">flag(whether flight is domestic or not)</param>
        /// <param name="prodType">prodType</param>
        /// <returns>decimal</returns>
        public static decimal CalculateProductServiceTax(decimal grossFare, bool isDomestic, ProductType prodType)
        {
            //Trace.TraceInformation("AccountingEngine.CalculateProductServiceTax entered : grossFare = " + grossFare);
            float serviceTax = 0.0f;
            try
            {
                //SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@isDomestic", isDomestic);
                if (prodType == ProductType.Hotel)
                {
                    //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetHotelServiceTax, paramList,connection);
                    using (DataTable dtHotelService = DBGateway.FillDataTableSP(SPNames.GetHotelServiceTax, paramList))
                    {
                        if (dtHotelService != null && dtHotelService.Rows.Count > 0)
                        {
                            DataRow dataReader = dtHotelService.Rows[0];
                            if (dataReader != null && dataReader["serviceTax"] != DBNull.Value)
                            {
                                serviceTax = Convert.ToSingle(dataReader["serviceTax"]);
                            }
                        }
                    }
                    //dataReader.Close();
                }
                else if (prodType == ProductType.Transfers)
                {
                    //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetTransferServiceTax, paramList, connection);
                    using (DataTable dtTransferService = DBGateway.FillDataTableSP(SPNames.GetTransferServiceTax, paramList))
                    {
                        if (dtTransferService != null && dtTransferService.Rows.Count > 0)
                        {
                            DataRow dataReader = dtTransferService.Rows[0];
                            if (dataReader != null && dataReader["serviceTax"] != DBNull.Value)
                            {
                                serviceTax = Convert.ToSingle(dataReader["serviceTax"]);
                            }
                        }
                    }
                    //dataReader.Close();
                }
                //connection.Close();
            }
            catch (SqlException exception)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
            }
            //Trace.TraceInformation("AccountingEngine.CalculateProductServiceTax exited : serviceTax = " + serviceTax);
            return Math.Round((grossFare * Convert.ToDecimal(serviceTax) / 100), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
        }

        /// <summary>
        /// Method calculate expected price(Cost Price+our PLB)
        /// </summary>
        /// <param name="costPrice">Cost Price</param>
        /// <param name="airlineCode">Airline Code</param>
        /// <returns>decimal</returns>
        private static decimal CalculateOurPLB(decimal costPrice, string airlineCode)
        {
            //Trace.TraceInformation("AccountingEngine.CalculateExpectedPrice entered : costPrice = " + costPrice);
            float ourPLB = 0.0f;
            try
            {
                //SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@airlineCode", airlineCode);
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetAirlinePLB, paramList,connection);
                using (DataTable dtAirline = DBGateway.FillDataTableSP(SPNames.GetAirlinePLB, paramList))
                {
                    if (dtAirline != null && dtAirline.Rows.Count > 0)
                    {
                        DataRow dataReader = dtAirline.Rows[0];
                        if (dataReader != null && dataReader["plb"] != DBNull.Value)
                        {
                            ourPLB = Convert.ToSingle(dataReader["plb"]);
                        }
                    }
                }
                //dataReader.Close();
                //connection.Close();
            }
            catch (SqlException exception)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
            }
            //Trace.TraceInformation("AccountingEngine.CalculateExpectedPrice exited : ourPLB = " + ourPLB);
            return Math.Round((costPrice * Convert.ToDecimal(ourPLB) / 100), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
        }
        /// <summary>
        /// Method calculates the commission of the agent
        /// </summary>
        /// <param name="grossFare">Gross Fare</param>
        /// <param name="agencyId">Agency Id</param>
        /// <param name="airlineCode">Airline Code</param>
        /// <returns>decimal</returns>
        private static decimal CalculateCommissionAgent(decimal grossFare, int agencyId, string airlineCode)
        {
            //Trace.TraceInformation("AccountingEngine.CalculateCommissionAgent entered : grossFare = " + grossFare);
            float commission = 0.0f;
            if (agencyId == 0)
            {
                return Convert.ToDecimal(commission);
            }
            try
            {
                //SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[2];
                //SqlDataReader dataReader;
                DataTable dt;
                paramList[0] = new SqlParameter("@agencyId", agencyId);
                paramList[1] = new SqlParameter("@airlineCode", airlineCode);

                // ## for multiple airlines
                if (airlineCode == "##")
                {
                    //dataReader=DBGateway.ExecuteReader(SPNames.GetDefaultCommission, new SqlParameter[0], connection)
                    dt = DBGateway.FillDataTableSP(SPNames.GetDefaultCommission, new SqlParameter[0]);
                }
                else
                {
                    //dataReader = DBGateway.ExecuteReader(SPNames.GetCommission, new SqlParameter[0], connection)
                    dt = DBGateway.FillDataTableSP(SPNames.GetCommission, paramList);
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow dataReader = dt.Rows[0];
                    if (dataReader != null && dataReader["commission"] != DBNull.Value)
                    {
                        commission = Convert.ToSingle(dataReader["commission"]);
                    }
                }
                //dataReader.Close();
                //connection.Close();
            }
            catch (SqlException exception)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
            }
            //Trace.TraceInformation("AccountingEngine.CalculateCommissionAgent exited : commission = " + commission);
            //decimal finalCommission = Convert.ToDecimal(commission) * grossFare / 100;
            //if (airlineCode.ToUpper() == "DN" && finalCommission < 125)
            //{
            //    finalCommission = 125;
            //}
            //return Math.Round(finalCommission);
            return Math.Round((Convert.ToDecimal(commission) * grossFare / 100), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
        }
        /// <summary>
        /// Method calculate our cost price(Gross Fare+our commission)
        /// </summary>
        /// <param name="grossFare">Gross Fare</param>
        /// <param name="airlineCode">Airline Code</param>
        /// <returns>CostPrice(in terms of rupees)</returns>
        private static decimal GetOurCommission(decimal grossFare, string airlineCode)
        {
            //Trace.TraceInformation("AccountingEngine.GetOurCostPrice entered : grossFare = " + grossFare);
            float ourCommission = 0.0f;
            try
            {
                //SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@airlineCode", airlineCode);
                //SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetAirlineCommission, paramList,connection);
                using (DataTable dtAirlines = DBGateway.FillDataTableSP(SPNames.GetAirlineCommission, paramList))
                {
                    if (dtAirlines != null && dtAirlines.Rows.Count > 0)
                    {
                        DataRow dataReader = dtAirlines.Rows[0];
                        if (dataReader != null && dataReader["commission"] != DBNull.Value)
                        {
                            ourCommission = Convert.ToSingle(dataReader["commission"]);
                        }
                    }
                }
                //dataReader.Close();
                //connection.Close();
            }
            catch (SqlException exception)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
            }
            //Trace.TraceInformation("AccountingEngine.GetOurCostPrice exited : our commission = " + ourCommission);
            return Math.Round((Convert.ToDecimal(ourCommission) * grossFare / 100),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
        }

        /// <summary>
        /// Method calculates the commission of the agent - For Hotel
        /// </summary>
        /// <param name="grossFare">Gross Fare</param>
        /// <param name="agencyId">Agency Id</param>
        /// <param name="hSource">hSource</param>
        /// <param name="nights">nights</param>
        /// <param name="noOfRooms">noOfRooms</param>
        /// <param name="rateOfExchange">rateOfExchange</param>
        /// <returns>Dictionary object</returns>
        public static Dictionary<string, decimal> CalculateCommission(decimal grossFare, int agencyId, HotelBookingSource hSource, int noOfRooms, int nights, decimal rateOfExchange)
        {
            //Trace.TraceInformation("AccountingEngine.CalculateCommissionAgent entered : grossFare = " + grossFare);
            float agentCommission = 0.0f;
            float ourCommission = 0.0f;
            int commissionType = 0; //  Commission Type Id, either percentage/Fixed    
            int fareType = 0; // Fare Type Id, either Net/Published       

            Dictionary<string, decimal> commissionList = new Dictionary<string, decimal>();
            if (agencyId == 0)
            {
                commissionList.Add("AgentCommission", Convert.ToDecimal(agentCommission));
                commissionList.Add("OurCommission", Convert.ToDecimal(ourCommission));
                return commissionList;
            }
            try
            {
                HotelSource hotelS = new HotelSource();
                if (hSource == HotelBookingSource.Desiya)
                {
                    hotelS.Load("Desiya", agencyId);
                }
                else if (hSource == HotelBookingSource.GTA)
                {
                    hotelS.Load("GTA", agencyId);
                }
                else if (hSource == HotelBookingSource.HotelBeds)
                {
                    hotelS.Load("HotelBeds", agencyId);
                }
                else if (hSource == HotelBookingSource.Tourico)
                {
                    hotelS.Load("Tourico", agencyId);
                }
                else if (hSource == HotelBookingSource.IAN)
                {
                    hotelS.Load("IAN", agencyId);
                }
                else if (hSource == HotelBookingSource.TBOConnect)
                {
                    hotelS.Load("TBOConnect", agencyId);
                }
                else if (hSource == HotelBookingSource.Miki)
                {
                    hotelS.Load("Miki", agencyId);
                }
                else if (hSource == HotelBookingSource.Travco)
                {
                    hotelS.Load("Travco", agencyId);
                }
                else if (hSource == HotelBookingSource.DOTW)
                {
                    hotelS.Load("DOTW", agencyId);
                }
                else if (hSource == HotelBookingSource.WST)
                {
                    hotelS.Load("WST", agencyId);
                }
                else
                {
                    hotelS.Load(hSource.ToString(), agencyId);
                }

                agentCommission = Convert.ToSingle(hotelS.AgentCommission);
                ourCommission = Convert.ToSingle(hotelS.OurCommission);
                commissionType = (int)hotelS.CommissionTypeId; // Percentage, fixed
                fareType = (int)hotelS.FareTypeId; // Published, Net
            }
            catch (SqlException exception)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
            }

            if (commissionType == (int)CommissionType.Percentage)
            {
                //remove Math.Round from commission
                if (fareType == (int)FareType.Published)
                {
                    commissionList.Add("AgentCommission", Convert.ToDecimal(agentCommission) * grossFare / 100);
                    commissionList.Add("OurCommission", Convert.ToDecimal(ourCommission) * grossFare / 100);
                }
                else if (fareType == (int)FareType.Net)
                {
                    commissionList.Add("AgentCommission", 0);
                    //commissionList.Add("OurCommission", Convert.ToDecimal(ourCommission) * grossFare / 100);
                    commissionList.Add("OurCommission", Convert.ToDecimal(ourCommission) * grossFare / 100);
                }
            }
            if (commissionType == (int)CommissionType.Fixed)
            {
                // For Fixed Fare Type we need to calculate our commission on per Room/Per night basis
                decimal calcOurComm = 0;
                decimal calcAgentComm = 0;
                if (grossFare != 0)
                {
                    calcOurComm = Convert.ToDecimal(ourCommission * nights * noOfRooms);
                    calcAgentComm = Convert.ToDecimal(agentCommission * nights * noOfRooms);
                }
                else
                {
                    calcOurComm = 0;
                    calcAgentComm = 0;
                }

                if (fareType == (int)FareType.Published)
                {
                    commissionList.Add("AgentCommission", calcAgentComm / rateOfExchange);
                    commissionList.Add("OurCommission", calcOurComm / rateOfExchange);
                }
                else if (fareType == (int)FareType.Net)
                {
                    commissionList.Add("AgentCommission", 0);
                    commissionList.Add("OurCommission", calcOurComm / rateOfExchange);
                }
            }
            //Trace.TraceInformation("AccountingEngine.CalculateCommissionAgent exit");
            return commissionList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="grossFare">Gross Fare</param>
        /// <param name="agencyId">Agency Id</param>
        /// <param name="hSource">hSource</param>
        /// <param name="noOfRooms">noOfRooms</param>
        /// <param name="nights">nights</param>
        /// <param name="rateOfExchange">rateOfExchange</param>
        /// <param name="productType">productType</param>
        /// <returns>Dictionary object</returns>
        public static Dictionary<string, decimal> CalculateCommission(decimal grossFare, int agencyId, HotelBookingSource hSource, int noOfRooms, int nights, decimal rateOfExchange, ProductType productType)
        {
            //Trace.TraceInformation("AccountingEngine.CalculateCommissionAgent entered : grossFare = " + grossFare);
            decimal markup = 0;
            decimal discount = 0;
            string markupType = string.Empty; //  Commission Type Id, either percentage/Fixed    
            string discountType = string.Empty; // Fare Type Id, either Net/Published 
            int fareType = 0;

            Dictionary<string, decimal> commissionList = new Dictionary<string, decimal>();
            //if (agencyId == 0)
            //{
            //    commissionList.Add("AgentCommission", Convert.ToDecimal(agentCommission));
            //    commissionList.Add("OurCommission", Convert.ToDecimal(ourCommission));
            //    return commissionList;
            //}
            try
            {
                DataTable dtMarkup=UpdateMarkup.Load(agencyId,hSource.ToString(),(int)productType);
                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    DataRow dr = dtMarkup.Rows[0];
                    markup = Convert.ToDecimal(dr["Markup"]);
                    discount = Convert.ToDecimal(dr["Discount"]);
                    markupType = Convert.ToString(dr["MarkupType"]); // Percentage, fixed
                    discountType = Convert.ToString(dr["DiscountType"]);// Percentage, fixed
                }
                HotelSource hotelS = new HotelSource();
                hotelS.Load(hSource.ToString(), agencyId);
                fareType = (int)hotelS.FareTypeId;  // Published, Net
            }
            catch (SqlException exception)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
            }

            if (markupType == "P")
            {
                if (fareType == (int)FareType.Published)
                {
                    commissionList.Add("Markup", Convert.ToDecimal(markup) * grossFare / 100);
                }
                else if (fareType == (int)FareType.Net)
                {
                    commissionList.Add("Markup", Convert.ToDecimal(markup) * grossFare / 100);
                    //commissionList.Add("Markup", 0);
                }
            }
            else if (markupType == "F")
            {
                decimal calcMarkup = 0;
                if (grossFare != 0)
                {
                    calcMarkup = Convert.ToDecimal(markup * noOfRooms);
                }
                else
                {
                    calcMarkup = markup;
                }

                //if (fareType == (int)FareType.Published)
                //{
                //    commissionList.Add("Markup", calcMarkup / rateOfExchange);

                //}
                //else if (fareType == (int)FareType.Net)
                //{
                //    commissionList.Add("Markup", calcMarkup);
                //}
                commissionList.Add("Markup", calcMarkup);
            }

            if (discountType == "P")
            {
                //if (fareType == (int)FareType.Published)
                //{
                //    commissionList.Add("Discount", Convert.ToDecimal(discount) * grossFare / 100);
                //}
                //else if (fareType == (int)FareType.Net)
                //{
                //    commissionList.Add("Discount", 0);
                //}
                commissionList.Add("Discount", Convert.ToDecimal(discount) * grossFare / 100);
            }
            else if (discountType == "F")
            {
                decimal calcDiscount = 0;
                if (grossFare != 0)
                {
                    calcDiscount = Convert.ToDecimal(discount);
                }
                else
                {
                    calcDiscount = 0;
                }

                //if (fareType == (int)FareType.Published)
                //{
                //    commissionList.Add("Discount", calcDiscount / rateOfExchange);

                //}
                //else if (fareType == (int)FareType.Net)
                //{
                //    commissionList.Add("Discount", 0);
                //}
                commissionList.Add("Discount", calcDiscount);
            }
            //Trace.TraceInformation("AccountingEngine.CalculateCommissionAgent exit");
            return commissionList;
        }

        /// <summary>
        /// Method calculates the commission of the agent - For Transfer
        /// </summary>
        /// <param name="grossFare">Gross Fare</param>
        /// <param name="agencyId">Agency Id</param>
        /// <param name="rateOfExchange">rateOfExchange</param>
        /// <param name="tSource">tSource</param>
        /// <returns>Dictionary object</returns>
        public static Dictionary<string, decimal> CalculateCommission(decimal grossFare, int agencyId, TransferBookingSource tSource, decimal rateOfExchange)
        {
            //Trace.TraceInformation("AccountingEngine.CalculateCommissionAgent entered : grossFare = " + grossFare);
            float agentCommission = 0.0f;
            float ourCommission = 0.0f;
            int commissionType = 0; //  Commission Type Id, either percentage/Fixed    
            int fareType = 0; // Fare Type Id, either Net/Published       

            Dictionary<string, decimal> commissionList = new Dictionary<string, decimal>();
            if (agencyId == 0)
            {
                commissionList.Add("AgentCommission", Convert.ToDecimal(agentCommission));
                commissionList.Add("OurCommission", Convert.ToDecimal(ourCommission));
                return commissionList;
            }
            try
            {
                TransferSource transferS = new TransferSource();
                if (tSource == TransferBookingSource.GTA)
                {
                    transferS.Load("GTA");
                }
                agentCommission = Convert.ToSingle(transferS.AgentCommission);
                ourCommission = Convert.ToSingle(transferS.OurCommission);
                commissionType = (int)transferS.CommissionTypeId; // Percentage, fixed
                fareType = (int)transferS.FareTypeId; // Published, Net
            }
            catch (SqlException exception)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
            }

            if (commissionType == (int)CommissionType.Percentage)
            {
                //remove Math.Round from commission
                if (fareType == (int)FareType.Published)
                {
                    commissionList.Add("AgentCommission", Convert.ToDecimal(agentCommission) * grossFare / 100);
                    commissionList.Add("OurCommission", Convert.ToDecimal(ourCommission) * grossFare / 100);
                }
                else if (fareType == (int)FareType.Net)
                {
                    commissionList.Add("AgentCommission", 0);
                    commissionList.Add("OurCommission", Convert.ToDecimal(ourCommission) * grossFare / 100);
                }
            }
            if (commissionType == (int)CommissionType.Fixed)
            {
                // For Fixed Fare Type we need to calculate our commission on per Room/Per night basis
                decimal calcOurComm = 0;
                decimal calcAgentComm = 0;
                if (grossFare != 0)
                {
                    calcOurComm = Convert.ToDecimal(ourCommission);
                    calcAgentComm = Convert.ToDecimal(agentCommission);
                }
                else
                {
                    calcOurComm = 0;
                    calcAgentComm = 0;
                }

                if (fareType == (int)FareType.Published)
                {
                    commissionList.Add("AgentCommission", calcAgentComm / rateOfExchange);
                    commissionList.Add("OurCommission", calcOurComm / rateOfExchange);
                }
                else if (fareType == (int)FareType.Net)
                {
                    commissionList.Add("AgentCommission", 0);
                    commissionList.Add("OurCommission", calcOurComm / rateOfExchange);
                }
            }
            //Trace.TraceInformation("AccountingEngine.CalculateCommissionAgent exit");
            return commissionList;
        }
        /// <summary>
        /// This Method is used to caluculate commission for Sightseeings.
        /// </summary>
        /// <param name="grossFare">grossFare</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="sSource">sSource</param>
        /// <param name="rateOfExchange">rateOfExchange</param>
        /// <returns>Dictionary object</returns>
       /* public static Dictionary<string, decimal> CalculateCommission(decimal grossFare, int agencyId, SightseeingBookingSource Source, decimal rateOfExchange) ziya-todo
        {
            Trace.TraceInformation("AccountingEngine.CalculateCommissionAgent entered : grossFare = " + grossFare);
            float agentCommission = 0.0f;
            float ourCommission = 0.0f;
            int commissionType = 0; //  Commission Type Id, either percentage/Fixed    
            int fareType = 0; // Fare Type Id, either Net/Published       

            Dictionary<string, decimal> commissionList = new Dictionary<string, decimal>();
            if (agencyId == 0)
            {
                commissionList.Add("AgentCommission", Convert.ToDecimal(agentCommission));
                commissionList.Add("OurCommission", Convert.ToDecimal(ourCommission));
                return commissionList;
            }
            try
            {
                SightseeingSource sourceInfo = new SightseeingSource();
                if (Source == SightseeingBookingSource.GTA)
                {
                    sourceInfo.Load("GTA");
                }
                agentCommission = Convert.ToSingle(sourceInfo.AgentCommission);
                ourCommission = Convert.ToSingle(sourceInfo.OurCommission);
                commissionType = (int)sourceInfo.CommissionTypeId; // Percentage, fixed
                fareType = (int)sourceInfo.FareTypeId; // Published, Net
            }
            catch (SqlException exception)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
            }

            if (commissionType == (int)CommissionType.Percentage)
            {
                //remove Math.Round from commission
                if (fareType == (int)FareType.Published)
                {
                    commissionList.Add("AgentCommission", Convert.ToDecimal(agentCommission) * grossFare / 100);
                    commissionList.Add("OurCommission", Convert.ToDecimal(ourCommission) * grossFare / 100);
                }
                else if (fareType == (int)FareType.Net)
                {
                    commissionList.Add("AgentCommission", 0);
                    commissionList.Add("OurCommission", Convert.ToDecimal(ourCommission) * grossFare / 100);
                }
            }
            if (commissionType == (int)CommissionType.Fixed)
            {
                // For Fixed Fare Type we need to calculate our commission on per Room/Per night basis
                decimal calcOurComm = 0;
                decimal calcAgentComm = 0;
                if (grossFare != 0)
                {
                    calcOurComm = Convert.ToDecimal(ourCommission);
                    calcAgentComm = Convert.ToDecimal(agentCommission);
                }
                else
                {
                    calcOurComm = 0;
                    calcAgentComm = 0;
                }

                if (fareType == (int)FareType.Published)
                {
                    commissionList.Add("AgentCommission", calcAgentComm / rateOfExchange);
                    commissionList.Add("OurCommission", calcOurComm / rateOfExchange);
                }
                else if (fareType == (int)FareType.Net)
                {
                    commissionList.Add("AgentCommission", 0);
                    commissionList.Add("OurCommission", calcOurComm / rateOfExchange);
                }
            }
            Trace.TraceInformation("AccountingEngine.CalculateCommissionAgent exit");
            return commissionList;
        }*/

        /// <summary>
        /// Method give the plb in percentage ofr a particular airline
        /// </summary>
        /// <param name="agencyId">AgencyId</param>
        /// <param name="airlineCode">Airline code</param>
        /// <returns>float</returns>
        public static float CalculatePLBAgentInPercent(int agencyId, string airlineCode)
        {
            //Trace.TraceInformation("AccountingEngine.CalculatePLBAgentInPercent entered :");
            float plb = 0.0f;
            if (agencyId == 0)
            {
                return plb;
            }
            try
            {
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@agencyId", agencyId);
                paramList[1] = new SqlParameter("@airlineCode", airlineCode);
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetPLB, paramList,connection);
                if (dataReader.Read())
                {
                    plb = Convert.ToSingle(dataReader["plb"]);
                }
                dataReader.Close();
                connection.Close();
            }
            catch (SqlException exception)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
            }
            //Trace.TraceInformation("AccountingEngine.CalculatePLBAgentInPercent exited : plb = " + plb);
            return plb;
        }

        /// <summary>
        /// Gets the flight is soto or not
        /// </summary>
        /// <param name="origin">Origin country code</param>
        /// <param name="destination">Destination Country Code</param>
        /// <param name="itinerary">FlightItinerary object to find selected flight isLCC</param>
        /// <returns>bool</returns>
        private static bool IsSOTO(string origin, string destination, FlightItinerary itinerary)
        {
            if (origin != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "" && !IsLCC(itinerary))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the flight is soto or not
        /// </summary>
        /// <param name="origin">Origin country code</param>
        /// <param name="destination">Destination Country Code</param>
        /// <param name="result">SearchResult object to find selected flight isLCC</param>
        /// <returns>bool</returns>
        //private static bool IsSOTO(string origin, string destination, CT.BookingEngine.SearchResult result)
        //{
        //    if (origin != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "" && !IsLCC(result))
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        /// <summary>
        /// Calculates plb as per rules defined in airline
        /// </summary>
        /// <param name="netFare">Amount on which plb is applied</param>
        /// <param name="agencyId">agencyId - 0 indicates admin</param>
        /// <param name="airlineCode">airline code</param>
        /// <param name="paxType">pax type</param>
        /// <param name="bookingClass">booking class</param>
        /// <param name="departureDate">departure date</param>
        /// <param name="origin">origin</param>
        /// <param name="destination">destination</param>
        /// <returns>Total agent plb (plb out in rs)</returns>
        private static decimal CalculatePLBAgent(decimal netFare, int agencyId, string airlineCode, string paxType, string bookingClass, DateTime departureDate, string origin, string destination) 
         {
             //Trace.TraceInformation("AccountingEngine.CalculatePLBAgent entered : netFare = " + netFare);

             float plb = 0.0f;
             float tempPlb = 0.0f;
             string[] classes = bookingClass.Split(',');
             Airport originAirport = new Airport(origin);
             Airport destinationAirport = new Airport(destination);
             List<RegionTEMP> originRegions = RegionTEMP.GetRegions(originAirport.CityCode);
             List<RegionTEMP> destinationRegions = RegionTEMP.GetRegions(destinationAirport.CityCode);
             string csvOriginRegions = string.Empty;
             string csvDestinationRegions = string.Empty;
             if (originRegions != null)
             {
                 foreach (RegionTEMP region in originRegions)
                 {
                     if (csvOriginRegions.Length == 0)
                     {
                         csvOriginRegions = region.RegionCode;
                     }
                     else
                     {
                         csvOriginRegions = csvOriginRegions + "," + region.RegionCode;
                     }
                 }
             }
             if (destinationRegions != null)
             {
                 foreach (RegionTEMP region in destinationRegions)
                 {
                     if (csvDestinationRegions.Length == 0)
                     {
                         csvDestinationRegions = region.RegionCode;
                     }
                     else
                     {
                         csvDestinationRegions = csvDestinationRegions + "," + region.RegionCode;
                     }
                 }
             }
             if (agencyId == 0)
             {
                 return Convert.ToDecimal(plb);
             }
             try
             {
                 float previousPlb = 0.0f;
                 //PLBRuleMaster plbRuleMaster=new PLBRuleMaster();
                 SqlConnection connection = DBGateway.GetConnection();

                 for (int i = 0; i < classes.Length; i++)
                 {
                     SqlParameter[] paramList = new SqlParameter[12];
                     paramList[0] = new SqlParameter("@airlineCode", airlineCode);
                     paramList[1] = new SqlParameter("@paxType", paxType);
                     paramList[3] = new SqlParameter("@departureDate", departureDate);
                     paramList[4] = new SqlParameter("@origin", origin);
                     paramList[5] = new SqlParameter("@destination", destination);
                     paramList[6] = new SqlParameter("@originCityCode", originAirport.CityCode);
                     paramList[7] = new SqlParameter("@originCountryCode", originAirport.CountryCode);
                     paramList[8] = new SqlParameter("@originRegionCodes", csvOriginRegions);
                     paramList[9] = new SqlParameter("@destinationCityCode", destinationAirport.CityCode);
                     paramList[10] = new SqlParameter("@destinationCountryCode", destinationAirport.CountryCode);
                     paramList[11] = new SqlParameter("@destinationRegionCodes", csvDestinationRegions);
                     paramList[2] = new SqlParameter("@bookingClass", Convert.ToString(classes[i].Trim()));
                     SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetPLBFromRule, paramList, connection);
                     if (dataReader.Read())
                     {
                         tempPlb = Convert.ToSingle(dataReader["plbOut"]);
                         if (tempPlb == 0)
                         {
                             plb = 0.0f;
                         }
                         else
                         {
                             paramList = new SqlParameter[2];
                             paramList[0] = new SqlParameter("@agencyId", agencyId);
                             paramList[1] = new SqlParameter("@airlineCode", airlineCode);
                             dataReader.Close();
                             SqlDataReader dataReader2 = DBGateway.ExecuteReaderSP(SPNames.GetPLB, paramList, connection);
                             if (dataReader2.Read())
                             {
                                 plb = Convert.ToSingle(dataReader2["plb"]);

                             }
                             if (Convert.ToBoolean(dataReader2["isException"]))
                             {
                                 if (tempPlb < plb)
                                 {
                                     plb = tempPlb;
                                 }
                             }
                             else
                             {
                                 plb = tempPlb;
                             }
                             dataReader2.Close();
                         }
                     }
                     else
                     {
                         paramList = new SqlParameter[2];
                         paramList[0] = new SqlParameter("@agencyId", agencyId);
                         paramList[1] = new SqlParameter("@airlineCode", airlineCode);
                         dataReader.Close();
                         SqlDataReader dataReader3 = DBGateway.ExecuteReaderSP(SPNames.GetPLB, paramList, connection);
                         if (dataReader3.Read())
                         {
                             plb = Convert.ToSingle(dataReader3["plb"]);
                             float defaultPlb = 0;
                             if (dataReader3.FieldCount == 3)
                             {
                                 defaultPlb = Convert.ToSingle(dataReader3["defaultPlb"]);
                                 if (defaultPlb <= plb)
                                 {
                                     plb = defaultPlb;
                                 }
                             }
                         }
                         dataReader3.Close();
                     }
                     if (!dataReader.IsClosed)
                     {
                         dataReader.Close();
                     }
                     if (i == 0)
                     {
                         previousPlb = plb;
                     }
                     else
                     {
                         if (plb < previousPlb)
                         {
                             previousPlb = plb;
                         }
                         else
                         {
                             plb = previousPlb;
                         }
                     }
                 }


                 ////connection.Close();
             }
             catch (SqlException exception)
             {
                 CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
             }
             //Trace.TraceInformation("AccountingEngine.CalculatePLBAgent exited : plb = " + plb);
             return Math.Round((Convert.ToDecimal(plb) * netFare / 100),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
         }
        /// <summary>
         /// Calculates plb as per rules defined i airline
         /// </summary>
         /// <param name="costPrice">Amount on which we take plb</param>
         /// <param name="airlineCode">airline Code</param>
         /// <param name="paxType">Passenger type</param>
         /// <param name="bookingClass">Booking Class</param>
         /// <param name="departureDate">departure date</param>
         /// <param name="origin">origin-three digit code</param>
         /// <param name="destination">destination-three digit code</param>
         /// <returns>PLB in(in rs)</returns>
         private static decimal CalculateOurPLB(decimal costPrice, string airlineCode, string paxType, string bookingClass, DateTime departureDate, string origin, string destination)
         {
             //Trace.TraceInformation("AccountingEngine.CalculateExpectedPrice entered : costPrice = " + costPrice);
             float ourPLB = 0.0f;
             float tempPlb = 0.0f;
             Airport originAirport = new Airport(origin);
             Airport destinationAirport = new Airport(destination);
             List<RegionTEMP> originRegions = RegionTEMP.GetRegions(originAirport.CityCode);
             List<RegionTEMP> destinationRegions = RegionTEMP.GetRegions(destinationAirport.CityCode);
             string csvOriginRegions = string.Empty;
             string csvDestinationRegions = string.Empty;
             if (originRegions != null)
             {
                 foreach (RegionTEMP region in originRegions)
                 {
                     if (csvOriginRegions.Length == 0)
                     {
                         csvOriginRegions = region.RegionCode;
                     }
                     else
                     {
                         csvOriginRegions = csvOriginRegions + "," + region.RegionCode;
                     }
                 }
             }
             if (destinationRegions != null)
             {
                 foreach (RegionTEMP region in destinationRegions)
                 {
                     if (csvDestinationRegions.Length == 0)
                     {
                         csvDestinationRegions = region.RegionCode;
                     }
                     else
                     {
                         csvDestinationRegions = csvDestinationRegions + "," + region.RegionCode;
                     }
                 }
             }
             try
             {
                 SqlConnection connection = DBGateway.GetConnection();
                 SqlParameter[] paramList = new SqlParameter[12];
                 paramList[0] = new SqlParameter("@airlineCode", airlineCode);
                 paramList[1] = new SqlParameter("@paxType", paxType);
                 paramList[2] = new SqlParameter("@bookingClass", bookingClass);
                 paramList[3] = new SqlParameter("@departureDate", departureDate);
                 paramList[4] = new SqlParameter("@origin", origin);
                 paramList[5] = new SqlParameter("@destination", destination);
                 paramList[6] = new SqlParameter("@originCityCode", originAirport.CityCode);
                 paramList[7] = new SqlParameter("@originCountryCode", originAirport.CountryCode);
                 paramList[8] = new SqlParameter("@originRegionCodes", csvOriginRegions);
                 paramList[9] = new SqlParameter("@destinationCityCode", destinationAirport.CityCode);
                 paramList[10] = new SqlParameter("@destinationCountryCode", destinationAirport.CountryCode);
                 paramList[11] = new SqlParameter("@destinationRegionCodes", csvDestinationRegions);
                 SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetPLBFromRule, paramList, connection);
                 if (dataReader.Read())
                 {
                     if (dataReader.FieldCount == 2)
                     {
                         tempPlb = Convert.ToSingle(dataReader["plbIn"]);
                         ourPLB = tempPlb;
                     }
                 }
                 else
                 {
                     paramList = new SqlParameter[1];
                     paramList[0] = new SqlParameter("@airlineCode", airlineCode);
                     dataReader.Close();
                     SqlDataReader dataReader2 = DBGateway.ExecuteReaderSP(SPNames.GetAirlinePLB, paramList, connection);
                     if (dataReader2.Read())
                     {
                         ourPLB = Convert.ToSingle(dataReader2["plb"]);
                     }
                     dataReader2.Close();
                 }
                 if (!dataReader.IsClosed)
                 {
                     dataReader.Close();
                 }
                 connection.Close();
             }
             catch (SqlException exception)
             {
                 CT.Core.Audit.Add(CT.Core.EventType.Price, CT.Core.Severity.Normal, 0, exception.Message, "0");
             }
             //Trace.TraceInformation("AccountingEngine.CalculateExpectedPrice exited : ourPLB = " + ourPLB);
             return Math.Round( (costPrice * Convert.ToDecimal(ourPLB) / 100),Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
         }
        /// <summary>
        /// Method gets the price for the whitelabel customer also.
        /// </summary>
        /// <param name="result">result</param>
        /// <param name="fareBreakdownIndex">fareBreakdownIndex</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="otherCharges">otherCharges</param>
        /// <param name="isWhitelabel">isWhitelabel</param>
        /// <returns>PriceAccounts</returns>
        /*public static PriceAccounts GetPrice(SearchResult result, int fareBreakdownIndex, int agencyId, decimal otherCharges, bool isWhitelabel) ziya-todo
        {
          
            
            if (isWhitelabel)
            {
                PriceAccounts price = GetPrice(result, fareBreakdownIndex, agencyId, otherCharges);
                string airlineCode = result.Flights[0][0].Airline;
                for (int g = 0; g < result.Flights.Length; g++)
                {
                    for (int f = 0; f < result.Flights[g].Length; f++)
                    {
                        if (airlineCode != result.Flights[g][f].Airline)
                        {
                            airlineCode = "##";
                            break;
                        }
                    }
                }

                WLAccountingEngine.PriceWithWLDiscount(ref price, agencyId, airlineCode);

                return price;
            }
            else
            {
                return GetPrice(result, fareBreakdownIndex, agencyId, otherCharges);
            }
        }
        

        

        /// <summary>
        /// Gets the flight is lcc or not
        /// </summary>
        /// <param name="result">SearchResult object to find selected flight isLCC</param>
        /// <returns></returns>
        private static bool IsLCC(SearchResult result)
        {
            bool isLCC = false;
            Airline airline = new Airline();
            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    airline.Load(result.Flights[i][j].Airline);
                    if (airline.IsLCC)
                    {
                        isLCC = true;
                        break;
                    }
                }
                if (isLCC)
                {
                    break;
                }

            }
            return isLCC;
        }*/

        /// <summary>
        /// Gets the flight is lcc or not
        /// </summary>
        /// <param name="itinerary">FlightItinerary object to find selected flight isLCC</param>
        /// <returns>bool</returns>
        private static bool IsLCC(FlightItinerary itinerary)
        {
            bool isLCC = false;
            Airline airline = new Airline();
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                airline.Load(itinerary.Segments[i].Airline);
                if (airline.IsLCC)
                {
                    isLCC = true;
                    break;
                }
            }
            return isLCC;
        }

        /// <summary>
        /// Method gets the price for the Hotel whitelabel customer also.
        /// </summary>
        /// <param name="result">result</param>
        /// <param name="nights">nights</param>
        /// <param name="noofRooms">noofRooms</param>
        /// <param name="pType">pType</param>
        /// <param name="roomTypeCode">roomTypeCode</param>
        /// <param name="agencyId">agencyId</param>
        /// <param name="otherCharges">otherCharges</param>
        /// <param name="isWhitelabel">isWhitelabel</param>
        /// <returns>PriceAccounts</returns>
        public static PriceAccounts GetPrice(HotelSearchResult result, string roomTypeCode, int agencyId, decimal otherCharges, int noofRooms, int nights, PriceType pType, bool isWhitelabel)
        {
            if (isWhitelabel)
            {
                PriceAccounts price = GetPrice(result, roomTypeCode, agencyId, otherCharges, noofRooms, nights, pType);

               // WLAccountingEngine.PriceWithHotelWLDiscount(ref price, agencyId, pType); ziya-todo

                return price;
            }
            else
            {
                return GetPrice(result, roomTypeCode, agencyId, otherCharges, noofRooms, nights, pType);
            }
        }

      }
}
