using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.AccountingEngine
{
    public class AirlineCommission
    {
        #region Private fields
        /// <summary>
        /// unique key of airlineSrvice fee
        /// </summary>
        private int airlineCommissionId;
        /// <summary>
        /// 2 char Airline code 
        /// </summary>
        private string airlineCode;        
        /// <summary>
        /// value of airline commission
        /// </summary>
        private decimal commission;
        /// <summary>
        /// UniqueId of an agency
        /// </summary>        
        private int agencyId;
        /// <summary>
        /// check weather a row is active or not
        /// </summary>
        private bool isActive;
        /// <summary>
        /// Unique ID of the member who created this record
        /// </summary>        
        private int createdBy;
        /// <summary>
        /// Date when the record was created
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// Unique ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;
        /// <summary>
        /// Date when the record was last modified
        /// </summary>
        private DateTime lastModifiedOn;

        #endregion

        #region Public Properties
        /// <summary>
        /// 2 char Airline code 
        /// </summary>
        public int AirlineCommissionId
        {
            get { return airlineCommissionId; }
            set { airlineCommissionId = value; }
        }
        /// <summary>
        /// 2 char Airline code 
        /// </summary>
        public string AirlineCode
        {
            get { return airlineCode; }
            set { airlineCode = value; }
        }        
        /// <summary>
        ///  value of service fee
        /// </summary>
        public decimal Commission
        {
            get { return commission; }
            set { commission = value; }
        }
        /// <summary>
        /// agencyId property
        /// </summary>
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }
        /// <summary>
        /// Gets Id of member who created the record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }
        /// <summary>
        /// Gets the date when the record was created on
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets Id of member who modified the record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets the date when the record was last Modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        #endregion 

        # region Class Methods

        /// <summary>
        /// Saves the Airline Commission
        /// </summary>
        //public void Save()
        public int Save()
        {
            if (airlineCode.Trim().Length <= 0)
            {
                throw new ArgumentException("airlinecode can't be null or empty");
            }
            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
            }
            SqlParameter[] paramList = new SqlParameter[6];
            int i = 1;
            paramList[i++] = new SqlParameter("@airlineCode", airlineCode);
            paramList[i++] = new SqlParameter("@commission", commission);            
            paramList[i++] = new SqlParameter("@agencyId", agencyId);
            paramList[i++] = new SqlParameter("@isActive", isActive);
            int rowsAffected = 0;
            if (airlineCommissionId > 0)
            {
                if (lastModifiedBy <= 0)
                {
                    throw new ArgumentException("lastModifiedBy must have a positive non zero integer value", "lastModifiedBy");
                }
                paramList[0] = new SqlParameter("@airlineCommissionId", airlineCommissionId);
                paramList[i++] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateAirlineCommission, paramList);
            }
            else
            {
                if (createdBy <= 0)
                {
                    throw new ArgumentException("createdBy must have a positive non zero integer value", "createdBy");
                }
                paramList[0] = new SqlParameter("@airlineCommissionId", SqlDbType.Int);
                paramList[0].Direction = ParameterDirection.Output;
                paramList[i++] = new SqlParameter("@createdBy", createdBy);
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddAirlineCommission, paramList);

            }
            airlineCommissionId = Convert.ToInt32(paramList[0].Value);
            if (airlineCommissionId == -1)
            {
                throw new ArgumentException("Entry already exist for this airline");
            }
            return airlineCommissionId;
        }

        /// <summary>
        /// To Get Airline commission of a agency for all airline
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static List<AirlineCommission> GetAirlineCommissionOfAgency(int agencyId)
        {
            List<AirlineCommission> tempList = new List<AirlineCommission>();
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAirlineCommissionForAgency, paramList, connection);
            if (data.HasRows)
            {
                while (data.Read())
                {
                    AirlineCommission ac = new AirlineCommission();
                    ac.AirlineCommissionId = Convert.ToInt32(data["airlineCommissionId"]);
                    ac.agencyId = Convert.ToInt32(data["agencyId"]);
                    ac.airlineCode = Convert.ToString(data["airLineCode"]);
                    ac.Commission = Convert.ToDecimal(data["commission"]);
                    ac.isActive = Convert.ToBoolean(data["isActive"]);
                    ac.createdOn = Convert.ToDateTime(data["createdOn"]);
                    ac.createdBy = Convert.ToInt32(data["createdBy"]);
                    ac.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                    ac.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    tempList.Add(ac);
                }
            }
            data.Close();
            connection.Close();
            return tempList;
        }

        /// <summary>
        /// To Get Airline commission of a agency for all airline
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static List<AirlineCommission> GetActiveAirlineCommissionOfAgency(int agencyId)
        {
            List<AirlineCommission> tempList = new List<AirlineCommission>();
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetActiveAirlineCommissionForAgency, paramList, connection);
            using (DataTable dtActiveAirline = DBGateway.FillDataTableSP(SPNames.GetActiveAirlineCommissionForAgency, paramList))
            {
                if (dtActiveAirline != null && dtActiveAirline.Rows.Count > 0)
                {
                    foreach (DataRow data in dtActiveAirline.Rows)
                    {
                        AirlineCommission ac = new AirlineCommission();
                        ac.AirlineCommissionId = Convert.ToInt32(data["airlineCommissionId"]);
                        ac.agencyId = Convert.ToInt32(data["agencyId"]);
                        ac.airlineCode = Convert.ToString(data["airLineCode"]);
                        ac.Commission = Convert.ToDecimal(data["commission"]);
                        ac.isActive = Convert.ToBoolean(data["isActive"]);
                        ac.createdOn = Convert.ToDateTime(data["createdOn"]);
                        ac.createdBy = Convert.ToInt32(data["createdBy"]);
                        ac.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                        ac.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                        tempList.Add(ac);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            return tempList;
        }

        /// <summary>
        /// To get AirlineCommission of a particular airline of any agency
        /// </summary>
        /// <param name="airlineCode"></param>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static AirlineCommission Load(string airlineCode, int agencyId)
        {
            AirlineCommission ac = new AirlineCommission();
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
           // SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@airlineCode", airlineCode);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAirlineCommissionOfAirlineForAgency, paramList, connection);
            using (DataTable dtAirline = DBGateway.FillDataTableSP(SPNames.GetAirlineCommissionOfAirlineForAgency, paramList))
            {
                if (dtAirline !=null && dtAirline.Rows.Count > 0)
                {
                    foreach (DataRow data in dtAirline.Rows)
                    {
                        ac.airlineCommissionId = Convert.ToInt32(data["airlineCommissionId"]);
                        ac.agencyId = Convert.ToInt32(data["agencyId"]);
                        ac.airlineCode = Convert.ToString(data["airLineCode"]);
                        ac.commission = Convert.ToDecimal(data["commission"]);
                        ac.isActive = Convert.ToBoolean(data["isActive"]);
                        ac.createdOn = Convert.ToDateTime(data["createdOn"]);
                        ac.createdBy = Convert.ToInt32(data["createdBy"]);
                        ac.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                        ac.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            return ac;
        }

        /// <summary>
        /// To Acivate or Deactivate the Airline commission
        /// </summary>
        /// <param name="airlineCommissionId"></param>
        /// <param name="isActive"></param>
        public static void ActivateDeactiveAirlineCommission(int airlineCommissionId, bool isActive)
        {
            int rowsAffected = 0;
            if (airlineCommissionId > 0)
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@airlineCommissionId", airlineCommissionId);
                paramList[1] = new SqlParameter("@isActive", isActive);
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.ActivateDeactiveAirlineCommission, paramList);
            }
            else
            {
                throw new ArgumentException("airlineCommissionId should have positive integer value", "airlineCommissionId");
            }
        }
        #endregion
    }
}
