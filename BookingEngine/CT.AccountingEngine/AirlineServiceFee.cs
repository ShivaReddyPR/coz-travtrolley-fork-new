using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using CT.BookingEngine;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.AccountingEngine
{
    public class AirlineServiceFee
    {
        #region Private fields
        /// <summary>
        /// unique key of airlineSrvice fee
        /// </summary>
        private int airlineServiceFeeId;
        /// <summary>
        /// 2 char Airline code 
        /// </summary>
        private string airlineCode;
        /// <summary>
        /// Type of Service fee percentage or fixed
        /// </summary>
        private ServiceFeeType  serviceFeeType;
        /// <summary>
        /// value of service fee
        /// </summary>
        private decimal serviceFee;
        /// <summary>
        /// UniqueId of an agency
        /// </summary>        
        private int agencyId;
        /// <summary>
        /// check weather a row is valid or not
        /// </summary>
        private bool isActive;
        /// <summary>
        /// Unique ID of the member who created this record
        /// </summary>        
        private int createdBy;
        /// <summary>
        /// Date when the record was created
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// Unique ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;
        /// <summary>
        /// Date when the record was last modified
        /// </summary>
        private DateTime lastModifiedOn;

        #endregion

        #region Public Properties
        /// <summary>
        /// 2 char Airline code 
        /// </summary>
        public int AirlineServiceFeeId
        {
            get { return airlineServiceFeeId; }
            set { airlineServiceFeeId = value; }
        }
        /// <summary>
        /// 2 char Airline code 
        /// </summary>
        public string AirlineCode
        {
            get { return airlineCode; }
            set { airlineCode = value; }
        }
        /// <summary>
        /// Type of Service fee percentage or fixed
        /// </summary>
        public ServiceFeeType ServiceFeeType
        {
            get { return serviceFeeType; }
            set { serviceFeeType = value; }
        }
        /// <summary>
        ///  value of service fee
        /// </summary>
        public decimal ServiceFee
        {
            get { return serviceFee; }
            set { serviceFee = value; }
        }
        /// <summary>
        /// agencyId property
        /// </summary>
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }
        /// <summary>
        /// Gets Id of member who created the record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }
        /// <summary>
        /// Gets the date when the record was created on
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets Id of member who modified the record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets the date when the record was last Modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        #endregion 

        # region Class Methods

        /// <summary>
        /// Saves the ledger object
        /// </summary>
        //public void Save()
        public int Save()
        {            
            if (airlineCode.Trim().Length  <= 0)
            {
                throw new ArgumentException("airlinecode can't be null or empty");
            }
            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
            }
            //if (createdBy <= 0)
            //{
            //    throw new ArgumentException("createdBy must have a positive non zero integer value", "createdBy");
            //}
            SqlParameter[] paramList = new SqlParameter[7];
            
            paramList[1] = new SqlParameter("@airlineCode", airlineCode);
            paramList[2] = new SqlParameter("@serviceFeeTypeId", (int)serviceFeeType);
            paramList[3] = new SqlParameter("@serviceFee", serviceFee);
            paramList[4] = new SqlParameter("@agencyId", agencyId);
            paramList[5] = new SqlParameter("@isActive", isActive);
            int rowsAffected = 0;
            if (airlineServiceFeeId > 0)
            {
                if (lastModifiedBy <= 0)
                {
                    throw new ArgumentException("lastModifiedBy must have a positive non zero integer value", "lastModifiedBy");
                }
                paramList[0] = new SqlParameter("@airlineServiceFeeId", airlineServiceFeeId);
                paramList[6] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateServiceFeeForAirlineOfAgency, paramList);                
            }
            else
            {
                if (createdBy <= 0)
                {
                    throw new ArgumentException("createdBy must have a positive non zero integer value", "createdBy");
                }
                paramList[0] = new SqlParameter("@airlineServiceFeeId", SqlDbType.Int);
                paramList[0].Direction = ParameterDirection.Output;
                paramList[6] = new SqlParameter("@createdBy", createdBy);
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddServiceFeeForAirlineOfAgency, paramList);
                
            }
            airlineServiceFeeId = Convert.ToInt32(paramList[0].Value);
            if (airlineServiceFeeId == -1)
            {
                throw new ArgumentException("Entry already exist for this airline");
            }
            return airlineServiceFeeId;
        }

        /// <summary>
        /// To Get Airline service fee of a agency for all airline
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static List<AirlineServiceFee> GetAirlineServiceFeeOfAgency(int agencyId)
        {
            List<AirlineServiceFee> tempList = new List<AirlineServiceFee>();
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetServiceFeeOfAllAirlineOfAgency, paramList, connection);
            if (data.HasRows)
            {
                while (data.Read())
                {
                    AirlineServiceFee asf = new AirlineServiceFee();
                    asf.airlineServiceFeeId = Convert.ToInt32(data["airlineServiceFeeId"]);
                    asf.agencyId = Convert.ToInt32(data["agencyId"]);
                    asf.airlineCode = Convert.ToString(data["airLineCode"]);
                    asf.ServiceFee = Convert.ToDecimal(data["serviceFee"]);
                    asf.ServiceFeeType = (ServiceFeeType)(Enum.Parse(typeof(ServiceFeeType), data["serviceFeeTypeId"].ToString()));
                    asf.isActive = Convert.ToBoolean(data["isActive"]);
                    asf.createdOn = Convert.ToDateTime(data["createdOn"]);
                    asf.createdBy = Convert.ToInt32(data["createdBy"]);
                    asf.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);                    
                    asf.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    tempList.Add(asf);
                }
            }
            data.Close();
            connection.Close();            
            return tempList;
        }

        public static AirlineServiceFee Load(string airlineCode, int agencyId)
        {
            AirlineServiceFee asf = new AirlineServiceFee();
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@airlineCode", airlineCode);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetServiceFeeOfAirlineOfAgency, paramList, connection);
            using (DataTable dtService = DBGateway.FillDataTableSP(SPNames.GetServiceFeeOfAirlineOfAgency, paramList))
            {
                if (dtService != null && dtService.Rows.Count > 0)
                {
                    foreach (DataRow data in dtService.Rows)
                    {
                        asf.airlineServiceFeeId = Convert.ToInt32(data["airlineServiceFeeId"]);
                        asf.agencyId = Convert.ToInt32(data["agencyId"]);
                        asf.airlineCode = Convert.ToString(data["airLineCode"]);
                        asf.ServiceFee = Convert.ToDecimal(data["serviceFee"]);
                        asf.ServiceFeeType = (ServiceFeeType)(Enum.Parse(typeof(ServiceFeeType), data["serviceFeeTypeId"].ToString()));
                        asf.isActive = Convert.ToBoolean(data["isActive"]);
                        asf.createdOn = Convert.ToDateTime(data["createdOn"]);
                        asf.createdBy = Convert.ToInt32(data["createdBy"]);
                        asf.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                        asf.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    }
                }
            }
            //data.Close();
            //connection.Close();
            return asf;
        }

        /// <summary>
        /// To Acivate or Deactivate the account of any User
        /// </summary>
        /// <param name="airlineServiceFeeId"></param>
        /// <param name="isActive"></param>
        public static void ActivateDeactiveServiceFee(int airlineServiceFeeId, bool isActive)
        {            
            int rowsAffected = 0;
            if (airlineServiceFeeId > 0)
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@airlineServiceFeeId", airlineServiceFeeId);
                paramList[1] = new SqlParameter("@isActive", isActive);
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.ActivateDeactiveServiceFee, paramList);
            }
            else
            {
                throw new ArgumentException("memberId should have positive integer value", "memberId");
            }            
        }
        #endregion

    }
}
