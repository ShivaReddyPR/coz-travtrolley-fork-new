namespace CT.AccountingEngine
{
    internal static class SPNames
    {
        public const string GetMarkup = "usp_GetMarkup";
        public const string GetPLB = "usp_GetPLB";
        public const string GetTDSInfo = "usp_GetTDSInfo";
        public const string GetServiceTax = "usp_GetServiceTax";
        public const string GetAirlinePLB = "usp_GetAirlinePLB";
        public const string GetCommission = "usp_GetCommission";
        public const string GetAirlineCommission = "usp_GetAirlineCommission";
        public const string GetTDSDefault = "usp_GetTDSDefault";
        public const string GetServiceTaxPercent = "usp_GetServiceTaxPercent";
        public const string GetCess = "usp_GetCess";
        public const string AddLedgerTransaction = "usp_AddLedgerTransaction";
        public const string UpdateLedgerTransaction = "usp_UpdateLedgerTransaction"; 
        public const string AddLedgerEntry = "usp_AddLedgerEntry";
        public const string GetTransactionDetail = "usp_GetTransactionDetail";
        public const string GetTransactionDetailByReferenceId = "usp_GetTransactionDetailByReferenceId";
        public const string GetLedgerDetail = "usp_GetLedgerDetail";
        public const string GetAllTransactions = "usp_GetAllTransactions";
        public const string GetTransactionsWithinTime = "usp_GetTransactionsWithinTime";
        public const string GetAdminLedger = "usp_GetAdminLedger";
        public const string GetAdminLedgerForAgencies = "usp_GetAdminLedgerForAgencies";
        public const string AddPaymentDetail = "usp_AddPaymentDetail";
        public const string GetPaymentQueueForCSV = "usp_GetPaymentQueueForCSV";
        public const string AddPaymentDetailPaymentProcessing = "usp_AddPaymentDetailPaymentProcessing"; 
        public const string AddPaymentInvoice = "usp_AddPaymentInvoice";
        public const string AddInvoiceTxn = "usp_AddInvoiceTxn";
        public const string UpdateCurrentBalance = "usp_UpdateCurrentBalance";
        public const string UpdateOneDayCredit = "usp_UpdateOneDayCredit";
        public const string GetMaxDocumentSequenceNumber = "usp_GetMaxDocumentSequenceNumber";


        public const string CheckFortnightFullyPaid = "usp_CheckFortnightFullyPaid";
        public const string GetCurrentFortNightUsage = "usp_GetCurrentFortNightUsage";
        public const string GetPaymentDetail = "usp_GetPaymentDetail";
        public const string GetPaymentInvoice = "usp_GetPaymentInvoice";
        public const string UpdatePaymentDetail = "usp_UpdatePaymentDetail";
        public const string PaymentAccept = "usp_PaymentAccepted";
        public const string AddPaymentAgainstInvoice = "usp_AddPaymentAgainstInvoice";
        public const string UpdatePaymentInvoice = "usp_UpdatePaymentInvoice";
        public const string UpdateInvoicePaymentStatus = "usp_UpdateInvoicePaymentStatus";
        public const string GetPayments = "usp_GetPayments";
        public const string GetPaymentsOfStatus = "usp_GetPaymentsOfStatus";
        public const string GetAgentEarning = "usp_GetAgentEarning";
        public const string GetAgentEarningInFortnight = "usp_GetAgentEarningInFortnight";
        public const string GetDefaultCommission = "usp_GetDefaultCommission";
        public const string GetPLBFromRule = "usp_GetPLBFromRule";
        public const string GetLedgerTxnIdByFlight = "usp_GetLedgerTxnIdByFlight";
        public const string UpdatePaymentStatusId = "usp_UpdatePaymentStatusId";
        public const string GetBalanceTillLastFortnight = "usp_GetBalanceTillLastFortnight";
        public const string GetPaymentsThisFortnight = "usp_GetPaymentsThisFortnight";
        public const string GetRecentPayments = "usp_GetRecentPayments";
        public const string GetBankDetail = "usp_GetBankDetail";
        public const string GetAllBankDetail = "usp_GetAllBankDetail";
        public const string GetBalanceLeftofAdvancePayments = "usp_GetBalanceLeftofAdvancePayments";
        public const string GetBalanceLeftAndPaymentModeofAdvancePayments = "usp_GetBalanceLeftAndPaymentModeofAdvancePayments";
        public const string GetReceiptNumberAgainstPaymentDetailId = "usp_GetReceiptNumberAgainstPaymentDetailId";
        
        public const string GetTotalAdvancePayments = "usp_GetTotalAdvancePayments";
        public const string UpdateRemainingBalance = "usp_UpdateRemainingBalance";
        public const string GetMaxTrackID = "usp_GetMaxTrackID";
        public const string AddLedgerTransactionForOfflineBooking = "usp_AddLedgerTransactionForOfflineBooking";
        public const string GetReceiptReport = "usp_GetReceiptReport";

        public const string GetTrainServiceTaxRate = "usp_GetTrainServiceTaxRate";
        public const string GetInsuranceServiceTaxRate = "usp_GetInsuranceServiceTaxRate";
        public const string UpdateAutoPaymentStatus = "usp_UpdateAutoPaymentStatus";

        public const string GetTransactionsWithinTimeOfType = "usp_GetTransactionsWithinTimeOfType";
        public const string GetAdminLedgerOfType = "usp_GetAdminLedgerOfType";
        public const string GetAdminLedgerForAgenciesOfType = "usp_GetAdminLedgerForAgenciesOfType";
        public const string GetPaymentEntry = "usp_GetPaymentEntry";
        public const string GetPreviousPaymentEntry = "usp_GetPreviousPaymentEntry";
        public const string SaveHotelDeal = "usp_SaveHotelDeal";
        public const string UpdateHotelDeal = "usp_UpdateHotelDeal";
        public const string DeleteHotelDeal= "usp_DeleteHotelDeal";
        public const string GetHotelDealById="usp_GetHotelDealById";

        public const string GetReceivedAmountOnBank = "usp_GetReceivedAmountOnBank";
        public const string GetReceivedAmountOnBankForAgent = "usp_GetReceivedAmountOnBankForAgent";
        public const string DeleteInvoiceAgainstPayment = "usp_DeleteInvoiceAgainstPayment";
        public const string GetAmountSettledThisFortnight = "usp_GetAmountSettledThisFortnight";

        public const string GetPLBOfTxn = "usp_GetPLBOfTxn";
        public const string GetOpeningBalanceTillDate = "usp_GetOpeningBalanceTillDate";
        //For Whitelable Invoice 
        public const string AddWLInvoice = "usp_AddWLInvoice";
        public const string AddWLLineItem = "usp_AddWLLineItem";
        public const string GetWLInvoice = "usp_GetWLInvoice";
        public const string GetWLLineItem = "usp_GetWLLineItem";
        public const string GetWLInvoiceByInvoiceNumber = "usp_GetWLInvoiceByInvoiceNumber";

        public const string GetHotelSourceByName = "usp_GetHotelSourceByName";
        public const string AddCreditCardPayment = "usp_AddCreditCardPayment";
        public const string GetCreditCardCharge = "usp_GetCreditCardCharge";
        public const string GetAllAccountsDetailCharges = "usp_GetAllAccountsDetailCharges";
        public const string GetHotelServiceTax = "usp_GetHotelServiceTax";

        public const string InvoiceRaisedForInsurance = "usp_InvoiceRaisedForInsurance";

        //For TBO Banks On the Portal 
        public const string AddBankDetail = "usp_AddBankDetail";
        public const string UpdateBankDetail = "usp_UpdateBankDetail";
        public const string GetTBOBankDetail = "usp_GetTBOBankDetail";
        public const string GetAllBanks = "usp_GetAllBanks";
        public const string GetOurBankDetailIdBySource = "usp_GetOurBankDetailIdBySource";

        // For Transfers

        public const string GetTransferServiceTax = "usp_GetTransferServiceTax";

        // For Airline Service Fee
        public const string AddServiceFeeForAirlineOfAgency = "usp_AddServiceFeeForAirlineOfAgency";
        public const string UpdateServiceFeeForAirlineOfAgency = "usp_UpdateServiceFeeForAirlineOfAgency";
        public const string GetServiceFeeOfAllAirlineOfAgency = "usp_GetServiceFeeOfAllAirlineOfAgency";
        public const string GetAirlineServiceFee = "usp_GetAirlineServiceFee";
        public const string GetAllAirlineServiceFee = "usp_GetAllAirlineServiceFee";
        public const string ActivateDeactiveServiceFee = "usp_ActivateDeactiveServiceFee";
        public const string GetServiceFeeOfAirlineOfAgency = "usp_GetServiceFeeOfAirlineOfAgency";

        // For Airline Commission

        public const string AddAirlineCommission = "usp_AddAirlineCommission";
        public const string GetAirlineCommissionForAgency = "usp_GetAirlineCommissionForAgency";
        public const string GetActiveAirlineCommissionForAgency = "usp_GetActiveAirlineCommissionForAgency";
        public const string GetAirlineCommissionOfAirlineForAgency = "usp_GetAirlineCommissionOfAirlineForAgency";
        public const string UpdateAirlineCommission = "usp_UpdateAirlineCommission";
        public const string ActivateDeactiveAirlineCommission = "usp_ActivateDeactiveAirlineCommission";

        //For Credit Card Payment Request

        public const string SaveCreditCardPaymentRequest = "usp_SaveCreditCardPaymentRequest";
        public const string UpdateCreditCardPaymentRequest = "usp_UpdateCreditCardPaymentRequest";
        public const string LoadCreditCardPaymentRequest = "usp_LoadCreditCardPaymentRequest";
        public const string GetPaymentGWRequestsForAgency = "usp_GetPaymentGWRequestsForAgency";
        // Auto Ticketing       

        //public const string AddRule = "usp_AddRule";
        //public const string AddPLBSectorException = "usp_AddPLBSectorException";
        //public const string AddPLBBlackouts = "usp_AddPLBBlackouts";
        //public const string GetPLBRulesForAirline = "usp_GetPLBRulesForAirline";
        //public const string GetExceptionalSectors = "usp_GetExceptionalSectors";
        //public const string GetBlackouts = "usp_GetBlackouts";
        //public const string GetPLBRule = "usp_GetPLBRule";
        //public const string UpdateRule = "usp_UpdateRule";
        //public const string DeleteBlackouts = "usp_DeleteBlackouts";
        //public const string DeleteExcptionalSectors = "usp_DeleteExcptionalSectors";
        //public const string DeleteRule = "usp_DeleteRule";
        //public const string AddRegion = "usp_AddRegion";
        //public const string DeleteRegion = "usp_DeleteRegion";
        //public const string AddCityInRegion = "usp_AddCityInRegion";
        //public const string GetAllRegions = "usp_GetAllRegions";
        /*public const string GetAllCategory = "usp_GetAllCategory";
        public const string GetAllPLBRule = "usp_GetAllPLBRule";
        public const string GetAllPLBBlackout = "usp_GetAllPLBBlackout";
        public const string UpdateRule = "usp_UpdateRule";
        public const string GetAllPLBSectorException = "usp_GetAllPLBSectorException";
        public const string GetAllAgencyCategory = "usp_GetAllAgencyCategory";
        public const string AddAgencyInCategory = "usp_AddAgencyInCategory";
        public const string AddRule = "usp_AddRule";
        public const string AddPLBBlackouts = "usp_AddPLBBlackouts";
        public const string DeleteExceptionalSectors = "usp_DeleteExcptionalSectors";
        public const string DeleteBlackouts = "usp_DeleteBlackouts";
        public const string DeleteRule = "usp_DeleteRule";
        public const string AddPLBSectorException = "usp_AddPLBSectorException";*/
       
        // New Markup

        public const string ProductTypeGetList = "usp_PRODUCT_TYPE_GETLIST";
        public const string MarkupRulesAddUpdate = "USP_MarkupRules_Add_Update";
        public const string MarkupRuleDetailsAddUpdate = "USP_MarkupRuleDetails_Add_Update";
        public const string GetMarkupList = "usp_GetMarkupList";
        public const string GetAgentMarkup = "usp_GetAgentMarkup";

    }
}
