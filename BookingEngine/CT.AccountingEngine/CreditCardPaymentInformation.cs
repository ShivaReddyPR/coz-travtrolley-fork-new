using System;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;


namespace CT.AccountingEngine
{
    public enum PaymentGatewaySource
    {
        /// <summary>
        /// This is payment gateway for HDFC
        /// </summary>
        HDFC = 1,
        /// <summary>
        /// This is payment gateway for American Express
        /// </summary>
        AMEX = 2,
        /// <summary>
        /// This is payment gateway for ICICI
        /// </summary>
        ICICI = 3,
        /// <summary>
        /// This is payment gateway for SBI
        /// </summary>
        OXICASH = 4,
        /// <summary>
        /// This is payment gateway for APICustomer
        /// </summary>
        APICustomer = 5,
        /// <summary>
        /// This is payment gateway for SBI
        /// </summary>
        SBI = 6,
        /// <summary>
        /// This is payment gateway for CCAvenue
        /// </summary>
        CCAvenue = 7,
        /// <summary>
        /// This is payment gateway for Beam
        /// </summary>
        Beam = 9,
        /// <summary>
        /// This is payment gateway for TicketVala
        /// </summary>
        TicketVala = 10,
        /// <summary>
        /// This is payment gateway for Axis
        /// </summary>
        Axis = 11,
        /// <summary>
        /// This is payment gateway for BillDesk
        /// </summary>
        BillDesk = 12,
         /// <summary>
        /// This is payment gateway for Cozmo
        /// </summary>
        CozmoPG = 13,
        /// <summary>
        /// This is payment gateway for NEOPG
        /// </summary>
        NEOPG = 14,
            /// <summary>
            /// This is payment gateway for KNPay 
            /// added by Somasekhar on 08/05/2018
            /// </summary>
        KNPay = 15,
        /// <summary>
        /// This is payment gateway for SafexPay 
        /// added by Somasekhar on 27/06/2018
        /// </summary>
        SafexPay = 16,
        /// <summary>
        /// This is payment gateway for PayFort 
        /// added by Somasekhar on 10/11/2018
        /// </summary>
        PayFort = 17,
        /// <summary>
        /// This is payment gateway for RazorPay 
        /// added by Praveen on 13/04/2020
        /// </summary>
        RazorPay = 18
    }
    /// <summary>
    /// 
    /// </summary>
    public class CreditCardPaymentInformation
    {
        private int paymentInformationId;
        private int referenceId;
        private ReferenceIdType referenceType;
        private string paymentId;
        private decimal amount;
        private string ipAddress;
        private string trackId;
        private PaymentGatewaySource paymentGateway;
        private string address;
        private int zip;
        private decimal charges;
        //fields for recording credit card payment in B2B
        private string transType;
        private int status;
        private string remarks;
        //private CT.BookingEngine.ProductType productTypeId;
        /// <summary>
        /// Unique Key for the the table Credit Card Payment
        /// </summary>
        public int PaymentInformationId
        {
            get
            {
                return paymentInformationId;
            }
            set
            {
                paymentInformationId = value;
            }
        }

        /// <summary>
        /// It's shows the type of reference Id(payment/invoice)
        /// </summary>
        public ReferenceIdType ReferenceType
        {
            get
            {
                return referenceType;
            }
            set
            {
                referenceType = value;
            }
        }

        /// <summary>
        /// Invoice number(When the ticket is genrated and payment is taken at the same time)
        /// </summary>
        public int ReferenceId
        {
            get
            {
                return referenceId;
            }
            set
            {
                referenceId = value;
            }
        }
        /// <summary>
        /// Payment Id provided by gateway to track
        /// </summary>
        public string PaymentId
        {
            get
            {
                return paymentId;
            }
            set
            {
                paymentId = value;
            }
        }
        /// <summary>
        /// Amount 
        /// </summary>
        public decimal Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }
        /// <summary>
        /// Ip address (Right we are not entering it)
        /// </summary>
        public string IPAddress
        {
            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
            }
        }
        /// <summary>
        /// Track Id we genrate to track the payment
        /// </summary>
        public string TrackId
        {
            get
            {
                return trackId;
            }
            set
            {
                trackId = value;
            }
        }
        /// <summary>
        /// Payment of which payment source
        /// </summary>
        public PaymentGatewaySource PaymentGateway
        {
            get
            {
                return paymentGateway;
            }
            set
            {
                paymentGateway = value;
            }
        }
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }
        public int Zip
        {
            get
            {
                return zip;
            }
            set
            {
                zip = value;
            }
        }
        /// <summary>
        /// credit card Charges
        /// </summary>
        public decimal Charges
        {
            get
            {
                return charges;
            }
            set
            {
                charges = value;
            }
        }

        public string TransType
        {
            get { return transType; }
            set { transType = value; }
        }

        public int PaymentStatus
        {
            get { return status; }
            set { status = value; }
        }

        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }
        //public CT.BookingEngine.ProductType ProductTypeId
        //{
        //    get { return productTypeId; }
        //    set { productTypeId = value; }
        //}

        /// <summary>
        /// Gets maximum trackId  
        /// </summary>
        /// <returns></returns>
        public static int GetMaxTrackId(PaymentGatewaySource paymentGatewaySource)
        {
            int trackId=0 ; 
           // Trace.TraceInformation("CreditCardPaymentInformation.GetMaxTrackId entered : ");
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paymentGatewaySourceId", (int)paymentGatewaySource);
             SqlDataReader data = DBGateway.ExecuteReaderSP("usp_GetMaxTrackID", paramList, connection);
             if (data.Read())
             {
                 if (data["trackId"] != DBNull.Value)
                 {
                     trackId = Convert.ToInt32(data["trackId"]);
                 }
                 else
                 {
                     trackId = 0;
                 }
             }
             data.Close();
             connection.Close();   
             //Trace.TraceInformation("CreditCardPaymentInformation.GetMaxTrackId  exiting ");
             return trackId; 
        }
        /// <summary>
        /// Save the credit card information
        /// </summary>
        public void Save()
        {
            //Trace.TraceInformation("CreditCardPaymentInformation.Save entered : ");
            SqlParameter[] paramList = new SqlParameter[14];
            paramList[0] = new SqlParameter("@paymentInformationId", paymentInformationId);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@referenceId", referenceId);
            paramList[2] = new SqlParameter("@paymentId", paymentId);
            paramList[3] = new SqlParameter("@amount", amount);
            paramList[4] = new SqlParameter("@ipAddress", ipAddress);
            paramList[5] = new SqlParameter("@trackId", trackId);
            paramList[6] = new SqlParameter("@paymentGatewayId", (int)paymentGateway);
            paramList[7] = new SqlParameter("@referenceIdType", (int)referenceType);
            paramList[8] = new SqlParameter("@address", address);
            paramList[9] = new SqlParameter("@zip", zip);
            paramList[10] = new SqlParameter("@charges", charges);
            if(!string.IsNullOrEmpty(transType)) paramList[11] = new SqlParameter("@transType", transType);
            paramList[12] = new SqlParameter("@paymentStatus", status);
            if(!string.IsNullOrEmpty(remarks)) paramList[13] = new SqlParameter("@remarks", remarks);
            int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_AddCreditCardPayment", paramList);
            paymentInformationId = Convert.ToInt32(paramList[0].Value);
            //Trace.TraceInformation("CreditCardPaymentInformation.Save exiting : rowsAffected = " + rowsAffected);
        }

        //Added on 01/06/2016
        public void Load(int bookingId)
        {
           
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@BOOKING_ID", bookingId);
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlDataReader reader = DBGateway.ExecuteReaderSP("usp_GetPaymentInfoByBookingId", paramList, connection);
                if (reader.Read())
                {
                    
                    referenceId = Convert.ToInt32(reader["referenceId"]);
                    if (reader["orderId"] != DBNull.Value)
                    {
                        trackId = Convert.ToString(reader["orderId"]);

                    }
                    if (reader["paymentId"] != DBNull.Value)
                    {
                        paymentId = Convert.ToString(reader["paymentId"]);

                    }
                  
                    if (reader["amount"] != DBNull.Value)
                    {
                        amount = Convert.ToDecimal(reader["amount"]);

                    }
                    if (reader["paymentGatewaySourceId"] != DBNull.Value)
                    {
                        PaymentGateway = (PaymentGatewaySource)Convert.ToInt32(reader["paymentGatewaySourceId"]);

                    }
                    if (reader["charges"] != DBNull.Value)
                    {
                        charges = Convert.ToDecimal(reader["charges"]);
                        
                    }

                    if (reader["Status"] != DBNull.Value)
                    {
                        status = Convert.ToInt32(reader["Status"]);
                    }
                }
                reader.Close();
            }
           
        }

        /// <summary>
        /// Update Payment Status after returning from Payment Gateway.                
        /// </summary>
        public void UpdatePaymentDetails()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[5];
                paramList[0] = new SqlParameter("@referenceId", referenceId);
                paramList[1] = new SqlParameter("@paymentId", paymentId);
                paramList[2] = new SqlParameter("@paymentStatus", status);
                paramList[3] = new SqlParameter("@remarks", remarks);
                paramList[4] = new SqlParameter("@paymentInformationId", paymentInformationId);

                int rowsAffected = DBGateway.ExecuteNonQuerySP("usp_UpdateCreditCardPayment", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.CreditCardPaymentRequest, Severity.High, 1, ex.ToString(), "");
            }
        }

        public void getPaymentDetailsByOrderId(string orderId)
        {
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@OrderId", orderId);
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlDataReader reader = DBGateway.ExecuteReaderSP("usp_GetPaymentInfoByOrderId", paramList, connection);
                if (reader.Read())
                {

                    if (reader["orderId"] != DBNull.Value)
                    {
                        trackId = Convert.ToString(reader["orderId"]);
                    }
                    if (reader["paymentId"] != DBNull.Value)
                    {
                        paymentId = Convert.ToString(reader["paymentId"]);
                    }
                    if (reader["amount"] != DBNull.Value)
                    {
                        amount = Convert.ToDecimal(reader["amount"]);
                    }
                    if (reader["paymentGatewaySourceId"] != DBNull.Value)
                    {
                        PaymentGateway = (PaymentGatewaySource)Convert.ToInt32(reader["paymentGatewaySourceId"]);
                    }
                    if (reader["charges"] != DBNull.Value)
                    {
                        charges = Convert.ToDecimal(reader["charges"]);
                    }
                }
                reader.Close();
            }
        }
    }
}
