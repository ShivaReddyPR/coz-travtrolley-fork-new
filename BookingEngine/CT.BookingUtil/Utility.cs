using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Web;
//using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using CT.BookingEngine;
//using CT.BookingEngine.GDS;
using CT.Core;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using CT.Configuration;
using System.Data.SqlClient;
using System.Text;
using System.Net;
//using System.Transactions;
using System.Security.Cryptography;
using CT.AccountingEngine;
using CT.TicketReceipt.BusinessLayer;

namespace CT.BookingUtil
{
    public enum WebServer
    {
        web1 = 1,
        web2 = 2
    }
    /// <summary>
    /// Summary description for Util
    /// </summary>
    public class Utility
    {
        public Utility()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public static Dictionary<int, decimal> AgencyLccBal = new Dictionary<int, decimal>();


        public static DictionaryEntry GetLogo(FlightInfo[][] flight, string applPath)
        {
            DictionaryEntry logo = new DictionaryEntry();
            string tempAirline = flight[0][0].Airline;
            bool multiAirline = false;
            // checking for multiple airlines
            for (int g = 0; g < flight.Length; g++)
            {
                for (int f = 0; f < flight[g].Length; f++)
                {
                    if (tempAirline != flight[g][f].Airline)
                    {
                        multiAirline = true;
                        break;
                    }
                }
            }
            //string logoFile = string.Empty;
            //string airlineName = string.Empty;
            // logoFile and airlineName for multiple carrier.
            if (multiAirline)
            {
                logo.Value = Airline.logoDirectory + "/" + Airline.multipleAirlineLogo;
                logo.Key = "Multiple Airlines";
            }
            else
            {
                // logofile and airline name for single carrier.
                try
                {
                    Airline departingAirline = new Airline();
                    departingAirline.Load(flight[0][0].Airline);
                    string logoPath = applPath + Airline.logoDirectoryPath + "\\" + departingAirline.LogoFile;
                    logo.Key = departingAirline.AirlineName + " (" + departingAirline.AirlineCode + ")";
                    // if logo file does not exist then assign default logo.
                    if (departingAirline.LogoFile.Length == 0 || !File.Exists(logoPath))
                    {
                        logo.Value = Airline.logoDirectory + "/" + Airline.defaultAirlineLogo;
                    }
                    else
                    {
                        logo.Value = Airline.logoDirectory + "/" + departingAirline.LogoFile;
                    }
                }
                catch (ArgumentException)
                {
                    logo.Value = logo.Value = Airline.logoDirectory + "/" + Airline.defaultAirlineLogo;
                    logo.Key = flight[0][0].Airline;
                }
            }
            return logo;
        }

        /// <summary>
        /// Checking if flight is domestic or international
        /// </summary>
        /// <param name="origin">origing country code</param>
        /// <param name="destination">destination country code</param>
        /// <returns></returns>
        public static bool IsDomestic(string origin, string dest)
        {
            bool isDomestic = true;
            if (origin != dest || origin != "" + Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "")
            {
                isDomestic = false;
            }

            return isDomestic;
        }

        /// <summary>
        /// Pagination
        /// Generic message for showing previos,next and pages 
        /// </summary>
        /// <param name="noOfPages">total no of pages</param>
        /// <param name="url"> url with page type info (as "BookingQueue?pageType=flightDate" using on Booking Queue page)</param>
        /// <param name="pageNo"> which page showing ex- 1, 2 etc</param>
        /// <returns></returns>
        public static string Paging(int noOfPages, string url, int pageNo)
        {
            string show = string.Empty;
            string previous = string.Empty;
            string first = string.Empty;
            string last = string.Empty;
            string next = string.Empty;
            int showperPage = 2;

            // records per page from configuration       
            showperPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["showPerPage"]);

            showperPage = showperPage - 1;
            int minPageNo;
            int maxPageNo;
            if ((pageNo + showperPage) <= noOfPages)
            {
                maxPageNo = pageNo + showperPage;
                minPageNo = pageNo;
            }
            else
            {
                maxPageNo = noOfPages;
                minPageNo = noOfPages - showperPage;
                if (minPageNo < 1)
                {
                    minPageNo = 1;
                }
            }

            if (pageNo != 1)
            {
                previous = "<a href=\"" + url + "&pageno=" + (pageNo - 1) + "\"> Previous </a>| ";
                first = "<a href=\"" + url + "&pageno=1\"> First </a>| ";
            }

            if (pageNo != noOfPages)
            {
                next = "<a href=\"" + url + "&pageno=" + (pageNo + 1) + "\"> Next </a>";
                last = "| <a href=\"" + url + "&pageno=" + noOfPages + "\"> Last </a>";
            }
            show = first + previous;
            for (int k = minPageNo; k <= maxPageNo; k++)
            {
                if (k == minPageNo)
                {
                    show += "<b><a href=\"" + url + "&pageno=" + k + "\">" + k + "</a></b> | ";
                }
                else
                {
                    show += "<a href=\"" + url + "&pageno=" + k + "\">" + k + "</a> | ";
                }

            }
            show = show + next + last;

            return show;
        }

        public static string PagingJavascript(int noOfPages, string url, int pageNo, bool showLast)
        {
            string show = string.Empty;
            string previous = string.Empty;
            string first = string.Empty;
            string last = string.Empty;
            string next = string.Empty;
            int showperPage = 2;

            // records per page from configuration       
            showperPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["showPerPage"]);

            showperPage = showperPage - 1;
            int showPageNos;
            if ((pageNo + showperPage) <= noOfPages)
            {
                showPageNos = pageNo + showperPage;
            }
            else
            {
                showPageNos = noOfPages;
            }

            if (pageNo != 1)
            {
                previous = "<a href=\"" + "javascript:ShowPage(" + (pageNo - 1) + ")\"> Previous </a>| ";
                first = "<a href=\"" + "javascript:ShowPage(1)\"> First </a>| ";
            }

            if (pageNo != noOfPages)
            {
                next = "<a href=\"" + "javascript:ShowPage(" + (pageNo + 1) + ")\"> Next </a>";
                if (showLast)
                {
                    last = "| <a href=\"" + "javascript:ShowPage(" + noOfPages + ")\"> Last </a>";
                }
            }
            show = first + previous;
            if (pageNo != 1)
            {
                show += "<b><a href=\"" + "javascript:ShowPage(" + pageNo + ")\">" + pageNo + "</a></b> | ";
            }

            if (showLast)
            {
                show = show + next + last;
            }
            else
            {
                show = show + next;
            }


            return show;
        }

        public static string PagingJavascript(int noOfPages, string url, int pageNo)
        {
            string show = string.Empty;
            string previous = string.Empty;
            string first = string.Empty;
            string last = string.Empty;
            string next = string.Empty;
            int showperPage = 2;

            // records per page from configuration       
            showperPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["showPerPage"]);

            showperPage = showperPage - 1;
            int showPageNos;
            if ((pageNo + showperPage) <= noOfPages)
            {
                showPageNos = pageNo + showperPage;
            }
            else
            {
                showPageNos = noOfPages;
            }

            if (pageNo != 1)
            {
                previous = "<a href=\"" + "javascript:ShowPage(" + (pageNo - 1) + ")\"> Previous </a>| ";
                first = "<a href=\"" + "javascript:ShowPage(1)\"> First </a>| ";
            }

            if (pageNo != noOfPages)
            {
                next = "<a href=\"" + "javascript:ShowPage(" + (pageNo + 1) + ")\"> Next </a>";
                last = "| <a href=\"" + "javascript:ShowPage(" + noOfPages + ")\"> Last </a>";
            }
            show = first + previous;
            for (int k = pageNo; k <= showPageNos; k++)
            {
                if (k == pageNo)
                {
                    show += "<b><a href=\"" + "javascript:ShowPage(" + k + ")\">" + k + "</a></b> | ";
                }
                else
                {
                    show += "<a href=\"" + "javascript:ShowPage(" + k + ")\">" + k + "</a> | ";
                }
            }
            show = show + next + last;

            return show;
        }
        /// <summary>
        /// UTC time convert into IST time
        /// </summary>
        /// <param name="dateTime"> UTC time that is to be converted in to IST time </param>
        /// <returns> IST time </returns>
        public static DateTime UTCtoISTtimeZoneConverter(string dateTime)
        {
            // UTC Time zone is (-00:00:00) & IST is (+05:30:00)
            TimeSpan absoluteOffset = new TimeSpan(00, 00, 00) - new TimeSpan(04, 00, 00);

            absoluteOffset = absoluteOffset.Duration();

            DateTime ISTtime = DateTime.Parse(dateTime) + absoluteOffset;

            return ISTtime;
        }

        /// <summary>
        /// IST time convert into UTC time 
        /// </summary>
        /// <param name="dateTime"> datetime </param>
        /// <returns> UST time </returns>
        public static DateTime ISTToUTC(DateTime dateTime)
        {
            return dateTime - new TimeSpan(4, 00, 0);

        }

        /// <summary>
        /// Gets current date and time in IST.
        /// </summary>
        /// <returns>DateTime in IST.</returns>
        public static DateTime GetIST()
        {
            return DateTime.Now.ToUniversalTime() + new TimeSpan(4, 00, 0);
        }

        public static KeyValuePair<DateTime, DateTime> GetFortnightStartingTillCurrent()
        {
            DateTime currentDateTime = DateTime.Now.ToUniversalTime();
            DateTime startingDate;
            DateTime endingDate;
            int startDate = 0;
            //Case for first fortNight
            if (currentDateTime.Day > 0 && currentDateTime.Day <= 15)
            {
                startDate = 1;
            }
            //Case for second fortnight
            else
            {
                startDate = 16;
            }
            //Assign from just starting from the given date
            startingDate = new DateTime(currentDateTime.Year, currentDateTime.Month, startDate, 0, 0, 0);
            //Assisn till the end of ending date
            endingDate = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day, 23, 59, 59);
            return new KeyValuePair<DateTime, DateTime>(startingDate, endingDate);
        }
        public static KeyValuePair<DateTime, DateTime> GetFinancialStartingTillCurrent()
        {
            DateTime currentDateTime = DateTime.Now.ToUniversalTime();
            DateTime startingDate;
            DateTime endingDate;

            //Assign from just starting from the given date
            if (currentDateTime.Month >= 4)
            {
                startingDate = new DateTime(currentDateTime.Year, 4, 1, 0, 0, 0);
            }
            else
            {
                startingDate = new DateTime(currentDateTime.Year - 1, 4, 1, 0, 0, 0);
            }
            //Assisn till the end of ending date
            endingDate = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day, 23, 59, 59);
            return new KeyValuePair<DateTime, DateTime>(startingDate, endingDate);
        }

        public static void UpdateAirlinePNR(FlightItinerary itinerary)
        {
            int flightId = FlightItinerary.GetFlightId(itinerary.PNR);
            FlightInfo[] segment = FlightInfo.GetSegments(flightId);
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                // if worldspan sends the segments in sequence.
                if (Convert.ToInt32(itinerary.Segments[i].FlightNumber) == Convert.ToInt32(segment[i].FlightNumber) &&
                    itinerary.Segments[i].Airline == segment[i].Airline &&
                    itinerary.Segments[i].Origin.AirportCode == segment[i].Origin.AirportCode &&
                    itinerary.Segments[i].Destination.AirportCode == segment[i].Destination.AirportCode)
                {
                    segment[i].AirlinePNR = itinerary.Segments[i].AirlinePNR;
                    segment[i].Save();
                }
                else    // if not sequential.
                {
                    for (int j = 0; j < segment.Length; j++)
                    {
                        if (Convert.ToInt32(itinerary.Segments[i].FlightNumber) == Convert.ToInt32(segment[j].FlightNumber) &&
                            itinerary.Segments[i].Airline == segment[j].Airline &&
                            itinerary.Segments[i].Origin.AirportCode == segment[j].Origin.AirportCode &&
                            itinerary.Segments[i].Destination.AirportCode == segment[j].Destination.AirportCode)
                        {
                            segment[j].AirlinePNR = itinerary.Segments[i].AirlinePNR;
                            segment[j].Save();
                            break;
                        }
                    }
                }
            }
        }

        public static bool IsDomestic(FlightInfo[] segment, string country)
        {
            bool result = true;
            if (country != segment[0].Origin.CountryCode)
            {
                result = false;
            }
            else
            {
                for (int i = 0; i < segment.Length; i++)
                {
                    if (country != segment[i].Destination.CountryCode)
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// This method converts the table in a comma seperated string
        /// </summary>
        /// <param name="table">Datatable</param>
        public static string ExportToExcel(DataTable table)
        {
            string printString = string.Empty;
            foreach (DataColumn dc in table.Columns)
            {
                if (printString.Length != 0)
                {
                    printString += "," + dc.ColumnName;
                }
                else
                {
                    printString += dc.ColumnName;
                }
            }
            int count = 0;
            foreach (DataRow dr in table.Rows)
            {
                printString += "\n";
                count = 0;

                foreach (DataColumn dc in table.Columns)
                {
                    string value = string.Empty;
                    if (dr[dc] == null)
                    {
                        value = "";
                    }
                    else
                    {
                        value = dr[dc].ToString().Replace(',', ' ');
                    }
                    if (count == 0)
                    {
                        printString += value;
                    }
                    else
                    {
                        printString += "," + value;
                    }
                    count++;
                }
            }
            return printString;
        }
        /// <summary>
        /// This method  converts the reader in comma seperated string
        /// </summary>
        /// <param name="dataReader">datareader</param>
        public static string ExportToExcel(SqlDataReader dataReader)
        {
            string printString = string.Empty;
            int count = 0;
            for (int i = 0; i < dataReader.FieldCount; i++)
            {
                if (printString.Length == 0)
                {
                    printString += dataReader.GetName(i);
                }
                else
                {
                    printString += "," + dataReader.GetName(i);
                }
            }
            while (dataReader.Read())
            {
                printString += "\n";
                count = 0;
                for (int i = 0; i < dataReader.FieldCount; i++)
                {

                    if (count == 0)
                    {
                        printString += dataReader[i].ToString().Replace(',', '.');
                    }
                    else
                    {
                        printString += "," + dataReader[i].ToString().Replace(',', '.');
                    }
                    count++;
                }
            }
            return printString;
        }

        /// <summary>
        /// Gets exception information and stack trace from exception in string format.
        /// </summary>
        /// <param name="ex">exception</param>
        /// <param name="message">this message will be prefixed with the information.</param>
        /// <returns></returns>
        public static string GetExceptionInformation(Exception ex, string message)
        {
            Type type = ex.GetType();
            string errorCode = string.Empty;
            if (type.Name == "SqlException")
            {
                SqlException sqEx = (SqlException)ex;
                errorCode = sqEx.ErrorCode.ToString();
            }
            StringBuilder messageText = new StringBuilder();
            messageText.Append("\r\n");
            messageText.Append(message);                    // prefixing the message given.
            messageText.Append("\r\n\nError: ");
            messageText.Append(ex.Message);                 // appending exception message.
            messageText.Append("\r\nTargetsite: ");
            messageText.Append(ex.TargetSite);              // appending the method from where the exception originated.
            messageText.Append("\r\nSource: ");
            messageText.Append(ex.Source);                  // appending the name of application that caused exception.
            messageText.Append("\r\n\nStackTrace:\r\n");

            messageText.Append(ex.StackTrace);              // appending the stack trace.
            if (errorCode.Length > 0)
            {
                messageText.Append("\r\n\nError Code:\r\n");
                messageText.Append(errorCode);
            }
            // checking if additional data is there in exception.
            if (ex.Data != null)
            {
                messageText.Append("\r\n\nAdditional Data: \r\n\t");
                // appending the data infromation with exception.
                foreach (DictionaryEntry de in ex.Data)
                {
                    messageText.Append("\r\n");
                    messageText.Append(de.Key);
                    messageText.Append(": ");
                    messageText.Append(de.Value);
                }
            }
            return messageText.ToString();
        }
        private static Dictionary<BookingSource, string> bookSource = new Dictionary<BookingSource, string>();

        /// <summary>
        /// Method to get the Code for booking Source like 1A for Amdeus or 1P for Worldspan
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string GetBookingSourceCode(BookingSource source)
        {
            string bookingSource = string.Empty;
            if (bookSource.ContainsKey(source))
            {
                bookingSource = bookSource[source];
            }
            else
            {
                if (source == BookingSource.WorldSpan)
                {
                    bookingSource = "1P";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.WorldSpan, bookingSource);
                    }
                }
                else if (source == BookingSource.Amadeus)
                {
                    bookingSource = "1A";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.Amadeus, bookingSource);
                    }
                }
                else if (source == BookingSource.Galileo)
                {
                    bookingSource = "1G";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.Galileo, bookingSource);
                    }
                }
                else if (source == BookingSource.SpiceJet)
                {
                    bookingSource = "SG";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.SpiceJet, bookingSource);
                    }
                }
                else if (source == BookingSource.Indigo)
                {
                    bookingSource = "6E";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.Indigo, bookingSource);
                    }
                }
                else if (source == BookingSource.Paramount)
                {
                    bookingSource = "I7";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.Paramount, bookingSource);
                    }
                }
                else if (source == BookingSource.AirDeccan)
                {
                    bookingSource = "DN";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.AirDeccan, bookingSource);
                    }
                }
                else if (source == BookingSource.Mdlr)
                {
                    bookingSource = "9H";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.Mdlr, bookingSource);
                    }
                }
                else if (source == BookingSource.GoAir)
                {
                    bookingSource = "G8";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.GoAir, bookingSource);
                    }
                }
                else if (source == BookingSource.Sama)
                {
                    bookingSource = "ZS";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.Sama, bookingSource);
                    }
                }
                else if (source == BookingSource.Abacus)
                {
                    bookingSource = "1B";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.Abacus, bookingSource);
                    }
                }
                else if (source == BookingSource.AirArabia)
                {
                    bookingSource = "G9";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.AirArabia, bookingSource);
                    }
                }
                else if (source == BookingSource.AirIndiaExpressDom)
                {
                    bookingSource = "IX";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.AirIndiaExpressDom, bookingSource);
                    }
                }
                else if (source == BookingSource.AirIndiaExpressIntl)
                {
                    bookingSource = "XI";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.AirIndiaExpressIntl, bookingSource);
                    }
                }
                else if (source == BookingSource.HermesAirLine)
                {
                    bookingSource = "1H";
                    if (!bookSource.ContainsKey(source))
                    {
                        bookSource.Add(BookingSource.HermesAirLine, bookingSource);
                    }
                }
            }
            return bookingSource;
        }

        public static string GetItineraryMail(FlightItinerary itinerary)
        {
            StringBuilder mail = new StringBuilder("Details of Itinerary\n");
            mail.Append("==========================\n");
            if (itinerary.PNR == null || itinerary.PNR.Length == 0)
            {
                mail.Append("No PNR");
            }
            else
            {
                mail.Append("PNR: ");
                mail.Append(itinerary.PNR);
            }
            mail.Append(" ");
            mail.Append(itinerary.FlightBookingSource.ToString());
            mail.Append("\n\nSegment Information\n");
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                mail.Append(i + 1);
                mail.Append(". ");
                mail.Append(itinerary.Segments[i].Airline);
                mail.Append(itinerary.Segments[i].FlightNumber);
                mail.Append(" ");
                mail.Append(itinerary.Segments[i].Origin.AirportCode);
                mail.Append(" ");
                mail.Append(itinerary.Segments[i].Destination.AirportCode);
                mail.Append(itinerary.Segments[i].DepartureTime.ToString(" ddMMM HHmm").ToUpper());
                mail.Append(itinerary.Segments[i].ArrivalTime.ToString(" ddMMM HHmm ").ToUpper());
                mail.Append(itinerary.Segments[i].BookingClass);
                mail.Append("\n");
            }
            mail.Append("\nPassenger Detail\n");
            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                mail.Append(i + 1);
                mail.Append(". ");
                mail.Append(itinerary.Passenger[i].FullName);
                mail.Append(" (");
                mail.Append(itinerary.Passenger[i].Type.ToString());
                mail.Append(")\n");
            }
            mail.Append("\n\nMail generated on ");
            mail.Append(GetIST());
            mail.Append("\n");
            return mail.ToString();
        }

        /// <summary>
        /// Checks if given date is today's date.
        /// </summary>
        /// <param name="utcDateTime">DateTime IST.</param>
        /// <returns>True if given date is today's date</returns>
        public static bool IsTodayIST(DateTime istDateTime)
        {
            if (GetIST().Year == istDateTime.Year && GetIST().DayOfYear == istDateTime.DayOfYear)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Checks if given date is today's date.
        /// </summary>
        /// <param name="utcDateTime">DateTime UTC.</param>
        /// <returns>True if given date is today's date</returns>
        public static bool IsToday(DateTime utcDateTime)
        {
            if (DateTime.UtcNow.Year == utcDateTime.Year && DateTime.UtcNow.DayOfYear == utcDateTime.DayOfYear)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public static void FileSaveAtClient(string fileName, string text)
        {
            //System.Web.UI.Page page = new Page();
            // Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ".csv");
            if (text != null && text.Length > 0)
            {
                HttpContext.Current.Response.AddHeader("Content-Length", text.Length.ToString());
                HttpContext.Current.Response.Write(text);
                HttpContext.Current.Response.End();
            }

        }
        /// <summary>
        /// This Method will get currency symbol by taking currency code
        /// </summary>
        /// <param name="currencyCode"></param>
        /// <returns></returns>
        public static string GetCurrencySymbol(string currencyCode)
        {
            string symbol = string.Empty;
            if (currencyCode == "USD")
            {
                symbol = "$";
            }
            else if (currencyCode == "GBP")
            {
                symbol = "�";
            }
            else if (currencyCode == "EUR")
            {
                symbol = "�";
            }
            else if (currencyCode == "AUD")
            {
                symbol = "A$";
            }
            else if (currencyCode == "" + Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
            {
                symbol = "" + Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "";
            }
            else
            {
                symbol = currencyCode;
            }
            return symbol;
        }
        public static int SaveQueueAgain(FlightItinerary itinerary, BookingDetail booking, List<string> pmTicketNo, ref bool notSaved, CT.TicketReceipt.BusinessLayer.UserMaster loggedMember, CT.TicketReceipt.BusinessLayer.AgentMaster agency)
        {
            bool txnSucceded = true;
            int TimeLimitToComplete = 7;
            Ticket[] ticket = new Ticket[0];
            if (itinerary.FlightBookingSource == BookingSource.WorldSpan || itinerary.FlightBookingSource == BookingSource.Amadeus || itinerary.FlightBookingSource == BookingSource.Galileo)
            {
                int flightId = FlightItinerary.GetFlightId(itinerary.PNR);
                booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(flightId));
                itinerary = new FlightItinerary(flightId);
                FlightItinerary itineraryGDS = null;
                try
                {
                    //if (itinerary.FlightBookingSource == BookingSource.WorldSpan)
                    //{
                    //    itineraryGDS = Worldspan.RetrieveItinerary(itinerary.PNR, out ticket);
                    //}
                    //else if (itinerary.FlightBookingSource == BookingSource.Amadeus)
                    //{
                    //    itineraryGDS = Amadeus.RetrieveItinerary(itinerary.PNR, out ticket);
                    //}
                    //else if (itinerary.FlightBookingSource == BookingSource.UAPI)
                    //{
                    //    itineraryGDS = GalileoApi.RetrieveItinerary(itinerary.PNR, out ticket);
                    //}
                    itineraryGDS = CT.BookingEngine.GDS.UAPI.RetrieveItinerary(itinerary.PNR, out ticket);
                }
                catch (Exception ex)
                {
                    notSaved = true;
                }
                try
                {
                    if (ticket.Length > 0)
                    {
                        CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
                        mse.SaveTicketInfo(ref ticket, itinerary, itineraryGDS, (int)loggedMember.ID, (int)agency.ID, booking.BookingId, new Dictionary<string, string>(), string.Empty);
                        if (ticket.Length == itinerary.Passenger.Length)
                        {
                            booking.SetBookingStatus(BookingStatus.Ticketed, (int)loggedMember.ID);
                            try
                            {
                                //int invoiceNumber = AccountingEngine.AccountUtility.RaiseInvoice(itinerary, string.Empty, loggedMember.MemberId); TODO ziyad
                                int invoiceNumber = 001;
                                if (itinerary.PaymentMode == ModeOfPayment.CreditCard)
                                {
                                    string dummy = string.Empty;
                                 //   CT.MetaSearchEngine.MetaSearchEngine.MakeCCPaymentEntry(booking, itinerary, loggedMember, invoiceNumber); todo  ziya
                                }
                                //try
                                //{
                                //    System.Threading.Thread mailThread = new System.Threading.Thread(CT.MetaSearchEngine.MetaSearchEngine.SendLowBalanceMail);
                                //    mailThread.Start(loggedMember);
                                //}
                                //catch (Exception)
                                //{ }
                            }
                            catch (Exception ex)
                            {
                                try
                                {
                                    Email.Send("Invoice save failed", itinerary.PNR);
                                }
                                catch (System.Net.Mail.SmtpException) { } // Do nothing                                
                                Audit.Add(EventType.Exception, Severity.High, itinerary.CreatedBy, "Save Invoice failed. Error : " + ex.Message, "0");
                            }
                        }
                        else
                        {
                            booking.Lock(BookingStatus.InProgress, (int)loggedMember.ID);
                        }
                        #region Update FailedBooking Status
                        //code to check and update status if there is a record corresponding to PNR in FailedBooking table
                        if (itinerary.PNR != null && itinerary.PNR.Length > 0 && FailedBooking.CheckPNR(itinerary.PNR))
                        {
                            FailedBooking.UpdateStatus(itinerary.PNR, CT.BookingEngine.Status.Saved, "Booking is Saved");
                        }
                        #endregion
                    }
                    return booking.BookingId;
                }
                catch (Exception e)
                {
                    notSaved = true;
                    string messageText = "PNR = " + itinerary.PNR + "\r\n Booking Source is " + itinerary.FlightBookingSource.ToString() + "\r\n AgencyId is " + itinerary.AgencyId + "\r\n MemberId is " + itinerary.CreatedBy + "\r\n. Ticket failed to save.";
                    string message = Util.GetExceptionInformation(e, "Ticket save failed again while loading from Ticket Queue.\r\n" + messageText);
                    try
                    {
                        Email.Send("Ticket save failed again while loading from Ticket Queue", message);
                    }
                    catch (System.Net.Mail.SmtpException) { } // Do nothing
                    return booking.BookingId;
                }
            }
            else
            {
                # region oldcode
                //        try
                //        {
                //            using (TransactionScope updateTransaction = new TransactionScope())
                //            {
                //                booking.Save(itinerary, true);
                //                CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
                //                mse.SaveTicketInfo(itinerary, itinerary.CreatedBy, itinerary.AgencyId, pmTicketNo, booking.BookingId);
                //                CoreLogic.Queue queue = new CoreLogic.Queue();
                //                queue.QueueTypeId = (int)QueueType.Booking;
                //                queue.StatusId = (int)QueueStatus.Assigned;
                //                queue.AssignedTo = itinerary.CreatedBy;
                //                queue.AssignedBy = itinerary.CreatedBy;
                //                queue.AssignedDate = DateTime.UtcNow;
                //                queue.CompletionDate = DateTime.UtcNow.AddDays(TimeLimitToComplete);
                //                queue.CreatedBy = itinerary.CreatedBy;
                //                queue.ItemId = booking.BookingId;
                //                queue.Save();
                //                try
                //                {
                //                    #region Update FailedBooking Status
                //                    //code to check and update status if there is a record corresponding to PNR in FailedBooking table
                //                    if (itinerary.PNR != null && itinerary.PNR.Length > 0 && FailedBooking.CheckPNR(itinerary.PNR))
                //                    {
                //                        FailedBooking.UpdateStatus(itinerary.PNR, Technology.BookingEngine.Status.Saved, "Booking is Saved");
                //                    }
                //                    #endregion
                //                    int invoiceNo = AccountUtility.RaiseInvoice(itinerary, string.Empty, itinerary.CreatedBy);
                //                }
                //                catch(Exception e)
                //                {
                //                    try
                //                    {
                //                        Email.Send("Invoice save failed", itinerary.PNR);
                //                    }
                //                    catch (System.Net.Mail.SmtpException) { } // Do nothing                                
                //                    CoreLogic.Audit.Add(CoreLogic.EventType.Exception, CoreLogic.Severity.High, itinerary.CreatedBy, "Save Invoice failed. Error : " + e.Message, "0");
                //                }
                //                updateTransaction.Complete();
                //            }
                //        }
                //        catch (Exception ex)
                //        {
                //            //txnSucceded = false;

                //            if (ex.Message.IndexOf("already exist in database.") > 0)
                //            {
                //                //int lower = ex.Message.IndexOf("(");
                //                //int upper = ex.Message.IndexOf(")");
                //                //return BookingDetail.GetBookingIdByFlightId(FlightItinerary.GetFlightId(ex.Message.Substring(lower + 1, upper - lower - 1)));
                //                return BookingDetail.GetBookingIdByFlightId(FlightItinerary.GetFlightId(itinerary.PNR));
                //            }
                //            else
                //            {
                //                string messageText = "PNR = " + itinerary.PNR + "\r\n Booking Source is " + itinerary.FlightBookingSource.ToString() + "\r\n AgencyId is " + itinerary.AgencyId + "\r\n MemberId is " + itinerary.CreatedBy + "\r\n. PNR created successfully but failed to save.";
                //                string message = Util.GetExceptionInformation(ex, "Booking save failed again while loading.\r\n" + messageText);
                //                try
                //                {
                //                    Email.Send("Booking save failed again while loading", message);
                //                }
                //                catch (System.Net.Mail.SmtpException) { } // Do nothing
                //                return 0;
                //            }
                //        }
                //    }
                ////try
                ////{
                ////    if (txnSucceded == true)
                ////    {
                ////        int invoiceNo = AccountUtility.RaiseInvoice(itinerary, string.Empty, itinerary.CreatedBy);
                ////        FailedBooking.UpdateStatus(itinerary.PNR, Technology.BookingEngine.Status.Saved, "Saved by User On ViewBookingForTicket.aspx page");
                ////        return booking.BookingId;
                ////    }
                ////}
                ////catch (Exception ex)
                ////{

                ////    try
                ////    {
                ////        Email.Send("Invoice save failed", itinerary.PNR);
                ////    }
                ////    catch (System.Net.Mail.SmtpException) { } // Do nothing                                
                ////    CoreLogic.Audit.Add(CoreLogic.EventType.Exception, CoreLogic.Severity.High, itinerary.CreatedBy, "Save Invoice failed. Error : " + ex.Message, "0");
                ////}
                #endregion

                try
                {
                    bool saveBooking = false;
                    int flightId = FlightItinerary.GetFlightId(itinerary.PNR);
                    if (flightId > 0)
                    {
                        saveBooking = false;
                        booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(flightId));
                        itinerary = new FlightItinerary(flightId);
                    }
                    else
                    {
                        saveBooking = true;
                    }
                    if (saveBooking)
                    {
                        using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                        {
                            booking.Save(itinerary, true);
                            CT.Core.Queue queue = new CT.Core.Queue();
                            queue.QueueTypeId = (int)QueueType.Booking;
                            queue.StatusId = (int)QueueStatus.Assigned;
                            queue.AssignedTo = itinerary.CreatedBy;
                            queue.AssignedBy = itinerary.CreatedBy;
                            queue.AssignedDate = DateTime.UtcNow;
                            queue.CompletionDate = DateTime.UtcNow.AddDays(TimeLimitToComplete);
                            queue.CreatedBy = itinerary.CreatedBy;
                            queue.ItemId = booking.BookingId;
                            queue.Save();
                            updateTransaction.Complete();
                        }
                    }
                    CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
                    mse.SaveTicketInfo(itinerary, itinerary.CreatedBy, itinerary.AgencyId, pmTicketNo, booking.BookingId, string.Empty, new Dictionary<string, string>());
                }
                catch (Exception ex)
                {
                    txnSucceded = false;
                    notSaved = true;
                    if (ex.Message.IndexOf("already exist in database.") > 0)
                    {
                        //int lower = ex.Message.IndexOf("(");
                        //int upper = ex.Message.IndexOf(")");
                        //return BookingDetail.GetBookingIdByFlightId(FlightItinerary.GetFlightId(ex.Message.Substring(lower + 1, upper - lower - 1)));
                        return BookingDetail.GetBookingIdByFlightId(FlightItinerary.GetFlightId(itinerary.PNR));
                    }
                    else
                    {
                        string messageText = "PNR = " + itinerary.PNR + "\r\n Booking Source is " + itinerary.FlightBookingSource.ToString() + "\r\n AgencyId is " + itinerary.AgencyId + "\r\n MemberId is " + itinerary.CreatedBy + "\r\n. PNR created successfully but failed to save.";
                        string message = Util.GetExceptionInformation(ex, "Booking save failed.\r\n" + messageText);
                        try
                        {
                            Email.Send("Booking save failed", message);
                        }
                        catch (System.Net.Mail.SmtpException) { } // Do nothing
                        return 0;
                    }
                }
                try
                {
                    if (txnSucceded == true)
                    {
                        #region Update FailedBooking Status
                        //code to check and update status if there is a record corresponding to PNR in FailedBooking table
                        if (itinerary.PNR != null && itinerary.PNR.Length > 0 && FailedBooking.CheckPNR(itinerary.PNR))
                        {
                            FailedBooking.UpdateStatus(itinerary.PNR, CT.BookingEngine.Status.Saved, "Booking is Saved");
                        }
                        #endregion

                        // ziya todo air invoicing 
                       // int invoiceNo = AccountingEngine.AccountUtility.RaiseInvoice(itinerary, string.Empty, itinerary.CreatedBy); TODO ziya
                        //try
                        //{
                        //    System.Threading.Thread mailThread = new System.Threading.Thread(CT.MetaSearchEngine.MetaSearchEngine.SendLowBalanceMail);
                        //    mailThread.Start(loggedMember);
                        //}
                        //catch (Exception)
                        //{ }
                        return booking.BookingId;
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        Email.Send("Invoice save failed", itinerary.PNR);
                    }
                    catch (System.Net.Mail.SmtpException) { } // Do nothing                                
                    Audit.Add(EventType.Exception, Severity.High, itinerary.CreatedBy, "Save Invoice failed. Error : " + ex.Message, "0");
                }
            }
            return booking.BookingId;
        }


        /// <summary>
        /// Unique key Generation which is not as heavy as GUID and is also unique then random class. 
        /// </summary>
        /// <returns></returns>
        public static string GetUniqueKeyThroughRNG()
        {
            int maxSize = 14;
            char[] chars = new char[62];
            string a;
            a = "71268903453281";       //Todo: Alphanumeric key would be more strong.
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }
        public static WebServer GetLandingServer(int agencyId)
        {
            WebServer server = WebServer.web1;
            try
            {
                bool enableWebCluster = Convert.ToBoolean(ConfigurationSystem.Core["EnableWebCluster"]);
                if (enableWebCluster)
                {
                    if (false)
                    {
                    }
                    //if (Agency.AgencyTypeList.ContainsKey(agencyId)) todo ziya
                    //{
                    //    switch (Agency.AgencyTypeList[agencyId])
                    //    {
                    //        case Agencytype.Cash:
                    //            server = (WebServer)Enum.Parse(typeof(WebServer), ConfigurationSystem.Core["CashServer"]);
                    //            break;
                    //        case Agencytype.Cheque:
                    //            server = (WebServer)Enum.Parse(typeof(WebServer), ConfigurationSystem.Core["ChequeServer"]);
                    //            break;
                    //        case Agencytype.Credit:
                    //            server = (WebServer)Enum.Parse(typeof(WebServer), ConfigurationSystem.Core["CreditServer"]);
                    //            break;
                    //        case Agencytype.Service:
                    //            server = (WebServer)Enum.Parse(typeof(WebServer), ConfigurationSystem.Core["ServiceServer"]);
                    //            break;
                    //    }
                    //}
                    else
                    {
                        server = (WebServer)Enum.Parse(typeof(WebServer), ConfigurationSystem.Core["UserServer"]);
                    }
                }
                else
                {
                    server = WebServer.web1;
                }
                //if (enableWebCluster && (!Agency.AgencyTypeList.ContainsKey(agencyId) || Agency.AgencyTypeList[agencyId] == Agencytype.Credit ||Agency.AgencyTypeList[agencyId] == Agencytype.Service ||Agency.AgencyTypeList[agencyId] == Agencytype.Cheque))
                //{
                //    server = WebServer.web2; 
                //}
                //else
                //{
                //    server = WebServer.web1; 
                //}
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Login, Severity.High, 0, "Exception in GetLandingServer Exception = " + ex.Message + " Stack Trace = " + ex.StackTrace, "");

            }
            return server;
        }


        ///<summery>
        ///method to refund the ticket and return the reponse true or false as success or fail
        ///</summery>
        public bool CancelTicket(Ticket ticket, decimal adminFee, decimal supplierFee, int loggedMemberId, string remarks, string voided, string voidedString, bool isCancelTicket, bool isDirect, int requestIndex, bool isRefundTicket)
        {
            /*requestId is the id of service request*/
            bool isLCC = false;
            FlightItinerary itinerary = new FlightItinerary(ticket.FlightId);
            foreach (FlightInfo segment in itinerary.Segments)
            {
                string airlineCode = segment.Airline;
                Airline airline = new Airline();
                airline.Load(airlineCode);
                if (airline.IsLCC)
                {
                    isLCC = true;
                    break;
                }
            }
            DataTable dataTable = Ticket.GetSelectedColumnsFromTicketAgainstFlightId(ticket.FlightId);

            bool areAllTicketsRefunded = true;

            foreach (DataRow dr in dataTable.Rows)
            {
                if (Convert.ToInt32(dr["ticketid"]) != ticket.TicketId)
                {

                    if (dr["status"].ToString() == "Cancelled" || dr["status"].ToString() == "Voided" || dr["status"].ToString() == "Refunded")
                    {
                        areAllTicketsRefunded = true;
                    }
                    else
                    {
                        areAllTicketsRefunded = false;
                        break;
                    }
                }
            }

            int agencyId = itinerary.AgencyId;
            AgentMaster agency = new AgentMaster(agencyId);

            decimal money = (agency.AgencyTypeId == (int)Agencytype.Service) ? ticket.Price.GetServiceAgentPrice() - (adminFee + supplierFee + ticket.Price.OtherCharges + ticket.Price.Discount) : ticket.Price.GetAgentPrice() - (adminFee + supplierFee + ticket.Price.OtherCharges + ticket.Price.Discount);

            PaymentDetails pd = new PaymentDetails();
            pd.AgencyId = agencyId;
            pd.PaymentDate = DateTime.Now.ToUniversalTime();
            pd.Remarks = "Credit Note as Ticket Refund for Ticket No: " + ticket.TicketNumber;
            if (agency.AgencyTypeId == (int)Agencytype.Cash || agency.AgencyTypeId == (int)Agencytype.Service)
            {
                pd.IsLCC = true;
            }
            else
            {
                pd.IsLCC = isLCC;
            }
            pd.Amount = money;
            pd.RemainingAmount = 0;
            pd.OurBankDetailId = 1;
            pd.ModeOfPayment = PaymentMode.CreditNote;
            pd.Invoices = new List<InvoiceCollection>();
            pd.Status = PaymentStatus.Accepted;
            pd.CreatedBy = loggedMemberId;

            CancellationCharges cancellationCharge = new CancellationCharges();
            cancellationCharge.AdminFee = adminFee;
            cancellationCharge.SupplierFee = supplierFee;
            cancellationCharge.ReferenceId = ticket.TicketId;
            cancellationCharge.ProductType = ProductType.Flight;
            cancellationCharge.ItemTypeId = InvoiceItemTypeId.Ticketed;

            BookingHistory bh = new BookingHistory();
            bh.BookingId = itinerary.BookingId;
            bh.EventCategory = EventCategory.Refund;
            bh.Remarks = "Refunded for Ticket No -" + ticket.TicketNumber;
            bh.CreatedBy = loggedMemberId;

            Ledger ledger = new Ledger();
            ledger.Load(agencyId);


            LedgerTransaction ledgerTxn = new LedgerTransaction();
            ledgerTxn.LedgerId = ledger.LedgerId;
            ledgerTxn.Amount = (agency.AgencyTypeId == (int)Agencytype.Service) ? ticket.Price.GetServiceAgentPrice() - ticket.Price.OtherCharges : ticket.Price.GetAgentPrice() - ticket.Price.OtherCharges;
            //ledgerTxn.Narration = remarks;
            ledgerTxn.ReferenceType = ReferenceType.TicketRefund;
            if (agency.AgencyTypeId == (int)Agencytype.Cash || agency.AgencyTypeId == (int)Agencytype.Service)
            {
                ledgerTxn.IsLCC = true;
            }
            else
            {
                ledgerTxn.IsLCC = isLCC;
            }

            if (isRefundTicket)
            {
                ledgerTxn.Notes = remarks;
                remarks = "Refunded for Ticket No -" + ticket.TicketNumber;
            }
            else
            {
                ledgerTxn.Notes = "Ticket Refunded";
            }
            ledgerTxn.Date = DateTime.UtcNow;
            ledgerTxn.CreatedBy = loggedMemberId;

            LedgerTransaction ledgerTxnCancelCharge = new LedgerTransaction();
            ledgerTxnCancelCharge.LedgerId = ledger.LedgerId;
            ledgerTxnCancelCharge.Amount = -(supplierFee + adminFee);
            NarrationBuilder objNarration = new NarrationBuilder();
            ledgerTxnCancelCharge.ReferenceType = ReferenceType.TicketCancellationCharge;
            if (agency.AgencyTypeId == (int)Agencytype.Cash || agency.AgencyTypeId == (int)Agencytype.Service)
            {
                ledgerTxnCancelCharge.IsLCC = true;

            }
            else
            {
                ledgerTxnCancelCharge.IsLCC = isLCC;
            }
            ledgerTxnCancelCharge.Notes = "";
            ledgerTxnCancelCharge.Date = DateTime.UtcNow;
            ledgerTxnCancelCharge.CreatedBy = loggedMemberId;

            NarrationBuilder objNarrationDiscount = new NarrationBuilder();
            LedgerTransaction ledgerTxnDiscount = new LedgerTransaction();
            if (ticket.Price.Discount > 0)
            {
                ledgerTxnDiscount.LedgerId = ledger.LedgerId;
                ledgerTxnDiscount.Amount = -(ticket.Price.Discount);
                ledgerTxnDiscount.ReferenceId = pd.PaymentDetailId;
                ledgerTxnDiscount.ReferenceType = ReferenceType.CashDiscountReversal;
                if (agency.AgencyTypeId == (int)Agencytype.Cash || agency.AgencyTypeId == (int)Agencytype.Service)
                {
                    ledgerTxnDiscount.IsLCC = true;
                }
                else
                {
                    ledgerTxnDiscount.IsLCC = isLCC;
                }

                ledgerTxnDiscount.Notes = "";
                ledgerTxnDiscount.Date = DateTime.UtcNow;
                ledgerTxnDiscount.CreatedBy = loggedMemberId;

            }
            #region Raise credit note code


            Invoice invoice = new Invoice();

            decimal ticketAmount = ((int)Agencytype.Service == agency.AgencyTypeId) ? ticket.Price.GetServiceAgentPrice() - ticket.Price.OtherCharges : ticket.Price.GetAgentPrice() - ticket.Price.OtherCharges;// ; ticket.Price.PublishedFare + ticket.Price.Tax + ticket.Price.OtherCharges;           
            PriceAccounts refundPrice = ticket.Price;
            refundPrice.PriceId = 0;

            // bool isServiceAgency = agency.AgencyTypeId == Convert.ToInt32(ConfigurationSystem.Core["ServiceAgencyTypeId"]);
            invoice.PaymentMode = string.Empty;
            invoice.Remarks = remarks;
            invoice.AgencyId = itinerary.AgencyId;
            invoice.CreatedBy = loggedMemberId;
            invoice.CreatedOn = DateTime.UtcNow;
            // Generating Invoice line items.
            invoice.LineItem = new List<InvoiceLineItem>();
            InvoiceLineItem line = new InvoiceLineItem();
            line.ItemDescription = string.Empty;
            line.ItemReferenceNumber = ticket.TicketId;

            //TODO: line item type Id. should not be hardcoded 1.
            line.ItemTypeId = 1;
            invoice.Status = InvoiceStatus.CreditNote;


            Invoice origInvoice = new Invoice();
            int invoiceNumber = 0;
            #endregion
            #region knock off invoice
            if (!isLCC)
            {
                invoiceNumber = Invoice.GetInvoiceNumberByTicketId(ticket.TicketId);
                origInvoice.Load(invoiceNumber);
                decimal totalPrice = 0;
                decimal amountToBeKnockOff = 0;
                //To Calculate total bill amount            
                foreach (InvoiceLineItem lineItem in origInvoice.LineItem)
                {
                    if ((int)Agencytype.Service == agency.AgencyTypeId)
                    {
                        totalPrice = totalPrice + Math.Round(lineItem.Price.GetServiceAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                    }
                    else
                    {
                        totalPrice = totalPrice + Math.Round(lineItem.Price.GetAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                    }
                }
                decimal invoiceDueAmount = 0;
                // To calculate due amount(Amount to be paid against Unpaid/Partialy paid invoice)
                if (origInvoice.Status != InvoiceStatus.Paid)
                {
                    invoiceDueAmount = totalPrice - Invoice.GetAcceptedPaymentsAgainstInvoice(invoiceNumber);
                }
                //To calculate amount to be KnockOff(Balance amount to be refund against refunded/voided invoces)            
                amountToBeKnockOff = (ticketAmount - (cancellationCharge.AdminFee + cancellationCharge.SupplierFee)) - invoiceDueAmount;

                InvoiceCollection ic = new InvoiceCollection();

                pd.Invoices = new List<InvoiceCollection>();
                //If amount to be refund>due amount then invoice(against that credit note generated)

                //will be settled and a payment will be created equal to balance amount(amount to be refund-due amount)            

                if (amountToBeKnockOff > 0)
                {
                    //To settle Invoice
                    ic.InvoiceNumber = invoiceNumber;
                    ic.Amount = invoiceDueAmount;
                    origInvoice.Status = InvoiceStatus.Paid;
                    ic.IsPartial = false;
                    pd.Invoices.Add(ic);
                    pd.RemainingAmount = amountToBeKnockOff;//pd.Amount-invoiceDueAmount;                
                }

                //If amount to be refund<due amount then due amount of the invoice(against that credit note generated)
                //will be equal to balance amount(due amount-amount to be refund)
                else if (amountToBeKnockOff < 0)
                {
                    //To settle Invoice                
                    ic.InvoiceNumber = invoiceNumber;
                    ic.Amount = invoiceDueAmount + amountToBeKnockOff;// +invoiceDueAmount;
                    ic.IsPartial = true;
                    origInvoice.Status = InvoiceStatus.PartiallyPaid;
                    pd.RemainingAmount = 0;// -amountToBeKnockOff;// pd.Amount - (amountToBeKnockOff + invoiceDueAmount); 
                    pd.Invoices.Add(ic);
                }

                //If amount to be refund=due amount then invoice(against that credit note generated)
                //will be settled
                else
                {
                    //To settle Invoice
                    ic.InvoiceNumber = invoiceNumber;
                    ic.Amount = invoiceDueAmount;
                    ic.IsPartial = false;
                    origInvoice.Status = InvoiceStatus.Paid;
                    pd.RemainingAmount = 0;
                    pd.Invoices.Add(ic);
                }
            }
            #endregion



            RequestType requestType = RequestType.Refund;
            BookingStatus bookingStatus = BookingStatus.Cancelled;
            if (voided == "Voided")
            {
                bookingStatus = BookingStatus.Void;
                requestType = RequestType.Void;
            }

            bool hasTranCompleted = false;
            int noOfLineItems = invoice.CreditNoteRaisedForTicketId(ticket.TicketId);
            if (noOfLineItems == 0)
            {
                try
                {
                    using (System.Transactions.TransactionScope updateTransaction =
                    new System.Transactions.TransactionScope())
                    {
                        pd.Save();
                        refundPrice.Save();
                        line.Price = refundPrice;
                        invoice.LineItem.Add(line);
                        invoice.Save(itinerary.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + ""), 0, ProductType.Flight, bookingStatus);
                        if (!isLCC)
                        {
                            Invoice.UpdateInvoiceStatus(invoiceNumber, origInvoice.Status, loggedMemberId);
                        }
                        #region Entry in Cancellation Charge Table
                        objNarration.TicketNo = ticket.TicketNumber;
                        cancellationCharge.PaymentDetailId = pd.PaymentDetailId;
                        cancellationCharge.Save();
                        #endregion

                        #region Two New Entries in LedgerTransaction One for ticket refund one for charge
                        objNarration.Remarks = remarks;
                        ledgerTxn.ReferenceId = pd.PaymentDetailId;
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxn.TxnId);
                        ledgerTxnCancelCharge.ReferenceId = pd.PaymentDetailId;
                        objNarration.Remarks = "Cancellation Charges";
                        ledgerTxnCancelCharge.Narration = objNarration;
                        ledgerTxnCancelCharge.Save();
                        LedgerTransaction.AddInvoiceTxn(invoice.InvoiceNumber, ledgerTxnCancelCharge.TxnId);
                        if (ticket.Price.Discount > 0)
                        {
                            ledgerTxnDiscount.ReferenceId = pd.PaymentDetailId;
                            objNarrationDiscount.TicketNo = ticket.TicketNumber;
                            objNarrationDiscount.Remarks = "Cash Discount Reversal";
                            objNarrationDiscount.DocNo = objNarration.DocNo;
                            ledgerTxnDiscount.Narration = objNarrationDiscount;
                            ledgerTxnDiscount.Save();
                        }
                        #endregion

                        #region Update Ledger's Current Balance
                        if (!isLCC && agency.AgencyTypeId != (int)Agencytype.Cash && agency.AgencyTypeId != (int)Agencytype.Service)
                        {
                            Ledger.UpdateCurrentBalance(agencyId, money, loggedMemberId);
                        }
                        else
                        {
                            Agency.UpdateLCCBalance(agencyId, money, loggedMemberId);
                        }
                        #endregion

                        if (areAllTicketsRefunded)
                        {
                            BookingDetail.SetBookingStatus(itinerary.BookingId, BookingStatus.Cancelled, loggedMemberId);
                        }

                        #region Update Queue Status
                        //Update Queue Status, Change Request  staus
                        if (isCancelTicket)
                        {
                            ServiceRequest serviceRequest = new ServiceRequest();
                            if (requestIndex != 0)
                            {
                                //Update Queue Status
                                CT.Core.Queue.SetStatus(QueueType.Request, requestIndex, QueueStatus.Completed, loggedMemberId, 0, "Completed");
                                serviceRequest.CompleteAirServiceRequest(requestIndex, loggedMemberId, (invoice.DocTypeCode + invoice.DocumentNumber.ToString()), ticket.Status, adminFee, supplierFee);
                            }

                        }
                        if (isDirect)
                        {
                            ServiceRequest serviceRequest = new ServiceRequest();
                            int requestId = ServiceRequest.GetServiceRequestIdByTicketId(ticket.TicketId);
                            if (requestId == 0)
                            {
                                serviceRequest.BookingId = itinerary.BookingId;
                                serviceRequest.ReferenceId = ticket.TicketId;
                                serviceRequest.ProductType = ProductType.Flight;
                                serviceRequest.RequestType = requestType;
                                serviceRequest.Data = remarks;
                                serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Completed;
                                serviceRequest.CreatedBy = loggedMemberId;
                                serviceRequest.ItemTypeId = InvoiceItemTypeId.Ticketed;
                                serviceRequest.AgencyId = itinerary.AgencyId;
                                serviceRequest.AgencyTypeId = (Agencytype)agency.AgencyTypeId;
                                serviceRequest.Details = ServiceRequest.GenerateFlightDetailXML(ticket, itinerary, agency);
                                serviceRequest.IsDomestic = itinerary.IsDomestic;
                                serviceRequest.PaxName = ticket.Title + " " + ticket.PaxFirstName + " " + ticket.PaxLastName;
                                serviceRequest.Pnr = itinerary.PNR;
                                serviceRequest.ReferenceNumber = ticket.TicketNumber;
                                serviceRequest.Source = (int)itinerary.FlightBookingSource;
                                serviceRequest.PriceId = ticket.Price.PriceId;
                                serviceRequest.SupplierName = itinerary.AirlineCode;
                                serviceRequest.StartDate = itinerary.TravelDate;
                                serviceRequest.AdminFee = adminFee;
                                serviceRequest.SupplierFee = supplierFee;
                                serviceRequest.TicketStatus = voided;
                                serviceRequest.Tax = ticket.Price.Tax;
                                serviceRequest.PublishedFare = ticket.Price.PublishedFare;
                                serviceRequest.AirlineCode = itinerary.ValidatingAirline;
                                serviceRequest.CreditNoteNumber = invoice.DocTypeCode + invoice.DocumentNumber.ToString();

                                serviceRequest.Save();
                                CT.Core.Queue.SetStatus(QueueType.Request, serviceRequest.RequestId, QueueStatus.Completed, loggedMemberId, 0, "Completed");
                            }
                            else
                            {
                                CT.Core.Queue.SetStatus(QueueType.Request, requestId, QueueStatus.Completed, loggedMemberId, 0, "Completed");
                                serviceRequest.CompleteAirServiceRequest(requestId, loggedMemberId, (invoice.DocTypeCode + invoice.DocumentNumber.ToString()), voided, adminFee, supplierFee);
                            }
                        }
                        #endregion
                        if (voided != string.Empty)
                        {
                            Ticket.SetTicketStatus(ticket.TicketId, voided, loggedMemberId);
                        }
                        updateTransaction.Complete();
                        hasTranCompleted = true;
                    }

                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Account, Severity.High, loggedMemberId, ex.Message + ex.StackTrace, "");
                }
                bh.Save();
            }
            else
            {
                //Condition when credit note is already raised
            }
            if (hasTranCompleted)
            {
                //send SMS
                Utility.AgencyLccBal.Remove(agencyId);
                if (SMS.IsSMSSubscribed(agencyId, SMSEvent.FlightChangeRequest))
                {
                    Hashtable hTable = new Hashtable();
                    hTable.Add("AgencyId", agencyId);
                    hTable.Add("PNR", ticket.TicketNumber);
                    hTable.Add("EventType", SMSEvent.FlightChangeRequest);
                    SMS.SendSMSThread(hTable);
                }
                //Response.Write(voidedString);
            }
            return hasTranCompleted;
        }


        public static void EmailVoucher(Dictionary<string, string> hotelVariables, Dictionary<string, string[]> hotelVariablesArray, List<string> toArray, string messageText)
        {
            if (Convert.ToBoolean(ConfigurationSystem.Email["isSendEmail"]))
            {
                Hashtable globalHashTable = new Hashtable();
                globalHashTable.Add("DateOfissue", hotelVariables["DateOfIssue"]);
                globalHashTable.Add("BookingRefNo", hotelVariables["BookingRefNo"]);
                globalHashTable.Add("NoOfAdult", hotelVariables["NoOfAdult"]);
                globalHashTable.Add("NoOfChild", hotelVariables["NoOfChild"]);
                globalHashTable.Add("NoOfInf", hotelVariables["NoOfInf"]);
                globalHashTable.Add("LeadPaxName", hotelVariables["LeadPaxName"]);
                globalHashTable.Add("HotelCity", hotelVariables["city"]);
                globalHashTable.Add("HotelName", hotelVariables["HotelName"]);
                globalHashTable.Add("NoOfNights", hotelVariables["nights"]);
                //globalHashTable.Add("HotelDescription", hotelVariablesArray["hotelDescription"][0]);
                globalHashTable.Add("HotelDescription", hotelVariables["hotelDescription"]);
                globalHashTable.Add("HotelCheckInDate", hotelVariables["HotelCheckinDate"]);
                globalHashTable.Add("HotelCheckOutDate", hotelVariables["HotelCheckOutDate"]);
                globalHashTable.Add("agencyName", hotelVariables["AgencyName"]);
                globalHashTable.Add("agencyContactNo", hotelVariables["AgencyContactNo"]);
                globalHashTable.Add("agencyEmail", hotelVariables["AgencyEmail"]);
                globalHashTable.Add("Message", messageText.ToString());

                //Html code in text file for E-ticket
                string filePath = ConfigurationManager.AppSettings["EPackageVoucher"].ToString();
                StreamReader sr = new StreamReader(filePath);
                //string contains the code of loop
                string loopString = string.Empty;

                bool loopStarts = false;
                //string contains the code before loop 
                string startString = string.Empty;
                //string contains the code after loop
                string endString = string.Empty;
                bool loopEnds = false;
                #region seperate out the loop code and the other code
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (line.IndexOf("%ItnLoopEnd%") >= 0 || loopEnds)
                    {
                        loopEnds = true;
                        loopStarts = false;
                        if (line.IndexOf("%ItnLoopEnd%") >= 0)
                        {
                            line = sr.ReadLine();
                        }
                        endString += line;
                    }
                    if (line.IndexOf("%ItnLoopStart%") >= 0 || loopStarts)
                    {
                        if (line.IndexOf("%ItnLoopStart%") >= 0)
                        {
                            line = sr.ReadLine();
                        }
                        loopString += line;
                        loopStarts = true;
                    }
                    else
                    {
                        if (!loopEnds)
                        {
                            startString += line;
                        }
                    }
                }
                #endregion

                string midString = string.Empty;

                DateTime strTourStartDate;
                strTourStartDate = Convert.ToDateTime(hotelVariables["TourStartDate"]);

                for (int count = 0; count <= Convert.ToInt32(hotelVariables["nights"]); count++)
                {
                    Hashtable tempTable = new Hashtable();
                    tempTable.Add("ItiniaryDate", strTourStartDate.AddDays(count).ToString("dd-MMM-yyyy"));
                    tempTable.Add("Itneray", hotelVariablesArray["day"][count]);
                    tempTable.Add("ItiniaryDescription", hotelVariablesArray["desc"][count]);
                    midString += Email.ReplaceHashVariable(loopString, tempTable);
                }

                //=====================================================
                string fullString;
                fullString = startString + midString + endString;

                string fromEmail = hotelVariables["AgencyEmail"];
                string replyTo = fromEmail;

                try
                {
                    Email.Send(fromEmail, replyTo, toArray, "Package Voucher", fullString, globalHashTable);
                }
                catch (System.Net.Mail.SmtpException ex)
                {
                    Audit.Add(EventType.HolidayPackageQueue, Severity.High, 0, "Sending Email Failed Of Package Voucher from b2b:" + ex.Message + " | " + DateTime.Now, "");
                }
            }
        }

        public static string BaseSiteUrl
        {
            get
            {
                HttpContext context = HttpContext.Current;
                string baseUrl = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/') + '/';
                return baseUrl;
            }
        }


    }
}