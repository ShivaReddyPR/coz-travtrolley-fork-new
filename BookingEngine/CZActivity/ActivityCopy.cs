﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using CT.Configuration;
using CT.BookingEngine;
using CT.Core;
using CZActivity;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.IO.Compression;
using System.Xml.Serialization;
using System.Xml;
using System.Linq;
using CZActivity.ActivityAPI;
using System.Web.Script.Serialization;

namespace CZActivity
{
    public class ActivityCopy
    {
        private string clientId = string.Empty;
        private string password = string.Empty;
        private string ipAddress = string.Empty;
        private string interfaceURL = string.Empty;
        private string fileSavingPath = string.Empty;

        static string tokenId = string.Empty;
        public int userId = 0;
        string activity = "(Activity)";
        private Dictionary<string, decimal> exchangeRates;
        public int agentDecimalpoint = 0;
        public string markupType = null;
        public int markupValue = 0;
        public string agentCurrency = null;
        private decimal rateOfExchange = 1;
        private decimal baseRateOfExchange = 1;

        public Dictionary<string, decimal> AgentExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        public static string TokenId
        {
            get { return tokenId; }
            set { tokenId = value; }
        }
        public int AgentDecimalPoint
        {
            get { return agentDecimalpoint; }
            set { agentDecimalpoint = value; }
        }
        public string MarkupType
        {
            get { return markupType; }
            set { markupType = value; }
        }
        public int MarkupValue
        {
            get { return markupValue; }
            set { markupValue = value; }
        }
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        ActivityServiceClient client = new ActivityServiceClient();
        public ActivityCopy()
        {
            clientId = ConfigurationSystem.ActivityServiceConfig["UserName"];
            password = ConfigurationSystem.ActivityServiceConfig["Password"];
            fileSavingPath = ConfigurationSystem.ActivityServiceConfig["XmlLogPath"];
            interfaceURL = ConfigurationSystem.ActivityServiceConfig["URL"];

            if (!System.IO.Directory.Exists(fileSavingPath))
            {
                System.IO.Directory.CreateDirectory(fileSavingPath);
            }
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(interfaceURL);
        }

        public bool authenticate()
        {
            bool authenticated = false;
            AuthenticationRequest request = new AuthenticationRequest();
            //AuthenticationResponse response = null;
            request.UserName = clientId;
            request.Password = password;
            request.UserIPAddress = "10.200.44.29";

            string requestString = JsonConvert.SerializeObject(request);

            try
            {
                Random random = new Random();
                string rndNumber = random.Next(100000, 999999).ToString();
                XmlSerializer serializer = new XmlSerializer(request.GetType());
                StringBuilder sb = new StringBuilder();
                StringWriter strwriter = new StringWriter(sb);
                serializer.Serialize(strwriter, request);

                string filePath = fileSavingPath + "ActivityAuthenticationRequest" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_" + rndNumber + ".xml";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                try
                {
                    doc.Save(filePath);
                }
                catch { }

                Audit.Add(EventType.Book, Severity.High, userId, activity + filePath, request.UserIPAddress);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SightseeingSearch, Severity.Normal, 0, activity + "Saving Login request Failed:" + ex.ToString(), request.UserIPAddress);
            }

            //string url = ConfigurationSystem.ActivityServiceConfig["Authenticate"];
            //string responseString = SendRequest(requestString, url);

            #region WCF service call using Web Client for Authenticate method           
            string inputReq = (new  JavaScriptSerializer()).Serialize(request);
            WebClient client = new WebClient();
            client.Headers["Content-type"] = "application/json";
            client.Encoding = Encoding.UTF8;
            var resp = client.UploadString(interfaceURL + "/Authenticate", inputReq);
            AuthenticationResponse response = null;
            if (resp != null)
            {
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                response = jsonSerializer.Deserialize<AuthenticationResponse>(resp);
            }
            #endregion

            //AuthenticationResponse response = client.Authenticate(request);
            try
            {
                Random random = new Random();
                string rndNumber = random.Next(100000, 999999).ToString();
                XmlSerializer serializer = new XmlSerializer(response.GetType());
                StringBuilder sb = new StringBuilder();
                StringWriter strwriter = new StringWriter(sb);
                serializer.Serialize(strwriter, response);

                string filePath = fileSavingPath + "ActivityAuthenticationResponse" + DateTime.Now.ToString("ddMMyyyy_hhmmss") +"_"+rndNumber+ ".xml";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                try
                {
                    doc.Save(filePath);
                }
                catch { }

                Audit.Add(EventType.SightseeingSearch, Severity.High, userId, activity + filePath, request.UserIPAddress);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SightseeingSearch, Severity.Normal, userId, activity + "Saving Login response Failed:" + ex.ToString(), request.UserIPAddress);
            }

            if (response != null && response.Error.ErrorCode == "000")
            {
                TokenId = response.TokenId;
                authenticated = true;
            }
            return authenticated;
        }

        public SightseeingSearchResult[] getActivityResults(SightSeeingReguest request, decimal markup, string markupType)
        {
            SightseeingSearchResult[] results = null;
            try
            {
                authenticate();
                if (!string.IsNullOrEmpty(TokenId))
                {
                    ActivitySearchRequest searchRequest = new ActivitySearchRequest();
                    searchRequest.TokenID = TokenId;
                    searchRequest.UserIPAddress = "10.200.44.29";
                    searchRequest.CountryCode = request.CountryName;
                    searchRequest.CityCode = request.DestinationCode;
                    searchRequest.FromDate = request.TourDate.ToString("yyyy-MM-dd");
                    searchRequest.ToDate = request.TourDate.AddDays(request.NoDays).ToString("yyyy-MM-dd");
                    searchRequest.AdultCount = request.NoOfAdults;
                    int infantCount = 0;
                    foreach (int childAge in request.ChildrenList)
                    {
                        if (childAge < 2)
                        {
                            infantCount++;
                        }
                    }
                    searchRequest.ChildCount = (request.ChildCount - infantCount);
                    searchRequest.InfantCount = infantCount;
                    searchRequest.Category = request.CategoryCodeList.ToArray();
                    searchRequest.Type = request.TypeCodeList.ToArray();
                    searchRequest.ActivityName = request.ItemName;

                    string requestString = JsonConvert.SerializeObject(searchRequest);

                    try
                    {
                        Random random = new Random();
                        string rndNumber = random.Next(100000, 999999).ToString();
                        XmlSerializer serializer = new XmlSerializer(searchRequest.GetType());
                        StringBuilder sb = new StringBuilder();
                        StringWriter strwriter = new StringWriter(sb);
                        serializer.Serialize(strwriter, searchRequest);

                        string filePath = fileSavingPath + "ActivitySearchRequest" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + TokenId +"_"+rndNumber+ ".xml";
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());
                        try
                        {
                            doc.Save(filePath);
                        }
                        catch { }

                        Audit.Add(EventType.SightseeingSearch, Severity.High, userId, activity + filePath, string.Empty);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SightseeingSearch, Severity.Normal, userId, activity + "Saving SightSeeing SearchRequest Failed:" + ex.ToString(), string.Empty);
                    }

                    #region WCF service call using Web Client for ActivitySearch method           
                    string inputsearchRequest = (new JavaScriptSerializer()).Serialize(searchRequest);
                    WebClient client = new WebClient();
                    client.Headers["Content-type"] = "application/json";
                    client.Encoding = Encoding.UTF8;
                    var resp = client.UploadString(interfaceURL + "/ActivitySearch", inputsearchRequest);
                    ActivitySearchResponse searchResponse = null;
                    if (resp != null)
                    {
                        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                        searchResponse = jsonSerializer.Deserialize<ActivitySearchResponse>(resp);
                    }
                    #endregion

                    // ActivitySearchResponse searchResponse = client.ActivitySearch(searchRequest);

                    try
                    {
                        Random random = new Random();
                        string rndNumber = random.Next(100000, 999999).ToString();
                        XmlSerializer serializer = new XmlSerializer(searchResponse.GetType());
                        StringBuilder sb = new StringBuilder();
                        StringWriter strwriter = new StringWriter(sb);
                        serializer.Serialize(strwriter, searchResponse);

                        string filePath = fileSavingPath + "ActivitySearchResponse" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + TokenId +"_"+rndNumber + ".xml";
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());
                        try
                        {
                            doc.Save(filePath);

                        }
                        catch { }

                        Audit.Add(EventType.SightseeingSearch, Severity.High, userId, activity + filePath, string.Empty);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SightseeingSearch, Severity.Normal, userId, activity + "Saving SightSeeing SearchResponse Failed:" + ex.ToString(), string.Empty);
                    }

                    if (searchResponse!=null && searchResponse.Error.ErrorCode == "000")
                    {
                        List<SightseeingSearchResult> searchResults = new List<SightseeingSearchResult>();
                        for (int i = 0; i < searchResponse.SearchResults.Length; i++)
                        {
                            SightseeingSearchResult result = new SightseeingSearchResult();
                            result.CityCode = searchResponse.SearchResults[i].ActivityCityCode;
                            if (searchResponse.SearchResults[i].TransferIncluded)
                            {
                                result.DepaturePointRequired = true;
                            }
                            else
                            {
                                result.DepaturePointRequired = false;
                            }
                            result.Description = searchResponse.SearchResults[i].Overview;
                            result.Duration = searchResponse.SearchResults[i].DurationHours;

                            result.Image = searchResponse.SearchResults[i].ImagePath1;
                            result.ItemCode = searchResponse.SearchResults[i].ResultIndex.ToString();
                            result.ItemName = searchResponse.SearchResults[i].ActivityName;
                            result.CityName = request.CityName;
                            
                            result.Source = SightseeingBookingSource.CZA;
                            if (!string.IsNullOrEmpty(searchResponse.SearchResults[i].ThingsToBring))
                            {
                                string[] additionalInformationList = searchResponse.SearchResults[i].ThingsToBring.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                result.AdditionalInformationList = new string[additionalInformationList.Length];
                                result.AdditionalInformationList = additionalInformationList;
                            }

                            List<DayWiseTourOperation> daywiseTourOperationList = new List<DayWiseTourOperation>();
                            // here we are adding extra one day for no.of days because the tour date is not included in that.
                            for (int j = 0; j < request.NoDays + 1; j++)
                            {
                                DayWiseTourOperation dayWiseTourOperation = new DayWiseTourOperation();
                                List<CT.BookingEngine.TourOperation> lstTourOperation = new List<CT.BookingEngine.TourOperation>();
                                CT.BookingEngine.TourOperation tourOperation = new CT.BookingEngine.TourOperation();
                                DateTime date = request.TourDate.AddDays(j);
                                dayWiseTourOperation.BookingDate = date;

                                //List<string> departureList = new List<string>();
                                //departureList.Add(searchResponse.SearchResults[i].PickupLocation);

                                dayWiseTourOperation.departurePointList = new List<string>();
                                if (!string.IsNullOrEmpty(searchResponse.SearchResults[i].PickupLocation))
                                {
                                    string[] departureList = searchResponse.SearchResults[i].PickupLocation.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                    if (departureList != null && departureList.Length > 0)
                                    {
                                        for (int m = 0; m < departureList.Length; m++)
                                        {
                                            dayWiseTourOperation.departurePointList.Add(departureList[m]);
                                        }
                                    }
                                }
                                else
                                {
                                    dayWiseTourOperation.departurePointList.Add(string.Empty);
                                }
                                dayWiseTourOperation.isHotelRequired = false;
                                bool isUnavailableDate = false;
                                if (searchResponse.SearchResults[i].UnavailableDates != null && searchResponse.SearchResults[i].UnavailableDates.Length > 0)
                                {
                                    if (searchResponse.SearchResults[i].UnavailableDates.Contains(date.ToString("dd/MM/yyyy")))
                                    {
                                        isUnavailableDate = true;
                                    }
                                }
                                decimal totPrice = 0m;
                                decimal ItemPrice = 0m;
                                decimal totalB2BMarkup = 0m;
                                if (!isUnavailableDate)
                                {
                                    tourOperation.ConfirmationCodeList = new Dictionary<string, string>();
                                    //tourOperation.Currency = "AED"; //Vinay said that always Activity gives AED only
                                    tourOperation.Currency = searchResponse.SearchResults[i].Currency;
                                    if (exchangeRates.ContainsKey(tourOperation.Currency))
                                    {
                                        rateOfExchange = exchangeRates[tourOperation.Currency];
                                    }
                                    else
                                    {
                                        rateOfExchange = 1;
                                    }
                                    //checking Transfer type if b2c need to display transaction currency else agentCurrency
                                    if (!string.IsNullOrEmpty(request.TransType) && request.TransType == "B2C")
                                    {
                                        StaticData staticInfo = new StaticData();
                                        staticInfo.BaseCurrency = request.Currency;
                                        Dictionary<string, decimal> baseRateOfExList = new Dictionary<string, decimal>();
                                        baseRateOfExList = staticInfo.CurrencyROE;
                                        if (baseRateOfExList.ContainsKey(agentCurrency))
                                        {
                                            baseRateOfExchange = baseRateOfExList[agentCurrency];
                                        }
                                        else
                                        {
                                            baseRateOfExchange = 1;
                                        }
                                    }

                                    tourOperation.LangName = null;
                                    tourOperation.LanguageCode = null;
                                    tourOperation.SpecialItemList = null;
                                    tourOperation.SpecialItemName = null;
                                    tourOperation.TourLanguageList = null;
                                    ItemPrice = Convert.ToDecimal(searchResponse.SearchResults[i].TotalAmount);
                                    //checking Transfer type if b2c need to display transaction currency else agentCurrency
                                    if (!string.IsNullOrEmpty(request.TransType) && request.TransType == "B2C")
                                    {

                                        totPrice = (ItemPrice) * rateOfExchange;
                                        tourOperation.PriceInfo = new PriceAccounts();
                                        tourOperation.PriceInfo.AgentBaseAmount = totPrice;
                                        tourOperation.PriceInfo.AgentBaseMarkup = ((markupType == "F") ? markup : (Convert.ToDecimal(totPrice) * (markup / 100m)));
                                        tourOperation.PriceInfo.AgentBaseCurrency = agentCurrency;
                                        tourOperation.PriceInfo.AgentBaseROE = rateOfExchange;


                                        //Transaction currency wise Amount calucation
                                        tourOperation.ItemPrice = Math.Round((tourOperation.PriceInfo.AgentBaseAmount * baseRateOfExchange), agentDecimalpoint);
                                        totalB2BMarkup = (tourOperation.PriceInfo.AgentBaseMarkup * baseRateOfExchange);//((markupType == "F") ? markup : (Convert.ToDecimal(tourOperation.ItemPrice) * (markup / 100m)));
                                        tourOperation.PriceInfo.Markup = Math.Round(totalB2BMarkup, agentDecimalpoint);
                                        tourOperation.PriceInfo.MarkupType = markupType;
                                        tourOperation.PriceInfo.MarkupValue = markup;
                                        tourOperation.PriceInfo.SupplierCurrency = tourOperation.Currency;
                                        tourOperation.PriceInfo.SupplierPrice = ItemPrice;
                                        tourOperation.PriceInfo.RateOfExchange = baseRateOfExchange;
                                        tourOperation.PriceInfo.Currency = request.Currency;
                                        tourOperation.PriceInfo.CurrencyCode = request.Currency;
                                        tourOperation.PriceInfo.NetFare = tourOperation.ItemPrice;
                                    }
                                    else
                                    {
                                        totPrice = Math.Round((ItemPrice) * rateOfExchange, agentDecimalpoint);
                                        tourOperation.PriceInfo = new PriceAccounts();
                                        tourOperation.PriceInfo.SupplierCurrency = tourOperation.Currency;
                                        tourOperation.PriceInfo.SupplierPrice = ItemPrice;
                                        tourOperation.PriceInfo.RateOfExchange = rateOfExchange;
                                        tourOperation.PriceInfo.Currency = agentCurrency;
                                        tourOperation.PriceInfo.CurrencyCode = agentCurrency;
                                        totalB2BMarkup = ((markupType == "F") ? markup : (Convert.ToDecimal(totPrice) * (markup / 100m)));
                                        tourOperation.PriceInfo.Markup = Math.Round(totalB2BMarkup, agentDecimalpoint);
                                        tourOperation.PriceInfo.MarkupType = markupType;
                                        tourOperation.PriceInfo.MarkupValue = markup;
                                        tourOperation.ItemPrice = Math.Round(totPrice, agentDecimalpoint);
                                        tourOperation.PriceInfo.NetFare = totPrice;
                                    }
                                    //checking Transfer type if b2c need to display transaction currency else agentCurrency
                                    if (!string.IsNullOrEmpty(request.TransType) && request.TransType == "B2C")
                                    {
                                        tourOperation.Currency = request.Currency;
                                    }
                                    else
                                    {
                                        tourOperation.Currency = agentCurrency;
                                    }
                                    lstTourOperation.Add(tourOperation);
                                    dayWiseTourOperation.tourOperationList = lstTourOperation;
                                    daywiseTourOperationList.Add(dayWiseTourOperation);
                                }
                            }
                            if (daywiseTourOperationList != null && daywiseTourOperationList.Count > 0)
                            {
                                result.DayWisetourOperationList = daywiseTourOperationList;
                            }
                            else
                            {
                                continue;
                            }
                            Dictionary<string, string> categoryNames = new Dictionary<string, string>();
                            if (searchResponse.SearchResults[i].ThemeNames != null && searchResponse.SearchResults[i].ThemeNames.Length > 0)
                            {
                                for (int k = 0; k < searchResponse.SearchResults[i].ThemeNames.Length; k++)
                                {
                                    categoryNames.Add(searchResponse.SearchResults[i].ThemeNames[k], searchResponse.SearchResults[i].ThemeNames[k]);
                                }
                            }
                            result.EssentialInformationList = null;
                            result.SightseeingCategoryList = categoryNames;

                            if (!string.IsNullOrEmpty(searchResponse.SearchResults[i].CancellationPolicy))
                            {
                                result.CancellationCharges = searchResponse.SearchResults[i].CancellationPolicy.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                            }
                            searchResults.Add(result);
                        }
                        results = searchResults.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.Normal, userId, activity + "failed when getting the Activity results:" + ex.ToString(), ipAddress);
            }
            return results;
        }

        public SightSeeingCancellationDetails getCancellationDetails(int resultIndex, string userIPAddress)
        {
            SightSeeingCancellationDetails response = new SightSeeingCancellationDetails();
            try
            {
                if (resultIndex > 0 && !string.IsNullOrEmpty(userIPAddress))
                {

                    if (string.IsNullOrEmpty(tokenId))
                    {
                        authenticate();
                    }

                    ActivityCancellationRequest cancellationRequest = new ActivityCancellationRequest();
                    cancellationRequest.TokenId = tokenId;
                    cancellationRequest.ResultIndex = resultIndex;
                    cancellationRequest.UserIPAddress = userIPAddress;
                    string requestString = JsonConvert.SerializeObject(cancellationRequest);

                    try
                    {
                        Random random = new Random();
                        string rndNumber = random.Next(100000, 999999).ToString();
                        XmlSerializer serializer = new XmlSerializer(cancellationRequest.GetType());
                        StringBuilder sb = new StringBuilder();
                        StringWriter strwriter = new StringWriter(sb);
                        serializer.Serialize(strwriter, cancellationRequest);

                        string filePath = fileSavingPath + "ActivityCancellationRequest" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + TokenId+"_"+rndNumber + ".xml";
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());
                        try
                        {
                            doc.Save(filePath);
                        }
                        catch { }

                        Audit.Add(EventType.SightseeingSearch, Severity.High, userId, activity + filePath, string.Empty);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SightseeingSearch, Severity.Normal, userId, activity + "Saving SightSeeing SearchRequest Failed:" + ex.ToString(), string.Empty);
                    }

                    //string url = ConfigurationSystem.ActivityServiceConfig["CancellationPolicy"];
                    //string cancellationResponse = SendRequest(requestString, url);

                    #region WCF service call using Web Client for Authenticate method           
                    string inputReq = (new  JavaScriptSerializer()).Serialize(cancellationRequest);
                    WebClient client = new WebClient();
                    client.Headers["Content-type"] = "application/json";
                    client.Encoding = Encoding.UTF8;
                    var resp = client.UploadString(interfaceURL + "/ActivityCancellationDetails", inputReq);
                    ActivityCancellationResponse cancellationResponse = null;
                    if (resp != null)
                    {
                        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                        cancellationResponse = jsonSerializer.Deserialize<ActivityCancellationResponse>(resp);
                        response = ReadActivityCancellationResponse(cancellationResponse);
                    }
                    #endregion

                   // ActivityCancellationResponse cancellationResponse = client.ActivityCancellationDetails(cancellationRequest);
                   // response = ReadActivityCancellationResponse(cancellationResponse);
                    try
                    {
                        Random random = new Random();
                        string rndNumber = random.Next(100000, 999999).ToString();
                        XmlSerializer serializer = new XmlSerializer(response.GetType());
                        StringBuilder sb = new StringBuilder();
                        StringWriter strwriter = new StringWriter(sb);
                        serializer.Serialize(strwriter, response);

                        string filePath = fileSavingPath + "ActivityCancellationResponse" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + TokenId +"_"+rndNumber+ ".xml";
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(sb.ToString());
                        try
                        {
                            doc.Save(filePath);
                        }
                        catch { }

                        Audit.Add(EventType.SightseeingSearch, Severity.High, userId, activity + filePath, string.Empty);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SightseeingSearch, Severity.Normal, userId, activity + "Saving SightSeeing SearchResponse Failed:" + ex.ToString(), string.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SightseeingSearch, Severity.Normal, 0, activity + "Failed due to:" + ex.ToString(), string.Empty);
            }

            return response;
        }
        private SightSeeingCancellationDetails ReadActivityCancellationResponse(ActivityCancellationResponse cancellationResponse)
        {
            SightSeeingCancellationDetails response = new SightSeeingCancellationDetails();

            if (cancellationResponse !=null && cancellationResponse.Error.ErrorCode == "000")
            {
                CancellationDetails cancelDetails = cancellationResponse.CancellationDetails;
                response.ActivityCityCode = cancelDetails.ActivityCityCode;
                response.ActivityDetails = cancelDetails.ActivityDetails;
                if (cancelDetails.ActivityFlexDetails != null && cancelDetails.ActivityFlexDetails.Length > 0)
                {
                    response.SightSeeingFlexFields = new SightSeeingFlexFields[cancelDetails.ActivityFlexDetails.Length];
                    for (int i = 0; i < cancelDetails.ActivityFlexDetails.Length; i++)
                    {
                        SightSeeingFlexFields flexFields = new SightSeeingFlexFields();
                        flexFields.FlexFieldDataType = cancelDetails.ActivityFlexDetails[i].FlexFieldDataType;
                        flexFields.FlexFieldsMandatory = cancelDetails.ActivityFlexDetails[i].FlexFieldsMandatory;
                        flexFields.FlexFieldsName = cancelDetails.ActivityFlexDetails[i].FlexFieldsName;
                        response.SightSeeingFlexFields[i] = flexFields;
                    }
                }
                response.ActivityName = cancelDetails.ActivityName;
                response.CancellationPolicies = cancelDetails.CancellationPolicies;
                response.DeparturePoint = cancelDetails.DeparturePoint;
                response.Image = cancelDetails.Image;
                response.Includes = cancelDetails.Includes;
                response.Introduction = cancelDetails.Introduction;
                response.ItineraryDetails = cancelDetails.ItineraryDetails;
                response.Notes = cancelDetails.Notes;
                response.ResultIndex = cancelDetails.ResultIndex;
                if (cancelDetails.TourOperation != null && cancelDetails.TourOperation.Length > 0)
                {
                    List<CT.BookingEngine.ActivityTourOperation> lstToutOperation = new List<CT.BookingEngine.ActivityTourOperation>();
                    for (int i = 0; i < cancelDetails.TourOperation.Length; i++)
                    {
                        CT.BookingEngine.ActivityTourOperation tourOperation = new CT.BookingEngine.ActivityTourOperation();
                        tourOperation.CityCode = cancelDetails.TourOperation[i].CityCode;
                        tourOperation.DepName = cancelDetails.TourOperation[i].DepName;
                        tourOperation.FromTime = cancelDetails.TourOperation[i].FromTime;
                        tourOperation.ItemCode = cancelDetails.TourOperation[i].ItemCode;
                        tourOperation.ToTime = cancelDetails.TourOperation[i].ToTime;
                        tourOperation.FromDate = cancelDetails.TourOperation[i].FromDate;
                        tourOperation.ToDate = cancelDetails.TourOperation[i].ToDate;
                        lstToutOperation.Add(tourOperation);
                    }
                    response.TourOperation = lstToutOperation;
                }
                response.Excludes = cancelDetails.Excludes;

            }
            return response;
        }
        public BookingResponse ActivityBooking(ref SightseeingItinerary itinerary)
        {
            BookingResponse bookingResponse = new BookingResponse();
            if (itinerary != null)
            {
                authenticate();
                ActivityBookingRequest bookingRequest = generateBookingRequest(itinerary, tokenId);
                string requestString = JsonConvert.SerializeObject(bookingRequest);

                try
                {
                    Random random = new Random();
                    string rndNumber = random.Next(100000, 999999).ToString();
                    XmlSerializer serializer = new XmlSerializer(bookingRequest.GetType());
                    StringBuilder sb = new StringBuilder();
                    StringWriter strwriter = new StringWriter(sb);
                    serializer.Serialize(strwriter, bookingRequest);

                    string filePath = fileSavingPath + "ActivityBookingRequest" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + TokenId+"_"+rndNumber + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sb.ToString());
                    try
                    {
                        doc.Save(filePath);
                    }
                    catch { }

                    Audit.Add(EventType.SightseeingSearch, Severity.High, userId, activity + filePath, string.Empty);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SightseeingSearch, Severity.Normal, userId, activity + "Saving SightSeeing BookingRequest Failed:" + ex.ToString(), string.Empty);
                }
                //string url = ConfigurationSystem.ActivityServiceConfig["Booking"];
                //string response = null;//SendRequest(requestString, url);

                #region WCF service call using Web Client for ActivityBooking method           
                string inputReq = (new JavaScriptSerializer()).Serialize(bookingRequest);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                var resp = client.UploadString(interfaceURL + "/ActivityBooking", inputReq);
                ActivityBookingResponse bookResponse = null;
                if (resp != null)
                {
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    bookResponse = jsonSerializer.Deserialize<ActivityBookingResponse>(resp);
                }
                #endregion

               // CZActivity.ActivityAPI.ActivityBookingResponse bookResponse = client.ActivityBooking(bookingRequest);
               ////ReadActivityBookingResponse(response);

                try
                {
                    Random random = new Random();
                    string rndNumber = random.Next(100000, 999999).ToString();
                    XmlSerializer serializer = new XmlSerializer(bookResponse.GetType());
                    StringBuilder sb = new StringBuilder();
                    StringWriter strwriter = new StringWriter(sb);
                    serializer.Serialize(strwriter, bookResponse);

                    string filePath = fileSavingPath + "ActivityBookingResponse" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + TokenId +"_"+rndNumber+ ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sb.ToString());
                    try
                    {
                        doc.Save(filePath);
                    }
                    catch { }

                    Audit.Add(EventType.SightseeingSearch, Severity.High, userId, activity + filePath, string.Empty);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SightseeingSearch, Severity.Normal, userId, activity + "Saving SightSeeing SearchResponse Failed:" + ex.ToString(), string.Empty);
                }

                if (bookResponse!=null && ( bookResponse.Error.ErrorCode == "000" || bookResponse.Error.ErrorCode == "013"))
                {
                    bookingResponse.BookingId = bookResponse.BookingId;
                    bookingResponse.ConfirmationNo = bookResponse.TripId;
                    bookingResponse.ProdType = ProductType.SightSeeing;
                    bookingResponse.Status = BookingResponseStatus.Successful;

                    itinerary.BookingStatus = SightseeingBookingStatus.Confirmed;
                    itinerary.ConfirmationNo = bookResponse.TripId;
					
					//added by venkatesh 13-06-2017
					//because will we are booking the exception is raised in the saving part in MetaSearchEngine Book Method
                    itinerary.BookingReference = "";
                    itinerary.SupplierInfo = "";
                    if (string.IsNullOrEmpty(itinerary.Note))
                    {
                        itinerary.Note = "";
                    }
                    itinerary.TelePhno = "";

                    ////Added on 21-06-2017   
                    //itinerary.Price.AgentBaseCurrency = agentCurrency;
                    //itinerary.Price.AgentBaseROE = baseRateOfExchange;
                    //itinerary.Price.AgentBaseAmount = 0;
                    //itinerary.Price.AgentBaseTax = 0;
                    //itinerary.Price.AgentBaseMarkup = 0;
                    //itinerary.Price.AgentBaseB2CMarkup = 0;
                }
                else
                {
                    bookingResponse.Status = BookingResponseStatus.Failed;
                    bookingResponse.Error = bookResponse.Error.ErrorMessage;
                    bookingResponse.ConfirmationNo = "";
                    bookingResponse.BookingId = 0;
                }
            }
            else
            {
                throw new Exception("Request itinerary is empty");
            }
            return bookingResponse;
        }

        private ActivityBookingRequest generateBookingRequest(SightseeingItinerary itinerary, string tokenId)
        {
            ActivityBookingRequest request = new ActivityBookingRequest();
            request.AdultCount = itinerary.AdultCount;
            int infantCount = 0;
            string childAges=string.Empty;
            if (itinerary.ChildAge != null && itinerary.ChildAge.Count > 0)
            {
                foreach (int age in itinerary.ChildAge)
                {
                    if (age < 2)
                    {
                        infantCount++;
                    }
                    if(!string.IsNullOrEmpty(childAges))
                    {
                        childAges=childAges+","+age.ToString();
                    }
                    else
                    {
                        childAges=age.ToString();
                    }
                }
            }
            request.ChildCount = (itinerary.ChildCount) - infantCount;
            request.InfantCount = infantCount;
            request.EmailAddress = itinerary.Email;
            request.JourneyDate = itinerary.TourDate.ToString("yyyy-MM-dd");
            
            DOTWCountry dotwApi = new DOTWCountry();
            Dictionary<string, string> Countries = dotwApi.GetAllCountries();
            itinerary.Nationality = Country.GetCountryCodeFromCountryName(Countries[itinerary.Nationality]);

            request.Nationality = itinerary.Nationality;
            if (itinerary.PaxNames != null && itinerary.PaxNames.Count > 0)
            {
                string[] paxNames = itinerary.PaxNames[0].Split(' ');
                if (paxNames != null && paxNames.Length > 0)
                {
                    string[] firstName = paxNames[0].Split('.');
                    if (firstName != null && firstName.Length > 0)
                    {
                        request.PassengerFirstName = firstName[0];
                        if (paxNames.Length > 1 && !string.IsNullOrEmpty(paxNames[1]))
                        {
                            request.PassengerLastName = paxNames[1];
                        }
                    }
                }
            }
            request.PhoneNumber = itinerary.PhNo;
            request.ResultIndex = Convert.ToInt32(itinerary.ItemCode);
            request.TokenID = tokenId;
            request.TotalAmount = itinerary.Price.SupplierPrice;
            request.UserIPAddress = "10.200.44.29";
            request.ActivityFlexDetails = null;//here we are not including the flex fields so are making it null.
            request.TransType = itinerary.TransType;
            request.DeparturePoint = itinerary.DepPointInfo;
            DateTime departureTime = Convert.ToDateTime(itinerary.TourDate.ToShortDateString()+itinerary.DepTime);
            request.DepartureTime = departureTime;
            request.FlightInfo = new ActivityAPI.FlightDetails();
            request.FlightInfo.Arrivaltime = itinerary.FlightInfo.Arrivaltime;
            request.FlightInfo.Destination = itinerary.FlightInfo.Destination;
            request.FlightInfo.FligtNo = itinerary.FlightInfo.FligtNo;
            request.FlightInfo.PNR = itinerary.FlightInfo.PNR;
            request.FlightInfo.ChildCount = request.InfantCount + request.ChildCount;
            request.FlightInfo.ChildAges = childAges;
            return request;
        }
        private CZActivity.ActivityAPI.ActivityBookingResponse ReadActivityBookingResponse(string responseString)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ObjectCreationHandling = ObjectCreationHandling.Auto;
            settings.StringEscapeHandling = StringEscapeHandling.Default;
            settings.Formatting = Newtonsoft.Json.Formatting.Indented;

            TextReader reader1 = new StringReader(responseString);
            CZActivity.ActivityAPI.ActivityBookingResponse response = (CZActivity.ActivityAPI.ActivityBookingResponse)JsonSerializer.Create(settings).Deserialize(reader1, typeof(CZActivity.ActivityAPI.ActivityBookingResponse));

            return response;
        }
    }
  

}
