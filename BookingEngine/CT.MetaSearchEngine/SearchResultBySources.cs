using CT.BookingEngine;

namespace CT.MetaSearchEngine
{
    public class SearchResultBySources //ziya-todo
    {
        /// <summary>
        /// </summary>
        private string source;

        public string Source
        {
            get { return source; }
            set { source = value; }
        }

        // Search result
        private SearchResult[] result;

        public SearchResult[] Result
        {
            get { return result; }
            set { result = value; }
        }

        private string error;

        public string Error
        {
            get { return error; }
            set { error = value; }
        }
    }

    /// <summary>
    /// For hotel result
    /// </summary>
    public class HotelSearchResultBySources
    {
        /// <summary>
        /// Source Like 'Desiya'
        /// </summary>
        private string source;

        public string Source
        {
            get { return source; }
            set { source = value; }
        }

        // Search result
        private HotelSearchResult[] result;

        public HotelSearchResult[] Result
        {
            get { return result; }
            set { result = value; }
        }

        private string error;

        public string Error
        {
            get { return error; }
            set { error = value; }
        }
    }
    public class TransferSearchResultBySources 
    {
        /// <summary>
        /// Source Like 'GTA'
        /// </summary>
        private string source;

        public string Source
        {
            get { return source; }
            set { source = value; }
        }

        // Search result
        private TransferSearchResult[] result;

        public TransferSearchResult[] Result
        {
            get { return result; }
            set { result = value; }
        }

        private string error;

        public string Error
        {
            get { return error; }
            set { error = value; }
        }
    }
    public class SightseeingSearchResultBySources
    {
        /// <summary>
        /// Source Like 'GTA'
        /// </summary>
        private string source;

        public string Source
        {
            get { return source; }
            set { source = value; }
        }

        // Search result
        private SightseeingSearchResult[] result;

        public SightseeingSearchResult[] Result
        {
            get { return result; }
            set { result = value; }
        }

        private string error;

        public string Error
        {
            get { return error; }
            set { error = value; }
        }
    }

    public class FleetResultBySources
    {
        /// <summary>
        /// Source Like 'Sayara'
        /// </summary>
        private string source;

        public string Source
        {
            get { return source; }
            set { source = value; }
        }

        // Search result
        private FleetSearchResult[] result;

        public FleetSearchResult[] Result
        {
            get { return result; }
            set { result = value; }
        }

        private string error;

        public string Error
        {
            get { return error; }
            set { error = value; }
        }
    }
    
}
