using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine.GDS;
using System.Threading;
using System.Data;
using System.Diagnostics;
using CT.Configuration;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Configuration;
using System.Collections;
using System.Transactions;
using CT.AccountingEngine;
using CT.Core;
using System.Text.RegularExpressions;
//using Technology.IRCTC;
using System.Web;
using System.Xml;
using TBOAir;
//using CT.BookingEngine.GDS;
//using HotelConnect;
//using FlyDubai;
//using TouricoApi;
//using AirDeccan;
//using Galileo;
//using GoAir;
//using Hermes;
//using IANApi;
//using SpiceJet;
//using WSTApi;




//using InsuranceBookingEngine;
//using IFFCOTokio;


namespace CT.MetaSearchEngine
{

    public class MetaSearchEngine
    {
        //private string timeLogFile = @"C:\configFiles\timeLog.csv";

        #region SessionID for booking
        //TODO: Some processes need to fired in a seperate thread. eg Clean();
        //TODO: Maintain only one session and remove older session.
        /// <summary>
        /// Booking sessionId. Runs from Time of search to Booking/Ticketing completion.
        /// </summary>
        private string sessionId;
        /// <summary>
        /// Session Id for current booking.
        /// </summary>
        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }

       
        private int appUserId;

        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }
        private Int64 agencyId;

        private LoginInfo loginInfo;

        public LoginInfo SettingsLoginInfo
        {
            get { return loginInfo; }
            set { loginInfo = value; }
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public MetaSearchEngine() { }

        /// <summary>
        /// Constructor with sessionId.
        /// </summary>
        /// <param name="sessionId">Session Id to initiate with.</param>
        public MetaSearchEngine(string sessionId)
        {
            this.sessionId = sessionId;
        }

        /// <summary>
        /// To Create sessionId, and doing entries of session data in BookingSession
        /// </summary>
        /// <returns>sessionId as a string</returns>
        public string StartSession()
        {
            sessionId = Guid.NewGuid().ToString();
            lock (Basket.BookingSession)
            {
                try
                {
                    Basket.BookingSession.Add(sessionId, new Dictionary<string, object>());
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception while adding in Basket.BookingSession.Attempt 1. Error Message:" + ex.Message + " Stack trace = " + ex.StackTrace + " | " + DateTime.Now + "|", "");
                    try
                    {
                        Basket.BookingSession.Add(sessionId, new Dictionary<string, object>());
                    }
                    catch (Exception excep)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception while adding in Basket.BookingSession.Attempt 2. Error Message:" + excep.Message + " Stack trace = " + excep.StackTrace + " | " + DateTime.Now + "|", "");
                        Basket.BookingSession = new BookingSessionThreadSafeDic();
                        Basket.FlightBookingSession = new FlightBookingSessionThreadSafeDic();
                        throw new Exception("New search session creation failed." + excep.Message);
                    }
                }
                try
                {
                    Basket.BookingSession[sessionId].Add("time", (object)DateTime.Now);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception while adding in Basket.BookingSession[sessionId].Add . Attempt 1. Error Message:" + ex.Message + " Stack trace = " + ex.StackTrace + " | " + DateTime.Now + "|", "");

                    try
                    {
                        Basket.BookingSession[sessionId].Add("time", (object)DateTime.Now);
                    }
                    catch (Exception excep)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception while adding in Basket.BookingSession[sessionId].Add . Attempt 2. Error Message:" + excep.Message + " Stack trace = " + excep.StackTrace + " | " + DateTime.Now + "|", "");
                        Basket.BookingSession = new BookingSessionThreadSafeDic();
                        Basket.FlightBookingSession = new FlightBookingSessionThreadSafeDic();
                        throw new Exception("New search session creation failed." + excep.Message);

                    }
                }
            }
            lock (Basket.FlightBookingSession)
            {
                try
                {
                    Basket.FlightBookingSession.Add(sessionId, new FlightSessionData());
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception while adding in Basket.FlightBookingSession . Attempt 1. Error Message:" + ex.Message + " Stack trace = " + ex.StackTrace + " | " + DateTime.Now + "|", "");
                    try
                    {
                        Basket.FlightBookingSession.Add(sessionId, new FlightSessionData());
                    }
                    catch (Exception excep)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception while adding in Basket.FlightBookingSession . Attempt 2. Error Message:" + excep.Message + " Stack trace = " + excep.StackTrace + " | " + DateTime.Now + "|", "");
                        Basket.FlightBookingSession = new FlightBookingSessionThreadSafeDic();
                        Basket.BookingSession = new BookingSessionThreadSafeDic();
                        throw new Exception("New search session creation failed." + excep.Message);


                    }
                }
                try
                {
                    Basket.FlightBookingSession[sessionId].SearchTime = DateTime.Now;
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception while adding in FlightBookingSession[sessionId].SearchTime . Attempt 1. Error Message:" + ex.Message + " Stack trace = " + ex.StackTrace + " | " + DateTime.Now + "|", "");
                    try
                    {
                        Basket.FlightBookingSession[sessionId].SearchTime = DateTime.Now;
                    }
                    catch (Exception excep)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception while adding in FlightBookingSession[sessionId].SearchTime . Attempt 2. Error Message:" + excep.Message + " Stack trace = " + excep.StackTrace + " | " + DateTime.Now + "|", "");
                        Basket.FlightBookingSession = new FlightBookingSessionThreadSafeDic();
                        Basket.BookingSession = new BookingSessionThreadSafeDic();
                        throw new Exception("New search session creation failed." + excep.Message);
                    }
                }
            }
            return sessionId;
        }
        //public string StartInsuranceSession()
        //{
        //    sessionId = Guid.NewGuid().ToString();
        //    lock (Basket.InsuranceBookingSession)
        //    {
        //        try
        //        {
        //            Basket.InsuranceBookingSession.Add(sessionId, new InsuranceSessionData());
        //        }
        //        catch (Exception ex)
        //        {
        //            CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception while adding in Basket.InsuranceBookingSession . Attempt 1. Error Message:" + ex.Message + " Stack trace = " + ex.StackTrace + " | " + DateTime.Now + "|", "");
        //            try
        //            {
        //                Basket.InsuranceBookingSession.Add(sessionId, new InsuranceSessionData());
        //            }
        //            catch (Exception excep)
        //            {
        //                CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception while adding in Basket.InsuranceBookingSession . Attempt 2. Error Message:" + excep.Message + " Stack trace = " + excep.StackTrace + " | " + DateTime.Now + "|", "");
        //                Basket.InsuranceBookingSession = new InsuranceBookingSessionThreadSafeDic();
        //                throw new Exception("New search session creation failed." + excep.Message);
        //            }
        //        }
        //        try
        //        {
        //            Basket.InsuranceBookingSession[sessionId].Time = DateTime.Now;
        //        }
        //        catch (Exception ex)
        //        {
        //            CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception while adding in Basket.InsuranceBookingSession[sessionId].Add . Attempt 1. Error Message:" + ex.Message + " Stack trace = " + ex.StackTrace + " | " + DateTime.Now + "|", "");

        //            try
        //            {
        //                Basket.InsuranceBookingSession[sessionId].Time = DateTime.Now;
        //            }
        //            catch (Exception excep)
        //            {
        //                CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception while adding in Basket.InsuranceBookingSession[sessionId].Add . Attempt 2. Error Message:" + excep.Message + " Stack trace = " + excep.StackTrace + " | " + DateTime.Now + "|", "");
        //                Basket.InsuranceBookingSession = new InsuranceBookingSessionThreadSafeDic();
        //                throw new Exception("New search session creation failed." + excep.Message);

        //            }
        //        }
        //    }
        //    return sessionId;
        //}


        /// <summary>
        /// Checks if the given sessionId corresponds to an active booking session or session is expired.
        /// </summary>
        /// <param name="sessionId">sessionId to be checked.</param>
        /// <returns>True if the booking session is active.</returns>
        public static bool IsActiveSession(string sessionId)
        {
            return sessionId != null ? Basket.BookingSession.ContainsKey(sessionId) : false;
        }
        public static bool IsActiveInsuranceSession(string sessionId)
        {
            return sessionId != null ? Basket.InsuranceBookingSession.ContainsKey(sessionId) : false;
        }
        /// <summary>
        /// Cleans the inactive/expired sessions from memory.
        /// </summary>
        public void ClearSession()
        {
            Thread clean = new Thread(CleanBasket);
            clean.Start();
        }

        /// <summary>
        /// Cleans the inactive/expired sessions from memory.
        /// </summary>
        private void CleanBasket()
        {
            try
            {
                if (Basket.FlightBookingSession.ContainsKey(sessionId))
                {
                    if ((Basket.FlightBookingSession[sessionId].DumpOnClean && Convert.ToBoolean(ConfigurationSystem.BookingEngineConfig["dumpError"])) || Convert.ToBoolean(ConfigurationSystem.BookingEngineConfig["dumpAll"]))
                    {
                        Basket.DumpSession(sessionId);
                    }
                    Basket.BookingSession.Remove(sessionId);
                    Basket.FlightBookingSession.Remove(sessionId);
                    TimeSpan basketCleanTime = TimeSpan.FromMinutes(Convert.ToDouble(ConfigurationSystem.BookingEngineConfig["basketCleanTime"]));
                    if (Basket.LastCleaned + basketCleanTime < DateTime.Now)
                    {
                        Basket.Clean();
                    }
                }
                if (Basket.InsuranceBookingSession.ContainsKey(sessionId))
                {
                    Basket.InsuranceBookingSession.Remove(sessionId);
                    TimeSpan basketCleanTime = TimeSpan.FromMinutes(Convert.ToDouble(ConfigurationSystem.BookingEngineConfig["basketCleanTime"]));
                    if (Basket.LastCleaned + basketCleanTime < DateTime.Now)
                    {
                        Basket.Clean();
                    }
                }
            }
            catch (Exception excep)
            {
                try
                {
                    CT.Core.Email.Send("Dump Error", Util.GetExceptionInformation(excep, "Error occured while session dump"));
                }
                catch (Exception) { }   // Just for thread not to crash.
            }
        }

        /// <summary>
        /// Makes a log entry with given entry as detail.
        /// </summary>
        /// <param name="sessionId">Session Id for which the log is to be added.</param>
        /// <param name="log"></param>
        //public static void LogActivity(string sessionId, string log)
        //{
        //    if (Basket.FlightBookingSession.ContainsKey(sessionId))
        //    {
        //        Basket.FlightBookingSession[sessionId].Log.Add(log);
        //    }
        //}

        /// <summary>
        /// Logs the xml in memory with descriptive key given.
        /// </summary>
        /// <param name="sessionId">Session Id for which the log is to be added.</param>
        /// <param name="xml">The XML message string.</param>
        public static void LogXml(string sessionId, string xml)
        {
            if (Basket.FlightBookingSession.ContainsKey(sessionId))
            {
                Basket.FlightBookingSession[sessionId].XmlMessage.Add(xml);
            }
        }

        /// <summary>
        /// Sets Dump on so that the instance will be dumped in a file before cleaning.
        /// </summary>
        /// <param name="sessionId">Session Id for which the dump is to be turned on.</param>
        public static void SetDumpOn(string sessionId)
        {
            if (Basket.FlightBookingSession.ContainsKey(sessionId))
            {
                Basket.FlightBookingSession[sessionId].DumpOnClean = true;
            }
        }
        #endregion

        #region Search
        /// <summary>
        /// Request Object
        /// </summary>
        private SearchRequest request;

        /// <summary>
        /// Hotel search request
        /// </summary>
        private HotelRequest hotelRequest;

        /// <summary>
        /// Transfer Search request
        /// </summary>
        private TransferRequest transferRequest;
         //<summary>
         //Sightseeing Search Request
         //</summary>
        private SightSeeingReguest sightseeingRequest;

        /// <summary>
        /// It contains data for Threads Results, error, Thread source type 
        /// </summary>
        private List<SearchResultBySources> SearchResBySrc = new List<SearchResultBySources>();

        /// <summary>
        /// hotel search result by sources
        /// </summary>
        private List<HotelSearchResultBySources> hotelSearchResBySrc = new List<HotelSearchResultBySources>();
        /// <summary>
        /// Transfer search result by sources
        /// </summary>
        private List<TransferSearchResultBySources> transferSearchResBySrc = new List<TransferSearchResultBySources>();
        /// <summary>
        /// Sightseeing Search Result By Source
        /// </summary>
        private List<SightseeingSearchResultBySources> sightseeingSearchResBySrc = new List<SightseeingSearchResultBySources>();
        /// <summary>
        /// event Flag for threads - Auto Reser Event is used for communicating with threads already running
        /// </summary>
        private AutoResetEvent[] eventFlag = new AutoResetEvent[0];

        /// <summary>
        ///  List Of Threads to run
        /// </summary>       
        private Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();

        /// <summary>
        /// Get result for given request
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public SearchResult[] GetResults(SearchRequest req)
        {
            StartSession();
            Trace.TraceInformation("MetaSearchEngine.GetResults Entered");
            SearchResult[] result = new SearchResult[0];
            //If any required paranmeter is missing in the search request, return no result without source hit
            //if (IsValidSearchRequest(req))
            {
                request = req;
                // Add Threads to be executed
                //listOfThreads.Add("1P", new WaitCallback(SearchResultWs));
                listOfThreads.Add("SG", new WaitCallback(SearchResultSj));
                //listOfThreads.Add("6E", new WaitCallback(SearchResultInd));
                //listOfThreads.Add("DN", new WaitCallback(SearchResultDN));
                //listOfThreads.Add("I7", new WaitCallback(SearchResultPm));
                //listOfThreads.Add("ZS", new WaitCallback(SearchResultSa));
                ////listOfThreads.Add("1A", new WaitCallback(SearchResultAm));
                //listOfThreads.Add("9H", new WaitCallback(SearchResultMdlr));
                //listOfThreads.Add("G8", new WaitCallback(SearchResultGA));
                //listOfThreads.Add("1G", new WaitCallback(SearchResultGalileo));
                //listOfThreads.Add("1H", new WaitCallback(SearchResultHermes));
                listOfThreads.Add("FZ", new WaitCallback(SearchResultFlyDubai));
                listOfThreads.Add("G9", new WaitCallback(SearchResultAA));
                listOfThreads.Add("UA", new WaitCallback(SearchResultUAPI));
                listOfThreads.Add("TA", new WaitCallback(SearchResultTBOAir));
                // pick origin, destinion and sources from request object
                Dictionary<string, int> readySources = GetReadySources();

                eventFlag = new AutoResetEvent[readySources.Count];
                int maxTimeOut = 0;
                int i = 0;
                foreach (KeyValuePair<string, int> activeSource in readySources)
                {
                    maxTimeOut = (int)activeSource.Value > maxTimeOut ? (int)activeSource.Value : maxTimeOut;
                    i++;
                }

                int j = 0;

                foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                {
                    if (readySources.ContainsKey(deThread.Key))
                    {
                        ThreadPool.QueueUserWorkItem(deThread.Value, j);
                        eventFlag[j] = new AutoResetEvent(false);
                        j++;
                    }
                }

                if (j != 0)
                {
                    if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 0, maxTimeOut), true))
                    {
                        //TODO: audit which thread is timed out                
                    }
                }

                //// combined sources    
                result = CombineSources(request);

                foreach (SearchResult searchResult in result)
                {
                    if (searchResult.ResultBookingSource != BookingSource.WorldSpan && searchResult.ResultBookingSource != BookingSource.Amadeus)
                    {
                        CalculateDuration(searchResult);
                    }
                }
            }
            Trace.TraceInformation("MetaSearchEngine.GetResults Exit");
            Basket.FlightBookingSession[sessionId].Result = result;
            return result;
        }

        /// <summary>
        /// To save Lcc's ground time, accumulation time and duration(flight time)
        /// </summary>
        /// <param name="result">SearchResult of Flight of type SearchResult</param>
        public static void CalculateDuration(SearchResult result)
        {
            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    if (j == 0)
                    {
                        result.Flights[i][j].AccumulatedDuration = result.Flights[i][j].Duration;
                    }
                    else
                    {
                        result.Flights[i][j].GroundTime = result.Flights[i][j].DepartureTime - result.Flights[i][j - 1].ArrivalTime;
                        result.Flights[i][j].AccumulatedDuration = result.Flights[i][j].GroundTime + result.Flights[i][j].Duration + result.Flights[i][j - 1].AccumulatedDuration;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="destination"></param>
        /// <returns>Dictionary<string, int></returns>
        private Dictionary<string, int> GetReadySources()
        {
            Trace.TraceInformation("MetaSearchEngine.GetReadySources Entered");
            // getting data from configuration xml file            
            Dictionary<string, string> sourcesConfig = ConfigurationSystem.ActiveSources;
            Airline airline = new Airline();
            Dictionary<string, List<string>> airlineSources = CT.Core.Airline.GetAirlineGDS();
            Dictionary<string, string> airlineGDS = new Dictionary<string, string>();
            List<string> domesticGDS = new List<string>();
            foreach (KeyValuePair<string, List<string>> ga in airlineSources)
            {
                bool onForDomestic = false;
                for (int i = 0; i < ga.Value.Count; i++)
                {
                    if (!airlineGDS.ContainsKey(ga.Value[i]))
                    {
                        airlineGDS.Add(ga.Value[i], ga.Key);
                    }
                    if (ga.Value[i].Length == 2)
                    {
                        onForDomestic = true;
                    }
                }
                if (onForDomestic)
                {
                    domesticGDS.Add(ga.Key);
                }
            }
            //Hermes GDS Type source : Add new Airline here
            List<string> allHermesLcc = new List<string>(new string[] { "IX" });
            foreach (string code in allHermesLcc)
            {
                airlineGDS.Add(code, "1H");
            }
            List<string> allLcc = new List<string>(new string[] { "6E", "SG", "DN", "G8", "I7", "9H", "ZS", "FZ", "G9", "TA" });
            foreach (string lccSource in allLcc)
            {
                airlineGDS.Add(lccSource, lccSource);
            }
            List<string> gdsPrefList = new List<string>();
            for (int i = 0; i < request.Segments[0].PreferredAirlines.Length; i++)
            {
                string code = request.Segments[0].PreferredAirlines[i] = request.Segments[0].PreferredAirlines[i].ToUpper();
                if (code.Length == 2 && !allHermesLcc.Contains(code) && !allLcc.Contains(code))
                {
                    gdsPrefList.Add(code);
                }
            }

            Dictionary<string, int> sources = new Dictionary<string, int>();
            //if (request.IsDomestic)
            //{
            //    for (int i = 0; i < request.Sources.Count; i++)
            //    {
            //        if (request.Sources[i].Length == 2 && sourcesConfig.ContainsKey(request.Sources[i]) && !sources.ContainsKey(request.Sources[i]))
            //        {
            //            sources.Add(request.Sources[i], sourcesConfig[request.Sources[i]]);
            //        }
            //    }
            //    if (request.Segments[0].PreferredAirlines.Length > 0)
            //    {
            //        for (int i = 0; i < request.Segments[0].PreferredAirlines.Length; i++)
            //        {
            //            if (!sources.ContainsKey(airlineGDS[request.Segments[0].PreferredAirlines[i]])
            //                && sourcesConfig.ContainsKey(airlineGDS[request.Segments[0].PreferredAirlines[i]]))
            //            {
            //                sources.Add(airlineGDS[request.Segments[0].PreferredAirlines[i]], sourcesConfig[airlineGDS[request.Segments[0].PreferredAirlines[i]]]);
            //            }
            //        }
            //    }
            //    if (request.Sources.Contains("GDS") && gdsPrefList.Count == 0)
            //    {
            //        foreach (string domGds in domesticGDS)
            //        {
            //            if (sourcesConfig.ContainsKey(domGds))
            //            {
            //                sources.Add(domGds, sourcesConfig[domGds]);
            //            }
            //        }
            //    }
            //}
            //else
            {
                if (request.Sources.Contains("GDS") && sourcesConfig.ContainsKey(airlineGDS["INTL"]))
                {
                    sources.Add(airlineGDS["INTL"], Convert.ToInt32(sourcesConfig[airlineGDS["INTL"]].Split(',')[1]));
                }
                if (request.Sources.Contains("ZS") && sourcesConfig.ContainsKey("ZS"))
                {
                    sources.Add("ZS", Convert.ToInt32(sourcesConfig["ZS"].Split(',')[1]));
                }
                if (request.Sources.Contains("1H") && sourcesConfig.ContainsKey("1H"))
                {
                    sources.Add("1H", Convert.ToInt32(sourcesConfig["1H"].Split(',')[1]));
                }
                if (request.Sources.Contains("FZ") && sourcesConfig.ContainsKey("FZ"))
                {
                    sources.Add("FZ", Convert.ToInt32(sourcesConfig["FZ"].Split(',')[1]));
                }
                if (request.Sources.Contains("G9") && sourcesConfig.ContainsKey("G9"))
                {
                    sources.Add("G9", Convert.ToInt32(sourcesConfig["G9"].Split(',')[1]));
                }
                if (request.Sources.Contains("UA") && sourcesConfig.ContainsKey("UA"))
                {
                    sources.Add("UA", Convert.ToInt32(sourcesConfig["UA"].Split(',')[1]));
                }
                if (request.Sources.Contains("SG") && sourcesConfig.ContainsKey("SG"))
                {
                    sources.Add("SG", Convert.ToInt32(sourcesConfig["SG"].Split(',')[1]));
                }
                if (request.Sources.Contains("TA") && sourcesConfig.ContainsKey("TA"))
                {
                    sources.Add("TA", Convert.ToInt32(sourcesConfig["TA"].Split(',')[1]));
                }
            }


            #region Old Code // for reference
            //if (request != null)
            //{
            //    bool wsAllowed = false;
            //    bool amAllowed = false;
            //    bool galiAllowed = false;
            //    if (!isDomestic)
            //    {
            //        if (airlineSources.ContainsKey("1P"))
            //        {
            //            for (int j = 0; j < airlineSources["1P"].Count; j++)
            //            {
            //                if ((airlineSources["1P"][j].Length == 4) && (airlineSources["1P"][j].ToUpper() == "INTL"))
            //                {
            //                    wsAllowed = true;
            //                    break;
            //                }
            //            }
            //        }
            //        if (airlineSources.ContainsKey("1A"))
            //        {
            //            for (int j = 0; j < airlineSources["1A"].Count; j++)
            //            {
            //                if ((airlineSources["1A"][j].Length == 4) && (airlineSources["1A"][j].ToUpper() == "INTL"))
            //                {
            //                    amAllowed = true;
            //                    break;
            //                }
            //            }
            //        }
            //        if (airlineSources.ContainsKey("1G"))
            //        {
            //            for (int j = 0; j < airlineSources["1G"].Count; j++)
            //            {
            //                if ((airlineSources["1G"][j].Length == 4) && (airlineSources["1G"][j].ToUpper() == "INTL"))
            //                {
            //                    galiAllowed = true;
            //                    break;
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        wsAllowed = true;
            //        amAllowed = true;
            //        galiAllowed = true;
            //    }

            //    //// adding World span if its active
            //    if (!sources.ContainsKey("1P"))
            //    {
            //        if (sourcesConfig.ContainsKey("1P") && request.Sources.Contains("1P") && wsAllowed)
            //        {
            //            sources.Add("1P", sourcesConfig["1P"]);
            //        }
            //    }
            //    if (sourcesConfig.ContainsKey("1A") && request.Sources.Contains("1A") && amAllowed)
            //    {
            //        sources.Add("1A", sourcesConfig["1A"]);
            //    }

            //    if (sourcesConfig.ContainsKey("1G") && request.Sources.Contains("1G") && galiAllowed)
            //    {
            //        sources.Add("1G", sourcesConfig["1G"]);
            //    }

            //    bool searchForSG = Technology.BookingEngine.GDS.Navitaire.SearchForLCCType("SG", request.Segments[0].Origin, request.Segments[0].Destination, isDomestic);
            //    if (searchForSG)
            //    {
            //        if (sourcesConfig.ContainsKey("SG") && request.Sources.Contains("SG"))
            //        {
            //            sources.Add("SG", sourcesConfig["SG"]);
            //        }
            //    }

            //    bool searchFor6E = Technology.BookingEngine.GDS.Navitaire.SearchForLCCType("6E", request.Segments[0].Origin, request.Segments[0].Destination, isDomestic);
            //    if (searchFor6E)
            //    {
            //        if (sourcesConfig.ContainsKey("6E") && request.Sources.Contains("6E"))
            //        {
            //            sources.Add("6E", sourcesConfig["6E"]);
            //        }
            //    }
            //    if (sourcesConfig.ContainsKey("DN") && request.Sources.Contains("DN"))
            //    {
            //        sources.Add("DN", sourcesConfig["DN"]);
            //    }

            //    if (sourcesConfig.ContainsKey("I7") && request.Sources.Contains("I7"))
            //    {
            //        sources.Add("I7", sourcesConfig["I7"]);
            //    }
            //    if (sourcesConfig.ContainsKey("ZS") && request.Sources.Contains("ZS"))
            //    {
            //        sources.Add("ZS", sourcesConfig["ZS"]);
            //    }
            //    if (sourcesConfig.ContainsKey("9H") && request.Sources.Contains("9H"))
            //    {
            //        sources.Add("9H", sourcesConfig["9H"]);
            //    }
            //    if (sourcesConfig.ContainsKey("G8") && request.Sources.Contains("G8"))
            //    {
            //        sources.Add("G8", sourcesConfig["G8"]);
            //    }
            //}
            #endregion
            Trace.TraceInformation("MetaSearchEngine.GetReadySources Exit");
            return sources;
        }

        private Dictionary<string, int> GetReadyHotelSources()
        {
            Trace.TraceInformation("MetaSearchEngine.GetReadyHotelSources Entered");

            // getting data from configuration xml file            
            Dictionary<string, string> sourcesConfig = ConfigurationSystem.ActiveSources;
            Dictionary<string, int> sources = new Dictionary<string, int>();
            if (hotelRequest != null)
            {

                if (sourcesConfig.ContainsKey("Desiya") && listOfThreads.ContainsKey("Desiya"))
                {
                    sources.Add("Desiya", Convert.ToInt32(sourcesConfig["Desiya"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("GTA") && listOfThreads.ContainsKey("GTA"))
                {
                    sources.Add("GTA", Convert.ToInt32(sourcesConfig["GTA"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("HotelBeds") && listOfThreads.ContainsKey("HotelBeds"))
                {
                    sources.Add("HotelBeds", Convert.ToInt32(sourcesConfig["HotelBeds"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("Tourico") && listOfThreads.ContainsKey("Tourico"))
                {
                    sources.Add("Tourico", Convert.ToInt32(sourcesConfig["Tourico"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("IAN") && listOfThreads.ContainsKey("IAN"))
                {
                    sources.Add("IAN", Convert.ToInt32(sourcesConfig["IAN"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("HotelConnect") && listOfThreads.ContainsKey("HotelConnect"))
                {
                    sources.Add("HotelConnect", Convert.ToInt32(sourcesConfig["HotelConnect"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("Miki") && listOfThreads.ContainsKey("Miki"))
                {
                    sources.Add("Miki", Convert.ToInt32(sourcesConfig["Miki"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("Travco") && listOfThreads.ContainsKey("Travco"))
                {
                    sources.Add("Travco", Convert.ToInt32(sourcesConfig["Travco"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("DOTW") && listOfThreads.ContainsKey("DOTW"))
                {
                    sources.Add("DOTW", Convert.ToInt32(sourcesConfig["DOTW"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("WST") && listOfThreads.ContainsKey("WST"))
                {
                    sources.Add("WST", Convert.ToInt32(sourcesConfig["WST"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("HIS") && listOfThreads.ContainsKey("HIS"))
                {
                    sources.Add("HIS", Convert.ToInt32(sourcesConfig["HIS"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("RezLive") && listOfThreads.ContainsKey("RezLive"))
                {
                    sources.Add("RezLive", Convert.ToInt32(sourcesConfig["RezLive"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("LOH") && listOfThreads.ContainsKey("LOH"))
                {
                    sources.Add("LOH", Convert.ToInt32(sourcesConfig["LOH"].Split(',')[1]));
                }
                //Added TBOHOtel source 
                if (sourcesConfig.ContainsKey("TBOHotel") && listOfThreads.ContainsKey("TBOHotel"))
                {
                    sources.Add("TBOHotel", Convert.ToInt32(sourcesConfig["TBOHotel"].Split(',')[1]));
                }

            }


            Trace.TraceInformation("MetaSearchEngine.GetReadyHotelSources Exit");
            return sources;
        }

        private Dictionary<string, int> GetReadyTransferSources()
        {
            Trace.TraceInformation("MetaSearchEngine.GetReadyTransferSources Entered");

            // getting data from configuration xml file            
            Dictionary<string, string> sourcesConfig = ConfigurationSystem.ActiveSources;
            Dictionary<string, int> sources = new Dictionary<string, int>();
            if (transferRequest != null)
            {

                if (sourcesConfig.ContainsKey("GTA") && listOfThreads.ContainsKey("GTA"))
                {
                    sources.Add("GTA", Convert.ToInt32(sourcesConfig["GTA"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("HotelBeds") && listOfThreads.ContainsKey("HotelBeds"))
                {
                    sources.Add("HotelBeds", Convert.ToInt32(sourcesConfig["HotelBeds"].Split(',')[1]));
                }
            }


            Trace.TraceInformation("MetaSearchEngine.GetReadyTransferSources Exit");
            return sources;
        }
        //private Dictionary<string, int> GetReadySightseeingSources()
        //{
        //    Trace.TraceInformation("MetaSearchEngine.GetReadySightseeingSources Entered");

        //    // getting data from configuration xml file            
        //    Dictionary<string, int> sourcesConfig = ConfigurationSystem.ActiveSources;
        //    Dictionary<string, int> sources = new Dictionary<string, int>();
        //    if (sightseeingRequest != null)
        //    {

        //        if (sourcesConfig.ContainsKey("GTA") && listOfThreads.ContainsKey("GTA"))
        //        {
        //            sources.Add("GTA", sourcesConfig["GTA"]);
        //        }
        //        if (sourcesConfig.ContainsKey("HotelBeds") && listOfThreads.ContainsKey("HotelBeds"))
        //        {
        //            sources.Add("HotelBeds", sourcesConfig["HotelBeds"]);
        //        }
        //    }


        //    Trace.TraceInformation("MetaSearchEngine.GetReadySightseeingSources Exit");
        //    return sources;
        //}
        //// thread call method - World Span
        //private void SearchResultWs(object eventNumber)
        //{
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultWs Enter");
        //    SearchResultBySources src = new SearchResultBySources();
        //    src.Source = "1P";
        //    try
        //    {
        //        src.Result = (SearchResult[])Technology.BookingEngine.GDS.Worldspan.Search(GetGdsRequest("1P"), sessionId);
        //        // resizing according to search result count
        //        SearchResult[] finalResult = new SearchResult[src.Result.Length];
        //        finalResult = src.Result;
        //        int showResultCount = Configuration.ConfigurationSystem.ResultCountBySources["1P"];
        //        if (src.Result.Length > showResultCount)
        //        {
        //            Array.Resize(ref finalResult, showResultCount);
        //        }
        //        src.Result = finalResult;
        //    }
        //    catch (WebException ex)
        //    {
        //        src.Error = ex.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultWs :WebException for worldSpan = " + ex.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Web Exception returned from WorldSpan. Error Message:" + ex.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (BookingEngineException excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultWs :Exception for worldSpan = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from WorldSpan. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (Exception excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultWs :Exception for worldSpan = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from WorldSpan. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }

        //    SearchResBySrc.Add(src);
        //    eventFlag[(int)eventNumber].Set();

        //    Trace.TraceInformation("MetaSearchEngine.SearchResultWs Exit");
        //}

        //// thread call method - Amadeus
        //private void SearchResultAm(object eventNumber)
        //{
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultWs Enter");
        //    SearchResultBySources src = new SearchResultBySources();
        //    src.Source = "1A";
        //    try
        //    {

        //        src.Result = Amadeus.Search(GetGdsRequest("1A"), sessionId);
        //        // resizing according to search result count
        //        SearchResult[] finalResult = new SearchResult[src.Result.Length];
        //        finalResult = src.Result;
        //        int showResultCount = Configuration.ConfigurationSystem.ResultCountBySources["1A"];
        //        if (src.Result.Length > showResultCount)
        //        {
        //            Array.Resize(ref finalResult, showResultCount);
        //        }
        //        src.Result = finalResult;
        //    }
        //    catch (Exception excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultAm :Exception for Amadeus = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from Amadeus. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }

        //    SearchResBySrc.Add(src);
        //    eventFlag[(int)eventNumber].Set();

        //    Trace.TraceInformation("MetaSearchEngine.SearchResultWs Exit");
        //}
        struct TaxInfo
        {
            internal decimal tax;
            internal decimal fuelSurcharge;
            internal decimal txnFeePercent;
            internal decimal totalTax;
            internal TaxInfo(decimal d1, decimal d2, decimal d3, decimal d4)
            {
                tax = d1;
                fuelSurcharge = d2;
                txnFeePercent = d3;
                totalTax = d4;
            }
        }
        ////thread call method - Hermes
        //private void SearchResultHermes(object eventNumber)
        //{
        //    Trace.TraceInformation("MetaSearchEngine.SearchResult Hermes Enter");
        //    SearchResultBySources src = new SearchResultBySources();
        //    src.Source = "1H";
        //    try
        //    {
        //        HermesApi hAPI = new HermesApi(sessionId);
        //        SearchResult[] result = null;//hAPI.Search(request);
        //        src.Result = result;
        //        // adding taxes in the result
        //        Dictionary<string, TaxInfo> taxInfoList = new Dictionary<string, TaxInfo>();
        //        for (int i = 0; i < src.Result.Length; i++)
        //        {
        //            string airLineCode = src.Result[i].Airline;
        //            if (taxInfoList.Count == 0 || !taxInfoList.ContainsKey(src.Result[i].Airline))
        //            {
        //                decimal tax = Util.GetTax(airLineCode, request.Segments[0].Origin, request.Segments[0].Destination);
        //                if (tax == 0)
        //                {
        //                    //GetFareQuote(src.Result[i]);
        //                    tax = Util.GetTax(airLineCode, request.Segments[0].Origin, request.Segments[0].Destination);
        //                }
        //                decimal fuelSurcharge = Util.GetFuelSurcharge(airLineCode, request.Segments[0].Origin, request.Segments[0].Destination);
        //                decimal txnFeePercent = 0;
        //                if (src.Result[i].Airline == "G8")
        //                {
        //                    txnFeePercent = Convert.ToDecimal(ConfigurationSystem.GoAirConfig["txnFeePercent"]);
        //                }
        //                decimal totalTax = 0;
        //                if (request.Type == SearchType.Return)
        //                {
        //                    tax = 2 * tax;
        //                    fuelSurcharge = fuelSurcharge * 2;
        //                }
        //                int paxCount = request.AdultCount + request.ChildCount + request.SeniorCount;
        //                for (int p = 0; p < paxCount; p++)
        //                {
        //                    totalTax = totalTax + tax;
        //                }
        //                taxInfoList.Add(airLineCode, new TaxInfo(tax, fuelSurcharge, txnFeePercent, totalTax));
        //            }
        //            for (int j = 0; j < src.Result[i].FareBreakdown.Length; j++)
        //            {
        //                src.Result[i].FareBreakdown[j].SellingFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].BaseFare + Convert.ToDouble(taxInfoList[airLineCode].tax));
        //                src.Result[i].FareBreakdown[j].TotalFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].TotalFare + (Convert.ToDouble(taxInfoList[airLineCode].tax) * src.Result[i].FareBreakdown[j].PassengerCount));
        //                src.Result[i].FareBreakdown[j].AdditionalTxnFee = Math.Round((Convert.ToDecimal(src.Result[i].FareBreakdown[j].BaseFare) + (taxInfoList[airLineCode].fuelSurcharge * src.Result[i].FareBreakdown[j].PassengerCount)) * taxInfoList[airLineCode].txnFeePercent / 100, MidpointRounding.AwayFromZero);
        //            }
        //            src.Result[i].TotalFare = src.Result[i].TotalFare + Convert.ToDouble(taxInfoList[airLineCode].totalTax);
        //            src.Result[i].Tax = Convert.ToDouble(taxInfoList[airLineCode].totalTax);
        //        }
        //        //// resizing according to search result count
        //        SearchResult[] finalResult = new SearchResult[src.Result.Length];
        //        finalResult = src.Result;
        //    }
        //    catch (Exception excep)
        //    {
        //        src.Error = excep.Message;
        //        try
        //        {
        //            CT.Core.Audit.Add(CT.Core.EventType.HermesAirSearch, CT.Core.Severity.High, 99999, "Exception returned from Hermes. Error Message:" + excep.Message + "| Stack Trace : " + excep.StackTrace + " | " + DateTime.Now, "");
        //            CT.Core.Audit.Add(CT.Core.EventType.HermesAirSearch, CT.Core.Severity.High, 99998, "Exception returned from Hermes. Error Message:" + excep.Message + "| Stack Trace : " + excep.StackTrace + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //        }
        //        catch
        //        {
        //            CT.Core.Audit.Add(CT.Core.EventType.HermesAirSearch, CT.Core.Severity.High, 99997, "Audit crashed Error Message: " + excep.Message + "| Stack Trace : " + excep.StackTrace + " | " + DateTime.Now, "");
        //        }
        //    }
        //    SearchResBySrc.Add(src);
        //    eventFlag[(int)eventNumber].Set();

        //    Trace.TraceInformation("MetaSearchEngine.SearchResult Hermes Exit");
        //}
        ////thread call method - FlyDubai
        //private void SearchResultFlyDubai(object eventNumber)
        //{
        //    CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Inside search fly dubai", "");
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultFlyDubai  Enter");
        //    SearchResultBySources src = new SearchResultBySources();
        //    src.Source = "1H";
        //    try
        //    {
              
        //        Technology.BookingEngine.GDS.FlyDubaiApi flyDubai = new Technology.BookingEngine.GDS.FlyDubaiApi();
        //        string flyDubaiId = flyDubai.CreateGUID();
        //        SearchResult[] result = flyDubai.GetFareQuotes(request, flyDubaiId, sessionId);
        //        src.Result = result;
        //        // resizing according to search result count
        //        //SearchResult[] finalResult = new SearchResult[src.Result.Length];
        //        //finalResult = src.Result;
        //        SearchResBySrc.Add(src);
        //    }
        //    catch (Exception excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultFlyDubai :Exception for Amadeus = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from FlyDubai. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    eventFlag[(int)eventNumber].Set();
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultFlyDubai  Exit");
        //}
        //// thread call method - UAPI
        private void SearchResultUAPI(object eventNumber)
        {
            Trace.TraceInformation("MetaSearchEngine.SearchResultUAPI Enter");
            SearchResultBySources src = new SearchResultBySources();
            src.Source = "UA";
            try
            {
                SourceDetails agentDetails = new SourceDetails();

                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    agentDetails = SettingsLoginInfo.OnBehalfAgentSourceCredentials["UA"];
                    UAPI.agentBaseCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    UAPI.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                }
                else
                {
                    agentDetails = SettingsLoginInfo.AgentSourceCredentials["UA"];
                    UAPI.agentBaseCurrency = SettingsLoginInfo.Currency;
                    UAPI.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                }
               
                //UAPI uApi = new UAPI();
                UAPI.UserName = agentDetails.UserID;
                UAPI.Password = agentDetails.Password;
                UAPI.TargetBranch = agentDetails.HAP;

                UAPI.AppUserId = SettingsLoginInfo.UserID.ToString();
                UAPI.SessionId = sessionId;
               
                //src.Result = UAPI.Search(GetGdsRequest("1G"), sessionId);
                src.Result = UAPI.Search(GetGdsRequest("UA"), sessionId);

                // resizing according to search result count
                SearchResult[] finalResult = new SearchResult[src.Result.Length];
                finalResult = src.Result;
                int showResultCount = Configuration.ConfigurationSystem.ResultCountBySources["UA"];
                if (src.Result.Length > showResultCount)
                {
                    Array.Resize(ref finalResult, showResultCount);
                }
                Audit.Add(EventType.Search, Severity.Normal, 1, "UAPI filtered results from Active Source : " + finalResult.Length, "");
                src.Result = finalResult;
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultUAPI:Exception for UAPI = " + excep.Message);
                CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from UAPI. Error Message:" + excep.Message + " Stack Trace:" + excep.StackTrace + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
            }

            SearchResBySrc.Add(src);
            eventFlag[(int)eventNumber].Set();

            Trace.TraceInformation("MetaSearchEngine.SearchResultUAPI Exit");
        }
        private void SearchResultAA(object eventNumber)
        {
            Trace.TraceInformation("MetaSearchEngine.SearchResultAA Enter");
            SearchResultBySources src = new SearchResultBySources();
            src.Source = "G9";
            SearchRequest req = request.Copy();
            try
            {
                CT.BookingEngine.GDS.AirArabia airArab = new CT.BookingEngine.GDS.AirArabia();
                SourceDetails agentDetails = new SourceDetails();
                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    agentDetails = SettingsLoginInfo.OnBehalfAgentSourceCredentials["G9"];
                    airArab.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    airArab.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    //airArab.UserName = SettingsLoginInfo.OnBehalfAgentSourceCredentials["G9"].UserID;
                    //airArab.Password = SettingsLoginInfo.OnBehalfAgentSourceCredentials["G9"].Password;
                    //airArab.Code = SettingsLoginInfo.OnBehalfAgentSourceCredentials["G9"].HAP;
                }
                else
                {
                    agentDetails = SettingsLoginInfo.AgentSourceCredentials["G9"];
                    airArab.AgentCurrency = SettingsLoginInfo.Currency;
                    airArab.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    //airArab.UserName = SettingsLoginInfo.AgentSourceCredentials["G9"].UserID;
                    //airArab.Password = SettingsLoginInfo.AgentSourceCredentials["G9"].Password;
                    //airArab.Code = SettingsLoginInfo.AgentSourceCredentials["G9"].HAP;
                }
                airArab.UserName = SettingsLoginInfo.AgentSourceCredentials["G9"].UserID;
                airArab.Password = SettingsLoginInfo.AgentSourceCredentials["G9"].Password;
                airArab.Code = SettingsLoginInfo.AgentSourceCredentials["G9"].HAP;

              airArab.AppUserId = appUserId.ToString();
              airArab.SessionID = sessionId;   
             src.Result = (SearchResult[])airArab.Search(req, sessionId);
             Audit.Add(EventType.Search, Severity.Normal, 1, "AirArabia Result count : " + src.Result.Length, "");
            }
            catch (WebException ex)
            {
                src.Error = ex.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultAA :WebException for AirArabia = " + ex.Message);
                Audit.Add(EventType.Search, Severity.High, 0, "Web Exception returned from AirArabia. Error Message:" + ex.ToString() + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination , "");
            }
            catch (BookingEngineException excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultGA :Exception for AirArabia = " + excep.Message);
                Audit.Add(EventType.Search, Severity.High, 0, "Exception returned from AirArabia . Error Message:" + excep.ToString() + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination , "");
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultGA :Exception for AirArabia = " + excep.Message);
                Audit.Add(EventType.Search, Severity.High, 0, "Exception returned from AirArabia . Error Message:" + excep.ToString() + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination  , "");
            }
            SearchResBySrc.Add(src);
            eventFlag[(int)eventNumber].Set();
            Trace.TraceInformation("MetaSearchEngine.SearchResultGA Exit.");
        }

        private void SearchResultFlyDubai(object eventNumber)
        {
            Audit.Add(EventType.Search, Severity.High, 0, "Inside search fly dubai", "");
            Trace.TraceInformation("MetaSearchEngine.SearchResultFlyDubai  Enter");
            SearchResultBySources src = new SearchResultBySources();
            src.Source = "1H";
            try
            {
                FlyDubaiApi flyDubai = new FlyDubaiApi(request.Segments[0].Origin, sessionId);
                flyDubai.AppUserId = appUserId;
                flyDubai.SessionId = sessionId;
                SourceDetails agentDetails = new SourceDetails();

                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    flyDubai.AgentBaseCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    flyDubai.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    flyDubai.AgentDecimalValue = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                    agentDetails = SettingsLoginInfo.OnBehalfAgentSourceCredentials["FZ"];
                }
                else
                {
                    flyDubai.AgentBaseCurrency = SettingsLoginInfo.Currency;
                    flyDubai.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    flyDubai.AgentDecimalValue = SettingsLoginInfo.DecimalValue;
                    agentDetails = SettingsLoginInfo.AgentSourceCredentials["FZ"];
                }

                flyDubai.LoginName = agentDetails.UserID;
                flyDubai.Password = agentDetails.Password;

                string flyDubaiId = flyDubai.CreateGUID();
                
                //if (flyDubai.GetValidateGUID(flyDubaiId))
                {
                    if (flyDubai.LoginAgency(flyDubaiId))
                    {
                        SearchResult[] result = flyDubai.GetFareQuotes(request, flyDubaiId, sessionId);
                        src.Result = result;
                        // resizing according to search result count
                        //SearchResult[] finalResult = new SearchResult[src.Result.Length];
                        //finalResult = src.Result;
                        Audit.Add(EventType.Search, Severity.Normal, 1, "FlyDubai Result count : " + src.Result.Length, "");
                        SearchResBySrc.Add(src);
                    }
                }
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultFlyDubai :Exception for Amadeus = " + excep.Message);
                Audit.Add(EventType.Search, Severity.High, 0, "Exception returned from FlyDubai. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
            }
            eventFlag[(int)eventNumber].Set();
            Trace.TraceInformation("MetaSearchEngine.SearchResultFlyDubai  Exit");
        }

        private void SearchResultTBOAir(object eventNumber)
        {
            Audit.Add(EventType.Search, Severity.High, 0, "Inside search tbo air", "");
            Trace.TraceInformation("MetaSearchEngine.SearchResultTBOAir  Enter");
            SearchResultBySources src = new SearchResultBySources();
            src.Source = "TA";//TEMP Source, TODO: Change Later
            try
            {
                AirV10 tbo = new AirV10();

                SourceDetails agentDetails = new SourceDetails();

                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    tbo.AgentBaseCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    tbo.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    tbo.AgentDecimalValue = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                    //agentDetails = SettingsLoginInfo.OnBehalfAgentSourceCredentials["TA"];
                }
                else
                {
                    tbo.AgentBaseCurrency = SettingsLoginInfo.Currency;
                    tbo.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    tbo.AgentDecimalValue = SettingsLoginInfo.DecimalValue;
                    //agentDetails = SettingsLoginInfo.AgentSourceCredentials["TA"];
                }

                //tbo.LoginName = agentDetails.UserID;
                //tbo.Password = agentDetails.Password;


                SearchResult[] result = tbo.Search(request);
                src.Result = result;
                // resizing according to search result count
                SearchResult[] finalResult = new SearchResult[src.Result.Length];
                finalResult = src.Result;
                if (finalResult.Length > 0)
                {
                    Array.Sort<SearchResult>(finalResult, finalResult[0]);
                    int showResultCount = Configuration.ConfigurationSystem.ResultCountBySources["TA"];
                    if (src.Result.Length > showResultCount)
                    {
                        Array.Resize(ref finalResult, showResultCount);
                    }
                }
                src.Result = finalResult;
                SearchResBySrc.Add(src);
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultTBOAir :Exception for TBOAir = " + excep.Message);
                Audit.Add(EventType.Search, Severity.High, 0, "Exception returned from TBOAir. Error Message:" + excep.ToString() + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
            }
            eventFlag[(int)eventNumber].Set();
            Trace.TraceInformation("MetaSearchEngine.SearchResultTBOAir  Exit");
        }
        //// thread call method - Galileo
        //private void SearchResultGalileoORG(object eventNumber)
        //{
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultGalileo Enter");
        //    SearchResultBySources src = new SearchResultBySources();
        //    src.Source = "1G";
        //    try
        //    {
        //        src.Result = GalileoApi.Search(GetGdsRequest("1G"), sessionId);
        //        // resizing according to search result count
        //        SearchResult[] finalResult = new SearchResult[src.Result.Length];
        //        finalResult = src.Result;
        //        int showResultCount = Configuration.ConfigurationSystem.ResultCountBySources["1G"];
        //        if (src.Result.Length > showResultCount)
        //        {
        //            Array.Resize(ref finalResult, showResultCount);
        //        }
        //        src.Result = finalResult;
        //    }
        //    catch (Exception excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultGalileo :Exception for Galileo = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from Galileo. Error Message:" + excep.Message + " Stack Trace:" + excep.StackTrace + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }

        //    SearchResBySrc.Add(src);
        //    eventFlag[(int)eventNumber].Set();

        //    Trace.TraceInformation("MetaSearchEngine.SearchResultGalileo Exit");
        //}

        //// thread call method - Spice Jet - will start working when SG booking from navitaire
        //private void SearchResultSjForNavi(object eventNumber)
        //{
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultSj Enter");
        //    Technology.BookingEngine.GDS.Navitaire sj = new Technology.BookingEngine.GDS.Navitaire("SG");
        //    SearchResultBySources src = new SearchResultBySources();
        //    src.Source = "SG";
        //    try
        //    {
        //        src.Result = (SearchResult[])sj.Search(request, sessionId);

        //        // adding taxes in the result
        //        decimal tax = Util.GetTax("SG", request.Segments[0].Origin, request.Segments[0].Destination);
        //        decimal totalTax = 0;
        //        if (request.Type == SearchType.Return)
        //        {
        //            tax = 2 * tax;
        //        }
        //        int paxCount = request.AdultCount + request.ChildCount + request.SeniorCount;
        //        for (int i = 0; i < paxCount; i++)
        //        {
        //            totalTax = totalTax + tax;
        //        }

        //        for (int i = 0; i < src.Result.Length; i++)
        //        {
        //            for (int j = 0; j < src.Result[i].FareBreakdown.Length; j++)
        //            {
        //                if (src.Result[i].FareBreakdown[j].PassengerType != PassengerType.Infant)
        //                {
        //                    src.Result[i].FareBreakdown[j].SellingFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].BaseFare + Convert.ToDouble(tax));
        //                    src.Result[i].FareBreakdown[j].TotalFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].TotalFare + (Convert.ToDouble(tax) * src.Result[i].FareBreakdown[j].PassengerCount));
        //                }
        //            }
        //            src.Result[i].TotalFare = src.Result[i].TotalFare + Convert.ToDouble(totalTax);
        //            src.Result[i].Tax = Convert.ToDouble(totalTax);
        //        }
        //    }
        //    catch (WebException ex)
        //    {
        //        src.Error = ex.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultSj :WebException for SpiceJet = " + ex.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Web Exception returned from Navitaire for SpiceJet. Error Message:" + ex.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (BookingEngineException excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultSj :Exception for SpiceJet = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from Navitaire for SpiceJet. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (Exception excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultSj :Exception for SpiceJet = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from Navitaire for SpiceJet. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    SearchResBySrc.Add(src);
        //    eventFlag[(int)eventNumber].Set();
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultSj Enter");
        //}

        //// thread call method - Spice Jet
        private void SearchResultSj(object eventNumber)
        {
            Trace.TraceInformation("MetaSearchEngine.SearchResultSj Enter");
            SearchResultBySources src = new SearchResultBySources();
            try
            {
                //if (ConfigurationSystem.SpiceJetConfig["Mode"] == "Navitaire")
                //{
                //    Navitaire sjn = new Navitaire("0S");    //  Zero or Sierra
                //    src.Result = (SearchResult[])sjn.Search(request, sessionId);
                //}
                //else
                {
                    SpiceJetAPIV1 sjObj = new SpiceJetAPIV1();

                    if (SettingsLoginInfo.IsOnBehalfOfAgent)
                    {
                        sjObj.LoginName = SettingsLoginInfo.OnBehalfAgentSourceCredentials["SG"].UserID;
                        sjObj.Password = SettingsLoginInfo.OnBehalfAgentSourceCredentials["SG"].Password;
                        sjObj.AgentDomain = SettingsLoginInfo.OnBehalfAgentSourceCredentials["SG"].HAP;
                        sjObj.AgentBaseCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                        sjObj.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                        sjObj.AgentDecimalValue = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                    }
                    else
                    {
                        sjObj.LoginName = SettingsLoginInfo.AgentSourceCredentials["SG"].UserID;
                        sjObj.Password = SettingsLoginInfo.AgentSourceCredentials["SG"].Password;
                        sjObj.AgentDomain = SettingsLoginInfo.AgentSourceCredentials["SG"].HAP;
                        sjObj.AgentBaseCurrency = SettingsLoginInfo.Currency;
                        sjObj.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                        sjObj.AgentDecimalValue = SettingsLoginInfo.DecimalValue;
                    }

                    src.Result = sjObj.Search(request);
                }

                // adding taxes in the result
                //decimal tax = Util.GetTax("SG", request.Segments[0].Origin, request.Segments[0].Destination);
                //decimal fuelSurcharge = Util.GetFuelSurcharge("SG", request.Segments[0].Origin, request.Segments[0].Destination);
                //decimal txnFeePercent = Convert.ToDecimal(ConfigurationSystem.SpiceJetConfig["txnFeePercent"]);
                //decimal totalTax = 0;
                //if (request.Type == SearchType.Return)
                //{
                //    tax = 2 * tax;
                //    fuelSurcharge = 2 * fuelSurcharge;
                //}
                //int paxCount = request.AdultCount + request.ChildCount + request.SeniorCount;
                //for (int i = 0; i < paxCount; i++)
                //{
                //    totalTax = totalTax + tax;
                //}

                //for (int i = 0; i < src.Result.Length; i++)
                //{
                //    for (int j = 0; j < src.Result[i].FareBreakdown.Length; j++)
                //    {
                //        if (src.Result[i].FareBreakdown[j].PassengerType != PassengerType.Infant)
                //        {
                //            //src.Result[i].FareBreakdown[j].SellingFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].BaseFare + Convert.ToDouble(tax));
                //            src.Result[i].FareBreakdown[j].TotalFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].TotalFare + (Convert.ToDouble(tax) * src.Result[i].FareBreakdown[j].PassengerCount));
                //            src.Result[i].FareBreakdown[j].AdditionalTxnFee = Math.Round((Convert.ToDecimal(src.Result[i].FareBreakdown[j].BaseFare) + (fuelSurcharge * src.Result[i].FareBreakdown[j].PassengerCount)) * txnFeePercent / 100, MidpointRounding.AwayFromZero);
                //        }
                //    }
                //    src.Result[i].TotalFare = src.Result[i].TotalFare + Convert.ToDouble(totalTax);
                //    src.Result[i].Tax = Convert.ToDouble(totalTax);
                //}

                // resizing according to search result count
                SearchResult[] finalResult = new SearchResult[src.Result.Length];
                finalResult = src.Result;
                int showResultCount = Configuration.ConfigurationSystem.ResultCountBySources["SG"];
                if (src.Result.Length > showResultCount)
                {
                    Array.Resize(ref finalResult, showResultCount);
                }
                src.Result = finalResult;
            }
            catch (WebException ex)
            {
                src.Error = ex.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultSj :WebException for SpiceJet = " + ex.Message);
                CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Web Exception returned from SpiceJet. Error Message:" + ex.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
            }
            catch (BookingEngineException excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultSj :Exception for SpiceJet = " + excep.Message);
                CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from SpiceJet. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultSj :Exception for SpiceJet = " + excep.Message);
                CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from SpiceJet. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
            }
            SearchResBySrc.Add(src);
            eventFlag[(int)eventNumber].Set();
            Trace.TraceInformation("MetaSearchEngine.SearchResultSj Enter");
        }



        //// thread call method - Indigo
        //private void SearchResultInd(object eventNumber)
        //{
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultInd Enter");
        //    Technology.BookingEngine.GDS.Navitaire navi = new Technology.BookingEngine.GDS.Navitaire("6E");
        //    SearchResultBySources src = new SearchResultBySources();
        //    src.Source = "6E";

        //    try
        //    {
        //        src.Result = (SearchResult[])navi.Search(request, sessionId);

        //        // adding taxes in the result
        //        decimal dbTax = Util.GetTax("6E", request.Segments[0].Origin, request.Segments[0].Destination);
        //        decimal dbFuelSurcharge = Util.GetFuelSurcharge("6E", request.Segments[0].Origin, request.Segments[0].Destination);

        //        decimal txnFeePercent = Convert.ToDecimal(ConfigurationSystem.IndigoConfig["txnFeePercent"]);
        //        decimal totalTax = 0;
        //        for (int i = 0; i < src.Result.Length; i++)
        //        {
        //            totalTax = 0;
        //            decimal tax = 0;
        //            decimal fuelSurcharge = 0;
        //            if (src.Result[i].Flights[0].Length == 1 && src.Result[i].Flights[0][0].BookingClass == "V")
        //            {
        //                src.Result[i].FareBreakdown = GetFareQuote(src.Result[i]);
        //                for (int j = 0; j < src.Result[i].FareBreakdown.Length; j++)
        //                {
        //                    totalTax += src.Result[i].FareBreakdown[j].Tax;
        //                }
        //                src.Result[i].TotalFare = src.Result[i].TotalFare + Convert.ToDouble(totalTax);
        //                src.Result[i].Tax = Convert.ToDouble(totalTax);
        //            }
        //            else
        //            {
        //                for (int j = 0; j < src.Result[i].FareBreakdown.Length; j++)
        //                {
        //                    tax = dbTax;
        //                    fuelSurcharge = dbFuelSurcharge;
        //                    totalTax += tax * src.Result[i].FareBreakdown[j].PassengerCount;
        //                    if (src.Result[i].FareBreakdown[j].PassengerType != PassengerType.Infant)
        //                    {
        //                        //src.Result[i].FareBreakdown[j].SellingFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].BaseFare + Convert.ToDouble(tax));
        //                        src.Result[i].FareBreakdown[j].TotalFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].TotalFare + (Convert.ToDouble(tax) * src.Result[i].FareBreakdown[j].PassengerCount));
        //                        src.Result[i].FareBreakdown[j].AdditionalTxnFee = Math.Round((Convert.ToDecimal(src.Result[i].FareBreakdown[j].BaseFare) + (fuelSurcharge * src.Result[i].FareBreakdown[j].PassengerCount)) * txnFeePercent / 100, MidpointRounding.AwayFromZero);
        //                    }
        //                }
        //                src.Result[i].TotalFare = src.Result[i].TotalFare + Convert.ToDouble(totalTax);
        //                src.Result[i].Tax = Convert.ToDouble(totalTax);
        //            }
        //        }
        //    }
        //    catch (WebException ex)
        //    {
        //        src.Error = ex.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultInd :WebException for Indigo = " + ex.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Web Exception returned from Navitaire for Indigo. Error Message:" + ex.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (BookingEngineException excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultInd :Exception for Indigo = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from Navitaire for Indigo. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (Exception excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultInd :Exception for Indigo = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from Navitaire for Indigo. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    SearchResBySrc.Add(src);
        //    eventFlag[(int)eventNumber].Set();
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultInd Exit.");
        //}

        //// thread call method - Air Deccan
        //private void SearchResultDN(object eventNumber)
        //{
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultDN Enter");
        //    Technology.BookingEngine.GDS.AirDeccanApi airDeccan = new Technology.BookingEngine.GDS.AirDeccanApi();
        //    //TODO:
        //    SearchResultBySources src = new SearchResultBySources();
        //    src.Source = "DN";
        //    try
        //    {
                
        //        Technology.BookingEngine.GDS.AirDeccanApi airD = new Technology.BookingEngine.GDS.AirDeccanApi();
        //        string nGuid = airD.CreateGUID();
        //        //string pass = airD.GetValidateGUID(nGuid);
        //        bool loginTA = airD.LoginAgency(nGuid);
        //        if (loginTA)
        //        {
        //            src.Result = (SearchResult[])airD.GetFlightAvailability(request, nGuid, sessionId);


        //            // adding taxes in the result
        //            decimal tax = Util.GetTax("DN", request.Segments[0].Origin, request.Segments[0].Destination);
        //            decimal totalTax = 0;
        //            if (request.Type == SearchType.Return)
        //            {
        //                tax = 2 * tax;
        //            }
        //            int paxCount = request.AdultCount + request.ChildCount + request.SeniorCount;
        //            for (int i = 0; i < paxCount; i++)
        //            {
        //                totalTax = totalTax + tax;
        //            }

        //            for (int i = 0; i < src.Result.Length; i++)
        //            {
        //                for (int j = 0; j < src.Result[i].FareBreakdown.Length; j++)
        //                {
        //                    //TODO: 
        //                    //if (src.Result[i].FareBreakdown[j].PassengerType != PassengerType.Infant)
        //                    //{
        //                    src.Result[i].FareBreakdown[j].SellingFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].BaseFare + Convert.ToDouble(tax));
        //                    src.Result[i].FareBreakdown[j].TotalFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].TotalFare + (Convert.ToDouble(tax) * src.Result[i].FareBreakdown[j].PassengerCount));
        //                    // }
        //                }
        //                src.Result[i].TotalFare = src.Result[i].TotalFare + Convert.ToDouble(totalTax);
        //                src.Result[i].Tax = Convert.ToDouble(totalTax);
        //            }

        //            // resizing according to search result count
        //            SearchResult[] finalResult = new SearchResult[src.Result.Length];
        //            finalResult = src.Result;
        //            int showResultCount = Configuration.ConfigurationSystem.ResultCountBySources["DN"];
        //            if (src.Result.Length > showResultCount)
        //            {
        //                Array.Resize(ref finalResult, showResultCount);
        //            }
        //            src.Result = finalResult;
        //        }
        //        else
        //        {
        //            src.Result = new SearchResult[0];
        //        }
        //    }
        //    catch (WebException ex)
        //    {
        //        src.Error = ex.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultDN :WebException for AirDeccan = " + ex.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Web Exception returned from AirDeccano. Error Message:" + ex.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (BookingEngineException excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultDN :Exception for AirDeccan = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from AirDeccan . Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (Exception excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultDN :Exception for AirDeccan = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from AirDeccan . Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    SearchResBySrc.Add(src);
        //    eventFlag[(int)eventNumber].Set();
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultDN Exit.");
        //}

        //// thread call method - Paramount
        //private void SearchResultPm(object eventNumber)
        //{
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultPm Enter");
        //    SearchResultBySources src = new SearchResultBySources();
        //    src.Source = "I7";
        //    try
        //    {
        //        ParamountApi paramount = new ParamountApi();
        //        src.Result = (SearchResult[])paramount.Search(request, sessionId);
        //        //Removed to make search as function, not static function.
        //        //src.Result = (SearchResult[])ParamountApi.Search(request, sessionId);

        //        //decimal tax = Util.GetTax("I7", request.Segments[0].Origin, request.Segments[0].Destination);
        //        //decimal totalTax = 0;
        //        //if (request.Type == SearchType.Return)
        //        //{
        //        //    tax = 2 * tax;
        //        //}
        //        //int paxCount = request.AdultCount + request.ChildCount + request.SeniorCount;
        //        //for (int i = 0; i < paxCount; i++)
        //        //{
        //        //    totalTax = totalTax + tax;
        //        //}

        //        //for (int i = 0; i < src.Result.Length; i++)
        //        //{
        //        //    for (int j = 0; j < src.Result[i].FareBreakdown.Length; j++)
        //        //    {
        //        //        if (src.Result[i].FareBreakdown[j].PassengerType != PassengerType.Infant)
        //        //        {
        //        //            src.Result[i].FareBreakdown[j].SellingFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].BaseFare + Convert.ToDouble(tax));
        //        //            src.Result[i].FareBreakdown[j].TotalFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].TotalFare + (Convert.ToDouble(tax) * src.Result[i].FareBreakdown[j].PassengerCount));
        //        //        }
        //        //    }
        //        //    src.Result[i].TotalFare = src.Result[i].TotalFare + Convert.ToDouble(totalTax);
        //        //    src.Result[i].Tax = Convert.ToDouble(totalTax);
        //        //}
        //    }
        //    catch (WebException ex)
        //    {
        //        src.Error = ex.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultPm :WebException for Paramount = " + ex.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Web Exception returned from Paramount. Error Message:" + ex.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (BookingEngineException excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultPm :Exception for Paramount= " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from Paramount. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (Exception excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultPm :Exception for Paramount= " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from Paramount. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    SearchResBySrc.Add(src);
        //    eventFlag[(int)eventNumber].Set();
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultPm Exit");
        //}


        //private void SearchResultSa(object eventNumber)
        //{
        //    //throw new NotImplementedException("This method is not yet implemented");
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultSa Enter");
        //    SearchResultBySources src = new SearchResultBySources();
        //    src.Source = "ZS";
        //    try
        //    {
        //        src.Result = (SearchResult[])Sama.Search(request, sessionId);
        //    }
        //    catch (WebException ex)
        //    {
        //        src.Error = ex.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultSa :WebException for Sama= " + ex.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Web Exception returned from Sama. Error Message:" + ex.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (BookingEngineException excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultSa :Exception for Paramount= " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from Sama. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (Exception excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultSa :Exception for Sama= " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from Sama. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    SearchResBySrc.Add(src);
        //    eventFlag[(int)eventNumber].Set();
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultSa Exit");
        //}

        //private void SearchResultMdlr(object eventNumber)
        //{
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultPm Enter");
        //    SearchResultBySources src = new SearchResultBySources();
        //    src.Source = "9H";
        //    try
        //    {
        //        src.Result = (SearchResult[])Mdlr.Search(request, sessionId);

        //        for (int i = 0; i < src.Result.Length; i++)
        //        {
        //            for (int j = 0; j < src.Result[i].FareBreakdown.Length; j++)
        //            {
        //                src.Result[i].FareBreakdown[j].AdditionalTxnFee = Convert.ToInt32(ConfigurationSystem.MdlrConfig["serviceFee"]) * src.Result[i].FareBreakdown[j].PassengerCount;
        //            }
        //        }
        //    }
        //    catch (WebException ex)
        //    {
        //        src.Error = ex.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultMdlr :WebException for MDLR = " + ex.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Web Exception returned from MDLR. Error Message:" + ex.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (BookingEngineException excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultMdlr :Exception for Paramount= " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from MDLR. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (Exception excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultPm :Exception for MDLR= " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from MDLR. Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    SearchResBySrc.Add(src);
        //    eventFlag[(int)eventNumber].Set();
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultMdlr Exit");
        //}
        //thread call method-Air Arabia
        //private void SearchResultAA(object eventNumber)
        //{
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultAA Enter");
        //    SearchResultBySources src = new SearchResultBySources();
        //    src.Source = "G9";
        //    try
        //    {
        //        Technology.BookingEngine.GDS.AirArabia airArab = new Technology.BookingEngine.GDS.AirArabia();
        //        src.Result = (SearchResult[])airArab.Search(request, sessionId);

        //    }
        //    catch (WebException ex)
        //    {
        //        src.Error = ex.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultAA :WebException for AirArabia = " + ex.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Web Exception returned from AirArabia. Error Message:" + ex.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (BookingEngineException excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultGA :Exception for AirArabia = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from AirArabia . Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (Exception excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultGA :Exception for AirArabia = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from AirArabia . Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    SearchResBySrc.Add(src);
        //    eventFlag[(int)eventNumber].Set();
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultGA Exit.");
        //}
        // thread call method - Go Air
        //private void SearchResultGA(object eventNumber)
        //{
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultGA Enter");
        //    SearchResultBySources src = new SearchResultBySources();
        //    src.Source = "G8";
        //    try
        //    {
        //        Technology.BookingEngine.GDS.GoAirApi airD = new Technology.BookingEngine.GDS.GoAirApi();
        //        string nGuid = airD.CreateGUID();
        //        //string pass = airD.GetValidateGUID(nGuid);
        //        bool loginTA = airD.LoginAgency(nGuid);
        //        if (loginTA)
        //        {
        //            src.Result = (SearchResult[])airD.GetFlightAvailability(request, nGuid, sessionId);
        //            bool isExcepSecInfant = false;
        //            if (ConfigurationSystem.GoAirConfig["ExceptionSectorForInfant"].Contains(request.Segments[0].Origin))
        //            {
        //                isExcepSecInfant = true;
        //            }

        //            // adding taxes in the result
        //            decimal dbTax = Util.GetTax("G8", request.Segments[0].Origin, request.Segments[0].Destination);
        //            decimal dbFuelSurcharge = Util.GetFuelSurcharge("G8", request.Segments[0].Origin, request.Segments[0].Destination);
        //            decimal txnFeePercent = Convert.ToDecimal(ConfigurationSystem.GoAirConfig["txnFeePercent"]);
        //            decimal totalTax = 0;
        //            for (int i = 0; i < src.Result.Length; i++)
        //            {
        //                totalTax = 0;
        //                decimal tax = 0;
        //                decimal fuelSurcharge = 0;
        //                if (request.Segments[0].flightCabinClass == CabinClass.Business)
        //                {
        //                    src.Result[i].FareBreakdown = GetFareQuote(src.Result[i]);
        //                    for (int j = 0; j < src.Result[i].FareBreakdown.Length; j++)
        //                    {
        //                        totalTax += src.Result[i].FareBreakdown[j].Tax;
        //                    }
        //                    src.Result[i].TotalFare = src.Result[i].TotalFare + Convert.ToDouble(totalTax);
        //                    src.Result[i].Tax = Convert.ToDouble(totalTax);
        //                }
        //                else
        //                {
        //                    for (int j = 0; j < src.Result[i].FareBreakdown.Length; j++)
        //                    {
        //                        if (src.Result[i].FareBreakdown[j].PassengerType == PassengerType.Infant)
        //                        {
        //                            if (isExcepSecInfant)
        //                            {
        //                                tax = Convert.ToDecimal(ConfigurationSystem.GoAirConfig["ExSectorInfTax"]);
        //                            }
        //                            else
        //                            {
        //                                tax = Convert.ToDecimal(ConfigurationSystem.GoAirConfig["InfantTax"]);
        //                            }
        //                            fuelSurcharge = 0;
        //                        }
        //                        else
        //                        {
        //                            tax = dbTax;
        //                            fuelSurcharge = dbFuelSurcharge;
        //                        }
        //                        totalTax += tax * src.Result[i].FareBreakdown[j].PassengerCount;

        //                        src.Result[i].FareBreakdown[j].SellingFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].BaseFare + Convert.ToDouble(tax));
        //                        src.Result[i].FareBreakdown[j].TotalFare = Convert.ToDouble(src.Result[i].FareBreakdown[j].TotalFare + (Convert.ToDouble(tax) * src.Result[i].FareBreakdown[j].PassengerCount));
        //                        if (src.Result[i].FareBreakdown[j].PassengerType != PassengerType.Infant)
        //                        {
        //                            src.Result[i].FareBreakdown[j].AdditionalTxnFee = Math.Round((Convert.ToDecimal(src.Result[i].FareBreakdown[j].BaseFare) + (fuelSurcharge * src.Result[i].FareBreakdown[j].PassengerCount)) * txnFeePercent / 100, MidpointRounding.AwayFromZero);
        //                        }
        //                    }
        //                    src.Result[i].TotalFare = src.Result[i].TotalFare + Convert.ToDouble(totalTax);
        //                    src.Result[i].Tax = Convert.ToDouble(totalTax);
        //                }
        //            }

        //            // resizing according to search result count
        //            SearchResult[] finalResult = new SearchResult[src.Result.Length];
        //            finalResult = src.Result;
        //            int showResultCount = Configuration.ConfigurationSystem.ResultCountBySources["G8"];
        //            if (src.Result.Length > showResultCount)
        //            {
        //                Array.Resize(ref finalResult, showResultCount);
        //            }
        //            src.Result = finalResult;
        //        }
        //        else
        //        {
        //            src.Result = new SearchResult[0];
        //        }
        //    }
        //    catch (WebException ex)
        //    {
        //        src.Error = ex.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultGA :WebException for GoAir = " + ex.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Web Exception returned from GoAir. Error Message:" + ex.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (BookingEngineException excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultGA :Exception for GoAir = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from GoAir . Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    catch (Exception excep)
        //    {
        //        src.Error = excep.Message;
        //        Trace.TraceInformation("MetaSearchEngine.SearchResultGA :Exception for GoAir = " + excep.Message);
        //        CT.Core.Audit.Add(CT.Core.EventType.Search, CT.Core.Severity.High, 0, "Exception returned from GoAir . Error Message:" + excep.Message + " | " + DateTime.Now + "| origin =" + request.Segments[0].Origin + " | Destination =" + request.Segments[0].Destination + "Pref Carrier =" + request.Segments[0].PreferredAirlines, "");
        //    }
        //    SearchResBySrc.Add(src);
        //    eventFlag[(int)eventNumber].Set();
        //    Trace.TraceInformation("MetaSearchEngine.SearchResultGA Exit.");
        //}
        /// <summary>
        /// combine all GDS and LCC sources
        /// </summary>
        /// <returns></returns>
        private SearchResult[] CombineSources(SearchRequest request)
        {
            Trace.TraceInformation("MetaSearchEngine.CombineSources Enter");
            int bookingTimeWindow = Convert.ToInt16(ConfigurationSystem.BookingEngineConfig["bookingTimeWindow"]);
            List<SearchResult> finalResult = new List<SearchResult>();
            for (int i = 0; i < SearchResBySrc.Count; i++)
            {
                if (SearchResBySrc[i].Result != null)
                {
                    for (int j = 0; j < SearchResBySrc[i].Result.Length; j++)
                    {
                        int timeCheck = SearchResBySrc[i].Result[j].Flights[0][0].DepartureTime.CompareTo(DateTime.Now.AddMinutes(bookingTimeWindow));
                        if (timeCheck > 0)
                        {
                            if (request.Segments[0].PreferredDepartureTime.ToString("HHmm") == "0000")
                            {
                                finalResult.Add(SearchResBySrc[i].Result[j]);
                            }
                            else
                            {
                                DateTime flightTime = SearchResBySrc[i].Result[j].Flights[0][0].DepartureTime;
                                DateTime requestedTime = request.Segments[0].PreferredDepartureTime;
                                if (requestedTime.ToString("HHmm") == "0800")
                                {
                                    if ((flightTime >= requestedTime.AddHours(-4)) && (flightTime <= requestedTime.AddHours(3)))
                                    {
                                        finalResult.Add(SearchResBySrc[i].Result[j]);
                                    }
                                }
                                else if (requestedTime.ToString("HHmm") == "1400")
                                {
                                    if ((flightTime >= requestedTime.AddHours(-3)) && (flightTime <= requestedTime.AddHours(2)))
                                    {
                                        finalResult.Add(SearchResBySrc[i].Result[j]);
                                    }
                                }
                                else if (requestedTime.ToString("HHmm") == "1900")
                                {
                                    if ((flightTime >= requestedTime.AddHours(-3)) && (flightTime <= requestedTime.AddHours(2)))
                                    {
                                        finalResult.Add(SearchResBySrc[i].Result[j]);
                                    }
                                }
                                else if (requestedTime.ToString("HHmm") == "0100")
                                {
                                    if (((flightTime >= requestedTime.AddHours(-1)) && (flightTime <= requestedTime.AddHours(3))) || ((flightTime >= requestedTime.AddHours(20)) && (flightTime <= requestedTime.AddHours(23))))
                                    {
                                        finalResult.Add(SearchResBySrc[i].Result[j]);
                                    }
                                }
                            }
                        }
                        if (request.Type == SearchType.Return)
                        {
                            if (!(request.Segments[1].PreferredDepartureTime.ToString("HHmm") == "0000"))
                            {
                                DateTime flightTime = SearchResBySrc[i].Result[j].Flights[1][0].DepartureTime;
                                DateTime requestedTime = request.Segments[1].PreferredDepartureTime;
                                if (requestedTime.ToString("HHmm") == "0800")
                                {
                                    if (!((flightTime >= requestedTime.AddHours(-4)) && (flightTime <= requestedTime.AddHours(3))))
                                    {
                                        Audit.Add(EventType.Search, Severity.Normal, 1, "Result removed for Airline : " + SearchResBySrc[i].Result[j].Flights[0][0].Airline + " Dep Time : " + flightTime + " Preferred Time : " + requestedTime, "");
                                        finalResult.Remove(SearchResBySrc[i].Result[j]);
                                    }
                                }
                                else if (requestedTime.ToString("HHmm") == "1400")
                                {
                                    if (!((flightTime >= requestedTime.AddHours(-3)) && (flightTime <= requestedTime.AddHours(2))))
                                    {
                                        Audit.Add(EventType.Search, Severity.Normal, 1, "Result removed for Airline : " + SearchResBySrc[i].Result[j].Flights[0][0].Airline + " Dep Time : " + flightTime + " Preferred Time : " + requestedTime, "");
                                        finalResult.Remove(SearchResBySrc[i].Result[j]);
                                    }
                                }
                                else if (requestedTime.ToString("HHmm") == "1900")
                                {
                                    if (!((flightTime >= requestedTime.AddHours(-3)) && (flightTime <= requestedTime.AddHours(2))))
                                    {
                                        Audit.Add(EventType.Search, Severity.Normal, 1, "Result removed for Airline : " + SearchResBySrc[i].Result[j].Flights[0][0].Airline + " Dep Time : " + flightTime + " Preferred Time : " + requestedTime, "");
                                        finalResult.Remove(SearchResBySrc[i].Result[j]);
                                    }
                                }
                                else if (requestedTime.ToString("HHmm") == "0100")
                                {
                                    if (!(((flightTime >= requestedTime.AddHours(-1)) && (flightTime <= requestedTime.AddHours(3))) || ((flightTime >= requestedTime.AddHours(20)) && (flightTime <= requestedTime.AddHours(23)))))
                                    {
                                        Audit.Add(EventType.Search, Severity.Normal, 1, "Result removed for Airline : " + SearchResBySrc[i].Result[j].Flights[0][0].Airline + " Dep Time : " + flightTime + " Preferred Time : " + requestedTime, "");
                                        finalResult.Remove(SearchResBySrc[i].Result[j]);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Audit.Add(EventType.Search, Severity.Normal, 1, "filtered result for BookingTimeWindow (" + bookingTimeWindow + " mins) : " + finalResult.Count, "");


            Dictionary<string, List<short>> flightResultCount = new Dictionary<string, List<short>>();
            // sorting data by Price
            //for (int n = 0; n < finalResult.Count - 1; n++)
            //{
            //    for (int m = 0; m < (finalResult.Count - n - 1); m++)
            //    {

            //        if (finalResult[m].TotalFare > finalResult[m + 1].TotalFare)
            //        {
            //            SearchResult temp = finalResult[m];
            //            finalResult[m] = finalResult[m + 1];
            //            finalResult[m + 1] = temp;
            //        }
            //    }
            //}
            finalResult.Sort(delegate(SearchResult sr1, SearchResult sr2) { return sr1.TotalFare.CompareTo(sr2.TotalFare); });
            for (int n = 0; n < finalResult.Count; n++)
            {
                string airline = finalResult[n].Flights[0][0].Airline;
                for (int i = 0; i < finalResult[n].Flights.Length; i++)
                {
                    for (int j = 0; j < finalResult[n].Flights[i].Length; j++)
                    {

                        if (airline != finalResult[n].Flights[i][j].Airline)
                        {
                            airline = "YY";
                            break;
                        }
                    }
                }
                finalResult[n].Airline = airline;
                if (flightResultCount.ContainsKey(airline))
                {
                    flightResultCount[airline][0]++;
                }
                else
                {
                    List<short> countList = new List<short>();
                    countList.Add(1);
                    flightResultCount.Add(airline, countList);
                }
            }

            // selecting 'count' of how many result show on page from configuration xml
            int showResultCount = Convert.ToInt16(ConfigurationSystem.BookingEngineConfig["displayResultCount"]);
            short configCount = Convert.ToInt16(ConfigurationSystem.BookingEngineConfig["minResultsForAirline"]);
            float eachFlightCount = (float)finalResult.Count / (float)showResultCount;
            if (eachFlightCount < 1)
            {
                eachFlightCount = 1;
            }
            foreach (KeyValuePair<string, List<short>> flightcount in flightResultCount)
            {
                List<short> resultcount = flightcount.Value;
                short ratio = Convert.ToInt16(Math.Round(resultcount[0] / eachFlightCount, MidpointRounding.ToEven));
                if (ratio > configCount)
                {
                    if (resultcount[0] > ratio)
                    {
                        resultcount.Add(ratio);
                    }
                    else
                    {
                        resultcount.Add(resultcount[0]);
                    }
                }
                else
                {
                    if (resultcount[0] > configCount)
                    {
                        resultcount.Add(configCount);
                    }
                    else
                    {
                        resultcount.Add(resultcount[0]);
                    }
                }
            }

            List<SearchResult> resultFinal = new List<SearchResult>();
            for (int i = 0; i < finalResult.Count; i++)
            {
                if (flightResultCount.ContainsKey(finalResult[i].Airline))
                {
                    if (flightResultCount[finalResult[i].Airline][1] > 0)
                    {
                        flightResultCount[finalResult[i].Airline][1]--;
                        resultFinal.Add(finalResult[i]);
                    }
                }
            }
            // Adding resultId and Eticketable information.
            Airline tempAirline = new Airline();
            bool airlineError = false;
            for (int i = 0; i < resultFinal.Count; i++)
            {
                resultFinal[i].ResultId = i + 1;
                resultFinal[i].EticketEligible = true;
                for (int g = 0; g < resultFinal[i].Flights.Length; g++)
                {
                    for (int f = 0; f < resultFinal[i].Flights[g].Length; f++)
                    {
                        try
                        {
                            tempAirline.Load(resultFinal[i].Flights[g][f].Airline);
                        }
                        catch (ArgumentException)
                        {
                            airlineError = true;
                        }
                        if (airlineError)
                        {
                            Audit.Add(EventType.Search, Severity.Normal, 1, "Result omitted due to " + resultFinal[i].Flights[g][f].Airline + " Airline Code unavailable", "");
                            resultFinal.Remove(resultFinal[i]);
                            i--;
                            break;
                        }
                        else
                        {
                            if (!(resultFinal[i].Flights[g][f].ETicketEligible && tempAirline.ETicketEligible))
                            {
                                resultFinal[i].EticketEligible = false;
                            }
                        }
                    }
                    if (airlineError)
                    {
                        airlineError = false;
                        break;
                    }
                }
            }
            Trace.TraceInformation("MetaSearchEngine.CombineSources Exit");
            Audit.Add(EventType.Search, Severity.Normal, 1, "Displayed result count : " + finalResult.Count, "");
            return resultFinal.ToArray();
        }

        private SearchRequest GetGdsRequest(string gds)
        {
            Dictionary<string, List<string>> airlineSources = Airline.GetAirlineGDS();
            //Hermes GDS Type source : Add new Airline here
            List<string> allHermesLcc = new List<string>(new string[] { "IX", "G9" });
            List<string> prefList = new List<string>();
            if (request.Segments[0].PreferredAirlines != null
                && request.Segments[0].PreferredAirlines.Length > 0)
            {
                for (int i = 0; i < request.Segments[0].PreferredAirlines.Length; i++)
                {
                    if (request.IsDomestic)
                    {
                        if (airlineSources[gds].Contains(request.Segments[0].PreferredAirlines[i])
                            && !prefList.Contains(request.Segments[0].PreferredAirlines[i]))
                        {
                            prefList.Add(request.Segments[0].PreferredAirlines[i]);
                        }
                    }
                    else
                    {
                        if (!allHermesLcc.Contains(request.Segments[0].PreferredAirlines[i]) && !prefList.Contains(request.Segments[0].PreferredAirlines[i]))
                        {
                            prefList.Add(request.Segments[0].PreferredAirlines[i]);
                        }
                    }
                }
            }
            else if (request.IsDomestic && airlineSources.Count > 0)//For Domestic airlineSources will be null 
            {
                for (int i = 0; i < airlineSources[gds].Count; i++)
                {
                    if (airlineSources[gds][i].Length == 2)
                    {
                        prefList.Add(airlineSources[gds][i]);
                    }
                }
            }
            SearchRequest req = request.Copy();
            if (req.IsDomestic)
            {
                req.RestrictAirline = true;
            }
            req.Segments[0].PreferredAirlines = prefList.ToArray();
            return req;
        }

        ///// <summary>
        ///// To Release Seats
        ///// </summary>
        ///// <param name="requestedPnr">Pnr, of which booking is to be cancelled</param>
        ///// <param name="bookingSource">booking source of the booking</param>
        ///// <returns>string containing Respone of Canceling booking</returns>
        public static string CancelItinerary(string requestedPnr, BookingSource bookingSource)
        {
            string response = string.Empty;
            //if (bookingSource == BookingSource.WorldSpan)
            //{
            //    response = Worldspan.CancelItinerary(requestedPnr);
            //}
            //else if (bookingSource == BookingSource.Amadeus)
            //{
            //    response = Amadeus.CancelItinerary(requestedPnr);
            //}
            //else if (bookingSource == BookingSource.Galileo)
            //{
            //    response = GalileoApi.CancelItinerary(requestedPnr);
            //}
            if (bookingSource == BookingSource.UAPI)
            {
                response = UAPI.CancelItinerary(requestedPnr, "AR");// UAPI Cancel PNR-- AR-Air Reservation(Booking PNR)
            }
            else if (bookingSource == BookingSource.TBOAir)
            {
                TBOAir.AirV10 tbo = new AirV10();
                //FlightItinerary itinerary = new FlightItinerary(FlightItinerary.GetFlightId(requestedPnr));
                string bookingIds = string.Empty;
                string sourceIds = string.Empty;
                string[] bookings = new string[0];
                string[] sources = new string[0];
                if (requestedPnr != null && requestedPnr.Length > 0)
                {
                    bookingIds = requestedPnr.Split('+')[0];
                    sourceIds = requestedPnr.Split('+')[1];
                    if (bookingIds != null && bookingIds.Length > 0 && sourceIds != null && sourceIds.Length > 0)
                    {
                        bookings = bookingIds.Split('|');
                        sources = sourceIds.Split('|');

                        // string source = "4";

                        for (int i = 0; i < bookings.Length; i++)
                        {
                            string bookingId = bookings[i];
                            bool released = false;
                            if (!string.IsNullOrEmpty(bookingId) && !string.IsNullOrEmpty(sources[i]))
                            {
                                released = tbo.ReleasePNR(Convert.ToInt32(bookingId), sources[i]);//sources[i]
                                if (released)
                                {
                                    if (!string.IsNullOrEmpty(response))
                                    {
                                        response = response + "|" + bookingId;
                                    }
                                    else
                                    {
                                        response = bookingId;
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                throw new ArgumentException("Cancellation is not allowed for " + bookingSource.ToString());
            }
            return response;
        } 

        # endregion


        #region FareQuote for LCCs
        public Fare[] GetFareQuote(SearchResult result)
        {
            Fare[] fare = new Fare[0];
            decimal fuelSurcharge = 0;
            decimal txnFeePercent = 0;
            decimal tax = 0;
            decimal dbTax = 0;
            string origin = result.Flights[0][0].Origin.CityCode;
            string destination = result.Flights[0][result.Flights[0].Length - 1].Destination.CityCode;
            Hashtable fareQuote = new Hashtable();
           
          
            #region AIRARABIA G9
            if (result.ResultBookingSource == BookingSource.AirArabia)
            {
                AirArabia airarbia = new AirArabia();
                airarbia.GetFareQuotes(result, sessionId);
                fare = new Fare[result.FareBreakdown.Length];
                fare = result.FareBreakdown;
                //for getting fare quote 
            }
            #endregion
            else
            {
                throw new ArgumentException("Fare Quote is not available for the given result source.");
            }
            return fare;
        }
        #endregion
        #region Hotel Search

        /// <summary>
        /// Get the hotel results for the given search
        /// </summary>
        /// <param name="req">request info</param>
        /// <param name="member">current logged member</param>
        /// <returns></returns>
        private HotelCity hCity = null;
        public HotelSearchResult[] GetHotelResults(HotelRequest req, long member)
        {
            agencyId = member;
            Trace.TraceInformation("MetaSearchEngine.GetHotelResults Entered");
            StartSession();
            hotelRequest = req;
            hCity = HotelCity.Load(hotelRequest.CityId);
            //Audit.Add(EventType.HotelSearch, Severity.Normal, 1, "GetHotelResults Entered city name = " + req.CityName, "0");

            //// Add Threads to be executed
           
            if (req.Sources.Contains("DOTW") )//&& Role.IsAllowedTask(member.RoleId, (int)Task.DOTW)) ziya-todo
            {
                //Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, (int)member, "Entered DOTW Hotel Search", "0");
                listOfThreads.Add("DOTW", new WaitCallback(SearchResultDOTW));
            }
            if (req.Sources.Contains("HIS"))
            {
               // Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, (int)member, "Entered HIS Hotel Search", "1");
                listOfThreads.Add("HIS", new WaitCallback(SearchResultHotelConnect));
            }

            if (req.Sources.Contains("RezLive"))
            {
                //Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, (int)member, "Entered RezLive Hotel Search", "2");
                listOfThreads.Add("RezLive", new WaitCallback(SearchResultRezLive));
            }

            if (req.Sources.Contains("LOH"))
            {
               // Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, (int)member, "Entered LotsOfHotels Hotel Search", "3");
                listOfThreads.Add("LOH", new WaitCallback(SearchResultLotsOfHotels));
            }

            if (req.Sources.Contains("HotelBeds"))
            {
                //Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, (int)member, "Entered HotelBeds Hotel Search", "4");
                listOfThreads.Add("HotelBeds", new WaitCallback(SearchResultHotelBeds));
            }

            if (req.Sources.Contains("GTA"))
            {                
                listOfThreads.Add("GTA", new WaitCallback(SearchResultGTA));
            }
            //Miki source  added by brahmam 02.09.2015
            if (req.Sources.Contains("Miki"))
            {
                //Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, (int)member, "Entered HotelBeds Hotel Search", "4");
                listOfThreads.Add("Miki", new WaitCallback(SearchResultMiki));
            }
            //TBOHotel source  added by brahmam 16.04.2016
            if (req.Sources.Contains("TBOHotel"))
            {
                
                listOfThreads.Add("TBOHotel", new WaitCallback(SearchResultTBOHotel));
            }
            //WST source  added by brahmam 09.06.2016
            if (req.Sources.Contains("WST"))
            {

                listOfThreads.Add("WST", new WaitCallback(SearchResultWST));
            }
            HotelSearchResult[] result = new HotelSearchResult[0];

            // take only those sources in ready sources for which the search is allowed

            Dictionary<string, int> readySources = GetReadyHotelSources();

            eventFlag = new AutoResetEvent[readySources.Count];
            int maxTimeOut = 0;
            int i = 0;
            foreach (KeyValuePair<string, int> activeSource in readySources)
            {
                maxTimeOut = (int)activeSource.Value > maxTimeOut ? (int)activeSource.Value : maxTimeOut;
                i++;
            }

            int j = 0;

            foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
            {
                if (readySources.ContainsKey(deThread.Key))
                {
                    ThreadPool.QueueUserWorkItem(deThread.Value, j);
                    eventFlag[j] = new AutoResetEvent(false);
                    j++;
                }
            }

            if (j != 0)
            {
                if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 0, maxTimeOut), true))
                {
                    //TODO: audit which thread is timed out                
                }
            }

            // combined sources    
            result = CombineHotelSources();

            //try
            //{
            //    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(hotelSearchResBySrc.GetType());
            //    System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //    System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            //    ser.Serialize(writer, result);  // Here Classes are converted to XML String. 
            //    // This can be viewed in SB or writer.
            //    // Above XML in SB can be loaded in XmlDocument object
            //    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            //    doc.LoadXml(sb.ToString());
            //    doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\DOTW\HotelSearchResults.xml");
            //}
            //catch
            //{
            //}
            Trace.TraceInformation("MetaSearchEngine.GetHotelResults Exit");
            return result;
        }

        

        private void SearchResultDOTW(object eventNumber)
        {
           Trace.TraceInformation("MetaSearchEngine.SearchResultDOTWApi Enter");
            //DOTW DBG
            //Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, 1, "Entered SearchResultDOTWApi", "0");
            HotelSearchResultBySources src = new HotelSearchResultBySources();
            src.Source = "DOTW";
            //this hReqDOTW is for only DOTW since the city code changes for the source
            //copy by value the object variable to resolve the reference update of city code
            HotelRequest hReqDOTW = new HotelRequest();
            hReqDOTW = hotelRequest.CopyByValue();

            try
            {
                //get the city code for DOTW
                //Also set the currency for that city
                if (hCity.DOTWCode != null && hCity.DOTWCode.Length > 0)
                {
                    hReqDOTW.CityCode = hCity.DOTWCode;
                    hReqDOTW.Currency =Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"];
                }
                else
                {
                    //TODO -- Email whenever the city code is not found
                    eventFlag[(int)eventNumber].Set();//the thread is no longer use
                    Audit.Add(EventType.DOTWAvailSearch, Severity.High, 0, "MetaSearchEngine.SearchResultDOTW : City code not Found for CountryName = " + hotelRequest.CountryName + " and CityName = " + hotelRequest.CityName, string.Empty);
                    Trace.TraceInformation("MetaSearchEngine.SearchResultDOTW : City code not Found for CountryName = " + hotelRequest.CountryName + " and CityName = " + hotelRequest.CityName);
                    return;//there will no DOTW call if the city code not found for the selected city.
                }
                DataTable dtMarkup = UpdateMarkup.Load((int)agencyId, HotelBookingSource.DOTW.ToString(), (int)ProductType.Hotel);
                DataView dv = dtMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2B')";
                dtMarkup = dv.ToTable();

                //assign the current session to the DOTW search
                CT.BookingEngine.GDS.DOTWApi DOTW = new CT.BookingEngine.GDS.DOTWApi(sessionId);
                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    DOTW.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    DOTW.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                }
                else
                {
                    DOTW.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    DOTW.AgentCurrency = SettingsLoginInfo.Currency;
                }

                decimal markup=0; string markupType=string.Empty;

                if(dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                }
                //DOTW.AppUserId = 1;//TO DO
                DOTW.AppUserId = appUserId;
                //src.Result = DOTW.GetHotelAvailability(hReqDOTW); //version 1
                src.Result = DOTW.GetHotelAvailabilityV2(hReqDOTW, markup, markupType);// version 2
                // resizing according to search result count
                HotelSearchResult[] finalResult = new HotelSearchResult[src.Result.Length];
                finalResult = src.Result;
                //Array.Sort(finalResult);
                //Array.Sort(finalResult, finalResult[0]);
                Array.Sort(finalResult, delegate(HotelSearchResult h1, HotelSearchResult h2) { return h1.TotalPrice.CompareTo(h2.TotalPrice); });
                int showResultCount = Configuration.ConfigurationSystem.ResultCountBySources["DOTW"];
                if (src.Result.Length > showResultCount)
                {
                    Array.Resize(ref finalResult, showResultCount);
                }
                src.Result = finalResult;
            }
            catch (WebException ex)
            {
                src.Error = ex.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultDOTW :WebException for DOTW = " + ex.Message);
                CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Web Exception returned from DOTW. Error Message:" + ex.Message + " | " + DateTime.Now + "| city =" + hReqDOTW.CityCode + " | checkin Date =" + hReqDOTW.StartDate, "");
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultDOTW :Exception for DOTW = " + excep.Message);
                CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned from DOTW. Error Message:" + excep.Message + " | " + DateTime.Now + "| city =" + hReqDOTW.CityCode + " | checkin Date =" + hReqDOTW.StartDate, "");
            }
            hotelSearchResBySrc.Add(src);
            eventFlag[(int)eventNumber].Set();
            Trace.TraceInformation("MetaSearchEngine.SearchResultDOTW Exit");
        }

        private void SearchResultHotelConnect(object eventNumber)
        {
            Trace.TraceInformation("MetaSearchEngine.SearchResultHIS Enter");
            //DOTW DBG
            //Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "HotelConnect Entered", "1");
            HotelSearchResultBySources src = new HotelSearchResultBySources();
            src.Source = "HIS";
            //this hReqDOTW is for only DOTW since the city code changes for the source
            //copy by value the object variable to resolve the reference update of city code
            HotelRequest hReqHIS = new HotelRequest();
            hReqHIS = hotelRequest.CopyByValue();

            try
            {
                //get the city code for HIS
                //Also set the currency for that city
                if (hCity.HISCode != null && hCity.HISCode.Length > 0)
                {
                    hReqHIS.CityCode = hCity.HISCode;
                    hReqHIS.CityId = Convert.ToInt32(hCity.HISCode);
                    hReqHIS.Currency = Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"];
                    hReqHIS.CountryName = hCity.CountryName;
                }
                else
                {
                    //TODO -- Email whenever the city code is not found
                    eventFlag[(int)eventNumber].Set();//the thread is no longer use
                    Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultHIS : City code not Found for CountryName = " + hReqHIS.CountryName + " and CityName = " + hReqHIS.CityName, "1");
                    Trace.TraceInformation("MetaSearchEngine.SearchResultHIS : City code not Found for CountryName = " + hReqHIS.CountryName + " and CityName = " + hReqHIS.CityName);
                    return;//there will no DOTW call if the city code not found for the selected city.
                }

                Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultHIS : City code Found for CountryName = " + hReqHIS.CountryName + " and CityId = " + hReqHIS.CityId, "1");
                CZInventory.SearchEngine se = new CZInventory.SearchEngine();

                Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "HotelConnect Connection String: " + System.Configuration.ConfigurationSettings.AppSettings["HotelConnect"], "1");
                Dictionary<string, decimal> ExchangeRates = new Dictionary<string, decimal>();
                string currency = "";
                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    currency = SettingsLoginInfo.OnBehalfAgentCurrency;
                }
                else
                {
                    ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    currency = SettingsLoginInfo.Currency;
                }
                DOTWCountry dotw = new DOTWCountry();
                Dictionary<string, string> Countries = dotw.GetAllCountries();
                hReqHIS.PassengerNationality = Countries[hReqHIS.PassengerNationality];

                DataTable dtMarkup = UpdateMarkup.Load((int)agencyId, "HIS", (int)ProductType.Hotel);
                DataView dv = dtMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2B')";
                dtMarkup = dv.ToTable();

                decimal markup = 0; string markupType = string.Empty;

                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                }

                src.Result = se.GetHotels(hReqHIS, ExchangeRates, currency, markup, markupType).ToArray();
                Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "HotelConnect Search Results: " + src.Result.Length, "1");
                //Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "HotelConnect Search Results - First Hotel Name : " + src.Result[0].HotelName, "1");                

                HotelSearchResult[] finalResult = new HotelSearchResult[src.Result.Length];
                finalResult = src.Result;
                if (src.Result.Length > 0)
                {
                    Array.Sort(finalResult, delegate(HotelSearchResult h1, HotelSearchResult h2) { return h1.TotalPrice.CompareTo(h2.TotalPrice); });
                }
                int showResultCount = 100;
                if (src.Result.Length > showResultCount)
                {
                    Array.Resize(ref finalResult, showResultCount);
                }
                src.Result = finalResult;
            }
            catch (WebException ex)
            {
                src.Error = ex.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultHIS :WebException for HIS = " + ex.Message);
                Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultHIS : City code not Found for CountryName = " + hotelRequest.CountryName + " and CityName = " + hotelRequest.CityName, "1");
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultHIS :Exception for HIS =" + excep.Message);
                Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultHIS :Exception for HIS =" + excep.ToString(), "1");
            }
            hotelSearchResBySrc.Add(src);
            eventFlag[(int)eventNumber].Set();
            Trace.TraceInformation("MetaSearchEngine.SearchResultHIS Exit");
        }
        //modified by brahmam   16/09/2015
        private void SearchResultRezLive(object eventNumber)
        {
            Trace.TraceInformation("MetaSearchEngine.SearchResultRezLive Enter");
            //DOTW DBG
           // Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "RezLive Entered", "1");
            HotelSearchResultBySources src = new HotelSearchResultBySources();
            src.Source = "RezLive";
            //this hReqDOTW is for only DOTW since the city code changes for the source
            //copy by value the object variable to resolve the reference update of city code
            HotelRequest hReqRez = new HotelRequest();
            hReqRez = hotelRequest.CopyByValue();

            try
            {
                //get the city code for DOTW
                //Also set the currency for that city
                if (hCity.RezCode != null && hCity.RezCode.Length > 0)
                {
                    hReqRez.CityCode = hCity.RezCode;
                    hReqRez.CityId = Convert.ToInt32(hCity.CityId);
                    hReqRez.Currency = Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"];
                    hReqRez.CountryName = hCity.CountryName;
                }
                else
                {
                    //TODO -- Email whenever the city code is not found
                    eventFlag[(int)eventNumber].Set();//the thread is no longer use
                    Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultRezLive : City code not Found for CountryName = " + hReqRez.CountryName + " and CityName = " + hReqRez.CityName, "1");
                    Trace.TraceInformation("MetaSearchEngine.SearchResultRezLive : City code not Found for CountryName = " + hReqRez.CountryName + " and CityName = " + hReqRez.CityName);
                    return;//there will no Rezlive call if the city code not found for the selected city.
                }

                Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultRezLive : City code Found for CountryName = " + hReqRez.CountryName + " and CityId = " + hReqRez.CityId, "1");

                DOTWCountry dotw = new DOTWCountry();
                Dictionary<string,string> Countries = dotw.GetAllCountries();
                hReqRez.PassengerCountryOfResidence = Country.GetCountryCodeFromCountryName(Countries[hReqRez.PassengerCountryOfResidence]);
                hReqRez.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[hReqRez.PassengerNationality]);
                hReqRez.CountryName = Country.GetCountryCodeFromCountryName(hReqRez.CountryName);
                //hReqRez.CityName = hReqRez.CityName.Replace(" City", "");
                hReqRez.CityName = hReqRez.CityCode.Trim();// Rez Live using city code to search
                RezLive.XmlHub searchEngine = new RezLive.XmlHub(sessionId);
                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    searchEngine.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    searchEngine.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    searchEngine.DecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    searchEngine.AgentCurrency = SettingsLoginInfo.Currency;
                    searchEngine.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    searchEngine.DecimalPoint = SettingsLoginInfo.DecimalValue;
                }
                DataTable dtMarkup = UpdateMarkup.Load((int)agencyId, HotelBookingSource.RezLive.ToString(), (int)ProductType.Hotel);
                DataView dv = dtMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2B')";
                dtMarkup = dv.ToTable();
                decimal markup = 0; string markupType = string.Empty;

                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                }

                src.Result = searchEngine.GetHotelResults(hReqRez,markup,markupType);
                Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "RezLive Search Results: " + src.Result.Length, "1");
                //if (src.Result.Length > 0)
               // {
               //     Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "RezLive Search Results - First Hotel Name : " + src.Result[0].HotelName, "1");
              //      Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "RezLive Search Results - Booking Source : " + src.Result[0].BookingSource, "1");
              //  }
                HotelSearchResult[] finalResult = new HotelSearchResult[src.Result.Length];
                finalResult = src.Result;
        Array.Sort(finalResult, delegate(HotelSearchResult h1, HotelSearchResult h2) { return h1.TotalPrice.CompareTo(h2.TotalPrice); });

                //Sort the results by price first then resize the result
                //Newly added by shiva...earlier it was there commented in CombineHotelSources() on 26 May 2014
                //for (int n = 0; n < finalResult.Length; n++)
                //{
                //    for (int m = n + 1; m < finalResult.Length; m++)
                //    {
                //        decimal exRateN = 1, exRateM = 1;
                //        //if (rateOfEx.ContainsKey(finalResult[n].Currency))
                //        //{
                //        //    exRateN = rateOfEx[finalResult[n].Currency];
                //        //}
                //        //if (rateOfEx.ContainsKey(finalResult[m].Currency))
                //        //{
                //        //    exRateM = rateOfEx[finalResult[m].Currency];
                //        //}
                //        decimal priceN = finalResult[n].RoomDetails[0].TotalPrice * exRateN;
                //        decimal priceM = finalResult[m].RoomDetails[0].TotalPrice * exRateM;
                //        if (priceN > priceM)
                //        {
                //            HotelSearchResult temp = finalResult[n];
                //            finalResult[n] = finalResult[m];
                //            finalResult[m] = temp;
                //        }
                //    }
                //}

                //int showResultCount = 100;
                int showResultCount = Configuration.ConfigurationSystem.ResultCountBySources["RezLive"];
                if (src.Result.Length > showResultCount)
                {
                    Array.Resize(ref finalResult, showResultCount);
                }
                src.Result = finalResult;
            }
            catch (WebException ex)
            {
                src.Error = ex.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultRezLive :WebException for RezLive = " + ex.Message);
                Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultRezLive : City code not Found for CountryName = " + hotelRequest.CountryName + " and CityName = " + hotelRequest.CityName, "1");
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultRezLive :Exception for RezLive =" + excep.Message);
                Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultRezLive :Exception for RezLive =" + excep.ToString(), "1");
            }
            hotelSearchResBySrc.Add(src);
            eventFlag[(int)eventNumber].Set();
            Trace.TraceInformation("MetaSearchEngine.SearchResultRezLive Exit");
        }

        private void SearchResultLotsOfHotels(object eventNumber)
        {
            Trace.TraceInformation("MetaSearchEngine.SearchResultLotsOfHotels Enter");
            //Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "LotsOfHotels Entered", "1");
            HotelSearchResultBySources src = new HotelSearchResultBySources();
            src.Source = "LotsOfHotels";
            //this hReqDOTW is for only DOTW since the city code changes for the source
            //copy by value the object variable to resolve the reference update of city code
            HotelRequest hReqLOH = new HotelRequest();
            hReqLOH = hotelRequest.CopyByValue();

            try
            {
                //get the city code for DOTW
                //Also set the currency for that city
                if (hCity.LOHCode != null && hCity.LOHCode.Length > 0)
                {
                    hReqLOH.CityCode = hCity.LOHCode;
                    hReqLOH.CityId = Convert.ToInt32(hCity.LOHCode);
                    hReqLOH.Currency = Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"];
                    hReqLOH.CountryName = hCity.CountryName;
                }
                else
                {
                    //TODO -- Email whenever the city code is not found
                    eventFlag[(int)eventNumber].Set();//the thread is no longer use
                    Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultLotsOfHotels : City code not Found for CountryName = " + hReqLOH.CountryName + " and CityName = " + hReqLOH.CityName, "1");
                    Trace.TraceInformation("MetaSearchEngine.SearchResultLotsOfHotels : City code not Found for CountryName = " + hReqLOH.CountryName + " and CityName = " + hReqLOH.CityName);
                    return;//there will no DOTW call if the city code not found for the selected city.
                }

                Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultLotsOfHotels : City code Found for CountryName = " + hReqLOH.CountryName + " and CityId = " + hReqLOH.CityCode, "1");

                DOTWCountry dotw = new DOTWCountry();
                Dictionary<string, string> Countries = dotw.GetAllCountries();
                //hReqLOH.PassengerCountryOfResidence = Country.GetCountryCodeFromCountryName(Countries[hReqLOH.PassengerCountryOfResidence]);
                hReqLOH.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[hReqLOH.PassengerNationality]);
                //hReqLOH.CountryName = Country.GetCountryCodeFromCountryName(hReqLOH.CountryName);
                ////hReqLOH.CityName = hReqLOH.CityCode;

                LotsOfHotels.JuniperXMLEngine searchEngine = new LotsOfHotels.JuniperXMLEngine(sessionId);
                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    searchEngine.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    searchEngine.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                }
                else
                {
                    searchEngine.AgentCurrency = SettingsLoginInfo.Currency;
                    searchEngine.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                }
                DataTable dtMarkup = UpdateMarkup.Load((int)agencyId, HotelBookingSource.LOH.ToString(), (int)ProductType.Hotel);
                DataView dv = dtMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2B')";
                dtMarkup = dv.ToTable();
                decimal markup = 0; string markupType = string.Empty;

                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                }
                src.Result = searchEngine.GetSearchResults(hReqLOH, markup, markupType);
                Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "LotsOfHotels Search Results: " + src.Result.Length, "1");
                
                HotelSearchResult[] finalResult = new HotelSearchResult[src.Result.Length];
                finalResult = src.Result;
                if (src.Result.Length > 0)
                {                    
                    Array.Sort(finalResult, delegate(HotelSearchResult h1, HotelSearchResult h2) { return h1.TotalPrice.CompareTo(h2.TotalPrice); });
                }

                int showResultCount = ConfigurationSystem.ResultCountBySources["LOH"];
                if (src.Result.Length > showResultCount)
                {
                    Array.Resize(ref finalResult, showResultCount);
                }
                src.Result = finalResult;
            }
            catch (WebException ex)
            {
                src.Error = ex.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultLotsOfHotels :WebException for LotsOfHotels = " + ex.Message);
                Audit.Add(EventType.DOTWAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultLotsOfHotels : City code not Found for CountryName = " + hotelRequest.CountryName + " and CityName = " + hotelRequest.CityName, "1");
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultLotsOfHotels :Exception for LOH =" + excep.Message);
                Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultLotsOfHotels :Exception for LOH =" + excep.ToString(), "1");
            }
            hotelSearchResBySrc.Add(src);
            eventFlag[(int)eventNumber].Set();
            Trace.TraceInformation("MetaSearchEngine.SearchResultLotsOfHotels Exit");
        }

        private void SearchResultHotelBeds(object eventNumber)
        {
            Trace.TraceInformation("MetaSearchEngine.SearchResultHotelBeds Enter");
            //HOB DBG
            //Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "HotelBeds Entered", "1");
            HotelSearchResultBySources src = new HotelSearchResultBySources();
            src.Source = "HotelBeds";
            //this hReqHOB is for only HOB since the city code changes for the source
            //copy by value the object variable to resolve the reference update of city code
            HotelRequest hReqHOB = new HotelRequest();
            hReqHOB = hotelRequest.CopyByValue();

            try
            {
                //get the city code for HOB
                //Also set the currency for that city
                if (hCity.HOBCode != null && hCity.HOBCode.Length > 0)
                {
                    hReqHOB.CityCode = hCity.HOBCode;
                    hReqHOB.CityId = Convert.ToInt32(hCity.CityId);
                    hReqHOB.Currency = Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"];
                    hReqHOB.CountryName = hCity.CountryName;
                }
                else
                {
                    //TODO -- Email whenever the city code is not found
                    eventFlag[(int)eventNumber].Set();//the thread is no longer use
                    Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultHotelBeds : City code not Found for CountryName = " + hReqHOB.CountryName + " and CityName = " + hReqHOB.CityName, "1");
                    Trace.TraceInformation("MetaSearchEngine.SearchResultHotelBeds : City code not Found for CountryName = " + hReqHOB.CountryName + " and CityName = " + hReqHOB.CityName);
                    return;//there will no DOTW call if the city code not found for the selected city.
                }

                Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultHotelBeds : City code Found for CountryName = " + hReqHOB.CountryName + " and CityId = " + hReqHOB.CityCode, "1");

                DOTWCountry dotw = new DOTWCountry();
                Dictionary<string, string> Countries = dotw.GetAllCountries();
                //hReqLOH.PassengerCountryOfResidence = Country.GetCountryCodeFromCountryName(Countries[hReqLOH.PassengerCountryOfResidence]);
                hReqHOB.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[hReqHOB.PassengerNationality]);
                //hReqLOH.CountryName = Country.GetCountryCodeFromCountryName(hReqLOH.CountryName);
                ////hReqLOH.CityName = hReqLOH.CityCode;

                HotelBeds hotelBeds = new HotelBeds();
                hotelBeds.sessionId = sessionId;
                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    hotelBeds.AgentExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    hotelBeds.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    hotelBeds.AgentDecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    hotelBeds.AgentExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    hotelBeds.AgentCurrency = SettingsLoginInfo.Currency;
                    hotelBeds.AgentDecimalPoint = SettingsLoginInfo.DecimalValue;
                }

                DataTable dtMarkup = UpdateMarkup.Load((int)agencyId, HotelBookingSource.HotelBeds.ToString(), (int)ProductType.Hotel);
                DataView dv = dtMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2B')";
                dtMarkup = dv.ToTable();
                decimal markup = 0; string markupType = string.Empty;

                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                }
                src.Result = hotelBeds.GetHotelAvailability(hReqHOB, markup, markupType);
                Audit.Add(EventType.HotelConnectAvailSearch, Severity.Normal, 0, "HotelBeds Search Results: " + src.Result.Length, "1");

                HotelSearchResult[] finalResult = new HotelSearchResult[src.Result.Length];
                finalResult = src.Result;
                if (src.Result.Length > 0)
                {
                    Array.Sort(finalResult, delegate(HotelSearchResult h1, HotelSearchResult h2) { return h1.TotalPrice.CompareTo(h2.TotalPrice); });
                }

                int showResultCount = ConfigurationSystem.ResultCountBySources["HotelBeds"];
                if (src.Result.Length > showResultCount)
                {
                    Array.Resize(ref finalResult, showResultCount);
                }
                src.Result = finalResult;
            }
            catch (WebException ex)
            {
                src.Error = ex.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultHotelBeds :WebException for HotelBeds = " + ex.Message);
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultHotelBedsOfHotels : City code not Found for CountryName = " + hotelRequest.CountryName + " and CityName = " + hotelRequest.CityName, "1");
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultHotelBeds :Exception for HotelBeds =" + excep.Message);
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultHotelBeds :Exception for HotelBeds =" + excep.ToString(), "1");
            }
            hotelSearchResBySrc.Add(src);
            eventFlag[(int)eventNumber].Set();
            Trace.TraceInformation("MetaSearchEngine.SearchResultHotelBeds Exit");
        }

        private void SearchResultGTA(object eventNumber)
        {
            Trace.TraceInformation("MetaSearchEngine.SearchResultGTA Enter");
            //Audit.Add(EventType.GTAAvailSearch, Severity.Normal, 1, "Entered SearchResultGTA", "0");
            HotelSearchResultBySources src = new HotelSearchResultBySources();
            src.Source = "GTA";
            HotelRequest hReqGTA = new HotelRequest();
            hReqGTA = hotelRequest.CopyByValue();
            try
            {

                if (hCity.GTACode != null && hCity.GTACode.Length > 0)
                {
                    hReqGTA.CityCode = hCity.GTACode;
                    hReqGTA.CityId = Convert.ToInt32(hCity.CityId);
                    hReqGTA.Currency = Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"];
                    hReqGTA.CountryName = hCity.CountryName;
                }
                else
                {
                    eventFlag[(int)eventNumber].Set();
                    Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, "MetaSearchEngine.SearchResultGTA : City code not Found for CountryName = " + this.hotelRequest.CountryName + " and CityName = " + this.hotelRequest.CityName, string.Empty);
                    Trace.TraceInformation("MetaSearchEngine.SearchResultGTA : City code not Found for CountryName = " + this.hotelRequest.CountryName + " and CityName = " + this.hotelRequest.CityName);
                    return;
                }

                Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultGTA : City code Found for CountryName = " + hReqGTA.CountryName + " and CityId = " + hReqGTA.CityCode, "1");
                DOTWCountry dotw = new DOTWCountry();
                Dictionary<string, string> Countries = dotw.GetAllCountries();
                hReqGTA.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[hReqGTA.PassengerNationality]);

                GTA gtaApi = new GTA();
                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    gtaApi.AgentExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    gtaApi.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    gtaApi.AgentDecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    gtaApi.AgentExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    gtaApi.AgentCurrency = SettingsLoginInfo.Currency;
                    gtaApi.AgentDecimalPoint = SettingsLoginInfo.DecimalValue;
                }
                DataTable dtMarkup = UpdateMarkup.Load((int)agencyId, HotelBookingSource.GTA.ToString(), (int)ProductType.Hotel);
                DataView dv = dtMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2B')";
                dtMarkup = dv.ToTable();
                decimal markup = 0; string markupType = string.Empty;

                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                }
                src.Result = gtaApi.GetHotelAvailability(hReqGTA, markup, markupType, string.Empty, string.Empty);
                HotelSearchResult[] finalResult = new HotelSearchResult[src.Result.Length];
                finalResult = src.Result;
                if (src.Result.Length > 0)
                {
                    Array.Sort(finalResult, delegate(HotelSearchResult h1, HotelSearchResult h2) { return h1.TotalPrice.CompareTo(h2.TotalPrice); });
                }
                int showResultCount = ConfigurationSystem.ResultCountBySources["GTA"];
                if (src.Result.Length > showResultCount)
                {
                    Array.Resize<HotelSearchResult>(ref finalResult, showResultCount);
                }
                src.Result = finalResult;
            }
            catch (WebException ex)
            {
                src.Error = ex.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultGTA :WebException for GTA = " + ex.Message);
                Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, "Web Exception returned from GTA. Error Message:" + ex.Message + " | " + DateTime.Now + "| city =" + hReqGTA.CityCode + " | checkin Date =" + hReqGTA.StartDate, "");
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultGTA :Exception for GTA = " + excep.Message);
                CT.Core.Audit.Add(CT.Core.EventType.GTAAvailSearch, CT.Core.Severity.High, 0, "Exception returned from GTA. Error Message:" + excep.Message + " | " + DateTime.Now + "| city =" + hReqGTA.CityCode + " | checkin Date =" + hReqGTA.StartDate, "");
            }
            this.hotelSearchResBySrc.Add(src);
            this.eventFlag[(int)eventNumber].Set();
            Trace.TraceInformation("MetaSearchEngine.SearchResultGTA Exit");
        }


        /// <summary>
        /// Miki Search Results   Added by brahmam 02.09.2015
        /// </summary>
        /// <param name="eventNumber"></param>
        private void SearchResultMiki(object eventNumber)
        {
            Trace.TraceInformation("MetaSearchEngine.SearchResultMikiApi Enter");
            //Miki DBG
            //Audit.Add(EventType.MikiAvailSearch, Severity.Normal, 1, "Entered SearchResultMikiApi", "0");
            HotelSearchResultBySources src = new HotelSearchResultBySources();
            src.Source = "Miki";
            //this hReqMiki is for only Miki since the city code changes for the source
            //copy by value the object variable to resolve the reference update of city code
            HotelRequest hReqMiki = new HotelRequest();
            hReqMiki = hotelRequest.CopyByValue();

            try
            {
                //get the city code for Miki
                if (hCity.MikiCode != null && hCity.MikiCode.Length > 0)
                {
                    hReqMiki.CityCode = hCity.MikiCode;
                    hReqMiki.CityId = hCity.CityId;
                    hReqMiki.Currency = Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"];
                    hReqMiki.CountryName = hCity.CountryName;
                }
                else
                {
                    //TODO -- Email whenever the city code is not found
                    eventFlag[(int)eventNumber].Set();//the thread is no longer use
                    //Audit.Add(EventType.MikiAvailSearch, Severity.High, 0, "MetaSearchEngine.SearchResultMiki : City code not Found for CountryName = " + hotelRequest.CountryName + " and CityName = " + hotelRequest.CityName, string.Empty);
                    Trace.TraceInformation("MetaSearchEngine.SearchResultMiki : City code not Found for CountryName = " + hotelRequest.CountryName + " and CityName = " + hotelRequest.CityName);
                    return;//there will no Miki call if the city code not found for the selected city.
                }
                //Audit.Add(EventType.MikiAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultMiki : City code Found for CountryName = " + hReqMiki.CountryName + " and CityId = " + hReqMiki.CityCode, "1");
                DOTWCountry dotw = new DOTWCountry();
                Dictionary<string, string> Countries = dotw.GetAllCountries();
                hReqMiki.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[hReqMiki.PassengerNationality]);
                //assign the current session to the Miki search
                MikiApi miki = new MikiApi(sessionId);
                //MikiApi miki = new MikiApi();
                DataTable dtMarkup = UpdateMarkup.Load((int)agencyId, HotelBookingSource.Miki.ToString(), (int)ProductType.Hotel);
                DataView dv = dtMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2B')";
                dtMarkup = dv.ToTable();
                decimal markup = 0; string markupType = string.Empty;

                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                }

                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    miki.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    miki.AgentExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    miki.AgentDecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    miki.AgentCurrency = SettingsLoginInfo.Currency;
                    miki.AgentExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    miki.AgentDecimalPoint = SettingsLoginInfo.DecimalValue;
                }
               
                src.Result = miki.GetHotelAvailability(hReqMiki, markup, markupType,string.Empty);
                // resizing according to search result count
                HotelSearchResult[] finalResult = new HotelSearchResult[src.Result.Length];
                finalResult = src.Result;
                Array.Sort(finalResult, delegate(HotelSearchResult h1, HotelSearchResult h2) { return h1.TotalPrice.CompareTo(h2.TotalPrice); });
                int showResultCount = Configuration.ConfigurationSystem.ResultCountBySources["Miki"];
                if (src.Result.Length > showResultCount)
                {
                    Array.Resize(ref finalResult, showResultCount);
                }
                src.Result = finalResult;
            }
            catch (WebException ex)
            {
                src.Error = ex.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultMiki :WebException for Miki = " + ex.Message);
                Audit.Add(EventType.MikiAvailSearch, Severity.High, 0, "Web Exception returned from Miki. Error Message:" + ex.Message + " | " + DateTime.Now + "| city =" + hReqMiki.CityCode + " | checkin Date =" + hReqMiki.StartDate, "");
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchResultMiki :Exception for Miki = " + excep.Message);
                Audit.Add(EventType.MikiAvailSearch, Severity.High, 0, "Exception returned from Miki. Error Message:" + excep.Message + " | " + DateTime.Now + "| city =" + hReqMiki.CityCode + " | checkin Date =" + hReqMiki.StartDate, "");
            }
            hotelSearchResBySrc.Add(src);
            eventFlag[(int)eventNumber].Set();
            Trace.TraceInformation("MetaSearchEngine.SearchResultMiki Exit");
        }

        /// <summary>
        /// TBOHotel Search Results   Added by brahmam 16.04.2016
        /// </summary>
        /// <param name="eventNumber"></param>
        private void SearchResultTBOHotel(object eventNumber)
        {
            //Audit.Add(EventType.TBOHotelAvailSearch, Severity.Normal, 1, "Entered SearchResultTBOHotel", "0");
            HotelSearchResultBySources src = new HotelSearchResultBySources();
            src.Source = "TBOHotel";
            HotelRequest hReqTBO = new HotelRequest();
            hReqTBO = hotelRequest.CopyByValue();
            try
            {

                if (hCity.TBOCode != null && hCity.TBOCode.Length > 0)
                {
                    hReqTBO.CityCode = hCity.TBOCode;
                    hReqTBO.CityId = Convert.ToInt32(hCity.CityId);
                    hReqTBO.Currency = Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"];
                    hReqTBO.CountryName = hCity.CountryName;
                }
                else
                {
                    eventFlag[(int)eventNumber].Set();
                    Audit.Add(EventType.TBOHotelAvailSearch, Severity.High, 0, "MetaSearchEngine.SearchResultTBOHotel : City code not Found for CountryName = " + this.hotelRequest.CountryName + " and CityName = " + this.hotelRequest.CityName, string.Empty);
                    return;
                }

                Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultGTA : City code Found for CountryName = " + hReqTBO.CountryName + " and CityId = " + hReqTBO.CityCode, "1");
                DOTWCountry dotw = new DOTWCountry();
                Dictionary<string, string> Countries = dotw.GetAllCountries();
                hReqTBO.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[hReqTBO.PassengerNationality]);
                hReqTBO.CountryName = Country.GetCountryCodeFromCountryName(hReqTBO.CountryName);
                TBOHotel.HotelV10 tboApi = new TBOHotel.HotelV10();
                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    tboApi.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    tboApi.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    tboApi.DecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    tboApi.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    tboApi.AgentCurrency = SettingsLoginInfo.Currency;
                    tboApi.DecimalPoint = SettingsLoginInfo.DecimalValue;
                }
                DataTable dtMarkup = UpdateMarkup.Load((int)agencyId, HotelBookingSource.TBOHotel.ToString(), (int)ProductType.Hotel);
                DataView dv = dtMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2B')";
                dtMarkup = dv.ToTable();
                decimal markup = 0; string markupType = string.Empty;

                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                }
                src.Result = tboApi.GetHotelSearch(hReqTBO, markup, markupType);
                HotelSearchResult[] finalResult = new HotelSearchResult[src.Result.Length];
                finalResult = src.Result;
                if (src.Result.Length > 0)
                {
                    Array.Sort(finalResult, delegate(HotelSearchResult h1, HotelSearchResult h2) { return h1.TotalPrice.CompareTo(h2.TotalPrice); });
                }
                int showResultCount = ConfigurationSystem.ResultCountBySources["TBOHotel"];
                if (src.Result.Length > showResultCount)
                {
                    Array.Resize<HotelSearchResult>(ref finalResult, showResultCount);
                }
                src.Result = finalResult;
            }
            catch (WebException ex)
            {
                src.Error = ex.Message;
                Audit.Add(EventType.Search, Severity.High, 0, "Web Exception returned from TBOHotel. Error Message:" + ex.Message + " | " + DateTime.Now + "| city =" + hReqTBO.CityCode + " | checkin Date =" + hReqTBO.StartDate, "");
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                CT.Core.Audit.Add(CT.Core.EventType.TBOHotelAvailSearch, CT.Core.Severity.High, 0, "Exception returned from TBOHotel. Error Message:" + excep.Message + " | " + DateTime.Now + "| city =" + hReqTBO.CityCode + " | checkin Date =" + hReqTBO.StartDate, "");
            }
            this.hotelSearchResBySrc.Add(src);
            this.eventFlag[(int)eventNumber].Set();
        }

        /// <summary>
        /// WhiteSandsHotels GetSearchResults
        /// </summary>
        /// <param name="eventNumber"></param>
        private void SearchResultWST(object eventNumber)
        {
            Trace.TraceInformation("MetaSearchEngine.SearchResultWSTApi Enter");
            //WST DBG
            Audit.Add(EventType.WSTAvailSearch, Severity.Normal, 1, "Entered SearchResultWSTApi", "0");
            HotelSearchResultBySources src = new HotelSearchResultBySources();
            src.Source = "WST";
            //this hReqWST is for only WST since the city code changes for the source
            //copy by value the object variable to resolve the reference update of city code
            HotelRequest hReqWST = new HotelRequest();
            hReqWST = hotelRequest.CopyByValue();

            try
            {
                //get the city code for WST
                if (hCity.WSTCode != null && hCity.WSTCode.Length > 0)
                {
                    hReqWST.CityCode = hCity.WSTCode;
                    hReqWST.CityId = Convert.ToInt32(hCity.CityId);
                    hReqWST.Currency = Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"];
                    hReqWST.CountryName = hCity.CountryName;
                }
                else
                {
                    eventFlag[(int)eventNumber].Set();//the thread is no longer use
                    Audit.Add(EventType.WSTAvailSearch, Severity.High, 0, "MetaSearchEngine.SearchResultWST : City code not Found for CountryName = " + hotelRequest.CountryName + " and CityName = " + hotelRequest.CityName, string.Empty);
                    return;//there will no WST call if the city code not found for the selected city.
                }
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, 0, "MetaSearchEngine.SearchResultWST : City code Found for CountryName = " + hReqWST.CountryName + " and CityId = " + hReqWST.CityCode, "1");
                DOTWCountry dotw = new DOTWCountry();
                Dictionary<string, string> Countries = dotw.GetAllCountries();
                hReqWST.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[hReqWST.PassengerNationality]);

                CT.BookingEngine.GDS.WST wstApi = new CT.BookingEngine.GDS.WST();
                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    wstApi.AgentExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    wstApi.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    wstApi.AgentDecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    wstApi.AgentExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    wstApi.AgentCurrency = SettingsLoginInfo.Currency;
                    wstApi.AgentDecimalPoint = SettingsLoginInfo.DecimalValue;
                }
                //assign the current session to the WST search
                DataTable dtMarkup = UpdateMarkup.Load((int)agencyId, HotelBookingSource.WST.ToString(), (int)ProductType.Hotel);
                DataView dv = dtMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2B')";
                dtMarkup = dv.ToTable();
                decimal markup = 0; string markupType = string.Empty;

                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                }
                src.Result = wstApi.GetHotelAvailability(hReqWST, markup, markupType);
                // resizing according to search result count
                HotelSearchResult[] finalResult = new HotelSearchResult[src.Result.Length];
                finalResult = src.Result;
                int showResultCount = Configuration.ConfigurationSystem.ResultCountBySources["WST"];
                if (src.Result.Length > showResultCount)
                {
                    Array.Resize(ref finalResult, showResultCount);
                }
                src.Result = finalResult;
            }
            catch (WebException ex)
            {
                src.Error = ex.Message;
                CT.Core.Audit.Add(CT.Core.EventType.WSTAvailSearch, CT.Core.Severity.High, 0, "Web Exception returned from WST. Error Message:" + ex.Message + " | " + DateTime.Now + "| city =" + hReqWST.CityCode + " | checkin Date =" + hReqWST.StartDate, "");
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                CT.Core.Audit.Add(CT.Core.EventType.WSTAvailSearch, CT.Core.Severity.High, 0, "Exception returned from WST. Error Message:" + excep.Message + " | " + DateTime.Now + "| city =" + hReqWST.CityCode + " | checkin Date =" + hReqWST.StartDate, "");
            }
            hotelSearchResBySrc.Add(src);
            eventFlag[(int)eventNumber].Set();
        }


        /// <summary>
        /// combine all results from different hotel sources
        /// </summary>
        /// <returns></returns>
        private CT.BookingEngine.HotelSearchResult[] CombineHotelSources()
        {
            Trace.TraceInformation("MetaSearchEngine.CombineHotelSources Enter");
            int totalRes = 0;
            for (int i = 0; i < hotelSearchResBySrc.Count; i++)
            {
                if (hotelSearchResBySrc[i].Result != null)
                {
                    totalRes = totalRes + hotelSearchResBySrc[i].Result.Length;
                }
            }

            CT.BookingEngine.HotelSearchResult[] finalResult = new CT.BookingEngine.HotelSearchResult[totalRes];
            int count = 0;

            for (int i = 0; i < hotelSearchResBySrc.Count; i++)
            {
                if (hotelSearchResBySrc[i].Result != null)
                {
                    for (int k = 0; k < hotelSearchResBySrc[i].Result.Length; k++)
                    {
                        int j = k + count;
                        finalResult[j] = hotelSearchResBySrc[i].Result[k];
                    }
                    count = count + hotelSearchResBySrc[i].Result.Length;
                }
            }
            //THIS CODE HAVE BEEN MOVED FROM HERE TO HOTELSEARCHRESULT.CS TO SAVE FOR FILTERING
            ////since all the sources does not give price sorted results
            ////rate of exchange to compare the price in INR
            //StaticData staticInfo = new StaticData();
            //Dictionary<string, decimal> rateOfEx = staticInfo.CurrencyROE;
            //for (int n = 0; n < finalResult.Length - 1; n++)
            //{
            //    for (int m = n + 1; m < finalResult.Length; m++)
            //    {
            //        decimal exRateN = 1, exRateM = 1;
            //        if (rateOfEx.ContainsKey(finalResult[n].Currency))
            //        {
            //            exRateN = rateOfEx[finalResult[n].Currency];
            //        }
            //        if (rateOfEx.ContainsKey(finalResult[m].Currency))
            //        {
            //            exRateM = rateOfEx[finalResult[m].Currency];
            //        }
            //        decimal priceN = finalResult[n].RoomDetails[0].TotalPrice * exRateN;
            //        decimal priceM = finalResult[m].RoomDetails[0].TotalPrice * exRateM;
            //        if (priceN > priceM)
            //        {
            //            HotelSearchResult temp = finalResult[n];
            //            finalResult[n] = finalResult[m];
            //            finalResult[m] = temp;
            //        }
            //    }
            //}

            Trace.TraceInformation("MetaSearchEngine.CombineHotelSources Exit");
            return finalResult;
        }

        #endregion
        
        #region Book
       /* public BookingResponse Book(ref FlightItinerary itinerary, int agencyId, BookingStatus status, long member)
        {
            return Book(ref itinerary, agencyId, status, member, false, BookingType.PowerBook, BookingMode.Auto);
        }
        public BookingResponse Book(ref FlightItinerary itinerary, int agencyId, BookingStatus status, long member, BookingMode bookingMode)
        {
            return Book(ref itinerary, agencyId, status, member, false, BookingType.PowerBook, bookingMode);
        }
        public BookingResponse Book(ref FlightItinerary itinerary, int agencyId, BookingStatus status, long member, BookingMode bookingMode, string bookRequestXML)
        {
            return Book(ref itinerary, agencyId, status, member, false, BookingType.PowerBook, bookingMode, bookRequestXML);
        }
        public BookingResponse Book(ref FlightItinerary itinerary, int agencyId, BookingStatus status, long member, bool avbBook)
        {
            return Book(ref itinerary, agencyId, status, member, avbBook, BookingType.PowerBook, BookingMode.Auto);
        }
        public BookingResponse Book(ref FlightItinerary itinerary, int agencyId, BookingStatus status, long member, bool avbBook, BookingType type)
        {
            return Book(ref itinerary, agencyId, status, member, avbBook, type, BookingMode.Auto);
        }
        public BookingResponse Book(ref FlightItinerary itinerary, int agencyId, BookingStatus status, long member, bool avbBook, string ipAddr, Dictionary<string, string> ticketData)
        {
            return Book(ref itinerary, agencyId, status, member, avbBook, BookingType.PowerBook, BookingMode.Auto, string.Empty, ipAddr, ticketData);
        }*/
        public BookingResponse Book(ref FlightItinerary itinerary, int agencyId, BookingStatus status, UserMaster member, bool avbBook, string ipAddr, Dictionary<string, string> ticketData)
        {
            //ticketData ticket = new Ticket();
            return Book(ref itinerary, agencyId, status, member, avbBook, BookingType.PowerBook, BookingMode.Auto, string.Empty, ipAddr, ticketData);

        }
        public BookingResponse Book(ref FlightItinerary itinerary, int agencyId, BookingStatus status, UserMaster member, bool avbBook, string ipAddr)
        {
            Dictionary<string, string> ticketData = new Dictionary<string, string>();
            return Book(ref itinerary, agencyId, status, member, avbBook, BookingType.PowerBook, BookingMode.Auto, string.Empty, ipAddr, ticketData);
        }
        

        public TicketingResponse Ticket(string pnr, UserMaster member, string corporateCode, string ipAddr)
        {
            Dictionary<string, string> ticketData = new Dictionary<string, string>();
            ticketData.Add("corporateCode", corporateCode);
            return Ticket(pnr, member, ticketData, ipAddr);
        }
        public TicketingResponse Ticket(string pnr, UserMaster member, Dictionary<string, string> ticketData, string ipAddr)
        {
            TicketingResponse ticketingResponse = new TicketingResponse();  // Response generated after ticketing.
            FlightItinerary itineraryDB = null;
            try
            {
                if (pnr == null || pnr.Length == 0)
                {
                    throw new ArgumentException("PNR must be given a value.");
                }
                itineraryDB = new FlightItinerary(FlightItinerary.GetFlightId(pnr)); // Loading itinerary from Database.
                List<string> barredStatus = new List<string>();
                barredStatus.AddRange(new string[] { "WL", "RQ", "HX", "LL", "HL", "UC", "KL" });
                bool allowed = false;   // Flag that sets to true if Autoticketing is allowed to the logged in user.
                string errorMessage = string.Empty; // String to store error message.
                bool isTicketedDB = false;  // Flag that sets to true if ticket for all the pax exist in DB.
                bool inProgress = false;    // Flag that sets to true if ticket for any of the pax exist in DB.
                // Incrementing E-Ticket Hit.
                itineraryDB.LastModifiedBy = (int)member.ID;
                itineraryDB.IncrementETicketHit();
                // Loading booking from database.
                BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(itineraryDB.FlightId));

                bool isDomestic = itineraryDB.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
                bool isAgent = member.AgentId > 1;// agency
                // checking if the autoticketing is allowed for this itinerary.
                // Domestic auto ticketing and International autoticketing are two different task. User may not be allowed for both.
                if (isAgent ? booking.AgencyId == member.AgentId : true)
                {
                    allowed = true;
                    //if (isDomestic)
                    //{
                    //    allowed = Role.IsAllowedTask(member.RoleId, (int)Task.DomesticAutoTicketing);
                    //}
                    //else
                    //{
                    //    allowed = Role.IsAllowedTask(member.RoleId, (int)Task.InternationalAutoTicketing);
                    //}
                }

                // Checking if each segment of the flight is E-Ticket eligible.
                BookingMode bookingMode = Product.GetBookingMode(itineraryDB.FlightId, ProductType.Flight);
                if (allowed)
                {
                    if (booking.Status != BookingStatus.Ready && booking.Status != BookingStatus.InProgress && booking.Status != BookingStatus.OutsidePurchase)
                    {
                        allowed = false;
                        errorMessage = "Auto Ticketing not allowed for the current status of the booking.";
                    }
                    if (allowed)
                    {
                        for (int i = 0; i < itineraryDB.Segments.Length; i++)
                        {
                            if (!itineraryDB.Segments[i].ETicketEligible)
                            {
                                errorMessage = "Segment " + itineraryDB.Segments[i].Origin.CityCode + "-" + itineraryDB.Segments[i].Destination.CityCode;
                                errorMessage = " is not eligible for E-Ticket.";
                                allowed = false;
                                break;
                            }
                            if (barredStatus.Contains(itineraryDB.Segments[i].Status))
                            {
                                allowed = false;
                                errorMessage = "Segment " + itineraryDB.Segments[i].Origin.CityCode + "-" + itineraryDB.Segments[i].Destination.CityCode;
                                errorMessage += " has status " + itineraryDB.Segments[i].Status + " which is not allowed for ticketing.";
                                break;
                            }
                        }
                        if (allowed)
                        {
                            for (int i = 0; i < itineraryDB.Segments.Length; i++)
                            {
                                Airline airline = new Airline();
                                airline.Load(itineraryDB.Segments[i].Airline);
                                if (!airline.ETicketEligible)
                                {
                                    errorMessage = "Auto ticketing is not allowed for airline " + itineraryDB.Segments[i].Airline + ".";
                                    allowed = false;
                                    break;
                                }
                                if (!airline.ETicketForImportPNR && bookingMode == BookingMode.Import)
                                {
                                    errorMessage = "Auto ticketing is not allowed for airline " + itineraryDB.Segments[i].Airline + ", for PNRs imported.";
                                    allowed = false;
                                    break;
                                }
                                //if (itineraryDB.Segments[i].Airline == "AI" && !itineraryDB.IsDomestic)
                                //{
                                //    errorMessage = "Auto ticketing is not allowed for AI International.";
                                //    allowed = false;
                                //    break;
                                //}
                                //if (itineraryDB.Segments[i].Airline == "9W" && !itineraryDB.IsDomestic)
                                //{
                                //    errorMessage = "Auto ticketing is not allowed for 9W International.";
                                //    allowed = false;
                                //    break;
                                //} ziyad
                            }
                            if (allowed)
                            {
                                List<Ticket> ticketList = BookingEngine.Ticket.GetTicketList(FlightItinerary.GetFlightId(itineraryDB.PNR));
                                if (FlightItinerary.IsTicketed(itineraryDB, ticketList))
                                {
                                    allowed = false;
                                    errorMessage = "Already ticketed.";
                                    isTicketedDB = true;
                                }
                                else if (ticketList.Count > 0)
                                {
                                    allowed = false;
                                    errorMessage = "Ticketing already in progress.";
                                    inProgress = true;
                                }
                            }
                        } // else: error message already generated.
                    }
                }
                // Task not allowed.
                else
                {
                    errorMessage = "No access for AutoTicketing.";
                }
                AgentMaster agent = new AgentMaster(itineraryDB.AgencyId);
                if (allowed // Just to prevent further checks if not allowed
                    && itineraryDB.BookingMode != BookingMode.WhiteLabel    // credit check is not applicable for WL booking
                    && itineraryDB.BookingMode != BookingMode.Itimes    // credit check is not applicable for iTimes booking
                    //&& (isAgent || !Role.IsAllowedTask(member.RoleId, (int)Task.OverrideCreditLimit))    // excluding the admin who can override credit
                    && (itineraryDB.AgentPrice > agent.CurrentBalance && itineraryDB.TransactionType == "B2B"))
                {
                    allowed = false;
                    errorMessage = "Agency do not have enough balance.";
                }

                // proceed if allowed.
                if (allowed)
                {
                    //if (itineraryDB.FlightBookingSource == BookingSource.WorldSpan)
                    //{
                    //    ticketingResponse = TicketWS(itineraryDB, booking, member, ticketData, ipAddr);
                    //}
                    //else if (itineraryDB.FlightBookingSource == BookingSource.Amadeus)
                    //{
                    //    ticketingResponse = TicketAM(itineraryDB, booking, member, ticketData, ipAddr);
                    //}
                    //else if (itineraryDB.FlightBookingSource == BookingSource.Galileo)
                    //{
                    //    ticketingResponse = TicketGL(itineraryDB, booking, member, ticketData, ipAddr);
                    //}
                    if (itineraryDB.FlightBookingSource == BookingSource.UAPI)
                    {
                        ticketingResponse = TicketUAPI(itineraryDB, booking, member, ticketData, ipAddr);
                    }
                    else if (itineraryDB.FlightBookingSource == BookingSource.TBOAir)
                    {
                        AirV10 tbo = new AirV10();

                        SourceDetails agentDetails = new SourceDetails();

                        if (SettingsLoginInfo.IsOnBehalfOfAgent)
                        {
                            tbo.AgentBaseCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                            tbo.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                            tbo.AgentDecimalValue = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                            agentDetails = SettingsLoginInfo.OnBehalfAgentSourceCredentials["TA"];
                        }
                        else
                        {
                            tbo.AgentBaseCurrency = SettingsLoginInfo.Currency;
                            tbo.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                            tbo.AgentDecimalValue = SettingsLoginInfo.DecimalValue;
                            agentDetails = SettingsLoginInfo.AgentSourceCredentials["TA"];
                        }

                        if (System.Web.HttpContext.Current.Session["FlightItinerary"] != null)
                        {
                            FlightItinerary itinerary = System.Web.HttpContext.Current.Session["FlightItinerary"] as FlightItinerary;

                            itineraryDB.TBOFareBreakdown = itinerary.TBOFareBreakdown;
                            itineraryDB.Passenger = itinerary.Passenger;
                        }
                        //tbo.LoginName = agentDetails.UserID;
                        //tbo.Password = agentDetails.Password;
                        CT.BookingEngine.Ticket[] tickets = new Ticket[0];
                        ticketingResponse = tbo.CreateTicket(ref itineraryDB, out tickets);

                        if (ticketingResponse.Status == TicketingResponseStatus.Successful)
                        {
                            Audit.Add(EventType.TBOAir, Severity.Low, 0, "(TBO Air)Saving Tickets : " + itineraryDB.PNR, HttpContext.Current.Request["REMOTE_ADDR"]);

                            SaveTicketInfo(ref tickets, itineraryDB, itineraryDB, booking.CreatedBy, booking.AgencyId, booking.BookingId, ticketData, HttpContext.Current.Request["REMOTE_ADDR"]);

                            Audit.Add(EventType.TBOAir, Severity.Low, 0, "(TBO Air)Saved Tickets successfully", HttpContext.Current.Request["REMOTE_ADDR"]);

                            booking.Unlock(BookingStatus.Ticketed, (int)member.ID);
                        }
                    }
                }
                else
                {

                    if (isTicketedDB)
                    {
                        ticketingResponse.Status = TicketingResponseStatus.TicketedDB;
                    }
                    else if (inProgress)
                    {
                        ticketingResponse.Status = TicketingResponseStatus.InProgress;
                    }
                    else
                    {
                        ticketingResponse.Status = TicketingResponseStatus.NotAllowed;
                    }
                    ticketingResponse.Message = errorMessage;
                    ticketingResponse.PNR = pnr;
                }
            }
            catch (Exception ex)
            {
                #region Handling booking save failure
                // Email - In case of Booking created successfully   

                AgentMaster bookingAgency = new AgentMaster(agencyId);
                //UserMaster userMember = new UserMaster(member);
                string messageText = "PNR = " + ticketingResponse.PNR + "\r\n Booking Source is " + itineraryDB.FlightBookingSource.ToString() + "\r\n Agency Name is " + bookingAgency.Name + "\r\n AgencyId is " + bookingAgency.ID + "\r\n MemberId is " + (int)member.ID + "\r\n Member Name is " + member.FirstName + member.LastName + "\r\n Member Email Address is " + member.Email + "\r\n. PNR created successfully but failed to save.";
                string message = Util.GetExceptionInformation(ex, "Booking save failed.\r\n" + messageText);

                //if (bookingResponse.PNR != null && bookingResponse.PNR.Length > 0) //&& isLcc removed sai
                {
                    itineraryDB.FlightId = 0;
                    for (int i = 0; i < itineraryDB.Passenger.Length; i++)
                    {
                        itineraryDB.Passenger[i].PaxId = 0;
                        itineraryDB.Passenger[i].Price.PriceId = 0;
                    }
                    for (int i = 0; i < itineraryDB.Segments.Length; i++)
                    {
                        itineraryDB.Segments[i].SegmentId = 0;
                    }
                    FailedBooking fb = new FailedBooking();
                    fb.Product = ProductType.Flight;
                    fb.PNR = (ticketingResponse.PNR != null ? ticketingResponse.PNR : pnr);
                    fb.AirLocatorCode = itineraryDB.AirLocatorCode;
                    fb.UniversalRecord = itineraryDB.UniversalRecord;
                    fb.MemberId = (int)member.ID;
                    fb.AgencyId = (int)bookingAgency.ID;
                    fb.AgencyName = bookingAgency.Name;
                    fb.CurretnStatus = Status.NotSaved;
                    fb.Source = (BookingSource)Enum.Parse(typeof(BookingSource), itineraryDB.FlightBookingSource.ToString());
                    try
                    {
                        fb.ItineraryXML = FailedBooking.GetXML(itineraryDB);
                        fb.Save();
                    }
                    catch { }
                }

                try
                {
                    ticketingResponse.Status = TicketingResponseStatus.NotCreated;
                    SendMail(itineraryDB, member, (ticketingResponse.Status == TicketingResponseStatus.InProgress ? true : false), message, ticketingResponse.Status);
                }
                catch (System.Net.Mail.SmtpException) { } // Do nothing

                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)member.ID, "Save booking failed. Error : " + ex.Message, "0");
                throw ex;
                #endregion
                throw ex;
            }
            return ticketingResponse;
        }

        private TicketingResponse TicketUAPI(FlightItinerary itineraryDB, BookingDetail booking, UserMaster member, Dictionary<string, string> ticketData, string ipAddr)
        {
            // Applying Credentials
            SourceDetails agentDetails = new SourceDetails();

            if (SettingsLoginInfo.IsOnBehalfOfAgent)
            {
                agentDetails = SettingsLoginInfo.OnBehalfAgentSourceCredentials["UA"];
                UAPI.agentBaseCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                UAPI.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
            }
            else
            {
                agentDetails = SettingsLoginInfo.AgentSourceCredentials["UA"];
                UAPI.agentBaseCurrency = SettingsLoginInfo.Currency;
                UAPI.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
            }

            UAPI.UserName = agentDetails.UserID;
            UAPI.Password = agentDetails.Password;
            UAPI.TargetBranch = agentDetails.HAP;
            //
            string errorMessage = string.Empty; // String to store error message.
            TicketingResponse ticketingResponse = new TicketingResponse();  // Response generated after ticketing.
            Ticket[] ticket = new Ticket[0];
            bool autoTicketSuccessful = false;
            FlightItinerary itineraryUAPI = new FlightItinerary();

            AgentMaster agency = new AgentMaster(booking.AgencyId);

            // Checking Lock status. Proceed if not locked by others (either not locked or locked by logged in user).
            if (booking.LockedBy == 0 || booking.LockedBy == (int)member.ID)
            {
                // Retrieving itinerary information from Amadeus. It also loads e-Ticket if it exists.
                bool itineraryRetrieved = false;
                try
                {

                    itineraryUAPI = UAPI.RetrieveItinerary(itineraryDB.PNR, out ticket);
                    itineraryRetrieved = true;
                }
                catch (BookingEngineException ex)
                {
                    throw ex;
                    //GeneralError((Exception)ex, itineraryDB.PNR, ipAddr, member.MemberId, booking.AgencyId);
                }
                itineraryUAPI.Origin = itineraryDB.Origin;
                itineraryUAPI.Destination = itineraryDB.Destination;
                // Checking if Ticket already exist. 
                // In case of partially ticketed pnr autoticketing will not proceed.
                // Partially ticketed means there are more than one pax and ticketing is done for at least one of them but not for all.
                if (itineraryUAPI.Ticketed)
                {
                    // TODO: Change the status TicketedWS to Ticketed GDS.
                    ticketingResponse.Status = TicketingResponseStatus.TicketedWS;
                    ticketingResponse.Message = "Already Ticketed. Please refresh PNR.";
                }
                // Ticketing is not done.
                else if (itineraryRetrieved)
                {
                    bool autoTicketDone = false;    // flag indicating if the Autoticketing is successful.
                    try
                    {
                        //using (TransactionScope autoTicketScope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 2, 0)))
                        //{
                        // locking the booking for currently logged in member.
                        booking.Lock(BookingStatus.InProgress, (int)member.ID);

                        //TicketingResponse tResponse = UAPI.Ticket(itineraryDB, (int)member.ID, ticketData, ipAddr);
                        //autoTicketDone = tResponse.Status == TicketingResponseStatus.Successful;
                        ticketingResponse  = UAPI.Ticket(itineraryDB, (int)member.ID, ticketData, ipAddr);
                        autoTicketDone = ticketingResponse.Status == TicketingResponseStatus.Successful;

                        bool partialTicketRetrieved = false;
                        if (autoTicketDone)// ticketing is failed
                        {
                            // Retrieving itinerary from Amadeus after autoticketing is successful.
                            // This will also retrieve the list of tickets generated.
                            itineraryUAPI = UAPI.RetrieveItinerary(itineraryDB, out ticket);

                            //  in case retrive fails ticket length is 0 and we have nothing to save.
                            if (ticket.Length > 0)
                            {
                                if (ticket.Length < itineraryDB.Passenger.Length)
                                {
                                    partialTicketRetrieved = true;
                                }
                                itineraryUAPI.FlightId = itineraryDB.FlightId;
                                bool txnSucceded = false;
                                try
                                {
                                    //System.IO.File.AppendAllText(timeLogFile, "\r\nTicket,\t" + DateTime.Now.ToString("HH:mm:ss.ffffff") + ",\t");
                                    int txnRetryCount = Convert.ToInt32(ConfigurationSystem.BookingEngineConfig["txnRetryCount"]);
                                    int numberRetry = 0;
                                    //while (!txnSucceded && numberRetry < txnRetryCount)
                                    //{
                                    //    numberRetry++;
                                    //    try
                                    //    {
                                    //        using (TransactionScope autoTicketScope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 2, 0)))
                                    //        {
                                    try
                                    {
                                        SaveTicketInfo(ref ticket, itineraryDB, itineraryDB, (int)member.ID, booking.AgencyId, booking.BookingId, ticketData, ipAddr);       // Saving Ticket information.
                                    }
                                    catch{
                                        Audit.Add(EventType.Ticketing, Severity.High,(int) member.ID, "Error in Save Ticket info. PNR : " + itineraryDB.PNR + " Source : " + itineraryDB.FlightBookingSource.ToString(), "0");
                                        throw;

                                    }

                                    //        autoTicketScope.Complete();
                                    txnSucceded = true;
                                    //    }
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    if (numberRetry < txnRetryCount)
                                    //    {
                                    //        for (int i = 0; i < ticket.Length; i++)
                                    //        {
                                    //            ticket[i].TicketId = 0;
                                    //        }
                                    //        continue;
                                    //    }
                                    //    else
                                    //    {
                                    //        throw ex;
                                    //    }
                                    //}
                                    //}
                                    if (!partialTicketRetrieved && txnSucceded && (itineraryDB.BookingMode != BookingMode.Itimes || Convert.ToBoolean(ConfigurationSystem.BookingEngineConfig["invoiceForItimes"])))
                                    {
                                        // Raising invoice for the ticket.
                                        try
                                        {
                                            
                                            //int invoiceNumber = AccountUtility.RaiseInvoice(itineraryDB, string.Empty, member.ID);
                                            if (itineraryDB.PaymentMode == ModeOfPayment.CreditCard)
                                            {
                                                // Invoice status is updated inside MakeCCPaymentEntry method.
                                                //MakeCCPaymentEntry(booking, itineraryDB, member, invoiceNumber);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            string messageText = "PNR = " + itineraryDB.PNR + "\r\n Booking Source is " + itineraryDB.FlightBookingSource.ToString() + "\r\n Agency Name is " + agency.Name + "\r\n AgencyId is " + agency.ID + "\r\n MemberId is " + member.ID + "\r\n Member Name is " + member.FirstName + member.LastName + "\r\n Member Email Address is " + member.Email + "\r\n. Ticket created successfully but failed to save Invoice";
                                            string message = Util.GetExceptionInformation(ex, "Invoice save failed.\r\n" + messageText);
                                            try
                                            {
                                                Email.Send("Invoice save failed", message);
                                            }
                                            catch (System.Net.Mail.SmtpException) { } // Do nothing                                
                                            Audit.Add(EventType.Exception, Severity.High, (int)member.ID, "Save Invoice failed. Error : " + ex.Message, "0");
                                        }
                                    }
                                    //System.IO.File.AppendAllText(timeLogFile, DateTime.Now.ToString("HH:mm:ss.ffffff") + " " + numberRetry.ToString());
                                    autoTicketSuccessful = true;    // All the steps of auto ticketing went succesful if this point is reached.
                                    ticketingResponse.Status = TicketingResponseStatus.Successful;
                                    ticketingResponse.Message = string.Empty;
                                }
                                // Exception should not occure at this point. But if somehow save fails admin needs to be aware of it.
                                catch (Exception ex)
                                {
                                    AgentMaster bookingAgency = new AgentMaster(booking.AgencyId);
                                    // Sending mail to admin and generating error when save fails after Ticketing is done.
                                    autoTicketSuccessful = true;
                                    ticketingResponse.Status = TicketingResponseStatus.NotSaved;
                                    booking.SetBookingStatus(BookingStatus.Ticketed, (int)member.ID);
                                    itineraryDB.ProductType = ProductType.Flight;
                                    FailedBooking fb = new FailedBooking();
                                    fb.Product = ProductType.Flight; // sai
                                    fb.PNR = itineraryDB.PNR;
                                    //-----For retrieving in FailedBooking Queue------
                                    fb.AirLocatorCode = itineraryDB.AirLocatorCode;
                                    fb.UniversalRecord = itineraryDB.UniversalRecord;
                                    //------------------------------------------------
                                    fb.MemberId = (int)member.ID;
                                    fb.AgencyId = (int)bookingAgency.ID;
                                    fb.AgencyName = bookingAgency.Name;
                                    fb.CurretnStatus = Status.NotSaved;
                                    fb.Source = (BookingSource)Enum.Parse(typeof(BookingSource), itineraryDB.FlightBookingSource.ToString());
                                    fb.ItineraryXML = FailedBooking.GetXML(itineraryDB);
                                    try
                                    {
                                        fb.Save();
                                    }
                                    catch (Exception e)
                                    {
                                        Audit.Add(EventType.Ticketing, Severity.High, (int)member.ID, ex.ToString(), "1");
                                    }



                                    errorMessage = "ETicket generated successfully but could not be saved. Please refresh PNR. PNR = " + itineraryDB.PNR + ".\r\n Agency Name is " + bookingAgency.Name + "\r\n AgencyId is " + bookingAgency.ID + "\r\n MemberId is " + member.ID + "\r\n Member Name is " + member.FirstName + member.LastName + "\r\n Member Email Address is " + member.Email;
                                    ticketingResponse.Message = errorMessage;
                                    string messageText = Util.GetExceptionInformation(ex, errorMessage);
                                    Audit.Add(EventType.Ticketing, Severity.High, (int)member.ID, messageText, ipAddr);
                                    try
                                    {
                                        SendMail(itineraryDB, member, (ticketingResponse.Status == TicketingResponseStatus.InProgress ? true : false), errorMessage, ticketingResponse.Status);
                                        //Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["ErrorNotificationMailingId"], "Cozmo Flight Error", messageText);
                                    }
                                    catch (Exception)
                                    {
                                        // do nothing. its just for avoiding crash.
                                    }
                                    //AutoTicketView.ActiveViewIndex = 0;
                                    //throw ex;
                                }
                            }
                            else
                            {
                                // Sending mail to admin and generating error when save fails after Ticketing is done.
                                AgentMaster bookingAgency = new AgentMaster(booking.AgencyId);
                                ticketingResponse.Status = TicketingResponseStatus.NotSaved;
                                errorMessage = "ETicket generated successfully but could not be retrieved. Please refresh PNR. PNR = " + itineraryDB.PNR + ".\r\n Agency Name is " + bookingAgency.Name + "\r\n AgencyId is " + bookingAgency.ID + "\r\n MemberId is " + member.ID + "\r\n Member Name is " + member.FirstName + member.LastName + "\r\n Member Email Address is " + member.Email;
                                ticketingResponse.Message = errorMessage;
                                Audit.Add(EventType.Ticketing, Severity.High, (int)member.ID, errorMessage, ipAddr);
                                try
                                {
                                    Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["ErrorNotificationMailingId"], "COZMO Error", "E-Ticket generated but failed to retrieve ticket information from Amadeus. PNR = " + itineraryDB.PNR);
                                }
                                catch (Exception)
                                {
                                    // do nothing. its just for avoiding crash.
                                }
                            }
                        }
                        else
                        {
                            Audit.Add(EventType.Ticketing, Severity.High, (int)member.ID, errorMessage + itineraryDB.PNR, ipAddr);
                            //ticketingResponse.Message = errorMessage;
                            ticketingResponse.Status = TicketingResponseStatus.NotCreated;
                        }
                        // Releasing the lock of the booking.
                        if (autoTicketSuccessful)
                        {
                            if (!partialTicketRetrieved)
                            {
                                booking.Unlock(BookingStatus.Ticketed, (int)member.ID);
                            }
                        }
                        else
                        {
                            // this unlock will restore the status of booking to previous one.
                            booking.Unlock(BookingStatus.Ready, (int)member.ID);
                        }
                        // Committing the Transaction.
                        //    autoTicketScope.Complete();
                        //}
                    }
                    catch (TimeoutException ex)
                    {
                        //GeneralError((Exception)ex, itineraryDB.PNR, ipAddr, member.MemberId, booking.AgencyId);
                        throw ex;
                    }
                    catch (TransactionException ex)
                    {
                        throw ex;
                        //GeneralError((Exception)ex, itineraryDB.PNR, ipAddr, member.MemberId, booking.AgencyId);
                    }
                    catch (BookingEngineException ex)
                    {
                        throw ex;
                        //GeneralError((Exception)ex, itineraryDB.PNR, ipAddr, member.MemberId, booking.AgencyId);
                    }
                    catch (ArgumentException ex)
                    {
                        throw ex;
                        //GeneralError((Exception)ex, itineraryDB.PNR, ipAddr, member.MemberId, booking.AgencyId);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Book, Severity.High, (int)member.ID, ex.Message, "");
                        throw ex;
                        //GeneralError((Exception)ex, itineraryDB.PNR, ipAddr, member.MemberId, booking.AgencyId);
                    }
                }
                // else not needed since errorMessage  is already set if itinerary not retrieved.
            }
            else
            {
                // Generating error if booking is locked by some other user.
                ticketingResponse.Message = "Booking Locked.";
                ticketingResponse.Status = TicketingResponseStatus.NotAllowed;
            }
            return ticketingResponse;
        }

        // Changes for new product - START
        public BookingResponse Book(ref Product product, int agencyId, BookingStatus status, long member)
        {
            return Book(ref product, agencyId, status, member, false, CT.Core.BookingType.PowerBook, BookingMode.Auto);
        }
        public BookingResponse Book(ref Product product, int agencyId, BookingStatus status, long member, BookingMode bookingMode)
        {
            return Book(ref product, agencyId, status, member, false, CT.Core.BookingType.PowerBook, bookingMode);
        }
        public BookingResponse Book(ref Product product, int agencyId, BookingStatus status, long member, bool avbBook)
        {
            return Book(ref product, agencyId, status, member, avbBook, CT.Core.BookingType.PowerBook, BookingMode.Auto);
        }
        public BookingResponse Book(ref Product product, int agencyId, BookingStatus status, long member, bool avbBook, CT.Core.BookingType type)
        {
            return Book(ref product, agencyId, status, member, avbBook, type, BookingMode.Auto);
        }
        // Changes for new product - END

        // Made changes for new product type
        public BookingResponse Book(ref Product product, int agencyId, BookingStatus status, long member, bool avbBook, CT.Core.BookingType type, BookingMode bookingMode) // not using currently
        {
            BookingResponse bookingResponse = new BookingResponse();

            HotelBookingResponse hotelbookingResponse = new HotelBookingResponse();
            TransferBookingResponse transferbookingResponse = new TransferBookingResponse();
            SightseeingBookingResponse sightseeingbookingResponse = new SightseeingBookingResponse();
            List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
            # region Flight Booking
            if (product.ProductType == ProductType.Flight)
            {
                FlightItinerary itinerary = (FlightItinerary)product;

                // TODO: If booking type is not Book or PowerBook - throw exception.
                //bool modeSpicejet = (ConfigurationSystem.SpiceJetConfig["Mode"] == "SpiceJet");
                BookingDetail booking = new BookingDetail();
                //SpiceJetAPI sjObj = new SpiceJetAPI();
                //Navitaire naviObj = new Navitaire("6E");
                //if (itinerary.FlightBookingSource == BookingSource.SpiceJet && modeSpicejet)
                //{
                //    naviObj = new Navitaire("0S");
                //}
                //ParamountApi pmObj = new ParamountApi();
                //Mdlr mdlr = new Mdlr();
                List<string> pmTicketNo = new List<string>();
                //AirDeccanApi adObj = new AirDeccanApi();
                //Sama sama = new Sama();
                // else it goes for "6E"
                int TimeLimitToComplete = 7;
                bool hasInfant = false;

                string titleRegex = "^[a-zA-Z ]{2,4}[.]{0,1}[a-zA-Z]{0,4}[.]{0,1}$";
                string nameRegex = "[a-zA-Z. ]+$";
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    if (itinerary.Passenger[i].Title != null && itinerary.Passenger[i].Title.Trim() != string.Empty && !Regex.IsMatch(itinerary.Passenger[i].Title, titleRegex))
                    {
                        throw new ArgumentException("Invalid title", "title");
                    }
                    if (itinerary.Passenger[i].FirstName != null && !Regex.IsMatch(itinerary.Passenger[i].FirstName, nameRegex))
                    {
                        throw new ArgumentException("Invalid FirstName", "firstName");
                    }
                    if (itinerary.Passenger[i].LastName != null && (!Regex.IsMatch(itinerary.Passenger[i].LastName, nameRegex) || itinerary.Passenger[i].LastName.Trim().Length < 2))
                    {
                        throw new ArgumentException("Invalid LastName", "lastName");
                    }
                }
                // This will be true in case of Spicejet or Indigo.
                bool isLcc = (itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai);

                booking.AgencyId = agencyId;
                booking.CreatedBy = (int)member;
                booking.Status = status;
                itinerary.CreatedBy = (int)member;
                
                if (!isLcc)
                {
                    // Sending itinerary for booking     
                    try
                    {
                        bookingResponse = UAPI.Book(itinerary, sessionId, out ptcDetail);
                    }
                    catch (BookingEngineException)
                    {
                        bookingResponse.Error = "Unable to Book...UAPI";
                        Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                    }
                }
                if (isLcc)
                {
                    try
                    {
                        if (itinerary.FlightBookingSource == BookingSource.AirArabia)
                        {
                            AirArabia aaObj = new AirArabia();
                            if (SettingsLoginInfo.IsOnBehalfOfAgent)
                            {
                                aaObj.UserName = SettingsLoginInfo.OnBehalfAgentSourceCredentials["G9"].UserID;
                                aaObj.Password = SettingsLoginInfo.OnBehalfAgentSourceCredentials["G9"].Password;
                                aaObj.Code = SettingsLoginInfo.OnBehalfAgentSourceCredentials["G9"].HAP;
                                aaObj.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                                aaObj.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                            }
                            else
                            {
                                aaObj.UserName = SettingsLoginInfo.AgentSourceCredentials["G9"].UserID;
                                aaObj.Password = SettingsLoginInfo.AgentSourceCredentials["G9"].Password;
                                aaObj.Code = SettingsLoginInfo.AgentSourceCredentials["G9"].HAP;
                                aaObj.AgentCurrency = SettingsLoginInfo.Currency;
                                aaObj.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                            }
                            aaObj.AppUserId = appUserId.ToString();
                            aaObj.SessionID = sessionId;
                            bookingResponse = aaObj.Booking(itinerary, booking.AgencyId, sessionId);
                        }
                    }
                    // All catches are send to audit from Spice Jet API already, so showing only error message.
                    catch (BookingEngineException excep)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Book, CT.Core.Severity.High, 0, "Booking failed | error = " + excep.Message + "|" + DateTime.Now, "");
                        bookingResponse.Status = BookingResponseStatus.Failed;
                        bookingResponse.Error = "Unable to Book";
                    }
                    catch (System.Net.WebException excep)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Book, CT.Core.Severity.High, 0, "Booking failed | error = " + excep.Message + "|" + DateTime.Now, "");
                        bookingResponse.Status = BookingResponseStatus.Failed;
                        bookingResponse.Error = "Unable to Book";
                    }
                    catch (System.Web.Services.Protocols.SoapException excep)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Book, CT.Core.Severity.High, 0, "Booking failed | error = " + excep.Message + "|" + DateTime.Now, "");
                        bookingResponse.Status = BookingResponseStatus.Failed;
                        bookingResponse.Error = "Unable to Book";
                    }
                }
                // TODO: handle else. Generate error.

                if (bookingResponse.Status != BookingResponseStatus.Failed)
                {
                    // Sending mail to the user.
                    bool isHold = status == BookingStatus.Hold;
                    //SendMail(itinerary, member, isHold);


                    int turnPoint = 0;
                    for (int i = 0; i < itinerary.Segments.Length; i++)
                    {
                        if (itinerary.Destination == itinerary.Segments[i].Destination.CityCode || itinerary.Destination == itinerary.Segments[i].Destination.AirportCode)
                        {
                            turnPoint = i + 1;
                        }
                    }
                    DateTime returnDate = new DateTime();
                    if (itinerary.Segments.Length > turnPoint)
                    {
                        returnDate = itinerary.Segments[turnPoint].DepartureTime;
                    }
                    if (itinerary.FareRules != null)
                    {
                        for (int i = 0; i < itinerary.FareRules.Count; i++)
                        {
                            itinerary.FareRules[i].ReturnDate = returnDate;
                            itinerary.FareRules[i].DepartureTime = itinerary.Segments[0].DepartureTime;
                        }
                    }




                    #region Fetching terminal information for SG and 6E.
                    //if (itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo)
                    //{
                    //    try
                    //    {
                    //        ArrayList naviInfo = new ArrayList();
                    //        //if (itinerary.FlightBookingSource == BookingSource.SpiceJet && modeSpicejet)
                    //        //{
                    //        //    if (modeSpicejet)
                    //        //    {
                    //        //        sjObj = new SpiceJetAPI();
                    //        //        naviInfo = sjObj.DisplayReservation(itinerary.PNR);
                    //        //    }
                    //        //    else
                    //        //    {
                    //        //        naviObj = new Navitaire("0S");
                    //        //        naviInfo = naviObj.DisplayReservation(itinerary.PNR);
                    //        //    }
                    //        //}
                    //        //else
                    //        //{
                    //        //    naviObj = new Navitaire("6E");
                    //        //    naviInfo = naviObj.DisplayReservation(itinerary.PNR);
                    //        //}
                    //        for (int i = 0; i < itinerary.Segments.Length; i++)
                    //        {
                    //            itinerary.Segments[i].DepTerminal = Convert.ToString(naviInfo[i]);
                    //        }
                    //    }
                    //    // Thes exception is catched just to continue the process.
                    //    // Booking should not fail even ev terminal information is not retrieved
                    //    catch (System.Net.WebException) { }
                    //    catch (BookingEngineException) { }
                    //}
                    #endregion

                    // Getting fare Rules. (Also terminal info in case of spicejet or indigo)

                    itinerary.FareRules = GetFareRule(itinerary);

                    if (bookingResponse.Status == BookingResponseStatus.BookedOther)
                    {
                        booking.Status = BookingStatus.Hold;
                        for (int i = 0; i < itinerary.Passenger.Length; i++)
                        {
                            //itinerary.Passenger[i].Price = AccountingEngine.AccountingEngine.GetPrice(itinerary, agencyId, i, 0);// todo Shiva
                        }
                    }
                    //for (int i = 0; i < itinerary.Passenger.Length; i++)
                    //{
                    //    if (itinerary.Passenger[i].Type == PassengerType.Infant)
                    //    {
                    //        hasInfant = true;
                    //        break;
                    //    }
                    //}
                    //if (hasInfant)
                    //{
                    //    for (int i = 0; i < itinerary.Segments.Length; i++)
                    //    {
                    //        itinerary.Segments[i].ETicketEligible = false;
                    //    }
                    //}

                    CT.Core.Queue queue = new CT.Core.Queue();
                    queue.QueueTypeId = (int)QueueType.Booking;
                    queue.StatusId = (int)QueueStatus.Assigned;
                    queue.AssignedTo = (int)member;
                    queue.AssignedBy = (int)member;
                    queue.AssignedDate = DateTime.UtcNow;
                    queue.CompletionDate = DateTime.UtcNow.AddDays(TimeLimitToComplete);
                    queue.CreatedBy = (int)member;

                    //TODO: Ziyad 05 Aug 2013
                    //Comment ssrComment = new Comment();
                    //ssrComment.AgencyId = booking.AgencyId;
                    //ssrComment.CommentDescription = bookingResponse.SSRMessage;
                    //ssrComment.CommentTypes = CommmentType.Booking;
                    //TODO: remove this hard coded 1. 1 is for administrator ID.
                    //ssrComment.CreatedBy = 1;
                    //ssrComment.IsPublic = true;

                    //TODO: For New product -- Start                       
                    booking.ProductsList = new Product[1];
                    booking.ProductsList[0] = (Product)itinerary;
                    //For New product -- End

                    // Saving the booked itinerary to database
                    //TODO: what to do if the transaction fails, the booking at GDS is already done.
                    try
                    {
                        using (TransactionScope updateTransaction = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                        {
                            itinerary.BookingMode = bookingMode;
                            itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
                            //itinerary.IsDomestic = itinerary.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
                            //booking.Save(itinerary, false);


                            //For New product -- Start                       
                            booking.SaveBooking(false);
                            //For New product -- End

                            //Booking History
                            BookingHistory bookingHistory = new BookingHistory();
                            bookingHistory.BookingId = booking.BookingId;
                            string remarks = string.Empty;
                            bookingHistory.EventCategory = EventCategory.Booking;
                            if (booking.Status == BookingStatus.Hold)
                            {
                                remarks = "Booking is Hold";
                            }
                            else if (booking.Status == BookingStatus.Cancelled)
                            {
                                remarks = "Seat is Released";
                            }
                            else if (booking.Status == BookingStatus.Ticketed)
                            {
                                bookingHistory.EventCategory = EventCategory.Ticketing;
                                remarks = "Booking status is Ticketed";
                            }
                            else if (booking.Status == BookingStatus.Ready)
                            {
                                remarks = "Booking is Ready";
                            }
                            else if (booking.Status == BookingStatus.Inactive)
                            {
                                bookingHistory.EventCategory = EventCategory.Booking;
                                remarks = "Booking is Inactive";
                            }
                            else
                            {
                                bookingHistory.EventCategory = EventCategory.Miscellaneous;
                                remarks = booking.Status.ToString();
                            }
                            bookingHistory.Remarks = remarks;
                            bookingHistory.CreatedBy = (int)member;
                            bookingHistory.Save();
                            //TODO: Ziyad 05 Aug 2013
                            //if (bookingResponse.SSRDenied)
                            //{
                            //    ssrComment.ParentId = booking.BookingId;
                            //    ssrComment.AddComment();
                            //}
                            // In the case of spice jet there is no seperate ticketing process.
                            // Once booking is made, it means ticket is generated
                            Airline airline = new Airline();
                            airline.Load(itinerary.Segments[0].Airline);
                            if (airline.IsLCC && isLcc)
                            {
                                SaveTicketInfo(itinerary, (int)member, agencyId, pmTicketNo, booking.BookingId, string.Empty, new Dictionary<string, string>()); // ticket save 
                                //TODO: Ziyad 05 Aug 2013
                                //int invoiceNumber = AccountUtility.RaiseInvoice(itinerary, string.Empty, (int)member);
                                //Invoice.UpdateInvoiceStatus(invoiceNumber, InvoiceStatus.Paid, (int)member);
                            }

                            //If b2b2b agent todo ziyad
                            

                            queue.ItemId = booking.BookingId;
                            queue.Save();
                            updateTransaction.Complete();
                            //try
                            //{
                            //    Thread mailThread = new Thread(MetaSearchEngine.SendLowBalanceMail);
                            //    mailThread.Start(member);
                            //}
                            //catch (Exception)
                            //{ }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Email - In case of Booking created successfully   
                        AgentMaster bookingAgency = new AgentMaster(agencyId);
                        //TODO: ziyad 05 Aug 2013
                        //if (!AutoImport(ref itinerary, member, agencyId, isLcc))
                        //{
                        //    if (isLcc)
                        //    {
                        //        ConfigurationSystem con = new ConfigurationSystem();
                        //        Hashtable hostPort = con.GetHostPort();
                        //        string fromEmail = hostPort["fromEmail"].ToString();
                        //        string toEmail = hostPort["ErrorNotificationMailingId"].ToString();
                        //        // TODO: send stack trace also in the mail
                        //        string messageText = "PNR = " + bookingResponse.PNR + "\r\n Booking Source is " + itinerary.FlightBookingSource.ToString() + "\r\n Agency Name is " + bookingAgency.Name + "\r\n AgencyId is " + bookingAgency.AgencyId + "\r\n MemberId is " + (int)member + "\r\n Member Name is " + member.FirstName + member.LastName + "\r\n Member Email Address is " + member.Email + ". PNR created successfully but failed to save.";
                        //        try
                        //        {
                        //            //Email.Send(fromEmail, toEmail, "Error in Navitaire PNR saving.", messageText);
                        //        }
                        //        catch (System.Net.Mail.SmtpException excep)
                        //        {
                        //            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, "SMTP Exception returned from Review Booking Page. Error Message:" + excep.Message + " | " + DateTime.Now + "| PNR = " + bookingResponse.PNR, "");
                        //        }
                        //    }
                        //    else
                        //    {
                        //        //Worldspan.CancelItinerary(itinerary.PNR);
                        //        BookingHistory bh = new BookingHistory();
                        //        bh.BookingId = booking.BookingId;
                        //        bh.EventCategory = EventCategory.Booking;
                        //        bh.Remarks = "Booked seat are released pnr: " + itinerary.PNR;
                        //        bh.CreatedBy = (int)member;
                        //        bh.Save();
                        //    }
                        //    CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)member, "Save booking failed. Error : " + ex.Message, "0");
                        //    bookingResponse = new BookingResponse(BookingResponseStatus.Failed, bookingResponse.Error, string.Empty);
                        //}
                    }
                }
                // TODO: for white label start after implementing sessionID.
                if (bookingMode == BookingMode.Auto)
                {
                    ClearSession();
                }
                bookingResponse.BookingId = booking.BookingId;
            }
            #endregion
            # region Hotel Booking
            //else 
            if (product.ProductType == ProductType.Hotel)
            {
                HotelItinerary itinerary = (HotelItinerary)product;

                BookingDetail booking = new BookingDetail();

                booking.AgencyId = agencyId;
                booking.CreatedBy = (int)member;
                booking.Status = status; //TODO: Booking status choose for hotel
                

                #region Commented old code
                //itinerary.CreatedBy = (int)member;
                // Sending itinerary for booking                
                /*if (itinerary.Source == HotelBookingSource.Desiya) ziya-todo
                {
                    try
                    {

                        DesiyaApi desi = new DesiyaApi();
                        BookingResponse proBookResponse = new BookingResponse();
                        proBookResponse = desi.GetProvisionalBooking(ref itinerary);
                        if (proBookResponse.Status == BookingResponseStatus.Successful && proBookResponse.ConfirmationNo != null && proBookResponse.ConfirmationNo != "")
                        {
                            bookingResponse = desi.GetBooking(ref itinerary, proBookResponse.ConfirmationNo);
                        }
                        else if (proBookResponse.Status == BookingResponseStatus.OtherFare)
                        {
                            //updating rates accounting
                            for (int i = 0; i < itinerary.Roomtype.Length; i++)
                            {
                                //Taking 1 as default value - For 'INR'
                                decimal rateofExchange = 1;
                                CT.Core.StaticData staticInfo = new CT.Core.StaticData();
                                Dictionary<string, decimal> rateOfEx = staticInfo.CurrencyROE;
                                if (itinerary.Roomtype[i].Price.CurrencyCode != null && rateOfEx.ContainsKey(itinerary.Roomtype[i].Price.CurrencyCode))
                                {
                                    rateofExchange = rateOfEx[itinerary.Roomtype[i].Price.CurrencyCode];
                                }
                                for (int k = 0; k < itinerary.Roomtype[i].RoomFareBreakDown.Length; k++)
                                {
                                    // Commission - Room Rate Break down
                                    Dictionary<string, decimal> commission = AccountingEngine.AccountingEngine.CalculateCommission(itinerary.Roomtype[i].RoomFareBreakDown[k].RoomPrice, agencyId, itinerary.Source, 1, 1, rateofExchange);
                                    itinerary.Roomtype[i].RoomFareBreakDown[k].RoomPrice = itinerary.Roomtype[i].RoomFareBreakDown[k].RoomPrice - commission["AgentCommission"];
                                }
                                System.TimeSpan diffResult = itinerary.EndDate.Subtract(itinerary.StartDate);
                                int nights = diffResult.Days;
                                // Commission - Total Room Price
                                Dictionary<string, decimal> commissionRoom = AccountingEngine.AccountingEngine.CalculateCommission(itinerary.Roomtype[i].Price.PublishedFare, agencyId, itinerary.Source, 1, nights, rateofExchange);

                                itinerary.Roomtype[i].Price.OurCommission = commissionRoom["OurCommission"];
                                itinerary.Roomtype[i].Price.AgentCommission = commissionRoom["AgentCommission"];

                                itinerary.Roomtype[i].Price.TdsCommission = AccountingEngine.AccountingEngine.CalculateTDS(itinerary.Roomtype[i].Price.AgentCommission, agencyId, itinerary.IsDomestic);
                                itinerary.Roomtype[i].Price.SeviceTax = AccountingEngine.AccountingEngine.CalculateProductServiceTax(itinerary.Roomtype[i].Price.PublishedFare + itinerary.Roomtype[i].Price.Tax, itinerary.IsDomestic, ProductType.Hotel);

                                // Commission Extra Gueast Charge
                                Dictionary<string, decimal> commissionEGC = AccountingEngine.AccountingEngine.CalculateCommission(itinerary.Roomtype[i].ExtraGuestCharge, agencyId, itinerary.Source, 1, nights, rateofExchange);
                                itinerary.Roomtype[i].ExtraGuestCharge = itinerary.Roomtype[i].ExtraGuestCharge - commissionEGC["AgentCommission"];

                            }
                            bookingResponse = new BookingResponse(BookingResponseStatus.OtherFare, "", "");
                        }
                        else
                        {
                            bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "No Provisional details", "");
                        }
                    }
                    catch (BookingEngineException)
                    {
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to Book";
                    }
                }
                if (itinerary.Source == HotelBookingSource.GTA)
                {
                    try
                    {
                        GTA gtaApi = new GTA();
                        bookingResponse = gtaApi.GetBooking(ref itinerary);
                    }
                    catch (BookingEngineException)
                    {
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to Book";
                    }
                }
                if (itinerary.Source == HotelBookingSource.HotelBeds)
                {
                    try
                    {
                        HotelBeds hBeds = new HotelBeds();
                        hBeds.sessionId = sessionId;
                        bookingResponse = hBeds.GetBooking(ref itinerary);
                    }
                    catch (BookingEngineException)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.HotelBedsBooking, CT.Core.Severity.High, 0, "Exception while booking for HotelBeds " + DateTime.Now, "");
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to Book";
                    }
                }
                if (itinerary.Source == HotelBookingSource.Tourico)
                {
                    try
                    {
                        Tourico tourico = new Tourico();
                        tourico.sessionId = sessionId;
                        bookingResponse = tourico.GetBooking(ref itinerary);
                    }
                    catch (BookingEngineException)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.TouricoBooking, CT.Core.Severity.High, 0, "Exception while booking for Tourico " + DateTime.Now, "");
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to Book";
                    }
                }
                if (itinerary.Source == HotelBookingSource.IAN)
                {
                    try
                    {
                        IAN ian = new IAN(sessionId);
                        bookingResponse = ian.GetBooking(ref itinerary, agencyId);
                    }
                    catch (BookingEngineException exp)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.IANBooking, CT.Core.Severity.High, 0, "Exception while booking for IAN " + DateTime.Now, "");
                        hotelbookingResponse.Error = exp.Message;
                        bookingResponse.Error = exp.Message;
                    }
                }
                if (itinerary.Source == HotelBookingSource.TBOConnect)
                {
                    try
                    {
                        HotelConnect hc = new HotelConnect();
                        bookingResponse = hc.GetBooking(ref itinerary, agencyId);
                    }
                    catch (BookingEngineException)
                    {
                        Audit.Add(EventType.HotelConnectBooking, Severity.High, 0, "Exception while booking for HotelConnect" + DateTime.Now, string.Empty);
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to make booking";
                    }
                }
                if (itinerary.Source == HotelBookingSource.Miki)
                {
                    try
                    {
                        MikiApi miki = new MikiApi(sessionId);
                        bookingResponse = miki.GetBooking(ref itinerary);
                    }
                    catch (BookingEngineException)
                    {
                        Audit.Add(EventType.MikiBooking, Severity.High, 0, "Exception while booking for Miki" + DateTime.Now, string.Empty);
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to make booking";
                    }
                }
                if (itinerary.Source == HotelBookingSource.Travco)
                {
                    try
                    {
                        TravcoApi travco = new TravcoApi();
                        bookingResponse = travco.GetBooking(ref itinerary);
                    }
                    catch (BookingEngineException)
                    {
                        Audit.Add(EventType.TravcoBooking, Severity.High, 0, "Exception while booking for Travco" + DateTime.Now, string.Empty);
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to make booking";
                    }
                }
               
                if (itinerary.Source == HotelBookingSource.WST)
                {
                    try
                    {
                        WST wst = new WST();
                        bookingResponse = wst.GetBooking(ref itinerary);
                    }
                    catch (BookingEngineException)
                    {
                        Audit.Add(EventType.WSTBooking, Severity.High, 0, "Exception while booking for WST" + DateTime.Now, string.Empty);
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to make booking";
                    }
                }*/

                #endregion
                if (itinerary.Source == HotelBookingSource.DOTW) //ziya-todo
                {
                    try
                    {
                        CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi(sessionId);
                        dotw.AppUserId = appUserId;
                        bookingResponse = dotw.GetBooking(ref itinerary);
                    }
                    catch (BookingEngineException be)
                    {
                        Audit.Add(EventType.DOTWBooking, Severity.High, 0, "Exception while booking for DOTW:" + be.ToString(), string.Empty);
                        //Audit.Add(EventType.DOTWBooking, Severity.High, 0, "Exception while booking for DOTW:"+be.Message.ToString(), string.Empty);
                        hotelbookingResponse.Error = "Unable to Book";


                        bookingResponse.Error = "Unable to make booking " + be.Message;
                        bookingResponse.Status = BookingResponseStatus.Failed;
                    }
                }
                else if (itinerary.Source == HotelBookingSource.RezLive)
                {
                    try
                    {
                        RezLive.XmlHub rezAPI = new RezLive.XmlHub(sessionId);
                        bookingResponse = rezAPI.Book(ref itinerary);
                    }
                    catch (BookingEngineException be)
                    {


                        Audit.Add(EventType.DOTWBooking, Severity.High, 0, "Exception while booking for RezLive: " + be.InnerException.ToString() + DateTime.Now, string.Empty);
                        hotelbookingResponse.Error = "Error while Booking";
                        bookingResponse.Status = BookingResponseStatus.Failed;
                    }
                }
                else if (itinerary.Source == HotelBookingSource.LOH)
                {
                    try
                    {
                        LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine();
                        bookingResponse = jxe.BookHotel(ref itinerary);
                    }
                    catch (BookingEngineException be)
                    {


                        Audit.Add(EventType.DOTWBooking, Severity.High, 0, "Exception while booking for LOH: " + be.InnerException.ToString() + DateTime.Now, string.Empty);
                        hotelbookingResponse.Error = "Error while Booking";
                        bookingResponse.Status = BookingResponseStatus.Failed;
                    }
                }
else if (itinerary.Source == HotelBookingSource.HotelBeds) //Added by brahmam 26.09.2014
                {
                    try
                    {
                        HotelBeds hBeds = new HotelBeds();
                        hBeds.sessionId = sessionId;
                        bookingResponse = hBeds.GetBooking(ref itinerary);
                    }
                    catch (BookingEngineException)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.HotelBedsBooking, CT.Core.Severity.High, 0, "Exception while booking for HotelBeds " + DateTime.Now, "");
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to Book";
                    }
                }
                if (itinerary.Source == HotelBookingSource.GTA)
                {
                    try
                    {
                        DOTWCountry dotw = new DOTWCountry();
                        Dictionary<string, string> Countries = dotw.GetAllCountries();
                        itinerary.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[itinerary.PassengerNationality]);
                        GTA gtaApi = new GTA();
                        bookingResponse = gtaApi.GetBooking(ref itinerary);
                    }
                    catch (BookingEngineException)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.GTABooking, CT.Core.Severity.High, 0, "Exception while booking for GTA " + DateTime.Now, "");
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to Book";
                    }
                }
                if (itinerary.Source == HotelBookingSource.Miki)
                {
                    try
                    {
                        //Get Booking Response From Miki Source
                        MikiApi mikiApi = new MikiApi(sessionId);
                        bookingResponse = mikiApi.GetBooking(ref itinerary);
                    }
                    catch (BookingEngineException)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.MikiBooking, CT.Core.Severity.High, 0, "Exception while booking for Miki " + DateTime.Now, "");
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to Book";
                    }
                }
                else if (itinerary.Source == HotelBookingSource.HotelConnect)
                {
                    try
                    {
                        CZInventory.SearchEngine se = new CZInventory.SearchEngine();
                        bookingResponse = se.BookHotel(itinerary);
                    }
                    catch (Exception ex)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.GTABooking, CT.Core.Severity.High, 0, "Exception while booking for HIS " + DateTime.Now + ex.ToString(), "");
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to Book";
                    }
                }
                if (itinerary.Source == HotelBookingSource.TBOHotel)
                {
                    try
                    {
                        DOTWCountry dotw = new DOTWCountry();
                        Dictionary<string, string> Countries = dotw.GetAllCountries();
                        itinerary.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[itinerary.PassengerNationality]);
                        TBOHotel.HotelV10 tboHotel = new TBOHotel.HotelV10();
                        bookingResponse = tboHotel.GetBooking(ref itinerary);
                    }
                    catch (BookingEngineException be)
                    {
                        Audit.Add(EventType.TBOBooking, Severity.High, 0, "Exception while booking for TBOHotel:" + be.ToString(), string.Empty);
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to make booking " + be.Message;
                        bookingResponse.Status = BookingResponseStatus.Failed;
                    }
                }
                if (itinerary.Source == HotelBookingSource.WST)
                {
                    try
                    {
                        CT.BookingEngine.GDS.WST wst = new CT.BookingEngine.GDS.WST();
                        bookingResponse = wst.GetBooking(ref itinerary);
                    }
                    catch (BookingEngineException)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.WSTBooking, CT.Core.Severity.High, 0, "Exception while booking for WST " + DateTime.Now, "");
                        hotelbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = "Unable to make booking";
                        bookingResponse.Status = BookingResponseStatus.Failed;
                    }
                }

                // TODO: handle else. Generate error.

                if (bookingResponse.Status == BookingResponseStatus.Successful)
                {
                    BookingHistory bookingHistory = new BookingHistory();
                    string remarks = string.Empty;
                    bookingHistory.EventCategory = EventCategory.Booking;
                    bookingHistory.CreatedBy = itinerary.CreatedBy;
                    CT.Core.Queue queue = new CT.Core.Queue();
                    queue.QueueTypeId = (int)QueueType.Booking;
                    queue.StatusId = (int)QueueStatus.Assigned;
                    queue.AssignedTo = (int)member;
                    queue.AssignedBy = (int)member;
                    queue.AssignedDate = DateTime.UtcNow;
                    queue.CompletionDate = DateTime.UtcNow.AddDays(1);
                    queue.CreatedBy = (int)member;

                    //TODO: For New product -- Start                       
                    booking.ProductsList = new Product[1];
                    booking.ProductsList[0] = (Product)itinerary;
                    //For New product -- End
                    remarks = "Booking is Ready";
                    // Saving the booked hotel itinerary to database                  
                    try
                    {
                        using (TransactionScope updateTransaction = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                        {
                            itinerary.BookingMode = bookingMode;
                            itinerary.Status = HotelBookingStatus.Confirmed;
                            //For New product -- Start                       
                            booking.SaveBooking(false);
                            //For New product -- End
                            //if (itinerary.Source == HotelBookingSource.Desiya)
                            //{
                            //    // Sending default Rate of exchange 1 for Desiya. beacuse its in INR.
                            //    int invoiceNumber = AccountUtility.RaiseInvoice(itinerary, string.Empty, (int)member, itinerary.Roomtype[0].Price.RateOfExchange);
                            //    Invoice.UpdateInvoiceStatus(invoiceNumber, InvoiceStatus.Paid, (int)member);
                            //}
                            queue.ItemId = booking.BookingId;
                            bookingHistory.Remarks = remarks;
                            bookingHistory.BookingId = booking.BookingId;
                            queue.Save();
                            bookingHistory.Save();
                            updateTransaction.Complete();
                            //try
                            //{
                            //    Thread mailThread = new Thread(MetaSearchEngine.SendLowBalanceMail);
                            //    mailThread.Start(member);
                            //}
                            //catch (Exception)
                            //{ }
                        }
                        try
                        {
                            //SendHotelItineraryMail(itinerary, "Hotel booking details");
                        }
                        catch (Exception Ex)
                        {
                            CT.Core.Audit.Add(CT.Core.EventType.HotelBook, CT.Core.Severity.High, (int)member, "Hotel : SMTP Send Mail Failure. Error : " + Ex.StackTrace.ToString() + " | " + DateTime.Now + " | " + itinerary.Source.ToString() + " Confirmation No = " + bookingResponse.ConfirmationNo.ToString(), "0");

                        }
                    }
                    catch (Exception ex)
                    {
                        //Assignign primary ids of Hotel Transaction tables to zero.
                        itinerary.HotelId = 0;
                        for (int i = 0; i < itinerary.Roomtype.Length; i++)
                        {
                            itinerary.Roomtype[i].RoomId = 0;
                            itinerary.Roomtype[i].Price.PriceId = 0;
                            for (int j = 0; j < itinerary.Roomtype[i].PassenegerInfo.Count; j++)
                            {
                                itinerary.Roomtype[i].PassenegerInfo[j].PaxId = 0;
                            }
                        }
                           
                        AgentMaster bookingAgency = new AgentMaster(agencyId);
                        string _errorMsg = "Confirmation NO." + bookingResponse.ConfirmationNo + " --Booking is confirmed but not saved in the system.Please contact Cozmo Travel before trying to book again.";
                        if (bookingResponse.ConfirmationNo != null && bookingResponse.ConfirmationNo.Length > 0)
                        {
                            
                            FailedBooking fb = new FailedBooking();
                            fb.Product=ProductType.Hotel;
                            fb.PNR = bookingResponse.ConfirmationNo;
                            fb.AirLocatorCode= bookingResponse.ConfirmationNo;
                            fb.UniversalRecord= bookingResponse.ConfirmationNo;
                            fb.MemberId = (int)member;
                            fb.AgencyId = (int)bookingAgency.ID;
                            fb.AgencyName = bookingAgency.Name;
                            fb.CurretnStatus = Status.NotSaved;
                            fb.Source = (BookingSource)Enum.Parse(typeof(HotelBookingSource), itinerary.Source.ToString());
                            fb.ItineraryXML = FailedBooking.GetXML(itinerary);
                            fb.Remarks = _errorMsg+":err->"+ex.Message;
                            fb.Save();
                        }

                        string subject = "Booking is failed for " + bookingAgency.Name;
                        SendHotelItineraryMail(itinerary, subject);
                        CT.Core.Audit.Add(CT.Core.EventType.HotelBook, CT.Core.Severity.High, (int)member, "confirmation No:" + bookingResponse .ConfirmationNo+ "Save booking failed for hotel. Error : " + ex.Message + " | " + DateTime.Now + " | " + itinerary.Source + " Confirmation No = " + bookingResponse.ConfirmationNo, "0");
                        string messageText = Util.GetExceptionInformation(ex, _errorMsg);
                        Audit.Add(EventType.HotelBook, Severity.High, (int)member, messageText, "");
                        try
                        {
                            Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["ErrorNotificationMailingId"], "Cozmo Hotel Error", messageText);
                        }
                        catch (Exception)
                        {
                            // do nothing. its just for avoiding crash.
                        }
                        throw new BookingEngineException(_errorMsg); // Catch this exception and handle at UI
                        //bookingResponse = new BookingResponse(BookingResponseStatus.Failed, bookingResponse.Error, string.Empty);
                    }
                }
                bookingResponse.BookingId = booking.BookingId;
            }
            #endregion
            # region Transfer Booking
            else if (product.ProductType == ProductType.Transfers)
            {
                UserMaster loggedMember = new UserMaster(member);
                TransferItinerary itinerary = (TransferItinerary)product;

                BookingDetail booking = new BookingDetail();

                booking.AgencyId = agencyId;
                booking.CreatedBy = (int)member;
                booking.Status = status;
                itinerary.CreatedBy = (int)member;
                if (itinerary.Source == TransferBookingSource.GTA)
                {
                    try
                    {
                        GTA gtaApi = new GTA();
                        bookingResponse = gtaApi.GetTransferBooking(ref itinerary);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.GTABooking, Severity.High, 0, "Exception while booking for GTA: " + ex + DateTime.Now, string.Empty);
                        transferbookingResponse.Error = "Unable to Book";
                        //bookingResponse.Error = "Unable to Book";
                        //Added on 18042016 for getting error message from supplier end
                        bookingResponse.Error = ex.Message;
                        bookingResponse.Status = BookingResponseStatus.Failed;
                    }
                }
                if (bookingResponse.Status == BookingResponseStatus.Successful)
                {
                    CT.Core.Queue queue = new CT.Core.Queue();
                    queue.QueueTypeId = (int)QueueType.Booking;
                    queue.StatusId = (int)QueueStatus.Assigned;
                    queue.AssignedTo = (int)member;
                    queue.AssignedBy = (int)member;
                    queue.AssignedDate = DateTime.UtcNow;
                    queue.CompletionDate = DateTime.UtcNow.AddDays(1);
                    queue.CreatedBy = (int)member;

                    //TODO: For New product -- Start                       
                    booking.ProductsList = new Product[1];
                    booking.ProductsList[0] = (Product)itinerary;
                    //For New product -- End

                    // Saving the booked transfer itinerary to database                  
                    try
                    {
                        using (TransactionScope updateTransaction = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                        {
                            itinerary.BookingMode = bookingMode;
                            //For New product -- Start                       
                            booking.SaveBooking(false);
                            bookingResponse.ConfirmationNo = itinerary.ConfirmationNo;
                            queue.ItemId = booking.BookingId;
                            queue.Save();
                            updateTransaction.Complete();
                        }
                    }
                    catch (Exception ex)
                    {
                        itinerary.TransferId = 0;
                        AgentMaster bookingAgency = new AgentMaster(agencyId);
                        UserMaster user = new UserMaster(member);
                        ConfigurationSystem con = new ConfigurationSystem();
                        string _errorMsg = "Confirmation NO." + itinerary.ConfirmationNo + " --Booking is confirmed but not saved in the system.Please contact Cozmo Travel before trying to book again.";
                        if (itinerary.ConfirmationNo != null && itinerary.ConfirmationNo.Length > 0)
                        {

                            FailedBooking fb = new FailedBooking();
                            fb.Product = ProductType.Transfers;
                            fb.PNR = itinerary.ConfirmationNo;
                            fb.AirLocatorCode = itinerary.ConfirmationNo;
                            fb.UniversalRecord = itinerary.ConfirmationNo;
                            fb.MemberId = (int)member;
                            fb.AgencyId = (int)bookingAgency.ID;
                            fb.AgencyName = bookingAgency.Name;
                            fb.CurretnStatus = Status.NotSaved;
                            fb.Source = (BookingSource)Enum.Parse(typeof(TransferBookingSource), itinerary.Source.ToString());
                            fb.ItineraryXML = FailedBooking.GetXML(itinerary);
                            fb.Remarks = _errorMsg + ":err->" + ex.Message;
                            fb.Save();
                        }

                        Hashtable hostPort = con.GetHostPort();
                        string fromEmail = hostPort["fromEmail"].ToString();
                        string toEmail = hostPort["ErrorNotificationMailingId"].ToString();
                        string messageText = string.Concat(new object[]
									{
										"Transfers Confirmation No : ",
										itinerary.ConfirmationNo,
										"<br/> Transfer Booking Ref No : ",
										itinerary.BookingReference,
										"<br/> Booking is successful but failed to save Transfers information (",
										itinerary.Source.ToString(),
										").<br/> Agency Name is ",
										bookingAgency.Name,
										"<br/> AgencyId is ",
										bookingAgency.ID,
										"<br/> MemberId is ",
										user.ID,
										"<br/> Member Name is ",
										user.FirstName,
										user.LastName,
										"<br/> Member Email Address is ",
										user.Email,
                                        "<br/> Reason For Failed:- ",ex.Message
									});
                        try
                        {
                            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                            Email.Send(fromEmail, toEmail, toArray, "Error while saving Transfer Booking.", messageText, new Hashtable());
                        }
                        catch (System.Net.Mail.SmtpException excep4)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 0, string.Concat(new object[]
										{
											"SMTP Exception returned from Review Booking Page. Error Message:",
											excep4.Message,
											" | ",
											DateTime.Now,
											" | Transfers (",
											itinerary.Source.ToString(),
											") Confirmation No = ",
											itinerary.ConfirmationNo
										}), "");
                        }
                        Audit.Add(EventType.Exception, Severity.High, (int)member, string.Concat(new object[]
									{
										"Save booking failed for Transfers. Error : ",
										ex.Message,
										" | ",
										DateTime.Now,
										" | ",
										itinerary.Source,
										" Confirmation No = ",
										itinerary.ConfirmationNo
									}), "0");
                        throw new BookingEngineException(ex.Message);
                        //// Email - In case of Booking created successfully   
                        //AgentMaster bookingAgency = new AgentMaster(agencyId);
                        //ConfigurationSystem con = new ConfigurationSystem();
                        //Hashtable hostPort = con.GetHostPort();
                        //string fromEmail = hostPort["fromEmail"].ToString();
                        //string toEmail = hostPort["ErrorNotificationMailingId"].ToString();
                        //// TODO: send stack trace also in the mail
                        //string messageText = "Transfer Confirmation No : " + bookingResponse.ConfirmationNo + "\r\n " + "Transfer Booking Ref No : " + itinerary.BookingReference + "\r\n " + "Booking is successful but failed to save Transfer information (" + itinerary.Source.ToString() + ").\r\n AgentMaster Name is " + bookingAgency.Name + "\r\n AgencyId is " + bookingAgency.ID + "\r\n MemberId is " + (int)member + "\r\n Member Name is " + loggedMember.FirstName + loggedMember.LastName + "\r\n Member Email Address is " + loggedMember.Email;
                        //try
                        //{
                        //    Email.Send(fromEmail, toEmail, "Error while saving Transfer Booking.", messageText);
                        //}
                        //catch (System.Net.Mail.SmtpException excep)
                        //{
                        //    CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, "SMTP Exception returned from Review Booking Page. Error Message:" + excep.Message + " | " + DateTime.Now + " | Transfer (" + itinerary.Source.ToString() + ") Confirmation No = " + bookingResponse.ConfirmationNo, "");
                        //}
                        //CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)member, "Save booking failed for transfer. Error : " + ex.Message + " | " + DateTime.Now + " | " + itinerary.Source + " Confirmation No = " + bookingResponse.ConfirmationNo, "0");
                        //throw new BookingEngineException(ex.Message); // Catch this exception and handle at UI
                        ////bookingResponse = new BookingResponse(BookingResponseStatus.Failed, bookingResponse.Error, string.Empty);
                    }
                }
                bookingResponse.BookingId = booking.BookingId;
            }

            #endregion
            #region Sightseeing Booking
            if (product.ProductType == ProductType.SightSeeing)
            {
                SightseeingItinerary itinerary = (SightseeingItinerary)product;

                BookingDetail booking = new BookingDetail();

                booking.AgencyId = agencyId;
                booking.CreatedBy = (int)member;
                booking.Status = status; //TODO: Booking status choose for hotel
                itinerary.CreatedBy = (int)member;
                if (itinerary.Source == SightseeingBookingSource.GTA)
                {
                    try
                    {
                        GTA gtaApi = new GTA();
                        bookingResponse = gtaApi.GetSightseeingBooking(ref itinerary);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.GTABooking, Severity.High, 0, "Exception while booking for GTA: " + ex + DateTime.Now, string.Empty);
                        sightseeingbookingResponse.Error = "Unable to Book";
                        bookingResponse.Error = ex.Message;
                        bookingResponse.Status = BookingResponseStatus.Failed;
                    }
                }
                if (bookingResponse.Status == BookingResponseStatus.Successful)
                {
                    CT.Core.Queue queue = new CT.Core.Queue();
                    queue.QueueTypeId = (int)QueueType.Booking;
                    queue.StatusId = (int)QueueStatus.Assigned;
                    queue.AssignedTo = (int)member;
                    queue.AssignedBy = (int)member;
                    queue.AssignedDate = DateTime.UtcNow;
                    queue.CompletionDate = DateTime.UtcNow.AddDays(1);
                    queue.CreatedBy = (int)member;

                    //TODO: For New product -- Start                       
                    booking.ProductsList = new Product[1];
                    booking.ProductsList[0] = (Product)itinerary;
                    //For New product -- End

                    // Saving the booked SightSeeing itinerary to database                  
                    try
                    {
                        using (TransactionScope updateTransaction = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                        {
                            itinerary.BookingMode = bookingMode;
                            bookingResponse.ConfirmationNo = itinerary.ConfirmationNo;
                            //For New product -- Start                       
                            booking.SaveBooking(false);
                            bookingResponse.ConfirmationNo = itinerary.ConfirmationNo;
                            queue.ItemId = booking.BookingId;
                            queue.Save();
                            updateTransaction.Complete();
                        }
                    }
                    catch (Exception ex)
                    {

                        itinerary.SightseeingId = 0;
                        AgentMaster bookingAgency = new AgentMaster(agencyId);
                        UserMaster user = new UserMaster(member);
                        ConfigurationSystem con = new ConfigurationSystem();
                        string _errorMsg = "Confirmation NO." + bookingResponse.ConfirmationNo + " --Booking is confirmed but not saved in the system.Please contact Cozmo Travel before trying to book again.";
                        if (bookingResponse.ConfirmationNo != null && bookingResponse.ConfirmationNo.Length > 0)
                        {

                            FailedBooking fb = new FailedBooking();
                            fb.Product = ProductType.SightSeeing;
                            fb.PNR = bookingResponse.ConfirmationNo;
                            fb.AirLocatorCode = bookingResponse.ConfirmationNo;
                            fb.UniversalRecord = bookingResponse.ConfirmationNo;
                            fb.MemberId = (int)member;
                            fb.AgencyId = (int)bookingAgency.ID;
                            fb.AgencyName = bookingAgency.Name;
                            fb.CurretnStatus = Status.NotSaved;
                            fb.Source = (BookingSource)Enum.Parse(typeof(SightseeingBookingSource), itinerary.Source.ToString());
                            fb.ItineraryXML = FailedBooking.GetXML(itinerary);
                            fb.Remarks = _errorMsg + ":err->" + ex.Message;
                            fb.Save();
                        }

                        Hashtable hostPort = con.GetHostPort();
                        string fromEmail = hostPort["fromEmail"].ToString();
                        string toEmail = hostPort["ErrorNotificationMailingId"].ToString();
                        string messageText = string.Concat(new object[]
									{
										"Sightseeing Confirmation No : ",
										bookingResponse.ConfirmationNo,
										"\r\n Sightseeing Booking Ref No : ",
										itinerary.BookingReference,
										"\r\n Booking is successful but failed to save SightSeeing information (",
										itinerary.Source.ToString(),
										").\r\n Agency Name is ",
										bookingAgency.Name,
										"\r\n AgencyId is ",
										bookingAgency.ID,
										"\r\n MemberId is ",
										user.ID,
										"\r\n Member Name is ",
										user.FirstName,
										user.LastName,
										"\r\n Member Email Address is ",
										user.Email
									});
                        try
                        {
                            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                            Email.Send(fromEmail, toEmail,toArray, "Error while saving Sightseeing Booking.", messageText,new Hashtable());
                        }
                        catch (System.Net.Mail.SmtpException excep4)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 0, string.Concat(new object[]
										{
											"SMTP Exception returned from Review Booking Page. Error Message:",
											excep4.Message,
											" | ",
											DateTime.Now,
											" | Hotel (",
											itinerary.Source.ToString(),
											") Confirmation No = ",
											bookingResponse.ConfirmationNo
										}), "");
                        }
                        Audit.Add(EventType.Exception, Severity.High, (int)member, string.Concat(new object[]
									{
										"Save booking failed for SightSeeing. Error : ",
										ex.Message,
										" | ",
										DateTime.Now,
										" | ",
										itinerary.Source,
										" Confirmation No = ",
										bookingResponse.ConfirmationNo
									}), "0");
                        throw new BookingEngineException(ex.Message);
                    }
                }
                bookingResponse.BookingId = booking.BookingId;
            }
            #endregion
            return bookingResponse;
        }
        /// <summary>
        /// This Method is used to send the email whenever booking is done or booking failed.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="subject"></param>
        private void SendHotelItineraryMail(HotelItinerary itinerary, string subject)
        {
            ConfigurationSystem con = new ConfigurationSystem(); //ziya-todo
            Hashtable hostPort = con.GetHostPort();
            string fromEmail = hostPort["fromEmail"].ToString();
            string toEmail = hostPort["ErrorNotificationMailingId"].ToString();
            string messageText = string.Empty;
            //GTA gtaAPi = new GTA();
            // messageText+="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head id=\"ctl00_Head1\"><title>Hotel Booking Details</title></head><body>";
            messageText += " <table cellpadding=\"7\" cellspacing=\"0\" width=\"658\" border=\"0\">";
            messageText += "<tr><td style=\"background:#ededed; padding-bottom:15px; width:64px;\"><img height=\"48\" width=\"48\" title=\"" + itinerary.HotelName + "\" /></td>";
            messageText += "<td style=\"background:#ededed; font-weight:bold; vertical-align:top; width:550px;\">" + itinerary.HotelName + "";
            messageText += "<kbd style=\"display:block; font-family:Arial; font-weight:normal; font-size:12px; padding-top:6px; font-style:normal\">" + itinerary.HotelAddress1 + "<br/> " + itinerary.HotelAddress2 + "</kbd>";
            messageText += "<kbd style=\"display:block; font-family:Arial; font-weight:normal; font-size:12px; padding-top:6px; font-style:normal\">" + itinerary.CityRef + "</kbd>";
            messageText += "</td></tr><tr><td colspan=\"2\" style=\"line-height:2px; font-size:6px;\">&nbsp;</td></tr><tr><td colspan=\"2\" style=\"background:#ededed; padding-bottom:15px; border:solid 1px #727272;\">";
            messageText += "<table cellpadding=\"7\" cellspacing=\"0\" width=\"100%\" border=\"0\"><tr><td colspan=\"2\" style=\"text-transform:uppercase; font-size:14px; font-weight:bold;\">YOUR RESERVATION HAS BEEN BOOKED!</td>";
            messageText += "</tr><tr><td style=\"font-weight:bold;\">Your Itinerary Number:</td>";
            messageText += "<td style=\"text-transform:uppercase; font-size:13px; font-weight:bold;\">" + itinerary.ConfirmationNo + "</td></tr>";
            if (!itinerary.IsDomestic)
            {
                messageText += "<tr><td style=\"font-weight:bold;\">Reference Number(s):</td>";
                string[] roomRef = itinerary.BookingRefNo.Split('|');
                foreach (string str in roomRef)
                {
                    messageText += "<td style=\"font-weight:bold;\">" + str + "</td>";
                }
                messageText += "</tr>";
            }
            messageText += "<tr><td colspan=\"2\" style=\"font-size:11px; font-weight:normal;\">Please refer to your itinerary number above if you contact Customer Service for any reason.</td>";
            messageText += "</tr></table></td></tr><tr><td colspan=\"2\" style=\"line-height:2px; font-size:6px;\">&nbsp;</td></tr><tr><td colspan=\"2\" style=\"padding-bottom:15px; border:solid 1px #727272;\"><table cellpadding=\"7\" cellspacing=\"0\" width=\"100%\" border=\"0\">";
            messageText += "<tr><td colspan=\"2\" style=\"text-transform:uppercase; font-size:14px; font-weight:bold; background:#ededed; border:solid 1px #8b8a8a;\">YOUR RESERVATION HAS BEEN BOOKED!</td></tr><tr><td style=\"font-weight:normal;\">Check In:<b style=\"padding-left:10px;\">" + itinerary.StartDate.ToString("MMM dd, yyyy") + "</b></td>";
            messageText += "<td style=\"font-weight:normal;\">Check Out:<b style=\"padding-left:10px;\">" + itinerary.EndDate.ToString("MMM dd, yyyy") + "</b></td></tr>";
            for (int i = 0; i < itinerary.Roomtype.Length; i++)
            {
                messageText += "<tr><td><b>Room " + (i + 1) + "</b>&nbsp;:&nbsp;" + itinerary.Roomtype[i].RoomName + " ( " + itinerary.Roomtype[i].AdultCount + " Adult , " + itinerary.Roomtype[i].ChildCount.ToString() + " Children)";
                if (itinerary.Roomtype[i].PassenegerInfo != null)
                {
                    messageText += "<b>&nbsp;Guest : </b>" + itinerary.Roomtype[i].PassenegerInfo[0].Title + " " + itinerary.Roomtype[i].PassenegerInfo[0].Firstname + " " + itinerary.Roomtype[i].PassenegerInfo[0].Lastname;
                }
                messageText += "<b>&nbsp;Rate : </b>" + (itinerary.Roomtype[i].Price.Currency) + Math.Round(itinerary.Roomtype[i].Price.PublishedFare + itinerary.Roomtype[i].Price.NetFare, Convert.ToInt32(Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                messageText += " </td></tr>";
            }
            messageText += "<tr><td colspan=\"2\" style=\"font-size:11px; font-weight:normal;\"><hr /></td></tr><tr>";
            messageText += "<td style=\"font-weight:normal; font-size:13px;\"><b>Rates per Room</b><br /><em style=\"font-style:normal; font-size:10px;\">(excluding tax recovery charges and service fees)</em></td>";
            messageText += "</tr><tr><td colspan=\"2\" style=\"font-size:12px; font-weight:bold; background:#ededed; border:solid 1px #8b8a8a;\">Hotel Policy</td>";
            messageText += "</tr><tr><td colspan=\"2\" style=\"font-size:11px;\">" + itinerary.HotelPolicyDetails.Replace("|", "<br/>") + "</td>";
            messageText += "</tr><tr><td colspan=\"2\" style=\"font-size:12px; font-weight:bold; background:#ededed; border:solid 1px #8b8a8a;\">Cancellation Policy</td>";
            messageText += "</tr><tr><td colspan=\"2\" style=\"font-size:11px;\">" + itinerary.HotelCancelPolicy.Replace("|", "<br/>") + "</td></tr></table>";
            try
            {
                List<string> toArray = new List<string>();
                toArray.Add(toEmail);
                CT.Core.Email.Send(fromEmail, fromEmail, toArray, subject, messageText, new Hashtable(), fromEmail);
            }
            catch (System.Net.Mail.SmtpException excep)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, 0, "SMTP Exception returned from Review Booking Page. Error Message:" + excep.Message + " | " + DateTime.Now + " | Hotel (" + itinerary.Source.ToString() + ") Confirmation No = " + itinerary.ConfirmationNo, "");
            }
        }
      /*  public BookingResponse Book(ref FlightItinerary itinerary, int agencyId, BookingStatus status, int memberid, Ticket[] ticket, Hashtable table)
        {
            BookingResponse bookingResponse = new BookingResponse();
            BookingDetail booking = new BookingDetail();

            CT.Core.Queue queue = new CT.Core.Queue();
            queue.QueueTypeId = (int)QueueType.Booking;
            queue.StatusId = (int)QueueStatus.Assigned;
            queue.AssignedTo = memberid;
            queue.AssignedBy = memberid;
            queue.AssignedDate = DateTime.UtcNow;
            queue.CompletionDate = DateTime.UtcNow;
            queue.CreatedBy = memberid;

            booking.AgencyId = agencyId;
            booking.Status = BookingStatus.Ticketed;
            booking.CreatedBy = memberid;

            Comment ssrComment = new Comment();
            ssrComment.AgencyId = booking.AgencyId;
            ssrComment.CommentDescription = bookingResponse.SSRMessage;
            ssrComment.CommentTypes = CommmentType.Booking;
            ssrComment.CreatedBy = 1;
            ssrComment.IsPublic = true;

            try
            {
                BookingHistory bh = new BookingHistory();
                string remarks = string.Empty;
                bh.EventCategory = EventCategory.Ticketing;
                remarks = "Ticket is created";

                bh.Remarks = remarks;
                bh.CreatedBy = memberid;
                int invoiceNumber = 0;

                int txnRetryCount = Convert.ToInt32(ConfigurationSystem.BookingEngineConfig["txnRetryCount"]);
                int numberRetry = 0;
                bool txnSucceded = false;
                while (!txnSucceded && numberRetry < txnRetryCount)
                {
                    numberRetry++;
                    try
                    {
                        using (TransactionScope updateTransaction = new TransactionScope())
                        {
                            booking.Save(itinerary, false);
                            SaveManualTicketInfo(itinerary, memberid, agencyId, ticket, booking.BookingId);
                            queue.ItemId = booking.BookingId;
                            queue.Save();
                            updateTransaction.Complete();
                            txnSucceded = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        if (numberRetry < txnRetryCount)
                        {
                            booking.BookingId = 0;
                            itinerary.FlightId = 0;
                            for (int i = 0; i < itinerary.Passenger.Length; i++)
                            {
                                itinerary.Passenger[i].PaxId = 0;
                                itinerary.Passenger[i].Price.PriceId = 0;
                            }
                            for (int i = 0; i < itinerary.Segments.Length; i++)
                            {
                                itinerary.Segments[i].SegmentId = 0;
                            }
                            continue;
                        }
                        else
                        {
                            Trace.TraceInformation(ex.Message);
                            throw ex;
                        }
                    }
                }
                if (txnSucceded)
                {
                    try
                    {
                        invoiceNumber = AccountUtility.RaiseInvoiceForManualBooking(itinerary, ticket, table, memberid, booking);
                    }
                    catch (Exception ex)
                    {
                        string messageText = "PNR = " + bookingResponse.PNR + "\r\n Booking Source is " + itinerary.FlightBookingSource.ToString();
                        string message = Util.GetExceptionInformation(ex, "Invoice save failed.\r\n" + messageText);
                        try
                        {
                            Email.Send("Invoice save failed", message);
                        }
                        catch (System.Net.Mail.SmtpException) { } // Do nothing                                
                        CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, memberid, "Save Invoice failed. Error : " + ex.Message, "0");

                    }
                    //try
                    //{
                    //    Thread mailThread = new Thread(MetaSearchEngine.SendLowBalanceMail);
                    //    long member = new Member(memberid);
                    //    mailThread.Start(member);
                    //}
                    //catch (Exception)
                    //{ }
                }
                if (table["isSendEmail"].ToString().Equals("true", StringComparison.OrdinalIgnoreCase))
                {
                    string to = ConfigurationSystem.Email["GroupEmailAddress"];
                    string from = ConfigurationSystem.Email["fromEmail"];
                    Agency agency = new Agency();
                    agency.Load(agencyId);
                    Invoice inv = new Invoice();
                    inv.Load(invoiceNumber);
                    string inVoiceNumber = inv.DocTypeCode + inv.DocumentNumber;
                    string subject = agency.Name + "has exceeded Credit Limit";
                    string body = "Hello,\n" + agency.Name + " don't have sufficient balance to issue Ticket(s).But Manual ticket(s) of Amount " + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + " " + table["netAmount"] + " has been issued in its behalf. \n\n Last Invoice Generated Date of InvoiceNumber " + inVoiceNumber + " is : " + Util.UTCToIST(DateTime.Parse(table["InvoiceDate"].ToString())).ToString().Split(new char[] { ' ' })[0] + "\n\n Regards, \n Admin";
                    Email.Send(from, to, subject, body);
                }
                bookingResponse.Status = BookingResponseStatus.Successful;
                bookingResponse.SSRMessage = "Tickets Booked successfuly with invoice Number :" + invoiceNumber.ToString() + " .";
            }
            catch (Exception ex)
            {
                bookingResponse.Status = BookingResponseStatus.Failed;
                bookingResponse.Error = "Unable to save the Booking.";
                //#region Handling booking save failure
                //#endregion
                CT.Core.Audit.Add(EventType.ManualInvoice, Severity.High, memberid, ex.Message + ".Unable to save Imported pnr booking or Manual Invoice Booking.", "0");

            }
            if (itinerary.FlightBookingSource == BookingSource.Amadeus || itinerary.FlightBookingSource == BookingSource.WorldSpan)
            {
                SaveSSR(itinerary);
            }
            bookingResponse.BookingId = booking.BookingId;
            return bookingResponse;
        }
        public BookingResponse Book(ref FlightItinerary itinerary, int agencyId, BookingStatus status, long member, bool avbBook, BookingType type, BookingMode bookingMode)
        {
            return Book(ref itinerary, agencyId, status, member, avbBook, type, bookingMode, string.Empty);
        }
        public BookingResponse Book(ref FlightItinerary itinerary, int agencyId, BookingStatus status, long member, bool avbBook, BookingType type, BookingMode bookingMode, string bookRequestXML)
        {
            Dictionary<string, string> ticketData = new Dictionary<string, string>();
            return Book(ref itinerary, agencyId, status, member, avbBook, type, bookingMode, bookRequestXML, string.Empty, ticketData);
        }*/
        public BookingResponse Book(ref FlightItinerary itinerary, int agencyId, BookingStatus status, UserMaster member, bool avbBook, BookingType type, BookingMode bookingMode, string bookRequestXML, string ipAddr, Dictionary<string, string> ticketData)
        {
            BookingResponse bookingResponse = new BookingResponse();
            bool isLcc = false;
            BookingDetail booking = new BookingDetail();
            try
            {
                // TODO: If booking type is not Book or PowerBook - throw exception.
                if (!Basket.FlightBookingSession.ContainsKey(sessionId))
                {
                    throw new BookingEngineException("Booking session invalid.");
                }
                else
                {
                    Basket.FlightBookingSession[sessionId].SearchTime = DateTime.Now;
                }
                Basket.FlightBookingSession[sessionId].Itinerary = itinerary;
                //bool modeSpicejet = (ConfigurationSystem.SpiceJetConfig["Mode"] == "SpiceJet");

                AirArabia aaObj;
                /*SpiceJetAPI sjObj;
                Navitaire naviObj;
                ParamountApi pmObj;
                Mdlr mdlr;
                AirDeccanApi adObj;
                GoAirApi gaObj;
            
                Sama sama;
                HermesApi hApi;*/
                FlyDubaiApi flyDubaiApi;
                //List<BoardingPass> boardingPassList = new List<BoardingPass>();
                List<string> pmTicketNo = new List<string>();
                // else it goes for "6E"
                int TimeLimitToComplete = 7;
                bool hasInfant = false;
                // This will be true in case of Spicejet or Indigo.
                isLcc = (itinerary.FlightBookingSource == BookingSource.HermesAirLine || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.IsLCC);

                booking.AgencyId = agencyId;
                booking.CreatedBy = (int)member.ID;
                booking.Status = status;
                itinerary.CreatedBy = (int)member.ID;
                itinerary.AgencyId = agencyId;

                string titleRegex = "^[a-zA-Z ]{2,4}[.]{0,1}[a-zA-Z]{0,4}[.]{0,1}$";
                string nameRegex = "[a-zA-Z. ]+$";
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    if (itinerary.Passenger[i].Title != null && itinerary.Passenger[i].Title.Trim() != string.Empty && !Regex.IsMatch(itinerary.Passenger[i].Title, titleRegex))
                    {
                        Email.Send("Invalid Title", itinerary.ToString());
                        Audit.Add(EventType.Book, Severity.Normal, (int)member.ID, "Invalid Title: \"" + itinerary.Passenger[i].Title + "\"", "0");
                        Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                        throw new ArgumentException("Invalid title", "title");
                    }
                    if (itinerary.Passenger[i].FirstName != null && !Regex.IsMatch(itinerary.Passenger[i].FirstName, nameRegex))
                    {
                        Email.Send("Invalid FirstName", itinerary.ToString());
                        Audit.Add(EventType.Book, Severity.Normal, (int)member.ID, "Invalid FirstName: \"" + itinerary.Passenger[i].FirstName + "\"", "0");
                        Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                        throw new ArgumentException("Invalid FirstName", "firstName");
                    }
                    if (itinerary.Passenger[i].LastName != null && (!Regex.IsMatch(itinerary.Passenger[i].LastName, nameRegex) || itinerary.Passenger[i].LastName.Trim().Replace(".", string.Empty).Length < 2))
                    {
                        Email.Send("Invalid LastName", itinerary.ToString());
                        Audit.Add(EventType.Book, Severity.Normal, (int)member.ID, "Invalid LastName: \"" + itinerary.Passenger[i].LastName + "\"", "0");
                        Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                        throw new ArgumentException("Invalid LastName", "lastName");
                    }
                    //if (itinerary.Passenger[i].Type == PassengerType.Infant && (itinerary.FlightBookingSource != BookingSource.AirArabia && itinerary.FlightBookingSource != BookingSource.UAPI))
                    //{
                    //    itinerary.Passenger[i].Title = string.Empty;
                    //}
                }
                // Sending itinerary for booking 

                /*if (itinerary.FlightBookingSource == BookingSource.WorldSpan)
                {
                    try
                    {
                        if (avbBook || type == BookingType.Book)    // Book in the same class.
                        {
                            bookingResponse = Worldspan.AvailabilityBook(ref itinerary, member.FullName, sessionId);
                        }
                        else
                        {   // Power book.
                            bookingResponse = Worldspan.HoldItinerary(ref itinerary, member.FullName, sessionId);
                        }
                    }
                    catch (BookingEngineException)
                    {
                        bookingResponse.Error = "Unable to Book";
                        Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                    }
                }
                else if (itinerary.FlightBookingSource == BookingSource.Amadeus)
                {
                    try
                    {
                        if (avbBook || type == BookingType.Book)    // Book in the same class.
                        {
                            bookingResponse = Amadeus.AvailabilityBook(itinerary, sessionId);
                        }
                        else
                        {   // Power book.
                            bookingResponse = Amadeus.Book(itinerary, sessionId);
                        }
                    }
                    catch (BookingEngineException)
                    {
                        bookingResponse.Error = "Unable to Book";
                        Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                    }
                }
                else if (itinerary.FlightBookingSource == BookingSource.Galileo)
                {
                    try
                    {
                        bookingResponse = GalileoApi.Book(itinerary, sessionId);
                    }
                    catch (BookingEngineException)
                    {
                        bookingResponse.Error = "Unable to Book";
                        Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                    }
                }*/
                List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();
                if (itinerary.FlightBookingSource == BookingSource.UAPI)
                {
                    try
                    {
                        SourceDetails agentDetails = new SourceDetails();

                        if (SettingsLoginInfo.IsOnBehalfOfAgent)
                        {
                            agentDetails = SettingsLoginInfo.OnBehalfAgentSourceCredentials["UA"];
                            UAPI.agentBaseCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                            UAPI.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                        }
                        else
                        {
                            agentDetails = SettingsLoginInfo.AgentSourceCredentials["UA"];
                            UAPI.agentBaseCurrency = SettingsLoginInfo.Currency;
                            UAPI.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                        }

                        //UAPI uApi = new UAPI();
                        UAPI.UserName = agentDetails.UserID;
                        UAPI.Password = agentDetails.Password;
                        UAPI.TargetBranch = agentDetails.HAP;
                        try
                        {
                            if (!itinerary.Segments[0].UAPIReservationValues.ContainsKey(UAPI.PARENT_AGENT_LOCATION))
                            {
                                string details = AgentMaster.GetParentAgentLocationAndPhone(itinerary.AgencyId);
                                itinerary.Segments[0].UAPIReservationValues.Add(UAPI.PARENT_AGENT_LOCATION, details.Split(',')[0]);
                                itinerary.Segments[0].UAPIReservationValues.Add(UAPI.PARENT_AGENT_PHONE, details.Split(',')[1]);
                            }
                        }
                        catch { }
                        bookingResponse = UAPI.Book(itinerary, sessionId, out ptcDetails);
                    }
                    catch (BookingEngineException)
                    {
                        bookingResponse.Error = "Unable to Book...UAPI";
                        Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                    }
                }
                else if (itinerary.FlightBookingSource == BookingSource.TBOAir && !isLcc)
                {
                    SourceDetails agentDetails = new SourceDetails();

                    AirV10 tbo = new AirV10();

                    if (SettingsLoginInfo.IsOnBehalfOfAgent)
                    {
                        agentDetails = SettingsLoginInfo.OnBehalfAgentSourceCredentials["TA"];
                        tbo.AgentBaseCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                        tbo.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    }
                    else
                    {
                        agentDetails = SettingsLoginInfo.AgentSourceCredentials["TA"];
                        tbo.AgentBaseCurrency = SettingsLoginInfo.Currency;
                        tbo.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    }

                    //tbo.LoginName = agentDetails.UserID;
                    //tbo.Password = agentDetails.Password;
                    bookingResponse = tbo.Book(ref itinerary);
                }
                else if (isLcc) // todo handling LCCL
                {

                    try
                    {
                        if (itinerary.FlightBookingSource == BookingSource.AirArabia)
                        {
                            aaObj = new AirArabia();
                            if (SettingsLoginInfo.IsOnBehalfOfAgent)
                            {
                                aaObj.UserName = SettingsLoginInfo.OnBehalfAgentSourceCredentials["G9"].UserID;
                                aaObj.Password = SettingsLoginInfo.OnBehalfAgentSourceCredentials["G9"].Password;
                                aaObj.Code = SettingsLoginInfo.OnBehalfAgentSourceCredentials["G9"].HAP;
                                aaObj.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                                aaObj.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                            }
                            else
                            {
                                aaObj.UserName = SettingsLoginInfo.AgentSourceCredentials["G9"].UserID;
                                aaObj.Password = SettingsLoginInfo.AgentSourceCredentials["G9"].Password;
                                aaObj.Code = SettingsLoginInfo.AgentSourceCredentials["G9"].HAP;
                                aaObj.AgentCurrency = SettingsLoginInfo.Currency;
                                aaObj.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                            }
                            aaObj.AppUserId = appUserId.ToString();
                            aaObj.SessionID = sessionId;
                            bookingResponse = aaObj.Booking(itinerary, booking.AgencyId, sessionId);
                        }

                        else if (itinerary.FlightBookingSource == BookingSource.SpiceJet)
                        {
                            SpiceJetAPIV1 sjObj = new SpiceJetAPIV1();
                            if (SettingsLoginInfo.IsOnBehalfOfAgent)
                            {
                                sjObj.LoginName = SettingsLoginInfo.OnBehalfAgentSourceCredentials["SG"].UserID;
                                sjObj.Password = SettingsLoginInfo.OnBehalfAgentSourceCredentials["SG"].Password;
                                sjObj.AgentDomain = SettingsLoginInfo.OnBehalfAgentSourceCredentials["SG"].HAP;
                                sjObj.AgentBaseCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                                sjObj.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                            }
                            else
                            {
                                sjObj.LoginName = SettingsLoginInfo.AgentSourceCredentials["SG"].UserID;
                                sjObj.Password = SettingsLoginInfo.AgentSourceCredentials["SG"].Password;
                                sjObj.AgentDomain = SettingsLoginInfo.AgentSourceCredentials["SG"].HAP;
                                sjObj.AgentBaseCurrency = SettingsLoginInfo.Currency;
                                sjObj.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                            }
                            bookingResponse = sjObj.BookItinerary(ref itinerary);
                        }
                        else if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                        {
                            SourceDetails agentDetails = new SourceDetails();

                            AirV10 tbo = new AirV10();

                            if (SettingsLoginInfo.IsOnBehalfOfAgent)
                            {
                                agentDetails = SettingsLoginInfo.OnBehalfAgentSourceCredentials["TA"];
                                tbo.AgentBaseCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                                tbo.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                            }
                            else
                            {
                                agentDetails = SettingsLoginInfo.AgentSourceCredentials["TA"];
                                tbo.AgentBaseCurrency = SettingsLoginInfo.Currency;
                                tbo.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                            }

                            //tbo.LoginName = agentDetails.UserID;
                            //tbo.Password = agentDetails.Password;                        
                            bookingResponse = tbo.Ticket(ref itinerary);
                        }
                        #region Commented Old Code
                        /*else if (itinerary.FlightBookingSource == BookingSource.Indigo)
                        {
                            naviObj = new Navitaire("6E");
                            bookingResponse = naviObj.NavitaireBooking(itinerary, booking.AgencyId, member.FullName, sessionId, bookRequestXML);
                        }
                        else if (itinerary.FlightBookingSource == BookingSource.Paramount)
                        {
                            pmObj = new ParamountApi();
                            bookingResponse = pmObj.Book(ref itinerary, sessionId, ref pmTicketNo);
                        }
                        else if (itinerary.FlightBookingSource == BookingSource.AirDeccan)
                        {
                            adObj = new AirDeccanApi();
                            bookingResponse = adObj.BookReservation(itinerary, sessionId);
                        }
                        else if (itinerary.FlightBookingSource == BookingSource.Mdlr)
                        {
                            mdlr = new Mdlr();
                            bookingResponse = mdlr.Book(ref itinerary, sessionId, ref pmTicketNo);
                        }
                        else if (itinerary.FlightBookingSource == BookingSource.GoAir)
                        {
                            gaObj = new GoAirApi();
                            bookingResponse = gaObj.BookReservation(itinerary, sessionId);
                        }
                        else if (itinerary.FlightBookingSource == BookingSource.AirArabia)
                        {
                            aaObj = new AirArabia();
                            bookingResponse = aaObj.Booking(itinerary, booking.AgencyId, sessionId);
                        }else if (itinerary.FlightBookingSource == BookingSource.Sama)
                        {
                            sama = new Sama();
                            bookingResponse = sama.Book(ref itinerary, sessionId);
                        }
                        else if (itinerary.FlightBookingSource == BookingSource.HermesAirLine)
                        {
                            hApi = new HermesApi(sessionId);
                            bookingResponse = hApi.Book(itinerary, booking.AgencyId, ref pmTicketNo);
                        }*/
                        #endregion
                        else if (itinerary.FlightBookingSource == BookingSource.FlyDubai)
                        {
                            flyDubaiApi = new FlyDubaiApi(itinerary.Segments[0].Origin.AirportCode, sessionId);
                            flyDubaiApi.AppUserId = appUserId;
                            bookingResponse = flyDubaiApi.Book(itinerary, sessionId);
                        }
                        else
                        {
                            throw new ArgumentException("Unknown source of flight", "itinerary.FlightBookingSource");
                        }

                    }
                    // All catches are send to audit from Spice Jet API already, so showing only error message.
                    catch (BookingEngineException excep)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Book, CT.Core.Severity.High, 0, "Booking failed | error = " + excep.Message + "|" + DateTime.Now, "");
                        bookingResponse.Status = BookingResponseStatus.Failed;
                        bookingResponse.Error = "Unable to Book";
                        Basket.FlightBookingSession[sessionId].Log.Add("Unable to Book");
                        Basket.FlightBookingSession[sessionId].Excep.Add(Util.GetExceptionInformation((Exception)excep, string.Empty));
                        Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                    }
                    catch (System.Net.WebException excep)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Book, CT.Core.Severity.High, 0, "Booking failed | error = " + excep.Message + "|" + DateTime.Now, "");
                        bookingResponse.Status = BookingResponseStatus.Failed;
                        bookingResponse.Error = "Unable to Book";
                        Basket.FlightBookingSession[sessionId].Log.Add("Unable to Book");
                        Basket.FlightBookingSession[sessionId].Excep.Add(Util.GetExceptionInformation((Exception)excep, string.Empty));
                        Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                    }
                    catch (System.Web.Services.Protocols.SoapException excep)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Book, CT.Core.Severity.High, 0, "Booking failed | error = " + excep.Message + "|" + DateTime.Now, "");
                        bookingResponse.Status = BookingResponseStatus.Failed;
                        bookingResponse.Error = "Unable to Book";
                        Basket.FlightBookingSession[sessionId].Log.Add("Unable to Book");
                        Basket.FlightBookingSession[sessionId].Excep.Add(Util.GetExceptionInformation((Exception)excep, string.Empty));
                        Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                    }
                }
                // TODO: handle else. Generate error.


                if (bookingResponse.Status == BookingResponseStatus.Successful || bookingResponse.Status == BookingResponseStatus.BookedOther || bookingResponse.Status == BookingResponseStatus.ReturnFailed)
                {
                    // Sending mail to the user.
                    bool isHold = status == BookingStatus.Hold;
                    //SendMail(itinerary, member, isHold); Apply later  TODO ziyad


                    int turnPoint = 0;
                    for (int i = 0; i < itinerary.Segments.Length; i++)
                    {
                        if (itinerary.Destination == itinerary.Segments[i].Destination.CityCode || itinerary.Destination == itinerary.Segments[i].Destination.AirportCode)
                        {
                            turnPoint = i + 1;
                        }
                    }
                    DateTime returnDate = new DateTime();
                    if (itinerary.Segments.Length > turnPoint)
                    {
                        returnDate = itinerary.Segments[turnPoint].DepartureTime;
                    }
                    if (itinerary.FareRules != null)
                    {
                        for (int i = 0; i < itinerary.FareRules.Count; i++)
                        {
                            itinerary.FareRules[i].ReturnDate = returnDate;
                            itinerary.FareRules[i].DepartureTime = itinerary.Segments[0].DepartureTime;
                        }
                    }
                    // Getting fare Rules. (Also terminal info in case of spicejet or indigo)
                    // ziya to remove
                    // itinerary.FareRules = GetFareRule(itinerary); temporarly farerule commented as UAPI is showing error
                    Basket.FlightBookingSession[sessionId].Log.Add("Fare Rules obtained.");
                    if (bookingResponse.Status == BookingResponseStatus.BookedOther)
                    {
                        booking.Status = BookingStatus.Hold;
                        if (itinerary.BookingMode != BookingMode.Itimes)
                        {
                            for (int i = 0; i < itinerary.Passenger.Length; i++)
                            {
                                itinerary.Passenger[i].Price = AccountingEngine.AccountingEngine.GetPrice(itinerary, agencyId, i, 0);
                                //if (HttpContext.Current != null && HttpContext.Current.Session != null && Convert.ToBoolean(HttpContext.Current.Session["isB2B2BAgent"]))
                                //{
                                //    itinerary.IsDomestic = itinerary.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
                                //    itinerary.Passenger[i].Price = GetSubAgentPrice(itinerary, itinerary.Passenger[i].Price);
                                //}
                            }
                        }
                    }
                    //for (int i = 0; i < itinerary.Passenger.Length; i++)
                    //{
                    //    if (itinerary.Passenger[i].Type == PassengerType.Infant)
                    //    {
                    //        hasInfant = true;
                    //        break;
                    //    }
                    //}
                    //if (hasInfant)
                    //{
                    //    for (int i = 0; i < itinerary.Segments.Length; i++)
                    //    {
                    //        itinerary.Segments[i].ETicketEligible = false;
                    //    }
                    //}

                    CT.Core.Queue queue = new CT.Core.Queue();
                    queue.QueueTypeId = (int)QueueType.Booking;
                    queue.StatusId = (int)QueueStatus.Assigned;
                    queue.AssignedTo = (int)member.ID;
                    queue.AssignedBy = (int)member.ID;
                    queue.AssignedDate = DateTime.UtcNow;
                    queue.CompletionDate = DateTime.UtcNow.AddDays(TimeLimitToComplete);
                    queue.CreatedBy = (int)member.ID;

                    Comment ssrComment = new Comment();
                    ssrComment.AgencyId = booking.AgencyId;
                    ssrComment.CommentDescription = bookingResponse.SSRMessage;
                    ssrComment.CommentTypes = CommmentType.Booking;
                    //TODO: remove this hard coded 1. 1 is for administrator ID.
                    ssrComment.CreatedBy = 1;
                    ssrComment.IsPublic = true;

                    //TODO: For New product -- Start                       
                    //booking.ProductsList = new Product[1];
                    //booking.ProductsList[0] = (Product)itinerary;
                    //For New product -- End

                    // Saving the booked itinerary to database
                    //TODO: what to do if the transaction fails, the booking at GDS is already done.
                    try
                    {
                        itinerary.BookingMode = bookingMode;
                        itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
                        //itinerary.IsDomestic = itinerary.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
                        Airline airline = new Airline();
                        airline.Load(itinerary.Segments[0].Airline);
                        if (airline.IsLCC && isLcc)
                        {
                            booking.Status = BookingStatus.Ticketed;
                        }

                        BookingHistory bh = new BookingHistory();
                        string remarks = string.Empty;
                        bh.EventCategory = EventCategory.Booking;
                        string remark = string.Empty;
                        try
                        {
                            if (HttpContext.Current != null && HttpContext.Current.Session != null && Convert.ToBoolean(HttpContext.Current.Session["isB2B2BAgent"]))
                            {
                                Dictionary<string, string> b2b2bVariableList = new Dictionary<string, string>();
                                b2b2bVariableList = (Dictionary<string, string>)HttpContext.Current.Session["b2b2bVariable"];

                                remark = " By B2B2B Subagent Site URL " + b2b2bVariableList["b2b2bSiteName"] + " ,Subagent " + b2b2bVariableList["b2b2bSubAgencyName"];
                            }

                        }
                        catch (Exception)
                        {
                        }
                        if (booking.Status == BookingStatus.Hold)
                        {
                            remarks = "Booking is on hold" + remark;
                        }
                        else if (booking.Status == BookingStatus.Cancelled)
                        {
                            remarks = "Booked seat are released";
                        }
                        else if (booking.Status == BookingStatus.Ticketed)
                        {
                            bh.EventCategory = EventCategory.Ticketing;
                            remarks = "Ticket is created";
                        }
                        else if (booking.Status == BookingStatus.Ready)
                        {
                            remarks = "Booking is in ready state";
                        }
                        else if (booking.Status == BookingStatus.Inactive)
                        {
                            remarks = "Booking is Inactive";
                        }
                        else if (booking.Status == BookingStatus.InProgress || booking.Status == BookingStatus.OutsidePurchase)
                        {
                            remarks = "Booking is in-progress";
                        }
                        else
                        {
                            bh.EventCategory = EventCategory.Miscellaneous;
                            remarks = booking.Status.ToString();
                        }
                        if (ipAddr.Length > 0)
                        {
                            remarks += "(IP Address :- " + ipAddr + ")";
                        }
                        bh.Remarks = remarks;
                        bh.CreatedBy = (int)member.ID;

                        //System.IO.File.AppendAllText(timeLogFile, "\r\nBook,\t" + DateTime.Now.ToString("HH:mm:ss.ffffff") + ",\t");
                        int txnRetryCount = Convert.ToInt32(ConfigurationSystem.BookingEngineConfig["txnRetryCount"]);
                        int numberRetry = 0;
                        bool txnSucceded = false;
                        while (!txnSucceded && numberRetry < txnRetryCount)
                        {
                            numberRetry++;
                            try
                            {
                                using (TransactionScope updateTransaction = new TransactionScope())
                                {
                                    booking.Save(itinerary, false);
                                    if (!airline.IsLCC)
                                    {
                                        bh.BookingId = booking.BookingId;
                                        bh.Save();
                                    }

                                    if (bookingResponse.SSRDenied)
                                    {
                                        ssrComment.ParentId = booking.BookingId;
                                        ssrComment.AddComment();
                                    }
                                    queue.ItemId = booking.BookingId;
                                    queue.Save();

                                    if (itinerary.FlightBookingSource == BookingSource.UAPI)// To save Baggae after Hold
                                    {
                                        for (int i = 0; i < itinerary.Segments.Length; i++)
                                        {
                                            FlightInfo segment = itinerary.Segments[i];
                                            for (int j = 0; j < ptcDetails.Count; j++)
                                            {
                                                SegmentPTCDetail ptcDetail = ptcDetails[j];
                                                if (ptcDetail.FlightKey == segment.FlightKey)
                                                {
                                                    ptcDetail.SegmentId = segment.SegmentId;
                                                    ptcDetail.FlightKey = segment.FlightKey;

                                                    ptcDetail.Save();                                                    
                                                }
                                            }
                                        }
                                    }


                                    updateTransaction.Complete();
                                    txnSucceded = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                if (numberRetry < txnRetryCount)
                                {
                                    Basket.FlightBookingSession[sessionId].Log.Add("Retrying Booking Save" + numberRetry);
                                    booking.BookingId = 0;
                                    itinerary.FlightId = 0;
                                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                                    {
                                        itinerary.Passenger[i].PaxId = 0;
                                        itinerary.Passenger[i].Price.PriceId = 0;
                                    }
                                    for (int i = 0; i < itinerary.Segments.Length; i++)
                                    {
                                        itinerary.Segments[i].SegmentId = 0;
                                        foreach (SegmentPTCDetail ptcDetail in ptcDetails)
                                        {
                                            if (ptcDetail.FlightKey == itinerary.Segments[i].FlightKey)
                                            {
                                                ptcDetail.SegmentId = itinerary.Segments[i].Group;
                                            }
                                        }
                                    }                                    
                                    continue;
                                }
                                else
                                {
                                    throw ex;
                                }
                            }
                            // In the case of LCCs there is no seperate ticketing process.
                            // Once booking is made, it means ticket is generated.
                            if (airline.IsLCC && isLcc && itinerary.PNR != null && itinerary.PNR.Length > 0)
                            {
                                SaveTicketInfo(itinerary, (int)member.ID, agencyId, pmTicketNo, booking.BookingId, ipAddr, ticketData); // ticket save     
                            }
                            if (airline.IsLCC && isLcc && txnSucceded && (itinerary.BookingMode != BookingMode.Itimes || Convert.ToBoolean(ConfigurationSystem.BookingEngineConfig["invoiceForItimes"])))
                            {
                                try
                                {

                                    itinerary.LastModifiedBy = itinerary.CreatedBy;
                                    itinerary.IncrementETicketHit();
                                    // int invoiceNumber = AccountUtility.RaiseInvoice(itinerary, string.Empty, (int)member);// TODO ziyad
                                }
                                catch (Exception ex)
                                {
                                    Basket.FlightBookingSession[sessionId].Log.Add("Save Invoice failed");
                                    Basket.FlightBookingSession[sessionId].Excep.Add(Util.GetExceptionInformation(ex, string.Empty));
                                    Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                                    AgentMaster bookingAgency = new AgentMaster(agencyId);

                                    //UserMaster usermember = new UserMaster(member);
                                    string messageText = "PNR = " + bookingResponse.PNR + "\r\n Booking Source is " + itinerary.FlightBookingSource.ToString() + "\r\n Agency Name is " + bookingAgency.Name + "\r\n AgencyId is " + bookingAgency.ID + "\r\n MemberId is " + (int)member.ID + "\r\n Member Name is " + member.FirstName + member.LastName + "\r\n Member Email Address is " + member.Email + "\r\n. PNR created successfully but failed to save Invoice";
                                    string message = Util.GetExceptionInformation(ex, "Invoice save failed.\r\n" + messageText);
                                    try
                                    {
                                        Email.Send("Invoice save failed", message);
                                    }
                                    catch (System.Net.Mail.SmtpException) { } // Do nothing                                
                                    CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)member.ID, "Save Invoice failed. Error : " + ex.Message, "0");
                                }
                                //try
                                //{
                                //    Thread mailThread = new Thread(MetaSearchEngine.SendLowBalanceMail);
                                //    mailThread.Start(member);
                                //}
                                //catch (Exception)
                                //{ }
                            }

                        }
                        //System.IO.File.AppendAllText(timeLogFile, DateTime.Now.ToString("HH:mm:ss.ffffff") + " " + numberRetry.ToString());
                    }
                    catch (Exception ex)
                    {
                        #region Handling booking save failure
                        // Email - In case of Booking created successfully   
                        Basket.FlightBookingSession[sessionId].Log.Add("Save booking failed");
                        Basket.FlightBookingSession[sessionId].Excep.Add(Util.GetExceptionInformation(ex, string.Empty));
                        Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                        AgentMaster bookingAgency = new AgentMaster(agencyId);
                        //UserMaster userMember = new UserMaster(member);
                        string messageText = "PNR = " + bookingResponse.PNR + "\r\n Booking Source is " + itinerary.FlightBookingSource.ToString() + "\r\n Agency Name is " + bookingAgency.Name + "\r\n AgencyId is " + bookingAgency.ID + "\r\n MemberId is " + (int)member.ID + "\r\n Member Name is " + member.FirstName + member.LastName + "\r\n Member Email Address is " + member.Email + "\r\n. PNR created successfully but failed to save.";
                        string message = Util.GetExceptionInformation(ex, "Booking save failed.\r\n" + messageText);
                        if (bookingResponse.PNR != null && bookingResponse.PNR.Length > 0) //&& isLcc removed sai
                        {
                            booking.BookingId = 0;
                            itinerary.FlightId = 0;
                            for (int i = 0; i < itinerary.Passenger.Length; i++)
                            {
                                itinerary.Passenger[i].PaxId = 0;
                                itinerary.Passenger[i].Price.PriceId = 0;
                            }
                            for (int i = 0; i < itinerary.Segments.Length; i++)
                            {
                                itinerary.Segments[i].SegmentId = 0;
                            }
                            FailedBooking fb = new FailedBooking();
                            fb.Product = ProductType.Flight;
                            fb.PNR = bookingResponse.PNR;
                            fb.MemberId = (int)member.ID;
                            fb.AgencyId = (int)bookingAgency.ID;
                            fb.AgencyName = bookingAgency.Name;
                            fb.CurretnStatus = Status.NotSaved;
                            fb.Source = (BookingSource)Enum.Parse(typeof(BookingSource), itinerary.FlightBookingSource.ToString());
                            try
                            {
                                fb.ItineraryXML = FailedBooking.GetXML(itinerary);
                                fb.Save();
                            }
                            catch { }
                        }

                        try
                        {
                            SendMail(itinerary, member, (status == BookingStatus.Hold ? true : false), message, bookingResponse.Status);
                        }
                        catch (System.Net.Mail.SmtpException) { } // Do nothing
                        if (isLcc)
                        {
                            bookingResponse.Status = BookingResponseStatus.Failed;
                        }
                        else
                        {
                            if (itinerary.FlightBookingSource == BookingSource.UAPI)
                            {
                                MetaSearchEngine.CancelItinerary(itinerary.UniversalRecord, itinerary.FlightBookingSource);
                            }
                            else
                            {
                                MetaSearchEngine.CancelItinerary(itinerary.PNR, itinerary.FlightBookingSource);
                            }
                            bookingResponse = new BookingResponse(BookingResponseStatus.Failed, bookingResponse.Error, string.Empty);
                        }
                        CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)member.ID, "Save booking failed. Error : " + ex.Message, "0");

                        #endregion
                    }
                    if (itinerary.FlightBookingSource == BookingSource.Amadeus || itinerary.FlightBookingSource == BookingSource.WorldSpan || itinerary.FlightBookingSource == BookingSource.UAPI)
                    {
                        SaveSSR(itinerary);
                    }
                }
                else
                {
                    Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                    if (bookingResponse.Error != null && bookingResponse.Error.IndexOf("There was a problem communicating to the Reservation System.") >= 0 && itinerary.FlightBookingSource == BookingSource.SpiceJet)
                    {
                        AgentMaster agency = new AgentMaster(agencyId);
                        //UserMaster
                        bookingResponse.Error += "Your booking might have been confirmed, Please contact Cozmo Travel before trying to book again.";
                        Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["ErrorNotificationMailingId"], "Booking Failed. There was a problem communicating to the Reservation System.", bookingResponse.Error + itinerary.ToString() + "\nAgencyId is " + agency.Name + "(ID: " + agency.ID + ")\nLogged in Member is " + member.FirstName + "(" + (int)member.ID + ", " + member.LoginName + ")\n");
                        //Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["ErrorNotificationMailingId"], "SpiceJet Booking Failed. There was a problem communicating to the Reservation System.", bookingResponse.Error + itinerary.ToString() + "\nAgencyId is " + agency.Name + "(ID: " + agency.AgencyId + ")\nLogged in Member is " + member.FullName + "(" + (int)member + ", " + member.LoginName + ")\n");
                    }
                }

            ClearSession();

                bookingResponse.BookingId = booking.BookingId;
            }
            catch (Exception ex)
            {
                #region Handling booking save failure
                // Email - In case of Booking created successfully   
                Basket.FlightBookingSession[sessionId].Log.Add("Save booking failed");
                Basket.FlightBookingSession[sessionId].Excep.Add(Util.GetExceptionInformation(ex, string.Empty));
                Basket.FlightBookingSession[sessionId].DumpOnClean = true;
                AgentMaster bookingAgency = new AgentMaster(agencyId);
                //UserMaster userMember = new UserMaster(member);
                string messageText = "PNR = " + bookingResponse.PNR + "\r\n Booking Source is " + itinerary.FlightBookingSource.ToString() + "\r\n Agency Name is " + bookingAgency.Name + "\r\n AgencyId is " + bookingAgency.ID + "\r\n MemberId is " + (int)member.ID + "\r\n Member Name is " + member.FirstName + member.LastName + "\r\n Member Email Address is " + member.Email + "\r\n. PNR created successfully but failed to save.";
                string message = Util.GetExceptionInformation(ex, "Booking save failed.\r\n" + messageText);
                bookingResponse.Status = BookingResponseStatus.Failed;
                //if (bookingResponse.PNR != null && bookingResponse.PNR.Length > 0) //&& isLcc removed sai
                {
                    booking.BookingId = 0;
                    itinerary.FlightId = 0;
                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        itinerary.Passenger[i].PaxId = 0;
                        itinerary.Passenger[i].Price.PriceId = 0;
                    }
                    for (int i = 0; i < itinerary.Segments.Length; i++)
                    {
                        itinerary.Segments[i].SegmentId = 0;
                    }
                    FailedBooking fb = new FailedBooking();
                    fb.Product = ProductType.Flight;
                    fb.PNR = (bookingResponse.PNR != null ? bookingResponse.PNR : "");
                    fb.MemberId = (int)member.ID;
                    fb.AgencyId = (int)bookingAgency.ID;
                    fb.AgencyName = bookingAgency.Name;
                    fb.CurretnStatus = Status.NotSaved;
                    fb.Source = (BookingSource)Enum.Parse(typeof(BookingSource), itinerary.FlightBookingSource.ToString());
                    try
                    {
                        fb.ItineraryXML = FailedBooking.GetXML(itinerary);
                        fb.Save();
                    }
                    catch { }
                }

                try
                {
                    SendMail(itinerary, member, (status == BookingStatus.Hold ? true : false), message, bookingResponse.Status);
                }
                catch (System.Net.Mail.SmtpException) { } // Do nothing

                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)member.ID, "Save booking failed. Error : " + ex.Message, "0");
                throw ex;
                #endregion
            }
            return bookingResponse;
        }

        #region Hotel Cancel
        public Dictionary<string, string> CancelHotel(HotelItinerary itineary, List<DateTime> dates)
        {
            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            
            #region commented code (other sources)
            /*if (itineary.Source == HotelBookingSource.Desiya)
            {
                try
                {
                    DesiyaApi desiya = new DesiyaApi();
                    cancelInfo = desiya.CancelBooking(itineary, dates);
                }
                catch (BookingEngineException Ex)
                {
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    throw new Exception(exp.Message);
                }

            }
            if (itineary.Source == HotelBookingSource.GTA)
            {
                try
                {
                    GTA gtaApi = new GTA();
                    cancelInfo = gtaApi.CancelHotelBooking(itineary);

                }
                catch (BookingEngineException Ex)
                {
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    throw new Exception(exp.Message);
                }

            }
            if (itineary.Source == HotelBookingSource.HotelBeds)
            {
                try
                {
                    HotelBeds hBeds = new HotelBeds();
                    cancelInfo = hBeds.CancelBooking(itineary);
                }
                catch (BookingEngineException Ex)
                {
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    throw new Exception(exp.Message);
                }
            }
            if (itineary.Source == HotelBookingSource.Tourico)
            {
                try
                {
                    Tourico tourico = new Tourico();
                    cancelInfo = tourico.CancelBooking(itineary);
                }
                catch (BookingEngineException Ex)
                {
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    throw new Exception(exp.Message);
                }
            }
            if (itineary.Source == HotelBookingSource.IAN)
            {
                try
                {
                    IAN ian = new IAN();
                    cancelInfo = ian.CancelBooking(itineary);
                }
                catch (BookingEngineException Ex)
                {
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    throw new Exception(exp.Message);
                }
            }
            if (itineary.Source == HotelBookingSource.TBOConnect)
            {
                try
                {
                    HotelConnect hc = new HotelConnect();
                    cancelInfo = hc.CancelBooking(itineary);
                }
                catch (Exception exp)
                {
                    throw new Exception(exp.Message);
                }
            }
            if (itineary.Source == HotelBookingSource.Miki)
            {
                try
                {
                    MikiApi miki = new MikiApi();
                    cancelInfo = miki.CancelBooking(itineary);
                }
                catch (BookingEngineException Ex)
                {
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    throw new Exception(exp.Message);
                }
            }
            if (itineary.Source == HotelBookingSource.Travco)
            {
                try
                {
                    TravcoApi travco = new TravcoApi();
                    cancelInfo = travco.CancelBooking(itineary);
                }
                catch (BookingEngineException Ex)
                {
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    throw new Exception(exp.Message);
                }
            }
       
            if (itineary.Source == HotelBookingSource.WST)
            {
                try
                {
                    WST wst = new WST();
                    cancelInfo = wst.CancelBooking(itineary);
                }
                catch (BookingEngineException Ex)
                {
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    throw new Exception(exp.Message);
                }
            }*/
#endregion

            if (itineary.Source == HotelBookingSource.DOTW)
            {
                try
                {
                    CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi();
                    dotw.AppUserId = (int)Settings.LoginInfo.UserID;
                    cancelInfo = dotw.CancelBooking(itineary);
                }
                catch (BookingEngineException Ex)
                {
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    throw new Exception(exp.Message);
                }
            }
            else if (itineary.Source == HotelBookingSource.RezLive)
            {
                try
                {
                    RezLive.XmlHub rezAPI = new RezLive.XmlHub();
                    cancelInfo = rezAPI.CancelHotelBooking(itineary.ConfirmationNo, itineary.BookingRefNo);
                }
                catch (BookingEngineException Ex)
                {
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    throw new Exception(exp.Message);
                }
            }
            return cancelInfo;
        }

        #endregion
        #region Transfer Cancel
        public string CancelTransfer(TransferItinerary itineary, List<DateTime> dates, int bookingId, long member)
        {
            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            string message = string.Empty;
            try
            {

                if (itineary.Source == TransferBookingSource.GTA)
                {
                    try
                    {
                        GTA gtaApi = new GTA();
                        cancelInfo = gtaApi.CancelTransferBooking(itineary);

                    }
                    catch (BookingEngineException Ex)
                    {
                        throw new BookingEngineException(Ex.Message);
                    }
                    catch (Exception exp)
                    {
                        throw new Exception(exp.Message);
                    }

                }
                if (itineary.BookingStatus == TransferBookingStatus.Cancelled || itineary.BookingStatus == TransferBookingStatus.Failed)
                {
                    message = "Already Refunded";
                    return message;
                }
                string remarks = "Refunded for Voucher No -" + itineary.ConfirmationNo;
                if (cancelInfo.ContainsKey("ID"))
                {
                    int loggedMemberId = (int)member;
                    if (cancelInfo["Status"].ToUpper() == "CANCELLED")
                    {
                        itineary.BookingStatus = TransferBookingStatus.Cancelled;
                    }
                    else
                    {
                        itineary.BookingStatus = TransferBookingStatus.Pending;
                    }
                    BookingDetail bookingInfo = new BookingDetail(bookingId);
                    int agencyId = bookingInfo.AgencyId;
                    bookingInfo.SetBookingStatus(BookingStatus.Cancelled, loggedMemberId);
                    itineary.CancelId = cancelInfo["ID"];
                    itineary.UpdateBookingStatus();
                    CT.Core.Audit.Add(CT.Core.EventType.TransferCancel, CT.Core.Severity.High, loggedMemberId, "Transfer Cancellation Details:" + " | Cancellation ID:" + cancelInfo["ID"] + "| Amount:" + cancelInfo["Currency"] + ". " + cancelInfo["Amount"], "");
                    message = "Refund succeeded";
                }
                else
                {
                    message = "Unable to Cancel the Booking";
                    return message;
                }
            }
            catch (BookingEngineException)
            {
                message = "Error: There is some error.";
                return message;

            }
            catch (Exception exp)
            {
                message = exp.Message;
                CT.Core.Audit.Add(EventType.TransferBooking, CT.Core.Severity.High, 1, "Excception while Cancelling Transfers...Message:" + exp.Message, "");
            }

            return message;
        } 
        #endregion
        /* #region Sightseeing Cancel
        public string CancelSightseeing(SightseeingItinerary itineary, List<DateTime> dates, int bookingId, long member) // ziya-todo
        {
            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            string message = string.Empty;
            try
            {

                if (itineary.Source == SightseeingBookingSource.GTA)
                {
                    try
                    {
                        GTA gtaApi = new GTA();
                        cancelInfo = gtaApi.CancelSightseeingBooking(itineary);

                    }
                    catch (BookingEngineException Ex)
                    {
                        throw new BookingEngineException(Ex.Message);
                    }
                    catch (Exception exp)
                    {
                        throw new Exception(exp.Message);
                    }

                }
                if (itineary.BookingStatus == SightseeingBookingStatus.Cancelled || itineary.BookingStatus == SightseeingBookingStatus.Failed)
                {
                    message = "Already Refunded";
                    return message;
                }
                string remarks = "Refunded for Voucher No -" + itineary.ConfirmationNo;
                if (cancelInfo.ContainsKey("ID"))
                {
                    int loggedMemberId = (int)member;
                    if (cancelInfo["Status"].ToUpper() == "CANCELLED")
                    {
                        itineary.BookingStatus = SightseeingBookingStatus.Cancelled;
                    }
                    else
                    {
                        itineary.BookingStatus = SightseeingBookingStatus.Pending;
                    }
                    BookingDetail bookingInfo = new BookingDetail(bookingId);
                    int agencyId = bookingInfo.AgencyId;
                    bookingInfo.SetBookingStatus(BookingStatus.Cancelled, loggedMemberId);
                    itineary.CancelId = cancelInfo["ID"];
                    itineary.UpdateBookingStatus();
                    CT.Core.Audit.Add(CT.Core.EventType.SightseeingCancel, CT.Core.Severity.High, loggedMemberId, "Sightseeing Cancellation Details:" + " | Cancellation ID:" + cancelInfo["ID"] + "| Amount:" + cancelInfo["Currency"] + ". " + cancelInfo["Amount"], "");
                    message = "Refund succeeded";
                }
                else
                {
                    message = "Unable to Cancel the Booking";
                    return message;
                }
            }
            catch (BookingEngineException)
            {
                message = "Error: There is some error.";
                return message;

            }
            catch (Exception exp)
            {
                message = exp.Message;
                CT.Core.Audit.Add(EventType.SightseeingBooking, CT.Core.Severity.High, 1, "Excception while Cancelling Sightseeings...Message:" + exp.Message, "");
            }

            return message;
        }
        #endregion
          
        public void SaveTicketInfo(FlightItinerary itinerary, int memberId, int agencyId, List<string> pmTicketNo, int bookingId, string ipAddr, Dictionary<string, string> ticketData)
        {
            //send SMS  for ETicket generation
            if (SMS.IsSMSSubscribed(agencyId, SMSEvent.ETicketGeneration))
            {
                Hashtable hTable = new Hashtable();
                hTable.Add("AgencyId", agencyId);
                string pnrInfo = string.Empty;
                int count = 1;
                pnrInfo += "PNR:" + itinerary.PNR + " ";
                foreach (FlightInfo segment in itinerary.Segments)
                {
                    pnrInfo += segment.Origin.CityCode + "-" + segment.Destination.CityCode + ":Flight " + segment.Airline + "-" + segment.FlightNumber + "(" + segment.DepartureTime.ToString("ddMMMyy HH:mm") + ")";
                    if (count < itinerary.Segments.Length)
                        pnrInfo += ", ";
                    count++;
                }
                hTable.Add("PNR", pnrInfo);
                if (itinerary.Passenger[0].FullName.Length <= 20)
                {
                    hTable.Add("PaxName", itinerary.Passenger[0].FullName);
                }
                else
                {
                    hTable.Add("PaxName", itinerary.Passenger[0].FirstName);
                }
                hTable.Add("EventType", SMSEvent.ETicketGeneration);
                SMS.SendSMSThread(hTable);
            }
            if (SMS.IsSMSSubscribed(agencyId, SMSEvent.ETicketforPassenger))
            {
                Hashtable hTable = new Hashtable();
                hTable.Add("AgencyId", agencyId);
                string pnrInfo = string.Empty;
                int count = 1;
                pnrInfo += "PNR:" + itinerary.PNR + " ";
                foreach (FlightInfo segment in itinerary.Segments)
                {
                    pnrInfo += segment.Origin.CityCode + "-" + segment.Destination.CityCode + ":Flight " + segment.Airline + "-" + segment.FlightNumber + "(" + segment.DepartureTime.ToString("ddMMMyy HH:mm") + ")";
                    if (count < itinerary.Segments.Length)
                        pnrInfo += ", ";
                    count++;
                }
                hTable.Add("PNR", pnrInfo);
                if (itinerary.Passenger[0].FullName.Length <= 20)
                {
                    hTable.Add("PaxName", itinerary.Passenger[0].FullName);
                }
                else
                {
                    hTable.Add("PaxName", itinerary.Passenger[0].FirstName);
                }
                hTable.Add("MobileNo", itinerary.Passenger[0].CellPhone);
                hTable.Add("EventType", SMSEvent.ETicketforPassenger);
                SMS.SendSMSThread(hTable);
            }


            Ticket[] ticket = GetTicketForLCC(itinerary, memberId, agencyId, pmTicketNo, bookingId, ticketData);

            Airline air = new Airline();
            air.Load(itinerary.ValidatingAirlineCode);
            string supplierCode = string.Empty;
            if (ticket.Length > 0)
            {
                if (ticket[0].SupplierId > 0)
                {
                    supplierCode = Supplier.GetSupplierCodeById(ticket[0].SupplierId);
                }
            }
            for (int i = 0; i < ticket.Length; i++)
            {
                ticket[i].SupplierRemmitance = new SupplierRemmitance();
                ticket[i].SupplierRemmitance.PublishedFare = itinerary.Passenger[i].Price.PublishedFare;
                ticket[i].SupplierRemmitance.AccpriceType = itinerary.Passenger[i].Price.AccPriceType;
                ticket[i].SupplierRemmitance.NetFare = itinerary.Passenger[i].Price.NetFare;
                ticket[i].SupplierRemmitance.CessTax = 0;
                ticket[i].SupplierRemmitance.Currency = itinerary.Passenger[i].Price.Currency;
                ticket[i].SupplierRemmitance.CurrencyCode = itinerary.Passenger[i].Price.CurrencyCode;
                ticket[i].SupplierRemmitance.Markup = itinerary.Passenger[i].Price.Markup;
                ticket[i].SupplierRemmitance.OtherCharges = itinerary.Passenger[i].Price.OtherCharges;
                ticket[i].SupplierRemmitance.OurCommission = itinerary.Passenger[i].Price.OurCommission;
                ticket[i].SupplierRemmitance.OurPlb = itinerary.Passenger[i].Price.OurPLB;
                ticket[i].SupplierRemmitance.RateOfExchange = itinerary.Passenger[i].Price.RateOfExchange;
                ticket[i].SupplierRemmitance.ServiceTax = itinerary.Passenger[i].Price.SeviceTax;
                ticket[i].SupplierRemmitance.SupplierCode = supplierCode;

                ticket[i].SupplierRemmitance.Tax = itinerary.Passenger[i].Price.Tax;
                ticket[i].SupplierRemmitance.TdsCommission = 0;
                ticket[i].SupplierRemmitance.TdsPLB = 0;
                ticket[i].SupplierRemmitance.CreatedBy = ticket[i].CreatedBy;
                ticket[i].SupplierRemmitance.CreatedOn = DateTime.UtcNow;
                ticket[i].SupplierRemmitance.CommissionType = air.CommissionType;
            }

            int txnRetryCount = Convert.ToInt32(ConfigurationSystem.BookingEngineConfig["txnRetryCount"]);
            int numberRetry = 0;
            bool txnSucceded = false;
            while (!txnSucceded && numberRetry < txnRetryCount)
            {
                numberRetry++;
                try
                {
                    using (TransactionScope autoTicketScope = new TransactionScope())
                    {
                        for (int i = 0; i < ticket.Length; i++)
                        {
                            ticket[i].Save();
                            ticket[i].SupplierRemmitance.TicketId = ticket[i].TicketId;
                            ticket[i].SupplierRemmitance.Save();
                            for (int j = 0; j < ticket[i].PtcDetail.Count; j++)
                            {
                                ticket[i].PtcDetail[j].Save();
                            }
                        }
                        autoTicketScope.Complete();
                        txnSucceded = true;
                    }
                }
                catch (Exception ex)
                {
                    if (numberRetry < txnRetryCount)
                    {
                        for (int i = 0; i < ticket.Length; i++)
                        {
                            ticket[i].TicketId = 0;
                            ticket[i].SupplierRemmitance.SupplierRemmitanceId = 0;
                        }
                        continue;
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            string remarks = string.Empty;
            try
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null && Convert.ToBoolean(HttpContext.Current.Session["isB2B2BAgent"]))
                {
                    Dictionary<string, string> b2b2bVariableList = new Dictionary<string, string>();
                    b2b2bVariableList = (Dictionary<string, string>)HttpContext.Current.Session["b2b2bVariable"];


                    remarks = " By B2B2B Subagent Site URL " + b2b2bVariableList["b2b2bSiteName"] + " ,Subagent " + b2b2bVariableList["b2b2bSubAgencyName"];
                }

            }
            catch (Exception)
            {
            }
            BookingHistory bookingHistory = new BookingHistory();
            bookingHistory.BookingId = bookingId;
            bookingHistory.EventCategory = EventCategory.Ticketing;
            bookingHistory.Remarks = "Ticket Created" + remarks + " (IP Address - " + ipAddr + ")";
            bookingHistory.CreatedBy = memberId;
            bookingHistory.Save();
            #region Getting TBOConnect results for Cache
            //if (Convert.ToBoolean(ConfigurationSystem.HotelConnectConfig["TBOConnectOnETicket"]))
            //{
            //    try
            //    {
            //        Thread ResultThread = new Thread
            //        (
            //            delegate()
            //            {
            //                //SearchResultTBOConnectForCache(itinerary, memberId);
            //            }
            //        );
            //        ResultThread.Start();
            //        ResultThread.IsBackground = true;
            //    }
            //    catch { }
            //}
            #endregion
        }
        */
        /* private void SearchResultTBOConnectForCache(FlightItinerary itinerary, int memberId)
         {
             try
             {
                 RoomGuestData[] guestInfo = new RoomGuestData[0];
                 List<string> sources = new List<string>();
                 CT.Core.City city = new City();
                 sources.Add("TBOConnect");
                 HotelRequest req = new HotelRequest();
                 city.Load(itinerary.Destination);
                 req.CityName = city.CityName;
                 req.IsDomestic = true;
                 req.Sources = sources;
                 req.StartDate = itinerary.TravelDate.Date;
                 req.EndDate = req.StartDate.AddDays(1).Date;
                 req.CityId = HotelCity.GetCityIdFromCityName(req.CityName);
                 req.NoOfRooms = 1;
                 req.IsMultiRoom = false;
                 guestInfo = new RoomGuestData[req.NoOfRooms];
                 guestInfo[0].noOfChild = 0;
                 guestInfo[0].childAge = null;
                 guestInfo[0].noOfAdults = 1;
                 req.CountryName = Technology.Configuration.ConfigurationSystem.LocaleConfig["CountryName"];
                 req.RoomGuest = guestInfo;
                 req.HotelName = string.Empty;
                 req.MinRating = 0;
                 req.MaxRating = 5;
                 UserMaster mem = new UserMaster(memberId);
                
                 HotelSearchResult[] result = new HotelSearchResult[0];
                 result = GetHotelResults(req, mem);
                 HotelSearchResult res = new HotelSearchResult();
                 //res.Save(sessionId, result, req.NoOfRooms);
                 res.Save(result, city.CityName);
             }
             catch { }
         }
         **/
        public Ticket[] GetTicketForLCC(FlightItinerary itinerary, int memberId, int agencyId, List<string> pmTicketNo, int bookingId, Dictionary<string, string> lccTicketData)
        {
            List<string> airlineNameRemarksList = new List<string>();
            List<string> airlineNameFareRuleList = new List<string>();
            string remarks = string.Empty;
            string fareRule = string.Empty;
            UpdateAirlinePNR(itinerary);
            UserPreference preference = new UserPreference();
            Ticket[] ticket = new Ticket[itinerary.Passenger.Length];
            //Airline service fee for Email Itinerary
            Dictionary<string, string> sFeeTypeList = new Dictionary<string, string>();
            Dictionary<string, string> sFeeValueList = new Dictionary<string, string>();
            FlightItinerary.GetMemberPrefServiceFee(agencyId, ref sFeeTypeList, ref sFeeValueList);

            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                ticket[i] = new Ticket();
                ticket[i].Status = "OK";
                string baggage = ConfigurationSystem.SpiceJetConfig["Baggage"];

                if (itinerary.FlightBookingSource == BookingSource.Indigo)
                {
                    ticket[i].TicketNumber = itinerary.PNR;
                    baggage = ConfigurationSystem.IndigoConfig["Baggage"];
                }
                else if (itinerary.FlightBookingSource == BookingSource.AirDeccan)
                {
                    ticket[i].TicketNumber = itinerary.PNR;
                    baggage = ConfigurationSystem.AirDeccanConfig["Baggage"];
                }
                else if (itinerary.FlightBookingSource == BookingSource.Paramount)
                {
                    ticket[i].TicketNumber = pmTicketNo[i];
                    baggage = ConfigurationSystem.ParamountConfig["baggage"];
                }
                else if (itinerary.FlightBookingSource == BookingSource.Mdlr)
                {
                    ticket[i].TicketNumber = pmTicketNo[i];
                    baggage = ConfigurationSystem.MdlrConfig["baggage"];
                }
                else if (itinerary.FlightBookingSource == BookingSource.GoAir)
                {
                    ticket[i].TicketNumber = itinerary.PNR;
                    baggage = ConfigurationSystem.GoAirConfig["Baggage"];
                }
                else if (itinerary.FlightBookingSource == BookingSource.AirArabia)
                {
                    ticket[i].TicketNumber = itinerary.PNR;
                    // baggage = ConfigurationSystem.AirArabiaConfig["Baggage"]; //not define yet
                }
                else if (itinerary.FlightBookingSource == BookingSource.Sama)
                {
                    ticket[i].TicketNumber = itinerary.PNR;
                    baggage = ConfigurationSystem.SamaConfig["baggage"];
                }
                else if (itinerary.FlightBookingSource == BookingSource.FlyDubai)
                {
                    ticket[i].TicketNumber = itinerary.PNR;                    
                }
                else if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                {
                    ticket[i].TicketNumber = itinerary.PNR;
                    ticket[i].StockType = itinerary.Passenger[i].CategoryId;//Assign TicketId's returned from TBO to be used for cancellation 
                }
                else
                {
                    ticket[i].TicketNumber = itinerary.PNR; // In case of Spice Jet we are saving PNR as Ticket Number
                }

                ticket[i].CreatedBy = memberId;
                ticket[i].PaxId = itinerary.Passenger[i].PaxId;
                ticket[i].PaxType = itinerary.Passenger[i].Type;
                ticket[i].PaxFirstName = itinerary.Passenger[i].FirstName;
                ticket[i].PaxLastName = itinerary.Passenger[i].LastName;
                ticket[i].Title = itinerary.Passenger[i].Title;//Title was not assigned.
                ticket[i].IssueDate = DateTime.Now;
                ticket[i].TaxBreakup = new List<KeyValuePair<string, decimal>>();
                ticket[i].TaxBreakup.Add(new KeyValuePair<string, decimal>("TotalTax", itinerary.Passenger[i].Price.Tax));

                ticket[i].FlightId = itinerary.FlightId;

                ticket[i].Price = itinerary.Passenger[i].Price;
                ticket[i].ETicket = true;
                ticket[i].ValidatingAriline = itinerary.ValidatingAirline;
                ticket[i].IssueInExchange = string.Empty;

                if (itinerary.TourCode != null && itinerary.TourCode.Length > 0)
                {
                    ticket[i].TourCode = itinerary.TourCode;

                }
                else if (lccTicketData != null && lccTicketData.ContainsKey("tourCode") && lccTicketData["tourCode"].Length > 0)
                {
                    ticket[i].TourCode = lccTicketData["tourCode"];
                }
                else
                {
                    ticket[i].TourCode = string.Empty;
                }
                if (itinerary.Endorsement != null && itinerary.Endorsement.Length > 0)
                {
                    ticket[i].Endorsement = itinerary.Endorsement;
                }
                else if (lccTicketData != null && lccTicketData.ContainsKey("endorsement") && lccTicketData["endorsement"].Length > 0)
                {
                    ticket[i].Endorsement = lccTicketData["endorsement"];
                }
                else
                {
                    ticket[i].Endorsement = string.Empty;
                }
                if (lccTicketData != null && lccTicketData.ContainsKey("corporateCode") && lccTicketData["corporateCode"].Length > 0)
                {
                    ticket[i].CorporateCode = lccTicketData["corporateCode"];
                }
                else
                {
                    ticket[i].CorporateCode = string.Empty;
                }
                if (lccTicketData != null && lccTicketData.ContainsKey("remarks") && lccTicketData["remarks"].Length > 0)
                {
                    ticket[i].Remarks = lccTicketData["remarks"];
                }
                else
                {
                    ticket[i].Remarks = string.Empty;
                }

                ticket[i].OriginalIssue = string.Empty;
                ticket[i].FareCalculation = string.Empty;
                int agencyPrimaryMemberId = new UserMaster(memberId).AgentId;

                if (itinerary.BookingMode == BookingMode.WhiteLabel || itinerary.BookingMode == BookingMode.Itimes)
                {
                    if (itinerary.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + ""))
                    {
                        int chargeType = 1;
                        decimal chargeValue = 0;
			// TO DO Ziyad... adding service fee as per new logic
                        //AirlineServiceFee asf = AirlineServiceFee.Load(itinerary.Segments[0].Airline, agencyId);
                        //if (asf.AirlineCode != null && asf.AirlineCode != "" && asf.IsActive)
                        //{
                        //    chargeType = Convert.ToInt32(asf.ServiceFeeType);
                        //    chargeValue = asf.ServiceFee;
                        //}
                        //else
                        //{
                        //    chargeType = Convert.ToInt32(preference.GetPreference(agencyPrimaryMemberId, WLPreferenceKeys.ServiceChargeType, "Booking & Confirmation").Value);
                        //    chargeValue = Convert.ToDecimal(preference.GetPreference(agencyPrimaryMemberId, WLPreferenceKeys.ServiceCharge, "Booking & Confirmation").Value);
                        //}
                        if (chargeType == 1)
                        {
                            ticket[i].ServiceFee += chargeValue;
                        }
                        else
                        {
                            ticket[i].ServiceFee += (ticket[i].Price.PublishedFare + ticket[i].Price.Tax) * (chargeValue / 100);
                        }
                    }
                    else
                    {
                        int chargeType = Convert.ToInt32(preference.GetPreference(agencyPrimaryMemberId, WLPreferenceKeys.IntlServiceChargeType, "Booking & Confirmation").Value);
                        decimal chargeValue = Convert.ToDecimal(preference.GetPreference(agencyPrimaryMemberId, WLPreferenceKeys.IntlServiceCharge, "Booking & Confirmation").Value);
                        if (chargeType == 1)
                        {
                            ticket[i].ServiceFee += chargeValue;
                        }
                        else
                        {
                            ticket[i].ServiceFee += (ticket[i].Price.PublishedFare + ticket[i].Price.Tax) * (chargeValue / 100);
                        }
                    }
                }
                else
                {
                    string sfType = string.Empty;
                    decimal sfValue = 0;
                    if (!itinerary.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + ""))
                    {
                        if (sFeeTypeList.ContainsKey("INTL") && sFeeValueList.ContainsKey("INTL"))
                        {
                            sfType = sFeeTypeList["INTL"];
                            sfValue = Convert.ToDecimal(sFeeValueList["INTL"]);
                        }
                    }
                    else if (sFeeTypeList.ContainsKey(itinerary.ValidatingAirlineCode) && sFeeValueList.ContainsKey(itinerary.ValidatingAirlineCode))
                    {
                        sfType = sFeeTypeList[itinerary.ValidatingAirlineCode];
                        sfValue = Convert.ToDecimal(sFeeValueList[itinerary.ValidatingAirlineCode]);
                    }
                    if (sfType == "FIXED")
                    {
                        ticket[i].ServiceFee = sfValue;
                    }
                    else if (sfType == "PERCENTAGE")
                    {
                        decimal totalfare = ticket[i].Price.PublishedFare + ticket[i].Price.Tax + ticket[i].Price.OtherCharges + ticket[i].Price.AdditionalTxnFee + ticket[i].Price.TransactionFee;
                        ticket[i].ServiceFee = totalfare * sfValue / 100;
                    }
                    else
                    {
                        ticket[i].ServiceFee = 0;
                    }
                }
                //preference = preference.GetPreference(agencyPrimaryMemberId, UserPreference.ServiceFee, UserPreference.ServiceFee);
                if (preference.Value != null)
                {
                    if (preference.Value == UserPreference.ServiceFeeInTax)
                    {
                        ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowInTax;
                    }
                    else if (preference.Value == UserPreference.ServiceFeeShow)
                    {
                        ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowSeparately;
                    }
                }
                else
                {
                    ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowInTax;
                }
                // To get remarks and fare rule.
                int consolidatorId = 0;
                foreach (FlightInfo segment in itinerary.Segments)
                {
                    string airlineCode = segment.Airline;
                    Airline airline = new Airline();
                    airline.Load(airlineCode);
                    if (airlineCode == itinerary.ValidatingAirlineCode)
                    {
                        consolidatorId = airline.ConsolidatorId;
                    }
                    if (!airlineNameRemarksList.Contains(airline.AirlineName) && airline.Remarks.Length != 0)
                    {
                        airlineNameRemarksList.Add(airline.AirlineName);
                        remarks += "<span style=\"font-weight:bold\">" + airline.AirlineName + "</span>" + "&nbsp;:&nbsp;" + airline.Remarks + "<br />";
                    }
                    if (!airlineNameFareRuleList.Contains(airline.AirlineName) && airline.FareRule.Length != 0)
                    {
                        airlineNameFareRuleList.Add(airline.AirlineName);
                        fareRule += "<span style=\"font-weight:bold\">" + airline.AirlineName + "</span>" + "&nbsp;:&nbsp;" + airline.FareRule + "<br />";
                    }
                }
                ticket[i].SupplierId = consolidatorId;
                ticket[i].FareRule = fareRule;

                ticket[i].PtcDetail = new List<SegmentPTCDetail>();
                for (int f = 0; f < itinerary.Segments.Length; f++)
                {
                    ticket[i].PtcDetail.Add(new SegmentPTCDetail());
                    ticket[i].PtcDetail[f].SegmentId = itinerary.Segments[f].SegmentId;

                    // Picking LCC baggage information from config
                    if (itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                        {
                            if (ticket[i].PtcDetail[f].Baggage != null)
                            {
                                ticket[i].PtcDetail[f].Baggage = itinerary.Passenger[i].BaggageCode.Split(',')[f];
                            }
                        }
                        else
                        {
                            if (ticket[i].PtcDetail[f].Baggage != null)
                            {
                                ticket[i].PtcDetail[f].Baggage = itinerary.Passenger[i].BaggageCode;
                            }
                        }
                    }
                    else
                    {
                        if (ticket[i].PtcDetail[f].Baggage != null)
                        {
                            ticket[i].PtcDetail[f].Baggage = baggage;
                        }
                    }
                    ticket[i].PtcDetail[f].PaxType = FlightPassenger.GetPTC(ticket[i].PaxType);
                    if (itinerary.FareRules.Count > 0)
                    {
                        ticket[i].PtcDetail[f].FareBasis = itinerary.FareRules[0].FareBasisCode;
                    }
                    else
                    {
                        ticket[i].PtcDetail[f].FareBasis = "";
                    }
                }
            }
            return ticket;
        }
        /*
        private void SaveManualTicketInfo(FlightItinerary itinerary, int memberId, int agencyId, Ticket[] ticket, int bookingId)
        {
            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                ticket[i].Status = "OK";
                string baggage = ConfigurationSystem.SpiceJetConfig["Baggage"];
                ticket[i].PaxId = itinerary.Passenger[i].PaxId;
                ticket[i].IssueDate = DateTime.UtcNow;
                ticket[i].FlightId = itinerary.FlightId;
                int paxIndexPricing = i;
                ticket[i].Price = itinerary.Passenger[i].Price;

                ticket[i].Endorsement = string.Empty;
                ticket[i].FareCalculation = string.Empty;
                ticket[i].TicketId = 0;
                ticket[i].Save();

                ticket[i].SupplierRemmitance.TicketId = ticket[i].TicketId;
                ticket[i].SupplierRemmitance.Save();


                ticket[i].PtcDetail = new List<SegmentPTCDetail>();

                for (int f = 0; f < itinerary.Segments.Length; f++)
                {
                    ticket[i].PtcDetail.Add(new SegmentPTCDetail());
                    ticket[i].PtcDetail[f].SegmentId = itinerary.Segments[f].SegmentId;

                    // Picking LCC baggage information from config
                    ticket[i].PtcDetail[f].Baggage = baggage;
                    ticket[i].PtcDetail[f].PaxType = FlightPassenger.GetPTC(ticket[i].PaxType);
                    ticket[i].PtcDetail[f].FareBasis = itinerary.FareRules[0].FareBasisCode;
                    ticket[i].PtcDetail[f].Save();
                }

                BookingHistory bookingHistory = new BookingHistory();
                bookingHistory.BookingId = bookingId;
                string remarks = string.Empty;
                bookingHistory.EventCategory = EventCategory.Ticketing;
                if (BookingMode.ManualImport == (BookingMode)itinerary.BookingMode)
                {
                    bookingHistory.Remarks = "Manual Ticket Created for passenger via Import PNR.";
                }
                else
                {
                    bookingHistory.Remarks = "Manual Ticket Created for passenger.";
                }
                bookingHistory.CreatedBy = memberId;
                bookingHistory.Save();
            }

        }


        private bool AutoImport(ref FlightItinerary itinerary, long member, int agencyId, bool isLcc)
        {
            int TimeLimitToComplete = 7;
            string fareBasisCodeOB = string.Empty;
            string fareBasisCodeIB = string.Empty;
            string fareCode = string.Empty;
            List<string> ticketNumbers = new List<string>();
            Agency agency = new Agency(agencyId);
            BookingDetail booking = new BookingDetail();

            Ticket[] ticket = new Ticket[0];
            if (itinerary.FlightBookingSource == BookingSource.WorldSpan)
            {
                //For world span
                itinerary = Worldspan.RetrieveItinerary(itinerary.PNR, out ticket);
            }
            else if (itinerary.FlightBookingSource == BookingSource.SpiceJet)
            {
                // For Spice Jet
                SpiceJetAPI sp = new SpiceJetAPI();
                itinerary = sp.RetrieveItinerary(itinerary.PNR, ref fareBasisCodeOB, ref fareBasisCodeIB);
                ticket = new Ticket[0];
            }
            else if (itinerary.FlightBookingSource == BookingSource.Indigo)
            {
                // For Spice Jet
                Navitaire navi = new Navitaire("6E");
                itinerary = navi.RetrieveItinerary(itinerary.PNR, ref fareBasisCodeOB, ref fareBasisCodeIB);
                ticket = new Ticket[0];
            }
            else if (itinerary.FlightBookingSource == BookingSource.Paramount)
            {
                // For Paramount
                itinerary = ParamountApi.RetrieveItinerary(itinerary.PNR, ref fareCode, ref ticketNumbers);
                itinerary.FareRules = ParamountApi.GetFareRuleScreenScrape(itinerary.Origin, itinerary.Destination, fareCode);
                ticket = new Ticket[0];
            }
            else if (itinerary.FlightBookingSource == BookingSource.AirDeccan)
            {
                // For Air Deccan
                AirDeccanApi airDeccan = new AirDeccanApi();
                itinerary = airDeccan.RetrieveItinerary(itinerary.PNR, ref fareBasisCodeOB);
                itinerary.FareRules = AirDeccanApi.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB);
                ticket = new Ticket[0];
            }
            else if (itinerary.FlightBookingSource == BookingSource.Amadeus)
            {
                itinerary = Amadeus.RetrieveItinerary(itinerary.PNR, out ticket);
            }
            else if (itinerary.FlightBookingSource == BookingSource.Mdlr)
            {
                // For MDLR
                itinerary = Mdlr.RetrieveItinerary(itinerary.PNR, ref fareCode, ref ticketNumbers);
                itinerary.FareRules = Mdlr.GetFareRule(itinerary.Origin, itinerary.Destination, fareCode);
                ticket = new Ticket[0];
            }
            itinerary.BookingMode = BookingMode.Auto;

            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                if (itinerary.Passenger[i].Price != null)
                {
                    itinerary.Passenger[i].Price = AccountingEngine.AccountingEngine.GetPrice(itinerary, agency.AgencyId, i, 0);
                }
                else
                {
                    itinerary.Passenger[i].Price = new PriceAccounts();
                }
                itinerary.Passenger[i].CreatedBy = (int)member;

            }
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                if (itinerary.Segments[i] != null)
                {
                    itinerary.Segments[i].CreatedBy = (int)member;
                }
            }

            List<FareRule> fareRule = new List<FareRule>();
            if (itinerary.FlightBookingSource == BookingSource.WorldSpan)
            {
                fareRule = Worldspan.GetFareRuleList(itinerary.FareRules);
            }
            else if (itinerary.FlightBookingSource == BookingSource.SpiceJet)
            {
                fareRule = SpiceJetAPI.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB);
            }
            else if (itinerary.FlightBookingSource == BookingSource.Indigo)
            {
                fareRule = Navitaire.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB, "6E");
            }
            else if (itinerary.FlightBookingSource == BookingSource.Paramount)
            {
                fareRule = itinerary.FareRules;
                for (int i = 0; i < fareRule.Count; i++)
                {
                    fareRule[i].Origin = itinerary.Segments[0].Origin.CityCode;
                    fareRule[i].Destination = itinerary.Destination;
                }
            }
            else if (itinerary.FlightBookingSource == BookingSource.AirDeccan)
            {
                fareRule = AirDeccanApi.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB);
            }
            else if (itinerary.FlightBookingSource == BookingSource.Amadeus)
            {
                fareRule = itinerary.FareRules;
            }
            else if (itinerary.FlightBookingSource == BookingSource.Mdlr)
            {
                fareRule = itinerary.FareRules;
                for (int i = 0; i < fareRule.Count; i++)
                {
                    fareRule[i].Origin = itinerary.Segments[0].Origin.CityCode;
                    fareRule[i].Destination = itinerary.Destination;
                }
            }
            itinerary.FareRules = fareRule;

            CT.Core.Queue queue = new CT.Core.Queue();
            queue.QueueTypeId = (int)QueueType.Booking;
            queue.StatusId = (int)QueueStatus.Assigned;
            queue.AssignedTo = (int)member;
            queue.AssignedBy = (int)member;
            queue.AssignedDate = DateTime.UtcNow;
            queue.CompletionDate = DateTime.UtcNow.AddDays(TimeLimitToComplete);
            queue.CreatedBy = (int)member;

            Comment comment = new Comment();
            comment.AgencyId = booking.AgencyId;
            comment.CommentDescription = "Booking saved by the AutoImport Method";
            comment.CommentTypes = CommmentType.Booking;
            //TODO: remove this hard coded 1. 1 is for administrator ID.
            comment.CreatedBy = 1;
            comment.IsPublic = false;
            comment.ParentId = booking.BookingId;
            comment.AddComment();

            booking.ProductsList = new Product[1];
            booking.ProductsList[0] = (Product)itinerary;
            booking.AgencyId = agency.AgencyId;
            booking.CreatedBy = (int)member;
            itinerary.CreatedBy = (int)member;

            try
            {
                itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
                itinerary.IsDomestic = itinerary.CheckDomestic("" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
                Airline airline = new Airline();
                airline.Load(itinerary.Segments[0].Airline);
                if (airline.IsLCC && isLcc)
                {
                    booking.Status = BookingStatus.Ticketed;
                }
                else
                {
                    booking.Status = BookingStatus.Ready;
                }

                BookingHistory bh = new BookingHistory();
                string remarks = string.Empty;
                bh.EventCategory = EventCategory.Booking;
                if (booking.Status == BookingStatus.Hold)
                {
                    remarks = "Booking is on hold";
                }
                else if (booking.Status == BookingStatus.Cancelled)
                {
                    remarks = "Booked seat are released";
                }
                else if (booking.Status == BookingStatus.Ticketed)
                {
                    bh.EventCategory = EventCategory.Ticketing;
                    remarks = "Ticket is created";
                }
                else if (booking.Status == BookingStatus.Ready)
                {
                    remarks = "Booking is in ready state";
                }
                else if (booking.Status == BookingStatus.Inactive)
                {
                    remarks = "Booking is Inactive";
                }
                else if (booking.Status == BookingStatus.InProgress || booking.Status == BookingStatus.OutsidePurchase)
                {
                    remarks = "Booking is in-progress";
                }
                else
                {
                    bh.EventCategory = EventCategory.Miscellaneous;
                    remarks = booking.Status.ToString();
                }
                bh.Remarks = remarks;
                bh.CreatedBy = (int)member;

                //System.IO.File.AppendAllText(timeLogFile, "\r\nABook,\t" + DateTime.Now.ToString("HH:mm:ss.ffffff") + ",\t");
                using (TransactionScope updateTransaction = new TransactionScope())
                {
                    booking.Save(itinerary, false);
                    if (!airline.IsLCC)
                    {
                        bh.BookingId = booking.BookingId;
                        bh.Save();
                    }
                    // In the case of LCCs there is no seperate ticketing process.
                    // Once booking is made, it means ticket is generated.
                    if (airline.IsLCC && isLcc)
                    {
                        SaveTicketInfo(itinerary, (int)member, agency.AgencyId, ticketNumbers, booking.BookingId, string.Empty, new Dictionary<string, string>()); // ticket save 
                        booking.SetBookingStatus(BookingStatus.Ticketed, (int)member);
                        int invoiceNumber = AccountUtility.RaiseInvoice(itinerary, string.Empty, (int)member);
                        Invoice.UpdateInvoiceStatus(invoiceNumber, InvoiceStatus.Paid, (int)member);
                    }
                    queue.ItemId = booking.BookingId;
                    queue.Save();
                    updateTransaction.Complete();
                    //try
                    //{
                    //    Thread mailThread = new Thread(MetaSearchEngine.SendLowBalanceMail);
                    //    mailThread.Start(member);
                    //}
                    //catch (Exception)
                    //{ }
                }
                //System.IO.File.AppendAllText(timeLogFile, DateTime.Now.ToString("HH:mm:ss.ffffff"));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        private void SendMail(FlightItinerary itinerary, long member, bool isHold)
        {
            UserPreference preference = new UserPreference();
            try
            {
                preference = preference.GetPreference((int)member, preference.BookingMade, preference.EmailNotification);
            }
            catch (ArgumentException)
            {
                preference.Value = "False";
            }
            if (Convert.ToBoolean(preference.Value))
            {
                string subject = "Booking Confirmed - PNR(" + itinerary.PNR + ")";
                string flightItinerary = string.Empty;
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    flightItinerary = flightItinerary + "-" + itinerary.Segments[i].Origin.AirportCode;
                }
                flightItinerary = flightItinerary + "-" + itinerary.Segments[itinerary.Segments.Length - 1].Destination.AirportCode;
                if (Convert.ToBoolean(ConfigurationSystem.Email["isSendEmail"]))
                {
                    Hashtable table = new Hashtable(3);
                    table.Add("firstName", member.FirstName);
                    table.Add("lastName", member.LastName);
                    table.Add("time", DateTime.UtcNow.Add(new TimeSpan(4, 0, 0)).ToString("hh:mm:ss tt"));
                    table.Add("date", DateTime.UtcNow.Add(new TimeSpan(4, 0, 0)).ToString("dd/MMM/yyyy"));
                    table.Add("pnr", itinerary.PNR);
                    table.Add("paxName", itinerary.Passenger[0].FullName);
                    table.Add("travelDate", itinerary.Segments[0].DepartureTime.ToString("dd/MMM/yyyy"));
                    table.Add("flight", itinerary.Segments[0].Airline + itinerary.Segments[0].FlightNumber);
                    table.Add("itinerary", flightItinerary);
                    //preference is getting written again here
                    preference = new UserPreference();
                    preference = preference.GetPreference((int)member, preference.AlternateEmail, preference.AlternateEmail);

                    List<string> toArray = new List<string>();
                    if (preference.Value != null)
                    {
                        toArray.Add(preference.Value);
                    }
                    else
                    {
                        toArray.Add(member.Email);
                    }
                    try
                    {
                        if (isHold)
                        {
                            CT.Core.Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["replyTo"], toArray, subject, ConfigurationSystem.Email["bookingHold"], table);
                        }
                        else
                        {
                            CT.Core.Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["replyTo"], toArray, subject, ConfigurationSystem.Email["bookingTicketed"], table);
                        }
                    }
                    catch (Exception ex)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.Normal, (int)member, "Sending Mail failed. Error : " + ex.Message, "0");
                    }
                }
            }
        }
        //public static void SendLowBalanceMail(object state)
        //{
        //    long member = (Member)state;
        //    UserPreference preference = new UserPreference();
        //    try
        //    {
        //        preference = preference.GetPreference((int)member, preference.LowBalance, preference.EmailNotification);
        //    }
        //    catch (ArgumentException)
        //    {
        //        preference.Value = "0";
        //    }
        //    try
        //    {
        //        if (preference.Value != null && Convert.ToDecimal(preference.Value) > 0)
        //        {
        //            Agency agency = new Agency(member.AgencyId);
        //            if (agency.LccBalance < Convert.ToDecimal(preference.Value))
        //            {
        //                CT.Core.Email.Send("low balance", "your accoutna balance is low please recharge your account");
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}
        */
        /// <summary>
        /// This method is used to get last cancellation and also checking whether last cancellation is not working day..if it is forward to working day.
        /// </summary>
        /// <param name="lastCancellationDays"></param>
        /// <param name="itineary"></param>
        /// <returns></returns>
        public DateTime GetLastCancellationDateforHotel(int lastCancellationDays, DateTime startDate, HotelBookingSource source)
        {
            Double buffer = 0;
            DateTime lastCancellationDate;
            if (source == HotelBookingSource.GTA)
            {
                if (ConfigurationSystem.GTAConfig.ContainsKey("buffer"))
                {
                    buffer = Convert.ToDouble(ConfigurationSystem.GTAConfig["buffer"]);
                }
            }
            else if (source == HotelBookingSource.HotelBeds)
            {
                if (ConfigurationSystem.HotelBedsConfig.ContainsKey("buffer"))
                {
                    buffer = Convert.ToDouble(ConfigurationSystem.HotelBedsConfig["buffer"]);
                }
            }
            else if (source == HotelBookingSource.Tourico)
            {
                if (ConfigurationSystem.TouricoConfig.ContainsKey("buffer"))
                {
                    buffer = Convert.ToDouble(ConfigurationSystem.TouricoConfig["buffer"]);
                }
            }
            else if (source == HotelBookingSource.Miki)
            {
                if (ConfigurationSystem.MikiConfig.ContainsKey("Buffer"))
                {
                    buffer = Convert.ToDouble(ConfigurationSystem.MikiConfig["Buffer"]);
                }
            }
            else if (source == HotelBookingSource.Travco)
            {
                if (ConfigurationSystem.TravcoConfig.ContainsKey("Buffer"))
                {
                    buffer = Convert.ToDouble(ConfigurationSystem.TravcoConfig["Buffer"]);
                }
            }
            else if (source == HotelBookingSource.DOTW)
            {
                if (ConfigurationSystem.DOTWConfig.ContainsKey("Buffer"))
                {
                    buffer = Convert.ToDouble(ConfigurationSystem.DOTWConfig["Buffer"]);
                }
            }
            else if (source == HotelBookingSource.WST)
            {
                if (ConfigurationSystem.WSTConfig.ContainsKey("Buffer"))
                {
                    buffer = Convert.ToDouble(ConfigurationSystem.WSTConfig["Buffer"]);
                }
            }
            else if (Convert.ToBoolean(ConfigurationSystem.DesiyaConfig["RunDesiyaBufferCode"]) && source == HotelBookingSource.Desiya)
            {
                if (ConfigurationSystem.DesiyaConfig.ContainsKey("Buffer"))
                {
                    buffer = Convert.ToDouble(ConfigurationSystem.DesiyaConfig["Buffer"]);
                }
            }
            lastCancellationDate = startDate.AddDays(-(lastCancellationDays + buffer));
            if (startDate.AddDays(-(lastCancellationDays + buffer)).DayOfWeek == DayOfWeek.Saturday)
            {
                lastCancellationDate = startDate.AddDays(-(lastCancellationDays + buffer + 1));
            }
            else if (startDate.AddDays(-(lastCancellationDays + buffer)).DayOfWeek == DayOfWeek.Sunday)
            {
                lastCancellationDate = startDate.AddDays(-(lastCancellationDays + buffer + 2));
            }
            return lastCancellationDate;
        }
        /// <summary>
        /// Retrieves the fare rules for a given result
        /// </summary>
        /// <param name="result">SearchResult Object for which fare rules are to be retrieved</param>
        /// <returns>List of object of FareRule</returns>
        public List<FareRule> GetFareRule(SearchResult result)
        {
            List<FareRule> fareRule = new List<FareRule>();
            //if (result.ResultBookingSource == BookingSource.WorldSpan)
            //{
            //    fareRule = Worldspan.GetFareRuleList(result.FareRules);
            //}
            //else if (result.ResultBookingSource == BookingSource.Galileo)
            //{
            //    fareRule = GalileoApi.GetFareRuleList(result.FareRules);
            //    //fareRule = UAPI.GetFareRuleList(result.FareRules);
            //}
            if (result.ResultBookingSource == BookingSource.UAPI)
            {
                //fareRule = GalileoApi.GetFareRuleList(result.FareRules);
                SourceDetails agentDetails = new SourceDetails();

                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    agentDetails = SettingsLoginInfo.OnBehalfAgentSourceCredentials["UA"];
                    UAPI.agentBaseCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    UAPI.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                }
                else
                {
                    agentDetails = SettingsLoginInfo.AgentSourceCredentials["UA"];
                    UAPI.agentBaseCurrency = SettingsLoginInfo.Currency;
                    UAPI.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                }

                //UAPI uApi = new UAPI();
                UAPI.UserName = agentDetails.UserID;
                UAPI.Password = agentDetails.Password;
                UAPI.TargetBranch = agentDetails.HAP;

                UAPI.AppUserId = SettingsLoginInfo.UserID.ToString();
                UAPI.SessionId = sessionId;
                fareRule = UAPI.GetFareRuleList(result.FareRules);
            }
            else if (result.ResultBookingSource == BookingSource.AirArabia)
            {
                fareRule = AirArabia.GetFareRule(result.Flights[0][0].Origin.CityCode, result.Flights[0][0].Destination.CityCode, "P");//to insert farebasiscode through object

            }
            else if (result.ResultBookingSource == BookingSource.SpiceJet)
            {
                SpiceJetAPIV1 sjObj = new SpiceJetAPIV1();

                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    sjObj.LoginName = SettingsLoginInfo.OnBehalfAgentSourceCredentials["SG"].UserID;
                    sjObj.Password = SettingsLoginInfo.OnBehalfAgentSourceCredentials["SG"].Password;
                    sjObj.AgentDomain = SettingsLoginInfo.OnBehalfAgentSourceCredentials["SG"].HAP;
                    sjObj.AgentBaseCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    sjObj.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                }
                else
                {
                    sjObj.LoginName = SettingsLoginInfo.AgentSourceCredentials["SG"].UserID;
                    sjObj.Password = SettingsLoginInfo.AgentSourceCredentials["SG"].Password;
                    sjObj.AgentDomain = SettingsLoginInfo.AgentSourceCredentials["SG"].HAP;
                    sjObj.AgentBaseCurrency = SettingsLoginInfo.Currency;
                    sjObj.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                }

                fareRule = sjObj.GetFareRule("", "", "");
            }
            //else if (result.ResultBookingSource == BookingSource.Indigo)
            //{
            //    NavitaireSearchResult sjRes = new NavitaireSearchResult();
            //    sjRes = (NavitaireSearchResult)result;
            //    fareRule = Navitaire.GetFareRule(result.Flights[0][0].Origin.CityCode, result.Flights[0][0].Destination.CityCode, sjRes.SelectedFareBasisCode[0], "6E");
            //}
            //else if (result.ResultBookingSource == BookingSource.Paramount)
            //{
            //    ParamountSearchResult pmRes = new ParamountSearchResult();
            //    pmRes.FareRules = result.FareRules;
            //    pmRes.Flights = result.Flights;
            //    pmRes.TicketAdvisory = result.TicketAdvisory;
            //    ParamountApi paramount = new ParamountApi();
            //    fareRule = paramount.GetFareRule(result, sessionId);
            //}
            //else if (result.ResultBookingSource == BookingSource.AirDeccan)
            //{
            //    AirDeccanSearchResult dnRes = new AirDeccanSearchResult();
            //    dnRes = (AirDeccanSearchResult)result;
            //    fareRule = AirDeccanApi.GetFareRule(result.Flights[0][0].Origin.CityCode, result.Flights[0][0].Destination.CityCode, dnRes.FareRules[0].FareBasisCode);
            //}
            //else if (result.ResultBookingSource == BookingSource.Amadeus)
            //{
            //    fareRule = Amadeus.GetFareRuleList(result);
            //}
            //else if (result.ResultBookingSource == BookingSource.Mdlr)
            //{
            //    MdlrSearchResult mdlrRes = new MdlrSearchResult();
            //    mdlrRes = (MdlrSearchResult)result;
            //    fareRule = Mdlr.GetFareRule(result, sessionId);
            //}
            //else if (result.ResultBookingSource == BookingSource.GoAir)
            //{
            //    GoAirSearchResult gaRes = new GoAirSearchResult();
            //    gaRes = (GoAirSearchResult)result;
            //    fareRule = GoAirApi.GetFareRule(result.Flights[0][0].Origin.CityCode, result.Flights[0][0].Destination.CityCode, gaRes.FareRules[0].FareBasisCode);
            //}
            //else if (result.ResultBookingSource == BookingSource.AirArabia)
            //{
            //    fareRule = AirArabia.GetFareRule(result.Flights[0][0].Origin.CityCode, result.Flights[0][0].Destination.CityCode, "P");//to insert farebasiscode through object

           //}
            //else if (result.ResultBookingSource == BookingSource.Sama)
            //{
            //    Sama zsRes = new Sama();
            //    fareRule = Sama.GetFareRule(result.FareRules);
            //}
            //else if (result.ResultBookingSource == BookingSource.HermesAirLine)
            //{
            //    HermesApi hApi = new HermesApi(sessionId);
            //    fareRule = hApi.GetFareRule(result.FareRules);
            //}
            else if (result.ResultBookingSource == BookingSource.FlyDubai)
            {
                fareRule = FlyDubaiApi.GetFareRule(result.Flights[0][0].Origin.CityCode, result.Flights[0][0].Destination.CityCode, result.FareRules[0].FareBasisCode);
            }
            else if (result.ResultBookingSource == BookingSource.TBOAir)
            {
                AirV10 tbo = new AirV10();

                SourceDetails agentDetails = new SourceDetails();

                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    agentDetails = SettingsLoginInfo.OnBehalfAgentSourceCredentials["UA"];
                    tbo.AgentBaseCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    tbo.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                }
                else
                {
                    agentDetails = SettingsLoginInfo.AgentSourceCredentials["UA"];
                    tbo.AgentBaseCurrency = SettingsLoginInfo.Currency;
                    tbo.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                }

                //tbo.LoginName = agentDetails.UserID;
                //tbo.Password = agentDetails.Password;

                fareRule = tbo.GetFareRule(result);
            }
            return fareRule;
        }
        
        /// <summary>
        /// Method to retrieve the fare rules for the given itinerary
        /// </summary>
        /// <param name="itinerary">FlightItinerary object for which fare rules are to be retrieved</param>
        /// <returns>List of object of FareRule</returns>
        public List<FareRule> GetFareRule(FlightItinerary itinerary)
        {
            List<FareRule> fareRules = new List<FareRule>();
            //bool modeSpicejet = (ConfigurationSystem.SpiceJetConfig["Mode"] == "SpiceJet");
            //if (itinerary.FlightBookingSource == BookingSource.WorldSpan)
            //{
            //    if (itinerary.FareRules != null && itinerary.FareRules.Count > 0 && (itinerary.FareRules[0].FareRuleDetail == null || itinerary.FareRules[0].FareRuleDetail.Length == 0))
            //    {
            //        itinerary.FareRules = Worldspan.GetFareRuleList(itinerary.FareRules);
            //    }
            //}
            //else if (itinerary.FlightBookingSource == BookingSource.Amadeus)
            //{
            //    if (itinerary.FareRules != null && itinerary.FareRules.Count > 0 && (itinerary.FareRules[0].FareRuleDetail == null || itinerary.FareRules[0].FareRuleDetail.Length == 0))
            //    {
            //        itinerary.FareRules = Amadeus.GetFareRuleList((List<FareRule>)itinerary.FareRules);
            //    }
            //}
            //else if (itinerary.FlightBookingSource == BookingSource.Galileo)
            //{
            //    if (itinerary.FareRules != null && itinerary.FareRules.Count > 0 && (itinerary.FareRules[0].FareRuleDetail == null || itinerary.FareRules[0].FareRuleDetail.Length == 0))
            //    {
            //        itinerary.FareRules = GalileoApi.GetFareRuleList((List<FareRule>)itinerary.FareRules);
            //    }
            //}
            if (itinerary.FlightBookingSource == BookingSource.UAPI)
            {
                if (itinerary.FareRules != null && itinerary.FareRules.Count > 0 && (itinerary.FareRules[0].FareRuleDetail == null || itinerary.FareRules[0].FareRuleDetail.Length == 0))
                {
                    itinerary.FareRules = UAPI.GetFareRuleList((List<FareRule>)itinerary.FareRules);
                }
            }
            else if (itinerary.FlightBookingSource == BookingSource.AirArabia)
            {
                itinerary.FareRules = AirArabia.GetFareRule(itinerary.Origin, itinerary.Destination, itinerary.FareRules[0].FareBasisCode);
            }
            //if (itinerary.FlightBookingSource == BookingSource.SpiceJet)
            //{
            //    if (modeSpicejet)
            //    {
            //        itinerary.FareRules = SpiceJetAPI.GetFareRule(itinerary.Origin, itinerary.Destination, itinerary.FareRules[0].FareBasisCode);
            //    }
            //    else
            //    {
            //        itinerary.FareRules = Navitaire.GetFareRule(itinerary.Origin, itinerary.Destination, itinerary.FareRules[0].FareBasisCode, "SG");
            //    }
            //}
            //else if (itinerary.FlightBookingSource == BookingSource.Indigo)
            //{
            //    itinerary.FareRules = Navitaire.GetFareRule(itinerary.Origin, itinerary.Destination, itinerary.FareRules[0].FareBasisCode, "6E");
            //}
            ////else if (itinerary.FlightBookingSource == BookingSource.Paramount)
            ////{
            ////    itinerary.FareRules = ParamountApi.GetFareRule(itinerary, sessionId);
            ////}
            //else if (itinerary.FlightBookingSource == BookingSource.AirDeccan)
            //{
            //    itinerary.FareRules = AirDeccanApi.GetFareRule(itinerary.Origin, itinerary.Destination, itinerary.FareRules[0].FareBasisCode);
            //}
            //else if (itinerary.FlightBookingSource == BookingSource.Mdlr)
            //{
            //    itinerary.FareRules = Mdlr.GetFareRule(itinerary, sessionId);
            //}
            //else if (itinerary.FlightBookingSource == BookingSource.GoAir)
            //{
            //    itinerary.FareRules = GoAirApi.GetFareRule(itinerary.Origin, itinerary.Destination, itinerary.FareRules[0].FareBasisCode);
            //}
            //else if (itinerary.FlightBookingSource == BookingSource.AirArabia)
            //{
            //    itinerary.FareRules = AirArabia.GetFareRule(itinerary.Origin, itinerary.Destination, itinerary.FareRules[0].FareBasisCode);
            //}
            else if (itinerary.FlightBookingSource == BookingSource.FlyDubai)
            {
                itinerary.FareRules = FlyDubaiApi.GetFareRule(itinerary.Origin, itinerary.Destination, itinerary.FareRules[0].FareBasisCode);
            }
            //else if (itinerary.FlightBookingSource == BookingSource.Sama)
            //{
            //    itinerary.FareRules = Sama.GetFareRule(itinerary.FareRules);
            //}
            //else if (itinerary.FlightBookingSource == BookingSource.HermesAirLine)
            //{
            //    HermesApi hApi = new HermesApi(sessionId);
            //    itinerary.FareRules = hApi.GetFareRule(itinerary.FareRules);
            //}
            fareRules = itinerary.FareRules;
            return fareRules;
        }
       
        /// <summary>
        /// This Method is used to get Last Cancellation Date for Transfers
        /// </summary>
        /// <param name="lastCancellationDays"></param>
        /// <param name="startDate"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public DateTime GetLastCancellationDateforTransfer(int lastCancellationDays, DateTime startDate, TransferBookingSource source)
        {
            Double buffer = 0;
            DateTime lastCancellationDate;
            if (source == TransferBookingSource.GTA)
            {
                if (ConfigurationSystem.GTAConfig.ContainsKey("buffer"))
                {
                    buffer = Convert.ToDouble(ConfigurationSystem.GTAConfig["buffer"]);
                }
            }
            lastCancellationDate = startDate.AddDays(-(lastCancellationDays + buffer));
            if (startDate.AddDays(-(lastCancellationDays + buffer)).DayOfWeek == DayOfWeek.Saturday)
            {
                lastCancellationDate = startDate.AddDays(-(lastCancellationDays + buffer + 1));
            }
            else if (startDate.AddDays(-(lastCancellationDays + buffer)).DayOfWeek == DayOfWeek.Sunday)
            {
                lastCancellationDate = startDate.AddDays(-(lastCancellationDays + buffer + 2));
            }
            return lastCancellationDate;
        }
        /// <summary>
        /// This Method is used to get Last Cancellation Date for Sightseeing
        /// </summary>
        /// <param name="lastCancellationDays"></param>
        /// <param name="startDate"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public DateTime GetLastCancellationDateforSightseeing(int lastCancellationDays, DateTime startDate, SightseeingBookingSource source)
        {
            Double buffer = 0;
            DateTime lastCancellationDate;
            if (source == SightseeingBookingSource.GTA)
            {
                if (ConfigurationSystem.GTAConfig.ContainsKey("buffer"))
                {
                    buffer = Convert.ToDouble(ConfigurationSystem.GTAConfig["buffer"]);
                }
            }
            lastCancellationDate = startDate.AddDays(-(lastCancellationDays + buffer));
            if (startDate.AddDays(-(lastCancellationDays + buffer)).DayOfWeek == DayOfWeek.Saturday)
            {
                lastCancellationDate = startDate.AddDays(-(lastCancellationDays + buffer + 1));
            }
            else if (startDate.AddDays(-(lastCancellationDays + buffer)).DayOfWeek == DayOfWeek.Sunday)
            {
                lastCancellationDate = startDate.AddDays(-(lastCancellationDays + buffer + 2));
            }
            return lastCancellationDate;
        } 

        #endregion
        


        #region Supporting Private Methods.
        /// <summary>
        /// Saves Ticket information.
        /// </summary>
        /*public void SaveTicketInfo(ref Ticket[] ticket, FlightItinerary itineraryDB, FlightItinerary itineraryWorldspan, int memberId, int agencyId, int bookingId, Dictionary<string, string> ticketData, string ipAddr)
        {
            //send SMS  for ETicket generation
            if (SMS.IsSMSSubscribed(agencyId, SMSEvent.ETicketGeneration))
            {
                Hashtable hTable = new Hashtable();
                hTable.Add("AgencyId", agencyId);
                string pnrInfo = string.Empty;
                int count = 1;
                foreach (FlightInfo segment in itineraryWorldspan.Segments)
                {
                    pnrInfo += segment.Origin.CityCode + "-" + segment.Destination.CityCode + ":Flight " + segment.Airline + "-" + segment.FlightNumber + "(" + segment.DepartureTime.ToString("ddMMMyy HH:mm") + ") PNR:" + segment.AirlinePNR;
                    if (count < itineraryDB.Segments.Length)
                        pnrInfo += ", ";
                    count++;
                }
                hTable.Add("PNR", pnrInfo);
                if (itineraryDB.Passenger[0].FullName.Length <= 20)
                {
                    hTable.Add("PaxName", itineraryDB.Passenger[0].FullName);
                }
                else
                {
                    hTable.Add("PaxName", itineraryDB.Passenger[0].FirstName);
                }
                hTable.Add("EventType", SMSEvent.ETicketGeneration);
                SMS.SendSMSThread(hTable);
            }
            if (SMS.IsSMSSubscribed(agencyId, SMSEvent.ETicketforPassenger))
            {
                Hashtable hTable = new Hashtable();
                hTable.Add("AgencyId", agencyId);
                string pnrInfo = string.Empty;
                int count = 1;
                foreach (FlightInfo segment in itineraryWorldspan.Segments)
                {
                    pnrInfo += segment.Origin.CityCode + "-" + segment.Destination.CityCode + ":Flight " + segment.Airline + "-" + segment.FlightNumber + "(" + segment.DepartureTime.ToString("ddMMMyy HH:mm") + ") PNR:" + segment.AirlinePNR;
                    if (count < itineraryDB.Segments.Length)
                        pnrInfo += ", ";
                    count++;
                }
                hTable.Add("PNR", pnrInfo);
                if (itineraryDB.Passenger[0].FullName.Length <= 20)
                {
                    hTable.Add("PaxName", itineraryDB.Passenger[0].FullName);
                }
                else
                {
                    hTable.Add("PaxName", itineraryDB.Passenger[0].FirstName);
                }
                hTable.Add("MobileNo", itineraryDB.Passenger[0].CellPhone);
                hTable.Add("EventType", SMSEvent.ETicketforPassenger);
                SMS.SendSMSThread(hTable);
            }
            int ticketSaved = 0;
            // Updatig AirlinePNR in database. 
            // FlightId of itineraryWorldspan has to be set as in database before updating Airline PNR.
            UpdateAirlinePNR(itineraryWorldspan);
            for (int i = 0; i < ticket.Length; i++)
            {
                // PaxKeys will be same if ticket is in same sequence as the corresponding passengers are in our database.
                if (ticket[i].PaxKey == itineraryDB.Passenger[i].PaxKey)
                {
                    // SaveTIcket needs ticket index and passenger index. If in sequence both are same.
                    SetTicketForNonLCC(i, i, ref ticket, itineraryDB, itineraryWorldspan, memberId, agencyId, bookingId, ticketData);
                    ticketSaved++;
                }
                else
                {
                    // If not in sequence then finding the corresponding passenger.
                    for (int j = 0; j < itineraryDB.Passenger.Length; j++)
                    {
                        if (ticket[i].PaxKey == itineraryDB.Passenger[j].PaxKey)
                        {
                            // i is ticket index and j is passenger index.
                            SetTicketForNonLCC(i, j, ref ticket, itineraryDB, itineraryWorldspan, memberId, agencyId, bookingId, ticketData);
                            ticketSaved++;
                            break;
                        }
                    }
                }
            }

            Airline air = new Airline();
            air.Load(itineraryDB.ValidatingAirlineCode);
            string supplierCode = string.Empty;
            if (ticket.Length > 0)
            {
                if (ticket[0].SupplierId > 0)
                {
                    supplierCode = Supplier.GetSupplierCodeById(ticket[0].SupplierId);
                }
            }
            for (int i = 0; i < ticket.Length; i++)
            {
                ticket[i].SupplierRemmitance = new SupplierRemmitance();
                ticket[i].SupplierRemmitance.PublishedFare = ticket[i].Price.PublishedFare;
                ticket[i].SupplierRemmitance.AccpriceType = ticket[i].Price.AccPriceType;
                ticket[i].SupplierRemmitance.NetFare = ticket[i].Price.NetFare;
                ticket[i].SupplierRemmitance.CessTax = 0;
                ticket[i].SupplierRemmitance.Currency = ticket[i].Price.Currency;
                ticket[i].SupplierRemmitance.CurrencyCode = ticket[i].Price.CurrencyCode;
                ticket[i].SupplierRemmitance.Markup = ticket[i].Price.Markup;
                ticket[i].SupplierRemmitance.OtherCharges = ticket[i].Price.OtherCharges;
                ticket[i].SupplierRemmitance.OurCommission = ticket[i].Price.OurCommission;
                ticket[i].SupplierRemmitance.OurPlb = ticket[i].Price.OurPLB;
                ticket[i].SupplierRemmitance.RateOfExchange = ticket[i].Price.RateOfExchange;
                ticket[i].SupplierRemmitance.ServiceTax = ticket[i].Price.SeviceTax;
                ticket[i].SupplierRemmitance.SupplierCode = supplierCode;

                ticket[i].SupplierRemmitance.Tax = ticket[i].Price.Tax;
                ticket[i].SupplierRemmitance.TdsCommission = 0;
                ticket[i].SupplierRemmitance.TdsPLB = 0;
                ticket[i].SupplierRemmitance.CreatedBy = ticket[i].CreatedBy;
                ticket[i].SupplierRemmitance.CreatedOn = DateTime.UtcNow;
                ticket[i].SupplierRemmitance.CommissionType = air.CommissionType;
            }

            int txnRetryCount = Convert.ToInt32(ConfigurationSystem.BookingEngineConfig["txnRetryCount"]);
            int numberRetry = 0;
            bool txnSucceded = false;
            while (!txnSucceded && numberRetry < txnRetryCount)
            {
                numberRetry++;
                try
                {
                    using (TransactionScope autoTicketScope = new TransactionScope())
                    {
                        for (int i = 0; i < ticket.Length; i++)
                        {
                            ticket[i].Save();
                            ticket[i].SupplierRemmitance.TicketId = ticket[i].TicketId;
                            ticket[i].SupplierRemmitance.Save();
                            for (int j = 0; j < ticket[i].PtcDetail.Count; j++)
                            {
                                ticket[i].PtcDetail[j].Save();
                            }
                        }
                        autoTicketScope.Complete();
                        txnSucceded = true;
                    }
                }
                catch (Exception ex)
                {
                    if (numberRetry < txnRetryCount)
                    {
                        for (int i = 0; i < ticket.Length; i++)
                        {
                            ticket[i].TicketId = 0;
                            ticket[i].SupplierRemmitance.SupplierRemmitanceId = 0;
                        }
                        continue;
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }


            //Save Booking History
            string remarks = string.Empty;
            try
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null && Convert.ToBoolean(HttpContext.Current.Session["isB2B2BAgent"]))
                {
                    Dictionary<string, string> b2b2bVariableList = new Dictionary<string, string>();
                    b2b2bVariableList = (Dictionary<string, string>)HttpContext.Current.Session["b2b2bVariable"];


                    remarks = " By B2B2B Subagent Site URL " + b2b2bVariableList["b2b2bSiteName"] + " ,Subagent " + b2b2bVariableList["b2b2bSubAgencyName"];


                }
            }
            catch (Exception)
            {
            }
            BookingHistory bookingHistory = new BookingHistory();
            bookingHistory.BookingId = bookingId;
            bookingHistory.EventCategory = EventCategory.Ticketing;
            bookingHistory.Remarks = "Ticket Created" + remarks + " (IP Address - " + ipAddr + ")";
            bookingHistory.CreatedBy = memberId;
            bookingHistory.Save();
            if (ticket.Length != ticketSaved)
            {
                Audit.Add(EventType.Ticketing, Severity.High, memberId, "Error in Save Ticket. PNR : " + itineraryDB.PNR + " Source : " + itineraryDB.FlightBookingSource.ToString(), "0");
                throw new BookingEngineException("Error in Save Ticket. PNR : " + itineraryDB.PNR + " Source : " + itineraryDB.FlightBookingSource.ToString());
            }
            #region Getting TBOConnect results for Cache
            if (Convert.ToBoolean(ConfigurationSystem.HotelConnectConfig["TBOConnectOnETicket"]))
            {

                try
                {
                    Thread ResultThread = new Thread
                    (
                        delegate()
                        {
                            //SearchResultTBOConnectForCache(itineraryDB, memberId);
                        }
                    );
                    ResultThread.Start();
                    ResultThread.IsBackground = true;
                }
                catch { }
            }
            #endregion
        }

        private void SetTicketForNonLCC(int ticketIndex, int paxIndex, ref Ticket[] ticket, FlightItinerary itineraryDB, FlightItinerary itineraryWorldspan, int memberId, int agencyId, int bookingId, Dictionary<string, string> ticketData)
        {
            List<string> airlineNameRemarksList = new List<string>();
            List<string> airlineNameFareRuleList = new List<string>();
            string remarks = string.Empty;
            string fareRule = string.Empty;
            UserPreference preference = new UserPreference();
            //Airline service fee for Email Itinerary
            Dictionary<string, string> sFeeTypeList = new Dictionary<string, string>();
            Dictionary<string, string> sFeeValueList = new Dictionary<string, string>();
            FlightItinerary.GetMemberPrefServiceFee(agencyId, ref sFeeTypeList, ref sFeeValueList);
            int i = ticketIndex;    // index of ticket in ticket array rtrieved from worldspan.
            int j = paxIndex;       // index of passenger as in itinerary made from database.
            ticket[i].Status = "OK";
            ticket[i].CreatedBy = memberId;
            ticket[i].PaxId = itineraryDB.Passenger[j].PaxId;
            ticket[i].PaxType = itineraryDB.Passenger[j].Type;
            ticket[i].FlightId = itineraryDB.FlightId;
            int paxIndexPricing = i;
            // We need to save price received from worldspan.
            // Finding mathing index of corresponding passenger in itinerary received from worldspan if the sequence is not same.
            if (ticket[i].PaxKey != itineraryWorldspan.Passenger[i].PaxKey)
            {
                for (int k = 0; k < itineraryWorldspan.Passenger.Length; k++)
                {
                    if (ticket[i].PaxKey == itineraryWorldspan.Passenger[k].PaxKey)
                    {
                        paxIndexPricing = k;
                        break;
                    }
                }
            }
            ticket[i].Price = itineraryDB.Passenger[j].Price;
            //}

            ticket[i].ETicket = true;
            if (ticket[i].IssueInExchange == null)
            {
                ticket[i].IssueInExchange = string.Empty;
            }
            if (itineraryDB.TourCode != null && itineraryDB.TourCode.Length > 0)
            {
                ticket[i].TourCode = itineraryDB.TourCode;
            }
            else if (ticketData != null && ticketData.ContainsKey("tourCode") && ticketData["tourCode"].Length > 0)
            {
                ticket[i].TourCode = ticketData["tourCode"];
            }
            else
            {
                ticket[i].TourCode = string.Empty;
            }
            if (ticket[i].OriginalIssue == null)
            {
                ticket[i].OriginalIssue = string.Empty;
            }
            if (itineraryDB.Endorsement != null && itineraryDB.Endorsement.Length > 0)
            {
                ticket[i].Endorsement = itineraryDB.Endorsement;
            }
            else if (ticketData != null && ticketData.ContainsKey("endorsement") && ticketData["endorsement"].Length > 0)
            {
                ticket[i].Endorsement = ticketData["endorsement"];
            }
            else
            {
                ticket[i].Endorsement = string.Empty;
            }
            if (ticketData != null && ticketData.ContainsKey("corporateCode") && ticketData["corporateCode"].Length > 0)
            {
                ticket[i].CorporateCode = ticketData["corporateCode"];
            }
            else
            {
                ticket[i].CorporateCode = string.Empty;
            }
            if (ticketData != null && ticketData.ContainsKey("remarks") && ticketData["remarks"].Length > 0)
            {
                ticket[i].Remarks = ticketData["remarks"];
            }
            else
            {
                ticket[i].Remarks = string.Empty;
            }
            if (ticket[i].FareCalculation == null)
            {
                ticket[i].FareCalculation = string.Empty;
            }
            int agencyPrimaryMemberId = Member.GetPrimaryMemberId(agencyId);


            if (itineraryDB.BookingMode == BookingMode.WhiteLabel || itineraryDB.BookingMode == BookingMode.Itimes)
            {
                if (itineraryDB.CheckDomestic("" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + ""))
                {
                    int chargeType = 1;
                    decimal chargeValue = 0;
                    AirlineServiceFee asf = AirlineServiceFee.Load(itineraryDB.Segments[0].Airline, agencyId);
                    if (asf.AirlineCode != null && asf.AirlineCode != "" && asf.IsActive)
                    {
                        chargeType = Convert.ToInt32(asf.ServiceFeeType);
                        chargeValue = asf.ServiceFee;
                    }
                    else
                    {
                        chargeType = Convert.ToInt32(preference.GetPreference(agencyPrimaryMemberId, WLPreferenceKeys.ServiceChargeType, "Booking & Confirmation").Value);
                        chargeValue = Convert.ToDecimal(preference.GetPreference(agencyPrimaryMemberId, WLPreferenceKeys.ServiceCharge, "Booking & Confirmation").Value);
                    }
                    if (chargeType == 1)
                    {
                        ticket[i].ServiceFee += chargeValue;
                    }
                    else
                    {
                        ticket[i].ServiceFee += (ticket[i].Price.PublishedFare + ticket[i].Price.Tax) * (chargeValue / 100);
                    }
                }
                else
                {
                    int chargeType = Convert.ToInt32(preference.GetPreference(agencyPrimaryMemberId, WLPreferenceKeys.IntlServiceChargeType, "Booking & Confirmation").Value);
                    decimal chargeValue = Convert.ToDecimal(preference.GetPreference(agencyPrimaryMemberId, WLPreferenceKeys.IntlServiceCharge, "Booking & Confirmation").Value);
                    if (chargeType == 1)
                    {
                        ticket[i].ServiceFee += chargeValue;
                    }
                    else
                    {
                        ticket[i].ServiceFee += (ticket[i].Price.PublishedFare + ticket[i].Price.Tax) * (chargeValue / 100);
                    }
                }
            }
            else
            {
                string sfType = string.Empty;
                decimal sfValue = 0;
                if (!itineraryDB.CheckDomestic("" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + ""))
                {
                    if (sFeeTypeList.ContainsKey("INTL") && sFeeValueList.ContainsKey("INTL"))
                    {
                        sfType = sFeeTypeList["INTL"];
                        sfValue = Convert.ToDecimal(sFeeValueList["INTL"]);
                    }
                }
                else if (sFeeTypeList.ContainsKey(itineraryDB.ValidatingAirlineCode) && sFeeValueList.ContainsKey(itineraryDB.ValidatingAirlineCode))
                {
                    sfType = sFeeTypeList[itineraryDB.ValidatingAirlineCode];
                    sfValue = Convert.ToDecimal(sFeeValueList[itineraryDB.ValidatingAirlineCode]);
                }

                if (sfType == "FIXED")
                {
                    ticket[i].ServiceFee = sfValue;
                }
                else if (sfType == "PERCENTAGE")
                {
                    decimal totalfare = ticket[i].Price.PublishedFare + ticket[i].Price.Tax + ticket[i].Price.OtherCharges + ticket[i].Price.AdditionalTxnFee + ticket[i].Price.TransactionFee;
                    ticket[i].ServiceFee = totalfare * sfValue / 100;
                }
                else
                {
                    ticket[i].ServiceFee = 0;
                }
            }
            preference = preference.GetPreference(agencyPrimaryMemberId, UserPreference.ServiceFee, UserPreference.ServiceFee);
            if (preference.Value != null)
            {
                if (preference.Value == UserPreference.ServiceFeeInTax)
                {
                    ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowInTax;
                }
                else if (preference.Value == UserPreference.ServiceFeeShow)
                {
                    ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowSeparately;
                }
            }
            else
            {
                ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowInTax;
            }
            int consolidatorId = 0;
            foreach (FlightInfo segment in itineraryDB.Segments)
            {
                string airlineCode = segment.Airline;
                Airline airline = new Airline();
                airline.Load(airlineCode);
                if (airlineCode == itineraryDB.ValidatingAirlineCode)
                {
                    consolidatorId = airline.ConsolidatorId;
                }
                if (!airlineNameRemarksList.Contains(airline.AirlineName) && airline.Remarks.Length != 0)
                {
                    airlineNameRemarksList.Add(airline.AirlineName);
                    remarks += "<span style=\"font-weight:bold\">" + airline.AirlineName + "</span>" + "&nbsp;:&nbsp;" + airline.Remarks + "<br />";
                }
                if (!airlineNameFareRuleList.Contains(airline.AirlineName) && airline.FareRule.Length != 0)
                {
                    airlineNameFareRuleList.Add(airline.AirlineName);
                    fareRule += "<span style=\"font-weight:bold\">" + airline.AirlineName + "</span>" + "&nbsp;:&nbsp;" + airline.FareRule + "<br />";
                }
            }
            //Set the consolidator id from AirlineHAP if exists there
            AirlineHAP airHap = new AirlineHAP();
            if (itineraryDB.FlightBookingSource == BookingSource.Amadeus)
            {
                airHap.Load(itineraryDB.ValidatingAirline, "1A");
            }
            else if (itineraryDB.FlightBookingSource == BookingSource.Galileo)
            {
                airHap.Load(itineraryDB.ValidatingAirline, "1G");
            }
            else if (itineraryDB.FlightBookingSource == BookingSource.UAPI)// todo check 
            {
                airHap.Load(itineraryDB.ValidatingAirline, "1G");
            }
            if (airHap != null && !string.IsNullOrEmpty(airHap.Airline))
            {
                consolidatorId = airHap.SupplierId;
            }
            ticket[i].ValidatingAriline= itineraryDB.ValidatingAirline;// TODO ziyad..... check the Air line numericcode too
            ticket[i].SupplierId = consolidatorId;
            ticket[i].FareRule = fareRule;
            // Saving PTC Detail information after ticket is saved.
            for (int f = 0; f < itineraryDB.Segments.Length; f++)
            {
                for (int p = 0; p < ticket[i].PtcDetail.Count; p++)
                {
                    if (ticket[i].PtcDetail[p].FlightKey == itineraryDB.Segments[f].FlightKey)
                    {
                        ticket[i].PtcDetail[p].SegmentId = itineraryDB.Segments[f].SegmentId;
                        ticket[i].PtcDetail[p].PaxType = FlightPassenger.GetPTC(ticket[i].PaxType);
                        break;
                    }
                }
            }
          try
            {
                int timeDiff = Convert.ToInt32(Configuration.ConfigurationSystem.Core["TimeDiffInFlightKey"]);
                for (int p = 0; p < ticket[i].PtcDetail.Count; p++)
                {
                    if (ticket[i].PtcDetail[p].SegmentId <= 0 || ticket[i].PtcDetail[p].PaxType == null || ticket[i].PtcDetail[p].PaxType == string.Empty)
                    {
                        string ptcKey = ticket[i].PtcDetail[p].FlightKey.Substring(0, 6);
                        DateTime ptcDate = DateTime.ParseExact(ticket[i].PtcDetail[p].FlightKey.Substring(6), "ddMMMyyyyHHmm", null);
                        for (int f = 0; f < itineraryDB.Segments.Length; f++)
                        {
                            if (ptcKey.Contains(itineraryDB.Segments[f].FlightKey.Substring(0, 6)) && (ptcDate.Subtract(itineraryDB.Segments[f].DepartureTime) < TimeSpan.FromMinutes(timeDiff) || itineraryDB.Segments[f].DepartureTime.Subtract(ptcDate) < TimeSpan.FromMinutes(timeDiff)))
                            {
                                ticket[i].PtcDetail[p].SegmentId = itineraryDB.Segments[f].SegmentId;
                                ticket[i].PtcDetail[p].PaxType = FlightPassenger.GetPTC(ticket[i].PaxType);
                                break;
                            }
                        }
                    }
                }
                for (int p = 0; p < ticket[i].PtcDetail.Count; p++)
                {
                    if (ticket[i].PtcDetail[p].SegmentId <= 0 || ticket[i].PtcDetail[p].PaxType == null || ticket[i].PtcDetail[p].PaxType == string.Empty)
                    {
                        ticket[i].PtcDetail[p].SegmentId = itineraryDB.Segments[p].SegmentId;
                        ticket[i].PtcDetail[p].PaxType = FlightPassenger.GetPTC(ticket[i].PaxType);
                        break;
                    }
                }
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Ticketing, Severity.High, 0, "Exception in string pax type in ptc detail " + ex.Message + " Stack trace:- " + ex.StackTrace, ""); 
            }
        }

        /// <summary>
        /// Finding the total published price of all the passengers in the itinerary.
        /// </summary>
        /// <param name="itinerary">Itinerary for which the price is to be calculated.</param>
        /// <returns>The total published price of all the passengers.</returns>
        private decimal GetTotalPrice(FlightItinerary itinerary)
        {
            decimal totalPrice = 0;
            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                totalPrice += itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.Tax;
            }
            return totalPrice;
        }
        */
        ///// <summary>
        ///// Gets exception information and stack trace from exception in string format.
        ///// </summary>
        ///// <param name="ex">exception</param>
        ///// <param name="message">this message will be prefixed with the information.</param>
        ///// <returns></returns>
        //private string GetExceptionInformation(Exception ex, string message)
        //{
        //    StringBuilder messageText = new StringBuilder(2048);
        //    messageText.Append("\r\n");
        //    messageText.Append(message);                    // prefixing the message given.
        //    messageText.Append("\r\n\nError: ");
        //    messageText.Append(ex.Message);                 // appending exception message.
        //    messageText.Append("\r\nTargetsite: ");
        //    messageText.Append(ex.TargetSite);              // appending the method from where the exception originated.
        //    messageText.Append("\r\nSource: ");
        //    messageText.Append(ex.Source);                  // appending the name of application that caused exception.
        //    messageText.Append("\r\n\nStackTrace:\r\n");
        //    messageText.Append(ex.StackTrace);              // appending the stack trace.

        //    // checking if additional data is there in exception.
        //    if (ex.Data != null)
        //    {
        //        messageText.Append("\r\n\nAdditional Data: \r\n\t");
        //        // appending the data infromation with exception.
        //        foreach (DictionaryEntry de in ex.Data)
        //        {
        //            messageText.Append("\r\n");
        //            messageText.Append(de.Key);
        //            messageText.Append(": ");
        //            messageText.Append(de.Value);
        //        }
        //    }
        //    string excepMessage = messageText.ToString();
        //    if (ex.InnerException != null)
        //    {
        //        excepMessage = GetExceptionInformation(ex.InnerException, excepMessage + "\r\nInner Exception\r\n");
        //    }
        //    return excepMessage;
        //}

        private void SendMail(FlightItinerary itinerary, UserMaster member, bool isHold)
        {
            UserPreference preference = new UserPreference();
            try
            {
                preference = preference.GetPreference((int)member.ID, preference.BookingMade, preference.EmailNotification);
            }
            catch (ArgumentException)
            {
                preference.Value = "False";
            }
            if (Convert.ToBoolean(preference.Value))
            {
                string subject = "Booking Confirmed - PNR(" + itinerary.PNR + ")";
                string flightItinerary = string.Empty;
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    flightItinerary = flightItinerary + "-" + itinerary.Segments[i].Origin.AirportCode;
                }
                flightItinerary = flightItinerary + "-" + itinerary.Segments[itinerary.Segments.Length - 1].Destination.AirportCode;
                if (Convert.ToBoolean(ConfigurationSystem.Email["isSendEmail"]))
                {
                    Hashtable table = new Hashtable(3);
                    table.Add("firstName", member.FirstName);
                    table.Add("lastName", member.LastName);
                    table.Add("time", DateTime.UtcNow.Add(new TimeSpan(4, 0, 0)).ToString("hh:mm:ss tt"));
                    table.Add("date", DateTime.UtcNow.Add(new TimeSpan(4, 0, 0)).ToString("dd/MMM/yyyy"));
                    table.Add("pnr", itinerary.PNR);
                    table.Add("paxName", itinerary.Passenger[0].FullName);
                    table.Add("travelDate", itinerary.Segments[0].DepartureTime.ToString("dd/MMM/yyyy"));
                    table.Add("flight", itinerary.Segments[0].Airline + itinerary.Segments[0].FlightNumber);
                    table.Add("itinerary", flightItinerary);
                    //preference is getting written again here
                    preference = new UserPreference();
                    preference = preference.GetPreference((int)member.ID, preference.AlternateEmail, preference.AlternateEmail);

                    List<string> toArray = new List<string>();
                    if (preference.Value != null)
                    {
                        toArray.Add(preference.Value);
                    }
                    else
                    {
                        toArray.Add(member.Email);
                    }
                    try
                    {
                        if (isHold)
                        {
                            CT.Core.Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["replyTo"], toArray, subject, ConfigurationSystem.Email["bookingHold"], table);
                        }
                        else
                        {
                            CT.Core.Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["replyTo"], toArray, subject, ConfigurationSystem.Email["bookingTicketed"], table);
                        }
                    }
                    catch (Exception ex)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.Normal, (int)member.ID, "Sending Mail failed. Error : " + ex.Message, "0");
                    }
                }
            }
        }

        private void SendMail(FlightItinerary itinerary, UserMaster member, bool isHold, string errorMessage, BookingResponseStatus status)
        {
            //UserPreference preference = new UserPreference();
            //try
            //{
            //    preference = preference.GetPreference((int)member.ID, preference.BookingMade, preference.EmailNotification);
            //}
            //catch (ArgumentException)
            //{
            //    preference.Value = "False";
            //}
            //if (Convert.ToBoolean(preference.Value))
            {
                string subject = "Booking is Failed - PNR(" + itinerary.PNR + ")";
                string flightItinerary = string.Empty;
                Hashtable journeyDates = new Hashtable();
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    flightItinerary = flightItinerary + "-" + itinerary.Segments[i].Origin.AirportCode;
                    if (!journeyDates.ContainsKey(itinerary.Segments[i].Group))
                    {
                        journeyDates.Add(itinerary.Segments[i].Group, itinerary.Segments[i].DepartureTime);
                    }
                }
                flightItinerary = flightItinerary + "-" + itinerary.Segments[itinerary.Segments.Length - 1].Destination.AirportCode;
                flightItinerary += " - Onward Date : " + Convert.ToDateTime(journeyDates[0]).ToString("dd/MM/yyyy") + " - Return Date : " + Convert.ToDateTime(journeyDates[1]).ToString("dd/MM/yyyy");
                int adults = 0, childs = 0, infants = 0;
                foreach (FlightPassenger pax in itinerary.Passenger)
                {
                    switch (pax.Type)
                    {
                        case PassengerType.Adult:
                            adults++;
                            break;
                        case PassengerType.Child:
                            childs++;
                            break;
                        case PassengerType.Infant:
                            infants++;
                            break;
                    }
                }
                //if (Convert.ToBoolean(ConfigurationSystem.Email["SendEmail"]))
                {
                    AgentMaster agency = new AgentMaster(itinerary.AgencyId);
                    Hashtable table = new Hashtable(3);
                    table.Add("firstName", member.FirstName);
                    table.Add("lastName", member.LastName);
                    table.Add("time", DateTime.Now.ToString("hh:mm:ss tt"));
                    table.Add("date", DateTime.Now.ToString("dd/MMM/yyyy"));
                    table.Add("pnr", itinerary.PNR);
                    table.Add("paxName", itinerary.Passenger[0].FullName);
                    table.Add("travelDate", itinerary.Segments[0].DepartureTime.ToString("dd/MMM/yyyy"));
                    table.Add("flight", itinerary.Segments[0].Airline + itinerary.Segments[0].FlightNumber);
                    table.Add("itinerary", flightItinerary);
                    table.Add("agencyName", agency.Name);
                    table.Add("errorMessage", errorMessage);
                    table.Add("bookingStatus", status.ToString());
                    table.Add("paxCount", itinerary.Passenger.Length);
                    table.Add("adults", adults);
                    table.Add("childs", childs);
                    table.Add("infants", infants);

                    //preference is getting written again here
                    //preference = new UserPreference();
                    //preference = preference.GetPreference((int)member.ID, preference.AlternateEmail, preference.AlternateEmail);

                    List<string> toArray = new List<string>();
                    //if (preference.Value != null)
                    //{
                    //    toArray.Add(preference.Value);
                    //}
                    //else
                    //{
                    //    toArray.Add(member.Email);
                    //}
                    ConfigurationSystem con = new ConfigurationSystem();
                    Hashtable hostPort = con.GetHostPort();
                    toArray.Add(hostPort["ErrorNotificationMailingId"].ToString());


                    try
                    {
                        if (isHold)
                        {
                            CT.Core.Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["replyTo"], toArray, subject, ConfigurationSystem.Email["bookingHold"], table);
                        }
                        else
                        {
                            CT.Core.Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["replyTo"], toArray, subject, ConfigurationSystem.Email["bookingTicketed"], table);
                        }
                    }
                    catch (Exception ex)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.Normal, (int)member.ID, "Sending Mail failed. Error : " + ex.Message, "0");
                    }
                }
            }
        }


        private void SendMail(FlightItinerary itinerary, UserMaster member, bool isHold, string errorMessage, TicketingResponseStatus status)
        {
            //UserPreference preference = new UserPreference();
            //try
            //{
            //    preference = preference.GetPreference((int)member.ID, preference.BookingMade, preference.EmailNotification);
            //}
            //catch (ArgumentException)
            //{
            //    preference.Value = "False";
            //}
            //if (Convert.ToBoolean(preference.Value))
            {
                string subject = "Ticketing Failed for " + itinerary.FlightBookingSource.ToString() + " - PNR(" + itinerary.PNR + ")";
                string flightItinerary = string.Empty;
                Hashtable journeyDates = new Hashtable();
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    flightItinerary = flightItinerary + "-" + itinerary.Segments[i].Origin.AirportCode;
                    if (!journeyDates.ContainsKey(itinerary.Segments[i].Group))
                    {
                        journeyDates.Add(itinerary.Segments[i].Group, itinerary.Segments[i].DepartureTime);
                    }
                }
                flightItinerary = flightItinerary + "-" + itinerary.Segments[itinerary.Segments.Length - 1].Destination.AirportCode;
                flightItinerary += " - Onward Date : " + Convert.ToDateTime(journeyDates[0]).ToString("dd/MM/yyyy") + " - Return Date : " + Convert.ToDateTime(journeyDates[1]).ToString("dd/MM/yyyy");
                int adults = 0, childs = 0, infants = 0;
                foreach (FlightPassenger pax in itinerary.Passenger)
                {
                    switch (pax.Type)
                    {
                        case PassengerType.Adult:
                            adults++;
                            break;
                        case PassengerType.Child:
                            childs++;
                            break;
                        case PassengerType.Infant:
                            infants++;
                            break;
                    }
                }
                //if (Convert.ToBoolean(ConfigurationSystem.Email["SendEmail"]))
                {
                    AgentMaster agency = new AgentMaster(itinerary.AgencyId);
                    Hashtable table = new Hashtable(3);
                    table.Add("firstName", member.FirstName);
                    table.Add("lastName", member.LastName);
                    table.Add("time", DateTime.Now.ToString("hh:mm:ss tt"));
                    table.Add("date", DateTime.Now.ToString("dd/MMM/yyyy"));
                    table.Add("pnr", itinerary.PNR);
                    table.Add("paxName", itinerary.Passenger[0].FullName);
                    table.Add("travelDate", itinerary.Segments[0].DepartureTime.ToString("dd/MMM/yyyy"));
                    table.Add("flight", itinerary.Segments[0].Airline + itinerary.Segments[0].FlightNumber);
                    table.Add("itinerary", flightItinerary);
                    table.Add("agencyName", agency.Name);
                    table.Add("errorMessage", errorMessage);
                    table.Add("bookingStatus", status.ToString());
                    table.Add("paxCount", itinerary.Passenger.Length);
                    table.Add("adults", adults);
                    table.Add("childs", childs);
                    table.Add("infants", infants);

                    //preference is getting written again here
                    //preference = new UserPreference();
                    //preference = preference.GetPreference((int)member.ID, preference.AlternateEmail, preference.AlternateEmail);

                    List<string> toArray = new List<string>();
                    //if (preference.Value != null)
                    //{
                    //    toArray.Add(preference.Value);
                    //}
                    //else
                    //{
                    //    toArray.Add(member.Email);
                    //}
                    ConfigurationSystem con = new ConfigurationSystem();
                    Hashtable hostPort = con.GetHostPort();
                    toArray.Add(hostPort["ErrorNotificationMailingId"].ToString());

                    try
                    {
                        CT.Core.Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["replyTo"], toArray, subject, ConfigurationSystem.Email["bookingTicketed"], table);
                    }
                    catch (Exception ex)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.Normal, (int)member.ID, "Sending Mail failed. Error : " + ex.Message, "0");
                    }
                }
            }
        }

        private void UpdateAirlinePNR(FlightItinerary itinerary)
        {
            int flightId = FlightItinerary.GetFlightId(itinerary.PNR);
            if (flightId > 0)
            {
                FlightInfo[] segment = FlightInfo.GetSegments(flightId);
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    // if worldspan sends the segments in sequence.
                    if (itinerary.Segments[i].FlightKey == segment[i].FlightKey)
                    {
                        segment[i].AirlinePNR = itinerary.Segments[i].AirlinePNR;
                        segment[i].Save();
                    }
                    else    // if not sequential.
                    {
                        for (int j = 0; j < segment.Length; j++)
                        {
                            if (itinerary.Segments[i].FlightKey == segment[j].FlightKey)
                            {
                                segment[j].AirlinePNR = itinerary.Segments[i].AirlinePNR;
                                segment[j].Save();
                                break;
                            }
                        }
                    }
                }
            }
        }

        public static FlightItinerary RetrieveInfo(string requestedPnr, BookingSource bookingSource)
        {
            FlightItinerary itinerary = new FlightItinerary();
            //if (bookingSource == BookingSource.WorldSpan)
            //{
            //    itinerary = Worldspan.RetrieveInfo(requestedPnr);
            //}
            //else if (bookingSource == BookingSource.Amadeus)
            //{
            //    itinerary = Amadeus.RetrieveItinerary(requestedPnr);
            //}
            //else if (bookingSource == BookingSource.Galileo)
            //{
            //    itinerary = GalileoApi.RetrieveItinerary(requestedPnr);
            //}
            if (bookingSource == BookingSource.UAPI)
            {
              itinerary = UAPI.RetrieveItinerary(requestedPnr);
            }
            return itinerary;
        }

        public static void SaveSSR(FlightItinerary itinerary)
        {
            List<SSR> ssrList = itinerary.GetSsrList();
            foreach (SSR ssr in ssrList)
            {
                ssr.FlightId = itinerary.FlightId;
                ssr.Status = SSRStatus.Unknown;
                ssr.Save();
            }
        }

        public static void UpdateSSR(int flightId)
        {
            FlightItinerary itinerary = new FlightItinerary(flightId);
            if (itinerary.PNR == null || itinerary.PNR.Length == 0)
            {
                throw new ArgumentException("PNR should have value.", "PNR");
            }
            if (itinerary.FlightId == 0)
            {
                throw new ArgumentException("FlightId must be a positive integer.", "FlightId");
            }
            if ((int)itinerary.FlightBookingSource == 0)
            {
                throw new ArgumentException("FlightBookingSource must be defined.", "FlightBookingSource");
            }
            Dictionary<string, SSR> ssrList = new Dictionary<string, SSR>();
            List<SSR> tempSsrList = SSR.GetList(flightId);
            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                foreach (SSR ssr in tempSsrList)
                {
                    if (ssr.PaxId == itinerary.Passenger[i].PaxId)
                    {
                        if (ssr.SsrCode == "MEAL")
                        {
                            if (ssr.Detail.Length == 12)
                            {
                                string key = itinerary.Passenger[i].PaxKey + "-" + ssr.SsrCode + ssr.Detail.Substring(5, 7);
                                ssrList.Add(key, ssr);
                            }
                        }
                        else
                        {
                            ssrList.Add(itinerary.Passenger[i].PaxKey + "-" + ssr.SsrCode, ssr);
                        }
                    }
                }
            }
            Dictionary<string, SSR> gdsList = new Dictionary<string, SSR>();

            //if (itinerary.FlightBookingSource == BookingSource.WorldSpan)
            //{
            //    gdsList = Worldspan.RetrieveSSRInfo(itinerary.PNR);
            //}
            //else if (itinerary.FlightBookingSource == BookingSource.Amadeus)
            //{
            //    gdsList = Amadeus.RetrieveSSRInfo(itinerary.PNR);
            //}
            List<SSR> finalList = new List<SSR>();
            foreach (string ssrPaxKey in ssrList.Keys)
            {
                SSR ssr = new SSR();
                if (gdsList.ContainsKey(ssrPaxKey))
                {
                    ssr = SSR.Copy(ssrList[ssrPaxKey]);
                    ssr.Detail = gdsList[ssrPaxKey].Detail;
                    ssr.Status = SSRStatus.Accepted;
                    ssr.SsrStatus = gdsList[ssrPaxKey].SsrStatus;
                }
                else
                {
                    if (ssrList[ssrPaxKey].Status == SSRStatus.Accepted)
                    {
                        ssr = SSR.Copy(ssrList[ssrPaxKey]);
                        ssr.Status = SSRStatus.Deleted;
                    }
                    else if (ssrList[ssrPaxKey].Status == SSRStatus.Unknown)
                    {
                        ssr = SSR.Copy(ssrList[ssrPaxKey]);
                        ssr.Status = SSRStatus.Denied;
                    }
                    else
                    {
                        ssr = SSR.Copy(ssrList[ssrPaxKey]);
                    }
                }
                ssr.SsrId = 0;
                finalList.Add(ssr);
            }

            foreach (string ssrPaxKey in gdsList.Keys)
            {
                if (!ssrList.ContainsKey(ssrPaxKey))
                {
                    if (gdsList[ssrPaxKey].SsrCode == "OTHS")
                    {
                        SSR ssr = SSR.Copy(gdsList[ssrPaxKey]);
                        ssr.FlightId = itinerary.FlightId;
                        ssr.PaxId = 0;
                        ssr.Status = SSRStatus.Accepted;
                        finalList.Add(ssr);
                    }
                    else
                    {
                        string paxKey = (ssrPaxKey.Split('-'))[0];
                        for (int i = 0; i < itinerary.Passenger.Length; i++)
                        {
                            if (itinerary.Passenger[i].PaxKey == paxKey)
                            {
                                SSR ssr = SSR.Copy(gdsList[ssrPaxKey]);
                                ssr.FlightId = itinerary.FlightId;
                                ssr.PaxId = itinerary.Passenger[i].PaxId;
                                ssr.Status = SSRStatus.Accepted;
                                finalList.Add(ssr);
                                break;
                            }
                        }
                    }
                }
            }
            SSR.Delete(itinerary.FlightId);
            foreach (SSR ssr in finalList)
            {
                ssr.Save();
            }
        }
        #endregion

        #region Supporting Public Methods.
        public static bool IsInstantPurchase(SearchResult result) //ziya-todo
        {
            bool isInstantPurchase = false;
            //if (result.ResultBookingSource == BookingSource.Amadeus)
            //{
            //    isInstantPurchase = Amadeus.IsInstantPurchase(result);
            //}
            //else if (result.ResultBookingSource == BookingSource.WorldSpan)
            //{
            //    isInstantPurchase = Worldspan.IsInstantPurchase(result);
            //}
             if (result.ResultBookingSource == BookingSource.Galileo)
            {
                isInstantPurchase = false;
            }
            else if (result.ResultBookingSource == BookingSource.UAPI)
            {
                isInstantPurchase = false;
            }
            else
            {
                isInstantPurchase = true;   // For LCC. 
            }
            return isInstantPurchase;
        }

        #endregion


        #region Import Hotel
        public bool GetBookedHotel(ref HotelItinerary itinerary)
        {
            Trace.TraceInformation("MetaSearchEngine.GetBookedHotel Entered | " + DateTime.Now);
            bool hasItinerary = false;
           /* if (itinerary.Source == HotelBookingSource.Desiya)
            {
                //no code implemented now
            }
            if (itinerary.Source == HotelBookingSource.GTA)
            {
                try
                {
                    GTA gtaApi = new GTA();
                    hasItinerary = gtaApi.GetBookedHotel(ref itinerary);
                }
                catch (BookingEngineException Ex)
                {
                    CT.Core.Audit.Add(EventType.GTAImportHotel, Severity.High, 1, "Exception in Importing GTA Hotel. Message:" + Ex.Message, "");
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    CT.Core.Audit.Add(EventType.GTAImportHotel, Severity.High, 1, "Exception in Importing GTA Hotel. Message:" + exp.Message, "");
                    throw new Exception(exp.Message);
                }

            }
            if (itinerary.Source == HotelBookingSource.Tourico)
            {
                try
                {
                    Tourico touricoApi = new Tourico();
                    hasItinerary = touricoApi.GetBookedHotel(ref itinerary);
                }
                catch (BookingEngineException Ex)
                {
                    CT.Core.Audit.Add(EventType.TouricoImport, Severity.High, 1, "Exception in Importing Tourico Hotel. Message:" + Ex.Message, "");
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    CT.Core.Audit.Add(EventType.TouricoImport, Severity.High, 1, "Exception in Importing Tourico Hotel. Message:" + exp.Message, "");
                    throw new Exception(exp.Message);
                }
            }
            if (itinerary.Source == HotelBookingSource.IAN)
            {
                try
                {
                    IAN ianApi = new IAN();
                    hasItinerary = ianApi.GetBookedHotel(ref itinerary);
                }
                catch (BookingEngineException Ex)
                {
                    CT.Core.Audit.Add(EventType.IANBooking, Severity.High, 1, "Exception in Importing IAN Hotel. Message:" + Ex.Message, "");
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    CT.Core.Audit.Add(EventType.IANBooking, Severity.High, 1, "Exception in Importing IAN Hotel. Message:" + exp.Message, "");
                    throw new Exception(exp.Message);
                }
            }
            if (itinerary.Source == HotelBookingSource.HotelBeds)
            {
                try
                {
                    HotelBeds hotelBedsApi = new HotelBeds();
                    hasItinerary = hotelBedsApi.GetBookedHotel(ref itinerary);
                }
                catch (BookingEngineException Ex)
                {
                    CT.Core.Audit.Add(EventType.HotelBedsImport, Severity.High, 1, "Exception in Importing HotelBeds Hotel. Message:" + Ex.Message, "");
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    CT.Core.Audit.Add(EventType.HotelBedsImport, Severity.High, 1, "Exception in Importing HotelBeds Hotel. Message:" + exp.Message, "");
                    throw new Exception(exp.Message);
                }
            }
           
            if (itinerary.Source == HotelBookingSource.WST)
            {
                try
                {
                    WST wst = new WST();
                    hasItinerary = wst.GetBookedHotel(ref itinerary);
                }
                catch (BookingEngineException Ex)
                {
                    CT.Core.Audit.Add(EventType.WSTImport, Severity.High, 1, "Exception in Importing WST Hotel. Message:" + Ex.Message, "");
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    CT.Core.Audit.Add(EventType.WSTImport, Severity.High, 1, "Exception in Importing WST Hotel. Message:" + exp.Message, "");
                    throw new Exception(exp.Message);
                }
            }*/
            if (itinerary.Source == HotelBookingSource.DOTW)
            {
                /*try ziya-todo
                {
                    DOTWApi dotw = new DOTWApi();
                    hasItinerary = dotw.GetBookedHotel(ref itinerary);
                }
                catch (BookingEngineException Ex)
                {
                    CT.Core.Audit.Add(EventType.DOTWImport, Severity.High, 1, "Exception in Importing DOTW Hotel. Message:" + Ex.Message, "");
                    throw new BookingEngineException(Ex.Message);
                }
                catch (Exception exp)
                {
                    CT.Core.Audit.Add(EventType.DOTWImport, Severity.High, 1, "Exception in Importing DOTW Hotel. Message:" + exp.Message, "");
                    throw new Exception(exp.Message);
                }*/
            }
            Trace.TraceInformation("MetaSearchEngine.GetBookedHotel Exited | " + DateTime.Now);
            return hasItinerary;
        }

        public string SaveHotelItinerary(HotelItinerary itinerary, BookingStatus status, int agencyId, long member)
        {
            Trace.TraceInformation("MetaSearchEngine.SaveHotelItinerary Entered | " + DateTime.Now);
            // Saving the booked hotel itinerary to database
            Audit.Add(EventType.MetaSearchEngine, Severity.High, (int)member, "Try to save the booking data : BookingId " + itinerary.BookingRefNo, "");
            string errorString = string.Empty;
            //booking details here
            BookingDetail booking = new BookingDetail();
            booking.AgencyId = agencyId;
            booking.CreatedBy = (int)member;
            booking.CreatedOn = DateTime.UtcNow;
            booking.Status = status;//itinerary.Status;
            //queue details here
            CT.Core.Queue queue = new CT.Core.Queue();
            queue.QueueTypeId = (int)QueueType.Booking;
            queue.StatusId = (int)QueueStatus.Assigned;
            queue.AssignedTo = (int)member;
            queue.AssignedBy = (int)member;
            queue.AssignedDate = DateTime.UtcNow;
            queue.CompletionDate = DateTime.UtcNow.AddDays(1);
            queue.CreatedBy = (int)member;

            //TODO: For New product -- Start                       
            itinerary.ProductType = ProductType.Hotel;
            booking.ProductsList = new Product[1];
            booking.ProductsList[0] = (Product)itinerary;

            //For New product -- End

            // Saving the booked hotel itinerary to database                  
            try
            {
                HotelItinerary hIten = new HotelItinerary();
                bool isBookingExists = hIten.IsBookingAlreadyExist(itinerary.ConfirmationNo, itinerary.Source);
                //save the data into database only when it is not present
                if (!isBookingExists)
                {
                    using (TransactionScope updateTransaction = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                    {
                        itinerary.BookingMode = BookingMode.Import;
                        //For New product -- Start                       
                        booking.SaveBooking(false);//true for import flight booking
                        queue.ItemId = booking.BookingId;
                        queue.Save();
                        updateTransaction.Complete();
                        errorString = "Booking Successfully Saved.";
                    }
                }
                else
                {
                    errorString = "Booking Already exists.";
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)member, "Save booking failed for hotel. Error : " + ex.Message + " | " + DateTime.Now + " | " + itinerary.Source + " Confirmation No = " + itinerary.ConfirmationNo, "0");
                throw new BookingEngineException(ex.Message);
            }
            Trace.TraceInformation("MetaSearchEngine.SaveHotelItinerary Exited | " + DateTime.Now);
            return errorString;
        }

        #endregion

       

        #region Amendment Hotel
        public bool AmendBookedHotel(ref HotelItinerary itinearary, bool isPaxChange, bool isItemChange, int bookingId)
        {
            Trace.TraceInformation("MetaSearchEngine.AmendBookedHotel Entered | " + DateTime.Now);
            try
            {
               /* GTA gtaApi = new GTA();
                // checks whether both Pax and itemchange
                if (isItemChange && isPaxChange)
                {
                    gtaApi.AmendPaxDetails(ref itinearary);
                    gtaApi.ModifyBookingItem(ref itinearary);
                }
                else if (isItemChange && !isPaxChange)
                {
                    gtaApi.ModifyBookingItem(ref itinearary);
                }
                else if (!isItemChange && isPaxChange)
                {
                    gtaApi.AmendPaxDetails(ref itinearary);
                }

                using (TransactionScope updateTransaction = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                {
                    //Save the data in booking History
                    BookingHistory amendHistory = new BookingHistory();
                    //Loading Previous to store it.
                    HotelItinerary prevItineary = new HotelItinerary();
                    prevItineary.Load(itinearary.HotelId);
                    HotelRoom roomData = new HotelRoom();
                    HotelRoom[] prevRoomInfo = new HotelRoom[0];
                    prevRoomInfo = roomData.Load(prevItineary.HotelId);
                    string remarks = string.Empty;
                    remarks = "Hotel Itinearary Updated,Previous data: HotelId" + prevItineary.HotelId.ToString() + ", HotelName: " + prevItineary.HotelName + ", Check In :" + prevItineary.StartDate.ToString("dd/MM/yyyy") + ", CheckOut :" + prevItineary.EndDate.ToString("dd/MM/yyyy");
                    PriceAccounts priceInfo = new PriceAccounts();
                    foreach (HotelRoom roomInfo in prevRoomInfo)
                    {
                        priceInfo = roomInfo.Price;
                        remarks += ",Price Info# NetFare: " + priceInfo.NetFare + ",MarkUp:  " + priceInfo.NetFare + ",Currency Code: " + priceInfo.CurrencyCode + ",RateOf Exchange :" + priceInfo.RateOfExchange;
                        remarks += "*** Passgener Info ***\n";
                        foreach (HotelPassenger passInfo in roomInfo.PassenegerInfo)
                        {
                            remarks += ",First Name:" + passInfo.Firstname + " ,LastName: " + passInfo.Lastname + "  ,Is Lead Pax:" + passInfo.LeadPassenger.ToString() + " |\n";
                        }
                    }
                    amendHistory.BookingId = bookingId;
                    amendHistory.EventCategory = EventCategory.HotelAmendment;
                    amendHistory.CreatedBy = 1;
                    amendHistory.LastModifiedBy = 1;
                    amendHistory.Remarks = remarks;
                    amendHistory.Save();
                    // Gets Updated status.
                    if (gtaApi.GetUpdatedStatus(ref itinearary))
                    {
                        //Update the Itinearary.
                        UpdateItinearary(itinearary, isItemChange, isPaxChange);
                    }
                    updateTransaction.Complete();
                }&*/

            }
            catch (Exception exp)
            {
                CT.Core.Audit.Add(CT.Core.EventType.GTABooking, CT.Core.Severity.High, 1, "Amendment for Hotel is Failed. Error : " + exp.Message + " | " + DateTime.Now + " | " + itinearary.Source + "0", string.Empty);
                throw new Exception(exp.Message);

            }
            Trace.TraceInformation("MetaSearchEngine.AmendBookedHotel Exited | " + DateTime.Now);
            return true;
        }
        /// <summary>
        /// This method is used to update the Hotel Itinearary.
        /// </summary>
        /// <param name="itinearary"></param>
        /// <param name="isItemChange"></param>
        /// <param name="isPaxChange"></param>
        public void UpdateItinearary(HotelItinerary itinearary, bool isItemChange, bool isPaxChange)
        {
            Trace.TraceInformation("MetaSearchEngine.UpdateItinearary Entered | " + DateTime.Now);
            itinearary.UpdateItinearary((Product)itinearary, isItemChange, isPaxChange);
            Trace.TraceInformation("MetaSearchEngine.UpdateItinearary Exited | " + DateTime.Now);
        }

        #endregion

        #region CANCELLATION CHARGES AND HOTEL DETAILS

        /// <summary>
        /// get the hotel detail for any source
        /// </summary>
        /// <param name="cityCode"></param>
        /// <param name="itemName"></param>
        /// <param name="itemCode"></param>
        /// <returns></returns>
        public HotelDetails GetHotelDetails(string cityCode,  string itemCode,DateTime startDate,DateTime endDate,string paxNationality,string paxResidence, HotelBookingSource source)
        {
            HotelDetails hotelDetail = new HotelDetails();
            //Getting Specific Hotel Details.
            /*if (source == HotelBookingSource.Desiya)
            {
                try
                {
                    DesiyaApi desiya = new DesiyaApi();
                    hotelDetail = desiya.GetHotelDetails(cityCode, itemCode);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DesiyaSpecificDetail, CT.Core.Severity.High, 0, "Exception returned from Desiya.GetHotelDetails Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.GTA)
            {
                try
                {
                    GTA gtaApi = new GTA();
                    hotelDetail = gtaApi.GetItemInformation(cityCode, itemName, itemCode);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.GTAItemSearch, CT.Core.Severity.High, 0, "Exception returned from GTA.GetItemInformation Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.HotelBeds)
            {
                try
                {
                    HotelBeds hBeds = new HotelBeds();
                    hBeds.sessionId = sessionId;
                    hotelDetail = hBeds.GetItemInformation(cityCode, itemName, itemCode);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelBedsAvailSearch, CT.Core.Severity.High, 0, "Exception returned from HotelBeds.GetItemInformation Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.Tourico)
            {
                try
                {
                    Tourico tourico = new Tourico();
                    tourico.sessionId = sessionId;
                    hotelDetail = tourico.GetItemInformation(cityCode, itemName, itemCode);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.TouricoAvailSearch, CT.Core.Severity.High, 0, "Exception returned from Tourico.GetItemInformation Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.IAN)
            {
                try
                {
                    IAN ian = new IAN(sessionId);
                    hotelDetail = ian.GetItemInformation(cityCode, itemName, itemCode);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.IANItemInformation, CT.Core.Severity.High, 0, "Exception returned from IAN.GetItemInformation Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.TBOConnect)
            {
                try
                {
                    HotelConnect hc = new HotelConnect();
                    hotelDetail = hc.GetItemInformation(cityCode, itemName, itemCode);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HotelConnectItemInformation, Severity.High, 0, "Exception returned from hotelconnect" + ex.Message, "");
                    throw new BookingEngineException("Error:" + ex.Message);
                }
            }
            else if (source == HotelBookingSource.Miki)
            {
                try
                {
                    MikiApi miki = new MikiApi(sessionId);
                    hotelDetail = miki.GetItemInformation(cityCode, itemName, itemCode);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.MikiAvailSearch, CT.Core.Severity.High, 0, "Exception returned from Miki.GetItemInformation Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.Travco)
            {
                try
                {
                    TravcoApi travco = new TravcoApi();
                    hotelDetail = travco.GetItemInformation(cityCode, itemName, itemCode);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.TravcoAvailSearch, CT.Core.Severity.High, 0, "Exception returned from Travco.GetItemInformation Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.DOTW)
            {
                try
                {
                    DOTWApi dotw = new DOTWApi(sessionId);
                    hotelDetail = dotw.GetItemInformation(cityCode, itemName, itemCode);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetItemInformation Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.WST)
            {
                try
                {
                    WST wst = new WST();
                    hotelDetail = wst.GetItemInformation(cityCode, itemName, itemCode);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.WSTAvailSearch, CT.Core.Severity.High, 0, "Exception returned from WST.GetItemInformation Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }*/
            if (source == HotelBookingSource.DOTW)
            {
                try
                {
                    CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi(sessionId);
                    dotw.AppUserId = (int)SettingsLoginInfo.UserID;
                    //DOTWApi dotw = new DOTWApi(sessionId);
                    hotelDetail = dotw.GetItemInformation(cityCode,itemCode,startDate,endDate,paxNationality,paxResidence  );
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned from DOTW.GetItemInformation Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.LOH)
            {
                try
                {
                    LotsOfHotels.JuniperXMLEngine engine = new LotsOfHotels.JuniperXMLEngine(sessionId);
                    hotelDetail = engine.GetHotelDetails(itemCode, cityCode);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DOTWAvailSearch, CT.Core.Severity.High, 0, "Exception returned from LOH.GetItemDetails Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.HotelBeds) //Added by brahmam 26.09.2014
            {
                try
                {
                    HotelBeds hBeds = new HotelBeds();
                    hBeds.sessionId = sessionId;
                    hotelDetail = hBeds.GetItemInformation(cityCode, "", itemCode);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelBedsAvailSearch, CT.Core.Severity.High, 0, "Exception returned from HotelBeds.GetItemInformation Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.GTA)
            {
                try
                {
                    GTA gtaApi = new GTA();
                    hotelDetail = gtaApi.GetItemInformation(cityCode, "", itemCode);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.GTAItemSearch, CT.Core.Severity.High, 0, "Exception returned from GTA.GetItemInformation Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.RezLive)
            {
                RezLive.XmlHub rezAPI = new RezLive.XmlHub(sessionId);
                //hotelDetail = rezAPI.GetHotelDetails(itemCode);
                //Modified by brahmam requarding if static data is not there at the time should be download
                hotelDetail = rezAPI.GetHotelDetailsInformation(cityCode,"",itemCode);
            }
            else if (source == HotelBookingSource.Miki) //Added by Brahmam St
            {
                CT.BookingEngine.GDS.MikiApi miki = new CT.BookingEngine.GDS.MikiApi();
                hotelDetail = miki.getStaticInfoOfProduct(cityCode, string.Empty, itemCode);
            }
            else if (source == HotelBookingSource.WST) //Added by brahmam static data purpose
            {
                CT.BookingEngine.GDS.WST wstApi = new CT.BookingEngine.GDS.WST();
                hotelDetail = wstApi.GetItemInformation(cityCode, string.Empty, itemCode);
            }
            return hotelDetail;
        }

        /// <summary>
        /// get the cancellation detail before booking
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="penalityList"></param>
        /// <param name="isSave">whether to save penalty list : incase of Ajax call it will be false, while booking true</param>
        /// <param name="source">Booking Source</param>
        /// <returns></returns>
        public Dictionary<string, string> GetCancellationDetails(HotelItinerary itinerary, ref List<HotelPenality> penalityList, bool isSave, HotelBookingSource source)
        {
            Dictionary<string, string> polList = new Dictionary<string, string>();
            // Hotel agreement Details (Hotel policy and Cancellation Policy) - start
            /*if (source == HotelBookingSource.Desiya)
            {
                try
                {
                    DesiyaApi desiya = new DesiyaApi();
                    polList = desiya.GetBookingAgreement(itinerary.HotelCode, itinerary.Roomtype[0].RatePlanCode, itinerary.Roomtype[0].RoomTypeCode, itinerary.StartDate, itinerary.EndDate);  // bug send check In data & check out date in request                 }
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.DesiyaBookingAgreement, CT.Core.Severity.High, 0, "Exception returned from Desiya.GetBookingAgreement Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.GTA)
            {
                try
                {
                    GTA gtaApi = new GTA();
                    polList = gtaApi.GetHotelChargeCondition(itinerary, ref penalityList, isSave);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.GTAItemSearch, CT.Core.Severity.High, 0, "Exception returned from GTA.GetHotelChargeCondition Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.HotelBeds)
            {
                try
                {
                    HotelBeds hBeds = new HotelBeds();
                    hBeds.sessionId = sessionId;
                    polList = hBeds.GetHotelChargeCondition(itinerary, ref penalityList, isSave);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelBedsCancel, CT.Core.Severity.High, 0, "Exception returned from  HotelBeds.GetHotelChargeCondition Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.Tourico)
            {
                try
                {
                    Tourico tourico = new Tourico();
                    tourico.sessionId = sessionId;
                    polList = tourico.GetCancellationPolicy(itinerary, ref penalityList, isSave);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.TouricoCancel, CT.Core.Severity.High, 0, "Exception returned from  Tourico.GetCancellationPolicy Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.IAN)
            {
                try
                {
                    IAN ian = new IAN(sessionId);
                    polList = ian.GetCancellationPolicy(itinerary, ref penalityList, isSave);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.IANCancel, CT.Core.Severity.High, 0, "Exception returned from  IAN.GetCancellationPolicy Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.TBOConnect)
            {
                try
                {
                    HotelConnect hc = new HotelConnect();
                    polList = hc.GetCancellationPolicy(itinerary, ref penalityList, isSave);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HotelConnectCancel, Severity.High, 0, "Exception returned from HotelConnect.GetCancelationPolicy Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error:" + ex.Message);
                }
            }
            else if (source == HotelBookingSource.Miki)
            {
                try
                {
                    MikiApi mikiCan = new MikiApi(sessionId);
                    polList = mikiCan.GetCancellationPolicy(itinerary, ref penalityList, isSave);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.MikiCancel, Severity.High, 0, "Exception returned from Miki.GetCancelationPolicy Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error:" + ex.Message);
                }
            }
            else if (source == HotelBookingSource.Travco)
            {
                try
                {
                    TravcoApi travco = new TravcoApi();
                    polList = travco.GetHotelChargeCondition(itinerary, ref penalityList, isSave);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.TravcoCancel, Severity.High, 0, "Exception returned from Travco.GetCancelationPolicy Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error:" + ex.Message);
                }
            }
            
            else if (source == HotelBookingSource.WST)
            {
                try
                {
                    WST wst = new WST();
                    polList = wst.GetHotelChargeCondition(itinerary, ref penalityList, isSave);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.WSTCancel, Severity.High, 0, "Exception returned from WST.GetHotelChargeCondition Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error:" + ex.Message);
                }
            }
             */
            if (source == HotelBookingSource.DOTW)
            {
                try
                {
                    CT.BookingEngine.GDS.DOTWApi dotwCan = new CT.BookingEngine.GDS.DOTWApi(sessionId);
                    dotwCan.AppUserId = (int)SettingsLoginInfo.UserID;
                    if (SettingsLoginInfo.IsOnBehalfOfAgent)
                    {
                        dotwCan.DecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                    }
                    else
                    {
                        dotwCan.DecimalPoint = SettingsLoginInfo.DecimalValue;
                    }
                    //dotwCan.BlockRooms(itinerary, true);
                    polList = dotwCan.GetCancellationPolicy(itinerary, ref penalityList, isSave);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.DOTWCancel, Severity.High, 0, "Exception returned from DOTW.GetCancelationPolicy Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error:" + ex.Message);
                }
            }
            else if (source == HotelBookingSource.HotelBeds) //Added by brahmam 26.09.2014
            {
                try
                {
                    HotelBeds hBeds = new HotelBeds();
                    hBeds.sessionId = sessionId;
                    if (SettingsLoginInfo.IsOnBehalfOfAgent)
                    {
                        hBeds.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                        hBeds.AgentExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                        hBeds.AgentDecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                    }
                    else
                    {
                        hBeds.AgentCurrency = SettingsLoginInfo.Currency;
                        hBeds.AgentExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                        hBeds.AgentDecimalPoint = SettingsLoginInfo.DecimalValue;
                    }
                    polList = hBeds.GetHotelChargeCondition(itinerary, ref penalityList, isSave);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelBedsCancel, CT.Core.Severity.High, 0, "Exception returned from  HotelBeds.GetHotelChargeCondition Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.GTA)
            {
                try
                {
                    GTA gtaApi = new GTA();
                    if (SettingsLoginInfo.IsOnBehalfOfAgent)
                    {
                        gtaApi.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                        gtaApi.AgentExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                        gtaApi.AgentDecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                    }
                    else
                    {
                        gtaApi.AgentCurrency = SettingsLoginInfo.Currency;
                        gtaApi.AgentExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                        gtaApi.AgentDecimalPoint = SettingsLoginInfo.DecimalValue;
                    }
                    DOTWCountry dotw = new DOTWCountry();
                    Dictionary<string, string> Countries = dotw.GetAllCountries();
                    itinerary.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[itinerary.PassengerNationality]);
                    polList = gtaApi.GetHotelChargeCondition(itinerary, ref penalityList, isSave);
                }
                catch (Exception ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.GTAItemSearch, CT.Core.Severity.High, 0, "Exception returned from GTA.GetHotelChargeCondition Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else if (source == HotelBookingSource.RezLive)
            {
                decimal supplierPrice = 0;
                foreach (HotelRoom room in itinerary.Roomtype)
                {
                    supplierPrice += room.Price.SupplierPrice;
                }
                RezLive.XmlHub rezAPI = new RezLive.XmlHub(sessionId);
                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    rezAPI.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    rezAPI.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    rezAPI.DecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    rezAPI.AgentCurrency = SettingsLoginInfo.Currency;
                    rezAPI.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    rezAPI.DecimalPoint = SettingsLoginInfo.DecimalValue;
                }
                polList = rezAPI.GetCancellationInfo(itinerary, supplierPrice);
            }
            else if (source == HotelBookingSource.WST)
            {
                try
                {
                    CT.BookingEngine.GDS.WST wstApi = new CT.BookingEngine.GDS.WST();
                    if (SettingsLoginInfo.IsOnBehalfOfAgent)
                    {
                        wstApi.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                        wstApi.AgentExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                        wstApi.AgentDecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                    }
                    else
                    {
                        wstApi.AgentCurrency = SettingsLoginInfo.Currency;
                        wstApi.AgentExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                        wstApi.AgentDecimalPoint = SettingsLoginInfo.DecimalValue;
                    }
                    polList = wstApi.GetHotelChargeCondition(itinerary, ref penalityList, isSave);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.WSTCancel, Severity.High, 0, "Exception returned from wst.GetHotelChargeCondition Error Message:" + ex.Message + DateTime.Now, "");
                    throw new BookingEngineException("Error:" + ex.Message);
                }
            }

            #region Currency Petra commented
            //Load rate of exchange and multiply with the cancellation charge and apply the jod currency
            //string cancellationPolicy = polList["CancelPolicy"];
            //string currencyCode = string.Empty;
            //string currencySign = string.Empty;
            //int index = 0;
            //currencyCode = itinerary.Roomtype[0].Price.CurrencyCode;
            //currencySign = Util.GetCurrencySymbol(currencyCode);
            //StaticData staticInfo = new StaticData(); // Sheel : To fetch the rate of exchange from table for INR
            //Dictionary<string, decimal> rateList = new Dictionary<string, decimal>();
            //rateList = staticInfo.CurrencyROE;
            //decimal rateOfExc = 1;
            //if (rateList.ContainsKey(currencyCode))
            //{
            //    rateOfExc = rateList[currencyCode];

            //}


            //}


            //string tempS = string.Empty;
            //string[] tempSplit = cancellationPolicy.Split('|');
            //foreach (string temp in tempSplit)
            //{
            //    if (temp.IndexOf(currencySign) != -1)
            //    {
            //        index = temp.IndexOf(currencySign);

            //        if (temp.Substring(index + 1)[0] == ' ')
            //        {
            //            tempS = temp.Substring(index + 2);
            //        }
            //        else
            //        {
            //            tempS = temp.Substring(index + 1);
            //        }
            //        string priceString = string.Empty;
            //        foreach (char i in tempS)
            //        {
            //            if (i.ToString() == " ")
            //            {
            //                break;
            //            }
            //            priceString += i;
            //        }
            //        cancellationPolicy = cancellationPolicy.Replace(priceString, ((rateOfExc * (Convert.ToDecimal(priceString))).ToString()));
            //    }
            //}

            //cancellationPolicy = cancellationPolicy.Replace(currencySign, Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"]);

            //polList["CancelPolicy"] = cancellationPolicy;
            #endregion



            return polList;

        }

        //modified by brahmam   20/09/2015// depriciated by ziyad
        public Dictionary<string, string> GetCancellationDetailsOLDNotUsing(HotelItinerary itinerary, decimal hprice)
        {
            Dictionary<string, string> polList = new Dictionary<string, string>();
            try
            {
                RezLive.XmlHub rezAPI = new RezLive.XmlHub(sessionId);
                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    rezAPI.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    rezAPI.ExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    rezAPI.DecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    rezAPI.AgentCurrency = SettingsLoginInfo.Currency;
                    rezAPI.ExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    rezAPI.DecimalPoint = SettingsLoginInfo.DecimalValue;
                }
                polList = rezAPI.GetCancellationInfo(itinerary,hprice);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return polList;
        }

        /// <summary>
        /// get all the available rooms for the selected hotel.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public HotelRoomsDetails[] GetMoreRoomsOfAHotel(HotelItinerary itinerary)
        {
            //IAN ian = new IAN(sessionId);
            //return ian.GetAllAvailableRooms(itinerary);
            return new HotelRoomsDetails[1];
        }

        #endregion


        # region To ReCalculate WLCharge ans WLDiscount in case of B2B2B agents
        /// <summary>
        /// Get Subagent price in case of booked other
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        /*private PriceAccounts GetSubAgentPrice(FlightItinerary itinerary, PriceAccounts price) ziya-todo
        {
            Dictionary<string, string> b2b2bVariableList = new Dictionary<string, string>();
            b2b2bVariableList = (Dictionary<string, string>)HttpContext.Current.Session["b2b2bVariable"];
            string airlineCommissionXML = Convert.ToString(b2b2bVariableList["b2b2bAirlineCommissionList"]);
            Dictionary<string, string> airlineCommissionList = new Dictionary<string, string>();

            airlineCommissionList = GetAirLineCommission(airlineCommissionXML);
            if (airlineCommissionList.ContainsKey(itinerary.Segments[0].Airline))
            {
                price.WhiteLabelDiscount = Math.Round((price.AgentCommission + price.AgentPLB) * Convert.ToDecimal(airlineCommissionList[itinerary.Segments[0].Airline]) / 100);
            }
            else
            {
                price.WhiteLabelDiscount = price.AgentCommission;
            }
            decimal serviceFee = price.AdditionalTxnFee;
            // airline wise service fee
            price.WLCharge = GetServiceFeeOfB2B2B(price, Convert.ToString(b2b2bVariableList["b2b2bAirlineServiceFeeList"]), itinerary.Segments[0].Airline);
            // apply default service fee only when there is no airline wise service fee for this airline
            if (price.WLCharge <= 0)
            {
                price.WLCharge = GetDefaultServiceFeeOfB2B2B(price, Convert.ToString(b2b2bVariableList["b2b2bDefaultServiceFee"]), itinerary.IsDomestic);
            }
            return price;
        }

        /// <summary>
        /// Get dictionary from Airline commission XML
        /// </summary>
        /// <param name="airlineCommissionList"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetAirLineCommission(string airlineCommissionXML)
        {
            XmlDocument doc = new XmlDocument();
            Dictionary<string, string> airlineCommissionList = new Dictionary<string, string>();
            doc.InnerXml = airlineCommissionXML;
            XmlNodeList nodeList = doc.SelectNodes("ArrayOfAirlineCommission/AirlineCommission");
            string airlineCode = string.Empty;
            foreach (XmlNode node in nodeList)
            {
                foreach (XmlNode childNodePassenger in node.ChildNodes)
                {
                    if (childNodePassenger.Name == "AirlineCode")
                    {
                        airlineCode = childNodePassenger.FirstChild.Value;
                    }
                    else if (childNodePassenger.Name == "Commission")
                    {
                        airlineCommissionList.Add(airlineCode, childNodePassenger.FirstChild.Value);
                    }
                }
            }
            return airlineCommissionList;
        }

        /// <summary>
        /// Send Service fee xml and Add service fee in OC
        /// </summary>
        /// <param name="serviceFeeXML"></param>
        /// <returns></returns>
        private static decimal GetServiceFeeOfB2B2B(PriceAccounts price, string serviceFeeXML, string airlineCode)
        {
            decimal serviceCharge = 0;
            XmlDocument doc = new XmlDocument();
            doc.InnerXml = serviceFeeXML;
            XmlNodeList nodeList = doc.SelectNodes("ArrayOfAirlineServiceFee/AirlineServiceFee");
            string airlineCodeName = string.Empty;
            decimal serviceFee = 0;
            string serviceFeeType = "";
            foreach (XmlNode node in nodeList)
            {
                foreach (XmlNode childNodePassenger in node.ChildNodes)
                {
                    if (childNodePassenger.Name == "AirlineCode")
                    {
                        airlineCodeName = childNodePassenger.FirstChild.Value;
                    }
                    if (childNodePassenger.Name == "ServiceFee")
                    {
                        serviceFee = Convert.ToDecimal(childNodePassenger.FirstChild.Value);
                    }
                    if (childNodePassenger.Name == "ServiceFeeType")
                    {
                        serviceFeeType = childNodePassenger.FirstChild.Value;
                    }
                }
                if (airlineCodeName == airlineCode)
                {
                    if (serviceFeeType == "Fixed")
                    {

                        serviceCharge = serviceFee;
                    }
                    else if (serviceFeeType == "Percentage")
                    {
                        decimal b2b2bCharges = 0;
                        b2b2bCharges = Math.Round((price.PublishedFare + price.Tax + price.OtherCharges) * serviceFee / 100);
                        serviceCharge = b2b2bCharges;
                    }
                    return serviceCharge;
                }
            }
            return serviceCharge;
        }

        /// <summary>
        /// Get Default Service Charge of B2B2B
        /// </summary>
        /// <param name="result">Return Price with Service Fee</param>
        /// <param name="serviceFeeXML"> </param>
        /// <returns></returns>
        private static decimal GetDefaultServiceFeeOfB2B2B(PriceAccounts price, string defultServiceFeeXML, bool isDomesticFlight)
        {
            decimal serviceCharge = 0;
            XmlDocument doc = new XmlDocument();
            doc.InnerXml = defultServiceFeeXML;
            XmlNodeList nodeList = doc.SelectNodes("ArrayOfFlightServiceCharge/FlightServiceCharge");
            decimal serviceFee = 0;
            string serviceFeeType = "";
            bool isDomestic = true;
            foreach (XmlNode node in nodeList)
            {
                foreach (XmlNode childNodePassenger in node.ChildNodes)
                {
                    if (childNodePassenger.Name == "ServiceCharge")
                    {
                        serviceFee = Convert.ToDecimal(childNodePassenger.FirstChild.Value);
                    }
                    if (childNodePassenger.Name == "ServiceChargeType")
                    {
                        serviceFeeType = childNodePassenger.FirstChild.Value;
                    }
                    if (childNodePassenger.Name == "IsDomesticServiceCharge")
                    {
                        isDomestic = Convert.ToBoolean(childNodePassenger.FirstChild.Value);
                    }
                }
                if (isDomesticFlight == isDomestic)
                {
                    if (serviceFeeType == "Fixed")
                    {
                        serviceCharge = serviceFee;
                    }
                    else if (serviceFeeType == "Percentage")
                    {
                        decimal b2b2bCharges = 0;
                        serviceCharge = b2b2bCharges;
                    }
                }
                return serviceCharge;
            }
            return serviceCharge;
        }*/

        #endregion



        
# region Shiva New Changes
        public static string PagingJavascript(int noOfPages, string url, int pageNo)
        {
            string show = string.Empty;
            string previous = string.Empty;
            string first = string.Empty;
            string last = string.Empty;
            string next = string.Empty;
            int showperPage = 2;

            // records per page from configuration       
            showperPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["showPerPage"]);

            showperPage = showperPage - 1;
            int showPageNos;
            if ((pageNo + showperPage) <= noOfPages)
            {
                showPageNos = pageNo + showperPage;
            }
            else
            {
                showPageNos = noOfPages;
            }

            if (pageNo != 1)
            {
                previous = "<a href=\"" + "javascript:ShowPage(" + (pageNo - 1) + ")\"> Previous </a>| ";
                first = "<a href=\"" + "javascript:ShowPage(1)\"> First </a>| ";
            }

            if (pageNo != noOfPages)
            {
                next = "<a href=\"" + "javascript:ShowPage(" + (pageNo + 1) + ")\"> Next </a>";
                last = "| <a href=\"" + "javascript:ShowPage(" + noOfPages + ")\"> Last </a>";
            }
            show = first + previous;
            for (int k = pageNo; k <= showPageNos; k++)
            {
                if (k == pageNo)
                {
                    show += "<b><a href=\"" + "javascript:ShowPage(" + k + ")\">" + k + "</a></b> | ";
                }
                else
                {
                    show += "<a href=\"" + "javascript:ShowPage(" + k + ")\">" + k + "</a> | ";
                }
            }
            show = show + next + last;

            return show;
        }

       

        /// <summary>
        /// Gets current date and time in IST.
        /// </summary>
        /// <returns>DateTime in IST.</returns>
        public static DateTime GetIST()
        {
            return DateTime.Now.ToUniversalTime() + new TimeSpan(4, 00, 0);
        }

        /// <summary>
        /// Update HotelRoom for Hotel Itinerary
        /// </summary>
        /// <param name="result"></param>
        /// <param name="roomInfo"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static HotelRoom[] GetSubAgentHotelPrice(HotelRoom[] roomInfo, HotelRequest request)
        {
            Dictionary<string, string> b2b2bVariableList = new Dictionary<string, string>();
            b2b2bVariableList = (Dictionary<string, string>)HttpContext.Current.Session["b2b2bVariable"];
            decimal agentHotelMarkUp = 0, subAgentHotelMarkUp = 0;
            if (request.IsDomestic)
            {
                agentHotelMarkUp = Convert.ToDecimal(b2b2bVariableList["agentDomesticHotelMarkUp"]);
                subAgentHotelMarkUp = Convert.ToDecimal(b2b2bVariableList["SubAgentDomesticHotelMarkUp"]);
            }
            else
            {
                agentHotelMarkUp = Convert.ToDecimal(b2b2bVariableList["agentIntlHotelMarkUp"]);
                subAgentHotelMarkUp = Convert.ToDecimal(b2b2bVariableList["SubAgentIntlHotelMarkUp"]);
            }

            for (int i = 0; i < roomInfo.Length; i++)
            {
                TimeSpan tSpan = request.EndDate.Subtract(request.StartDate);
                int noOfNight = tSpan.Days;
                if (roomInfo[i].Price.AccPriceType == PriceType.PublishedFare)
                {
                    roomInfo[i].Price.PublishedFare += (agentHotelMarkUp + subAgentHotelMarkUp) * noOfNight;
                    roomInfo[i].Price.AgentCommission += subAgentHotelMarkUp * noOfNight;
                }
                else
                {
                    roomInfo[i].Price.NetFare += (agentHotelMarkUp + subAgentHotelMarkUp) * noOfNight;
                    roomInfo[i].Price.AgentCommission = 0;
                }
                for (int j = 0; j < roomInfo[i].RoomFareBreakDown.Length; j++)
                {
                    roomInfo[i].RoomFareBreakDown[j].RoomPrice += (agentHotelMarkUp + subAgentHotelMarkUp);
                }
            }
            return roomInfo;
        }


        //Added by shiva from B2B2BMethod.cs
        public static HotelItinerary UpdateHotelItinerary(HotelItinerary itinerary)
        {
            TimeSpan tSpan = itinerary.EndDate.Subtract(itinerary.StartDate);
            int noOfDays = tSpan.Days;
            Dictionary<string, string> b2b2bVariableList = new Dictionary<string, string>();
            decimal agentHotelMarkUp = 0, subAgentHotelMarkUp = 0;
            
            //b2b2bVariableList = (Dictionary<string, string>)HttpContext.Current.Session["b2b2bVariable"];
            
            //if (itinerary.IsDomestic)
            //{
            //    agentHotelMarkUp = Convert.ToDecimal(b2b2bVariableList["agentDomesticHotelMarkUp"]);
            //    subAgentHotelMarkUp = Convert.ToDecimal(b2b2bVariableList["SubAgentDomesticHotelMarkUp"]);
            //}
            //else
            //{
            //    agentHotelMarkUp = Convert.ToDecimal(b2b2bVariableList["agentIntlHotelMarkUp"]);
            //    subAgentHotelMarkUp = Convert.ToDecimal(b2b2bVariableList["SubAgentIntlHotelMarkUp"]);
            //}

            //TODO Ziyad
            //Hard coding for time being
            agentHotelMarkUp = 5;
            subAgentHotelMarkUp = 10;


            for (int i = 0; i < itinerary.Roomtype.Length; i++)
            {
                if (itinerary.Roomtype[i].Price.AccPriceType == PriceType.PublishedFare)
                {
                    itinerary.Roomtype[i].Price.PublishedFare -= (agentHotelMarkUp + subAgentHotelMarkUp) * (noOfDays);
                    itinerary.Roomtype[i].Price.AgentCommission -= subAgentHotelMarkUp * noOfDays;
                    itinerary.Roomtype[i].Price.WLCharge = agentHotelMarkUp * noOfDays;
                }
                else
                {
                    itinerary.Roomtype[i].Price.NetFare -= (agentHotelMarkUp + subAgentHotelMarkUp) * (noOfDays);
                    itinerary.Roomtype[i].Price.WLCharge = (agentHotelMarkUp + subAgentHotelMarkUp) * noOfDays;
                }
                for (int j = 0; j < itinerary.Roomtype[i].RoomFareBreakDown.Length; j++)
                {
                    itinerary.Roomtype[i].RoomFareBreakDown[j].RoomPrice -= (agentHotelMarkUp + subAgentHotelMarkUp);
                }
            }
            return itinerary;
        }

        public static decimal GetSubAgentCommission(PriceAccounts price, string airlineCode)
        {
            try
            {
                Dictionary<string, string> b2b2bVariableList = new Dictionary<string, string>();
                Dictionary<string, string> airlineCommissionList = new Dictionary<string, string>();
                //b2b2bVariableList = (Dictionary<string, string>)HttpContext.Current.Session["b2b2bVariable"];
                string airlineCommissionXML = "";//Convert.ToString(b2b2bVariableList["b2b2bAirlineCommissionList"]);
                airlineCommissionList = GetAirLineCommission(airlineCommissionXML);
                if (airlineCommissionList.ContainsKey(airlineCode))
                {
                    price.AgentCommission = Math.Round((price.AgentCommission + price.AgentPLB) * Convert.ToDecimal(airlineCommissionList[airlineCode]) / 100);
                }
                else
                {
                    price.AgentCommission = price.AgentCommission + price.AgentPLB;
                }
                return price.AgentCommission;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Price, Severity.High, (int)HttpContext.Current.Session["memberId"], "Pricing Error : Pricing failed for B2B2B " + ex.Message + ex.StackTrace, "");
                return price.AgentCommission;
            }
        }

        /// <summary>
        /// Get dictionary from Airline commission XML
        /// </summary>
        /// <param name="airlineCommissionList"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetAirLineCommission(string airlineCommissionXML)
        {
            XmlDocument doc = new XmlDocument();
            Dictionary<string, string> airlineCommissionList = new Dictionary<string, string>();
            doc.InnerXml = airlineCommissionXML;
            XmlNodeList nodeList = doc.SelectNodes("ArrayOfAirlineCommission/AirlineCommission");
            string airlineCode = string.Empty;
            foreach (XmlNode node in nodeList)
            {
                foreach (XmlNode childNodePassenger in node.ChildNodes)
                {
                    if (childNodePassenger.Name == "AirlineCode")
                    {
                        airlineCode = childNodePassenger.FirstChild.Value;
                    }
                    else if (childNodePassenger.Name == "Commission")
                    {
                        airlineCommissionList.Add(airlineCode, childNodePassenger.FirstChild.Value);
                    }
                }
            }
            return airlineCommissionList;
        }

# endregion


# region Ticket
       public void SaveTicketInfo(ref Ticket[] ticket, FlightItinerary itineraryDB, FlightItinerary itineraryWorldspan, int memberId, int agencyId, int bookingId, Dictionary<string, string> ticketData, string ipAddr)
        {
            //send SMS  for ETicket generation
           /* if (SMS.IsSMSSubscribed(agencyId, SMSEvent.ETicketGeneration))
            {
                Hashtable hTable = new Hashtable();
                hTable.Add("AgencyId", agencyId);
                string pnrInfo = string.Empty;
                int count = 1;
                foreach (FlightInfo segment in itineraryWorldspan.Segments)
                {
                    pnrInfo += segment.Origin.CityCode + "-" + segment.Destination.CityCode + ":Flight " + segment.Airline + "-" + segment.FlightNumber + "(" + segment.DepartureTime.ToString("ddMMMyy HH:mm") + ") PNR:" + segment.AirlinePNR;
                    if (count < itineraryDB.Segments.Length)
                        pnrInfo += ", ";
                    count++;
                }
                hTable.Add("PNR", pnrInfo);
                if (itineraryDB.Passenger[0].FullName.Length <= 20)
                {
                    hTable.Add("PaxName", itineraryDB.Passenger[0].FullName);
                }
                else
                {
                    hTable.Add("PaxName", itineraryDB.Passenger[0].FirstName);
                }
                hTable.Add("EventType", SMSEvent.ETicketGeneration);
                SMS.SendSMSThread(hTable);
            }
            if (SMS.IsSMSSubscribed(agencyId, SMSEvent.ETicketforPassenger))
            {
                Hashtable hTable = new Hashtable();
                hTable.Add("AgencyId", agencyId);
                string pnrInfo = string.Empty;
                int count = 1;
                foreach (FlightInfo segment in itineraryWorldspan.Segments)
                {
                    pnrInfo += segment.Origin.CityCode + "-" + segment.Destination.CityCode + ":Flight " + segment.Airline + "-" + segment.FlightNumber + "(" + segment.DepartureTime.ToString("ddMMMyy HH:mm") + ") PNR:" + segment.AirlinePNR;
                    if (count < itineraryDB.Segments.Length)
                        pnrInfo += ", ";
                    count++;
                }
                hTable.Add("PNR", pnrInfo);
                if (itineraryDB.Passenger[0].FullName.Length <= 20)
                {
                    hTable.Add("PaxName", itineraryDB.Passenger[0].FullName);
                }
                else
                {
                    hTable.Add("PaxName", itineraryDB.Passenger[0].FirstName);
                }
                hTable.Add("MobileNo", itineraryDB.Passenger[0].CellPhone);
                hTable.Add("EventType", SMSEvent.ETicketforPassenger);
                SMS.SendSMSThread(hTable);
            }*/
            try
            {
                int ticketSaved = 0;
                // Updatig AirlinePNR in database. 
                // FlightId of itineraryWorldspan has to be set as in database before updating Airline PNR.
                //UpdateAirlinePNR(itineraryWorldspan);
                for (int i = 0; i < ticket.Length; i++)
                {
                    // PaxKeys will be same if ticket is in same sequence as the corresponding passengers are in our database.
                    if (ticket[i].PaxKey == itineraryDB.Passenger[i].PaxKey)
                    {
                        // SaveTIcket needs ticket index and passenger index. If in sequence both are same.
                        SetTicketForNonLCC(i, i, ref ticket, itineraryDB, itineraryWorldspan, memberId, agencyId, bookingId, ticketData);
                        ticketSaved++;
                    }
                    else
                    {
                        // If not in sequence then finding the corresponding passenger.
                        for (int j = 0; j < itineraryDB.Passenger.Length; j++)
                        {
                            if (ticket[i].PaxKey == itineraryDB.Passenger[j].PaxKey)
                            {
                                // i is ticket index and j is passenger index.
                                SetTicketForNonLCC(i, j, ref ticket, itineraryDB, itineraryWorldspan, memberId, agencyId, bookingId, ticketData);
                                ticketSaved++;
                                break;
                            }
                        }
                    }
                }

                Airline air = new Airline();
                air.Load(itineraryDB.ValidatingAirlineCode);
                string supplierCode = string.Empty;
                if (ticket.Length > 0)
                {
                    if (ticket[0].SupplierId > 0)
                    {
                        supplierCode = Supplier.GetSupplierCodeById(ticket[0].SupplierId);
                    }
                }
                for (int i = 0; i < ticket.Length; i++)
                {
                    ticket[i].SupplierRemmitance = new SupplierRemmitance();
                    ticket[i].SupplierRemmitance.PublishedFare = ticket[i].Price.PublishedFare;
                    ticket[i].SupplierRemmitance.AccpriceType = ticket[i].Price.AccPriceType;
                    ticket[i].SupplierRemmitance.NetFare = ticket[i].Price.NetFare;
                    ticket[i].SupplierRemmitance.CessTax = 0;
                    ticket[i].SupplierRemmitance.Currency = ticket[i].Price.Currency;
                    ticket[i].SupplierRemmitance.CurrencyCode = ticket[i].Price.CurrencyCode;
                    ticket[i].SupplierRemmitance.Markup = ticket[i].Price.Markup;
                    ticket[i].SupplierRemmitance.OtherCharges = ticket[i].Price.OtherCharges;
                    ticket[i].SupplierRemmitance.OurCommission = ticket[i].Price.OurCommission;
                    ticket[i].SupplierRemmitance.OurPlb = ticket[i].Price.OurPLB;
                    ticket[i].SupplierRemmitance.RateOfExchange = ticket[i].Price.RateOfExchange;
                    ticket[i].SupplierRemmitance.ServiceTax = ticket[i].Price.SeviceTax;
                    ticket[i].SupplierRemmitance.SupplierCode = supplierCode;

                    ticket[i].SupplierRemmitance.Tax = ticket[i].Price.Tax;
                    ticket[i].SupplierRemmitance.TdsCommission = 0;
                    ticket[i].SupplierRemmitance.TdsPLB = 0;
                    ticket[i].SupplierRemmitance.CreatedBy = ticket[i].CreatedBy;
                    ticket[i].SupplierRemmitance.CreatedOn = DateTime.UtcNow;
                    ticket[i].SupplierRemmitance.CommissionType = air.CommissionType;
                }

                int txnRetryCount = Convert.ToInt32(ConfigurationSystem.BookingEngineConfig["txnRetryCount"]);
                int numberRetry = 0;
                bool txnSucceded = false;
                while (!txnSucceded && numberRetry < txnRetryCount)
                {
                    numberRetry++;
                    try
                    {
                        using (TransactionScope autoTicketScope = new TransactionScope())
                        {
                            for (int i = 0; i < ticket.Length; i++)
                            {
                                ticket[i].Save();
                                ticket[i].SupplierRemmitance.TicketId = ticket[i].TicketId;
                                ticket[i].SupplierRemmitance.Save();
                                for (int j = 0; j < ticket[i].PtcDetail.Count; j++)
                                {
                                    ticket[i].PtcDetail[j].Save();
                                }
                            }
                            autoTicketScope.Complete();
                            txnSucceded = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        if (numberRetry < txnRetryCount)
                        {
                            for (int i = 0; i < ticket.Length; i++)
                            {
                                ticket[i].TicketId = 0;
                                ticket[i].SupplierRemmitance.SupplierRemmitanceId = 0;
                            }
                            continue;
                        }
                        else
                        {
                            throw ex;
                        }
                    }
                }


                //Save Booking History
                string remarks = string.Empty;
                try
                {
                    if (HttpContext.Current != null && HttpContext.Current.Session != null && Convert.ToBoolean(HttpContext.Current.Session["isB2B2BAgent"]))
                    {
                        Dictionary<string, string> b2b2bVariableList = new Dictionary<string, string>();
                        b2b2bVariableList = (Dictionary<string, string>)HttpContext.Current.Session["b2b2bVariable"];


                        remarks = " By B2B2B Subagent Site URL " + b2b2bVariableList["b2b2bSiteName"] + " ,Subagent " + b2b2bVariableList["b2b2bSubAgencyName"];


                    }
                }
                catch (Exception)
                {

                }
                BookingHistory bookingHistory = new BookingHistory();
                bookingHistory.BookingId = bookingId;
                bookingHistory.EventCategory = EventCategory.Ticketing;
                bookingHistory.Remarks = "Ticket Created" + remarks + " (IP Address - " + ipAddr + ")";
                bookingHistory.CreatedBy = memberId;
                bookingHistory.Save();
                if (ticket.Length != ticketSaved)
                {
                    Audit.Add(EventType.Ticketing, Severity.High, memberId, "Error in Save Ticket Booking History. PNR : " + itineraryDB.PNR + " Source : " + itineraryDB.FlightBookingSource.ToString(), "0");
                    throw new BookingEngineException("Error in Save Ticket Booking History. PNR : " + itineraryDB.PNR + " Source : " + itineraryDB.FlightBookingSource.ToString());
                }
            //    #region Getting TBOConnect results for Cache
            //    if (Convert.ToBoolean(ConfigurationSystem.HotelConnectConfig["TBOConnectOnETicket"]))
            //    {

            //        try
            //        {
            //            Thread ResultThread = new Thread
            //            (
            //                delegate()
            //                {
            //                    //SearchResultTBOConnectForCache(itineraryDB, memberId);
            //                }
            //            );
            //            ResultThread.Start();
            //            ResultThread.IsBackground = true;
            //        }
            //        catch { }
            //    }
            
            //#endregion
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Ticketing, Severity.High, memberId, "Error in MSE.SaveTicketInfo info. PNR : " + itineraryDB.PNR + " Source : " + itineraryDB.FlightBookingSource.ToString()+" , Error:"+ex.ToString(), "0");
                throw new Exception(ex.ToString());
            }
        }
       public void SaveTicketInfo(FlightItinerary itinerary, int memberId, int agencyId, List<string> pmTicketNo, int bookingId, string ipAddr, Dictionary<string, string> ticketData)
       {
           //send SMS  for ETicket generation
           /*if (SMS.IsSMSSubscribed(agencyId, SMSEvent.ETicketGeneration))
           {
               Hashtable hTable = new Hashtable();
               hTable.Add("AgencyId", agencyId);
               string pnrInfo = string.Empty;
               int count = 1;
               pnrInfo += "PNR:" + itinerary.PNR + " ";
               foreach (FlightInfo segment in itinerary.Segments)
               {
                   pnrInfo += segment.Origin.CityCode + "-" + segment.Destination.CityCode + ":Flight " + segment.Airline + "-" + segment.FlightNumber + "(" + segment.DepartureTime.ToString("ddMMMyy HH:mm") + ")";
                   if (count < itinerary.Segments.Length)
                       pnrInfo += ", ";
                   count++;
               }
               hTable.Add("PNR", pnrInfo);
               if (itinerary.Passenger[0].FullName.Length <= 20)
               {
                   hTable.Add("PaxName", itinerary.Passenger[0].FullName);
               }
               else
               {
                   hTable.Add("PaxName", itinerary.Passenger[0].FirstName);
               }
               hTable.Add("EventType", SMSEvent.ETicketGeneration);
               SMS.SendSMSThread(hTable);
           }
           if (SMS.IsSMSSubscribed(agencyId, SMSEvent.ETicketforPassenger))
           {
               Hashtable hTable = new Hashtable();
               hTable.Add("AgencyId", agencyId);
               string pnrInfo = string.Empty;
               int count = 1;
               pnrInfo += "PNR:" + itinerary.PNR + " ";
               foreach (FlightInfo segment in itinerary.Segments)
               {
                   pnrInfo += segment.Origin.CityCode + "-" + segment.Destination.CityCode + ":Flight " + segment.Airline + "-" + segment.FlightNumber + "(" + segment.DepartureTime.ToString("ddMMMyy HH:mm") + ")";
                   if (count < itinerary.Segments.Length)
                       pnrInfo += ", ";
                   count++;
               }
               hTable.Add("PNR", pnrInfo);
               if (itinerary.Passenger[0].FullName.Length <= 20)
               {
                   hTable.Add("PaxName", itinerary.Passenger[0].FullName);
               }
               else
               {
                   hTable.Add("PaxName", itinerary.Passenger[0].FirstName);
               }
               hTable.Add("MobileNo", itinerary.Passenger[0].CellPhone);
               hTable.Add("EventType", SMSEvent.ETicketforPassenger);
               SMS.SendSMSThread(hTable);
           }*/

           // todo ziya... to be uncommented below line
           Ticket[] ticket = GetTicketForLCC(itinerary, memberId, agencyId, pmTicketNo, bookingId, ticketData);

           Airline air = new Airline();
           air.Load(itinerary.ValidatingAirlineCode);
           string supplierCode = string.Empty;
           if (ticket.Length > 0)
           {
               if (ticket[0].SupplierId > 0)
               {
                   supplierCode = Supplier.GetSupplierCodeById(ticket[0].SupplierId);
               }
           }
           for (int i = 0; i < ticket.Length; i++)
           {
               ticket[i].SupplierRemmitance = new SupplierRemmitance();
               ticket[i].SupplierRemmitance.PublishedFare = itinerary.Passenger[i].Price.PublishedFare;
               ticket[i].SupplierRemmitance.AccpriceType = itinerary.Passenger[i].Price.AccPriceType;
               ticket[i].SupplierRemmitance.NetFare = itinerary.Passenger[i].Price.NetFare;
               ticket[i].SupplierRemmitance.CessTax = 0;
               ticket[i].SupplierRemmitance.Currency = itinerary.Passenger[i].Price.Currency;
               ticket[i].SupplierRemmitance.CurrencyCode = itinerary.Passenger[i].Price.CurrencyCode;
               ticket[i].SupplierRemmitance.Markup = itinerary.Passenger[i].Price.Markup;
               ticket[i].SupplierRemmitance.OtherCharges = itinerary.Passenger[i].Price.OtherCharges;
               ticket[i].SupplierRemmitance.OurCommission = itinerary.Passenger[i].Price.OurCommission;
               ticket[i].SupplierRemmitance.OurPlb = itinerary.Passenger[i].Price.OurPLB;
               ticket[i].SupplierRemmitance.RateOfExchange = itinerary.Passenger[i].Price.RateOfExchange;
               ticket[i].SupplierRemmitance.ServiceTax = itinerary.Passenger[i].Price.SeviceTax;
               ticket[i].SupplierRemmitance.SupplierCode = supplierCode;

               ticket[i].SupplierRemmitance.Tax = itinerary.Passenger[i].Price.Tax;
               ticket[i].SupplierRemmitance.TdsCommission = 0;
               ticket[i].SupplierRemmitance.TdsPLB = 0;
               ticket[i].SupplierRemmitance.CreatedBy = ticket[i].CreatedBy;
               ticket[i].SupplierRemmitance.CreatedOn = DateTime.UtcNow;
               ticket[i].SupplierRemmitance.CommissionType = air.CommissionType;
           }

           int txnRetryCount = Convert.ToInt32(ConfigurationSystem.BookingEngineConfig["txnRetryCount"]);
           int numberRetry = 0;
           bool txnSucceded = false;
           while (!txnSucceded && numberRetry < txnRetryCount)
           {
               numberRetry++;
               try
               {
                   using (TransactionScope autoTicketScope = new TransactionScope())
                   {
                       for (int i = 0; i < ticket.Length; i++)
                       {
                           ticket[i].Save();
                           ticket[i].SupplierRemmitance.TicketId = ticket[i].TicketId;
                           ticket[i].SupplierRemmitance.Save();
                           for (int j = 0; j < ticket[i].PtcDetail.Count; j++)
                           {
                               ticket[i].PtcDetail[j].Save();
                           }
                       }
                       autoTicketScope.Complete();
                       txnSucceded = true;
                   }
               }
               catch (Exception ex)
               {
                   if (numberRetry < txnRetryCount)
                   {
                       for (int i = 0; i < ticket.Length; i++)
                       {
                           ticket[i].TicketId = 0;
                           ticket[i].SupplierRemmitance.SupplierRemmitanceId = 0;
                       }
                       continue;
                   }
                   else
                   {
                       throw ex;
                   }
               }
           }
           string remarks = string.Empty;
           try
           {
               if (HttpContext.Current != null && HttpContext.Current.Session != null && Convert.ToBoolean(HttpContext.Current.Session["isB2B2BAgent"]))
               {
                   Dictionary<string, string> b2b2bVariableList = new Dictionary<string, string>();
                   b2b2bVariableList = (Dictionary<string, string>)HttpContext.Current.Session["b2b2bVariable"];


                   remarks = " By B2B2B Subagent Site URL " + b2b2bVariableList["b2b2bSiteName"] + " ,Subagent " + b2b2bVariableList["b2b2bSubAgencyName"];
               }

           }
           catch (Exception)
           {
           }
           BookingHistory bookingHistory = new BookingHistory();
           bookingHistory.BookingId = bookingId;
           bookingHistory.EventCategory = EventCategory.Ticketing;
           bookingHistory.Remarks = "Ticket Created" + remarks + " (IP Address - " + ipAddr + ")";
           bookingHistory.CreatedBy = memberId;
           bookingHistory.Save();
           #region Getting TBOConnect results for Cache
           //if (Convert.ToBoolean(ConfigurationSystem.HotelConnectConfig["TBOConnectOnETicket"]))
           //{
           //    try
           //    {
           //        Thread ResultThread = new Thread
           //        (
           //            delegate()
           //            {
           //              //  SearchResultTBOConnectForCache(itinerary, memberId); todo ziya
           //            }
           //        );
           //        ResultThread.Start();
           //        ResultThread.IsBackground = true;
           //    }
           //    catch { }
           //}
           #endregion
       }

       
       private void SetTicketForNonLCC(int ticketIndex, int paxIndex, ref Ticket[] ticket, FlightItinerary itineraryDB, FlightItinerary itineraryWorldspan, int memberId, int agencyId, int bookingId, Dictionary<string, string> ticketData)
       {
           List<string> airlineNameRemarksList = new List<string>();
           List<string> airlineNameFareRuleList = new List<string>();
           string remarks = string.Empty;
           string fareRule = string.Empty;
           UserPreference preference = new UserPreference();
           //Airline service fee for Email Itinerary
           Dictionary<string, string> sFeeTypeList = new Dictionary<string, string>();
           Dictionary<string, string> sFeeValueList = new Dictionary<string, string>();
           FlightItinerary.GetMemberPrefServiceFee(agencyId, ref sFeeTypeList, ref sFeeValueList);
           int i = ticketIndex;    // index of ticket in ticket array rtrieved from worldspan.
           int j = paxIndex;       // index of passenger as in itinerary made from database.
           ticket[i].Status = "OK";
           ticket[i].CreatedBy = memberId;
           ticket[i].PaxId = itineraryDB.Passenger[j].PaxId;
           ticket[i].PaxType = itineraryDB.Passenger[j].Type;
           ticket[i].FlightId = itineraryDB.FlightId;
           int paxIndexPricing = i;
           // We need to save price received from worldspan.
           // Finding mathing index of corresponding passenger in itinerary received from worldspan if the sequence is not same.
           if (ticket[i].PaxKey != itineraryWorldspan.Passenger[i].PaxKey)
           {
               for (int k = 0; k < itineraryWorldspan.Passenger.Length; k++)
               {
                   if (ticket[i].PaxKey == itineraryWorldspan.Passenger[k].PaxKey)
                   {
                       paxIndexPricing = k;
                       break;
                   }
               }
           }
           ticket[i].Price = itineraryDB.Passenger[j].Price;
           //}

           ticket[i].ETicket = true;
           if (ticket[i].IssueInExchange == null)
           {
               ticket[i].IssueInExchange = string.Empty;
           }
           if (ticket[i].TourCode == null || ticket[i].TourCode.Length == 0)//If TourCode is not assigned then assign from Itinerary or ticketData
           {
               if (itineraryDB.TourCode != null && itineraryDB.TourCode.Length > 0)
               {
                   ticket[i].TourCode = itineraryDB.TourCode;
               }
               else if (ticketData != null && ticketData.ContainsKey("tourCode") && ticketData["tourCode"].Length > 0)
               {
                   ticket[i].TourCode = ticketData["tourCode"];
               }
               else
               {
                   ticket[i].TourCode = string.Empty;
               }
           }
          
           if (ticket[i].OriginalIssue == null)
           {
               ticket[i].OriginalIssue = string.Empty;
           }
           if (itineraryDB.Endorsement != null && itineraryDB.Endorsement.Length > 0)
           {
               ticket[i].Endorsement = itineraryDB.Endorsement;
           }
           else if (ticketData != null && ticketData.ContainsKey("endorsement") && ticketData["endorsement"].Length > 0)
           {
               ticket[i].Endorsement = ticketData["endorsement"];
           }
           else
           {
               ticket[i].Endorsement = string.Empty;
           }
           if (ticketData != null && ticketData.ContainsKey("corporateCode") && ticketData["corporateCode"].Length > 0)
           {
               ticket[i].CorporateCode = ticketData["corporateCode"];
           }
           else
           {
               ticket[i].CorporateCode = string.Empty;
           }
           if (ticketData != null && ticketData.ContainsKey("remarks") && ticketData["remarks"].Length > 0)
           {
               ticket[i].Remarks = ticketData["remarks"];
           }
           else
           {
               ticket[i].Remarks = string.Empty;
           }
           if (ticket[i].FareCalculation == null)
           {
               ticket[i].FareCalculation = string.Empty;
           }
           //int agencyPrimaryMemberId = Member.GetPrimaryMemberId(agencyId); todo Ziya
           int agencyPrimaryMemberId = 1;


           if (itineraryDB.BookingMode == BookingMode.WhiteLabel || itineraryDB.BookingMode == BookingMode.Itimes)
           {
               if (itineraryDB.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + ""))
               {
                   int chargeType = 1;
                   decimal chargeValue = 0;
                   AirlineServiceFee asf = AirlineServiceFee.Load(itineraryDB.Segments[0].Airline, agencyId);
                   if (asf.AirlineCode != null && asf.AirlineCode != "" && asf.IsActive)
                   {
                       chargeType = Convert.ToInt32(asf.ServiceFeeType);
                       chargeValue = asf.ServiceFee;
                   }
                   else
                   {
                       chargeType = Convert.ToInt32(preference.GetPreference(agencyPrimaryMemberId, WLPreferenceKeys.ServiceChargeType, "Booking & Confirmation").Value);
                       chargeValue = Convert.ToDecimal(preference.GetPreference(agencyPrimaryMemberId, WLPreferenceKeys.ServiceCharge, "Booking & Confirmation").Value);
                   }
                   if (chargeType == 1)
                   {
                       ticket[i].ServiceFee += chargeValue;
                   }
                   else
                   {
                       ticket[i].ServiceFee += (ticket[i].Price.PublishedFare + ticket[i].Price.Tax) * (chargeValue / 100);
                   }
               }
               else
               {
                   int chargeType = Convert.ToInt32(preference.GetPreference(agencyPrimaryMemberId, WLPreferenceKeys.IntlServiceChargeType, "Booking & Confirmation").Value);
                   decimal chargeValue = Convert.ToDecimal(preference.GetPreference(agencyPrimaryMemberId, WLPreferenceKeys.IntlServiceCharge, "Booking & Confirmation").Value);
                   if (chargeType == 1)
                   {
                       ticket[i].ServiceFee += chargeValue;
                   }
                   else
                   {
                       ticket[i].ServiceFee += (ticket[i].Price.PublishedFare + ticket[i].Price.Tax) * (chargeValue / 100);
                   }
               }
           }
           else
           {
               string sfType = string.Empty;
               decimal sfValue = 0;
               if (!itineraryDB.CheckDomestic("" + Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + ""))
               {
                   if (sFeeTypeList.ContainsKey("INTL") && sFeeValueList.ContainsKey("INTL"))
                   {
                       sfType = sFeeTypeList["INTL"];
                       sfValue = Convert.ToDecimal(sFeeValueList["INTL"]);
                   }
               }
               else if (sFeeTypeList.ContainsKey(itineraryDB.ValidatingAirlineCode) && sFeeValueList.ContainsKey(itineraryDB.ValidatingAirlineCode))
               {
                   sfType = sFeeTypeList[itineraryDB.ValidatingAirlineCode];
                   sfValue = Convert.ToDecimal(sFeeValueList[itineraryDB.ValidatingAirlineCode]);
               }

               if (sfType == "FIXED")
               {
                   ticket[i].ServiceFee = sfValue;
               }
               else if (sfType == "PERCENTAGE")
               {
                   decimal totalfare = ticket[i].Price.PublishedFare + ticket[i].Price.Tax + ticket[i].Price.OtherCharges + ticket[i].Price.AdditionalTxnFee + ticket[i].Price.TransactionFee;
                   ticket[i].ServiceFee = totalfare * sfValue / 100;
               }
               else
               {
                   ticket[i].ServiceFee = 0;
               }
           }
           preference = preference.GetPreference(agencyPrimaryMemberId, UserPreference.ServiceFee, UserPreference.ServiceFee);
           if (preference.Value != null)
           {
               if (preference.Value == UserPreference.ServiceFeeInTax)
               {
                   ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowInTax;
               }
               else if (preference.Value == UserPreference.ServiceFeeShow)
               {
                   ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowSeparately;
               }
           }
           else
           {
               ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowInTax;
           }
           int consolidatorId = 0;
           foreach (FlightInfo segment in itineraryDB.Segments)
           {
               string airlineCode = segment.Airline;
               Airline airline = new Airline();
               airline.Load(airlineCode);
               if (airlineCode == itineraryDB.ValidatingAirlineCode)
               {
                   consolidatorId = airline.ConsolidatorId;
               }
               if (!airlineNameRemarksList.Contains(airline.AirlineName) && airline.Remarks.Length != 0)
               {
                   airlineNameRemarksList.Add(airline.AirlineName);
                   remarks += "<span style=\"font-weight:bold\">" + airline.AirlineName + "</span>" + "&nbsp;:&nbsp;" + airline.Remarks + "<br />";
               }
               if (!airlineNameFareRuleList.Contains(airline.AirlineName) && airline.FareRule.Length != 0)
               {
                   airlineNameFareRuleList.Add(airline.AirlineName);
                   fareRule += "<span style=\"font-weight:bold\">" + airline.AirlineName + "</span>" + "&nbsp;:&nbsp;" + airline.FareRule + "<br />";
               }
           }
           //Set the consolidator id from AirlineHAP if exists there
           AirlineHAP airHap = new AirlineHAP();
           if (itineraryDB.FlightBookingSource == BookingSource.Amadeus)
           {
               airHap.Load(itineraryDB.ValidatingAirline, "1A");
           }
           else if (itineraryDB.FlightBookingSource == BookingSource.Galileo)
           {
               airHap.Load(itineraryDB.ValidatingAirline, "1G");
           }
           else if (itineraryDB.FlightBookingSource == BookingSource.UAPI)// todo check 
           {
               airHap.Load(itineraryDB.ValidatingAirline, "1G");
           }
           if (airHap != null && !string.IsNullOrEmpty(airHap.Airline))
           {
               consolidatorId = airHap.SupplierId;
           }
           ticket[i].ValidatingAriline = itineraryDB.ValidatingAirline;// TODO ziyad..... check the Air line numericcode too
           ticket[i].SupplierId = consolidatorId;
           ticket[i].FareRule = fareRule;
           // Saving PTC Detail information after ticket is saved.
           for (int f = 0; f < itineraryDB.Segments.Length; f++)
           {
               for (int p = 0; p < ticket[i].PtcDetail.Count; p++)
               {
                   if (ticket[i].PtcDetail[p].FlightKey == itineraryDB.Segments[f].FlightKey)
                   {
                       ticket[i].PtcDetail[p].SegmentId = itineraryDB.Segments[f].SegmentId;
                       ticket[i].PtcDetail[p].PaxType = FlightPassenger.GetPTC(ticket[i].PaxType);
                       break;
                   }
               }
           }
           try
           {
               int timeDiff = Convert.ToInt32(Configuration.ConfigurationSystem.Core["TimeDiffInFlightKey"]);
               for (int p = 0; p < ticket[i].PtcDetail.Count; p++)
               {
                   if (ticket[i].PtcDetail[p].SegmentId <= 0 || ticket[i].PtcDetail[p].PaxType == null || ticket[i].PtcDetail[p].PaxType == string.Empty)
                   {
                       string ptcKey = ticket[i].PtcDetail[p].FlightKey.Substring(0, 6);
                       DateTime ptcDate = DateTime.ParseExact(ticket[i].PtcDetail[p].FlightKey.Substring(6), "ddMMMyyyyHHmm", null);
                       for (int f = 0; f < itineraryDB.Segments.Length; f++)
                       {
                           if (ptcKey.Contains(itineraryDB.Segments[f].FlightKey.Substring(0, 6)) && (ptcDate.Subtract(itineraryDB.Segments[f].DepartureTime) < TimeSpan.FromMinutes(timeDiff) || itineraryDB.Segments[f].DepartureTime.Subtract(ptcDate) < TimeSpan.FromMinutes(timeDiff)))
                           {
                               ticket[i].PtcDetail[p].SegmentId = itineraryDB.Segments[f].SegmentId;
                               ticket[i].PtcDetail[p].PaxType = FlightPassenger.GetPTC(ticket[i].PaxType);
                               break;
                           }
                       }
                   }
               }
               for (int p = 0; p < ticket[i].PtcDetail.Count; p++)
               {
                   if (ticket[i].PtcDetail[p].SegmentId <= 0 || ticket[i].PtcDetail[p].PaxType == null || ticket[i].PtcDetail[p].PaxType == string.Empty)
                   {
                       ticket[i].PtcDetail[p].SegmentId = itineraryDB.Segments[p].SegmentId;
                       ticket[i].PtcDetail[p].PaxType = FlightPassenger.GetPTC(ticket[i].PaxType);
                       break;
                   }
               }
           }
           catch (Exception ex)
           {
               Audit.Add(EventType.Ticketing, Severity.High, 0, "Exception in string pax type in ptc detail " + ex.Message + " Stack trace:- " + ex.StackTrace, "");
           }
       }

        public void SaveHotelBookingFromFailed(HotelItinerary itinerary)
        {
            BookingDetail booking = new BookingDetail();
            BookingHistory bookingHistory = new BookingHistory();
            string remarks = string.Empty;
            bookingHistory.EventCategory = EventCategory.Booking;
            bookingHistory.CreatedBy = itinerary.CreatedBy;
            CT.Core.Queue queue = new CT.Core.Queue();
            queue.QueueTypeId = (int)QueueType.Booking;
            queue.StatusId = (int)QueueStatus.Assigned;
            queue.AssignedTo = itinerary.CreatedBy; //(int)Settings.LoginInfo.UserID;
            queue.AssignedBy = itinerary.CreatedBy; //(int)Settings.LoginInfo.UserID;
            queue.AssignedDate = DateTime.UtcNow;
            queue.CompletionDate = DateTime.UtcNow.AddDays(1);
            queue.CreatedBy = itinerary.CreatedBy; //(int)Settings.LoginInfo.UserID;

            //TODO: For New product -- Start                       
            booking.ProductsList = new Product[1];
            booking.ProductsList[0] = (Product)itinerary;
            //For New product -- End
            remarks = "Booking is Ready";
            try
            {
                using (TransactionScope updateTransaction = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                {
                    //itinerary.BookingMode = bookingMode;
                    //For New product -- Start  
                    booking.Status = BookingStatus.Ready;
                    booking.AgencyId = itinerary.AgencyId; //Settings.LoginInfo.AgentId;
                    booking.CreatedBy = itinerary.CreatedBy; //(int)Settings.LoginInfo.UserID;
                    booking.SaveBooking(false);
                    //For New product -- End
                    //if (itinerary.Source == HotelBookingSource.Desiya)
                    //{
                    //    // Sending default Rate of exchange 1 for Desiya. beacuse its in INR.
                    //    int invoiceNumber = AccountUtility.RaiseInvoice(itinerary, string.Empty, (int)member, itinerary.Roomtype[0].Price.RateOfExchange);
                    //    Invoice.UpdateInvoiceStatus(invoiceNumber, InvoiceStatus.Paid, (int)member);
                    //}
                    queue.ItemId = booking.BookingId;
                    bookingHistory.Remarks = remarks;
                    bookingHistory.BookingId = booking.BookingId;
                    queue.Save();
                    bookingHistory.Save();
                    updateTransaction.Complete();
                    //try
                    //{
                    //    Thread mailThread = new Thread(MetaSearchEngine.SendLowBalanceMail);
                    //    mailThread.Start(member);
                    //}
                    //catch (Exception)
                    //{ }
                }
                try
                {
                    //SendHotelItineraryMail(itinerary, "Hotel booking details");
                }
                catch (Exception Ex)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelBook, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, "Hotel : SMTP Send Mail Failure. Error : " + Ex.StackTrace.ToString() + " | " + DateTime.Now + " | " + itinerary.Source.ToString() + " Confirmation No = " + itinerary.ConfirmationNo.ToString(), "0");

               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }


# endregion

        #region SightSeeing  Add by brahmam 22.12.2015
        public SightseeingSearchResult[] GetSightseeingResults(SightSeeingReguest req, long agentId)
        {
            agencyId = agentId;
            Trace.TraceInformation("MetaSearchEngine.GetSightSeeinglResults Entered");
            StartSession();
            sightseeingRequest = req;
            hCity = HotelCity.Load(Convert.ToInt32(sightseeingRequest.DestinationCode));
            listOfThreads.Add("GTA", new WaitCallback(SearchSightseeingResultGTA));
            SightseeingSearchResult[] result = new SightseeingSearchResult[0];
            Dictionary<string, int> readySources = GetReadySightseeingSources();
            eventFlag = new AutoResetEvent[readySources.Count];
            int maxTimeOut = 0;
            int i = 0;
            foreach (KeyValuePair<string, int> activeSource in readySources)
            {
                maxTimeOut = ((activeSource.Value > maxTimeOut) ? activeSource.Value : maxTimeOut);
                i++;
            }
            int j = 0;
            foreach (KeyValuePair<string, WaitCallback> deThread in this.listOfThreads)
            {
                if (readySources.ContainsKey(deThread.Key))
                {
                    ThreadPool.QueueUserWorkItem(deThread.Value, j);
                    this.eventFlag[j] = new AutoResetEvent(false);
                    j++;
                }
            }
            if (j != 0)
            {
                if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 0, maxTimeOut), true))
                {
                }
            }
            result = CombineSightseeingSources();
            Trace.TraceInformation("MetaSearchEngine.GetSightseeingResults Exit");
            return result;
        }
        private Dictionary<string, int> GetReadySightseeingSources()
        {
            Trace.TraceInformation("MetaSearchEngine.GetReadySightseeingSources Entered");
            Dictionary<string, string> sourcesConfig = ConfigurationSystem.ActiveSources;
            Dictionary<string, int> sources = new Dictionary<string, int>();
            if (sightseeingRequest != null)
            {
                if (sourcesConfig.ContainsKey("GTA") && this.listOfThreads.ContainsKey("GTA"))
                {
                    sources.Add("GTA", Convert.ToInt32(sourcesConfig["GTA"].Split(',')[1]));
                }
                if (sourcesConfig.ContainsKey("HotelBeds") && this.listOfThreads.ContainsKey("HotelBeds"))
                {
                    sources.Add("HotelBeds", Convert.ToInt32(sourcesConfig["HotelBeds"].Split(',')[1]));
                }
            }
            Trace.TraceInformation("MetaSearchEngine.GetReadySightseeingSources Exit");
            return sources;
        }
        private SightseeingSearchResult[] CombineSightseeingSources()
        {
            Trace.TraceInformation("MetaSearchEngine.CombineSightseeingSources Enter");
            int totalRes = 0;
            for (int i = 0; i < sightseeingSearchResBySrc.Count; i++)
            {
                if (sightseeingSearchResBySrc[i].Result != null)
                {
                    totalRes += sightseeingSearchResBySrc[i].Result.Length;
                }
            }
            SightseeingSearchResult[] finalResult = new SightseeingSearchResult[totalRes];
            int count = 0;
            for (int i = 0; i < sightseeingSearchResBySrc.Count; i++)
            {
                if (this.sightseeingSearchResBySrc[i].Result != null)
                {
                    for (int j = 0; j < sightseeingSearchResBySrc[i].Result.Length; j++)
                    {
                        int k = j + count;
                        finalResult[k] = sightseeingSearchResBySrc[i].Result[j];
                    }
                    count += sightseeingSearchResBySrc[i].Result.Length;
                }
            }
            Trace.TraceInformation("MetaSearchEngine.CombineSightseeingSources Exit");
            return finalResult;
        }
        private void SearchSightseeingResultGTA(object eventNumber)
        {
            Trace.TraceInformation("MetaSearchEngine.SearchSightseeingResultGTA Enter");
            Audit.Add(EventType.SightseeingSearch, Severity.Normal, 1, "Entered SearchSightseeingResultGTA", "0");
            SightseeingSearchResultBySources src = new SightseeingSearchResultBySources();
            src.Source = "GTA";
            SightSeeingReguest hReqGTA = new SightSeeingReguest();
            hReqGTA = sightseeingRequest.CopyByValue();
            try
            {
                if (hCity.GTACode != null && hCity.GTACode.Length > 0)
                {
                    hReqGTA.DestinationCode = hCity.GTACode;
                    hReqGTA.CountryName = hCity.CountryName;
                    hReqGTA.Currency = Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"];
                }
                else
                {
                    eventFlag[(int)eventNumber].Set();
                    Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, "MetaSearchEngine.SearchResultGTA : City code not Found for CountryName = " + hReqGTA.CountryName + " and CityName = " + hReqGTA.CityName, string.Empty);
                    Trace.TraceInformation("MetaSearchEngine.SearchResultGTA : City code not Found for CountryName = " + hReqGTA.CountryName + " and CityName = " + hReqGTA.CityName);
                    return;
                }
                GTA gtaApi = new GTA();
                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    gtaApi.AgentExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    gtaApi.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    gtaApi.AgentDecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    gtaApi.AgentExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    gtaApi.AgentCurrency = SettingsLoginInfo.Currency;
                    gtaApi.AgentDecimalPoint = SettingsLoginInfo.DecimalValue;
                }
                DataTable dtMarkup = UpdateMarkup.Load((int)agencyId, SightseeingBookingSource.GTA.ToString(), (int)ProductType.SightSeeing);
                DataView dv = dtMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2B')";
                dtMarkup = dv.ToTable();
                decimal markup = 0; string markupType = string.Empty;

                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                }
                src.Result = src.Result = gtaApi.GetSightseeingAvailability(hReqGTA, markup, markupType);
                SightseeingSearchResult[] finalResult = new SightseeingSearchResult[src.Result.Length];
                finalResult = src.Result;
                int showResultCount = ConfigurationSystem.ResultCountBySources["GTA"];
                if (src.Result.Length > showResultCount)
                {
                    Array.Resize<SightseeingSearchResult>(ref finalResult, showResultCount);
                }
                src.Result = finalResult;
            }
            catch (WebException ex)
            {
                src.Error = ex.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchSightseeingResultGTA :WebException for GTA = " + ex.Message);
                Audit.Add(EventType.SightseeingSearch, Severity.High, 0, string.Concat(new object[]
                {
                    "Web Exception returned from GTA. Error Message:",
                    ex.Message,
                    " | ",
                    DateTime.Now
                }), "");
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Trace.TraceInformation("MetaSearchEngine.SearchSightseeingResultGTA:Exception for GTA = " + excep.Message);
                Audit.Add(EventType.SightseeingSearch, Severity.High, 0, string.Concat(new object[]
                {
                    "Exception returned from GTA. Error Message:",
                    excep.Message,
                    " | ",
                    DateTime.Now
                }), "");
            }
            this.sightseeingSearchResBySrc.Add(src);
            this.eventFlag[(int)eventNumber].Set();
            Trace.TraceInformation("MetaSearchEngine.SearchTransferResultGTA  Exit");
        }


        #endregion
        #region Transfer Search
        /// <summary>
        /// This Method is used to get Transfer Results from various sources.
        /// </summary>
        /// <param name="req"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        public TransferSearchResult[] GetTransferResults(TransferRequest req, UserMaster member)
        {
            Trace.TraceInformation("MetaSearchEngine.GetTransferResults Entered");
            StartSession();
            transferRequest = req;


            //hCity = HotelCity.Load(HotelCity.GetCityIdFromCityName(req.CityName));
            hCity = HotelCity.Load(Convert.ToInt32(req.PickUpCityCode));
            Audit.Add(EventType.TransferSearch, Severity.Normal, 1, "GetTransferResults Entered ", "0");//modified on 27042016

            {
                Audit.Add(EventType.GTAAvailSearch, Severity.Normal, 1, "Entered GTA Transfer Search", "0");
                listOfThreads.Add("GTA", new WaitCallback(SearchTransferResultGTA));
            }
            TransferSearchResult[] result = new TransferSearchResult[0];

            // take only those sources in ready sources for which the search is allowed

            Dictionary<string, int> readySources = GetReadyTransferSources();

            eventFlag = new AutoResetEvent[readySources.Count];
            int maxTimeOut = 0;
            int i = 0;
            foreach (KeyValuePair<string, int> activeSource in readySources)
            {
                maxTimeOut = (int)activeSource.Value > maxTimeOut ? (int)activeSource.Value : maxTimeOut;
                i++;
            }

            int j = 0;

            foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
            {
                if (readySources.ContainsKey(deThread.Key))
                {
                    ThreadPool.QueueUserWorkItem(deThread.Value, j);
                    eventFlag[j] = new AutoResetEvent(false);
                    j++;
                }
            }

            if (j != 0)
            {
                if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 0, maxTimeOut), true))
                {
                    //TODO: audit which thread is timed out                
                }
            }

            // combined sources    
            result = CombineTransferSources();

            Trace.TraceInformation("MetaSearchEngine.GetTransferResults Exit");
            return result;
        }
        /// <summary>
        /// This Method is used to Search Transfers for GTA
        /// </summary>
        /// <param name="eventNumber"></param>
        private void SearchTransferResultGTA(object eventNumber)
        {
            //GTADBG
            Audit.Add(EventType.GTAAvailSearch, Severity.Normal, 1, "Entered SearchTransferResultGTA", "0");
            TransferSearchResultBySources src = new TransferSearchResultBySources();
            src.Source = "GTA";
            TransferRequest tRequest = new TransferRequest();
            tRequest = transferRequest.CopyByValue();
            try
            {
                if (hCity.GTACode != null && hCity.GTACode.Length > 0)
                {
                    tRequest.DropOffCityCode = hCity.GTACode;
                    tRequest.PickUpCityCode = hCity.GTACode;
                    //Added by brahmam Currency we need to pass AED only
                    tRequest.Currency = Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"];
                }
                else
                {
                    eventFlag[(int)eventNumber].Set();
                    Audit.Add(EventType.GTAAvailSearch, Severity.High, 0, "MetaSearchEngine.SearchResultGTA : City code not Found for Pick Up = " + this.transferRequest.PickUpCityCode + " and Drop Off = " + this.transferRequest.DropOffCityCode, string.Empty);
                    return;
                }


                #region Get Nationality Code| Added on 08062016
               
                DOTWCountry dotwApi = new DOTWCountry();
                Dictionary<string, string> Countries = dotwApi.GetAllCountries();
                tRequest.NationalityCode = Country.GetCountryCodeFromCountryName(Countries[tRequest.NationalityCode]);
                #endregion



                GTA gtaApi = new GTA();
                if (SettingsLoginInfo.IsOnBehalfOfAgent)
                {
                    gtaApi.AgentExchangeRates = SettingsLoginInfo.OnBehalfAgentExchangeRates;
                    gtaApi.AgentCurrency = SettingsLoginInfo.OnBehalfAgentCurrency;
                    gtaApi.AgentDecimalPoint = SettingsLoginInfo.OnBehalfAgentDecimalValue;
                }
                else
                {
                    gtaApi.AgentExchangeRates = SettingsLoginInfo.AgentExchangeRates;
                    gtaApi.AgentCurrency = SettingsLoginInfo.Currency;
                    gtaApi.AgentDecimalPoint = SettingsLoginInfo.DecimalValue;
                }

                DataTable dtMarkup = UpdateMarkup.Load((int)SettingsLoginInfo.AgentId,  TransferBookingSource.GTA.ToString(), (int)ProductType.Transfers); //modified on 27042016
               
               //modified by brahmam calculate B2B Markup
                DataView dv = dtMarkup.DefaultView;
                dv.RowFilter = "TransType IN('B2B')";
                dtMarkup = dv.ToTable();
                decimal markup = 0; string markupType = string.Empty;

                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                {
                    markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                    markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                }
                src.Result = gtaApi.GetTransferAvailability(tRequest, markup, markupType);

                //// resizing according to search result count
                TransferSearchResult[] finalResult = new TransferSearchResult[src.Result.Length];
                finalResult = src.Result;
                int showResultCount = Configuration.ConfigurationSystem.ResultCountBySources["GTA"];
                if (src.Result.Length > showResultCount)
                {
                    Array.Resize(ref finalResult, showResultCount);
                }
                src.Result = finalResult;
            }

            catch (WebException ex)
            {
                src.Error = ex.Message;
                Audit.Add(EventType.TransferSearch, Severity.High, 0, "Web Exception returned from GTA. Error Message:" + ex.Message + " | " + DateTime.Now, "");
            }
            catch (Exception excep)
            {
                src.Error = excep.Message;
                Audit.Add(EventType.TransferSearch, Severity.High, 0, "Exception returned from GTA. Error Message:" + excep.Message + " | " + DateTime.Now, "");
            }

            transferSearchResBySrc.Add(src);
            eventFlag[(int)eventNumber].Set();

            Trace.TraceInformation("MetaSearchEngine.SearchTransferResultGTA  Exit");
        }
        /// <summary>
        /// combine all results from different Transfer sources
        /// </summary>
        /// <returns></returns>
        private TransferSearchResult[] CombineTransferSources()
        {
            Trace.TraceInformation("MetaSearchEngine.CombineTransferSources Enter");
            int totalRes = 0;
            for (int i = 0; i < transferSearchResBySrc.Count; i++)
            {
                if (transferSearchResBySrc[i].Result != null)
                {
                    totalRes = totalRes + transferSearchResBySrc[i].Result.Length;
                }
            }

            TransferSearchResult[] finalResult = new TransferSearchResult[totalRes];
            int count = 0;

            for (int i = 0; i < transferSearchResBySrc.Count; i++)
            {
                if (transferSearchResBySrc[i].Result != null)
                {
                    for (int k = 0; k < transferSearchResBySrc[i].Result.Length; k++)
                    {
                        int j = k + count;
                        finalResult[j] = transferSearchResBySrc[i].Result[k];
                    }
                    count = count + transferSearchResBySrc[i].Result.Length;
                }
            }

            //if (transferSearchResBySrc.Count > 1)
            //{
            //    //rate of exchange to compare the price in INR
            //    StaticData staticInfo = new StaticData();
            //    Dictionary<string, decimal> rateOfEx = staticInfo.CurrencyROE;
            //    for (int n = 0; n < finalResult.Length - 1; n++)
            //    {
            //        for (int m = n + 1; m < finalResult.Length; m++)
            //        {
            //            decimal exRateN = 1, exRateM = 1;
            //            if (rateOfEx.ContainsKey(finalResult[n].Currency))
            //            {
            //                exRateN = rateOfEx[finalResult[n].Currency];
            //            }
            //            if (rateOfEx.ContainsKey(finalResult[m].Currency))
            //            {
            //                exRateM = rateOfEx[finalResult[m].Currency];
            //            }
            //            decimal priceN = finalResult[n].RoomDetails[0].TotalPrice * exRateN;
            //            decimal priceM = finalResult[m].RoomDetails[0].TotalPrice * exRateM;
            //            if (priceN > priceM)
            //            {
            //                HotelSearchResult temp = finalResult[n];
            //                finalResult[n] = finalResult[m];
            //                finalResult[m] = temp;
            //            }
            //        }
            //    }
            //}

            Trace.TraceInformation("MetaSearchEngine.CombineTransferSources Exit");
            return finalResult;
        }
        #endregion

    }
}
