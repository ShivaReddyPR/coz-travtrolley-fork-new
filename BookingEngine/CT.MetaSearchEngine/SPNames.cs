namespace CT.MetaSearchEngine
{
    internal static class SPNames
    {
        public const string GetSourceByOriginDest = "usp_GetSourceByOriginDest";
        public const string GetHoldPnrToRelease = "usp_GetHoldPnrToRelease";
    }
}
