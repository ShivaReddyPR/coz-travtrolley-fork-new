﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CT.BookingEngine;
using CT.Configuration;
using System.Xml;
using System.Net;
using System.IO;
using System.IO.Compression;
using CT.Core;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;

namespace LotsOfHotels
{
    public class JuniperXMLEngine
    {
        #region Members
        private string agentCode;
        private string password;
        //private Dictionary<string, decimal> rateOfExchange;
        private string sessionId;
        private int appUserId;
        decimal rateOfExchange = 1;
        string agentCurrency = "";
        Dictionary<string, decimal> exchangeRates;
        int decimalPoint;
        #endregion

        #region Constructors
        public JuniperXMLEngine()
        {
            agentCode = ConfigurationSystem.LOHConfig["AgentCode"];
            password = ConfigurationSystem.LOHConfig["Password"];
            
        }

        public JuniperXMLEngine(string SessionId)
        {
            agentCode = ConfigurationSystem.LOHConfig["AgentCode"];
            password = ConfigurationSystem.LOHConfig["Password"];
            
            this.sessionId = SessionId;
            
        } 
        #endregion

        #region Properties
        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        #endregion

        #region Common Methods
        private  Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        } 
        #endregion

        #region Get Hotel Results

        /// <summary>
        /// Returns Hotel Results for the Search Criteria.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public HotelSearchResult[] GetSearchResults(HotelRequest request, decimal markup, string markupType)
        {
            List<HotelSearchResult> Results = new List<HotelSearchResult>();
            try
            {
                DateTime before = DateTime.Now;
                string responseXML = GetHotelSearchResponse(request);
                DateTime after = DateTime.Now;
                TimeSpan ts = after - before;

                //Audit.Add(EventType.Search, Severity.Normal, 1, "LotsOfHotels Search returned in " + ts.Milliseconds + " Milliseconds", "1");
                Results = ReadHotelSearchResponse(responseXML, ref request, markup, markupType);

                if (request.HotelName != null && request.HotelName.Trim().Length > 0)
                {
                    Results = Results.FindAll(delegate(HotelSearchResult obj) { return obj.HotelName.ToLower().Contains(request.HotelName.ToLower()); });
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to get Hotel Results : " + ex.Message, ex);
            }

            return Results.ToArray();
        }

        /// <summary>
        /// Generates Hotel Search Request XML.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private string GenerateHotelSearchRequest(HotelRequest request)
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);                
                writer.WriteStartElement("OTA_HotelAvailService", "http://www.opentravel.org/OTA/2003/05");
                writer.WriteStartElement("OTA_HotelAvailRQ");
                writer.WriteAttributeString("PrimaryLangID", "EN");
                writer.WriteStartElement("POS");
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("AgentDutyCode", agentCode);
                writer.WriteStartElement("RequestorID");
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteEndElement();//RequestorID
                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS
                writer.WriteStartElement("AvailRequestSegments");
                writer.WriteStartElement("AvailRequestSegment");
                writer.WriteStartElement("StayDateRange");
                writer.WriteAttributeString("Start", request.StartDate.ToString("yyyy-MM-dd"));
                writer.WriteAttributeString("End", request.EndDate.ToString("yyyy-MM-dd"));
                writer.WriteEndElement();//StayDateRange

                writer.WriteStartElement("RoomStayCandidates");
                foreach (RoomGuestData guest in request.RoomGuest)
                {
                    writer.WriteStartElement("RoomStayCandidate");
                    writer.WriteAttributeString("Quantity", "1");
                    writer.WriteStartElement("GuestCounts");
                    writer.WriteStartElement("GuestCount");
                    //writer.WriteAttributeString("Age", "40"); not mandatory
                    writer.WriteAttributeString("Count", guest.noOfAdults.ToString());
                    writer.WriteEndElement();//GuestCount
                    if (guest.noOfChild > 0)
                    {
                        foreach (int age in guest.childAge)
                        {
                            writer.WriteStartElement("GuestCount");
                            writer.WriteAttributeString("Age", age.ToString());
                            writer.WriteAttributeString("Count", "1");
                            writer.WriteEndElement();//GuestCount
                        }
                    }
                    writer.WriteEndElement();//RoomStayCandidate
                    writer.WriteEndElement();//RoomStayCandidate    
                }
                
                writer.WriteEndElement();//RoomStayCandidates
                writer.WriteStartElement("HotelSearchCriteria");
                writer.WriteStartElement("Criterion");
                writer.WriteStartElement("HotelRef");
                writer.WriteAttributeString("HotelCityCode", request.CityCode);
                writer.WriteAttributeString("HotelCode", "");
                writer.WriteEndElement();//HotelRef
                writer.WriteStartElement("TPA_Extensions");
                writer.WriteElementString("ShowBasicInfo", "1");
                //writer.WriteElementString("ShowPromotions", "1");
                //writer.WriteElementString("ShowCatalogueData", "1");
                //writer.WriteElementString("ShowInternalRoomInfo", "1");
                writer.WriteElementString("ShowOnlyAvailable", "1");
                writer.WriteElementString("PaxCountry", request.PassengerNationality);
                writer.WriteElementString("SearchByPaymentType", "ExcludePaymentInDestination");
                writer.WriteEndElement();//TPA_Extensions
                writer.WriteEndElement();//Criterion
                writer.WriteEndElement();//HotelSearchCriterion
                writer.WriteEndElement();//AvailabilitySegment
                writer.WriteEndElement();//AvailabilitySegments
                writer.WriteEndElement();//OTA_HotelAvailRQ
                writer.WriteEndElement();//OTA_HotelAvailService
                writer.WriteEndElement();//Soap:Body
                writer.WriteEndElement();//Soap:Envelope

                writer.Flush();
                writer.Close();

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelSearchRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
               
                
            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to generate Hotel Request message : " + ex.Message, ex);
            }

            return requestMsg.ToString();
            //return "XML=" + System.Web.HttpUtility.UrlEncode(requestMsg.ToString());
        }

        /// <summary>
        /// Generates Hotel Search Response XML.
        /// </summary>
        /// <param name="hotelRequest"></param>
        /// <returns></returns>
        private string GetHotelSearchResponse(HotelRequest hotelRequest)
        {
            string responseFromServer = "";
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.LOHConfig["HotelAvailReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelSearchRequest(hotelRequest);// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;
                
                Stream requestWriter = (request.GetRequestStream());
                                
                requestWriter.Write(bytes, 0, bytes.Length);                       
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;
                 

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to get response from Hotel Search request : " + ex.Message, ex);
            }
            return responseFromServer;
        }        

        /// <summary>
        /// Reads Hotel Search Response XML and returns HotelSearchResult Array.
        /// </summary>
        /// <param name="response"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        private List<HotelSearchResult> ReadHotelSearchResponse(string response, ref HotelRequest request, decimal markup, string markupType)
        {
            List<HotelSearchResult> hotelResults = new List<HotelSearchResult>();

            try
            {
                XmlDocument doc = new XmlDocument();
                //string filePath = @"C:\Developments\DotNet\from ziyad\21Sep2014\LOH\" + "HotelSearchResponse.xml";
                doc.LoadXml(response);
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelSearchResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    doc.Save(filePath);
                    //Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
               
                XmlNode root = doc.DocumentElement;
                XmlNodeList Errors = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild.ChildNodes;

                if (Errors != null && Errors[0].Name == "Errors")
                {
                    hotelResults = new List<HotelSearchResult>();
                }
                else
                {
                    XmlNode HotelAvailResult = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild;

                    string sequenceNumber = HotelAvailResult.Attributes["SequenceNmbr"].Value;
                   
                    XmlNodeList roomStays = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild.ChildNodes[1].ChildNodes;
                    //Depicts a Hotel Room list
                    
                    foreach (XmlNode roomStay in roomStays)
                    {
                        HotelSearchResult result = new HotelSearchResult();
                        result.RoomGuest = request.RoomGuest;
                        result.SequenceNumber = sequenceNumber;
                        result.StartDate = request.StartDate;
                        result.EndDate = request.EndDate;
                        result.Currency = agentCurrency;//childRate.Attributes["CurrencyCode"].Value;
                        result.Price = new PriceAccounts();
                        foreach (XmlNode child in roomStay)
                        {
                            result.BookingSource = HotelBookingSource.LOH;
                            result.RateType = RateType.Negotiated;
                            switch (child.Name)
                            {
                                case "RoomRates":
                                    #region Room Details
                                    int rooms = 0;
                                    List<HotelRoomsDetails> roomDetails = new List<HotelRoomsDetails>();
                                    foreach (XmlNode rate in child.ChildNodes)
                                    {
                                        //Avoid OnRequest room types in the result
                                        if (rate.Attributes["AvailabilityStatus"].Value == "AvailableForSale")
                                        {
                                            HotelRoomsDetails room = new HotelRoomsDetails();
                                            room.RatePlanCode = rate.Attributes["RatePlanCode"].Value;
                                            room.RoomTypeCode = rate.Attributes["RatePlanCategory"].Value;
                                            room.mealPlanDesc = room.RoomTypeCode;//If MealPlan is not available, this will be shown and stored in DB.
                                            foreach (XmlNode childRate in rate.ChildNodes)
                                            {
                                                switch (childRate.Name)
                                                {
                                                    case "Rates":
                                                        foreach (XmlNode rrNode in childRate.ChildNodes)
                                                        {
                                                            List<RoomRates> roomRates = new List<RoomRates>();

                                                            foreach (XmlNode ratesNode in rrNode.ChildNodes)
                                                            {
                                                                room.SequenceNo = rrNode.Attributes["RateSource"].Value;
                                                                string cc = "USD";
                                                                
                                                                switch (ratesNode.Name)
                                                                {
                                                                    case "Total":
                                                                        #region Room Rates
                                                                        cc = (ratesNode.Attributes["CurrencyCode"] != null ? ratesNode.Attributes["CurrencyCode"].Value : "USD");
                                                                        rateOfExchange = (exchangeRates.ContainsKey(cc) ? exchangeRates[cc] : 1);
                                                                        result.Price = new PriceAccounts();
                                                                        result.Price.SupplierCurrency = cc;
                                                                        result.Price.RateOfExchange = rateOfExchange;
                                                                        int days = request.EndDate.Subtract(request.StartDate).Days;
                                                                        for (int i = 0; i < days; i++)
                                                                        {
                                                                            RoomRates roomRate = new RoomRates();
                                                                            roomRate.Amount = Convert.ToDecimal(ratesNode.Attributes["AmountAfterTax"].Value) * rateOfExchange;
                                                                            roomRate.RateType = RateType.Negotiated;
                                                                            roomRate.SellingFare = roomRate.Amount;
                                                                            roomRate.Totalfare = roomRate.Amount;
                                                                            roomRate.BaseFare = roomRate.Amount;
                                                                            roomRate.Days = request.EndDate;
                                                                            roomRates.Add(roomRate);
                                                                        }
                                                                        room.supplierPrice = Convert.ToDecimal(ratesNode.Attributes["AmountAfterTax"].Value);
                                                                        room.SellingFare = Convert.ToDecimal(ratesNode.Attributes["AmountAfterTax"].Value) * rateOfExchange;
                                                                        room.TotalPrice = room.SellingFare;
                                                                        room.Markup = (markupType == "F" ? markup : room.SellingFare * (markup / 100));
                                                                        room.MarkupType = markupType;
                                                                        room.MarkupValue = markup;
                                                                        room.Rates = roomRates.ToArray();
                                                                        #endregion
                                                                        break;
                                                                    case "RateDescription":
                                                                        room.RoomTypeName = ratesNode.FirstChild.InnerText;
                                                                        break;
                                                                    case "TPA_Extensions":
                                                                        if (ratesNode.ChildNodes != null)
                                                                        {
                                                                            foreach (XmlNode extNode in ratesNode.ChildNodes)
                                                                            {
                                                                                switch (extNode.Name.ToUpper())
                                                                                {
                                                                                    case "MEALPLAN":
                                                                                        //room.mealPlanDesc = (extNode.InnerText == "3" ? "Room Only" : extNode.InnerText);//TODO: code is stored now. need to retrieve from  data later.
                                                                                        try
                                                                                        {
                                                                                            CatalogueData cd = new CatalogueData();
                                                                                            room.mealPlanDesc = cd.GetStaticData(extNode.InnerText, "MP", HotelBookingSource.LOH);
                                                                                        }
                                                                                        catch { }
                                                                                        break;
                                                                                    case "RECOMMENDEDPRICE":
                                                                                        room.SellingFare = Convert.ToDecimal(extNode.InnerText) * rateOfExchange;
                                                                                        room.TotalPrice = room.SellingFare;
                                                                                        room.Markup = (markupType == "F" ? markup : room.SellingFare * (markup / 100));
                                                                                        room.MarkupType = markupType;
                                                                                        room.MarkupValue = markup;
                                                                                        for (int i = 0; i < room.Rates.Length; i++)
                                                                                        {
                                                                                            RoomRates rr = room.Rates[i];
                                                                                            rr.Amount = room.SellingFare;
                                                                                            rr.BaseFare = room.SellingFare;
                                                                                            rr.SellingFare = room.SellingFare;
                                                                                            rr.Totalfare = room.SellingFare;
                                                                                        }
                                                                                        break;
                                                                                }
                                                                            }
                                                                        }
                                                                        break;
                                                                }

                                                            }
                                                            
                                                            room.Rates = roomRates.ToArray();
                                                            if (!roomDetails.Contains(room))
                                                            {
                                                                roomDetails.Add(room);
                                                            }
                                                            if (Convert.ToInt32(rrNode.Attributes["NumberOfUnits"].Value) > 1)
                                                            {
                                                                int rc = Convert.ToInt32(rrNode.Attributes["NumberOfUnits"].Value) - 1;
                                                                for (int i = rc; i < Convert.ToInt32(rrNode.Attributes["NumberOfUnits"].Value); i++)
                                                                {
                                                                    roomDetails.Add(room);
                                                                }
                                                            }                                                       
                                                        }                                                        
                                                        break;
                                                    case "Features":
                                                        //TODO: need to discuss with Ziyad.
                                                        foreach (XmlNode offerNode in childRate.ChildNodes)
                                                        {
                                                            switch (offerNode.Attributes["RoomViewCode"].Value)
                                                            {
                                                                case "PROMO":
                                                                    result.PromoMessage = offerNode.FirstChild.FirstChild.InnerText;
                                                                    for (int i = 0; i < roomDetails.Count; i++)
                                                                    {
                                                                        HotelRoomsDetails rd = roomDetails[i];
                                                                        if (room.RatePlanCode == rd.RatePlanCode)
                                                                        {
                                                                            rd.PromoMessage = offerNode.FirstChild.FirstChild.InnerText;
                                                                            roomDetails[i] = rd;
                                                                        }
                                                                    }
                                                                    break;
                                                                case "RECOMMENDED_PRICE":
                                                                    string cur = (childRate.Attributes["CurrencyCode"] != null ? childRate.Attributes["CurrencyCode"].Value : "USD");
                                                                    result.Price.SupplierPrice = Convert.ToDecimal(offerNode.FirstChild.FirstChild.InnerText);
                                                                    result.Price.SupplierCurrency = cur;                                                                    
                                                                    if (rooms == 0)
                                                                    {
                                                                        result.SellingFare = Convert.ToDecimal(offerNode.FirstChild.FirstChild.InnerText) * rateOfExchange;
                                                                        result.TotalPrice = result.SellingFare + (markupType == "F" ? markup * request.NoOfRooms: result.SellingFare * (markup / 100));
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                        break;
                                                    case "Total":
                                                        if (rooms == 0 && result.TotalPrice == 0)
                                                        {
                                                            string ccode = (childRate.Attributes["CurrencyCode"] != null ? childRate.Attributes["CurrencyCode"].Value : "USD");
                                                            result.Price.SupplierPrice = Convert.ToDecimal(childRate.Attributes["AmountAfterTax"].Value);                                                            
                                                            result.SellingFare = Convert.ToDecimal(childRate.Attributes["AmountAfterTax"].Value) * rateOfExchange;
                                                            result.TotalPrice = result.SellingFare + (markupType == "F" ? markup * request.NoOfRooms : result.SellingFare * (markup / 100));
                                                        }
                                                        break;
                                                    case "TPA_Extensions":
                                                        //Avoid Direct Payment room types in the result
                                                        if (childRate != null && childRate.FirstChild != null)
                                                        {
                                                            if (childRate.FirstChild.Name.ToUpper() == "PAYMENTINDESTINATION")
                                                            {
                                                                room.RatePlanCode = string.Empty;
                                                            }
                                                            else if (childRate.FirstChild.Name.ToUpper() == "MEALPLAN")
                                                            {
                                                                //room.mealPlanDesc = (childRate.FirstChild.InnerText == "3" ? "Room Only" : "Other Meal Plan");//Need to fetch Meal Plan Desc                                                            
                                                                try
                                                                {
                                                                    CatalogueData cd = new CatalogueData();
                                                                    room.mealPlanDesc = cd.GetStaticData(childRate.FirstChild.InnerText, "MP", HotelBookingSource.LOH);
                                                                }
                                                                catch { }
                                                            }
                                                        }
                                                        break;
                                                }
                                                
                                            }
                                            rooms++;
                                        }
                                    }                                    
                                    result.RoomDetails = roomDetails.ToArray();
                                    #endregion
                                    break;
                                case "Total":
                                   // result.Currency = agentCurrency;//childRate.Attributes["CurrencyCode"].Value;
                                    break;
                                case "BasicPropertyInfo":                                    
                                    result.HotelName = child.Attributes["HotelName"].Value;
                                    result.HotelCode = child.Attributes["HotelCode"].Value;
                                    //result.SellingFare = (result.RoomDetails[0].SellingFare > 0 ? result.RoomDetails[0].SellingFare : 0);//Assuming lowest room fare comes first
                                    //result.TotalPrice = result.SellingFare;
                                    
                                    result.Price.NetFare = result.SellingFare;
                                    result.Price.AccPriceType = PriceType.NetFare;
                                    result.Price.CurrencyCode = result.Currency;
                                    result.Price.Currency = result.Currency;
                                    break;
                                case "TPA_Extensions":
                                    #region Hotel Info
                                    foreach (XmlNode subChild in child)
                                    {
                                        switch (subChild.Name)
                                        {
                                            case "HotelInfo":
                                                foreach (XmlNode childs in subChild.ChildNodes)
                                                {
                                                    switch (childs.Name)
                                                    {
                                                        #region Rating
                                                        case "Category":
                                                            switch (childs.Attributes["Code"].Value)
                                                            {
                                                                case "1":
                                                                    result.Rating = HotelRating.OneStar;
                                                                    break;
                                                                case "2":
                                                                    result.Rating = HotelRating.TwoStar;
                                                                    break;
                                                                case "3":
                                                                    result.Rating = HotelRating.ThreeStar;
                                                                    break;
                                                                case "4":
                                                                    result.Rating = HotelRating.FourStar;
                                                                    break;
                                                                case "5":
                                                                    result.Rating = HotelRating.FiveStar;
                                                                    break;
                                                                case "":  
                                                                case "0":
                                                                    result.Rating = HotelRating.OneStar;
                                                                    break;
                                                            }
                                                            break;
                                                        #endregion
                                                        case "Description":
                                                            result.HotelDescription = System.Web.HttpUtility.HtmlDecode(childs.InnerText);
                                                            break;
                                                        case "Thumb":
                                                            result.HotelPicture = System.Web.HttpUtility.HtmlDecode(childs.InnerText);
                                                            break;
                                                        case "Address":
                                                            result.HotelAddress = System.Web.HttpUtility.HtmlDecode(childs.InnerText);
                                                            break;
                                                        case "AreaID":
                                                            result.CityCode = (childs.InnerText == "96952" || childs.InnerText == "53607" ? "Dubai City" : childs.InnerText);
                                                            break;
                                                        case "Provider":
                                                            break;
                                                        case "Longitude":
                                                            break;
                                                        case "Latitude":
                                                            break;

                                                    }
                                                }
                                                break;
                                        }
                                    }
                                    #endregion
                                    break;
                            }
                        }
                        if (result.SellingFare > 0 )
                        {
                            hotelResults.Add(result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to read Hotel Search Response: " + ex.Message, ex);
            }

            return hotelResults;
        }
        #endregion

        #region Get Hotel Details
        /// <summary>
        /// Gets the Hotel Details for the Hotel.
        /// </summary>
        /// <param name="hotelCode"></param>
        /// <returns></returns>
        public HotelDetails GetHotelDetails(string hotelCode)
        {
            HotelDetails hotelDetails = new HotelDetails();
            try
            {
                string responseXML = GetHotelDescriptiveInfoResponse(hotelCode);

                hotelDetails = ReadHotelDescriptiveInfoResponse(responseXML);
            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to get Hotel Details : " + ex.Message, ex);
            }
            return hotelDetails;
        }

        /// <summary>
        /// Generates the Hotel Detail request.
        /// </summary>
        /// <param name="hotelCode"></param>
        /// <returns></returns>
        private string GenerateHotelDescriptiveInfoRequest(string hotelCode)
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("OTA_HotelDescriptiveInfoService", "http://www.opentravel.org/OTA/2003/05");
                writer.WriteStartElement("OTA_HotelDescriptiveInfoRQ");
                writer.WriteAttributeString("PrimaryLangID", "EN");
                writer.WriteStartElement("POS");
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("AgentDutyCode", agentCode);
                writer.WriteStartElement("RequestorID");
                writer.WriteAttributeString("Type", "1");
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteEndElement();//RequestorID
                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS

                writer.WriteStartElement("HotelDescriptiveInfos");
                writer.WriteStartElement("HotelDescriptiveInfo");
                writer.WriteAttributeString("HotelCode", hotelCode);
                writer.WriteEndElement();//HotelDescriptiveInfo
                writer.WriteEndElement();//HotelDescriptiveInfos
                writer.WriteEndElement();//OTA_HotelDescriptiveInfoRQ
                writer.WriteEndElement();//OTA_HotelDescriptiveInfoService
                writer.WriteEndElement();//Body
                writer.WriteEndElement();//Envelope

                writer.Flush();
                writer.Close();

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelDescInfoRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                    Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to generate Hotel Descriptive request :" + ex.Message, ex);
            }

            return requestMsg.ToString();
        }

        /// <summary>
        /// Generates the Response XML for the Request string.
        /// </summary>
        /// <param name="hotelCode"></param>
        /// <returns></returns>
        private string GetHotelDescriptiveInfoResponse(string hotelCode)
        {
            string responseFromServer = "";
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.LOHConfig["HotelDescInfoReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelDescriptiveInfoRequest(hotelCode);// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to get response from Hotel Descriptive Info request: " + ex.ToString(), ex);
            }
            return responseFromServer;
        }

        /// <summary>
        /// Reads the Hotel Detail response XML.
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private HotelDetails ReadHotelDescriptiveInfoResponse(string response)
        {
            HotelDetails hotelDetails = new HotelDetails();

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(response);
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelDescInfoResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";                    
                    doc.Save(filePath);
                    Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");             

                XmlNode root = doc.DocumentElement;
                XmlNodeList HotelDescContents = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild.ChildNodes;

                if (HotelDescContents[0].Name == "Errors")
                {
                    XmlNode Error = HotelDescContents[0];

                    throw new Exception("LOH-Failed to retrieve Hotel Descriptive Info for Hotel :" + Error.FirstChild.Attributes["ShortText"].Value);
                }
                else
                {
                    hotelDetails.HotelCode = HotelDescContents[0].Attributes["HotelCode"].Value;
                    hotelDetails.HotelName = HotelDescContents[0].Attributes["HotelName"].Value;
                    XmlNode DescContent = HotelDescContents[0].FirstChild;
                    foreach (XmlNode node in DescContent.ChildNodes)
                    {
                       
                        switch (node.Name)
                        {
                            case "HotelInfo":
                                #region HotelInfo
                                XmlNode HotelInfo = node;
                                XmlNode HotelCategory = HotelInfo.FirstChild.FirstChild;

                                //Rating
                                switch (HotelCategory.Attributes["CodeDetail"].Value)
                                {
                                    case "1":
                                        hotelDetails.hotelRating = HotelRating.OneStar;
                                        break;
                                    case "2":
                                        hotelDetails.hotelRating = HotelRating.TwoStar;
                                        break;
                                    case "3":
                                        hotelDetails.hotelRating = HotelRating.ThreeStar;
                                        break;
                                    case "4":
                                        hotelDetails.hotelRating = HotelRating.FourStar;
                                        break;
                                    case "5":
                                        hotelDetails.hotelRating = HotelRating.FiveStar;
                                        break;
                                    case "0":
                                        hotelDetails.hotelRating = HotelRating.All;
                                        break;
                                }

                                //HotelImages
                                XmlNodeList MultimediaDescs = HotelInfo["Descriptions"].FirstChild.ChildNodes;

                                foreach (XmlNode multiDesc in MultimediaDescs)
                                {
                                    switch (multiDesc.FirstChild.Name)
                                    {
                                        case "ImageItems":
                                            hotelDetails.Images = new List<string>();
                                            foreach (XmlNode image in multiDesc.FirstChild.ChildNodes)
                                            {
                                                hotelDetails.Images.Add(image.FirstChild.Attributes["FileName"].Value);
                                            }
                                            break;
                                        case "TextItems":
                                            hotelDetails.RoomData = new Room[1];
                                            foreach (XmlNode textItem in multiDesc.FirstChild.ChildNodes)
                                            {
                                                switch (textItem.Attributes[0].Value)
                                                {
                                                    case "LongDescription":
                                                        hotelDetails.Description = textItem.FirstChild.InnerText;
                                                        break;
                                                    case "RoomDescription":
                                                        hotelDetails.RoomData[0].Description = textItem.FirstChild.InnerText;
                                                        break;
                                                    case "ZoneDescription":
                                                        hotelDetails.Description += " " + textItem.FirstChild.InnerText;
                                                        break;
                                                }
                                            }
                                            break;
                                    }
                                }

                                XmlNode Position = HotelInfo["Position"];

                                hotelDetails.Map = Position.Attributes["Latitude"].Value;
                                hotelDetails.Map += "," + Position.Attributes["Longitude"].Value;

                                XmlNodeList Services = HotelInfo["Services"].ChildNodes;

                                hotelDetails.HotelFacilities = new List<string>();
                                hotelDetails.RoomFacilities = new List<string>();

                                foreach (XmlNode Service in Services)
                                {
                                    switch (Service.Attributes["Sort"].Value)
                                    {
                                        case "Hotel":
                                        case "Other":
                                            hotelDetails.HotelFacilities.Add(Service.Attributes["CodeDetail"].Value);
                                            break;
                                        case "Room":
                                            hotelDetails.RoomFacilities.Add(Service.Attributes["CodeDetail"].Value);
                                            break;
                                    }
                                }
                                
                                #endregion
                                break;
                            case "AreaInfo":
                                hotelDetails.Attractions = new Dictionary<string, string>();
                                foreach (XmlNode refPoint in node.FirstChild.ChildNodes)
                                {
                                    hotelDetails.Attractions.Add(refPoint.Attributes["Name"].Value, refPoint.Attributes["Distance"].Value);
                                }
                                break;
                            case "Policies":
                                XmlNodeList policies = node.ChildNodes;
                                foreach (XmlNode penalty in policies)
                                {
                                    hotelDetails.HotelPolicy += "|" + penalty.FirstChild.FirstChild.FirstChild.Attributes["Name"].Value;
                                }
                                break;
                            case "ContactInfos":
                                foreach (XmlNode contactInfo in node.FirstChild.ChildNodes)
                                {
                                    switch (contactInfo.Name)
                                    {
                                        case "Addresses":
                                            foreach (XmlNode address in contactInfo.FirstChild.ChildNodes)
                                            {
                                                switch (address.Name)
                                                {
                                                    case "AddressLine":
                                                        hotelDetails.Address = address.InnerText;
                                                        break;
                                                    case "CityName":
                                                        break;
                                                    case "PostalCode":
                                                        break;
                                                    case "County":
                                                        break;
                                                    case "CountryName":
                                                        break;
                                                }
                                            }
                                            break;
                                        case "Phones":
                                            foreach (XmlNode phone in contactInfo.ChildNodes)
                                            {
                                                if (hotelDetails.PhoneNumber != null && hotelDetails.PhoneNumber.Length > 0)
                                                {
                                                    hotelDetails.PhoneNumber += ", " + phone.Attributes["ID"].Value;
                                                }
                                                else
                                                {
                                                    hotelDetails.PhoneNumber = phone.Attributes["ID"].Value;
                                                }
                                            }
                                            break;
                                    }
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to read Hotel Descriptive response :" + ex.Message, ex);
            }

            return hotelDetails;
        }
        #endregion

        #region Get Hotel Cancellation policy
        /// <summary>
        /// Returns the Cancellation policy for the room of a Hotel.
        /// </summary>
        /// <param name="hotelCode"></param>
        /// <param name="ratePlanCode"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetCancellationDetails(string hotelCode, string ratePlanCode, DateTime startDate, DateTime endDate, string sequenceNumber)
        {
            Dictionary<string, string> cancellationPolicy = new Dictionary<string, string>();
            
            try
            {
                string responseXML = GenerateHotelBookingRuleResponse(hotelCode, ratePlanCode, startDate, endDate, sequenceNumber);

                cancellationPolicy = ReadCancellationPolicyResponse(responseXML);
            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to get Cancellation policy : " + ex.Message, ex);
            }

            return cancellationPolicy;
        }

        /// <summary>
        /// Generates Hotel Booking Rule request.
        /// </summary>
        /// <param name="hotelCode"></param>
        /// <param name="ratePlanCode"></param>
        /// <returns></returns>
        private string GenerateHotelBookingRuleRequest(string hotelCode, string ratePlanCode, DateTime startDate, DateTime endDate,string sequenceNumber)
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("OTA_HotelBookingRuleService", "http://www.opentravel.org/OTA/2003/05");
                writer.WriteStartElement("OTA_HotelBookingRuleRQ");
                writer.WriteAttributeString("PrimaryLangID", "EN");
                writer.WriteAttributeString("SequenceNmbr", sequenceNumber);
                writer.WriteStartElement("POS");
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("AgentDutyCode", agentCode);
                writer.WriteStartElement("RequestorID");
                writer.WriteAttributeString("Type", "1");
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteEndElement();//RequestorID
                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS

                writer.WriteStartElement("RuleMessage");
                writer.WriteAttributeString("HotelCode", hotelCode);
                writer.WriteStartElement("StatusApplication");
                writer.WriteAttributeString("RatePlanCode", ratePlanCode);
                writer.WriteAttributeString("Start", startDate.ToString("yyyy-MM-dd"));
                writer.WriteAttributeString("End", endDate.ToString("yyyy-MM-dd"));
                writer.WriteEndElement();//StatusApplication
                writer.WriteEndElement();//RuleMessage
                writer.WriteEndElement();//OTA_HotelBookingRuleRQ
                writer.WriteEndElement();//OTA_HotelBookingRuleService
                writer.WriteEndElement();//Body
                writer.WriteEndElement();//Envelope

                writer.Flush();
                writer.Close();

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelBookingRuleRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                    Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to generate Hotel Descriptive request :" + ex.Message, ex);
            }

            return requestMsg.ToString();
        }

        /// <summary>
        /// Generates response xml from the given request string.
        /// </summary>
        /// <param name="hotelCode"></param>
        /// <param name="ratePlanCode"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private string GenerateHotelBookingRuleResponse(string hotelCode, string ratePlanCode, DateTime startDate, DateTime endDate,string sequenceNumber)
        {
            string responseFromServer = "";
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.LOHConfig["HotelBookingRuleReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelBookingRuleRequest(hotelCode, ratePlanCode, startDate, endDate, sequenceNumber);// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to get response from Hotel Booking Rule Request: " + ex.ToString(), ex);
            }
            
            return responseFromServer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private Dictionary<string, string> ReadCancellationPolicyResponse(string response)
        {
            Dictionary<string, string> CancellationPolicy = new Dictionary<string, string>();

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(response);
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelBookingRuleResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";                    
                    
                    doc.Save(filePath);
                    Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");

                XmlNode root = doc.DocumentElement;
                XmlNodeList RuleMessage = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild.ChildNodes;

                if (RuleMessage[0].Name == "Errors")
                {
                    XmlNode Error = RuleMessage[0].FirstChild;

                    throw new Exception("LOH-Failed to retrieve Hotel Descriptive Info for Hotel :" + Error.Attributes["ShortText"].Value);
                }
                else
                {
                    string cancelPolicy = string.Empty,cancelRule = string.Empty;
                    foreach (XmlNode node in RuleMessage)
                    {
                        switch (node.Name)
                        {
                            case "Warnings":
                                CancellationPolicy.Add(node.FirstChild.FirstChild.Attributes["Code"].Value, node.FirstChild.FirstChild.Attributes["ShortText"].Value);
                                break;
                            case "RuleMessage":
                                foreach (XmlNode rules in node.ChildNodes)
                                {
                                    switch (rules.Name)
                                    {
                                        case "StatusApplication":
                                            CancellationPolicy.Add("RatePlanCode", rules.Attributes["RatePlanCode"].Value);
                                            break;
                                        case "BookingRules":
                                            foreach (XmlNode rule in rules.ChildNodes)
                                            {
                                                switch (rule.Name)
                                                {
                                                    case "BookingRule":
                                                        int buffer = Convert.ToInt32(ConfigurationSystem.LOHConfig["CancellationBuffer"]);
                                                        CancellationPolicy.Add("lastCancellationDate", Convert.ToDateTime(rule.Attributes["AbsoluteCutoff"].Value).AddDays(-buffer).ToString());
                                                        foreach (XmlNode brule in rule.ChildNodes)
                                                        {
                                                            switch (brule.Name)
                                                            {
                                                                case "CancelPenalties":
                                                                    
                                                                    //cancelPolicy = brule.FirstChild.FirstChild.FirstChild.InnerText.Trim();
                                                                    //decimal cancelAmount = 0; string cancelValue = string.Empty;
                                                                    //cancelPolicy = cancelPolicy.TrimStart(new char[] { '*' });
                                                                    //cancelPolicy = cancelPolicy.TrimEnd(new char[] { '$' });
                                                                    //cancelPolicy = cancelPolicy.Replace("$ *", "|");
                                                                    //if (cancelPolicy.Contains(": $"))
                                                                    //{
                                                                    //    cancelValue = cancelPolicy.Substring(cancelPolicy.IndexOf(": $") + 4);
                                                                    //    cancelAmount = Math.Round(Convert.ToDecimal(cancelValue.Replace(",", ".")) * rateOfExchange["USD"], 2);
                                                                    //    cancelPolicy = cancelPolicy.Replace(cancelValue, " " + cancelAmount.ToString());
                                                                    //}
                                                                    //cancelPolicy = cancelPolicy.Replace("$", "AED");
                                                                    break;
                                                                case "Description":
                                                                    CancellationPolicy.Add("HotelPolicy", System.Web.HttpUtility.HtmlDecode(brule.FirstChild.InnerText.Replace(".", "|")));
                                                                    break;
                                                                case "TPA_Extensions":
                                                                    foreach (XmlNode ext in brule.ChildNodes)
                                                                    {
                                                                        switch (ext.Name)
                                                                        {
                                                                            case "CancellationPolicyRules":
                                                                                string type = string.Empty, dateFrom = string.Empty, dateTo = string.Empty;
                                                                                int nights = 0, days =0;
                                                                                decimal price = 0, percentage = 0;
                                                                                foreach (XmlNode crule in ext.ChildNodes)
                                                                                {
                                                                                    foreach (XmlAttribute attrib in crule.Attributes)
                                                                                    {
                                                                                        switch (attrib.Name)
                                                                                        {
                                                                                            case "DateFrom":
                                                                                                dateFrom = attrib.Value;
                                                                                                //cancelRule = "Cancelling from " + Convert.ToDateTime(attrib.Value).ToString("dd/MMM/yyyy");
                                                                                                break;
                                                                                            case "DateTo":
                                                                                                dateTo = attrib.Value;
                                                                                                //cancelRule += " to " + Convert.ToDateTime(attrib.Value).ToString("dd/MMM/yyyy") + " you will have ";
                                                                                                break;
                                                                                            case "Type":
                                                                                                type = attrib.Value;
                                                                                                //switch (type)
                                                                                                //{
                                                                                                //    case "V":                                                                                                        
                                                                                                //        break;
                                                                                                //    case "R":
                                                                                                //        cancelRule = "Cancelling from " + attrib.Value + " days after confirmation you will have ";
                                                                                                //        break;
                                                                                                //    case "S":
                                                                                                //        cancelRule = "Cancelling No Show : ";                                                                                                        
                                                                                                //        break;
                                                                                                //}
                                                                                                break;
                                                                                            case "From":
                                                                                                days = Convert.ToInt32(attrib.Value);
                                                                                                //if (Convert.ToInt32(attrib.Value) > 0)
                                                                                                //{
                                                                                                //    switch (type)
                                                                                                //    {
                                                                                                //        case "V":
                                                                                                //            cancelRule = "Cancelling before " + attrib.Value + " days you will have ";
                                                                                                //            break;
                                                                                                //        case "R":
                                                                                                //            cancelRule = "Cancelling from " + attrib.Value + " days after confirmation you will have ";
                                                                                                //            break;
                                                                                                //        case "S":
                                                                                                //            cancelRule = "No Show incurs into ";
                                                                                                //            break;
                                                                                                //    }
                                                                                                //}
                                                                                                //else
                                                                                                //{
                                                                                                //    switch (type)
                                                                                                //    {
                                                                                                //        case "V":
                                                                                                //            cancelRule = "Cancelling from " + Convert.ToDateTime(dateFrom).ToString("dd/MMM/yyyy") + " to " + Convert.ToDateTime(dateTo).ToString("dd/MMM/yyyy") + " you will have ";
                                                                                                //            break;
                                                                                                //        case "R":
                                                                                                //            cancelRule = "Cancelling from " + attrib.Value + " days after confirmation you will have ";
                                                                                                //            break;
                                                                                                //        case "S":
                                                                                                //            cancelRule = "Cancelling No Show " + nights + "nights";
                                                                                                //            break;
                                                                                                //    }
                                                                                                //}
                                                                                                break;
                                                                                            case "To":
                                                                                                break;
                                                                                            case "StayLengthFrom":
                                                                                                break;
                                                                                            case "StayLengthTo":
                                                                                                break;
                                                                                            case "FixedPrice":
                                                                                                price = Math.Round(Convert.ToDecimal(attrib.Value) * (exchangeRates.ContainsKey("USD") ? exchangeRates["USD"] : 1), decimalPoint);
                                                                                                //cancelRule += "AED " + Math.Round(Convert.ToDecimal(attrib.Value) * rateOfExchange["USD"], 2) + " Penalty";
                                                                                                break;
                                                                                            case "PercentPrice":
                                                                                                percentage = Convert.ToDecimal(attrib.Value);
                                                                                                if (Convert.ToDecimal(attrib.Value) > 0)
                                                                                                {
                                                                                                    //cancelRule += attrib.Value + "% Penalty";
                                                                                                }
                                                                                                break;
                                                                                            case "Nights":
                                                                                                nights = Convert.ToInt32(attrib.Value);
                                                                                                
                                                                                                break;
                                                                                            case "ApplicationTypeNights":
                                                                                                if (attrib.Value.Length > 0)
                                                                                                {
                                                                                                    //cancelRule += attrib.Value + " of the penalty";
                                                                                                }
                                                                                                break;
                                                                                        }
                                                                                    }
                                                                                    if (dateFrom.Length > 0)
                                                                                    {
                                                                                        try
                                                                                        {
                                                                                            dateFrom = Convert.ToDateTime(dateFrom).ToString("dd/MMM/yyyy hh:mm:ss");
                                                                                        }
                                                                                        catch { }
                                                                                    }
                                                                                    if (dateTo.Length > 0)
                                                                                    {
                                                                                        try
                                                                                        {
                                                                                            dateTo = Convert.ToDateTime(dateTo).ToString("dd/MMM/yyyy hh:mm:ss");
                                                                                        }
                                                                                        catch { }
                                                                                    }                                                                                    

                                                                                    if (nights > 0 && price == 0)
                                                                                    {
                                                                                        if (type != "S")
                                                                                        {
                                                                                            cancelRule += "Cancelling after " + dateFrom + " charges applicable for " + nights + " nights";
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            cancelRule += "Cancelling No Show: " + nights + " nights";
                                                                                        }
                                                                                    }
                                                                                    else if (nights <= 0)
                                                                                    {
                                                                                        if (percentage <= 0)
                                                                                        {
                                                                                            if (price <= 0)
                                                                                            {
                                                                                                if (type != "S")
                                                                                                {
                                                                                                    cancelRule += "Cancelling from " + dateFrom + " to " + dateTo +" "+ agentCurrency+" " + price;
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                if (type != "S")
                                                                                                {
                                                                                                    if (dateTo.Length > 0)
                                                                                                    {
                                                                                                        cancelRule += "Cancelling from " + dateFrom + " to " + dateTo + " " + agentCurrency + " " + price;
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        cancelRule += "Cancelling from " + dateFrom + " " + agentCurrency + " " + price;
                                                                                                    }
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    cancelRule = "Cancelling No Show : "+ agentCurrency+" "  + price;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if (type != "S")
                                                                                            {
                                                                                                if (dateTo.Length > 0)
                                                                                                {
                                                                                                    cancelRule += "Cancelling from " + dateFrom + " to " + dateTo + " " + percentage + " %";
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    cancelRule += "Cancelling from " + dateFrom + " " + percentage + " %";
                                                                                                }

                                                                                                if (cancelPolicy.Contains("non-refundable"))
                                                                                                {
                                                                                                    cancelRule += ", This rate is non-refundable";
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                cancelRule += "Cancelling No Show : " + percentage + " %";
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    cancelRule += "|";
                                                                                }
                                                                                cancelPolicy += cancelRule.TrimEnd('|');
                                                                                break;
                                                                            case "RequirePaxName":
                                                                                CancellationPolicy.Add("RequirePaxName", "true");
                                                                                break;
                                                                            case "RequirePaxSurName":
                                                                                CancellationPolicy.Add("RequirePaxSurName", "true");
                                                                                break;
                                                                            case "RequireFullAddress":
                                                                                CancellationPolicy.Add("RequireFullAddress", "true");
                                                                                break;
                                                                            case "RequirePostalCode":
                                                                                CancellationPolicy.Add("RequirePostalCode", "true");
                                                                                break;
                                                                            case "RequireCityCode":
                                                                                CancellationPolicy.Add("RequireCityCode", "true");
                                                                                break;
                                                                            case "RequireCountryCode":
                                                                                CancellationPolicy.Add("RequireCountryCode", "true");
                                                                                break;
                                                                            case "RequireTelephoneHolder":
                                                                                CancellationPolicy.Add("RequireTelephoneHolder", "true");
                                                                                break;
                                                                        }
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                        break;
                                                }
                                            }
                                            break;
                                    }
                                }
                                break;
                            
                        }
                    }

                    //if (cancelRule.Length > 0)
                    //{
                    //    CancellationPolicy.Add("CancelPolicy", cancelRule);
                    //}
                    //else
                    {
                        CancellationPolicy.Add("CancelPolicy", cancelPolicy);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to Read the Hotel Booking Rule Response : " + ex.Message, ex);
            }

            return CancellationPolicy;
        }
        #endregion

        #region Book Hotel Reservation

        /// <summary>
        /// Used to Book a Hotel using HotelItinerary.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public BookingResponse BookHotel(ref HotelItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();

            try
            {
                string responseXML = GenerateHotelBookingResponse(itinerary);

                bookResponse = ReadHotelBookingResponse(responseXML, ref itinerary);
            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to Book Hotel : " + ex.Message, ex);
            }

            return bookResponse;
        }

        /// <summary>
        /// Generates Hotel Booking Request XML.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="sequenceNumber"></param>
        /// <returns></returns>
        private string GenerateHotelBookingRequest(HotelItinerary itinerary)
        {
           StringBuilder requestMsg = new StringBuilder();

           try
           {
               XmlWriter writer = XmlWriter.Create(requestMsg);
               writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
               writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
               writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
               writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
               writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

               writer.WriteStartElement("soap", "Body", null);
               writer.WriteStartElement("OTA_HotelResV2Service", "http://www.opentravel.org/OTA/2003/05");
               writer.WriteStartElement("OTA_HotelResRQ");
               writer.WriteAttributeString("PrimaryLangID", "EN");
               writer.WriteAttributeString("SequenceNmbr", itinerary.SequenceNumber);
               writer.WriteStartElement("POS");
               writer.WriteStartElement("Source");
               writer.WriteAttributeString("AgentDutyCode", agentCode);
               writer.WriteStartElement("RequestorID");
               writer.WriteAttributeString("MessagePassword", password);
               writer.WriteEndElement();//RequestorID
               writer.WriteEndElement();//Source
               writer.WriteEndElement();//POS
               writer.WriteStartElement("HotelReservations");
               writer.WriteStartElement("HotelReservation");
               writer.WriteStartElement("UniqueID");
               writer.WriteAttributeString("ID_Context", string.Empty);//Optional Reservation ID
               writer.WriteEndElement();//UniqueID
               writer.WriteStartElement("RoomStays");
               writer.WriteStartElement("RoomStay");
               writer.WriteStartElement("RoomTypes");

               foreach (HotelRoom room in itinerary.Roomtype)
               {
                   writer.WriteStartElement("RoomType");
                   writer.WriteStartElement("TPA_Extensions");
                   writer.WriteStartElement("Guests");
                   foreach (HotelPassenger pax in room.PassenegerInfo)
                   {
                       writer.WriteStartElement("Guest");
                       writer.WriteAttributeString("Name", pax.Firstname);
                       writer.WriteAttributeString("Surname", pax.Lastname);
                       if (pax.PaxType == HotelPaxType.Child)
                       {
                           writer.WriteAttributeString("Age", pax.Age.ToString());
                       }
                       writer.WriteEndElement();//Guest
                   }
                   writer.WriteEndElement();//Guests
                   writer.WriteEndElement();//TPA_Extensions
                   writer.WriteEndElement();//RoomType                   
               }
               writer.WriteEndElement();//RoomTypes
               writer.WriteStartElement("RatePlans");
               //foreach (HotelRoom room in itinerary.Roomtype)
               {
                   writer.WriteStartElement("RatePlan");
                   writer.WriteAttributeString("RatePlanCode", itinerary.Roomtype[0].RatePlanCode);
                   writer.WriteEndElement();//RatePlan                   
               }
               writer.WriteEndElement();//RatePlans

               writer.WriteStartElement("TimeSpan");
               writer.WriteAttributeString("Start", itinerary.StartDate.ToString("yyyy-MM-dd"));
               writer.WriteAttributeString("End", itinerary.EndDate.ToString("yyyy-MM-dd"));
               writer.WriteEndElement();//TimeSpan
               writer.WriteStartElement("Total");
               writer.WriteAttributeString("AmountAfterTax", itinerary.TotalPrice.ToString("0.00"));
               writer.WriteAttributeString("CurrencyCode", "USD");
               writer.WriteEndElement();//Total
               writer.WriteStartElement("BasicPropertyInfo");
               writer.WriteAttributeString("HotelCode", itinerary.HotelCode);
               writer.WriteEndElement();//BasicPropertyInfo
               writer.WriteStartElement("TPA_Extensions");
               writer.WriteStartElement("ExpectedPriceRange");
               writer.WriteAttributeString("min", "0");
               writer.WriteAttributeString("max", (itinerary.TotalPrice).ToString("0.00"));
               writer.WriteEndElement();//ExpectedPriceRange
               writer.WriteEndElement();//TPA_Extensions
               writer.WriteStartElement("Comments");
               writer.WriteStartElement("Comment");
               writer.WriteElementString("Text", string.Empty);
               writer.WriteEndElement();//Comment
               writer.WriteEndElement();//Comments
               writer.WriteStartElement("Reference");
               writer.WriteEndElement();//Reference
               writer.WriteEndElement();//RoomStay
               writer.WriteEndElement();//RoomStays               
               writer.WriteStartElement("ResGuests");//Lead Pax Information
               writer.WriteStartElement("ResGuest");
               writer.WriteStartElement("Profiles");
               writer.WriteStartElement("ProfileInfo");
               writer.WriteStartElement("Profile");
               writer.WriteAttributeString("ProfileType", "1");
               writer.WriteStartElement("Customer");
               writer.WriteStartElement("PersonName");
               writer.WriteElementString("GivenName", itinerary.HotelPassenger.Firstname);
               writer.WriteElementString("Surname", itinerary.HotelPassenger.Lastname);
               writer.WriteEndElement();//PersonName
               writer.WriteStartElement("Telephone");
               writer.WriteAttributeString("PhoneNumber", itinerary.HotelPassenger.Phoneno);
               writer.WriteEndElement();//Telephone
               writer.WriteElementString("Email", itinerary.HotelPassenger.Email);
               writer.WriteStartElement("Address");
               if (itinerary.HotelPassenger.Addressline1.Length > 0)
               {
                   writer.WriteElementString("AddressLine", itinerary.HotelPassenger.Addressline1);
               }
               if (itinerary.HotelPassenger.Zipcode.Length > 0)
               {
                   writer.WriteElementString("PostalCode", itinerary.HotelPassenger.Zipcode);
               }
               if (itinerary.HotelPassenger.City.Length > 0)
               {
                   writer.WriteElementString("CityName", itinerary.HotelPassenger.City);
               }               

               writer.WriteStartElement("CountryName");
               writer.WriteAttributeString("Code", itinerary.HotelPassenger.NationalityCode);
               writer.WriteString(itinerary.HotelPassenger.Nationality);
               writer.WriteEndElement();//Country
               writer.WriteEndElement();//Address
               writer.WriteEndElement();//Customer
               writer.WriteEndElement();//Profile
               writer.WriteEndElement();//ProfileInfo
               writer.WriteEndElement();//Profiles
               writer.WriteEndElement();//ResGuest
               writer.WriteEndElement();//ResGuests
               writer.WriteStartElement("TPA_Extensions");
               writer.WriteElementString("PaxCountry", itinerary.HotelPassenger.NationalityCode);
               writer.WriteEndElement();//TPA_Extensions
               writer.WriteEndElement();//HotelReservation
               writer.WriteEndElement();//HotelReservations
               writer.WriteEndElement();//OTA_HotelResRQ
               writer.WriteEndElement();//OTA_HotelResV2Service
               writer.WriteEndElement();//soap:Body
               writer.WriteEndElement();//soap:Envelope

               writer.Flush();
               writer.Close();

               if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
               {
                   string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelBookingRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                   XmlDocument doc = new XmlDocument();
                   doc.LoadXml(requestMsg.ToString());
                   doc.Save(filePath);
                   Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
               }
           }
           catch (Exception ex)
           {
               throw new Exception("LOH-Failed to generate Hotel Booking Request : " + ex.Message, ex);
           }

           return requestMsg.ToString();
        }

        /// <summary>
        /// Generates Hotel Booking Response XML.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="sequenceNumber"></param>
        /// <returns></returns>
        private string GenerateHotelBookingResponse(HotelItinerary itinerary)
        {
            string responseFromServer = "";
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.LOHConfig["HotelReservationReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelBookingRequest(itinerary);// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to get response from Hotel Search request : " + ex.Message, ex);
            }
            return responseFromServer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private BookingResponse ReadHotelBookingResponse(string response, ref HotelItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(response);
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelBookingResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";                    
                    
                    doc.Save(filePath);
                    Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
                //nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
                //nsmgr.AddNamespace(string.Empty, "");

                XmlNode root = doc.DocumentElement;
                XmlNodeList HotelRes = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild.ChildNodes;

                if (HotelRes[0].Name == "Errors")
                {
                    bookResponse.Status = BookingResponseStatus.Failed;
                    bookResponse.Error = HotelRes[0].FirstChild.Attributes["ShortText"].Value;
                }
                else
                {
                    foreach (XmlNode node in HotelRes)
                    {
                        switch (node.Name)
                        {
                            case "Success":
                                break;
                            case "HotelReservations":
                                foreach (XmlNode resNode in node.ChildNodes)
                                {
                                    switch (resNode.Name)
                                    {
                                        case "HotelReservation":
                                            switch (resNode.Attributes["ResStatus"].Value)
                                            {
                                                case "Pag":
                                                case "Ok":
                                                    bookResponse.Status = BookingResponseStatus.Successful;//Confirmed and Paid
                                                    itinerary.Status = HotelBookingStatus.Confirmed;
                                                    break;
                                                case "Con":
                                                case "Con*":
                                                    bookResponse.Status =  BookingResponseStatus.Successful;//Confirmed
                                                    itinerary.Status = HotelBookingStatus.Confirmed;
                                                    break;
                                                case "Tar"://Credit Card Reservation
                                                    break;
                                                case "Can":
                                                case "CanC":
                                                    bookResponse.Status = BookingResponseStatus.Failed;
                                                    itinerary.Status = HotelBookingStatus.Failed;
                                                    bookResponse.Error ="Cancelled From Supplier";
                                                    bookResponse.SSRMessage = "Cancelled From Supplier";
                                                    break;
                                                case "PRe":
                                                case "PDi":
                                                    bookResponse.Status = BookingResponseStatus.BookedOther;//OnRequest Reservation
                                                    bookResponse.Error = "Not Confirmed.Reservation ON request";
                                                    bookResponse.SSRMessage = "Not Confirmed.Reservation ON request";
                                                    break;
                                                case "Ini":
                                                    bookResponse.Status = BookingResponseStatus.Failed;//Initialized but not confirmed
                                                    itinerary.Status = HotelBookingStatus.Failed;
                                                    bookResponse.Error = "Initialized but not confirmed From supplier";
                                                    bookResponse.SSRMessage = "Initialized but not confirmed From supplier";
                                                    break;
                                            }
                                            if (resNode.FirstChild.Attributes["ID"] != null)
                                            {
                                                bookResponse.ConfirmationNo = resNode.FirstChild.Attributes["ID"].Value;
                                                itinerary.ConfirmationNo = bookResponse.ConfirmationNo;
                                            }
                                            foreach (XmlNode childs in resNode.ChildNodes)
                                            {
                                                switch (childs.Name)
                                                {
                                                    case "RoomStays":
                                                        foreach (XmlNode roomStay in childs.ChildNodes)
                                                        {
                                                            foreach (XmlNode rsChilds in roomStay.FirstChild.ChildNodes)
                                                            {
                                                                switch (rsChilds.Name)
                                                                {
                                                                    case "BasicPropertyInfo":
                                                                        foreach (XmlNode bpChilds in rsChilds.ChildNodes)
                                                                        {
                                                                            switch (bpChilds.Name)
                                                                            {
                                                                                case "VendorMessages":
                                                                                    if (bpChilds.FirstChild.Attributes["Title"] != null)
                                                                                    {
                                                                                        itinerary.HotelPolicyDetails = "<b>" + bpChilds.FirstChild.Attributes["Title"].Value + "</b>";                                                                                        
                                                                                    }
                                                                                    itinerary.HotelPolicyDetails += System.Web.HttpUtility.HtmlDecode(bpChilds.FirstChild.FirstChild.FirstChild.FirstChild.InnerText);
                                                                                    break;
                                                                            }
                                                                        }
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    case "ResGuests":
                                                        break;
                                                    case "ResGlobalInfo":
                                                        foreach (XmlNode rgiNode in childs.ChildNodes)
                                                        {
                                                            switch (rgiNode.Name)
                                                            {
                                                                case "Total":
                                                                    break;
                                                                case "HotelReservationIDs":
                                                                    //bookResponse.ConfirmationNo += "|" + rgiNode.FirstChild.Attributes["ResID_Source"].Value;
                                                                    if (rgiNode.FirstChild.Attributes["ResID_Source"] != null)
                                                                    {
                                                                        itinerary.BookingRefNo += rgiNode.FirstChild.Attributes["ResID_Source"].Value;
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                        break;
                                                    case "TPA_Extensions":
                                                        foreach (XmlNode tpaNode in childs.ChildNodes)
                                                        {
                                                            switch (tpaNode.Name)
                                                            {
                                                                case "ElementStatus":
                                                                    break;
                                                                case "CancellationPolicy":
                                                                    break;
                                                                case "ExternalReference":
                                                                    break;
                                                                case "Payable":
                                                                    itinerary.PaymentGuaranteedBy = "Payment Guaranteed by : " + System.Web.HttpUtility.HtmlDecode(tpaNode.InnerText);
                                                                    break;
                                                                case "FirstDateCancCost":
                                                                    break;
                                                                case "Delegation"://Point of Contact for Hotel
                                                                    string POC = string.Empty;
                                                                    foreach (XmlNode delNode in tpaNode.ChildNodes)
                                                                    {
                                                                        if (POC.Length > 0)
                                                                        {
                                                                            POC += delNode.Name + ":" + delNode.InnerText;
                                                                        }
                                                                        else
                                                                        {
                                                                            POC = delNode.Name + ":" + delNode.InnerText;
                                                                        }
                                                                    }
                                                                    itinerary.SpecialRequest = POC;
                                                                    break;
                                                            }
                                                        }
                                                        break;
                                                }
                                            }
                                            break;
                                        
                                    }
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to read Hotel Booking response : " + ex.Message, ex);
            }

            return bookResponse;
        }
        #endregion
        
        #region Hotel Cancellation

        /// <summary>
        /// 
        /// </summary>
        /// <param name="confirmationNumber"></param>
        /// <returns></returns>
        public Dictionary<string, string> CancelHotel(string confirmationNumber)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            try
            {
                string responseXML = GenerateHotelCancellationResponse(confirmationNumber);

                cancellationCharges = ReadHotelCancellationResponse(responseXML);
            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to Cancel Hotel : " + ex.Message, ex);
            }

            return cancellationCharges;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="confirmationNo"></param>
        /// <returns></returns>
        private string GenerateHotelCancellationRequest(string confirmationNo)
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("OTA_CancelService", "http://www.opentravel.org/OTA/2003/05");
                writer.WriteStartElement("OTA_CancelRQ");
                writer.WriteAttributeString("PrimaryLangID", "EN");
                writer.WriteStartElement("POS");
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("AgentDutyCode", agentCode);
                writer.WriteStartElement("RequestorID");
                writer.WriteAttributeString("Type", "1");
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteEndElement();//RequestorID
                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS

                
                writer.WriteStartElement("UniqueID");
                writer.WriteAttributeString("ID", confirmationNo);
                writer.WriteEndElement();//UniqueID
                writer.WriteEndElement();//OTA_CancelRQ
                writer.WriteEndElement();//OTA_CancellationService
                writer.WriteEndElement();//Body
                writer.WriteEndElement();//Envelope

                writer.Flush();
                writer.Close();

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelCancelRequest_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                    Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to generate Hotel Cancellation request :" + ex.Message, ex);
            }

            return requestMsg.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="confirmationNo"></param>
        /// <returns></returns>
        private string GenerateHotelCancellationResponse(string confirmationNo)
        {
            string responseFromServer = "";
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.LOHConfig["HotelCancellationReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateHotelCancellationRequest(confirmationNo);// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to get response from Hotel Descriptive Info request: " + ex.ToString(), ex);
            }
            return responseFromServer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private Dictionary<string, string> ReadHotelCancellationResponse(string response)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            try
            {
                XmlDocument doc = new XmlDocument();

                doc.LoadXml(response);
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelCancelResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";                    
                    doc.Save(filePath);
                    Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");

                XmlNode root = doc.DocumentElement;
                XmlNodeList CancelRS = root.SelectSingleNode("descendant::soap:Body", nsmgr).FirstChild.FirstChild.ChildNodes;

                foreach (XmlNode node in CancelRS)
                {
                    switch (node.Name)
                    {
                        case "Errors":
                            cancellationCharges.Add("Status", "Failed");
                            cancellationCharges.Add("Description", node.FirstChild.Attributes["ShortText"].Value);
                            break;
                        case "UniqueID":
                            break;
                        case "Warnings":
                            //cancellationCharges.Add("Amount", (Convert.ToDecimal(node.FirstChild.InnerText.Split(' ')[0]) * (exchangeRates.ContainsKey(node.FirstChild.InnerText.Split(' ')[1]) ? exchangeRates[node.FirstChild.InnerText.Split(' ')[1]] : 1)).ToString());
                            //cancellationCharges.Add("Currency", agentCurrency);
                            cancellationCharges.Add("Amount", node.FirstChild.InnerText.Split(' ')[0]);
                            cancellationCharges.Add("Currency", node.FirstChild.InnerText.Split(' ')[1]);
                            cancellationCharges.Add("Description", node.FirstChild.Attributes["ShortText"].Value);
                            if (node.FirstChild.Attributes["ShortText"].Value.Contains("Reservation was cancelled"))
                            {
                                cancellationCharges.Add("Status", "Cancelled");
                            }
                            else
                            {
                                cancellationCharges.Add("Status", "Failed");
                            }
                            break;
                        case "Success":
                            break;
                    }
                }                
            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to read Hotel Cancellation response : " + ex.Message, ex);
            }
            return cancellationCharges;
        }
        #endregion

        #region Download ZoneList

        public void GetZones()
        {
            string responseXML = GenerateZoneListResponse();

            if (responseXML.Length > 0)
            {
                string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelZoneListResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(responseXML);
                doc.Save(filePath);
            }
        }

        private string GenerateZoneListRequest()
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("JP_ZoneListService", "http://www.juniper.es/webservice/2007/");
                writer.WriteStartElement("JP_ZoneListRQ");
                writer.WriteAttributeString("PrimaryLangID", "EN");
                writer.WriteStartElement("POS");
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("AgentDutyCode", agentCode);
                writer.WriteStartElement("RequestorID");
                writer.WriteAttributeString("Type", "1");
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteEndElement();//RequestorID
                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS


                writer.WriteStartElement("TPA_Extensions");
                writer.WriteElementString("ShowHotels", "", "1");
                writer.WriteEndElement();//TPA_Extensions
                writer.WriteEndElement();//JP_ZoneListRQ
                writer.WriteEndElement();//JP_ZoneListService
                writer.WriteEndElement();//Body
                writer.WriteEndElement();//Envelope

                writer.Flush();
                writer.Close();

                //if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelZoneListRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                   // Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to generate Zone List request :" + ex.Message, ex);
            }

            return requestMsg.ToString();
        }

        private string GenerateZoneListResponse()
        {
            string responseFromServer = "";
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.LOHConfig["ZoneListReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateZoneListRequest();// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to get response from Hotel Descriptive Info request: " + ex.ToString(), ex);
            }
            return responseFromServer;
        }
        #endregion

        public void GetRoomTypeList()
        {
            string responseXML = GenerateRoomTypeListResponse();

            if (responseXML.Length > 0)
            {
                string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelRoomTypeListResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(responseXML);
                doc.Save(filePath);
            }
        }

        private string GenerateRoomTypeListRequest()
        {
            StringBuilder requestMsg = new StringBuilder();

            try
            {
                XmlWriter writer = XmlWriter.Create(requestMsg);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("JP_RoomTypeListService", "http://www.juniper.es/webservice/2007/");
                writer.WriteStartElement("JP_RoomTypeListRQ");
                writer.WriteAttributeString("PrimaryLangID", "EN");
                writer.WriteStartElement("POS");
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("AgentDutyCode", agentCode);
                writer.WriteStartElement("RequestorID");
                writer.WriteAttributeString("Type", "1");
                writer.WriteAttributeString("MessagePassword", password);
                writer.WriteEndElement();//RequestorID
                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS
                writer.WriteEndElement();//JP_ZoneListRQ
                writer.WriteEndElement();//JP_ZoneListService
                writer.WriteEndElement();//Body
                writer.WriteEndElement();//Envelope

                writer.Flush();
                writer.Close();

                //if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    string filePath = @"" + ConfigurationSystem.LOHConfig["XmlLogPath"] + "HotelRoomTypeListRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestMsg.ToString());
                    doc.Save(filePath);
                    // Audit.Add(EventType.HotelSearch, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }

            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to generate Zone List request :" + ex.Message, ex);
            }

            return requestMsg.ToString();
        }

        private string GenerateRoomTypeListResponse()
        {
            string responseFromServer = "";
            try
            {
                string contentType = "text/xml";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.LOHConfig["RoomTypeListReq"]);
                request.Method = "POST"; //Using POST method       
                string postData = GenerateRoomTypeListRequest();// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("LOH-Failed to get response from Hotel Descriptive Info request: " + ex.ToString(), ex);
            }
            return responseFromServer;
        }


    }

   
}
