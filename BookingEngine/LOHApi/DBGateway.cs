using System;
using System.Data;
using System.Data.SqlClient;


namespace CT.TicketReceipt.DataAccessLayer
{
    public class DBGateway1
    {
        private DBGateway1()
        {
        }
        # region VMS
        public static object ExecuteScalar(string commandText)
        {
            SqlCommand cmd = null;
            //SqlTransaction transaction=null;
            try
            {
                cmd = CreateCommand();
                cmd.CommandText = commandText;
                cmd.CommandType = CommandType.Text;
               // transaction = cmd.Connection.BeginTransaction();
                return (cmd.ExecuteScalar());
               //Transaction().Commit();
            }
            catch (Exception ex)
            {
                Transaction().Rollback();
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                //CloseConnection(cmd);
            }
        }

        private static DataTable FillDataTable(string procedureName, SqlParameter[] paramArr, CommandType cmdType)
        {
            SqlCommand cmd = null;
            // SqlTransaction transaction = null;
            try
            {
                cmd = CreateCommand();
                cmd.CommandText = procedureName;
                cmd.CommandType = cmdType;
                if (paramArr != null)
                {
                    for (int i = 0; i < paramArr.Length; i++)
                    {
                        if (paramArr[i] != null)
                        {
                            cmd.Parameters.Add(paramArr[i]);
                        }
                    }
                }
                // transaction = cmd.Connection.BeginTransaction();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);

                //DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                adp.Fill(dt);
                cmd.Connection.Close();
                //if (Transaction() != null) Transaction().Commit();
                return (dt);
            }
            catch
            {
                Transaction().Rollback();
                throw;
            }
            finally
            {
                //CloseConnection(cmd);
                cmd.Connection.Close();
            }
        }

        public static DataTable  FillDataTableSP(string procedureName, SqlParameter[] paramArr)
        {
            return FillDataTable(procedureName, paramArr, CommandType.StoredProcedure);
        }
        public static DataSet ExecuteQuery(string procedureName, SqlParameter[] paramArr, CommandType cmdType)
        {
            SqlCommand cmd = null;
           // SqlTransaction transaction = null;
            try
            {
                cmd = CreateCommand();
                cmd.CommandText = procedureName;
                cmd.CommandType = cmdType;
                if (paramArr != null)
                {
                    for (int i = 0; i < paramArr.Length; i++)
                    {
                        if (paramArr[i] != null)
                        {
                            cmd.Parameters.Add(paramArr[i]);
                        }
                    }
                }
               // transaction = cmd.Connection.BeginTransaction();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                
                DataSet ds = new DataSet();
                adp.Fill(ds);
                cmd.Connection.Close();
                //if (Transaction() != null) Transaction().Commit();
                return (ds);
            }
            catch 
            {
                Transaction().Rollback();
                throw;
            }
            finally
            {
                //CloseConnection(cmd);
                cmd.Connection.Close();
            }
        }

        public static DataSet ExecuteQuery(string query)
        {
            SqlCommand cmd = null;
            // SqlTransaction transaction = null;
            try
            {
                cmd = CreateCommand();
                cmd.CommandText = query;


                // transaction = cmd.Connection.BeginTransaction();
                SqlDataAdapter adp = new SqlDataAdapter(cmd);

                DataSet ds = new DataSet();
                adp.Fill(ds);

                return (ds);
            }
            catch
            {

                throw;
            }
            finally
            {
                // CloseConnection(cmd);
                cmd.Connection.Close();
            }
        }
        public static DataSet ExecuteQuery(string procedureName, SqlParameter[] paramArr)
        {
            return ExecuteQuery(procedureName, paramArr, CommandType.StoredProcedure);
        }
        // For Bookig Engine
        public static DataSet FillSP(String procedureName, SqlParameter[] paramArr)
        {
            return ExecuteQuery(procedureName, paramArr, CommandType.StoredProcedure);
        }


        public static int ExecuteNonQuerySP(string procedureName, SqlParameter[] paramArr)
        {

            return ExecuteNonQuery(procedureName, paramArr, CommandType.StoredProcedure);
        }
        public static int ExecuteNonQuery(string procedureName, SqlParameter[] paramArr)
        {

            return ExecuteNonQuery(procedureName, paramArr, CommandType.StoredProcedure);
        }

        private static int ExecuteNonQuery(string procedureName, SqlParameter[] paramArr, CommandType cmdType)
        {
            SqlCommand cmd = null;
            //SqlTransaction transaction = null;
            try
            {
                cmd = CreateCommand();
                cmd.CommandText = procedureName;
                cmd.CommandType = cmdType;
                if (paramArr != null)
                {
                    for (int i = 0; i < paramArr.Length; i++)
                    {
                        if (paramArr[i] != null)
                        {
                            cmd.Parameters.Add(paramArr[i]);
                        }
                    }
                }
               // transaction = cmd.Connection.BeginTransaction();
                
               int count= cmd.ExecuteNonQuery();
              // Transaction().Commit();
                return (count);
            }
            catch  
            {
                Transaction().Rollback();
                cmd.Connection.Close();
                throw;
            }
            finally
            {
                cmd.Connection.Close();
                //CloseConnection(cmd);
            }

        }
        public static int ExecuteNonQueryDetails(SqlCommand cmd,string procedureName, SqlParameter[] paramArr)
        {
            //SqlCommand cmd = null;
            //SqlTransaction transaction = null;
            try
            {
               // cmd = CreateCommand();
                cmd.CommandText = procedureName;
                cmd.CommandType = CommandType.StoredProcedure;
                if (paramArr != null)
                {
                    cmd.Parameters.Clear();
                    for (int i = 0; i < paramArr.Length; i++)
                    {
                        if (paramArr[i] != null)
                        {
                            cmd.Parameters.Add(paramArr[i]);
                        }
                    }
                }
                // transaction = cmd.Connection.BeginTransaction();

                int count = cmd.ExecuteNonQuery();
               // Transaction().Commit();
                return (count);
            }
            catch
            {
                Transaction().Rollback();
                throw;
            }
            finally
            {
                //cmd.Connection.Close();
                //CloseConnection(cmd);
            }

        }

        public static void CloseConnection(SqlCommand cmd)
        {
            try
            {
                if (cmd != null && cmd.Connection != null && cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
            }
            catch { throw; }
        }

        public static SqlTransaction Transaction()
        {
            try
            {
                SqlTransaction transaction;
                SqlConnection con = CreateCommand().Connection;
                if (con.State == ConnectionState.Open)
                {
                    transaction = con.BeginTransaction();
                }
                else transaction = null;
                return (transaction);
            }
            catch { throw; }
        }
        //public static SqlCommand CreateCommand()
        //{
        //    try
        //    {
        //        SqlConnection con = GetCurrentConnection();
        //        if (con.State == ConnectionState.Closed) con.Open();
        //        return (con.CreateCommand());
        //    }
        //    catch { throw; }
        //}

        public static SqlCommand CreateCommand()
        {
            try
            {
                SqlConnection con = GetCurrentConnection();
                if (con.State == ConnectionState.Closed) con.Open();
                return (con.CreateCommand());
            }
            catch(Exception ex)
            {
                //CT.TicketReceipt.Common.Utility.WriteLog(ex.Message, "test ");
                //CT.TicketReceipt.Common.Utility.WriteLog(ex.InnerException, "");
                throw ex;
            }
        }
        public static SqlCommand CreateCommand(SqlConnection con)
        {
            try
            {
                con = GetCurrentConnection();
                if (con.State == ConnectionState.Closed) con.Open();
                return (con.CreateCommand());
            }
            catch { throw; }
        }
        public static SqlConnection CreateConnection()
        {
            try
            {
                SqlConnection con = new SqlConnection(ConnectionString);
                return (con);
            }
            catch { throw; }
        }
        private static SqlConnection GetCurrentConnection()
        {

            SqlConnection con = null;
            con = new SqlConnection(ConnectionString);
            return (con);
        }
        private static string ConnectionString
        {
            get
            {
                string value = System.Configuration.ConfigurationManager.AppSettings["dbSetting"];
                if (value == null) value = string.Empty;
                return (value);
            }
        }
        # endregion

        # region Booking Engine
        /// <summary>
        /// Executes stored procedure or queries with sql statement using SqlCommand. Only select statements.
        /// </summary>
        /// <param name="sproc">Name of stored procedure or sql statement</param>
        /// <param name="paramList">List of SqlParameters</param>
        /// <param name="connection">Connection to SQL database</param>
        /// <param name="cmdType">Type of command. indicates if the query given is stored procedure or a sql statement</param>
        /// <returns>SqlDataReader</returns>
        //private static SqlDataReader ExecuteReader(String sproc, SqlParameter[] paramList, SqlConnection connection, CommandType cmdType)
        //{
        //    try
        //    {
               
        //        SqlCommand command = new SqlCommand(sproc, connection);
        //        command.CommandType = cmdType;
        //        command.CommandTimeout = 60;
        //        command.Parameters.AddRange(paramList);
        //        string detail = string.Empty;
               
        //        DateTime timeBeforeQuery = DateTime.UtcNow;
        //        SqlDataReader dataReader = command.ExecuteReader();
        //        DateTime timeAfterQuery = DateTime.UtcNow;
        //        //if (isAudit && (timeAfterQuery.Subtract(timeBeforeQuery) > new TimeSpan(0, 0, minQuerySecondsForLogging)))
        //        //{
        //        //    System.IO.File.AppendAllText(filePathForLogging, detail);
        //        //}
        //        return dataReader;
        //    }
        //    catch (SqlException sqEx)
        //    {
        //        connection.Close();
        //        throw new DALException("Query error occured", sqEx);
        //    }
        //}

        private static SqlDataReader ExecuteReader(string procedureName, SqlParameter[] paramArr, CommandType cmdType,SqlConnection connection)
        {
            SqlCommand cmd = null;
            try
            {
                
                //cmd = CreateCommand();
                cmd= new SqlCommand(procedureName, connection);
                //cmd.CommandText = procedureName;
                cmd.CommandType = cmdType;
                if (paramArr != null)
                {
                    for (int i = 0; i < paramArr.Length; i++)
                    {
                        if (paramArr[i] != null)
                        {
                            cmd.Parameters.Add(paramArr[i]);
                        }
                    }
                }
                SqlDataReader dataReader = cmd.ExecuteReader();
                return dataReader;

                


                //SqlCommand command = new SqlCommand(sproc, connection);
                //command.CommandType = cmdType;
                //command.CommandTimeout = 60;
                //command.Parameters.AddRange(paramList);
                //string detail = string.Empty;

                //DateTime timeBeforeQuery = DateTime.UtcNow;
                //SqlDataReader dataReader = command.ExecuteReader();
                //DateTime timeAfterQuery = DateTime.UtcNow;
                ////if (isAudit && (timeAfterQuery.Subtract(timeBeforeQuery) > new TimeSpan(0, 0, minQuerySecondsForLogging)))
                ////{
                ////    System.IO.File.AppendAllText(filePathForLogging, detail);
                ////}
                //return dataReader;
            }
            catch (SqlException sqEx)
            {
                cmd.Connection.Close();
                throw new Exception("Query error occured", sqEx);
            }
            catch (Exception  Ex)
            {
                cmd.Connection.Close();
                throw Ex;
                //throw new DALException("Query error occured", Ex);
            }
            finally
            {
                //cmd.Connection.Close();
                //CloseConnection(cmd);
            }
        }
        /// <summary>
        /// Makes query with a sql statement using SqlCommand. Only Select statements.
        /// </summary>
        /// <param name="query">Select statement</param>
        /// <param name="paramList">List of SqlParameters used in query</param>
        /// <param name="connection">Connection to SQL database</param>
        /// <returns>SqlDataReader</returns>
        /// <remarks>It need open sql connection as an arguement. The connection needs to be closed by callind function after reading data</remarks>
        public static SqlDataReader ExecuteReader(String query, SqlParameter[] paramList,SqlConnection connection)
        {
            return ExecuteReader(query, paramList,  CommandType.Text,connection);
        }

        /// <summary>
        /// Executes stored procedure using SqlCommand. Only Select statements
        /// </summary>
        /// <param name="sproc">Name of stored Procedure</param>
        /// <param name="paramList">List of SqlParameter</param>
        /// <param name="connection">Connection to SQL database</param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader ExecuteReaderSP(String sproc, SqlParameter[] paramList, SqlConnection connection)
        {
            return ExecuteReader(sproc, paramList, CommandType.StoredProcedure,connection);
        }
        /// <summary>
        /// Executes stored procedure or queries with sql statement using SqlCommand. Only select statements.
        /// </summary>
        /// <param name="sproc">Name of stored procedure or sql statement</param>
        /// <param name="paramList">List of SqlParameters</param>
        /// <param name="connection">Connection to SQL database</param>
        /// <param name="cmdType">Type of command. indicates if the query given is stored procedure or a sql statement</param>
        /// <returns>SqlDataReader</returns>
        /// 
        //public static SqlDataReader ExecuteReader(String sproc, SqlParameter[] paramList, SqlConnection connection)
        //{
        //    return ExecuteReader(sproc, paramList, connection, CommandType.StoredProcedure);

        //}
        private static SqlDataReader ExecuteReader(String sproc, SqlParameter[] paramList, SqlConnection connection, CommandType cmdType)
        {
            try
            {
                //Trace.TraceInformation("Dal.ExecuteReader entered : query = " + sproc + ", CommandType = " + cmdType.ToString());
                SqlCommand command = new SqlCommand(sproc, connection);
                command.CommandType = cmdType;
                command.CommandTimeout = 60;
                command.Parameters.AddRange(paramList);
                SqlDataReader dataReader = command.ExecuteReader();
                //string detail = string.Empty;
                //if (isAudit)
                //{
                //    detail = sproc;
                //    foreach (SqlParameter param in paramList)
                //    {
                //        detail += ":" + param.ParameterName + ":" + param.Value;
                //    }
                //    detail += DateTime.Now.ToUniversalTime().ToString("dd/MM/yyyy hh:mm:ss tt");
                //}
                //DateTime timeBeforeQuery = DateTime.UtcNow;
                //SqlDataReader dataReader = command.ExecuteReader();
                //DateTime timeAfterQuery = DateTime.UtcNow;
                //if (isAudit && (timeAfterQuery.Subtract(timeBeforeQuery) > new TimeSpan(0, 0, minQuerySecondsForLogging)))
                //{
                //    System.IO.File.AppendAllText(filePathForLogging, detail);
                //}
//                Trace.TraceInformation("Dal.ExecuteReader exited : DataReader has rows = " + dataReader.HasRows.ToString());
                return dataReader;
            }
            catch (SqlException sqEx)
            {
                connection.Close();
                throw new Exception("Query error occured", sqEx);
            }
        }
        /// <summary>
        /// To return SqlConnection of given connection string
        /// </summary>
        /// <param name="connectionString">Connection String</param>
        /// <returns>SqlConnection of any given connection string</returns>
        private static SqlConnection GetConnection(string connectionString)
        {
            try
            {
                // Trace.TraceInformation("Dal.GetConnection entered");
                SqlConnection connection = new SqlConnection(connectionString);
                //Trace.TraceInformation("Dal.GetConnection connection created");
                connection.Open();
                // Trace.TraceInformation("Dal.GetConnection exiting : Connection state = " + connection.State.ToString());
                return connection;
            }
            catch 
            {
                throw;
            }
        }
        /// <summary>
        /// Opens database connection
        /// </summary>
        public static SqlConnection GetConnection()
        {
            try
            {
                return GetConnection(ConnectionString);
            }
            catch (SqlException sqEx)
            {
                throw new Exception("Error in creating connection", sqEx);
            }
        }
        # endregion
    }
}
