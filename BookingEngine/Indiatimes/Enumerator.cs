using System;
using System.Collections.Generic;
using System.Text;

namespace Technology.Indiatimes
{
    public enum ITimesBookingStatus
    {
        Failed = 1,
        Ticketed = 2,
        Rejected = 3,
        Booked = 4,
        Released = 5,
        Moved = 6,
        ManualMove = 7,
        Duplicate = 8,
        Cancelled = 9
    }

    public enum ITimesBookingAction
    {
        BlacklistIP = 1,
        BlackListEmail = 2,
        BlackListPhone = 3,
        NormalizeIp = 4,
        NormalizeEmail = 5,
        NormalizePhone = 6,
        SuspiciousIP = 7,
        ClearIP = 8,
        AddNote = 9,
        SuspiciousEmail = 10
    }

    public enum IPAddressStatus
    {
        Blocked = 1,
        Suspicious = 2,
        Normal = 3
    }

    public enum CustomerStatus
    {
        Blocked = 1,
        Suspicious = 2,
        Genuine = 3
    }

    public enum Passenger_Type
    {
        ADT = 0,
        CHD = 1,
        INF = 2
    }

    public enum RefrenceType
    {
        /// <summary>
        /// Booking Type is Flight Booking
        /// </summary>
        Flight = 1,
        /// <summary>
        /// Booking Type is Hotel Booking
        /// </summary>
        Hotel = 2,
    }

    public enum PaymentStatus
    {
        Accepted = 1,
        InProcess = 2,
        Rejected = 3
    }

    public enum ITimesPendingQueueaction
    {
        Moved = 1,
        Released = 2,
        Rejected = 3,
        AddNote = 4
    }

    public enum Request_Type
    {
        Cancellation = 1,
        Reissuance = 2
    }

    public enum ChangeRequest_Status
    {
        Pending = 1,
        InProgress = 2,
        Processed = 3,
        Rejected = 4
    }

    public enum ITimesHotelRating
    {
        All = 0,
        OneStar = 1,
        TwoStar = 2,
        ThreeStar = 3,
        FourStar = 4,
        FiveStar = 5
    }

    public enum ITimesHotelBookingStatus
    {
        Failed = 1,
        Vouchered = 2,
        Rejected = 3,
        Confirmed = 4,
        Released = 5,
        Moved = 6,
        ManualMove = 7,
        Duplicate = 8,
        Cancelled = 9,
        Pending = 10
    }

    public enum ITimesHotelBookingSource
    {
        Desiya = 1,
        GTA = 2,
        HotelBeds = 3,
        Tourico = 4,
        IAN = 5,
        DOTW = 6,
        Miki = 7,
        Travco = 8
    }

    public enum IndiaTimesPackageQueriesStatus
    {
        New = 1,
        Opened = 2,
        Processed = 3,
        Rejected = 4,
        Closed = 5,
        Duplicate= 6
    }
}
