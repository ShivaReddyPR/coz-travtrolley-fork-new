using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Indiatimes;
using System.Diagnostics;
using System.Xml;
using System.IO;

namespace Technology.Indiatimes
{
    public class PackageTheme
    {
        #region privateFields
        int themeId;
        string themeName;
        bool isActive;
        #endregion

        #region publicProperties
        public int ThemeId
        {
            get
            {
                return themeId;
            }
            set
            {
                themeId = value;
            }
        }

        public string ThemeName
        {
            get
            {
                return themeName;
            }
            set
            {
                themeName = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }

        #endregion

        #region publicMethods
        public static List<PackageTheme> Load()
        {
            List<PackageTheme> tempList = new List<PackageTheme>();
            SqlDataReader data = null;
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[0];
                try
                {
                    data = Dal.ExecuteReaderSP(SPNames.GetAllActiveHotelThemes, paramList, connection);
                    while (data.Read())
                    {
                        PackageTheme itpq = new PackageTheme();
                        itpq.themeId = Convert.ToInt32(data["themeId"]);
                        itpq.themeName = Convert.ToString(data["themeName"]);
                        itpq.isActive = Convert.ToBoolean(data["isActive"]);
                        tempList.Add(itpq);
                    }
                }
                catch (Exception ex)
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during Load() in Theme.cs,PackageTheme | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    throw new Exception("Unable to load Themes for IndiaTimesHotelDeal");
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return tempList;
        }

        public static int AddThemes(int dealId, List<int> themeIdList)
        {
            int themeAdded = 1;
            DataTable dataTable = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[0];
                try
                {
                    SqlCommand command = new SqlCommand(SPNames.GetDealThemeIndexTableSchema, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 60;
                    command.Parameters.AddRange(paramList);
                    adapter.SelectCommand = command;                    
                    adapter.Fill(dataTable);
                    paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@dealId", dealId);
                    Dal.ExecuteNonQuerySP(SPNames.DeleteFromThemeIndexTableSchema, paramList);
                    for (int i = 0; i < themeIdList.Count; i++)
                    {
                        DataRow row = dataTable.NewRow();
                        row["dealId"] = dealId;
                        row["themeId"] = themeIdList[i];
                        dataTable.Rows.Add(row);
                    }
                    SqlCommandBuilder cmd = new SqlCommandBuilder(adapter);
                    adapter.Update(dataTable);
                }
                catch (Exception ex)
                {
                    themeAdded = 0;
                    CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during AddThemes() in Themes.cs,PackageTheme Unable to add themes for home page and paging on India Times. | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                }
                finally
                {
                    connection.Close();
                    adapter.Dispose();
                }
            }
            return themeAdded;
        }
        #endregion


    }




    public class DealThemeIndex
    {
        #region privateFields
        int themeId;
        int dealId;
        #endregion

        #region publicProperties
        public int DealId
        {
            get
            {
                return dealId;
            }
            set
            {
                dealId = value;
            }
        }

        public int ThemeId
        {
            get
            {
                return themeId;
            }
            set
            {
                themeId = value;
            }
        }

        #endregion

        #region publicMethods
        public static List<int> Load(int dealId)
        {
            List<int> tempList = new List<int>();
            SqlDataReader data = null;
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@dealId", dealId);
                try
                {
                    data = Dal.ExecuteReaderSP(SPNames.GetSelectedThemesForDeal, paramList, connection);
                    while (data.Read())
                    {
                        tempList.Add(Convert.ToInt32(data["themeId"]));
                    }
                }
                catch (Exception ex)
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during Load() in Theme.cs,DealThemeIndex for dealId= " + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    throw new Exception("Unable to load DealThemeIndex for dealId: " + dealId);
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return tempList;
        }
        #endregion
    }
}
