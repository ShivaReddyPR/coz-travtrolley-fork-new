using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using CoreLogic;
using System.Diagnostics;

namespace Technology.Indiatimes
{
    public class ITimesPendingQueueHistory
    {
        # region Private Fields
        int queueId;
        ITimesPendingQueueaction action;
        string remarks;
        int createdBy;
        DateTime createdOn;
        #endregion

        # region Public Properties

        public int QueueId
        {
            get
            {
                return queueId;
            }
            set
            {
                queueId = value;
            }
        }

        public ITimesPendingQueueaction Action
        {
            get
            {
                return action;
            }
            set
            {
                action = value;
            }
        }

        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        #endregion
        /// <summary>
        /// Loads History of a Queue in Pending Queue.
        /// </summary>
        /// <param name="queueId">Queue Id whose history needs to be seen.</param>
        /// <returns>List containing history of selected Queue</returns>
        public static List<ITimesPendingQueueHistory> Load(int queueId)
        {
            if (queueId <= 0)
            {
                throw new ArgumentException("queueId should not be less then equal to zero", "queueId");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@queueId", queueId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetHistoryForPendingQueue, paramList, connection);
            List<ITimesPendingQueueHistory> tempList = new List<ITimesPendingQueueHistory>();
            if (data.HasRows)
            {
                while (data.Read())
                {
                    ITimesPendingQueueHistory iHistory = new ITimesPendingQueueHistory();
                    iHistory.action = (ITimesPendingQueueaction)Enum.Parse(typeof(ITimesPendingQueueaction), data["action"].ToString());
                    iHistory.remarks = Convert.ToString(data["remarks"]);
                    iHistory.createdBy = Convert.ToInt32(data["createdBy"]);
                    iHistory.createdOn = Convert.ToDateTime(data["createdOn"]);
                    tempList.Add(iHistory);
                }
            }
            return tempList;
        }
        /// <summary>
        /// Save history for a specific queue in Pending Queue Panel.
        /// </summary>
        /// <param name="queueId">Queue Id</param>
        /// <param name="action">Action</param>
        /// <param name="remarks">Remarks</param>
        /// <param name="createdBy">Created By</param>
        /// <returns>Number of rows updated</returns>
        public static int SavePendingQueueHistory(int queueId, ITimesPendingQueueaction action, string remarks, int createdBy)
        {
            if (queueId <= 0)
            {
                throw new ArgumentException("queueId should not be less then equal to zero", "queueId");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy should not be less then equal to zero", "createdBy");
            }
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@queueId", queueId);
            paramList[1] = new SqlParameter("@action", action);
            paramList[2] = new SqlParameter("@remarks", remarks);
            paramList[3] = new SqlParameter("@createdBy", createdBy);
            //paramList[2] = new SqlParameter("@lastModifiedBy", modifiedById);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.SaveITimesPendingQueueHistory, paramList);
            if (rowsAffected > 0)
            {
                return rowsAffected;
            }
            else
            {
                return 0;
            }
        }
    }
}
