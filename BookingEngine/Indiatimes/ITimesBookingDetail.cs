using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Indiatimes;
using System.Diagnostics;
using System.Xml;
using System.IO;

namespace Technology.Indiatimes
{
    public struct PaxDetail
    {
        public string Title;
        public string FirstName;
        public string LastName;
        public string Type;
        public string AddressLine1;
        public string AddressLine2;
        public string Phone;
        public string Country;
        public string Email;
        public DateTime DOB;
    }
    public class ITimesBookingDetail
    {
        # region Private Fields
        int tripId;
        int bookingIdOut;
        int bookingIdIn;
        string outPNR;
        string inPNR;
        string paymentId;
        PaymentGatewaySource paySource;
        string sector;
        string passengerInfo;
        string ipAddress;
        string email;
        string phone;
        CustomerStatus customerStatus;
        ITimesBookingStatus bookingStatus;
        string reason;
        ITimesBookingHistory history;
        IPAddressStatus status;
        string backColor;
        List<PaxDetail> paxList;
        bool isDomesticReturn;
        decimal paymentAmount;
        bool outTicketed;
        bool inTicketed;
        int agencyId;        
        string airlineCodeOut;
        string airlineCodeIn;
        string flightNumberOut;
        string flightNumberIn;
        DateTime depDate;
        DateTime returnDate;
        string remarks;
        DateTime createdOn;
        int createdBy;
        DateTime lastModifiedOn;
        int lastModifiedBy;
        bool isDomestic;
        bool receiptGenerated;
        string orderId;
        # endregion

        # region Public Properties

        public int TripId
        {
            get
            {
                return tripId;
            }
            set
            {
                tripId = value;
            }
        }

        public int BookingIdOut
        {
            get
            {
                return bookingIdOut;
            }
            set
            {
                bookingIdOut = value;
            }
        }

        public int BookingIdIn
        {
            get
            {
                return bookingIdIn;
            }
            set
            {
                bookingIdIn = value;
            }
        }

        public string OutPNR
        {
            get
            {
                return outPNR;
            }
            set
            {
                outPNR = value;
            }
        }

        public string InPNR
        {
            get
            {
                return inPNR;
            }
            set
            {
                inPNR = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        public string PaymentId
        {
            get
            {
                return paymentId;
            }
            set
            {
                paymentId = value;
            }
        }

        public PaymentGatewaySource PaySource
        {
            get
            {
                return paySource;
            }
            set
            {
                paySource = value;
            }
        }

        public string Sector
        {
            get
            {
                return sector;
            }
            set
            {
                sector = value;
            }
        }

        public string PassengerInfo
        {
            get
            {
                return passengerInfo;
            }
            set
            {
                passengerInfo = value;
            }
        }

        public string IPAddress
        {
            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
            }
        }

        public CustomerStatus CustomerStatus
        {
            get
            {
                return customerStatus;
            }
            set
            {
                customerStatus = value;
            }
        }

        public ITimesBookingStatus BookingStatus
        {
            get
            {
                return bookingStatus;
            }
            set
            {
                bookingStatus = value;
            }
        }

        public string Reason
        {
            get
            {
                return reason;
            }
            set
            {
                reason = value;
            }
        }

        public ITimesBookingHistory History
        {
            get
            {
                return history;
            }
            set
            {
                history = value;
            }
        }

        public IPAddressStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public string BackColor
        {
            get
            {
                return backColor;
            }
            set
            {
                backColor = value;
            }
        }

        public List<PaxDetail> PaxList
        {
            get
            {
                return paxList;
            }
            set
            {
                paxList = value;
            }
        }

        public bool IsDomesticReturn
        {
            get
            {
                return isDomesticReturn;
            }
            set
            {
                isDomesticReturn = value;
            }
        }

        public decimal PaymentAmount
        {
            set
            {
                paymentAmount = value;
            }
            get
            {
                return paymentAmount;
            }
        }

        public bool OutTicketed
        {
            set
            {
                outTicketed = value;
            }
            get
            {
                return outTicketed;
            }
        }

        public bool InTicketed
        {
            set
            {
                inTicketed = value;
            }
            get
            {
                return inTicketed;
            }
        }

        public int AgencyId
        {
            set
            {
                agencyId = value;
            }
            get
            {
                return agencyId;
            }
        }               

        public string AirlineCodeOut
        {
            set
            {
                airlineCodeOut = value;
            }
            get
            {
                return airlineCodeOut;
            }
        }

        public string AirlineCodeIn
        {
            set
            {
                airlineCodeIn = value;
            }
            get
            {
                return airlineCodeIn;
            }
        }

        public string FlightNumberOut
        {
            set
            {
                flightNumberOut = value;
            }
            get
            {
                return flightNumberOut;
            }
        }

        public string FlightNumberIn
        {
            set
            {
                flightNumberIn = value;
            }
            get
            {
                return flightNumberIn;
            }
        }

        public DateTime DepDate
        {
            set
            {
                depDate = value;
            }
            get
            {
                return depDate;
            }
        }

        public DateTime ReturnDate
        {
            set
            {
                returnDate = value;
            }
            get
            {
                return returnDate;
            }
        }

        public string Remarks
        {
            set
            {
                remarks = value;
            }
            get
            {
                return remarks;
            }
        }

        public DateTime LastModifiedOn
        {
            set
            {
                lastModifiedOn = value;
            }
            get
            {
                return lastModifiedOn;
            }
        }

        public int CreatedBy
        {
            set
            {
                createdBy = value;
            }
            get
            {
                return createdBy;
            }
        }

        public int LastModifiedBy
        {
            set
            {
                lastModifiedBy = value;
            }
            get
            {
                return lastModifiedBy;
            }
        }

        public bool IsDomestic
        {
            set
            {
                isDomestic = value;
            }
            get
            {
                return isDomestic;
            }
        }

        public bool ReceiptGenerated
        {
            set
            {
                receiptGenerated = value;
            }
            get
            {
                return receiptGenerated;
            }
        }


        public string OrderId
        {
            set
            {
                orderId = value;
            }
            get
            {
                return orderId;
            }
        }

        #endregion

        public static List<ITimesBookingDetail> Load(int pageNo, string whereString)
        {
            Trace.TraceInformation("ITimesBookingDetails.Load entered : page number = " + pageNo);
            List<ITimesBookingDetail> tempList = new List<ITimesBookingDetail>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@rowNo", pageNo * 100);
            paramList[1] = new SqlParameter("@whereString", whereString);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetITimesBookingDetails, paramList, connection);
            if (data.HasRows)
            {
                while (data.Read())
                {
                    ITimesBookingDetail itbd = new ITimesBookingDetail();
                    itbd.tripId = Convert.ToInt32(data["tripId"]);
                    if (data["bookingIdOut"] != DBNull.Value)
                    {
                        itbd.bookingIdOut = Convert.ToInt32(data["bookingIdOut"]);
                    }
                    else
                    {
                        itbd.bookingIdOut = 0;
                    }
                    if (data["bookingIdIn"] != DBNull.Value)
                    {
                        itbd.bookingIdIn = Convert.ToInt32(data["bookingIdIn"]);
                    }
                    else
                    {
                        itbd.bookingIdIn = 0;
                    }
                    if (data["outPNR"] != DBNull.Value)
                    {
                        itbd.outPNR = Convert.ToString(data["outPNR"]);
                    }
                    else
                    {
                        itbd.outPNR = string.Empty;
                    }
                    if (data["inPNR"] != DBNull.Value)
                    {
                        itbd.inPNR = Convert.ToString(data["inPNR"]);
                    }
                    else
                    {
                        itbd.inPNR = string.Empty;
                    }
                    if (data["paymentId"] != DBNull.Value)
                    {
                        itbd.paymentId = Convert.ToString(data["paymentId"]);
                    }
                    else
                    {
                        itbd.paymentId = string.Empty;
                    }
                    itbd.paySource = (PaymentGatewaySource)(Enum.Parse(typeof(PaymentGatewaySource), data["paymentGatewaySourceId"].ToString()));
                    if (data["sector"] != DBNull.Value)
                    {
                        itbd.sector = Convert.ToString(data["sector"]);
                    }
                    else
                    {
                        itbd.sector = string.Empty;
                    }
                    if (data["passengerInfo"] != null)
                    {
                        itbd.passengerInfo = Convert.ToString(data["passengerInfo"]);
                    }
                    else
                    {
                        itbd.passengerInfo = string.Empty;
                    }
                    if (data["ipAddress"] != DBNull.Value)
                    {
                        itbd.ipAddress = Convert.ToString(data["ipAddress"]);
                    }
                    else
                    {
                        itbd.ipAddress = string.Empty;
                    }
                    if (data["email"] != DBNull.Value)
                    {
                        itbd.email = Convert.ToString(data["email"]);
                    }
                    else
                    {
                        itbd.email = string.Empty;
                    }
                    if (data["phone"] != DBNull.Value)
                    {
                        itbd.phone = Convert.ToString(data["phone"]);
                    }
                    else
                    {
                        itbd.phone = string.Empty;
                    }
                    if (data["customerStatus"] != DBNull.Value)
                    {
                        itbd.customerStatus = (CustomerStatus)Enum.Parse(typeof(CustomerStatus), data["customerStatus"].ToString());
                    }
                    itbd.bookingStatus = (ITimesBookingStatus)(Enum.Parse(typeof(ITimesBookingStatus), data["bookingStatus"].ToString()));
                    itbd.status = (IPAddressStatus)(Enum.Parse(typeof(IPAddressStatus), data["status"].ToString()));
                    if (data["isDomesticReturn"] != DBNull.Value)
                    {
                        itbd.isDomesticReturn = Convert.ToBoolean(data["isDomesticReturn"]);
                    }
                    itbd.createdOn = Convert.ToDateTime(data["createdOn"]);
                    if (data["paymentAmount"] != DBNull.Value)
                    {
                        itbd.paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                    }
                    else
                    {
                        itbd.paymentAmount = 0;
                    }
                    itbd.outTicketed = Convert.ToBoolean(data["outTicketed"]);
                    if (data["inTicketed"] != DBNull.Value)
                    {
                        itbd.inTicketed = Convert.ToBoolean(data["inTicketed"]);
                    }
                    itbd.agencyId = Convert.ToInt32(data["agencyId"]);                    
                    if (data["airlineCodeIn"] != DBNull.Value)
                    {
                        itbd.airlineCodeIn = Convert.ToString(data["airlineCodeIn"]);
                    }
                    itbd.airlineCodeOut = Convert.ToString(data["airlineCodeOut"]);
                    if (data["flightNumberIn"] != DBNull.Value)
                    {
                        itbd.flightNumberIn = Convert.ToString(data["flightNumberIn"]);
                    }
                    itbd.flightNumberOut = Convert.ToString(data["flightNumberOut"]);
                    itbd.depDate = Convert.ToDateTime(data["depDate"]);
                    if (data["returnDate"] != DBNull.Value)
                    {
                        itbd.returnDate = Convert.ToDateTime(data["returnDate"]);
                    }
                    if (data["remarks"] != DBNull.Value)
                    {
                        itbd.remarks = Convert.ToString(data["remarks"]);
                    }
                    else
                    {
                        itbd.remarks = string.Empty;
                    }
                    itbd.isDomestic = Convert.ToBoolean(data["isDomestic"]);
                    itbd.createdBy = Convert.ToInt32(data["createdBy"]);
                    itbd.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    itbd.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                    tempList.Add(itbd);
                }
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("ITimesBookingDetails.Load exited : page number = " + pageNo);
            return tempList;
        }

        public static string GetWhereString(bool stringFilter, string type, string value, bool redSelect, bool yellowSelect, bool greenSelect, bool failedSelect, bool bookedSelect, bool ticktedSelect, bool fraudSearch, bool domesticSearch, bool internationalSearch, bool dateSearch, string fromDateString, string toDateString,object agencyId)
        {
            StringBuilder sr = new StringBuilder();
            string whereString = string.Empty;
            List<int> color = new List<int>();
            if (redSelect)
            {
                color.Add(1);
            }
            if (yellowSelect)
            {
                color.Add(2);
            }
            if (greenSelect)
            {
                color.Add(3);
            }

            List<int> bookingStatusList = new List<int>();
            if (failedSelect)
            {
                bookingStatusList.Add(1);
            }
            if (bookedSelect)
            {
                bookingStatusList.Add(4);
            }
            if (ticktedSelect)
            {
                bookingStatusList.Add(2);
            }

            string filterCheckString = string.Empty;
            for (int i = 0; i < color.Count; i++)
            {
                filterCheckString += color[i] + ",";
            }
            filterCheckString = filterCheckString.Substring(0, filterCheckString.Length - 1);

            string bookingStatusSelectString = string.Empty;
            for (int i = 0; i < bookingStatusList.Count; i++)
            {
                bookingStatusSelectString += bookingStatusList[i] + ",";
            }
            bookingStatusSelectString = bookingStatusSelectString.Substring(0, bookingStatusSelectString.Length - 1);

            if (redSelect && !yellowSelect && !greenSelect)
            {
                sr.Append("where (ip.ipAddressStatus in (1) or cust.customerStatus in (1))");
                //whereString = " where (ip.ipAddressStatus in (1) or cust.customerStatus in (1))";
            }
            if (!redSelect && yellowSelect && !greenSelect)
            {
                sr.Append(" where ((ip.ipAddressStatus in (2) and cust.customerStatus in (2,3)) or (cust.customerStatus in (2) and ip.ipAddressStatus in (3)))");
                //whereString = " where ((ip.ipAddressStatus in (2) and cust.customerStatus in (2,3)) or (cust.customerStatus in (2) and ip.ipAddressStatus in (3)))";
            }
            if (!redSelect && !yellowSelect && greenSelect)
            {
                sr.Append(" where (ip.ipAddressStatus in (3) and cust.customerStatus in (3))");
                //whereString = " where (ip.ipAddressStatus in (3) and cust.customerStatus in (3))";
            }
            if (redSelect && yellowSelect && !greenSelect)
            {
                sr.Append(" where (ip.ipAddressStatus in (1,2) or cust.customerStatus in (1,2))");
                //whereString = " where (ip.ipAddressStatus in (1,2) or cust.customerStatus in (1,2))";
            }
            if (redSelect && !yellowSelect && greenSelect)
            {
                sr.Append(" where ((ip.ipAddressStatus in (1,3) and cust.customerStatus in (1,3)) or (ip.ipAddressStatus in (1) and cust.customerStatus in (2)) or (cust.customerStatus in (1) and ip.ipAddressStatus in (2)))");
                //whereString = " where ((ip.ipAddressStatus in (1,3) and cust.customerStatus in (1,3)) or (ip.ipAddressStatus in (1) and cust.customerStatus in (2)) or (cust.customerStatus in (1) and ip.ipAddressStatus in (2)))";
            }
            if (!redSelect && yellowSelect && greenSelect)
            {
                sr.Append(" where (ip.ipAddressStatus in (2,3) and cust.customerStatus in (2,3))");
                //whereString = " where (ip.ipAddressStatus in (2,3) and cust.customerStatus in (2,3))";
            }
            if (redSelect && yellowSelect && greenSelect)
            {
                sr.Append(" where (ip.ipAddressStatus in (1,2,3))");
                //whereString = " where (ip.ipAddressStatus in (1,2,3))";
            }
            //whereString += "and bd.bookingStatus in(" + bookingStatusSelectString + ",3)";// To change when rejected filter is implemented.
            sr.Append("and bd.bookingStatus in(" + bookingStatusSelectString + ",3)");
            if (!fraudSearch && !(domesticSearch && internationalSearch))
            {
                if (domesticSearch)
                {
                    //whereString += " and bd.isDomestic = 1";
                    sr.Append(" and bd.isDomestic = 1");
                }
                else
                {
                    //whereString += " and bd.isDomestic = 0";
                    sr.Append(" and bd.isDomestic = 0");
                }
            }
            if (stringFilter)
            {
                value = value.Replace("'", "''");
                if (type == "Trip")
                {
                    //whereString += " and bd.tripId = " + value.Trim();
                    sr.Append(" and bd.tripId = " + value.Trim());
                }
                if (type == "Agency")
                {
                    //whereString += " and bd.AgencyId IN (Select AgencyId From Agency Where AgencyName Like '%" + value.Trim() + "%')";
                    sr.Append(" and bd.AgencyId IN (Select AgencyId From Agency Where AgencyName Like '%" + value.Trim() + "%')");
                }
                if (type == "Email")
                {
                    //whereString += " and bd.email like '" + value.Trim().ToLower() + "%'";
                    sr.Append(" and bd.email like '" + value.Trim().ToLower() + "%'");
                }
                if (type == "IP")
                {
                    //whereString += " and bd.ipAddress = '" + value.Trim() + "'";
                    sr.Append(" and bd.ipAddress = '" + value.Trim() + "'");
                }
                if (type == "PaymentId")
                {
                    //whereString += " and bd.paymentId = '" + value.Trim() + "'";
                    sr.Append(" and bd.paymentId = '" + value.Trim() + "'");
                }
                if (type == "PNR")
                {
                    //whereString += " and bd.outPNR = '" + value.Trim() + "' or bd.inPNR = '" + value.Trim() + "'";
                    sr.Append(" and bd.outPNR = '" + value.Trim() + "' or bd.inPNR = '" + value.Trim() + "'");
                }
            }

            if (agencyId != null && (int)agencyId > 0 && whereString.Length > 4)
            {
                //whereString += " AND AgencyId = " + ((int)agencyId).ToString();
                sr.Append(" AND AgencyId = " + ((int)agencyId).ToString());
            }
            else if (agencyId != null && (int)agencyId > 0 && whereString.Length < 4)
            {
                //whereString += " Where AgencyId = " + ((int)agencyId).ToString();
                // no need to agency id disucss with anupam sir updated by pankaj
                //=================================================================
                //sr.Append(" Where AgencyId = " + ((int)agencyId).ToString());
            }

            if (dateSearch)
            {
                //char[] dateSplitter ={ '/' };
                string[] fromDateArr = fromDateString.Split('/');
                string[] toDateArr = toDateString.Split('/');
                DateTime fromDate = new DateTime(int.Parse(fromDateArr[2]), int.Parse(fromDateArr[1]), int.Parse(fromDateArr[0]), 00, 00, 00);
                DateTime toDate = new DateTime(int.Parse(toDateArr[2]), int.Parse(toDateArr[1]), int.Parse(toDateArr[0]), 23, 59, 59);
                fromDate = Util.ISTToUTC(fromDate);
                toDate = Util.ISTToUTC(toDate);
                //whereString += " and bd.createdOn between '" + fromDate.ToString() + "' and '" + toDate.ToString() + "'";
                sr.Append(" and bd.createdOn between '" + fromDate.ToString() + "' and '" + toDate.ToString() + "'");
            }
            return sr.ToString();
        }

        public static int GetRowCount(string whereString)
        {
            Trace.TraceInformation("ITimesBookingDetails.GetRowCount entered");
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@whereString", whereString);

            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetITimesBookingCount, paramList, connection);
            int numberOfRows = 0;
            if (data.Read())
            {
                numberOfRows = Convert.ToInt32(data["numberOfRows"]);
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("ITimesBookingDetails.GetRowCount exited. Number of rows = " + numberOfRows);
            return numberOfRows;
        }

        public static ITimesBookingDetail GetTrip(int tripId)
        {
            Trace.TraceInformation("ITimesBookingDetails.GetTrip entered : tripId = " + tripId);
            ITimesBookingDetail itbd = new ITimesBookingDetail();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@tripId", tripId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetBookingDetailByTripId, paramList, connection);
            if (data.Read())
            {
                //ITimesBookingDetail itbd = new ITimesBookingDetail();
                itbd.tripId = Convert.ToInt32(data["tripId"]);
                if (data["bookingIdOut"] != DBNull.Value)
                {
                    itbd.bookingIdOut = Convert.ToInt32(data["bookingIdOut"]);
                }
                else
                {
                    itbd.bookingIdOut = 0;
                }
                if (data["bookingIdIn"] != DBNull.Value)
                {
                    itbd.bookingIdIn = Convert.ToInt32(data["bookingIdIn"]);
                }
                else
                {
                    itbd.bookingIdIn = 0;
                }
                if (data["outPNR"] != DBNull.Value)
                {
                    itbd.outPNR = Convert.ToString(data["outPNR"]);
                }
                else
                {
                    itbd.outPNR = string.Empty;
                }
                if (data["inPNR"] != DBNull.Value)
                {
                    itbd.inPNR = Convert.ToString(data["inPNR"]);
                }
                else
                {
                    itbd.inPNR = string.Empty;
                }
                if (data["paymentId"] != DBNull.Value)
                {
                    itbd.paymentId = Convert.ToString(data["paymentId"]);
                }
                else
                {
                    itbd.paymentId = string.Empty;
                }
                itbd.paySource = (PaymentGatewaySource)(Enum.Parse(typeof(PaymentGatewaySource), data["paymentGatewaySourceId"].ToString()));
                if (data["sector"] != DBNull.Value)
                {
                    itbd.sector = Convert.ToString(data["sector"]);
                }
                else
                {
                    itbd.sector = string.Empty;
                }
                if (data["passengerInfo"] != null)
                {
                    itbd.passengerInfo = Convert.ToString(data["passengerInfo"]);
                }
                else
                {
                    itbd.passengerInfo = string.Empty;
                }
                if (data["ipAddress"] != DBNull.Value)
                {
                    itbd.ipAddress = Convert.ToString(data["ipAddress"]);
                }
                else
                {
                    itbd.ipAddress = string.Empty;
                }
                if (data["email"] != DBNull.Value)
                {
                    itbd.email = Convert.ToString(data["email"]);
                }
                else
                {
                    itbd.email = string.Empty;
                }
                if (data["phone"] != DBNull.Value)
                {
                    itbd.phone = Convert.ToString(data["phone"]);
                }
                else
                {
                    itbd.phone = string.Empty;
                }
                if (data["customerStatus"] != DBNull.Value)
                {
                    itbd.customerStatus = (CustomerStatus)Enum.Parse(typeof(CustomerStatus), data["customerStatus"].ToString());
                }
                itbd.bookingStatus = (ITimesBookingStatus)(Enum.Parse(typeof(ITimesBookingStatus), data["bookingStatus"].ToString()));
                itbd.status = (IPAddressStatus)(Enum.Parse(typeof(IPAddressStatus), data["status"].ToString()));
                if (data["isDomesticReturn"] != DBNull.Value)
                {
                    itbd.isDomesticReturn = Convert.ToBoolean(data["isDomesticReturn"]);
                }
                itbd.createdOn = Convert.ToDateTime(data["createdOn"]);
                if (data["paymentAmount"] != DBNull.Value)
                {
                    itbd.paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                }
                else
                {
                    itbd.paymentAmount = 0;
                }
                itbd.outTicketed = Convert.ToBoolean(data["outTicketed"]);
                if (data["inTicketed"] != DBNull.Value)
                {
                    itbd.inTicketed = Convert.ToBoolean(data["inTicketed"]);
                }
                itbd.agencyId = Convert.ToInt32(data["agencyId"]);
                if (data["airlineCodeIn"] != DBNull.Value)
                {
                    itbd.airlineCodeIn = Convert.ToString(data["airlineCodeIn"]);
                }
                itbd.airlineCodeOut = Convert.ToString(data["airlineCodeOut"]);
                if (data["flightNumberIn"] != DBNull.Value)
                {
                    itbd.flightNumberIn = Convert.ToString(data["flightNumberIn"]);
                }
                itbd.flightNumberOut = Convert.ToString(data["flightNumberOut"]);
                itbd.depDate = Convert.ToDateTime(data["depDate"]);
                if (data["returnDate"] != DBNull.Value)
                {
                    itbd.returnDate = Convert.ToDateTime(data["returnDate"]);
                }
                if (data["remarks"] != DBNull.Value)
                {
                    itbd.remarks = Convert.ToString(data["remarks"]);
                }
                else
                {
                    itbd.remarks = string.Empty;
                }
                itbd.isDomestic = Convert.ToBoolean(data["isDomestic"]);
                itbd.createdBy = Convert.ToInt32(data["createdBy"]);
                itbd.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                itbd.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                itbd.orderId = Convert.ToString(data["orderId"]);
            }
            else
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.IndiaTimesBookingDetailsTripSearch, CoreLogic.Severity.High, 0, "Error: Unable to find the Trip ID: " + tripId + " | " + DateTime.Now, "");
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("ITimesBookingDetails.GetTrip exited : tripId = " + tripId);
            return itbd;
        }
        public static Dictionary<string, int> LoadStatus(string ipAddress, string email)
        {
            Trace.TraceInformation("ITimesBookingDetails.LoadStatus entered : ipAddress = " + ipAddress + ", email = " + email);
            if (ipAddress == null || ipAddress.Trim().Length <= 0)
            {
                throw new ArgumentException("ipAddress should not be null or empty string", "ipAddress");
            }
            if (email == null || email.Trim().Length <= 0)
            {
                throw new ArgumentException("email should not be null or empty string", "email");
            }
            Dictionary<string, int> statusDictionary = new Dictionary<string, int>();

            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@ipAddress", ipAddress);
            paramList[1] = new SqlParameter("@email", email);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetBookingFullStatus, paramList, connection);
            ITimesBookingDetail itbd = new ITimesBookingDetail();
            bool dataRead = false;
            if (data.Read())
            {
                dataRead = true;
                statusDictionary.Add("ipStatus", (int)(IPAddressStatus)(Enum.Parse(typeof(IPAddressStatus), data["ipStatus"].ToString())));
                statusDictionary.Add("customerStatus", (int)(CustomerStatus)Enum.Parse(typeof(CustomerStatus), data["customerStatus"].ToString()));
            }
            else
            {
                Trace.TraceInformation("ITimesBookingDetails.LoadStatus exited, details not loaded : ipAddress = " + ipAddress + ", email = " + email);
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("ITimesBookingDetails.LoadStatus exited : ipAddress = " + ipAddress + ", email = " + email);
            if (!dataRead)
            {
                throw new ArgumentException("ipAddress or email doesnot exist in database.", "ipAddress,email");
            }
            return statusDictionary;
        }

        public static int UpdateCustomerStatus(string userName, CustomerStatus customerStatus)
        {
            Trace.TraceInformation("ITimesBookingDetails.UpdateCustomerStatus entered : userName = " + userName + ", customerStatus = " + customerStatus);
            if (userName == null || userName.Trim().Length <= 0)
            {
                throw new ArgumentException("userName should not be null or empty string", "userName");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@userName", userName);
            paramList[1] = new SqlParameter("@customerStatus", (int)customerStatus);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateCustomerStatus, paramList);
            if (rowsAffected > 0)
            {
                Trace.TraceInformation("ITimesBookingDetails.UpdateCustomerStatus exited : userName = " + userName);
                return rowsAffected;
            }
            else
            {
                Trace.TraceInformation("ITimesBookingDetails.UpdateCustomerStatus exited, userName status not modified : userName = " + userName + ", customerStatus = " + customerStatus.ToString());
                return 0;
            }
        }

        public int updateTripIdParametersOut()
        {
            SqlParameter[] paramList = new SqlParameter[4];

            if (airlineCodeOut == null || airlineCodeOut == "")
            {
                paramList[0] = new SqlParameter("@airlineCodeOut", DBNull.Value);
            }
            else
            {
                paramList[0] = new SqlParameter("@airlineCodeOut", airlineCodeOut);
            }
            if (flightNumberOut == null || flightNumberOut == "")
            {
                paramList[1] = new SqlParameter("@flightNumberOut", DBNull.Value);
            }
            else
            {
                paramList[1] = new SqlParameter("@flightNumberOut", flightNumberOut);
            }

            if (depDate == null || depDate == new DateTime())
            {
                throw new ArgumentException("departure date should not be null or new datetime()", "departuredate");
            }
            else
            {
                paramList[2] = new SqlParameter("@depDate", depDate);
            }
            paramList[3] = new SqlParameter("@tripId", tripId);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateITimesBookingDetailsParametersOut, paramList);
            if (rowsAffected > 0)
            {
                Trace.TraceInformation("ITimesBookingDetails.Save exited. Booking modified. Trip Id = " + tripId);
                return rowsAffected;
            }
            else
            {
                Trace.TraceInformation("ITimesBookingDetails.Save exited. Booking not modified. Trip Id = " + tripId);
                return 0;
            }
        }

        public int updateTripIdParametersIn()
        {
            SqlParameter[] paramList = new SqlParameter[4];

            if (airlineCodeIn == null || airlineCodeIn == "")
            {
                paramList[0] = new SqlParameter("@airlineCodeIn", DBNull.Value);
            }
            else
            {
                paramList[0] = new SqlParameter("@airlineCodeIn", airlineCodeIn);
            }
            if (flightNumberIn == null || flightNumberIn == "")
            {
                paramList[1] = new SqlParameter("@flightNumberIn", DBNull.Value);
            }
            else
            {
                paramList[1] = new SqlParameter("@flightNumberIn", flightNumberIn);
            }

            if (returnDate == null || returnDate == new DateTime())
            {
                throw new ArgumentException("return date should not be null or new datetime()", "returnDate");
            }
            else
            {
                paramList[2] = new SqlParameter("@returnDate", returnDate);
            }
            paramList[3] = new SqlParameter("@tripId", tripId);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateITimesBookingDetailsParametersIn, paramList);
            if (rowsAffected > 0)
            {
                Trace.TraceInformation("ITimesBookingDetails.Save exited. Booking modified. Trip Id = " + tripId);
                return rowsAffected;
            }
            else
            {
                Trace.TraceInformation("ITimesBookingDetails.Save exited. Booking not modified. Trip Id = " + tripId);
                return 0;
            }
        }

        public static int UpdateTripIdAmount(decimal amount, int tripId)
        {
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[2];

            if (amount == 0)
            {
                throw new ArgumentException("Amount for a tripId cannot be null.", "amount");
            }
            else
            {
                paramList[0] = new SqlParameter("@amount", amount);
            }
            paramList[1] = new SqlParameter("@tripId", tripId);
            try
            {
                rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateITimesBookingDetailsAmount, paramList);
            }
            catch (Exception ex)
            {
                rowsAffected = 0;
                CoreLogic.Audit.Add(CoreLogic.EventType.IndiaTimesBookingDetailsTicketing, CoreLogic.Severity.High, 0, "Unable to update price of tripId: " + tripId + "Error: " + ex.Message + " | " + ex.StackTrace + " | " + DateTime.Now, "");
            }
            if (rowsAffected < 0)
            {
                rowsAffected = 0;
            }
            return rowsAffected;
        }

        public int Save()
        {
            Trace.TraceInformation("ITimesBookingDetails.Save entered.");
            if (tripId == 0)
            {
                SqlParameter[] paramList = new SqlParameter[28];

                paramList[0] = new SqlParameter("@paymentGatewaySourceId", (int)(paySource));
                paramList[1] = new SqlParameter("@paymentId", paymentId);
                paramList[2] = new SqlParameter("@paymentAmount", paymentAmount);
                paramList[3] = new SqlParameter("@isDomesticReturn", isDomesticReturn);
                if (bookingIdOut == 0)
                {
                    paramList[4] = new SqlParameter("@bookingIdOut", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@bookingIdOut", bookingIdOut);
                }
                if (bookingIdIn == 0)
                {
                    paramList[5] = new SqlParameter("@bookingIdIn", DBNull.Value);
                }
                else
                {
                    paramList[5] = new SqlParameter("@bookingIdIn", bookingIdIn);
                }
                paramList[6] = new SqlParameter("@outTicketed", outTicketed);
                paramList[7] = new SqlParameter("@inTicketed", inTicketed);
                paramList[8] = new SqlParameter("@agencyId", agencyId);
                paramList[9] = new SqlParameter("@airlineCodeOut", airlineCodeOut);
                if (airlineCodeIn == null)
                {
                    paramList[10] = new SqlParameter("@airlineCodeIn", DBNull.Value);
                }
                else
                {
                    paramList[10] = new SqlParameter("@airlineCodeIn", airlineCodeIn);
                }
                paramList[11] = new SqlParameter("@sector", sector);
                paramList[12] = new SqlParameter("@ipAddress", ipAddress);
                paramList[13] = new SqlParameter("@flightNumberOut", flightNumberOut);
                if (flightNumberIn == null)
                {
                    paramList[14] = new SqlParameter("@flightNumberIn", DBNull.Value);
                }
                else
                {
                    paramList[14] = new SqlParameter("@flightNumberIn", flightNumberIn);
                }
                paramList[15] = new SqlParameter("@passengerInfo", passengerInfo);
                paramList[16] = new SqlParameter("@depDate", depDate);
                if (returnDate.ToString() == "1/1/0001 12:00:00 AM")
                {
                    paramList[17] = new SqlParameter("@returnDate", DBNull.Value);
                }
                else
                {
                    paramList[17] = new SqlParameter("@returnDate", returnDate);
                }
                paramList[18] = new SqlParameter("@tripId", SqlDbType.Int);
                paramList[18].Direction = ParameterDirection.Output;
                paramList[19] = new SqlParameter("@bookingStatus", bookingStatus);
                if (inPNR == null)
                {
                    paramList[20] = new SqlParameter("@inPNR", DBNull.Value);
                }
                else
                {
                    paramList[20] = new SqlParameter("@inPNR", inPNR);
                }
                if (outPNR == null)
                {
                    paramList[21] = new SqlParameter("@outPNR", DBNull.Value);
                }
                else
                {
                    paramList[21] = new SqlParameter("@outPNR", outPNR);
                }
                paramList[22] = new SqlParameter("@createdBy", createdBy);
                if (remarks == null || remarks == "")
                {
                    paramList[23] = new SqlParameter("@remarks", DBNull.Value);
                }
                else
                {
                    paramList[23] = new SqlParameter("@remarks", remarks);
                }
                paramList[24] = new SqlParameter("@email", email);
                if (phone == null || phone == "")
                {
                    paramList[25] = new SqlParameter("@phone", DBNull.Value);
                }
                else
                {
                    paramList[25] = new SqlParameter("@phone", phone);
                }
                paramList[26] = new SqlParameter("@isDomestic", isDomestic);
                paramList[27] = new SqlParameter("@orderId", orderId);

                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddITimesBookingDetails, paramList);
                if (rowsAffected > 0)
                {
                    SqlParameter[] paramList2 = new SqlParameter[2];
                    if (inPNR == null)
                    {
                        paramList2[0] = new SqlParameter("@inPNR", DBNull.Value);
                    }
                    else
                    {
                        paramList2[0] = new SqlParameter("@inPNR", inPNR);
                    }
                    if (outPNR == null)
                    {
                        paramList2[1] = new SqlParameter("@outPNR", DBNull.Value);
                    }
                    else
                    {
                        paramList2[1] = new SqlParameter("@outPNR", outPNR);
                    }
                    Dal.ExecuteNonQuerySP(SPNames.UpdateITimesPendingQueueDuplicateEntry, paramList2);
                    return (int)paramList[18].Value;
                }
                else
                {
                    Trace.TraceInformation("ITimesBookingDetails.Save exited. Booking not saved.");
                    return 0;
                }
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[9];

                if (bookingIdOut == 0)
                {
                    paramList[0] = new SqlParameter("@bookingIdOut", DBNull.Value);
                }
                else
                {
                    paramList[0] = new SqlParameter("@bookingIdOut", bookingIdOut);
                }
                if (bookingIdIn == 0)
                {
                    paramList[1] = new SqlParameter("@bookingIdIn", DBNull.Value);
                }
                else
                {
                    paramList[1] = new SqlParameter("@bookingIdIn", bookingIdIn);
                }
                paramList[2] = new SqlParameter("@bookingStatus", (int)bookingStatus);
                if (inPNR == null || inPNR == "")
                {
                    paramList[3] = new SqlParameter("@inPNR", DBNull.Value);
                }
                else
                {
                    paramList[3] = new SqlParameter("@inPNR", inPNR);
                }
                if (outPNR == null || outPNR == "")
                {
                    paramList[4] = new SqlParameter("@outPNR", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@outPNR", outPNR);
                }
                if (remarks == null || remarks == "")
                {
                    paramList[5] = new SqlParameter("@remarks", DBNull.Value);
                }
                else
                {
                    paramList[5] = new SqlParameter("@remarks", remarks);
                }
                paramList[6] = new SqlParameter("@tripId", tripId);
                paramList[7] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                paramList[8] = new SqlParameter("@receiptGenerated", receiptGenerated);
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateITimesBookingDetails, paramList);
                if (rowsAffected > 0)
                {
                    Trace.TraceInformation("ITimesBookingDetails.Save exited. Booking modified. Trip Id = " + tripId);
                    return rowsAffected;
                }
                else
                {
                    Trace.TraceInformation("ITimesBookingDetails.Save exited. Booking not modified. Trip Id = " + tripId);
                    return 0;
                }
            }
        }
        /// <summary>
        /// To get number of booking from a IP in no of days
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <param name="noOfDays"></param>
        /// <returns></returns>
        public static int GetBookingCountFromIP(string ipAddress, int noOfDays)
        {
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@ipAddress", ipAddress);
            paramList[1] = new SqlParameter("@noOfDays", noOfDays);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetBookingCountFromIP, paramList, connection);
            int bookingCount = 0;
            if (data.Read())
            {
                bookingCount = Convert.ToInt32(data["count"]);
            }
            data.Close();
            connection.Close();
            return bookingCount;
        }

        /// <summary>
        /// To get number of booking from a Email in no of days
        /// </summary>
        /// <param name="email"></param>
        /// <param name="noOfDays"></param>
        /// <returns></returns>
        public static int GetBookingCountFromEmail(string email, int noOfDays)
        {
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@email", email);
            paramList[1] = new SqlParameter("@noOfDays", noOfDays);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetBookingCountFromEmail, paramList, connection);
            int bookingCount = 0;
            if (data.Read())
            {
                bookingCount = Convert.ToInt32(data["count"]);
            }
            data.Close();
            connection.Close();
            return bookingCount;
        }

        public static int TestPNR(string pnr)
        {
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@PNR", pnr);

            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.TestUniquePNRInITimesBookingDetails, paramList, connection);
            int numberOfRows = -1;
            if (data.Read())
            {
                numberOfRows = Convert.ToInt32(data["Count"]);
            }
            data.Close();
            connection.Close();
            if (numberOfRows < 0)
            {
                throw new ArgumentException("Unable to read data from ItimesBookingDetails", "TestPNR");
            }
            return numberOfRows;
        }

        public static string LoadBookingPanelCSV(string whereString)
        {
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@whereString", whereString);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetAllITimesBookingDetails, paramList, connection);
            StringBuilder csvString = new StringBuilder();
            csvString.Append("TripId,OutPNR,InPNR,Date,Amount,Gateway,Payment ID,Sector,Pax,AirlineCodeOut,FlightNumberOut,AirlineCodeIn,FlightNumberIn,DepartureDate,ReturnDate,BookingStatus,Phone,Email,\n");
            if (data.HasRows)
            {
                while (data.Read())
                {
                    csvString.Append(Convert.ToString(data["tripId"]) + ",");
                    if (data["outPNR"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToString(data["outPNR"]) + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    if (data["inPNR"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToString(data["inPNR"]) + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    csvString.Append(Util.UTCToIST(Convert.ToDateTime(data["createdOn"])).ToString("dd/MM/yy HH:mm") + ",");
                    if (data["paymentAmount"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToDecimal(data["paymentAmount"]).ToString() + ",");
                    }
                    else
                    {
                        csvString.Append("0.00,");
                    }
                    csvString.Append(((PaymentGatewaySource)(Enum.Parse(typeof(PaymentGatewaySource), data["paymentGatewaySourceId"].ToString()))).ToString() + ",");
                    if (data["paymentId"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToString(data["paymentId"]) + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    if (data["sector"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToString(data["sector"]) + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    int paxCounter = 0;
                    PaxDetail pax = new PaxDetail();
                    if (data["passengerInfo"] != null)
                    {
                        string temp = Convert.ToString(data["passengerInfo"]).Replace("xmlns=\"http://192.168.0.170/TT/BookingAPI\"", string.Empty);
                        TextReader text = new StringReader(temp);
                        XmlDocument doc = new XmlDocument();
                        doc.Load(text);
                        XmlNodeList nodeListPax = doc.SelectNodes("/Passenger/WSPassenger");
                        foreach (XmlNode node in nodeListPax)
                        {
                            if (node.HasChildNodes)
                            {
                                if (paxCounter == 0)
                                {
                                    if (node.SelectSingleNode("Title") != null)
                                    {
                                        pax.Title = node.SelectSingleNode("Title").InnerText;
                                    }
                                    if (node.SelectSingleNode("FirstName") != null)
                                    {
                                        pax.FirstName = node.SelectSingleNode("FirstName").InnerText;
                                    }
                                    if (node.SelectSingleNode("LastName") != null)
                                    {
                                        pax.LastName = node.SelectSingleNode("LastName").InnerText;
                                    }
                                }
                                paxCounter++;
                            }
                        }
                    }
                    if (pax.FirstName != null && pax.FirstName != "")
                    {
                        csvString.Append(pax.Title + " " + pax.FirstName + " " + pax.LastName);
                        if (paxCounter > 1)
                        {
                            csvString.Append(" x " + paxCounter + ",");
                        }
                        else
                        {
                            csvString.Append(" ,");
                        }
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    csvString.Append(Convert.ToString(data["airlineCodeOut"]) + ",");
                    csvString.Append(Convert.ToString(data["flightNumberOut"]) + ",");
                    if (data["airlineCodeIn"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToString(data["airlineCodeIn"]) + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    if (data["flightNumberIn"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToString(data["flightNumberIn"]) + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    csvString.Append(Convert.ToDateTime(data["depDate"]).ToString("dd/MM/yy HH:mm") + ",");
                    if (data["returnDate"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToDateTime(data["returnDate"]).ToString("dd/MM/yy HH:mm") + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    csvString.Append(((ITimesBookingStatus)(Enum.Parse(typeof(ITimesBookingStatus), data["bookingStatus"].ToString()))).ToString() + ",");
                    if (data["phone"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToString(data["phone"]) + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    } 
                    if (data["email"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToString(data["email"]) + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    csvString.Append("\n");
                }
            }
            data.Close();
            connection.Close();
            return csvString.ToString();
        }

        public static List<ITimesBookingDetail> Load(string whereString)
        {
            List<ITimesBookingDetail> tempList = new List<ITimesBookingDetail>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            //paramList[0] = new SqlParameter("@rowNo", pageNo * 100);
            paramList[0] = new SqlParameter("@whereString", whereString);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetAllITimesBookingDetails, paramList, connection);
            if (data.HasRows)
            {
                while (data.Read())
                {
                    ITimesBookingDetail itbd = new ITimesBookingDetail();
                    itbd.tripId = Convert.ToInt32(data["tripId"]);
                    if (data["bookingIdOut"] != DBNull.Value)
                    {
                        itbd.bookingIdOut = Convert.ToInt32(data["bookingIdOut"]);
                    }
                    else
                    {
                        itbd.bookingIdOut = 0;
                    }
                    if (data["bookingIdIn"] != DBNull.Value)
                    {
                        itbd.bookingIdIn = Convert.ToInt32(data["bookingIdIn"]);
                    }
                    else
                    {
                        itbd.bookingIdIn = 0;
                    }
                    if (data["outPNR"] != DBNull.Value)
                    {
                        itbd.outPNR = Convert.ToString(data["outPNR"]);
                    }
                    else
                    {
                        itbd.outPNR = string.Empty;
                    }
                    if (data["inPNR"] != DBNull.Value)
                    {
                        itbd.inPNR = Convert.ToString(data["inPNR"]);
                    }
                    else
                    {
                        itbd.inPNR = string.Empty;
                    }
                    if (data["paymentId"] != DBNull.Value)
                    {
                        itbd.paymentId = Convert.ToString(data["paymentId"]);
                    }
                    else
                    {
                        itbd.paymentId = string.Empty;
                    }
                    itbd.paySource = (PaymentGatewaySource)(Enum.Parse(typeof(PaymentGatewaySource), data["paymentGatewaySourceId"].ToString()));
                    if (data["sector"] != DBNull.Value)
                    {
                        itbd.sector = Convert.ToString(data["sector"]);
                    }
                    else
                    {
                        itbd.sector = string.Empty;
                    }
                    if (data["passengerInfo"] != null)
                    {
                        itbd.passengerInfo = Convert.ToString(data["passengerInfo"]);
                    }
                    else
                    {
                        itbd.passengerInfo = string.Empty;
                    }
                    if (data["ipAddress"] != DBNull.Value)
                    {
                        itbd.ipAddress = Convert.ToString(data["ipAddress"]);
                    }
                    else
                    {
                        itbd.ipAddress = string.Empty;
                    }
                    if (data["email"] != DBNull.Value)
                    {
                        itbd.email = Convert.ToString(data["email"]);
                    }
                    else
                    {
                        itbd.email = string.Empty;
                    }
                    if (data["phone"] != DBNull.Value)
                    {
                        itbd.phone = Convert.ToString(data["phone"]);
                    }
                    else
                    {
                        itbd.phone = string.Empty;
                    }
                    if (data["customerStatus"] != DBNull.Value)
                    {
                        itbd.customerStatus = (CustomerStatus)Enum.Parse(typeof(CustomerStatus), data["customerStatus"].ToString());
                    }
                    itbd.bookingStatus = (ITimesBookingStatus)(Enum.Parse(typeof(ITimesBookingStatus), data["bookingStatus"].ToString()));
                    itbd.status = (IPAddressStatus)(Enum.Parse(typeof(IPAddressStatus), data["status"].ToString()));
                    if (data["isDomesticReturn"] != DBNull.Value)
                    {
                        itbd.isDomesticReturn = Convert.ToBoolean(data["isDomesticReturn"]);
                    }
                    itbd.createdOn = Convert.ToDateTime(data["createdOn"]);
                    if (data["paymentAmount"] != DBNull.Value)
                    {
                        itbd.paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                    }
                    else
                    {
                        itbd.paymentAmount = 0;
                    }
                    itbd.outTicketed = Convert.ToBoolean(data["outTicketed"]);
                    if (data["inTicketed"] != DBNull.Value)
                    {
                        itbd.inTicketed = Convert.ToBoolean(data["inTicketed"]);
                    }
                    itbd.agencyId = Convert.ToInt32(data["agencyId"]);
                    if (data["airlineCodeIn"] != DBNull.Value)
                    {
                        itbd.airlineCodeIn = Convert.ToString(data["airlineCodeIn"]);
                    }
                    itbd.airlineCodeOut = Convert.ToString(data["airlineCodeOut"]);
                    if (data["flightNumberIn"] != DBNull.Value)
                    {
                        itbd.flightNumberIn = Convert.ToString(data["flightNumberIn"]);
                    }
                    itbd.flightNumberOut = Convert.ToString(data["flightNumberOut"]);
                    itbd.depDate = Convert.ToDateTime(data["depDate"]);
                    if (data["returnDate"] != DBNull.Value)
                    {
                        itbd.returnDate = Convert.ToDateTime(data["returnDate"]);
                    }
                    if (data["remarks"] != DBNull.Value)
                    {
                        itbd.remarks = Convert.ToString(data["remarks"]);
                    }
                    else
                    {
                        itbd.remarks = string.Empty;
                    }
                    itbd.isDomestic = Convert.ToBoolean(data["isDomestic"]);
                    itbd.createdBy = Convert.ToInt32(data["createdBy"]);
                    itbd.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    itbd.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                    tempList.Add(itbd);
                }
            }
            data.Close();
            connection.Close();
            return tempList;
        }

        /// <summary>
        /// Gets if a cash receipt has already been generetaed for the given PNR
        /// </summary>
        /// <param name="pnr">PNR</param>
        /// <returns>bool isReceiptGenerated</returns>
        public static bool IsReceiptGeneratedForPnr(string pnr)
        {
            if (pnr == null || pnr == "")
            {
                throw new ArgumentException("Pnr cannot be null", "pnr");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader data = null;
            bool isReceiptGenerated = false;
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@pnr", pnr);
            try
            {
                data = Dal.ExecuteReaderSP(SPNames.GetIsReceiptGeneratedForPnr, paramList, connection);
                if (data.Read())
                {
                    isReceiptGenerated = Convert.ToBoolean(data["result"]);
                }
            }
            catch (Exception ex)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.IndiaTimesBookingDetailsTicketing, CoreLogic.Severity.High, 0, "Unable to obtain if receipt is generated for the PNR: " + pnr + "Error: " + ex.Message + " | " + ex.StackTrace + " | " + DateTime.Now, "");
                isReceiptGenerated = false;
                throw new Exception("Unable to obtain if receipt is generated for the PNR: " + pnr);
            }
            finally
            {
                data.Close();
                connection.Close();
            }
            return isReceiptGenerated;
        }
    }
}
