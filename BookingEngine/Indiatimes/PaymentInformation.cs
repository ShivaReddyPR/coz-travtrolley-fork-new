using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using Technology.Data;

namespace Technology.Indiatimes
{
    public class PaymentInformation
    {
        #region private member
        private int paymentInformationId;
        private string paymentId;
        private int customerId;
        private decimal amount;
        private PaymentSource paymentSource;
        private string trackId;
        private string remarks;
        private Payment_Status paymentStatus;
        private string requestId;
        private PaymentRequestType paymentRequestType;
        #endregion


        #region public properties
        public int PaymentInformationId
        {
            get
            {
                return paymentInformationId;
            }
            set
            {
                paymentInformationId = value;
            }
        }

        public string PaymentId
        {
            get
            {
                return paymentId;
            }
            set
            {
                paymentId = value;
            }
        }

        public int CustomerId
        {
            get
            {
                return customerId;
            }
            set
            {
                customerId = value;
            }
        }

        public decimal Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }

        public PaymentSource PaymentSource
        {
            get
            {
                return paymentSource;
            }
            set
            {
                paymentSource = value;
            }
        }

        public string TrackId
        {
            get
            {
                return trackId;
            }
            set
            {
                trackId = value;
            }
        }

        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }

        public Payment_Status PaymentStatus
        {
            get
            {
                return paymentStatus;
            }
            set
            {
                paymentStatus = value;
            }
        }

        public string RequestId
        {
            get
            {
                return requestId;
            }
            set
            {
                requestId = value;
            }
        }

        public PaymentRequestType PaymentRequestType
        {
            get
            {
                return paymentRequestType;
            }
            set
            {
                paymentRequestType = value;
            }
        }
        #endregion

        #region public method

        /// <summary>
        /// Zero Argument Constructor
        /// </summary>
        public PaymentInformation()
        {

        }

        /// <summary>
        /// Constructor of PaymentInformation, takes paymentInformationId as parameter
        /// </summary>
        /// <param name="paymentInformationId">paymentInformationId of PaymentInformation</param>
        public PaymentInformation(int paymentInformationId)
        {
            Load(paymentInformationId);
        }

        /// <summary>
        /// function to Load PaymentInformation by paymentInformationId
        /// </summary>
        /// <param name="agencyId">paymentInformationId of PaymentInformation</param>
        public void Load(int paymentInformationId)
        {
            if (paymentInformationId <= 0)
            {
                throw new ArgumentException("paymentInformation Id must be greater than Zero");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paymentInformationId", paymentInformationId);
            SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetPaymentInformationByPaymentInformationId, paramList, connection);
            if (dataReader.Read())
            {
                this.paymentInformationId = (int)dataReader["paymentInformationId"];
                if (dataReader["paymentId"] != DBNull.Value)
                {
                    paymentId = (String)dataReader["paymentId"];
                }
                else
                {
                    paymentId = string.Empty;
                }
                customerId = (int)dataReader["customerId"];
                amount = (decimal)dataReader["amount"];
                paymentSource = (PaymentSource)Convert.ToInt32(dataReader["paymentSourceId"]);
                trackId = (String)dataReader["trackId"];
                remarks = (String)dataReader["remarks"];
                paymentStatus = (Payment_Status)Convert.ToInt32(dataReader["paymentStatusId"]);
                if (dataReader["requestId"] != DBNull.Value)
                {
                    requestId = (String)dataReader["requestId"];
                }
                else
                {
                    requestId = string.Empty;
                } 
                paymentRequestType = (PaymentRequestType)Convert.ToInt32(dataReader["paymentRequestTypeId"]);
                dataReader.Close();
                connection.Close();
            }
            else
            {
                dataReader.Close();
                connection.Close();
                throw new ArgumentException("Payment information does not exist : paymentInformationId = " + paymentInformationId);
            }
        }

        public void Save()
        {
            int retVal;
            if (customerId < 0)
            {
                throw new ArgumentException("customerId must be a positive integer and greater then Zero, customerId :" + customerId);
            }
            if (amount <= 0)
            {
                throw new ArgumentException("amount must not be negative or zero");
            }
            if (remarks == null || remarks.Length == 0)
            {
                throw new ArgumentException("remarks must not be null or of Length Zero");
            }
            if (trackId == null || trackId.Length == 0)
            {
                throw new ArgumentException("trackId must not be null or of Length Zero");
            }
            if (Convert.ToInt32(paymentSource) <= 0 || paymentSource.ToString().Length == 0)
            {
                throw new ArgumentException("paymentSource value is not valid.");
            }
            if (Convert.ToInt32(paymentStatus) <= 0 || paymentStatus.ToString().Length == 0)
            {
                throw new ArgumentException("paymentStatus value is not valid.");
            }
            SqlParameter[] paramList = new SqlParameter[10];
            paramList[0] = new SqlParameter("@paymentInformationId", paymentInformationId);
            paramList[1] = new SqlParameter("@paymentId", paymentId);
            paramList[2] = new SqlParameter("@customerId", customerId);
            paramList[3] = new SqlParameter("@amount", amount);
            paramList[4] = new SqlParameter("@paymentSourceId", (int)paymentSource);
            paramList[5] = new SqlParameter("@trackId", trackId);
            paramList[6] = new SqlParameter("@remarks", remarks);
            paramList[7] = new SqlParameter("@paymentStatusId", (int)paymentStatus);
            if (requestId != null)
            {
                paramList[8] = new SqlParameter("@requestId", requestId);
            }
            else
            {
                paramList[8] = new SqlParameter("@requestId", DBNull.Value);
            }
            paramList[9] = new SqlParameter("@paymentRequestTypeId", (int)paymentRequestType);
            if (paymentInformationId > 0)
            {
                retVal = Dal.ExecuteNonQuerySP(SPNames.UpdateIndiatimesPayment, paramList);
                if (retVal <= 0)
                {
                    throw new ArgumentException("This payment for trackId: " + trackId + " has not Updated.");
                }
            }
            else
            {
                paramList[0].Direction = ParameterDirection.Output;
                retVal = Dal.ExecuteNonQuerySP(SPNames.AddIndiatimesPayment, paramList);
                paymentInformationId = (int)paramList[0].Value;
                if (paymentInformationId == 0)
                {
                    throw new ArgumentException("This payment for trackId: " + trackId + " has not saved.");
                }
            }
        }

        /// <summary>
        /// Method to retrieve Payments information based on customerId
        /// </summary>
        /// <param name="customerId">customerId of Member</param>
        /// <returns>array of BookingDetails object</returns>
        public static PaymentInformation[] GetPaymentsByCustomerId(int customerId)
        {
            if (customerId <= 0)
            {
                throw new ArgumentException("customerId must be a positive integer and greater then Zero, customerId :" + customerId);
            }
            DataSet dataset;
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@customerId", customerId);
            dataset = Dal.FillSP(SPNames.GetPaymentsByCustomerId, paramList);
            PaymentInformation[] payments = new PaymentInformation[0];
            if (dataset.Tables.Count > 0)
            {
                payments = ReadData(dataset.Tables[0]);
            }
            return payments;
        }

        private static PaymentInformation[] ReadData(DataTable table)
        {
            int count = table.Rows.Count;
            PaymentInformation[] payments = new PaymentInformation[count];
            int i = 0;
            foreach (DataRow row in table.Rows)
            {
                payments[i] = new PaymentInformation();
                payments[i].paymentInformationId = Convert.ToInt32(row["paymentInformationId"]);
                if (row["paymentId"] != DBNull.Value)
                {
                    payments[i].paymentId = Convert.ToString(row["paymentId"]);
                }
                else
                {
                    payments[i].paymentId = string.Empty;
                }
                payments[i].customerId = Convert.ToInt32(row["customerId"]);
                payments[i].amount = Convert.ToDecimal(row["amount"]);
                payments[i].paymentSource = (PaymentSource)Convert.ToInt32(row["paymentSourceId"]);
                payments[i].trackId = Convert.ToString(row["trackId"]);
                payments[i].remarks = Convert.ToString(row["remarks"]);
                payments[i].paymentStatus = (Payment_Status)Convert.ToInt32(row["paymentStatusId"]);
                if (row["requestId"] != DBNull.Value)
                {
                    payments[i].requestId = Convert.ToString(row["requestId"]);
                }
                else
                {
                    payments[i].requestId = string.Empty;
                } 
                payments[i].paymentRequestType = (PaymentRequestType)Convert.ToInt32(row["paymentRequestTypeId"]);
                i++;
            }
            return payments;
        }

        public static string GetWhereStringForPayment(FilterPaymentBy filterPaymentBy, string searchString)
        {
            string whereString = string.Empty;
            if (filterPaymentBy == FilterPaymentBy.ChangeRequestId)
            {
                whereString = " where requestId = '" + searchString + "'" + " and paymentRequestTypeId = 1";
            }
            else if (filterPaymentBy == FilterPaymentBy.PaymentId)
            {
                whereString = " where paymentId = '" + searchString + "'";
            }
            else if (filterPaymentBy == FilterPaymentBy.CustomerEmailId)
            {
                whereString = " where customerId in (select customerId from B2C_Customer where username like '" + searchString + "')";
            }   
            else
            {
                whereString = " ";
            }
            return whereString;
        }

        public static int GetTotalFilteredPaymentsCount(string whereString, string orderByString)
        {
            DataSet dataset = new DataSet();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@whereString", whereString);
            paramList[1] = new SqlParameter("@orderByString", orderByString);
            dataset = Dal.FillSP(SPNames.GetTotalFilteredPaymentsCount, paramList);
            int queueCount = 0;
            if (dataset.Tables.Count != 0)
            {
                queueCount = Convert.ToInt32(dataset.Tables[0].Rows[0]["count"]);
            }
            dataset.Dispose();
            return queueCount;
        }

        public static PaymentInformation[] GetTotalFilteredPayments(int pageNumber, int noOfRecordsPerPage, int totalrecords, string whereString, string orderByString)
        {
            DataSet dataset = new DataSet();
            SqlParameter[] paramList = new SqlParameter[4];
            int endrow = 0;
            int startrow = ((noOfRecordsPerPage * (pageNumber - 1)) + 1);
            if ((startrow + noOfRecordsPerPage) - 1 < totalrecords)
            {
                endrow = (startrow + noOfRecordsPerPage) - 1;
            }
            else
            {
                endrow = totalrecords;
            }
            paramList[0] = new SqlParameter("@whereString", whereString);
            paramList[1] = new SqlParameter("@orderByString", orderByString);
            paramList[2] = new SqlParameter("@startrow", startrow);
            paramList[3] = new SqlParameter("@endrow", endrow);// changes for paging

            dataset = Dal.FillSP(SPNames.GetTotalFilteredPayments, paramList);


            int queueCount = 0;
            if (dataset.Tables.Count != 0)
            {
                queueCount = dataset.Tables[0].Rows.Count;
            }
            PaymentInformation[] payments = new PaymentInformation[queueCount];
            int i = 0;
            foreach (DataRow row in dataset.Tables[0].Rows)
            {
                payments[i] = new PaymentInformation();
                payments[i].paymentInformationId = Convert.ToInt32(row["paymentInformationId"]);
                if (row["paymentId"] != DBNull.Value)
                {
                    payments[i].paymentId = Convert.ToString(row["paymentId"]);
                }
                else
                {
                    payments[i].paymentId = string.Empty;
                }
                payments[i].customerId = Convert.ToInt32(row["customerId"]);
                payments[i].amount = Convert.ToDecimal(row["amount"]);
                payments[i].paymentSource = (PaymentSource)Convert.ToInt32(row["paymentSourceId"]);
                payments[i].trackId = Convert.ToString(row["trackId"]);
                payments[i].remarks = Convert.ToString(row["remarks"]);
                payments[i].paymentStatus = (Payment_Status)Convert.ToInt32(row["paymentStatusId"]);
                if (row["requestId"] != DBNull.Value)
                {
                    payments[i].requestId = Convert.ToString(row["requestId"]);
                }
                else
                {
                    payments[i].requestId = string.Empty;
                } 
                payments[i].paymentRequestType = (PaymentRequestType)Convert.ToInt32(row["paymentRequestTypeId"]);
                i++;
            }
            dataset.Dispose();
            return payments;
        }
        #endregion
    }

    public enum PaymentSource
    {
        HDFC = 1,
        AMEX = 2,
        ICICI = 3,
        OXICASH = 4,
        APICustomer = 5,
        SBI = 6,
        CCAvenue = 7,
        CozmoPG = 13
    }

    public enum Payment_Status
    {
        Accepted = 1,
        InProcess = 2,
        Rejected = 3,
    }

    public enum FilterPaymentBy
    {
        NoFilter = 1,
        ChangeRequestId = 2,
        PaymentId = 3,
        CustomerEmailId = 4
    }

    public enum PaymentRequestType
    {
        ChangeRequest = 1,
        TripRequest = 2,
        DirectPayment = 3
    }
}
