using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Web;

namespace Technology.Indiatimes
{
    public class IndiaTimesHotel
    {
        #region privateFields
        int agencyId;
        int dealId;
        string dealName;
        string city;
        string country;
        string description;
        bool isInternational;
        int nights;
        string mainImagePath;
        string imagePath;
        string thumbnailPath;
        bool hasAirfare;
        bool hasHotel;
        bool hasSightSeeing;
        bool hasMeals;
        bool hasTransport;
        string overview;
        string inclusions;
        string itinerary1;
        string itinerary2;
        string itinerary3;
        string itinerary4;
        string itinerary5;
        string itinerary6;
        string itinerary7;
        string itinerary8;
        string itinerary9;
        string itinerary10;
        string itinerary11;
        string notes;
        string priceExclude;
        string termsAndConditions;
        int priority;
        int paging;
        int leftDeal1;
        int leftDeal2;
        bool isActive;
        bool isDeleted;
        int createdBy;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        List<IndiaTimesHotelSeason> hotelSeasonList;
        int leftDeal1Nights;
        int leftDeal2Nights;
        string leftDeal1Name;
        string leftDeal2Name;
        #endregion

        #region publicProperties
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId= value;
            }
        }

        
        public int DealId
        {
            get
            {
                return dealId;
            }
            set
            {
                dealId = value;
            }
        }

        public string DealName
        {
            get
            {
                return dealName;
            }
            set
            {
                dealName = value;
            }
        }

        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        public string Country
        {
            get
            {
                return country;
            }
            set
            {
                country = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public bool IsInternational
        {
            get
            {
                return isInternational;
            }
            set
            {
                isInternational = value;
            }
        }

        public int Nights
        {
            get
            {
                return nights;
            }
            set
            {
                nights = value;
            }
        }

        public string MainImagePath
        {
            get
            {
                return mainImagePath;
            }
            set
            {
                mainImagePath = value;
            }
        }

        public string ImagePath
        {
            get
            {
                return imagePath;
            }
            set
            {
                imagePath = value;
            }
        }

        public string ThumbnailPath
        {
            get
            {
                return thumbnailPath;
            }
            set
            {
                thumbnailPath = value;
            }
        }

        public bool HasAirfare
        {
            get
            {
                return hasAirfare;
            }
            set
            {
                hasAirfare = value;
            }
        }

        public bool HasHotel
        {
            get
            {
                return hasHotel;
            }
            set
            {
                hasHotel = value;
            }
        }

        public bool HasSightSeeing
        {
            get
            {
                return hasSightSeeing;
            }
            set
            {
                hasSightSeeing = value;
            }
        }

        public bool HasMeals
        {
            get
            {
                return hasMeals;
            }
            set
            {
                hasMeals = value;
            }
        }

        public bool HasTransport
        {
            get
            {
                return hasTransport;
            }
            set
            {
                hasTransport = value;
            }
        }

        public string Overview
        {
            get
            {
                return overview;
            }
            set
            {
                overview = value;
            }
        }

        public string Inclusions
        {
            get
            {
                return inclusions;
            }
            set
            {
                inclusions = value;
            }
        }

        public string Itinerary1
        {
            get
            {
                return itinerary1;
            }
            set
            {
                itinerary1 = value;
            }
        }

        public string Itinerary2
        {
            get
            {
                return itinerary2;
            }
            set
            {
                itinerary2 = value;
            }
        }

        public string Itinerary3
        {
            get
            {
                return itinerary3;
            }
            set
            {
                itinerary3 = value;
            }
        }

        public string Itinerary4
        {
            get
            {
                return itinerary4;
            }
            set
            {
                itinerary4 = value;
            }
        }

        public string Itinerary5
        {
            get
            {
                return itinerary5;
            }
            set
            {
                itinerary5 = value;
            }
        }

        public string Itinerary6
        {
            get
            {
                return itinerary6;
            }
            set
            {
                itinerary6 = value;
            }
        }

        public string Itinerary7
        {
            get
            {
                return itinerary7;
            }
            set
            {
                itinerary7 = value;
            }
        }

        public string Itinerary8
        {
            get
            {
                return itinerary8;
            }
            set
            {
                itinerary8 = value;
            }
        }

        public string Itinerary9
        {
            get
            {
                return itinerary9;
            }
            set
            {
                itinerary9 = value;
            }
        }

        public string Itinerary10
        {
            get
            {
                return itinerary10;
            }
            set
            {
                itinerary10 = value;
            }
        }

        public string Itinerary11
        {
            get
            {
                return itinerary11;
            }
            set
            {
                itinerary11 = value;
            }
        }

        public string Notes
        {
            get
            {
                return notes;
            }
            set
            {
                notes = value;
            }
        }

        public string PriceExclude
        {
            get
            {
                return priceExclude;
            }
            set
            {
                priceExclude = value;
            }
        }

        public string TermsAndConditions
        {
            get
            {
                return termsAndConditions;
            }
            set
            {
                termsAndConditions = value;
            }
        }

        public int Priority
        {
            get
            {
                return priority;
            }
            set
            {
                priority = value;
            }
        }

        public int Paging
        {
            get
            {
                return paging;
            }
            set
            {
                paging = value;
            }
        }

        public int LeftDeal1
        {
            get
            {
                return leftDeal1;
            }
            set
            {
                leftDeal1 = value;
            }
        }

        public int LeftDeal1Nights
        {
            get
            {
                return leftDeal1Nights;
            }
            set
            {
                leftDeal1Nights = value;
            }
        }

        public int LeftDeal2
        {
            get
            {
                return leftDeal2;
            }
            set
            {
                leftDeal2 = value;
            }
        }

        public int LeftDeal2Nights
        {
            get
            {
                return leftDeal2Nights;
            }
            set
            {
                leftDeal2Nights = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }

        public bool IsDeleted
        {
            get
            {
                return isDeleted;
            }
            set
            {
                isDeleted = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        public List<IndiaTimesHotelSeason> HotelSeasonList
        {
            get
            {
                return hotelSeasonList;
            }
            set
            {
                hotelSeasonList = value;
            }
        }

        public string LeftDeal1Name
        {
            get
            {
                return leftDeal1Name;
            }
            set
            {
                leftDeal1Name = value;
            }
        }

        public string LeftDeal2Name
        {
            get
            {
                return leftDeal2Name;
            }
            set
            {
                leftDeal2Name = value;
            }
        }

        #endregion

        #region publicMethods
        /// <summary>
        /// Saves and updates hotel deal pakage.
        /// </summary>
        /// <returns>dealId when save and number of rows affected when update</returns>
        public int Save()
        {
            int dealIdReturnValue = 0;
            SqlParameter[] paramList;
            if (dealId == 0)
            {
                paramList = new SqlParameter[33];
            }
            else
            {
                paramList = new SqlParameter[30];
            }

            paramList[0] = new SqlParameter("@dealName", dealName);
            paramList[1] = new SqlParameter("@city", city);
            paramList[2] = new SqlParameter("@country", country);
            paramList[3] = new SqlParameter("@description", description);
            paramList[4] = new SqlParameter("@isInternational", isInternational);
            paramList[5] = new SqlParameter("@nights", nights);
            paramList[6] = new SqlParameter("@hasAirfare", hasAirfare);
            paramList[7] = new SqlParameter("@hasHotel", hasHotel);
            paramList[8] = new SqlParameter("@hasSightSeeing", hasSightSeeing);
            paramList[9] = new SqlParameter("@hasMeals", hasMeals);
            paramList[10] = new SqlParameter("@hasTransport", hasTransport);
            paramList[11] = new SqlParameter("@overview", overview);
            paramList[12] = new SqlParameter("@inclusions", inclusions);
            if (itinerary1 != null && itinerary1.Trim() != "")
            {
                paramList[13] = new SqlParameter("@itinerary1", itinerary1);
            }
            else
            {
                paramList[13] = new SqlParameter("@itinerary1", DBNull.Value);
            }
            if (itinerary2 != null && itinerary2.Trim() != "")
            {
                paramList[14] = new SqlParameter("@itinerary2", itinerary2);
            }
            else
            {
                paramList[14] = new SqlParameter("@itinerary2", DBNull.Value);
            }
            if (itinerary3 != null && itinerary3.Trim() != "")
            {
                paramList[15] = new SqlParameter("@itinerary3", itinerary3);
            }
            else
            {
                paramList[15] = new SqlParameter("@itinerary3", DBNull.Value);
            }
            if (itinerary4 != null && itinerary4.Trim() != "")
            {
                paramList[16] = new SqlParameter("@itinerary4", itinerary4);
            }
            else
            {
                paramList[16] = new SqlParameter("@itinerary4", DBNull.Value);
            }
            if (itinerary5 != null && itinerary5.Trim() != "")
            {
                paramList[17] = new SqlParameter("@itinerary5", itinerary5);
            }
            else
            {
                paramList[17] = new SqlParameter("@itinerary5", DBNull.Value);
            }
            if (itinerary6 != null && itinerary6.Trim() != "")
            {
                paramList[18] = new SqlParameter("@itinerary6", itinerary6);
            }
            else
            {
                paramList[18] = new SqlParameter("@itinerary6", DBNull.Value);
            }
            if (itinerary7 != null && itinerary7.Trim() != "")
            {
                paramList[19] = new SqlParameter("@itinerary7", itinerary7);
            }
            else
            {
                paramList[19] = new SqlParameter("@itinerary7", DBNull.Value);
            }
            if (itinerary8 != null && itinerary8.Trim() != "")
            {
                paramList[20] = new SqlParameter("@itinerary8", itinerary8);
            }
            else
            {
                paramList[20] = new SqlParameter("@itinerary8", DBNull.Value);
            }
            if (itinerary9 != null && itinerary9.Trim() != "")
            {
                paramList[21] = new SqlParameter("@itinerary9", itinerary9);
            }
            else
            {
                paramList[21] = new SqlParameter("@itinerary9", DBNull.Value);
            }
            if (itinerary10 != null && itinerary10.Trim() != "")
            {
                paramList[22] = new SqlParameter("@itinerary10", itinerary10);
            }
            else
            {
                paramList[22] = new SqlParameter("@itinerary10", DBNull.Value);
            }
            if (itinerary11 != null && itinerary11.Trim() != "")
            {
                paramList[23] = new SqlParameter("@itinerary11", itinerary11);
            }
            else
            {
                paramList[23] = new SqlParameter("@itinerary11", DBNull.Value);
            }
            if (notes != null && notes.Trim() != "")
            {
                paramList[24] = new SqlParameter("@notes", notes);
            }
            else
            {
                paramList[24] = new SqlParameter("@notes", DBNull.Value);
            }
            if (priceExclude != null && priceExclude.Trim() != "")
            {
                paramList[25] = new SqlParameter("@priceExclude", priceExclude);
            }
            else
            {
                paramList[25] = new SqlParameter("@priceExclude", DBNull.Value);
            }
            paramList[26] = new SqlParameter("@termsAndConditions", termsAndConditions);
            paramList[27] = new SqlParameter("@dealId", dealId);
            paramList[28] = new SqlParameter("@agencyId", agencyId);

            if (dealId == 0)
            {
                paramList[27].Direction = ParameterDirection.Output;
                paramList[29] = new SqlParameter("@createdBy", createdBy);
                if (mainImagePath != null && mainImagePath.Trim() != "")
                {
                    paramList[30] = new SqlParameter("@mainImagePath", mainImagePath);
                }
                else
                {
                    paramList[30] = new SqlParameter("@mainImagePath", DBNull.Value);
                }
                if (imagePath != null && imagePath.Trim() != "")
                {
                    paramList[31] = new SqlParameter("@imagePath", imagePath);
                }
                else
                {
                    paramList[31] = new SqlParameter("@imagePath", DBNull.Value);
                }
                if (thumbnailPath != null && thumbnailPath.Trim() != "")
                {
                    paramList[32] = new SqlParameter("@thumbnailPath", thumbnailPath);
                }
                else
                {
                    paramList[32] = new SqlParameter("@thumbnailPath", DBNull.Value);
                }

                try
                {
                    using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                    {
                        Dal.ExecuteNonQuerySP(SPNames.IndiaTimesHotelSave, paramList);
                        for (int i = 0; i < hotelSeasonList.Count; i++)
                        {
                            hotelSeasonList[i].DealId = (int)paramList[27].Value;
                            hotelSeasonList[i].Save();
                        }
                        updateTransaction.Complete();
                        dealIdReturnValue = (int)paramList[27].Value;
                    }
                }
                catch (Exception ex)
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during Save() in IndiaTimesHotel.cs | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    dealIdReturnValue = 0;
                }
            }
            else
            {
                paramList[28] = new SqlParameter("@createdBy", lastModifiedBy);

                try
                {
                    using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                    {
                        dealIdReturnValue = Dal.ExecuteNonQuerySP(SPNames.UpdateIndiaTimesHotel, paramList);
                        for (int i = 0; i < hotelSeasonList.Count; i++)
                        {
                            hotelSeasonList[i].DealId = dealId;
                            hotelSeasonList[i].Save();
                        }
                        updateTransaction.Complete();
                    }
                }
                catch (Exception ex)
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during update in Save() in IndiaTimesHotel.cs for dealId=" + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    dealIdReturnValue = 0;
                }
            }
            return dealIdReturnValue;
        }

        /// <summary>
        /// updates image path in IndiaTimesHotel table
        /// </summary>
        /// <param name="mainImagePath">Main image path</param>
        /// <param name="imagePath">Side image path</param>
        /// <param name="thumbnailPath">Thumbnail image path</param>
        /// <param name="hotelDealId">Deal Id against update to be made.</param>
        /// <returns>no of rows affected</returns>
        public static int UpdateImagePath(string mainImagePath, string imagePath, string thumbnailPath, int hotelDealId)// check to use...
        {
            int rowsAffected = 0;
            if (mainImagePath == null || mainImagePath.Trim().Length <= 0)
            {
                throw new ArgumentException("mainImagePath should not be null or empty string", "mainImagePath");
            }
            if (imagePath == null || imagePath.Trim().Length <= 0)
            {
                throw new ArgumentException("imagePath should not be null or empty string", "imagePath");
            }
            if (thumbnailPath == null || thumbnailPath.Trim().Length <= 0)
            {
                throw new ArgumentException("thumbnailPath should not be null or empty string", "thumbnailPath");
            }
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@mainImagePath", mainImagePath);
            paramList[1] = new SqlParameter("@imagePath", imagePath);
            paramList[2] = new SqlParameter("@thumbnailPath", thumbnailPath);
            paramList[3] = new SqlParameter("@dealId", (int)hotelDealId);
            try
            {
                rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateImagePath, paramList);
            }
            catch (Exception ex)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during UpdateImagePath() in IndiaTimesHotel.cs for dealId=" + hotelDealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }

        /// <summary>
        /// updates image path in IndiaTimesHotel table
        /// </summary>
        /// <param name="mainImagePath">Main image path</param>
        /// <param name="hotelDealId">Deal Id against update to be made.</param>
        /// <returns>no of rows affected</returns>
        public static int UpdateMainImagePath(string mainImagePath, int hotelDealId)
        {
            int rowsAffected = 0;
            if (mainImagePath == null || mainImagePath.Trim().Length <= 0)
            {
                throw new ArgumentException("mainImagePath should not be null or empty string", "mainImagePath");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@mainImagePath", mainImagePath);
            paramList[1] = new SqlParameter("@dealId", (int)hotelDealId);
            try
            {
                rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateMainImagePath, paramList);
            }
            catch (Exception ex)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during UpdateImagePath() in IndiaTimesHotel.cs for dealId=" + hotelDealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }

        /// <summary>
        /// updates image path in IndiaTimesHotel table
        /// </summary>
        /// <param name="mainImagePath">Main image path</param>
        /// <param name="imagePath">Side image path</param>
        /// <param name="thumbnailPath">Thumbnail image path</param>
        /// <param name="hotelDealId">Deal Id against update to be made.</param>
        /// <returns>no of rows affected</returns>
        public static int UpdateSideImagePath(string imagePath, int hotelDealId)
        {
            int rowsAffected = 0;
            if (imagePath == null || imagePath.Trim().Length <= 0)
            {
                throw new ArgumentException("imagePath should not be null or empty string", "imagePath");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@imagePath", imagePath);
            paramList[1] = new SqlParameter("@dealId", (int)hotelDealId);
            try
            {
                rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateSideImagePath, paramList);
            }
            catch (Exception ex)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during UpdateImagePath() in IndiaTimesHotel.cs for dealId=" + hotelDealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }

        /// <summary>
        /// updates image path in IndiaTimesHotel table
        /// </summary>
        /// <param name="thumbnailPath">Thumbnail image path</param>
        /// <param name="hotelDealId">Deal Id against update to be made.</param>
        /// <returns>no of rows affected</returns>
        public static int UpdateThumbnailImagePath(string thumbnailPath, int hotelDealId)
        {
            int rowsAffected = 0;
            if (thumbnailPath == null || thumbnailPath.Trim().Length <= 0)
            {
                throw new ArgumentException("thumbnailPath should not be null or empty string", "thumbnailPath");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@thumbnailPath", thumbnailPath);
            paramList[1] = new SqlParameter("@dealId", (int)hotelDealId);
            try
            {
                rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateThumbnailImagePath, paramList);
            }
            catch (Exception ex)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during UpdateImagePath() in IndiaTimesHotel.cs for dealId=" + hotelDealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }

        /// <summary>
        /// Loads a specific hotel deal
        /// </summary>
        /// <param name="dealId">deal id to be loaded</param>
        /// <returns>IndiaTimesHotel object</returns>
        public static IndiaTimesHotel LoadHotelDealForEdit(int dealId)
        {
            //SqlDataReader data = new SqlDataReader();
            IndiaTimesHotel htl = new IndiaTimesHotel();
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@dealId", dealId);
                //try
                //{
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetIndiaTimesHotelDealForEdit, paramList, connection);
                if (data.Read())
                {
                    htl.dealId = Convert.ToInt32(data["dealId"]);
                    htl.dealName = Convert.ToString(data["dealName"]);
                    htl.city = Convert.ToString(data["city"]);
                    htl.country = Convert.ToString(data["country"]);
                    htl.description = Convert.ToString(data["description"]);
                    htl.isInternational = Convert.ToBoolean(data["isInternational"]);
                    htl.nights = Convert.ToInt32(data["nights"]);
                    htl.mainImagePath = Convert.ToString(data["mainImagePath"]);
                    htl.imagePath = Convert.ToString(data["imagePath"]);
                    htl.thumbnailPath = Convert.ToString(data["thumbnailPath"]);
                    htl.hasAirfare = Convert.ToBoolean(data["hasAirfare"]);
                    htl.hasHotel = Convert.ToBoolean(data["hasHotel"]);
                    htl.hasSightSeeing = Convert.ToBoolean(data["hasSightSeeing"]);
                    htl.hasMeals = Convert.ToBoolean(data["hasMeals"]);
                    htl.hasTransport = Convert.ToBoolean(data["hasTransport"]);
                    htl.overview = Convert.ToString(data["overview"]);
                    htl.inclusions = Convert.ToString(data["inclusions"]);
                    if (data["itinerary1"] != DBNull.Value)
                    {
                        htl.itinerary1 = Convert.ToString(data["itinerary1"]);
                    }
                    else
                    {
                        htl.itinerary1 = string.Empty;
                    }
                    if (data["itinerary2"] != DBNull.Value)
                    {
                        htl.itinerary2 = Convert.ToString(data["itinerary2"]);
                    }
                    else
                    {
                        htl.itinerary2 = string.Empty;
                    }
                    if (data["itinerary3"] != DBNull.Value)
                    {
                        htl.itinerary3 = Convert.ToString(data["itinerary3"]);
                    }
                    else
                    {
                        htl.itinerary3 = string.Empty;
                    }
                    if (data["itinerary4"] != DBNull.Value)
                    {
                        htl.itinerary4 = Convert.ToString(data["itinerary4"]);
                    }
                    else
                    {
                        htl.itinerary4 = string.Empty;
                    }
                    if (data["itinerary5"] != DBNull.Value)
                    {
                        htl.itinerary5 = Convert.ToString(data["itinerary5"]);
                    }
                    else
                    {
                        htl.itinerary5 = string.Empty;
                    }
                    if (data["itinerary6"] != DBNull.Value)
                    {
                        htl.itinerary6 = Convert.ToString(data["itinerary6"]);
                    }
                    else
                    {
                        htl.itinerary6 = string.Empty;
                    }
                    if (data["itinerary7"] != DBNull.Value)
                    {
                        htl.itinerary7 = Convert.ToString(data["itinerary7"]);
                    }
                    else
                    {
                        htl.itinerary7 = string.Empty;
                    }
                    if (data["itinerary8"] != DBNull.Value)
                    {
                        htl.itinerary8 = Convert.ToString(data["itinerary8"]);
                    }
                    else
                    {
                        htl.itinerary8 = string.Empty;
                    }
                    if (data["itinerary9"] != DBNull.Value)
                    {
                        htl.itinerary9 = Convert.ToString(data["itinerary9"]);
                    }
                    else
                    {
                        htl.itinerary9 = string.Empty;
                    }
                    if (data["itinerary10"] != DBNull.Value)
                    {
                        htl.itinerary10 = Convert.ToString(data["itinerary10"]);
                    }
                    else
                    {
                        htl.itinerary10 = string.Empty;
                    }
                    if (data["itinerary11"] != DBNull.Value)
                    {
                        htl.itinerary11 = Convert.ToString(data["itinerary11"]);
                    }
                    else
                    {
                        htl.itinerary11 = string.Empty;
                    }
                    if (data["notes"] != DBNull.Value)
                    {
                        htl.notes = Convert.ToString(data["notes"]);
                    }
                    else
                    {
                        htl.notes = string.Empty;
                    }
                    if (data["priceExclude"] != DBNull.Value)
                    {
                        htl.priceExclude = Convert.ToString(data["priceExclude"]);
                    }
                    else
                    {
                        htl.priceExclude = string.Empty;
                    }
                    htl.termsAndConditions = Convert.ToString(data["termsAndConditions"]);
                    htl.priority = Convert.ToInt32(data["priority"]);
                    htl.paging = Convert.ToInt32(data["paging"]);
                    htl.leftDeal1 = Convert.ToInt32(data["leftDeal1"]);
                    htl.leftDeal2 = Convert.ToInt32(data["leftDeal2"]);
                    htl.isActive = Convert.ToBoolean(data["dealId"]);
                }
                if (htl.dealId > 0)
                {
                    htl.hotelSeasonList = IndiaTimesHotelSeason.LoadhotelSeasons(htl.dealId);
                }
                //}
                //catch (Exception ex)
                //{
                //    CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during LoadHotelDeal() in IndiaTimesHotel.cs for dealId=" + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                //    throw new Exception("Unable to load hotel for deal id: " + dealId);
                //}
                //finally
                //{
                data.Close();
                connection.Close();
                //}
            }
            return htl;
        }

        /// <summary>
        /// Loads list of deals for priority, paging etc of deals for India times 
        /// </summary>
        /// <param name="whereString">where string</param>
        /// <returns>List of IndiaTimesHotel</returns>
        public static List<IndiaTimesHotel> LoadForPageSetting(string whereString)
        {
            //SqlDataReader data;
            List<IndiaTimesHotel> tempList = new List<IndiaTimesHotel>();
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@whereString", whereString);
                //try
                //{
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetAllIndiaTimesHotelDeals, paramList, connection);
                while (data.Read())
                {
                    IndiaTimesHotel htl = new IndiaTimesHotel();
                    htl.dealId = Convert.ToInt32(data["dealId"]);
                    htl.dealName = Convert.ToString(data["dealName"]);
                    htl.city = Convert.ToString(data["city"]);
                    htl.country = Convert.ToString(data["country"]);
                    htl.nights = Convert.ToInt32(data["nights"]);
                    htl.IsInternational = Convert.ToBoolean(data["isInternational"]);
                    htl.priority = Convert.ToInt32(data["priority"]);
                    htl.paging = Convert.ToInt32(data["paging"]);
                    if (data["leftDeal1"] == DBNull.Value)
                    {
                        htl.leftDeal1 = 0;
                    }
                    else
                    {
                        htl.leftDeal1 = Convert.ToInt32(data["leftDeal1"]);
                    }
                    if (data["leftDeal1Nights"] == DBNull.Value)
                    {
                        htl.leftDeal1Nights = 0;
                    }
                    else
                    {
                        htl.leftDeal1Nights = Convert.ToInt32(data["leftDeal1Nights"]);
                    }
                    if (data["leftDeal2"] == DBNull.Value)
                    {
                        htl.leftDeal2 = 0;
                    }
                    else
                    {
                        htl.leftDeal2 = Convert.ToInt32(data["leftDeal2"]);
                    }
                    if (data["leftDeal2Nights"] == DBNull.Value)
                    {
                        htl.leftDeal2Nights = 0;
                    }
                    else
                    {
                        htl.leftDeal2Nights = Convert.ToInt32(data["leftDeal2Nights"]);
                    }
                    htl.isActive = Convert.ToBoolean(data["isActive"]);
                    if (data["leftDeal1Name"] == DBNull.Value)
                    {
                        htl.leftDeal1Name = string.Empty;
                    }
                    else
                    {
                        htl.leftDeal1Name = Convert.ToString(data["leftDeal1Name"]);
                    }
                    if (data["leftDeal2Name"] == DBNull.Value)
                    {
                        htl.leftDeal2Name = string.Empty;
                    }
                    else
                    {
                        htl.leftDeal2Name = Convert.ToString(data["leftDeal2Name"]);
                    }
                    tempList.Add(htl);
                }
                //}
                //catch (Exception ex)
                //{
                //    CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during LoadForPageSetting() in IndiaTimesHotel.cs for whereString= " + whereString + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                //    throw new Exception("Unable to load hotel for whereString: " + whereString);
                //}
                //finally
                //{
                data.Close();
                connection.Close();
                //}

            }
            return tempList;
        }

        /// <summary>
        /// Loads deal names for left panel setting.
        /// </summary>
        /// <returns>list of IndiaTimes deals.</returns>
        public static List<IndiaTimesHotel> LoadDealNamesForPageSetting()
        {
            List<IndiaTimesHotel> tempList = new List<IndiaTimesHotel>();
            //SqlDataReader data;
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[0];
                //try
                //{
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetAllNamesOfIndiaTimesHotelDeals, paramList, connection);
                while (data.Read())
                {
                    IndiaTimesHotel htl = new IndiaTimesHotel();
                    htl.dealId = Convert.ToInt32(data["dealId"]);
                    htl.dealName = Convert.ToString(data["dealName"]);
                    htl.IsInternational = Convert.ToBoolean(data["isInternational"]);
                    htl.nights = Convert.ToInt32(data["nights"]);
                    htl.isActive = Convert.ToBoolean(data["isActive"]);
                    tempList.Add(htl);
                }
                //}
                //catch (Exception ex)
                //{
                //    CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during LoadDealNamesForPageSetting() in IndiaTimesHotel.cs | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                //    throw new Exception("Unable to Load Deal Names For Page Setting");
                //}
                //finally
                //{
                data.Close();
                connection.Close();
                //}
            }
            return tempList;
        }

        /// <summary>
        /// Generates where string
        /// </summary>
        /// <param name="active">bool active</param>
        /// <param name="inactive">bool inactive</param>
        /// <returns>where string</returns>
        public static string GenerateWhereString(bool active, bool inactive,int agencyId)
        {
            string whereString = string.Empty;
            if (active && inactive)
            {
                whereString = "where agencyid = " + agencyId.ToString() + " and ";
            }
            else
            {
                if (active)
                {
                    whereString = "where isActive = 1 and agencyid = " + agencyId.ToString() + " and ";
                }
                else
                {
                    whereString = "where isActive = 0 and agencyid = " + agencyId.ToString() + " and ";
                }
            }
           
            return whereString;
        }

        /// <summary>
        /// updates all deals for changes made in them on hotel deal setting page.
        /// </summary>
        /// <param name="whereString">where string</param>
        /// <param name="priorityList">priority List</param>
        /// <param name="pagingList">paging List</param>
        /// <param name="leftDeal1List">left panel Deal List</param>
        /// <param name="leftDeal2List">left panel Deal List</param>
        /// <returns>DataTable of IndiaTimesHotel</returns>
        public static DataTable UpdateHotelTable(string whereString, List<int> priorityList, List<int> pagingList, List<int> leftDeal1List, List<int> leftDeal2List)
        {
            DataTable dataTable = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@whereString", whereString);
                try
                {
                    SqlCommand command = new SqlCommand(SPNames.GetAllIndiaTimesHotelDeals, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 60;
                    command.Parameters.AddRange(paramList);
                    adapter = new SqlDataAdapter(command);
                    int rowsAdded = adapter.Fill(dataTable);
                    //DataTable dt = Dal.FillDataTableSP(SPNames.GetAllIndiaTimesHotelDeals, paramList);
                    for (int i = 0; i < priorityList.Count; i++)
                    {
                        dataTable.Rows[i]["priority"] = priorityList[i];
                        dataTable.Rows[i]["paging"] = pagingList[i];
                        dataTable.Rows[i]["leftDeal1"] = leftDeal1List[i];
                        dataTable.Rows[i]["leftDeal2"] = leftDeal2List[i];
                    }
                    SqlCommandBuilder cmd = new SqlCommandBuilder(adapter);
                    adapter.Update(dataTable);
                }
                catch (Exception ex)
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during UpdateHotelTable() in IndiaTimesHotel.cs Unable to update deals setting for home page and paging on India Times. | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    throw new Exception("Unable to update deals setting for home page and paging on India Times.");
                }
                finally
                {
                    connection.Close();
                    adapter.Dispose();
                }
            }
            return dataTable;
        }

        /// <summary>
        /// Updates a hotel deal status
        /// </summary>
        /// <param name="dealId">deal id</param>
        /// <param name="status">status</param>
        /// <param name="leftDeal1">left panel Deal</param>
        /// <param name="leftDeal2">left panel Deal</param>
        /// <returns>no of rows updated</returns>
        public static int UpdateHotelDealStatus(int dealId, bool status, int leftDeal1, int leftDeal2)
        {
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@dealId", dealId);
            paramList[1] = new SqlParameter("@status", status);
            paramList[2] = new SqlParameter("@leftDeal1", leftDeal1);
            paramList[3] = new SqlParameter("@leftDeal2", leftDeal2);
            try
            {
                rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateIndiaTimesHotelDealStatus, paramList);
            }
            catch (Exception ex)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during UpdateHotelDealStatus() in IndiaTimesHotel.cs for dealId = " + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }

        /// <summary>
        /// Deletes a deal.
        /// </summary>
        /// <param name="dealId">deal Id</param>
        /// <returns>no of rows affected</returns>
        public static int DeleteHotelDealStatus(int dealId)
        {
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@dealId", dealId);
            try
            {
                rowsAffected = Dal.ExecuteNonQuerySP(SPNames.DeleteIndiaTimesHotelDeal, paramList);
            }
            catch (Exception ex)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during DeleteHotelDealStatus() in IndiaTimesHotel.cs for dealId = " + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                rowsAffected = 0;
            }
            return rowsAffected;
        }

        /// <summary>
        /// chechs if deal to be removed is not mapped with other deals.
        /// </summary>
        /// <param name="dealId">deal Id</param>
        /// <returns>List of IndiaTimesHotel deals with which deal is mapped</returns>
        public static List<IndiaTimesHotel> CheckDealIdMapping(int dealId)
        {
            //SqlDataReader data;
            List<IndiaTimesHotel> tempList = new List<IndiaTimesHotel>();
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@dealId", dealId);
                //try
                //{
                    SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetMappedHotelDealIDs, paramList, connection);
                    while (data.Read())
                    {
                        IndiaTimesHotel htl = new IndiaTimesHotel();
                        htl.dealId = Convert.ToInt32(data["dealId"]);
                        htl.dealName = Convert.ToString(data["dealName"]);
                        htl.nights = Convert.ToInt32(data["nights"]);
                        tempList.Add(htl);
                    }
                //}
                //catch (Exception ex)
                //{
                //    CoreLogic.Audit.Add(CoreLogic.EventType.HotelCMS, CoreLogic.Severity.High, 0, "Error: Exception during CheckDealIdMapping() in IndiaTimesHotel.cs for dealId = " + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                //    throw new Exception("Unable to check mapping for deal id:" + dealId);
                //}
                //finally
                //{
                    data.Close();
                    connection.Close();
                //}
            }
            return tempList;
        }
        #endregion
    }
}
