using System;
using System.Collections.Generic;
using System.Text;

namespace Technology.Indiatimes
{
    internal static class SPNames
    {

        /*** Replace Indiatimes by B2C_  and ITimes by API_ ***/

        //IPAddressDetails
        public const string GetIPAddressDetailsByIPAddress = "usp_GetIPAddressDetailsByIPAddress";
        public const string AddIPAddressDetail = "usp_AddIPAddressDetail";
        public const string UpdateIPAddressStatus = "usp_UpdateIPAddressStatus";
        public const string GetIPAddressStatus = "usp_GetIPAddressStatus";
        public const string UpdateIPAddressDetail = "usp_UpdateIPAddressDetail";
        public const string IsIPAddressExists = "usp_IsIPAddressExists";

        // for ITimesBookingDetails
        public const string GetITimesBookingDetails = "usp_GetAPI_BookingDetails";
        public const string GetITimesBookingCount = "usp_GetAPI_BookingCount";
        public const string GetHistoryForTrip = "usp_GetHistoryForTrip";
        public const string GetBookingDetailByTripId = "usp_GetBookingDetailByTripId";
        public const string GetBookingFullStatus = "usp_GetBookingFullStatus";
        public const string UpdateCustomerStatus = "usp_UpdateCustomerStatus";
        public const string SaveITimesBookingHistory = "usp_SaveAPI_BookingHistory";
        public const string AddITimesBookingDetails = "usp_AddAPI_BookingDetails";
        public const string UpdateITimesBookingDetails = "usp_UpdateAPI_BookingDetails";
        public const string GetBookingCountFromIP = "usp_GetBookingCountFromIP";
        public const string GetBookingCountFromEmail = "usp_GetBookingCountFromEmail";
        public const string SaveAllITimesBookingHistoryIP = "usp_SaveAllAPI_BookingHistoryIP";
        public const string SaveAllITimesBookingHistoryEmail = "usp_SaveAllAPI_BookingHistoryEmail";
        public const string TestUniquePNRInITimesBookingDetails = "usp_TestUniquePNRInAPI_BookingDetails";
        public const string UpdateITimesBookingDetailsParametersOut = "usp_UpdateAPI_BookingDetailsParametersOut";
        public const string UpdateITimesBookingDetailsParametersIn = "usp_UpdateAPI_BookingDetailsParametersIn";
        public const string GetAllITimesBookingDetails = "usp_GetAllAPI_BookingDetails";
        public const string UpdateITimesPendingQueueDuplicateEntry = "usp_UpdateAPI_PendingQueueDuplicateEntry";
        public const string GetIsReceiptGeneratedForPnr = "usp_GetIsReceiptGeneratedForPnr";
        public const string GenerateReceiptForPnr = "usp_GenerateReceiptForPnr";
        public const string UpdateITimesBookingDetailsAmount = "usp_UpdateAPI_BookingDetailsAmount";

        // for IndiaTimesCustomer
        public const string GetIndiaTimesCustomerStatus = "usp_GetB2C_CustomerStatus";
        public const string UpdateIndiaTimesCustomerStatus = "usp_UpdateB2C_CustomerStatus";
        public const string GetIndiaTimesCustomerByUserName = "usp_GetB2C_CustomerByUserName";
        public const string GetIndiaTimesCustomers = "usp_GetB2C_Customers";
        public const string GetIndiaTimesCustomersCount = "usp_GetB2C_CustomersCount";
        public const string GetIndiaTimesCustomerByCustomerId = "usp_GetB2C_CustomerByCustomerId";


        // for IndiaTimesBookingDetails
        public const string AddIndiaTimesBookingDetails = "usp_AddB2C_BookingDetails";
        public const String GetBookingDetailsByTransactionId = "usp_GetB2C_BookingDetailsByTransactionId";

        // for ITimesPendingQueue
        public const string GetITimesPendingQueue = "usp_GetAPI_PendingQueue";
        public const string GetITimesPendingQueueCount = "usp_GetAPI_PendingQueueCount";
        public const string GetPendingQueueDetailByQueueId = "usp_GetPendingQueueDetailByQueueId";
        public const string AddITimesPendingQueueDetails = "usp_AddAPI_PendingQueueDetails";
        public const string UpdateITimesPendingQueueStatus = "usp_UpdateAPI_PendingQueueStatus";
        public const string UpdateITimesPendingQueueDetails = "usp_UpdateAPI_PendingQueueDetails";
        public const string SaveITimesPendingQueueHistory = "usp_SaveAPI_PendingQueueHistory";
        public const string GetHistoryForPendingQueue = "usp_GetHistoryForPendingQueue";

        //PaymentInformation
        public const string AddIndiatimesPayment = "usp_AddB2C_Payment";
        public const string UpdateIndiatimesPayment = "usp_UpdateB2C_Payment";
        public const string GetPaymentsByCustomerId = "usp_GetB2C_PaymentsByCustomerId";
        public const string GetPaymentInformationByPaymentInformationId = "usp_GetPaymentInformationByPaymentInformationId";
        public const string GetTotalFilteredPaymentsCount = "usp_GetTotalFilteredB2C_PaymentsCount";
        public const string GetTotalFilteredPayments = "usp_GetTotalFilteredB2C_Payments";

        //ChangeRequest
        public const String UpdateChangeRequest = "usp_UpdateB2C_ChangeRequest";
        public const String AddChangeRequest = "usp_AddB2C_ChangeRequest";
        public const String GetTotalFilteredRequestsCount = "usp_GetTotalFilteredB2C_RequestsCount";
        public const String GetTotalFilteredRequests = "usp_GetTotalFilteredB2C_Requests";
        public const string IsIndiatimesChangeRequestIDExists = "usp_IsB2C_ChangeRequestIDExists";
        public const string GetChangeRequestByChangeRequestId = "usp_GetB2C_ChangeRequestByChangeRequestId";
        public const string UpdateChangeRequestPaymentStatus = "usp_UpdateB2C_ChangeRequestPaymentStatus";

        // For ITimesHotelPendingQueue
        public const string GetITimesHotelPendingQueue = "usp_GetAPI_HotelPendingQueue";
        public const string GetITimesHotelPendingQueueCount = "usp_GetAPI_HotelPendingQueueCount";
        public const string GetITimesHotelPendingQueueDetailByHotelQueueId = "usp_GetAPI_HotelPendingQueueDetailByHotelQueueId"; 
        public const string AddITimesHotelPendingQueueDetails = "usp_AddAPI_HotelPendingQueueDetails";
        public const string UpdateITimesHotelPendingQueueDetails = "usp_UpdateAPI_HotelPendingQueueDetails";
        public const string UpdateITimesHotelPendingQueueStatus = "usp_UpdateAPI_HotelPendingQueueStatus";

        public const string AddITimesHotelBookingDetails = "usp_AddAPI_HotelBookingDetails";
        public const string UpdateITimesHotelBookingDetails = "usp_UpdateAPI_HotelBookingDetails";
        public const string GetITimesHotelBookingDetails = "usp_GetAPI_HotelBookingDetails";
        public const string GetHotelBookingDetailByTripId = "usp_GetHotelBookingDetailByTripId";
        public const string GetITimesHotelBookingCount = "usp_GetAPI_HotelBookingCount";
        public const string IsConfExistAndVouchered = "usp_IsConfExistAndVouchered";
        public const string GetHotelIdFromSource = "usp_GetHotelIdFromSource";
        public const string UpdateHotelVoucherStatus = "usp_UpdateHotelVoucherStatus";
        public const string GetAllITimesHotelBookingDetails = "usp_GetAllAPI_HotelBookingDetails";
        public const string IsConfirmationNoUnique = "usp_IsConfirmationNoUnique";

        // For India Times Hotel deal automation
        public const string IndiaTimesHotelSave = "usp_B2C_HotelSave";
        public const string UpdateImagePath = "usp_UpdateImagePath";
        public const string IndiaTimesHotelSeasonSave = "usp_B2C_HotelSeasonSave";
        public const string GetAllIndiaTimesHotelDeals = "usp_GetAllB2C_HotelDeals";
        public const string UpdateIndiaTimesHotelDealStatus = "usp_UpdateB2C_HotelDealStatus";
        public const string DeleteIndiaTimesHotelDeal = "usp_DeleteB2C_HotelDeal";
        public const string GetAllNamesOfIndiaTimesHotelDeals = "usp_GetAllNamesOfB2C_HotelDeals";
        public const string GetMappedHotelDealIDs = "usp_GetMappedHotelDealIDs";
        public const string GetIndiaTimesHotelDealForEdit = "usp_GetB2C_HotelDealForEdit";
        public const string GetIndiaTimesHotelSeasons = "usp_GetB2C_HotelSeasons";
        public const string UpdateIndiaTimesHotel = "usp_UpdateB2C_Hotel";
        public const string UpdateIndiaTimesHotelSeason = "usp_UpdateB2C_HotelSeason";
        public const string UpdateMainImagePath = "usp_UpdateMainImagePath";
        public const string UpdateSideImagePath = "usp_UpdateSideImagePath";
        public const string UpdateThumbnailImagePath = "usp_UpdateThumbnailImagePath";
        public const string DeleteIndiaTimesHotelSeason = "usp_DeleteB2C_HotelSeason";

        //IndiatimesHotelBookingDetails
        public const String AddIndiaTimesHotelBookingDetails = "usp_AddB2C_HotelBookingDetails";
        public const String UpdateIndiaTimesHotelBookingDetails = "usp_UpdateB2C_HotelBookingDetails";
        public const String AddIndiaTimesHotelRoomDetails = "usp_AddB2C_HotelRoomDetails";
        public const String GetRoomGuestDetailsForIndiaTimesHotelBooking = "usp_GetRoomGuestDetailsForB2C_HotelBooking";

        //For package queries
        public const String GetIndiaTimesPackageQueries = "usp_GetB2C_PackageQueries";
        public const String GetIndiaTimesPackageQueriesCount = "usp_GetB2C_PackageQueriesCount";
        public const String IndiaTimesPackageQueryStatusUpdate = "usp_B2C_PackageQueryStatusUpdate";
        public const String GetIndiaTimesPackageQueriesRemarks = "usp_GetB2C_PackageQueriesRemarks";
        public const String IndiaTimesPackageQueryDetailsUpdate = "usp_B2C_PackageQueryDetailsUpdate";
        public const String GetAllPackageQueries = "usp_GetAllPackageQueries";

        //For Holiday Package Home page.
        public const String GetAllActiveHotelThemes = "usp_GetAllActiveHotelThemes";
        public const String GetDealThemeIndexTableSchema = "usp_GetDealThemeIndexTableSchema";
        public const String GetSelectedThemesForDeal = "usp_GetSelectedThemesForDeal";
        public const String DeleteFromThemeIndexTableSchema = "usp_DeleteFromThemeIndexTableSchema";
        
    }
}
