using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using CoreLogic;
using System.Diagnostics;

namespace Technology.Indiatimes
{
    public class ITimesBookingHistory
    {
        # region Private Fields
        int tripId;
        ITimesBookingAction action;
        string remarks;
        int createdBy;
        DateTime createdOn;
        #endregion

        # region Public Properties

        public int TripId
        {
            get
            {
                return tripId;
            }
            set
            {
                tripId = value;
            }
        }

        public ITimesBookingAction Action
        {
            get
            {
                return action;
            }
            set
            {
                action = value;
            }
        }

        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tripId"></param>
        /// <returns></returns>
        public static List<ITimesBookingHistory> Load(int tripId)
        {
            Trace.TraceInformation("ITimesBookingDetails.Load entered. Trip ID = " + tripId);
            if (tripId <= 0)
            {
                throw new ArgumentException("tripId should not be less then equal to zero", "tripId");
            } 
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@tripId", tripId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetHistoryForTrip, paramList, connection);
            List<ITimesBookingHistory> tempList = new List<ITimesBookingHistory>();
            if (data.HasRows)
            {
                while (data.Read())
                {
                    ITimesBookingHistory iHistory = new ITimesBookingHistory();
                    iHistory.action = (ITimesBookingAction)Enum.Parse(typeof(ITimesBookingAction), data["action"].ToString());
                    iHistory.remarks = Convert.ToString(data["remarks"]);
                    iHistory.createdBy = Convert.ToInt32(data["createdBy"]);
                    iHistory.createdOn = Convert.ToDateTime(data["createdOn"]);
                    tempList.Add(iHistory);
                }
            }
            Trace.TraceInformation("ITimesBookingDetails.Load exited. Trip ID = " + tripId);
            return tempList;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tripId"></param>
        /// <param name="action"></param>
        /// <param name="remarks"></param>
        /// <param name="createdBy"></param>
        /// <returns></returns>
        public static int SaveBookingHistory(int tripId, ITimesBookingAction action, string remarks, int createdBy)
        {
            Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory entered. Trip ID = " + tripId + ", action = " + action.ToString() + ", remarks = " + remarks + ", created by: " + createdBy);
            if (tripId <= 0)
            {
                throw new ArgumentException("tripId should not be less then equal to zero", "tripId");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy should not be less then equal to zero", "createdBy");
            }
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@tripId", tripId);
            paramList[1] = new SqlParameter("@action", action);
            paramList[2] = new SqlParameter("@remarks", remarks);
            paramList[3] = new SqlParameter("@createdBy", createdBy);
            //paramList[2] = new SqlParameter("@lastModifiedBy", modifiedById);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.SaveITimesBookingHistory, paramList);
            if (rowsAffected > 0)
            {
                Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory exited. Trip ID = " + tripId + ", action = " + action.ToString() + ", remarks = " + remarks + ", created by: " + createdBy);
                return rowsAffected;
            }
            else
            {
                Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory exited, tripId history not saved. Trip ID = " + tripId + ", action = " + action.ToString() + ", remarks = " + remarks + ", created by: " + createdBy);
                Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory exited, tripId history not saved : tripId = " + tripId);
                return 0;
            }
        }


        public static int SaveAllBookingHistory(int tripId, ITimesBookingAction action, string remarks, int createdBy, string parameter, string saveType)
        {
            Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory entered. Trip ID = " + tripId + ", action = " + action.ToString() + ", remarks = " + remarks + ", created by: " + createdBy);
            if (tripId <= 0)
            {
                throw new ArgumentException("tripId should not be less then equal to zero", "tripId");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy should not be less then equal to zero", "createdBy");
            }
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@tripId", tripId);
            paramList[1] = new SqlParameter("@action", action);
            paramList[2] = new SqlParameter("@remarks", remarks);
            paramList[3] = new SqlParameter("@createdBy", createdBy);
            switch (saveType)
            {
                case "email":
                    paramList[4] = new SqlParameter("@email", parameter);
                    rowsAffected = Dal.ExecuteNonQuerySP(SPNames.SaveAllITimesBookingHistoryEmail, paramList);
                    break;

                case "ip":
                    paramList[4] = new SqlParameter("@ipAddress", parameter);
                    rowsAffected = Dal.ExecuteNonQuerySP(SPNames.SaveAllITimesBookingHistoryIP, paramList);
                    break;
            }
            if (rowsAffected > 0)
            {
                Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory exited. Trip ID = " + tripId + ", action = " + action.ToString() + ", remarks = " + remarks + ", created by: " + createdBy);
                return rowsAffected;
            }
            else
            {
                Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory exited, tripId history not saved. Trip ID = " + tripId + ", action = " + action.ToString() + ", remarks = " + remarks + ", created by: " + createdBy);
                Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory exited, tripId history not saved : tripId = " + tripId);
                return 0;
            }
        }

    }
}
