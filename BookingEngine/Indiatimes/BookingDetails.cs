using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using Technology.Data;
using Technology.BookingEngine;
using Technology.Configuration;
using System.Collections;
using System.Configuration;
using System.IO;
using CoreLogic;
using CoreLogic;

namespace Technology.Indiatimes
{
    public class BookingDetails
    {
        private int transactionId;
        private int referenceId;
        private string referenceNumber;
        private RefrenceType referenceType;
        private int customerId;
        private string tripId;
        private string pnr;
        private string airlineCode;
        private string title;
        private string paxFirstName;
        private string paxLastName;
        private Passenger_Type paxType;
        private string validatingAirline;
        private DateTime issueDate;
        private DateTime travelDate;
        private DateTime arrivalDate;
        private string sector;
        private string origin;
        private string destination;
        private bool isLcc;
        private bool isDomestic;
        private decimal fare;
        private decimal tax;
        private int bookingId;
        private int paxId;
        private bool isRequestSended;
        private bool isCancelled; 
        private string errorMessage = String.Empty;

        /// <summary>
        /// AirlineCode of Booking
        /// </summary>
        public string AirlineCode
        {
            get
            {
                return airlineCode;
            }
            set
            {
                airlineCode = value;
            }
        }

        /// <summary>
        /// Id of Trip
        /// </summary>
        public string TripId
        {
            get
            {
                return tripId;
            }
            set
            {
                tripId = value;
            }
        }

        /// <summary>
        /// Travel Destination
        /// </summary>
        public string Destination
        {
            get
            {
                return destination;
            }
            set
            {
                destination = value;
            }
        }

        /// <summary>
        /// Ticket Issue Date if Ticketed
        /// </summary>
        public DateTime IssueDate
        {
            get
            {
                return issueDate;
            }
            set
            {
                issueDate = value;
            }
        }

        /// <summary>
        /// Id of customer for which booking is made
        /// </summary>
        public int CustomerId
        {
            get
            {
                return customerId;
            }
            set
            {
                customerId = value;
            }
        }

        /// <summary>
        /// Travel Origin
        /// </summary>
        public string Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
            }
        }

        /// <summary>
        /// Passanger First Name
        /// </summary>
        public string PaxFirstName
        {
            get
            {
                return paxFirstName;
            }
            set
            {
                paxFirstName = value;
            }
        }

        /// <summary>
        /// Passanger Last Name
        /// </summary>
        public string PaxLastName
        {
            get
            {
                return paxLastName;
            }
            set
            {
                paxLastName = value;
            }
        }

        /// <summary>
        /// Type of Passanger
        /// </summary>
        public Passenger_Type PaxType
        {
            get
            {
                return paxType;
            }
            set
            {
                paxType = value;
            }
        }

        /// <summary>
        /// PNR no. of Booking
        /// </summary>
        public string Pnr
        {
            get
            {
                return pnr;
            }
            set
            {
                pnr = value;
            }
        }

        /// <summary>
        /// id of Ticketing if Ticketed
        /// </summary>
        public int ReferenceId
        {
            get
            {
                return referenceId;
            }
            set
            {
                referenceId = value;
            }
        }

        /// <summary>
        /// Ticket Number genrated if Ticketed
        /// </summary>
        public string ReferenceNumber
        {
            get
            {
                return referenceNumber;
            }
            set
            {
                referenceNumber = value;
            }
        }

        /// <summary>
        /// Title of Passanger
        /// </summary>
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        /// <summary>
        /// unique Id for each booking made by member
        /// </summary>
        public int TransactionId
        {
            get
            {
                return transactionId;
            }
            set
            {
                transactionId = value;
            }
        }

        /// <summary>
        /// Date of Travel
        /// </summary>
        public DateTime TravelDate
        {
            get
            {
                return travelDate;
            }
            set
            {
                travelDate = value;
            }
        }

        /// <summary>
        /// Date of Arrival
        /// </summary>
        public DateTime ArrivalDate
        {
            get
            {
                return arrivalDate;
            }
            set
            {
                arrivalDate = value;
            }
        }

        /// <summary>
        /// Sector of Travel
        /// </summary>
        public string Sector
        {
            get
            {
                return sector;
            }
            set
            {
                sector = value;
            }
        }

        /// <summary>
        /// three digit code for Airline validation
        /// </summary>
        public string ValidatingAirline
        {
            get
            {
                return validatingAirline;
            }
            set
            {
                validatingAirline = value;
            }
        }

        public RefrenceType ReferenceType
        {
            get
            {
                return referenceType;
            }
            set
            {
                referenceType = value;
            }
        }

        /// <summary>
        /// boolean value, true if booking is for Lcc
        /// </summary>
        public bool IsLcc
        {
            get
            {
                return isLcc;
            }
            set
            {
                isLcc = value;
            }
        }

        /// <summary>
        /// boolean value, true if booking is Domestic
        /// </summary>
        public bool IsDomestic
        {
            get
            {
                return isDomestic;
            }
            set
            {
                isDomestic = value;
            }
        }

        /// <summary>
        /// Fare charged on Ticket
        /// </summary>
        public decimal Fare
        {
            get
            {
                return fare;
            }
            set
            {
                fare = value;
            }
        }

        /// <summary>
        /// Tax charged on Ticket
        /// </summary>
        public decimal Tax
        {
            get
            {
                return tax;
            }
            set
            {
                tax = value;
            }
        }
        public int BookingId
        {
            get
            {
                return bookingId;
            }
            set
            {
                bookingId = value;
            }
        }
        public int PaxId
        {
            get
            {
                return paxId;
            }
            set
            {
                paxId = value;
            }
        }
        public bool IsRequestSended
        {
            get
            {
                return isRequestSended;
            }
            set
            {
                isRequestSended = value;
            }
        }
        public bool IsCancelled
        {
            get
            {
                return isCancelled;
            }
            set
            {
                isCancelled = value;
            }
        }
        /// <summary>
        /// Method to Save Booking Details
        /// </summary>
        public void Save()
        {
            int retVal;
            if (customerId < 0)
            {
                throw new ArgumentException("customerId must be a positive integer and greater then Zero, customerId :" + customerId);
            }
            if (tripId == null || tripId.Length == 0)
            {
                throw new ArgumentException("tripId must not be null or of Length Zero");
            }
            if (paxFirstName == null || paxFirstName.Length == 0)
            {
                throw new ArgumentException("paxFirstName must not be null or of Length Zero");
            }
            if (paxLastName == null || paxLastName.Length == 0)
            {
                throw new ArgumentException("paxLastName must not be null or of Length Zero");
            }
            if (destination == null || destination.Length == 0)
            {
                throw new ArgumentException("destination must not be null or of Length Zero");
            }
            if (transactionId == 0)
            {
                SqlParameter[] paramList = new SqlParameter[25];
                paramList[0] = new SqlParameter("@transactionId", SqlDbType.Int);
                paramList[1] = new SqlParameter("@referenceId", referenceId);
                paramList[2] = new SqlParameter("@referenceNumber", referenceNumber);
                paramList[3] = new SqlParameter("@referenceTypeId", (int)referenceType);
                paramList[4] = new SqlParameter("@customerId", customerId);
                paramList[5] = new SqlParameter("@tripId", tripId);
                paramList[6] = new SqlParameter("@pnr", pnr);
                paramList[7] = new SqlParameter("@airlineCode", airlineCode);
                if (title != null)
                {
                    paramList[8] = new SqlParameter("@title", title);
                }
                else
                {
                    paramList[8] = new SqlParameter("@title", DBNull.Value);
                }
                paramList[9] = new SqlParameter("@paxFirstName", paxFirstName);
                paramList[10] = new SqlParameter("@PaxLastName", PaxLastName);
                paramList[11] = new SqlParameter("@paxTypeId", (int)paxType);
                paramList[12] = new SqlParameter("@validatingAirline", validatingAirline);
                paramList[13] = new SqlParameter("@issueDate", issueDate);
                paramList[14] = new SqlParameter("@travelDate", travelDate);
                paramList[15] = new SqlParameter("@arrivalDate", arrivalDate);
                paramList[16] = new SqlParameter("@sector", sector);
                paramList[17] = new SqlParameter("@origin", origin);
                paramList[18] = new SqlParameter("@destination", destination);
                paramList[19] = new SqlParameter("@IsDomestic", IsDomestic);
                paramList[20] = new SqlParameter("@IsLcc", IsLcc);
                paramList[21] = new SqlParameter("@fare", fare);
                paramList[22] = new SqlParameter("@tax", tax);
                paramList[23] = new SqlParameter("@bookingId", bookingId);
                paramList[24] = new SqlParameter("@paxId", paxId);
                paramList[0].Direction = ParameterDirection.Output;
                retVal = Dal.ExecuteNonQuerySP(SPNames.AddIndiaTimesBookingDetails, paramList);
                transactionId = (int)paramList[0].Value;
                if (transactionId == 0)
                {
                    errorMessage = "This RefrenceId : " + referenceId + " entry already exists in Database.";
                    throw new ArgumentException("This RefrenceId : " + referenceId + " entry already exists");
                }
            }
        }

        public static void EmailIndiaTimesTicket(string from, string replyTo, List<string> toArray, string agencyName, string phoneNo, string subject, FlightItinerary itinerary, string tripId, int paxId, string bcc, string filePath)
        {
            if (Convert.ToBoolean(ConfigurationSystem.Email["isSendEmail"]))
            {
                Hashtable globalHashTable = new Hashtable();
                List<Ticket> ticketList = Ticket.GetTicketList(itinerary.FlightId);
                string eTicketString = "E-Ticket";
                string ticketNumber = string.Empty;
                if (itinerary.FlightBookingSource == BookingSource.Amadeus || itinerary.FlightBookingSource == BookingSource.WorldSpan || itinerary.FlightBookingSource == BookingSource.Galileo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.Mdlr)
                {
                    ticketNumber = "Ticket Number: " + ticketList[paxId].ValidatingAriline.Trim() + ticketList[paxId].TicketNumber.Trim();
                }
                else
                {
                    ticketNumber = "Reference Number: " + ticketList[paxId].TicketNumber.Trim();
                }
                string PNR = string.Empty;
                if (itinerary.FlightBookingSource == BookingSource.Amadeus)
                {
                    PNR = PNR + "1A-";
                }
                else if (itinerary.FlightBookingSource == BookingSource.WorldSpan)
                {
                    PNR = PNR + "1P-";
                }
                else if (itinerary.FlightBookingSource == BookingSource.Galileo)
                {
                    PNR = PNR + "1G-";
                }
                else
                {
                    PNR = PNR + itinerary.Segments[0].Airline.ToUpper() + "-";
                }
                PNR = PNR + itinerary.PNR.Trim();
                string issueDate = ticketList[paxId].IssueDate.ToString("ddd dd MMM yyyy");
                string paxName = ticketList[paxId].Title + " " + ticketList[paxId].PaxFirstName.Trim() + " " + ticketList[paxId].PaxLastName.Trim();
                string addressLine1 = string.Empty;
                string addressLine2 = string.Empty;
                string cityName = string.Empty;
                string addressPin = string.Empty;
                string tripIdString = string.Empty;
                tripIdString = "Trip ID : " + tripId;
                string addressPhone = string.Empty;
                if (phoneNo != null && phoneNo.Trim() != string.Empty)
                {
                    addressPhone = "Phone : " + phoneNo.Trim();
                }
                else
                {
                    addressPhone = "";
                }
                string message = string.Empty;
                message = "This is an electronic ticket. Please carry a positive identification for check in. ";
                string fareString = string.Empty;
                string transactionFee = string.Empty;
                decimal otherCharges = 0;
                if (itinerary.Passenger[paxId].Price.AirlineTransFee > 0)
                {
                    transactionFee = "<tr><td style=\"width:120px;\"><span style=\"margin:0px;padding:0px;float:right;font-size:14px;text-align:right;font-weight:500;\">Tra Fee:</span></td><td style=\"width:120px;\"><span style=\"margin:0px;	padding:0px;float:right;font-size:14px;	text-align:right;font-weight:500;\">Rs. " + itinerary.Passenger[paxId].Price.AirlineTransFee.ToString("N2") + "</span></td></tr>";
                }
                if (ticketList.Count - 1 >= paxId)
                {
                    otherCharges = itinerary.Passenger[paxId].Price.OtherCharges + itinerary.Passenger[paxId].Price.WLCharge + ticketList[paxId].ServiceFee;
                }
                else
                {
                    otherCharges = itinerary.Passenger[paxId].Price.OtherCharges + itinerary.Passenger[paxId].Price.WLCharge;
                }

                fareString = "<td style=\"width:240px;\">							<table style=\"width:240px;	margin:-4px 0px 0px 0px;	padding:0px;	float:left;\"border=\"0\" cellspacing=\"0\" cellpadding=\"0\">								<tr>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											Air Fare:										</span>									</td>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											Rs. " + (itinerary.Passenger[paxId].Price.PublishedFare - itinerary.Passenger[paxId].Price.WhiteLabelDiscount).ToString("N2") + "										</span>									</td>								</tr>								<tr>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											Tax:										</span>									</td>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											Rs." + (itinerary.Passenger[paxId].Price.Tax + otherCharges + itinerary.Passenger[paxId].Price.SeviceTax + itinerary.Passenger[paxId].Price.AdditionalTxnFee).ToString("N2") + "										</span>									</td>								</tr>									" + transactionFee + "								<tr>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											Total Air Fare:										</span>									</td>									<td style=\"width:120px;\">										<span style=\"margin:0px;	padding:0px;	float:right;	font-size:14px;	text-align:right;	font-weight:500;\">											Rs." + Convert.ToDouble((itinerary.Passenger[paxId].Price.PublishedFare - itinerary.Passenger[paxId].Price.WhiteLabelDiscount) + itinerary.Passenger[paxId].Price.Tax + otherCharges + itinerary.Passenger[paxId].Price.SeviceTax + itinerary.Passenger[paxId].Price.AdditionalTxnFee + itinerary.Passenger[paxId].Price.AirlineTransFee).ToString("N2") + "										</span>									</td>								</tr>							</table>						</td>";
                string promotionalCode = string.Empty;
                globalHashTable.Add("agencyName", agencyName);
                globalHashTable.Add("eTicketString", eTicketString);
                globalHashTable.Add("ticketNumber", ticketNumber);
                globalHashTable.Add("PNR", PNR);
                globalHashTable.Add("issueDate", issueDate);
                globalHashTable.Add("paxName", paxName);
                globalHashTable.Add("addressLine1", addressLine1);
                globalHashTable.Add("addressLine2", addressLine2);
                globalHashTable.Add("cityName", cityName);
                globalHashTable.Add("addressPin", addressPin);
                globalHashTable.Add("tripId", tripIdString);
                globalHashTable.Add("addressPhone", addressPhone);
                globalHashTable.Add("message", message);
                globalHashTable.Add("fare", fareString);
                globalHashTable.Add("promotionalCode", promotionalCode);

                //Html code in text file for E-ticket
                StreamReader sr = new StreamReader(filePath);
                //string contains the code of loop
                string loopString = string.Empty;

                bool loopStarts = false;
                //string contains the code before loop 
                string startString = string.Empty;
                //string contains the code after loop
                string endString = string.Empty;
                bool loopEnds = false;
                #region seperate out the loop code and the other code
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (line.IndexOf("%loopEnds%") >= 0 || loopEnds)
                    {
                        loopEnds = true;
                        loopStarts = false;
                        if (line.IndexOf("%loopEnds%") >= 0)
                        {
                            line = sr.ReadLine();
                        }
                        endString += line;
                    }
                    if (line.IndexOf("%loopStarts%") >= 0 || loopStarts)
                    {
                        if (line.IndexOf("%loopStarts%") >= 0)
                        {
                            line = sr.ReadLine();
                        }
                        loopString += line;
                        loopStarts = true;
                    }
                    else
                    {
                        if (!loopEnds)
                        {
                            startString += line;
                        }
                    }
                }
                #endregion

                string midString = string.Empty;
                for (int count = 0; count < itinerary.Segments.Length; count++)
                {
                    Hashtable tempTable = new Hashtable();
                    tempTable.Add("depDay", itinerary.Segments[count].DepartureTime.ToString("ddd dd MMM yyyy"));
                    tempTable.Add("depDate", "");
                    Airline airline = new Airline();
                    try
                    {
                        airline.Load(itinerary.Segments[count].Airline);
                    }
                    catch(Exception ex)
                    {
                        Audit.Add(EventType.IndiaTimesEmailTicket, Severity.High, 0, "Error while loading airline airlineCode " + itinerary.Segments[count].Airline + ex.Message + " StackTrace : " + ex.StackTrace, "");
                    }
                    tempTable.Add("airlineName", airline.AirlineName.Trim());
                    tempTable.Add("airlineCode", airline.AirlineCode.Trim());
                    tempTable.Add("flightNumber", itinerary.Segments[count].FlightNumber.Trim());
                    if (itinerary.Segments[count].AirlinePNR.Trim().Length > 0)
                    {
                        tempTable.Add("airlinePNR", "Airline Ref : " + itinerary.Segments[count].AirlinePNR.Trim());
                    }
                    else
                    {
                        tempTable.Add("airlinePNR", "");
                    }
                    tempTable.Add("flightStatus", "Status : Confirmed");
                    tempTable.Add("origAirportCode", itinerary.Segments[count].Origin.AirportCode.Trim());
                    tempTable.Add("origAirportName", itinerary.Segments[count].Origin.AirportName.Trim());
                    if (itinerary.Segments[count].DepTerminal != null && itinerary.Segments[count].DepTerminal.Trim().Length > 0)
                    {
                        tempTable.Add("arrTerminal", "Terminal " + itinerary.Segments[count].DepTerminal.Trim());
                    }
                    else
                    {
                        tempTable.Add("arrTerminal", "");
                    }
                    tempTable.Add("depTime", itinerary.Segments[count].DepartureTime.ToShortTimeString());
                    tempTable.Add("destAirportCode", itinerary.Segments[count].Destination.AirportCode.Trim());
                    tempTable.Add("destAirportName", itinerary.Segments[count].Destination.AirportName.Trim());
                    if (itinerary.Segments[count].ArrTerminal != null && itinerary.Segments[count].ArrTerminal.Trim().Length > 0)
                    {
                        tempTable.Add("depTerminal", "Terminal " + itinerary.Segments[count].ArrTerminal.Trim());
                    }
                    else
                    {
                        tempTable.Add("depTerminal", "");
                    }
                    tempTable.Add("arrTime", itinerary.Segments[count].ArrivalTime.ToShortTimeString());
                    tempTable.Add("bookingClass", itinerary.Segments[count].BookingClass.Trim());
                    string duration = string.Empty;
                    TimeSpan t = itinerary.Segments[count].Duration;
                    duration = (t.Days * 24 + t.Hours).ToString("00:") + t.Minutes.ToString("00");
                    if (duration != null && duration != string.Empty && duration.Trim() != "00:00")
                    {
                        tempTable.Add("flightDuration", duration.Trim() + " Hours Flight");
                    }
                    else
                    {
                        tempTable.Add("flightDuration", "");
                    }
                    if (itinerary.Segments.Length == 1)
                    {
                        tempTable.Add("stopString", "Non stop");
                    }
                    else
                    {
                        tempTable.Add("stopString", Convert.ToString(itinerary.Segments.Length - 1).Trim() + " stop/s");
                    }
                    ticketList[paxId].PtcDetail = SegmentPTCDetail.GetSegmentPTCDetail(itinerary.FlightId);
                    if (ticketList[paxId].PtcDetail[count].Baggage != null && ticketList[paxId].PtcDetail[count].Baggage.Trim().Length > 0)
                    {
                        tempTable.Add("baggage", "Baggage: " + ticketList[paxId].PtcDetail[count].Baggage.Trim());
                    }
                    else
                    {
                        tempTable.Add("baggage", "Baggage: 0 Kg");
                    }
                    if (itinerary.Segments[count].Craft != null && itinerary.Segments[count].Craft.ToString().Trim() != string.Empty)
                    {
                        tempTable.Add("craft", "Aircraft: " + itinerary.Segments[count].Craft);
                    }
                    else
                    {
                        tempTable.Add("craft", "");
                    }
                    tempTable.Add("mealDescription", "");
                    midString += Email.ReplaceHashVariable(loopString, tempTable);
                }
                string fullString = startString + midString + endString;
                try
                {
                    Email.Send(from, replyTo, toArray, subject, fullString, globalHashTable, bcc);
                }
                catch (System.Net.Mail.SmtpException ex)
                {
                    throw new System.Net.Mail.SmtpException(ex.Message);
                }
            }
        }

        /// <summary>
        /// Method to retrieve Booking detail
        /// </summary>
        /// <param name="transactionId">transactionId of Booking</param>
        public void GetBookingDetailsByTransactionId(int transactionId)
        {
            if (transactionId <= 0)
            {
                throw new ArgumentException("transactionId must not be less then or equal to Zero");
            }
            SqlDataReader datareader;
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@transactionId", transactionId);
            datareader = Dal.ExecuteReaderSP(SPNames.GetBookingDetailsByTransactionId, paramList, connection);
            ReadDataReader(datareader);
            datareader.Close();
            connection.Close();
        }

        /// <summary>
        /// Reads the data from SqlDataReader
        /// </summary>
        /// <param name="data"></param>
        private void ReadDataReader(SqlDataReader data)
        {
            if (data.Read())
            {
                transactionId = Convert.ToInt32(data["transactionId"]);
                referenceId = Convert.ToInt32(data["referenceId"]);
                referenceNumber = Convert.ToString(data["referenceNumber"]);
                referenceType = (RefrenceType)Convert.ToInt32(data["referenceTypeId"]);
                customerId = Convert.ToInt32(data["customerId"]);
                tripId = Convert.ToString(data["tripId"]);
                pnr = Convert.ToString(data["pnr"]);
                airlineCode = Convert.ToString(data["airlineCode"]);
                if (data["title"] != DBNull.Value)
                {
                    title = Convert.ToString(data["title"]);
                }
                else
                {
                    title = string.Empty;
                }
                paxFirstName = Convert.ToString(data["paxFirstName"]);
                PaxLastName = Convert.ToString(data["PaxLastName"]);
                paxType = (Passenger_Type)Convert.ToInt32(data["paxTypeId"]);
                validatingAirline = Convert.ToString(data["validatingAirline"]);
                issueDate = Convert.ToDateTime(data["issueDate"]);
                travelDate = Convert.ToDateTime(data["travelDate"]);
                arrivalDate = Convert.ToDateTime(data["arrivalDate"]);
                sector = Convert.ToString(data["sector"]);
                origin = Convert.ToString(data["origin"]);
                destination = Convert.ToString(data["destination"]);
                isDomestic = Convert.ToBoolean(data["isDomestic"]);
                isLcc = Convert.ToBoolean(data["isLcc"]);
                fare = Convert.ToDecimal(data["fare"]);
                tax = Convert.ToDecimal(data["tax"]);
                bookingId = Convert.ToInt32(data["bookingId"]);
                paxId = Convert.ToInt32(data["paxId"]);
                isRequestSended = Convert.ToBoolean(data["isRequestSended"]);
                isCancelled = Convert.ToBoolean(data["isCancelled"]);
            }
        }

    }
}
