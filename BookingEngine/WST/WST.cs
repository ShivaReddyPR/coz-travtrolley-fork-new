#region .Net Base Class Namespace Imports
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Globalization;
#endregion

#region Custom Namespace Imports
using CT.Configuration;
using CT.Core;
#endregion

#region WST Namespace Imports
using WST.Common;             //Web service for static data
using WST.getavailability;   //Web services for searching of available hotel
using WST.bookingservice;
using System.Data;
using System.Threading;//web services for booking ,canceling and amendment of hotels
#endregion

namespace CT.BookingEngine.GDS
{
    public class WST:IDisposable
    {
        public enum BookStaus
        {
            Commit,
            RollBack,
        }
        #region Private Variables
        private string XmlPath = string.Empty;
        //private bool testMode;
        private string loginCode;
        private string cozRef;
        private string currency;
        private decimal rateOfExchange = 1;
        private Dictionary<string, decimal> exchangeRates;
        private int decimalPoint;
        private string agentCurrency;
        string sourceCountryCode;

        private Common wstStaticData = new Common();
        
        private GetAvailability availService = new GetAvailability();
        private Booking bookingService = new Booking();
        #endregion

        #region Properties
        public Dictionary<string, decimal> AgentExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public int AgentDecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }

        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }

        public string SourceCountryCode
        {
            get
            {
                return sourceCountryCode;
            }

            set
            {
                sourceCountryCode = value;
            }
        }

        #endregion

        protected Dictionary<string, decimal> rateOfEx = new Dictionary<string, decimal>();
        protected double rateofExchange = 1;
        List<HotelSearchResult> hotelResults = new List<HotelSearchResult>();//This will contain the all the hotels.


        /*********************multithreading **********************/
        HotelRequest request = new HotelRequest();
        decimal markup = 0;
        string markupType = string.Empty;
        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        /**********************************************************/
         #region Constrcutor
        /// <summary>
        /// Constrcutor for WST
        /// </summary>
        public WST()
        {
            loginCode = ConfigurationSystem.WSTConfig["loginCode"];
            currency = ConfigurationSystem.WSTConfig["currency"];
            cozRef = "CZT" + DateTime.Now.ToString("ddMMyyyyHHmmss");

            //AvailSearch URL
            availService.Url = ConfigurationSystem.WSTConfig["AvailURL"];
            //StaticDataURL
            wstStaticData.Url = ConfigurationSystem.WSTConfig["StaticURL"];

            //Booking Url
            bookingService.Url = ConfigurationSystem.WSTConfig["BookingURL"];
            XmlPath = ConfigurationSystem.WSTConfig["XmlLogPath"];
            XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            //Create Hotel Xml Log path folder
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }
           
        }
        #endregion

         #region HotelSearch
        /// <summary>
        /// This method is to get the available hotels.
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public HotelSearchResult[] GetHotelAvailability(HotelRequest req, decimal markup, string markupType)
        {
            try
            {
                //Audit.Add(EventType.WSTAvailSearch, Severity.Normal, 1, "WSTApi.GetHotelAvailability Entered", "0");
                List<HotelSearchResult> hotelResultsNew = new List<HotelSearchResult>();//This will contain the List of hotels satisfying our request.
                this.request = req;
                this.markup = markup;
                this.markupType = markupType;

                //To validate request for no of passengers in each room
                if (!IsValidRequest(req))
                {
                    // Audit.Add(EventType.WSTAvailSearch, Severity.Normal, 1, "WST.GetHotelAvailability Error Message : The request is not allowed by WST i.e 1) No of childs in each room must not exceed 2.", "0");
                    return hotelResultsNew.ToArray();
                }
                #region Request string
                string request = GenerateAvailabilityRequest(req);
                try
                {
                    XmlDocument XmlDoc = new XmlDocument();
                    XmlDoc.LoadXml(request);
                    string filePath = @"" + XmlPath + Guid.NewGuid() + "_GetavailabilityNewRequest.xml";
                    XmlDoc.Save(filePath);
                }
                catch { }
                #endregion
                //This dictionary will hold what fare search we want to call
                Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();
                //ReadySources will hold same source orderings i.e SG1, SG2 ....
                Dictionary<string, int> readySources = new Dictionary<string, int>();
                string room1Details = string.Empty;
                for (int i = 0; i < req.RoomGuest.Length; i++)  //Room wise should be sent request
                {
                    listOfThreads.Add((i + 1).ToString(), new WaitCallback(GenarateSearchrequest));
                    readySources.Add((i + 1).ToString(), 300);

                }
                eventFlag = new AutoResetEvent[req.RoomGuest.Length];
                int j = 0;
                //Start each fare search within a Thread which will automatically terminate after the results are received.
                foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                {
                    if (readySources.ContainsKey(deThread.Key))
                    {
                        ThreadPool.QueueUserWorkItem(deThread.Value, j);
                        eventFlag[j] = new AutoResetEvent(false);
                        j++;
                    }

                }
                if (j != 0)
                {
                    if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 0, 1500), true))
                    {
                        //TODO: audit which thread is timed out                
                    }
                }

                #region 
                //In case of multi room search some hotels might not conatin all the room types requested.
                //So following code is implemented to remove those

                foreach (HotelSearchResult hsr in hotelResults)
                {
                    List<string> seqNo = new List<string>();
                    foreach (HotelRoomsDetails rm in hsr.RoomDetails)
                    {
                        if (!seqNo.Contains(rm.SequenceNo))
                        {
                            seqNo.Add(rm.SequenceNo);
                        }
                    }
                    //Adding only those hotels that satisfy the requirement of the user
                    if (seqNo.Count == req.NoOfRooms)
                    {
                        hotelResultsNew.Add(hsr);
                    }
                }

                foreach (HotelSearchResult hotelResult in hotelResultsNew)
                {
                    Array.Sort(hotelResult.RoomDetails, delegate (HotelRoomsDetails rd1, HotelRoomsDetails rd2) { return rd1.TotalPrice.CompareTo(rd2.TotalPrice); });
                    for (int i = 0; i < req.NoOfRooms; i++)
                    {
                        for (int k = 0; k < hotelResult.RoomDetails.Length; k++)
                        {
                            if (hotelResult.RoomDetails[k].SequenceNo.Contains((i + 1).ToString()))
                            {
                                hotelResult.TotalPrice += hotelResult.RoomDetails[k].TotalPrice + hotelResult.RoomDetails[k].Markup;
                                hotelResult.Price.NetFare = hotelResult.TotalPrice;
                                hotelResult.Price.AccPriceType = PriceType.NetFare;
                                break;
                            }
                        }
                    }
                }
                #endregion
                return hotelResultsNew.ToArray();
            }
            catch { throw; }
        }

        private void GenarateSearchrequest(object eventNumber)
        {
            try
            {
                bool isValidReq = true;
                string responseXml = string.Empty;
                string requestXml = string.Empty;
                string room1Details = string.Empty;
                int i = Convert.ToInt32(eventNumber);
                //Passenger details adults and childs  2,1,10,0
                room1Details = this.request.RoomGuest[i].noOfAdults.ToString() + ",";
                if (this.request.RoomGuest[i].noOfChild > 0)
                {
                    room1Details += this.request.RoomGuest[i].noOfChild.ToString();
                }
                else
                {
                    room1Details += "0";
                }
                if (this.request.RoomGuest[i].childAge.Count > 0)
                {
                    if (this.request.RoomGuest[i].noOfChild <= 2)
                    {
                        for (int k = 0; k < this.request.RoomGuest[i].childAge.Count; k++)
                        {
                            room1Details += "," + this.request.RoomGuest[i].childAge[k];
                        }
                        if (this.request.RoomGuest[i].childAge.Count == 1)
                        {
                            room1Details += ",0";
                        }
                    }
                    else
                    {
                        isValidReq = false;
                    }
                }
                else
                {
                    room1Details += ",0,0";
                }
                if (isValidReq)
                {
                    try
                    {
                        responseXml = availService.getavailabilityNew(loginCode, request.PassengerNationality, string.Empty, this.request.CityCode, "HTL", this.request.StartDate.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture), this.request.EndDate.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture), string.Empty, string.Empty, 1, string.Empty, 1, room1Details, string.Empty, string.Empty, string.Empty);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.WSTAvailSearch, Severity.High, 0, "Exception returned from WST.getavailabilityNew Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + requestXml + "|response XML" + responseXml, "");
                    }
                }
                try
                {
                    if (!string.IsNullOrEmpty(responseXml))
                    {
                        GenerateSearchResult(responseXml, this.request, i, markup, markupType);
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.WSTAvailSearch, Severity.High, 0, "Exception returned from WST.GenerateSearchResult Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + requestXml + "|response XML" + responseXml, "");
                    //throw new BookingEngineException("Error: " + ex.Message);
                    //throw new Exception("Error: " + ex.Message);
                }
                eventFlag[(int)eventNumber].Set();
            }
            catch { throw; }
        }
       
        /// <summary>
        /// To parse the response XML
        /// </summary>
        /// <param name="responseXml"></param>
        /// <param name="req"></param>
        /// <param name="reqRooms"></param>
        /// <returns></returns>
        private HotelSearchResult[] GenerateSearchResult(string responseXml, HotelRequest req, int roomIndex, decimal markup, string markupType)
        {
            XmlNode tempNode;
            TextReader stringRead = new StringReader(responseXml);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            try
            {

                string filePath = @"" + XmlPath + Guid.NewGuid() + "_getavailabilityNewResponse" + (roomIndex + 1) + ".xml";
                xmlDoc.Save(filePath);
            }
            catch { }

            //Loading All Hotels Static data and images city wise
            //optimized Code
            DataTable dtHotels = HotelStaticData.GetStaticHotelIds(req.CityCode, HotelBookingSource.WST);
            DataTable dtImages = HotelImages.GetImagesByCityCode(req.CityCode, HotelBookingSource.WST);

            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("NewDataSet/Table");
            if (ErrorInfo == null)
            {
                Audit.Add(EventType.WSTAvailSearch, Severity.High, 0, "Error Message: Result not coming from WST | " + DateTime.Now + "| Response XML" + responseXml, "");
                throw new BookingEngineException("<br>No Results Found");
            }
            else
            {
                PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                int r = roomIndex + 1;
                Dictionary<string, string> hotelCodes = new Dictionary<string, string>();
                XmlNodeList tempNodeList = xmlDoc.SelectNodes("NewDataSet/Table");
                foreach (XmlNode temp in tempNodeList)
                {
                    tempNode = temp.SelectSingleNode("available");
                    if (tempNode != null && tempNode.InnerText == "0")
                    {
                        continue;
                    }
                    HotelSearchResult hotelResult = new HotelSearchResult();
                    hotelResult.AvailType = AvailabilityType.Confirmed;
                    hotelResult.BookingSource = HotelBookingSource.WST;
                    hotelResult.CityCode = req.CityCode;
                    hotelResult.Currency = currency;
                    hotelResult.StartDate = req.StartDate;
                    hotelResult.EndDate = req.EndDate;
                    tempNode = temp.SelectSingleNode("partycode");
                    if (tempNode != null)
                    {
                        hotelResult.HotelCode = tempNode.InnerText;
                    }
                    tempNode = temp.SelectSingleNode("partyname");
                    if (tempNode != null)
                    {
                        hotelResult.HotelName = tempNode.InnerText;
                    }
                    tempNode = temp.SelectSingleNode("catcode");
                    if (tempNode != null)
                    {
                        string star = tempNode.InnerText;
                        switch (star)
                        {
                            case "TWO": hotelResult.Rating = HotelRating.TwoStar;
                                break;
                            case "THREE": hotelResult.Rating = HotelRating.ThreeStar;
                                break;
                            case "FOUR": hotelResult.Rating = HotelRating.FourStar;
                                break;
                            case "FIVE": hotelResult.Rating = HotelRating.FiveStar;
                                break;
                        }
                    }
                    //to get only those satisfy the search conditions
                    string pattern = req.HotelName;
                    if (Convert.ToInt16(hotelResult.Rating) >= (int)req.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)req.MaxRating)
                    {
                        if (!(pattern != null && pattern.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(pattern.ToUpper()) : true))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }

                    if (!hotelCodes.ContainsKey(hotelResult.HotelCode))
                    {
                        #region Modified by brahmam search time avoid Staticinfo downloading
                        DataRow[] hotelStaticData = new DataRow[0];
                        try
                        {
                            hotelStaticData = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");
                            if (hotelStaticData != null && hotelStaticData.Length > 0)
                            {
                                hotelResult.HotelDescription = hotelStaticData[0]["description"].ToString();
                                hotelResult.HotelAddress = hotelStaticData[0]["address"].ToString();
                                hotelResult.HotelMap = hotelStaticData[0]["hotelMaps"].ToString();
                                hotelResult.HotelLocation = hotelStaticData[0]["location"].ToString();
                            }
                        }
                        catch { continue; }

                        DataRow[] hotelImages = new DataRow[0];
                        try
                        {
                            hotelImages = dtImages.Select("hotelCode='" + hotelResult.HotelCode + "'");
                            string hImages = (hotelImages != null && hotelImages.Length > 0 ? hotelImages[0]["images"].ToString() : string.Empty);
                            hotelResult.HotelPicture = string.Empty;
                            if (!string.IsNullOrEmpty(hImages))
                            {
                                hotelResult.HotelPicture = hImages.Split('|')[0];
                            }
                        }
                        catch { continue; }

                        #endregion

                        hotelCodes.Add(hotelResult.HotelCode, hotelResult.HotelCode);
                    }
                    hotelResult.RoomGuest = request.RoomGuest;
                    List<HotelRoomsDetails> roomDetails = new List<HotelRoomsDetails>();
                    HotelRoomsDetails rmDetail = new HotelRoomsDetails();
                    string promotionDesc = string.Empty;
                    tempNode = temp.SelectSingleNode("rmtypname");
                    if (tempNode != null)
                    {
                        rmDetail.RoomTypeName = tempNode.InnerText;
                    }
                    tempNode = temp.SelectSingleNode("rmtypcode");
                    if (tempNode != null)
                    {
                        rmDetail.RoomTypeCode = tempNode.InnerText;
                    }
                    tempNode = temp.SelectSingleNode("XML_ID");
                    if (tempNode != null)
                    {
                        rmDetail.RoomTypeCode += "|" + tempNode.InnerText;
                    }
                    tempNode = temp.SelectSingleNode("Cartid");
                    if (tempNode != null)
                    {
                        rmDetail.RoomTypeCode += "|" + tempNode.InnerText + "|" + r.ToString();
                    }
                    rmDetail.RoomTypeCode += "|" + hotelResult.HotelName;
                    // ROOMTYPECODE------- rtCode|XMLID|CartID|Sequence|HotelName
                    tempNode = temp.SelectSingleNode("mealcode");
                    if (tempNode != null)
                    {
                        rmDetail.RatePlanCode = tempNode.InnerText;
                    }
                    tempNode = temp.SelectSingleNode("rmcatcode");
                    if (tempNode != null)
                    {
                        rmDetail.RatePlanCode += "|" + tempNode.InnerText.Trim();
                    }
                    tempNode = temp.SelectSingleNode("ratetype");
                    if (tempNode != null)
                    {
                        rmDetail.RatePlanCode += "|" + tempNode.InnerText.Trim();
                    }
                    // RATEPLANCODE-------- mealcode|rmcatCode|rateType
                    //tempNode = temp.SelectSingleNode("special");
                    //if (tempNode != null)
                    //{
                    //    rmDetail.RatePlanCode += "|" + tempNode.InnerText;
                    //}

                    // RATEPLANCODE-------- mealcode|rmcatCode|rateType|promotion
                    tempNode = temp.SelectSingleNode("mealname");
                    if (tempNode != null)
                    {
                        rmDetail.Amenities = new List<string>();
                        rmDetail.Amenities.Add(tempNode.InnerText);
                        rmDetail.mealPlanDesc = tempNode.InnerText;
                    }
                    tempNode = temp.SelectSingleNode("split"); //split means same rooms is coming diffent nodes
                    if (tempNode != null)
                    {
                        rmDetail.Supplements = tempNode.InnerText;
                    }

                    DateTime frmdate = new DateTime();
                    DateTime todate = new DateTime();
                    tempNode = temp.SelectSingleNode("frmdate");
                    if (tempNode != null)
                    {
                        frmdate = Convert.ToDateTime(tempNode.InnerText);
                    }
                    tempNode = temp.SelectSingleNode("todate");
                    if (tempNode != null)
                    {
                        todate = Convert.ToDateTime(tempNode.InnerText);
                    }
                    //price node in each hotel represents charge /night
                    decimal pricePerNight = 0;
                    tempNode = temp.SelectSingleNode("price");
                    if (tempNode != null)
                    {
                        pricePerNight = Convert.ToDecimal(tempNode.InnerText);
                    }
                    else
                    {
                        continue;
                    }
                    rateOfExchange = exchangeRates[hotelResult.Currency];
                    System.TimeSpan diffResult = todate.Subtract(frmdate);
                    System.TimeSpan diffDates = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                    pricePerNight = pricePerNight * diffResult.Days;
                    rmDetail.SequenceNo = r.ToString();
                    bool IsSplit = false;
                    if (hotelResults.Count > 0 && rmDetail.Supplements == "1") //split time should be combined what ever splited same rooms
                    {
                        for (int n = 0; n < hotelResults.Count; n++)
                        {
                            if (hotelResult.HotelCode == hotelResults[n].HotelCode)
                            {
                                for (int s = 0; s < hotelResults[n].RoomDetails.Length; s++)
                                {
                                    if (hotelResults[n].RoomDetails[s].RatePlanCode == rmDetail.RatePlanCode && hotelResults[n].RoomDetails[s].RoomTypeName == rmDetail.RoomTypeName && hotelResults[n].RoomDetails[s].SequenceNo == rmDetail.SequenceNo)
                                    {
                                        IsSplit = true;
                                        pricePerNight += hotelResults[n].RoomDetails[s].supplierPrice;

                                        //VAT Calucation Changes Modified 14.12.2017
                                        decimal hotelTotalPrice = 0m;
                                        decimal vatAmount = 0m;
                                        hotelTotalPrice = Math.Round(((pricePerNight) * rateOfExchange), decimalPoint);
                                        if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                                        {
                                            hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                                        }
                                        hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);

                                        hotelResults[n].RoomDetails[s].TotalPrice = hotelTotalPrice;
                                        hotelResults[n].RoomDetails[s].Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(hotelResults[n].RoomDetails[s].TotalPrice) * (markup / 100m)));
                                        hotelResults[n].RoomDetails[s].MarkupType = markupType;
                                        hotelResults[n].RoomDetails[s].MarkupValue = markup;
                                        hotelResults[n].RoomDetails[s].SellingFare = hotelResults[n].RoomDetails[s].TotalPrice;
                                        hotelResults[n].RoomDetails[s].supplierPrice = pricePerNight;
                                        hotelResults[n].RoomDetails[s].TaxDetail = new PriceTaxDetails();
                                        hotelResults[n].RoomDetails[s].TaxDetail = priceTaxDet;
                                        hotelResults[n].RoomDetails[s].InputVATAmount = vatAmount;

                                        hotelResult.Price = new PriceAccounts();
                                        hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                                        hotelResult.Price.SupplierPrice = pricePerNight;
                                        hotelResult.Price.RateOfExchange = rateOfExchange;
                                        hotelResult.Currency = agentCurrency;
                                        //day wise fare breakdown

                                        RoomRates[] hRoomRatesList = new RoomRates[diffDates.Days];
                                        decimal totprice = hotelResults[n].RoomDetails[s].TotalPrice;
                                        for (int m = 0; m < diffDates.Days; m++)
                                        {
                                            decimal price = hotelResults[n].RoomDetails[s].TotalPrice / diffDates.Days;
                                            if (m == diffDates.Days - 1)
                                            {
                                                price = totprice;
                                            }
                                            totprice -= price;
                                            hRoomRatesList[m].Amount = price;
                                            hRoomRatesList[m].BaseFare = price;
                                            hRoomRatesList[m].SellingFare = price;
                                            hRoomRatesList[m].Totalfare = price;
                                            hRoomRatesList[m].Days = req.StartDate.AddDays(m);
                                            hRoomRatesList[m].RateType = RateType.Negotiated;
                                        }
                                        hotelResults[n].RoomDetails[s].Rates = hRoomRatesList;
                                        break;
                                    }

                                }

                            }
                        }
                    }
                    if (IsSplit)
                    {
                        continue;
                    }
                    //VAT Calucation Changes Modified 14.12.2017
                    decimal hotelTotalPrice1 = 0m;
                    decimal vatAmount1 = 0m;
                    hotelTotalPrice1 = Math.Round(((pricePerNight) * rateOfExchange), decimalPoint);
                    if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                    {
                        hotelTotalPrice1 = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice1, ref vatAmount1, decimalPoint);
                    }
                    hotelTotalPrice1 = Math.Round(hotelTotalPrice1, decimalPoint);

                    rmDetail.TotalPrice = hotelTotalPrice1;
                    rmDetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(rmDetail.TotalPrice) * (markup / 100m)));
                    rmDetail.MarkupType = markupType;
                    rmDetail.MarkupValue = markup;
                    rmDetail.SellingFare = rmDetail.TotalPrice;
                    rmDetail.supplierPrice = pricePerNight;
                    rmDetail.TaxDetail = new PriceTaxDetails();
                    rmDetail.TaxDetail = priceTaxDet;
                    rmDetail.InputVATAmount = vatAmount1;

                    hotelResult.Price = new PriceAccounts();
                    hotelResult.Price.SupplierCurrency = hotelResult.Currency;
                    hotelResult.Price.SupplierPrice = pricePerNight;
                    hotelResult.Price.RateOfExchange = rateOfExchange;
                    hotelResult.Currency = agentCurrency;
                    
                    //day wise fare breakdown
                    RoomRates[] hRoomRates = new RoomRates[diffDates.Days];
                    decimal totalprice = rmDetail.TotalPrice;
                    for (int m = 0; m < diffDates.Days; m++)
                    {
                        decimal price = rmDetail.TotalPrice / diffDates.Days;
                        if (m == diffDates.Days - 1)
                        {
                            price = totalprice;
                        }
                        totalprice -= price;
                        hRoomRates[m].Amount = price;
                        hRoomRates[m].BaseFare = price;
                        hRoomRates[m].SellingFare = price;
                        hRoomRates[m].Totalfare = price;
                        hRoomRates[m].Days = req.StartDate.AddDays(m);
                        hRoomRates[m].RateType = RateType.Negotiated;
                    }
                    rmDetail.Rates = hRoomRates;

                    roomDetails.Add(rmDetail);

                    bool flag = false;
                    if (hotelResults.Count > 0)
                    {
                        for (int n = 0; n < hotelResults.Count; n++)
                        {
                            if (hotelResult.HotelCode == hotelResults[n].HotelCode)
                            {
                                flag = true;
                                foreach (HotelRoomsDetails rmDtl in hotelResults[n].RoomDetails)
                                {
                                    roomDetails.Add(rmDtl);
                                }
                                if (string.IsNullOrEmpty(hotelResults[n].PromoMessage))
                                {
                                    hotelResults[n].PromoMessage = promotionDesc;
                                }
                                hotelResults[n].RoomDetails = roomDetails.ToArray();
                                Dictionary<int, string> roomTypeCode = new Dictionary<int, string>();
                                for (int i = 0; i < hotelResults[n].RoomDetails.Length; i++)
                                {
                                    if (!roomTypeCode.ContainsValue(hotelResults[n].RoomDetails[i].RoomTypeCode))
                                    {
                                        roomTypeCode.Add(i, hotelResults[n].RoomDetails[i].RoomTypeCode);
                                    }
                                    else
                                    {
                                        hotelResults[n].RoomDetails[i].RoomTypeCode = hotelResults[n].RoomDetails[i].RoomTypeCode + "|" + i.ToString();
                                        roomTypeCode.Add(i, hotelResults[n].RoomDetails[i].RoomTypeCode);
                                    }
                                }
                                break;
                            }
                        }
                        if (!flag)
                        {
                            Dictionary<int, string> roomTypeCode = new Dictionary<int, string>();
                            hotelResult.PromoMessage = promotionDesc;
                            hotelResult.RoomDetails = roomDetails.ToArray();
                            for (int i = 0; i < hotelResult.RoomDetails.Length; i++)
                            {
                                if (!roomTypeCode.ContainsValue(hotelResult.RoomDetails[i].RoomTypeCode))
                                {
                                    roomTypeCode.Add(i, hotelResult.RoomDetails[i].RoomTypeCode);
                                }

                                hotelResult.RoomDetails[i].RoomTypeCode = hotelResult.RoomDetails[i].RoomTypeCode + "|" + i.ToString();
                            }
                            if (hotelResult.RoomDetails.Length > 0)
                            {
                                hotelResults.Add(hotelResult);
                            }
                        }
                    }
                    else
                    {
                        hotelResult.RoomDetails = roomDetails.ToArray();
                        if (hotelResult.RoomDetails.Length > 0)
                        {
                            hotelResults.Add(hotelResult);
                        }
                    }
                }
            }
            return hotelResults.ToArray();
        }


         private string GenerateAvailabilityRequest(HotelRequest req)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("getavailabilityNew");

            xmlString.WriteElementString("agentregcode", loginCode);
            xmlString.WriteElementString("natcode", req.PassengerNationality);
            xmlString.WriteElementString("ctrycode", "");
            xmlString.WriteElementString("citycode", req.CityCode);
            xmlString.WriteElementString("sptype", "HTL");
            xmlString.WriteElementString("frmdate", req.StartDate.ToString("yyyy/MM/dd"));
            xmlString.WriteElementString("todate", req.EndDate.ToString("yyyy/MM/dd"));
            xmlString.WriteElementString("catcode", "");
            xmlString.WriteElementString("partycode", "");
            xmlString.WriteElementString("availableonly", "1");
            xmlString.WriteElementString("rmtypcode", "");
            xmlString.WriteElementString("units", "1");
            for (int i = 0; i < req.RoomGuest.Length; i++)
            {
                xmlString.WriteStartElement("Room" + (i+1) + "Details");
                xmlString.WriteElementString("Adults", req.RoomGuest[i].noOfAdults.ToString());
                xmlString.WriteElementString("Childs", req.RoomGuest[i].noOfChild.ToString());
                if (req.RoomGuest[i].noOfChild > 0)
                {
                    xmlString.WriteStartElement("ChildAges");
                    for (int j = 0; j < req.RoomGuest[i].childAge.Count; j++)
                    {
                        xmlString.WriteElementString("Age", req.RoomGuest[i].childAge[j].ToString());
                    }
                    xmlString.WriteEndElement();
                }
                xmlString.WriteEndElement();
            }
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
         /// <summary>
         /// This method is to check if the request is valid as per  WST
         /// </summary>
         /// <param name="req"></param>
         /// <returns></returns>
         private bool IsValidRequest(HotelRequest req)
         {
             //White sands max passenger count in each room is 4
             bool isValidRequest = true;
             int paxCount = 0;
             for (int i = 0; i < req.NoOfRooms; i++)
             {
                 paxCount = req.RoomGuest[i].noOfAdults + req.RoomGuest[i].noOfChild;
                 if (paxCount > 9 || req.RoomGuest[i].noOfChild > 2)
                 {
                     isValidRequest = false;
                     break;
                 }
             }
             return isValidRequest;
         }
        #endregion

         #region Booking
        /// <summary>
        /// this method is to make booking
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public BookingResponse GetBooking(ref HotelItinerary itinerary)
        {
            string requestXml = string.Empty;
            string ourRefNo = cozRef;
            requestXml = GenerateBookingRequest(itinerary, ourRefNo);
           
            string resp = string.Empty;
            try
            {
                resp = bookingService.checkBookingDataNew(loginCode, requestXml);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.WSTBooking, Severity.High, 0, "Exception returned from WST.GetBooking Error Message:" + ex.Message + " | Request XML:" + requestXml + "| Response XML :" + resp + DateTime.Now, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Audit.Add(EventType.WSTBooking, Severity.High, 0, "Booking from WST. Response:" + resp + " | Request XML:" + requestXml + DateTime.Now, "");

            // Process the Response.
            BookingResponse searchRes = new BookingResponse();
            try
            {
                searchRes = ReadResponseBooking(resp, ref itinerary, ourRefNo);
            }
            catch (Exception ex)
            { 
                Audit.Add(EventType.WSTBooking, Severity.High, 0, "Exception returned from WST.GetBooking Error Message:" + ex.Message + " | Request XML:" + requestXml + "| Response XML :" + resp + DateTime.Now, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            
            return searchRes;
        }
        
        /// <summary>
        /// To create XML request
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="agentXO"></param>
        /// <returns></returns>
        private string GenerateBookingRequest(HotelItinerary itinerary, string agentXO)
        {
            string resp = string.Empty;
            string request = string.Empty;
            string promotionId = string.Empty;
            string timeLimit = string.Empty;
            Dictionary<string, List<string>> rateDet;
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteStartElement("TEST");
           
            for (int i = 0; i < itinerary.Roomtype.Length; i++)
            {
               
                // ROOMTYPECODE------- rtCode|XMLID|CartID|Sequence|HotelName
                string[] rtCode = itinerary.Roomtype[i].RoomTypeCode.Split('|');
                // RATEPLANCODE-------- mealcode|rmcatCode|rateType
                string[] rpCode = itinerary.Roomtype[i].RatePlanCode.Split('|');

                try
                {
                    request = GenerateChargeConditionRequest(loginCode, rtCode[1], Convert.ToInt16(rtCode[2]), itinerary.HotelCode, rtCode[0], rpCode[1], rpCode[0], rpCode[2], 3);
                }
                catch(Exception ex)
                {
                    Audit.Add(EventType.WSTBooking, Severity.High, 0, "Exception returned from WST.GenerateChargeConditionRequest Error Message:" + ex.Message, "");
                }
                try
                {
                    XmlDocument XmlDoc = new XmlDocument();
                    XmlDoc.LoadXml(request);
                    string filePath = @"" + XmlPath + Guid.NewGuid() + "_BeforeBookinggetAvailabiltyDetRequest" + (i+1) + ".xml";
                    XmlDoc.Save(filePath);
                }
                catch { }

                //Data to get from WST before booking.
                try
                {
                    resp = availService.getAvailabiltyDet(loginCode, rtCode[1], Convert.ToInt16(rtCode[2]), itinerary.HotelCode, rtCode[0], rpCode[1], rpCode[0], rpCode[2], 3);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.WSTBooking, Severity.High, 0, "Exception returned from WST.getAvailabiltyDet Error Message:" + ex.Message, "");
                }
                try
                {
                    XmlDocument XmlDoc = new XmlDocument();
                    XmlDoc.LoadXml(resp);
                    string filePath = @"" + XmlPath + Guid.NewGuid() + "_BeforeBookinggetAvailabiltyDetResponse" + (i+1) + ".xml";
                    XmlDoc.Save(filePath);
                }
                catch { }

                //Load price details and split
                rateDet = new Dictionary<string, List<string>>();
                rateDet = GetRateDetail(resp, itinerary);

                //Read promation Id    
                promotionId = ReadPromotionId(resp, ref timeLimit);
                int lino = 1;
                //split time genarating multiple bookings 
                for (int c = 0; c < rateDet["lv"].Count; c++)
                {
                    xmlString.WriteStartElement("htlrequestdet");
                    xmlString.WriteElementString("requestid", rtCode[1]);
                    xmlString.WriteElementString("line_no", lino.ToString());
                    lino++; //split time every request line no increse and 2 rooms time need reset lino
                    xmlString.WriteElementString("partycode", itinerary.HotelCode);
                    xmlString.WriteElementString("rmtypcode", rtCode[0]);
                    xmlString.WriteElementString("mealcode", rpCode[0]);
                    xmlString.WriteElementString("noofrooms", "1");
                    xmlString.WriteElementString("noofadults", itinerary.Roomtype[i].AdultCount.ToString());
                    xmlString.WriteElementString("noofchild", itinerary.Roomtype[i].ChildCount.ToString());

                    int p = 0;
                    //Adult Title
                    ///////////////////////////////brahmam////////////////////////////////////////////////////
                    //1.every room need to send 4 pax nodes(ex 4 title,firstname......)
                    //2.if 1 pax is there need to send first pax details and send oather details empty
                    //3.if 4 adult 2 childs time need to send first 2 adult and 2 childs(max 2 childs) Total 4 pax nodes
                    ////////////////////////////////////////////////////////////////////////////////////////////////

                    int serial = 1;
                    for (p = 1; p <= itinerary.Roomtype[i].PassenegerInfo.Count; p++)
                    {
                        if ((itinerary.Roomtype[i].PassenegerInfo[p - 1].PaxType == HotelPaxType.Adult) && serial <= 2)
                        {
                            xmlString.WriteElementString("title" + serial.ToString(), itinerary.Roomtype[i].PassenegerInfo[p - 1].Title);
                            serial++;
                        }
                        else if (itinerary.Roomtype[i].PassenegerInfo[p - 1].PaxType == HotelPaxType.Child)
                        {
                            xmlString.WriteElementString("title" + serial.ToString(), itinerary.Roomtype[i].PassenegerInfo[p - 1].Title);
                            serial++;
                        }
                    }
                    for (int z = serial; z <= 4; z++)
                    {
                        xmlString.WriteElementString("title" + z.ToString(), "");
                    }
                    //familyname
                    serial = 1;
                    for (p = 1; p <= itinerary.Roomtype[i].PassenegerInfo.Count; p++)
                    {
                        if ((itinerary.Roomtype[i].PassenegerInfo[p - 1].PaxType == HotelPaxType.Adult) && serial <= 2)
                        {
                            xmlString.WriteElementString("familyname" + serial.ToString(), itinerary.Roomtype[i].PassenegerInfo[p - 1].Lastname);
                            serial++;
                        }
                        else if (itinerary.Roomtype[i].PassenegerInfo[p - 1].PaxType == HotelPaxType.Child)
                        {
                            xmlString.WriteElementString("familyname" + serial.ToString(), itinerary.Roomtype[i].PassenegerInfo[p - 1].Lastname);
                            serial++;
                        }
                    }
                    for (int z = serial; z <= 4; z++)
                    {
                        xmlString.WriteElementString("familyname" + z.ToString(), "");
                    }
                    //firstname
                    serial = 1;
                    for (p = 1; p <= itinerary.Roomtype[i].PassenegerInfo.Count; p++)
                    {
                        if ((itinerary.Roomtype[i].PassenegerInfo[p - 1].PaxType == HotelPaxType.Adult) && serial <= 2)
                        {
                            xmlString.WriteElementString("firstname" + serial.ToString(), itinerary.Roomtype[i].PassenegerInfo[p - 1].Firstname);
                            serial++;
                        }
                        else if (itinerary.Roomtype[i].PassenegerInfo[p - 1].PaxType == HotelPaxType.Child)
                        {
                            xmlString.WriteElementString("firstname" + serial.ToString(), itinerary.Roomtype[i].PassenegerInfo[p - 1].Firstname);
                            serial++;
                        }
                    }
                    for (int z = serial; z <= 4; z++)
                    {
                        xmlString.WriteElementString("firstname" + z.ToString(), "");
                    }
                    //age
                    serial = 1;
                    for (p = 1; p <= itinerary.Roomtype[i].PassenegerInfo.Count; p++)
                    {
                        if ((itinerary.Roomtype[i].PassenegerInfo[p - 1].PaxType == HotelPaxType.Adult) && serial <= 2)
                        {
                            xmlString.WriteElementString("age" + serial.ToString(), "0");
                            serial++;
                        }
                        else if (itinerary.Roomtype[i].PassenegerInfo[p - 1].PaxType == HotelPaxType.Child)
                        {
                            xmlString.WriteElementString("age" + serial.ToString(), itinerary.Roomtype[i].PassenegerInfo[p - 1].Age.ToString());
                            serial++;
                        }
                    }
                    for (int z = serial; z <= 4; z++)
                    {
                        xmlString.WriteElementString("age" + z.ToString(), "0");
                    }
                    xmlString.WriteElementString("aflightno", "TBA");
                    xmlString.WriteElementString("aflightdate", itinerary.StartDate.ToString("yyyy-MM-ddTHH:mm:ss"));
                    xmlString.WriteElementString("aflighttime", "15:00");
                    xmlString.WriteElementString("dflightno", "TBA");
                    xmlString.WriteElementString("dflightdate", itinerary.EndDate.ToString("yyyy-MM-ddTHH:mm:ss"));
                    xmlString.WriteElementString("dflighttime", "15:00");
                    xmlString.WriteElementString("chkindate", Convert.ToDateTime(rateDet["fr"][c]).ToString("yyyy-MM-ddTHH:mm:ss"));
                    xmlString.WriteElementString("chkoutdate", Convert.ToDateTime(rateDet["to"][c]).ToString("yyyy-MM-ddTHH:mm:ss"));//.EndDate.ToString("yyyy-MM-ddTHH:mm:ss"));
                    if (itinerary.SpecialRequest != null)
                    {
                        xmlString.WriteElementString("remarks", itinerary.SpecialRequest);
                    }
                    else
                    {
                        xmlString.WriteElementString("remarks", "");
                    }
                    xmlString.WriteElementString("agentxoref", agentXO);
                    xmlString.WriteElementString("nights", rateDet["ni"][c]);
                    xmlString.WriteElementString("requestdate", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
                    xmlString.WriteElementString("availableyesno", "1");

                    //time limit is same as it comes from White sands
                    xmlString.WriteElementString("time_limit", timeLimit);
                    xmlString.WriteElementString("subuser", "");
                    xmlString.WriteElementString("block", "0");
                    xmlString.WriteElementString("minnights", "0");
                    xmlString.WriteElementString("message", "");

                    xmlString.WriteStartElement("htlrequestsubdet");
                    xmlString.WriteElementString("requestid", rtCode[1]);
                    xmlString.WriteElementString("line_no", Convert.ToString(c + 1));
                    xmlString.WriteElementString("rmcatcode", rpCode[1]);
                    xmlString.WriteElementString("units", "1");
                    xmlString.WriteElementString("linevalue", rateDet["lv"][c]);
                    xmlString.WriteElementString("plistcode", rateDet["lc"][c]);
                    xmlString.WriteElementString("plistlineno", rateDet["ll"][c]);
                    xmlString.WriteElementString("ratetype", rpCode[2]);
                    xmlString.WriteElementString("pkgnights", "1");
                    xmlString.WriteElementString("freenights", rateDet["fn"][c]);
                    xmlString.WriteElementString("promotionid", promotionId);
                    xmlString.WriteElementString("agentsellprice", rateDet["lv"][c]);
                    xmlString.WriteEndElement();
                    xmlString.WriteEndElement();
                }
            }
            xmlString.WriteEndElement();
            xmlString.Close();
            try
            {
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(strWriter.ToString());
                string filePath = @"" + XmlPath + Guid.NewGuid() + "_checkBookingDataNewRequest.xml";
                XmlDoc.Save(filePath);
            }
            catch { }
            strWriter = strWriter.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
            strWriter = strWriter.Replace("<TEST>", "");
            strWriter = strWriter.Replace("</TEST>", "");
            return strWriter.ToString();
        }

        /// <summary>
        /// after booking should commit
        /// </summary>
        /// <param name="ConfirmationNo"></param>
        /// <param name="status"></param>
        /// <param name="refNo"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        public string BookingCommitOrRollBack(string ConfirmationNo, BookStaus status, string refNo, ref bool isCommit)
        {
            string resp = string.Empty;
            string confirNo = string.Empty;
            string request=string.Empty;
            request = GenarateCommitRollBackRequest(loginCode, ConfirmationNo, refNo, status.ToString());
            TextReader textReader = new StringReader(request);
            XmlDocument doc = new XmlDocument();
            doc.Load(textReader);
            try
            {
                string filePath = @"" + XmlPath + Guid.NewGuid() + "_BookingCommitOrRollBackRequest.xml";
                doc.Save(filePath);
            }
            catch { }
            try
            {
                resp = bookingService.BookingCommitOrRollBack(loginCode, ConfirmationNo, refNo, status.ToString());
            }
            catch { }
            XmlNode tempNode;
            TextReader stringRead = new StringReader(resp);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + XmlPath + Guid.NewGuid() + "_BookingCommitOrRollBackResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("request/ReqStatus");
            if (ErrorInfo == null)
            {
                ErrorInfo = xmlDoc.SelectSingleNode("request/message");
                Audit.Add(EventType.WSTBooking, Severity.High, 0, "Error Returned from WST. Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + resp, "");
                confirNo = ErrorInfo.InnerText;
            }
            else
            {
                tempNode = xmlDoc.SelectSingleNode("request/ReqStatus");
                if (tempNode != null && tempNode.InnerText == "A")//A means successfully Booked
                {
                    tempNode = xmlDoc.SelectSingleNode("request/message");
                    if (tempNode != null)
                    {
                        confirNo = tempNode.InnerText;
                    }
                    isCommit = true;
                }
                else
                {
                    confirNo = "Booking RollBacked";
                    isCommit = false;
                }
            }
            return confirNo;
        }

        private string GenarateCommitRollBackRequest(string loginCode, string ConfirmationNo, string refNo, string status)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("BookingCommitOrRollBack");
            xmlString.WriteElementString("agentregcode", loginCode);
            xmlString.WriteElementString("RequestId", ConfirmationNo);
            xmlString.WriteElementString("AgentXo", refNo);
            xmlString.WriteElementString("BookingStatus", status);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        /// <summary>
        /// To get free Nights,lineno,lineValue,lineCode
        /// </summary>
        /// <param name="resp"></param>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private Dictionary<string, List<string>> GetRateDetail(string resp, HotelItinerary itinerary)
        {
            TextReader stringRead = new StringReader(resp);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            Dictionary<string, List<string>> rateDetails = new Dictionary<string, List<string>>();
            List<string> lineVal = new List<string>();
            List<string> listCode = new List<string>();
            List<string> listLine = new List<string>();
            List<string> freeNights = new List<string>();
            List<string> nights = new List<string>();
            List<string> fromDate = new List<string>();
            List<string> toDate = new List<string>();
            XmlNode tempNode;

            XmlNodeList tempNodeList = xmlDoc.SelectNodes("NewDataSet/getAvailabiltyDet1");
            foreach (XmlNode temp in tempNodeList)
            {
                tempNode = temp.SelectSingleNode("FreeNights");
                if (tempNode != null)
                {
                    freeNights.Add(tempNode.InnerText);
                }
                tempNode = temp.SelectSingleNode("Plistcode");
                if (tempNode != null)
                {
                    listCode.Add(tempNode.InnerText);
                }
                tempNode = temp.SelectSingleNode("PlistLineNo");
                if (tempNode != null)
                {
                    listLine.Add(tempNode.InnerText);
                }
                tempNode = temp.SelectSingleNode("Price");
                if (tempNode != null)
                {
                    lineVal.Add(tempNode.InnerText);
                }
                tempNode = temp.SelectSingleNode("Nights");
                if (temp != null)
                {
                    nights.Add(tempNode.InnerText);
                }
                tempNode = temp.SelectSingleNode("FrmDate");
                if (temp != null)
                {
                    fromDate.Add(tempNode.InnerText);
                }
                tempNode = temp.SelectSingleNode("ToDate");
                if (temp != null)
                {
                    toDate.Add(tempNode.InnerText);
                }
            }
            rateDetails.Add("fn", freeNights);
            rateDetails.Add("lv", lineVal);
            rateDetails.Add("lc", listCode);
            rateDetails.Add("ll", listLine);
            rateDetails.Add("ni", nights);
            rateDetails.Add("fr", fromDate);
            rateDetails.Add("to", toDate);
            return rateDetails;
        }
        /// <summary>
        /// This method is to read get the promotion Id and timilimit for the booking
        /// </summary>
        /// <param name="resp"></param>
        /// <param name="timeLimit"></param>
        /// <returns></returns>
        private string ReadPromotionId(string resp, ref string timeLimit)
        {
            TextReader stringRead = new StringReader(resp);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            XmlNode tempNode;
            string promotionId = string.Empty;
            tempNode = xmlDoc.SelectSingleNode("NewDataSet/getAvailabiltyDet/timelimit");
            if (tempNode != null)
            {
                DateTime tl = DateTime.ParseExact(tempNode.InnerText, "yyyy/MM/dd", CultureInfo.InvariantCulture); // yyyy/MM/dd
                timeLimit = tl.ToString("yyyy-MM-ddTHH:mm:ss");
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/getAvailabiltyDet/promotionid");
                if (tempNode != null)
                {
                    promotionId = tempNode.InnerText;
                }
            }
            return promotionId;
        }
        /// <summary>
        /// To parse the booking response
        /// </summary>
        /// <param name="resp"></param>
        /// <param name="itinerary"></param>
        /// <param name="ourRefNo"></param>
        /// <returns></returns>
        private BookingResponse ReadResponseBooking(string resp, ref HotelItinerary itinerary, string ourRefNo)
        {
            BookingResponse hotelBookRes = new BookingResponse();
            XmlNode tempNode;
            TextReader stringRead = new StringReader(resp);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + XmlPath + Guid.NewGuid() + "_checkBookingDataNewResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("request/ReqStatus");
            if (ErrorInfo == null)
            {
                ErrorInfo = xmlDoc.SelectSingleNode("request/message");
                Audit.Add(EventType.WSTBooking, Severity.High, 0, "Error Returned from WST. Error Message:" + ErrorInfo.InnerText + " | " + DateTime.Now + "| Response XML" + resp, "");
                hotelBookRes = new BookingResponse(BookingResponseStatus.Failed, ErrorInfo.InnerText, string.Empty, ProductType.Hotel, "");
            }
            else
            {
                tempNode = xmlDoc.SelectSingleNode("request/ReqStatus");
                if (tempNode != null && tempNode.InnerText == "Pending")
                {
                    tempNode = xmlDoc.SelectSingleNode("request/message");
                    string confirmationNo = string.Empty;
                    bool isCommit = false;
                    if (tempNode != null)
                    {
                        confirmationNo = BookingCommitOrRollBack(tempNode.InnerText, BookStaus.Commit, ourRefNo, ref isCommit);

                    }
                    if (isCommit)
                    {
                        //Audit.Add(EventType.WSTBooking, Severity.High, 0, "Booking successfully created from WST. Confirmation:" + confirmationNo + " | " + DateTime.Now, "");
                        itinerary.ConfirmationNo = confirmationNo;
                        itinerary.BookingRefNo = ourRefNo;
                        itinerary.AgencyReference = ourRefNo;
                        itinerary.Status = HotelBookingStatus.Confirmed;
                        hotelBookRes = new BookingResponse(BookingResponseStatus.Successful, "", "", ProductType.Hotel, itinerary.ConfirmationNo);
                        itinerary.PaymentGuaranteedBy = "Payment is guaranteed by: WST, as per final booking form reference No: " + itinerary.ConfirmationNo;
                    }
                    else
                    {
                        Audit.Add(EventType.WSTBooking, Severity.High, 0, "Error Returned from WST. Error Message:" + confirmationNo, "");
                        hotelBookRes = new BookingResponse(BookingResponseStatus.Failed, "", "", ProductType.Hotel, "");
                        return hotelBookRes;
                    }
                }
                else
                {
                    tempNode = xmlDoc.SelectSingleNode("request/message");
                    Audit.Add(EventType.WSTBooking, Severity.High, 0, "Error Returned from WST. Error Message:" + tempNode.InnerText + " | " + DateTime.Now + "| Response XML" + resp, "");
                    hotelBookRes = new BookingResponse(BookingResponseStatus.Failed, tempNode.InnerText, "", ProductType.Hotel, "");
                    return hotelBookRes;
                }
            }
            return hotelBookRes;
        }

        #endregion

         #region StaticData
        /// <summary>
        /// This method is to bring static data for the hotels
        /// </summary>
        /// <param name="cityCode"></param>
        /// <param name="itemName"></param>
        /// <param name="itemCode"></param>
        /// <returns></returns>
        public HotelDetails GetItemInformation(string cityCode, string itemName, string itemCode)
        {
            string request = string.Empty;
            string responseXml = string.Empty;
            bool isUpdateReq = false;
            double timeStamp = 0;
            timeStamp = Convert.ToDouble(ConfigurationSystem.WSTConfig["TimeStamp"]);
            HotelDetails hotelInfo = new HotelDetails();
            HotelStaticData hotelSData = new HotelStaticData();
            HotelImages imageInfo = new HotelImages();
            try
            {
                hotelSData.Load(itemCode, cityCode, HotelBookingSource.WST);
                if (DateTime.Now.Subtract(hotelSData.TimeStamp).Days > timeStamp && hotelSData.HotelName != null)
                {
                    isUpdateReq = true;
                }
            }
            catch { }

            if (string.IsNullOrEmpty(hotelSData.HotelName) || string.IsNullOrEmpty(hotelSData.HotelAddress) || string.IsNullOrEmpty(hotelSData.HotelDescription) || isUpdateReq)
            {
                try
                {
                    #region Request string
                    request = GenerateItemInfoRequest(loginCode,itemCode);
                    try
                    {
                        XmlDocument XmlDoc = new XmlDocument();
                        XmlDoc.LoadXml(request);
                        string filePath = @"" + XmlPath + Guid.NewGuid() + "_getProviderInfoRequest.xml";
                        XmlDoc.Save(filePath);
                    }
                    catch { }
                    #endregion
                    responseXml = wstStaticData.getProviderInfoNew(loginCode, itemCode);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.WSTAvailSearch, Severity.High, 0, "Exception returned from WST.GetItemInformation Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + responseXml, "");
                    throw new BookingEngineException("Error: " + ex.Message);

                }
                try
                {
                    hotelInfo = ReadItemInfo(responseXml, itemCode, isUpdateReq);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.WSTAvailSearch, Severity.High, 0, "Exception returned from WST.GenerateItemInfo Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + responseXml, "");
                    throw new BookingEngineException("Error: " + ex.Message);
                }
            }
            else
            {
                #region Hotel Static Data into Hotel Details Obj
                imageInfo.Load(itemCode, cityCode, HotelBookingSource.WST);
                hotelInfo.HotelCode = hotelSData.HotelCode;
                hotelInfo.HotelName = hotelSData.HotelName;
                hotelInfo.Address = hotelSData.HotelAddress;
                hotelInfo.Description = hotelSData.HotelDescription;
                hotelInfo.FaxNumber = hotelSData.FaxNumber;
                hotelInfo.PhoneNumber = hotelSData.PhoneNumber;
                if (!string.IsNullOrEmpty(hotelSData.HotelFacilities))
                {
                    string[] facilities = hotelSData.HotelFacilities.Split('|');
                    hotelInfo.HotelFacilities = new List<string>(facilities);
                }
                else
                {
                    hotelInfo.HotelFacilities = new List<string>();
                }
                if (!string.IsNullOrEmpty(hotelSData.SpecialAttraction))
                {
                    hotelInfo.Attractions = new Dictionary<string, string>();
                    string[] splAttractions = hotelSData.SpecialAttraction.Split('|');
                    for (int i = 0; i < splAttractions.Length - 1; i++)
                    {
                        string[] attraction = splAttractions[i].Split('#');
                        hotelInfo.Attractions.Add(attraction[0], attraction[1]);
                    }
                }
                else
                {
                    hotelInfo.Attractions = new Dictionary<string, string>();
                }
                if (imageInfo.DownloadedImgs != null)
                {
                    string[] dwldedImages = imageInfo.DownloadedImgs.Split('|');
                    hotelInfo.Images = new List<string>(dwldedImages);
                }
                else
                {
                    hotelInfo.Images = new List<string>();
                }
                List<string> Location = new List<string>();
                Location.Add(hotelSData.HotelLocation);
                hotelInfo.Location = Location;
                hotelInfo.Map = hotelSData.HotelMap;
                #endregion
            }
            return hotelInfo;
        }

        /// <summary>
        /// Genarating ItemInfo Request Xml
        /// </summary>
        /// <param name="loginCode"></param>
        /// <param name="itemCode"></param>
        /// <returns></returns>
        private string GenerateItemInfoRequest(string loginCode, string itemCode)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("getProviderInfoNew");
            xmlString.WriteElementString("agentregcode", loginCode);
            xmlString.WriteElementString("itemCode", itemCode);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        /// <summary>
        /// To read the response XML of static data
        /// </summary>
        /// <param name="responseXml"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private HotelDetails ReadItemInfo(string responseXml, string code, bool updateReq)
        {
            HotelDetails hotelInfo = new HotelDetails();
            HotelStaticData hotelSData = new HotelStaticData();
            HotelImages imageInfo = new HotelImages();
            XmlNode tempNode;
            TextReader stringRead = new StringReader(responseXml);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + XmlPath + Guid.NewGuid() + "_getProviderInfoResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/CATEGORY");
            if (tempNode == null)
            {
                Audit.Add(EventType.WSTAvailSearch, Severity.High, 0, "Error Message: Static data for hotel " + code + " not found. | " + DateTime.Now + "| Response XML" + responseXml, "");
                throw new BookingEngineException("<br>No static data");
            }
            else
            {
                hotelSData.HotelCode = code;
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/partyname");
                if (tempNode != null)
                {
                    hotelSData.HotelName = tempNode.InnerText;
                }
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/address");
                if (tempNode != null)
                {
                    hotelSData.HotelAddress = tempNode.InnerText;
                }
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/tel");
                if (tempNode != null)
                {
                    hotelSData.PhoneNumber = tempNode.InnerText;
                }
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/fax");
                if (tempNode != null)
                {
                    hotelSData.FaxNumber = tempNode.InnerText;
                }
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/citycode");
                if (tempNode != null)
                {
                    hotelSData.CityCode = tempNode.InnerText;
                }
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/Description");
                if (tempNode != null)
                {
                    hotelSData.HotelDescription = tempNode.InnerText;
                }
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/facilities");
                if (tempNode != null)
                {
                    string[] faci = tempNode.InnerText.Split(',');
                    for (int f = 0; f < faci.Length; f++)
                    {
                        hotelSData.HotelFacilities += faci[f] + "|";
                    }
                }
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/childpolicy");
                if (tempNode != null)
                {
                    hotelSData.HotelPolicy = tempNode.InnerText;
                }
                for (int i = 0; i < 7; i++)
                {
                    tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/oimage" + i.ToString());
                    if (tempNode != null)
                    {
                        if (tempNode.InnerText != null && tempNode.InnerText.Length > 0)
                            hotelSData.HotelPicture += tempNode.InnerText + "|";
                    }
                }
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/starint");
                if (tempNode != null)
                {
                    string star = tempNode.InnerText;
                    switch (star)
                    {
                        case "2": hotelSData.Rating = HotelRating.TwoStar;
                            break;
                        case "3": hotelSData.Rating = HotelRating.ThreeStar;
                            break;
                        case "4": hotelSData.Rating = HotelRating.FourStar;
                            break;
                        case "5": hotelSData.Rating = HotelRating.FiveStar;
                            break;
                    }
                }
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/latitude");
                if (tempNode != null)
                {
                    hotelSData.HotelMap = tempNode.InnerText;
                }
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/longitude");
                if (tempNode != null)
                {
                    hotelSData.HotelMap = hotelSData.HotelMap + "|" + tempNode.InnerText;
                }
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/Location");
                if (tempNode != null)
                {
                    hotelSData.HotelLocation = tempNode.InnerText;
                }
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/Email");
                if (tempNode != null)
                {
                    hotelSData.EMail = tempNode.InnerText;
                }
                tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProviderinfo_XML_New/WebSite");
                if (tempNode != null)
                {
                    hotelSData.URL = tempNode.InnerText;
                }
                hotelSData.PinCode = string.Empty;
                hotelSData.Source = HotelBookingSource.WST;
                hotelSData.SpecialAttraction = string.Empty;
                hotelSData.Status = true;

                hotelSData.Save();

                imageInfo.Load(code, hotelSData.CityCode, HotelBookingSource.WST);
                bool imageUpdate = false;
                if (imageInfo.Images != null)
                {
                    imageUpdate = true;
                }
                //for images
                imageInfo.HotelCode = hotelSData.HotelCode;
                imageInfo.Source = HotelBookingSource.WST;
                imageInfo.CityCode = hotelSData.CityCode;
                imageInfo.Images = hotelSData.HotelPicture;
                imageInfo.DownloadedImgs = hotelSData.HotelPicture;

                if (imageUpdate)
                {
                    imageInfo.Update();
                }
                else
                {
                    imageInfo.Save();
                }

                #region putting Static Data into Hotel Detail obj
                hotelInfo.HotelCode = hotelSData.HotelCode;
                hotelInfo.HotelName = hotelSData.HotelName;
                hotelInfo.Address = hotelSData.HotelAddress;
                hotelInfo.Description = hotelSData.HotelDescription;
                hotelInfo.FaxNumber = hotelSData.FaxNumber;
                hotelInfo.PhoneNumber = hotelSData.PhoneNumber;
                if (!string.IsNullOrEmpty(hotelSData.HotelFacilities))
                {
                    string[] facilities = hotelSData.HotelFacilities.Split('|');
                    hotelInfo.HotelFacilities = new List<string>(facilities);
                }
                else
                {
                    hotelInfo.HotelFacilities = new List<string>();
                }
                if (!string.IsNullOrEmpty(hotelSData.SpecialAttraction))
                {
                    hotelInfo.Attractions = new Dictionary<string, string>();
                    string[] splAttractions = hotelSData.SpecialAttraction.Split('|');
                    for (int i = 0; i < splAttractions.Length - 1; i++)
                    {
                        string[] attraction = splAttractions[i].Split('#');
                        hotelInfo.Attractions.Add(attraction[0], attraction[1]);
                    }
                }
                else
                {
                    hotelInfo.Attractions = new Dictionary<string, string>();
                }
                if (imageInfo.DownloadedImgs != null)
                {
                    string[] dwldedImages = imageInfo.DownloadedImgs.Split('|');
                    hotelInfo.Images = new List<string>(dwldedImages);
                }
                else
                {
                    hotelInfo.Images = new List<string>();
                }
                List<string> Location = new List<string>();
                Location.Add(hotelSData.HotelLocation);
                hotelInfo.Location = Location;
                hotelInfo.Map = hotelSData.HotelMap;
                #endregion
            }
            return hotelInfo;
        }

        #endregion

         #region CancellationPolicy
        /// <summary>
        /// This Method is used for getting Hotel Cancel Charge Info.
        /// </summary>
        /// <param name="req"></param>
        /// <param name="hotelCode"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetHotelChargeCondition(HotelItinerary itinerary, ref List<HotelPenality> penalityList, bool savePenality)
        {
            string request = string.Empty;
            string resp = string.Empty;
            string responseXml = string.Empty;
            Dictionary<string, string> polList = new Dictionary<string, string>();
            Dictionary<string, string> roomTypeCode = new Dictionary<string, string>();
            string CancellationPolicy = string.Empty;
            string ChildPilicy = string.Empty;
            
            // ROOMTYPECODE------- rtCode|XMLID|CartID|Sequence|HotelName
            string[] rtCode = itinerary.Roomtype[0].RoomTypeCode.Split('|');
            // RATEPLANCODE-------- mealcode|rmcatCode|rateType|promotion
            string[] rpCode = itinerary.Roomtype[0].RatePlanCode.Split('|');
            string lastCancelllation = string.Empty;
            #region Request string
            request = GenerateChargeConditionRequest(loginCode, rtCode[1], Convert.ToInt16(rtCode[2]), itinerary.HotelCode, rtCode[0], rpCode[1], rpCode[0], rpCode[2], 0);
            try
            {
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(request);
                string filePath = @"" + XmlPath + Guid.NewGuid() + "_CancelgetAvailabiltyDetRequest.xml";
                XmlDoc.Save(filePath);
            }
            catch { }
            #endregion
            try
            {
                try
                {
                    resp = availService.getAvailabiltyDet(loginCode, rtCode[1], Convert.ToInt16(rtCode[2]), itinerary.HotelCode, rtCode[0], rpCode[1], rpCode[0], rpCode[2], 0);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.WSTAvailSearch, Severity.High, 0, "Error Message: getAvailabiltyDet exception"+ex.ToString(), "");
                }
                TextReader stringReader = new StringReader(resp);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(stringReader);
                try
                {
                    string filePath = @"" + XmlPath + Guid.NewGuid() + "_CancelgetAvailabiltyDetResponse.xml";
                    xmlDoc.Save(filePath);
                }
                catch { }

                XmlNode detNode;
                XmlNode ErrorInfo = xmlDoc.SelectSingleNode("NewDataSet/getAvailabiltyDet");
                if (ErrorInfo == null)
                {
                    Audit.Add(EventType.WSTAvailSearch, Severity.High, 0, "Error Message: CancelAvailabiltyDet not coming from WST | " + DateTime.Now + "| Response XML" + responseXml, "");
                    throw new BookingEngineException("<br>No CancelAvailabiltyDet Found");
                }
                else
                {
                    detNode = xmlDoc.SelectSingleNode("NewDataSet/getAvailabiltyDet/cancellationpolicy");
                    if (detNode != null)
                    {
                        CancellationPolicy = detNode.InnerText;
                    }
                    //brahmam
                    //Last CancelationDate
                    detNode = xmlDoc.SelectSingleNode("NewDataSet/getAvailabiltyDet/timelimit");
                    if (detNode != null)
                    {
                        int BufferDays = Convert.ToInt32(ConfigurationSystem.WSTConfig["Buffer"]);
                        lastCancelllation = Convert.ToDateTime(detNode.InnerText).AddDays(-(BufferDays)).ToString();
                    }
                    //ChildPolicy
                    detNode = xmlDoc.SelectSingleNode("NewDataSet/getAvailabiltyDet/childpolicy");
                    if (detNode != null)
                    {
                        ChildPilicy = detNode.InnerText;
                    }
                    CancellationPolicy = CancellationPolicy.Trim();
                    string[] cancelPolicy = CancellationPolicy.Split('.');
                    if (cancelPolicy.Length > 0)
                    {
                        string canPolicies = string.Empty;
                        canPolicies = "Cancelling before " + Convert.ToDateTime(lastCancelllation).ToString("dd/MMM/yyyy") + " No Charges Applicable";
                        for (int k = 0; k < cancelPolicy.Length - 1; k++)
                        {
                            canPolicies += "|" + cancelPolicy[k];
                            ////if (k > 0)
                            ////{

                            ////}
                            ////else
                            ////{
                            ////    canPolicies += cancelPolicy[k];
                            ////}
                        }
                        polList.Add("CancelPolicy", canPolicies);
                        //break;
                    }
                    string[] childPolicyarr = ChildPilicy.Split(new string[] { "\n", "\n\n" }, StringSplitOptions.None);
                    if (childPolicyarr.Length > 0)
                    {
                        string hotelNorms = string.Empty;
                        for (int k = 0; k < childPolicyarr.Length; k++)
                        {
                            if (k > 0)
                            {
                                hotelNorms += "|" + childPolicyarr[k];
                            }
                            else
                            {
                                hotelNorms += childPolicyarr[k];
                            }
                        }
                        polList.Add("HotelPolicy", hotelNorms);
                    }
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.WSTBooking, Severity.High, 0, "Exception returned from WST.cancellation Policy Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML(avail)" + resp + "|response XML(static)" + responseXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            polList.Add("lastCancellationDate", lastCancelllation);
            return polList;
        }

        private string GenerateChargeConditionRequest(string loginCode, string xmlId, int cartId, string hotelCode, string rmTypeCode, string rmcatcode, string mealCode, string ratetype, int flag)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("getAvailabiltyDet");

            xmlString.WriteElementString("agentregcode", loginCode);
            xmlString.WriteElementString("xmlId", xmlId);
            xmlString.WriteElementString("cartId", cartId.ToString());
            xmlString.WriteElementString("hotelCode", hotelCode);
            xmlString.WriteElementString("rmtypcode", rmTypeCode);
            xmlString.WriteElementString("rmcatcode", rmcatcode);
            xmlString.WriteElementString("mealCode", mealCode);
            xmlString.WriteElementString("ratetype", ratetype);
            xmlString.WriteElementString("flag", flag.ToString());
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();

            xmlString.Close();
            return strWriter.ToString();
        }

        #endregion

         #region CancelBooking
        /// <summary>
        /// this method is to cancel a booked itinerary
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public Dictionary<string, string> CancelBooking(string confirmationNo, string remarks)
        {
            string request = string.Empty;
            string response = string.Empty;
            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            request = GenarateCancelRequest(confirmationNo, remarks);
            TextReader reader = new StringReader(request);
            XmlDocument docXml = new XmlDocument();
            docXml.Load(reader);
            try
            {
                string filePath = @"" + XmlPath + Guid.NewGuid() + "_cancel_xmlbookingRequest.xml";
                docXml.Save(filePath);
            }
            catch { }
            try
            {
                response = bookingService.cancel_xmlbooking(loginCode, string.Empty, confirmationNo, remarks);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.WSTBooking, Severity.High, 0, "CancelBooking Error Message:" + ex.Message, "");
            }
            TextReader stringRead = new StringReader(response);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + XmlPath + Guid.NewGuid() + "_cancel_xmlbookingResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            string status = string.Empty;
            XmlNode tempNode;
            tempNode = xmlDoc.SelectSingleNode("request/cancelsuccess");
            if (tempNode != null)
            {
                status = tempNode.InnerText;
            }
            if (!string.IsNullOrEmpty(status) && Convert.ToInt32(status) == 1)
            {
                cancelInfo.Add("Status", "Cancelled");
                cancelInfo.Add("ID", confirmationNo);
                cancelInfo.Add("Currency", currency);
                //In inernational booking cancellation Cancellation Amount is not used.
                cancelInfo.Add("Amount", "0");
            }
            else
            {
                cancelInfo.Add("Status", "Failed");
            }
            return cancelInfo;
        }

        private string GenarateCancelRequest(string confirmationNo, string remarks)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("cancel_xmlbooking");
            xmlString.WriteElementString("confirmationNo", confirmationNo);
            xmlString.WriteElementString("Message", remarks);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }

        #endregion

         #region Promotions
        public string GetPromotion(string ratePlanCode, string roomTypeCode,string hotelCode)
        {
            string promotion = string.Empty;
            string request = string.Empty;
            try
            {
                // ROOMTYPECODE------- rtCode|XMLID|CartID|Sequence|HotelName
                string[] rtCode = roomTypeCode.Split('|');
                // RATEPLANCODE-------- mealcode|rmcatCode|rateType|promotion
                string[] rpCode = ratePlanCode.Split('|');
                #region Request string
                request = GeneratePromotionRequest(loginCode, rtCode[1], Convert.ToInt16(rtCode[2]), hotelCode, rtCode[0], rpCode[0], rpCode[1], rpCode[2]);
                try
                {
                    XmlDocument XmlDoc = new XmlDocument();
                    XmlDoc.LoadXml(request);
                    string filePath = @"" + XmlPath + Guid.NewGuid() + "_getWebProRemRequest.xml";
                    XmlDoc.Save(filePath);
                }
                catch { }
                #endregion
                string responsePromo = availService.getWebProRem(loginCode, rtCode[1], Convert.ToInt16(rtCode[2]), hotelCode, rtCode[0], rpCode[0], rpCode[1], rpCode[2]);
                promotion= ReadPromo(responsePromo);
            }
            catch (Exception ex) 
            {
                Audit.Add(EventType.WSTAvailSearch, Severity.Low, 0, "Error:GetPromotion method:" + ex.Message, "");
            }
            return promotion;
        }
        
        private string GeneratePromotionRequest(string loginCode, string xmlId, int cartId, string hotelCode, string rmTypeCode, string rmcatcode, string mealCode, string ratetype)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("getWebProRem");

            xmlString.WriteElementString("agentregcode", loginCode);
            xmlString.WriteElementString("xmlId", xmlId);
            xmlString.WriteElementString("cartId", cartId.ToString());
            xmlString.WriteElementString("hotelCode", hotelCode);
            xmlString.WriteElementString("rmtypcode", rmTypeCode);
            xmlString.WriteElementString("rmcatcode", rmcatcode);
            xmlString.WriteElementString("mealCode", mealCode);
            xmlString.WriteElementString("ratetype", ratetype);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();

            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// reads the promotion XML
        /// </summary>
        /// <param name="resp"></param>
        /// <returns></returns>
        private string ReadPromo(string resp)
        {
            string promotion = string.Empty;
            TextReader stringRead = new StringReader(resp);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            try
            {
                string filePath = @"" + XmlPath + Guid.NewGuid() + "_getWebProRemResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }

            XmlNode tempNode;
            tempNode = xmlDoc.SelectSingleNode("NewDataSet/getWebProRem/REMARKS");
            if (tempNode != null)
            {
                promotion = tempNode.InnerText;
            }
            return promotion;
        }
        #endregion

         #region GettingExtraBed 
       
        public void GetExtraBed(ref HotelSearchResult result, HotelRequest req)
        {
            string request = string.Empty;
            try
            {
                request = GenerateProviderDetailRequest(loginCode, result.HotelCode);
                TextReader stringRead = new StringReader(request);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(stringRead);
                try
                {
                    string filePath = @"" + XmlPath + Guid.NewGuid() + "_getProviderDetailRequest.xml";
                    xmlDoc.Save(filePath);
                }
                catch { }
                string response = wstStaticData.getProviderDetail(loginCode, result.HotelCode);
                ReadProviderDetails(response, ref result, req);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.WSTAvailSearch, Severity.Low, 0, "Error:GetExtraBed method:" + ex.Message, "");
            }
        }

        /// <summary>
        /// RoomWise Loading ExtraBed
        /// </summary>
        /// <param name="response"></param>
        /// <param name="result"></param>
        /// <param name="req"></param>
        private void ReadProviderDetails(string response, ref HotelSearchResult result, HotelRequest req)
        {
            try
            {
                TextReader stringRead = new StringReader(response);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(stringRead);
                try
                {
                    string filePath = @"" + XmlPath + Guid.NewGuid() + "_getProviderDetailResponse.xml";
                    xmlDoc.Save(filePath);
                }
                catch { }
                XmlNode tempNode;
                for (int i = 0; i < req.NoOfRooms; i++)
                {
                    XmlNodeList tempNodeList = xmlDoc.SelectNodes("NewDataSet/Sp_GetProvider_Details4");
                    for (int k = 0; k < result.RoomDetails.Length; k++)
                    {
                        if (result.RoomDetails[k].SequenceNo == (i + 1).ToString())
                        {
                            foreach (XmlNode temp in tempNodeList)
                            {
                                // ROOMTYPECODE------- rtCode|XMLID|CartID|Sequence|HotelName
                                string RmtypCode = result.RoomDetails[k].RoomTypeCode.Split('|')[0];
                                // RATEPLANCODE-------- rpCode|rmcatCode|rateType|promotion
                                string RmcatCode = result.RoomDetails[k].RatePlanCode.Split('|')[1];
                                tempNode = temp.SelectSingleNode("RmtypCode");
                                if (tempNode != null)
                                {
                                    if (RmtypCode == tempNode.InnerText)
                                    {
                                        tempNode = temp.SelectSingleNode("RmcatCode");
                                        if (tempNode != null)
                                        {
                                            if (RmcatCode == tempNode.InnerText)
                                            {
                                                int adultsExtraBeds = 0, childExtraBeds = 0;
                                                tempNode = temp.SelectSingleNode("MaxAdult");
                                                if (tempNode != null)
                                                {
                                                    adultsExtraBeds = req.RoomGuest[i].noOfAdults - Convert.ToInt32(tempNode.InnerText);
                                                    if (adultsExtraBeds > 0)
                                                    {
                                                        tempNode = temp.SelectSingleNode("MaxAdultWithExtraBed");
                                                        if (tempNode != null)
                                                        {
                                                            if (req.RoomGuest[i].noOfAdults == Convert.ToInt32(tempNode.InnerText))
                                                            {
                                                                result.RoomDetails[k].IsExtraBed = true;
                                                            }
                                                        }
                                                    }

                                                }
                                                tempNode = temp.SelectSingleNode("MaxChild");
                                                if (tempNode != null)
                                                {
                                                    childExtraBeds = req.RoomGuest[i].noOfChild - Convert.ToInt32(tempNode.InnerText);
                                                    if (childExtraBeds > 0)
                                                    {
                                                        tempNode = temp.SelectSingleNode("MaxChildWithExtraBed");
                                                        if (tempNode != null)
                                                        {
                                                            if (req.RoomGuest[i].noOfChild == Convert.ToInt32(tempNode.InnerText))
                                                            {
                                                                result.RoomDetails[k].IsExtraBed = true;
                                                            }
                                                        }

                                                    }
                                                }
                                                if (adultsExtraBeds < 0) adultsExtraBeds = 0;
                                                if (childExtraBeds < 0) childExtraBeds = 0;
                                                tempNode = temp.SelectSingleNode("MaxExtraBed");
                                                if (tempNode != null && result.RoomDetails[k].IsExtraBed)
                                                {
                                                    if (Convert.ToInt32(tempNode.InnerText) != (adultsExtraBeds + childExtraBeds))
                                                    {
                                                        result.RoomDetails[k].IsExtraBed = false;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                continue;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.WSTAvailSearch, Severity.Low, 0, "Error:ReadExtraBed method:" + ex.Message, "");
            }
        }
        private string GenerateProviderDetailRequest(string loginCode, string hotelCode)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            xmlString.WriteStartElement("RequestDetails");
            xmlString.WriteStartElement("getProviderDetail");
            xmlString.WriteElementString("agentregcode", loginCode);
            xmlString.WriteElementString("itemCode", hotelCode);
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }


        #endregion
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~WST()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
        #region oldMethods not for use right now
        //public bool GetExtraBed(string partycode, string ratePlanCode, string roomTypeCode,int adultCount,int childCount)
        //{
        //    bool hasExtraBed = false;
        //    string request = string.Empty;
        //    try
        //    {
        //        request = GenerateProviderDetailRequest(loginCode, partycode);
        //        TextReader stringRead = new StringReader(request);
        //        XmlDocument xmlDoc = new XmlDocument();
        //        xmlDoc.Load(stringRead);
        //        try
        //        {
        //            string filePath = @"" + ConfigurationSystem.WSTConfig["XmlLogPath"] + Guid.NewGuid() + "_getProviderDetailRequest.xml";
        //            xmlDoc.Save(filePath);
        //        }
        //        catch { }

        //        string response = staticData.getProviderDetail(loginCode, partycode);
        //        hasExtraBed = ReadProviderDetails(response, ratePlanCode, roomTypeCode, adultCount, childCount);
        //    }
        //    catch (Exception ex)
        //    {
        //        
        //    }
        //    return hasExtraBed;
        //}

        //private decimal CalculateCancellationAmount(HotelItinerary itinerary)
        //{
        //    decimal amt = 0;
        //    string[] cancelDaysArr1 =  itinerary.HotelCancelPolicy.Split(new string[] { "done" }, StringSplitOptions.None);
        //    string[] cancelDaysArr2 = cancelDaysArr1[1].Split(' ');
        //    int NoofDays = Convert.ToInt32(cancelDaysArr2[1]);
        //    int BufferDays = Convert.ToInt32(ConfigurationSystem.WSTConfig["Buffer"]);
        //    DateTime lastCancellationDate = itinerary.StartDate.AddDays(-(NoofDays + BufferDays));

        //    string[] cancelNightsArr1 = itinerary.HotelCancelPolicy.Split(new string[] { "else" }, StringSplitOptions.None);
        //    string[] cancelNightsArr2 = cancelNightsArr1[1].Split(' ');
        //    int NoofNights = Convert.ToInt32(cancelNightsArr2[1]);

        //    if (lastCancellationDate > DateTime.MinValue)
        //    {
        //        if (DateTime.Now > lastCancellationDate)
        //        {
        //            System.TimeSpan diffResult = itinerary.EndDate.Subtract(itinerary.StartDate);
        //            foreach (HotelRoom room in itinerary.Roomtype)
        //            {
        //                decimal tmpAmount = room.Price.NetFare / diffResult.Days;
        //                amt += tmpAmount * NoofNights;
        //            }
        //        }
        //        else
        //        {
        //            amt = 0;
        //        }
        //    }

        //    return amt;
        //}
        /// <summary>
        /// This method is to import booking from WST
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        //public bool GetBookedHotel(ref HotelItinerary itinerary)
        //{
        //    Trace.TraceInformation("WST.GetBookedHotel entered");
        //    bool itemFound = false;

        //    string request = string.Empty;
        //    string resp = string.Empty;
        //    try
        //    {
        //        resp = bookingService.getReservation(loginCode, itinerary.ConfirmationNo, 1);
        //    }
        //    catch (Exception ex)
        //    {
        //        Audit.Add(EventType.WSTImport, Severity.High, 0, "Exception returned from WST.GetBookedHotel Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
        //    }
        //    try
        //    {
        //        itemFound = ReadGetBookedHotelResponse(resp, ref itinerary);
        //    }
        //    catch (Exception ex)
        //    {
        //        Audit.Add(EventType.WSTImport, Severity.High, 0, "Exception returned from WST.GetBookedHotel Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + resp, "");
        //        Trace.TraceError("Error: " + ex.Message);
        //        throw new BookingEngineException("Error: " + ex.Message);
        //    }
        //    Trace.TraceInformation("WST.GetBookedHotel exited");
        //    return itemFound;
        //}

        /// <summary>
        /// This method brings the country code for a city from the WSTcityCode table
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        //private string GetCountryCode(string cityCode)
        //{
        //    SqlParameter[] paramlist = new SqlParameter[1];
        //    paramlist[0] = new SqlParameter("@cityCode", cityCode);
        //    //SqlConnection connection = Dal.GetConnection();
        //    SqlConnection connection = DBGateway.GetConnection();
        //    string countryCode = string.Empty;
        //    try
        //    {
        //        SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetCountryCodeWST, paramlist, connection);
        //        if (dataReader.Read())
        //        {
        //            countryCode = dataReader["countryCode"].ToString();
        //        }
        //        dataReader.Close();
        //        connection.Close();
        //    }
        //    catch (SqlException ex)
        //    {
        //        connection.Close();
        //        Audit.Add(EventType.WSTAvailSearch, Severity.High, 0, "Exception returned from WST. country code not found for " + cityCode + ". exception: " + ex.Message + DateTime.Now, "");
        //        Trace.TraceError("Error:country code not found for :" + cityCode + ". exception: " + ex.Message);
        //    }
        //    return countryCode;
        //}

        ///// <summary>
        ///// this methos is to get the type of room required. SINGLE OR DOUBLE
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //private List<string> GetReqRoomType(HotelRequest request)
        //{
        //    Trace.TraceInformation("WSTApi.GetReqRoomType entered");
        //    List<string> roomTypes = new List<string>(); roomTypes = new List<string>();
        //    int adults = 0, childs, infant;
        //    for (int i = 0; i < request.NoOfRooms; i++)
        //    {
        //        //Since the child policy of White Sands is not known at this time, a request containing child from user is not processed
        //        adults = request.RoomGuest[i].noOfAdults;
        //        childs = request.RoomGuest[i].noOfChild;
        //        infant = 0;
        //        //for (int c = 0; c < request.RoomGuest[i].noOfChild; c++)
        //        //{
        //        //    if (request.RoomGuest[i].childAge[c] >= 12)
        //        //    {
        //        //        adults = adults + 1;
        //        //    }
        //        //    else if (request.RoomGuest[i].childAge[c] >= 2 && request.RoomGuest[i].childAge[c] < 12)
        //        //    {
        //        //        childs = childs + 1;
        //        //    }
        //        //    else
        //        //    {
        //        //        infant = infant + 1;
        //        //    }
        //        //}

        //        //Three types of rooms available as of now :SGL,DBL,TRI (Single,Double,Triple)
        //        if (adults == 1)
        //        {
        //            // Here Room Type Contains 1.room type code i.e SGL or DBL
        //            if (childs == 0) 
        //            {
        //                roomTypes.Add("SGL");
        //            }
        //            else if (childs == 1 || childs == 2)
        //            {
        //                roomTypes.Add("DBL");
        //            }
        //        }
        //        if (adults == 2)
        //        {
        //            if (childs == 0 || childs == 1)
        //            {
        //                roomTypes.Add("DBL");
        //            }
        //            else if (childs == 2)
        //            {
        //                roomTypes.Add("TRI");
        //            }
        //        }
        //        if (adults == 3)
        //        {
        //            if (childs == 0 || childs == 1) 
        //            {
        //                roomTypes.Add("TRI");
        //            }
        //        }
        //    }
        //    return roomTypes;
        //}



        ///// <summary>
        ///// To create XML request
        ///// </summary>
        ///// <param name="itinerary"></param>
        ///// <param name="agentXO"></param>
        ///// <returns></returns>
        //private string GenerateBookingRequest(HotelItinerary itinerary, string agentXO)
        //{
        //    Trace.TraceInformation("WST.GenerateBookingRequest entered");
        //    string resp = string.Empty;
        //    string promotionId = string.Empty;
        //    string timeLimit = string.Empty;
        //    Dictionary<string, Dictionary<string, int>> rooms = new Dictionary<string, Dictionary<string, int>>();
        //    rooms = GetBookingDataDict(itinerary);
        //    Dictionary<string, List<string>> rateDet;
        //    StringBuilder strWriter = new StringBuilder();
        //    XmlWriter xmlString = XmlTextWriter.Create(strWriter);
        //    int c = 1;
        //    foreach(KeyValuePair<string ,Dictionary<string,int>> room in rooms)
        //    {
        //        xmlString.WriteStartElement("htlrequestdet");
        //        // ROOMTYPECODE------- rtCode|XMLID|CartID|Sequence|HotelName
        //        string[] rtCode = itinerary.Roomtype[room.Value["firstIndex"]].RoomTypeCode.Split('|');
        //        // RATEPLANCODE-------- rpCode|rmcatCode|rateType|promotion
        //        string[] rpCode = itinerary.Roomtype[room.Value["firstIndex"]].RatePlanCode.Split('|');

        //        resp = availService.getAvailabiltyDet(loginCode, rtCode[1], Convert.ToInt16(rtCode[2]), itinerary.HotelCode, rtCode[0], rpCode[1], rpCode[0], rpCode[2], 1);
        //        rateDet = new Dictionary<string, List<string>>();
        //        rateDet = GetRateDetail(resp, itinerary);

        //        resp = availService.getAvailabiltyDet(loginCode, rtCode[1], Convert.ToInt16(rtCode[2]), itinerary.HotelCode, rtCode[0], rpCode[1], rpCode[0], rpCode[2], 0);
        //        promotionId = ReadPromotionId(resp, ref timeLimit);

        //        xmlString.WriteElementString("requestid", rtCode[1]);
        //        xmlString.WriteElementString("line_no", c.ToString());
        //        xmlString.WriteElementString("partycode", itinerary.HotelCode);
        //        xmlString.WriteElementString("rmtypcode", rtCode[0]);
        //        xmlString.WriteElementString("mealcode", rpCode[0]);
        //        xmlString.WriteElementString("noofrooms", room.Value["noofrooms"].ToString());
        //        xmlString.WriteElementString("noofadults", room.Value["noofadults"].ToString());
        //        xmlString.WriteElementString("noofchild", room.Value["noofchilds"].ToString());

        //        int p = 0;
        //        for (p = 1; p <= itinerary.Roomtype[room.Value["firstIndex"]].PassenegerInfo.Count; p++)
        //        {
        //            xmlString.WriteElementString("title" + p.ToString(), itinerary.Roomtype[room.Value["firstIndex"]].PassenegerInfo[p - 1].Title);
        //        }
        //        for (int z = p; z <= 4; z++)
        //        {
        //            xmlString.WriteElementString("title" + z.ToString(), "");
        //        }
        //        for (p = 1; p <= itinerary.Roomtype[room.Value["firstIndex"]].PassenegerInfo.Count; p++)
        //        {
        //            xmlString.WriteElementString("familyname" + p.ToString(), itinerary.Roomtype[room.Value["firstIndex"]].PassenegerInfo[p - 1].Lastname);
        //        }
        //        for (int z = p; z <= 4; z++)
        //        {
        //            xmlString.WriteElementString("familyname" + z.ToString(), "");
        //        }
        //        for (p = 1; p <= itinerary.Roomtype[room.Value["firstIndex"]].PassenegerInfo.Count; p++)
        //        {
        //            xmlString.WriteElementString("firstname" + p.ToString(), itinerary.Roomtype[room.Value["firstIndex"]].PassenegerInfo[p - 1].Firstname);
        //        }
        //        for (int z = p; z <= 4; z++)
        //        {
        //            xmlString.WriteElementString("firstname" + z.ToString(), "");
        //        }
        //        xmlString.WriteElementString("age1", "0");
        //        xmlString.WriteElementString("age2", "0");
        //        xmlString.WriteElementString("age3", "0");
        //        xmlString.WriteElementString("age4", "0");
        //        xmlString.WriteElementString("aflightno", "TBA");
        //        xmlString.WriteElementString("aflightdate", itinerary.StartDate.ToString("yyyy-MM-ddTHH:mm:ss"));
        //        xmlString.WriteElementString("aflighttime", "15:00");
        //        xmlString.WriteElementString("dflightno", "TBA");
        //        xmlString.WriteElementString("dflightdate", itinerary.EndDate.ToString("yyyy-MM-ddTHH:mm:ss"));
        //        xmlString.WriteElementString("dflighttime", "15:00");
        //        xmlString.WriteElementString("chkindate", itinerary.StartDate.ToString("yyyy-MM-ddTHH:mm:ss"));
        //        xmlString.WriteElementString("chkoutdate", itinerary.EndDate.ToString("yyyy-MM-ddTHH:mm:ss"));
        //        if (itinerary.SpecialRequest != null)
        //        {
        //            xmlString.WriteElementString("remarks", itinerary.SpecialRequest);
        //        }
        //        else
        //        {
        //            xmlString.WriteElementString("remarks", "");
        //        }
        //        xmlString.WriteElementString("agentxoref", agentXO);
        //        xmlString.WriteElementString("nights", Convert.ToString(itinerary.EndDate.Subtract(itinerary.StartDate).Days));
        //        xmlString.WriteElementString("requestdate", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
        //        xmlString.WriteElementString("availableyesno", "1");
        //        double bufferTime = 0;
        //        if (ConfigurationSystem.WSTConfig.ContainsKey("timeLimitBuffer"))
        //        {
        //            bufferTime = Convert.ToDouble(ConfigurationSystem.WSTConfig["timeLimitBuffer"]);
        //        }
        //        xmlString.WriteElementString("time_limit", timeLimit);
        //        xmlString.WriteElementString("subuser", "");
        //        xmlString.WriteElementString("block", "0");
        //        xmlString.WriteElementString("minnights", Convert.ToString(itinerary.EndDate.Subtract(itinerary.StartDate).Days));
        //        xmlString.WriteElementString("message", "");

        //        xmlString.WriteStartElement("htlrequestsubdet");
        //        xmlString.WriteElementString("requestid", rtCode[1]);
        //        xmlString.WriteElementString("line_no", c.ToString());
        //        xmlString.WriteElementString("rmcatcode", rpCode[1]);
        //        xmlString.WriteElementString("units", room.Value["noofrooms"].ToString());
        //        xmlString.WriteElementString("linevalue", rateDet["lv"][0]);
        //        xmlString.WriteElementString("plistcode", rateDet["lc"][0]);
        //        xmlString.WriteElementString("plistlineno", rateDet["ll"][0]);
        //        xmlString.WriteElementString("ratetype", rtCode[2]);
        //        xmlString.WriteElementString("pkgnights", Convert.ToString(itinerary.EndDate.Subtract(itinerary.StartDate).Days));
        //        xmlString.WriteElementString("freenights", rateDet["fn"][0]);
        //        xmlString.WriteElementString("promotionid", promotionId);
        //        xmlString.WriteElementString("agentsellprice", rateDet["lv"][0]);
        //        xmlString.WriteEndElement();

        //        xmlString.WriteEndElement();
        //        c++;
        //    }
        //    xmlString.Close();
        //    strWriter = strWriter.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "");
        //    Trace.TraceInformation("WST.GenerateBookingRequest exiting");
        //    return strWriter.ToString();
        //}

        ///// <summary>
        ///// This method is to get the total no of SGL and DBL no of rooms to be booked, No of adults, Children
        ///// </summary>
        ///// <param name="itinerary"></param>
        ///// <returns></returns>
        //private Dictionary<string, Dictionary<string, int>> GetBookingDataDict(HotelItinerary itinerary)
        //{
        //    Dictionary<string, Dictionary<string, int>> rooms = new Dictionary<string, Dictionary<string, int>>();
        //    for (int i = 0; i < itinerary.Roomtype.Length; i++)
        //    {
        //        Dictionary<string, int> roomContains = new Dictionary<string, int>();
        //        // RATEPLANCODE-------- rpCode|rmcatCode|rateType|promotion
        //        string[] rpCode = itinerary.Roomtype[i].RatePlanCode.Split('|');
        //        if (rpCode[1] == "SGL" && !rooms.ContainsKey("SGL"))
        //        {
        //            roomContains.Add("noofrooms", 1);
        //            roomContains.Add("noofadults", itinerary.Roomtype[i].AdultCount);
        //            roomContains.Add("noofchilds", itinerary.Roomtype[i].ChildCount);
        //            roomContains.Add("firstIndex", i);
        //            rooms.Add("SGL", roomContains);
        //        }
        //        else if (rpCode[1] == "SGL" && rooms.ContainsKey("SGL"))
        //        {
        //            rooms["SGL"]["noofrooms"]++;
        //            rooms["SGL"]["noofadults"] += itinerary.Roomtype[i].AdultCount;
        //            rooms["SGL"]["noofchilds"] += itinerary.Roomtype[i].ChildCount;
        //        }
        //        else if (rpCode[1] == "DBL" && !rooms.ContainsKey("DBL"))
        //        {
        //            roomContains.Add("noofrooms", 1);
        //            roomContains.Add("noofadults", itinerary.Roomtype[i].AdultCount);
        //            roomContains.Add("noofchilds", itinerary.Roomtype[i].ChildCount);
        //            roomContains.Add("firstIndex", i);
        //            rooms.Add("DBL", roomContains);
        //        }
        //        else if (rpCode[1] == "DBL" && rooms.ContainsKey("DBL"))
        //        {
        //            rooms["DBL"]["noofrooms"]++;
        //            rooms["DBL"]["noofadults"] += itinerary.Roomtype[i].AdultCount;
        //            rooms["DBL"]["noofchilds"] += itinerary.Roomtype[i].ChildCount;
        //        }
        //    }
        //    return rooms;
        //}


        /// <summary>
        /// Checking for Extra beds
        /// </summary>
        /// <param name="response"></param>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        //private bool ReadProviderDetails(string response, string ratePlanCode, string roomTypeCode, int adultCount, int childCount)
        //{
        //    // ROOMTYPECODE------- rtCode|XMLID|CartID|Sequence|HotelName
        //    string RmtypCode = roomTypeCode.Split('|')[0];
        //    // RATEPLANCODE-------- rpCode|rmcatCode|rateType|promotion
        //    string RmcatCode = ratePlanCode.Split('|')[1];

        //    bool hasExtraBed = false;

        //    Trace.TraceInformation("Entered into method WST.ReadProviderDetails");
        //    TextReader stringRead = new StringReader(response);
        //    XmlDocument xmlDoc = new XmlDocument();
        //    xmlDoc.Load(stringRead);
        //    try
        //    {
        //        string filePath = @"" + ConfigurationSystem.WSTConfig["XmlLogPath"] + Guid.NewGuid() + "_getProviderDetailResponse.xml";
        //        xmlDoc.Save(filePath);
        //    }
        //    catch { }
        //    XmlNode tempNode;
        //    XmlNodeList tempNodeList = xmlDoc.SelectNodes("NewDataSet/Sp_GetProvider_Details4");
        //    {
        //        foreach (XmlNode temp in tempNodeList)
        //        {
        //            tempNode = temp.SelectSingleNode("RmtypCode");
        //            if (tempNode != null)
        //            {
        //                if (RmtypCode == tempNode.InnerText)
        //                {
        //                    tempNode = temp.SelectSingleNode("RmcatCode");
        //                    if (tempNode != null)
        //                    {
        //                        if (RmcatCode == tempNode.InnerText)
        //                        {
        //                            int adultsExtraBeds = 0, childExtraBeds = 0;
        //                            tempNode = temp.SelectSingleNode("MaxAdult");
        //                            if (tempNode != null)
        //                            {
        //                                adultsExtraBeds = adultCount - Convert.ToInt32(tempNode.InnerText);
        //                                if (adultsExtraBeds > 0)
        //                                {
        //                                    tempNode = temp.SelectSingleNode("MaxAdultWithExtraBed");
        //                                    if (tempNode != null)
        //                                    {
        //                                        if (adultCount == Convert.ToInt32(tempNode.InnerText))
        //                                        {
        //                                            hasExtraBed = true;
        //                                        }
        //                                    }
        //                                }

        //                            }
        //                            tempNode = temp.SelectSingleNode("MaxChild");
        //                            if (tempNode != null)
        //                            {
        //                                childExtraBeds = childCount - Convert.ToInt32(tempNode.InnerText);
        //                                if (childExtraBeds > 0)
        //                                {
        //                                    tempNode = temp.SelectSingleNode("MaxChildWithExtraBed");
        //                                    if (tempNode != null)
        //                                    {
        //                                        if (childCount == Convert.ToInt32(tempNode.InnerText))
        //                                        {
        //                                            hasExtraBed = true;
        //                                        }
        //                                    }

        //                                }
        //                            }
        //                            if (adultsExtraBeds < 0) adultsExtraBeds = 0;
        //                            if (childExtraBeds < 0) childExtraBeds = 0;
        //                            tempNode = temp.SelectSingleNode("MaxExtraBed");
        //                            if (tempNode != null && hasExtraBed)
        //                            {
        //                                if (Convert.ToInt32(tempNode.InnerText) != (adultsExtraBeds +  childExtraBeds))
        //                                {
        //                                    hasExtraBed = false;
        //                                }
        //                            }
        //                        }
        //                        else
        //                        {
        //                            continue;
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    continue;
        //                }
        //            }
        //        }
        //    }
        //    tempNode = xmlDoc.SelectSingleNode("NewDataSet/Sp_GetProvider_Details4");
        //    if (tempNode != null)
        //    {

        //    }

        //    return hasExtraBed;
        //}

        #region Static data & Cancel Policy



        ///// <summary>
        ///// To read the cancellation detail XML
        ///// </summary>
        ///// <param name="resp"></param>
        ///// <param name="responseXml"></param>
        ///// <param name="penalityList"></param>
        ///// <param name="savePenality"></param>
        ///// <returns></returns>
        //private Dictionary<string, string> ReadResponseChargeCondition(string resp, string responseXml, ref List<HotelPenality> penalityList, bool savePenality, HotelItinerary itinerary)
        //{
        //    Trace.TraceInformation("TravcoApi.ReadResponseChargeCondition entered");
        //    XmlNode tempNode;
        //    string lastCancellation = string.Empty;
        //    string message = string.Empty;
        //    string hotelPolicy = string.Empty;
        //    DateTime lastCancel = DateTime.Now;
        //    double buffer = 0;
        //    if (ConfigurationSystem.WSTConfig.ContainsKey("Buffer"))
        //    {
        //        buffer = Convert.ToDouble(ConfigurationSystem.WSTConfig["Buffer"]);
        //    }
        //    Dictionary<string, string> polList = new Dictionary<string, string>();
        //    TextReader stringRead = new StringReader(resp);
        //    //Loading two diff xmls in diff xml docs
        //    XmlDocument xmlAvail = new XmlDocument();
        //    xmlAvail.Load(stringRead);
        //    //stringRead = new StringReader(responseXml);
        //    //XmlDocument xmlStatic = new XmlDocument();
        //    //xmlStatic.Load(stringRead);
        //    // RATEPLANCODE-------- rpCode|rmcatCode|rateType|promotion
        //    string[] rpCode = itinerary.Roomtype[0].RatePlanCode.Split('|');
        //    // ROOMTYPECODE------- rtCode|XMLID|CartID|Sequence|HotelName
        //    string[] rtCode = itinerary.Roomtype[0].RoomTypeCode.Split('|');
        //    XmlNodeList tempNodeList = xmlAvail.SelectNodes("NewDataSet/getavailabilty");
        //    foreach (XmlNode temp in tempNodeList)
        //    {
        //        tempNode = temp.SelectSingleNode("rmcatcode");
        //        if (tempNode.InnerText == rpCode[1])
        //        {
        //            tempNode = temp.SelectSingleNode("rmtypcode");
        //            if (tempNode.InnerText == rtCode[0])
        //            {
        //                //tempNode = xmlAvail.SelectSingleNode("timelimit");
        //                //if (tempNode != null)
        //                //{
        //                //    lastCancellation = tempNode.InnerText;
        //                //}
        //                tempNode = temp.SelectSingleNode("timelimit");
        //                if (tempNode != null)
        //                {
        //                    lastCancel = DateTime.ParseExact(tempNode.InnerText, "yyyy/MM/dd", null);
        //                    lastCancellation = Convert.ToString(itinerary.StartDate.Subtract(lastCancel).Days);
        //                    lastCancel = lastCancel.AddDays(-buffer);
        //                    if (lastCancel.DayOfWeek == DayOfWeek.Saturday)
        //                    {
        //                        lastCancel = lastCancel.AddDays(-1);
        //                    }
        //                    else if (lastCancel.DayOfWeek == DayOfWeek.Sunday)
        //                    {
        //                        lastCancel = lastCancel.AddDays(-2);
        //                    }
        //                    message = "Full refund if cancelled on or before " + lastCancel.ToString("dd MMM yyyy") + "|NO REFUND AFTER " + lastCancel.ToString("dd MMM yyyy") + "|";
        //                }
        //                HotelPenality hp = new HotelPenality();
        //                hp.BookingSource = HotelBookingSource.WST;
        //                hp.FromDate = DateTime.Now;
        //                hp.ToDate = lastCancel;
        //                hp.PolicyType = ChangePoicyType.Other;
        //                hp.Price = 0;
        //                hp.IsAllowed = true;
        //                //tempNode = xmlStatic.SelectSingleNode("NewDataSet/getProviderInfo/cancellationpolicy");
        //                //if (tempNode != null)
        //                //{
        //                //    message += tempNode.InnerText + "|";
        //                //}
        //                //tempNode = xmlStatic.SelectSingleNode("NewDataSet/getProviderInfo/childpolicy");
        //                //if (tempNode != null)
        //                //{
        //                //    hotelPolicy = tempNode.InnerText + "|";
        //                //}
        //                penalityList.Add(hp);
        //                polList.Add("lastCancellationDate", lastCancellation);
        //                polList.Add("CancelPolicy", message);
        //                polList.Add("HotelPolicy", "");
        //                break;
        //            }
        //        }
        //    }
        //    if (polList.Count == 0)
        //    {
        //        Audit.Add(EventType.WSTAvailSearch, Severity.High, 0, "Error: xml returned from avail method is empty or doesnt contain any data|responseXML:" + resp, "");
        //    }
        //    return polList;
        //}
        #endregion

        /// <summary>
        /// To read the import booking xml response
        /// </summary>
        /// <param name="resp"></param>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        //private bool ReadGetBookedHotelResponse(string resp, ref HotelItinerary itinerary)
        //{
        //    Trace.TraceInformation("WST.ReadGetBookedHotelResponse entered");
        //    XmlNode tempNode;
        //    StaticData staticInfo = new StaticData();
        //    TextReader stringRead = new StringReader(resp);
        //    XmlDocument xmlDoc = new XmlDocument();
        //    xmlDoc.Load(stringRead);

        //    #region ErrorInfo Check
        //    XmlNode ErrorInfo = xmlDoc.SelectSingleNode("result/request/error/details");
        //    if (ErrorInfo != null)
        //    {
        //        Audit.Add(EventType.WSTImport, Severity.High, 0, " WST:ReadGetBookedHotelResponse,Error Message:" + ErrorInfo.Value + " | " + DateTime.Now + "| Response XML" + resp, "");
        //        Trace.TraceError("Error: " + ErrorInfo.InnerText);
        //        return false;
        //    }
        //    #endregion

        //    XmlNodeList tempNodeList = xmlDoc.SelectNodes("NewDataSet/Testing");
        //    HotelSource sourceInfo = new HotelSource();
        //    sourceInfo.Load("WST");
        //    decimal ourCommission = sourceInfo.OurCommission;
        //    int commissionType = 0;
        //    commissionType = (int)sourceInfo.CommissionTypeId;
        //    Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
        //    rateOfExList = staticInfo.CurrencyROE;
        //    decimal rateOfExchange = 1;
        //    string cCode = string.Empty;
        //    itinerary.Roomtype = new HotelRoom[tempNodeList.Count];
        //    itinerary.NoOfRooms = tempNodeList.Count;
        //    int i = 0;
        //    foreach (XmlNode temp in tempNodeList)
        //    {
        //        if (i == 0)
        //        {
        //            itinerary.Source = HotelBookingSource.WST;
        //            //HotelCode and HotelName
        //            tempNode = temp.SelectSingleNode("partycode");
        //            itinerary.HotelCode = tempNode.InnerText;
        //            tempNode = temp.SelectSingleNode("partyname");
        //            itinerary.HotelName = tempNode.InnerText;
        //            tempNode = temp.SelectSingleNode("partyaddress");
        //            itinerary.HotelAddress1 = tempNode.InnerText;
        //            tempNode = temp.SelectSingleNode("chkindate");
        //            itinerary.StartDate = DateTime.ParseExact(tempNode.InnerText, "dd/MM/yyyy", null);
        //            tempNode = temp.SelectSingleNode("chkoutdate");
        //            itinerary.EndDate = DateTime.ParseExact(tempNode.InnerText, "dd/MM/yyyy", null);
        //            //currency details
        //            tempNode = temp.SelectSingleNode("currency");
        //            tempNode.InnerText = tempNode.InnerText.Replace("(", "");
        //            cCode = tempNode.InnerText.Replace(")", "");
        //            tempNode = temp.SelectSingleNode("agentref");
        //            itinerary.BookingRefNo = tempNode.InnerText;
        //        }
        //        //search for Passenger Details
        //        List<HotelPassenger> paxList = new List<HotelPassenger>();
        //        List<int> childAge = new List<int>();

        //        #region Passenger Details
        //        tempNode = temp.SelectSingleNode("guestname");
        //        string[] guests = tempNode.InnerText.Split('/');
        //        foreach (string guest in guests)
        //        {
        //            HotelPassenger pax = new HotelPassenger();
        //            pax.Firstname = guest;
        //            pax.Lastname = string.Empty;
        //            paxList.Add(pax);
        //        }
        //        if (i == 0)
        //        {
        //            itinerary.HotelPassenger = paxList[0];
        //        }
        //        #endregion


        //        //hotel rooms details here
        //        itinerary.Roomtype[i] = new HotelRoom();
        //        tempNode = temp.SelectSingleNode("services");
        //        itinerary.Roomtype[i].RoomTypeCode = tempNode.InnerText;
        //        tempNode = temp.SelectSingleNode("mealcode");
        //        itinerary.Roomtype[i].RatePlanCode = tempNode.InnerText;
        //        tempNode = temp.SelectSingleNode("services");
        //        itinerary.Roomtype[i].RoomName = tempNode.InnerText;
        //        tempNode = temp.SelectSingleNode("adults");
        //        itinerary.Roomtype[i].AdultCount = Convert.ToInt16(tempNode.InnerText);
        //        tempNode = temp.SelectSingleNode("child");
        //        itinerary.Roomtype[i].ChildCount = Convert.ToInt16(tempNode.InnerText);
        //        itinerary.Roomtype[i].ChildAge = childAge;
        //        itinerary.Roomtype[i].PassenegerInfo = paxList;
        //        if (rateOfExList.ContainsKey(cCode))
        //        {
        //            rateOfExchange = rateOfExList[cCode];
        //        }
        //        else
        //        {
        //            rateOfExchange = 1;
        //        }
        //        tempNode = temp.SelectSingleNode("saleprice");
        //        double ratePerDay = Convert.ToDouble(tempNode.InnerText);
        //        tempNode = temp.SelectSingleNode("nights");
        //        int nights = Convert.ToInt16(tempNode.InnerText);
        //        itinerary.Roomtype[i].RoomFareBreakDown = new HotelRoomFareBreakDown[nights];
        //        decimal totalPrice = 0; 
        //        for (int n = 0; n < nights; n++)
        //        {
        //            itinerary.Roomtype[i].RoomFareBreakDown[n] = new HotelRoomFareBreakDown();
        //            itinerary.Roomtype[i].RoomFareBreakDown[n].Date = itinerary.StartDate.AddDays(n);
        //            itinerary.Roomtype[i].RoomFareBreakDown[n].RoomPrice = Convert.ToDecimal(ratePerDay);
        //            totalPrice += Convert.ToDecimal(ratePerDay);
        //        }
        //        itinerary.Roomtype[i].NoOfUnits = "1";
        //        itinerary.Roomtype[i].Price = new PriceAccounts();
        //        itinerary.Roomtype[i].Price.Currency = cCode;
        //        itinerary.Roomtype[i].Price.PublishedFare = totalPrice;
        //        itinerary.Roomtype[i].Price.OurCommission = ourCommission;
        //        i++;
        //    }
        //    itinerary.Status = HotelBookingStatus.Confirmed;
        //    return true;
        //}
        #endregion

    }


}