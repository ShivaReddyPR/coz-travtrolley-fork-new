﻿/// <summary>
/// Summary description for SPNames
/// </summary>
/// 
namespace CT.ActivityDeals
{
    internal static class SPNames
    {
        public const string GetTheme = "usp_GetTheme";
        public const string GetCityByCountryCode = "usp_GetCityByCountryCode";
        public const string GetCountryList = "usp_GetCountryList";
        public const string ACTIVITY_ADD = "usp_Activity_Add";
        public const string ACTIVITY_PRICE_ADD = "usp_addPricedetails";
        public const string addActivityFlexMaster = "usp_addActivityFlexMaster";
        public const string ActivityGetData = "usp_ActivityMaster_getdata";
        public const string ActivityGetList = "usp_ActivityMaster_getList";
        // FD
         public const string FixedDepartureItineraryAdd = "usp_AddFixedDepartureItinerary";
    }
}
