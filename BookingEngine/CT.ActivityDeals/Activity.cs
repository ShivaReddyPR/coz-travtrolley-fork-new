﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

//using Technology.BookingEngine;

/// <summary>
/// Summary description for Activity
/// </summary>
/// 
namespace CT.ActivityDeals
{
    public class Activity
    {
        private const long NEW_RECORD = -1;

        #region Members
        private long _id;
        long _activityId;
        string _activityName;
        int _stockInHand;
        int _activityDays;
        decimal _startingFrom;
        string _city;
        string _country;
        string _region = string.Empty;
        string _image1;
        string _image2;
        string _image3;
        string _theme;
        string _introduction;
        string _overview;
        DateTime _startFrom;
        DateTime _endTo;
        string _activityDuration;
        DateTime _availableDateStart;
        DateTime _availableDateEnd;
        string _unvailableDates;
        string _itinerary1;
        string _itinerary2;
        string _details;
        string _supplierName;
        string _supplierEmail;
        string _mealsIncluded;
        string _transferInclude;
        string _pickLocation;
        DateTime _pickUpDateTime;
        string _dropOffLocation;
        DateTime _dropOffDateTime;
        string _exclusion;
        string _inclusion;
        string _cancellationPolicy;
        string _unAvailableDays;
        string _thingsToBring;
        int _bookingCutOff;

        int _flexOrder;
        string _flexLabel;
        string _flexControl;
        string _flexSqlQuery;
        string _flexDataType;
        string _flexMandatoryStatus;
        DataTable _dtACFDetails;
        DataTable _dtPriceDetails;
        string _image1Extn;
        string _image2Extn;
        string _image3Extn;
        int _createdBy;
        string _isFixedDeparture;
        DataTable _dtFDItineraryDetails;
        int _agencyId;
        string _fdInclusions = string.Empty;
        string _currency;
        long _stockinUsed;
        string _suppierServiceName = string.Empty;
        string _supplierTelephone = string.Empty;
        string _pickupPoint=string.Empty;
        #endregion

        #region Properties
        public long stockinUsed
        {
            get { return _stockinUsed; }
            set { _stockinUsed = value; }
        }
        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        public int ActivityDays
        {
            get { return _activityDays; }
            set { _activityDays = value; }
        }
        public int StockInHand
        {
            get { return _stockInHand; }
            set { _stockInHand = value; }
        }
        public decimal StartingFrom
        {
            get { return _startingFrom; }
            set { _startingFrom = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }
        public string Image1
        {
            get { return _image1; }
            set { _image1 = value; }
        }
        public string Image2
        {
            get { return _image2; }
            set { _image2 = value; }
        }
        public string Image3
        {
            get { return _image3; }
            set { _image3 = value; }
        }
        public string Theme
        {
            get { return _theme; }
            set { _theme = value; }
        }
        public string Introduction
        {
            get { return _introduction; }
            set { _introduction = value; }
        }
        public string Overview
        {
            get { return _overview; }
            set { _overview = value; }
        }
        public DateTime StartFrom
        {
            get { return _startFrom; }
            set { _startFrom = value; }
        }
        public DateTime EndTo
        {
            get { return _endTo; }
            set { _endTo = value; }
        }
        public string ActivityDuration
        {
            get { return _activityDuration; }
            set { _activityDuration = value; }
        }
        public DateTime AvailableDateStart
        {
            get { return _availableDateStart; }
            set { _availableDateStart = value; }
        }
        public DateTime AvailableDateEnd
        {
            get { return _availableDateEnd; }
            set { _availableDateEnd = value; }
        }
        public string UnvailableDates
        {
            get { return _unvailableDates; }
            set { _unvailableDates = value; }
        }
        public string Itinerary1
        {
            get { return _itinerary1; }
            set { _itinerary1 = value; }
        }
        public string Itinerary2
        {
            get { return _itinerary2; }
            set { _itinerary2 = value; }
        }
        public string Details
        {
            get { return _details; }
            set { _details = value; }
        }
        public string SupplierName
        {
            get { return _supplierName; }
            set { _supplierName = value; }
        }
        public string SupplierEmail
        {
            get { return _supplierEmail; }
            set { _supplierEmail = value; }
        }
        public string MealsIncluded
        {
            get { return _mealsIncluded; }
            set { _mealsIncluded = value; }
        }
        public string TransferInclude
        {
            get { return _transferInclude; }
            set { _transferInclude = value; }
        }
        public string PickLocation
        {
            get { return _pickLocation; }
            set { _pickLocation = value; }
        }
        public DateTime PickUpDateTime
        {
            get { return _pickUpDateTime; }
            set { _pickUpDateTime = value; }
        }
        public string DropOffLocation
        {
            get { return _dropOffLocation; }
            set { _dropOffLocation = value; }
        }
        public DateTime DropOffDateTime
        {
            get { return _dropOffDateTime; }
            set { _dropOffDateTime = value; }
        }
        public string Exclusion
        {
            get { return _exclusion; }
            set { _exclusion = value; }
        }
        public string Inclusion
        {
            get { return _inclusion; }
            set { _inclusion = value; }
        }
        public string CancellationPolicy
        {
            get { return _cancellationPolicy; }
            set { _cancellationPolicy = value; }
        }
        public string UnAvailableDays
        {
            get { return _unAvailableDays; }
            set { _unAvailableDays = value; }
        }
        public string ThingsToBring
        {
            get { return _thingsToBring; }
            set { _thingsToBring = value; }
        }
        public int BookingCutOff
        {
            get { return _bookingCutOff; }
            set { _bookingCutOff = value; }
        }

        public int FlexOrder
        {
            get { return _flexOrder; }
            set { _flexOrder = value; }
        }

        public string FlexLabel
        {
            get { return _flexLabel; }
            set { _flexLabel = value; }
        }
        public string FlexControl
        {
            get { return _flexControl; }
            set { _flexControl = value; }
        }
        public string FlexSqlQuery
        {
            get { return _flexSqlQuery; }
            set { _flexSqlQuery = value; }
        }
        public string FlexDataType
        {
            get { return _flexDataType; }
            set { _flexDataType = value; }
        }
        public string FlexMandatoryStatus
        {
            get { return _flexMandatoryStatus; }
            set { _flexMandatoryStatus = value; }
        }
        public DataTable DTAFCDetails
        {
            get { return _dtACFDetails; }
            set { _dtACFDetails = value; }
        }
        public DataTable DTPriceDetails
        {
            get { return _dtPriceDetails; }
            set { _dtPriceDetails = value; }
        }


        public string Image1Extn
        {
            get { return _image1Extn; }
            set { _image1Extn = value; }
        }
        public string Image2Extn
        {
            get { return _image2Extn; }
            set { _image2Extn = value; }
        }
        public string Image3Extn
        {
            get { return _image3Extn; }
            set { _image3Extn = value; }
        }

        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public string IsFixedDeparture
        {
            get { return _isFixedDeparture; }
            set { _isFixedDeparture = value; }
        }
        public DataTable DTFDItineraryDetails
        {
            get { return _dtFDItineraryDetails; }
            set { _dtFDItineraryDetails = value; }
        }

        public int AgencyId
        {
            get { return _agencyId; }
            set { _agencyId = value; }
        }
        public string FDInclusions
        {
            get { return _fdInclusions; }
            set { _fdInclusions = value; }
        }

        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }

        public string SupplierServiceName
        {
            get { return _suppierServiceName; }
            set { _suppierServiceName = value; }
        }

        public string SupplierTelePhone
        {
            get { return _supplierTelephone; }
            set { _supplierTelephone = value; }
        }

        public string PickupPoint
        {
            get { return _pickupPoint; }
            set { _pickupPoint = value; }
        }
        #endregion

        public Activity()
        {
            _id = -1;
            DataSet ds = GetData(_id);
            _dtACFDetails = ds.Tables[0];
            _dtPriceDetails = ds.Tables[1];
        }

        public Activity(long id)
        {
            _id = id;
            GetDetails(id);
        }

        private void GetDetails(long id)
        {

            DataSet ds = GetData(id);
            if (ds.Tables[0] != null) UpdateBusinessData(ds.Tables[0].Rows[0]);

            _dtACFDetails = ds.Tables[1];
            _dtPriceDetails = ds.Tables[2];
            _dtFDItineraryDetails = ds.Tables[3];
        }

        private DataSet GetData(long id)
        {
            DataSet ds = new DataSet();
            //using (SqlConnection connection = DBGateway.Dal.GetConnection()) 
            //{
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ACTIVITY_ID", id);
                ds = DBGateway.FillSP(SPNames.ActivityGetData, paramList);
                return ds;
            }
            catch
            {
                throw;
            }

        }

        private void UpdateBusinessData(DataRow dr)
        {

            try
            {
                if (_id >= 1)
                {

                    _id = Convert.ToInt64(dr["activityId"]);
                    _agencyId = Convert.ToInt16(dr["agencyId"]);
                    _activityName = Convert.ToString(dr["activityName"]);
                    _activityDays = Convert.ToInt16(dr["activityDays"]);
                    _startingFrom = Convert.ToDecimal(dr["activityStartingFrom"]);
                    if(dr["stockInHand"]!=DBNull.Value)_stockInHand = Convert.ToInt32(dr["stockInHand"]);
                    if (dr["stockUsed"] != DBNull.Value) _stockinUsed = Convert.ToInt32(dr["stockUsed"]);
                    else _stockinUsed = 0;
                    // _stockinUsed = Convert.ToInt32(dr["stockUsed"]);
                    _city = Convert.ToString(dr["activityCity"]);
                    _country = Convert.ToString(dr["activityCountry"]);
                    _region = dr["region"].ToString();
                    _image1 = Convert.ToString(dr["imagePath1"]);
                    _image2 = Convert.ToString(dr["imagePath2"]);
                    _image3 = Convert.ToString(dr["imagePath3"]);
                    _theme = Convert.ToString(dr["themeId"]);
                    _introduction = Convert.ToString(dr["introduction"]);
                    _overview = Convert.ToString(dr["overview"]);
                    _startFrom = Convert.ToDateTime(dr["startFrom"]);
                    _endTo = Convert.ToDateTime(dr["endTo"]);
                    _activityDuration = Convert.ToString(dr["durationHours"]);
                    _availableDateStart = Convert.ToDateTime(dr["availableFrom"]);
                    _availableDateEnd = Convert.ToDateTime(dr["availableTo"]);
                    _unAvailableDays = Convert.ToString(dr["unavailableDays"]);
                    _itinerary1 = Convert.ToString(dr["itinerary1"]);
                    _itinerary2 = Convert.ToString(dr["itinerary2"]);
                    _details = Convert.ToString(dr["details"]);
                    _supplierName = Convert.ToString(dr["supplierName"]);
                    _supplierEmail = Convert.ToString(dr["supplierEmail"]);
                    _mealsIncluded = Convert.ToString(dr["mealsIncluded"]);
                    _transferInclude = Convert.ToString(dr["transferIncluded"]);
                    _pickLocation = Convert.ToString(dr["pickupLocation"]);
                    if (dr["pickupDate"] != DBNull.Value)
                    {
                        _pickUpDateTime = Convert.ToDateTime(dr["pickupDate"]);
                    }
                    _dropOffLocation = Convert.ToString(dr["dropoffLocation"]);
                    if (dr["dropoffDate"] != DBNull.Value)
                    {
                        _dropOffDateTime = Convert.ToDateTime(dr["dropoffDate"]);
                    }
                    _exclusion = Convert.ToString(dr["exclusions"]);
                    _inclusion = Convert.ToString(dr["inclusions"]);
                    _cancellationPolicy = Convert.ToString(dr["cancellPolicy"]);
                    _thingsToBring = Convert.ToString(dr["thingsToBring"]);
                    _unvailableDates = Convert.ToString(dr["unavailableDates"]);
                    _bookingCutOff = Convert.ToInt16(dr["bookingCutOff"]);
                    _suppierServiceName = Convert.ToString(dr["SupplierServiceName"]);
                    _supplierTelephone = Convert.ToString(dr["SupplierTelephone"]);
                    _pickupPoint = Convert.ToString(dr["pickup_point"]);
                    if (dr["FDInclusions"] != DBNull.Value)
                    {
                        _fdInclusions = dr["FDInclusions"].ToString();
                    }
                    else
                    {
                        _fdInclusions = "";
                    }
                    if (dr["currency"] != DBNull.Value)
                    {
                        _currency = dr["currency"].ToString();
                    }
                    else
                    {
                        _currency = "";
                    }
                }

            }
            catch
            {
                throw;
            }
        }

        public static DataTable GetTheme(string themeType)
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {

                    paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@themeType", themeType);
                    data = DBGateway.ExecuteReaderSP(SPNames.GetTheme, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        public static DataTable GetCountry()
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {

                    paramList = new SqlParameter[0];
                    data = DBGateway.ExecuteReaderSP(SPNames.GetCountryList, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }


        public static DataTable GetCity(string id)
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@countryCode", id);

                    data = DBGateway.ExecuteReaderSP(SPNames.GetCityByCountryCode, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }

        public static DataTable GetList(string isFixedDep, int agentId)
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList;
                try
                {

                    paramList = new SqlParameter[2];
                    paramList[0] = new SqlParameter("@isFixedDep", isFixedDep);
                    //paramList[1] = new SqlParameter( "@P_AGENT_ID",  agentId);
                    if (agentId > 0) paramList[1] = new SqlParameter("@P_AGENT_ID", agentId);
                    else paramList[1] = new SqlParameter("@P_AGENT_ID", DBNull.Value);
                    data = DBGateway.ExecuteReaderSP(SPNames.ActivityGetList, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                    connection.Close();
                }
                catch
                {
                    throw;
                }
            }
            return dt;
        }
        public void SaveActivity()
        {

            //SqlTransaction trans = null;
            //SqlCommand cmd = null;

            //using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
            //{
            try
            {
                using (SqlConnection connection = DBGateway.GetConnection())
                    try
                    {
                        SqlParameter[] paramList = new SqlParameter[51];
                        paramList[0] = new SqlParameter("@activityName", _activityName);
                        paramList[1] = new SqlParameter("@activityDays", _activityDays);
                        paramList[2] = new SqlParameter("@activityStartingFrom", _startingFrom);
                        paramList[3] = new SqlParameter("@stockinHand", _stockInHand);
                        paramList[4] = new SqlParameter("@activityCity", _city);
                        paramList[5] = new SqlParameter("@activityCountry", _country);
                        paramList[6] = new SqlParameter("@imagePath1", _image1);
                        paramList[7] = new SqlParameter("@imagePath2", _image2);
                        if (_image3 == string.Empty)
                        {
                            paramList[8] = new SqlParameter("@imagePath3", DBNull.Value);//_image3 == string.Empty ? DBNull.Value.ToString() : _image3);
                        }
                        else
                        {
                            paramList[8] = new SqlParameter("@imagePath3", _image3);
                        }
                        paramList[9] = new SqlParameter("@themeId", _theme == string.Empty ? DBNull.Value.ToString() : _theme);
                        paramList[10] = new SqlParameter("@introduction", _introduction);

                        paramList[11] = new SqlParameter("@overview", _overview);
                        paramList[12] = new SqlParameter("@startFrom", _startFrom);
                        paramList[13] = new SqlParameter("@endTo", _endTo);
                        paramList[14] = new SqlParameter("@ActivityDuration", _activityDuration == string.Empty ? DBNull.Value.ToString() : _activityDuration);
                        paramList[15] = new SqlParameter("@availableFrom", _availableDateStart);
                        paramList[16] = new SqlParameter("@availableTo", _availableDateEnd);
                        paramList[17] = new SqlParameter("@unavailableDates", _unvailableDates == string.Empty ? DBNull.Value.ToString() : _unvailableDates);
                        paramList[18] = new SqlParameter("@itinerary1", _itinerary1);
                        paramList[19] = new SqlParameter("@itinerary2", _itinerary2 == string.Empty ? DBNull.Value.ToString() : _itinerary2);
                        paramList[20] = new SqlParameter("@details", _details == string.Empty ? DBNull.Value.ToString() : _details);
                        paramList[21] = new SqlParameter("@supplierName", _supplierName == string.Empty ? DBNull.Value.ToString() : _supplierName);

                        paramList[22] = new SqlParameter("@supplierEmail", _supplierEmail);
                        paramList[23] = new SqlParameter("@mealsIncluded", _mealsIncluded);
                        paramList[24] = new SqlParameter("@transferIncluded", _transferInclude);
                        paramList[25] = new SqlParameter("@pickupLocation", _pickLocation);
                        if (_pickUpDateTime == DateTime.MinValue)
                            paramList[26] = new SqlParameter("@pickupDate", DBNull.Value);
                        else
                            paramList[26] = new SqlParameter("@pickupDate", _pickUpDateTime);
                        // paramList[26] = new SqlParameter("@pickupDate", _pickUpDateTime == DateTime.MinValue ? DateTime.MinValue : _pickUpDateTime);
                        paramList[27] = new SqlParameter("@dropoffLocation", _dropOffLocation);

                        if (_dropOffDateTime == DateTime.MinValue)
                            paramList[28] = new SqlParameter("@dropoffDate", DBNull.Value);
                        else
                            paramList[28] = new SqlParameter("@dropoffDate", _dropOffDateTime);

                        // paramList[28] = new SqlParameter("@dropoffDate", _dropOffDateTime == DateTime.MinValue ? DateTime.MinValue : _dropOffDateTime);

                        paramList[29] = new SqlParameter("@exclusions", _exclusion);
                        paramList[30] = new SqlParameter("@inclusions", _inclusion);
                        paramList[31] = new SqlParameter("@cancellPolicy", _cancellationPolicy);
                        paramList[32] = new SqlParameter("@thingsToBring", _thingsToBring);
                        paramList[33] = new SqlParameter("@unavailableDays", _unAvailableDays);
                        paramList[34] = new SqlParameter("@bookingCutOff", _bookingCutOff);
                        paramList[35] = new SqlParameter("@createdBy", _createdBy);
                        paramList[36] = new SqlParameter("@img1_extn", _image1Extn);
                        paramList[37] = new SqlParameter("@img2_extn", _image2Extn);
                        paramList[38] = new SqlParameter("@img3_extn", _image3Extn == null ? DBNull.Value.ToString() : _image3Extn);
                        paramList[39] = new SqlParameter("@activityId", SqlDbType.BigInt);
                        paramList[39].Direction = ParameterDirection.Output;
                        paramList[40] = new SqlParameter("@A_MSG_TYPE", SqlDbType.VarChar, 10);
                        paramList[40].Direction = ParameterDirection.Output;
                        paramList[41] = new SqlParameter("@A_MSG_TEXT", SqlDbType.VarChar, 200);
                        paramList[41].Direction = ParameterDirection.Output;

                        paramList[42] = new SqlParameter("@id", _id);
                        paramList[43] = new SqlParameter("@isfixedDeparture", _isFixedDeparture);
                        paramList[44] = new SqlParameter("@region", _region == string.Empty ? DBNull.Value.ToString() : _region);
                        paramList[45] = new SqlParameter("@agencyid", _agencyId);
                        paramList[46] = new SqlParameter("@fdInclusions", _fdInclusions);
                        paramList[47] = new SqlParameter("@currency", _currency);
                        paramList[48] = new SqlParameter("@serviceName", _suppierServiceName);
                        paramList[49] = new SqlParameter("@supplierTelephone", _supplierTelephone);
                        if (_pickupPoint.Length > 0)
                            paramList[50] = new SqlParameter("@pickupPoint", _pickupPoint);
                        else
                            paramList[50] = new SqlParameter("@pickupPoint", string.Empty);
                        DBGateway.ExecuteReaderSP(SPNames.ACTIVITY_ADD, paramList, connection);

                        string messageType = Convert.ToString(paramList[40].Value);
                        if (messageType == "E")
                        {
                            string message = Convert.ToString(paramList[41].Value);

                            if (message != string.Empty) throw new Exception(message);
                        }

                        _activityId = Convert.ToInt64(paramList[39].Value);
                        _id = _activityId;
                        if (_dtACFDetails != null)
                        {
                            DataTable dt = _dtACFDetails.GetChanges();

                            int recordStatus = 0;
                            if (dt != null && dt.Rows.Count > 0)
                            {

                                foreach (DataRow dr in dt.Rows)
                                {
                                    switch (dr.RowState)
                                    {
                                        case DataRowState.Added:
                                            recordStatus = 1;
                                            break;
                                        case DataRowState.Modified:
                                            recordStatus = 2;
                                            break;
                                        case DataRowState.Deleted:
                                            recordStatus = -1;
                                            dr.RejectChanges();
                                            break;
                                        default: break;

                                    }
                                    if (Convert.ToInt32(dr["flexid"]) > 0)
                                    {
                                        SaveFlexDeatils(_activityId, Convert.ToInt32(dr["flexOrder"]), Convert.ToString(dr["flexLabel"]), Convert.ToString(dr["flexControl"]), Convert.ToString(dr["flexSqlQuery"]), Convert.ToString(dr["flexDataType"]), Convert.ToString(dr["flexMandatoryStatus"]), Convert.ToInt32(dr["flexid"]), Convert.ToInt32(dr["flexCreatedBy"]), recordStatus);
                                    }

                                }
                            }
                        }
                        if (_dtPriceDetails != null)
                        {
                            DataTable dt = _dtPriceDetails.GetChanges();
                            int recordStatus = 0;
                            if (dt != null && dt.Rows.Count > 0)
                            {


                                foreach (DataRow dr in dt.Rows)
                                {
                                    switch (dr.RowState)
                                    {
                                        case DataRowState.Added:
                                            recordStatus = 1;
                                            break;
                                        case DataRowState.Modified:
                                            recordStatus = 2;
                                            break;
                                        case DataRowState.Deleted:
                                            recordStatus = -1;
                                            dr.RejectChanges();
                                            break;
                                        default: break;


                                    }
                                    if (Convert.ToInt32(dr["priceId"]) > 0)
                                    {
                                        if (_isFixedDeparture == "Y")
                                        {
                                            SavePriceDeatils(_activityId, Convert.ToString(dr["Label"]), Convert.ToInt32(dr["amount"]), Convert.ToDecimal(dr["SupplierCost"]), Convert.ToInt32(dr["tax"]), Convert.ToDecimal(dr["markup"]), Convert.ToDateTime(dr["PriceDate"]), recordStatus, Convert.ToInt32(dr["priceId"]), Convert.ToString(dr["PaxType"]), Convert.ToInt32(dr["StockInHand"]), Convert.ToInt32(dr["createdBy"]), Convert.ToInt32(dr["MinPax"]));
                                        }
                                        else
                                        {
                                            SavePriceDeatils(_activityId, Convert.ToString(dr["Label"]), Convert.ToInt32(dr["amount"]), Convert.ToDecimal(dr["SupplierCost"]), Convert.ToInt32(dr["tax"]), Convert.ToDecimal(dr["markup"]), DateTime.MinValue, recordStatus, Convert.ToInt32(dr["priceId"]), string.Empty, 0, Convert.ToInt32(dr["createdBy"]), Convert.ToInt32(dr["MinPax"]));
                                        }
                                    }
                                }
                            }
                        }
                        if (_isFixedDeparture == "Y") SaveFixedDepartureItineraryDetails();

                        connection.Close();
                    }

                    catch
                    {
                        throw;
                    }
                //scope.Complete();
            }
            catch (System.Transactions.TransactionAbortedException ex)
            {
                throw ex;
            }

            //}
        }

        public void SaveFlexDeatils(long activityid, int flexorder, string flexlabel, string flexcontrol, string flexquery, string flexdatatype, string flexmandatorystatus, long flexId, int createdBy, int recordStatus)
        {
            using (SqlConnection connection = DBGateway.GetConnection())
                try
                {
                    SqlParameter[] paramList = new SqlParameter[12];
                    paramList[0] = new SqlParameter("@flexActivityId", activityid);
                    paramList[1] = new SqlParameter("@flexOrder", flexorder);
                    paramList[2] = new SqlParameter("@flexLabel", flexlabel);
                    paramList[3] = new SqlParameter("@flexControl", flexcontrol);
                    paramList[4] = new SqlParameter("@flexSqlQuery", flexquery);
                    paramList[5] = new SqlParameter("@flexDataType", flexdatatype);
                    paramList[6] = new SqlParameter("@flexMandatoryStatus", flexmandatorystatus);
                    paramList[7] = new SqlParameter("@flexCreatedBy", createdBy);
                    SqlParameter paramMsgType = new SqlParameter("@A_MSG_TYPE", SqlDbType.NVarChar);
                    paramMsgType.Size = 10;
                    paramMsgType.Direction = ParameterDirection.Output;
                    paramList[8] = paramMsgType;

                    SqlParameter paramMsgText = new SqlParameter("@A_MSG_TEXT", SqlDbType.NVarChar);
                    paramMsgText.Size = 100;
                    paramMsgText.Direction = ParameterDirection.Output;
                    paramList[9] = paramMsgText;
                    paramList[10] = new SqlParameter("@flexId", flexId);
                    paramList[11] = new SqlParameter("@recordStatus", recordStatus);
                    DBGateway.ExecuteReaderSP(SPNames.addActivityFlexMaster, paramList, connection);
                    if (Convert.ToString(paramMsgType.Value) == "E")
                        throw new Exception(Convert.ToString(paramMsgText.Value));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
        }

        public void SavePriceDeatils(long activityid, string label, decimal amount, decimal supplierCost, decimal tax, decimal markup, DateTime priceDate, int recordStatus, long priceId, string paxType, int stockInHand, int createdBy, int minPax)
        {


            using (SqlConnection connection = DBGateway.GetConnection())
                try
                {
                    SqlParameter[] paramList = new SqlParameter[15];
                    paramList[0] = new SqlParameter("@priceActivityId", activityid);
                    paramList[1] = new SqlParameter("@Label", label);
                    paramList[2] = new SqlParameter("@amount", amount);
                    paramList[3] = new SqlParameter("@SupplierCost", supplierCost);
                    paramList[4] = new SqlParameter("@tax", tax);
                    paramList[5] = new SqlParameter("@markup", markup);
                    paramList[6] = new SqlParameter("@PriceCreatedBy", createdBy);
                    SqlParameter paramMsgType = new SqlParameter("@A_MSG_TYPE", SqlDbType.NVarChar);
                    paramMsgType.Size = 10;
                    paramMsgType.Direction = ParameterDirection.Output;
                    paramList[7] = paramMsgType;

                    SqlParameter paramMsgText = new SqlParameter("@A_MSG_TEXT", SqlDbType.NVarChar);
                    paramMsgText.Size = 100;
                    paramMsgText.Direction = ParameterDirection.Output;
                    paramList[8] = paramMsgText;
                    paramList[9] = new SqlParameter("@priceId", priceId);
                    paramList[10] = new SqlParameter("@recordStatus", recordStatus);
                    if (priceDate == DateTime.MinValue) paramList[11] = new SqlParameter("@priceDate", DBNull.Value);
                    else
                        paramList[11] = new SqlParameter("@priceDate", priceDate);
                    if (paxType == string.Empty)
                    {
                        paramList[12] = new SqlParameter("@PaxType", DBNull.Value);
                    }
                    else
                    {
                        paramList[12] = new SqlParameter("@PaxType", paxType);
                    }
                    if (stockInHand == 0)
                    {
                        paramList[13] = new SqlParameter("@StockInHand", DBNull.Value);
                    }
                    else
                    {
                        paramList[13] = new SqlParameter("@StockInHand", stockInHand);
                    }
                    //newly added minPax    Added by brahmam 17.10.2016
                    if (minPax == 0)
                    {
                        paramList[14] = new SqlParameter("@MinPax", DBNull.Value);
                    }
                    else
                    {
                        paramList[14] = new SqlParameter("@MinPax", minPax);
                    }
                    DBGateway.ExecuteReaderSP(SPNames.ACTIVITY_PRICE_ADD, paramList, connection);
                    if (Convert.ToString(paramMsgType.Value) == "E")
                        throw new Exception(Convert.ToString(paramMsgText.Value));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
        }

        void SaveFixedDepartureItineraryDetails()
        {
            if (_dtFDItineraryDetails != null)
            {
                using (SqlConnection connection = DBGateway.GetConnection())

                    for (int i = 0; i < _dtFDItineraryDetails.Rows.Count; i++)
                    {
                        try
                        {
                            DataRow row = _dtFDItineraryDetails.Rows[i];
                            SqlParameter[] paramList = new SqlParameter[10];
                            paramList[0] = new SqlParameter("@FDId", _id);
                            paramList[1] = new SqlParameter("@ItineraryName", row["ItineraryName"].ToString());
                            paramList[2] = new SqlParameter("@Meals", row["meals"].ToString());
                            paramList[3] = new SqlParameter("@Itinerary", row["itinerary"].ToString());
                            paramList[4] = new SqlParameter("@Day", (i + 1));
                            paramList[5] = new SqlParameter("@Status", "A");
                            paramList[6] = new SqlParameter("@CreatedBy", _createdBy);
                            SqlParameter paramMsgType = new SqlParameter("@A_MSG_TYPE", SqlDbType.NVarChar);
                            paramMsgType.Size = 10;
                            paramMsgType.Direction = ParameterDirection.Output;
                            paramList[7] = paramMsgType;

                            SqlParameter paramMsgText = new SqlParameter("@A_MSG_TEXT", SqlDbType.NVarChar);
                            paramMsgText.Size = 100;
                            paramMsgText.Direction = ParameterDirection.Output;
                            paramList[8] = paramMsgText;
                            if (row.RowState == DataRowState.Added)
                            {
                                paramList[9] = new SqlParameter("@Id", -1);
                            }
                            else
                            {
                                paramList[9] = new SqlParameter("@Id", row["Id"]);
                            }

                            DBGateway.ExecuteNonQuerySP(SPNames.FixedDepartureItineraryAdd, paramList);
                            if (Convert.ToString(paramMsgType.Value) == "E")
                                throw new Exception(Convert.ToString(paramMsgText.Value));
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
            }
        }


        //Added by brahmam Activity Status Changing
        public static void ActivityStatusUpdate(int activityId, string activityStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_ActivityId", activityId);
                paramList[1] = new SqlParameter("@P_ActivityStatus", activityStatus);
                DBGateway.FillDataTableSP("usp_ActivityStatus_Update", paramList);
            }
            catch { throw; }
        }
    }
}
