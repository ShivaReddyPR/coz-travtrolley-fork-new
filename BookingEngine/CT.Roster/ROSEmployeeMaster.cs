﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

namespace CT.Roster
{
    public class ROSEmployeeMaster
    {
        #region Variabule Declaration
        int New_Record = -1;
        int empId;
        string staffId;
        string title;
        string firstName;
        string lastName;
        string email;
        string mobileNo;
        string alternateNo;
        string staffType;
        string password;
        int locFrom;
        string locFromDetail;
        string locFromMap;
        int locTo;
        string locToDetails;
        string locToMap;
        string designation;
        int createdBy;
        long _tariffId;
        decimal _tariffAmount;
        string _serviceStatus;
        string _remarks;

       #endregion


        #region Properties
		public int EmpId
        {
            get { return empId; }
            set { empId = value; }
        }
        

        public string StaffId
        {
            get { return staffId; }
            set { staffId = value; }
        }
       

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
       

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
       

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
       

        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        

        public string MobileNo
        {
            get { return mobileNo; }
            set { mobileNo = value; }
        }
        

        public string AlternateNo
        {
            get { return alternateNo; }
            set { alternateNo = value; }
        }
        public string StaffType
        {
            get { return staffType; }
            set { staffType = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }
       

        public int LocFrom
        {
            get { return locFrom; }
            set { locFrom = value; }
        }
       

        public string LocFromDetail
        {
            get { return locFromDetail; }
            set { locFromDetail = value; }
        }
        

        public string LocFromMap
        {
            get { return locFromMap; }
            set { locFromMap = value; }
        }
       

        public int LocTo
        {
            get { return locTo; }
            set { locTo = value; }
        }
        

        public string LocToDetails
        {
            get { return locToDetails; }
            set { locToDetails = value; }
        }
        

        public string LocToMap
        {
            get { return locToMap; }
            set { locToMap = value; }
        }
        

        public string Designation
        {
            get { return designation; }
            set { designation = value; }
        }
        

        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public long TariffId
        {
            get { return _tariffId; }
            set { _tariffId = value; }        
        }

        public decimal  TariffAmount
        {
            get { return _tariffAmount; }
            set { _tariffAmount = value; }
        }

       public string ServiceStatus
       {
           get { return _serviceStatus; }
           set { _serviceStatus = value; }    
        }

       public string Remarks
       {
           get { return _remarks; }
           set { _remarks = value; }
       }
	#endregion

        public ROSEmployeeMaster()
        {
            empId = New_Record;
        }
        public  ROSEmployeeMaster(int id)
        {
            empId = id;
            getDetails(empId);
        }
        private void getDetails(int id)
        {
            try
            {
                DataSet ds = GetData(id);
                UpdateBusinessData(ds);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Load ROS Employee Master Details. Error: " + ex.Message, "0");
                throw ex;
            }

        }

        private DataSet GetData(int id)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_EMP_ID", id);

                DataSet dsResult = DBGateway.ExecuteQuery("P_ROS_EmpMasterGetData", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToInteger(dr["emp_Id"]) > 0 && dr.ItemArray != null) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }


                }

            }
            catch
            { throw; }

        }
        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                if (empId < 0)
                {
                    empId = Utility.ToInteger(dr["emp_Id"]);
                }
                else
                {
                    empId = Utility.ToInteger(dr["emp_Id"]);
                    staffId = Utility.ToString(dr["staff_id"]);
                    title = Utility.ToString(dr["title"]);
                    firstName = Utility.ToString(dr["firstName"]);
                    lastName = Utility.ToString(dr["lastName"]);
                    email = Utility.ToString(dr["email"]);
                    mobileNo = Utility.ToString(dr["mobileNo"]);
                    alternateNo = Utility.ToString(dr["alternateNo"]);
                    staffType = Utility.ToString(dr["staffType"]);
                    locFrom = Utility.ToInteger(dr["locationFrom"]);
                    locFromDetail = Utility.ToString(dr["locationFromDetails"]);
                    locFromMap = Utility.ToString(dr["locationFromMap"]);
                    locTo = Utility.ToInteger(dr["locationTo"]);
                    locToDetails = Utility.ToString(dr["locationToDetails"]);
                    locToMap = Utility.ToString(dr["locationToMap"]);
                    designation = Utility.ToString(dr["designation"]);
                    _tariffId = Utility.ToInteger(dr["emp_trf_id"]);
                    _tariffAmount =  Utility.ToDecimal(dr["emp_trf_amount"]);
                    _serviceStatus = Utility.ToString(dr["EMP_SERVICE_STATUS"]);
                    _remarks = Utility.ToString(dr["EMP_REMARKS"]);
                  
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Load ROS Employee Master Details. Error: " + ex.Message, "0");
                throw ex;
            }

        }

        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[24];
                paramList[0] = new SqlParameter("@P_EMP_ID", empId);
                paramList[1] = new SqlParameter("@P_STAFF_ID", staffId);
                paramList[2] = new SqlParameter("@P_PASSWORD", password);
                paramList[3] = new SqlParameter("@P_TITLE", title);
                paramList[4] = new SqlParameter("@P_FIRST_NAME", firstName);
                paramList[5] = new SqlParameter("@P_LAST_NAME", lastName);
                paramList[6] = new SqlParameter("@P_EMAIL", email);
                paramList[7] = new SqlParameter("@P_MOBILE_NO", mobileNo);
                if(!string.IsNullOrEmpty(alternateNo))paramList[8] = new SqlParameter("@P_ALTERNATE_NO", alternateNo);
                paramList[9] = new SqlParameter("@P_STAFF_TYPE", staffType);
                paramList[10] = new SqlParameter("@P_LOCATION_FROM", locFrom);
                if (!string.IsNullOrEmpty(locFromDetail)) paramList[11] = new SqlParameter("@P_LOCATION_FROM_DETAILS", locFromDetail);
                if (!string.IsNullOrEmpty(locFromMap)) paramList[12] = new SqlParameter("@P_LOCATION_FROM_MAP", locFromMap);
                paramList[13] = new SqlParameter("@P_LOCATION_TO", locTo);
                if (!string.IsNullOrEmpty(locToDetails)) paramList[14] = new SqlParameter("@P_LOCATION_TO_DETAILS", locToDetails);
                if (!string.IsNullOrEmpty(locToMap)) paramList[15] = new SqlParameter("@P_LOCATION_TO_MAP", locToMap);
                if (!string.IsNullOrEmpty(designation)) paramList[16] = new SqlParameter("@P_DESIGNATION", designation);
                paramList[17] = new SqlParameter("@P_CREATED_BY", createdBy);
                paramList[18] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[18].Direction = ParameterDirection.Output;
                paramList[19] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 100);
                paramList[19].Direction = ParameterDirection.Output;
                if (false)
                {
                    if(_tariffId > 0) paramList[20] = new SqlParameter("@P_EMP_TRF_ID", _tariffId);
                    if (_tariffAmount > 0) paramList[21] = new SqlParameter("@P_EMP_TRF_AMOUNT", _tariffAmount);
                    if (!string.IsNullOrEmpty(_serviceStatus)) paramList[22] = new SqlParameter("@P_EMP_SERVICE_STATUS", _serviceStatus);
                    if (!string.IsNullOrEmpty(_remarks)) paramList[23] = new SqlParameter("@P_EMP_REMARKS", _remarks);
                }

                int rowAffected = DBGateway.ExecuteNonQuery("P_ROS_EmpMasterAddUpdate", paramList);
                  string messageType = Utility.ToString(paramList[18].Value);
                  if (messageType == "E")
                  {
                      string message = Utility.ToString(paramList[19].Value);
                      if (message != string.Empty) throw new Exception(message);
                  }

            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Save ROS Employee Master Details. Error: " + ex.Message, "0");
                throw ex;
            }
        }

        #region Static Methods
        public static DataTable GetROSEmpMasterList(RecordStatus recordStatus,ListStatus listStatus,string staffType)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                paramList[1] = new SqlParameter("@P_LIST_STATUS", Utility.ToInteger(listStatus));
                if (!string.IsNullOrEmpty(staffType)) paramList[2] = new SqlParameter("@P_STAFFTYPE", staffType);
                return DBGateway.ExecuteQuery("P_ROS_EmpMasterGetList", paramList).Tables[0];
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "Failed To Get List Of ROS Employee Master. ERROR: " + ex.Message, "0");
                throw ex;
            }
        }

        public static int ValidatePasswordChangeRequest(string requestId)
        {
            {
                if (requestId == null || requestId.Length == 0)
                {
                    throw new ArgumentException("requestId should be non empty string");
                }
                int num = 0;
                SqlConnection connection = DBGateway.GetConnection();
                SqlDataReader sqlDataReader = DBGateway.ExecuteReaderSP("P_ROS_IsValidPasswordChangeRequest", new SqlParameter[]
			    {
				    new SqlParameter("@requestId", requestId)
			    }, connection);
                if (sqlDataReader.Read())
                {
                    num = (int)sqlDataReader["emp_Id"];
                }
                sqlDataReader.Close();
                connection.Close();
                return num;
            }
        }

        public static void DeletePasswordChangeRequest(string requestId)
        {
            try
            {
                if (requestId == null || requestId.Length == 0)
                {
                    throw new ArgumentException("requestId should be non empty string");
                }
                int rowsAffected = DBGateway.ExecuteNonQuerySP("P_ROS_DeletePasswordChangeRequest", new SqlParameter[]
			    {
				    new SqlParameter("@requestId", requestId)
			    });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void UpdateForGotPassword(int empId, string password)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@P_EMP_ID", empId);
                paramList[1] = new SqlParameter("@P_PASSWORD", password);
                int updated = DBGateway.ExecuteNonQuery("P_ROS_UpdateForGotPassword", paramList);
                if (updated == 0)
                {
                    throw new Exception("Your password is not Updated");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed Update ForgotPassword. ERROR: " + ex.Message, "0");
                throw ex;
            }
        }
        #endregion


    }
}
