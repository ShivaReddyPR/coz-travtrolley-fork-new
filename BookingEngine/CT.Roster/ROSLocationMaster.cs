﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;



namespace CT.Roster
{
    public class ROSLocationMaster
    {
        #region Variables
        int newRecord = -1;
        int locId;
        string locName;
        string locDetail;
        string locType;
        int createdBy;
        #endregion

        #region Properties
        public int LocId
        {
            get { return locId; }
            set { locId = value; }
        }


        public string LocName
        {
            get { return locName; }
            set { locName = value; }
        }


        public string LocDetail
        {
            get { return locDetail; }
            set { locDetail = value; }
        }


        public string LocType
        {
            get { return locType; }
            set { locType = value; }
        }


        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }


        #endregion

        #region Constructors
        public ROSLocationMaster()
        {
            locId = newRecord;
        }
        public ROSLocationMaster(int rosId)
        {
            locId = rosId;
            getDetails(locId);
        } 
        #endregion

        private void getDetails(int id)
        {
            try
            {
                DataSet ds = GetData(id);
                UpdateBusinessData(ds);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Load ROS Location Master Details. Error: " + ex.Message, "0");
                throw ex;
            }
            

        }

        private DataSet GetData(int id)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_LOC_ID", id);

                DataSet dsResult = DBGateway.ExecuteQuery("P_ROS_LocMasterGetData", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToInteger(dr["loc_id"]) > 0 && dr.ItemArray != null) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }
                }

            }
            catch
            { throw; }

        }

        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                if (locId < 0)
                {
                    locId = Utility.ToInteger(dr["loc_id"]);
                }
                else
                {

                    locId = Utility.ToInteger(dr["loc_id"]);
                    locName = Utility.ToString(dr["loc_name"]);
                    locType = Utility.ToString(dr["loc_type"]);
                    locDetail = Utility.ToString(dr["loc_detail"]);
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Load ROS Location Master Details. Error: " + ex.Message, "0");
                throw ex;
            }

        }

        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[7];
                paramList[0] = new SqlParameter("@P_LOC_ID",locId);
                 paramList[1] = new SqlParameter("@P_LOC_NAME",locName);
                if(!string.IsNullOrEmpty(locDetail)) paramList[2] = new SqlParameter("@P_LOC_DETAIL",locDetail);
                 paramList[3] = new SqlParameter("@P_LOC_TYPE",locType);
                 paramList[4] = new SqlParameter("@P_LOC_CREATED_BY",createdBy);
                 paramList[5] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                 paramList[5].Direction = ParameterDirection.Output;
                 paramList[6] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 100);
                 paramList[6].Direction = ParameterDirection.Output;


                 int rowAffected = DBGateway.ExecuteNonQuery("P_ROS_LocMasterAddUpdate", paramList);
                 string messageType = Utility.ToString(paramList[5].Value);
                 if (messageType == "E")
                 {
                     string message = Utility.ToString(paramList[6].Value);
                     if (message != string.Empty) throw new Exception(message);
                 }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Save ROS Location Master Details. Error: " + ex.Message, "0");
                throw ex;
            }
        }


        #region Static Methods
        public static string GetLocDetailsByLocId(int locId)
        {
            string locDetail = string.Empty;
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LOC_ID", locId);
                paramList[1] = new SqlParameter("@P_LOC_DETAIL", SqlDbType.NVarChar, 250);
                paramList[1].Direction = ParameterDirection.Output;
                DBGateway.FillDataTableSP("P_ROS_LocMasterGetDetailById", paramList);
                locDetail = Utility.ToString(paramList[1].Value);

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
                throw ex;

            }
            return locDetail;
        }
        public static DataTable GetROSLocMasterList(RecordStatus recordStatus, ListStatus listStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                paramList[1] = new SqlParameter("@P_LIST_STATUS", Utility.ToInteger(listStatus));
                return DBGateway.ExecuteQuery("P_ROS_LocMasterGetList", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed To Get List Of ROS Location Master. ERROR: " + ex.Message, "0");
                throw ex;
            }
        }
        #endregion

    }
}
