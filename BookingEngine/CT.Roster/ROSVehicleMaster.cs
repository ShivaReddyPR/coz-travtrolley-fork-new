﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

namespace CT.Roster
{
    public class ROSVehicleMaster
    {
        #region Variables
        int NEW_RECORD = -1;
        int vehId;
        string vehCode;
        string vehName;
        string vehNumber;
        int vehCreatedBy;
        #endregion

        #region Properties
        public int VehCreatedBy
        {
            get { return vehCreatedBy; }
            set { vehCreatedBy = value; }
        }

        public string VehNumber
        {
            get { return vehNumber; }
            set { vehNumber = value; }
        }

        public string VehName
        {
            get { return vehName; }
            set { vehName = value; }
        }

        public string VehCode
        {
            get { return vehCode; }
            set { vehCode = value; }
        }


        public int VehId
        {
            get { return vehId; }
            set { vehId = value; }
        }

        #endregion
        public ROSVehicleMaster()
        {
            vehId = NEW_RECORD;
        }
        public ROSVehicleMaster(int id)
        {
            vehId = id;
            getDetails(vehId);
        }

        private void getDetails(int id)
        {
            try
            {
                DataSet ds = GetData(id);
                UpdateBusinessData(ds);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Load ROS Vehicle Master Details. Error: " + ex.Message, "0");
                throw ex;
            }

        }

        private DataSet GetData(int id)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_VE_ID", id);

                DataSet dsResult = DBGateway.ExecuteQuery("P_ROS_VehicleMasterGetData", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToInteger(dr["ve_id"]) > 0 && dr.ItemArray != null) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }


                }

            }
            catch
            { throw; }

        }
        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                if (vehId < 0)
                {
                    vehId = Utility.ToInteger(dr["ve_id"]);
                }
                else
                {
                    vehId = Utility.ToInteger(dr["ve_id"]);
                    vehCode = Utility.ToString(dr["ve_code"]);
                    vehName = Utility.ToString(dr["ve_name"]);
                    vehNumber = Utility.ToString(dr["ve_number"]);
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Load ROS Vehicle Master Details. Error: " + ex.Message, "0");
                throw ex;
            }

        }

        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[7];
                paramList[0] = new SqlParameter("@P_VE_ID", vehId);
                paramList[1] = new SqlParameter("@P_VE_CODE", VehCode);
                paramList[2] = new SqlParameter("@P_VE_NAME", vehName);
                paramList[3] = new SqlParameter("@P_VE_NUMBER", vehNumber);
                paramList[4] = new SqlParameter("@P_VE_CREATEDBY", vehCreatedBy);
                
                paramList[5] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[5].Direction = ParameterDirection.Output;
                paramList[6] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 100);
                paramList[6].Direction = ParameterDirection.Output;

                int rowAffected = DBGateway.ExecuteNonQuery("P_ROS_VehicleMasterAddUpdate", paramList);
                string messageType = Utility.ToString(paramList[5].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[6].Value);
                    if (message != string.Empty) throw new Exception(message);
                }

            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Save ROS Vehicle Master Details. Error: " + ex.Message, "0");
                throw ex;
            }
        }
        #region Static Methods
        public static DataTable GetList(RecordStatus recordStatus, ListStatus listStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                paramList[1] = new SqlParameter("@P_LIST_STATUS", Utility.ToInteger(listStatus));
                return DBGateway.ExecuteQuery("P_ROS_VehicleMasterGetList", paramList).Tables[0]; ;
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "Failed To Get List Of ROS Vehicle Master. ERROR: " + ex.Message, "0");
                throw ex;
            }
            
        }
        #endregion
    }
}
