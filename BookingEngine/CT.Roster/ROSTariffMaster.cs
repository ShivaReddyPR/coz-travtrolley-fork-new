﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

namespace CT.Roster
{
    public class ROSTariffMaster
    {
        #region Variabule Declaration
        int New_Record = -1;
        long _trfId;
        string _code;
        int _companyId;
        int _areaId;
        decimal _amount;
        string _status;      
        int _createdBy;

       #endregion


        #region Properties
		public long TrfId
        {
            get { return _trfId; }
            set { _trfId = value; }
        }


        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }


        public int CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }


        public int AreaId
        {
            get { return _areaId; }
            set { _areaId = value; }
        }


        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }


        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        } 
	#endregion

        public ROSTariffMaster()
        {
            _trfId = New_Record;
        }
        public  ROSTariffMaster(long id)
        {
            _trfId  = id;
            getDetails(_trfId);
        }
        private void getDetails(long id)
        {
            try
            {
                DataSet ds = GetData(id);
                UpdateBusinessData(ds);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Load ROS Tariff Master Details. Error: " + ex.Message, "0");
                throw ex;
            }

        }

        private DataSet GetData(long id)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_TRF_ID", id);

                DataSet dsResult = DBGateway.ExecuteQuery("P_ROS_TARIFF_GETDATA", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }

        private void UpdateBusinessData(DataSet ds)
        {

            try
            {
                if (ds != null)
                {
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        if (Utility.ToInteger(dr["TRF_ID"]) > 0 && dr.ItemArray != null) UpdateBusinessData(ds.Tables[0].Rows[0]);
                    }


                }

            }
            catch
            { throw; }

        }
        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                if (_trfId < 0)
                {
                    _trfId = Utility.ToInteger(dr["TRF_ID"]);
                }
                else
                {
                    _trfId = Utility.ToInteger(dr["TRF_ID"]);
                    _code = Utility.ToString(dr["TRF_CODE"]);
                    _companyId = Utility.ToInteger(dr["TRF_COMPANY_ID"]);
                    _areaId = Utility.ToInteger(dr["TRF_AREA_ID"]);
                    _amount = Utility.ToDecimal(dr["TRF_AMOUNT"]);
                    _status  = Utility.ToString(dr["TRF_STATUS"]);
                    _createdBy = Utility.ToInteger(dr["TRF_CREATED_BY"]);
                  
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Load ROS Tariff Master Details. Error: " + ex.Message, "0");
                throw ex;
            }

        }

        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[09];
                paramList[0] = new SqlParameter("@P_TRF_ID", _trfId);
                paramList[1] = new SqlParameter("@P_TRF_CODE", _code);
                paramList[2] = new SqlParameter("@P_TRF_COMPANY_ID", _companyId);
                paramList[3] = new SqlParameter("@P_TRF_AREA_ID", _areaId);
                paramList[4] = new SqlParameter("@P_TRF_AMOUNT", _amount);
                paramList[5] = new SqlParameter("@P_TRF_STATUS", _status);
                paramList[06] = new SqlParameter("@P_CREATED_BY", _createdBy);
                paramList[07] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[07].Direction = ParameterDirection.Output;
                paramList[08] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 100);
                paramList[08].Direction = ParameterDirection.Output;

                int rowAffected = DBGateway.ExecuteNonQuery("P_ROS_TARIFF_MASTER_ADD_UPDATE", paramList);
                  string messageType = Utility.ToString(paramList[07].Value);
                  if (messageType == "E")
                  {
                      string message = Utility.ToString(paramList[08].Value);
                      if (message != string.Empty) throw new Exception(message);
                  }

            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Save ROS Tariff Master Details. Error: " + ex.Message, "0");
                throw ex;
            }
        }

        #region Static Methods
        public static DataTable GetList(RecordStatus recordStatus,ListStatus listStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                paramList[1] = new SqlParameter("@P_LIST_STATUS", Utility.ToInteger(listStatus));
                //if (!string.IsNullOrEmpty(staffType)) paramList[2] = new SqlParameter("@P_STAFFTYPE", staffType);
                return DBGateway.ExecuteQuery("P_ROS_TARIFF_GETLIST", paramList).Tables[0];
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "Failed To Get List Of ROS Tariff Master. ERROR: " + ex.Message, "0");
                throw ex;
            }
        }

       
        #endregion


    }
}
