using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;


namespace CT.Roster
{
    public class ROSTranxHeader
    {

        #region Members

        private long _rm_id;
        //private long _rm_id_ret;
        private string _rm_doc_number;
        private int _rm_empid;
        private string _rm_flightno;
        private string _rm_from_sector;
        private string _rm_to_sector;
        private DateTime _rm_sign_in;
        private DateTime _rm_sign_out;
        private string _rm_duty_hrs;
        private int _rm_from_loc_id;
        private string _rm_from_loc_type;
        private string _rm_from_loc_map;
        private int _rm_to_loc_id;
        private string _rm_to_loc_type;
        private string _rm_to_loc_map;
        private DateTime _rm_date;
        private string _rm_tranx_status;
        private string _rm_status;
        private int _rm_created_by;
        private string _rm_From_Loc_Details;
        private string _rm_TO_Loc_Details;
        private string _rm_staff_id;
        #endregion

        #region
        public long Rm_id
        {
            get { return _rm_id; }
            set { _rm_id = value; }
        }

        public string Rm_doc_number
        {
            get { return _rm_doc_number; }
            set { _rm_doc_number = value; }
        }

        public int Rm_empid
        {
            get { return _rm_empid; }
            set { _rm_empid = value; }

        }
        public string Rm_flightno
        {
            get { return _rm_flightno; }
            set { _rm_flightno = value; }
        }
        public string Rm_from_sector
        {
            get { return _rm_from_sector; }
            set { _rm_from_sector = value; }
        }
        public string Rm_to_sector
        {
            get { return _rm_to_sector; }
            set { _rm_to_sector = value; }
        }
        public DateTime Rm_sign_in
        {
            get { return _rm_sign_in; }
            set { _rm_sign_in = value; }
        }
        public DateTime Rm_sign_out
        {
            get { return _rm_sign_out; }
            set { _rm_sign_out = value; }
        }
        public string Rm_duty_hrs
        {
            get { return _rm_duty_hrs; }
            set { _rm_duty_hrs = value; }
        }
        public int Rm_from_loc_id
        {
            get { return _rm_from_loc_id; }
            set { _rm_from_loc_id = value; }
        }
        public string Rm_from_loc_type
        {
            get { return _rm_from_loc_type; }
            set { _rm_from_loc_type = value; }
        }
        public string Rm_from_loc_map
        {
            get { return _rm_from_loc_map; }
            set { _rm_from_loc_map = value; }
        }
        public int Rm_to_loc_id
        {
            get { return _rm_to_loc_id; }
            set { _rm_to_loc_id = value; }
        }
        public string Rm_to_loc_type
        {
            get { return _rm_to_loc_type; }
            set { _rm_to_loc_type = value; }
        }
        public string Rm_to_loc_map
        {
            get { return _rm_to_loc_map; }
            set { _rm_to_loc_map = value; }
        }
        public DateTime Rm_date
        {
            get { return _rm_date; }
            set { _rm_date = value; }
        }
        public string Rm_tranx_status
        {
            get { return _rm_tranx_status; }
            set { _rm_tranx_status = value; }
        }
        public string Rm_status
        {
            get { return _rm_status; }
            set { _rm_status = value; }
        }
        public int Rm_created_by
        {
            get { return _rm_created_by; }
            set { _rm_created_by = value; }
        }

        public string Rm_From_Loc_Details
        {
            get { return _rm_From_Loc_Details; }
            set { _rm_From_Loc_Details = value; }
        }
        public string Rm_TO_Loc_Details
        {
            get { return _rm_TO_Loc_Details; }
            set { _rm_TO_Loc_Details = value; }
        }

        public string Rm_staff_id
        {
            get { return _rm_staff_id; }
            set { _rm_staff_id = value; }
        }
        #endregion


        public ROSTranxHeader()
        {
            _rm_id = -1;
            DataTable dt = GetDetails(_rm_id);
            //if (dt != null && dt.Rows.Count > 0)
            UpdateBusinessData(dt);
        }
        public ROSTranxHeader(long transactionId)
        {
            try
            {
                DataTable dt = GetDetails(transactionId);
                //if(dt!=null && dt.Rows.Count>0)
                UpdateBusinessData(dt);
            }
            catch { throw; }

        }

        private DataTable GetDetails(long transactionId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                if (transactionId > 0) paramList[0] = new SqlParameter("@P_EMP_ID", transactionId);
                return DBGateway.ExecuteQuery("P_ROS_TRANX_HEADER_GETDATA", paramList).Tables[0];
            }
            catch { throw; }
        }


        private void UpdateBusinessData(DataTable dt)
        {
            try
            {
                if (dt != null)
                {
                    _rm_id = Utility.ToInteger(dt.Rows[0]["RM_ID"]);
                    _rm_doc_number = Utility.ToString(dt.Rows[0]["RM_DOC_NUMBER"]);
                    _rm_empid = Utility.ToInteger(dt.Rows[0]["RM_EMPID"]);
                    _rm_flightno = Utility.ToString(dt.Rows[0]["RM_FLIGHTNO"]);
                    _rm_from_sector = Utility.ToString(dt.Rows[0]["RM_FROM_SECTOR"]);
                    _rm_to_sector = Utility.ToString(dt.Rows[0]["RM_TO_SECTOR"]);
                    _rm_sign_in = Utility.ToDate(dt.Rows[0]["RM_SIGN_IN"]);
                    _rm_sign_out = Utility.ToDate(dt.Rows[0]["RM_SIGN_OUT"]);
                    _rm_duty_hrs = Utility.ToString(dt.Rows[0]["RM_DUTY_HRS"]);
                    _rm_from_loc_id = Utility.ToInteger(dt.Rows[0]["RM_FROM_LOC_ID"]);
                    _rm_from_loc_type = Utility.ToString(dt.Rows[0]["RM_FROM_LOC_TYPE"]);
                    _rm_from_loc_map = Utility.ToString(dt.Rows[0]["RM_FROM_LOC_MAP"]);
                    _rm_to_loc_id = Utility.ToInteger(dt.Rows[0]["RM_TO_LOC_ID"]);
                    _rm_to_loc_type = Utility.ToString(dt.Rows[0]["RM_TO_LOC_TYPE"]);
                    _rm_to_loc_map = Utility.ToString(dt.Rows[0]["RM_TO_LOC_MAP"]);
                    _rm_date = Utility.ToDate(dt.Rows[0]["RM_DATE"]);
                    _rm_status = Utility.ToString(dt.Rows[0]["RM_STATUS"]);
                    _rm_tranx_status = Utility.ToString(dt.Rows[0]["RM_TRANX_STATUS"]);
                    _rm_created_by = Utility.ToInteger(dt.Rows[0]["RM_CREATED_BY"]);
                    _rm_From_Loc_Details = Utility.ToString(dt.Rows[0]["locationFromDetails"]);
                    _rm_TO_Loc_Details = Utility.ToString(dt.Rows[0]["locationToDetails"]);
                    _rm_staff_id = Utility.ToString(dt.Rows[0]["STAFF_ID"]);
                }


            }
            catch { throw; }
        }

        public void Save(SqlTransaction trans, SqlCommand cmd)
        {
            //SqlTransaction trans = null;
            //SqlCommand cmd = null;
            try
            {

                //SqlCommand cmd = DBGateway.CreateCommand(trans);
                //cmd = new SqlCommand();
                //cmd.Connection = DBGateway.CreateConnection();
                //cmd.Connection.Open();
                //trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                //cmd.Transaction = trans;

                SqlParameter[] paramArr = new SqlParameter[24];

                paramArr[0] = new SqlParameter("@P_RM_ID", _rm_id);
                paramArr[1] = new SqlParameter("@P_RM_EMPID", _rm_empid);
                paramArr[2] = new SqlParameter("@P_RM_FLIGHTNO", _rm_flightno);
                paramArr[3] = new SqlParameter("@P_RM_FROM_SECTOR", _rm_from_sector);
                paramArr[4] = new SqlParameter("@P_RM_TO_SECTOR", _rm_to_sector);
                paramArr[5] = new SqlParameter("@P_RM_SIGN_IN", _rm_sign_in);
                paramArr[6] = new SqlParameter("@P_RM_SIGN_OUT", _rm_sign_out);
                paramArr[7] = new SqlParameter("@P_RM_DUTY_HRS", _rm_duty_hrs);
                paramArr[8] = new SqlParameter("@P_RM_FROM_LOC_ID", _rm_from_loc_id);
                paramArr[9] = new SqlParameter("@P_RM_FROM_LOC_TYPE", _rm_from_loc_type);
                paramArr[10] = new SqlParameter("@P_RM_FROM_LOC_MAP", _rm_from_loc_map);
                paramArr[11] = new SqlParameter("@P_RM_FROM_LOC_DTLS", _rm_From_Loc_Details);
                paramArr[12] = new SqlParameter("@P_RM_TO_LOC_ID", _rm_to_loc_id);
                paramArr[13] = new SqlParameter("@P_RM_TO_LOC_TYPE", _rm_to_loc_type);
                paramArr[14] = new SqlParameter("@P_RM_TO_LOC_MAP", _rm_to_loc_map);
                paramArr[15] = new SqlParameter("@P_RM_TO_LOC_DTLS", _rm_TO_Loc_Details);
                paramArr[16] = new SqlParameter("@P_RM_DATE", _rm_date);
                paramArr[17] = new SqlParameter("@P_RM_TRANX_STATUS", _rm_tranx_status);
                paramArr[18] = new SqlParameter("@P_RM_STATUS", _rm_status);

                paramArr[19] = new SqlParameter("@P_RM_CREATED_BY", _rm_created_by);
                SqlParameter paramMsgText1 = new SqlParameter("@P_RM_ID_RET", SqlDbType.BigInt);
                paramMsgText1.Size = 20;
                paramMsgText1.Direction = ParameterDirection.Output;
                paramArr[20] = paramMsgText1;

                paramArr[21] = new SqlParameter("@P_RM_DOC_NUMBER", _rm_doc_number);


                SqlParameter paramMsgType = new SqlParameter("@p_msg_type", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramArr[22] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@p_msg_text", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramArr[23] = paramMsgText;

                DBGateway.ExecuteNonQueryDetails(cmd, "P_ROS_TRANX_HEADER_ADD_UPDATE", paramArr);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));
               


            }
            catch
            {
                trans.Rollback();
                //cmd.Connection.Close();
                throw;
            }

        }

        public static void SaveROSListDetails(List<ROSTranxHeader> rosListDetails)
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {


                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                foreach (ROSTranxHeader rosHdr in rosListDetails)
                {
                    rosHdr.Save(trans, cmd);
                }
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public static DataTable GetEmpList(ListStatus listStatus, RecordStatus recStatus)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", listStatus);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recStatus));
                return DBGateway.ExecuteQuery("p_rosEmpMaster_GetList", paramList).Tables[0];
            }
            catch { throw; }

        }

        public static DataTable GetEmpLocationDetails(int empId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_emp_Id", empId);
                return DBGateway.ExecuteQuery("P_ROS_STAFF_GETDETAILS", paramList).Tables[0];
            }
            catch { throw; }
        }

        public static DataTable GetLocationTypewiseList(string locationType)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_LOC_TYPE", locationType);
                return DBGateway.ExecuteQuery("P_ROS_LOCATION_TYPE_WISE_LIST", paramList).Tables[0];
            }
            catch { throw; }

        }

        public static DataTable GetList(DateTime fromDate, DateTime toDate, int empId, string tripStatus, RecordStatus recStatus,string driverStatus,string direction)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[7];
                paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
                paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
                if (empId > 0) paramList[2] = new SqlParameter("@P_RM_EMPID", empId);
                if (!string.IsNullOrEmpty(tripStatus)) paramList[3] = new SqlParameter("@P_ROS_TRIP_STATUS", tripStatus);
                if (recStatus != RecordStatus.All) paramList[4] = new SqlParameter("@P_REC_STATUS", Utility.ToString((char)recStatus));
                if (!string.IsNullOrEmpty(driverStatus)) paramList[5] = new SqlParameter("@P_ROS_DRIVER_STATUS", driverStatus);
                if (!string.IsNullOrEmpty(direction)) paramList[6] = new SqlParameter("@P_ROS_FROM_LOC_TYPE", direction);

                return DBGateway.ExecuteQuery("P_ROS_TRANX_HEADER_GETLIST", paramList).Tables[0];
            }
            catch { throw; }

        }

        public static void UpdateROSTranxStatus(long rmId, int employeeId, string rosTranxStatus, int driverId, string vehicleNumber, int vehicleId,string driverStatus, long createdBy,
            string editStatus,string flightNo,string fromSector, string toSector,int fromLocId,string fromLocType,string fromLocMap,string fromLocDetails,
                int toLocId,string toLocType,string toLocMap,string toLocDetails,DateTime rmDate,string remarks)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[24];
                paramList[0] = new SqlParameter("@P_RM_ID", rmId);
                paramList[1] = new SqlParameter("@P_RM_EMPID", employeeId);
                paramList[2] = new SqlParameter("@P_RM_TRANX_STATUS", rosTranxStatus);
                paramList[3] = new SqlParameter("@P_RM_CREATED_BY", createdBy);
                paramList[4] = new SqlParameter("@P_DRIVER_ID", driverId);
                paramList[5] = new SqlParameter("@P_ROS_VEHICLE_NO", vehicleNumber);
                paramList[6] = new SqlParameter("@P_ROS_VEHICLE_ID", vehicleId);
                paramList[7] = new SqlParameter("@P_ROS_DRIVER_STATUS", driverStatus);
                SqlParameter paramMsgType = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramList[8] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@P_MSG_TEXT", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramList[9] = paramMsgText;
                paramList[10] = new SqlParameter("@P_EDIT_DETAILS", editStatus);
                if (!string.IsNullOrEmpty(flightNo)) paramList[11] = new SqlParameter("@P_RM_FLIGHTNO", flightNo);
                if (!string.IsNullOrEmpty(fromSector)) paramList[12] = new SqlParameter("@P_RM_FROM_SECTOR", fromSector);
                if (!string.IsNullOrEmpty(toSector)) paramList[13] = new SqlParameter("@P_RM_TO_SECTOR", toSector);
                if (fromLocId > 0) paramList[14] = new SqlParameter("@P_RM_FROM_LOC_ID", fromLocId);
                if (!string.IsNullOrEmpty(fromLocType)) paramList[15] = new SqlParameter("@P_RM_FROM_LOC_TYPE", fromLocType);
                if (!string.IsNullOrEmpty(fromLocMap)) paramList[16] = new SqlParameter("@P_RM_FROM_LOC_MAP", fromLocMap);
                if (!string.IsNullOrEmpty(fromLocDetails)) paramList[17] = new SqlParameter("@P_RM_FROM_LOC_DTLS", fromLocDetails);
                if (toLocId > 0) paramList[18] = new SqlParameter("@P_RM_TO_LOC_ID", toLocId);
                if (!string.IsNullOrEmpty(toLocType)) paramList[19] = new SqlParameter("@P_RM_TO_LOC_TYPE", toLocType);
                if (!string.IsNullOrEmpty(toLocMap)) paramList[20] = new SqlParameter("@P_RM_TO_LOC_MAP", toLocMap);
                if (!string.IsNullOrEmpty(toLocDetails)) paramList[21] = new SqlParameter("@P_RM_TO_LOC_DTLS", toLocDetails);
                if (rmDate!=DateTime.MinValue ) paramList[22] = new SqlParameter("@P_RM_DATE", rmDate);
                if (!string.IsNullOrEmpty(remarks)) paramList[23] = new SqlParameter("@P_ROS_REMARKS", remarks);
                DBGateway.ExecuteNonQuery("P_ROS_TRANX_STATUS_UPDATE", paramList);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));
            }
            catch { throw; }

        }


        public static void DeleteROSTranxHeader(long rmId,int deletedBy)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[24];
                paramList[0] = new SqlParameter("@P_RM_ID", rmId);               
                paramList[1] = new SqlParameter("@P_DELETED_BY", deletedBy);
                 SqlParameter paramMsgType = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramList[8] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@P_MSG_TEXT", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramList[9] = paramMsgText;
                  DBGateway.ExecuteNonQuery("P_ROS_TRANX_DELETE", paramList);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));
            }
            catch { throw; }
        
        
        
        }

       


       
    }

    public class RosterUpload
    {
        #region
        private long _upld_id;
        private long _upld_id_ret;
        private string _upld_doc_number;
        private string _upld_staff_id;
        private string _upld_flightno;
        private string _upld_from_sector;
        private string _upld_to_sector;        
        private string _upld_duty_hrs;       
        private DateTime _upld_date;        
        private DateTime _upld_rm_date;            
        private int _upld_created_by;     
        private string _upld_etd;
            private string _upld_eta;
            private string _upld_file_name;
            private int _home_driver_time;
            private int _airport_driver_time;
        #endregion

        #region
        public long Upld_id
        {
            get { return _upld_id; }
            set { _upld_id = value; }
        }

        public string Upld_doc_number
        {
            get { return _upld_doc_number; }
            set { _upld_doc_number = value; }
        }

        public string Upld_staffId
        {
            get { return _upld_staff_id; }
            set { _upld_staff_id = value; }

        }
        public string Upld_flightno
        {
            get { return _upld_flightno; }
            set { _upld_flightno = value; }
        }
        public string Upld_from_sector
        {
            get { return _upld_from_sector; }
            set { _upld_from_sector = value; }
        }
        public string Upld_to_sector
        {
            get { return _upld_to_sector; }
            set { _upld_to_sector = value; }
        }
        public string  Upld_etd
        {
            get { return _upld_etd; }
            set { _upld_etd = value; }
        }
        public string  Upld_eta
        {
            get { return _upld_eta; }
            set { _upld_eta = value; }
        }
        public string Upld_duty_hrs
        {
            get { return _upld_duty_hrs; }
            set { _upld_duty_hrs = value; }
        }
        
        public DateTime Upld_date
        {
            get { return _upld_date; }
            set { _upld_date = value; }
        }

        public DateTime Upld_rm_date
        {
            get { return _upld_rm_date; }
            set { _upld_rm_date = value; }
        }
     
        public int Upld_created_by
        {
            get { return _upld_created_by; }
            set { _upld_created_by = value; }
        }

        public string Upld_file_name
        {
            get { return _upld_file_name; }
            set { _upld_file_name = value; }
        }
        public int Home_driver_time
        {
            get { return _home_driver_time; }
            set { _home_driver_time = value; }
        }

        public int Airport_driver_time
        {
            get { return _airport_driver_time; }
            set { _airport_driver_time = value; }
        }

        #endregion

        public void SaveUpload(SqlTransaction trans, SqlCommand cmd)
        {
            
            try
            {                               
                SqlParameter[] paramList = new SqlParameter[16];
                paramList[0] = new SqlParameter("@P_UPLD_ID", _upld_id );
                paramList[1] = new SqlParameter("@P_UPLD_DATE", _upld_date);
                paramList[2] = new SqlParameter("@P_UPLD_RM_DOC_NUMBER", _upld_doc_number);
                paramList[3] = new SqlParameter("@P_UPLD_FILE_NAME", _upld_file_name);
                paramList[4] = new SqlParameter("@P_UPLD_STAFF_ID", _upld_staff_id);
                paramList[5] = new SqlParameter("@P_UPLD_FLIGHT_NO", _upld_flightno);
                paramList[6] = new SqlParameter("@P_UPLD_FROM_SECTOR", _upld_from_sector);
                paramList[7] = new SqlParameter("@P_UPLD_TO_SECTOR", _upld_to_sector);
                paramList[8] = new SqlParameter("@P_UPLD_DUTY_HRS", _upld_duty_hrs);
                paramList[9] = new SqlParameter("@P_UPLD_RM_DATE", _upld_rm_date);
                paramList[10] = new SqlParameter("@P_UPLD_ETD", _upld_etd);
                paramList[11] = new SqlParameter("@P_UPLD_ETA", _upld_eta);
                paramList[12] = new SqlParameter("@P_UPLD_CREATED_BY", _upld_created_by);

                SqlParameter paramMsgText1 = new SqlParameter("@P_UPLD_ID_RET", SqlDbType.BigInt);
                paramMsgText1.Size = 20;
                paramMsgText1.Direction = ParameterDirection.Output;
                paramList[13] = paramMsgText1;

                SqlParameter paramMsgType = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramList[14] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@P_MSG_TEXT", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramList[15] = paramMsgText;
                DBGateway.ExecuteNonQuery("P_ROS_ROSTER_UPLOAD_ADD", paramList);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));
                else
                    _upld_id_ret = Utility.ToLong(paramMsgText1.Value);

                 string fromLocType=string.Empty ;
                        string toLocType=string.Empty ;
                            if(_upld_from_sector=="SHJ")
                            {
                                fromLocType ="H";
                                toLocType="A";
                            }
                            else
                            {
                              fromLocType ="A";
                              toLocType="H";
                            }
                            SetPriorTime(Upld_rm_date, fromLocType, _upld_etd, _upld_eta,_home_driver_time ,_airport_driver_time);

                            UploadRosterDetails(_upld_staff_id, _upld_flightno, _upld_from_sector, _upld_to_sector, _upld_duty_hrs, fromLocType, toLocType, _upld_rm_date,
                                _upld_etd, _upld_eta, _upld_created_by, _upld_doc_number, _upld_id_ret);

                           
            }
            catch
            {
               // trans.Rollback();
                //cmd.Connection.Close();
                throw;
            }
           
        }

        public void UploadRosterDetails(string staffId, string flightNo, string fromSector, string toSector, string dutyHours,
              string fromLocType, string toLocType, DateTime rmDate, string etdTime, string etaTime, int createdBy, string DocNumber,long uploadRefId)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[15];
                paramList[0] = new SqlParameter("@P_STAFF_ID", staffId);
                paramList[1] = new SqlParameter("@P_RM_FLIGHTNO", flightNo);
                paramList[2] = new SqlParameter("@P_RM_FROM_SECTOR", fromSector);
                paramList[3] = new SqlParameter("@P_RM_TO_SECTOR", toSector);
                paramList[4] = new SqlParameter("@P_RM_DUTY_HRS", dutyHours);
                paramList[5] = new SqlParameter("@P_RM_FROM_LOC_TYPE", fromLocType);
                paramList[6] = new SqlParameter("@P_RM_TO_LOC_TYPE", toLocType);
                paramList[7] = new SqlParameter("@P_RM_DATE", rmDate);
                paramList[8] = new SqlParameter("@P_ETD", etdTime);
                paramList[9] = new SqlParameter("@P_ETA", etaTime);
                paramList[10] = new SqlParameter("@P_RM_CREATED_BY", createdBy);
                paramList[11] = new SqlParameter("@P_RM_DOC_NUMBER", DocNumber);
                SqlParameter paramMsgType = new SqlParameter("@P_MSG_TYPE", SqlDbType.NVarChar);
                paramMsgType.Size = 10;
                paramMsgType.Direction = ParameterDirection.Output;
                paramList[12] = paramMsgType;

                SqlParameter paramMsgText = new SqlParameter("@P_MSG_TEXT", SqlDbType.NVarChar);
                paramMsgText.Size = 100;
                paramMsgText.Direction = ParameterDirection.Output;
                paramList[13] = paramMsgText;
                paramList[14] = new SqlParameter("@P_UPLD_REF_ID", uploadRefId);
                DBGateway.ExecuteNonQuery("P_ROS_TRANX_HEADER_UPLOAD", paramList);
                if (Utility.ToString(paramMsgType.Value) == "E")
                    throw new Exception(Utility.ToString(paramMsgText.Value));

            }
            catch { throw; }

        }

        private void SetPriorTime(DateTime rmDate, string fromLocType, string etd, string eta, int homeDriverTime, int airportDriverTime)
        {
            try
            {
                if (fromLocType == "H")
                {
                    _upld_rm_date = Utility.ToDate(rmDate.ToString("dd-MM-yyyy") + " " + etd);
                    _upld_rm_date = _upld_rm_date.AddMinutes(homeDriverTime);
                    _upld_etd = _upld_rm_date.ToString("HH:mm");
                }
                else
                {
                    //if(Utility.ToDate(eta,.<=etd
                        DateTime etaTime = DateTime.ParseExact(eta, "HH:mm",System.Globalization.CultureInfo.InvariantCulture);
                        DateTime etdTime = DateTime.ParseExact(etd, "HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                        
                    _upld_rm_date = Utility.ToDate(rmDate.ToString("dd-MM-yyyy") + " " + eta);
                    
                    if (etaTime <= etdTime) //if arrival time is in next day add ing one date to rm_date
                        _upld_rm_date = _upld_rm_date.AddDays(1);

                    _upld_rm_date = _upld_rm_date.AddMinutes(airportDriverTime);
                    _upld_eta= _upld_rm_date.ToString("HH:mm");
                }
            }
            catch { throw; }
        
        }
        public static void SaveRosterUploadList(List<RosterUpload> rosUploadList)
        {
            SqlTransaction trans = null;
            SqlCommand cmd = null;
            try
            {


                cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                foreach (RosterUpload rosupload in rosUploadList)
                {
                    rosupload.SaveUpload(trans, cmd);
                }
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public static DataTable GetROSReconcileDetails(string docNumber, string isMatched)
        {


            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_ROS_DOC_NUMBER", docNumber);
                paramList[1] = new SqlParameter("@P_MATCHED_STATUS", isMatched);
                return DBGateway.ExecuteQuery("P_ROS_GET_MISMATCHED_UPLOAD_LIST", paramList).Tables[0];
            }
            catch { throw; }
        
        }

    }
   
}
