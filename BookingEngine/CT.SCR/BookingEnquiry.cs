﻿using System;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using System.Data;

namespace CT.SCR
{
    public class BookingEnquiry
    {
        #region Fields
        int _EnquiryId;
        string _VoucherNo;
        string _Name;
        string _Email;
        string _Mobile;
        int _PaxCount;
        int _PickupLocationId;
        string _PickupLocationOthers;
        int _DropOffLocationID;
        string _DropOffLocationOthers;
        string _ComingFrom;
        string _FlightNo;
        DateTime _ArrivalDateTime;
        DateTime _PickUpDateTime;
        int _SuitcaseCount;
        string _Remarks;
        decimal _Charges;
        decimal _TotalAmount;
        string _Currency;
        string _VMSreceiptNo;
        int _AllocatedDriverId;
        int _AllocatedVehicleId;
        int _PaymentMode;
        string _Status;
        int _CreatedBy;
        #endregion

        #region Properties

        public int EnquiryId
        {
            get { return _EnquiryId; }
            set { _EnquiryId = value; }
        }
        public string VoucherNo
        {
            get { return _VoucherNo; }
            set { _VoucherNo = value; }
        }
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        public string Mobile
        {

            get { return _Mobile; }
            set { _Mobile = value; }
        }

        public int PaxCount
        {
            get { return _PaxCount; }
            set { _PaxCount = value; }
        }
        public int PickupLocationId
        {
            get { return _PickupLocationId; }
            set { _PickupLocationId = value; }
        }
        public string PickupLocationOthers
        {
            get { return _PickupLocationOthers; }
            set { _PickupLocationOthers = value; }
        }
        public int DropOffLocationID
        {
            get { return _DropOffLocationID; }
            set { _DropOffLocationID = value; }
        }
        public string DropOffLocationOthers
        {
            get { return _DropOffLocationOthers; }
            set { _DropOffLocationOthers = value; }
        }
        public string ComingFrom
        {
            get { return _ComingFrom; }
            set { _ComingFrom = value; }
        }
        public string FlightNo
        {
            get { return _FlightNo; }
            set { _FlightNo = value; }
        }
        public DateTime ArrivalDateTime
        {
            get { return _ArrivalDateTime; }
            set { _ArrivalDateTime = value; }
        }
        public DateTime PickUpDateTime
        {
            get { return _PickUpDateTime; }
            set { _PickUpDateTime = value; }
        }
        public int SuitcaseCount
        {
            get { return _SuitcaseCount; }
            set { _SuitcaseCount = value; }
        }
        public string Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }
        public decimal Charges
        {

            get { return _Charges; }
            set { _Charges = value; }
        }
        public decimal TotalAmount
        {
            get { return _TotalAmount; }
            set { _TotalAmount = value; }
        }
        public string Currency
        {
            get { return _Currency; }
            set { _Currency = value; }
        }
        public string VMSreceiptNo
        {
            get { return _VMSreceiptNo; }
            set { _VMSreceiptNo = value; }
        }
        public int AllocatedDriverId
        {
            get { return _AllocatedDriverId; }
            set { _AllocatedDriverId = value; }
        }
        public int AllocatedVehicleId
        {
            get { return _AllocatedVehicleId; }
            set { _AllocatedVehicleId = value; }
        }
        public int PaymentMode
        {
            get { return _PaymentMode; }
            set { _PaymentMode = value; }
        }
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        public int CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }

        #endregion

        #region Constructor
        public BookingEnquiry()
        {
        }
        public BookingEnquiry(int enqId)
        {
            try
            {
                DataSet ds = GetData(enqId);
                UpdateBusinessData(ds);
            }
            catch
            {
                throw;
            }
        }
        private DataSet GetData(int enqId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_EnqId", enqId);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_GetSCRBookEnQData", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataSet ds)
        {
            try
            {
                if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    UpdateBusinessData(ds.Tables[0].Rows[0]);
                }
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                if (dr["EnquiryId"] != DBNull.Value)
                {
                    _EnquiryId = Convert.ToInt32(dr["EnquiryId"]);
                }
                if (dr["VoucherNo"] != DBNull.Value)
                {
                    _VoucherNo = Convert.ToString(dr["VoucherNo"]);
                }
                if (dr["Name"] != DBNull.Value)
                {
                    _Name = Convert.ToString(dr["Name"]);
                }
                if (dr["Email"] != DBNull.Value)
                {
                    _Email = Convert.ToString(dr["Email"]);
                }
                if (dr["Mobile"] != DBNull.Value)
                {
                    _Mobile = Convert.ToString(dr["Mobile"]);
                }
                if (dr["PaxCount"] != DBNull.Value)
                {
                    _PaxCount = Convert.ToInt32(dr["PaxCount"]);
                }
                if (dr["PickupLocationId"] != DBNull.Value)
                {
                    _PickupLocationId = Convert.ToInt32(dr["PickupLocationId"]);
                }
                if (dr["PickupLocationOthers"] != DBNull.Value)
                {
                    _PickupLocationOthers = Convert.ToString(dr["PickupLocationOthers"]);
                }
                if (dr["DropOffLocationID"] != DBNull.Value)
                {
                    _DropOffLocationID = Convert.ToInt32(dr["DropOffLocationID"]);
                }
                if (dr["DropOffLocationOthers"] != DBNull.Value)
                {
                    _DropOffLocationOthers = Convert.ToString(dr["DropOffLocationOthers"]);
                }
                if (dr["ComingFrom"] != DBNull.Value)
                {
                    _ComingFrom = Convert.ToString(dr["ComingFrom"]);
                }
                if (dr["FlightNo"] != DBNull.Value)
                {
                    _FlightNo = Convert.ToString(dr["FlightNo"]);
                }
                if (dr["ArrivalDateTime"] != DBNull.Value)
                {
                    _ArrivalDateTime = Convert.ToDateTime(dr["ArrivalDateTime"]);
                }
                if (dr["PickUpDateTime"] != DBNull.Value)
                {
                    _PickUpDateTime = Convert.ToDateTime(dr["PickUpDateTime"]);
                }
                if (dr["SuitcaseCount"] != DBNull.Value)
                {
                    _SuitcaseCount = Convert.ToInt32(dr["SuitcaseCount"]);
                }
                if (dr["Remarks"] != DBNull.Value)
                {
                    _Remarks = Convert.ToString(dr["Remarks"]);
                }
                if (dr["Charges"] != DBNull.Value)
                {
                    _Charges = Convert.ToDecimal(dr["Charges"]);
                }
                if (dr["TotalAmount"] != DBNull.Value)
                {
                    _TotalAmount = Convert.ToDecimal(dr["TotalAmount"]);
                }
                if (dr["Currency"] != DBNull.Value)
                {
                    _Currency = Convert.ToString(dr["Currency"]);
                }
                if (dr["VMSreceiptNo"] != DBNull.Value)
                {
                    _VMSreceiptNo = Convert.ToString(dr["VMSreceiptNo"]);
                }
                if (dr["AllocatedDriverId"] != DBNull.Value)
                {
                    _AllocatedDriverId = Convert.ToInt32(dr["AllocatedDriverId"]);
                }
                if (dr["AllocatedVehicleId"] != DBNull.Value)
                {
                    _AllocatedVehicleId = Convert.ToInt32(dr["AllocatedVehicleId"]);
                }
                if (dr["PaymentMode"] != DBNull.Value)
                {
                    _PaymentMode = Convert.ToInt32(dr["PaymentMode"]);
                }
                if (dr["Status"] != DBNull.Value)
                {
                    _Status = Convert.ToString(dr["Status"]);
                }
                if (dr["CreatedBy"] != DBNull.Value)
                {
                    _CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                }

            }
            catch
            {
                throw;
            }
        }


        #endregion

        public static DataTable GetTariff(int pickupLocation, int dropOffLocation)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_pickupLocation", pickupLocation);
                paramList[1] = new SqlParameter("@P_dropOffLocation", dropOffLocation);
                DataTable dtTariiff = DBGateway.FillDataTableSP("usp_GetSCRBookEnqTariff", paramList);
                return dtTariiff;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable GetPickupDropoffLocations(string LocationStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_LocationStatus", LocationStatus);
                // paramList[1] = new SqlParameter("@P_LocationType", LocationType);
                DataTable dtLocations = DBGateway.FillDataTableSP("usp_GetSCRBookEnqLocations", paramList);
                return dtLocations;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetCityName(int cityId)
        {
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@cityId", cityId);
            string cityName = string.Empty;
            SqlDataReader dataReader = null;
            try
            {
                dataReader = DBGateway.ExecuteReaderSP("usp_GetSCRCityName", paramList, connection);
                if (dataReader.Read())
                {
                    cityName = (string)dataReader["loc_name"];
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
                connection.Close();
            }
            return cityName;
        }

        public static DataTable GetBookingEnquiryRecords(DateTime fromDate, DateTime toDate,string bookingtype,string vehicletype)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@P_From_Date", fromDate);
                paramList[1] = new SqlParameter("@P_To_Date", toDate);
                paramList[2] = new SqlParameter("@P_bookingType",bookingtype);
                paramList[3] = new SqlParameter("@P_VehicleType", vehicletype);
                DataTable dtTariiff = DBGateway.FillDataTableSP("usp_GetSCRBookEnqList", paramList);
                return dtTariiff;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable GetDriverDetails(int listStatus, string recordStatus,string staffType)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", listStatus);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                paramList[2] = new SqlParameter("@P_STAFFTYPE", staffType);
                DataTable dtDrivers = DBGateway.FillDataTableSP("P_ROS_EmpMasterGetList", paramList);
                return dtDrivers;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetVehicleType(int listStatus, string recordStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", listStatus);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                DataTable dtVehicles = DBGateway.FillDataTableSP("P_ROS_VehicleMasterGetList", paramList);
                return dtVehicles;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdatBookingEnqStatus(int enqId, string txtVMSRef,int driverId,int vehicleid,string bookingStatus,int userId,string vehNumber)
        {
            try
            {
                SqlParameter[] paramArr = new SqlParameter[7];
                paramArr[0] = new SqlParameter("@P_Enq_ID", enqId);
                paramArr[1] = new SqlParameter("@P_VMS_Ref", txtVMSRef);
                paramArr[2] = new SqlParameter("@P_Driver_Id", driverId);
                paramArr[3] = new SqlParameter("@P_Vehicle_Id", vehicleid);
                paramArr[4] = new SqlParameter("@P_Book_Status", bookingStatus);
                paramArr[5] = new SqlParameter("@P_Modified_By", userId);
                paramArr[6] = new SqlParameter("@P_Veh_Numb", vehNumber);
                DBGateway.ExecuteNonQuery("usp_UpdateSCRBookStatus", paramArr);
            }
            catch { throw; }
        }


    }
}
