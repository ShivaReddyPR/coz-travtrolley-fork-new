using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
//using Technology.Indiatimes;

namespace CT.BookingEngine.WhiteLabel
{
    public struct PendingQueuePaxDetail
    {
        public string Title;
        public string FirstName;
        public string LastName;
        public string Type;
        public string AddressLine1;
        public string AddressLine2;
        public string Phone;
        public string Country;
        public string Email;
        public DateTime DOB;
    }
    public class WLPendingQueue
    {
        # region Private Fields
        int queueId;
        int bookingIdOut;
        int bookingIdIn;
        string outPNR;
        string inPNR;
        string paymentId;
        PaymentGatewaySource paySource;
        string sector;
        string passengerInfo;
        string ipAddress;
        string email;
        string phone;
        WLBookingStatus bookingStatus;
        WLBookingHistory history;
        List<PendingQueuePaxDetail> paxList;
        bool isDomesticReturn;
        decimal paymentAmount;
        bool outTicketed;
        bool inTicketed;
        int agencyId;        
        string airlineCodeOut;
        string airlineCodeIn;
        string flightNumberOut;
        string flightNumberIn;
        DateTime depDate;
        DateTime returnDate;
        string remarks;
        DateTime createdOn;
        int createdBy;
        DateTime lastModifiedOn;
        int lastModifiedBy;
        bool isDomestic;
        PaymentStatus paymentStatus;
        string orderId;
        # endregion

        # region Public Properties

        public int QueueId
        {
            get
            {
                return queueId;
            }
            set
            {
                queueId = value;
            }
        }

        public int BookingIdOut
        {
            get
            {
                return bookingIdOut;
            }
            set
            {
                bookingIdOut = value;
            }
        }

        public int BookingIdIn
        {
            get
            {
                return bookingIdIn;
            }
            set
            {
                bookingIdIn = value;
            }
        }

        public string OutPNR
        {
            get
            {
                return outPNR;
            }
            set
            {
                outPNR = value;
            }
        }

        public string InPNR
        {
            get
            {
                return inPNR;
            }
            set
            {
                inPNR = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        public string PaymentId
        {
            get
            {
                return paymentId;
            }
            set
            {
                paymentId = value;
            }
        }

        public PaymentGatewaySource PaySource
        {
            get
            {
                return paySource;
            }
            set
            {
                paySource = value;
            }
        }

        public string Sector
        {
            get
            {
                return sector;
            }
            set
            {
                sector = value;
            }
        }

        public string PassengerInfo
        {
            get
            {
                return passengerInfo;
            }
            set
            {
                passengerInfo = value;
            }
        }

        public string IPAddress
        {
            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
            }
        }

        public WLBookingStatus BookingStatus
        {
            get
            {
                return bookingStatus;
            }
            set
            {
                bookingStatus = value;
            }
        }

        public WLBookingHistory History
        {
            get
            {
                return history;
            }
            set
            {
                history = value;
            }
        }

        public List<PendingQueuePaxDetail> PaxList
        {
            get
            {
                return paxList;
            }
            set
            {
                paxList = value;
            }
        }

        public bool IsDomesticReturn
        {
            get
            {
                return isDomesticReturn;
            }
            set
            {
                isDomesticReturn = value;
            }
        }

        public decimal PaymentAmount
        {
            set
            {
                paymentAmount = value;
            }
            get
            {
                return paymentAmount;
            }
        }

        public bool OutTicketed
        {
            set
            {
                outTicketed = value;
            }
            get
            {
                return outTicketed;
            }
        }

        public bool InTicketed
        {
            set
            {
                inTicketed = value;
            }
            get
            {
                return inTicketed;
            }
        }

        public int AgencyId
        {
            set
            {
                agencyId = value;
            }
            get
            {
                return agencyId;
            }
        }
                
        public string AirlineCodeOut
        {
            set
            {
                airlineCodeOut = value;
            }
            get
            {
                return airlineCodeOut;
            }
        }

        public string AirlineCodeIn
        {
            set
            {
                airlineCodeIn = value;
            }
            get
            {
                return airlineCodeIn;
            }
        }

        public string FlightNumberOut
        {
            set
            {
                flightNumberOut = value;
            }
            get
            {
                return flightNumberOut;
            }
        }

        public string FlightNumberIn
        {
            set
            {
                flightNumberIn = value;
            }
            get
            {
                return flightNumberIn;
            }
        }

        public DateTime DepDate
        {
            set
            {
                depDate = value;
            }
            get
            {
                return depDate;
            }
        }

        public DateTime ReturnDate
        {
            set
            {
                returnDate = value;
            }
            get
            {
                return returnDate;
            }
        }

        public string Remarks
        {
            set
            {
                remarks = value;
            }
            get
            {
                return remarks;
            }
        }

        public DateTime LastModifiedOn
        {
            set
            {
                lastModifiedOn = value;
            }
            get
            {
                return lastModifiedOn;
            }
        }

        public int CreatedBy
        {
            set
            {
                createdBy = value;
            }
            get
            {
                return createdBy;
            }
        }

        public int LastModifiedBy
        {
            set
            {
                lastModifiedBy = value;
            }
            get
            {
                return lastModifiedBy;
            }
        }

        public bool IsDomestic
        {
            set
            {
                isDomestic = value;
            }
            get
            {
                return isDomestic;
            }
        }

        public PaymentStatus PaymentStatus
        {
            set
            {
                paymentStatus = value;
            }
            get
            {
                return paymentStatus;
            }
        }

        public string OrderId
        {
            set
            {
                orderId = value;
            }
            get
            {
                return orderId;
            }
        }

        #endregion

        /// <summary>
        /// Loads queues in Pending Queue panel according to filter set.
        /// </summary>
        /// <param name="pageNo">Page No</param>
        /// <param name="whereString">Where String (Filter String)</param>
        /// <returns>List containing pending queue.</returns>
        public static List<WLPendingQueue> Load(int pageNo, string whereString)
        {
            //Trace.TraceInformation("ITimesBookingDetails.Load entered : page number = " + pageNo);
            List<WLPendingQueue> tempList = new List<WLPendingQueue>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@rowNo", pageNo * 100);
            paramList[1] = new SqlParameter("@whereString", whereString);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetITimesPendingQueue, paramList, connection);
            if (data.HasRows)
            {
                while (data.Read())
                {
                    WLPendingQueue itbd = new WLPendingQueue();
                    itbd.queueId = Convert.ToInt32(data["queueId"]);
                    if (data["bookingIdOut"] != DBNull.Value)
                    {
                        itbd.bookingIdOut = Convert.ToInt32(data["bookingIdOut"]);
                    }
                    else
                    {
                        itbd.bookingIdOut = 0;
                    }
                    if (data["bookingIdIn"] != DBNull.Value)
                    {
                        itbd.bookingIdIn = Convert.ToInt32(data["bookingIdIn"]);
                    }
                    else
                    {
                        itbd.bookingIdIn = 0;
                    }
                    if (data["outPNR"] != DBNull.Value)
                    {
                        itbd.outPNR = Convert.ToString(data["outPNR"]);
                    }
                    else
                    {
                        itbd.outPNR = string.Empty;
                    }
                    if (data["inPNR"] != DBNull.Value)
                    {
                        itbd.inPNR = Convert.ToString(data["inPNR"]);
                    }
                    else
                    {
                        itbd.inPNR = string.Empty;
                    }
                    if (data["paymentId"] != DBNull.Value)
                    {
                        itbd.paymentId = Convert.ToString(data["paymentId"]);
                    }
                    else
                    {
                        itbd.paymentId = string.Empty;
                    }
                    if (data["orderId"] != DBNull.Value)
                    {
                        itbd.OrderId = Convert.ToString(data["orderId"]);
                    }
                    else
                    {
                        itbd.OrderId = string.Empty;
                    }
                    itbd.paySource = (PaymentGatewaySource)(Enum.Parse(typeof(PaymentGatewaySource), data["paymentGatewaySourceId"].ToString()));
                    if (data["sector"] != DBNull.Value)
                    {
                        itbd.sector = Convert.ToString(data["sector"]);
                    }
                    else
                    {
                        itbd.sector = string.Empty;
                    }
                    if (data["passengerInfo"] != null)
                    {
                        itbd.passengerInfo = Convert.ToString(data["passengerInfo"]);
                    }
                    else
                    {
                        itbd.passengerInfo = string.Empty;
                    }
                    if (data["ipAddress"] != DBNull.Value)
                    {
                        itbd.ipAddress = Convert.ToString(data["ipAddress"]);
                    }
                    else
                    {
                        itbd.ipAddress = string.Empty;
                    }
                    if (data["email"] != DBNull.Value)
                    {
                        itbd.email = Convert.ToString(data["email"]);
                    }
                    else
                    {
                        itbd.email = string.Empty;
                    }
                    if (data["phone"] != DBNull.Value)
                    {
                        itbd.phone = Convert.ToString(data["phone"]);
                    }
                    else
                    {
                        itbd.phone = string.Empty;
                    }
                    itbd.bookingStatus = (WLBookingStatus)(Enum.Parse(typeof(WLBookingStatus), data["bookingStatus"].ToString()));
                    if (data["isDomesticReturn"] != DBNull.Value)
                    {
                        itbd.isDomesticReturn = Convert.ToBoolean(data["isDomesticReturn"]);
                    }
                    itbd.createdOn = Convert.ToDateTime(data["createdOn"]);
                    if (data["paymentAmount"] != DBNull.Value)
                    {
                        itbd.paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                    }
                    else
                    {
                        itbd.paymentAmount = 0;
                    }
                    itbd.outTicketed = Convert.ToBoolean(data["outTicketed"]);
                    if (data["inTicketed"] != DBNull.Value)
                    {
                        itbd.inTicketed = Convert.ToBoolean(data["inTicketed"]);
                    }
                    itbd.agencyId = Convert.ToInt32(data["agencyId"]);                    
                    if (data["airlineCodeIn"] != DBNull.Value)
                    {
                        itbd.airlineCodeIn = Convert.ToString(data["airlineCodeIn"]);
                    }
                    itbd.airlineCodeOut = Convert.ToString(data["airlineCodeOut"]);
                    if (data["flightNumberIn"] != DBNull.Value)
                    {
                        itbd.flightNumberIn = Convert.ToString(data["flightNumberIn"]);
                    }
                    itbd.flightNumberOut = Convert.ToString(data["flightNumberOut"]);
                    itbd.depDate = Convert.ToDateTime(data["depDate"]);
                    if (data["returnDate"] != DBNull.Value)
                    {
                        itbd.returnDate = Convert.ToDateTime(data["returnDate"]);
                    }
                    if (data["remarks"] != DBNull.Value)
                    {
                        itbd.remarks = Convert.ToString(data["remarks"]);
                    }
                    else
                    {
                        itbd.remarks = string.Empty;
                    }
                    itbd.isDomestic = Convert.ToBoolean(data["isDomestic"]);
                    itbd.PaymentStatus = (PaymentStatus)(Enum.Parse(typeof(PaymentStatus), data["paymentStatus"].ToString()));
                    itbd.createdBy = Convert.ToInt32(data["createdBy"]);
                    itbd.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    itbd.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                    tempList.Add(itbd);
                }
            }
            data.Close();
            connection.Close();
            return tempList;
        }

        /// <summary>
        /// Gets number of rows present in DB according to filter set. Required for paging.
        /// </summary>
        /// <param name="whereString">where String. (Filters)</param>
        /// <returns>Number of rows present in DB according to filters</returns>
        public static int GetRowCount(string whereString)
        {
            //Trace.TraceInformation("ITimesBookingDetails.GetRowCount entered");
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@whereString", whereString);

            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetITimesPendingQueueCount, paramList, connection);
            int numberOfRows = 0;
            if (data.Read())
            {
                numberOfRows = Convert.ToInt32(data["numberOfRows"]);
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("ITimesBookingDetails.GetRowCount exited. Number of rows = " + numberOfRows);
            return numberOfRows;
        }
        
        /// <summary>
        /// Creates a where string according to filters set.
        /// </summary>
        /// <param name="stringFilter">Drop down filter is selected or not.</param>
        /// <param name="type">Value of selected dropdown.</param>
        /// <param name="value">Value in text box.</param>
        /// <param name="agencyId">agency id object</param>
        /// <returns>Where string according to filters.</returns>
        public static string GetWhereString(bool stringFilter, string type, string value, object agencyId)
        {
            string whereString = string.Empty;
            if (stringFilter)
            {
                if (value != null)
                {
                    value = value.Replace("'", "''");
                }
                if (type == "Queue")
                {
                    whereString += " where pq.QueueId = " + value.Trim();
                }                
                if (type == "Email")
                {
                    whereString += " where pq.email like '" + value.Trim().ToLower() + "%'";
                }
                if (type == "IP")
                {
                    whereString += " where pq.ipAddress = '" + value.Trim() + "'";
                }
                if (type == "PNR")
                {
                    whereString += " where pq.outPNR = '" + value.Trim() + "' or pq.inPNR = '" + value.Trim() + "'";
                }
                if (type == "PaymentId")
                {
                    whereString += " where pq.paymentId = '" + value.Trim() + "'";
                }
                if (type == "OrderId")
                {
                    whereString += " where pq.orderId = '" + value.Trim() + "'";
                }
                if (type == "Moved")
                {
                    whereString = " where pq.bookingStatus = 7";
                }
                if (type == "Released")
                {
                    whereString = " where pq.bookingStatus = 5";
                }
                if (type == "Rejected")
                {
                    whereString = " where pq.bookingStatus = 3";
                }
            }
            else
            {
                whereString = " where pq.bookingStatus NOT IN (3,5,6,7,8,9)";
            }

            if (agencyId != null && (int)agencyId > 0 && whereString.Length > 4)
            {
                whereString += " AND AgencyId = " + ((int)agencyId).ToString();
            }
            else if (agencyId != null && (int)agencyId > 0 && whereString.Length < 4)
            {
                whereString += " Where AgencyId = " + ((int)agencyId).ToString();
            }

            return whereString;
        }

        /// <summary>
        /// Loads details of a single queue according to queue id.
        /// </summary>
        /// <param name="queueId">Queue Id</param>
        /// <returns>Details of a single queue according to queue id.</returns>
        public static WLPendingQueue GetTrip(int queueId)
        {
            //Trace.TraceInformation("ITimesBookingDetails.GetTrip entered : queueId = " + queueId);
            WLPendingQueue itbd = new WLPendingQueue();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@queueId", queueId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetPendingQueueDetailByQueueId, paramList, connection);
            if (data.Read())
            {
                //ITimesBookingDetail itbd = new ITimesBookingDetail();
                itbd.queueId = Convert.ToInt32(data["queueId"]);
                if (data["bookingIdOut"] != DBNull.Value)
                {
                    itbd.bookingIdOut = Convert.ToInt32(data["bookingIdOut"]);
                }
                else
                {
                    itbd.bookingIdOut = 0;
                }
                if (data["bookingIdIn"] != DBNull.Value)
                {
                    itbd.bookingIdIn = Convert.ToInt32(data["bookingIdIn"]);
                }
                else
                {
                    itbd.bookingIdIn = 0;
                }
                if (data["outPNR"] != DBNull.Value)
                {
                    itbd.outPNR = Convert.ToString(data["outPNR"]);
                }
                else
                {
                    itbd.outPNR = string.Empty;
                }
                if (data["inPNR"] != DBNull.Value)
                {
                    itbd.inPNR = Convert.ToString(data["inPNR"]);
                }
                else
                {
                    itbd.inPNR = string.Empty;
                }
                if (data["paymentId"] != DBNull.Value)
                {
                    itbd.paymentId = Convert.ToString(data["paymentId"]);
                }
                else
                {
                    itbd.paymentId = string.Empty;
                }
                itbd.paySource = (PaymentGatewaySource)(Enum.Parse(typeof(PaymentGatewaySource), data["paymentGatewaySourceId"].ToString()));
                if (data["sector"] != DBNull.Value)
                {
                    itbd.sector = Convert.ToString(data["sector"]);
                }
                else
                {
                    itbd.sector = string.Empty;
                }
                if (data["passengerInfo"] != null)
                {
                    itbd.passengerInfo = Convert.ToString(data["passengerInfo"]);
                }
                else
                {
                    itbd.passengerInfo = string.Empty;
                }
                if (data["ipAddress"] != DBNull.Value)
                {
                    itbd.ipAddress = Convert.ToString(data["ipAddress"]);
                }
                else
                {
                    itbd.ipAddress = string.Empty;
                }
                if (data["email"] != DBNull.Value)
                {
                    itbd.email = Convert.ToString(data["email"]);
                }
                else
                {
                    itbd.email = string.Empty;
                }
                if (data["phone"] != DBNull.Value)
                {
                    itbd.phone = Convert.ToString(data["phone"]);
                }
                else
                {
                    itbd.phone = string.Empty;
                }
                itbd.bookingStatus = (WLBookingStatus)(Enum.Parse(typeof(WLBookingStatus), data["bookingStatus"].ToString()));
                if (data["isDomesticReturn"] != DBNull.Value)
                {
                    itbd.isDomesticReturn = Convert.ToBoolean(data["isDomesticReturn"]);
                }
                itbd.createdOn = Convert.ToDateTime(data["createdOn"]);
                if (data["paymentAmount"] != DBNull.Value)
                {
                    itbd.paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                }
                else
                {
                    itbd.paymentAmount = 0;
                }
                itbd.outTicketed = Convert.ToBoolean(data["outTicketed"]);
                if (data["inTicketed"] != DBNull.Value)
                {
                    itbd.inTicketed = Convert.ToBoolean(data["inTicketed"]);
                }
                itbd.agencyId = Convert.ToInt32(data["agencyId"]);
                if (data["airlineCodeIn"] != DBNull.Value)
                {
                    itbd.airlineCodeIn = Convert.ToString(data["airlineCodeIn"]);
                }
                itbd.airlineCodeOut = Convert.ToString(data["airlineCodeOut"]);
                if (data["flightNumberIn"] != DBNull.Value)
                {
                    itbd.flightNumberIn = Convert.ToString(data["flightNumberIn"]);
                }
                itbd.flightNumberOut = Convert.ToString(data["flightNumberOut"]);
                itbd.depDate = Convert.ToDateTime(data["depDate"]);
                if (data["returnDate"] != DBNull.Value)
                {
                    itbd.returnDate = Convert.ToDateTime(data["returnDate"]);
                }
                if (data["remarks"] != DBNull.Value)
                {
                    itbd.remarks = Convert.ToString(data["remarks"]);
                }
                else
                {
                    itbd.remarks = string.Empty;
                }
                if (data["orderId"] != DBNull.Value)
                {
                    itbd.OrderId = Convert.ToString(data["orderId"]);
                }
                else
                {
                    itbd.OrderId = string.Empty;
                }
                itbd.isDomestic= Convert.ToBoolean(data["isDomestic"]);
                itbd.PaymentStatus = (PaymentStatus)(Enum.Parse(typeof(PaymentStatus), data["paymentStatus"].ToString()));
                itbd.createdBy = Convert.ToInt32(data["createdBy"]);
                itbd.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                itbd.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
            }
            else
            {
                Audit.Add(EventType.IndiaTimesBookingDetailsTripSearch, Severity.High, 0, "Error: Unable to find the Trip ID: " + queueId + " | " + DateTime.Now, "");
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("ITimesBookingDetails.GetTrip exited : tripId = " + queueId);
            return itbd;
        }

        /// <summary>
        /// Updates status of a specific queue on which action is taken.
        /// </summary>
        /// <param name="queueId">Queue Id</param>
        /// <param name="bookingStatus">WLBookingStatus Booking Status</param>
        /// <returns>Number of rows affected.</returns>
        public static int UpdateBookingStatus(int queueId, WLBookingStatus bookingStatus)
        {
            if (queueId <= 0)
            {
                throw new ArgumentException("QueueId Cannot be less than 0.", "queueId");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@queueId", queueId);
            paramList[1] = new SqlParameter("@bookingStatus", (int)bookingStatus);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateITimesPendingQueueStatus, paramList);
            return rowsAffected;
        }

        /// <summary>
        /// Saves or updates value of a Queue in Pending Queue.
        /// </summary>
        /// <returns>Queue ID in case of save and number of rows affected in case of update.</returns>
        public int Save()
        {
            if (queueId == 0)
            {
                SqlParameter[] paramList = new SqlParameter[29];

                paramList[0] = new SqlParameter("@paymentGatewaySourceId", (int)(paySource));
                paramList[1] = new SqlParameter("@paymentId", paymentId);
                paramList[2] = new SqlParameter("@paymentAmount", paymentAmount);
                paramList[3] = new SqlParameter("@isDomesticReturn", isDomesticReturn);
                if (bookingIdOut == 0)
                {
                    paramList[4] = new SqlParameter("@bookingIdOut", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@bookingIdOut", bookingIdOut);
                }
                if (bookingIdIn == 0)
                {
                    paramList[5] = new SqlParameter("@bookingIdIn", DBNull.Value);
                }
                else
                {
                    paramList[5] = new SqlParameter("@bookingIdIn", bookingIdIn);
                }
                paramList[6] = new SqlParameter("@outTicketed", outTicketed);
                paramList[7] = new SqlParameter("@inTicketed", inTicketed);
                paramList[8] = new SqlParameter("@agencyId", agencyId);
                paramList[9] = new SqlParameter("@airlineCodeOut", airlineCodeOut);
                if (airlineCodeIn == null)
                {
                    paramList[10] = new SqlParameter("@airlineCodeIn", DBNull.Value);
                }
                else
                {
                    paramList[10] = new SqlParameter("@airlineCodeIn", airlineCodeIn);
                }
                paramList[11] = new SqlParameter("@sector", sector);
                paramList[12] = new SqlParameter("@ipAddress", ipAddress);
                paramList[13] = new SqlParameter("@flightNumberOut", flightNumberOut);
                if (flightNumberIn == null)
                {
                    paramList[14] = new SqlParameter("@flightNumberIn", DBNull.Value);
                }
                else
                {
                    paramList[14] = new SqlParameter("@flightNumberIn", flightNumberIn);
                }
                paramList[15] = new SqlParameter("@passengerInfo", passengerInfo);
                paramList[16] = new SqlParameter("@depDate", depDate);
                if (returnDate.ToString() == "1/1/0001 12:00:00 AM")
                {
                    paramList[17] = new SqlParameter("@returnDate", DBNull.Value);
                }
                else
                {
                    paramList[17] = new SqlParameter("@returnDate", returnDate);
                }
                paramList[18] = new SqlParameter("@queueId", SqlDbType.Int);
                paramList[18].Direction = ParameterDirection.Output;
                paramList[19] = new SqlParameter("@bookingStatus", bookingStatus);
                if (inPNR == null)
                {
                    paramList[20] = new SqlParameter("@inPNR", DBNull.Value);
                }
                else
                {
                    paramList[20] = new SqlParameter("@inPNR", inPNR);
                }
                if (outPNR == null)
                {
                    paramList[21] = new SqlParameter("@outPNR", DBNull.Value);
                }
                else
                {
                    paramList[21] = new SqlParameter("@outPNR", outPNR);
                }
                paramList[22] = new SqlParameter("@createdBy", createdBy);
                if (remarks == null || remarks == "")
                {
                    paramList[23] = new SqlParameter("@remarks", DBNull.Value);
                }
                else
                {
                    paramList[23] = new SqlParameter("@remarks", remarks);
                }
                paramList[24] = new SqlParameter("@email", email);
                if (phone == null || phone == "")
                {
                    paramList[25] = new SqlParameter("@phone", DBNull.Value);
                }
                else
                {
                    paramList[25] = new SqlParameter("@phone", phone);
                }
                paramList[26] = new SqlParameter("@isDomestic", isDomestic);
                paramList[27] = new SqlParameter("@orderId", orderId);
                paramList[28] = new SqlParameter("@paymentStatus", (int)paymentStatus);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddITimesPendingQueueDetails, paramList);
                if (rowsAffected > 0)
                {
                    //Trace.TraceInformation("ITimesBookingDetails.Save exited. New booking saved. Trip ID = " + (int)paramList[18].Value);
                    return (int)paramList[18].Value;
                }
                else
                {
                    //Trace.TraceInformation("ITimesBookingDetails.Save exited. Booking not saved.");
                    return 0;
                }
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[12];

                if (bookingIdOut == 0)
                {
                    paramList[0] = new SqlParameter("@bookingIdOut", DBNull.Value);
                }
                else
                {
                    paramList[0] = new SqlParameter("@bookingIdOut", bookingIdOut);
                }
                if (bookingIdIn == 0)
                {
                    paramList[1] = new SqlParameter("@bookingIdIn", DBNull.Value);
                }
                else
                {
                    paramList[1] = new SqlParameter("@bookingIdIn", bookingIdIn);
                }
                paramList[2] = new SqlParameter("@bookingStatus", (int)bookingStatus);
                if (inPNR == null || inPNR == "")
                {
                    paramList[3] = new SqlParameter("@inPNR", DBNull.Value);
                }
                else
                {
                    paramList[3] = new SqlParameter("@inPNR", inPNR);
                }
                if (outPNR == null || outPNR == "")
                {
                    paramList[4] = new SqlParameter("@outPNR", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@outPNR", outPNR);
                }
                if (remarks == null || remarks == "")
                {
                    paramList[5] = new SqlParameter("@remarks", DBNull.Value);
                }
                else
                {
                    paramList[5] = new SqlParameter("@remarks", remarks);
                }
                paramList[6] = new SqlParameter("@queueId", queueId);
                if (paymentId == null || paymentId == "")
                {
                    paramList[7] = new SqlParameter("@paymentId", DBNull.Value);
                }
                else
                {
                    paramList[7] = new SqlParameter("@paymentId", paymentId);
                }

                if (orderId == null || orderId == "")
                {
                    paramList[8] = new SqlParameter("@orderId", DBNull.Value);
                }
                else
                {
                    paramList[8] = new SqlParameter("@orderId", orderId);
                }

                paramList[9] = new SqlParameter("@outTicketed", outTicketed);
                paramList[10] = new SqlParameter("@InTicketed", InTicketed);
                paramList[11] = new SqlParameter("@paymentStatus", (int)paymentStatus);

                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateITimesPendingQueueDetails, paramList);
                if (rowsAffected > 0)
                {
                    return rowsAffected;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
