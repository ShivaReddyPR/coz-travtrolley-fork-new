using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine.WhiteLabel
{
    public class IPAddressDetails
    {
        # region Private Fields

        string ipAddress;
        string countryCode;
        string countryName;
        string region;
        string city;
        float latitude;
        float longitude;
        string zipcode;
        string ispName;
        string domainName;
        IPAddressStatus ipAddressStatus;
        DateTime createdOn;
        int createdBy;
        DateTime lastModifiedOn;
        int lastModifiedBy;

        # endregion

        # region Public Properties

        public string IPAddress
        {
            set
            {
                ipAddress = value;
            }
            get
            {
                return ipAddress;
            }
        }

        public string CountryCode
        {
            set
            {
                countryCode = value;
            }
            get
            {
                return countryCode;
            }
        }

        public string CountryName
        {
            set
            {
                countryName = value;
            }
            get
            {
                return countryName;
            }
        }

        public string Region
        {
            set
            {
                region = value;
            }
            get
            {
                return region;
            }
        }

        public string City
        {
            set
            {
                city = value;
            }
            get
            {
                return city;
            }
        }

        public float Latitude
        {
            set
            {
                latitude = value;
            }
            get
            {
                return latitude;
            }
        }

        public float Longitude
        {
            set
            {
                longitude = value;
            }
            get
            {
                return longitude;
            }
        }

        public string Zipcode
        {
            set
            {
                zipcode = value;
            }
            get
            {
                return zipcode;
            }
        }

        public string IspName
        {
            set
            {
                ispName = value;
            }
            get
            {
                return ispName;
            }
        }

        public string DomainName
        {
            set
            {
                domainName = value;
            }
            get
            {
                return domainName;
            }
        }

        public IPAddressStatus IPAddressStatus
        {
            set
            {
                ipAddressStatus = value;
            }
            get
            {
                return ipAddressStatus;
            }
        }

        public DateTime CreatedOn
        {
            set
            {
                createdOn = value;
            }
            get
            {
                return createdOn;
            }
        }

        public DateTime LastModifiedOn
        {
            set
            {
                lastModifiedOn = value;
            }
            get
            {
                return lastModifiedOn;
            }
        }

        public int CreatedBy
        {
            set
            {
                createdBy = value;
            }
            get
            {
                return createdBy;
            }
        }

        public int LastModifiedBy
        {
            set
            {
                lastModifiedBy = value;
            }
            get
            {
                return lastModifiedBy;
            }
        }

        # endregion

        #region Class Constructors

        public IPAddressDetails()
        { 

        }
        public IPAddressDetails(string ipAddress)
        {
            Load(ipAddress);
        }


        #endregion

        # region Methods

        public void Load(string ipAddress)
        {
            //Trace.TraceInformation("IPAddressDetails.Load entered : ipAddress = " + ipAddress);
            if (ipAddress == null || ipAddress.Trim().Length <= 0)
            {
                throw new ArgumentException("ipAddress should not be null or empty string", "ipAddress");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ipAddress", ipAddress);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetIPAddressDetailsByIPAddress, paramList, connection);
            using (DataTable dtIPAddress = DBGateway.FillDataTableSP(SPNames.GetIPAddressDetailsByIPAddress, paramList))
            {
                if (dtIPAddress !=null && dtIPAddress.Rows.Count > 0)
                {
                    DataRow data = dtIPAddress.Rows[0];
                    if (data["countryCode"] != DBNull.Value)
                    {
                        countryCode = Convert.ToString(data["countryCode"]);
                    }
                    if (data["countryName"] != DBNull.Value)
                    {
                        countryName = Convert.ToString(data["countryName"]);
                    }
                    if (data["region"] != DBNull.Value)
                    {
                        region = Convert.ToString(data["region"]);
                    }
                    if (data["city"] != DBNull.Value)
                    {
                        city = Convert.ToString(data["city"]);
                    }
                    if (data["latitude"] != DBNull.Value)
                    {
                        latitude = float.Parse(Convert.ToString(data["latitude"]));
                    }
                    if (data["longitude"] != DBNull.Value)
                    {
                        longitude = float.Parse(Convert.ToString(data["longitude"]));
                    }
                    if (data["zipcode"] != DBNull.Value)
                    {
                        zipcode = Convert.ToString(data["zipcode"]);
                    }
                    if (data["ispName"] != DBNull.Value)
                    {
                        ispName = Convert.ToString(data["ispName"]);
                    }
                    if (data["domainName"] != DBNull.Value)
                    {
                        domainName = Convert.ToString(data["domainName"]);
                    }
                    ipAddressStatus = (IPAddressStatus)Enum.Parse(typeof(IPAddressStatus), Convert.ToString(data["ipAddressStatus"]));
                    createdBy = Convert.ToInt32(data["createdBy"]);
                    createdOn = Convert.ToDateTime(data["createdOn"]);
                    lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                    this.ipAddress = ipAddress;
                    //connection.Close();
                    //data.Close();
                    //Trace.TraceInformation("IPAddressDetails.Load exiting :" + ipAddress.ToString());
                }
                else
                {
                    //connection.Close();
                    //data.Close();
                    //Trace.TraceInformation("IPAddressDetails.Load exiting: ipAddress does not exist. ipAddress = " + ipAddress.ToString());
                    throw new ArgumentException("ipAddress does not exist in database");
                }
            }
        }

        public int Save()
        {
            int status = 0;
            if (ipAddress == null || ipAddress.Trim().Length < 1)
            {
                throw new ArgumentException("IPAddress should not be null or empty string");
            }
            else if ((int)ipAddressStatus < 1)
            {
                throw new ArgumentException("ipAddressStatus should be greater then zero");
            }
            SqlParameter[] paramList = new SqlParameter[13];
            paramList[0] = new SqlParameter("@ipAddress", ipAddress);
            if (countryCode == null)
            {
                paramList[1] = new SqlParameter("@countryCode", DBNull.Value);
            }
            else
            {
                paramList[1] = new SqlParameter("@countryCode", countryCode);
            }
            if (countryName == null)
            {
                paramList[2] = new SqlParameter("@countryName", DBNull.Value);
            }
            else
            {
                paramList[2] = new SqlParameter("@countryName", countryName);
            }
            if (region == null)
            {
                paramList[3] = new SqlParameter("@region", DBNull.Value);
            }
            else
            {
                paramList[3] = new SqlParameter("@region", region);
            }
            if (city == null)
            {
                paramList[4] = new SqlParameter("@city", DBNull.Value);
            }
            else
            {
                paramList[4] = new SqlParameter("@city", city);
            }
            if (latitude == 0)
            {
                paramList[5] = new SqlParameter("@latitude", DBNull.Value);
            }
            else
            {
                paramList[5] = new SqlParameter("@latitude", latitude);
            }
            if (longitude == 0)
            {
                paramList[6] = new SqlParameter("@longitude", DBNull.Value);
            }
            else
            {
                paramList[6] = new SqlParameter("@longitude", longitude);
            }
            if (zipcode == null)
            {
                paramList[7] = new SqlParameter("@zipcode", DBNull.Value);
            }
            else
            {
                paramList[7] = new SqlParameter("@zipcode", zipcode);
            }
            if (ispName == null)
            {
                paramList[8] = new SqlParameter("@ispName", DBNull.Value);
            }
            else
            {
                paramList[8] = new SqlParameter("@ispName", ispName);
            }
            if (domainName == null)
            {
                paramList[9] = new SqlParameter("@domainName", DBNull.Value);
            }
            else
            {
                paramList[9] = new SqlParameter("@domainName", domainName);
            }

            paramList[10] = new SqlParameter("@ipAddressStatus", (int)ipAddressStatus);
            paramList[11] = new SqlParameter("@createdBy", createdBy);
            paramList[12] = new SqlParameter("@status", status);
            paramList[12].Direction = ParameterDirection.Output;
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddIPAddressDetail, paramList);
            status = (int)paramList[12].Value;
            if (status == -1)
            {
                throw new ArgumentException("IPAddress entry already exists.");
            }
            return status;
        }

        public int Update()
        {
            int status = 0;
            if (ipAddress == null || ipAddress.Trim().Length < 1)
            {
                throw new ArgumentException("IPAddress should not be null or empty string");
            }
            else if ((int)ipAddressStatus < 1)
            {
                throw new ArgumentException("ipAddressStatus value should be greater then zero");
            }
            SqlParameter[] paramList = new SqlParameter[13];
            paramList[0] = new SqlParameter("@ipAddress", ipAddress);
            if (countryCode == null)
            {
                paramList[1] = new SqlParameter("@countryCode", DBNull.Value);
            }
            else
            {
                paramList[1] = new SqlParameter("@countryCode", countryCode);
            }
            if (countryName == null)
            {
                paramList[2] = new SqlParameter("@countryName", DBNull.Value);
            }
            else
            {
                paramList[2] = new SqlParameter("@countryName", countryName);
            }
            if (region == null)
            {
                paramList[3] = new SqlParameter("@region", DBNull.Value);
            }
            else
            {
                paramList[3] = new SqlParameter("@region", region);
            }
            if (city == null)
            {
                paramList[4] = new SqlParameter("@city", DBNull.Value);
            }
            else
            {
                paramList[4] = new SqlParameter("@city", city);
            }
            if (latitude == 0)
            {
                paramList[5] = new SqlParameter("@latitude", DBNull.Value);
            }
            else
            {
                paramList[5] = new SqlParameter("@latitude", latitude);
            }
            if (longitude == 0)
            {
                paramList[6] = new SqlParameter("@longitude", DBNull.Value);
            }
            else
            {
                paramList[6] = new SqlParameter("@longitude", longitude);
            }
            if (zipcode == null)
            {
                paramList[7] = new SqlParameter("@zipcode", DBNull.Value);
            }
            else
            {
                paramList[7] = new SqlParameter("@zipcode", zipcode);
            }
            if (ispName == null)
            {
                paramList[8] = new SqlParameter("@ispName", DBNull.Value);
            }
            else
            {
                paramList[8] = new SqlParameter("@ispName", ispName);
            }
            if (domainName == null)
            {
                paramList[9] = new SqlParameter("@domainName", DBNull.Value);
            }
            else
            {
                paramList[9] = new SqlParameter("@domainName", domainName);
            }

            paramList[10] = new SqlParameter("@ipAddressStatus", (int)ipAddressStatus);
            paramList[11] = new SqlParameter("@createdBy", createdBy);
            paramList[12] = new SqlParameter("@status", status);
            paramList[12].Direction = ParameterDirection.Output;
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateIPAddressDetail, paramList);
            status = (int)paramList[12].Value;
            if (status == -1)
            {
                throw new ArgumentException("IPAddress entry not exists.");
            }
            return status;
        }

        public int UpdateIPAddressStatus(string ipAddress, IPAddressStatus ipAddressStatus, int modifiedById)
        {
            //Trace.TraceInformation("IPAddressDetails.UpdateIPAddressStatus entered : ipAddress = " + ipAddress);
            if (ipAddress == null || ipAddress.Trim().Length <= 0)
            {
                throw new ArgumentException("ipAddress should not be null or empty string", "ipAddress");
            }
            if (ipAddressStatus.ToString().Trim().Length <= 1)
            {
                throw new ArgumentException("ipAddressStatus should not empty string", "ipAddressStatus");
            }
            if (modifiedById <= 0)
            {
                throw new ArgumentException("modifiedById should not be less then equal to zero", "modifiedById");
            }
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@ipAddress", ipAddress);
            paramList[1] = new SqlParameter("@ipAddressStatus", (int)ipAddressStatus);
            paramList[2] = new SqlParameter("@lastModifiedBy", modifiedById);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateIPAddressStatus, paramList);
            if (rowsAffected > 0)
            {
                //Trace.TraceInformation("IPAddressDetails.UpdateIPAddressStatus exited : ipAddress = " + ipAddress);
                return rowsAffected;
            }
            else
            {
                //Trace.TraceInformation("IPAddressDetails.UpdateIPAddressStatus exited, IPAddress status not modified : ipAddress = " + ipAddress);
                return 0;
            }
        }

        public static bool IsIPAddressExists(string ipAddress)
        {
            //Trace.TraceInformation("IPAddressDetails.IsIPAddressExists entered ipAddress=" + ipAddress);
            bool isExist = false;
            if (ipAddress == null || ipAddress.Length == 0)
            {
                throw new ArgumentException("ipAddress must not be null or of length Zero.");
            }
            int returnValue;
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@ipAddress", ipAddress);
            paramList[1] = new SqlParameter("@returnValue", SqlDbType.Int);
            paramList[1].Direction = ParameterDirection.Output;
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.IsIPAddressExists, paramList);
            returnValue = (int)paramList[1].Value;
            if (returnValue == 1)
            {
                isExist = true;
            }
            else
            {
                isExist = false;
            }
            return isExist;
        }

        public static IPAddressStatus GetIPAddressStatus(string ipAddress)
        {
            ////Trace.TraceInformation("IPAddressDetails.GetIPAddressStatus entered : ipAddress = " + ipAddress);
            if (ipAddress == null || ipAddress.Trim().Length <= 0)
            {
                throw new ArgumentException("ipAddress should not be null or empty string", "ipAddress");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ipAddress", ipAddress);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetIPAddressStatus, paramList, connection);
            WhiteLabel.IPAddressStatus status = new IPAddressStatus();
            using (DataTable dtIPAddress = DBGateway.FillDataTableSP(SPNames.GetIPAddressStatus, paramList))
            {
                if (dtIPAddress !=null && dtIPAddress.Rows.Count > 0)
                {
                    DataRow data = dtIPAddress.Rows[0];
                    if (data["ipAddressStatus"] != DBNull.Value)
                    {
                        status = (IPAddressStatus)Enum.Parse(typeof(IPAddressStatus), Convert.ToString(data["ipAddressStatus"]));
                    }
                }
                else
                {
                    throw new ArgumentException("IPAddress entry not found");
                }
            }
            return status;
        }
        # endregion
    }
}
