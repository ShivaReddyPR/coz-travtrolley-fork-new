﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// Summary description for Feedback
/// </summary>
namespace CT.BookingEngine.WhiteLabel
{
    public class Feedback
    {
        public Feedback()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        public static DataTable GetFeedbackDetails(DateTime fromDate, DateTime toDate)
        {   
            DataTable dt = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@Fromdate", fromDate);
                paramList[1] = new SqlParameter("@Todate", toDate);
                dt = DBGateway.FillDataTableSP(SPNames.GetFeedbackDetails, paramList);                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

         public static DataTable GetFeedbackDetailsById(int Id)
        {   
            DataTable dt = new DataTable();

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_CFId", Id);
              //@@@@ SPName class required so that Hardcoded SP by chandan
                dt = DBGateway.FillDataTableSP("usp_GetB2C_CorpFeedBack", paramList);                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }
    }
}
