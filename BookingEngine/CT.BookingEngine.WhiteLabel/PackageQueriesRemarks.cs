using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
//using Technology.Indiatimes;
namespace CT.BookingEngine.WhiteLabel
{
    public class PackageQueriesRemarks
    {
        #region privateFields
        int queryId;
        string remarks;
        WLPackageQueriesStatus queryStatus;
        int createdBy;
        DateTime createdOn;
        #endregion

        #region publicProperties
        public int QueryId
        {
            get
            {
                return queryId;
            }
            set
            {
                queryId = value;
            }
        }

        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }

        public WLPackageQueriesStatus QueryStatus
        {
            get
            {
                return queryStatus;
            }
            set
            {
                queryStatus = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }
        #endregion

        #region publicMethods
        /// <summary>
        /// Loads remarks against holiday package query by user of package queries panel.
        /// </summary>
        /// <param name="queryId">queryId</param>
        /// <returns></returns>
        public static List<PackageQueriesRemarks> Load(int queryId)
        {
            List<PackageQueriesRemarks> tempList = new List<PackageQueriesRemarks>();
            //SqlDataReader data = null;
            //using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@queryId", queryId);
                try
                {
                    //data = DBGateway.ExecuteReaderSP(SPNames.GetIndiaTimesPackageQueriesRemarks, paramList, connection);
                    using (DataTable dtIndiaTimes = DBGateway.FillDataTableSP(SPNames.GetIndiaTimesPackageQueriesRemarks, paramList))
                    {
                        if (dtIndiaTimes != null && dtIndiaTimes.Rows.Count > 0)
                        {
                            foreach(DataRow data in dtIndiaTimes.Rows)
                            {
                                PackageQueriesRemarks pqRemarks = new PackageQueriesRemarks();
                                pqRemarks.queryId = Convert.ToInt32(data["queryId"]);
                                pqRemarks.remarks = Convert.ToString(data["remarks"]);
                                pqRemarks.queryStatus = (WLPackageQueriesStatus)(Enum.Parse(typeof(WLPackageQueriesStatus), data["queryStatus"].ToString()));
                                pqRemarks.createdBy = Convert.ToInt32(data["createdBy"]);
                                pqRemarks.createdOn = Convert.ToDateTime(data["createdOn"]);
                                tempList.Add(pqRemarks);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HolidayPackageQuery, Severity.High, 0, "Error: Exception during Load() in PackageQueriesRemarks.cs for queryId= " + queryId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    throw new Exception("Unable to load Package Queries Remarks for queryId: " + queryId);
                }
                finally
                {
                    //if (data != null)
                    //{
                    //    data.Close();
                    //}
                    //connection.Close();
                }
            }
            return tempList;
        }
        #endregion
    }
}
