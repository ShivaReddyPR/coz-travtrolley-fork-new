using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine.WhiteLabel
{
    public class ChangeRequest
    {
        private int requestQueueId;
        private int transactionId;
        private string changeRequestId;
        private int customerId;
        private Request_Type requestType;
        private string remarks;
        private decimal refundedAmount;
        private decimal cancellationCharge;
        private ChangeRequest_Status changeRequestStatus;
        private decimal paymentAmount;
        private bool isPaymentDone;        

        /// <summary>
        /// Auto incremented and unique Id for Change request
        /// </summary>
        public int RequestQueueId
        {
            get
            {
                return requestQueueId;
            }
            set
            {
                requestQueueId = value;
            }
        }               

        /// <summary>
        /// Transaction Id for Booking Details
        /// </summary>
        public int TransactionId
        {
            get
            {
                return transactionId;
            }
            set
            {
                transactionId = value;
            }
        }

        /// <summary>
        /// Unique Id of each Change Request
        /// </summary>
        public string ChangeRequestId
        {
            get
            {
                return changeRequestId;
            }
            set
            {
                changeRequestId = value;
            }
        }

        /// <summary>
        /// subAgentId for subagent
        /// </summary>
        public int CustomerId
        {
            get
            {
                return customerId;
            }
            set
            {
                customerId = value;
            }
        }

        /// <summary>
        /// Request Type of change request
        /// </summary>
        public Request_Type RequestType
        {
            get
            {
                return requestType;
            }
            set
            {
                requestType = value;
            }
        }

        /// <summary>
        /// Remarks written for change request
        /// </summary>
        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }

        /// <summary>
        /// amount refunded after cancelation of booking
        /// </summary>
        public decimal RefundedAmount
        {
            get
            {
                return refundedAmount;
            }
            set
            {
                refundedAmount = value;
            }
        }

        /// <summary>
        /// charge for completing request
        /// </summary>
        public decimal CancellationCharge
        {
            get
            {
                return cancellationCharge;
            }
            set
            {
                cancellationCharge = value;
            }
        }

        /// <summary>
        /// Status of Change Request
        /// </summary>
        public ChangeRequest_Status ChangeRequestStatus
        {
            get
            {
                return changeRequestStatus;
            }
            set
            {
                changeRequestStatus = value;
            }
        }

        /// <summary>
        /// amount that will be used for Payment against change request
        /// </summary>
        public decimal PaymentAmount
        {
            get
            {
                return paymentAmount;
            }
            set
            {
                paymentAmount = value;
            }
        }

        /// <summary>
        /// amount that will be used for Payment against change request
        /// </summary>
        public bool IsPaymentDone
        {
            get
            {
                return isPaymentDone;
            }
            set
            {
                isPaymentDone = value;
            }
        }

        public void Save()
        {
            if (changeRequestId == null || changeRequestId.Length == 0)
            {
                throw new ArgumentException("changeRequestId should not be null or empty string.");
            }
            if (transactionId <= 0)
            {
                throw new ArgumentException("transactionId should not be less then or equal to zero.");
            }
            if (remarks == null || remarks == string.Empty)
            {
                throw new ArgumentException("Remarks should not be null or empty string.");
            }
            int retVal;

            if (requestQueueId != 0)
            {
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@requestQueueId", requestQueueId);
                paramList[1] = new SqlParameter("@refundedAmount", refundedAmount);
                paramList[2] = new SqlParameter("@cancellationCharge", cancellationCharge);
                paramList[3] = new SqlParameter("@changeRequestStatus", changeRequestStatus);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateChangeRequest, paramList);
                if (rowsAffected <= 0)
                {
                    throw new ArgumentException("Change Requested changeRequestId : " + changeRequestId + " not updated.");
                }
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[9];
                paramList[0] = new SqlParameter("@transactionId", transactionId);
                paramList[1] = new SqlParameter("@changeRequestId", changeRequestId);
                paramList[2] = new SqlParameter("@customerId", customerId);
                paramList[3] = new SqlParameter("@requestType", (int)requestType);
                paramList[4] = new SqlParameter("@remarks", remarks);
                paramList[5] = new SqlParameter("@refundedAmount", refundedAmount);
                paramList[6] = new SqlParameter("@cancellationCharge", cancellationCharge);
                paramList[7] = new SqlParameter("@changeRequestStatus", (int)changeRequestStatus);
                paramList[8] = new SqlParameter("@requestQueueId", SqlDbType.Int);
                paramList[8].Direction = ParameterDirection.Output;
                retVal = DBGateway.ExecuteNonQuerySP(SPNames.AddChangeRequest, paramList);
                requestQueueId = (int)paramList[8].Value;
                if (requestQueueId == 0)
                {
                    throw new ArgumentException("changeRequestId already exists.");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="whereString"></param>
        /// <param name="orderByString"></param>
        /// <returns></returns>
        public static int GetTotalFilteredRequestsCount(string whereString, string orderByString)
        {
            ////Trace.TraceInformation("ChangeRequest.GetTotalFilteredRecordsCount entered whereString = " + whereString + " and OrderByString = " + orderByString);
            DataSet dataset = new DataSet();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@whereString", whereString);
            paramList[1] = new SqlParameter("@orderByString", orderByString);
            dataset = DBGateway.FillSP(SPNames.GetTotalFilteredRequestsCount, paramList);
            int queueCount = 0;
            if (dataset.Tables.Count != 0)
            {
                queueCount = Convert.ToInt32(dataset.Tables[0].Rows[0]["count"]);
            }
            dataset.Dispose();
            ////Trace.TraceInformation("ChangeRequest.GetTotalFilteredRecordsCountt Exited queueCount=" + queueCount);
            return queueCount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="noOfRecordsPerPage"></param>
        /// <param name="totalrecords"></param>
        /// <param name="whereString"></param>
        /// <param name="orderByString"></param>
        /// <returns></returns>
        public static List<ChangeRequest> GetTotalFilteredRequests(int pageNumber, int noOfRecordsPerPage, int totalrecords, string whereString, string orderByString)
        {
            ////Trace.TraceInformation("ChangeRequest.GetTotalFilteredRequests entered wherestring=" + whereString + " and OrderByString = " + orderByString);

            SqlDataReader datareader;
            List<ChangeRequest> cRequests = new List<ChangeRequest>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[4];
            int endrow = 0;
            int startrow = ((noOfRecordsPerPage * (pageNumber - 1)) + 1);
            if ((startrow + noOfRecordsPerPage) - 1 < totalrecords)
            {
                endrow = (startrow + noOfRecordsPerPage) - 1;
            }
            else
            {
                endrow = totalrecords;
            }
            paramList[0] = new SqlParameter("@whereString", whereString);
            paramList[1] = new SqlParameter("@orderByString", orderByString);
            paramList[2] = new SqlParameter("@startrow", startrow);
            paramList[3] = new SqlParameter("@endrow", endrow);// changes for paging
            datareader = DBGateway.ExecuteReaderSP(SPNames.GetTotalFilteredRequests, paramList, connection);
            while (datareader.Read())
            {
                ChangeRequest cRequest = new ChangeRequest();
                cRequest.requestQueueId = Convert.ToInt32(datareader["requestQueueId"]);
                cRequest.transactionId = Convert.ToInt32(datareader["transactionId"]);
                cRequest.changeRequestId = Convert.ToString(datareader["changeRequestId"]);
                cRequest.customerId = Convert.ToInt32(datareader["customerId"]);
                cRequest.requestType = (Request_Type)Convert.ToInt32(datareader["requestType"]);
                cRequest.remarks = Convert.ToString(datareader["remarks"]);
                if (datareader["refundedAmount"] != DBNull.Value)
                {
                    cRequest.refundedAmount = Convert.ToDecimal(datareader["refundedAmount"]);
                }
                else
                {
                    cRequest.refundedAmount = 0;
                }
                if (datareader["cancellationCharge"] != DBNull.Value)
                {
                    cRequest.cancellationCharge = Convert.ToDecimal(datareader["cancellationCharge"]);
                }
                else
                {
                    cRequest.cancellationCharge = 0;
                }
                cRequest.changeRequestStatus = (ChangeRequest_Status)Convert.ToInt32(datareader["changeRequestStatus"]);
                if (datareader["paymentAmount"] != DBNull.Value)
                {
                    cRequest.paymentAmount = Convert.ToDecimal(datareader["paymentAmount"]);
                }
                else
                {
                    cRequest.paymentAmount = 0;
                }
                cRequest.isPaymentDone = Convert.ToBoolean(datareader["isPaymentDone"]);                
                cRequests.Add(cRequest);
            }
            ////Trace.TraceInformation("ChangeRequest.GetTotalFilteredRequests Exited");
            datareader.Close();
            return cRequests;
        }

        public static bool IsChangeRequestIDExists(int customerId, string changeRequestId)
        {
            bool isExists = false;
            int requestCount = 0;
            if (changeRequestId == null || changeRequestId.Length == 0)
            {
                throw new ArgumentException("changeRequestId should not be null or empty string.");
            }
            if (customerId <= 0)
            {
                throw new ArgumentException("customerId should be a positive integer number.");
            }
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@customerId", customerId);
            paramList[1] = new SqlParameter("@changeRequestId", changeRequestId);
            paramList[2] = new SqlParameter("@requestCount", requestCount);
            paramList[2].Direction = ParameterDirection.Output;
            int retVal = DBGateway.ExecuteNonQuerySP(SPNames.IsIndiatimesChangeRequestIDExists, paramList);
            requestCount = (int)paramList[2].Value;
            if (requestCount > 0)
            {
                isExists = true;
            }
            else
            {
                isExists = false;
            }
            return isExists;
        }

        /// <summary>
        /// Method to retrieve Change Request
        /// </summary>
        /// <param name="transactionId">changeRequestId of Change Request</param>
        public void GetChangeRequestByChangeRequestId(string changeRequestId)
        {
            if (changeRequestId == null || changeRequestId == "")
            {
                throw new ArgumentException("changeRequestId must not be null or empty string.");
            }
            SqlDataReader datareader;
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@changeRequestId", changeRequestId);
            datareader = DBGateway.ExecuteReaderSP(SPNames.GetChangeRequestByChangeRequestId, paramList, connection);
            ReadDataReader(datareader);
            datareader.Close();
            connection.Close();
        }

        /// <summary>
        /// Reads the data from SqlDataReader
        /// </summary>
        /// <param name="data"></param>
        private void ReadDataReader(SqlDataReader data)
        {
            if (data.Read())
            {
                requestQueueId = Convert.ToInt32(data["requestQueueId"]);
                transactionId = Convert.ToInt32(data["transactionId"]);
                changeRequestId = Convert.ToString(data["changeRequestId"]);
                customerId = Convert.ToInt32(data["customerId"]);
                requestType = (Request_Type)Convert.ToInt32(data["requestType"]);
                remarks = Convert.ToString(data["remarks"]);
                if (data["refundedAmount"] != DBNull.Value)
                {
                    refundedAmount = Convert.ToDecimal(data["refundedAmount"]);
                }
                else
                {
                    refundedAmount = 0;
                }
                if (data["cancellationCharge"] != DBNull.Value)
                {
                    cancellationCharge = Convert.ToDecimal(data["cancellationCharge"]);
                }
                else
                {
                    cancellationCharge = 0;
                }
                changeRequestStatus = (ChangeRequest_Status)Convert.ToInt32(data["changeRequestStatus"]);
                if (data["paymentAmount"] != DBNull.Value)
                {
                    paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                }
                else
                {
                    paymentAmount = 0;
                }
                isPaymentDone = Convert.ToBoolean(data["isPaymentDone"]);
            }
        }

        public void UpdatePaymentStatus(string changeRequestId, decimal paymentAmount, bool isPaymentDone)
        {
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@changeRequestId", changeRequestId);
            paramList[1] = new SqlParameter("@paymentAmount", paymentAmount);
            paramList[2] = new SqlParameter("@isPaymentDone", isPaymentDone);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateChangeRequestPaymentStatus, paramList);
            if (rowsAffected <= 0)
            {
                throw new ArgumentException("Payment Status of Change Request changeRequestId : " + changeRequestId + " not updated.");
            }
        }

        public static string GetWhereStringForChangeRequest(FilterPaymentBy filterPaymentBy, string searchString, object agencyId)
        {
            string whereString = string.Empty;
            if (filterPaymentBy == FilterPaymentBy.ChangeRequestId)
            {
                whereString = " where changeRequestId = '" + searchString + "'";
            }
            else if (filterPaymentBy == FilterPaymentBy.CustomerEmailId)
            {
                whereString = " where customerId in (select customerId from IndiaTimesCustomer where username like '" + searchString + "')";
            }            
            else
            {
                whereString = " ";
            }

            //if (agencyId != null && (int)agencyId > 0 && whereString.Length > 4)
            //{
            //    whereString += " AND AgencyId = " + ((int)agencyId).ToString();
            //}
            //else if (agencyId != null && (int)agencyId > 0 && whereString.Length < 4)
            //{
            //    whereString += " Where AgencyId = " + ((int)agencyId).ToString();
            //}
            
            return whereString;
        }

    }
}
