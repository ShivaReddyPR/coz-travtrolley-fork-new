using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;

namespace CT.BookingEngine.WhiteLabel
{
    public class WLHotelSeason
    {
        #region privateFields
        int seasonId;
        int dealId;
        DateTime startDate;
        DateTime endDate;
        string hotelName;
        WLHotelRating star;
        decimal twinSharingPrice;
        decimal childWithBedPrice;
        decimal childWithoutBedPrice;
        int createdBy;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        #endregion

        #region publicProperties
        public int SeasonId
        {
            get
            {
                return seasonId;
            }
            set
            {
                seasonId = value;
            }
        }

        public int DealId
        {
            get
            {
                return dealId;
            }
            set
            {
                dealId = value;
            }
        }

        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        public string HotelName
        {
            get
            {
                return hotelName;
            }
            set
            {
                hotelName = value;
            }
        }

        public WLHotelRating Star
        {
            get
            {
                return star;
            }
            set
            {
                star = value;
            }
        }

        public decimal TwinSharingPrice
        {
            get
            {
                return twinSharingPrice;
            }
            set
            {
                twinSharingPrice = value;
            }
        }

        public decimal ChildWithBedPrice
        {
            get
            {
                return childWithBedPrice;
            }
            set
            {
                childWithBedPrice = value;
            }
        }

        public decimal ChildWithoutBedPrice
        {
            get
            {
                return childWithoutBedPrice;
            }
            set
            {
                childWithoutBedPrice = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
               lastModifiedOn = value;
            }
        }

        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        #endregion

        #region publicMethods

        /// <summary>
        /// Saves and updates seasons for india times pakage deals.
        /// </summary>
        /// <returns>season Id in case of save and rows affected in case of update</returns>
        public int Save()
        {
            int returnSeasonId = 0;
            SqlParameter[] paramList;
            if (seasonId == 0)
            {
                paramList = new SqlParameter[10];
            }
            else
            {
                paramList = new SqlParameter[9];
            }
            paramList[0] = new SqlParameter("@startDate", startDate);
            paramList[1] = new SqlParameter("@endDate", endDate);
            paramList[2] = new SqlParameter("@hotelName", hotelName);
            paramList[3] = new SqlParameter("@star", (int)star);
            paramList[4] = new SqlParameter("@twinSharingPrice", twinSharingPrice);
            paramList[5] = new SqlParameter("@childWithBedPrice", childWithBedPrice);
            paramList[6] = new SqlParameter("@childWithoutBedPrice", childWithoutBedPrice);
            paramList[7] = new SqlParameter("@createdBy", createdBy);
            paramList[8] = new SqlParameter("@seasonId", seasonId);
            if (seasonId == 0)
            {
                paramList[8].Direction = ParameterDirection.Output;
                paramList[9] = new SqlParameter("@dealId", dealId);
                try
                {
                    DBGateway.ExecuteNonQuerySP(SPNames.IndiaTimesHotelSeasonSave, paramList);
                    returnSeasonId = (int)paramList[9].Value;
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HotelCMS, Severity.High, 0, "Error: Exception during Save() in IndiaTimesHotelSeason.cs for dealId=" + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    returnSeasonId = 0;
                }
            }
            else
            {
                try
                {
                    returnSeasonId = DBGateway.ExecuteNonQuerySP(SPNames.UpdateIndiaTimesHotelSeason, paramList);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HotelCMS, Severity.High, 0, "Error: Exception during update in Save() in IndiaTimesHotelSeason.cs for dealId=" + dealId + ", seasonId=" + seasonId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    returnSeasonId = 0;
                }
                
            }
            return returnSeasonId;
        }

        /// <summary>
        /// Loads list of hotel season against a deal id
        /// </summary>
        /// <param name="dealId">deal id</param>
        /// <returns>List for IndiaTimesHotelSeason</returns>
        public static List<WLHotelSeason> LoadhotelSeasons(int dealId)
        {
            //SqlDataReader data;
            List<WLHotelSeason> tempList = new List<WLHotelSeason>();
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@dealId", dealId);
                //try
                //{
                    SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetIndiaTimesHotelSeasons, paramList, connection);
                    while (data.Read())
                    {
                        WLHotelSeason ssn = new WLHotelSeason();
                        ssn.seasonId = Convert.ToInt32(data["seasonId"]);
                        ssn.dealId = Convert.ToInt32(data["dealId"]);
                        ssn.startDate = Convert.ToDateTime(data["startDate"]);
                        ssn.endDate = Convert.ToDateTime(data["endDate"]);
                        ssn.hotelName = Convert.ToString(data["hotelName"]);
                        ssn.star = (WLHotelRating)(Enum.Parse(typeof(WLHotelRating), Convert.ToString(data["star"])));
                        ssn.twinSharingPrice = Convert.ToDecimal(data["twinSharingPrice"]);
                        ssn.childWithBedPrice = Convert.ToDecimal(data["childWithBedPrice"]);
                        ssn.childWithoutBedPrice = Convert.ToDecimal(data["childWithoutBedPrice"]);
                        tempList.Add(ssn);
                    }
                //}
                //catch (Exception ex)
                //{
                //    Audit.Add(EventType.HotelCMS, Severity.High, 0, "Error: Exception during LoadhotelSeasons() in IndiaTimesHotelSeason.cs for dealId=" + dealId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                //    throw new Exception("Unable to load hotel seasons for deal id: " + dealId);
                //}
                //finally
                //{
                    data.Close();
                    connection.Close();
                //}
            }
            return tempList;
        }

        public static int Delete(string seasonId)
        {
            int returnSeasonId = 0;
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@seasonId", seasonId);
            try
            {
                returnSeasonId = DBGateway.ExecuteNonQuerySP(SPNames.DeleteIndiaTimesHotelSeason, paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelCMS, Severity.High, 0, "Error: Exception during Delete() in IndiaTimesHotelSeason.cs for season id:" + seasonId + "| " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                returnSeasonId = 0;
            }
            return returnSeasonId;
        }
        #endregion

    }

}
