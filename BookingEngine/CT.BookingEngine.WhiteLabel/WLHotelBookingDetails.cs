using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
//using Technology.Indiatimes;
using System.Xml;
using System.IO;

namespace CT.BookingEngine.WhiteLabel
{
    public struct BookingPanelHotelGuestDetail
    {
        public string Title;
        public string FirstName;
        public string MiddleName;
        public string LastName;
        public bool LeadGuest;
        public string Age;
        public string AddressLine1;
        public string AddressLine2;
        public string Countrycode;
        public string Areacode;
        public string Phoneno;
        public string Email;
        public string City;
        public string State;
        public string Country;
        public string Zipcode;
        public string GuestType;
        public int RoomIndex;
    }

    public class WLHotelBookingDetails
    {
        #region privateFields
        int hotelTripId;
        string confirmationNo;
        WLHotelRating starRating;
        PaymentGatewaySource paySource;
        string paymentId;
        decimal paymentAmount;
        int bookingId;
        int agencyId;
        string passengerInfo;
        string cityRef;
        string hotelName;
        string hotelCode;
        string address1;
        string address2;
        DateTime checkInDate;
        DateTime checkOutDate;
        int noOfRooms;
        string roomName;
        WLHotelBookingStatus bookingStatus;
        WLHotelBookingSource source;
        bool isDomestic;
        string cityCode;
        string bookingRefNo;
        bool voucherStatus;
        string remarks;
        string ipAddress;
        string email;
        string phone;
        int adultCount;
        int childCount;
        string country;
        DateTime createdOn;
        int createdBy;
        DateTime lastModifiedOn;
        int lastModifiedBy;
        string reason;
        string backColor;
        IPAddressStatus status;
        CustomerStatus customerStatus;
        List<BookingPanelHotelGuestDetail> guestList;
        int leadPaxId;
        #endregion

        #region publicProperties

        public int HotelTripId
        {
            get
            {
                return hotelTripId;
            }
            set
            {
                hotelTripId = value;
            }
        }

        public string ConfirmationNo
        {
            get
            {
                return confirmationNo;
            }
            set
            {
                confirmationNo = value;
            }
        }

        public WLHotelRating StarRating
        {
            get
            {
                return starRating;
            }
            set
            {
                starRating = value;
            }
        }

        public PaymentGatewaySource PaySource
        {
            get
            {
                return paySource;
            }
            set
            {
                paySource = value;
            }
        }

        public string PaymentId
        {
            get
            {
                return paymentId;
            }
            set
            {
                paymentId = value;
            }
        }

        public decimal PaymentAmount
        {
            get
            {
                return paymentAmount;
            }
            set
            {
                paymentAmount = value;
            }
        }

        public int BookingId
        {
            get
            {
                return bookingId;
            }
            set
            {
                bookingId = value;
            }
        }

        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }

        public string PassengerInfo
        {
            get
            {
                return passengerInfo;
            }
            set
            {
                passengerInfo = value;
            }
        }

        public string CityRef
        {
            get
            {
                return cityRef;
            }
            set
            {
                cityRef = value;
            }
        }

        public string HotelName
        {
            get
            {
                return hotelName;
            }
            set
            {
                hotelName = value;
            }
        }

        public string HotelCode
        {
            get
            {
                return hotelCode;
            }
            set
            {
                hotelCode = value;
            }
        }

        public string Address1
        {
            get
            {
                return address1;
            }
            set
            {
                address1 = value;
            }
        }

        public string Address2
        {
            get
            {
                return address2;
            }
            set
            {
                address2 = value;
            }
        }

        public DateTime CheckInDate
        {
            get
            {
                return checkInDate;
            }
            set
            {
                checkInDate = value;
            }
        }

        public DateTime CheckOutDate
        {
            get
            {
                return checkOutDate;
            }
            set
            {
                checkOutDate = value;
            }
        }

        public int NoOfRooms
        {
            get
            {
                return noOfRooms;
            }
            set
            {
                noOfRooms = value;
            }
        }

        public string RoomName
        {
            get
            {
                return roomName;
            }
            set
            {
                roomName = value;
            }
        }

        public WLHotelBookingStatus BookingStatus
        {
            get
            {
                return bookingStatus;
            }
            set
            {
                bookingStatus = value;
            }
        }

        public WLHotelBookingSource Source
        {
            get
            {
                return source;
            }
            set
            {
                source = value;
            }
        }

        public bool IsDomestic
        {
            get
            {
                return isDomestic;
            }
            set
            {
                isDomestic = value;
            }
        }

        public string CityCode
        {
            get
            {
                return cityCode;
            }
            set
            {
                cityCode = value;
            }
        }

        public string BookingRefNo
        {
            get
            {
                return bookingRefNo;
            }
            set
            {
                bookingRefNo = value;
            }
        }

        public bool VoucherStatus
        {
            get
            {
                return voucherStatus;
            }
            set
            {
                voucherStatus = value;
            }
        }

        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }

        public string IPAddress
        {
            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
            }
        }

        public int AdultCount
        {
            get
            {
                return adultCount;
            }
            set
            {
                adultCount = value;
            }
        }

        public int ChildCount
        {
            get
            {
                return childCount;
            }
            set
            {
                childCount = value;
            }
        }

        public string Country
        {
            get
            {
                return country;
            }
            set
            {
                country = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        public string Reason
        {
            get
            {
                return reason;
            }
            set
            {
                reason = value;
            }
        }

        public string BackColor
        {
            get
            {
                return backColor;
            }
            set
            {
                backColor = value;
            }
        }

        public IPAddressStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public CustomerStatus CustomerStatus
        {
            get
            {
                return customerStatus;
            }
            set
            {
                customerStatus = value;
            }
        }

        public List<BookingPanelHotelGuestDetail> GuestList
        {
            get
            {
                return guestList;
            }
            set
            {
                guestList = value;
            }
        }

        public int LeadPaxId
        {
            get
            {
                return leadPaxId;
            }
            set
            {
                leadPaxId = value;
            }
        }
        #endregion
        /// <summary>
        /// Loads list of ITimesHotelBookingDetails based on where string and page number.
        /// </summary>
        /// <param name="pageNo">Page Number</param>
        /// <param name="whereString">where string</param>
        /// <returns>List of ITimesHotelBookingDetails</returns>
        public static List<WLHotelBookingDetails> Load(int pageNo, string whereString)
        {
            List<WLHotelBookingDetails> tempList = new List<WLHotelBookingDetails>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@rowNo", pageNo * 100);
            paramList[1] = new SqlParameter("@whereString", whereString);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetITimesHotelBookingDetails, paramList, connection);
            if (data.HasRows)
            {
                while (data.Read())
                {
                    WLHotelBookingDetails itbd = new WLHotelBookingDetails();
                    itbd.hotelTripId = Convert.ToInt32(data["hotelTripId"]);
                    if (data["confirmationNo"] != DBNull.Value)
                    {
                        itbd.confirmationNo = Convert.ToString(data["confirmationNo"]);
                    }
                    else
                    {
                        itbd.confirmationNo = string.Empty;
                    }
                    itbd.starRating = (WLHotelRating)(Enum.Parse(typeof(WLHotelRating), data["starRating"].ToString()));
                    itbd.paySource = (PaymentGatewaySource)(Enum.Parse(typeof(PaymentGatewaySource), data["paymentGatewaySourceId"].ToString()));
                    itbd.paymentId = Convert.ToString(data["paymentId"]);
                    itbd.paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                    itbd.bookingId = Convert.ToInt32(data["bookingId"]);
                    itbd.agencyId = Convert.ToInt32(data["agencyId"]);
                    itbd.passengerInfo = Convert.ToString(data["passengerInfo"]);
                    itbd.cityRef = Convert.ToString(data["cityRef"]);
                    itbd.hotelName = Convert.ToString(data["hotelName"]);
                    itbd.hotelCode = Convert.ToString(data["hotelCode"]);
                    itbd.address1 = Convert.ToString(data["address1"]);
                    if (data["address2"] != DBNull.Value)
                    {
                        itbd.address2 = Convert.ToString(data["address2"]);
                    }
                    else
                    {
                        itbd.address2 = string.Empty;
                    }
                    itbd.checkInDate = Convert.ToDateTime(data["checkInDate"]);
                    itbd.checkOutDate = Convert.ToDateTime(data["checkOutDate"]);
                    itbd.noOfRooms = Convert.ToInt32(data["noOfRooms"]);
                    itbd.roomName = data["roomName"].ToString();
                    itbd.bookingStatus = (WLHotelBookingStatus)(Enum.Parse(typeof(WLHotelBookingStatus), data["bookingStatus"].ToString()));
                    itbd.source = (WLHotelBookingSource)(Enum.Parse(typeof(WLHotelBookingSource), data["source"].ToString()));
                    itbd.isDomestic = Convert.ToBoolean(data["isDomestic"]);
                    if (data["cityCode"] != DBNull.Value)
                    {
                        itbd.cityCode = Convert.ToString(data["cityCode"]);
                    }
                    else
                    {
                        itbd.cityCode = string.Empty;
                    }
                    if (data["bookingRefNo"] != DBNull.Value)
                    {
                        itbd.bookingRefNo = Convert.ToString(data["bookingRefNo"]);
                    }
                    else
                    {
                        itbd.bookingRefNo = string.Empty;
                    }
                    itbd.voucherStatus = Convert.ToBoolean(data["voucherStatus"]);
                    if (data["remarks"] != DBNull.Value)
                    {
                        itbd.remarks = Convert.ToString(data["remarks"]);
                    }
                    else
                    {
                        itbd.remarks = string.Empty;
                    }
                    itbd.ipAddress = Convert.ToString(data["ipAddress"]);
                    itbd.email = Convert.ToString(data["email"]);
                    if (data["phone"] != DBNull.Value)
                    {
                        itbd.phone = Convert.ToString(data["phone"]);
                    }
                    else
                    {
                        itbd.phone = string.Empty;
                    }
                    itbd.adultCount = Convert.ToInt32(data["adultCount"]);
                    itbd.childCount = Convert.ToInt32(data["childCount"]);
                    itbd.country = Convert.ToString(data["country"]);
                    itbd.createdOn = Convert.ToDateTime(data["createdOn"]);
                    itbd.createdBy = Convert.ToInt32(data["createdBy"]);
                    itbd.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    itbd.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                    itbd.status = (IPAddressStatus)(Enum.Parse(typeof(IPAddressStatus), data["status"].ToString()));
                    itbd.customerStatus = (CustomerStatus)Enum.Parse(typeof(CustomerStatus), data["customerStatus"].ToString());
                    tempList.Add(itbd);
                }
            }
            data.Close();
            connection.Close();
            return tempList;
        }

        /// <summary>
        /// rteurns where string.
        /// </summary>
        /// <param name="stringFilter">stringFilter</param>
        /// <param name="type">type</param>
        /// <param name="value">value</param>
        /// <param name="redSelect">redSelect</param>
        /// <param name="yellowSelect">yellowSelect</param>
        /// <param name="greenSelect">greenSelect</param>
        /// <param name="failedSelect">failedSelect</param>
        /// <param name="bookedSelect">bookedSelect</param>
        /// <param name="ticktedSelect">ticktedSelect</param>
        /// <param name="fraudSearch">fraudSearch</param>
        /// <param name="domesticSearch">domesticSearch</param>
        /// <param name="internationalSearch">internationalSearch</param>
        /// <param name="dateSearch">dateSearch</param>
        /// <param name="fromDateString">fromDateString</param>
        /// <param name="toDateString">toDateString</param>
        /// <returns>Where string</returns>
        public static string GetWhereString(bool stringFilter, string type, string value, bool redSelect, bool yellowSelect, bool greenSelect, bool failedSelect, bool bookedSelect, bool ticktedSelect, bool fraudSearch, bool domesticSearch, bool internationalSearch, bool dateSearch, string fromDateString, string toDateString)
        {
            string whereString = string.Empty;
            List<int> color = new List<int>();
            if (redSelect)
            {
                color.Add(1);
            }
            if (yellowSelect)
            {
                color.Add(2);
            }
            if (greenSelect)
            {
                color.Add(3);
            }

            List<int> bookingStatusList = new List<int>();
            if (failedSelect)
            {
                bookingStatusList.Add(1);
            }
            if (bookedSelect)
            {
                bookingStatusList.Add(4);
            }
            if (ticktedSelect)
            {
                bookingStatusList.Add(2);
            }

            string filterCheckString = string.Empty;
            for (int i = 0; i < color.Count; i++)
            {
                filterCheckString += color[i] + ",";
            }
            filterCheckString = filterCheckString.Substring(0, filterCheckString.Length - 1);

            string bookingStatusSelectString = string.Empty;
            for (int i = 0; i < bookingStatusList.Count; i++)
            {
                bookingStatusSelectString += bookingStatusList[i] + ",";
            }
            bookingStatusSelectString = bookingStatusSelectString.Substring(0, bookingStatusSelectString.Length - 1);

            if (redSelect && !yellowSelect && !greenSelect)
            {
                whereString = " where (ip.ipAddressStatus in (1) or cust.customerStatus in (1))";
            }
            if (!redSelect && yellowSelect && !greenSelect)
            {
                whereString = " where ((ip.ipAddressStatus in (2) and cust.customerStatus in (2,3)) or (cust.customerStatus in (2) and ip.ipAddressStatus in (3)))";
            }
            if (!redSelect && !yellowSelect && greenSelect)
            {
                whereString = " where (ip.ipAddressStatus in (3) and cust.customerStatus in (3))";
            }
            if (redSelect && yellowSelect && !greenSelect)
            {
                whereString = " where (ip.ipAddressStatus in (1,2) or cust.customerStatus in (1,2))";
            }
            if (redSelect && !yellowSelect && greenSelect)
            {
                whereString = " where ((ip.ipAddressStatus in (1,3) and cust.customerStatus in (1,3)) or (ip.ipAddressStatus in (1) and cust.customerStatus in (2)) or (cust.customerStatus in (1) and ip.ipAddressStatus in (2)))";
            }
            if (!redSelect && yellowSelect && greenSelect)
            {
                whereString = " where (ip.ipAddressStatus in (2,3) and cust.customerStatus in (2,3))";
            }
            if (redSelect && yellowSelect && greenSelect)
            {
                whereString = " where (ip.ipAddressStatus in (1,2,3))";
            }
            whereString += "and bd.bookingStatus in(" + bookingStatusSelectString + ",3,9)";// To change when rejected filter is implemented.
            if (!fraudSearch && !(domesticSearch && internationalSearch))
            {
                if (domesticSearch)
                {
                    whereString += " and bd.isDomestic = 1";
                }
                else
                {
                    whereString += " and bd.isDomestic = 0";
                }
            }
            if (stringFilter)
            {
                value = value.Replace("'", "''");
                if (type == "Trip")
                {
                    whereString += " and bd.hotelTripId = " + value.Trim();
                }
                if (type == "Email")
                {
                    whereString += " and bd.email like '" + value.Trim().ToLower() + "%'";
                }
                if (type == "IP")
                {
                    whereString += " and bd.ipAddress = '" + value.Trim() + "'";
                }
                if (type == "PaymentId")
                {
                    whereString += " and bd.paymentId = '" + value.Trim() + "'";
                }
                if (type == "PNR")
                {
                    whereString += " and bd.outPNR = '" + value.Trim() + "' or bd.inPNR = '" + value.Trim() + "'";
                }
                if (type == "Conf")
                {
                    whereString += " and bd.confirmationNo = '" + value.Trim() + "'";
                }
            }
            if (dateSearch)
            {
                //char[] dateSplitter ={ '/' };
                string[] fromDateArr = fromDateString.Split('/');
                string[] toDateArr = toDateString.Split('/');
                DateTime fromDate = new DateTime(int.Parse(fromDateArr[2]), int.Parse(fromDateArr[1]), int.Parse(fromDateArr[0]), 00, 00, 00);
                DateTime toDate = new DateTime(int.Parse(toDateArr[2]), int.Parse(toDateArr[1]), int.Parse(toDateArr[0]), 23, 59, 59);
                fromDate = Util.ISTToUTC(fromDate);
                toDate = Util.ISTToUTC(toDate);
                whereString += " and bd.createdOn between '" + fromDate.ToString() + "' and '" + toDate.ToString() + "'";
            }
            return whereString;
        }

        /// <summary>
        /// Number of rows for the given where string.
        /// </summary>
        /// <param name="whereString">where string.</param>
        /// <returns>number of rows</returns>
        public static int GetRowCount(string whereString)
        {
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@whereString", whereString);

            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetITimesHotelBookingCount, paramList, connection);
            int numberOfRows = 0;
            if (data.Read())
            {
                numberOfRows = Convert.ToInt32(data["numberOfRows"]);
            }
            data.Close();
            connection.Close();
            return numberOfRows;
        }

        /// <summary>
        /// loads a single trip.
        /// </summary>
        /// <param name="hotelTripId">hotel trip id</param>
        /// <returns>ITimesHotelBookingDetails</returns>
        public static WLHotelBookingDetails GetTrip(int hotelTripId)
        {
            WLHotelBookingDetails itbd = new WLHotelBookingDetails();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelTripId", hotelTripId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelBookingDetailByTripId, paramList, connection);
            if (data.Read())
            {
                itbd.hotelTripId = Convert.ToInt32(data["hotelTripId"]);
                if (data["confirmationNo"] != DBNull.Value)
                {
                    itbd.confirmationNo = Convert.ToString(data["confirmationNo"]);
                }
                else
                {
                    itbd.confirmationNo = string.Empty;
                }
                itbd.starRating = (WLHotelRating)(Enum.Parse(typeof(WLHotelRating), data["starRating"].ToString()));
                itbd.paySource = (PaymentGatewaySource)(Enum.Parse(typeof(PaymentGatewaySource), data["paymentGatewaySourceId"].ToString()));
                itbd.paymentId = Convert.ToString(data["paymentId"]);
                itbd.paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                itbd.bookingId = Convert.ToInt32(data["bookingId"]);
                itbd.agencyId = Convert.ToInt32(data["agencyId"]);
                itbd.passengerInfo = Convert.ToString(data["passengerInfo"]);
                itbd.cityRef = Convert.ToString(data["cityRef"]);
                itbd.hotelName = Convert.ToString(data["hotelName"]);
                itbd.hotelCode = Convert.ToString(data["hotelCode"]);
                itbd.address1 = Convert.ToString(data["address1"]);
                if (data["address2"] != DBNull.Value)
                {
                    itbd.address2 = Convert.ToString(data["address2"]);
                }
                else
                {
                    itbd.address2 = string.Empty;
                }
                itbd.checkInDate = Convert.ToDateTime(data["checkInDate"]);
                itbd.checkOutDate = Convert.ToDateTime(data["checkOutDate"]);
                itbd.noOfRooms = Convert.ToInt32(data["noOfRooms"]);
                itbd.roomName = data["roomName"].ToString();
                itbd.bookingStatus = (WLHotelBookingStatus)(Enum.Parse(typeof(WLHotelBookingStatus), data["bookingStatus"].ToString()));
                itbd.source = (WLHotelBookingSource)(Enum.Parse(typeof(WLHotelBookingSource), data["source"].ToString()));
                itbd.isDomestic = Convert.ToBoolean(data["isDomestic"]);
                if (data["cityCode"] != DBNull.Value)
                {
                    itbd.cityCode = Convert.ToString(data["cityCode"]);
                }
                else
                {
                    itbd.cityCode = string.Empty;
                }
                if (data["bookingRefNo"] != DBNull.Value)
                {
                    itbd.bookingRefNo = Convert.ToString(data["bookingRefNo"]);
                }
                else
                {
                    itbd.bookingRefNo = string.Empty;
                }
                itbd.voucherStatus = Convert.ToBoolean(data["voucherStatus"]);
                if (data["remarks"] != DBNull.Value)
                {
                    itbd.remarks = Convert.ToString(data["remarks"]);
                }
                else
                {
                    itbd.remarks = string.Empty;
                }
                itbd.ipAddress = Convert.ToString(data["ipAddress"]);
                itbd.email = Convert.ToString(data["email"]);
                if (data["phone"] != DBNull.Value)
                {
                    itbd.phone = Convert.ToString(data["phone"]);
                }
                else
                {
                    itbd.phone = string.Empty;
                }
                itbd.adultCount = Convert.ToInt32(data["adultCount"]);
                itbd.childCount = Convert.ToInt32(data["childCount"]);
                itbd.country = Convert.ToString(data["country"]);
                itbd.createdOn = Convert.ToDateTime(data["createdOn"]);
                itbd.createdBy = Convert.ToInt32(data["createdBy"]);
                itbd.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                itbd.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                itbd.status = (IPAddressStatus)(Enum.Parse(typeof(IPAddressStatus), data["status"].ToString()));
                itbd.customerStatus = (CustomerStatus)Enum.Parse(typeof(CustomerStatus), data["customerStatus"].ToString());
            }
            else
            {
                Audit.Add(EventType.IndiaTimesBookingDetailsHotel, Severity.High, 0, "Error: Unable to find the Trip ID: " + hotelTripId + " | " + DateTime.Now, "");
            }
            data.Close();
            connection.Close();
            return itbd;
        }

        /// <summary>
        /// status of ipaddress and email
        /// </summary>
        /// <param name="ipAddress">ip address</param>
        /// <param name="email">email</param>
        /// <returns>Dictionary<string, int></returns>
        public static Dictionary<string, int> LoadStatus(string ipAddress, string email)// check to use...
        {
            if (ipAddress == null || ipAddress.Trim().Length <= 0)
            {
                throw new ArgumentException("ipAddress should not be null or empty string", "ipAddress");
            }
            if (email == null || email.Trim().Length <= 0)
            {
                throw new ArgumentException("email should not be null or empty string", "email");
            }
            Dictionary<string, int> statusDictionary = new Dictionary<string, int>();

            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@ipAddress", ipAddress);
            paramList[1] = new SqlParameter("@email", email);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetBookingFullStatus, paramList, connection);
            if (data.Read())
            {
                statusDictionary.Add("ipStatus", (int)(IPAddressStatus)(Enum.Parse(typeof(IPAddressStatus), data["ipStatus"].ToString())));
                statusDictionary.Add("customerStatus", (int)(CustomerStatus)Enum.Parse(typeof(CustomerStatus), data["customerStatus"].ToString()));
            }
            else
            {
                throw new ArgumentException("ipAddress or email doesnot exist in database.", "ipAddress,email");
            }
            data.Close();
            connection.Close();
            return statusDictionary;
        }

        /// <summary>
        /// updates customer status
        /// </summary>
        /// <param name="userName">user name</param>
        /// <param name="customerStatus">customer status</param>
        /// <returns>number of rows updated</returns>
        public static int UpdateCustomerStatus(string userName, CustomerStatus customerStatus)// check to use...
        {
            if (userName == null || userName.Trim().Length <= 0)
            {
                throw new ArgumentException("userName should not be null or empty string", "userName");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@userName", userName);
            paramList[1] = new SqlParameter("@customerStatus", (int)customerStatus);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateCustomerStatus, paramList);
            if (rowsAffected > 0)
            {
                return rowsAffected;
            }
            else
            {
                
                return 0;
            }
        }

        /// <summary>
        /// Saves booking panel details for hotel.
        /// </summary>
        /// <returns>Trip Id/Number of rows affected.</returns>
        public int Save()
        {
            if (hotelTripId == 0)
            {
                SqlParameter[] paramList = new SqlParameter[32];
                if (confirmationNo != null)
                {
                    paramList[0] = new SqlParameter("@confirmationNo", confirmationNo);
                }
                else
                {
                    paramList[0] = new SqlParameter("@confirmationNo", DBNull.Value);
                }
                paramList[1] = new SqlParameter("@starRating", (int)starRating);
                paramList[2] = new SqlParameter("@paySource", (int)paySource);
                paramList[3] = new SqlParameter("@paymentId", paymentId);
                paramList[4] = new SqlParameter("@paymentAmount", paymentAmount);
                paramList[5] = new SqlParameter("@bookingId", bookingId);
                paramList[6] = new SqlParameter("@agencyId", agencyId);
                paramList[7] = new SqlParameter("@passengerInfo", passengerInfo);
                paramList[8] = new SqlParameter("@cityRef", cityRef);
                paramList[9] = new SqlParameter("@hotelName", hotelName);
                paramList[10] = new SqlParameter("@hotelCode", hotelCode);
                paramList[11] = new SqlParameter("@address1", address1);
                if (address2 == null)
                {
                    paramList[12] = new SqlParameter("@address2", DBNull.Value);
                }
                else
                {
                    paramList[12] = new SqlParameter("@address2", address2);
                }
                paramList[13] = new SqlParameter("@checkInDate", checkInDate);
                paramList[14] = new SqlParameter("@checkOutDate", checkOutDate);
                paramList[15] = new SqlParameter("@noOfRooms", noOfRooms);
                paramList[16] = new SqlParameter("@roomName", roomName);
                paramList[17] = new SqlParameter("@bookingStatus", (int)bookingStatus);
                paramList[18] = new SqlParameter("@source", (int)source);
                paramList[19] = new SqlParameter("@isDomestic", isDomestic);
                if (cityCode == null)
                {
                    paramList[20] = new SqlParameter("@cityCode", DBNull.Value);
                }
                else
                {
                    paramList[20] = new SqlParameter("@cityCode", cityCode);
                }
                if (bookingRefNo == null)
                {
                    paramList[21] = new SqlParameter("@bookingRefNo", DBNull.Value);
                }
                else
                {
                    paramList[21] = new SqlParameter("@bookingRefNo", bookingRefNo);
                }
                paramList[22] = new SqlParameter("@voucherStatus", voucherStatus);
                if (remarks == null || remarks == "")
                {
                    paramList[23] = new SqlParameter("@remarks", DBNull.Value);
                }
                else
                {
                    paramList[23] = new SqlParameter("@remarks", remarks);
                }
                paramList[24] = new SqlParameter("@ipAddress", ipAddress);
                paramList[25] = new SqlParameter("@email", email);
                if (phone == null || phone == "")
                {
                    paramList[26] = new SqlParameter("@phone", DBNull.Value);
                }
                else
                {
                    paramList[26] = new SqlParameter("@phone", phone);
                }
                paramList[27] = new SqlParameter("@adultCount", adultCount);
                paramList[28] = new SqlParameter("@childCount", childCount);
                paramList[29] = new SqlParameter("@country", country);
                paramList[30] = new SqlParameter("@createdBy", createdBy);
                paramList[31] = new SqlParameter("@hotelTripId", hotelTripId);
                paramList[31].Direction = ParameterDirection.Output;

                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddITimesHotelBookingDetails, paramList);
                if (rowsAffected > 0)
                {
                    return (int)paramList[31].Value;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[7];

                paramList[0] = new SqlParameter("@bookingId", bookingId);
                paramList[1] = new SqlParameter("@bookingStatus", (int)bookingStatus);
                paramList[2] = new SqlParameter("@voucherStatus", voucherStatus);
                if (confirmationNo == null || confirmationNo == "")
                {
                    paramList[3] = new SqlParameter("@confirmationNo", DBNull.Value);
                }
                else
                {
                    paramList[3] = new SqlParameter("@confirmationNo", confirmationNo);
                }
                if (remarks == null || remarks == "")
                {
                    paramList[4] = new SqlParameter("@remarks", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@remarks", remarks);
                }
                paramList[5] = new SqlParameter("@hotelTripId", hotelTripId);
                paramList[6] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateITimesHotelBookingDetails, paramList);
                if (rowsAffected > 0)
                {
                    return rowsAffected;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Searches in hotel itinerary table
        /// </summary>
        /// <param name="confirmationNo">confirmationNo</param>
        /// <param name="source">source</param>
        /// <returns>void</returns>
        public static void IsConfExistAndVouchered(string confirmationNo, HotelBookingSource source, out bool isExists, out bool isVouchered, out int bookingId)
        {
            isExists = false;
            isVouchered = false;
            bookingId = 0;
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@confNo", confirmationNo);
            paramList[1] = new SqlParameter("@source", (int)source);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsConfExistAndVouchered, paramList, connection);
            if (data.Read())
            {
                isExists = true;
                isVouchered = Convert.ToBoolean(data["voucherStatus"]);
                bookingId = Convert.ToInt32(data["bookingId"]);
            }
            data.Close();
            connection.Close();
            return;
        }

        /// <summary>
        /// Searches in hotel itinerary table
        /// </summary>
        /// <param name="confirmationNo">confirmationNo</param>
        /// <param name="source">source</param>
        /// <returns>void</returns>
        public static bool IsConfirmationNoUnique(string confirmationNo)
        {
            bool status = false;
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@confNo", confirmationNo);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsConfirmationNoUnique, paramList, connection);
            data.Read();
            if (Convert.ToInt32(data["counter"]) > 0)
            {
                status = false;
            }
            else
            {
                status = true;
            }
            data.Close();
            connection.Close();
            return status;
        }

        /// <summary>
        /// gets hotelid from hotel itinerary table based on conformation number and source.
        /// </summary>
        /// <param name="confNo">confirmation number</param>
        /// <param name="source">source</param>
        /// <returns>hotel id</returns>
        public static int GetHotelId(string confNo, WLHotelBookingSource source)
        {
            int hotelId = 0;
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@confirmationNo", confNo);
            paramList[1] = new SqlParameter("@source", source);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelIdFromSource, paramList, connection);
            if (data.Read())
            {
                hotelId = Convert.ToInt32(data["hotelId"]);
            }
            data.Close();
            connection.Close();
            return hotelId;
        }

        /// <summary>
        /// upadtes voucher status
        /// </summary>
        /// <param name="hotelId"></param>
        /// <param name="voucherStatus"></param>
        /// <param name="hotelTripId"></param>
        /// <returns>number of rows affected.</returns>
        public static int UpdateVoucherStatus(int hotelId, bool voucherStatus, int hotelTripId)
        {
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@status", voucherStatus);
            paramList[1] = new SqlParameter("@hotelId", hotelId);
            paramList[2] = new SqlParameter("@hotelTripId", hotelTripId);

            int retVal = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHotelVoucherStatus, paramList);
            return retVal;
        }

        /// <summary>
        /// Loads all rows based on where string.
        /// </summary>
        /// <param name="whereString">where string.</param>
        /// <returns>List of ITimesHotelBookingDetails</returns>
        public static List<WLHotelBookingDetails> Load(string whereString)
        {
            List<WLHotelBookingDetails> tempList = new List<WLHotelBookingDetails>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            //paramList[0] = new SqlParameter("@rowNo", pageNo * 100);
            paramList[0] = new SqlParameter("@whereString", whereString);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAllITimesHotelBookingDetails, paramList, connection);
            if (data.HasRows)
            {
                while (data.Read())
                {
                    WLHotelBookingDetails itbd = new WLHotelBookingDetails();
                    itbd.hotelTripId = Convert.ToInt32(data["hotelTripId"]);
                    if (data["confirmationNo"] != DBNull.Value)
                    {
                        itbd.confirmationNo = Convert.ToString(data["confirmationNo"]);
                    }
                    else
                    {
                        itbd.confirmationNo = string.Empty;
                    }
                    itbd.starRating = (WLHotelRating)(Enum.Parse(typeof(WLHotelRating), data["starRating"].ToString()));
                    itbd.paySource = (PaymentGatewaySource)(Enum.Parse(typeof(PaymentGatewaySource), data["paymentGatewaySourceId"].ToString()));
                    itbd.paymentId = Convert.ToString(data["paymentId"]);
                    itbd.paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                    itbd.bookingId = Convert.ToInt32(data["bookingId"]);
                    itbd.agencyId = Convert.ToInt32(data["agencyId"]);
                    itbd.passengerInfo = Convert.ToString(data["passengerInfo"]);
                    itbd.cityRef = Convert.ToString(data["cityRef"]);
                    itbd.hotelName = Convert.ToString(data["hotelName"]);
                    itbd.hotelCode = Convert.ToString(data["hotelCode"]);
                    itbd.address1 = Convert.ToString(data["address1"]);
                    if (data["address2"] != DBNull.Value)
                    {
                        itbd.address2 = Convert.ToString(data["address2"]);
                    }
                    else
                    {
                        itbd.address2 = string.Empty;
                    }
                    itbd.checkInDate = Convert.ToDateTime(data["checkInDate"]);
                    itbd.checkOutDate = Convert.ToDateTime(data["checkOutDate"]);
                    itbd.noOfRooms = Convert.ToInt32(data["noOfRooms"]);
                    itbd.roomName = data["roomName"].ToString();
                    itbd.bookingStatus = (WLHotelBookingStatus)(Enum.Parse(typeof(WLHotelBookingStatus), data["bookingStatus"].ToString()));
                    itbd.source = (WLHotelBookingSource)(Enum.Parse(typeof(WLHotelBookingSource), data["source"].ToString()));
                    itbd.isDomestic = Convert.ToBoolean(data["isDomestic"]);
                    if (data["cityCode"] != DBNull.Value)
                    {
                        itbd.cityCode = Convert.ToString(data["cityCode"]);
                    }
                    else
                    {
                        itbd.cityCode = string.Empty;
                    }
                    if (data["bookingRefNo"] != DBNull.Value)
                    {
                        itbd.bookingRefNo = Convert.ToString(data["bookingRefNo"]);
                    }
                    else
                    {
                        itbd.bookingRefNo = string.Empty;
                    }
                    itbd.voucherStatus = Convert.ToBoolean(data["voucherStatus"]);
                    if (data["remarks"] != DBNull.Value)
                    {
                        itbd.remarks = Convert.ToString(data["remarks"]);
                    }
                    else
                    {
                        itbd.remarks = string.Empty;
                    }
                    itbd.ipAddress = Convert.ToString(data["ipAddress"]);
                    itbd.email = Convert.ToString(data["email"]);
                    if (data["phone"] != DBNull.Value)
                    {
                        itbd.phone = Convert.ToString(data["phone"]);
                    }
                    else
                    {
                        itbd.phone = string.Empty;
                    }
                    itbd.adultCount = Convert.ToInt32(data["adultCount"]);
                    itbd.childCount = Convert.ToInt32(data["childCount"]);
                    itbd.country = Convert.ToString(data["country"]);
                    itbd.createdOn = Convert.ToDateTime(data["createdOn"]);
                    itbd.createdBy = Convert.ToInt32(data["createdBy"]);
                    itbd.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    itbd.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                    itbd.status = (IPAddressStatus)(Enum.Parse(typeof(IPAddressStatus), data["status"].ToString()));
                    itbd.customerStatus = (CustomerStatus)Enum.Parse(typeof(CustomerStatus), data["customerStatus"].ToString());
                    tempList.Add(itbd);
                }
            }
            data.Close();
            connection.Close();
            return tempList;
        }

        /// <summary>
        /// for export to excel. creates string for csv.
        /// </summary>
        /// <param name="whereString">where string.</param>
        /// <returns>string having data for scv</returns>
        public static string LoadBookingPanelCSV(string whereString)
        {
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@whereString", whereString);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAllITimesHotelBookingDetails, paramList, connection);
            StringBuilder csvString = new StringBuilder();
            //csvString.Append("TripId,OutPNR,InPNR,Date,Amount,Gateway,Payment ID,Sector,Pax,AirlineCodeOut,FlightNumberOut,AirlineCodeIn,FlightNumberIn,DepartureDate,ReturnDate,BookingStatus,Phone,Email,\n");
            csvString.Append("TripId,Confirmation No,Date,Amount,Gateway,Payment ID,City,Pax,Hotel Name,Checkin Date,Checkout Date,BookingStatus,Phone,Email,\n");
            if (data.HasRows)
            {
                while (data.Read())
                {
                    csvString.Append(Convert.ToString(data["hotelTripId"]) + ",");
                    if (data["confirmationNo"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToString(data["confirmationNo"]) + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    csvString.Append(Util.UTCToIST(Convert.ToDateTime(data["createdOn"])).ToString("dd/MM/yy HH:mm") + ",");
                    if (data["paymentAmount"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToDecimal(data["paymentAmount"]).ToString() + ",");
                    }
                    else
                    {
                        csvString.Append("0.00,");
                    }
                    csvString.Append(((PaymentGatewaySource)(Enum.Parse(typeof(PaymentGatewaySource), data["paymentGatewaySourceId"].ToString()))).ToString() + ",");
                    if (data["paymentId"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToString(data["paymentId"]) + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    if (data["cityRef"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToString(data["cityRef"]) + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    //PaxDetail pax = new PaxDetail();
                    if (data["passengerInfo"] != null)
                    {
                        //ITimesHotelBookingDetails bookingHotel = new ITimesHotelBookingDetails();
                        List<BookingPanelHotelGuestDetail> guestListCSV = new List<BookingPanelHotelGuestDetail>();
                        int leadPaxNumber = 0;
                        string temp = Convert.ToString(data["passengerInfo"]).Replace("xmlns=\"http://Technology/HotelBookingApi\"", string.Empty);
                        temp = temp.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", string.Empty);
                        TextReader text = new StringReader(temp);
                        XmlDocument doc = new XmlDocument();
                        doc.Load(text);
                        XmlNodeList nodeList = doc.SelectNodes("/Guest/WSGuest");

                        List<BookingPanelHotelGuestDetail> tempGuestList = new List<BookingPanelHotelGuestDetail>();
                        int guestCounter = 0;
                        try
                        {
                            foreach (XmlNode node in nodeList)
                            {
                                if (node.HasChildNodes)
                                {
                                    BookingPanelHotelGuestDetail guest = new BookingPanelHotelGuestDetail();
                                    if (node.SelectSingleNode("Title") != null)
                                    {
                                        guest.Title = node.SelectSingleNode("Title").InnerText;
                                    }
                                    if (node.SelectSingleNode("FirstName") != null)
                                    {
                                        guest.FirstName = node.SelectSingleNode("FirstName").InnerText;
                                    }
                                    if (node.SelectSingleNode("MiddleName") != null)
                                    {
                                        guest.MiddleName = node.SelectSingleNode("MiddleName").InnerText;
                                    }
                                    if (node.SelectSingleNode("LastName") != null)
                                    {
                                        guest.LastName = node.SelectSingleNode("LastName").InnerText;
                                    }
                                    tempGuestList.Add(guest);
                                    if (guest.RoomIndex == 0 && guest.LeadGuest)
                                    {
                                        leadPaxNumber = guestCounter;
                                    }
                                    guestCounter++;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                        //bookingHotel.GuestList = new List<BookingPanelHotelGuestDetail>();
                        guestListCSV = tempGuestList;
                        if (guestListCSV[leadPaxNumber].FirstName != null && guestListCSV[leadPaxNumber].FirstName != "")
                        {
                            csvString.Append(guestListCSV[leadPaxNumber].Title + " " + guestListCSV[leadPaxNumber].FirstName + " " + guestListCSV[leadPaxNumber].LastName);
                            if (Convert.ToInt32(data["noOfRooms"]) > 1)
                            {
                                csvString.Append(" x " + Convert.ToInt32(data["noOfRooms"]) + ",");
                            }
                            else
                            {
                                csvString.Append(" ,");
                            }
                        }
                        else
                        {
                            csvString.Append(" ,");
                        }
                    }
                    csvString.Append(Convert.ToString(data["hotelName"]) + ",");
                    csvString.Append(Convert.ToDateTime(data["checkInDate"]).ToString("dd/MM/yy HH:mm") + ",");
                    csvString.Append(Convert.ToDateTime(data["checkOutDate"]).ToString("dd/MM/yy HH:mm") + ",");
                    csvString.Append(((WLHotelBookingStatus)(Enum.Parse(typeof(WLHotelBookingStatus), data["bookingStatus"].ToString()))).ToString() + ",");
                    if (data["phone"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToString(data["phone"]) + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    if (data["email"] != DBNull.Value)
                    {
                        csvString.Append(Convert.ToString(data["email"]) + ",");
                    }
                    else
                    {
                        csvString.Append(" ,");
                    }
                    csvString.Append("\n");
                }
            }
            data.Close();
            connection.Close();
            return csvString.ToString();
        }
    }
}
