using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine.WhiteLabel
{
    public class WLBookingHistory
    {
        # region Private Fields
        int tripId;
        WLBookingAction action;
        string remarks;
        int createdBy;
        DateTime createdOn;
        #endregion

        # region Public Properties

        public int TripId
        {
            get
            {
                return tripId;
            }
            set
            {
                tripId = value;
            }
        }

        public WLBookingAction Action
        {
            get
            {
                return action;
            }
            set
            {
                action = value;
            }
        }

        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tripId"></param>
        /// <returns></returns>
        public static List<WLBookingHistory> Load(int tripId)
        {
            //Trace.TraceInformation("ITimesBookingDetails.Load entered. Trip ID = " + tripId);
            if (tripId <= 0)
            {
                throw new ArgumentException("tripId should not be less then equal to zero", "tripId");
            } 
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@tripId", tripId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHistoryForTrip, paramList, connection);
            List<WLBookingHistory> tempList = new List<WLBookingHistory>();
            if (data.HasRows)
            {
                while (data.Read())
                {
                    WLBookingHistory iHistory = new WLBookingHistory();
                    iHistory.action = (WLBookingAction)Enum.Parse(typeof(WLBookingAction), data["action"].ToString());
                    iHistory.remarks = Convert.ToString(data["remarks"]);
                    iHistory.createdBy = Convert.ToInt32(data["createdBy"]);
                    iHistory.createdOn = Convert.ToDateTime(data["createdOn"]);
                    tempList.Add(iHistory);
                }
            }
            //Trace.TraceInformation("ITimesBookingDetails.Load exited. Trip ID = " + tripId);
            return tempList;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tripId"></param>
        /// <param name="action"></param>
        /// <param name="remarks"></param>
        /// <param name="createdBy"></param>
        /// <returns></returns>
        public static int SaveBookingHistory(int tripId, WLBookingAction action, string remarks, int createdBy)
        {
            //Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory entered. Trip ID = " + tripId + ", action = " + action.ToString() + ", remarks = " + remarks + ", created by: " + createdBy);
            if (tripId <= 0)
            {
                throw new ArgumentException("tripId should not be less then equal to zero", "tripId");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy should not be less then equal to zero", "createdBy");
            }
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@tripId", tripId);
            paramList[1] = new SqlParameter("@action", action);
            paramList[2] = new SqlParameter("@remarks", remarks);
            paramList[3] = new SqlParameter("@createdBy", createdBy);
            //paramList[2] = new SqlParameter("@lastModifiedBy", modifiedById);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.SaveWLBookingHistory, paramList);
            if (rowsAffected > 0)
            {
                //Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory exited. Trip ID = " + tripId + ", action = " + action.ToString() + ", remarks = " + remarks + ", created by: " + createdBy);
                return rowsAffected;
            }
            else
            {
                //Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory exited, tripId history not saved. Trip ID = " + tripId + ", action = " + action.ToString() + ", remarks = " + remarks + ", created by: " + createdBy);
                //Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory exited, tripId history not saved : tripId = " + tripId);
                return 0;
            }
        }


        public static int SaveAllBookingHistory(int tripId, WLBookingAction action, string remarks, int createdBy, string parameter, string saveType)
        {
            //Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory entered. Trip ID = " + tripId + ", action = " + action.ToString() + ", remarks = " + remarks + ", created by: " + createdBy);
            if (tripId <= 0)
            {
                throw new ArgumentException("tripId should not be less then equal to zero", "tripId");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy should not be less then equal to zero", "createdBy");
            }
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@tripId", tripId);
            paramList[1] = new SqlParameter("@action", action);
            paramList[2] = new SqlParameter("@remarks", remarks);
            paramList[3] = new SqlParameter("@createdBy", createdBy);
            switch (saveType)
            {
                case "email":
                    paramList[4] = new SqlParameter("@email", parameter);
                    rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.SaveAllWLBookingHistoryEmail, paramList);
                    break;

                case "ip":
                    paramList[4] = new SqlParameter("@ipAddress", parameter);
                    rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.SaveAllWLBookingHistoryIP, paramList);
                    break;
            }
            if (rowsAffected > 0)
            {
                //Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory exited. Trip ID = " + tripId + ", action = " + action.ToString() + ", remarks = " + remarks + ", created by: " + createdBy);
                return rowsAffected;
            }
            else
            {
                //Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory exited, tripId history not saved. Trip ID = " + tripId + ", action = " + action.ToString() + ", remarks = " + remarks + ", created by: " + createdBy);
                //Trace.TraceInformation("ITimesBookingDetails.SaveBookingHistory exited, tripId history not saved : tripId = " + tripId);
                return 0;
            }
        }

    }
}
