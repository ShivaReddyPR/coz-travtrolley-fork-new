﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

namespace CT.BookingEngine.WhiteLabel
{
    public class SEOMaster
    {
        #region Variabule Declaration
        int New_Record = -1;
        int _id;
        string _pageName;
        string _url;
        string _urlRewriting;
        string _pageTitle;
        string _imageAltText;
        string _metaDescription;
        string _metaKeywords;
        int _memberId;
        int _agentId;
        int _createdBy;
        string _h1Tag;
        string _socialTag;
        string _h1TagDesc;
        #endregion


        #region Properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string PageName
        {
            get { return _pageName; }
            set { _pageName = value; }
        }
        public string URL
        {
            get { return _url; }
            set { _url = value; }
        }
        public string URLRewriting
        {
            get { return _urlRewriting; }
            set { _urlRewriting = value; }
        }
        public string PageTitle
        {
            get { return _pageTitle; }
            set { _pageTitle = value; }
        }
        public string ImageAltText
        {
            get { return _imageAltText; }
            set { _imageAltText = value; }
        }
        public string MetaDescription
        {
            get { return _metaDescription; }
            set { _metaDescription = value; }
        }
        public string MetaKeywords
        {
            get { return _metaKeywords; }
            set { _metaKeywords = value; }
        }
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        public int AgentId
        {
            get { return _agentId; }
            set { _agentId = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public string H1Tag
        {
            get
            {
                return _h1Tag;
            }

            set
            {
                _h1Tag = value;
            }
        }

        public string SocialTag
        {
            get
            {
                return _socialTag;
            }

            set
            {
                _socialTag = value;
            }
        }

        public string H1TagDesc
        {
            get
            {
                return _h1TagDesc;
            }

            set
            {
                _h1TagDesc = value;
            }
        }
        #endregion

        public SEOMaster()
        {
            _id = New_Record;
        }
        public SEOMaster(int id)
        {
            getDetails(id);
        }

        private void getDetails(int id)
        {
            try
            {
                DataSet ds = GetData(id);
                if (ds.Tables[0] != null & ds.Tables[0].Rows.Count == 1)
                {
                    UpdateBusinessData(ds.Tables[0].Rows[0]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private DataSet GetData(int id)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ID", id);
                DataSet dsResult = DBGateway.ExecuteQuery("usp_SEOMasterGetData", paramList);
                return dsResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataRow dr)
        {
            try
            {
                _id = Utility.ToInteger(dr["ID"]);
                _pageName = Utility.ToString(dr["PageName"]);
                _url = Utility.ToString(dr["URL"]);
                _urlRewriting = Utility.ToString(dr["URLRewriting"]);
                _pageTitle = Utility.ToString(dr["PageTitle"]);
                _imageAltText = Utility.ToString(dr["ImageAltText"]);
                _metaDescription = Utility.ToString(dr["MetaDescription"]);
                _metaKeywords = Utility.ToString(dr["MetaKeywords"]);
                _agentId = Utility.ToInteger(dr["AgentId"]);
                _memberId = Utility.ToInteger(dr["MemberId"]);
                _createdBy = Utility.ToInteger(dr["createdBy"]);
                _socialTag = Utility.ToString(dr["SocialTag"]);
                _h1Tag = Utility.ToString(dr["H1Tag"]);
                _h1TagDesc = Utility.ToString(dr["H1TagDesc"]);
            }
            catch
            {
                throw;
            }
        }

        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[16];
                paramList[0] = new SqlParameter("@P_ID", _id);
                paramList[1] = new SqlParameter("@P_PageName", _pageName);
                paramList[2] = new SqlParameter("@P_URL", _url);
                if(!string.IsNullOrEmpty(_urlRewriting)) paramList[3] = new SqlParameter("@P_URLRewriting", _urlRewriting);
                paramList[4] = new SqlParameter("@P_PageTitle", _pageTitle);
                paramList[5] = new SqlParameter("@P_ImageAltText", _imageAltText);
                paramList[6] = new SqlParameter("@P_MetaDescription", _metaDescription);
                paramList[7] = new SqlParameter("@P_MetaKeywords", _metaKeywords);
                paramList[8] = new SqlParameter("@P_MemberId", _memberId);
                paramList[9] = new SqlParameter("@P_AgentId", _agentId);
                paramList[10] = new SqlParameter("@P_CREATED_BY", _createdBy);
                paramList[11] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[11].Direction = ParameterDirection.Output;
                paramList[12] = new SqlParameter("@P_MSG_TEXT", SqlDbType.VarChar, 100);
                paramList[12].Direction = ParameterDirection.Output;
                paramList[13] = new SqlParameter("@P_SocialTag", _socialTag);
                if (!string.IsNullOrEmpty(_h1Tag)) paramList[14] = new SqlParameter("@P_H1Tag", _h1Tag);
                if (!string.IsNullOrEmpty(_h1TagDesc)) paramList[15] = new SqlParameter("@P_H1TagDesc", _h1TagDesc);
                int rowAffected = DBGateway.ExecuteNonQuery("usp_SEOMasterAddUpdate", paramList);
                string messageType = Utility.ToString(paramList[11].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[12].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region StaticMethods
        public static DataTable GetList(RecordStatus recordStatus, ListStatus listStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", listStatus);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus));
                return DBGateway.ExecuteQuery("usp_SEOMasterGetList", paramList).Tables[0];
            }
            catch
            {
                throw;
            }
        }
        #endregion


        
    }
}
