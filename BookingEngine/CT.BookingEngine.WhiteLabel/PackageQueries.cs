using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
//using Technology.Indiatimes;

namespace CT.BookingEngine.WhiteLabel
{
    public class PackageQueries
    {
        #region privateFields
        int queryId;
        int dealId;
        int nights;
        string dealName;
        bool isInternational;
        string paxName;
        string email;
        string phone;
        string paxMessage;
        WLPackageQueriesStatus queryStatus;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        #endregion

        #region publicProperties
        public int QueryId
        {
            get
            {
                return queryId;
            }
            set
            {
                queryId = value;
            }
        }

        public int DealId
        {
            get
            {
                return dealId;
            }
            set
            {
                dealId = value;
            }
        }

        public int Nights
        {
            get
            {
                return nights;
            }
            set
            {
                nights = value;
            }
        }

        public string DealName
        {
            get
            {
                return dealName;
            }
            set
            {
                dealName = value;
            }
        }

        public bool IsInternational
        {
            get
            {
                return isInternational;
            }
            set
            {
                isInternational = value;
            }
        }

        public string PaxName
        {
            get
            {
                return paxName;
            }
            set
            {
                paxName = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
            }
        }

        public string PaxMessage
        {
            get
            {
                return paxMessage;
            }
            set
            {
                paxMessage = value;
            }
        }

        public WLPackageQueriesStatus QueryStatus
        {
            get
            {
                return queryStatus;
            }
            set
            {
                queryStatus = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        #endregion

        #region publicMethods
        /// <summary>
        /// Loads list of PackageQueries to be shown on a specific page according to the where string.
        /// </summary>
        /// <param name="pageNo">Page no for which PackageQueries needs to be shown.</param>
        /// <param name="whereString">String created according to filters</param>
        /// <returns>List of PackageQueries</returns>
        public static List<PackageQueries> Load(int pageNo, string whereString)
        {
            List<PackageQueries> tempList = new List<PackageQueries>();
            SqlDataReader data = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@rowNo", pageNo * 50);
                paramList[1] = new SqlParameter("@whereString", whereString);
                try
                {
                    data = DBGateway.ExecuteReaderSP(SPNames.GetIndiaTimesPackageQueries, paramList, connection);
                    while (data.Read())
                    {
                        PackageQueries itpq = new PackageQueries();
                        itpq.queryId = Convert.ToInt32(data["queryId"]);
                        itpq.dealId = Convert.ToInt32(data["dealId"]);
                        itpq.dealName = Convert.ToString(data["dealName"]);
                        itpq.nights = Convert.ToInt32(data["nights"]);
                        itpq.isInternational = Convert.ToBoolean(data["isInternational"]);
                        if (data["paxName"] != DBNull.Value)
                        {
                            itpq.paxName = Convert.ToString(data["paxName"]);
                        }
                        else
                        {
                            itpq.paxName = string.Empty;
                        }
                        if (data["email"] != DBNull.Value)
                        {
                            itpq.email = Convert.ToString(data["email"]);
                        }
                        else
                        {
                            itpq.email = string.Empty;
                        }
                        if (data["phone"] != DBNull.Value)
                        {
                            itpq.phone = Convert.ToString(data["phone"]);
                        }
                        else
                        {
                            itpq.phone = string.Empty;
                        }
                        if (data["paxMessage"] != DBNull.Value)
                        {
                            itpq.paxMessage = Convert.ToString(data["paxMessage"]);
                        }
                        else
                        {
                            itpq.paxMessage = string.Empty;
                        }
                        itpq.queryStatus = (WLPackageQueriesStatus)(Enum.Parse(typeof(WLPackageQueriesStatus), data["queryStatus"].ToString()));
                        itpq.createdOn = Convert.ToDateTime(data["createdOn"]);
                        itpq.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                        itpq.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                        tempList.Add(itpq);
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HolidayPackageQuery, Severity.High, 0, "Error: Exception during Load() in PackageQueries.cs for whereString= " + whereString + " , page no:" + pageNo + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    throw new Exception("Unable to load PackageQueries for whereString: " + whereString + " pageNo:" + pageNo);
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return tempList;
        }

        /// <summary>
        /// Gets total no of rows in the db for the given filter string.
        /// </summary>
        /// <param name="whereString">filter string</param>
        /// <returns>No of rows in the db</returns>
        public static int GetRowCount(string whereString)
        {
            int numberOfRows = 0;
            SqlDataReader data = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@whereString", whereString);
                try
                {
                    data = DBGateway.ExecuteReaderSP(SPNames.GetIndiaTimesPackageQueriesCount, paramList, connection);
                    if (data.Read())
                    {
                        numberOfRows = Convert.ToInt32(data["numberOfRows"]);
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HolidayPackageQuery, Severity.High, 0, "Error: Exception during GetRowCount() in PackageQueries.cs for whereString= " + whereString + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    throw new Exception("Unable to load count of rows for whereString: " + whereString);
                }
                finally
                {
                    if (data != null)
                    {
                        data.Close();
                    }
                    connection.Close();
                }
            }
            return numberOfRows;
        }

        /// <summary>
        /// Generates filter string required on page load.
        /// </summary>
        /// <returns>filter string required on page load.</returns>
        public static string GetWhereString()
        {
            string whereString = string.Empty;
            whereString = " where queryStatus in(1,2) ";
            return whereString;
        }

        /// <summary>
        /// Generates filter string according to the parameters specified.
        /// </summary>
        /// <param name="stringFilter">Drop down filter choosen or not.</param>
        /// <param name="type">Type of drop down filter selected.</param>
        /// <param name="value">Value of search parameter if drop down filter selected</param>
        /// <param name="newSelect">newSelect check</param>
        /// <param name="openSelect">openSelect check</param>
        /// <param name="processedSelect">processedSelect check</param>
        /// <param name="rejectedSelect">rejectedSelect check</param>
        /// <param name="closedSelect">closedSelect check</param>
        /// <param name="duplicateSelect">duplicateSelect check</param>
        /// <param name="domesticSearch">domesticSearch check</param>
        /// <param name="internationalSearch">internationalSearch check</param>
        /// <param name="dateSearch">If date search is selected.</param>
        /// <param name="fromDateString">from date</param>
        /// <param name="toDateString">to date</param>
        /// <returns>filter string according to the parameters specified.</returns>
        public static string GetWhereString(bool stringFilter, string type, string value, bool newSelect, bool openSelect, bool processedSelect, bool rejectedSelect, bool closedSelect, bool duplicateSelect, bool domesticSearch, bool internationalSearch, bool dateSearch, string fromDateString, string toDateString)
        {
            string whereString = string.Empty;
            List<int> filters = new List<int>();
            if (newSelect)
            {
                filters.Add(1);
            }
            if (openSelect)
            {
                filters.Add(2);
            }
            if (processedSelect)
            {
                filters.Add(3);
            }
            if (rejectedSelect)
            {
                filters.Add(4);
            }
            if (closedSelect)
            {
                filters.Add(5);
            }
            if (duplicateSelect)
            {
                filters.Add(6);
            }
            string filterCheckString = string.Empty;
            for (int i = 0; i < filters.Count; i++)
            {
                filterCheckString += filters[i] + ",";
            }
            filterCheckString = filterCheckString.Substring(0, filterCheckString.Length - 1);
            whereString = " where queryStatus in(" + filterCheckString + ") ";
            if (!(domesticSearch && internationalSearch))
            {
                if (domesticSearch)
                {
                    whereString += " and isInternational = 0 ";
                }
                else
                {
                    whereString += " and isInternational = 1 ";
                }
            }
            if (stringFilter)
            {
                value = value.Replace("'", "''");
                if (type == "Query")
                {
                    whereString += " and queryId = " + value.Trim();
                }
                if (type == "Name")
                {
                    whereString += " and paxName like '" + value.Trim() + "%'";
                }
                if (type == "Phone")
                {
                    whereString += " and phone like '" + value.Trim() + "%'";
                }
                if (type == "Email")
                {
                    whereString += " and email like '" + value.Trim().ToLower() + "%'";
                }
                if (type == "Package")
                {
                    whereString += " and dealName like '" + value.Trim() + "%'";
                }
            }
            if (dateSearch)
            {
                string[] fromDateArr = fromDateString.Split('/');
                string[] toDateArr = toDateString.Split('/');
                DateTime fromDate = new DateTime(int.Parse(fromDateArr[2]), int.Parse(fromDateArr[1]), int.Parse(fromDateArr[0]), 00, 00, 00);
                DateTime toDate = new DateTime(int.Parse(toDateArr[2]), int.Parse(toDateArr[1]), int.Parse(toDateArr[0]), 23, 59, 59);
                fromDate = Util.ISTToUTC(fromDate);
                toDate = Util.ISTToUTC(toDate);
                whereString += " and createdOn between '" + fromDate.ToString() + "' and '" + toDate.ToString() + "'";
            }
            return whereString;

        }

        /// <summary>
        /// Enters the remarks of panel user and updates the status of a query.
        /// </summary>
        /// <param name="queryId">queryId</param>
        /// <param name="queryStatus">Status of query</param>
        /// <param name="remarks">remarks of panel user</param>
        /// <param name="memberId">memberId of panel user</param>
        /// <returns>Number of rows affected</returns>
        public static int Update(int queryId, WLPackageQueriesStatus queryStatus, string remarks, int memberId)
        {
            int queryUpdateReturnValue = 0;
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@queryId", queryId);
            paramList[1] = new SqlParameter("@queryStatus", queryStatus);
            paramList[2] = new SqlParameter("@remarks", remarks);
            paramList[3] = new SqlParameter("@memberId", memberId);
            try
            {
                queryUpdateReturnValue = DBGateway.ExecuteNonQuerySP(SPNames.IndiaTimesPackageQueryStatusUpdate, paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HolidayPackageQuery, Severity.High, memberId, "Error: Exception during Update() in PackageQueries.cs for queryId= " + queryId + " ,queryStatus=" + queryStatus.ToString() + " by:" + memberId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                queryUpdateReturnValue = 0;
            }
            return queryUpdateReturnValue;
        }

        /// <summary>
        /// Updates details of holiday package user.
        /// </summary>
        /// <param name="queryId">queryId</param>
        /// <param name="paxName">paxName</param>
        /// <param name="phone">phone</param>
        /// <param name="email">email</param>
        /// <param name="paxMessage">paxMessage</param>
        /// <param name="memberId">memberId</param>
        /// <param name="queryStatus">queryStatus</param>
        /// <param name="remarks">remarks</param>
        /// <returns>Number of rows affected</returns>
        public static int Update(int queryId, string paxName, string phone, string email, string paxMessage, int memberId, WLPackageQueriesStatus queryStatus, string remarks)
        {
            int queryUpdateReturnValue = 0;
            SqlParameter[] paramList = new SqlParameter[8];
            paramList[0] = new SqlParameter("@queryId", queryId);
            paramList[1] = new SqlParameter("@paxName", paxName);
            paramList[2] = new SqlParameter("@phone", phone);
            paramList[3] = new SqlParameter("@email", email);
            paramList[4] = new SqlParameter("@paxMessage", paxMessage);
            paramList[5] = new SqlParameter("@memberId", memberId);
            paramList[6] = new SqlParameter("@queryStatus", queryStatus);
            paramList[7] = new SqlParameter("@remarks", remarks);
            try
            {
                queryUpdateReturnValue = DBGateway.ExecuteNonQuerySP(SPNames.IndiaTimesPackageQueryDetailsUpdate, paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HolidayPackageQuery, Severity.High, memberId, "Error: Exception during Update() for updation of user details in PackageQueries.cs for queryId= " + queryId + " ,queryStatus=" + queryStatus.ToString() + " by:" + memberId + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                queryUpdateReturnValue = 0;
            }
            return queryUpdateReturnValue;
        }

        public static string LoadPackageQueriesPanelCSV(string whereString)
        {
            List<PackageQueriesRemarks> remarksList = new List<PackageQueriesRemarks>();
            StringBuilder csvHeaderString = new StringBuilder();
            StringBuilder csvBodyString = new StringBuilder();
            StringBuilder csvString = new StringBuilder();
            int remarksCount = 0;
            csvHeaderString.Append("Date,Name,Phone,Email,Message,Package Name,Status,");
            SqlDataReader data = null;
            using (SqlConnection connection = DBGateway.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@whereString", whereString);
                try
                {
                    data = DBGateway.ExecuteReaderSP(SPNames.GetAllPackageQueries, paramList, connection);
                    if (data.HasRows)
                    {
                        while (data.Read())
                        {
                            csvBodyString.Append(Util.UTCToIST(Convert.ToDateTime(data["createdOn"])).ToString("dd/MM/yy HH:mm") + ",");
                            if (data["paxName"] != DBNull.Value)
                            {
                                csvBodyString.Append(Convert.ToString(data["paxName"]) + ",");
                            }
                            else
                            {
                                csvBodyString.Append(" ,");
                            }
                            if (data["phone"] != DBNull.Value)
                            {
                                csvBodyString.Append(Convert.ToString(data["phone"]) + ",");
                            }
                            else
                            {
                                csvBodyString.Append(" ,");
                            }
                            if (data["email"] != DBNull.Value)
                            {
                                csvBodyString.Append(Convert.ToString(data["email"]) + ",");
                            }
                            else
                            {
                                csvBodyString.Append(" ,");
                            }
                            if (data["paxMessage"] != DBNull.Value)
                            {
                                csvBodyString.Append(Convert.ToString(data["paxMessage"]).Replace(",", " ") + ",");
                            }
                            else
                            {
                                csvBodyString.Append(" ,");
                            }
                            csvBodyString.Append(Convert.ToString(data["dealName"]) + "-" + Convert.ToString(data["Nights"]) + "/" + (Convert.ToInt32(data["Nights"]) + 1) + ",");
                            csvBodyString.Append(((WLPackageQueriesStatus)(Enum.Parse(typeof(WLPackageQueriesStatus), data["queryStatus"].ToString()))).ToString() + ",");
                            remarksList = PackageQueriesRemarks.Load(Convert.ToInt32(data["queryId"]));
                            if (remarksList.Count > 0)
                            {
                                if (remarksCount < remarksList.Count)
                                {
                                    remarksCount = remarksList.Count;
                                }
                                for (int i = 0; i < remarksList.Count; i++)
                                {
                                    csvBodyString.Append(remarksList[i].Remarks.Replace(",", " ") + ",");
                                }
                            }
                            else
                            {
                                csvBodyString.Append(" ,");
                            }
                            csvBodyString.Append("\n");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HolidayPackageQuery, Severity.High, 0, "Error: Exception during LoadPackageQueriesPanelCSV() in PackageQueries.cs for whereString= " + whereString + " | " + ex.Message + " | " + ex.InnerException + " | " + DateTime.Now, "");
                    csvBodyString.Append(" ");
                }
                finally
                {
                    data.Close();
                    connection.Close();
                }
            }
            if (remarksCount < 1)
            {
                csvHeaderString.Append("Remarks");
            }
            else
            {
                for (int i = 0; i < remarksCount; i++)
                {
                    csvHeaderString.Append("Remarks" + (i + 1) + ",");
                }
                csvHeaderString.Remove(csvHeaderString.Length - 1, 1);
            }
            csvHeaderString.Append("\n");
            csvString.Append(csvHeaderString.ToString());
            csvString.Append(csvBodyString.ToString());
            return csvString.ToString();
        }
        #endregion

    }
}
