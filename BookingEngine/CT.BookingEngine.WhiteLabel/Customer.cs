using System;
using System.Collections.Generic;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;

namespace CT.BookingEngine.WhiteLabel
{
    public class Customer
    {
        #region Private Members
        /// <summary>
        /// Unique id for each customer
        /// </summary>
        private int customerId;
        /// <summary>
        /// siteName of a Customer
        /// </summary>
        private string siteName;
        /// <summary>
        /// First name of the customer
        /// </summary>
        private string firstName;
        /// <summary>
        /// Last name of the customer
        /// </summary>
        private string lastName;
        /// <summary>
        /// mobile Number of customer
        /// </summary>
        private string mobileNo;
        /// <summary>
        /// Passport number of the customer
        /// </summary>
        private string passport;
        /// <summary>
        /// Date of birth of the customer
        /// </summary>
        private DateTime dateOfBirth;
        /// <summary>
        /// User name of the customer
        /// </summary>
        private string userName;
        /// <summary>
        /// Pasword of the customer
        /// </summary>
        private string password;
        /// <summary>
        /// MD5 encrypted Password Hash
        /// </summary>
        private byte[] passwordHash;
        /// <summary>
        /// IPAddress of Customer
        /// </summary>
        private string ipAddress;
        /// <summary>
        /// remarks entered when changing Customer status
        /// </summary>
        private string remarks;
        /// <summary>
        /// status of customer if customer is active or not
        /// </summary>
        private CustomerStatus customerStatus = CustomerStatus.Genuine;

        #endregion

        #region Public Properties

        public int CustomerId
        {
            get
            {
                return customerId;
            }
            set
            {
                customerId = value;
            }

        }
        public string SiteName
        {
            get
            {
                return siteName;
            }
            set
            {
                siteName = value;
            }
        }
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }
        public string MobileNo
        {
            get
            {
                return mobileNo;
            }
            set
            {
                mobileNo = value;
            }
        }
        public string Passport
        {
            get
            {
                return passport;
            }
            set
            {
                passport = value;
            }

        }
        public DateTime DateOfBirth
        {
            get
            {
                return dateOfBirth;
            }
            set
            {
                dateOfBirth = value;
            }

        }
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
            }

        }
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }

        }
        public byte[] PasswordHash
        {
            get
            {
                return passwordHash;
            }
        }
        public string IPAddress
        {
            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
            }
        }
        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }
        public CustomerStatus CustomerStatus
        {
            get
            {
                return customerStatus;
            }
            set
            {
                customerStatus = value;
            }
        }


        #endregion

        #region Public Methods

        public static CustomerStatus GetCustomerStatus(string userName)
        {
            ////Trace.TraceInformation("Customer.GetCustomerStatus entered : userName = " + userName);
            if (userName == null || userName.Trim().Length <= 0)
            {
                throw new ArgumentException("userName should not be null or empty string", "userName");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@userName", userName);
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetWLCustomerStatus, paramList, connection);
            CustomerStatus status = new CustomerStatus();
            using (DataTable dtWLCust = DBGateway.FillDataTableSP(SPNames.GetWLCustomerStatus, paramList))
            {
                if (dtWLCust != null && dtWLCust.Rows.Count > 0)
                {
                    DataRow data = dtWLCust.Rows[0];
                    if (data !=null && data["customerStatus"] !=DBNull.Value)
                    {
                        status = (CustomerStatus)Enum.Parse(typeof(CustomerStatus), Convert.ToString(data["customerStatus"]));
                    }
                }
                else
                {
                    throw new ArgumentException("CustomerStatus entry not found");
                }
            }
            return status;
        }

        public static int UpdateCustomerStatus(string userName, CustomerStatus customerStatus, string remarks)
        {
            ////Trace.TraceInformation("Customer.UpdateCustomerStatus entered : userName = " + userName);
            if (userName == null || userName.Trim().Length <= 0)
            {
                throw new ArgumentException("userName should not be null or empty string", "userName");
            }
            if (customerStatus.ToString().Trim().Length <= 1)
            {
                throw new ArgumentException("customerStatus should not empty string", "customerStatus");
            }
            if (remarks == null)
            {
                remarks = string.Empty;
            }
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@userName", userName);
            paramList[1] = new SqlParameter("@customerStatus", (int)customerStatus);
            paramList[2] = new SqlParameter("@remarks", remarks);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateWLCustomerStatus, paramList);
            if (rowsAffected > 0)
            {
                ////Trace.TraceInformation("Customer.UpdateCustomerStatus exited : userName = " + userName);
                return rowsAffected;
            }
            else
            {
                ////Trace.TraceInformation("Customer.UpdateCustomerStatus exited, customer status not modified : userName = " + userName);
                return 0;
            }
        }

        /// <summary>
        /// Loads customer by user name
        /// </summary>
        /// <param name="userName"></param>
        public void LoadByUserName(string userName)
        {
            ////Trace.TraceInformation("Customer.LoadByUserName entered : userName = " + userName);
            if (userName == null || userName == string.Empty)
            {
                throw new ArgumentException("userName can not be null or blank");
            }
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@userName", userName);
            SqlConnection con = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetWLCustomerByUserName, paramList, con);
            ReadData(data);
            data.Close();
            con.Close();
        }

        /// <summary>
        /// Reads the data from SqlDataReader
        /// </summary>
        /// <param name="data"></param>
        private void ReadData(SqlDataReader data)
        {
            if (data.Read())
            {
                if (data["password"] != DBNull.Value)
                {
                    passwordHash = (byte[])data["password"];
                }
                siteName = (string)data["siteName"];
                customerId = (int)data["customerId"];
                if (data["firstName"] != DBNull.Value)
                {
                    firstName = (string)data["firstName"];
                }
                else
                {
                    firstName = string.Empty;
                }
                if (data["lastName"] != DBNull.Value)
                {
                    lastName = (string)data["lastName"];
                }
                else
                {
                    lastName = string.Empty;
                }
                if (data["mobileNo"] != DBNull.Value)
                {
                    mobileNo = (string)data["mobileNo"];
                }
                else
                {
                    mobileNo = string.Empty;
                }
                if (data["passport"] != DBNull.Value)
                {
                    passport = (string)data["passport"];
                }
                else
                {
                    passport = string.Empty;
                }
                if (data["dateOfBirth"] != DBNull.Value)
                {
                    dateOfBirth = (DateTime)data["dateOfBirth"];
                }
                if (data["userName"] != DBNull.Value)
                {
                    userName = (string)data["userName"];
                }
                if (data["ipAddress"] != DBNull.Value)
                {
                    ipAddress = (string)data["ipAddress"];
                }
                if (data["remarks"] != DBNull.Value)
                {
                    remarks = (string)data["remarks"];
                }
                customerStatus = (CustomerStatus)Enum.Parse(typeof(CustomerStatus), (data["customerStatus"]).ToString()); 
            }
        }
        #endregion


        public static List<Customer> Load(int pageNo)
        {
            return Load(pageNo, "");
        }

        public static List<Customer> Load(int pageNo, string where)
        {
            int noofrecords = Convert.ToInt32(CT.Configuration.ConfigurationSystem.B2CSettingsConfig["noofrecords"]);
            List<Customer> customerlist = new List<Customer>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@rowNo", pageNo * noofrecords);
            param[1] = new SqlParameter("@noofrecords", noofrecords);
            param[2] = new SqlParameter("@whereString", where);
            SqlDataReader datareader = DBGateway.ExecuteReaderSP(SPNames.GetWLCustomers, param, connection);
            customerlist = FillCustomerList(datareader);
            datareader.Dispose();
            connection.Close();
            return customerlist;
        }

        private static List<Customer> FillCustomerList(SqlDataReader datareader)
        {
            List<Customer> customerlist = new List<Customer>();
            if (datareader.HasRows)
            {
                while (datareader.Read())
                {
                    Customer customer = new Customer();
                    customer.CustomerId = Convert.ToInt32(datareader["customerId"]);
                    customer.UserName = Convert.ToString(datareader["userName"]);
                    customer.CustomerStatus = (CustomerStatus)Enum.Parse(typeof(CustomerStatus), (datareader["customerStatus"]).ToString());
                    if (datareader["firstName"] != DBNull.Value)
                    {
                        customer.FirstName = Convert.ToString(datareader["firstName"]);
                    }
                    else
                    {
                        customer.FirstName = string.Empty;
                    }
                    if (datareader["lastName"] != DBNull.Value)
                    {
                        customer.LastName = Convert.ToString(datareader["lastName"]);
                    }
                    else
                    {
                        customer.LastName = string.Empty;
                    }
                    if (datareader["mobileNo"] != DBNull.Value)
                    {
                        customer.MobileNo = Convert.ToString(datareader["mobileNo"]);
                    }
                    else
                    {
                        customer.MobileNo = string.Empty;
                    }
                    if (datareader["ipAddress"] != DBNull.Value)
                    {
                        customer.IPAddress = Convert.ToString(datareader["ipAddress"]);
                    }
                    else
                    {
                        customer.IPAddress = string.Empty;
                    }
                    if(datareader["siteName"] != DBNull.Value)
                    {
                        customer.siteName = Convert.ToString(datareader["siteName"]);
                    }
                    else
                    {
                        customer.siteName = string.Empty;
                    }
                    customerlist.Add(customer);
                }
            }
            return customerlist;
        }
        public static string GeneratewhereString(string searchby, string Search, object agencyId)
        {
            string where=string.Empty;
            if (searchby == "Name")
            {
                where = " where patindex('%" + Search.Trim().ToUpper() + "%',firstName+' '+lastName)>0 ";
            }
            else if (searchby == "email")
                where = " where patindex('%" + Search.Trim().ToUpper() + "%',userName)>0";
            else if (searchby == "MobileNo")
                where = " where patindex('%" + Search.Trim().ToUpper() + "%',mobileNo)>0";
            else
                where = "";

            // No need to agency id in case of cozmo project as discuss with anupam sir updatd by pankaj
            //==================================================
            //else if (searchby == "Agency")
            //    where = " where AgencyId IN (Select AgencyId From Agency Where AgencyName Like '%" + Search.Trim() + "%')";
            //else
            //    where = "";

            //if (agencyId != null && (int)agencyId > 0 && where.Length > 4)
            //{
            //    where += " AND AgencyId = " + ((int)agencyId).ToString();
            //}
            //else if (agencyId != null && (int)agencyId > 0 && where.Length < 4)
            //{
            //    where += " Where AgencyId = " + ((int)agencyId).ToString();
            //}
            return where;
        }
        public static int GetPageCount()
        {
            return GetPageCount("");
        }
        public static int GetPageCount(string wherestring)
        {
            int noofrecords = Convert.ToInt32(CT.Configuration.ConfigurationSystem.B2CSettingsConfig["noofrecords"]);
            int count = 0;
            int noofpages;
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@whereString", wherestring);
            SqlDataReader datareader = DBGateway.ExecuteReaderSP(SPNames.GetWLCustomersCount, param, connection);
            if (datareader.Read())
                count = Convert.ToInt32(datareader["numberOfRows"]);
            datareader.Dispose();
            connection.Close();
            if (count % noofrecords == 0)
                noofpages = count / noofrecords;
            else
                noofpages = (count / noofrecords) + 1;
            return noofpages;
        }

        /// <summary>
        /// Loads customer by customer id
        /// </summary>
        /// <param name="customerId"></param>
        public void LoadByCustomerId(int customerId)
        {
            ////Trace.TraceInformation("Customer.LoadByCustomerId entered : customerId = " + customerId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@customerId", customerId);
            SqlConnection con = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetWLCustomerByCustomerId, paramList, con);
            ReadData(data);
            data.Close();
            con.Close();
        }
    }

}
