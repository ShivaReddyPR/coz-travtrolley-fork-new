using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using CT.Configuration;
using System.Collections;
using System.IO;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;


namespace CT.BookingEngine.WhiteLabel
{
    public class HotelBookingDetails
    {
        #region private variables
        private int hotelTransactionId;
        private string bookingId;
        private string hotelTripId;
        private int customerId;
        private string bookingRefNo;
        private string confirmationNo;
        private string hotelName;
        private WLHotelRating starRating;
        private DateTime checkInDate;
        private DateTime checkOutDate;
        private string cityRef;
        private int noOfRooms;
        private string guestTitle;
        private string guestFirstName;
        private string guestLastName;
        private bool isDomestic;
        private DateTime bookingDate;
        private DateTime lastCancellationDate;
        private HotelRoomGuest[] roomGuest;
        private HotelBookingStatus bookingStatus;
        private bool voucherStatus;
        private decimal rateofExchange;
        private string currency;
        private decimal totalRate;
        private bool isRequestSended;
        private bool isCancelled;
        private DateTime createdOn;
        private DateTime lastModifiedOn;
        #endregion

        #region public properties
        public int HotelTransactionId
        {
            get
            {
                return hotelTransactionId;
            }
            set
            {
                hotelTransactionId = value;
            }
        }
        public string BookingId
        {
            get
            {
                return bookingId;
            }
            set
            {
                bookingId = value;
            }
        }
        public string HotelTripId
        {
            get
            {
                return hotelTripId;
            }
            set
            {
                hotelTripId = value;
            }
        }
        public int CustomerId
        {
            get
            {
                return customerId;
            }
            set
            {
                customerId = value;
            }
        }
        public string BookingRefNo
        {
            get
            {
                return bookingRefNo;
            }
            set
            {
                bookingRefNo = value;
            }
        }
        public string ConfirmationNo
        {
            get
            {
                return confirmationNo;
            }
            set
            {
                confirmationNo = value;
            }
        }
        public string HotelName
        {
            get
            {
                return hotelName;
            }
            set
            {
                hotelName = value;
            }
        }
        public WLHotelRating StarRating
        {
            get
            {
                return starRating;
            }
            set
            {
                starRating = value;
            }
        }
        public DateTime CheckInDate
        {
            get
            {
                return checkInDate;
            }
            set
            {
                checkInDate = value;
            }
        }
        public DateTime CheckOutDate
        {
            get
            {
                return checkOutDate;
            }
            set
            {
                checkOutDate = value;
            }
        }
        public string CityRef
        {
            get
            {
                return cityRef;
            }
            set
            {
                cityRef = value;
            }
        }
        public int NoOfRooms
        {
            get
            {
                return noOfRooms;
            }
            set
            {
                noOfRooms = value;
            }
        }
        public string GuestTitle
        {
            get
            {
                return guestTitle;
            }
            set
            {
                guestTitle = value;
            }
        }
        public string GuestFirstName
        {
            get
            {
                return guestFirstName;
            }
            set
            {
                guestFirstName = value;
            }
        }
        public string GuestLastName
        {
            get
            {
                return guestLastName;
            }
            set
            {
                guestLastName = value;
            }
        }
        public bool IsDomestic
        {
            get
            {
                return isDomestic;
            }
            set
            {
                isDomestic = value;
            }
        }
        public DateTime BookingDate
        {
            get
            {
                return bookingDate;
            }
            set
            {
                bookingDate = value;
            }
        }
        public DateTime LastCancellationDate
        {
            get
            {
                return lastCancellationDate;
            }
            set
            {
                lastCancellationDate = value;
            }
        }
        public HotelRoomGuest[] RoomGuest
        {
            get
            {
                return roomGuest;
            }
            set
            {
                roomGuest = value;
            }
        }
        public HotelBookingStatus BookingStatus
        {
            get
            {
                return bookingStatus;
            }
            set
            {
                bookingStatus = value;
            }
        }
        public bool VoucherStatus
        {
            get
            {
                return voucherStatus;
            }
            set
            {
                voucherStatus = value;
            }
        }
        public decimal RateofExchange
        {
            get
            {
                return rateofExchange;
            }
            set
            {
                rateofExchange = value;
            }
        }
        public string Currency
        {
            get
            {
                return currency;
            }
            set
            {
                currency = value;
            }
        }
        public decimal TotalRate
        {
            get
            {
                return totalRate;
            }
            set
            {
                totalRate = value;
            }
        }
        public bool IsRequestSended
        {
            get
            {
                return isRequestSended;
            }
            set
            {
                isRequestSended = value;
            }
        }

        public bool IsCancelled
        {
            get
            {
                return isCancelled;
            }
            set
            {
                isCancelled = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method to Save Booking Details
        /// </summary>
        public void Save()
        {
            int retVal;
            if (customerId < 0)
            {
                throw new ArgumentException("customerId must be a positive integer and greater then Zero, customerId :" + customerId);
            }
            if (hotelTripId == null || hotelTripId.Length == 0)
            {
                throw new ArgumentException("hotelTripId must not be null or of Length Zero");
            }
            if (bookingId == null || bookingId.Length == 0)
            {
                throw new ArgumentException("bookingId must not be null or of Length Zero");
            }
            if (confirmationNo == null || confirmationNo.Length == 0)
            {
                throw new ArgumentException("confirmationNo must not be null or of Length Zero");
            }
            if (hotelName == null || hotelName.Length == 0)
            {
                throw new ArgumentException("hotelName must not be null or of Length Zero");
            }
            if (cityRef == null || cityRef.Length == 0)
            {
                throw new ArgumentException("hotelName must not be null or of Length Zero");
            }
            if (guestFirstName == null || guestFirstName.Length == 0)
            {
                throw new ArgumentException("guestFirstName must not be null or of Length Zero");
            }
            if (guestLastName == null || guestLastName.Length == 0)
            {
                throw new ArgumentException("guestLastName must not be null or of Length Zero");
            }
            if (currency == null || currency.Length == 0)
            {
                throw new ArgumentException("currency must not be null or of Length Zero");
            }

            if (hotelTransactionId == 0)
            {
                SqlParameter[] paramList = new SqlParameter[23];
                paramList[0] = new SqlParameter("@hotelTransactionId", SqlDbType.Int);
                paramList[1] = new SqlParameter("@bookingId", Convert.ToInt32(bookingId));
                paramList[2] = new SqlParameter("@hotelTripId", hotelTripId);
                paramList[3] = new SqlParameter("@customerId", customerId);
                if (bookingRefNo != null)
                {
                    paramList[4] = new SqlParameter("@bookingRefNo", bookingRefNo);
                }
                else
                {
                    paramList[4] = new SqlParameter("@bookingRefNo", DBNull.Value);
                }
                paramList[5] = new SqlParameter("@confirmationNo", confirmationNo);
                paramList[6] = new SqlParameter("@hotelName", hotelName);
                paramList[7] = new SqlParameter("@starRating", (int)starRating);
                paramList[8] = new SqlParameter("@checkInDate", checkInDate);
                paramList[9] = new SqlParameter("@checkOutDate", checkOutDate);
                paramList[10] = new SqlParameter("@cityRef", cityRef);
                paramList[11] = new SqlParameter("@noOfRooms", noOfRooms);
                if (guestTitle != null)
                {
                    paramList[12] = new SqlParameter("@guestTitle", guestTitle);
                }
                else
                {
                    paramList[12] = new SqlParameter("@guestTitle", DBNull.Value);
                }
                paramList[13] = new SqlParameter("@guestFirstName", guestFirstName);
                paramList[14] = new SqlParameter("@guestLastName", guestLastName);
                paramList[15] = new SqlParameter("@isDomestic", isDomestic);
                paramList[16] = new SqlParameter("@bookingDate", bookingDate);
                paramList[17] = new SqlParameter("@lastCancellationDate", lastCancellationDate);
                paramList[18] = new SqlParameter("@bookingStatus", (int)bookingStatus);
                paramList[19] = new SqlParameter("@voucherStatus", voucherStatus);
                paramList[20] = new SqlParameter("@rateofExchange", rateofExchange);
                paramList[21] = new SqlParameter("@currency", currency);
                paramList[22] = new SqlParameter("@totalRate", totalRate);
                paramList[0].Direction = ParameterDirection.Output;
                using (System.Transactions.TransactionScope updateTransaction =
                new System.Transactions.TransactionScope())
                {
                    retVal = DBGateway.ExecuteNonQuerySP(SPNames.AddIndiaTimesHotelBookingDetails, paramList);
                    hotelTransactionId = (int)paramList[0].Value;
                    if (hotelTransactionId == 0)
                    {
                        throw new ArgumentException("This Refrence Number  : " + confirmationNo + " entry already exists");
                    }
                    else
                    {
                        for (int i = 0; i < noOfRooms; i++)
                        {
                            HotelRoomGuest room = new HotelRoomGuest();
                            room.AdultCount = roomGuest[i].AdultCount;
                            room.ChildCount = roomGuest[i].ChildCount;
                            room.RoomName = roomGuest[i].RoomName;
                            room.HotelTransactionId = hotelTransactionId;
                            room.Save();
                        }
                    }
                    updateTransaction.Complete();
                }
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@hotelTransactionId", hotelTransactionId);
                paramList[1] = new SqlParameter("@isRequestSended", isRequestSended);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateIndiaTimesHotelBookingDetails, paramList);
                if (rowsAffected <= 0)
                {
                    throw new ArgumentException("Booking details Refrence Number  : " + confirmationNo + " not updated.");
                }
            }
        }

        public static void EmailHotelVoucher(string from, string replyTo, List<string> toArray, string subject, HotelItinerary itineary, int agencyId, string tripId, string filePath)
        {
            if (Convert.ToBoolean(ConfigurationSystem.Email["isSendEmail"]))
            {
                HotelRoom room = new HotelRoom();
                room = itineary.Roomtype[0];
                string cityName = string.Empty;
                cityName = itineary.CityRef;
                string phoneString = string.Empty;
                AgentMaster agency = new AgentMaster(agencyId);
                if (!string.IsNullOrEmpty(agency.Phone1))
                {
                    phoneString = agency.Phone1;
                }
                //This is a global hash table which we send in Email.Send which replaces 
                //hashvariables with values(code which is not in loop)
                Hashtable globalHashTable = new Hashtable();
                //globalHashTable.Add("logo", logo);
                globalHashTable.Add("agencyName", agency.Name);
                globalHashTable.Add("AaddressLine1", agency.Address);
                string agentSpan = "<span style=\"width:100%;float:left;margin:0px;font-size:12px;font-weight:normal;padding:0px;color:#000;\">";
                //if (agency.Address.Line2 != null && agency.Address.Line2.Length != 0)
                //{
                //    globalHashTable.Add("AaddressLine2", agentSpan + agency.Address.Line2 + "</span>");
                //}
                //else
                {
                    globalHashTable.Add("AaddressLine2", "");
                }
                if (!string.IsNullOrEmpty(agency.City ))
                {
                    globalHashTable.Add("AcityName", agentSpan + agency.City+ "</span>");
                }
                else
                {
                    globalHashTable.Add("AcityName", "");
                }
                if (!string.IsNullOrEmpty(agency.POBox))
                {
                    globalHashTable.Add("AaddressPin", agentSpan + agency.POBox+ "</span>");
                }
                else
                {
                    globalHashTable.Add("AaddressPin", "");
                }

                if (!string.IsNullOrEmpty(agency.Phone2 ))
                {
                    globalHashTable.Add("AaddressPhone", agentSpan + "Phone:" + agency.Phone2 + "</span>");
                }
                else
                {
                    globalHashTable.Add("AaddressPhone", "");
                }
                if (!string.IsNullOrEmpty(agency.Fax))
                {
                    globalHashTable.Add("agencyFax", agentSpan + "Fax:" + agency.Fax + "</span>");
                }
                else
                {
                    globalHashTable.Add("agencyFax", "");
                }
                if (!string.IsNullOrEmpty(agency.Email1))
                {
                    globalHashTable.Add("agencyEmail", agentSpan + "Email: " + agency.Email1 + "</span>");
                }
                else
                {
                    globalHashTable.Add("agencyEmail", "");
                }
                globalHashTable.Add("HotelName", itineary.HotelName);
                //string rating=string.Empty;
                //rating+="<em>";
                //for (int j = 0; j < Convert.ToInt16(itineary.Rating); j++)
                //{
                // rating+=" <b><img src=\"Images/star.gif\" alt=\"star\" /></b>";
                // }
                //rating += "</em>";
                //globalHashTable.Add("Rating", rating);
                globalHashTable.Add("AddressLine1", itineary.HotelAddress1);
                globalHashTable.Add("tripIdString", "<b>Trip ID :</b> " + tripId);
                string adLine2 = string.Empty;
                if (itineary.HotelAddress2 != null && itineary.HotelAddress2.Length > 0)
                {
                    adLine2 += "<span style=\"width:100%;float:left;margin:0px;font-size:12px;color:#000;font-weight:normal;padding:0px;\">";
                    adLine2 += itineary.HotelAddress2;
                    adLine2 += "</span>";
                }
                globalHashTable.Add("AddressLine2", adLine2);
                globalHashTable.Add("HotelCity", itineary.CityRef);
                string Rating = string.Empty;
                if (Convert.ToInt16(itineary.Rating) > 0)
                {
                    Rating = "( " + Convert.ToInt16(itineary.Rating) + " Star )";
                }
                globalHashTable.Add("Rating", Rating);
                if (itineary.IsDomestic)
                {
                    globalHashTable.Add("PassengerName", itineary.HotelPassenger.Title.ToUpper() + " " + itineary.HotelPassenger.Firstname.ToUpper() + " " + itineary.HotelPassenger.Lastname.ToUpper());
                }
                else
                {
                    string paxString = string.Empty;
                    for (int i = 0; i < itineary.NoOfRooms; i++)
                    {
                        foreach (HotelPassenger hpaxInfo in itineary.Roomtype[i].PassenegerInfo)
                        {
                            string name = hpaxInfo.Firstname + hpaxInfo.Lastname;
                            if (name.Length > 0)
                            {
                                paxString += "    Room " + (i + 1) + " :<b>" + hpaxInfo.Title + "  " + hpaxInfo.Firstname + "  " + hpaxInfo.Lastname + " - " + hpaxInfo.PaxType.ToString();
                                if (hpaxInfo.PaxType ==  HotelPaxType.Child)
                                {
                                    paxString += "(" + hpaxInfo.Age + " Yrs)";
                                }
                                paxString += "</b> <br />";
                            }
                        }
                    }
                    globalHashTable.Add("PassengerName", paxString);
                }
                if (itineary.Source == HotelBookingSource.HotelBeds)
                {
                    globalHashTable.Add("AgencyRefNo", "Agency Ref:" + itineary.AgencyReference);
                }
                else
                {
                    globalHashTable.Add("AgencyRefNo", "");
                }
                globalHashTable.Add("ConfirmationNo", itineary.ConfirmationNo);
                globalHashTable.Add("CheckIn", itineary.StartDate.ToString("dd MMM yyyy"));
                globalHashTable.Add("CheckOut", itineary.EndDate.ToString("dd MMM yyyy"));
                string roomDetails = string.Empty;
                if (itineary.IsDomestic)
                {

                    //globalHashTable.Add("RoomName", room.RoomName);
                    ////globalHashTable.Add("RoomStatus", "Status : " + bookingResponse.BookingDetail.Status.ToString());
                    string amenitys = string.Empty;
                    if (room.Ameneties.Count > 0)
                    {
                        amenitys += "Incl:";
                        foreach (string amenity in room.Ameneties)
                        {
                            amenitys += amenity + " ";
                        }
                    }
                    //globalHashTable.Add("Ameneties", amenitys);
                    //globalHashTable.Add("NoofRooms", bookingResponse.BookingDetail.NoOfRooms);
                    int adults = 0, children = 0;
                    for (int k = 0; k < itineary.NoOfRooms; k++)
                    {
                        adults += itineary.Roomtype[k].AdultCount;
                        children += itineary.Roomtype[k].ChildCount;
                    }
                    //globalHashTable.Add("paxCount", (adults + children).ToString());
                    string paxInfo = string.Empty;
                    for (int i = 1; i <= itineary.NoOfRooms; i++)
                    {
                        paxInfo += "   <p>";
                        paxInfo += " <span style=\"width:100px;float:left;margin:0px;padding:0px;font-size:12px;color:#000;\">Room" + i + ": </span> ";
                        paxInfo += " <span style=\"width:170px;float:left;margin:0px;padding:0px;font-size:12px;font-weight:bold;color:#000;\"> ";
                        paxInfo += itineary.Roomtype[i - 1].AdultCount + " <i style=\"font-style:normal; font-weight:normal;\">Adult(s)  </i>";
                        if (itineary.Roomtype[i - 1].ChildCount > 0)
                        {
                            paxInfo += itineary.Roomtype[i - 1].ChildCount + " <i style=\"font-style:normal; font-weight:normal;\">Children</i>";
                        }
                        paxInfo += "   </span></p>     ";
                    }
                    //globalHashTable.Add("paxInfo", paxInfo);


                    roomDetails += "<div style=\"width:270px;float:left;margin:0px;padding:0 0 7px 10px;\">";
                    roomDetails += "<p style=\"width:100%;float:left;margin:0px;padding:7px 0 0 0;_padding:7px 0 0 0;\">";
                    roomDetails += "<span style=\"width:100px;float:left;margin:0px;padding:0px;font-size:12px;color:#000;\"><b>Room Type</b> -</span>";
                    roomDetails += "<span style=\"width:170px;float:left;margin:0px;padding:0px;font-size:12px;font-weight:bold;color:#000;\">" + room.RoomName + "<br /><b>" + amenitys + "</b></span>";
                    roomDetails += "</p>";
                    roomDetails += "<p style=\"width:100%;float:left;margin:0px;padding:7px 0 0 0;_padding:7px 0 0 0;\">";
                    roomDetails += "<span style=\"width:100px;float:left;margin:0px;padding:0px;font-size:12px;color:#000;\">No. of Rooms -</span>";
                    roomDetails += "<span style=\"width:170px;float:left;margin:0px;padding:0px;font-size:12px;font-weight:bold;color:#000;\">" + itineary.NoOfRooms + "</span>";
                    roomDetails += "</p>";
                    roomDetails += "<p style=\"width:100%;float:left;margin:0px;padding:7px 0 0 0;_padding:7px 0 0 0;\">";
                    roomDetails += "<span style=\"width:100px;float:left;margin:0px;padding:0px;font-size:12px;color:#000;\">No. of Guests -</span>";
                    roomDetails += "<span style=\"width:170px;float:left;margin:0px;padding:0px;font-size:12px;font-weight:bold;color:#000;\"><b class=\"adults\"> <b>" + (adults + children).ToString() + "</b></span><p>" + paxInfo + "</p>";
                    roomDetails += "</p>";
                    roomDetails += "<span style=\"width:100%;float:left;margin:0px;padding:34px 0 0px 0;_padding:13px 0 0px 0;font-size:13px;\">";

                    roomDetails += "</span>";
                    roomDetails += "</div>";
                }
                else
                {
                    for (int iRoom = 0; iRoom < itineary.Roomtype.Length; iRoom++)
                    {
                        int noOfRooms = 1;
                        HotelRoom hRoom = itineary.Roomtype[iRoom];
                        string amenitys = string.Empty;
                        if (hRoom.Ameneties.Count > 0)
                        {
                            amenitys += "Incl:";
                            foreach (string amenity in hRoom.Ameneties)
                            {
                                amenitys += amenity + " ";
                            }
                        }
                        int adults = 0, children = 0;
                        for (int k = 0; k < noOfRooms; k++)
                        {
                            adults += hRoom.AdultCount;
                            children += hRoom.ChildCount;
                        }
                        string paxInfo = string.Empty;
                        for (int i = 1; i <= noOfRooms; i++)
                        {
                            paxInfo += "   <p>";
                            paxInfo += " <span style=\"width:100px;float:left;margin:0px;padding:0px;font-size:12px;color:#000;\">Room" + i + ": </span> ";
                            paxInfo += " <span style=\"width:170px;float:left;margin:0px;padding:0px;font-size:12px;font-weight:bold;color:#000;\"> ";
                            paxInfo += hRoom.AdultCount + " <i style=\"font-style:normal; font-weight:normal;\">Adult(s)  </i>";
                            if (hRoom.ChildCount > 0)
                            {
                                paxInfo += hRoom.ChildCount + " <i style=\"font-style:normal; font-weight:normal;\">Children</i>";
                            }
                            paxInfo += "   </span></p>     ";
                        }

                        if (iRoom == itineary.Roomtype.Length - 1)
                        {
                            roomDetails += "<div style=\"width:270px;float:left;margin:0px;padding:0 0 7px 10px;\">";
                        }
                        else
                        {
                            roomDetails += "<div style=\"width:270px;float:left;margin:0px;padding:0 0 7px 10px;border-bottom:solid 1px #000;\">";
                        }
                        roomDetails += "<p style=\"width:100%;float:left;margin:0px;padding:7px 0 0 0;_padding:7px 0 0 0;\">";
                        roomDetails += "<span style=\"width:100px;float:left;margin:0px;padding:0px;font-size:12px;color:#000;\"><b>Room Type</b> -</span>";
                        roomDetails += "<span style=\"width:170px;float:left;margin:0px;padding:0px;font-size:12px;font-weight:bold;color:#000;\">" + hRoom.RoomName + "<br /><b>" + amenitys + "</b></span>";
                        roomDetails += "</p>";
                        roomDetails += "<p style=\"width:100%;float:left;margin:0px;padding:7px 0 0 0;_padding:7px 0 0 0;\">";
                        roomDetails += "<span style=\"width:100px;float:left;margin:0px;padding:0px;font-size:12px;color:#000;\">No. of Rooms -</span>";
                        roomDetails += "<span style=\"width:170px;float:left;margin:0px;padding:0px;font-size:12px;font-weight:bold;color:#000;\">" + noOfRooms + "</span>";
                        roomDetails += "</p>";
                        roomDetails += "<p style=\"width:100%;float:left;margin:0px;padding:7px 0 0 0;_padding:7px 0 0 0;\">";
                        roomDetails += "<span style=\"width:100px;float:left;margin:0px;padding:0px;font-size:12px;color:#000;\">No. of Guests -</span>";
                        roomDetails += "<span style=\"width:170px;float:left;margin:0px;padding:0px;font-size:12px;font-weight:bold;color:#000;\"><b class=\"adults\"> <b>" + (adults + children).ToString() + "</b></span><p>" + paxInfo + "</p>";
                        roomDetails += "</p>";
                        roomDetails += "<span style=\"width:100%;float:left;margin:0px;padding:34px 0 0px 0;_padding:13px 0 0px 0;font-size:13px;\">";

                        roomDetails += "</span>";
                        roomDetails += "</div>";
                    }
                }
                globalHashTable.Add("RoomDetails", roomDetails);
                System.TimeSpan diffResult = itineary.EndDate.Subtract(itineary.StartDate);
                globalHashTable.Add("NoofDays", diffResult.Days.ToString());
                string ruleString = string.Empty;
                if (itineary.HotelPolicyDetails != null && itineary.HotelPolicyDetails.Length > 0)
                {
                    string[] tempStr = itineary.HotelPolicyDetails.Split('|');
                    ruleString += "<p style=\"width:100%;float:left;margin:0px;padding:15px 0 0 0;font-size:14px;font-weight:bold;color:#000;\">Hotel Rules and Regulations</p><ul style=\"width:540px;float:left;margin:0px;padding:7px 5px 0 30px;display:inline;\">";
                    for (int i = 0; i < tempStr.Length - 1; i++)
                    {
                        if (tempStr[i].Trim() != "")
                        {
                            ruleString += "<li style=\"margin:0px;padding:0;list-style:disc;list-style-position:outside;font-size:12px;color:#000;\">";
                            ruleString += tempStr[i];
                            ruleString += "</li>";
                        }
                    }
                    ruleString += "   </ul>";
                }
                globalHashTable.Add("HotelRules", ruleString);
                ruleString = string.Empty;
                if (itineary.HotelCancelPolicy != null)
                {
                    string[] tempStr = itineary.HotelCancelPolicy.Split('|');
                    ruleString += "<p style=\"width:100%;float:left;margin:0px;padding:15px 0 0 0;font-size:14px;font-weight:bold;color:#000;\">Cancellation and Changes</p><ul style=\"width:540px;float:left;margin:0px;padding:7px 5px 0 30px;display:inline;\">";

                    for (int i = 0; i < tempStr.Length - 1; i++)
                    {
                        if (tempStr[i].Trim() != "")
                        {
                            ruleString += " <li style=\"margin:0px;padding:0;list-style:disc;list-style-position:outside;font-size:12px;color:#000;\">";
                            ruleString += tempStr[i];
                            ruleString += "</li>";
                        }
                    }
                    ruleString += "</ul>";
                }
                string aotString = string.Empty;
                if (itineary.Source == HotelBookingSource.GTA)
                {
                    GTACity cityInfo = new GTACity();
                    cityInfo.Load(itineary.CityCode);
                    Dictionary<string, string> aotList = new Dictionary<string, string>();
                    aotList = cityInfo.LoadGTAAOTNumber(cityInfo.CountryCode, cityInfo.CountryName);
                    if (aotList != null && aotList.Count > 0)
                    {
                        string country = string.Empty;
                        string city = string.Empty;
                        string intlCall = string.Empty;
                        string nationalCall = string.Empty;
                        string localCall = string.Empty;
                        string officeHours = string.Empty;
                        string emergencyNo = string.Empty;
                        string language = string.Empty;
                        if (aotList["Country"] != null)
                        {
                            country = aotList["Country"];
                        }
                        if (aotList["City"] != null)
                        {
                            city = aotList["City"];
                        }
                        if (aotList["Language"] != null)
                        {
                            language = aotList["Language"];
                        }
                        if (aotList["OfficeHours"] != null)
                        {
                            officeHours = aotList["OfficeHours"];
                        }
                        if (aotList["InternationalNo"] != null)
                        {
                            intlCall = aotList["InternationalNo"];
                        }
                        if (aotList["LocalNo"] != null)
                        {
                            localCall = aotList["LocalNo"];
                        }
                        if (aotList["NationalNo"] != null)
                        {
                            nationalCall = aotList["NationalNo"];
                        }
                        if (aotList["EmergencyNo"] != null)
                        {
                            emergencyNo = aotList["EmergencyNo"];
                        }

                        aotString += "<div style=\"width:100%; float:left; padding:10px 0 0;\"> <table border=\"1\">";
                        aotString += "                <tr>";
                        aotString += "                     <td>Country</td>";
                        aotString += "                    <td>Contact</td>";
                        aotString += "                   <td>Intl Call</td>";
                        aotString += "         <td>Domestic Call</td>";
                        aotString += "                    <td>Local Call</td>  ";
                        aotString += "                     <td>Office Hours</td>";
                        aotString += "                      <td>Emergency</td>";
                        aotString += "                       <td>Language</td>";
                        aotString += "              </tr>";
                        aotString += "                <tr>";
                        aotString += "                     <td>" + country + "</td>";
                        aotString += "                     <td>" + city + "</td>";
                        aotString += "                    <td>" + intlCall + "</td>";
                        aotString += "                   <td>" + nationalCall + "</td>";
                        aotString += "                   <td>" + localCall + " </td>";
                        aotString += "                   <td>" + officeHours + "</td>";
                        aotString += "                   <td>" + emergencyNo + "</td>";
                        aotString += "                  <td>" + language + "</td>";
                        aotString += "            </tr>";
                        aotString += "   </table></div>";

                    }
                }
                globalHashTable.Add("HotelAOTContact", aotString);
                globalHashTable.Add("HotelCancelPolicy", ruleString);
                ruleString = string.Empty;
                //if (itineary.Source == HotelBookingSource.GTA)
                //{
                //    ruleString += "<p>Hotel Map</p><p style=\"width:100%;text-align:center;\">";
                //    //ruleString += "<a href=\""+itineary.Map +"\" target=\"_blank\">Click here</a></p>";
                //    if (bookingResponse.BookingDetail.Map != null && bookingResponse.BookingDetail.Map.Length > 0)
                //    {
                //        ruleString += "To view the map click on this Link" + bookingResponse.BookingDetail.Map + "</p>";
                //    }
                //    else
                //    {
                //        ruleString += "Map Not Available</p>";
                //    }
                //}
                globalHashTable.Add("HotelMap", ruleString);

                PriceType priceType;
                //Get Hotel source commission type
                HotelSource hSource = new HotelSource();
                hSource.Load(itineary.Source.ToString());

                //since there are two enums of similar type where one is in BookingEngine(PriceAccounts) & other in CoreLogic(Enumerator).
                if (hSource.FareTypeId == FareType.Net)
                {
                    priceType = PriceType.NetFare;
                }
                else
                {
                    priceType = PriceType.PublishedFare;
                }
                decimal totalRoomPrice = 0;
                if (priceType == PriceType.PublishedFare)
                {
                    totalRoomPrice = itineary.Roomtype[0].Price.PublishedFare;
                }
                else if (priceType == PriceType.NetFare)
                {
                    totalRoomPrice = itineary.Roomtype[0].Price.NetFare + itineary.Roomtype[0].Price.Markup;
                }
                globalHashTable.Add("PublishedFare", (totalRoomPrice + itineary.Roomtype[0].Price.SeviceTax + itineary.Roomtype[0].Price.WLCharge - itineary.Roomtype[0].Price.WhiteLabelDiscount).ToString("0.00"));
                globalHashTable.Add("Tax", itineary.Roomtype[0].Price.Tax.ToString("0.00"));
                globalHashTable.Add("TotalFare", Convert.ToDouble(totalRoomPrice + itineary.Roomtype[0].Price.SeviceTax + itineary.Roomtype[0].Price.WLCharge - itineary.Roomtype[0].Price.WhiteLabelDiscount + itineary.Roomtype[0].Price.Tax).ToString("0.00"));
                string specialReq = string.Empty;
                if (itineary.SpecialRequest.Length > 0)
                {
                    specialReq = "<p style=\"width:100%;float:left;margin:0px;padding:15px 0 0 0;font-size:14px;font-weight:bold;color:#000;\">Special Request <label style=\"margin:0px;padding:0 0 0 10px;font-size:11px;font-weight:normal;color:#555;\">(We will forward your requests to the property, but we can't guarantee that your requests will be honoured)</label></p><span style=\"width:560px;float:left;margin:0px;padding:7px 0 0 10px;font-size:12px;color:#000;\">" + itineary.SpecialRequest + "</span>";
                    globalHashTable.Add("SpecialRequest", specialReq);
                }
                else
                {
                    globalHashTable.Add("SpecialRequest", specialReq);
                }
                string flightInfo = string.Empty;
                if (itineary.FlightInfo.Length > 0)
                {
                    flightInfo = "<p style=\"width:100%;float:left;margin:0px;padding:15px 0 0 0;font-size:14px;font-weight:bold;color:#000;\">Flight Details <label style=\"margin:0px;padding:0 0 0 10px;font-size:11px;font-weight:normal;color:#555;\"></label></p><span style=\"width:560px;float:left;margin:0px;padding:7px 0 0 10px;font-size:12px;color:#000;\">" + itineary.FlightInfo + "</span>";
                    globalHashTable.Add("FlightInfo", flightInfo);
                }
                else
                {
                    globalHashTable.Add("FlightInfo", flightInfo);
                }
                if (itineary.VatDescription != null)
                {
                    globalHashTable.Add("vatInfo", itineary.VatDescription + ", acting as agent for the hotel operating company, details of which can be provided upon request.");
                }
                else
                {
                    globalHashTable.Add("vatInfo", "");
                }
                StreamReader sr = new StreamReader(filePath);
                string loopString = string.Empty;

                bool loopStarts = false;
                //string contains the code before loop 
                string startString = string.Empty;
                //string contains the code after loop
                string endString = string.Empty;
                bool loopEnds = false;
                #region seperate out the loop code and the other code
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (line.IndexOf("%loopEnds%") >= 0 || loopEnds)
                    {
                        loopEnds = true;
                        loopStarts = false;
                        if (line.IndexOf("%loopEnds%") >= 0)
                        {
                            line = sr.ReadLine();
                        }
                        endString += line;
                    }
                    if (line.IndexOf("%loopStarts%") >= 0 || loopStarts)
                    {
                        if (line.IndexOf("%loopStarts%") >= 0)
                        {
                            line = sr.ReadLine();
                        }
                        loopString += line;
                        loopStarts = true;
                    }
                    else
                    {
                        if (!loopEnds)
                        {
                            startString += line;
                        }
                    }
                }
                #endregion

                string fullString = startString;
                try
                {
                    Email.Send(from, replyTo, toArray, subject, fullString, globalHashTable);
                }
                catch (System.Net.Mail.SmtpException ex)
                {
                    Audit.Add(EventType.Email, Severity.Normal, 0, "Smtp is unable to send the message", "");
                    throw new System.Net.Mail.SmtpException(ex.Message);
                }
            }
        }
        #endregion
    }

    public class HotelRoomGuest
    {
        #region private variables
        private int roomId;
        private int hotelTransactionId;
        private int adultCount;
        private int childCount; // - Optional
        private string roomName;
        #endregion

        #region public properties
        public int RoomId
        {
            get { return roomId; }
            set { roomId = value; }
        }
        public int HotelTransactionId
        {
            get { return hotelTransactionId; }
            set { hotelTransactionId = value; }
        }
        public int AdultCount
        {
            get { return adultCount; }
            set { adultCount = value; }
        }
        public int ChildCount
        {
            get { return childCount; }
            set { childCount = value; }
        }
        public string RoomName
        {
            get { return roomName; }
            set { roomName = value; }
        }
        #endregion

        #region public Methods
        /// <summary>
        /// Method to Save Hotel Room Details
        /// </summary>
        public void Save()
        {
            int retVal;
            if (hotelTransactionId <= 0)
            {
                throw new ArgumentException("hotelTransactionId must be a positive integer and greater then Zero, hotelTransactionId :" + hotelTransactionId);
            }
            if (adultCount <= 0)
            {
                throw new ArgumentException("adultCount should have positive non zero value");
            }
            if (roomName == null || roomName.Length == 0)
            {
                throw new ArgumentException("roomName must not be null or of Length Zero");
            }
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@roomId", roomId);
            paramList[1] = new SqlParameter("@hotelTransactionId", hotelTransactionId);
            paramList[2] = new SqlParameter("@adultCount", adultCount);
            paramList[3] = new SqlParameter("@childCount", childCount);
            paramList[4] = new SqlParameter("@roomName", roomName);
            paramList[0].Direction = ParameterDirection.Output;
            retVal = DBGateway.ExecuteNonQuerySP(SPNames.AddIndiaTimesHotelRoomDetails, paramList);
            roomId = (int)paramList[0].Value;
            if (roomId <= 0)
            {
                throw new ArgumentException("This roomDetails not saved, hotelTransactionId : " + hotelTransactionId);
            }
        }

        private static HotelRoomGuest[] ReadHotelRoomData(DataTable table)
        {
            int count = table.Rows.Count;
            HotelRoomGuest[] hrgDetails = new HotelRoomGuest[count];
            int i = 0;
            foreach (DataRow row in table.Rows)
            {
                hrgDetails[i] = new HotelRoomGuest();
                hrgDetails[i].roomId = Convert.ToInt32(row["roomId"]);
                hrgDetails[i].hotelTransactionId = Convert.ToInt32(row["hotelTransactionId"]);
                hrgDetails[i].adultCount = Convert.ToInt32(row["adultCount"]);
                hrgDetails[i].childCount = Convert.ToInt32(row["childCount"]);
                hrgDetails[i].roomName = Convert.ToString(row["roomName"]);
                i++;
            }
            return hrgDetails;
        }

        public static List<HotelRoomGuest> GetRoomGuestDetailsForBooking(int hotelTransactionId)
        {
            List<HotelRoomGuest> roomGuestDetails = new List<HotelRoomGuest>();
            if (hotelTransactionId <= 0)
            {
                throw new ArgumentException("hotelTransactionId must not be less then or equal to Zero");
            }
            SqlDataReader datareader;
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelTransactionId", hotelTransactionId);
            datareader = DBGateway.ExecuteReaderSP(SPNames.GetRoomGuestDetailsForIndiaTimesHotelBooking, paramList, connection);
            roomGuestDetails = ReadHotelGuestData(datareader);
            datareader.Close();
            connection.Close();
            return roomGuestDetails;
        }

        private static List<HotelRoomGuest> ReadHotelGuestData(SqlDataReader data)
        {
            List<HotelRoomGuest> rgDetails = new List<HotelRoomGuest>();
            while (data.Read())
            {
                HotelRoomGuest rgDetail = new HotelRoomGuest();
                rgDetail.roomId = Convert.ToInt32(data["roomId"]);
                rgDetail.hotelTransactionId = Convert.ToInt32(data["hotelTransactionId"]);
                rgDetail.adultCount = Convert.ToInt32(data["adultCount"]);
                rgDetail.childCount = Convert.ToInt32(data["childCount"]);
                rgDetail.roomName = Convert.ToString(data["roomName"]);
                rgDetails.Add(rgDetail);
            }
            return rgDetails;
        }

        #endregion
    }

}
