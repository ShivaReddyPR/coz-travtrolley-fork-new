using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Configuration;
using System.Collections;
using System.Web;
using CT.BookingEngine;
using CT.Core;
using CT.Configuration;
using System.IO;

namespace CT.BookingEngine
{
    [Serializable]
    public class HotelRoomFareBreakDown
    {
        int roomId;
        DateTime date;
        decimal roomPrice;

        public int RoomId
        {
            get { return roomId; }
            set { roomId = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public decimal RoomPrice
        {
            get { return roomPrice; }
            set { roomPrice = value; }
        }

        public void Save()
        {
            // TODO: validate that the fields are properly set before calling the SP
            Trace.TraceInformation("HotelRoomFareBreakDown.Save entered.");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@roomId", roomId);
            paramList[1] = new SqlParameter("@date", date);
            paramList[2] = new SqlParameter("@roomPrice", roomPrice);
           
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddHotelFareBreakDown, paramList);
              
            Trace.TraceInformation("HotelRoomFareBreakDown.Save exiting");
        }

        public static HotelRoomFareBreakDown[] Load(int roomId)
        {
            Trace.TraceInformation("HotelRoomFareBreakDown.Load entered : roomId = " + roomId);
            if (roomId <= 0)
            {
                throw new ArgumentException("RoomId should be positive integer", "roomId");
            }
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@roomId", roomId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelFareBreakDownByRoomId, paramList);

            List<HotelRoomFareBreakDown> hotelRoomsFBD = new List<HotelRoomFareBreakDown>();
            while (data.Read())
            {
                HotelRoomFareBreakDown hroomFareBD = new HotelRoomFareBreakDown();
                hroomFareBD.roomId = Convert.ToInt32(data["roomId"]);
                hroomFareBD.date = Convert.ToDateTime(data["date"]);
                hroomFareBD.roomPrice = Convert.ToDecimal(data["roomPrice"]);

                hotelRoomsFBD.Add(hroomFareBD);
            }
            data.Close();
            //////connection.Close();

            HotelRoomFareBreakDown[] hotelRoomFBDArray = hotelRoomsFBD.ToArray();
            Trace.TraceInformation("HotelRoomFareBreakDown.Load exiting.");

            return hotelRoomFBDArray;
        }
        /// <summary>
        /// This method is used to delete the FareBreakUp information
        /// </summary>
        /// <param name="roomId"></param>
        public void DeleteFareBreakDown(int roomId)
        {
            Trace.TraceInformation("HotelRoomFareBreakDown.DeleteFareBreakDown entered.");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@roomId", roomId);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.DeleteFareBreakDown, paramList);
            Trace.TraceInformation("HotelRoomFareBreakDown.DeleteFareBreakDown exiting");
        }
    }
}
