using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Configuration;
using System.Collections;
using System.Web;
using CT.BookingEngine;
using CT.Core;
using CT.Configuration;
using System.IO;

namespace CT.BookingEngine
{
    public class DOTWCountry
    {
        /// <summary>
        /// Private variables
        /// </summary>

        private string countryCode;
        private string countryName;
        private string regionName;
        private string regionCode;
        private DateTime  updatedTime;
        private int index;
        /// <summary>
        /// Public Properties
        /// </summary>
        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }

        }
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }
        public string RegionName
        {
            get { return regionName; }
            set { regionName = value; }
        }
        public string RegionCode
        {
            get { return regionCode; }
            set { regionCode = value; }
        }
        public DateTime UpdatedTime
        {
            get { return updatedTime; }
            set { updatedTime = value; }
        }
        public int Index
        {
            get { return index; }
            set { index = value; }
        }
        /// <summary>
        /// This Method  is sued to Save the GTA city Codes
        /// </summary>
        public void Save()// PROC to be created -- todo
        {
            Trace.TraceInformation("GTACity.Save entered.");

            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@countryCode", countryCode);
            paramList[1] = new SqlParameter("@countryName", countryName);
            paramList[2] = new SqlParameter("@regionCode", regionCode);
            paramList[3] = new SqlParameter("@regioName", regionName);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddGTACity, paramList);
            Trace.TraceInformation("DOTWCity.Save exiting");

        }
       
        /// <summary>
        /// This Method is used to get All country Name of DOTW
        /// </summary>
        /// <returns></returns>
        public Dictionary<string,string> GetAllCountries()
        {
            Trace.TraceInformation("DOTWCity.GetAllCountries Entered");
            SqlParameter[] paramlist = new SqlParameter[0];
            //SqlConnection connection = DBGateway.GetConnection();
            //List<string> countryList = new List<string>();
            Dictionary<string, string> countryList = new Dictionary<string, string>();
            SqlDataReader datareader = DBGateway.ExecuteReaderSP(SPNames.GetDOTWCountries, paramlist);
            while (datareader.Read())
            {
                countryList.Add(datareader["countryCode"].ToString(),datareader["countryName"].ToString());
            }
            datareader.Close();
            
//            connection.Close();
            Trace.TraceInformation("DOTWCity.GetAllCountries exiting");
            return countryList;

        }
        
       
    }
}
