using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;

namespace CT.BookingEngine
{
    public enum InvoiceStatus
    {
        Raised = 1,
        PartiallyPaid = 2,
        Paid = 3,
        Inactive = 4,
        CreditNote = 5
    }

    /// <summary>
    /// it is used for itemTypeId
    /// </summary>
    public enum InvoiceItemTypeId
    {
        //TODO: Use it instead of using itemTypeId as Hardcoded
        Ticketed = 1,
        Offline = 2,
        HotelBooking = 3,
        ICCIInsuranceBooking = 4,
        TransferBooking = 5,
        SightseeingBooking = 6,
        TrainBooking = 7,
        Misc = 8,
        InsuranceBooking = 9,
        MobileRecharge = 10,
        TrainRefund = 11
    }


    public class Invoice
    {
        /// <summary>
        /// Unique number of an invoice
        /// </summary>
        private int invoiceNumber;
        /// <summary>
        /// Gets or sets invoice number.
        /// </summary>
        public int InvoiceNumber
        {
            get { return invoiceNumber; }
            set { invoiceNumber = value; }
        }
        private string leadPaxName = string.Empty;
        private string itinerary = string.Empty;
        private decimal totalPrice = 0;
        private int manualInvoiceCounter=0;

        /// <summary>
        /// Prefix Code for Domestic And International Flight
        /// </summary>
        private string docTypeCode;

        public string LeadPaxName
        {
            get { return leadPaxName; }
            set { leadPaxName = value; ; }
        }
        public string Itinerary
        {
            get { return itinerary; }
            set { itinerary = value; ; }
        }
        public decimal TotalPrice
        {
            get { return totalPrice; }
            set { totalPrice = value; ; }
        }
        /// <summary>
        /// Gets or Sets docTypeCode
        /// </summary>
        public string DocTypeCode
        {
            get { return docTypeCode; }
            set { docTypeCode = value; }
        }
        /// <summary>
        /// Sequence Number for Domestic And International Flight
        /// </summary>
        private int documentNumber;
        /// <summary>
        /// Gets or Sets DocumentNumber
        /// </summary>
        public int DocumentNumber
        {
            get { return documentNumber; }
            set { documentNumber = value; }
        }
        private string completeInvoiceNumber;
        /// <summary>
        /// Gets or Sets DocumentNumber
        /// </summary>
        public string CompleteInvoiceNumber
        {
            get { return docTypeCode + documentNumber.ToString(); }
        }

        /// <summary>
        /// Agency Id to which the invoice corresponds.
        /// </summary>
        private int agencyId;
        /// <summary>
        /// Gets or sets the agency id to which the invoice corresponds.
        /// </summary>
        public int AgencyId
        {
            get { return agencyId; }
            set { agencyId = value; }
        }

        /// <summary>
        /// Status of the invoice
        /// </summary>
        private InvoiceStatus status;
        /// <summary>
        /// Gets or sets the status of the invoice
        /// </summary>
        public InvoiceStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        /// <summary>
        /// Mode of payment
        /// </summary>
        private string paymentMode;
        /// <summary>
        /// Gets or sets the mode of payment
        /// </summary>
        public string PaymentMode
        {
            get { return paymentMode; }
            set { paymentMode = value; }
        }

        /// <summary>
        /// array of invoice line items
        /// </summary>
        private List<InvoiceLineItem> lineItem;
        /// <summary>
        /// Gets or sets invoice line items
        /// </summary>
        public List<InvoiceLineItem> LineItem
        {
            get { return lineItem; }
            set { lineItem = value; }
        }

        //List<Li

        /// <summary>
        /// Remarks
        /// </summary>
        private string remarks;
        /// <summary>
        /// Gets or sets the remarks
        /// </summary>
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }

        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }

        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get { return lastModifiedOn; }
            set { lastModifiedOn = value; }
        }
        /// <summary>
        /// Invoice due date field contains due date of that invoice
        /// </summary>
        private DateTime invoiceDueDate;
        /// <summary>
        /// Invoice due date property
        /// </summary>
        public DateTime InvoiceDueDate
        {
            get
            {
                return this.invoiceDueDate;
            }
            set
            {
                this.invoiceDueDate = value;
            }
        }
        /// <summary>
        /// Gets total Amount(price) of the invoice
        /// </summary>


        /// <summary>
        /// Boolean value to indicate whether the invoice is manually updated or not
        ///</summary>
        private bool manuallyUpdated;
        /// <summary>
        ///Property for manuallyUpdated field
        ///</summary>
        public bool ManuallyUpdated
        {
            get
            {
                return manuallyUpdated;

            }
            set
            {
                manuallyUpdated = value;

            }
        }


        /// <summary>
        ///XONumber for supplier 
        ///</summary>
        private string xoNumber;
        /// <summary>
        ///Property for XONumber field
        ///</summary>
        public string XONumber
        {
            get
            {
                return xoNumber;
            }
            set
            {
                xoNumber = value;
            }
        }

        /// <summary>
        /// Staff Remarks
        /// </summary>
        private string staffRemarks;
        /// <summary>
        /// Property  to Access Staff Remarks
        /// </summary>

        public string StaffRemarks
        {
            get
            {
                return (staffRemarks);
            }
            set
            {
                staffRemarks = value;
            }

        }

        public decimal Amount
        {
            get
            {
                decimal amount = 0;
                foreach (InvoiceLineItem lineItem in this.LineItem)
                {
                    amount += lineItem.Price.GetAgentPrice();
                }
                return amount;
            }
        }
        public void Save(bool isDomestic, byte counter, ProductType prodType, BookingStatus bookingStatus)
        {
            Trace.TraceInformation("Invoice.Save entered : invoice no = " + invoiceNumber);
            int rowsAffected = 0;
            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
            }
            if ((int)status == 0)
            {
                throw new ArgumentException("status must have a value", "status");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a positive non zero integer value", "createdBy");
            }
            GetInvoiceDueDate(isDomestic);
            SqlParameter[] paramList = new SqlParameter[17];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@status", (int)status);
            paramList[3] = new SqlParameter("@paymentMode", paymentMode);
            paramList[4] = new SqlParameter("@remarks", remarks);
            paramList[5] = new SqlParameter("@createdBy", createdBy);
            paramList[6] = new SqlParameter("@invoiceDueDate", invoiceDueDate);
            paramList[7] = new SqlParameter("@isDomestic", isDomestic);
            paramList[8] = new SqlParameter("@productType", prodType);
            paramList[9] = new SqlParameter("@xONumber", xoNumber);
            paramList[10] = new SqlParameter("@bookingStatus", bookingStatus);
            paramList[11] = new SqlParameter("@staffRemarks", staffRemarks);

            if (CreatedOn.Date.Equals(new DateTime(1, 1, 1)))
            {
                paramList[12] = new SqlParameter("@createdon", DateTime.UtcNow);
            }
            else
            {
                paramList[12] = new SqlParameter("@createdon", CreatedOn);
            }

            //int agencyTypeId = Agency.GetAgencyTypeId(agencyId); ziya-todo
            int agencyTypeId = 1;
            foreach (InvoiceLineItem invoiceLineItem in this.LineItem)
            {
                //if ((int)Agencytype.Service == agencyTypeId)  ziya-todo add agency typpe in agent master
                //{
                //totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetServiceAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                //}
                //else
                //{
                //totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                //}
                
            }
            string invoiceDetail ="";
            if (prodType == ProductType.Flight)
            {
                //Ticket ticket = new Ticket(); ziya-todo
                //ticket.Load(this.lineItem[0].ItemReferenceNumber);
                //FlightItinerary flightItinerary = new FlightItinerary(ticket.FlightId);

                //itinerary = GetItineraryString(flightItinerary);
                //leadPaxName = GetLeadPax(flightItinerary);
                //invoiceDetail = itinerary + "|" + leadPaxName;
            }
            paramList[13] = new SqlParameter("@invoiceDetail", invoiceDetail);
            paramList[14] = new SqlParameter("@totalPrice", Math.Round(totalPrice));
           
            paramList[15] = new SqlParameter("@docTypeCode","");
            paramList[15].Direction = ParameterDirection.Output;
            paramList[15].Size = 2;

            paramList[16] = new SqlParameter("@documentNumber",0);
            paramList[16].Direction = ParameterDirection.Output;
            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddInvoice, paramList);
            }
            catch (CT.TicketReceipt.DataAccessLayer.DALException exQc)
            {
                Trace.TraceInformation("Invoice.Save Violate Unique Key Constraints Error: " + exQc.Message);
                // If method fails once it should try once again.
                if (counter < 1)
                {
                    counter++;
                    Save(isDomestic, counter, prodType,bookingStatus);
                }
                else
                {
                    throw exQc;
                }
            }

            invoiceNumber = Convert.ToInt32(paramList[0].Value);
            docTypeCode = Convert.ToString(paramList[15].Value);
            documentNumber = Convert.ToInt32(paramList[16].Value); 

            for (int i = 0; i < lineItem.Count; i++)
            {
                InvoiceLineItem line = lineItem[i];
                line.InvoiceNumber = invoiceNumber;
                line.CreatedBy = createdBy;
                if (CreatedOn.Date.Equals(new DateTime(1, 1, 1)))
                {
                    line.CreatedOn = DateTime.UtcNow;
                }
                else
                {
                    line.CreatedOn = CreatedOn;
                }



                line.CreatedOn = createdOn;
                line.Save(bookingStatus);
                lineItem[i] = line;
            }
            Trace.TraceInformation("Invoice.Save exiting : rowsAffected = " + rowsAffected);
        }
        public void Save(bool isDomestic, byte counter, ProductType prodType)
        {
            Trace.TraceInformation("Invoice.Save entered : invoice no = " + invoiceNumber);
            int rowsAffected = 0;
            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
            }
            if ((int)status == 0)
            {
                throw new ArgumentException("status must have a value", "status");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a positive non zero integer value", "createdBy");
            }
            GetInvoiceDueDate(isDomestic);
            SqlParameter[] paramList = new SqlParameter[16];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@status", (int)status);
            paramList[3] = new SqlParameter("@paymentMode", paymentMode);
            paramList[4] = new SqlParameter("@remarks", remarks);
            paramList[5] = new SqlParameter("@createdBy", createdBy);
            paramList[6] = new SqlParameter("@invoiceDueDate", invoiceDueDate);
            paramList[7] = new SqlParameter("@isDomestic", isDomestic);
            paramList[8] = new SqlParameter("@productType", prodType);
            paramList[9] = new SqlParameter("@xONumber", xoNumber);
            paramList[10] = new SqlParameter("@staffRemarks", staffRemarks);

            if(CreatedOn.Date.Equals(new DateTime(1,1,1)))
            {
                paramList[11] = new SqlParameter("@createdon", DateTime.UtcNow);
            }
            else
            {
                paramList[11] = new SqlParameter("@createdon", CreatedOn);
            }
            //int agencyTypeId = Agency.GetAgencyTypeId(agencyId);
            int agencyTypeId = 1;
            foreach (InvoiceLineItem invoiceLineItem in this.LineItem)
            {
                if (prodType == ProductType.Insurance)
                {
                totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetAgentInsurancePrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                }
                else
                {
                    //if (agencyTypeId == (int)Agencytype.Service)
                    if (agencyTypeId == 1)
                    {
                    totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetServiceAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                    }
                    else
                    {
                    totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                    }
                }
            }
            string invoiceDetail = "";
            //if (prodType == ProductType.Flight)  ziya-todo
            //{
            //    Ticket ticket = new Ticket();
            //    ticket.Load(this.lineItem[0].ItemReferenceNumber);
            //    FlightItinerary flightItinerary = new FlightItinerary(ticket.FlightId);
            //    itinerary = GetItineraryString(flightItinerary);
            //    leadPaxName = GetLeadPax(flightItinerary);
            //    invoiceDetail = itinerary + "|" + leadPaxName;
            //}
            paramList[12] = new SqlParameter("@invoiceDetail", invoiceDetail);
            if (prodType == ProductType.MobileRecharge)
            {
            paramList[13] = new SqlParameter("@totalPrice", Math.Round(totalPrice, Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));

            }
            else
            {
                paramList[13] = new SqlParameter("@totalPrice", Math.Round(totalPrice));
            }
            paramList[14] = new SqlParameter("@docTypeCode", "");
            paramList[14].Direction = ParameterDirection.Output;
            paramList[14].Size = 2;
            paramList[15] = new SqlParameter("@documentNumber",0);
            paramList[15].Direction = ParameterDirection.Output;


            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddInvoice, paramList);
            }
            catch (CT.TicketReceipt.DataAccessLayer.DALException exQc)
            {
                Trace.TraceInformation("Invoice.Save Violate Unique Key Constraints Error: " + exQc.Message);
                // If method fails once it should try once again.
                if (counter < 1)
                {
                    counter++;
                    Save(isDomestic, counter, prodType);
                }
                else
                {
                    throw exQc;
                }
            }

            invoiceNumber = Convert.ToInt32(paramList[0].Value);
            docTypeCode = Convert.ToString(paramList[14].Value);
            documentNumber = Convert.ToInt32(paramList[15].Value); 

            for (int i = 0; i < lineItem.Count; i++)
            {
                InvoiceLineItem line = lineItem[i];
                line.InvoiceNumber = invoiceNumber;
                line.CreatedBy = createdBy;
                if (CreatedOn.Date.Equals(new DateTime(1, 1, 1)))
                {
                    line.CreatedOn = DateTime.UtcNow;
                }
                else
                {
                    line.CreatedOn =CreatedOn;
                }

                
                
                line.CreatedOn = createdOn;
                line.Save();
                lineItem[i] = line;
            }
            Trace.TraceInformation("Invoice.Save exiting : rowsAffected = " + rowsAffected);
        }

        public void Save(bool isDomestic, byte counter)
        {
            Trace.TraceInformation("Invoice.Save entered : invoice no = " + invoiceNumber);
            int rowsAffected = 0;
            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
            }
            if ((int)status == 0)
            {
                throw new ArgumentException("status must have a value", "status");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a positive non zero integer value", "createdBy");
            }
            GetInvoiceDueDate(isDomestic);
            SqlParameter[] paramList = new SqlParameter[14];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@status", (int)status);
            paramList[3] = new SqlParameter("@paymentMode", paymentMode);
            paramList[4] = new SqlParameter("@remarks", remarks);
            paramList[5] = new SqlParameter("@createdBy", createdBy);
            paramList[6] = new SqlParameter("@invoiceDueDate", invoiceDueDate);
            paramList[7] = new SqlParameter("@isDomestic", isDomestic);
            paramList[8] = new SqlParameter("@productType", ProductType.Flight);
            if (CreatedOn.Date.Equals(new DateTime(1, 1, 1)))
            {
                paramList[9] = new SqlParameter("@createdon", DateTime.UtcNow);
            }
            else
            {
                paramList[9] = new SqlParameter("@createdon", CreatedOn);
            }
            //int agencyTypeId = Agency.GetAgencyTypeId(agencyId); ziya-todo
            int agencyTypeId = 1;
            foreach (InvoiceLineItem invoiceLineItem in this.LineItem)
            {
                //if ( agencyTypeId == (int) Agencytype.Service) ziya-todo
                if (agencyTypeId == 1)
                {
                totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetServiceAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                }
                else
                {
                totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                }
            }

            //Ticket ticket = new Ticket();
            //ticket.Load(this.lineItem[0].ItemReferenceNumber);
            //FlightItinerary flightItinerary = new FlightItinerary(ticket.FlightId);

            //itinerary = GetItineraryString(flightItinerary);
            //leadPaxName = GetLeadPax(flightItinerary);
            //string invoiceDetail = itinerary + "|" + leadPaxName;
            string invoiceDetail = "ziya-todo";

            paramList[10] = new SqlParameter("@invoiceDetail", invoiceDetail);
            paramList[11] = new SqlParameter("@totalPrice",Math.Round(totalPrice));

            paramList[12] = new SqlParameter("@docTypeCode", "");
            paramList[12].Direction = ParameterDirection.Output;
            paramList[12].Size = 2;
            paramList[13] = new SqlParameter("@documentNumber", 0);
            paramList[13].Direction = ParameterDirection.Output;

            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddInvoice, paramList);
            }
            catch (CT.TicketReceipt.DataAccessLayer.DALException exQc)
            {
                Trace.TraceInformation("Invoice.Save Violate Unique Key Constraints Error: " + exQc.Message);
                // If method fails once it should try once again.
                if (counter < 1)
                {
                    counter++;
                    Save(isDomestic, counter);
                }
                else
                {
                    throw exQc;
                }
            }

            invoiceNumber = Convert.ToInt32(paramList[0].Value);
            docTypeCode = Convert.ToString(paramList[12].Value);
            documentNumber = Convert.ToInt32(paramList[13].Value); 

            for (int i = 0; i < lineItem.Count; i++)
            {
                InvoiceLineItem line = lineItem[i];
                line.InvoiceNumber = invoiceNumber;
                line.CreatedBy = createdBy;
                line.CreatedOn = createdOn;
                line.Save();
                lineItem[i] = line;
            }
            Trace.TraceInformation("Invoice.Save exiting : rowsAffected = " + rowsAffected);
        }
        /*public string GetItineraryString(FlightItinerary flightItinerary) ziya-todo
        {
            string itenaryString = string.Empty;
            if (flightItinerary.Segments.Length > 0)
            {
                itenaryString = flightItinerary.Segments[0].Origin.AirportCode;
                for (int k = 0; k < flightItinerary.Segments.Length; k++)
                {
                    itenaryString = itenaryString + "-" + flightItinerary.Segments[k].Destination.AirportCode;
                }
            }
            return itenaryString;
        }*/
        public void Save(bool isDomestic)
        {
            Trace.TraceInformation("Invoice.Save entered : invoice no = " + invoiceNumber);
            int rowsAffected = 0;
            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
            }
            if ((int)status == 0)
            {
                throw new ArgumentException("status must have a value", "status");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a positive non zero integer value", "createdBy");
            }
            SqlParameter[] paramList = new SqlParameter[16];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@status", (int)status);
            paramList[3] = new SqlParameter("@paymentMode", paymentMode);
            paramList[4] = new SqlParameter("@remarks", remarks);
            paramList[5] = new SqlParameter("@createdBy", createdBy);
            paramList[6] = new SqlParameter("@invoiceDueDate", invoiceDueDate);
            paramList[7] = new SqlParameter("@isDomestic", isDomestic);
            paramList[8] = new SqlParameter("@productType", ProductType.Flight);
            paramList[9] = new SqlParameter("@xONumber", xoNumber);
            paramList[10] = new SqlParameter("@staffRemarks", staffRemarks);
            if (CreatedOn.Date.Equals(new DateTime(1, 1, 1)))
            {
                paramList[11] = new SqlParameter("@createdon", DateTime.UtcNow);
            }
            else
            {
                paramList[11] = new SqlParameter("@createdon", CreatedOn);
            }
            //int agencyTypeId = Agency.GetAgencyTypeId(agencyId); ziya-todo
            int agencyTypeId = 1;
            foreach (InvoiceLineItem invoiceLineItem in this.LineItem)
            {
                //if (agencyTypeId == (int)Agencytype.Service) ziya-tod
                if (agencyTypeId == 1)
                {
                totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetServiceAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                }
                else
                {
                totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                }
              
            }

            //Ticket ticket = new Ticket();
            //ticket.Load(this.lineItem[0].ItemReferenceNumber);
            //FlightItinerary flightItinerary = new FlightItinerary(ticket.FlightId);

            //itinerary = GetItineraryString(flightItinerary);
            //leadPaxName = GetLeadPax(flightItinerary);
            //string invoiceDetail = itinerary + "|" + leadPaxName; ziya-todo
            string invoiceDetail = "ziya-todo";
            
            paramList[12] = new SqlParameter("@invoiceDetail", invoiceDetail);
            paramList[13] = new SqlParameter("@totalPrice",Math.Round(totalPrice));
            paramList[14] = new SqlParameter("@docTypeCode", "");
            paramList[14].Direction = ParameterDirection.Output;
            paramList[14].Size = 2;
            paramList[15] = new SqlParameter("@documentNumber", 0);
            paramList[15].Direction = ParameterDirection.Output;

           // int counter = 0;
            
            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddInvoice, paramList);
            }
          
            catch (CT.TicketReceipt.DataAccessLayer.DALException exQc)
            {
                Trace.TraceInformation("Invoice.Save Violate Unique Key Constraints Error: " + exQc.Message);
                // If method fails once it should try once again.
                if (manualInvoiceCounter < 1)
                {
                    manualInvoiceCounter++;
                    Save(isDomestic);
                }
                else
                {
                    throw exQc;
                }
            }

            invoiceNumber = Convert.ToInt32(paramList[0].Value);
            docTypeCode = Convert.ToString(paramList[14].Value);
            documentNumber = Convert.ToInt32(paramList[15].Value); 

            for (int i = 0; i < lineItem.Count; i++)
            {
                InvoiceLineItem line = lineItem[i];
                line.InvoiceNumber = invoiceNumber;
                line.CreatedBy = createdBy;
                if (CreatedOn.Date.Equals(new DateTime(1, 1, 1)))
                {
                    line.CreatedOn = DateTime.UtcNow;
                }
                else
                {
                    line.CreatedOn = CreatedOn;
                }
                line.Save();
                lineItem[i] = line;
            }
            Trace.TraceInformation("Invoice.Save exiting : rowsAffected = " + rowsAffected);
        }

        public void Save(ProductType prodType,bool isDomestic)
        {
            Trace.TraceInformation("Invoice.Save entered : invoice no = " + invoiceNumber);
            int rowsAffected = 0;
            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
            }
            if ((int)status == 0)
            {
                throw new ArgumentException("status must have a value", "status");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a positive non zero integer value", "createdBy");
            }
            SqlParameter[] paramList = new SqlParameter[16];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@status", (int)status);
            paramList[3] = new SqlParameter("@paymentMode", paymentMode);
            paramList[4] = new SqlParameter("@remarks", remarks);
            paramList[5] = new SqlParameter("@createdBy", createdBy);
            paramList[6] = new SqlParameter("@invoiceDueDate", invoiceDueDate);
            paramList[7] = new SqlParameter("@isDomestic", isDomestic);
            paramList[8] = new SqlParameter("@productType", prodType);
            paramList[9] = new SqlParameter("@xONumber", xoNumber);
            paramList[10] = new SqlParameter("@staffRemarks", staffRemarks);
            if (CreatedOn.Date.Equals(new DateTime(1, 1, 1)))
            {
                paramList[11] = new SqlParameter("@createdon", DateTime.UtcNow);
            }
            else
            {
                paramList[11] = new SqlParameter("@createdon", CreatedOn);
            }

            //int agencyTypeId=Agency.GetAgencyTypeId(agencyId); ziya-todo
            int agencyTypeId = 1;
            foreach (InvoiceLineItem invoiceLineItem in this.LineItem)
            {
//                if (agencyTypeId == (int)Agencytype.Service) ziya-todo
                if (agencyTypeId == 1)
                {
                totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetServiceAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                }
                else
                {
                totalPrice = totalPrice + Math.Round(invoiceLineItem.Price.GetAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                }
                
            }

            string invoiceDetail = "";
            paramList[12] = new SqlParameter("@invoiceDetail", invoiceDetail);
            paramList[13] = new SqlParameter("@totalPrice",Math.Round(totalPrice));

            paramList[14] = new SqlParameter("@docTypeCode", "");
            paramList[14].Direction = ParameterDirection.Output;
            paramList[14].Size = 2;
            paramList[15] = new SqlParameter("@documentNumber", 0);
            paramList[15].Direction = ParameterDirection.Output;

            int counter = 0;

            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddInvoice, paramList);
            }

            catch (CT.TicketReceipt.DataAccessLayer.DALException exQc)
            {
                Trace.TraceInformation("Invoice.Save Violate Unique Key Constraints Error: " + exQc.Message);
                // If method fails once it should try once again.
                if (counter < 1)
                {
                    counter++;
                    Save(prodType,isDomestic);
                }
                else
                {
                    throw exQc;
                }
            }

            invoiceNumber = Convert.ToInt32(paramList[0].Value);
            docTypeCode = Convert.ToString(paramList[14].Value);
            documentNumber = Convert.ToInt32(paramList[15].Value);
           
            for (int i = 0; i < lineItem.Count; i++)
            {
                InvoiceLineItem line = lineItem[i];
                line.InvoiceNumber = invoiceNumber;
                line.CreatedBy = createdBy;
                if (CreatedOn.Date.Equals(new DateTime(1, 1, 1)))
                {
                    line.CreatedOn = DateTime.UtcNow;
                }
                else
                {
                    line.CreatedOn = CreatedOn;
                }
                line.Save();
                lineItem[i] = line;
            }
            Trace.TraceInformation("Invoice.Save exiting : rowsAffected = " + rowsAffected);
        }

        //public string GetLeadPax(FlightItinerary flightItinerary) ziya-todo
        //{
        //    string leadPaxName = string.Empty;
        //    for (int j = 0; j < flightItinerary.Passenger.Length; j++)
        //    {
        //        if (flightItinerary.Passenger[j].IsLeadPax)
        //        {
        //            leadPaxName = flightItinerary.Passenger[j].FirstName + " " + flightItinerary.Passenger[j].LastName;
        //        }

        //    }
        //    if (leadPaxName.Length == 0)
        //    {
        //        leadPaxName = flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName;
        //    }
        //    return leadPaxName;
        //}
        /// <summary>
        /// Method to update an invoice
        /// </summary>

        public void UpdateInvoice()
        {
            Trace.TraceInformation("Invoice.Update entered : invoice no = " + invoiceNumber);
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("invoiceNumber must have a positive non zero integer value", "invoiceNumber");
            }
            SqlParameter[] paramList = new SqlParameter[9];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@status", (int)status);
            paramList[3] = new SqlParameter("@remarks", remarks);
            paramList[4] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            paramList[5] = new SqlParameter("@lastModifiedOn", lastModifiedOn);
            paramList[6] = new SqlParameter("@manuallyUpdated", manuallyUpdated);
            paramList[7] = new SqlParameter("@totalPrice",Math.Round(totalPrice));
            paramList[8] = new SqlParameter("@staffRemarks",staffRemarks);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateInvoice, paramList);
            Trace.TraceInformation("Invoice.Update exiting : rowsAffected = " + rowsAffected);

        }

        //public void Save(bool isDomestic, byte counter, ProductType prodType)
        //{
        //    Trace.TraceInformation("Invoice.Save entered : invoice no = " + invoiceNumber);
        //    int rowsAffected = 0;
        //    if (agencyId <= 0)
        //    {
        //        throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
        //    }
        //    if ((int)status == 0)
        //    {
        //        throw new ArgumentException("status must have a value", "status");
        //    }
        //    if (createdBy <= 0)
        //    {
        //        throw new ArgumentException("createdBy must have a positive non zero integer value", "createdBy");
        //    }

        //    GetInvoiceDueDate(isDomestic);
        //    SqlParameter[] paramList = new SqlParameter[10];
        //    paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
        //    paramList[0].Direction = ParameterDirection.Output;
        //    paramList[1] = new SqlParameter("@agencyId", agencyId);
        //    paramList[2] = new SqlParameter("@status", (int)status);
        //    paramList[3] = new SqlParameter("@paymentMode", paymentMode);
        //    paramList[4] = new SqlParameter("@remarks", remarks);
        //    paramList[5] = new SqlParameter("@createdBy", createdBy);
        //    paramList[6] = new SqlParameter("@invoiceDueDate", invoiceDueDate);
        //    paramList[7] = new SqlParameter("@isDomestic", isDomestic);
        //    paramList[8] = new SqlParameter("@productType", prodType);
        //    if (CreatedOn.Date.Equals(new DateTime(1, 1, 1)))
        //    {
        //        paramList[9] = new SqlParameter("@createdon", DateTime.UtcNow);
        //    }
        //    else
        //    {
        //        paramList[9] = new SqlParameter("@createdon", CreatedOn);
        //    }

        //    try
        //    {
        //        rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddInvoice, paramList);
        //    }
        //    catch (CT.Data.DalException exQc)
        //    {
        //        Trace.TraceInformation("Invoice.Save Violate Unique Key Constraints Error: " + exQc.Message);
        //        // If method fails once it should try once again.
        //        if (counter < 1)
        //        {
        //            counter++;
        //            Save(isDomestic, counter);
        //        }
        //        else
        //        {
        //            throw exQc;
        //        }
        //    }

        //    invoiceNumber = Convert.ToInt32(paramList[0].Value);

        //    for (int i = 0; i < lineItem.Count; i++)
        //    {
        //        InvoiceLineItem line = lineItem[i];
        //        line.InvoiceNumber = invoiceNumber;
        //        line.CreatedBy = createdBy;
        //        if (CreatedOn.Date.Equals(new DateTime(1, 1, 1)))
        //        {
        //            line.CreatedOn = DateTime.UtcNow;
        //        }
        //        else
        //        {
        //            line.CreatedOn = createdOn;   
        //        }
                
        //        line.Save();
        //        lineItem[i] = line;
        //    }
        //    Trace.TraceInformation("Invoice.Save exiting : rowsAffected = " + rowsAffected);
        //}

        /// <summary>
        /// Function calculates the tds given by agency till now for calculating 
        /// it is whether under exemption or not
        /// </summary>
        /// <param name="agencyId">agencyId</param>
        /// <param name="datetime">dateTime for which period it belongs to</param>
        /// <returns>total tds given within a defined period</returns>
        public static decimal GetTDSTillNow(int agencyId, DateTime datetime)
        {

            decimal tdsTotal = 0;
            Trace.TraceInformation("Invoice.GetTDSTillNow entered : dateTime = " + datetime.ToString("dd-mm-yyyy") + "agency Id:" + agencyId);
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
//            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@dateTime", datetime);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetTDSTillNow, paramList);
            if (dataReader.Read())
            {
                if (dataReader["tds"] != DBNull.Value)
                {
                    tdsTotal = Convert.ToDecimal(dataReader["tds"]);
                }
            }
            dataReader.Close();
            ////////connection.Close();
            return tdsTotal;
        }

        public int InvoiceRaised(int flightId)
        {
            Trace.TraceInformation("Invoice.InvoiceRaised entered : flightId = " + flightId);
//            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.InvoiceRaisedForFlight, paramList);
            int lineItemCount = 0;
            if (dataReader.Read())
            {
                lineItemCount = Convert.ToInt32(dataReader["lineCount"]);
            }
            dataReader.Close();
            ////////connection.Close();
            Trace.TraceInformation("Invoice.InvoiceRaised exiting : lineItemCount = " + lineItemCount);
            return lineItemCount;
        }

        public int InvoiceRaised(int productId, ProductType prodType)
        {
            Trace.TraceInformation("Invoice.InvoiceRaised entered : productId = " + productId);
            //////SqlConnection connection = DBGateway.GetConnection();
            int lineItemCount = 0;
            if (prodType == ProductType.Flight)
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@flightId", productId);
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.InvoiceRaisedForFlight, paramList);
                if (dataReader.Read())
                {
                    lineItemCount = Convert.ToInt32(dataReader["lineCount"]);
                }
                dataReader.Close();
            }
            else if (prodType == ProductType.Hotel)
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@hotelId", productId);
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.InvoiceRaisedForHotel, paramList);
                if (dataReader.Read())
                {
                    lineItemCount = Convert.ToInt32(dataReader["lineCount"]);
                }
                dataReader.Close();
            }
            else if (prodType == ProductType.Insurance)
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@insuranceId", productId);
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.InvoiceRaisedForInsurance, paramList);
                if (dataReader.Read())
                {
                    lineItemCount = Convert.ToInt32(dataReader["lineCount"]);
                }
                dataReader.Close();
            }
            ////////connection.Close();
            Trace.TraceInformation("Invoice.InvoiceRaised exiting : lineItemCount = " + lineItemCount);
            return lineItemCount;
        }
               
        public static int CreditNoteRaisedForItem(int itemId,int itemTypeId)
        {
            Trace.TraceInformation("Invoice.CreditNoteRaised entered : itemId = " + itemId+", itemTypeId:"+itemTypeId);
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@itemId", itemId);
            paramList[1] = new SqlParameter("@itemTypeId", itemTypeId);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.CreditNoteRaisedForItem, paramList);
            int invoiceNumber = 0;
            if (dataReader.Read())
            {
                invoiceNumber =  Convert.ToInt32(dataReader["InvoiceNumber"]);
            }
            dataReader.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.CreditNoteRaised exiting : with invoiceNumber = " + invoiceNumber);
            return invoiceNumber;
        }

        public int CreditNoteRaisedForTicketId(int ticketId)
        {
            Trace.TraceInformation("Invoice.CreditNoteRaisedForTicketId entered : ticketId = " + ticketId);
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ticketId", ticketId);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.CreditNoteRaisedForTicketId, paramList);
            int lineItemCount = 0;
            if (dataReader.Read())
            {
                lineItemCount = Convert.ToInt32(dataReader["lineCount"]);
            }
            dataReader.Close();
            ////////connection.Close();
            Trace.TraceInformation("Invoice.CreditNoteRaisedForTicketId exiting : lineItemCount = " + lineItemCount);
            return lineItemCount;
        }


        /// <summary>
        /// Method saves the billing address in billing address table
        /// </summary>
        /// <param name="invoiceNumber">invoice Number</param>
        /// <param name="billingAddress">BillingAddress</param>
        public void SaveBillingAddress(int invoiceNumber, string billingAddress)
        {
            Trace.TraceInformation("Invoice.SaveBillingAddress entered : invoiceId = " + invoiceNumber);
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("Invoice Number should be positive integer invoiceNumber=" + invoiceNumber);
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[1] = new SqlParameter("@billingAddress", billingAddress);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddBillingAddress, paramList);
            Trace.TraceInformation("Invoice.SaveBillingAddress exited : invoiceId = " + invoiceNumber);
        }

        /// <summary>
        /// Method updates the Invoice remarks
        /// </summary>
        /// <param name="invoiceNumber">invoice Number</param>
        /// <param name="billingAddress">BillingAddress</param>
        public static void SaveInvoiceRemarks(int invoiceNumber, string invoiceRemark)
        {
            Trace.TraceInformation("Invoice.SaveInvoiceRemarks entered : invoiceId = " + invoiceNumber);
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("Invoice Number should be positive integer invoiceNumber=" + invoiceNumber);
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[1] = new SqlParameter("@invoiceRemark", invoiceRemark);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateInvoiceRemark, paramList);
            Trace.TraceInformation("Invoice.SaveInvoiceRemarks exited : invoiceId = " + invoiceNumber);
        }
        /// <summary>
        /// Load method for a invoice
        /// </summary>
        /// <param name="invoiceNumber">invoice number</param>
        public void Load(int invoiceNumber)
        {
            Trace.TraceInformation("Invoice.Load entered : invoiceNumber = " + invoiceNumber);
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("Invoice Number should be positive integer invoiceNumber=" + invoiceNumber);
            }
//            ////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetInvoice, paramList);
            this.invoiceNumber = invoiceNumber;
            if (dataReader.Read())
            {
                this.docTypeCode = Convert.ToString(dataReader["docTypeCode"]);
                this.documentNumber = Convert.ToInt32(dataReader["documentNumber"]);
                this.agencyId = Convert.ToInt32(dataReader["agencyId"]);
                this.status = (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), dataReader["status"].ToString());
                InvoiceLineItem temp = new InvoiceLineItem();
                this.remarks = dataReader["remarks"].ToString();
                this.lineItem = temp.GetLineItems(invoiceNumber);
                if (dataReader["invoiceDueDate"] != DBNull.Value)
                {
                    this.invoiceDueDate = Convert.ToDateTime(dataReader["invoiceDueDate"]);
                }
                this.xoNumber = dataReader["XONumber"].ToString();
                this.staffRemarks = dataReader["StaffRemarks"].ToString();
                this.createdBy = Convert.ToInt32(dataReader["createdBy"]);
                this.createdOn = Convert.ToDateTime(dataReader["createdOn"]);
                this.lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
                this.lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
                if (dataReader["invoiceDetail"] != DBNull.Value)
                {
                    string[] invoiceDetail = Convert.ToString(dataReader["invoiceDetail"]).Split('|');
                    if (invoiceDetail.Length == 2)
                    {
                        itinerary = invoiceDetail[0];
                        leadPaxName = invoiceDetail[1];
                    }
                }
                if (dataReader["totalPrice"] != DBNull.Value)
                {
                    totalPrice = Convert.ToDecimal(dataReader["totalPrice"]);
                }
            }
            dataReader.Close();
            ////////connection.Close();
            Trace.TraceInformation("Invoice.Load exiting : ");
        }

        /// <summary>
        /// To get total price of given invoice
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <returns>invoice amount</returns>
        public static decimal GetTotalPrice(int invoiceNumber)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Account, CT.Core.Severity.Low, 0, "Invoice.GetTotalPrice entered with invoicenumber: " + invoiceNumber, "0");
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("Invoice Number should be positive integer invoiceNumber=" + invoiceNumber);
            }
//            ////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetInvoice, paramList);
            decimal totalAmount = 0;
            if (dataReader.Read())
            {
                if (dataReader["totalPrice"] != DBNull.Value)
                {
                    totalAmount = Convert.ToDecimal(dataReader["totalPrice"]);
                }
            }
            dataReader.Close();
            ////////connection.Close();
            CT.Core.Audit.Add(CT.Core.EventType.Account, CT.Core.Severity.Low, 0, "Invoice.GetTotalPrice exited with invoice amount: " + totalAmount, "0");
            return totalAmount;
        }

        /// <summary>
        /// To get total price of one day invoices in a given time range
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="startDate"></param>
        /// <param name="EndDate"></param>
        /// <returns>invoice amount</returns>
        public static decimal GetTotalInvoiceAmountInDateRange(int agencyId,DateTime startDate, DateTime EndDate )
        {

            if (agencyId <= 0)
            {
                throw new ArgumentException("AgencyId should be positive integer AgencyId=" + agencyId);
            }


            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1]= new SqlParameter("@startingDate",startDate);
            paramList[2] = new SqlParameter("@endingDate", EndDate);

            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetInvoicesTotalByDateRange, paramList);
            decimal totalAmount = 0;
            if (dataReader.Read())
            {
                if (dataReader["totalPrice"] != DBNull.Value)
                {
                    totalAmount = Convert.ToDecimal(dataReader["totalPrice"]);
                }
            }
            dataReader.Close();
            ////////connection.Close();
            CT.Core.Audit.Add(CT.Core.EventType.Account, CT.Core.Severity.Low, 0, "Invoice.GetTotalInvoiceAmountInDateRange exited with invoice amount: " + totalAmount, "0");
            return totalAmount;
        }

        /// <summary>
        /// Function return 0 or invoiceNumber which tell the invoice is generated or
        /// not for a particular tickets
        /// </summary>
        /// <param name="ticketId">ticketId</param>
        /// <returns>0 or invoiceNumber</returns>
        public static int isInvoiceGenerated(int ticketId)
        {
            int invoiceNumber = 0;
            Trace.TraceInformation("Invoice.isInvoiceGenerated entered : ticketId= " + ticketId);
            if (ticketId <= 0)
            {
                throw new ArgumentException("Ticket Id should be positive integer ticketId=" + ticketId);
            }
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ticketId", ticketId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsInvoiceGenerated, paramList);
            if (data.Read())
            {
                invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
            }
            data.Close();
            ////////connection.Close();
            return invoiceNumber;
        }

        public static int isCreditNoteGenerated(int ticketId)
        {
            int invoiceNumber = 0;
            Trace.TraceInformation("Invoice.isCreditNoteGenerated entered : ticketId= " + ticketId);
            if (ticketId <= 0)
            {
                throw new ArgumentException("Ticket Id should be positive integer ticketId=" + ticketId);
            }
//            ////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ticketId", ticketId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsCreditNoteGenerated, paramList);
            if (data.Read())
            {
                invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
            }
            data.Close();
            ////////connection.Close();
            return invoiceNumber;
        }

        /// <summary>
        /// To check credit note generated or not for a refrence number
        /// </summary>
        /// <param name="itemRefId">refrence id for invoice line item</param>
        /// <param name="productType">product type</param>
        /// <returns>invoice number</returns>
        public static int isCreditNoteGenerated(int itemRefId,ProductType productType)
        {
            int invoiceNumber = 0;
            Trace.TraceInformation("Invoice.isCreditNoteGenerated entered : ItemRefrenceId= " + itemRefId);
            if (itemRefId <= 0)
            {
                throw new ArgumentException("Item Reference Id should be positive integer =" + itemRefId);
            }
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@itemRefId", itemRefId);
            paramList[1] = new SqlParameter("@productType",(int) productType);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsCreditNoteGeneratedForProduct, paramList);
            if (data.Read())
            {
                invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
            }
            data.Close();
            //////connection.Close();
            return invoiceNumber;
        }


        /// <summary>
        /// Function return 0 or invoiceNumber which tell the invoice is generated or
        /// not for a particular hotel room
        /// </summary>
        /// <param name="roomId">roomId</param>
        /// <returns>0 or invoiceNumber</returns>
        public static int isInvoiceGenerated(int itemRefId, ProductType prodType)
        {
            int invoiceNumber = 0;
            Trace.TraceInformation("Invoice.isInvoiceGenerated entered : itemRefId= " + itemRefId);
            if (itemRefId <= 0)
            {
                throw new ArgumentException("Item Reference Id should be positive integer =" + itemRefId);
            }
            if (prodType == ProductType.Hotel)
            {
                //////SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@roomId", itemRefId);
                SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsInvoiceGeneratedForHotel, paramList);
                if (data.Read())
                {
                    invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
                }
                data.Close();
                //////connection.Close();
            }
            if (prodType == ProductType.Insurance)
            {
                //////SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@paxId", itemRefId);
                SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsInvoiceGeneratedForInsurance, paramList);
                if (data.Read())
                {
                    invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
                }
                data.Close();
                //////connection.Close();
            }
            if (prodType == ProductType.Transfers)
            {
                //////SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@transferId", itemRefId);
                SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsInvoiceGeneratedForTransfers, paramList);
                if (data.Read())
                {
                    invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
                }
                data.Close();
                //////connection.Close();
            }
            if (prodType == ProductType.SightSeeing)
            {
                //////SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@sightseeingId", itemRefId);
                SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsInvoiceGeneratedForSightseeing, paramList);
                if (data.Read())
                {
                    invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
                }
                data.Close();
                //////connection.Close();
            }
            if (prodType == ProductType.MobileRecharge)
            {
                //////SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@rechargeId", itemRefId);
                SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsInvoiceGeneratedForMobileRecharge, paramList);
                if (data.Read())
                {
                    invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
                }
                data.Close();
                //////connection.Close();
            }
            return invoiceNumber;
        }

        /// <summary>
        /// Get the unpaid invoice for agency.
        /// </summary>
        /// <param name="agencyId">agencyId</param>
        /// <returns>invoices Tables.</returns>

        public static DataTable GetNonPaidInvoices(int agencyId)
        {
            DataTable dataTable = new DataTable();
            Trace.TraceInformation("Invoice.GetNonPaidInvoices entered agencyId:" + agencyId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId",agencyId);
            dataTable = DBGateway.FillDataTableSP(SPNames.GetNonPaidInvoices,paramList);
            Trace.TraceInformation("Invoice.GetNonPaidInvoices exited, rows affected :" + dataTable.Rows.Count);
            return (dataTable);
        }

        /// <summary>
        /// This method finds the unpaid invoices of a particular agency
        /// </summary>
        /// <param name="agencyId">agencyId</param>
        /// <returns>Generic List contains the collection of invoices</returns>
        //public static List<Invoice> GetNonPaidInvoices(int agencyId)
        //{
        //    List<Invoice> tempList = new List<Invoice>();
        //    Trace.TraceInformation("Invoice.GetNonPaidInvoices entered : agencyId= " + agencyId);
        //    if (agencyId <= 0)
        //    {
        //        throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
        //    }
        //    ////SqlConnection connection = DBGateway.GetConnection();
        //    SqlParameter[] paramList = new SqlParameter[1];
        //    paramList[0] = new SqlParameter("@agencyId", agencyId);
        //    SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetNonPaidInvoices, paramList);
        //    while (data.Read())
        //    {
        //        Invoice tempItem = new Invoice();
        //        tempItem.invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
        //        tempItem.docTypeCode = Convert.ToString(data["docTypeCode"]);
        //        tempItem.documentNumber = Convert.ToInt32(data["documentNumber"]);
        //        tempItem.agencyId = Convert.ToInt32(data["agencyId"]);
        //        tempItem.status = (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), data["status"].ToString());
        //        if (data["invoiceDueDate"] != DBNull.Value)
        //        {
        //            tempItem.invoiceDueDate = Convert.ToDateTime(data["invoiceDueDate"]);
        //        }
        //        InvoiceLineItem temp = new InvoiceLineItem();
        //        tempItem.lineItem = temp.GetLineItems(tempItem.invoiceNumber);
        //        tempItem.remarks = data["remarks"].ToString();
        //        tempItem.createdBy = Convert.ToInt32(data["createdBy"]);
        //        tempItem.createdOn = Convert.ToDateTime(data["createdOn"]);
        //        tempItem.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
        //        tempItem.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
        //        if (data["invoiceDetail"] != DBNull.Value && Convert.ToString(data["invoiceDetail"]).Contains("|"))
        //        {
        //            string[] invoiceDetail = Convert.ToString(data["invoiceDetail"]).Split('|');
        //            tempItem.Itinerary = invoiceDetail[0];
        //            tempItem.LeadPaxName = invoiceDetail[1];
        //        }
        //        if (data["totalPrice"] != DBNull.Value)
        //        {
        //            tempItem.TotalPrice = Convert.ToDecimal(data["totalPrice"]);
        //        }
        //        tempList.Add(tempItem);
        //    }
        //    data.Close();
        //    //////connection.Close();
        //    Trace.TraceInformation("Invoice.GetNonPaidInvoices exited : agencyId= " + agencyId);
        //    return tempList;
        //}
        /// <summary>
        /// This method finds the unpaid invoices of a particular agency
        /// </summary>
        /// <param name="agencyId">agencyId</param>
        /// <returns>Generic List contains the collection of invoices for Agent</returns>
        public static List<Invoice> GetNonPaidInvoicesForAgent(int agencyId)
        {
            Trace.TraceInformation("Invoice.GetNonPaidInvoices entered : agencyId= " + agencyId);
            List<Invoice> tempList = new List<Invoice>();
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetNonPaidInvoicesForAgent, paramList);
            while (data.Read())
            {
                Invoice tempItem = new Invoice();
                tempItem.invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
                tempItem.docTypeCode = Convert.ToString(data["docTypeCode"]);
                tempItem.documentNumber = Convert.ToInt32(data["documentNumber"]);
                tempItem.agencyId = Convert.ToInt32(data["agencyId"]);
                tempItem.status = (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), data["status"].ToString());
                if (data["invoiceDueDate"] != DBNull.Value)
                {
                    tempItem.invoiceDueDate = Convert.ToDateTime(data["invoiceDueDate"]);
                }
                InvoiceLineItem temp = new InvoiceLineItem();
                tempItem.lineItem = temp.GetLineItems(tempItem.invoiceNumber);
                tempItem.remarks = data["remarks"].ToString();
                tempItem.createdBy = Convert.ToInt32(data["createdBy"]);
                tempItem.createdOn = Convert.ToDateTime(data["createdOn"]);
                tempItem.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                tempItem.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                if (data["invoiceDetail"] != DBNull.Value && Convert.ToString(data["invoiceDetail"]).Contains("|"))
                {
                    string[] invoiceDetail = Convert.ToString(data["invoiceDetail"]).Split('|');
                    tempItem.Itinerary = invoiceDetail[0];
                    tempItem.LeadPaxName = invoiceDetail[1];
                }
                if (data["totalPrice"] != DBNull.Value)
                {
                    tempItem.TotalPrice = Convert.ToDecimal(data["totalPrice"]);
                }
                tempList.Add(tempItem);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetNonPaidInvoices exited : count= " + tempList.Count);
            return tempList;
        }




        /// <summary>
        /// This method finds the unpaid invoices of a particular agency without lineitem
        /// </summary>
        /// <param name="agencyId">agencyId</param>
        /// <returns>Generic List contains the collection of invoices without lineitem</returns>
        public static List<Invoice> GetNonPaidInvoicesNew(int agencyId)
        {
            List<Invoice> tempList = new List<Invoice>();
            Trace.TraceInformation("Invoice.GetNonPaidInvoices entered : agencyId= " + agencyId);
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetNonPaidInvoices, paramList);
            while (data.Read())
            {
                Invoice tempItem = new Invoice();
                tempItem.invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
                tempItem.docTypeCode = Convert.ToString(data["docTypeCode"]);
                tempItem.documentNumber = Convert.ToInt32(data["documentNumber"]);
                tempItem.agencyId = Convert.ToInt32(data["agencyId"]);
                tempItem.status = (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), data["status"].ToString());
                if (data["invoiceDueDate"] != DBNull.Value)
                {
                    tempItem.invoiceDueDate = Convert.ToDateTime(data["invoiceDueDate"]);
                }
                List<InvoiceLineItem> temp = new List<InvoiceLineItem>();
                tempItem.lineItem = temp;//.GetLineItems(tempItem.invoiceNumber);
                tempItem.remarks = data["remarks"].ToString();
                tempItem.createdBy = Convert.ToInt32(data["createdBy"]);
                tempItem.createdOn = Convert.ToDateTime(data["createdOn"]);
                tempItem.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                tempItem.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                if (data["invoiceDetail"] != DBNull.Value && Convert.ToString(data["invoiceDetail"]).Contains("|"))
                {
                    string[] invoiceDetail = Convert.ToString(data["invoiceDetail"]).Split('|');
                    tempItem.Itinerary = invoiceDetail[0];
                    tempItem.LeadPaxName = invoiceDetail[1];
                }
                if (data["totalPrice"] != DBNull.Value)
                {
                    tempItem.TotalPrice = Convert.ToDecimal(data["totalPrice"]);
                }
                tempList.Add(tempItem);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetNonPaidInvoices exited : agencyId= " + agencyId);
            return tempList;
        }

        /// <summary>
        /// This function will return record count of non paid invoices for given agency
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns>Record count for non paid invoices</returns>
        public static int GetRecordCountForNonPaidInvoicesForAgent(int agencyId)
        {
            Trace.TraceInformation("Invoice.GetCountForNonPaidInvoicesForAgent entered : agencyId= " + agencyId);
            int recordCount = 0;
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetCountForNonPaidInvoicesForAgent, paramList);
            if(data.Read())
            {
                if (data["recordCount"] != DBNull.Value)
                {
                    recordCount = Convert.ToInt32(data["recordCount"]);
                }                
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetCountForNonPaidInvoicesForAgent exited : count= " + recordCount);
            return recordCount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static int GetRecordCountForNonPaidInvoicesForPaymentAcceptance(int agencyId,int paymentDetailId)
        {
            Trace.TraceInformation("Invoice.GetRecordCountForNonPaidInvoicesForPaymentAcceptance entered : agencyId= " + agencyId);
            int recordCount = 0;
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
            if (paymentDetailId <= 0)
            {
                throw new ArgumentException("Payment Detail Id should be positive integer agencyId=" + paymentDetailId);
            }
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@paymentDetailId", paymentDetailId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetCountForNonPaidInvoicesForPaymentAcceptance, paramList);
            if (data.Read())
            {
                if (data["recordCount"] != DBNull.Value)
                {
                    recordCount = Convert.ToInt32(data["recordCount"]);
                }
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetCountForNonPaidInvoicesForAgent exited : count= " + recordCount);
            return recordCount;
        }
        /// <summary>
        /// This method finds the unpaid invoices of a particular agency without lineitem
        /// </summary>
        /// <param name="agencyId">agencyId</param>
        /// <returns>Generic List contains the collection of invoices for Agent without lineitem</returns>
        public static List<Invoice> GetNonPaidInvoicesForAgentPageWise(int noOfRecordsPerPage, int pageNumber, int totalRecords, int agencyId, bool orderByDesc)
        {
            Trace.TraceInformation("Invoice.GetNonPaidInvoices entered : agencyId= " + agencyId);
            List<Invoice> tempList = new List<Invoice>();
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
            int endRow = 0;
            int startRow = ((noOfRecordsPerPage * (pageNumber - 1)) + 1);
            if ((startRow + noOfRecordsPerPage) - 1 < totalRecords)
            {
                endRow = (startRow + noOfRecordsPerPage) - 1;
            }
            else
            {
                endRow = totalRecords;
            }
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@orderByDesc", orderByDesc);
            paramList[2] = new SqlParameter("@startRow", startRow);
            paramList[3] = new SqlParameter("@endRow", endRow);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetNonPaidInvoicesForAgentPageWise, paramList);
            while (data.Read())
            {
                Invoice tempItem = new Invoice();
                tempItem.invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
                tempItem.docTypeCode = Convert.ToString(data["docTypeCode"]);
                tempItem.documentNumber = Convert.ToInt32(data["documentNumber"]);
                tempItem.agencyId = Convert.ToInt32(data["agencyId"]);
                tempItem.status = (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), data["status"].ToString());
                if (data["invoiceDueDate"] != DBNull.Value)
                {
                    tempItem.invoiceDueDate = Convert.ToDateTime(data["invoiceDueDate"]);
                }
                List<InvoiceLineItem> temp = new List<InvoiceLineItem>();
                tempItem.lineItem = temp;//.GetLineItems(tempItem.invoiceNumber);
                tempItem.remarks = data["remarks"].ToString();
                tempItem.createdBy = Convert.ToInt32(data["createdBy"]);
                tempItem.createdOn = Convert.ToDateTime(data["createdOn"]);
                tempItem.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                tempItem.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                if (data["invoiceDetail"] != DBNull.Value && Convert.ToString(data["invoiceDetail"]).Contains("|"))
                {
                    string[] invoiceDetail = Convert.ToString(data["invoiceDetail"]).Split('|');
                    tempItem.Itinerary = invoiceDetail[0];
                    tempItem.LeadPaxName = invoiceDetail[1];
                }
                if (data["totalPrice"] != DBNull.Value)
                {
                    tempItem.TotalPrice = Convert.ToDecimal(data["totalPrice"]);
                }
                tempList.Add(tempItem);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetNonPaidInvoices exited : count= " + tempList.Count);
            return tempList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="noOfRecordsPerPage"></param>
        /// <param name="pageNumber"></param>
        /// <param name="totalRecords"></param>
        /// <param name="agencyId"></param>
        /// <param name="orderByDesc"></param>
        /// <returns></returns>
        public static List<Invoice> GetNonPaidInvoicesForPaymentProcessingPageWise(int noOfRecordsPerPage, int pageNumber, int totalRecords, int agencyId,int paymentDetailId, bool orderByDesc)
        {
            Trace.TraceInformation("Invoice.GetNonPaidInvoices entered : agencyId= " + agencyId);
            List<Invoice> tempList = new List<Invoice>();
            if (agencyId <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }

            if (paymentDetailId <= 0)
            {
                throw new ArgumentException("Payment Detail Id should be positive integer agencyId=" + paymentDetailId);
            }
            int endRow = 0;
            int startRow = ((noOfRecordsPerPage * (pageNumber - 1)) + 1);
            if ((startRow + noOfRecordsPerPage) - 1 < totalRecords)
            {
                endRow = (startRow + noOfRecordsPerPage) - 1;
            }
            else
            {
                endRow = totalRecords;
            }
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@paymentDetailId", paymentDetailId);
            paramList[2] = new SqlParameter("@orderByDesc", orderByDesc);
            paramList[3] = new SqlParameter("@startRow", startRow);
            paramList[4] = new SqlParameter("@endRow", endRow);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetNonPaidInvoicesForPaymentProcessingPageWise, paramList);
            while (data.Read())
            {
                Invoice tempItem = new Invoice();
                tempItem.invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
                tempItem.docTypeCode = Convert.ToString(data["docTypeCode"]);
                tempItem.documentNumber = Convert.ToInt32(data["documentNumber"]);
                tempItem.agencyId = Convert.ToInt32(data["agencyId"]);
                tempItem.status = (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), data["status"].ToString());
                if (data["invoiceDueDate"] != DBNull.Value)
                {
                    tempItem.invoiceDueDate = Convert.ToDateTime(data["invoiceDueDate"]);
                }
                List<InvoiceLineItem> temp = new List<InvoiceLineItem>();
                tempItem.lineItem = temp;//.GetLineItems(tempItem.invoiceNumber);
                tempItem.remarks = data["remarks"].ToString();
                tempItem.createdBy = Convert.ToInt32(data["createdBy"]);
                tempItem.createdOn = Convert.ToDateTime(data["createdOn"]);
                tempItem.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                tempItem.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                if (data["invoiceDetail"] != DBNull.Value && Convert.ToString(data["invoiceDetail"]).Contains("|"))
                {
                    string[] invoiceDetail = Convert.ToString(data["invoiceDetail"]).Split('|');
                    tempItem.Itinerary = invoiceDetail[0];
                    tempItem.LeadPaxName = invoiceDetail[1];
                }
                if (data["totalPrice"] != DBNull.Value)
                {
                    tempItem.TotalPrice = Convert.ToDecimal(data["totalPrice"]);
                }
                tempList.Add(tempItem);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetNonPaidInvoices exited : count= " + tempList.Count);
            return tempList;
        }
        /// <summary>
        /// Method updates the status of invoice when paid or partiallly paid
        /// </summary>
        /// <param name="invoiceNumber">invoice Number</param>
        /// <param name="status">Enum status of invoice</param>
        /// <param name="lastModifiedBy">Who is going to modify</param>
        public static void UpdateInvoiceStatus(int invoiceNumber, InvoiceStatus status, int lastModifiedBy)
        {
            Trace.TraceInformation("Invoice.UpdateInvoiceStatus entered : ");
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("Invoice Number should be positive integer invoiceNumber=" + invoiceNumber);
            }
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[1] = new SqlParameter("@status", (int)status);
            paramList[2] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateInvoiceStatus, paramList);
            Trace.TraceInformation("Invoice.UpdateInvoiceStatus exiting : rowsAffected = ");
        }
        /// <summary>
        /// Method calculates the Total payment done against invoice
        /// </summary>
        /// <param name="invoiceNumber">invoice Number</param>
        /// <returns>Total payment done against invoice</returns>
        public static decimal GetPaymentDoneAgainstInvoice(int invoiceNumber)
        {
            decimal paymentDone = 0;
            Trace.TraceInformation("Invoice.GetPaymentDoneAgainstInvoice entered : ");
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetPaymentDoneAgainstInvoice, paramList);
            while (data.Read())
            {
                if (data["payment"] != DBNull.Value)
                {
                    paymentDone = paymentDone + Convert.ToDecimal(data["payment"]);
                }
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetPaymentDoneAgainstInvoice exited : ");
            return paymentDone;
        }

        /// <summary>
        /// Method checks whether there are any payments that are done against an invoice
        /// </summary>
        /// <param name="invoiceNumber">invoice Number</param>
        /// <returns>boolean value that indicates whether any payment has been done or not for the invoice</returns>
        public static bool IsPaymentDoneAgainstInvoice(int invoiceNumber)
        {
            Trace.TraceInformation("Invoice.IsPaymentDoneAgainstInvoice entered : invoiceNumber= " + invoiceNumber);
            bool paymentDoneAgainstInvoice = false;
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("invoiceNumber should be positive integer invoiceNumber=" + invoiceNumber);
            }
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsPaymentDoneAgainstInvoice, paramList);
            while (data.Read())
            {
                int paymentDoneCount = Convert.ToInt32(data["PaymentDoneCount"]);
                if (paymentDoneCount != 0)
                {
                    paymentDoneAgainstInvoice = true;
                }
            }
            data.Close();
            //////connection.Close();
            return paymentDoneAgainstInvoice;

        }
        /// <summary>
        /// Get All invoices during the period of a particular agency
        /// </summary>
        /// <param name="agencyId">AgencyId</param>
        /// <param name="startingDate">From Date</param>
        /// <param name="endingDate">To Date</param>
        /// <returns></returns>
        public static List<Invoice> GetInvoices(int agencyId, DateTime startingDate, DateTime endingDate)
        {
            Trace.TraceInformation("Invoice.GetInvoices entered : agencyId= " + agencyId);
            List<Invoice> tempList = new List<Invoice>();
            if (agencyId < 0)
            {
                throw new ArgumentException("Agency Id should be positive integer agencyId=" + agencyId);
            }
            startingDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
            endingDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
            startingDate = Util.ISTToUTC(startingDate);
            endingDate = Util.ISTToUTC(endingDate);
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@startingDate", startingDate);
            paramList[2] = new SqlParameter("@endingDate", endingDate);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetInvoices, paramList);
            while (data.Read())
            {
                Invoice tempItem = new Invoice();
                tempItem.invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
                tempItem.docTypeCode = Convert.ToString(data["docTypeCode"]);
                tempItem.documentNumber = Convert.ToInt32(data["documentNumber"]);
                tempItem.agencyId = Convert.ToInt32(data["agencyId"]);
                tempItem.status = (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), data["status"].ToString());
                if (data["invoiceDueDate"] != DBNull.Value)
                {
                    tempItem.invoiceDueDate = Convert.ToDateTime(data["invoiceDueDate"]);
                }
                InvoiceLineItem temp = new InvoiceLineItem();
                tempItem.lineItem = temp.GetLineItems(tempItem.invoiceNumber);
                if (data["remarks"] != null)
                {
                    tempItem.remarks = data["remarks"].ToString();
                }
                tempItem.createdBy = Convert.ToInt32(data["createdBy"]);
                tempItem.createdOn = Convert.ToDateTime(data["createdOn"]);
                tempItem.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                tempItem.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                tempList.Add(tempItem);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetInvoices exited : agencyId= " + agencyId);
            return tempList;
        }
        /// <summary>
        /// Get All invoices during the period of comma seperated agencies
        /// </summary>
        /// <param name="agencyId">AgencyId</param>
        /// <param name="startingDate">From Date</param>
        /// <param name="endingDate">To Date</param>
        /// <returns></returns>
        public static List<Invoice> GetInvoices(string csvAgencyId, DateTime startingDate, DateTime endingDate)
        {
            Trace.TraceInformation("Invoice.GetInvoices entered : csvAgencyId= " + csvAgencyId);
            List<Invoice> tempList = new List<Invoice>();
            if (csvAgencyId.Length <= 0)
            {
                throw new ArgumentException("Agency Id should be positive integer csvAgencyId=" + csvAgencyId);
            }
            startingDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
            endingDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
            startingDate = Util.ISTToUTC(startingDate);
            endingDate = Util.ISTToUTC(endingDate);
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@csvAgencyId", csvAgencyId);
            paramList[1] = new SqlParameter("@startingDate", startingDate);
            paramList[2] = new SqlParameter("@endingDate", endingDate);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetInvoicesForAgencies, paramList);
            while (data.Read())
            {
                Invoice tempItem = new Invoice();
                tempItem.invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
                tempItem.docTypeCode = Convert.ToString(data["docTypeCode"]);
                tempItem.documentNumber = Convert.ToInt32(data["documentNumber"]);
                tempItem.agencyId = Convert.ToInt32(data["agencyId"]);
                tempItem.status = (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), data["status"].ToString());
                if (data["invoiceDueDate"] != DBNull.Value)
                {
                    tempItem.invoiceDueDate = Convert.ToDateTime(data["invoiceDueDate"]);
                }
                InvoiceLineItem temp = new InvoiceLineItem();
                tempItem.lineItem = temp.GetLineItems(tempItem.invoiceNumber);
                if (data["remarks"] != null)
                {
                    tempItem.remarks = data["remarks"].ToString();
                }
                tempItem.createdBy = Convert.ToInt32(data["createdBy"]);
                tempItem.createdOn = Convert.ToDateTime(data["createdOn"]);
                tempItem.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                tempItem.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                tempList.Add(tempItem);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetInvoices exited : csvAgencyId= " + csvAgencyId);
            return tempList;
        }
        public static string GetInvoiceOfTxn(int txnId)
        {
            Trace.TraceInformation("Invoice.GetInvoiceOfTxn entered : txnId= " + txnId);
            string invoiceNumber = "";
            if (txnId <= 0)
            {
                throw new ArgumentException("Transaction Id should be positive integer txnId=" + txnId);
            }
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@txnId", txnId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetInvoiceOfTxn, paramList);
            if (data.Read())
            {
                invoiceNumber = Convert.ToString(data["invoiceNumber"]);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetInvoiceOfTxn exited : invoiceNumber= " + invoiceNumber);
            return invoiceNumber;
        }
        /// <summary>
        /// Sets the invoice due date field for the new invoice
        /// </summary>
        public void GetInvoiceDueDate(bool isDomestic)
        {
            Trace.TraceInformation("Invoice.GetInvoiceDueDate entered.");
            DateTime currentTime = Util.GetIST();
            ////TODO: This 13 and 28 are hard coded later it will be in config file
            if (isDomestic)
            {
                if (currentTime.Day >= 1 && currentTime.Day <= 15)
                {
                    invoiceDueDate = new DateTime(currentTime.Year, currentTime.Month, 25, 23, 59, 59);
                }
                else
                {
                    if (currentTime.Month == 12)
                    {
                        invoiceDueDate = new DateTime(currentTime.Year + 1, 1, 10, 23, 59, 59);
                    }
                    else
                    {
                        invoiceDueDate = new DateTime(currentTime.Year, currentTime.Month + 1, 10, 23, 59, 59);
                    }
                }
            }
            else
            {
                if (currentTime.Day >= 1 && currentTime.Day <= 15)
                {
                    DateTime currentISTDate = Util.GetIST();
                    int daysInMonth = DateTime.DaysInMonth(currentISTDate.Year, currentISTDate.Month);
                    invoiceDueDate = new DateTime(currentTime.Year, currentTime.Month, daysInMonth, 23, 59, 59);
                }
                else
                {
                    if (currentTime.Month == 12)
                    {
                        invoiceDueDate = new DateTime(currentTime.Year + 1, 1, 15, 23, 59, 59);
                    }
                    else
                    {
                        invoiceDueDate = new DateTime(currentTime.Year, currentTime.Month + 1, 15, 23, 59, 59);
                    }
                }
            }
            Trace.TraceInformation("Invoice.GetInvoiceDueDate exited.");
        }

        public void GetInvoiceDueDate(DateTime currentTime, bool isDomestic)
        {
            Trace.TraceInformation("Invoice.GetInvoiceDueDate entered.");
            //DateTime currentTime = Util.GetIST();
            ////TODO: This 13 and 28 are hard coded later it will be in config file
            if (isDomestic)
            {
                if (currentTime.Day >= 1 && currentTime.Day <= 15)
                {
                    invoiceDueDate = new DateTime(currentTime.Year, currentTime.Month, 25, 23, 59, 59);
                }
                else
                {
                    if (currentTime.Month == 12)
                    {
                        invoiceDueDate = new DateTime(currentTime.Year + 1, 1, 10, 23, 59, 59);
                    }
                    else
                    {
                        invoiceDueDate = new DateTime(currentTime.Year, currentTime.Month + 1, 10, 23, 59, 59);
                    }
                }
            }
            else
            {
                if (currentTime.Day >= 1 && currentTime.Day <= 15)
                {
                  //  DateTime currentISTDate = Util.GetIST();
                    int daysInMonth = DateTime.DaysInMonth(currentTime.Year, currentTime.Month);
                    invoiceDueDate = new DateTime(currentTime.Year, currentTime.Month, daysInMonth, 23, 59, 59);
                }
                else
                {
                    if (currentTime.Month == 12)
                    {
                        invoiceDueDate = new DateTime(currentTime.Year + 1, 1, 15, 23, 59, 59);
                    }
                    else
                    {
                        invoiceDueDate = new DateTime(currentTime.Year, currentTime.Month + 1, 15, 23, 59, 59);
                    }
                }
            }
            Trace.TraceInformation("Invoice.GetInvoiceDueDate exited.");
        }
        public void SaveOfflineBooking()
        {
            Trace.TraceInformation("Invoice.SaveOfflineBooking entered : invoice no = " + invoiceNumber);
            int rowsAffected = 0;
            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
            }
            if ((int)status == 0)
            {
                throw new ArgumentException("status must have a value", "status");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a positive non zero integer value", "createdBy");
            }
            SqlParameter[] paramList = new SqlParameter[11];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@docTypeCode", docTypeCode);
            paramList[2] = new SqlParameter("@documentNumber", documentNumber);
            paramList[3] = new SqlParameter("@agencyId", agencyId);
            paramList[4] = new SqlParameter("@status", (int)status);
            paramList[5] = new SqlParameter("@paymentMode", paymentMode);
            paramList[6] = new SqlParameter("@remarks", remarks);
            paramList[7] = new SqlParameter("@invoiceDueDate", invoiceDueDate);
            paramList[8] = new SqlParameter("@createdOn", createdOn);
            paramList[9] = new SqlParameter("@createdBy", createdBy);

            //int agencyTypeId = Agency.GetAgencyTypeId(agencyId); // ziya-todo
            int agencyTypeId = 1;
    
            for (int i = 0; i < lineItem.Count; i++)
            {
//                if (agencyTypeId == (int)Agencytype.Service) ziya-todo
                if (agencyTypeId == 1)
                {
                totalPrice = totalPrice + Math.Round(lineItem[i].Price.GetServiceAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                }
                else
                {
                totalPrice = totalPrice + Math.Round(lineItem[i].Price.GetAgentPrice(), Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                }
              
            }

            paramList[10] = new SqlParameter("@totalPrice", totalPrice);

            try
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddOfflineInvoice, paramList);
            }
            catch (CT.TicketReceipt.DataAccessLayer.DALException exQc)
            {
                Trace.TraceInformation("Invoice.Save Violate Unique Key Constraints Error: " + exQc.Message);
                // If method fails once it should try once again.
                throw exQc;
            }

            invoiceNumber = Convert.ToInt32(paramList[0].Value);

            for (int i = 0; i < lineItem.Count; i++)
            {
                InvoiceLineItem line = lineItem[i];
                line.InvoiceNumber = invoiceNumber;
                line.CreatedBy = createdBy;
                line.CreatedOn = createdOn;
                line.Save();
                lineItem[i] = line;                
            }
           
            Trace.TraceInformation("Invoice.Save exiting : rowsAffected = " + rowsAffected);
        }
        public static int GetInvoiceNumber(int documentNumber, string docTypeCode)
        {
            Trace.TraceInformation("Invoice.GetInvoiceNumber entered : documentNumber= " + documentNumber + " docTypeCode " + docTypeCode);
            int invoiceNumber = 0;
            if (documentNumber <= 0)
            {
                throw new ArgumentException("Document number should be positive integer document number=" + documentNumber);
            }
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@documentNumber", documentNumber);
            paramList[1] = new SqlParameter("@docTypeCode", docTypeCode);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetInvoiceNumber, paramList);
            if (data.Read())
            {
                invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetInvoiceOfTxn exited : invoiceNumber= " + invoiceNumber);
            return invoiceNumber;
        }
        /// <summary>
        /// Gets SaleReport
        /// </summary>
        /// <param name="stateId">Unique Id of Any State</param>
        /// <returns>DataTable contains the information about Agency</returns>
        public static DataTable GetSalesReport(string csvAgencyId, DateTime startingDate, DateTime endingDate, bool isInternational, bool isDomestic, bool isLCC, bool isNonLCC,string agencyTypeString, string airLineCodesString)
        {
            DateTime sDate = startingDate;
            Trace.TraceInformation("Invoice.GetSalesReport entered csvAgencyId =" + csvAgencyId);
            DataTable getSalesReport = new DataTable();
            SqlParameter[] paramList = new SqlParameter[9];
            startingDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
            endingDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
            startingDate = Util.ISTToUTC(startingDate);
            endingDate = Util.ISTToUTC(endingDate);
            paramList[0] = new SqlParameter("@csvAgencyId", csvAgencyId);
            paramList[1] = new SqlParameter("@startingDate", startingDate);
            paramList[2] = new SqlParameter("@endingDate", endingDate);
            paramList[3] = new SqlParameter("@isInternational", isInternational);
            paramList[4] = new SqlParameter("@isDomestic", isDomestic);
            paramList[5] = new SqlParameter("@isLCC", isLCC);
            paramList[6] = new SqlParameter("@isNonLCC", isNonLCC);
            paramList[7] = new SqlParameter("@agencyTypeString", agencyTypeString);
            paramList[8] = new SqlParameter("@airLineCodesString",airLineCodesString);
            //getSalesReport = DBGateway.FillDataTableSP(SPNames.GetSalesReport, paramList, ConncetionStringUsingModule.ReportingServer);
            getSalesReport = DBGateway.FillDataTableSP(SPNames.GetSalesReport, paramList);
            Trace.TraceInformation("Invoice.GetSalesReport exited count = " + getSalesReport.Rows.Count);
            return getSalesReport;
        }

        /// <summary>
        /// Function for getting MiscSalesReportData
        /// </summary>
        /// <param name="AgencyId"></param>
        /// <param name="StartingDate"></param>
        /// <param name="EndingDate"></param>
        /// <returns></returns>
        public static DataTable GetMiscSalesReport(string AgencyIdString, DateTime StartingDate, DateTime EndingDate,bool IsInternationalHotel,bool IsDomesticHotel,bool IsInsurance,bool IsSiteSeeing, bool IsTransfer,bool isIndianRailway)
        {

            DateTime sDate = StartingDate;
            Trace.TraceInformation("Invoice.GetMiscSalesReport enetered agencyId=" + AgencyIdString);
            DataTable getSalesReport = new DataTable();
            SqlParameter[] paramList = new SqlParameter[9];
            StartingDate = new DateTime(StartingDate.Year, StartingDate.Month, StartingDate.Day, 0, 0, 0);
            EndingDate = new DateTime(EndingDate.Year, EndingDate.Month, EndingDate.Day, 23, 59, 59);

            StartingDate = Util.ISTToUTC(StartingDate);
            EndingDate = Util.ISTToUTC(EndingDate);

            paramList[0] = new SqlParameter("@startingDate", StartingDate);
            paramList[1] = new SqlParameter("@endingDate",EndingDate);
            paramList[2] = new SqlParameter("@agencyIdString", AgencyIdString);
            paramList[3] = new SqlParameter("@isInternationalHotel",IsInternationalHotel);
            paramList[4] = new SqlParameter("@isDomesticHotel",IsDomesticHotel);
            paramList[5] = new SqlParameter("@isInsurance",IsInsurance);
            paramList[6] = new SqlParameter("@isSiteSeeing",IsSiteSeeing);
            paramList[7] = new SqlParameter("@isTransfer",IsTransfer);
            paramList[8] = new SqlParameter("@isIndianRailway",isIndianRailway);
//            getSalesReport = DBGateway.FillDataTableSP(SPNames.GetMiscSalesReport, paramList, ConncetionStringUsingModule.ReportingServer);
            getSalesReport = DBGateway.FillDataTableSP(SPNames.GetMiscSalesReport, paramList);
            Trace.TraceInformation("Invoice.GetSalesReport exited count = " + getSalesReport.Rows.Count);
            return getSalesReport;
        }
        /// <summary>
        /// Gets hotel sales report
        /// </summary>
        /// <param name="csvAgencyId"></param>
        /// <param name="startingDate"></param>
        /// <param name="endingDate"></param>
        /// <param name="isInternational"></param>
        /// <param name="isDomestic"></param>
        /// <returns></returns>
        public static DataTable GetHotelSalesReport(string csvAgencyId, DateTime startingDate, DateTime endingDate, bool isInternational, bool isDomestic)
        {
            Trace.TraceInformation("Invoice.GetHotelSalesReport entered csvAgencyId =" + csvAgencyId);
            DataTable getSalesReport = new DataTable();
            SqlParameter[] paramList = new SqlParameter[3];
            startingDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
            endingDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
            startingDate = Util.ISTToUTC(startingDate);
            endingDate = Util.ISTToUTC(endingDate);
            paramList[0] = new SqlParameter("@csvAgencyId", csvAgencyId);
            paramList[1] = new SqlParameter("@startingDate", startingDate);
            paramList[2] = new SqlParameter("@endingDate", endingDate);
            try
            {
                if (isDomestic)
                {
                    getSalesReport = DBGateway.FillDataTableSP(SPNames.GetHotelSalesReport, paramList);
                }
                else
                {
                    getSalesReport = DBGateway.FillDataTableSP(SPNames.GetIntlHotelSalesReport, paramList);
                }
            }
            catch (CT.TicketReceipt.DataAccessLayer.DALException exQc)
            {
                Trace.TraceInformation("Invoice.Save Violate Unique Key Constraints Error: " + exQc.Message);
                //TODO: Audit
                // If method fails once it should try once again.
                throw exQc;
            }
            Trace.TraceInformation("Invoice.GetHotelSalesReport exited count = " + getSalesReport.Rows.Count);
            return getSalesReport;
        }

        /// <summary>
        /// Gets Refund Report
        /// </summary>  
        /// <returns>DataTable contains the information about refund report</returns>
        public static DataTable GetRefundReport(string csvAgencyId, DateTime startingDate, DateTime endingDate)
        {
            Trace.TraceInformation("Invoice.GetRefundReport entered csvAgencyId =" + csvAgencyId);
            DataTable getSalesReport = new DataTable();
            SqlParameter[] paramList = new SqlParameter[3];
            startingDate = new DateTime(startingDate.Year, startingDate.Month, startingDate.Day, 0, 0, 0);
            endingDate = new DateTime(endingDate.Year, endingDate.Month, endingDate.Day, 23, 59, 59);
            startingDate = Util.ISTToUTC(startingDate);
            endingDate = Util.ISTToUTC(endingDate);
            paramList[0] = new SqlParameter("@csvAgencyId", csvAgencyId);
            paramList[1] = new SqlParameter("@startingDate", startingDate);
            paramList[2] = new SqlParameter("@endingDate", endingDate);            
            //getSalesReport = DBGateway.FillDataTableSP(SPNames.GetRefundReport, paramList,ConncetionStringUsingModule.ReportingServer);
            if (startingDate.CompareTo(Util.ISTToUTC(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0))) == 0)
            {
                getSalesReport = DBGateway.FillDataTableSP(SPNames.GetRefundReportToday, paramList);
            }
            else
            {
                //getSalesReport = DBGateway.FillDataTableSP(SPNames.GetRefundReport, paramList,ConncetionStringUsingModule.ReportingServer); ziya-todo
                getSalesReport = DBGateway.FillDataTableSP(SPNames.GetRefundReport, paramList);
                //getSalesReport = DBGateway.FillDataTableSP(SPNames.GetRefundReport, paramList,ConncetionStringUsingModule.ReportingServer);
            }
            Trace.TraceInformation("Invoice.GetRefundReport exited count = " + getSalesReport.Rows.Count);
            return getSalesReport;
        }
        /// <summary>
        /// Gets the Invoice Number
        /// </summary>
        /// <param name="flightId"></param>
        /// <returns></returns>
        public static int GetInvoiceNumberByFlightId(int flightId)
        {
            Trace.TraceInformation("Invoice.GetInvoiceNumberByFlightId entered : flightId=" + flightId);
            int invoiceNumber = 0;
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetInvoiceNumberByFlightId, paramList);
            if (data.Read())
            {
                invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetInvoiceNumberByFlightId exited : invoiceNumber=" + invoiceNumber);
            return invoiceNumber;
        }
        //
        public static int GetInvoiceNumberByItemId(int itemId,InvoiceItemTypeId itemTypeId)
        {
            Trace.TraceInformation("Invoice.GetInvoiceNumberByItemId entered : itemId=" + itemId);
            int invoiceNumber = 0;
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@itemId", itemId);
            paramList[1] = new SqlParameter("@itemTypeId", itemTypeId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetInvoiceNumberByItemId, paramList);
            if (data.Read())
            {
                invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetInvoiceNumberByFlightId exited : invoiceNumber=" + invoiceNumber);
            return invoiceNumber;
        }
        public static int GetInvoiceNumberByTicketId(int ticketId)
        {
            Trace.TraceInformation("Invoice.GetInvoiceNumberByTicketId entered : ticketId=" + ticketId);
            int invoiceNumber = 0;
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ticketId", ticketId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetInvoiceNumberByTicketId, paramList);
            if (data.Read())
            {
                invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetInvoiceNumberByTicketId exited : invoiceNumber=" + invoiceNumber);
            return invoiceNumber;
        }
        public static int GetInvoiceNumberByOfflineBookingId(int bookingId)
        {
            Trace.TraceInformation("Invoice.GetInvoiceNumberByBookingId entered : bookingId=" + bookingId);
            int invoiceNumber = 0;
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetInvoiceNumberByBookingId, paramList);
            if (data.Read())
            {
                invoiceNumber = Convert.ToInt32(data["invoiceNumber"]);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetInvoiceNumberByBookingId exited : invoiceNumber=" + invoiceNumber);
            return invoiceNumber;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flightId"></param>
        /// <returns></returns>
        public static string[] GetCreditNoteNumberByTicketId(int ticketId)
        {
            Trace.TraceInformation("Invoice.GetCreditNoteNumberByTicketId entered : ticketId=" + ticketId);
            string[] creditNoteNumber = new string[2];
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ticketId", ticketId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetCreditNoteNumberByTicketId, paramList);
            if (data.Read())
            {
                creditNoteNumber = Convert.ToString(data["CreditNoteNumber"]).Split('|');
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetCreditNoteNumberByTicketId exited : CreditNoteNumber=" + creditNoteNumber);
            return creditNoteNumber;
        }

        public static string[] GetCreditNoteNumberStringByOfflineBookingId(int OfflineBookingId)
        {
            Trace.TraceInformation("Invoice.GetCreditNoteNumberStringByOfflineBookingId entered : OfflineBookingId=" + OfflineBookingId);
            string[] creditNoteNumber = new string[2];
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@OfflineBookingId", OfflineBookingId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetCreditNoteNumberStringByOfflineBookingId, paramList);
            if (data.Read())
            {
                creditNoteNumber = Convert.ToString(data["CreditNoteNumber"]).Split('|');
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetCreditNoteNumberStringByOfflineBookingId exited : CreditNoteNumber=" + creditNoteNumber);
            return creditNoteNumber;
        }

        public static int GetCreditNoteNumberByOfflineBookingId(int OfflineBookingId)
        {
            Trace.TraceInformation("Invoice.GetCreditNoteNumberByOfflineBookingId entered : OfflineBookingId=" + OfflineBookingId);
            int creditNoteNumber = 0;
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@OfflineBookingId", OfflineBookingId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetCreditNoteNumberByOfflineBookingId, paramList);
            if (data.Read())
            {
                creditNoteNumber = Convert.ToInt32(data["CreditNoteNumber"]);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetCreditNoteNumberByTicketId exited : CreditNoteNumber=" + creditNoteNumber);
            return creditNoteNumber;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flightId"></param>
        /// <returns></returns>
        public static string[] GetCreditNoteNumberByFlightId(int flightId)
        {
            Trace.TraceInformation("Invoice.GetCreditNoteNumberByFlightId entered : flightId=" + flightId);
            string[] creditNoteNumber = new string[2];
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetCreditNoteNumberByFlightId, paramList);
            if (data.Read())
            {
                creditNoteNumber = Convert.ToString(data["CreditNoteNumber"]).Split('|');
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetCreditNoteNumberByFlightId exited : CreditNoteNumber=" + creditNoteNumber);
            return creditNoteNumber;
        }

        /// <summary>
        /// To get supplier name for given invoiceNumber
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <returns>supplierName</returns>
        public static string GetSupplierByInvoiceNumber(int invoiceNumber)
        {
            Trace.TraceInformation("Invoice.GetSupplierByInvoiceNumber entered : invoiceNumber= " + invoiceNumber);
            string supplierName = string.Empty;
            try
            {
                if (invoiceNumber <= 0)
                {
                    throw new ArgumentException("Invoice Number Id should be positive integer invoiceNumber=" + invoiceNumber);
                }
                //using (SqlConnection connection = DBGateway.GetConnection())
                //{
                    SqlParameter[] paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
                    SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetSupplierByInvoiceNumber, paramList);
                    if (data.Read())
                    {
                        supplierName = data["supplierName"].ToString();
                    }
                    data.Close();
                    //////connection.Close();
                //}
            }
            catch (SqlException ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Account, CT.Core.Severity.High, 0, ex.Message, "");
            }
            Trace.TraceInformation("Invoice.GetSupplierByInvoiceNumber exiting : ");
            return supplierName;
        }

        /// <summary>
        /// Get accepted payments against invoice
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <returns></returns>
        public static decimal GetAcceptedPaymentsAgainstInvoice(int invoiceNumber)
        {
            decimal paymentDone = 0;
            Trace.TraceInformation("Invoice.GetAcceptedPaymentsAgainstInvoice entered : ");
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAcceptedPaymentsAgainstInvoice, paramList);
            while (data.Read())
            {
                if (data["payment"] != DBNull.Value)
                {
                    paymentDone = paymentDone + Convert.ToDecimal(data["payment"]);
                }
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetAcceptedPaymentsAgainstInvoice exited : ");
            return paymentDone;
        }
        /// <summary>
        /// Gets the total amount of accepted payments against partial invoices
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <returns></returns>
        public static decimal GetAmountAgainstPartialInvoicesOfCurrentFortnight(int agencyId)
        {
            Trace.TraceInformation("Invoice.GetAmountAgainstPartialInvoicesOfCurrentFortnight entered ");
            decimal paymentDone = 0;
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAmountAgainstPartialInvoicesOfCurrentFortnight, paramList);
            if (data.Read())
            {
                if (data["amount"] != DBNull.Value)
                {
                    paymentDone = Convert.ToDecimal(data["amount"]);
                }
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetAmountAgainstPartialInvoicesOfCurrentFortnight exited ");
            return paymentDone;
        }
        /// <summary>
        /// Gets the list of all the unpaid invoices whose due date is over
        /// </summary>
        /// <returns></returns>
        public static List<Invoice> GetUnpaidInvoicesWhoseDueDateIsOver(int agencyId)
        {
            Trace.TraceInformation("Invoice.GetUnpaidInvoicesWhoseDueDateIsOver entered : agencyId = " + agencyId);
            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId should be positive integer agencyId=" + agencyId);
            }
            List<Invoice> invoiceList = new List<Invoice>();
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetUnpaidInvoicesWhoseDueDateIsOver, paramList);
            while (dataReader.Read())
            {
                Invoice invoice = new Invoice();
                invoice.invoiceNumber = Convert.ToInt32(dataReader["invoiceNumber"]);
                invoice.docTypeCode = Convert.ToString(dataReader["docTypeCode"]);
                invoice.documentNumber = Convert.ToInt32(dataReader["documentNumber"]);
                invoice.agencyId = Convert.ToInt32(dataReader["agencyId"]);
                invoice.status = (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), dataReader["status"].ToString());
                if (dataReader["remarks"] != DBNull.Value)
                {
                    invoice.remarks = dataReader["remarks"].ToString();
                }
                if (dataReader["invoiceDueDate"] != DBNull.Value)
                {
                    invoice.invoiceDueDate = Convert.ToDateTime(dataReader["invoiceDueDate"]);
                }
                invoice.paymentMode = Convert.ToString(dataReader["paymentMode"]);
                invoice.createdBy = Convert.ToInt32(dataReader["createdBy"]);
                invoice.createdOn = Convert.ToDateTime(dataReader["createdOn"]);
                invoice.lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
                invoice.lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
                invoiceList.Add(invoice);
            }
            dataReader.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetUnpaidInvoicesWhoseDueDateIsOver exiting : ");
            return invoiceList;
        }

        
        public static DataTable GetAllPaymentsAgainstInvoice(int invoiceNumber)
        {
            Trace.TraceInformation("Invoice.GetAllPaymentsAgainstInvoice entered : invoiceNumber= " + invoiceNumber);
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("Invoice Number Id should be positive integer invoiceNumber=" + invoiceNumber);
            }
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            //DBGateway.FillDataTableSP(SPNames.get
            DataTable data = DBGateway.FillDataTableSP(SPNames.GetAllPaymentAgainstInvoice, paramList);
            return (data);
        }        
    }
    public struct InvoiceLineItem
    {
        /// <summary>
        /// unique invoice number
        /// </summary>
        private int invoiceNumber;
        /// <summary>
        /// Gets or sets the invoice number
        /// </summary>
        public int InvoiceNumber
        {
            get { return invoiceNumber; }
            set { invoiceNumber = value; }
        }

        /// <summary>
        /// Reference number of item.
        /// </summary>
        private int itemReferenceNumber;
        /// <summary>
        /// Gets or sets reference number of item.
        /// </summary>
        public int ItemReferenceNumber
        {
            get { return itemReferenceNumber; }
            set { itemReferenceNumber = value; }
        }

        /// <summary>
        /// Description of item
        /// </summary>
        private string itemDescription;
        /// <summary>
        /// Gets or sets the description of item
        /// </summary>
        public string ItemDescription
        {
            get { return itemDescription; }
            set { itemDescription = value; }
        }

        /// <summary>
        /// Id of item type
        /// </summary>
        private int itemTypeId;
        /// <summary>
        /// Gets or sets the Id of item type
        /// </summary>
        public int ItemTypeId
        {
            get { return itemTypeId; }
            set { itemTypeId = value; }
        }
        /// <summary>
        /// Field for Price
        /// </summary>
        private PriceAccounts price;
        /// <summary>
        /// Property for price
        /// </summary>
        public PriceAccounts Price
        {
            get
            {
                return this.price;
            }
            set
            {
                this.price = value;
            }
        }

        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }

        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get { return lastModifiedOn; }
            set { lastModifiedOn = value; }
        }

        public void Save()
        {
            Trace.TraceInformation("InvoiceLineItem.Save entered : invoiceNumber = " + invoiceNumber + ", itemReferenceNumber = " + itemReferenceNumber);
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("invoiceNumber must have a value", "invoiceNumber");
            }
            if (itemReferenceNumber <= 0)
            {
                throw new ArgumentException("itemReferenceNumber must have a value", "itemReferenceNumber");
            }
            if (itemTypeId <= 0)
            {
                throw new ArgumentException("itemTypeId must have a value", "itemTypeId");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a value", "createdBy");
            }
            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[1] = new SqlParameter("@itemReferenceNumber", itemReferenceNumber);
            paramList[2] = new SqlParameter("@itemDescription", itemDescription);
            paramList[3] = new SqlParameter("@itemTypeId", itemTypeId);
            paramList[4] = new SqlParameter("@priceId", price.PriceId);
            paramList[5] = new SqlParameter("@createdBy", createdBy);
            paramList[6] = new SqlParameter("@createdOn", createdOn);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddInvoiceLineItem, paramList);
            Trace.TraceInformation("InvoiceLineItem.Save exiting : rowsAffected = " + rowsAffected);
        }

        public void Save(BookingStatus bookingStatus)
        {
            Trace.TraceInformation("InvoiceLineItem.Save entered : invoiceNumber = " + invoiceNumber + ", itemReferenceNumber = " + itemReferenceNumber);
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("invoiceNumber must have a value", "invoiceNumber");
            }
            if (itemReferenceNumber <= 0)
            {
                throw new ArgumentException("itemReferenceNumber must have a value", "itemReferenceNumber");
            }
            if (itemTypeId <= 0)
            {
                throw new ArgumentException("itemTypeId must have a value", "itemTypeId");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a value", "createdBy");
            }
            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[1] = new SqlParameter("@itemReferenceNumber", itemReferenceNumber);
            paramList[2] = new SqlParameter("@itemDescription", itemDescription);
            paramList[3] = new SqlParameter("@itemTypeId", itemTypeId);
            paramList[4] = new SqlParameter("@priceId", price.PriceId);
            paramList[5] = new SqlParameter("@createdBy", createdBy);
            paramList[6] = new SqlParameter("@createdOn", createdOn);
            int rowsAffected =0;
            if (bookingStatus == BookingStatus.Cancelled || bookingStatus == BookingStatus.Void)
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddCreditNoteLineItem, paramList);
            }
            else
            {
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddInvoiceLineItem, paramList);
            }
            Trace.TraceInformation("InvoiceLineItem.Save exiting : rowsAffected = " + rowsAffected);
        }

        public void Update()
        {
            Trace.TraceInformation("InvoiceLineItem.Save entered : invoiceNumber = " + invoiceNumber + ", itemReferenceNumber = " + itemReferenceNumber);
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("invoiceNumber must have a value", "invoiceNumber");
            }
            if (itemReferenceNumber <= 0)
            {
                throw new ArgumentException("itemReferenceNumber must have a value", "itemReferenceNumber");
            }
            if (itemTypeId <= 0)
            {
                throw new ArgumentException("itemTypeId must have a value", "itemTypeId");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a value", "createdBy");
            }
            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[1] = new SqlParameter("@itemReferenceNumber", itemReferenceNumber);
            paramList[2] = new SqlParameter("@itemDescription", itemDescription);
            paramList[3] = new SqlParameter("@itemTypeId", itemTypeId);
            paramList[4] = new SqlParameter("@priceId", price.PriceId);
            paramList[5] = new SqlParameter("@createdBy", lastModifiedBy);
            paramList[6] = new SqlParameter("@createdOn", DateTime.UtcNow);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateInvoiceLineItem, paramList);
            Trace.TraceInformation("InvoiceLineItem.Save exiting : rowsAffected = " + rowsAffected);
        }

        /// <summary>
        /// updating invoice line item by updating referene number using oldrefrencenumber 
        /// </summary>
        /// <param name="oldItemReferenceNumber">reference number to be replaced with new one</param>
        public void UpdateByReferenceNumber(int oldItemReferenceNumber)
        {
            Trace.TraceInformation("InvoiceLineItem.updaeteByrefrenceid entered : invoiceNumber = " + invoiceNumber + ", itemReferenceNumber = " + itemReferenceNumber);
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("invoiceNumber must have a value", "invoiceNumber");
            }
            if (itemReferenceNumber <= 0)
            {
                throw new ArgumentException("itemReferenceNumber must have a value", "itemReferenceNumber");
            }
            if (itemTypeId <= 0)
            {
                throw new ArgumentException("itemTypeId must have a value", "itemTypeId");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a value", "createdBy");
            }
            SqlParameter[] paramList = new SqlParameter[8];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);
            paramList[1] = new SqlParameter("@itemReferenceNumber", itemReferenceNumber);
            paramList[2] = new SqlParameter("@itemDescription", itemDescription);
            paramList[3] = new SqlParameter("@itemTypeId", itemTypeId);
            paramList[4] = new SqlParameter("@priceId", price.PriceId);
            paramList[5] = new SqlParameter("@createdBy", lastModifiedBy);
            paramList[6] = new SqlParameter("@createdOn", DateTime.UtcNow);
            paramList[7] = new SqlParameter("@oldItemReferenceNumber", oldItemReferenceNumber);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateInvoiceLineItemByReferenceNumber, paramList);
            Trace.TraceInformation("InvoiceLineItem.Save exiting : rowsAffected = " + rowsAffected);
        }

        /// <summary>
        /// Gets the list of line items of a invoice
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <returns></returns>
        public List<InvoiceLineItem> GetLineItems(int invoiceNumber)
        {
            Trace.TraceInformation("Invoice.GetLineItems entered : invoiceNumber= " + invoiceNumber);
            List<InvoiceLineItem> tempList = new List<InvoiceLineItem>();
            if (invoiceNumber <= 0)
            {
                throw new ArgumentException("Invoice Number Id should be positive integer invoiceNumber=" + invoiceNumber);
            }
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@invoiceNumber", invoiceNumber);            
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetLineItems, paramList);
            while (data.Read())
            {
                InvoiceLineItem instance = new InvoiceLineItem();
                instance.InvoiceNumber = invoiceNumber;
                instance.itemDescription = data["itemDescription"].ToString();
                instance.itemReferenceNumber = Convert.ToInt32(data["itemReferenceNumber"]);
                instance.itemTypeId = Convert.ToInt32(data["itemTypeId"]);
                instance.price = new PriceAccounts();
                instance.price.Load(Convert.ToInt32(data["priceId"]));
                createdBy = Convert.ToInt32(data["createdBy"]);
                createdOn = Convert.ToDateTime(data["createdOn"]);
                lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                tempList.Add(instance);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("Invoice.GetLineItems exiting : ");
            return tempList;
        }        
    }
}
