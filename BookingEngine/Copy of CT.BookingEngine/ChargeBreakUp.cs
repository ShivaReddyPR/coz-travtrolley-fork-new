using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
namespace CT.BookingEngine
{
    [Serializable]
    public  class ChargeBreakUp
    {
        #region Private members and Public Variables
        /// <summary>
        /// Price Id of a ticket
        /// </summary>
        private int priceId;
        /// <summary>
        /// Gets or sets priceId
        /// </summary>
        public int PriceId
        {
            get { return priceId; }
            set { priceId = value; }
        }
        /// <summary>
        ///  chargeTypeId of a particular constituent charge in other charges 
        /// </summary>
        private ChargeType chargeType;
        /// <summary>
        /// Gets or sets chargeTypeId
        /// </summary>
        public ChargeType ChargeType
        {
            get { return chargeType; }
            set { chargeType = value; }
        }
        /// <summary>
        /// Amount corresponding to a particular type of charge in other charges
        /// </summary>
        private decimal amount;
        /// <summary>
        /// Gets or sets amount corresponding to a particular type of charge in other charges
        /// </summary>
        public decimal Amount
        { 
            get { return amount; }
            set { amount = value; }
        }

        
        
#endregion
        /// <summary>
        /// Saves a charge breakup.
        /// </summary>
        public void Save() 
        {
            Trace.TraceInformation("ChargeBreakUp.Save entered : PriceId = " + priceId + "");
            if (priceId < 0)
            {
                throw new ArgumentException("priceId must be greater than or equal to zero", "priceId");
            }
            if (((int)chargeType) <= 0)
            {
                throw new ArgumentException("chargeTypeId must be a positive integer", "chargeTypeId");
            }
            
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@priceId", priceId);
            paramList[1] = new SqlParameter("@chargeTypeId", (int)chargeType);
            paramList[2] = new SqlParameter("@amount", amount);

            // insert section
            DBGateway.ExecuteNonQuerySP(SPNames.AddChargeBreakUpEntry, paramList);
            Trace.TraceInformation("ChargeBreakUp.Save exiting ");
        }
        public List<ChargeBreakUp> Load(int priceId) 
        {
           Trace.TraceInformation("ChargeBreakUp.Load entered : priceId = " + priceId);
           List<ChargeBreakUp> tempChargeBreakUp=new List<ChargeBreakUp>();
           if (priceId <= 0)
           {
               throw new ArgumentException("priceId must have a positive non-zero value");
           }
           SqlConnection connection = DBGateway.GetConnection();
           SqlParameter[] paramList = new SqlParameter[1];
           paramList[0] = new SqlParameter("@priceId", priceId);
           SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetChargeBreakUpEntry, paramList);
           //TODO: read data for a priceId.
           while (data.Read())
           {
               ChargeBreakUp temp = new ChargeBreakUp();
               temp.priceId= Convert.ToInt32(data["priceId"]);
               temp.chargeType = (ChargeType)Enum.Parse(typeof(ChargeType), data["chargeTypeId"].ToString());
               temp.amount = Convert.ToDecimal(data["amount"]);
               tempChargeBreakUp.Add(temp);
           }
           data.Close();
           //////connection.Close();
           Trace.TraceInformation("ChargeBreakUp.Load exiting");
           return tempChargeBreakUp;
          
       }
        

    }
}
