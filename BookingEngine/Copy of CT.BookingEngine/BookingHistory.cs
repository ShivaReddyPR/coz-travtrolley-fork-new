using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using CT.Configuration;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    public class BookingHistory
    {
        #region private property
        /// <summary>
        /// Unique Identity of a Booking History
        /// </summary>
        private int bookingHistoryId;
        /// <summary>
        /// EventCategory of event taken from BookingAction
        /// </summary>
        private EventCategory eventCategory;
        /// <summary>
        /// Remarks on the any event of booking
        /// </summary>
        private string remarks;
        /// <summary>
        /// Date and time when the record was created.
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// Unique ID of member who created the record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Date and time when the record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// Unique ID of member who modified the record last.
        /// </summary>
        private int lastModifiedBy;        
        /// <summary>
        /// Unique Id of each Booking Form Booking Detail Table
        /// </summary>
        private int bookingId;
#endregion 

        #region Public Property
        /// <summary>
        /// This is the auto incremented numeric Id for booking history
        /// </summary>
        public int BookingHistoryId
        {
            get
            {
                return bookingHistoryId;
            }            
        }

        /// <summary>
        /// Different Category of Event of event
        /// </summary>
        public EventCategory EventCategory
        {
            get
            {
                return eventCategory;
            }
            set
            {
                eventCategory = value;
            }
        }

        /// <summary>
        /// Date and time when the record was created.
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Unique ID of member who created the record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Date and time when the record was last modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Unique ID of member who modified the record last.
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy  = value;
            }
        }

        /// <summary>
        /// Remark on a particular booking History event
        /// </summary>
        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }

        /// <summary>
        /// Unique Id of each Bookking
        /// </summary>
        public int BookingId
        {
            get
            {
                return bookingId;
            }
            set
            {
                bookingId = value;
            }

        }
        #endregion

        /// <summary>
        /// Retrieve History of a event
        /// </summary>
        /// <param name="bookingId">Booking Id</param>
        /// <returns>Liat of BookingHistory Object</returns>
        public static List<BookingHistory> GetBookingHistory(int bookingId)
        {
            Trace.TraceInformation("BookingHistory.GetBookingHistory entered : bookingId = " + bookingId);
            if (bookingId <= 0)
            {
                throw new ArgumentException("Booking Id should be positive integer", "BookingId");
            }
            List<BookingHistory> listBookingHistory = new List<BookingHistory>();            
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetListBookingHistory, paramList);
            while (data.Read())
            {
                BookingHistory bh = new BookingHistory();                
                bh.bookingId = Convert.ToInt32(data["bookingId"]);
                bh.bookingHistoryId = Convert.ToInt32(data["bookingHistoryId"]);
                if (data["remarks"] != DBNull.Value)
                {
                    bh.remarks = data["remarks"].ToString();
                }
                else
                {
                    bh.remarks = string.Empty;
                }
                bh.eventCategory = (EventCategory)Enum.Parse(typeof(EventCategory), data["eventCategoryId"].ToString());
                bh.createdBy = Convert.ToInt32(data["createdBy"]);
                bh.createdOn = Convert.ToDateTime(data["createdOn"]);
                bh.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                bh.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                listBookingHistory.Add(bh);
            }
            data.Close();
            //connection.Close();
            Trace.TraceInformation("BookingHistory.GetBookingHistory exited : ");
            return listBookingHistory;
        }

       /// <summary>
       /// Save item of Booking History
       /// </summary>
        public void Save()
        {
            Trace.TraceInformation("BookingHistory.Save entered :");
            int rowsAffected = 0;
            if (bookingId <= 0)
            {
                throw new ArgumentException("bookingId must have a positive non zero integer value");
            }            
            if (createdBy <= 0)
            {
                throw new ArgumentException("createdBy must have a positive non zero integer value", "createdBy");
            }            
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@bookingHistoryId", bookingHistoryId);
            paramList[0].Direction = ParameterDirection.Output;
            paramList[1] = new SqlParameter("@bookingId", bookingId);
            paramList[2] = new SqlParameter("@eventCategoryId", (int)eventCategory);            
            paramList[3] = new SqlParameter("@remarks", remarks);
            paramList[4] = new SqlParameter("@createdBy", createdBy);
            rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddBookingHistory, paramList);
            bookingHistoryId = Convert.ToInt32(paramList[0].Value);
            Trace.TraceInformation("BookingHistory.Save exiting : rowsAffected = " + rowsAffected);
        }

        /// <summary>
        /// Retrieve History of a event
        /// </summary>
        /// <param name="bookingId">Booking Id</param>
        /// <returns>Liat of BookingHistory Object</returns>
        public static List<BookingHistory> GetBookingHistoryForProductId(ProductType prodType , int productId)
        {
            Trace.TraceInformation("BookingHistory.GetBookingHistoryForProductId entered : productId = " + productId);
            if (productId <= 0)
            {
                throw new ArgumentException("productId Id should be positive integer", "productId");
            }
            List<BookingHistory> listBookingHistory = new List<BookingHistory>();
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@productId",productId);
            paramList[1] = new SqlParameter("@productType", (int)prodType);            
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetBookingHistoryForProductId, paramList);
            while (data.Read())
            {
                BookingHistory bh = new BookingHistory();
                bh.bookingId = Convert.ToInt32(data["bookingId"]);
                bh.bookingHistoryId = Convert.ToInt32(data["bookingHistoryId"]);
                if (data["remarks"] != DBNull.Value)
                {
                    bh.remarks = data["remarks"].ToString();
                }
                else
                {
                    bh.remarks = string.Empty;
                }
                bh.eventCategory = (EventCategory)Enum.Parse(typeof(EventCategory), data["eventCategoryId"].ToString());
                bh.createdBy = Convert.ToInt32(data["createdBy"]);
                bh.createdOn = Convert.ToDateTime(data["createdOn"]);
                bh.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                bh.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                listBookingHistory.Add(bh);
            }
            data.Close();
            //connection.Close();
            Trace.TraceInformation("BookingHistory.GetBookingHistoryForProductId exited : ");
            return listBookingHistory;
        }
    }

    public enum EventCategory
    {
        Booking = 1,
        Ticketing = 2,
        Payment = 3,
        Miscellaneous = 4,
        HotelCancel = 5,
        HotelAmendment = 6,
        Insurance = 7,
        Refund = 8,
        TrainTicket = 9,
        TrainTicketRefund=10
    }

}
