using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Configuration;
using System.Collections;
using System.Web;
using CT.BookingEngine;
using CT.Core;
using CT.Configuration;
using System.IO;
namespace CT.BookingEngine
{
    /// <summary>
    /// Enum for Type of Change Policy
    /// </summary>
    public enum ChangePoicyType
    {
         Amendment=1,
         Namechange=2,
         Other=3
    }
    [Serializable]
    public class HotelPenality //: Technology.BookingEngine.Product
    {
        //private Variables
        int hotelId;
        int roomId;
        ChangePoicyType policyType;
        HotelBookingSource bookingSource;
        bool isAllowed;
        DateTime fromDate;
        DateTime toDate;
        decimal price;
        string remarks;
        //public Properties.
        public int HotelId
        {
            get { return hotelId; }
            set { hotelId = value; }
        }
        public int RoomId
        {
            get { return roomId; }
            set { roomId = value; }
        }
        public ChangePoicyType PolicyType
        {
            get { return policyType; }
            set { policyType = value; }
        }
        public HotelBookingSource BookingSource
        {
            get { return bookingSource; }
            set { bookingSource = value; }
        }
        public bool IsAllowed
        {
            get { return isAllowed; }
            set { isAllowed = value; }
        }
        public DateTime FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }
        public DateTime ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }
        public decimal Price
        {
            get { return price; }
            set { price = value; }
        }
        /// <summary>
        /// This Method is used to Save the Hotel Penality Data
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("HotelPenality.Save entered.");
            SqlParameter[] paramList = new SqlParameter[9];
            paramList[0] = new SqlParameter("@hotelID", this.hotelId);
            paramList[1] = new SqlParameter("@roomId", this.roomId);
            paramList[2] = new SqlParameter("@policyType", Convert.ToInt16(this.policyType));
            paramList[3] = new SqlParameter("@bookingSource", Convert.ToInt16(this.bookingSource));
            paramList[4] = new SqlParameter("@isAllowed", this.isAllowed);
            paramList[5] = new SqlParameter("@fromDate", this.fromDate);
            paramList[6] = new SqlParameter("@toDate", this.toDate);
            paramList[7] = new SqlParameter("@price", this.price);
            paramList[8] = new SqlParameter("@remarks", this.remarks);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.SaveHotelPenality, paramList);
            Trace.TraceInformation("HotelPenality.Save exiting");
        }
        /// <summary>
        /// This Method is used to Retrive all Hotel Penality details based on Hotel ID
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public List<HotelPenality> GetHotelPenality(int hotelId)
        {
            Trace.TraceInformation("HotelPenality.GetHotelPenality entered.hotel ID:" + hotelId);
            List<HotelPenality> penalityList = new List<HotelPenality>();
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelPenality, paramList);
            while (data.Read())
            {
                HotelPenality penalityInfo = new HotelPenality();
                penalityInfo.HotelId = hotelId;
                if (data["roomId"] != DBNull.Value)
                {
                    penalityInfo.RoomId = Convert.ToInt32(data["roomId"]);
                }
                else
                {
                    penalityInfo.RoomId = 0;
                }
                penalityInfo.IsAllowed = Convert.ToBoolean(data["isAllowed"]);
                penalityInfo.PolicyType = (ChangePoicyType)(Convert.ToInt32(data["policyType"]));
                penalityInfo.Price = Convert.ToDecimal(data["price"]);
                penalityInfo.Remarks = data["remarks"].ToString();
                penalityInfo.ToDate = Convert.ToDateTime(data["toDate"]);
                penalityInfo.FromDate = Convert.ToDateTime(data["fromDate"]);
                penalityInfo.BookingSource = (HotelBookingSource)(Convert.ToInt32(data["hotelSource"]));
                penalityList.Add(penalityInfo);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("HotelPenality.GetHotelPenality exiting");
            return penalityList;
        }

        /// <summary>
        /// This menthod is used to delete the Penality info for a Hotel.
        /// </summary>
        /// <param name="hotelId"></param>
        public void DeletePenalityInfo(int hotelId)
        {
            Trace.TraceInformation("HotelPenality.DeletePenalityInfo entered.Hotel ID:"+hotelId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelId",hotelId);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.DeleteHotelPenality, paramList);
            Trace.TraceInformation("HotelPenality.DeletePenalityInfoDeleteFareBreakDown exiting  Result:"+rowsAffected);
        }
    }

}
