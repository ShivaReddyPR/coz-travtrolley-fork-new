using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
    public class HotelAgents
    {
        # region private variables

        int sourceId;
        int agencyId;
        decimal commission;

        # endregion

        # region public properties

        public int SourceId
        {
            get { return sourceId; }
            set { sourceId = value; }
        }
        public int AgencyId
        {
            get { return agencyId; }
            set { agencyId = value; }
        }
        public decimal Commission
        {
            get { return commission; }
            set { commission = value; }
        }

        # endregion

        #region Methods

        public static DataView GetExceptionHotelAgentByStateSearch(int sourceId, int state)
        {
            Trace.TraceInformation("HotelSource.ExceptionHotelAgentByStateSearch entered : hotelSourceId = " + sourceId);
            if (sourceId <= 0)
            {
                throw new ArgumentException("Invalid HotelSource");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@sourceId", sourceId);
            paramList[1] = new SqlParameter("@state", state);
            DataSet ExceptionAgentSearch = DBGateway.FillSP(SPNames.GetExceptionHotelAgentByStateSearch, paramList);
            Trace.TraceInformation("HotelSource.ExceptionHotelAgentByStateSearch exited.");
            return new DataView(ExceptionAgentSearch.Tables[0]);
        }

        public static DataView GetExceptionHotelAgentByAgencySearch(int sourceId, string agency)
        {
            Trace.TraceInformation("HotelSource.ExceptionHotelAgentByStateSearch entered : hotelSourceId = " + sourceId);
            if (sourceId <= 0)
            {
                throw new ArgumentException("Invalid HotelSource");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@sourceId", sourceId);
            paramList[1] = new SqlParameter("@agency", agency);
            DataSet ExceptionAgentSearch = DBGateway.FillSP(SPNames.GetExceptionHotelAgentByAgencySearch, paramList);
            Trace.TraceInformation("HotelSource.ExceptionHotelAgentByAgencySearch exited.");
            return new DataView(ExceptionAgentSearch.Tables[0]);
        }

        public void AddInExceptionList(int agencyId)
        {
            Trace.TraceInformation("HotelSource.AddInExceptionList entered");
            if (this.commission < 0)
            {
                throw new ArgumentException("Commission can not be less than zero");
            }
            if (this.sourceId <= 0)
            {
                throw new ArgumentException("Invalid HotelSource code");
            }
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@sourceId", this.sourceId);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@commission", this.commission);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddInHotelAgentCommission, paramList);
            Trace.TraceInformation("HotelSource.AddInExceptionList exited. rowsAffected = " + rowsAffected);
        }

        public void LoadDefaultCommission(int sourceId)
        {
            HotelSource hSource = new HotelSource();
            hSource.Load(sourceId);
            this.sourceId = sourceId;
            //Incase of Net fare agent commission is always 0.
            if (hSource.FareTypeId == CT.Core.FareType.Net)
            {
                this.commission = hSource.OurCommission;
            }
            //Incase of Published fare we set agent commission
            else if (hSource.FareTypeId == CT.Core.FareType.Published)
            {
                this.commission = hSource.AgentCommission;
            }
        }
        public void LoadAgentCommission(int sourceId, int agencyId)
        {
            Trace.TraceInformation("HotelAgents.LoadAgentCommission entered");
            if (sourceId <= 0)
            {
                throw new ArgumentException("Invalid HotelSource code");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@sourceId", sourceId);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            //SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAgentHotelCommission, paramList);
            while (data.Read())
            {
                this.agencyId = (int)data["agencyId"];
                this.sourceId = (int)data["sourceId"];
                this.commission = (decimal)data["commission"];
            }
            data.Close();
            //connection.Close();
            Trace.TraceInformation("HotelAgents.LoadAgentCommission exited.");
        }

        public static void DeleteFromHotelAgentCommission(int sourceId, string commaSeperatedString)
        {
            Trace.TraceInformation("HotelAgents.DeleteFromHotelAgentCommission entered");
            if (sourceId <= 0)
            {
                throw new ArgumentException("Invalid HotelSource code");
            }
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@sourceId", sourceId);
            paramList[1] = new SqlParameter("@agencyIdString", commaSeperatedString);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.DeleteFromHotelAgentCommission, paramList);
            Trace.TraceInformation("HotelAgents.DeleteFromHotelAgentCommission exited. rowsAffected = " + rowsAffected);
        }
        public void UpdateHotelAgentCommission(int agencyId, decimal commission)
        {
            Trace.TraceInformation("HotelSource.UpdateHotelAgentCommission entered");
            if (commission < 0)
            {
                throw new ArgumentException("Commission can not be less than zero");
            }
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@sourceId", this.sourceId);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@commission", commission);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHotelAgentCommission, paramList);
            Trace.TraceInformation("HotelSource.UpdateHotelAgentCommission exited. rowsAffected = " + rowsAffected);
        }

        #endregion

    }
}