using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CT.BookingEngine;
using CT.Core;
using CT.Configuration;
using System.IO;
using System.Diagnostics;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.BookingEngine
{
     public enum TransferBookingStatus
    {
        Confirmed = 1,
        Cancelled = 2,
        Failed = 0,
        Pending=3,
        Error = 4
    }
    public enum TransferBookingSource
    {
        GTA=1
    }
   public enum PickDropType { ACCOMODATION, AIRPORT, STATION, PORT, OTHER }
    [Serializable ]
    public class TransferItinerary : CT.BookingEngine.Product
   {
       #region private variables
       int transferId;
       string confirmationNo;
       string cityCode;
       string itemCode;
       string itemName;
       string passInfo;
       PriceAccounts price;
       TransferBookingSource source;
       string cancelId;
       DateTime transferDate;
       PickDropType pickUpType;
       PickDropType dropOffType;
       DateTime createdOn;
       int createdBy;
       DateTime lastModifiedOn;
       int lastModifiedBy;
       //List<TransferVehicle> transferDetails;
       TransferBookingStatus status;
       string pickUpCode;
       string pickUpTime;
       string pickUpDescription;
       string pickUpRemarks;
       string dropOffCode;
       string dropOffTime;
       string dropOffDescription;
       string dropOffRemarks;
       //List<TransferPenalty> penalityInfo;
       string cancelPolicy;
       DateTime lastCancellationDate;
        string bookingRef;
        bool isDomestic;
        bool voucherStatus;
        string transferTime;
        string language;
        int numOfPax;
       #endregion


       #region Public Properties
       /// <summary>
       /// Transfer Id
       /// </summary>
       public int TransferId
       {
           get { return transferId; }
           set { transferId = value; }
       }
       /// <summary>
       /// Confirmation Number
       /// </summary>
       public string ConfirmationNo
       {
           get { return confirmationNo; }
           set { confirmationNo = value; }
       }
       /// <summary>
       /// City Code
       /// </summary>
       public string CityCode
       {
           get { return cityCode; }
           set { cityCode = value; }
       }
       /// <summary>
       /// Item Code
       /// </summary>
       public string ItemCode
       {
           get { return itemCode; }
           set { itemCode = value; }
       }
       /// <summary>
       /// Item Name
       /// </summary>
       public string ItemName
       {
           get { return itemName; }
           set { itemName = value; }
       }
       /// <summary>
       /// Passenger Information
       /// </summary>
       public string PassengerInfo
       {
           get { return passInfo; }
           set { passInfo = value; }
       }
       /// <summary>
       /// Price Information
       /// </summary>
       public PriceAccounts Price
       {
           get { return price; }
           set { price = value; }
       }
       /// <summary>
       /// booking source
       /// </summary>
       public TransferBookingSource Source
       {
           get { return source; }
           set { source = value; }
       }
       /// <summary>
       /// Cancellation ID
       /// </summary>
       public string CancelId
       {
           get { return cancelId; }
           set { cancelId = value; }
       }
       /// <summary>
       /// Transfer Date
       /// </summary>
       public DateTime TransferDate
       {
           get { return transferDate; }
           set { transferDate = value; }
       }
       /// <summary>
       /// The Type of the Pick up
       /// </summary>
       public PickDropType PickUpType
       {
           get { return pickUpType; }
           set { pickUpType = value; }
       }
       /// <summary>
       /// The Type of the DropOff
       /// </summary>
       public PickDropType DropOffType
       {
           get { return dropOffType; }
           set { dropOffType = value; }
       }
       /// <summary>
       /// Tranfer Details Information
       /// </summary>
       //public List<TransferVehicle> TransferDetails  ziya-todo
       //{
       //    get { return transferDetails; }
       //    set { transferDetails = value; }
       //}
       ///// <summary>
       /// Status of the Booking
       /// </summary>
       public TransferBookingStatus BookingStatus
       {
           get { return status; }
           set { status = value; }
       }
       /// <summary>
       /// PickUps Code like Airport Code etc
       /// </summary>
       public string PickUpCode
       {
           get { return pickUpCode; }
           set { pickUpCode = value; }
       }
       /// <summary>
       /// PickUp Time
       /// </summary>
       public string PickUpTime
       {
           get { return pickUpTime; }
           set { pickUpTime = value; }
       }
       /// <summary>
       /// Description about the pickUp
       /// </summary>
       public string PickUpDescription
       {
           get { return pickUpDescription; }
           set { pickUpDescription = value; }
       }
       /// <summary>
       /// Remarks /Other Info about the PickUp
       /// </summary>
       public string PickUpRemarks
       {
           get { return pickUpRemarks; }
           set { pickUpRemarks = value; }
       }

       /// <summary>
       /// DropOffs Code like Airport Code etc
       /// </summary>
       public string DropOffCode
       {
           get { return dropOffCode; }
           set { dropOffCode = value; }
       }
       /// <summary>
       /// DropOff Time
       /// </summary>
       public string DropOffTime
       {
           get { return dropOffTime; }
           set { dropOffTime = value; }
       }
       /// <summary>
       /// Description about the DropOff
       /// </summary>
       public string DropOffDescription
       {
           get { return dropOffDescription; }
           set { dropOffDescription = value; }
       }
       /// <summary>
       /// Remarks /Other Info about the DropOff
       /// </summary>
       public string DropOffRemarks
       {
           get { return dropOffRemarks; }
           set { dropOffRemarks = value; }
       }
       /// <summary>
       /// Gets or sets createdBy
       /// </summary>
       public int CreatedBy
       {
           get
           {
               return createdBy;
           }
           set
           {
               createdBy = value;
           }
       }

       /// <summary>
       /// Gets or sets createdOn Date
       /// </summary>
       public DateTime CreatedOn
       {
           get
           {
               return createdOn;
           }
           set
           {
               createdOn = value;
           }
       }

       /// <summary>
       /// Gets or sets lastModifiedBy
       /// </summary>
       public int LastModifiedBy
       {
           get
           {
               return lastModifiedBy;
           }
           set
           {
               lastModifiedBy = value;
           }
       }

       /// <summary>
       /// Gets or sets lastModifiedOn Date
       /// </summary>
       public DateTime LastModifiedOn
       {
           get
           {
               return lastModifiedOn;
           }
           set
           {
               lastModifiedOn = value;
           }
       }

       //public List<TransferPenalty> PenalityInfo ziya-todo
       //{
       //    get { return penalityInfo; }
       //    set { penalityInfo = value; }
       //}
       public string CancellationPolicy
       {
           get { return cancelPolicy; }
           set { cancelPolicy = value; }
       }
       public DateTime LastCancellationDate
       {
           get { return lastCancellationDate; }
           set { lastCancellationDate = value; }
       }
        public string BookingReference
        {
            get { return bookingRef; }
            set { bookingRef = value; }
        }
        public bool IsDomestic
        {
            get { return isDomestic; }
            set { isDomestic = value; }
        }
        public bool VoucherStatus
        {
            get { return voucherStatus; }
            set { voucherStatus = value; }
        }
        public string TransferTime
        {
            get { return transferTime; }
            set { transferTime = value; }
        }
        public int NumOfPax
        {
            get { return numOfPax; }
            set { numOfPax = value; }
        }
        public string Language
        {
            get { return language; }
            set { language = value; }
        }
       #endregion

       #region Methods
       /// <summary>
       /// This Method is used to Save the Transfer Details.
       /// </summary>
       public override void Save(Product prod)
       {
           Trace.TraceInformation("TransferItinerary.Save entered.");
           TransferItinerary itinearary = (TransferItinerary)prod;
           SqlParameter[] paramList = new SqlParameter[30];
           paramList[0] = new SqlParameter("@transferId",SqlDbType.Int);
           paramList[1] = new SqlParameter("@confirmationNo", itinearary.ConfirmationNo);
           paramList[2] = new SqlParameter("@cityCode", itinearary.CityCode);
           paramList[3] = new SqlParameter("@itemCode", itinearary.ItemCode);
           paramList[4] = new SqlParameter("@itemName", itinearary.ItemName);
           paramList[5] = new SqlParameter("@passInfo", itinearary.PassengerInfo);
           paramList[6] = new SqlParameter("@source", itinearary.Source);
           paramList[7] = new SqlParameter("@cancelId", itinearary.CancelId);
           paramList[8] = new SqlParameter("@pickUpType", itinearary.PickUpType);
           paramList[9] = new SqlParameter("@dropOffType", itinearary.DropOffType);
           paramList[10] = new SqlParameter("@createdBy", itinearary.CreatedBy);
           paramList[11] = new SqlParameter("@lastModifiedBy", itinearary.LastModifiedBy);
           paramList[12] = new SqlParameter("@status", itinearary.BookingStatus);
           paramList[13] = new SqlParameter("@pickUpCode", itinearary.PickUpCode);
           paramList[14] = new SqlParameter("@pickUpTime", itinearary.PickUpTime);
           paramList[15] = new SqlParameter("@pickUpDescription", itinearary.PickUpDescription);
           paramList[16] = new SqlParameter("@pickUpRemarks", itinearary.PickUpRemarks);
           paramList[17] = new SqlParameter("@dropOffCode", itinearary.DropOffCode);
           paramList[18] = new SqlParameter("@dropOffTime", itinearary.DropOffTime);
           paramList[19] = new SqlParameter("@dropOffDescription", itinearary.DropOffDescription);
           paramList[20] = new SqlParameter("@dropOffRemarks", itinearary.DropOffRemarks);
           paramList[21] = new SqlParameter("@cancelPolicy", itinearary.CancellationPolicy);
           paramList[22] = new SqlParameter("@lastCancellationDate", itinearary.LastCancellationDate);
           paramList[23] = new SqlParameter("@bookingRef", itinearary.BookingReference);
           paramList[24] = new SqlParameter("@isDomestic", itinearary.IsDomestic);
           paramList[25] = new SqlParameter("@voucherStatus", itinearary.VoucherStatus);
           paramList[26] = new SqlParameter("@transferDate", itinearary.TransferDate);
           paramList[27] = new SqlParameter("@transferTime", itinearary.TransferTime);
           paramList[28] = new SqlParameter("@numOfPax", itinearary.NumOfPax);
           paramList[29] = new SqlParameter("@language", itinearary.Language);
           paramList[0].Direction = ParameterDirection.Output;
           int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddTransferItinerary, paramList);
           transferId = (int)paramList[0].Value;
           //foreach(TransferVehicle vehInfo in itinearary.TransferDetails) ziya-todo
           //{
           //    vehInfo.ItemPrice.Save();
           //    vehInfo.TransferId = transferId;
           //    vehInfo.Save();
           //}
           //if (itinearary.PenalityInfo != null) ziya-to-do
           //{
           //    foreach (TransferPenalty penalityInfo in itinearary.PenalityInfo)
           //    {
           //        penalityInfo.TransferId = transferId;
           //        penalityInfo.Save();
           //    }
           //}
           Trace.TraceInformation("TransferItinerary.Save Exit.");
       }

       /// <summary>
       /// This Method is used to Load the  Transfer Details based on Transfer ID.
       /// </summary>
       /// <param name="transferId"></param>
       public void Load(int trId)
       {
           Trace.TraceInformation("TransferItinerary.Load entered : transferId = " + trId);
           if (trId <= 0)
           {
               throw new ArgumentException("TransferId Id should be positive integer");
           }
           this.transferId = trId;
           //////SqlConnection connection = DBGateway.GetConnection();
           SqlParameter[] paramList = new SqlParameter[1];
           paramList[0] = new SqlParameter("@transferId", transferId);
           try
           {

               SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetTransferItinerary, paramList);
               if (data.Read())
               {
                   confirmationNo = data["confirmationNo"].ToString();
                   transferId = trId;
                   cityCode = data["cityCode"].ToString();
                   itemCode = data["itemCode"].ToString();
                   itemName = data["itemName"].ToString();
                   passInfo = data["passInfo"].ToString();
                   source = (TransferBookingSource)Convert.ToInt16(data["source"]);
                   cancelId = data["cancelId"].ToString();
                   pickUpType = (PickDropType)Convert.ToInt16(data["pickUpType"]);
                   dropOffType = (PickDropType)Convert.ToInt16(data["dropOffType"]);
                   createdBy = Convert.ToInt32(data["createdBy"]);
                   createdOn = Convert.ToDateTime(data["createdOn"]);
                   lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                   status = (TransferBookingStatus)Convert.ToInt32(data["status"]);
                   pickUpCode = data["pickUpCode"].ToString();
                   pickUpTime = data["pickUpTime"].ToString();
                   pickUpDescription = data["pickUpDescription"].ToString();
                   pickUpRemarks = data["pickUpRemarks"].ToString();
                   dropOffCode = data["dropOffCode"].ToString();
                   dropOffTime = data["dropOffTime"].ToString();
                   dropOffDescription = data["dropOffDescription"].ToString();
                   dropOffRemarks = data["dropOffRemarks"].ToString();
                   cancelPolicy = data["cancelPolicy"].ToString();
                   lastCancellationDate = Convert.ToDateTime(data["lastCancellationDate"]);
                   bookingRef = data["bookingRef"].ToString();
                   isDomestic = Convert.ToBoolean(data["isDomestic"]);
                   voucherStatus = Convert.ToBoolean(data["voucherStatus"]);
                   cancelId = data["cancelId"].ToString();
                   transferDate = Convert.ToDateTime(data["transferDate"]);
                   transferTime = data["transferTime"].ToString();
                   numOfPax = Convert.ToInt32(data["numOfPax"]);
                   language = data["language"].ToString();
               }
               else
               {
                   data.Close();
                   //////connection.Close();
                   Trace.TraceInformation("TransferItinerary.Load exiting : transferId does not exist.transferId = " + trId.ToString());
               }
               data.Close();
               //////connection.Close();
               //TransferVehicle vehInfo=new TransferVehicle(); ziya-todo
               //transferDetails = vehInfo.Load(trId);
               //TransferPenalty penality = new TransferPenalty();
               //penalityInfo = penality.GetTransferPenality(trId);
           }
           catch (Exception exp)
           {
               //////connection.Close();
               throw new ArgumentException("Transfer id does not exist in database");
           }
       }
        /// <summary>
        /// This Method is used to get TransferId using Confirmation Number
        /// </summary>
        /// <param name="confNo"></param>
        /// <returns></returns>
        public static int GetTransferId(string confNo)
        {
            Trace.TraceInformation("TransferItinerary.GetTransferId entered : confNo = " + confNo);
            int transferId = 0;
            //////SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@confNo", confNo);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetTransferId, paramList);
            if (data.Read())
            {
                transferId = Convert.ToInt32(data["transferId"]);
            }
            data.Close();
            //////connection.Close();
            Trace.TraceInformation("TransferItinerary.GetTransferId exiting :" + transferId.ToString());
            return transferId;
        }

       /// <summary>
       /// This Method is used to Create and Sends the mail.
       /// </summary>
       public static void CreateMail()
       {

       }
        /// <summary>
        /// This Method is sued to Update the Voucher status
        /// </summary>
        public void UpdateVoucherStatus()
        {
            Trace.TraceInformation("TransferItinerary.UpdateVoucherStatus entered  ");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@voucherStatus", voucherStatus);
            paramList[1] = new SqlParameter("@transferId", transferId);

            int retVal = DBGateway.ExecuteNonQuerySP(SPNames.UpdateTransferVoucherStatus, paramList);
            Trace.TraceInformation("TransferItinerary.UpdateVoucherStatus exiting count" + retVal);
        }

        /// <summary>
        /// This MEthod is used to Update the Itinerary BookingStatus
        /// </summary>
        public void UpdateBookingStatus()
        {
            Trace.TraceInformation("TransferItinerary.UpdateBookingStatus entered  ");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@status", status);
            paramList[1] = new SqlParameter("@cancelId", cancelId);
            paramList[2] = new SqlParameter("@transferId", transferId);

            int retVal = DBGateway.ExecuteNonQuerySP(SPNames.UpdateTransferItineary, paramList);
            Trace.TraceInformation("TransferItinerary.UpdateBookingStatus  exiting count" + retVal);
        }
       #endregion


   }
}
