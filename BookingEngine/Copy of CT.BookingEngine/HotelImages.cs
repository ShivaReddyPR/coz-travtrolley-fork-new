using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Configuration;
using System.Collections;
using System.Web;
using CT.BookingEngine;
using CT.Core;
using CT.Configuration;
using System.IO;
using System.Net;
namespace CT.BookingEngine
{
    public class HotelImages
    {
        private int id;
        private string hotelCode;
        private string images;
        private string cityCode;
        private string downloadedImgs;
        private HotelBookingSource source;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string HotelCode
        {
            get { return hotelCode; }
            set { hotelCode = value; }

        }
        public string Images
        {
            get { return images; }
            set { images = value; }
        }
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }

        public string DownloadedImgs
        {
            get { return downloadedImgs; }
            set { downloadedImgs = value; }
        }

        public HotelBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }

        public void Load(string hotel, string cityCode, HotelBookingSource hSource)
        {
            Trace.TraceInformation("HotelImages.Load entered : hotelCode = " + hotel + " hotelSource = " + hSource);
            if (hotelCode != null && hotelCode.Length == 0)
            {
                throw new ArgumentException("hotelCode Should be enterd !", "hotelCode");
            }
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@hotelCode", hotel);
            paramList[1] = new SqlParameter("@cityCode", cityCode);
            paramList[2] = new SqlParameter("@source", (int)hSource);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetHotelImagesByCode, paramList);
            if (data.Read())
            {
                this.cityCode = cityCode;
                hotelCode = data["hotelCode"].ToString();
                images = data["imageFile"].ToString();
                downloadedImgs = data["images"].ToString();
                source = (HotelBookingSource)Convert.ToInt16(data["source"]);
            }
            data.Close();
            //connection.Close();
            Trace.TraceInformation("HotelImages.Load Exit ");
        }

        public void Save()
        {
            Trace.TraceInformation("HotelImages.Save entered.");

            // no  need to Download Images here since we download it there in source dll
            //downloadedImgs = DownloadImage(images, source);
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@hotelCode", hotelCode);
            paramList[1] = new SqlParameter("@cityCode", cityCode);
            paramList[2] = new SqlParameter("@imageFile", images);
            paramList[3] = new SqlParameter("@images", downloadedImgs);
            paramList[4] = new SqlParameter("@source", (int)source);

            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddHotelImages, paramList);
            Trace.TraceInformation("HotelImages.Save Exit.");
        }

        public string DownloadImage(string images, HotelBookingSource hSource)
        {
            Trace.TraceInformation("HotelImages.DownloadImage entered.");
            // Download image from given URL and save image name only in database.
            string imagesURL = string.Empty;
            string[] imgURL = images.Replace("\\", "/").Split('|');
            string imgPath = string.Empty;
            if (hSource == HotelBookingSource.GTA)
            {
                imgPath = Configuration.ConfigurationSystem.GTAConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.HotelBeds)
            {
                imgPath = Configuration.ConfigurationSystem.HotelBedsConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.Tourico)
            {
                imgPath = Configuration.ConfigurationSystem.TouricoConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.IAN)
            {
                imgPath = Configuration.ConfigurationSystem.IANConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.Miki)
            {
                imgPath = Configuration.ConfigurationSystem.MikiConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.Travco)
            {
                imgPath = Configuration.ConfigurationSystem.TravcoConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.DOTW)
            {
                imgPath = Configuration.ConfigurationSystem.DOTWConfig["downloadImagesPath"].ToString() + "\\";
            }
            else if (hSource == HotelBookingSource.WST)
            {
                imgPath = Configuration.ConfigurationSystem.WSTConfig["downloadImagesPath"].ToString() + "\\";
            }
            for (int k = 0; k < imgURL.Length - 1; k++)
            {
                WebClient Client = new WebClient();
                string guidValue = Guid.NewGuid().ToString();
                string[] fileName = imgURL[k].Split('/');
                string imageFileName = fileName[fileName.Length - 1];
                try
                {
                    Client.DownloadFile(imgURL[k], imgPath + guidValue + imageFileName);
                    if (imagesURL.Length > 0)
                    {
                        imagesURL += "|";
                    }
                    imagesURL += guidValue + imageFileName;
                }
                catch (Exception excep)
                {
                    Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.Low, 0, "HotelImage.DownloadImage Downloading Image Failed Message : " + excep.Message, "");
                }
            }
            Trace.TraceInformation("HotelImages.DownloadImage Exit.");
            return imagesURL;
        }
        public void Update()
        {
            Trace.TraceInformation("HotelImages.Update entered.");

            // no  need to Download Images here since we download it there in source dll
            //downloadedImgs = DownloadImage(images, source);
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@hotelCode", hotelCode);
            paramList[1] = new SqlParameter("@cityCode", cityCode);
            paramList[2] = new SqlParameter("@imageFile", images);
            paramList[3] = new SqlParameter("@images", downloadedImgs);
            paramList[4] = new SqlParameter("@source", (int)source);

            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHotelImages, paramList);
            if (rowsAffected == 0)
            {
                Save();
            }
            Trace.TraceInformation("HotelImages.Update Exit.");
        }
    }
}