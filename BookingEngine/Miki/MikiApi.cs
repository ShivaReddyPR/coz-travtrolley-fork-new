using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Net;
using CT.Configuration;
using CT.Core;
using System.Data;
using System.IO.Compression;

namespace CT.BookingEngine.GDS
{
    /// <summary>
    /// This class is used to interact with MIki.
    /// HotelBookingSource Value for MIKi=7
    /// Following is the steps in Booking Flow for this source
    /// STEP-1:HotelSearch(Availability)
    /// STEP-2:Every 15days(what we configuraed in App.config) Need to download StaticData
    /// STEP-3:Booking (Booking)
    /// STEP-4:Cancel
    /// </summary>
    public class MikiApi : IDisposable
    {
        #region Variables
        /// <summary>
        ///  This variable is used store the log file path, which is read from api configuration file
        /// </summary>
        private string XmlPath = string.Empty;
        /// <summary>
        /// reqURL will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string reqURL = string.Empty;
        /// <summary>
        /// agentCode will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string agentCode = string.Empty;
        /// <summary>
        /// requestPassword will be sent in each and every request, which is read from api configuration file
        /// </summary>
        static string requestPassword = string.Empty;
        /// <summary>
        /// userName will be sent in each and every request, which is read from api configuration file
        /// </summary>
        static string userName = string.Empty;
        /// <summary>
        /// requestId will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string requestId = string.Empty;
        /// <summary>
        /// requestTime will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string requestTime = string.Empty;
        /// <summary>
        /// lang will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string lang = string.Empty;
        /// <summary>
        /// paxDomicile will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string paxDomicile = string.Empty;
        /// <summary>
        /// productType will be sent in each and every request, which is read from api configuration file
        /// </summary>
        string productType = string.Empty;
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        string sessionId = string.Empty;
        /// <summary>
        /// this refNo is unquie for every Booking
        /// </summary>
        string refNo = string.Empty;
        /// <summary>
        /// this variable is used to Checking This Booking Test Or Live
        /// </summary>
        bool testMode;
        /// <summary>
        /// This variable is used,to store rateOfExchange value 
        /// </summary>
        private decimal rateOfExchange = 1;
        /// <summary>
        /// This variable is used,to get exchange rates based on the login agent, which is read from MetaserchEngine
        /// </summary>
        private Dictionary<string, decimal> exchangeRates;
        /// <summary>
        /// This variable is used,to be rounded off HotelFare, which is read from MetaserchEngine
        /// </summary>
        private int decimalPoint;
        /// <summary>
        /// This variable is used,to get agent currency based on the login agent, which is read from MetaserchEngine  
        /// </summary>
        private string agentCurrency;
        /// <summary>
        /// This variable is used,to get apicountry code based on the api, which is read from MetaserchEngine  
        /// </summary>
        string sourceCountryCode;  
        #endregion

        #region Properties
        /// <summary>
        /// Exchange rates to be used for conversion in Agent currency from suplier currency
        /// </summary>
        public Dictionary<string, decimal> AgentExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        /// <summary>
        /// Decimal value to be rounded off
        /// </summary>
        public int AgentDecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        /// <summary>
        /// Base currency used by the Agent
        /// </summary>
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        /// <summary>
        /// Api source countryCode
        /// </summary>
        public string SourceCountryCode
        {
            get
            {
                return sourceCountryCode;
            }

            set
            {
                sourceCountryCode = value;
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public MikiApi(string country)
        {
            getRequest(country);
        }
        /// <summary>
        /// To loading initial values, which is read from api config
        /// also Creating Day wise folder
        /// </summary>
        private void getRequest(string country)
        {

            if (ConfigurationSystem.MikiConfig["TestMode"].Equals("true"))
            {
                testMode = true;
            }
            else
            {
                testMode = false;
            }
            //Create Xml Log path folder
            XmlPath = ConfigurationSystem.MikiConfig["XmlLogPath"];
            XmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }

            if (testMode)
            {
                reqURL = ConfigurationSystem.MikiConfig["RequestURL"];
            }
            else
            {
                reqURL = ConfigurationSystem.MikiConfig["RequestProdURL"];
            }
            //if (datePassword.Key == DateTime.UtcNow.ToString("dd/MM/yyyy"))
            //{
            //    requestPassword = datePassword.Value;
            //}
            //else
            //{
            //    datePassword = GetDailyPassword();
            //    requestPassword = datePassword.Value;
            //}
            if (!string.IsNullOrEmpty(country) && country=="IN")//For checking Agent is indian or not
            {
                agentCode = "CZM001";
                requestPassword = "31Y22A18L09W17R61C19";//we need to change code for daily password
                userName = "INTERFACE";
            }
            else
            {
                agentCode = ConfigurationSystem.MikiConfig["AgentCode"];
                requestPassword = ConfigurationSystem.MikiConfig["requestPassword"];
                userName = ConfigurationSystem.MikiConfig["UserName"];
            }
            
            requestTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            lang = ConfigurationSystem.MikiConfig["lang"];
            // paxDomicile = ConfigurationSystem.MikiConfig["PaxDomicile"];
            // paxDomicile = req;
            productType = ConfigurationSystem.MikiConfig["ProductType"];

            requestId = DateTime.Now.ToString("HHmmssdd") + "0";

            refNo = "CZ" + DateTime.Now.ToString("ddMMyyHHmmss");
        }
        /// <summary>
        /// Parmeterized Constructor here only loading first time sessionId
        /// </summary>
        /// <param name="sessionId"></param>
        public MikiApi(string sessionId,string country)
        {
            this.sessionId = sessionId;
            getRequest(country);
        }
        #endregion

        //internal class MikiBookingData
        //{
        //    internal HotelStaticData staticInfo = new HotelStaticData();
        //    internal Dictionary<string, Dictionary<DateTime, decimal>> cancelDetails = new Dictionary<string, Dictionary<DateTime, decimal>>();
        //    internal MikiBookingData()
        //    {
        //    }
        //}

        #region HTTP Request TO MIKI and GETTING Response
        /// <summary>
        ///This method is converting ContentEncoding to Decompress(i.e GZIP, Deflate, default)
        /// </summary>
        /// <param name="webResponse">Response Object</param>
        /// <returns>Stream</returns>
        private static Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }
        /// <summary>
        /// Method for retriving response based on the POST request and url
        /// </summary>
        /// <param name="requestXML">Request Object</param>
        /// <returns>XmlDocument</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        private XmlDocument SendGetRequest(string requestXML)
        {
            //string responseFromServer = string.Empty;
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string strUrl = string.Empty;
                strUrl = reqURL + requestXML;
                HttpWebResponse objResponse;
                HttpWebRequest objRequest = (HttpWebRequest)HttpWebRequest.Create(strUrl);
                objRequest.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip");
                //objRequest = (HttpWebRequest)HttpWebRequest.Create(strUrl);
                objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (Stream dataStream = GetStreamForResponse(objResponse))
                {
                    xmlDoc.Load(dataStream);
                }
                //Stream dataStream = GetStreamForResponse(objResponse);
                //StreamReader reader = new StreamReader(dataStream);
                //responseFromServer = reader.ReadToEnd();

                //    reader.Close();
                //dataStream.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("Miki-Failed to get response for request : " + ex.Message, ex);
            }
            return xmlDoc;
        }
        #endregion

        #region Hotel Search Request & Response For MIKI V 7.0
        /// <summary>
        /// Get the Hotels for the destination Search criteria.
        /// </summary>
        /// <param name="req">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">This is B2B Markup</param>
        /// <param name="markupType">B2B is MarkupType(F(Fixed) OR P(Percentage)</param>
        /// <param name="hotelCode">Selected HotelCode(Repriceing timeonly)</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>This method only we are sending request to api and get the response</remarks>
        public HotelSearchResult[] GetHotelAvailability(HotelRequest req, decimal markup, string markupType, string hotelCode)
        {

            //Audit.Add(EventType.MikiAvailSearch, Severity.Normal, 1, "MikiApi.GetHotelAvailability Entered", "0");
            HotelSearchResult[] searchRes = new HotelSearchResult[0];

            string request = GenerateAvailabilityRequest(req, hotelCode);

            try
            {
                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(request);
                string filePath =XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_MikiCitySearchRequest.xml";
                XmlDoc.Save(filePath);

            }
            catch { }


            //Audit.Add(EventType.MikiAvailSearch, Severity.Normal, 1, "request message generated", "0");
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();
            try
            {
                xmlResp = SendGetRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.MikiAvailSearch, Severity.High, 0, "Exception returned from Miki.GetHotelAvailability Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            try
            {
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    searchRes = GenerateSearchResults(xmlResp, req, markup, markupType);
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.MikiAvailSearch, Severity.High, 0, "Exception returned from Miki.GetHotelAvailability Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlResp.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);

            }
            //Audit.Add(EventType.MikiAvailSearch, Severity.High, 0, " Response from Miki for Search Request:| " + DateTime.Now + "| request XML:" + request + "|response XML:" + resp, "");
            return searchRes;
        }
        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="req">>HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="hotelCode">Selected HotelCode</param>
        /// <returns>string</returns>
        private string GenerateAvailabilityRequest(HotelRequest req, string hotelCode)
        {
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteStartElement("hotelSearchRequest");
            xmlString.WriteAttributeString("versionNumber", "7.0");

            xmlString.WriteStartElement("requestAuditInfo");
            xmlString.WriteElementString("agentCode", agentCode);
            xmlString.WriteElementString("userName", userName); //Not Mandatory ..Checked in testing Phase
            xmlString.WriteElementString("requestPassword", requestPassword);//daily Password mechanism not using here
            xmlString.WriteElementString("requestID", requestId);
            xmlString.WriteElementString("requestDateTime", requestTime);
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("hotelSearchCriteria");
            xmlString.WriteAttributeString("currencyCode", "USD");
            xmlString.WriteAttributeString("paxNationality", req.PassengerNationality);
            xmlString.WriteAttributeString("languageCode", lang);

            xmlString.WriteStartElement("destination");

            if (hotelCode != null && hotelCode.Length > 0)
            {
                xmlString.WriteStartElement("hotelRefs");
                xmlString.WriteStartElement("productCodes");
                xmlString.WriteElementString("productCode", hotelCode.Trim());
                xmlString.WriteEndElement();
                xmlString.WriteEndElement();
            }
            else
            {

                xmlString.WriteStartElement("cityNumbers");
                xmlString.WriteElementString("cityNumber", req.CityCode);
                xmlString.WriteEndElement();
            }

            xmlString.WriteEndElement();
            xmlString.WriteStartElement("stayPeriod");
            xmlString.WriteElementString("checkinDate", req.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("numberOfNights", Convert.ToString(req.EndDate.Subtract(req.StartDate).Days));
            xmlString.WriteEndElement();
            xmlString.WriteStartElement("rooms");

            int adults = 0;
            int childs = 0;
            List<int> age = new List<int>();

            for (int i = 0; i < req.NoOfRooms; i++)
            {
                adults = req.RoomGuest[i].noOfAdults;
                childs = 0;

                for (int c = 0; c < req.RoomGuest[i].noOfChild; c++)
                {
                    if (req.RoomGuest[i].childAge[c] > 12)
                    {
                        adults = adults + 1;
                    }
                    else if (req.RoomGuest[i].childAge[c] <= 12)
                    {
                        childs = childs + 1;
                        age.Add(req.RoomGuest[i].childAge[c]);
                    }

                }
                xmlString.WriteStartElement("room");
                xmlString.WriteElementString("roomNo", Convert.ToString(i + 1));
                xmlString.WriteStartElement("guests");
                for (int ad = 0; ad < adults; ad++)
                {
                    xmlString.WriteStartElement("guest");
                    xmlString.WriteElementString("type", "ADT");
                    xmlString.WriteEndElement();
                }
                if (childs != 0)
                {

                    for (int ch = 0; ch < childs; ch++)
                    {
                        xmlString.WriteStartElement("guest");
                        xmlString.WriteElementString("type", "CHD");
                        xmlString.WriteElementString("age", age[ch].ToString());
                        xmlString.WriteEndElement();
                    }
                    age.Clear();

                }


                xmlString.WriteEndElement();

                xmlString.WriteEndElement();

            }
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("availabilityCriteria");
            xmlString.WriteElementString("availabilityIndicator", "2");
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("priceCriteria");
            xmlString.WriteElementString("returnBestPriceIndicator", "0");
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("resultDetails");
            xmlString.WriteElementString("returnDailyPrices", "false");
            xmlString.WriteElementString("returnHotelInfo", "1");
            xmlString.WriteElementString("returnSpecialOfferDetails", "1");
            xmlString.WriteEndElement();//resultDetails

            xmlString.WriteEndElement();//hotelSearchCriteria
            xmlString.WriteEndElement(); //hotelSearchRequest

            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// Used in preparing XML for the request.
        /// </summary>
        /// <param name="itemCode">Selected HotelCode</param>
        /// <returns>string</returns>
        private string GenerateProductInformatioRequest(string itemCode)
        {

            //XML Code Here
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);


            xmlString.WriteStartElement("hotelDataDownloadRequest");
            xmlString.WriteAttributeString("versionNumber", "7.0");

            xmlString.WriteStartElement("requestAuditInfo");
            xmlString.WriteElementString("agentCode", agentCode);
            xmlString.WriteElementString("userName", userName);
            xmlString.WriteElementString("requestPassword", requestPassword);
            xmlString.WriteElementString("requestID", requestId);
            xmlString.WriteElementString("requestDateTime", requestTime);
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("hotelDataDownloadCriteria");
            xmlString.WriteAttributeString("infoLevel", "2");//1 for basic information and 2 for full info of items
            xmlString.WriteAttributeString("language", "en");
            xmlString.WriteStartElement("hotelFilterCriteria");
            xmlString.WriteStartElement("hotels");
            xmlString.WriteStartElement("hotel");
            xmlString.WriteElementString("productCode", itemCode);
            //xmlString.WriteStartElement("giataCode");
            //xmlString.WriteEndElement();

            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.Close();


            return strWriter.ToString();
        }
        /// <summary>
        /// This method is used to get all the available hotels from Miki
        /// </summary>
        /// <param name="xmlDoc">xml Response Object</param>
        /// <param name="request">HotelRequest(this object is what we serched paramas like(cityname,checkinData.....)</param>
        /// <param name="markup">This is B2B Markup</param>
        /// <param name="markupType">This is B2B Markup Type EX:F(Fixed) or P(Percentage)</param>
        /// <returns>HotelSearchResult[]</returns>
        /// <remarks>Here only we are calucating B2B markup and InputVat
        ///   Here source Currency means supplierCurrency and source amount means supplier Amount
        ///   and also loading cancellation policy here, in cancellation policy price we are not calucating markup
        ///   means without markup only we are showing cancellation markup
        ///   in cancellation we are adding days buffer,which we are loading api config
        /// </remarks>
        private HotelSearchResult[] GenerateSearchResults(XmlDocument xmlDoc, HotelRequest request, decimal markup, string markupType)
        {

            //Loading All Hotels Static data and images city wise
            //optimized Code
            DataTable dtHotels = HotelStaticData.GetStaticHotelIds(request.CityCode, HotelBookingSource.Miki);
            DataTable dtImages = HotelImages.GetImagesByCityCode(request.CityCode, HotelBookingSource.Miki);
            DataTable dtRooms = new DataTable();
            //dtRooms=GetAllRoomDesc();

            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //// xmlDoc.Load(@"C:\Developments\DotNet\uAPI\Miki\18122015_045318_MikiCitySearchResponse.xml");
            //xmlDoc.Load(stringRead);

            // xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_MikiCitySearchResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            //--------------------------------------------


            HotelSearchResult[] hotelResults;

            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("/hotelSearchResponse/errors/error/description");

            if (ErrorInfo != null)
            {
                XmlNode ErrorSolution = xmlDoc.SelectSingleNode("/hotelSearchResponse/errors/error/Solution");
                string solution = string.Empty;
                if (ErrorSolution != null)
                {
                    solution = ErrorSolution.InnerText;
                }
                Audit.Add(EventType.MikiAvailSearch, Severity.High, 0, "Error Message:" + ErrorInfo.InnerText + "| Solution: " + solution + " | " + DateTime.Now + " | Response XML" + xmlDoc.OuterXml, "");
                throw new BookingEngineException("<br>" + ErrorInfo.InnerText);
            }

            XmlNode tempNode1 = xmlDoc.SelectSingleNode("/hotelSearchResponse/availabilitySummary/totalAvailable");
            if (tempNode1.InnerText == "0")
            {
                Audit.Add(EventType.MikiAvailSearch, Severity.High, 0, "Error returned from Miki. Error Message: No hotel is found| " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                throw new BookingEngineException("<br> No Hotel Details found! ");
            }
            XmlNodeList hotelInfoNode = xmlDoc.SelectNodes("/hotelSearchResponse/hotels/hotel");

            int k = 0;
            hotelResults = new HotelSearchResult[hotelInfoNode.Count];
            if (hotelInfoNode.Count == 0)
            {
                Audit.Add(EventType.MikiAvailSearch, Severity.High, 0, "Error returned from Miki. Error Message: No hotel is found! response not coming from Miki| " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
                throw new BookingEngineException("<br> No Hotel Details found!Response not coming ");
            }
            else
            {
                PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                foreach (XmlNode hInfo in hotelInfoNode)
                {
                    string supplierCurrency = string.Empty;
                    HotelSearchResult hotelResult = new HotelSearchResult();
                    hotelResult.BookingSource = HotelBookingSource.Miki;
                    hotelResult.AvailType = AvailabilityType.Confirmed;
                    XmlNode tempNode;
                    hotelResult.Price = new PriceAccounts();

                    hotelResult.CityCode = request.CityCode;
                    tempNode = hInfo.SelectSingleNode("productCode");

                    if (tempNode != null)
                    {
                        hotelResult.HotelCode = tempNode.InnerText.Trim();
                    }
                    tempNode = hInfo.SelectSingleNode("currencyCode");
                    if (tempNode != null)
                    {
                        supplierCurrency = tempNode.InnerText;
                    }

                    tempNode = hInfo.SelectSingleNode("hotelInfo/hotelName");
                    if (tempNode != null)
                    {
                        hotelResult.HotelName = tempNode.InnerText.Trim();
                    }
                    tempNode = hInfo.SelectSingleNode("hotelInfo/category");
                    if (tempNode != null)
                    {
                        hotelResult.HotelCategory = tempNode.InnerText.Trim();
                    }
                    tempNode = hInfo.SelectSingleNode("hotelInfo/starRating");
                    if (tempNode != null)
                    {
                        hotelResult.Rating = (HotelRating)Convert.ToInt32(tempNode.InnerText.Trim());
                    }



                    string pattern = request.HotelName;
                    if (Convert.ToInt16(hotelResult.Rating) >= (int)request.MinRating && Convert.ToInt16(hotelResult.Rating) <= (int)request.MaxRating)
                    {
                        if (!(pattern != null && pattern.Length > 0 ? hotelResult.HotelName.ToUpper().Contains(pattern.ToUpper()) : true))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }

                    hotelResult.StartDate = request.StartDate;
                    hotelResult.EndDate = request.EndDate;


                    //Commented by brahmam search time avoid Staticinfo downloading
                    ////Get all Product Information 
                    //HotelDetails hotelInfo = new HotelDetails();

                    //hotelInfo = getStaticInfoOfProduct(hotelResult.CityCode, hotelResult.HotelName, hotelResult.HotelCode);


                    #region Commented by brahmam search time avoid Staticinfo downloading
                    DataRow[] hotelStaticData = new DataRow[0];
                    List<string> facilities = new List<string>();
                    try
                    {
                        hotelStaticData = dtHotels.Select("hotelCode='" + hotelResult.HotelCode + "'");
                        if (hotelStaticData != null && hotelStaticData.Length > 0)
                        {
                            hotelResult.HotelDescription = hotelStaticData[0]["description"].ToString();
                            hotelResult.HotelAddress = hotelStaticData[0]["address"].ToString();
                            hotelResult.HotelLocation = hotelStaticData[0]["location"].ToString();
                            hotelResult.HotelContactNo = hotelStaticData[0]["phoneNumber"].ToString();
                            string hFacilities = hotelStaticData[0]["hotelFacility"].ToString();
                            if (!string.IsNullOrEmpty(hFacilities))
                            {
                                string[] facilList = hFacilities.Split('|');
                                foreach (string facl in facilList)
                                {
                                    facilities.Add(facl);
                                }
                            }
                            hotelResult.HotelFacilities = facilities;
                        }
                    }
                    catch { continue; }

                    DataRow[] hotelImages = new DataRow[0];
                    try
                    {
                        hotelImages = dtImages.Select("hotelCode='" + hotelResult.HotelCode + "'");
                        string hImages = (hotelImages != null && hotelImages.Length > 0 ? hotelImages[0]["images"].ToString() : string.Empty);
                        hotelResult.HotelPicture = string.Empty;
                        if (!string.IsNullOrEmpty(hImages))
                        {
                            hotelResult.HotelPicture = hImages.Split('|')[0];
                        }
                    }
                    catch { continue; }
                    #endregion


                    //Getting here Hotel Norms or Essential Info of Miki Item store in HotelResult.PropertyValue
                    XmlNodeList alertNotes = hInfo.SelectNodes("alerts/alert");
                    hotelResult.PropertyType = string.Empty; //added on 02032016 use for Alert Message 
                    foreach (XmlNode alertNote in alertNotes)
                    {

                        tempNode = alertNote.SelectSingleNode("fromDate");
                        if (tempNode != null)
                        {
                            hotelResult.PropertyType += Convert.ToDateTime(tempNode.InnerText.Trim()).ToString("dd-MMM-yyyy");
                        }
                        tempNode = alertNote.SelectSingleNode("toDate");
                        if (tempNode != null)
                        {
                            hotelResult.PropertyType += " To " + Convert.ToDateTime(tempNode.InnerText.Trim()).ToString("dd-MMM-yyyy");
                        }
                        tempNode = alertNote.SelectSingleNode("description");
                        if (tempNode != null)
                        {
                            hotelResult.PropertyType += "," + tempNode.InnerText.Trim() + "|";
                        }
                    }
                    if (hotelResult.PropertyType != null && hotelResult.PropertyType.Length > 0 && hotelResult.PropertyType.EndsWith("|"))
                    {
                        hotelResult.PropertyType = hotelResult.PropertyType.Remove(hotelResult.PropertyType.Length - 1);
                    }




                    #region RoomInfo
                    List<HotelRoomsDetails> roomDetails = new List<HotelRoomsDetails>();
                    XmlNodeList rmrtsInfo = hInfo.SelectNodes("roomOptions/roomOption");
                    try
                    {
                        foreach (XmlNode rmraInfo in rmrtsInfo)
                        {
                            HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                            string rTypeCode = string.Empty;

                            tempNode = rmraInfo;
                            if (tempNode != null)
                            {
                                roomDetail.RoomTypeCode = tempNode.Attributes["roomTypeCode"].Value.Trim();
                                rTypeCode = roomDetail.RoomTypeCode;
                            }

                            tempNode = rmraInfo.SelectSingleNode("roomDescription");
                            if (tempNode != null)
                            {
                                roomDetail.RoomTypeName = tempNode.InnerText.Trim();
                            }

                            tempNode = rmraInfo.SelectSingleNode("rateIdentifier");
                            if (tempNode != null)
                            {
                                // roomDetail.RatePlanCode = tempNode.InnerText.Trim().Split('|')[1];
                                roomDetail.RatePlanCode = tempNode.InnerText.Trim();
                                roomDetail.RoomTypeCode += "|" + roomDetail.RatePlanCode.Split('|')[1] + "|";
                            }
                            //Getting Meals Description
                            tempNode = rmraInfo.SelectSingleNode("mealBasis/includedMeals/includedMeal/description");
                            if (tempNode != null)
                            {
                                roomDetail.mealPlanDesc = tempNode.InnerText.ToString();
                            }

                            #region RoomSequences
                            XmlNodeList roomOptionList = rmraInfo.SelectNodes("roomNumbers/roomNumber");
                            foreach (XmlNode rmNo in roomOptionList)
                            {
                                tempNode = rmNo;
                                if (!string.IsNullOrEmpty(roomDetail.SequenceNo))
                                {
                                    if (tempNode != null)
                                    {
                                        roomDetail.SequenceNo += "|" + tempNode.InnerText;
                                    }
                                }
                                else
                                {
                                    roomDetail.SequenceNo = tempNode.InnerText;
                                }
                            }

                            #endregion

                            //Price Details
                            tempNode = rmraInfo.SelectSingleNode("roomTotalPrice/price");
                            decimal supplierPrice = 0m;
                            if (tempNode != null)
                            {
                                roomDetail.TotalPrice = Convert.ToDecimal(tempNode.InnerText.Trim());
                                supplierPrice = roomDetail.TotalPrice;
                            }
                            rateOfExchange = exchangeRates[supplierCurrency];
                            //VAT Calucation Changes Modified 14.12.2017
                            decimal hotelTotalPrice = 0m;
                            decimal vatAmount = 0m;
                            hotelTotalPrice = Math.Round((roomDetail.TotalPrice) * rateOfExchange, decimalPoint);
                            if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                            {
                                hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);
                            }
                            hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);
                            roomDetail.TotalPrice = hotelTotalPrice;
                            roomDetail.Markup = ((markupType == "F") ? markup : (Convert.ToDecimal(roomDetail.TotalPrice) * (markup / 100m)));
                            roomDetail.MarkupType = markupType;
                            roomDetail.MarkupValue = markup;
                            roomDetail.SellingFare = roomDetail.TotalPrice;
                            roomDetail.TaxDetail = new PriceTaxDetails();
                            roomDetail.TaxDetail = priceTaxDet;
                            roomDetail.InputVATAmount = vatAmount;
                            // roomDetail.Discount = Math.Round((roomDetail.Discount) * rateOfExchange, decimalPoint); //commented on 22022016
                            //Supplier Price Details
                            roomDetail.supplierPrice = supplierPrice;
                            //Hotel Facilities
                            roomDetail.Amenities = facilities;

                            hotelResult.Price = new PriceAccounts();

                            hotelResult.Currency = agentCurrency;
                            //supplier Price Details
                            hotelResult.Price.SupplierCurrency = supplierCurrency;
                            hotelResult.Price.SupplierPrice = Math.Round(supplierPrice);
                            hotelResult.Price.RateOfExchange = rateOfExchange;
                            //hotelResult.Price.Discount = Math.Round(discount); //commented on 22022016

                            //getting room rates for each day

                            System.TimeSpan NoOfDays = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                            roomDetail.Rates = new RoomRates[NoOfDays.Days];
                            decimal totalprice = roomDetail.TotalPrice;
                            for (int i = 0; i < NoOfDays.Days; i++)
                            {
                                decimal perDayPrice = roomDetail.TotalPrice / NoOfDays.Days;
                                if (i == NoOfDays.Days - 1)
                                {
                                    perDayPrice = totalprice;
                                }
                                totalprice -= perDayPrice;
                                roomDetail.Rates[i] = new RoomRates();
                                roomDetail.Rates[i].Amount = perDayPrice;
                                roomDetail.Rates[i].BaseFare = perDayPrice;
                                roomDetail.Rates[i].SellingFare = perDayPrice;
                                roomDetail.Rates[i].Totalfare = perDayPrice;
                                roomDetail.Rates[i].RateType = RateType.Negotiated;
                                roomDetail.Rates[i].Days = hotelResult.StartDate.AddDays(i);


                            }

                            #region Cancellation Details
                            string message = string.Empty;
                            XmlNodeList cancelNodeList = rmraInfo.SelectNodes("cancellationPolicies/cancellationPolicy");
                            foreach (XmlNode cancelNode in cancelNodeList)
                            {
                                //Cancellation Code
                                DateTime canDate = new DateTime();
                                decimal CancelCharge = 0m;
                                string CancelType = string.Empty;

                                tempNode = cancelNode.SelectSingleNode("appliesFrom");
                                string cancelMessage = string.Empty;
                                if (tempNode != null)
                                {
                                    cancelMessage = tempNode.InnerText.Trim();
                                }

                                // cancelMessage = cancelMessage.Replace('T', ' ').Split('+')[0];
                                if (!string.IsNullOrEmpty(cancelMessage))
                                {
                                    try
                                    {
                                        canDate = Convert.ToDateTime(cancelMessage);
                                    }
                                    catch { }
                                }

                                tempNode = cancelNode.SelectSingleNode("cancellationCharge/percentage");
                                if (tempNode != null)
                                {
                                    CancelCharge = Math.Round(Convert.ToDecimal(tempNode.InnerText.Trim()), decimalPoint);
                                    CancelType = "percentage";

                                }


                                tempNode = cancelNode.SelectSingleNode("cancellationCharge/amount");
                                if (tempNode != null)
                                {
                                    CancelCharge = Math.Round(Convert.ToDecimal(tempNode.InnerText.Trim()), decimalPoint);
                                    CancelType = "amount";
                                }


                                decimal cancelMarkup = 0;
                                if (ConfigurationSystem.MikiConfig.ContainsKey("CancellationMarkup"))
                                {
                                    cancelMarkup = Convert.ToDecimal(ConfigurationSystem.MikiConfig["CancellationMarkup"]);
                                }
                                double buffer = 0;
                                if (ConfigurationSystem.MikiConfig.ContainsKey("Buffer"))
                                {
                                    buffer = Convert.ToDouble(ConfigurationSystem.MikiConfig["Buffer"]);
                                }
                                DateTime fromDate = canDate.Date.AddDays(-buffer);
                                if (!string.IsNullOrEmpty(message))
                                {

                                    if (CancelType == "percentage")
                                    {
                                        message += "|Cancelling From " + fromDate.ToString("dd-MMM-yyyy hh:mm:ss tt") + " " + CancelCharge + " %";
                                    }
                                    else if (CancelType == "amount")
                                    {
                                        message += "|Cancelling From " + fromDate.ToString("dd-MMM-yyyy hh:mm:ss tt") + " " + agentCurrency + " " + CancelCharge;
                                    }
                                }
                                else
                                {
                                    message = "No Charge Before " + fromDate.ToString("dd-MMM-yyyy hh:mm:ss tt") + ".";
                                    if (CancelType == "percentage")
                                    {
                                        message += "|Cancelling From " + fromDate.ToString("dd-MMM-yyyy hh:mm:ss tt") + " " + CancelCharge + " %";
                                    }
                                    else if (CancelType == "amount")
                                    {
                                        message += "|Cancelling From " + fromDate.ToString("dd-MMM-yyyy hh:mm:ss tt") + " " + agentCurrency + " " + CancelCharge;
                                    }
                                }

                            }

                            roomDetail.CancellationPolicy = message;

                            #endregion


                            if (roomDetail.SequenceNo.Contains("|"))
                            {
                                string[] sequences = roomDetail.SequenceNo.Split('|');
                                foreach (string sequence in sequences)
                                {
                                    HotelRoomsDetails rd = new HotelRoomsDetails();
                                    rd = roomDetail;
                                    rd.RoomTypeCode = rd.RoomTypeCode + "|" + sequence;
                                    rd.SequenceNo = sequence;
                                    roomDetails.Add(rd);
                                }
                            }
                            else
                            {
                                roomDetail.RoomTypeCode += "|" + roomDetail.SequenceNo;
                                roomDetails.Add(roomDetail);
                            }

                        }
                    }
                    catch (Exception ex)
                    { throw ex; }

                    #endregion


                    hotelResult.RoomGuest = request.RoomGuest;
                    hotelResult.RoomDetails = roomDetails.ToArray();
                    Array.Sort(hotelResult.RoomDetails, delegate(HotelRoomsDetails rd1, HotelRoomsDetails rd2) { return rd1.TotalPrice.CompareTo(rd2.TotalPrice); });
                    if (IsValidResultSet(request, hotelResult.RoomDetails))
                    {
                        hotelResults[k++] = hotelResult;
                    }

                }
                if (hotelInfoNode.Count > k)
                {
                    Array.Resize(ref hotelResults, k);
                }
                //added on 22022016 for get price detail in hotelResult object
                foreach (HotelSearchResult hotelResult in hotelResults)
                {
                    //hotelResult.Price = new PriceAccounts();
                    for (int i = 0; i < request.NoOfRooms; i++)
                    {
                        for (int j = 0; j < hotelResult.RoomDetails.Length; j++)
                        {
                            if (hotelResult.RoomDetails[j].SequenceNo.Contains((i + 1).ToString()))
                            {
                                hotelResult.TotalPrice += hotelResult.RoomDetails[j].TotalPrice + hotelResult.RoomDetails[j].Markup;
                                hotelResult.Price.NetFare = hotelResult.TotalPrice;
                                hotelResult.Price.AccPriceType = PriceType.NetFare;
                                break;
                            }
                        }
                    }
                }
                //return the result.
                return hotelResults;
            }

        }

        #endregion

        #region Hotel Static Information Download MIKI V 7.0
        /// <summary>
        /// getStaticInfoOfProduct
        /// </summary>
        /// <param name="cityCode">Selected CityCode</param>
        /// <param name="itemName">Selected ItemName</param>
        /// <param name="itemCode">Selected HotelCode</param>
        /// <returns>HotelDetails</returns>
        /// <remarks>Every 15days (what we configuraed in App.config) Need to download StaticData
        /// here we are checking if static data is not there our DB we are sending static data request and get the response save our database
        /// </remarks>
        public HotelDetails getStaticInfoOfProduct(string cityCode, string itemName, string itemCode)
        {
            HotelDetails hotelInfo = default(HotelDetails);
            HotelImages imageInfo = new HotelImages();
            HotelStaticData hotelStaticResult = new HotelStaticData();
            int timeStampDays = Convert.ToInt32(ConfigurationSystem.MikiConfig["TimeStamp"]);
            hotelStaticResult.Load(itemCode, cityCode, HotelBookingSource.Miki);
            bool isUpdateReq = false;
            bool imageUpdate = true;
            bool isReqSend = false;
            imageInfo.Load(itemCode, cityCode, HotelBookingSource.Miki);
            if (imageInfo.DownloadedImgs != null && imageInfo.DownloadedImgs.Length > 0)
            {
                imageUpdate = false;
            }
            if (hotelStaticResult.HotelName != null && hotelStaticResult.HotelName.Length > 0 && !string.IsNullOrEmpty(hotelStaticResult.HotelDescription) && !string.IsNullOrEmpty(hotelStaticResult.HotelAddress))
            {
                //if (DateTime.UtcNow.Subtract(hotelStaticResult.TimeStamp).Days < timeStampDays)
                //{
                    isUpdateReq = false;
                    imageUpdate = false;
                    hotelInfo.HotelCode = hotelStaticResult.HotelCode.Trim();
                    hotelInfo.HotelName = hotelStaticResult.HotelName;
                    hotelInfo.Map = hotelStaticResult.HotelMap;

                    hotelInfo.Description = hotelStaticResult.HotelDescription;
                    //imageInfo.Load(itemCode, cityCode, HotelBookingSource.GTA);
                    string[] image = new string[0];
                    if (imageInfo.DownloadedImgs != null && imageInfo.DownloadedImgs.Length > 0)
                    {
                        image = imageInfo.DownloadedImgs.Split(new char[]
						{
							'|'
						});
                    }


                    List<string> imageList = new List<string>();
                    string[] array = image;
                    for (int j = 0; j < array.Length; j++)
                    {
                        string img = array[j];
                        imageList.Add(img);
                    }
                    hotelInfo.Images = imageList;
                    if (imageList != null && imageList.Count > 0)
                    {
                        hotelInfo.Image = imageList[0];
                    }
                    else
                    {
                        hotelInfo.Image = string.Empty;
                    }
                    hotelInfo.Address = hotelStaticResult.HotelAddress;
                    hotelInfo.PhoneNumber = hotelStaticResult.PhoneNumber;
                    hotelInfo.PinCode = hotelStaticResult.PinCode == "" ? string.Empty : hotelStaticResult.PinCode;
                    hotelInfo.Email = hotelStaticResult.EMail;
                    hotelInfo.URL = hotelStaticResult.URL;
                    hotelInfo.hotelRating = hotelStaticResult.Rating;
                    hotelInfo.FaxNumber = hotelStaticResult.FaxNumber;
                    Dictionary<string, string> attractions = new Dictionary<string, string>();
                    string[] attrList = hotelStaticResult.SpecialAttraction.Split(new char[]
					{
						'|'
					});
                    for (int i = 1; i < attrList.Length; i++)
                    {
                        attractions.Add(i.ToString() + ")", attrList[i - 1]);
                    }
                    hotelInfo.Attractions = attractions;
                    string[] hotelFaci = hotelStaticResult.HotelFacilities.Split('|');
                    List<string> facilities = new List<string>();
                    string[] facilList = hotelFaci[0].Split(new char[]
					{
						','
					});
                    array = facilList;
                    for (int j = 0; j < array.Length; j++)
                    {
                        string facl = array[j];
                        facilities.Add(facl);
                    }
                    hotelInfo.HotelFacilities = facilities;
                    if (hotelFaci.Length > 1 && hotelFaci[1] != null)
                    {
                        List<string> roomFacilities = new List<string>();
                        string[] roomFacilList = hotelFaci[1].Split(new char[]
					{
						','
					});
                        array = roomFacilList;
                        for (int j = 0; j < array.Length; j++)
                        {
                            string roomFacl = array[j];
                            roomFacilities.Add(roomFacl);
                        }
                        hotelInfo.RoomFacilities = roomFacilities;
                    }
                    List<string> locList = new List<string>();
                    locList.Add(hotelStaticResult.HotelLocation);
                    if (locList != null)
                    {
                        hotelInfo.Location = locList;
                    }

                //}
                //else
                //{
                //    isUpdateReq = true;
                //    imageUpdate = true;
                //}
            }
            //else
            //{
            //    isReqSend = true;
            //}
            //if (isReqSend || isUpdateReq)
            //{

            //    TextReader tr;
            //    XmlDocument xdc = new XmlDocument();
            //    string ProductInfo = GenerateProductInformatioRequest(itemCode);
            //    tr = new StringReader(ProductInfo);
            //    xdc.Load(tr);
            //    try
            //    {
            //        string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_MikiProductSearchRequest.xml";
            //        xdc.Save(filePath);
            //    }
            //    catch { }
            //    hotelInfo = getProductInformationResponse(ProductInfo);
            //    if (hotelInfo.HotelName != null)
            //    {
            //        hotelStaticResult.HotelCode = itemCode;
            //        hotelStaticResult.CityCode = cityCode;
            //        hotelStaticResult.EMail = string.Empty;
            //        hotelStaticResult.FaxNumber = hotelInfo.FaxNumber;
            //        hotelStaticResult.FromPrice = 0;
            //        hotelStaticResult.HotelAddress = hotelInfo.Address;
            //        hotelStaticResult.HotelDescription = hotelInfo.Description;
            //        if (hotelInfo.Location.Count > 0)
            //        {
            //            foreach (string hotelLocation in hotelInfo.Location)
            //            {
            //                hotelStaticResult.HotelLocation = hotelLocation;
            //            }
            //        }
            //        else
            //        {
            //            hotelStaticResult.HotelLocation = string.Empty;
            //        }
            //        hotelStaticResult.HotelFacilities = string.Empty;
            //        if (hotelInfo.HotelFacilities != null)
            //        {
            //            foreach (string fac in hotelInfo.HotelFacilities)
            //            {
            //                hotelStaticResult.HotelFacilities += fac + ",";
            //            }
            //        }
            //        hotelStaticResult.HotelFacilities += "|";
            //        if (hotelInfo.RoomFacilities != null)
            //        {
            //            if (hotelInfo.RoomFacilities != null)
            //            {
            //                foreach (string rFac in hotelInfo.RoomFacilities)
            //                {
            //                    hotelStaticResult.HotelFacilities += rFac + ",";
            //                }
            //            }
            //        }


            //        hotelStaticResult.HotelMap = string.Empty;
            //        hotelStaticResult.HotelName = hotelInfo.HotelName;
            //        hotelStaticResult.HotelPicture = hotelInfo.Image;
            //        hotelStaticResult.PhoneNumber = hotelInfo.PhoneNumber;
            //        hotelStaticResult.PinCode = hotelInfo.PinCode;
            //        hotelStaticResult.Rating = hotelInfo.hotelRating;
            //        hotelStaticResult.Source = HotelBookingSource.Miki;
            //        hotelStaticResult.SpecialAttraction = string.Empty;
            //        if (hotelInfo.Attractions != null)
            //        {
            //            foreach (KeyValuePair<string, string> hAttr in hotelInfo.Attractions)
            //            {
            //                if (string.IsNullOrEmpty(hotelStaticResult.SpecialAttraction))
            //                {
            //                    hotelStaticResult.SpecialAttraction = hAttr.Value;
            //                }
            //                else
            //                {
            //                    hotelStaticResult.SpecialAttraction += "|" + hAttr.Value;
            //                }
            //            }
            //        }
            //        hotelStaticResult.Status = true;
            //        hotelStaticResult.URL = string.Empty;
            //        hotelStaticResult.Save();



            //        imageInfo.HotelCode = itemCode;
            //        imageInfo.CityCode = cityCode;
            //        imageInfo.DownloadedImgs = string.Empty;
            //        if (hotelInfo.Images != null && hotelInfo.Images.Count > 0)
            //        {
            //            foreach (string img in hotelInfo.Images)
            //            {
            //                imageInfo.DownloadedImgs += img + "|";
            //            }
            //            if (imageInfo.DownloadedImgs.EndsWith("|"))
            //            {
            //                imageInfo.DownloadedImgs = imageInfo.DownloadedImgs.Remove(imageInfo.DownloadedImgs.Length - 1);
            //            }
            //        }
            //        imageInfo.Images = hotelInfo.Image;
            //        imageInfo.Source = HotelBookingSource.Miki;
            //        try
            //        {
            //            if (imageUpdate)
            //            {
            //                imageInfo.Update();
            //            }

            //        }
            //        catch (Exception ex)
            //        {
            //            throw new BookingEngineException("Error: " + ex.Message);
            //        }
            //    }

            //}

            return hotelInfo;
        }

        /// <summary>
        /// getProductInformationResponse
        /// </summary>
        /// <param name="productRequest">StaticData Response Object</param>
        /// <returns>HotelDetails</returns>
        private HotelDetails getProductInformationResponse(string productRequest)
        {
            HotelDetails hotelDetail = default(HotelDetails);
            HotelStaticData hotelStaticResult = new HotelStaticData();
            //TextReader tr;
            XmlDocument xdc = new XmlDocument();
            xdc = SendGetRequest(productRequest);

            //tr = new StringReader(productResponse);
            //xdc.Load(tr);

            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_MikiProductSearchResponse.xml";
                xdc.Save(filePath);
            }
            catch { }


            XmlNode ErrorProdInfo = xdc.SelectSingleNode("/hotelDataDownloadResponse/errors/error/description");

            if (ErrorProdInfo != null)
            {
                XmlNode ErrorSolution = xdc.SelectSingleNode("/hotelDataDownloadResponse/errors/error/Solution");
                string solution = string.Empty;
                if (ErrorSolution != null)
                {
                    solution = ErrorSolution.InnerText;
                }
                Audit.Add(EventType.MikiAvailSearch, Severity.High, 0, "Error Message:" + ErrorProdInfo.InnerText + " | Solution: " + solution + " | " + DateTime.Now + " | Response XML" + xdc.OuterXml, "");
                throw new BookingEngineException("<br>" + ErrorProdInfo.InnerText);
            }
            XmlNodeList prodNodes = xdc.SelectNodes("hotelDataDownloadResponse/productInfo/product");//to be continue after get right logs from supplier end @@@
            foreach (XmlNode hInfo in prodNodes)
            {
                XmlNode tempNode;
                tempNode = hInfo.SelectSingleNode("productDescription/productName");
                if (tempNode != null)
                {
                    hotelDetail.HotelName = tempNode.InnerText.Trim().ToString();
                }

                tempNode = hInfo.SelectSingleNode("productDescription/productDetailText");
                if (tempNode != null)
                {
                    hotelDetail.Description = tempNode.InnerText.Trim().ToString();

                }

                tempNode = hInfo.SelectSingleNode("hotelProductInfo/contactInfo/address/street1");
                if (tempNode != null)
                {
                    hotelDetail.Address = tempNode.InnerText.ToString() + " ";
                }
                tempNode = hInfo.SelectSingleNode("hotelProductInfo/contactInfo/address/street2");
                if (tempNode != null)
                {
                    hotelDetail.Address += tempNode.InnerText.Trim().ToString() + " ";
                }
                tempNode = hInfo.SelectSingleNode("hotelProductInfo/contactInfo/address/street3");
                if (tempNode != null)
                {
                    hotelDetail.Address += tempNode.InnerText.Trim().ToString() + " ";
                }
                tempNode = hInfo.SelectSingleNode("hotelProductInfo/contactInfo/address/city");
                if (tempNode != null)
                {
                    hotelDetail.Address += tempNode.InnerText.Trim().ToString() + " ";

                }
                tempNode = hInfo.SelectSingleNode("hotelProductInfo/contactInfo/address/zipCode");
                if (tempNode != null)
                {
                    hotelDetail.Address += tempNode.InnerText.Trim().ToString() + " ";
                    hotelDetail.PinCode =tempNode.InnerText.Trim();
                }

                tempNode = hInfo.SelectSingleNode("hotelProductInfo/contactInfo/address/country");
                if (tempNode != null)
                {
                    hotelDetail.Address += tempNode.InnerText.Trim().ToString() + " ";
                    hotelDetail.CountryName = tempNode.InnerText.Trim().ToString();
                }
                tempNode = hInfo.SelectSingleNode("hotelProductInfo/contactInfo/telephoneNumber");
                if (tempNode != null)
                {
                    hotelDetail.PhoneNumber = tempNode.InnerText.Trim().ToString();
                }
                tempNode = hInfo.SelectSingleNode("hotelProductInfo/contactInfo/faxNumber");
                if (tempNode != null)
                {
                    hotelDetail.FaxNumber = tempNode.InnerText.Trim().ToString();
                }

                tempNode = hInfo.SelectSingleNode("hotelProductInfo/hotelInfo/starRating");
                if (tempNode != null)
                {

                    string temp = tempNode.InnerText.ToString();
                    switch (temp)
                    {
                        case "1 Star": hotelDetail.hotelRating = HotelRating.OneStar;
                            break;
                        case "2 Star": hotelDetail.hotelRating = HotelRating.TwoStar;
                            break;
                        case "3 Star": hotelDetail.hotelRating = HotelRating.ThreeStar;
                            break;
                        case "4 Star": hotelDetail.hotelRating = HotelRating.FourStar;
                            break;
                        case "5 Star": hotelDetail.hotelRating = HotelRating.FiveStar;
                            break;


                    }
                }

                tempNode = hInfo.SelectSingleNode("hotelProductInfo/hotelInfo/location");
                List<string> hotelLocation = new List<string>();
                if (tempNode != null)
                {
                    hotelLocation.Add(tempNode.InnerText.Trim().ToString());

                }
                if (hotelLocation.Count > 0)
                {
                    hotelDetail.Location = hotelLocation;
                }


                //For Special Attraction
                XmlNodeList xmlAttraction = hInfo.SelectNodes("hotelProductInfo/locationInfo/placeName");
                int attr = 0;
                Dictionary<string, string> hotelAttraction = new Dictionary<string, string>();
                foreach (XmlNode nodeAttraction in xmlAttraction)
                {

                    string locationDetails = string.Empty;

                    if (nodeAttraction != null)
                    {
                        locationDetails += nodeAttraction.InnerText.ToString();

                    }

                    if (nodeAttraction.Attributes["placeType"] != null)
                    {
                        locationDetails += ", " + nodeAttraction.Attributes["placeType"].Value;
                    }

                    if (nodeAttraction.Attributes["distanceFromHotel"] != null)
                    {
                        locationDetails += ", " + nodeAttraction.Attributes["distanceFromHotel"].Value;
                    }

                    if (nodeAttraction.Attributes["distanceUnit"] != null)
                    {
                        locationDetails += " " + nodeAttraction.Attributes["distanceUnit"].Value;
                    }

                    hotelAttraction.Add(attr + "", locationDetails);
                    attr++;
                    //}
                    hotelDetail.Attractions = hotelAttraction;

                }


                //Hotel Facilities
                XmlNode facilitiesNode;
                facilitiesNode = hInfo.SelectSingleNode("hotelProductInfo/hotelInfo/facilities");
                List<string> hotelFacilities = new List<string>();
                if (facilitiesNode != null)
                {
                    tempNode = facilitiesNode.SelectSingleNode("bureauDeChange");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("bureauDeChange");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("businessCentre");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("businessCentre");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("casino");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("casino");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("clubFloor");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("clubFloor");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("coffeeShop");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("coffeeShop");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("conciergeDesk");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("conciergeDesk");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("receptionModems");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("receptionModems");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("businessCentreModems");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("businessCentreModems");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("hairdresser");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("hairdresser");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("nightclub");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("nightclub");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("sprinklers");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("sprinklers");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("shops");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("shops");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("safeDepBox");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        if (tempNode.Attributes["chargeable"] != null)
                        {
                            hotelFacilities.Add("safeDepBox:IsChargeable-" + tempNode.Attributes["chargeable"].Value);
                        }
                        else
                        {
                            hotelFacilities.Add("safeDepBox");
                        }
                    }
                    tempNode = facilitiesNode.SelectSingleNode("petsAllowed");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("petsAllowed");
                    }
                    facilitiesNode = hInfo.SelectSingleNode("hotelProductInfo/hotelInfo/leisureFacilities");
                    tempNode = facilitiesNode.SelectSingleNode("fitnessCentre");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("fitnessCentre");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("jacuzzi");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("jacuzzi");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("solarium");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("solarium");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("outdoorPool");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("outdoorPool");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("indoorPool");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("indoorPool");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("sauna");
                    if (tempNode != null && tempNode.InnerText == "Yes")
                    {
                        hotelFacilities.Add("sauna");
                    }
                    tempNode = facilitiesNode.SelectSingleNode("other");
                    if (tempNode != null && tempNode.InnerText.Length > 0)
                    {
                        hotelFacilities.Add(tempNode.InnerText.ToString());
                    }
                    hotelDetail.HotelFacilities = hotelFacilities;
                }


                XmlNodeList xmlRoomFacilities = hInfo.SelectNodes("hotelProductInfo/roomInfo/roomDetails");

                List<string> roomFacilities = new List<string>();

                foreach (XmlNode nodeRoomFacilities in xmlRoomFacilities)
                {

                    XmlNode roomFacilityNode;
                    if (nodeRoomFacilities.Attributes["stdSup"] != null)
                    {
                        roomFacilities.Add(nodeRoomFacilities.Attributes["stdSup"].Value);
                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("miniBar");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("MiniBar");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("hairDryer");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("HairDryer");
                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("ironing");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("ironing");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("trouserPress");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("trouserPress");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("smokeDetector");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("smokeDetector");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("teaCoffee");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("teaCoffee");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("airCon");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("airCon");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("doubleGlazing");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("doubleGlazing");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("slippers");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("slippers");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("bathrobe");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("bathrobe");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("sprinklers");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("sprinklers");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("checkOutFacility");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("checkOutFacility");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("billViewingFacility");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("billViewingFacility");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("alarmClock");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("alarmClock");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("kitchen");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("kitchen");

                    }
                    roomFacilityNode = nodeRoomFacilities.SelectSingleNode("shampoo");
                    if (roomFacilityNode != null && roomFacilityNode.InnerText.ToString() == "Yes")
                    {
                        roomFacilities.Add("shampoo");

                    }
                    hotelDetail.RoomFacilities = roomFacilities;
                }

                hotelDetail.Image = string.Empty;
                List<string> addImages = new List<string>();
                XmlNodeList imglist = hInfo.SelectNodes("hotelProductInfo/images/image");
                foreach (XmlNode img in imglist)
                {
                    if (img.Attributes["imageURL"] != null)
                    {
                        addImages.Add(img.Attributes["imageURL"].Value.ToString());
                        //hotelDetail.Image +=img.Attributes["imageURL"].Value.ToString()+"|";
                    }
                }
                if (addImages != null)
                {
                    hotelDetail.Images = addImages;
                }
                if (hotelDetail.Images != null && hotelDetail.Images.Count > 0)
                {
                    hotelDetail.Image = hotelDetail.Images[0];
                }


            }


            return hotelDetail;

        }
        #endregion

        #region Booking Request & Response MIKI 7.0
        /// <summary>
        /// GetBooking
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able to book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <exception cref="Exception">Timeout Exception,bad request</exception>
        /// <remarks>Here we are preparing Booking request object and sending book request to api </remarks>
        public BookingResponse GetBooking(ref HotelItinerary itinerary)
        {
            string requestXml = string.Empty;
            requestXml = GenerateBookingRequest(itinerary);
            //string resp = string.Empty;
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.LoadXml(requestXml);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_MikiBookingRequest.xml";
                XmlDoc.Save(filePath);
            }
            catch { }
            try
            {
                // Send request xml and get booking response
                XmlDoc = SendGetRequest(requestXml);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.MikiBooking, Severity.High, 0, "Exception returned from Miki.GetBooking Error Message:" + ex.Message + " | Request XML:" + requestXml + "| Response XML :" + XmlDoc.OuterXml + DateTime.Now, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Audit.Add(EventType.MikiBooking, Severity.High, 0, "Booking Response from Miki. Response:" + resp + " | Request XML:" + requestXml + DateTime.Now, "");
            // Process the Response.
            BookingResponse searchRes = new BookingResponse();
            try
            {
                if (XmlDoc != null && XmlDoc.ChildNodes != null && XmlDoc.ChildNodes.Count > 0)
                {
                    searchRes = ReadResponseBooking(XmlDoc, ref itinerary);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.MikiBooking, Severity.High, 0, "Exception returned from Miki.GetBooking Error Message:" + ex.Message + " | Request XML:" + requestXml + "| Response XML :" + XmlDoc.OuterXml + DateTime.Now, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            return searchRes;
        }

        /// <summary>
        /// GenerateBookingRequest
        /// </summary>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>string</returns>
        /// <remarks>First need to genarate xml after converting string object</remarks>
        private string GenerateBookingRequest(HotelItinerary itinerary)
        {
            /* NEW VERSION 7.0 */
            StringBuilder strWriter = new StringBuilder();
            XmlWriter xmlString = XmlTextWriter.Create(strWriter);
            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

            xmlString.WriteStartElement("hotelBookingRequest");
            xmlString.WriteAttributeString("versionNumber", "7.0");

            xmlString.WriteStartElement("requestAuditInfo");
            xmlString.WriteElementString("agentCode", agentCode);
            xmlString.WriteElementString("userName", userName);
            xmlString.WriteElementString("requestPassword", requestPassword);
            xmlString.WriteElementString("requestID", requestId);
            xmlString.WriteElementString("requestDateTime", requestTime);
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("booking");
            xmlString.WriteAttributeString("currencyCode", "USD");
            xmlString.WriteAttributeString("paxNationality", itinerary.PassengerNationality);

            xmlString.WriteStartElement("items");

            Dictionary<string, string> getBookingItem = new Dictionary<string, string>();
            string bid = string.Empty;

            xmlString.WriteStartElement("item");
            xmlString.WriteAttributeString("itemNumber", "1");

            xmlString.WriteElementString("immediateConfirmationRequired", "true");

            xmlString.WriteElementString("productCode", itinerary.HotelCode);

            xmlString.WriteStartElement("leadPaxName");
            if (itinerary.HotelPassenger.Title.Length > 0)
            {
                xmlString.WriteElementString("title", itinerary.HotelPassenger.Title.Replace(".", ""));
            }
            xmlString.WriteElementString("firstName", itinerary.HotelPassenger.Firstname.ToString());
            xmlString.WriteElementString("lastName", itinerary.HotelPassenger.Lastname.ToString());
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("hotel");

            xmlString.WriteStartElement("stayPeriod");
            xmlString.WriteElementString("checkinDate", itinerary.StartDate.ToString("yyyy-MM-dd"));
            xmlString.WriteElementString("numberOfNights", Convert.ToString(itinerary.EndDate.Subtract(itinerary.StartDate).Days));
            xmlString.WriteEndElement();

            xmlString.WriteStartElement("rooms");
            Dictionary<string, List<int>> roomsCount = new Dictionary<string, List<int>>();

            for (int c = 0; c < itinerary.Roomtype.Length; c++)
            {
                string subProductCode = string.Empty;
                subProductCode = itinerary.Roomtype[c].RoomTypeCode.Split('|')[0]; //GetMikiRoomCode(itinerary.Roomtype[c].RoomName);

                xmlString.WriteStartElement("room");
                xmlString.WriteAttributeString("roomTypeCode", subProductCode);
                xmlString.WriteAttributeString("roomNo", (c + 1).ToString());

                xmlString.WriteElementString("rateIdentifier", itinerary.Roomtype[c].RatePlanCode);

                xmlString.WriteElementString("roomTotalPrice", itinerary.Roomtype[c].Price.SupplierPrice.ToString());

                // string roomDesc = string.Empty;
                // Dictionary<string, int> occupancy = new Dictionary<string, int>();

                //GetRoomDesc(subProductCode, ref roomDesc, ref occupancy);
                // int namesReq = occupancy["NamesReq"];

                int adults = itinerary.Roomtype[c].AdultCount;
                int childs = itinerary.Roomtype[c].ChildCount;
                xmlString.WriteStartElement("guests");
                for (int r = 0; r < (adults + childs); r++)
                {
                    if (itinerary.Roomtype[c].PassenegerInfo[r].PaxType == HotelPaxType.Adult)
                    {
                        xmlString.WriteStartElement("guest");
                        xmlString.WriteElementString("type", "ADT");
                        xmlString.WriteStartElement("paxName");
                        xmlString.WriteElementString("title", itinerary.Roomtype[c].PassenegerInfo[r].Title.Replace(".", ""));
                        xmlString.WriteElementString("firstName", itinerary.Roomtype[c].PassenegerInfo[r].Firstname);
                        xmlString.WriteElementString("lastName", itinerary.Roomtype[c].PassenegerInfo[r].Lastname);

                        xmlString.WriteEndElement();

                        xmlString.WriteEndElement();
                    }
                    else if (itinerary.Roomtype[c].PassenegerInfo[r].PaxType == HotelPaxType.Child)
                    {
                        xmlString.WriteStartElement("guest");
                        xmlString.WriteElementString("type", "CHD");
                        xmlString.WriteElementString("age", itinerary.Roomtype[c].PassenegerInfo[r].Age.ToString());
                        xmlString.WriteStartElement("paxName");
                        xmlString.WriteElementString("title", itinerary.Roomtype[c].PassenegerInfo[r].Title.Replace(".", ""));
                        xmlString.WriteElementString("firstName", itinerary.Roomtype[c].PassenegerInfo[r].Firstname);
                        xmlString.WriteElementString("lastName", itinerary.Roomtype[c].PassenegerInfo[r].Lastname);

                        xmlString.WriteEndElement();//paxName

                        xmlString.WriteEndElement(); //guest
                    }

                }
                xmlString.WriteEndElement();//Guests


                xmlString.WriteEndElement();//room
            }
            xmlString.WriteEndElement();//rooms
            xmlString.WriteEndElement();//hotel

            xmlString.WriteEndElement();
            xmlString.WriteEndElement();
            xmlString.WriteEndElement();


            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// ReadResponseBooking
        /// </summary>
        /// <param name="xmlDoc">Booking response xml</param>
        /// <param name="itinerary">HotelItinerary object(what we are able book corresponding room and  hotel details)</param>
        /// <returns>BookingResponse</returns>
        /// <remarks>Here Reading booking response object
        /// here only we need to get confirmationNo and BookingId
        /// </remarks>
        private BookingResponse ReadResponseBooking(XmlDocument xmlDoc, ref HotelItinerary itinerary)
        {
            BookingResponse hotelBookRes = new BookingResponse();

            XmlNode tempNode;
            //TextReader stringRead = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_MikiBookingResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("hotelBookingResponse/errors/error/description");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                string solution = string.Empty;
                XmlNode sol = xmlDoc.SelectSingleNode("hotelBookingResponse/errors/error/Solution");
                if (sol != null)
                {
                    solution = sol.InnerText.Trim();
                }
                
                Audit.Add(EventType.MikiBooking, Severity.High, 0, "Error Returned from Miki. Error Message:" + ErrorInfo.InnerText + " | Solution: "+solution+" | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");

                hotelBookRes = new BookingResponse(BookingResponseStatus.Failed, ErrorInfo.InnerText+", "+solution, string.Empty, ProductType.Hotel, "");
                //(BookingResponseStatus.Failed, ErrorInfo.InnerText, string.Empty);
            }
            else
            {
                tempNode = xmlDoc.SelectSingleNode("hotelBookingResponse/booking");
                if (tempNode != null)
                {
                    itinerary.BookingRefNo = tempNode.Attributes["bookingReference"].Value.Trim();

                }
                XmlNodeList bookingItems = xmlDoc.SelectNodes("/hotelBookingResponse/booking/items/item");
                itinerary.ConfirmationNo = string.Empty;
                foreach (XmlNode bookingItem in bookingItems)
                {

                    // Gets the Provisional Id from Miki.
                   
                    if (tempNode != null)
                    {
                        if (itinerary.ConfirmationNo.Length > 0)
                        {
                            itinerary.ConfirmationNo += "|" + bookingItem.Attributes["tourReference"].Value.Trim();
                        }
                        else
                        {
                            itinerary.ConfirmationNo += bookingItem.Attributes["tourReference"].Value.Trim();
                        }
                        itinerary.PaymentGuaranteedBy = " Booked and payable by  Miki Travel </br> Only Payment For Extras To Be Collected From The Client.";
                    }
                }
                if (itinerary.ConfirmationNo != null || itinerary.BookingRefNo != null)
                {
                    Audit.Add(EventType.MikiBooking, Severity.High, 0, "Booking successfully created from Miki.|" + "Confirmation No:" + itinerary.ConfirmationNo + "|" + "Booking Reference No.:" + itinerary.BookingRefNo + " | " + DateTime.Now, "");
                }
                hotelBookRes = new BookingResponse(BookingResponseStatus.Successful, "", "", ProductType.Hotel, itinerary.ConfirmationNo);
                itinerary.Status = HotelBookingStatus.Confirmed;
            }
            return hotelBookRes;
        }
        #endregion

        #region Cancellation Request & Response MIKI V 7.0
        /// <summary>
        /// Used to Cancel a Hotel using confirmationNumber.
        /// </summary>
        /// <param name="confirmationNo">confirmationNo</param>
        /// <returns>Dictionary</returns>
        public Dictionary<string, string> CancelBooking(string confirmationNo)
        {
            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();

            string request = GenerateCancelBookingRequest(confirmationNo);
           
            TextReader stringRead = new StringReader(request);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(stringRead);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_MikiCancellationRequest.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            //Audit.Add(EventType.MikiBooking, Severity.High, 0, "Sent Cancellation Request:" + " | Request XML:" + request + "| "+ DateTime.Now, "");
            //string resp = string.Empty;
            XmlDocument xmlResp = new XmlDocument();

            try
            {
                xmlResp = SendGetRequest(request);
                if (xmlResp != null && xmlResp.ChildNodes != null && xmlResp.ChildNodes.Count > 0)
                {
                    cancelInfo = ReadCancelBookingResponse(xmlResp);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.MikiCancel, Severity.High, 0, "Exception returned from MikiApi.CancelBooking Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + request + "|response XML" + xmlDoc.OuterXml, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            finally
            {
                Audit.Add(EventType.MikiCancel, Severity.High, 0, "MikiApi.CancelBooking : request XML" + request + "|response XML" + xmlDoc.OuterXml + " " + DateTime.Now, "");
            }
            return cancelInfo;
        }
        /// <summary>
        /// This private function is used for Generates the Hotel Cancel request.
        /// </summary>
        /// <param name="confirmationNo">confirmationNo</param>
        /// <returns>string</returns>
        private string GenerateCancelBookingRequest(string confirmationNo)
        {
            string tourRef = confirmationNo.Trim();

            StringBuilder strWriter = new StringBuilder();

            XmlWriter xmlString = XmlTextWriter.Create(strWriter);

            xmlString.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-16\"");

            xmlString.WriteStartElement("cancellationRequest");
            xmlString.WriteAttributeString("versionNumber", "7.0");

            xmlString.WriteStartElement("requestAuditInfo");
            xmlString.WriteElementString("agentCode", agentCode);
            xmlString.WriteElementString("requestPassword", requestPassword);
            xmlString.WriteElementString("requestID", requestId);
            xmlString.WriteElementString("requestDateTime", requestTime);
            xmlString.WriteEndElement();

            xmlString.WriteElementString("tourReference", tourRef);

            xmlString.WriteEndElement();
            xmlString.Close();
            return strWriter.ToString();
        }
        /// <summary>
        /// ReadHotelCancellationResponse
        /// </summary>
        /// <param name="xmlDoc">Booking response xml</param>
        /// <returns>Dictionary</returns>
        private Dictionary<string, string> ReadCancelBookingResponse(XmlDocument xmlDoc)
        {

            Dictionary<string, string> cancelInfo = new Dictionary<string, string>();
            XmlNode tempNode;
            //TextReader strWriter = new StringReader(response);
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(strWriter);
            try
            {
                string filePath = XmlPath + sessionId + "_" + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_MikiCancellationResponse.xml";
                xmlDoc.Save(filePath);
            }
            catch { }
            XmlNode ErrorInfo = xmlDoc.SelectSingleNode("cancellationResponse/errors/error/description");
            if (ErrorInfo != null && ErrorInfo.InnerText.Length > 0)
            {
                string solution = string.Empty;
                XmlNode sol = xmlDoc.SelectSingleNode("cancellationResponse/errors/error/Solution");
                if (sol != null)
                {
                    solution = sol.InnerText.Trim();
                }
                Audit.Add(EventType.MikiCancel, Severity.High, 0, "Error Returned from Miki. Error Message:" + ErrorInfo.InnerText + "|Solution: " + solution + " | " + DateTime.Now + "| Response XML" + xmlDoc.OuterXml, "");
            }
            else
            {
                tempNode = xmlDoc.SelectSingleNode("cancellationResponse/cancelledTours/cancelledTour/status");
                if (tempNode != null && tempNode.InnerText == "Cancelled")
                {
                    cancelInfo.Add("Status", "Cancelled");
                }
                tempNode = xmlDoc.SelectSingleNode("cancellationResponse/cancelledTours/cancelledTour/cancellationReference");
                if (tempNode != null)
                {
                    cancelInfo.Add("ID", tempNode.InnerText.Trim()); //Cancelled Reference Number
                }

                tempNode = xmlDoc.SelectSingleNode("cancellationResponse/cancelledTours/cancelledTour/currencyCode");
                if (tempNode != null)
                {
                    cancelInfo.Add("Currency", tempNode.InnerText.Trim());
                }

                tempNode = xmlDoc.SelectSingleNode("cancellationResponse/cancelledTours/cancelledTour/tourTotalCancellationCharge");
                if (tempNode != null)
                {
                    cancelInfo.Add("Amount", tempNode.InnerText.Trim());

                }

            }

            return cancelInfo;
        } 
        #endregion

        #region Pax Wise Room Validation 
        private bool IsValidResultSet(HotelRequest req, HotelRoomsDetails[] hotelRoomsDetails)
        {
            bool isValid = false;
            for (int i = 1; i <= req.RoomGuest.Length; i++)
            {
                isValid = false;
                foreach (HotelRoomsDetails hd in hotelRoomsDetails)
                {
                    if (hd.SequenceNo.Contains(i.ToString()))
                    {
                        isValid = true;
                        break;
                    }
                }
                if (!isValid)
                    return isValid;
            }
            return isValid;
        }


        #endregion

        #region IDisposable Support
        /// <summary>
        /// disposedValue
        /// </summary>
        private bool disposedValue = false; // To detect redundant calls
        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing">Dispose</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        /// <summary>
        /// ~MikiApi
        /// </summary>
        ~MikiApi()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        /// <summary>
        /// Dispose
        /// </summary>
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

        #region DailyPassword Mechanism For Miki V 5.2
        //private KeyValuePair<string, string> GetDailyPassword()
        //{
        //    KeyValuePair<string, string> password = new KeyValuePair<string, string>();
        //    SqlConnection connection = DBGateway.GetConnection();
        //    SqlParameter[] paramlist = new SqlParameter[1];
        //    paramlist[0] = new SqlParameter("@date", DateTime.Now.Date);
        //    try
        //    {
        //        //For Live Env
        //        SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetDailyPassword, paramlist, connection);

        //        //For Test Environement
        //       // SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_GetDailyPasswordForMiki", paramlist, connection);
        //        if (dataReader.Read())
        //        {
        //            DateTime passdate = Convert.ToDateTime(dataReader["date"]);
        //            password = new KeyValuePair<string, string>(passdate.ToString("dd/MM/yyyy"), Convert.ToString(dataReader["password"]));
        //        }
        //        dataReader.Close();
        //        connection.Close();
        //    }
        //    catch (SqlException ex)
        //    {
        //        connection.Close();
        //        Audit.Add(EventType.MikiAvailSearch, Severity.High, 0, "Exception returned from Miki.password for the date:" + DateTime.Now.Date + " not found." + "exception:" + ex.Message + DateTime.Now, "");
        //    }
        //    return password;
        //} 
        #endregion
        #region Sequence No For MIKI V 5.2
        //private string GetSequenceNo(Dictionary<string, int> dictionary, HotelRequest request)
        //{
        //    string seqNo = string.Empty;
        //    int adults = 0;
        //    int childs = 0;
        //    List<int> age = new List<int>();
        //    for (int i = 0; i < request.RoomGuest.Length; i++)
        //    {
        //        adults = request.RoomGuest[i].noOfAdults;
        //        childs = 0;

        //        for (int c = 0; c < request.RoomGuest[i].noOfChild; c++)
        //        {
        //            if (request.RoomGuest[i].childAge[c] > 12)
        //            {
        //                adults = adults + 1;
        //            }
        //            else if (request.RoomGuest[i].childAge[c] <= 12)
        //            {
        //                childs = childs + 1;
        //                age.Add(request.RoomGuest[i].childAge[c]);
        //            }
        //        }
        //        if (adults <= dictionary["MaxAdult"])
        //        {
        //            if (childs <= dictionary["MaxChild"] || childs <= dictionary["MaxInfant"])
        //            {
        //                if (seqNo.Length > 0) seqNo += "|";
        //                seqNo += (i + 1);
        //            }
        //        }
        //        else if (adults + childs <= dictionary["MaxPax"])
        //        {
        //            if (seqNo.Length > 0) seqNo += "|";
        //            seqNo += (i + 1);
        //        }
        //    }
        //    return seqNo;
        //} 
        #endregion



    }
}
