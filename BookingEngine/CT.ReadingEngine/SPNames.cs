namespace CT.ReadingEngine
{
    internal static class SPNames
    {
        public const string AddOfflineBooking = "usp_AddOfflineBooking";
        public const string GetDistinctOfflineInvoiceKeys = "usp_GetDistinctOfflineInvoiceKeys";
        public const string GetOfflineBookings = "usp_GetOfflineBookings";
        public const string GetOfflineBooking = "usp_GetOfflineBooking";
        public const string GetPageCountForOfflineBooking = "usp_GetPageCountForOfflineBooking";       
        public const string SetOffBookingTicketStatus = "usp_SetOffBookingTicketStatus";
        public const string GetOfflineBookingsForAdmin = "usp_GetOfflineBookingsForAdmin";
        public const string GetQueueCountForOfflineBookings = "usp_GetQueueCountForOfflineBookings";
        public const string IsTicketNoPresentInOfflineBooking = "usp_IsTicketNoPresentInOfflineBooking";
    }
}
