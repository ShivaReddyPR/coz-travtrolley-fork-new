using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using CT.BookingEngine;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Diagnostics;
using CT.Core;
using System.Text.RegularExpressions;


namespace CT.ReadingEngine
{
    /// <summary>
    /// New Enum introduced for document type code(used for Offline purposes)
    /// </summary>
    public enum DocumentTypeCode
    {
        Ticket = 1,
        Refund = 2,
        Void = 3
    }


    public class OfflineBooking
    {
        #region Private Fields
        /// <summary>
        /// created date of invoice 
        /// </summary>
        private DateTime invoiceDate;
        /// <summary>
        /// Primary key for the class OfflineBooking
        /// </summary>
        private int offlineBookingId;
        /// <summary>
        /// document number as in invoice
        /// </summary>
        private string invoiceReference;
        /// <summary>
        /// agency id for which booking is created
        /// </summary>
        private int agencyId;
        /// <summary>
        /// Passenger name
        /// </summary>
        private string paxFullName;
        /// <summary>
        /// ticket number field
        /// </summary>
        private string ticketNo;
        /// <summary>
        /// itinerary (Del-Bom-Lon) type string
        /// </summary>
        private string itineararyString;
        /// <summary>
        /// price id field
        /// </summary>
        private int priceId;
        /// <summary>
        /// Price for the booking
        /// </summary>
        private PriceAccounts price;
        /// <summary>
        /// Validating Carrer field
        /// </summary>
        private string validatingCarrier;
        /// <summary>
        /// Booking is domestic or international field
        /// </summary>
        private bool isDomestic;
        /// <summary>
        /// payment to field(Either BSP or Air in case of LCC)
        /// </summary>
        private string paymentTo;
        /// <summary>
        /// Passenger Type field
        /// </summary>
        private PassengerType paxType;
        /// <summary>
        /// Flight detail field
        /// </summary>
        private string flightDetails;
        /// <summary>
        /// Fare basis Field
        /// </summary>
        private string fareBasis;
        /// <summary>
        /// Booking is for lcc or non LCC
        /// </summary>
        private bool isLCC;
        /// <summary>
        /// invoice due date field
        /// </summary>
        private DateTime invoiceDueDate;
        /// <summary>
        /// pnr for the booking
        /// </summary>
        private string pnr;

        /// <summary>
        /// documentType code(whether it is refund/void/ticket entry)
        /// </summary>
        private DocumentTypeCode documentTypeCode;
        /// <summary>
        /// Cancellation chage given to airline(in case of refund/void only)
        /// </summary>
        private decimal rafAirline;
        /// <summary>
        /// Cacellation charge which we take from agent(in case of refund/void only)
        /// </summary>
        private decimal rafClient;
        /// <summary>
        /// Status of the ticket
        /// </summary>
        private string status;
        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        //private int lastModifiedBy;
       
        #endregion

        #region Public Properties
        /// <summary>
        /// Get or Sets Invoice Date field
        /// </summary>
        public DateTime InvoiceDate
        {
            get
            {
                return this.invoiceDate;
            }
            set
            {
                this.invoiceDate = value;
            }
        }
        /// <summary>
        /// Gets or Sets Offline Booking Id Field
        /// </summary>
        public int OfflineBookingId
        {
            get
            {
                return this.offlineBookingId;
            }
            set
            {
                this.offlineBookingId = value;
            }
        }
        /// <summary>
        /// Gets or Sets InvoiceReference field
        /// </summary>
        public string InvoiceReference
        {
            get
            {
                return this.invoiceReference;
            }
            set
            {
                this.invoiceReference = value;
            }
        }
        /// <summary>
        /// Gets or Sets AgencyId field
        /// </summary>
        public int AgencyId
        {
            get
            {
                return this.agencyId;
            }
            set
            {
                this.agencyId = value;
            }
        }
        /// <summary>
        /// Gets or Sets PaxFullName field
        /// </summary>
        public string PaxFullName
        {
            get
            {
                return this.paxFullName;
            }
            set
            {
                this.paxFullName = value;
            }
        }
        /// <summary>
        /// Gets or Sets TicketNo field
        /// </summary>
        public string TicketNo
        {
            get
            {
                return this.ticketNo;
            }
            set
            {
                this.ticketNo = value;
            }
        }
        /// <summary>
        /// Gets or Sets ItineraryString field
        /// </summary>
        public string ItineararyString
        {
            get
            {
                return this.itineararyString;
            }
            set
            {
                this.itineararyString = value;
            }
        }
        /// <summary>
        /// Gets or Sets PriceId field
        /// </summary>
        public int PriceId
        {
            get
            {
                return this.priceId;
            }
            set
            {
                this.priceId = value;
            }
        }
        /// <summary>
        /// Gets or Sets validating carrier field
        /// </summary>
        public string ValidatingCarrier
        {
            get
            {
                return this.validatingCarrier;
            }
            set
            {
                this.validatingCarrier = value;
            }
        }
        /// <summary>
        /// Gets or Sets isDomestic field
        /// </summary>
        public bool IsDomestic
        {
            get
            {
                return this.isDomestic;
            }
            set
            {
                this.isDomestic = value;
            }
        }
        /// <summary>
        /// Gets or Sets payment to field
        /// </summary>
        public string PaymentTo
        {
            get
            {
                return this.paymentTo;
            }
            set
            {
                this.paymentTo = value;
            }
        }
        /// <summary>
        /// Gets or Sets Pax type field
        /// </summary>
        public PassengerType PaxType
        {
            get
            {
                return this.paxType;
            }
            set
            {
                this.paxType = value;
            }
        }
        /// <summary>
        /// Gets or Sets flight detail field
        /// </summary>
        public string FlightDetails
        {
            get
            {
                return this.flightDetails;
            }
            set
            {
                this.flightDetails = value;
            }
        }
        /// <summary>
        /// Gets or Sets fare basis field
        /// </summary>
        public string FareBasis
        {
            get
            {
                return this.fareBasis;
            }
            set
            {
                this.fareBasis = value;
            }
        }
        /// <summary>
        /// Gets or Sets IsLCC field
        /// </summary>
        public bool IsLCC
        {
            get
            {
                return this.isLCC;
            }
            set
            {
                this.isLCC = value;
            }
        }
        /// <summary>
        /// Gets or Sets InvoiceDueDate field
        /// </summary>
        public DateTime InvoiceDueDate
        {
            get
            {
                return this.invoiceDueDate;
            }
            set
            {
                this.invoiceDueDate = value;
            }
        }
        /// <summary>
        /// Gets or Sets Price field
        /// </summary>
        public PriceAccounts Price
        {
            get
            {
                return this.price;
            }
            set
            {
                this.price = value;
            }
 
        }
        /// <summary>
        /// Gets or Sets Pnr field
        /// </summary>
        public string PNR
        {
            get 
            {
                return this.pnr;
            }
            set 
            {
                this.pnr = value;
            }
        }
        /// <summary>
        /// Gets or Sets the documenttypecode field
        /// </summary>
        public DocumentTypeCode DocumentTypeCode
        {
            get
            {
                return this.documentTypeCode;
            }
            set
            {
                this.documentTypeCode = value;
            }
        }
        /// <summary>
        /// Gets or Sets the raf airline field
        /// </summary>
        public decimal RAFAirline
        {
            get
            {
                return this.rafAirline;
            }
            set
            {
                this.rafAirline = value;
            }
        }
        /// <summary>
        /// Gets or sets raf client field
        /// </summary>
        public decimal RAFClient
        {
            get
            {
                return this.rafClient;
            }
            set
            {
                this.rafClient = value;
            }
        }
        /// <summary>
        /// Gets or sets the status of the ticket
        /// </summary>
        public string Status 
        {
          get
          {
              return status;
          }
          set
          {
              this.status = value;
           
          }
        }
        
        
        #endregion

        public static string ErrorString=string.Empty;

        #region Public Methods
        /// <summary>
        /// Saves the offline booking object
        /// </summary>
        public void Save()
        {
            //Saves all the List of bookings in DataBase
            //If it find any duplicate row then it will ignore that row and continue the process of saving
            //If SameRow methood return false we need to update the content of that row
            ////Trace.TraceInformation("OfflineBooking.Save entered : ");
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[15];
            paramList[0] = new SqlParameter("@offlineBookingId", offlineBookingId);
            paramList[1] = new SqlParameter("@invoiceReference", invoiceReference);
            paramList[2] = new SqlParameter("@agencyId", agencyId);
            paramList[3] = new SqlParameter("@paxFullName", paxFullName);
            paramList[4] = new SqlParameter("@ticketNo", ticketNo);
            paramList[5] = new SqlParameter("@itinerary", itineararyString);
            paramList[6] = new SqlParameter("@priceId", priceId);
            paramList[7] = new SqlParameter("@validatingCarrier", validatingCarrier);
            paramList[8] = new SqlParameter("@isDomestic", isDomestic);
            paramList[9] = new SqlParameter("@paymentTo", paymentTo);
            paramList[10] = new SqlParameter("@paxType", FlightPassenger.GetPTC(paxType));
            paramList[11] = new SqlParameter("@flightDetails", flightDetails);
            paramList[12] = new SqlParameter("@fareBasis", fareBasis);
            paramList[13] = new SqlParameter("@isLCC", isLCC);
            paramList[14] = new SqlParameter("@pnr", pnr);
            paramList[0].Direction = ParameterDirection.Output;
            rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddOfflineBooking, paramList);
            offlineBookingId = Convert.ToInt32(paramList[0].Value);
            //Trace.TraceInformation("OfflineBooking.Save exiting : rowsAffected = " + rowsAffected);
        }
        /// <summary>
        /// Gets Offline Booking for the given agency id
        /// Gives all offline booking for 0 agency id
        /// </summary>
        /// <param name="agencyId">agencyId</param>
        /// <returns>Dictionary Contain keys and the corresponding offline Booking of that key 
        /// e.g(Dictionary("IW12345",List<OfflineBooking> listforGivenkey)</returns>
        

        public static List<KeyValuePair<string,OfflineBooking>> GetBookingsForAgency(int agencyId, int pageNo, int recordsPerPage, int totalRecords, string agentFilter)
        {
            string key=string.Empty;
            //If agencyId 0 return all offline bookings(for admin)
            //Otherwise return all offline booking of that agent
            //Trace.TraceInformation("OfflineBooking.Load entered");
          
            List<KeyValuePair<string, OfflineBooking>> offlineBookingDictionary = new List<KeyValuePair<string,OfflineBooking>>();
            if (agencyId < 0)
            {
                throw new ArgumentException("agencyId must have a positive or zero value");
            }
            SqlParameter[] paramList = new SqlParameter[3];
            int endrow = 0;
            int startrow = ((recordsPerPage * (pageNo - 1)) + 1);
            if ((startrow + recordsPerPage) - 1 < totalRecords)
            {
                endrow = (startrow + recordsPerPage) - 1;
            }
            else
            {
                endrow = totalRecords;
            }   
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@startrow", startrow);
            paramList[2] = new SqlParameter("@endrow", endrow);
            DataSet dataSet = DBGateway.FillSP(SPNames.GetOfflineBookings, paramList);
            if (dataSet.Tables.Count > 0)
            {
                
                    List<OfflineBooking> offlineBookings = new List<OfflineBooking>();
                    foreach (DataRow dr in dataSet.Tables[0].Rows)
                    {
                        OfflineBooking tempObject = new OfflineBooking();
                        tempObject.agencyId = Convert.ToInt32(dr["agencyId"]);
                        tempObject.FareBasis = dr["fareBasis"].ToString();
                        tempObject.flightDetails = dr["flightDetails"].ToString();
                        tempObject.invoiceDate = Convert.ToDateTime(dr["createdOn"]);
                        if (dr["invoiceDueDate"] != DBNull.Value)
                        {
                            tempObject.invoiceDueDate = Convert.ToDateTime(dr["invoiceDueDate"]);
                        }
                        tempObject.invoiceReference = dr["invoiceReference"].ToString();
                        tempObject.isDomestic = Convert.ToBoolean(dr["isDomestic"]);
                        tempObject.isLCC = Convert.ToBoolean(dr["isLCC"]);
                        tempObject.itineararyString = dr["itinerary"].ToString();
                        tempObject.paxFullName = dr["paxFullName"].ToString();
                        tempObject.paxType = FlightPassenger.GetPassengerType(dr["paxType"].ToString());
                        tempObject.paymentTo = dr["paymentTo"].ToString();
                        tempObject.priceId = Convert.ToInt32(dr["priceId"]);
                        tempObject.Price = new PriceAccounts();
                        tempObject.Price.Load(tempObject.priceId);
                        tempObject.ticketNo = dr["ticketNo"].ToString();
                        tempObject.validatingCarrier = dr["validatingCarrier"].ToString();
                        tempObject.OfflineBookingId =Convert.ToInt32(dr["offlineBookingId"]);
                        tempObject.status = dr["status"].ToString();
                       
                        key = dr["invoiceReferenceKey"].ToString();
                        if (dr["pnr"] != DBNull.Value)
                        {
                            tempObject.pnr = dr["pnr"].ToString();
                        }
                        else
                        {
                            tempObject.pnr = "";
                        }
                     
                        KeyValuePair<string, OfflineBooking> offlineBooking = new KeyValuePair<string,OfflineBooking>(key,tempObject);
                        
                        offlineBookingDictionary.Add(offlineBooking);
                    }
               
            }
            dataSet.Dispose();
            //Trace.TraceInformation("OfflineBooking.Load exiting : ");
            return offlineBookingDictionary;
        }

        /// <summary>
        /// Gets Offline Booking        
        /// </summary>
        /// <param name="agencyId">agencyId</param>
        /// <returns>Dictionary Contain keys and the corresponding offline Booking of that key 
        /// e.g(Dictionary("IW12345",List<OfflineBooking> listforGivenkey)</returns>
        public static Dictionary<string, List<OfflineBooking>> GetBookings(string whereString, int pageNo, int recordsPerPage, int totalRecords)
        {
            //If agencyId 0 return all offline bookings(for admin)
            //Otherwise return all offline booking of that agent
            //Trace.TraceInformation("OfflineBooking.Load entered");
            List<string> offlineInvoiceKeys = GetDistinctOfflineInvoiceKeys(whereString, pageNo, recordsPerPage, totalRecords);
            Dictionary<string, List<OfflineBooking>> offlineBookingDictionary = new Dictionary<string, List<OfflineBooking>>();          
            SqlParameter[] paramList = new SqlParameter[0];
            DataSet dataSet = DBGateway.FillSP(SPNames.GetOfflineBookingsForAdmin, paramList);
            if (dataSet.Tables.Count > 0)
            {
                DataTable table = dataSet.Tables[0];
                foreach (string key in offlineInvoiceKeys)
                {
                    List<OfflineBooking> offlineBookings = new List<OfflineBooking>();
                    DataRow[] offlineBookingsRows = table.Select("invoiceReferencekey='" + key + "'");
                    foreach (DataRow dr in offlineBookingsRows)
                    {
                        OfflineBooking tempObject = new OfflineBooking();
                        tempObject.offlineBookingId = Convert.ToInt32(dr["offlineBookingId"]);
                        tempObject.agencyId = Convert.ToInt32(dr["agencyId"]);
                        tempObject.FareBasis = dr["fareBasis"].ToString();
                        tempObject.flightDetails = dr["flightDetails"].ToString();
                        tempObject.invoiceDate = Convert.ToDateTime(dr["createdOn"]);
                        tempObject.invoiceDueDate = Convert.ToDateTime(dr["invoiceDueDate"]);
                        tempObject.invoiceReference = dr["invoiceReference"].ToString();
                        tempObject.isDomestic = Convert.ToBoolean(dr["isDomestic"]);
                        tempObject.isLCC = Convert.ToBoolean(dr["isLCC"]);
                        tempObject.itineararyString = dr["itinerary"].ToString();
                        tempObject.paxFullName = dr["paxFullName"].ToString();
                        tempObject.paxType = FlightPassenger.GetPassengerType(dr["paxType"].ToString());
                        tempObject.paymentTo = dr["paymentTo"].ToString();
                        tempObject.priceId = Convert.ToInt32(dr["priceId"]);
                        tempObject.Price = new PriceAccounts();
                        tempObject.Price.Load(tempObject.priceId);
                        tempObject.ticketNo = dr["ticketNo"].ToString();
                        tempObject.validatingCarrier = dr["validatingCarrier"].ToString();
                        if (dr["pnr"] != DBNull.Value)
                        {
                            tempObject.pnr = dr["pnr"].ToString();
                        }
                        else
                        {
                            tempObject.pnr = "";
                        }
                        offlineBookings.Add(tempObject);
                    }
                    offlineBookingDictionary.Add(key, offlineBookings);
                }
            }
            dataSet.Dispose();
            //Trace.TraceInformation("OfflineBooking.Load exiting : ");
            return offlineBookingDictionary;
        }


        /// <summary>
        /// Loads the offline booking object from database
        /// </summary>
        /// <param name="offlineBookingId">offlineBookingId</param>
        public void Load(int offlineBookingId)
        {
            //Trace.TraceInformation("OfflineBooking.Load entered: OfflineBookingId" + offlineBookingId);
            SqlParameter[] paramList = new SqlParameter[1];
            SqlConnection connection = DBGateway.GetConnection();
            paramList[0] = new SqlParameter("@offlineBookingId", offlineBookingId);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetOfflineBooking, paramList, connection);
            if (dataReader.Read())
            {
                agencyId = Convert.ToInt32(dataReader["agencyId"]);
                FareBasis = dataReader["fareBasis"].ToString();
                flightDetails = dataReader["flightDetails"].ToString();
                invoiceDate = Convert.ToDateTime(dataReader["createdOn"]);
                if (dataReader["invoiceDueDate"] != DBNull.Value)
                {
                    invoiceDueDate = Convert.ToDateTime(dataReader["invoiceDueDate"]);
                }
                invoiceReference = dataReader["invoiceReference"].ToString();
                isDomestic = Convert.ToBoolean(dataReader["isDomestic"]);
                isLCC = Convert.ToBoolean(dataReader["isLCC"]);
                itineararyString = dataReader["itinerary"].ToString();
                paxFullName = dataReader["paxFullName"].ToString();
                paxType = FlightPassenger.GetPassengerType(dataReader["paxType"].ToString());
                paymentTo = dataReader["paymentTo"].ToString();
                priceId = Convert.ToInt32(dataReader["priceId"]);
                Price = new PriceAccounts();
                Price.Load(priceId);
                ticketNo = dataReader["ticketNo"].ToString();
                validatingCarrier = dataReader["validatingCarrier"].ToString();
                status = dataReader["status"].ToString();
                this.offlineBookingId = Convert.ToInt32(dataReader["offlineBookingId"]);
                
                if (dataReader["pnr"] != DBNull.Value)
                {
                    pnr = dataReader["pnr"].ToString();
                }
                else
                {
                    pnr = "";
                }
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("OfflineBooking.Load exited: OfflineBookingId" + offlineBookingId);
        }

        /// <summary>
        /// Loads the offline booking object from database for credit note
        /// </summary>
        /// <param name="offlineBookingId">offlineBookingId</param>
        public void LoadForCreditNote(int offlineBookingId)
        {
            //Trace.TraceInformation("OfflineBooking.Load entered: OfflineBookingId" + offlineBookingId);
            SqlParameter[] paramList = new SqlParameter[1];
            SqlConnection connection = DBGateway.GetConnection();
            paramList[0] = new SqlParameter("@offlineBookingId", offlineBookingId);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetOfflineBooking, paramList, connection);
            if (dataReader.Read())
            {
                agencyId = Convert.ToInt32(dataReader["agencyId"]);
                FareBasis = dataReader["fareBasis"].ToString();
                flightDetails = dataReader["flightDetails"].ToString();
                invoiceDate = Convert.ToDateTime(dataReader["createdOn"]);
                if (dataReader["invoiceDueDate"] != DBNull.Value)
                {
                    invoiceDueDate = Convert.ToDateTime(dataReader["invoiceDueDate"]);
                }
                invoiceReference = dataReader["invoiceReference"].ToString();
                isDomestic = Convert.ToBoolean(dataReader["isDomestic"]);
                isLCC = Convert.ToBoolean(dataReader["isLCC"]);
                itineararyString = dataReader["itinerary"].ToString();
                paxFullName = dataReader["paxFullName"].ToString();
                paxType = FlightPassenger.GetPassengerType(dataReader["paxType"].ToString());
                paymentTo = dataReader["paymentTo"].ToString();
                priceId = Convert.ToInt32(dataReader["priceId"]);
                Price = new PriceAccounts();
                Price.LoadForCreditNote(offlineBookingId,(int)InvoiceItemTypeId.Offline);
                ticketNo = dataReader["ticketNo"].ToString();
                validatingCarrier = dataReader["validatingCarrier"].ToString();
                status = dataReader["status"].ToString();
                this.offlineBookingId = Convert.ToInt32(dataReader["offlineBookingId"]);

                if (dataReader["pnr"] != DBNull.Value)
                {
                    pnr = dataReader["pnr"].ToString();
                }
                else
                {
                    pnr = "";
                }
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("OfflineBooking.Load exited: OfflineBookingId" + offlineBookingId);
        }

        /// <summary>
        /// Loads Dictionary from the excel files
        /// </summary>
        /// <param name="salesDPath">salesD file path</param>
        /// <param name="salesMPath">salesM file path</param>
        /// <returns></returns>
        
        public static Dictionary<string, List<OfflineBooking>> LoadFromExcel(string salesDPath, string salesMPath)
        {
            Dictionary<string, List<OfflineBooking>> retList = new Dictionary<string, List<OfflineBooking>>();
            TextReader readerMasterFile = new StreamReader(salesMPath);
            DataTable masterTable = CSVParser.Parse(readerMasterFile, true);
            TextReader readerDetailFile = new StreamReader(salesDPath);
            DataTable detailTable = CSVParser.Parse(readerDetailFile, true); 
            string masterErrorString=CheckConsistency(masterTable,"Master");
            string detailErrorString=CheckConsistency(detailTable,"Detail");
            if (masterErrorString == string.Empty && detailErrorString == string.Empty)
            {
                List<DataRow> rowList = new List<DataRow>();
                foreach (DataRow dr in masterTable.Rows)
                {
                    if (IsIncorrectAccountCode(dr["AccountCode"].ToString()) || dr["DUEDATEOFPAYMENT"] == null || dr["DUEDATEOFPAYMENT"].ToString() == "")
                    {
                        rowList.Add(dr);
                    }
                }
                foreach (DataRow row in rowList)
                {
                    masterTable.Rows.Remove(row);
                } 
                foreach (DataRow dr in masterTable.Rows)
                {
                    string invoiceReferenceKey = dr["InvoiceNumber"].ToString();
                    string invoiceReferenceNumber = invoiceReferenceKey.Substring(2, invoiceReferenceKey.Length - 2);
                    string flightType = invoiceReferenceKey.Substring(0, 1);
                    string docTypeCode=invoiceReferenceKey.Substring(0, 2);
                    string typeStringToSearch = string.Empty;

                    if (flightType == "D")
                    {
                        typeStringToSearch = "DOM";
                    }
                    else
                    {
                        typeStringToSearch = "INTL";
                    }
                    DataRow[] rowArray = detailTable.Select("[Bill#]= '" + invoiceReferenceKey + "'");
                    List<OfflineBooking> tempListObject = new List<OfflineBooking>();
                    
                    foreach (DataRow row in rowArray)
                    {                      
                        OfflineBooking tempObject = new OfflineBooking();
                        tempObject.price = new PriceAccounts();
                        tempObject.invoiceDate = DateTime.ParseExact(row["DATE"].ToString(), "yyyyMMdd", null);
                        if (dr["DUEDATEOFPAYMENT"] != null && dr["DUEDATEOFPAYMENT"].ToString() != "")
                        {
                            tempObject.invoiceDueDate = DateTime.ParseExact(dr["DUEDATEOFPAYMENT"].ToString(), "yyyyMMdd", null);
                        }
                        tempObject.invoiceReference = invoiceReferenceNumber;
                        tempObject.paxFullName = row["PAX NAME"].ToString();
                        tempObject.ticketNo = row["TICKET NO"].ToString();
                        tempObject.itineararyString = row["SECTOR"].ToString();
                        tempObject.price.PublishedFare = Convert.ToDecimal(row["FARE"]);
                        tempObject.price.Markup = 0;
                        tempObject.price.NetFare = 0;
                        tempObject.price.OurCommission = 0;
                        tempObject.price.OurPLB = 0;
                        if (Convert.ToDecimal(row["H.CHRG"]) + Convert.ToDecimal(row["PLBAmount"]) > 0)
                        {
                            tempObject.price.TDSPLB = Math.Round(Convert.ToDecimal(row["PLBAmount"]) * Convert.ToDecimal(row["TDS"]) / (Convert.ToDecimal(row["H.CHRG"]) + Convert.ToDecimal(row["PLBAmount"])));
                        }
                        tempObject.price.Tax = Convert.ToDecimal(row["TAX"]);
                        tempObject.price.AgentCommission = Convert.ToDecimal(row["H.CHRG"]);
                        if (Convert.ToDecimal(row["H.CHRG"]) + Convert.ToDecimal(row["PLBAmount"]) > 0)
                        {
                            tempObject.price.TdsCommission = Math.Round(Convert.ToDecimal(row["H.CHRG"]) * Convert.ToDecimal(row["TDS"]) / (Convert.ToDecimal(row["H.CHRG"]) + Convert.ToDecimal(row["PLBAmount"])));
                        }
                        tempObject.price.SeviceTax = Convert.ToDecimal(row["S. TAX"]);
                        tempObject.price.AgentPLB = Convert.ToDecimal(row["PLBAMOUNT"]);
                        tempObject.price.OtherCharges = Convert.ToDecimal(row["OTH. CHRG"]);
                        tempObject.fareBasis = row["FAREBASIS"].ToString();
                       
                        tempObject.flightDetails = row["FLIGHTDETAILS"].ToString();
                                               
                        if (typeStringToSearch == "DOM")
                        {
                            tempObject.isDomestic = true;
                        }
                        else
                        {
                            tempObject.isDomestic = false;
                        }
                        tempObject.paxType = FlightPassenger.GetPassengerType(row["PAXTYPE"].ToString());
                        tempObject.paymentTo = row["PAYMENTTO"].ToString();
                        tempObject.validatingCarrier = row["CARRIER"].ToString();
                        string accountCode = dr["AccountCode"].ToString();
                        tempObject.agencyId = Convert.ToInt32(accountCode.Substring(1, accountCode.Length - 1));
                        //int agencyTypeId = Agency.GetAgencyTypeId(tempObject.agencyId);//TODO ziya
                        int agencyTypeId = 1;
                        if (accountCode[0] != 'Z' && agencyTypeId != (int)Agencytype.Cash && agencyTypeId != (int)Agencytype.Service)
                        {
                            tempObject.isLCC = false;
                        }
                        else
                        {
                            tempObject.isLCC = true;
                        }                        
                        tempObject.pnr = dr["PNR"].ToString();
                        if (row["RAF Airline"].ToString() != "")
                        {
                            tempObject.rafAirline = Convert.ToDecimal(row["RAF Airline"]);
                        }
                        else
                        {
                            tempObject.rafAirline = 0;
                        }
                        if (row["RAF Client"].ToString() != "")
                        {
                            tempObject.rafClient = Convert.ToDecimal(row["RAF Client"]);
                        }
                        else
                        {
                            tempObject.rafClient = 0;
                        }
                        if (docTypeCode == "DS" || docTypeCode == "IS")
                        {
                            tempObject.documentTypeCode = DocumentTypeCode.Ticket;
                        }
                        else if (docTypeCode == "DR" || docTypeCode == "IR")
                        {
                            tempObject.documentTypeCode = DocumentTypeCode.Refund;
                        }
                        else if (docTypeCode == "DV" || docTypeCode == "IV")
                        {
                            tempObject.documentTypeCode = DocumentTypeCode.Void;
                        }
                        tempListObject.Add(tempObject);
                    }
                    retList.Add(invoiceReferenceKey, tempListObject);
                }
            }
            else 
            {
                if (masterErrorString == string.Empty)
                {
                    retList.Add("Error:" + detailErrorString, new List<OfflineBooking>());
                }
                else
                {
                    retList.Add("Error:" + masterErrorString, new List<OfflineBooking>());
                }
            }
            return retList;
        }
        /// <summary>
        /// Method will change the status of the ticket(e.g OK to Voided or Cancelled)
        /// </summary>
        /// <param name="ticketId">OfflineBookingId</param>
        /// <param name="status">Status to be set</param>
        /// <param name="modifiedBy">Modified By</param>
        /// <returns>Rows Affected after seeting the status</returns>
        public int SetTicketStatus(int offBookingId, string status)
        {
            //Trace.TraceInformation("OfflineBooking.SetTicketStatus entered : new status = " + status);
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@offBookingId", offBookingId);
            paramList[1] = new SqlParameter("@status", status);
           // paramList[2] = new SqlParameter("@lastModifiedBy", modifiedBy);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.SetOffBookingTicketStatus, paramList);
            //Trace.TraceInformation("Ticket.SetStatus exiting : rowsAffected = " + rowsAffected);
            return rowsAffected;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Method retun error string if find any column error in excel file
        /// return empty string in case of no error
        /// </summary>
        /// <param name="table">table for chacking columns build by csv</param>
        /// <param name="fileType">it's detail or master file </param>
        /// <returns></returns>
        private static string CheckConsistency(DataTable table, string fileType)
        {            
            if (fileType == "Master")
            {
                if (!table.Columns.Contains("InvoiceNumber"))
                {
                    return "Invoice number column does not exist";
                }
                if (!table.Columns.Contains("InvoiceDate"))
                {
                    return "Invoice date column does not exist";
                }
                if (!table.Columns.Contains("AccountCode"))
                {
                    return "Account code column does not exist";
                }
                if (!table.Columns.Contains("DueDateOfPayment"))
                {
                    return "Due date of payment column does not exist";
                }
                if (!table.Columns.Contains("DCFlag"))
                {
                    return "Due date of payment column does not exist";
                }
                if (!table.Columns.Contains("BillAmount"))
                {
                    return "Bill Amount column does not exist";
                }
                if (!table.Columns.Contains("PNR"))
                {
                    return "PNR column does not exist";
                }
            }
            else
            {
                if (!table.Columns.Contains("Date"))
                {
                    return "Date column does not exist";
                }
                if (!table.Columns.Contains("Bill#"))
                {
                    return "Bill# column does not exist";
                }
                if (!table.Columns.Contains("Agency Name"))
                {
                    return "Agency Name column does not exist";
                }
                if (!table.Columns.Contains("PAX Name"))
                {
                    return "PAX Name column does not exist";
                }
                if (!table.Columns.Contains("Ticket No"))
                {
                    return "Ticket no column does not exist";
                }
                if (!table.Columns.Contains("Sector"))
                {
                    return "Sector column does not exist";
                }
                if (!table.Columns.Contains("Fare"))
                {
                    return "Fare column does not exist";
                }
                if (!table.Columns.Contains("Tax"))
                {
                    return "Tax column does not exist";
                }
                if (!table.Columns.Contains("Gross"))
                {
                    return "Gross column does not exist";
                }
                if (!table.Columns.Contains("APT Tax"))
                {
                    return "APT TAX column does not exist";
                }
                if (!table.Columns.Contains("H.CHRG"))
                {
                    return "H.Chrg column does not exist";
                }
                if (!table.Columns.Contains("TDS"))
                {
                    return "TDS column does not exist";
                }
                if (!table.Columns.Contains("S. Tax"))
                {
                    return "S.Tax column does not exist";
                }
                if (!table.Columns.Contains("NET"))
                {
                    return "Net column does not exist";
                }
                if (!table.Columns.Contains("OTH. CHRG"))
                {
                    return "OTH.Chrg column does not exist";
                }
                if (!table.Columns.Contains("Net Total"))
                {
                    return "Net Total column does not exist";
                }
                if (!table.Columns.Contains("PLB"))
                {
                    return "PLB column does not exist";
                }
                if (!table.Columns.Contains("PLBAmount"))
                {
                    return "PLBAmount column does not exist";
                }
                if (!table.Columns.Contains("Carrier"))
                {
                    return "Carrier column does not exist";
                }
                if (!table.Columns.Contains("StockType"))
                {
                    return "StockType column does not exist";
                }
                if (!table.Columns.Contains("Int-Dom"))
                {
                    return "Int-Dom column does not exist";
                }
                if (!table.Columns.Contains("PaymentTo"))
                {
                    return "PaymentTo column does not exist";
                }
                if (!table.Columns.Contains("PaxType"))
                {
                    return "PaxType column does not exist";
                }
                if (!table.Columns.Contains("FlightDetails"))
                {
                    return "FlightDetails column does not exist";
                }
                if (!table.Columns.Contains("FareBasis"))
                {
                    return "FareBasis column does not exist";
                }
                if (!table.Columns.Contains("Airline Commission"))
                {
                    return "AirlineCommission column does not exist";
                }
                if (!table.Columns.Contains("RAF Airline"))
                {
                    return "RAF Airline column does not exist";
                }
                if (!table.Columns.Contains("RAF Client"))
                {
                    return "RAF Client column does not exist";
                }
            }
            decimal checkVal = 0;
            DateTime checkDateVal;
            try
            {
                if (fileType == "Master")
                {
                    foreach (DataRow dr in table.Rows)
                    {

                        try
                        {
                            checkDateVal = DateTime.ParseExact(dr["InvoiceDate"].ToString(), "yyyyMMdd", null);
                        }
                        catch
                        {
                            return "InvoiceDate Format of the column is not correct";
                        }
                        //try
                        //{
                        //    checkDateVal = DateTime.ParseExact(dr["DueDateOfPayment"].ToString(), "yyyyMMdd", null);
                        //}
                        //catch
                        //{
                        //    return "DueDateOfPayment Format of the column is not correct" + dr["DueDateOfPayment"].ToString()+"  "+ dr["InvoiceNumber"].ToString();
                        //}
                    }
                }
                else
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        try
                        {
                            checkDateVal = DateTime.ParseExact(dr["Date"].ToString(), "yyyyMMdd", null);
                        }
                        catch
                        {
                            return "Date Format of the column is not correct";
                        }
                        try
                        {
                            checkVal = Convert.ToDecimal(dr["Fare"]);
                        }
                        catch
                        {
                            return "Fare Format of the column is not correct "+ dr["Bill#"].ToString();
                        }
                        try
                        {
                            checkVal = Convert.ToDecimal(dr["Tax"]);
                        }
                        catch
                        {
                            return "Tax Format of the column is not correct " + dr["Bill#"].ToString(); 
                        }
                        try
                        {
                            checkVal = Convert.ToDecimal(dr["Gross"]);
                        }
                        catch
                        {
                            return "Gross Format of the column is not correct " + dr["Bill#"].ToString();
                        }
                        try
                        {
                            checkVal = Convert.ToDecimal(dr["APT Tax"]);
                        }
                        catch
                        {
                            return "APT Tax Format of the column is not correct " + dr["Bill#"].ToString();
                        }
                        try
                        {
                            checkVal = Convert.ToDecimal(dr["H.CHRG"]);
                        }
                        catch
                        {
                            return "H.CHRG Format of the column is not correct " + dr["Bill#"].ToString();
                        }
                        try
                        {

                            checkVal = Convert.ToDecimal(dr["TDS"]);
                        }
                        catch
                        {
                            return "TDS Format of the column is not correct " + dr["Bill#"].ToString();
                        }
                        try
                        {
                            checkVal = Convert.ToDecimal(dr["S. Tax"]);
                        }
                        catch
                        {
                            return "S. Tax Format of the column is not correct " + dr["Bill#"].ToString();
                        }
                        try
                        {
                            checkVal = Convert.ToDecimal(dr["NET"]);
                        }
                        catch
                        {
                            return "NET Format of the column is not correct " + dr["Bill#"].ToString();
                        }
                        try
                        {
                            checkVal = Convert.ToDecimal(dr["OTH. CHRG"]);
                        }
                        catch
                        {
                            return "OTH. CHRG Format of the column is not correct " + dr["Bill#"].ToString();
                        }
                        try
                        {
                            checkVal = Convert.ToDecimal(dr["Net Total"]);
                        }
                        catch
                        {
                            return "Net Total Format of the column is not correct " + dr["Bill#"].ToString();
                        }
                        try
                        {
                            checkVal = Convert.ToDecimal(dr["PLB"]);
                        }
                        catch
                        {
                            return "PLB Format of the column is not correct " + dr["Bill#"].ToString();
                        }
                        try
                        {
                            checkVal = Convert.ToDecimal(dr["PLBAmount"]);
                        }
                        catch
                        {
                            return "PLBAmount Format of the column is not correct " + dr["Bill#"].ToString();
                        }
                        try
                        {
                            checkVal = Convert.ToDecimal(dr["RAF Airline"]);
                        }
                        catch
                        {
                            return "RAF Airline Format of the column is not correct " + dr["Bill#"].ToString();
                        }
                        try
                        {
                            checkVal = Convert.ToDecimal(dr["RAF Client"]);
                        }
                        catch
                        {
                            return "RAF Client Format of the column is not correct " + dr["Bill#"].ToString();
                        }
                        Match match = Regex.Match(dr["TICKET NO"].ToString(), "^[a-zA-Z0-9 ]+$");
                        if (!match.Success)
                        {
                            return "TICKET NO Format of the column is not correct " + dr["TICKET NO"].ToString();
                        }
                        string tempString = dr["FLIGHTDETAILS"].ToString();
                        string[] tempString1 = new string[1];
                        tempString1[0] = "LO";
                        //string[] splittedFlightDetails = tempString.Split(tempString1, StringSplitOptions.None);
                        //checkDateVal = DateTime.ParseExact(splittedFlightDetails[1].Substring(0, 8), "yyyyMMdd", null);
                    }
                }
            }
            catch (FormatException ex)
            {
                return "Format of the column is not correct exception:"+ex.Message;
            }
            return string.Empty;
        }
        /// <summary>
        /// This will return all the distinct offline bookings for the given agent
        /// </summary>
        /// <param name="agencyId">agencyId</param>
        /// <returns></returns>
        private static List<string> GetDistinctOfflineInvoiceKeys(string whereString, int pageNo, int recordsPerPage, int totalRecords)
        {
            //Trace.TraceInformation("OfflineBooking.GetOfflineInvoiceKeys entered  ");
            List<string> offlineInvoiceKeys = new List<string>();           
            int endRow = 0;
            int startRow = ((recordsPerPage * (pageNo - 1)) + 1);
            if ((startRow + recordsPerPage) - 1 < totalRecords)
            {
                endRow = (startRow + recordsPerPage) - 1;
            }
            else
            {
                endRow = totalRecords;
            }
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@whereString", whereString);            
            paramList[1] = new SqlParameter("@startRow", startRow);
            paramList[2] = new SqlParameter("@endRow", endRow);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetDistinctOfflineInvoiceKeys, paramList, connection);
            while (dataReader.Read())
            {
                offlineInvoiceKeys.Add(dataReader["InvoiceReferenceKey"].ToString());
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("OfflineBooking.GetOfflineInvoiceKeys entered : offlineInvoiceKeys count = " + offlineInvoiceKeys.Count);
            return offlineInvoiceKeys;
        }
        /// <summary>
        /// Counts the total number of pages
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="agentFilter"></param>
        /// <returns></returns>
        public static int TotalPages(string whereString)
        {
            //Trace.TraceInformation("OfflineBooking.TotalPages entered " );            
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@whereString", whereString);
            SqlConnection con = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetPageCountForOfflineBooking , paramList, con);
            int queueCount = 0;
            if (data.Read())
            {
                queueCount = Convert.ToInt32(data["count"]);
            }
            data.Close();
            con.Close();
            //Trace.TraceInformation("OfflineBooking.TotalPages Exited queueCount=" + queueCount);
            return queueCount;
        }
        /// <summary>
        /// Get the count for offline bookings for a particular agency
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static int GetQueueCountForOfflineBookings(int agencyId)
        {
            //Trace.TraceInformation("OfflineBooking.TotalPages entered ");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlConnection con = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetQueueCountForOfflineBookings, paramList, con);
            int queueCount = 0;
            if (data.Read())
            {
                queueCount = Convert.ToInt32(data["count"]);
            }
            data.Close();
            con.Close();
            //Trace.TraceInformation("OfflineBooking.TotalPages Exited queueCount=" + queueCount);
            return queueCount;
        }
        private static bool IsIncorrectAccountCode(string accountCode)
        {
            if (accountCode.Length == 5)
            {
                if (accountCode.Substring(0, 1).ToUpper() == "A" || accountCode.Substring(0, 1).ToUpper() == "Z")
                {
                    try
                    {
                        int agencyId = Convert.ToInt32(accountCode.Substring(1, 4));
                        //if (Agency.IsAgencyPresent(agencyId))// TODO ziya
                        if (true)
                        {
                            return false;
                        }
                        else
                        {
                            ErrorString += agencyId.ToString() + " not found/n";
                           // System.IO.File.AppendAllText("C://OfflineBookingError.txt",agencyId.ToString()+" not found/n");
                            return true;
                        }
                    }
                    catch
                    {
                        ErrorString += "Account Code error: account code=" + accountCode.ToString();
                        //System.IO.File.AppendAllText("C://OfflineBookingError.txt", "Account Code error: account code=" + accountCode.ToString());
                        return true;
                    }
                }
                else
                {
                    ErrorString += "Account Code error: account code=" + accountCode.ToString();
                    //System.IO.File.AppendAllText("C://OfflineBookingError.txt", "Account Code error: account code="+accountCode.ToString());
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        public static bool IsTicketNumberPresent(string ticketNumber)
        {
            //Trace.TraceInformation("OfflineBooking.IsTicketNumberPresent entered");
            bool isTicketNumberPresent = false;
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ticketNo", ticketNumber);
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsTicketNoPresentInOfflineBooking, paramList, connection);
            if (data.Read())
            {
                isTicketNumberPresent = true;
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("OfflineBooking.IsTicketNumberPresent exiting: return = " + isTicketNumberPresent);
            return isTicketNumberPresent;
        }
#endregion
    }

}
