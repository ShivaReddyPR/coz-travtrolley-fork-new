﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;

namespace CT.CMS
{
    public class B2BCMSDetails
    {
        #region properties
        public int CMS_ID { get; set; }
        public string CMS_AGENT_ID { get; set; }
        public string CMS_BANK_DETAILS { get; set; }
        public string CMS_SUPPORT_DETAILS { get; set; }
        public bool CMS_STATUS { get; set; }
        public int CMS_CREATED_BY { get; set; }
        public int CMS_MODIFIED_BY { get; set; }
        public bool CMS_IS_BANK { get; set; }
        public bool CMS_IS_SUPPORT { get; set; }
        public List<B2BCMSSlider> sliders { get; set; }
        #endregion
        #region methods
        public int save()
        {           
            SqlTransaction trans = null;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = DBGateway.CreateConnection();
                cmd.Connection.Open();
                trans = cmd.Connection.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@CMS_ID", CMS_ID));
                paramList[0].Direction = ParameterDirection.Output;
                paramList.Add(new SqlParameter("@ID", CMS_ID));
                paramList.Add(new SqlParameter("@CMS_AGENT_ID", CMS_AGENT_ID));
                paramList.Add(new SqlParameter("@CMS_BANK_DETAILS", CMS_BANK_DETAILS));
                paramList.Add(new SqlParameter("@CMS_SUPPORT_DETAILS", CMS_SUPPORT_DETAILS));
                paramList.Add(new SqlParameter("@CMS_STATUS", CMS_STATUS));
                paramList.Add(new SqlParameter("@CMS_CREATED_BY", CMS_CREATED_BY)); 
                paramList.Add(new SqlParameter("@CMS_IS_BANK", CMS_IS_BANK));
                paramList.Add(new SqlParameter("@CMS_IS_SUPPORT", CMS_IS_SUPPORT));               
                int res = DBGateway.ExecuteNonQuery("USP_Add_cms_details", paramList.ToArray());
                if (paramList[0].Value != DBNull.Value)
                    CMS_ID = Convert.ToInt32(paramList[0].Value);
                if (CMS_ID > 0)
                {
                    foreach (B2BCMSSlider slider in sliders)
                    {
                        slider.CMS_ID = CMS_ID;
                        slider.save();
                    }
                }
                else
                {
                    throw new Exception("Failed to saving the Master Details");
                }
                trans.Commit();
                return res;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                Audit.Add(EventType.Exception, Severity.High, GetUserID(), "CMS MAster failed to . Error:" + ex.ToString(), "");
                throw ex;
            }           
        }

        public static int GetUserID()
        {
            return (Settings.LoginInfo != null ? (int)Settings.LoginInfo.UserID : 1);
        }
        public static DataTable GetCMSSliders(int agentId)
        {
            DataTable dt = new DataTable();
            try
            {    
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@CMS_AGENT_ID", agentId));
                dt = DBGateway.FillDataTableSP(@"USP_GetSliders", paramList.ToArray());
                return dt;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, GetUserID(), "CMS Master failed to getting the sliders . Error:" + ex.ToString(), "");
                throw ex;
            }            
        }
        public static DataSet GetAgentsList(int companyId, string agentType, int loginAgent, ListStatus status, RecordStatus recordStatus)
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                if (companyId > 0) paramList.Add(new SqlParameter("@P_COMPANY_ID", companyId));
                paramList.Add(new SqlParameter("@P_LIST_STATUS", status));
                paramList.Add(new SqlParameter("@P_RECORD_STATUS", Utility.ToString((char)recordStatus)));
                if (!string.IsNullOrEmpty(agentType)) paramList.Add(new SqlParameter("@P_AGENT_TYPE", agentType));
                if (loginAgent > 0) paramList.Add(new SqlParameter("@P_LOGIN_AGENT_ID", loginAgent));
                return DBGateway.ExecuteQuery("USP_GetAgentMaster_CMS", paramList.ToArray());
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, GetUserID(), "CMS MAster failed to updating the sliders . Error:" + ex.ToString(), "");
                throw ex;
            }            
        }
        #endregion
        public static B2BCMSDetails GetCMSData(long AgentId, int AgentType)
        {
            B2BCMSDetails objcmsdetails = new B2BCMSDetails();
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_CMS_AGENTID", AgentId);
                paramList[1] = new SqlParameter("@P_AGENTTYPE", AgentType);
                DataSet dsCMSList = DBGateway.ExecuteQuery("CT_P_CMS_AGENT_GETDATA", paramList);
                if (dsCMSList != null && dsCMSList.Tables != null && dsCMSList.Tables.Count > 0)
                {
                    DataTable dtDetails = dsCMSList.Tables[0];
                    if (dtDetails != null && dtDetails.Rows != null && dtDetails.Rows.Count > 0)
                    {
                        if (dtDetails.Rows[0]["CMS_ID"] != DBNull.Value)
                        {
                            objcmsdetails.CMS_ID = Convert.ToInt32(dtDetails.Rows[0]["CMS_ID"]);
                        }
                        if (dtDetails.Rows[0]["CMS_BANK_DETAILS"] != DBNull.Value)
                        {
                            objcmsdetails.CMS_BANK_DETAILS = Convert.ToString(dtDetails.Rows[0]["CMS_BANK_DETAILS"]);
                        }
                        if (dtDetails.Rows[0]["CMS_SUPPORT_DETAILS"] != DBNull.Value)
                        {
                            objcmsdetails.CMS_SUPPORT_DETAILS = Convert.ToString(dtDetails.Rows[0]["CMS_SUPPORT_DETAILS"]);
                        }
                        if (dtDetails.Rows[0]["CMS_IS_BANK"] != DBNull.Value)
                        {
                            objcmsdetails.CMS_IS_BANK = Convert.ToBoolean(dtDetails.Rows[0]["CMS_IS_BANK"]);
                        }
                        if (dtDetails.Rows[0]["CMS_IS_SUPPORT"] != DBNull.Value)
                        {
                            objcmsdetails.CMS_IS_SUPPORT = Convert.ToBoolean(dtDetails.Rows[0]["CMS_IS_SUPPORT"]);
                        }
                    }
                    if (dsCMSList.Tables.Count > 1)
                    {
                        DataTable dtsliderlist = dsCMSList.Tables[1];
                        if (dtsliderlist != null && dtsliderlist.Rows != null && dtsliderlist.Rows.Count > 0)
                        {
                            objcmsdetails.sliders = new List<B2BCMSSlider>();
                            foreach (DataRow drslider in dtsliderlist.Rows)
                            {
                                B2BCMSSlider objsliderlist = new B2BCMSSlider();
                                if (drslider["CMS_IMAGE_FILE_PATH"] != DBNull.Value)
                                {
                                    objsliderlist.CMS_IMAGE_FILE_PATH = Convert.ToString(drslider["CMS_IMAGE_FILE_PATH"]);
                                }
                                if (drslider["CMS_IMAGE_LABEL"] != DBNull.Value)
                                {
                                    objsliderlist.CMS_IMAGE_LABEL = Convert.ToString(drslider["CMS_IMAGE_LABEL"]);
                                }
                                objcmsdetails.sliders.Add(objsliderlist);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objcmsdetails;
        }
    }
    public class B2BCMSSlider
    {
        #region properties     
        public int SLIDER_ID { get; set; }
        public int CMS_ID { get; set; }
        public string CMS_IMAGE_FILE_PATH { get; set; }
        public string CMS_FILE_TYPE { get; set; }
        public string CMS_IMAGE_LABEL { get; set; }
        public bool SLIDER_STATUS { get; set; }
        public int SLIDER_MODIFIED_BY { get; set; }
        public int SLIDER_CREATED_BY { get; set; }
        public DateTime? SLIDER_CREATED_ON { get; set; }
        public DateTime? SLIDER_MODIFIED_ON { get; set; }

        public int SLIDER_ORDER { get; set; }
        #endregion
        #region methods
        public void save()
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@CMS_ID", CMS_ID));
                paramList.Add(new SqlParameter("@CMS_IMAGE_FILE_PATH", CMS_IMAGE_FILE_PATH));
                paramList.Add(new SqlParameter("@CMS_FILE_TYPE", CMS_FILE_TYPE));
                paramList.Add(new SqlParameter("@CMS_IMAGE_LABEL", CMS_IMAGE_LABEL));
                paramList.Add(new SqlParameter("@SLIDER_STATUS", SLIDER_STATUS));
                paramList.Add(new SqlParameter("@SLIDER_CREATED_BY", SLIDER_CREATED_BY));
                paramList.Add(new SqlParameter("@SLIDER_ID", SLIDER_ID)); 
                int res = DBGateway.ExecuteNonQuery(@"USP_Add_CMS_Sliders", paramList.ToArray());
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, B2BCMSDetails.GetUserID(), "CMS Master failed to adding the sliders. Error:" + ex.ToString(), "");
                throw ex;
            }           
        }
        public static int Update_SliderStatus(ref string cmsId,ref string sliderId,ref bool status)
        {
            int res = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@CMS_ID", cmsId));
                paramList.Add(new SqlParameter("@SLIDER_ID", sliderId));
                paramList.Add(new SqlParameter("@SLIDER_STATUS", status));
                paramList.Add(new SqlParameter("@SLIDER_CREATED_BY", Settings.LoginInfo.UserID));               
                res = DBGateway.ExecuteNonQuery(@"USP_Add_CMS_Sliders", paramList.ToArray());
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, B2BCMSDetails.GetUserID(), "CMS MAster Failed to Update status. Error:" + ex.ToString(), "");
                throw ex;
            }
            return res;
        }
        public static int Delete_Slider(int sliderId)
        {
            int res = 0;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@SLIDER_ID", sliderId));
                res = DBGateway.ExecuteNonQuery(@"USP_CMS_Slider_Deletion", paramList.ToArray());
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, B2BCMSDetails.GetUserID(), "CMS MAster Failed to Delete the Slider. Error:" + ex.ToString(), "");
                throw ex;
            }
            return res;
        }
        public void Update_SliderDetails()
        {
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@SLIDER_ID", SLIDER_ID));
                paramList.Add(new SqlParameter("@CMS_IMAGE_FILE_PATH", CMS_IMAGE_FILE_PATH));
                paramList.Add(new SqlParameter("@CMS_FILE_TYPE", CMS_FILE_TYPE));
                paramList.Add(new SqlParameter("@CMS_IMAGE_LABEL", CMS_IMAGE_LABEL));
                paramList.Add(new SqlParameter("@SLIDER_CREATED_BY", SLIDER_CREATED_BY));
                paramList.Add(new SqlParameter("@SLIDER_ORDER",SLIDER_ORDER ));
                DBGateway.ExecuteNonQuery(@"USP_UPDATE_SLIDER_DETAILS", paramList.ToArray());
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, B2BCMSDetails.GetUserID(), "CMS MAster Failed to Update. Error:" + ex.ToString(), "");
                throw ex;
            }
        }
        #endregion
    }
}
