﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

namespace CT.CMS
{
    /// <summary>
    /// Summary description for Resource
    /// </summary>
    public class ResourceMaster
    {

        #region Members & Properties
        int resource_id;
        string culture_code;
        string resource_class;
        string resource_key;
        string resource_value;
        string status;
        string resource_description;
        public int Resource_Id
        {
            get { return resource_id; }
            set { resource_id = value; }
        }
        public string Culture_code
        {
            get { return culture_code; }
            set { culture_code = value; }
        }
        public string Resource_class
        {
            get { return resource_class; }
            set { resource_class = value; }
        }
        public string Resource_key
        {
            get { return resource_key; }
            set { resource_key = value; }
        }
        public string Resource_value
        {
            get { return resource_value; }
            set { resource_value = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        public string Resource_description
        {
            get { return resource_description; }
            set { resource_description = value; }
        }
        #endregion


        public ResourceMaster()
        {
            
        }
        public static DataTable GetResourceKeysList(string cultureCode,string resClass)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_Culture_Code", cultureCode);
                paramList[1] = new SqlParameter("@P_Res_Class", resClass);
                return DBGateway.ExecuteQuery("usp_GetResourceKeys", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void DeleteResourceKey(int resourceId, string status)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_RES_ID", resourceId);
                paramList[1] = new SqlParameter("@P_STATUS", status);
                DBGateway.ExecuteNonQuery("usp_Update_ResourceKey_Status", paramList);
            }
            catch { throw; }
        }

        public int save()
        {
            
            try
            {
                SqlParameter[] paramList = new SqlParameter[8];
                paramList[0] = new SqlParameter("@P_Resource_Id", resource_id);
                paramList[1] = new SqlParameter("@P_Culture_Code", culture_code);
                paramList[2] = new SqlParameter("@P_Resource_Class", resource_class);
                paramList[3] = new SqlParameter("@P_Resource_Key", resource_key);
                paramList[4] = new SqlParameter("@P_Resource_Value", resource_value);
                paramList[5] = new SqlParameter("@P_Resource_Status", status);
                paramList[6] = new SqlParameter("@P_Resource_Desc", resource_description);
                paramList[7] = new SqlParameter("@P_Resource_Id_Ret", SqlDbType.Int);
                paramList[7].Direction = ParameterDirection.Output;
                int count = DBGateway.ExecuteNonQuery("usp_ResKey_Add_Update", paramList);
                if (paramList[7].Value != DBNull.Value)
                {
                    resource_id = Convert.ToInt32(paramList[7].Value);
                }
            }
            catch
            {

                throw;
            }
            return resource_id;
        }



    }
}