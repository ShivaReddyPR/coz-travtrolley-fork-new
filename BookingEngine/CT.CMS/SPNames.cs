namespace CT.CMS
{
    internal static class SPNames
    {
        public const string SaveNewsItem = "usp_SaveNewsItem";
        public const string GetNews = "usp_GetNews";
        public const string GetNewsByNumOfRow = "usp_GetNewsByNumOfRow";
        public const string SaveAirlineDeal = "usp_SaveAirlineDeal";
        public const string GetAirlineDeal = "usp_GetAirlineDeal";
        public const string GetDealByAirlineCode = "usp_GetDealByAirlineCode";
        public const string GetDealsByNumOfRow = "usp_GetDealsByNumOfRow";
        public const string CheckAirlineCode = "usp_CheckAirlineCode";
        public const string UpdateNewsItem = "usp_UpdateNewsItem";
        public const string GetNewsByNewsId = "usp_GetNewsByNewsId";
        public const string DeleteNewsItem = "usp_DeleteNewsItem";
        public const string GetDealByDealId = "usp_GetDealByDealId";
        public const string DeleteAirlineDeal = "usp_DeleteAirlineDeal";
        public const string UpdateAirlineDeal = "usp_UpdateAirlineDeal";
        public const string SaveAdvertisement = "usp_SaveAdvertisement";
        public const string GetAdvertisement = "usp_GetAdvertisement";
        public const string UpdateAdvertisement = "usp_UpdateAdvertisement";
        public const string GetAdById = "usp_GetAdById";
        public const string SaveHotelDeal = "usp_SaveHotelDeal";
        public const string GetAllHotelDeals = "usp_GetAllHotelDeals";
        public const string CountAllHotelDeals = "usp_CountAllHotelDeals";
        public const string GetAllActiveHotelDeals = "usp_GetAllActiveHotelDeals";
        public const string CountActiveHotelDeals = "usp_CountActiveHotelDeals";
        public const string GetHotelDealById = "usp_GetHotelDealById";
        public const string SaveHotelRequest = "usp_SaveHotelRequest";
        public const string ActivateHotelDeal = "usp_ActivateHotelDeal";
        public const string DeactivateHotelDeal = "usp_DeactivateHotelDeal";
        public const string GetAllHotelRequests = "usp_GetAllHotelRequests";
        public const string CountPendingHotelRequests = "usp_CountPendingHotelRequests";
        public const string DeleteHotelRequest = "usp_DeleteHotelRequest";
        public const string ProcessedHotelRequest = "usp_ProcessedHotelRequest";
        public const string AddEntryInAdPlacement = "usp_AddEntryInAdPlacement";
        public const string DeleteEntriesofAdPlacement = "usp_DeleteEntriesofAdPlacement";
        public const string GetLoadAdBLock = "usp_GetLoadAdBLock";
        public const string GetAdOfAgent = "usp_GetAdOfAgent";
        public const string GetAdsOfModule = "usp_GetAdsOfModule";
        public const string GetModuleBlock = "usp_GetModuleBlock";
        public const string DeactivateAd = "usp_DeactivateAd";
        public const string GetAdModulebyProductType = "usp_GetAdModulebyProductType";

        //Hotel Deal
        public const string SaveHotelDealData = "usp_SaveHotelDealData";
        public const string GetHotelDealData = "usp_GetHotelDealData";
        public const string UpdateHotelDeal = "usp_UpdateHotelDeal";
        public const string GetAllFeaturedHotelDeals = "usp_GetAllFeaturedHotelDeals";
        public const string ActivateFeaturedHotelDeal = "usp_ActivateFeaturedHotelDeal";
        public const string DeactivateFeaturedHotelDeal = "usp_DeactivateFeaturedHotelDeal";
        public const string GetHotelDealDataByCity = "usp_GetHotelDealDataByCity";
        public const string DeleteHotelDealData = "usp_DeleteHotelDealData";
        public const string GetManageDealCount = "usp_GetManageDealCount";
        public const string GetHotelRequestByRequestId = "usp_GetHotelRequestByRequestId";
        public const string GetTotalFilteredHolidayRequestCount = "usp_GetTotalFilteredHolidayRequestCount";
        public const string GetTotalFilteredHolidayRequest = "usp_GetTotalFilteredHolidayRequest";

        //Airline Deal Sheet
        public const string GetAirlineDealSheet = "usp_GetAirlineDealSheet";
        public const string AddAirlineDealSheet = "usp_AddAirlineDealSheet";
        public const string UpdateAirlineDealSheet = "usp_UpdateAirlineDealSheet";
        public const string DeleteAirlineDealSheet = "usp_DeleteAirlineDealSheet";
        //dynamic Advertise
        public const string SaveDynamicAd = "usp_SaveDynamicAd";
        public const string GetAllDynamicAd = "usp_GetAllDynamicAd";
        public const string GetDynamicAdById = "usp_GetDynamicAdById";
        public const string UpdateDynamicAd = "usp_UpdateDynamicAd";
        public const string DynamicAdHitsCount = "usp_DynamicAdHitsCount";
        public const string DeactivateDyAd = "usp_DeactivateDyAd";
        public const string GetASingleDyAd = "usp_GetASingleDyAd";
        public const string GetDynamicAdByRandomId = "usp_GetDynamicAdByRandomId";
        public const string DyanamicAdOpenHitsCount = "usp_DyanamicAdOpenHitsCount";
    }
}
