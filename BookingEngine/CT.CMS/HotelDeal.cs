using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Transactions;
using System.Text.RegularExpressions;

namespace CT.CMS
{
    public struct HotelDealData
    {
        public DateTime ValidFrom;
        public DateTime ValidUpto;
        public String RoomType;
        public Double Price;
    }

    public class HotelDeal
    {
        #region Private Fields

        private int dealId;
        private string dealTitle;
        private DateTime createdOn;
        private int createdBy;
        private string hotelName;
        private int hotelRating;
        private string hotelLocation;
        private string description;
        private string includes;
        private string specialAttractions;
        private bool isActive;
        private int lastModifiedBy;
        private DateTime lastModifiedOn;
        List<HotelDealData> dealMoreData;
        string hotelProfile;
        List<string> hotelImage; // contains images seperated by |
        string inAndAround;
        string dealDesriptionDoc;   // Upload document for deal Description     
        bool isFeatured;
        /// <summary>
        /// Field having possible file extensions
        /// </summary>
        private string[] fileExt ={ "jpg", "jpeg", "jpe", "jfif", "bmp", "gif", "tiff", "tif", "png", "dib" };

        #endregion

        #region Public Properties

        /// <summary>
        /// Deal Id of the HotelDeal
        /// </summary>
        public int DealId
        {
            get { return dealId; }
            set { dealId = value; }
        }

        /// <summary>
        /// Title of the HotelDeal
        /// </summary>
        public string DealTitle
        {
            get { return dealTitle; }
            set { dealTitle = value; }
        }

        /// <summary>
        /// Creation time of the Deal
        /// </summary>
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }

        /// <summary>
        /// Id of the Deal Creator
        /// </summary>
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        /// <summary>
        /// Name of the Hotel or Hotel Chain for which the deal has been created
        /// </summary>
        public string HotelName
        {
            get
            {
                return hotelName;
            }
            set
            {
                hotelName = value;
            }
        }

        /// <summary>
        /// Star rating of the Hotel
        /// </summary>
        public int HotelRating
        {
            get
            {
                return hotelRating;
            }
            set
            {
                hotelRating = value;
            }
        }

        /// <summary>
        /// Location or city in which Hotel is situated
        /// </summary>
        public string HotelLocation
        {
            get
            {
                return hotelLocation;
            }
            set
            {
                hotelLocation = value;
            }
        }

        /// <summary>
        /// Hotel/deal description
        /// </summary>
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        /// <summary>
        /// Facilities Available in Hotel
        /// </summary>
        public string Includes
        {
            get
            {
                return includes;
            }
            set
            {
                includes = value;
            }
        }

        public string SpecialAttractions
        {
            get
            {
                return specialAttractions;
            }
            set
            {
                specialAttractions = value;
            }
        }

        /// <summary>
        /// Status of the Deal
        /// </summary>
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }

        /// <summary>
        /// Member Id of the person who lastly modified the Deal
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Last Date on which modification was done
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Time/Date from which Deal will be valid
        /// </summary>
        public List<HotelDealData> DealMoreData
        {
            get
            {
                return dealMoreData;
            }
            set
            {
                dealMoreData = value;
            }
        }

        /// <summary>
        /// Time/Date the deal will be valid upto
        /// </summary>
        public string HotelProfile
        {
            get
            {
                return hotelProfile;
            }
            set
            {
                hotelProfile = value;
            }
        }

        public List<string> HotelImage
        {
            get
            {
                return hotelImage;
            }
            set
            {
                hotelImage = value;
            }
        }

        public string InAndAround
        {
            get
            {
                return inAndAround;
            }
            set
            {
                inAndAround = value;
            }
        }

        public string DealDesriptionDoc
        {

            get
            {
                return dealDesriptionDoc;
            }
            set
            {
                dealDesriptionDoc = value;
            }
        }
        public bool IsFeatured
        {
            get { return isFeatured; }
            set { isFeatured = value; }
        }
        #endregion

        #region Public Methods
        #region Get Data Methods
        /// <summary>
        /// Method for getting all hotel deals
        /// </summary>
        /// <returns></returns>
        public List<HotelDeal> GetAllHotelDeals(int pageNumber, int noOfRecordsPerPage, int totalrecords, string whereString, string orderByString)
        {
            ////Trace.TraceInformation("HotelDeal.GetAllHotelDeals entered");

            List<HotelDeal> hotelData = new List<HotelDeal>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[4];
            int endrow = 0;
            int startrow = ((noOfRecordsPerPage * (pageNumber - 1)) + 1);
            if ((startrow + noOfRecordsPerPage) - 1 < totalrecords)
            {
                endrow = (startrow + noOfRecordsPerPage) - 1;
            }
            else
            {
                endrow = totalrecords;
            }
            paramList[0] = new SqlParameter("@whereString", whereString);
            paramList[1] = new SqlParameter("@orderByString", orderByString);
            paramList[2] = new SqlParameter("@startrow", startrow);
            paramList[3] = new SqlParameter("@endrow", endrow);
            SqlDataReader tempData = DBGateway.ExecuteReaderSP(SPNames.GetAllHotelDeals, paramList,connection);
            int j = 0;
            while (tempData.Read())
            {
                HotelDeal newHotel = new HotelDeal();
                newHotel.dealId = Convert.ToInt32(tempData["dealId"]);
                newHotel.isActive = Convert.ToBoolean(tempData["isActive"]);
                newHotel.dealTitle = Convert.ToString(tempData["dealTitle"]);
                newHotel.hotelName = Convert.ToString(tempData["hotelName"]);
                if (tempData["hotelImage"] != null)
                {
                    List<string> imgs = new List<string>();
                    string[] images = Convert.ToString(tempData["hotelImage"]).Split('|');
                    imgs.AddRange(images);
                    newHotel.hotelImage = imgs;
                }
                newHotel.hotelRating = Convert.ToInt32(tempData["hotelRating"]);
                newHotel.hotelLocation = Convert.ToString(tempData["hotelLocation"]);
                newHotel.description = Convert.ToString(tempData["description"]);
                newHotel.createdOn = Convert.ToDateTime(tempData["createdOn"]);
                newHotel.createdBy = Convert.ToInt32(tempData["createdBy"]);
                newHotel.includes = Convert.ToString(tempData["includes"]);
                newHotel.specialAttractions = Convert.ToString(tempData["specialAttractions"]);
                //newHotel.price = Convert.ToString(tempData["price"]);
                newHotel.lastModifiedBy = Convert.ToInt32(tempData["lastModifiedBy"]);
                newHotel.lastModifiedOn = Convert.ToDateTime(tempData["lastModifiedOn"]);
                newHotel.hotelProfile = Convert.ToString(tempData["hotelProfile"]);
                newHotel.inAndAround = Convert.ToString(tempData["inAndAround"]);
                newHotel.dealDesriptionDoc = Convert.ToString(tempData["dealDescDocument"]);
                newHotel.isFeatured = Convert.ToBoolean(tempData["isFeatured"]);

                //Get Deal More data
                List<HotelDealData> dealData = GetHotelDealData(newHotel.dealId);
                newHotel.dealMoreData = dealData;

                hotelData.Add(newHotel);

                j++;
            }
            tempData.Close();
            connection.Close();
            return hotelData;

        }

        /// <summary>
        /// Method for getting active hotel deals
        /// </summary>
        /// <returns></returns>
        public List<HotelDeal> GetAllActiveHotelDeals()
        {
            ////Trace.TraceInformation("HotelDeal.GetAllHotelDeals entered");
            List<HotelDeal> hotelData = new List<HotelDeal>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader tempData = DBGateway.ExecuteReader(SPNames.GetAllActiveHotelDeals, paramList, connection);
            
            while (tempData.Read())
            {
                HotelDeal newHotel = new HotelDeal();
                newHotel.dealId = Convert.ToInt32(tempData["dealId"]);
                newHotel.isActive = Convert.ToBoolean(tempData["isActive"]);
                newHotel.dealTitle = Convert.ToString(tempData["dealTitle"]);
                newHotel.hotelName = Convert.ToString(tempData["hotelName"]);
                if (tempData["hotelImage"] != null)
                {
                    List<string> imgs = new List<string>();
                    string[] images = Convert.ToString(tempData["hotelImage"]).Split('|');
                    imgs.AddRange(images);
                    newHotel.hotelImage = imgs;
                }
                newHotel.hotelRating = Convert.ToInt32(tempData["hotelRating"]);
                newHotel.hotelLocation = Convert.ToString(tempData["hotelLocation"]);
                newHotel.description = Convert.ToString(tempData["description"]);
                newHotel.createdOn = Convert.ToDateTime(tempData["createdOn"]);
                newHotel.createdBy = Convert.ToInt32(tempData["createdBy"]);
                newHotel.includes = Convert.ToString(tempData["includes"]);
                newHotel.specialAttractions = Convert.ToString(tempData["specialAttractions"]);
                //newHotel.price = Convert.ToString(tempData["price"]);
                newHotel.lastModifiedBy = Convert.ToInt32(tempData["lastModifiedBy"]);
                newHotel.lastModifiedOn = Convert.ToDateTime(tempData["lastModifiedOn"]);
                newHotel.hotelProfile = Convert.ToString(tempData["hotelProfile"]);
                newHotel.inAndAround = Convert.ToString(tempData["inAndAround"]);
                newHotel.dealDesriptionDoc = Convert.ToString(tempData["dealDescDocument"]);
                newHotel.isFeatured = Convert.ToBoolean(tempData["isFeatured"]);
                //Get Deal More data
                List<HotelDealData> dealData = GetHotelDealData(newHotel.dealId);
                newHotel.dealMoreData = dealData;

                hotelData.Add(newHotel);

            }
            tempData.Close();
            connection.Close();
            return hotelData;

        }

        /// <summary>
        /// This method will get All featured hotels.
        /// </summary>
        /// <returns></returns>
        public List<HotelDeal> GetAllFeaturedHotelDeals()
        {
            //Trace.TraceInformation("HotelDeal.GetAllFeaturedHotelDeals entered");
            List<HotelDeal> hotelData = new List<HotelDeal>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader tempData = DBGateway.ExecuteReader(SPNames.GetAllFeaturedHotelDeals, paramList, connection);
            
            while (tempData.Read())
            {
                HotelDeal newHotel = new HotelDeal();
                newHotel.dealId = Convert.ToInt32(tempData["dealId"]);
                newHotel.isActive = Convert.ToBoolean(tempData["isActive"]);
                newHotel.dealTitle = Convert.ToString(tempData["dealTitle"]);
                newHotel.hotelName = Convert.ToString(tempData["hotelName"]);
                if (tempData["hotelImage"] != null)
                {
                    List<string> imgs = new List<string>();
                    string[] images = Convert.ToString(tempData["hotelImage"]).Split('|');
                    imgs.AddRange(images);
                    newHotel.hotelImage = imgs;
                }
                newHotel.hotelRating = Convert.ToInt32(tempData["hotelRating"]);
                newHotel.hotelLocation = Convert.ToString(tempData["hotelLocation"]);
                newHotel.description = Convert.ToString(tempData["description"]);
                newHotel.createdOn = Convert.ToDateTime(tempData["createdOn"]);
                newHotel.createdBy = Convert.ToInt32(tempData["createdBy"]);
                newHotel.includes = Convert.ToString(tempData["includes"]);
                newHotel.specialAttractions = Convert.ToString(tempData["specialAttractions"]);
                //newHotel.price = Convert.ToString(tempData["price"]);
                newHotel.lastModifiedBy = Convert.ToInt32(tempData["lastModifiedBy"]);
                newHotel.lastModifiedOn = Convert.ToDateTime(tempData["lastModifiedOn"]);
                newHotel.hotelProfile = Convert.ToString(tempData["hotelProfile"]);
                newHotel.inAndAround = Convert.ToString(tempData["inAndAround"]);
                newHotel.dealDesriptionDoc = Convert.ToString(tempData["dealDescDocument"]);
                newHotel.isFeatured = Convert.ToBoolean(tempData["isFeatured"]);
                //Get Deal More data
                List<HotelDealData> dealData = GetHotelDealData(newHotel.dealId);
                newHotel.dealMoreData = dealData;

                hotelData.Add(newHotel);

            }
            tempData.Close();
            connection.Close();
            return hotelData;

        }

        /// <summary>
        /// Getting Data related to a particular hotel deal by it's id.
        /// </summary>
        /// <param name="selDealId"></param>
        /// <returns></returns>
        public HotelDeal GetHotelDealById(int selDealId)
        {
            //Trace.TraceInformation("HotelDeal.GetHotelDealById entered:dealId= " + selDealId);

            HotelDeal hotelData = new HotelDeal();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@dealId", selDealId);
            SqlConnection connection = DBGateway.GetConnection();
            try
            {
                SqlDataReader temData = DBGateway.ExecuteReaderSP(SPNames.GetHotelDealById, paramList, connection);

                if (temData.Read())
                {
                    if (temData["DealTitle"] != DBNull.Value)
                    {
                        hotelData.DealTitle = Convert.ToString(temData["DealTitle"]);
                    }
                    else
                    {
                        hotelData.DealTitle = string.Empty;
                    }

                    if (temData["hotelName"] != DBNull.Value)
                    {
                        hotelData.HotelName = Convert.ToString(temData["hotelName"]);
                    }

                    if (temData["hotelRating"] != DBNull.Value)
                    {
                        hotelData.HotelRating = Convert.ToInt32(temData["hotelRating"]);
                    }

                    if (temData["hotelLocation"] != DBNull.Value)
                    {
                        hotelData.HotelLocation = Convert.ToString(temData["hotelLocation"]);
                    }

                    if (temData["description"] != DBNull.Value)
                    {
                        hotelData.Description = Convert.ToString(temData["description"]);
                    }

                    //if (temData["price"] != DBNull.Value)
                    //{
                    //    hotelData.Price = Convert.ToString(temData["price"]);
                    //}

                    if (temData["includes"] != DBNull.Value)
                    {
                        hotelData.Includes = Convert.ToString(temData["includes"]);
                    }

                    if (temData["specialAttractions"] != DBNull.Value)
                    {
                        hotelData.SpecialAttractions = Convert.ToString(temData["specialAttractions"]);
                    }

                    if (temData["isActive"] != DBNull.Value)
                    {
                        hotelData.IsActive = Convert.ToBoolean(temData["isActive"]);
                    }

                    if (temData["createdBy"] != DBNull.Value)
                    {
                        hotelData.CreatedBy = Convert.ToInt32(temData["createdBy"]);
                    }

                    if (temData["createdOn"] != DBNull.Value)
                    {
                        hotelData.CreatedOn = Convert.ToDateTime(temData["createdOn"]);
                    }

                    if (temData["lastModifiedBy"] != DBNull.Value)
                    {
                        hotelData.LastModifiedBy = Convert.ToInt32(temData["lastModifiedBy"]);
                    }

                    if (temData["lastModifiedOn"] != DBNull.Value)
                    {
                        hotelData.LastModifiedOn = Convert.ToDateTime(temData["lastModifiedOn"]);
                    }

                    if (temData["hotelProfile"] != DBNull.Value)
                    {
                        hotelData.HotelProfile = Convert.ToString(temData["hotelProfile"]);
                    }

                    if (temData["inAndAround"] != DBNull.Value)
                    {
                        hotelData.InAndAround = Convert.ToString(temData["inAndAround"]);
                    }

                    if (temData["dealDescDocument"] != DBNull.Value)
                    {
                        hotelData.DealDesriptionDoc = Convert.ToString(temData["dealDescDocument"]);
                    }
                    if (temData["hotelImage"] != DBNull.Value)
                    {
                        string imgStr = Convert.ToString(temData["hotelImage"]);
                        string[] imgs = imgStr.Split('|');
                        List<string> images = new List<string>();
                        images.AddRange(imgs);
                        hotelData.HotelImage = images;
                    }
                    hotelData.DealId = selDealId;
                }

                temData.Close();
                connection.Close();
                //hotelData.dealMoreData = GetHotelDealData(hotelData.dealId);
                hotelData.dealMoreData = GetHotelDealData(selDealId);
            }
            catch (SqlException ex)
            {
                //connection.Close();
                throw new Exception("SQL Exception:" + ex.Message);
            }
            //Trace.TraceInformation("HotelDeal.GetHotelDealById exited : selDealId=" + selDealId);
            return hotelData;
        }

        #endregion

        /// <summary>
        /// Method to save a Hotel deal
        /// </summary>
        public void SaveHotelDeal()
        {
            //Trace.TraceInformation("HotelDeal.SaveHotelDeal entered ");
            Regex regex = new Regex(@"^[\+\*\?\.\^\$\(\)\{\}\|\\~!@#%\&\-_={}'>/\[\]]+$");
            Regex regex2 = new Regex(@"^[1-9]+$");

            string images = string.Empty;
            if (this.hotelImage != null && this.hotelImage.Count != 0)
            {
                for (int i = 0; i < this.hotelImage.Count; i++)
                {
                    // validate images
                    if (this.hotelImage[i] != null)
                    {
                        if (this.hotelImage[i].Length != 0)
                        {
                            if (this.hotelImage[i].IndexOf('.') <= 0)
                            {
                                throw new ArgumentException("Invalid Image URL");
                            }
                            string[] image = this.hotelImage[i].Split('.');
                            string fileExtension = image[image.Length - 1].ToLower();
                            bool flag = false;
                            for (int j = 0; j < fileExt.Length; j++)
                            {
                                if (fileExtension == fileExt[j])
                                {
                                    flag = true;
                                }
                            }
                            if (!flag)
                            {
                                throw new ArgumentException("Invalid File");
                            }
                        }
                    }

                    // adding in to a string
                    images += this.hotelImage[i];
                    if (i < this.hotelImage.Count - 1)
                    {
                        images += "|";
                    }
                }
            }

            if (this.dealTitle == null)
            {
                throw new ArgumentException("Title cannot be left blank");
            }
            else
            {
                if (this.dealTitle.Length == 0)
                { throw new ArgumentException("Title cannot be left blank"); }
            }

            if (this.description == null)
            {
                throw new ArgumentException("Description cannot be left blank as NULL");
            }
            else
            {
                if (this.description.Length == 0)
                { throw new ArgumentException("Description cannot be left blank"); }
            }

            if (this.hotelName == null)
            {
                throw new ArgumentException("Hotel Name cannot be left blank as NULL");
            }
            else
            {
                if (this.hotelName.Length == 0)
                { throw new ArgumentException("Hotel Name cannot be left blank"); }
            }

            if (this.hotelLocation == null)
            {
                throw new ArgumentException("Hotel Location cannot be left blank");
            }
            else
            {
                if (this.hotelLocation.Length == 0)
                { throw new ArgumentException("Hotel Location cannot be left blank"); }
            }

            if (regex.IsMatch(this.dealTitle))
            {
                throw new ArgumentException("Deal Name cannot have special characters only");
            }
            if (regex.IsMatch(this.hotelName))
            {
                throw new ArgumentException("Hotel Name cannot have special characters only");
            }
            if (regex.IsMatch(this.hotelLocation))
            {
                throw new ArgumentException("City Name cannot have special characters only");
            }
            if (regex.IsMatch(this.description))
            {
                throw new ArgumentException("Description cannot have special characters only");
            }

            if (regex2.IsMatch(this.dealTitle))
            {
                throw new ArgumentException("Deal Name cannot have numerals only");
            }
            if (regex2.IsMatch(this.hotelName))
            {
                throw new ArgumentException("Hotel Name cannot have numerals only");
            }
            if (regex2.IsMatch(this.hotelLocation))
            {
                throw new ArgumentException("City Name cannot have numerals only");
            }
            if (regex2.IsMatch(this.description))
            {
                throw new ArgumentException("Description cannot have numerals only");
            }

            if (dealId == 0)
            {
                try
                {
                    using (TransactionScope insertTransaction = new TransactionScope())
                    {
                        SqlParameter[] paramList = new SqlParameter[16];
                        paramList[0] = new SqlParameter("@dealtitle", this.dealTitle);
                        paramList[1] = new SqlParameter("@description", this.description);
                        paramList[2] = new SqlParameter("@hotelName", this.hotelName);
                        paramList[3] = new SqlParameter("@hotelRating", this.hotelRating);
                        paramList[4] = new SqlParameter("@hotelLocation", this.hotelLocation);
                        if (images != null && images.Length != 0)
                        {
                            paramList[5] = new SqlParameter("@hotelImage", images);
                        }
                        else
                        {
                            paramList[5] = new SqlParameter("@hotelImage", DBNull.Value);
                        }

                        paramList[6] = new SqlParameter("@includes", this.includes);
                        paramList[7] = new SqlParameter("@specialAttractions", this.specialAttractions);
                        //paramList[8] = new SqlParameter("@price", this.price);
                        paramList[8] = new SqlParameter("@isActive", this.isActive);
                        paramList[9] = new SqlParameter("@createdBy", this.createdBy);
                        paramList[10] = new SqlParameter("@lastModifiedBy", this.lastModifiedBy);
                        paramList[11] = new SqlParameter("@hotelProfile", this.hotelProfile);
                        paramList[12] = new SqlParameter("@dealDescDocument", this.dealDesriptionDoc);

                        paramList[13] = new SqlParameter("@dealId", 1);
                        paramList[13].Direction = System.Data.ParameterDirection.Output;
                        paramList[14] = new SqlParameter("@inAndAround", this.inAndAround);
                        paramList[15] = new SqlParameter("@isFeatured", this.isFeatured);
                        int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.SaveHotelDeal, paramList);

                        this.dealId = Convert.ToInt32(paramList[13].Value);
                        SaveHotelDealData(this.dealId, this.dealMoreData);
                        insertTransaction.Complete();
                    }
                }
                catch (Exception exp)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelDeal, CT.Core.Severity.High, 0, "Exception Saving Hotel Deal. Message: " + exp.Message, "");
                }
            }
            else
            {
                #region update code space
                try
                {
                    using (TransactionScope updateTransaction = new TransactionScope())
                    {
                        SqlParameter[] paramList = new SqlParameter[13];
                        paramList[0] = new SqlParameter("@dealtitle", this.dealTitle);
                        paramList[1] = new SqlParameter("@description", this.description);
                        paramList[2] = new SqlParameter("@hotelName", this.hotelName);
                        paramList[3] = new SqlParameter("@hotelRating", this.hotelRating);
                        paramList[4] = new SqlParameter("@hotelLocation", this.hotelLocation);

                        paramList[5] = new SqlParameter("@includes", this.includes);
                        paramList[6] = new SqlParameter("@specialAttractions", this.specialAttractions);
                        //paramList[8] = new SqlParameter("@price", this.price);
                        paramList[7] = new SqlParameter("@lastModifiedBy", this.lastModifiedBy);
                        paramList[8] = new SqlParameter("@hotelProfile", this.hotelProfile);
                        paramList[9] = new SqlParameter("@dealDescDocument", this.dealDesriptionDoc);

                        paramList[10] = new SqlParameter("@dealId", this.dealId);
                        paramList[11] = new SqlParameter("@inAndAround", this.inAndAround);
                        paramList[12] = new SqlParameter("@isFeatured", this.isFeatured);
                        int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHotelDeal, paramList);
                        DeleteHotelDealData(this.dealId);
                        SaveHotelDealData(this.dealId, this.dealMoreData);
                        updateTransaction.Complete();
                    }
                }
                catch (Exception exp)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.HotelDeal, CT.Core.Severity.High,0 , "Exception Updating Hotel Deal. Message: " + exp.Message, "");
                }
                #endregion
            }
            //Trace.TraceInformation("HotelDeal.Save exited.");
        }

        /// <summary>
        /// Method to activate a hotel deal when it's id is known.
        /// </summary>
        /// <param name="selDealId"></param>
        public void ActivateHotelDeal(int selDealId)
        {
            //Trace.TraceInformation("HotelDeal.ActivateHotelDeal entered : selDealId=" + selDealId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@dealId", selDealId);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.ActivateHotelDeal, paramList);
            //Trace.TraceInformation("AirlineDeal.Delete exited : selDealId=" + selDealId);

        }
        /// <summary>
        /// Method to Make Featured a hotel deal when it's id is known.
        /// </summary>
        /// <param name="selDealId"></param>
        public void ActivateFeaturedHotelDeal(int selDealId)
        {
            //Trace.TraceInformation("HotelDeal.ActivateFeaturedHotelDeal entered : selDealId=" + selDealId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@dealId", selDealId);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.ActivateFeaturedHotelDeal, paramList);
            //Trace.TraceInformation("HotelDeal.ActivateFeaturedHotelDeal exited : selDealId=" + selDealId);

        }
        /// <summary>
        /// Method for changing the status of Hotel Deal as Inactive  As featured by Deal Id.
        /// </summary>
        /// <param name="selDealId"></param>
        public void DeactivateFeaturedHotelDeal(int selDealId)
        {
            //Trace.TraceInformation("HotelDeal. DeactivateFeaturedHotelDeal entered : selDealId=" + selDealId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@dealId", selDealId);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.DeactivateFeaturedHotelDeal, paramList);
            //Trace.TraceInformation("HotelDeal. DeactivateFeaturedHotelDeal exited : selDealId=" + selDealId);
        }

        /// <summary>
        /// Method for changing the status of Hotel Deal as Inactive by Deal Id.
        /// </summary>
        /// <param name="selDealId"></param>
        public void DeactivateHotelDeal(int selDealId)
        {
            //Trace.TraceInformation("HotelDeal.DeactivateHotelDeal entered : selDealId=" + selDealId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@dealId", selDealId);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.DeactivateHotelDeal, paramList);
            //Trace.TraceInformation("HotelDeal.DeactivateHotelDeal exited : selDealId=" + selDealId);
        }

        public List<HotelDealData> GetHotelDealData(int dealId)
        {
            List<HotelDealData> dealdata = new List<HotelDealData>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@dealId", dealId);
            SqlDataReader tempData = DBGateway.ExecuteReaderSP(SPNames.GetHotelDealData, paramList, connection);
            while (tempData.Read())
            {
                HotelDealData hDeal = new HotelDealData();
                hDeal.ValidFrom = Convert.ToDateTime(tempData["validFrom"]);
                hDeal.ValidUpto = Convert.ToDateTime(tempData["validUpTo"]);
                hDeal.RoomType = Convert.ToString(tempData["roomType"]);
                hDeal.Price = Convert.ToDouble(tempData["price"]);
                dealdata.Add(hDeal);
            }

            tempData.Close();
            connection.Close();
            return dealdata;
        }

        private void SaveHotelDealData(int dealId, List<HotelDealData> hdata)
        {
            for (int i = 0; i < hdata.Count; i++)
            {
                if (dealId != 0)
                {
                    SqlParameter[] paramList = new SqlParameter[5];
                    paramList[0] = new SqlParameter("@dealId", dealId);
                    paramList[1] = new SqlParameter("@validFrom", hdata[i].ValidFrom);
                    paramList[2] = new SqlParameter("@validUpTo", hdata[i].ValidUpto);
                    paramList[3] = new SqlParameter("@roomType", hdata[i].RoomType);
                    paramList[4] = new SqlParameter("@price", hdata[i].Price);

                    int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.SaveHotelDealData, paramList);
                }
                else
                {
                    #region update code space
                    // Update here
                    #endregion
                }
            }
        }
        /// <summary>
        /// This Method is used to get the Hotel deals Details of a particular city.
        /// </summary>
        /// <param name="cityName"></param>
        /// <returns></returns>
        public List<HotelDeal> GetHotelDealByCity(string cityName)
        {
            List<HotelDeal> hotelData = new List<HotelDeal>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@cityName", cityName);
            SqlDataReader tempData = DBGateway.ExecuteReaderSP(SPNames.GetHotelDealDataByCity, paramList, connection);
            
            while (tempData.Read())
            {
                HotelDeal newHotel = new HotelDeal();
                newHotel.dealId = Convert.ToInt32(tempData["dealId"]);
                newHotel.isActive = Convert.ToBoolean(tempData["isActive"]);
                newHotel.dealTitle = Convert.ToString(tempData["dealTitle"]);
                newHotel.hotelName = Convert.ToString(tempData["hotelName"]);
                if (tempData["hotelImage"] != null)
                {
                    List<string> imgs = new List<string>();
                    string[] images = Convert.ToString(tempData["hotelImage"]).Split('|');
                    imgs.AddRange(images);
                    newHotel.hotelImage = imgs;
                }
                newHotel.hotelRating = Convert.ToInt32(tempData["hotelRating"]);
                newHotel.hotelLocation = Convert.ToString(tempData["hotelLocation"]);
                newHotel.description = Convert.ToString(tempData["description"]);
                newHotel.createdOn = Convert.ToDateTime(tempData["createdOn"]);
                newHotel.createdBy = Convert.ToInt32(tempData["createdBy"]);
                newHotel.includes = Convert.ToString(tempData["includes"]);
                newHotel.specialAttractions = Convert.ToString(tempData["specialAttractions"]);
                //newHotel.price = Convert.ToString(tempData["price"]);
                newHotel.lastModifiedBy = Convert.ToInt32(tempData["lastModifiedBy"]);
                newHotel.lastModifiedOn = Convert.ToDateTime(tempData["lastModifiedOn"]);
                newHotel.hotelProfile = Convert.ToString(tempData["hotelProfile"]);
                newHotel.inAndAround = Convert.ToString(tempData["inAndAround"]);
                newHotel.dealDesriptionDoc = Convert.ToString(tempData["dealDescDocument"]);
                newHotel.isFeatured = Convert.ToBoolean(tempData["isFeatured"]);
                //Get Deal More data
                List<HotelDealData> dealData = GetHotelDealData(newHotel.dealId);
                newHotel.dealMoreData = dealData;

                hotelData.Add(newHotel);

            }

            tempData.Close();
            connection.Close();
            return hotelData;
        }

        /// <summary>
        /// this Method is used to delete the DealData.
        /// </summary>
        /// <param name="dealId"></param>
        public void DeleteHotelDealData(int dealId)
        {
            //Trace.TraceInformation("HotelDeal.DeleteHotelDealData entered : DealId=" + dealId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@dealId", dealId);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.DeleteHotelDealData, paramList);
            //Trace.TraceInformation("HotelDeal.DeleteHotelDealData exited : DealId=" + dealId);
        }

        public  int GetManageDealCount(string whereString, string orderByString)
        {
            //Trace.TraceInformation("HotelDeal.GetManageDealCount entered whereString = " + whereString);
            DataSet dataset = new DataSet();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@whereString", whereString);
            paramList[1] = new SqlParameter("@orderByString", orderByString);
            dataset = DBGateway.FillSP(SPNames.GetManageDealCount, paramList);
            int queueCount = 0;
            if (dataset.Tables.Count != 0)
            {
                queueCount = Convert.ToInt32(dataset.Tables[0].Rows[0]["totCount"]);
            }
            dataset.Dispose();
            //Trace.TraceInformation("HotelDeal.GetManageDealCount Exited queueCount=" + queueCount);
            return queueCount;

        }
        #endregion
    }
}
