﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.TicketReceipt.Common;
namespace CT.CMS
{
    public class Testimonial
    {
        #region Variables
        private const int NEW_RECORD = -1;
        int id;
        int product;
        string description;
        string name;
        string title;
        int orderNo;
        string imagePath;
        string imageFileName;
        string status;
        long createdBy;
        int retTestimonialId;
        #endregion
        #region properities
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public int Product
        {
            get { return product; }
            set { product = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        public int OrderNo
        {
            get { return orderNo; }
            set { orderNo = value; }
        }
        public string ImagePath
        {
            get { return imagePath; }
            set { imagePath = value; }
        }
        public string ImageFileName
        {
            get { return imageFileName; }
            set { imageFileName = value; }
        }
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        public long CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public int RetTestimonialId
        {
            get { return retTestimonialId; }
            set { retTestimonialId = value; }
        }
        #endregion
        #region Constructors
        public Testimonial()
        {
            id = NEW_RECORD;
        }
        public Testimonial(long id)
        {
            getDetails(id);
        }
        #endregion

        #region privateMethods
        private void getDetails(long id)
        {
            DataTable dt = GetData(id);
            UpdateBusinessData(dt);
        }
        private DataTable GetData(long id)
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_ID", id);
                DataTable dtResult = DBGateway.ExecuteQuery("usp_Testimonial_GetData", paramList).Tables[0];
                return dtResult;
            }
            catch
            {
                throw;
            }
        }
        private void UpdateBusinessData(DataTable dt)
        {

            try
            {
                if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    if (dr != null)
                    {
                        id = Utility.ToInteger(dr["ID"]);
                        product = Utility.ToInteger(dr["PRODUCT"]);
                        name = Utility.ToString(dr["NAME"]);
                        title = Utility.ToString(dr["TITLE"]);
                        description = Utility.ToString(dr["DESCRIPTION"]);
                        imagePath = Utility.ToString(dr["IMG_PATH"]);
                        imageFileName = Utility.ToString(dr["IMG_FILENAME"]);
                        status = Utility.ToString(dr["STATUS"]);
                        orderNo = Utility.ToInteger(dr["ORDERNO"]);
                        createdBy = Utility.ToInteger(dr["CREATEDBY"]);
                    }
                }
            }
            catch
            { throw; }

        }
        #endregion
        #region public methods
        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[12];

                paramList[0] = new SqlParameter("@P_ID", id);
                paramList[1] = new SqlParameter("@P_PRODUCT", product);
                paramList[2] = new SqlParameter("@P_DESCRIPTION", description);
                paramList[3] = new SqlParameter("@P_NAME", name);
                paramList[4] = new SqlParameter("@P_TITLE", title);
                paramList[5] = new SqlParameter("@P_IMG_PATH", imagePath);
                paramList[6] = new SqlParameter("@P_IMG_FILENAME", imageFileName);
                paramList[7] = new SqlParameter("@P_STATUS", status);
                paramList[8] = new SqlParameter("@P_CREATEDBY", createdBy);
                paramList[9] = new SqlParameter("@P_MSG_TYPE", SqlDbType.VarChar, 10);
                paramList[9].Direction = ParameterDirection.Output;
                paramList[10] = new SqlParameter("@P_MSG_text", SqlDbType.VarChar, 200);
                paramList[10].Direction = ParameterDirection.Output;

                paramList[11] = new SqlParameter("@P_ID_RET", SqlDbType.Int, 200);
                paramList[11].Direction = ParameterDirection.Output;
                DBGateway.ExecuteNonQuery("usp_Testimonial_add_update", paramList);
                string messageType = Utility.ToString(paramList[9].Value);
                if (messageType == "E")
                {
                    string message = Utility.ToString(paramList[10].Value);
                    if (message != string.Empty) throw new Exception(message);
                }
                retTestimonialId = Utility.ToInteger(paramList[11].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetList()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[0];
                return DBGateway.ExecuteQuery("usp_Testimonial_GetList", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
