using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using CT.TicketReceipt.DataAccessLayer;
using System.Text.RegularExpressions;
using CT.BookingEngine;

namespace CT.CMS
{
    public class Advertisement
    {
        #region private Property
        /// <summary>
        /// Unique Id of the Advertisement
        /// </summary>
        private int advertisementId;
        /// <summary>
        /// Title of Ad
        /// </summary>
        private string title;
        /// <summary>
        /// Image Name
        /// </summary>
        private string imageFile;
        /// <summary>
        /// Desciption of Ad
        /// </summary>
        private string adDescription;
        /// <summary>
        /// Unique Id of a agency
        /// </summary>
        private int agencyId;
        /// <summary>
        /// Type of Ads
        /// </summary>
        private AdType adType;
        /// <summary>
        /// Add For
        /// </summary>
        private ProductType productType;      
        /// <summary>
        /// isActive to activate/ deactivate a Ad
        /// </summary>
        private bool isActive;
        /// <summary>
        /// Unique ID of the member who created this record
        /// </summary>
        private int createdBy;
        /// <summary>
        /// Date when the record was created
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// Unique ID of the member who modified the record last
        /// </summary>
        private int lastModifiedBy;
        /// <summary>
        /// Date when the record was last modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// Collection of AdBolcks
        /// </summary>
        private List<AdBlock> adBlocks;

    #endregion
        int flag = 0;
        string[] fileExt ={ "jpg", "jpeg", "jpe", "jfif", "bmp", "gif", "tiff", "tif", "png", "dib" };

        #region Public  Property
        /// <summary>
        /// Gets or sets Id of the Advertisement
        /// </summary>
        public int AdvertisementId
        {
            get
            {
                return advertisementId;
            }
            set
            {
                advertisementId = value;
            }
        }

        /// <summary>
        /// Title of the Advertisement
        /// </summary>
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }       

        /// <summary>
        ///FileName of the Image 
        /// </summary>
        public string ImageFile
        {
            get
            {
                return imageFile;
            }
            set
            {
                imageFile = value;
            }
        }

        /// <summary>
        ///Ad desciption
        /// </summary>
        public string AdDescription
        {
            get
            {
                return adDescription;
            }
            set
            {
                adDescription = value;
            }
        }
        /// <summary>
        /// Gets and Sets the AdType 
        /// </summary>
        public AdType AdType
        {
            get
            {
                return adType;
            }
            set
            {
                adType = value;
            }
        }
        /// <summary>
        /// Gets and Sets the productType 
        /// </summary>
        public ProductType ProductType
        {
            get
            {
                return productType;
            }
            set
            {
                productType = value;
            }
        }
        /// <summary>
        /// Gets and Sets the Agency 
        /// </summary>
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }        
        /// <summary>
        /// To Activate / Deactivate Ads 
        /// </summary>
        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }
                
        /// <summary>
        /// Gets the datetime when the record was created on.
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who created this record
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets the datetime when the record was last modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets the ID of member who modified this record last
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        /// <summary>
        /// List of AdBlocks
        /// </summary>
        public List<AdBlock> AdBlocks
        {
            get
            {
                return adBlocks;
            }
            set
            {
                adBlocks = value;
            }
        }

        #endregion

        /// <summary>
        /// Method to save the Advertisements
        /// </summary>
        /// <param name="title">Title of Advertisement</param>
        /// <param name="description">Description of Advertisement</param>
        /// <param name="imageFile">Filename for the image</param>
        public void Save()
        {
            //Trace.TraceInformation("Advertisement.Save entered ");
            int rowsAffected;
            Regex regex = new Regex(@"^[\+\*\?\.\^\$\(\)\{\}\|\\~!@#%\&\-_={}'>/\[\]]+$");
            title = title.Trim();            
            if (imageFile != null && imageFile.Trim().Length != 0)
            {
                if (imageFile.IndexOf('.') <= 0)
                {
                    throw new ArgumentException("Invalid Image URL");
                }
                string[] image = imageFile.Split('.');
                string fileExtension = image[image.Length - 1].ToLower();
                for (int i = 0; i < fileExt.Length; i++)
                {
                    if (fileExtension == fileExt[i])
                    {
                        flag = 1;
                        break;
                    }
                }
                if (flag == 0)
                {
                    throw new ArgumentException("Invalid File type");
                }
            }
            if (regex.IsMatch(title))
            {
                throw new ArgumentException("Title cannot have special characters only");
            }
            else if ((imageFile == null || imageFile.Length == 0) && (title == null || title.Length == 0))
            {
                throw new ArgumentException("Both text and imageFile can't be Blank");
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[8];
                paramList[0] = new SqlParameter("@advertisementId", advertisementId);
                if (title.Length == 0)
                {
                    throw new ArgumentException("Text can't be Blank");
                }
                else
                {
                    paramList[1] = new SqlParameter("@title", title);
                }
                if (imageFile.Length == 0)
                {
                    paramList[2] = new SqlParameter("@imageFile", DBNull.Value);
                }
                else
                {
                    paramList[2] = new SqlParameter("@imageFile", imageFile);
                }
                paramList[3] = new SqlParameter("@agencyId", agencyId);
                paramList[4] = new SqlParameter("@adDescription", adDescription);
                paramList[5] = new SqlParameter("@adTypeId", (int)adType);                
                if (advertisementId > 0)
                {
                    paramList[6] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                    paramList[7] = new SqlParameter("@productTypeId", (int)productType);
                    rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateAdvertisement, paramList);
                    UpdateEntriesofAdPlacement();
                }
                else
                {
                    paramList[6] = new SqlParameter("@createdBy", createdBy);
                    paramList[7] = new SqlParameter("@productTypeId", (int)productType);
                    paramList[0].Direction = ParameterDirection.Output;                    
                    rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.SaveAdvertisement, paramList);
                    advertisementId = Convert.ToInt32(paramList[0].Value);
                    AddEntryInAdPlacement();
                }
            }            
            //Trace.TraceInformation("Advertisement.Save exited : Rows Affected =" + rowsAffected);
        }
        /// <summary>
        /// Adds entry in Ad Placement
        /// </summary>
        private void AddEntryInAdPlacement()
        {
            //Trace.TraceInformation("PaymentDetail.AddEntryInPaymentInvoice entered : ");
            for(int i=0; i< adBlocks.Count;i++)
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@advertisementId", advertisementId);
                paramList[1] = new SqlParameter("@blockId", (int)adBlocks[i]);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.AddEntryInAdPlacement, paramList);
            }
            //Trace.TraceInformation("Advertisement.AddEntryInAdPlacement exiting ");
        }
        /// <summary>
        /// Updates entry in Ad Placement
        /// </summary>
        private void UpdateEntriesofAdPlacement()
        {
            //Trace.TraceInformation("Advertisement.UpdateEntriesofAdPlacement entered : ");
            DeleteEntriesofAdPlacement();
            AddEntryInAdPlacement();
            //Trace.TraceInformation("Advertisement.UpdateEntriesofAdPlacement exiting  ");
        }
        /// <summary>
        /// Delete entries of AdPlcement
        /// </summary>
        private void DeleteEntriesofAdPlacement()
        {
            //Trace.TraceInformation("Advertisement.DeleteEntriesofAdPlacement entered : ");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@advertisementId", advertisementId);
            int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.DeleteEntriesofAdPlacement, paramList);
            //Trace.TraceInformation("Advertisement.DeleteEntriesofAdPlacement exited : ");
        }
        /// <summary>
        /// Method to Load list of Advertisement for a agent
        /// </summary>
        /// <returns></returns>
        public List<Advertisement> GetAdsOfModule(int agencyId, AdModule adModule)
        {
            //Trace.TraceInformation("Advertisement.GetAdsOfModule entered : AgencyId = " + agencyId);
            List<Advertisement> tempList = new List<Advertisement>();
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader data;
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@adModuleId", (int)adModule);
            data = DBGateway.ExecuteReaderSP(SPNames.GetAdsOfModule, paramList, connection);
            if (data.HasRows)
            {
                tempList = GetAdList(data);
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("Advertisement.GetAdsOfModule exiting : ");
            return tempList;
        }                
        /// <summary>
        /// Method to Load list of Advertisement for a agent
        /// </summary>
        /// <returns></returns>
        public static List<Advertisement> GetAdsOfAgent(int agencyId, ProductType productType)
        {
            //Trace.TraceInformation("Advertisement.GetAdsOfAgent entered : AgencyId = " + agencyId);
            List<Advertisement> tempList = new List<Advertisement>();         
            
            SqlConnection connection = DBGateway.GetConnection();
            SqlDataReader data;
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            paramList[1] = new SqlParameter("@productTypeId", (int)productType);
            data = DBGateway.ExecuteReaderSP(SPNames.GetAdOfAgent, paramList, connection);
            if (data.HasRows)
            {
                tempList = GetAdList(data);                
            }
            data.Close();
            connection.Close();
            //Trace.TraceInformation("Advertisement.GetAdsOfAgent exiting : ");
            return tempList;
        }
        /// <summary>
        /// List of AdverTisement Object
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static List<Advertisement> GetAdList(SqlDataReader data)
        {
            List<Advertisement> tempList = new List<Advertisement>();
            if (data.HasRows)
            {
                while (data.Read())
                {
                    if (tempList.Count > 0)
                    {
                        bool duplicate = false;
                        foreach (Advertisement adv in tempList)
                        {
                            if (adv.advertisementId == Convert.ToInt32(data["advertisementId"]))
                            {
                                duplicate = true;
                            }
                            else
                            {
                                duplicate = false;
                            }
                            if (duplicate)
                            {
                                break;
                            }
                        }
                        if (duplicate)
                        {
                            continue;
                        }
                    }
                    Advertisement ad = new Advertisement();
                    ad.advertisementId = Convert.ToInt32(data["advertisementId"]);
                    ad.title = Convert.ToString(data["title"]);
                    if (data["imageFile"] == DBNull.Value)
                    {
                        ad.imageFile = "";
                    }
                    else
                    {
                        ad.imageFile = Convert.ToString(data["imageFile"]);
                    }
                    ad.adDescription = data["adDescription"].ToString();
                    ad.agencyId = Convert.ToInt32(data["agencyId"]);
                    ad.adType = (AdType)Enum.Parse(typeof(AdType), data["adTypeId"].ToString());
                    ad.productType = (ProductType)Enum.Parse(typeof(ProductType), data["productTypeId"].ToString());
                    ad.isActive = Convert.ToBoolean(data["isActive"]);
                    ad.createdOn = Convert.ToDateTime(data["createdOn"]);
                    ad.createdBy = Convert.ToInt32(data["createdBy"]);
                    ad.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                    ad.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    // Load the diffrent location of Ad on which it is added
                    ad.adBlocks = LoadAdBLock(Convert.ToInt32(data["advertisementId"]));                    
                    tempList.Add(ad);
                }
            }
            return (tempList);
        }
        /// <summary>
        /// Load Advertisement Object 
        /// </summary>
        /// <param name="advertisementId"></param>
        /// <returns></returns>
        public void Load(int advertisementId)
        {
            //Trace.TraceInformation("Advertisement.Load entered :advertisementId= " + advertisementId);
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@advertisementId", advertisementId);            
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetAdById, paramList, connection);
            
            if (!data.Read())
            {
                throw new ArgumentException("No Advertisement with this Id exists");
            }

            this.advertisementId = Convert.ToInt32(data["advertisementId"]);
            this.title = Convert.ToString(data["title"]);            
            if (data["imageFile"] == DBNull.Value)
            {
                this.imageFile = "";
            }
            else
            {
                this.imageFile = Convert.ToString(data["imageFile"]);
            }
            this.adDescription = data["adDescription"].ToString();
            this.agencyId = Convert.ToInt32(data["agencyId"]);
            this.adType = (AdType) Enum.Parse(typeof(AdType), data["adTypeId"].ToString());
            this.productType = (ProductType)Enum.Parse(typeof(ProductType), data["productTypeId"].ToString());
            this.isActive = Convert.ToBoolean(data["isActive"]);
            this.createdOn = Convert.ToDateTime(data["createdOn"]);
            this.createdBy = Convert.ToInt32(data["createdBy"]);
            this.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
            this.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);            
            // Load the diffrent location of Ad on which it is added
            this.adBlocks = LoadAdBLock(advertisementId);
            data.Close();
            connection.Close();
            //Trace.TraceInformation("Advertisement.GetAdById() Exited ");
        }
        /// <summary>
        /// Load method for AdPlacement where this Ad is placed
        /// </summary>
        /// <param name="advertisementId">advertisementId</param>
        private static List<AdBlock> LoadAdBLock(int advertisementId)
        {
            //Trace.TraceInformation("Advertisement.LoadAdBLock entered : ");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@advertisementId", advertisementId);
            //SqlConnection connection = DBGateway.GetConnection();
            //SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetLoadAdBLock, paramList, connection);
            List<AdBlock> adBlocks = new List<AdBlock>();
            using (DataTable dtLoad = DBGateway.FillDataTableSP(SPNames.GetLoadAdBLock, paramList))
            {
                if (dtLoad != null && dtLoad.Rows.Count > 0)
                {
                    foreach(DataRow data in dtLoad.Rows)
                    {
                        adBlocks.Add((AdBlock)Enum.Parse(typeof(AdBlock),Convert.ToString(data["blockId"])));
                    }
                }
            }
            //data.Close();
            //connection.Close();
            ////Trace.TraceInformation("Advertisement.LoadAdBLock exiting ");
            return adBlocks;
        }

        
        /// <summary>
        /// Gets a sorted list of all the role groups
        /// </summary>
        /// <returns>SortedList roleGroupId as key and roleGroup as value</returns>
        public static SortedList GetModuleBlock(int adModuleId, int agencyId, int advertisementId)
        {
            //Trace.TraceInformation("Advertisement.GetModuleBlock entered");
            SortedList moduleBlockList = new SortedList();
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@adModuleId", adModuleId);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            paramList[2] = new SqlParameter("@advertisementId", advertisementId);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetModuleBlock, paramList, connection);
            while (dataReader.Read())
            {
                moduleBlockList.Add((int)dataReader["blockId"], dataReader["ModuleBlock"].ToString());
            }
            dataReader.Close();
            connection.Close();
            moduleBlockList.TrimToSize();
            //Trace.TraceInformation("Advertisement.GetModuleBlock exiting : roleGroupList.Count = " + moduleBlockList.Count);
            return moduleBlockList;
        }
        /// <summary>
        /// Inactivates an Advertisement
        /// </summary>
        /// <param name="advertisementId">advertisementId of the agency to be deleted</param>
        public static int Delete(int advertisementId)
        {
            //Trace.TraceInformation("Advertisement.Delete entered : advertisementId = " + advertisementId);
            int rowsAffected;
            if (advertisementId > 0)
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@advertisementId", advertisementId);
                rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.DeactivateAd, paramList);
            }
            else
            {
                throw new ArgumentException("advertisementId should have positive integer value", "advertisementId");
            }
            //Trace.TraceInformation("Advertisement.Delete exiting : return = " + rowsAffected);
            return rowsAffected;
        }
        public static Array GetAdModulebyProductType(ProductType productType)
        {
            //Trace.TraceInformation("Advertisement.GetAdModulebyProductType entered");
            AdModule[] addModuleNames = new AdModule[0];// Array.CreateInstance(typeof(string), 0);
            SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@productTypeId", (int)productType);
            SqlDataReader dataReader = DBGateway.ExecuteReaderSP(SPNames.GetAdModulebyProductType, paramList, connection);
            int count=0;
            while (dataReader.Read())
            {
                Array.Resize<AdModule>(ref addModuleNames, addModuleNames.GetUpperBound(0) + 2);
                addModuleNames.SetValue(((AdModule)(int)dataReader["adModuleId"]), count);
                count++;
            }
            dataReader.Close();
            connection.Close();
            //Trace.TraceInformation("Advertisement.GetAdModulebyProductType exiting ");
            return (Array)addModuleNames;            
        }
    }

    /// <summary>
    /// Mode of Payment
    /// </summary>
    public enum AdModule
    {
        /// <summary>
        /// Search Module
        /// </summary>
        Search = 1,
        /// <summary>
        /// Search Result Module
        /// </summary>
        SearchResult = 2,
        /// <summary>
        /// Pax Detail Module
        /// </summary>
        PaxDetails = 3,
        /// <summary>
        /// Booking Confirmation
        /// </summary>
        Booking = 4,
        /// <summary>
        /// Booking Details Form
        /// </summary>
        BookingDeatils = 5,
        /// <summary>
        /// For customer registration page
        /// </summary>
        Register = 6,
        /// <summary>
        /// To Save Customer preference
        /// </summary>
        CustomerPreference = 7,
        /// <summary>
        /// ToUpdate customer setting
        /// </summary>
        UpdateCustomerProfile = 8,
        /// <summary>
        /// Page to show customer booking
        /// </summary>
        CustmerBookings = 9,
        /// <summary>
        /// Customer saved Tinerary List
        /// </summary>
        CustomerItinerary = 10,
        /// <summary>
        /// Master list of customer Passenger 
        /// </summary>
        CustomerPassengerList = 11,
        /// <summary>
        /// Hotel Search Module
        /// </summary>
        HotelSearch = 12,
        /// <summary>
        /// Hotel Search Result Module
        /// </summary>
        HotelSearchResult = 13,
        /// <summary>
        /// Hotel Pax Detail Module
        /// </summary>
        HotelPaxDetail = 14
    }
    /// <summary>
    /// Diffrent Type of Ad
    /// </summary>
    public enum AdType
    {
        /// <summary>
        /// Image type of Ad
        /// </summary>
        Image = 1,
        /// <summary>
        /// Text Type of Ad
        /// </summary>
        Text = 2
    }
    /// <summary>
    /// Diffrent Block on Diffrent Module
    /// </summary>
    public enum AdBlock
    {
        SearchLeftBarTop = 1,
        SearchLeftBarMiddle = 2,
        SearchLeftBarBottom = 3,
        SearchRightBarTop = 4,
        SearchRightBarMiddle = 5,
        SearchRightBarBottom = 6,
        SearchFooterLeft = 7,
        SearchFooterMiddle = 8,
        SearchFooterRight = 9,
        SearchResultLeftBarTop = 10,
        SearchResultLeftBarMiddle = 11,
        SearchResultLeftBarBottom = 12,
        SearchResultRightBarTop = 13,
        SearchResultRightBarMiddle = 14,
        SearchResultRightBarBottom = 15,
        SearchResultFooterLeft = 16,
        SearchResultFooterMiddle = 17,
        SearchResultFooterRight = 18,
        PaxDetailLeftBarTop = 19,
        PaxDetailLeftBarMiddle = 20,
        PaxDetailLeftBarBottom = 21,
        PaxDetailRightBarTop = 22,
        PaxDetailRightBarMiddle = 23,
        PaxDetailRightBarBottom = 24,
        PaxDetailFooterLeft = 25,
        PaxDetailFooterMiddle = 26,
        PaxDetailFooterRight = 27,
        BookingLeftBarTop = 28,
        BookingLeftBarMiddle = 29,
        BookingLeftBarBottom = 30,
        BookingRightBarTop = 31,
        BookingRightBarMiddle = 32,
        BookingRightBarBottom = 33,
        BookingFooterLeft = 34,
        BookingFooterMiddle = 35,
        BookingFooterRight = 36,
        BookingDetailLeftBarTop = 37,
        BookingDetailLeftBarMiddle = 38,
        BookingDetailLeftBarBottom = 39,
        BookingDetailRightBarTop = 40,
        BookingDetailRightBarMiddle = 41,
        BookingDetailRightBarBottom = 42,
        BookingDetailFooterLeft = 43,
        BookingDetailFooterMiddle = 44,
        BookingDetailFooterRight = 45,
        RegisterLeftBarTop = 46,
        RegisterLeftBarMiddle = 47,
        RegisterLeftBarBottom = 48,
        RegisterRightBarTop = 49,
        RegisterRightBarMiddle = 50,
        RegisterRightBarBottom = 51,
        RegisterFooterLeft = 52,
        RegisterFooterMiddle = 53,
        RegisterFooterRight = 54,
        CustomerPreferenceLeftBarTop = 55,
        CustomerPreferenceLeftBarMiddle = 56,
        CustomerPreferenceLeftBarBottom = 57,
        CustomerPreferenceRightBarTop = 58,
        CustomerPreferenceRightBarMiddle = 59,
        CustomerPreferenceRightBarBottom = 60,
        CustomerPreferenceFooterLeft = 61,
        CustomerPreferenceFooterMiddle = 62,
        CustomerPreferenceFooterRight = 63,
        UpdateCustomerProfileLeftBarTop = 64,
        UpdateCustomerProfileLeftBarMiddle = 65,
        UpdateCustomerProfileLeftBarBottom = 66,
        UpdateCustomerProfileRightBarTop = 67,
        UpdateCustomerProfileRightBarMiddle = 68,
        UpdateCustomerProfileRightBarBottom = 69,
        UpdateCustomerProfileFooterLeft = 70,
        UpdateCustomerProfileFooterMiddle = 71,
        UpdateCustomerProfileFooterRight = 72,
        CustmerBookingsLeftBarTop = 73,
        CustmerBookingsLeftBarMiddle = 74,
        CustmerBookingsLeftBarBottom = 75,
        CustmerBookingsRightBarTop = 76,
        CustmerBookingsRightBarMiddle = 77,
        CustmerBookingsRightBarBottom = 78,
        CustmerBookingsFooterLeft = 79,
        CustmerBookingsFooterMiddle = 80,
        CustmerBookingsFooterRight = 81,
        CustomerItineraryLeftBarTop = 82,
        CustomerItineraryLeftBarMiddle = 83,
        CustomerItineraryLeftBarBottom = 84,
        CustomerItineraryRightBarTop = 85,
        CustomerItineraryRightBarMiddle = 86,
        CustomerItineraryRightBarBottom = 87,
        CustomerItineraryFooterLeft = 88,
        CustomerItineraryFooterMiddle = 89,
        CustomerItineraryFooterRight = 90,
        CustomerPassengerListLeftBarTop = 91,
        CustomerPassengerListLeftBarMiddle = 92,
        CustomerPassengerListLeftBarBottom = 93,
        CustomerPassengerListRightBarTop = 94,
        CustomerPassengerListRightBarMiddle = 95,
        CustomerPassengerListRightBarBottom = 96,
        CustomerPassengerListFooterLeft = 97,
        CustomerPassengerListFooterMiddle = 98,
        CustomerPassengerListFooterRight = 99,
        HotelSearchLeftBarTop = 100,
        HotelSearchLeftBarMiddle = 101,
        HotelSearchLeftBarBottom = 102,
        HotelSearchRightBarTop = 103,
        HotelSearchRightBarMiddle = 104,
        HotelSearchRightBarBottom = 105,
        HotelSearchFooterLeft = 106,
        HotelSearchFooterMiddle = 107,
        HotelSearchFooterRight = 108,
        HotelSearchResultLeftBarTop = 109,
        HotelSearchResultLeftBarMiddle = 110,
        HotelSearchResultLeftBarBottom = 111,
        HotelSearchResultRightBarTop = 112,
        HotelSearchResultRightBarMiddle = 113,
        HotelSearchResultRightBarBottom = 114,
        HotelSearchResultFooterLeft = 115,
        HotelSearchResultFooterMiddle = 116,
        HotelSearchResultFooterRight = 117,
        HotelPaxDetailLeftBarTop = 118,
        HotelPaxDetailLeftBarMiddle = 119,
        HotelPaxDetailLeftBarBottom = 120,
        HotelPaxDetailRightBarTop = 121,
        HotelPaxDetailRightBarMiddle = 122,
        HotelPaxDetailRightBarBottom = 123,
        HotelPaxDetailFooterLeft = 124,
        HotelPaxDetailFooterMiddle = 125,
        HotelPaxDetailFooterRight = 126
    }
}
