﻿#region NameSpaceRegion
using System;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;
#endregion

namespace CT.CMS
{
    public class CMSHeader
    {
        #region MemberVariables

        /*Header Tab*/
        int _cmsId;
        string _countryCode;
        string _cityCode;
        string _copyRightText;
        string _phoneNo;
        string _logoPath;
        string _logoDescription;
        string _cultureCode;//Added by Harish on 20-Feb-2018(For Culture)

        /*Menu Settings Tab*/
        bool _showTopMenu;
        bool _showFooterMenu;
        bool _showSocialIcons;
        string _status;
        int _createdBy;
        #endregion

        #region Properties
        public int CmsId
        {
            get { return _cmsId; }
            set { _cmsId = value; }
        }
        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }
        public string CityCode
        {
            get { return _cityCode; }
            set { _cityCode = value; }
        }
        public string CopyRightText
        {
            get { return _copyRightText; }
            set { _copyRightText = value; }
        }
        public string PhoneNo
        {
            get { return _phoneNo; }
            set { _phoneNo = value; }
        }
        public string LogoPath
        {
            get { return _logoPath; }
            set { _logoPath = value; }
        }
        public string LogoDescription
        {
            get { return _logoDescription; }
            set { _logoDescription = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public string CultureCode
        {
            get { return _cultureCode; }
            set { _cultureCode = value; }
        }


        /*Menu Settings Tab*/
        public bool ShowTopMenu
        {
            get { return _showTopMenu; }
            set { _showTopMenu = value; }
        }
        public bool ShowFooterMenu
        {
            get { return _showFooterMenu; }
            set { _showFooterMenu = value; }
        }
        public bool ShowSocialIcons
        {
            get { return _showSocialIcons; }
            set { _showSocialIcons = value; }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public CMSHeader()
        {
            _cmsId = -1;
        }
        public CMSHeader(long id)
        {
            _cmsId = Convert.ToInt32(id);
            Load(_cmsId);
        }

        #endregion

        #region Methods
        //Load CMS DATA
        public void Load(int cmsId)
        {
            //SqlConnection connection = DBGateway.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@P_CMSId", cmsId);
            try
            {
                //SqlDataReader data = DBGateway.ExecuteReaderSP("usp_CMSGetData", paramList, connection);
                using (DataTable dtCms = DBGateway.FillDataTableSP("usp_CMSGetData", paramList))
                {
                    if (dtCms !=null && dtCms.Rows.Count > 0)
                    {
                        DataRow data = dtCms.Rows[0];
                        if (data["COUNTRYCODE"] != DBNull.Value)
                        {
                            _countryCode = Convert.ToString(data["COUNTRYCODE"]);
                        }
                        if (data["CITYCODE"] != DBNull.Value)
                        {
                            _cityCode = Convert.ToString(data["CITYCODE"]);
                        }
                        if (data["COPYRIGHTTEXT"] != DBNull.Value)
                        {
                            _copyRightText = Convert.ToString(data["COPYRIGHTTEXT"]);
                        }
                        if (data["PHONENO"] != DBNull.Value)
                        {
                            _phoneNo = Convert.ToString(data["PHONENO"]);
                        }
                        if (data["STATUS"] != DBNull.Value)
                        {
                            _status = Convert.ToString(data["STATUS"]);
                        }
                        if (data["CREATEDBY"] != DBNull.Value)
                        {
                            _createdBy = Convert.ToInt32(data["CREATEDBY"]);
                        }
                        if (data["LOGO_PATH"] != DBNull.Value)
                        {
                            _logoPath = Convert.ToString(data["LOGO_PATH"]);
                        }
                        if (data["LOGO_DESCRIPTION"] != DBNull.Value)
                        {
                            _logoDescription = Convert.ToString(data["LOGO_DESCRIPTION"]);
                        }
                        if (data["SHOW_TOP_MENU"] != DBNull.Value)
                        {
                            _showTopMenu = Convert.ToBoolean(data["SHOW_TOP_MENU"]);
                        }
                        if (data["SHOW_FOOTER_MENU"] != DBNull.Value)
                        {
                            _showFooterMenu = Convert.ToBoolean(data["SHOW_FOOTER_MENU"]);
                        }
                        if (data["SHOW_SOCIAL_ICONS"] != DBNull.Value)
                        {
                            _showSocialIcons = Convert.ToBoolean(data["SHOW_SOCIAL_ICONS"]);
                        }
                        if (data["CULTURECODE"] != DBNull.Value)
                        {
                            _cultureCode = Convert.ToString(data["CULTURECODE"]);
                        }
                        //data.Close();
                        //connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //connection.Close();
                throw ex;
            }
        }

        /// <summary>
        /// Saves the Header Details into the table CMS_HEADER
        /// </summary>
        /// 

        public int save()
        {
            int cmsId = 0;
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_CmsId", _cmsId);
                paramList[1] = new SqlParameter("@P_CountryCode", _countryCode);
                paramList[2] = new SqlParameter("@P_Status", _status);
                paramList[3] = new SqlParameter("@P_CreatedBy", _createdBy);
                paramList[4] = new SqlParameter("@P_CmsId_Ret", SqlDbType.Int);
                paramList[4].Direction = ParameterDirection.Output;
                paramList[5] = new SqlParameter("@P_CultureCode", _cultureCode);
                int count = DBGateway.ExecuteNonQuery("CMS_HEADER_DETAIL_ADD_UPDATE", paramList);
                if (paramList[4].Value != DBNull.Value)
                {
                    cmsId = Convert.ToInt32(paramList[4].Value);
                }
            }
            catch
            {

                throw;
            }
            return cmsId;
        }
        /// <summary>
        /// Update the Header Details into the table CMS_HEADER  against the CMS ID
        /// </summary>
        /// 
        public void SaveHeaderTabInfo()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[9];
                paramList[0] = new SqlParameter("@P_CmsId", _cmsId);
                paramList[1] = new SqlParameter("@P_PhoneNo", _phoneNo);
                paramList[2] = new SqlParameter("@P_CopyRightText", _copyRightText);
                paramList[3] = new SqlParameter("@P_LogoPath", _logoPath);
                paramList[4] = new SqlParameter("@P_LogoDesc", _logoDescription);
                paramList[5] = new SqlParameter("@P_CreatedBy", _createdBy);
                paramList[6] = new SqlParameter("@P_ShowFooterMenu", _showFooterMenu);
                paramList[7] = new SqlParameter("@P_ShowSocialMenu", _showSocialIcons);
                paramList[8] = new SqlParameter("@P_ShowTopMenu", _showTopMenu);
                int count = DBGateway.ExecuteNonQuery("CMS_HEADER_TAB_UPDATE", paramList);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// This method returns the previously saved CMS Records from the table "CMS_HEADER"
        /// </summary>
        /// <param name="recordStatus"></param>
        /// <returns></returns>
        public static DataTable GetAllCMSList(char recordStatus)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_RECORD_STATUS", recordStatus);
                return DBGateway.ExecuteQuery("usp_GetAllCMSList", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void UpdateRowStatus(int detailId, int modifiedBy, string status)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_DETAIL_ID", detailId);
                paramList[1] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                paramList[2] = new SqlParameter("@P_STATUS", status);
                DBGateway.ExecuteNonQuery("usp_Update_CMS_Header_Row_Status", paramList);
            }
            catch { throw; }
        }

        #endregion
    }

    public class CMSContentDetails
    {
        #region MemberVariables
        int _detailId;
        int _cmsId;

        /* HOME SETTINGS TAB DIFFERENT CONTENT TYPES:
         * FLIGHT =1, 
         * HOTEL =2,
         * HOLIDAYS =3,
         * VISA=12 ,
         * SOCIALMEDIA & TAGSMANAGER = 0
         */
        int _productType;
        string _contentTitle;
        string _contentName;
        string _contentDescription;
        string _contentValue;

        /*HOME SETTINGS TAB DIFFERENT CONTENT TYPES:
         *FLIGHT HOME TAB(1):HERO,PROMOMID,DEALSOFMONTH,DEALSOFMONTHSLIDER,VIDEO
         *HOTEL HOME TAB (2):HERO,PROMOMID
         *HOLIDAYS HOME TAB(3):HERO,VIDEO
         *VISA HOME TAB(12):HERO,PROMOMID    
         */

        /*SOCIAL MEDIA TAB DIFFERENT CONTENT TYPES:
         *(0):SOCIALFB
         *(0):SOCIALTW
         *(0):SOCIALINST
         *(0):SOCIALLIIN
         *(0):SOCIALYOU
         */

        /*TAGS MANAGER TAB DIFFERENT CONTENT TYPES: 
        *(0):GOOGLETAG
        *(0):FACEBOOKTAG
        *(0):TWITTERTAG
        *(0):OTHERTAG
        */

        string _contentType;
        string _contentFilePath;
        DateTime _effectiveDate;
        DateTime _expiryDate;
        string _contentURL;
        int _orderNo;
        string _status;
        int _createdBy;
        string _contentFileName;
        string _contentFileType;
        string _flex1;//Addedby Harish On 20-Feb-2018(for Flex Details)
        #endregion

        #region Properties
        public int DetailId
        {
            get { return _detailId; }
            set { _detailId = value; }
        }
        public int CmsId
        {
            get { return _cmsId; }
            set { _cmsId = value; }
        }
        public int ProductType
        {
            get { return _productType; }
            set { _productType = value; }
        }
        public string ContentTitle
        {
            get { return _contentTitle; }
            set { _contentTitle = value; }
        }
        public string ContentName
        {
            get { return _contentName; }
            set { _contentName = value; }
        }
        public string ContentDescription
        {
            get { return _contentDescription; }
            set { _contentDescription = value; }
        }
        public string ContentValue
        {
            get { return _contentValue; }
            set { _contentValue = value; }
        }
        public string ContentType
        {
            get { return _contentType; }
            set { _contentType = value; }
        }
        public string ContentFilePath
        {
            get { return _contentFilePath; }
            set { _contentFilePath = value; }
        }
        public DateTime EffectiveDate
        {
            get { return _effectiveDate; }
            set { _effectiveDate = value; }
        }
        public DateTime ExpiryDate
        {
            get { return _expiryDate; }
            set { _expiryDate = value; }
        }
        public string ContentURL
        {
            get { return _contentURL; }
            set { _contentURL = value; }
        }
        public int OrderNo
        {
            get { return _orderNo; }
            set { _orderNo = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public string ContentFileName
        {
            get { return _contentFileName; }
            set { _contentFileName = value; }
        }

        public string ContentFileType
        {
            get { return _contentFileType; }
            set { _contentFileType = value; }
        }
        public string Flex1
        {
            get { return _flex1; }
            set { _flex1 = value; }
        }
        #endregion

        #region Constructor
        public CMSContentDetails()
        {
        }
        #endregion

        #region Methods

        /// <summary>
        /// Saves the CMSContentDetails into the table CMS_CONTENT_DETAILS for the particular CMSID
        /// </summary>


        public int save()
        {
            int cmsdetailId = 0;
            try
            {
                SqlParameter[] paramList = new SqlParameter[19];
                paramList[0] = new SqlParameter("@P_DetailId", _detailId);
                paramList[1] = new SqlParameter("@P_CmsId", _cmsId);
                if (!string.IsNullOrEmpty(_contentTitle))
                {
                    paramList[2] = new SqlParameter("@P_ContentTitle", _contentTitle);
                }
                else
                {
                    paramList[2] = new SqlParameter("@P_ContentTitle", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(_contentName))
                {
                    paramList[3] = new SqlParameter("@P_ContentName", _contentName);
                }
                else
                {
                    paramList[3] = new SqlParameter("@P_ContentName", DBNull.Value);
                }

                if (!string.IsNullOrEmpty(_contentDescription))
                {
                    paramList[4] = new SqlParameter("@P_ContentDesc", _contentDescription);
                }
                else
                {
                    paramList[4] = new SqlParameter("@P_ContentDesc", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(_contentType))
                {
                    paramList[5] = new SqlParameter("@P_ContentType", _contentType);
                }
                else
                {
                    paramList[5] = new SqlParameter("@P_ContentType", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(_contentFilePath))
                {
                    paramList[6] = new SqlParameter("@P_ContentFilePath", _contentFilePath);
                }
                else
                {
                    paramList[6] = new SqlParameter("@P_ContentFilePath", DBNull.Value);
                }

                if (_effectiveDate != DateTime.MinValue)
                {
                    paramList[7] = new SqlParameter("@P_EffectiveDate", _effectiveDate);
                }
                else
                {
                    paramList[7] = new SqlParameter("@P_EffectiveDate", DBNull.Value);
                }

                if (_expiryDate != DateTime.MinValue)
                {
                    paramList[8] = new SqlParameter("@P_ExpiryDate", _expiryDate);
                }
                else
                {
                    paramList[8] = new SqlParameter("@P_ExpiryDate", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(_contentURL))
                {
                    paramList[9] = new SqlParameter("@P_ContentURL", _contentURL);
                }
                else
                {
                    paramList[9] = new SqlParameter("@P_ContentURL", DBNull.Value);
                }
                paramList[10] = new SqlParameter("@P_Status", _status);
                paramList[11] = new SqlParameter("@P_CreatedBy", _createdBy);
                if (_orderNo > 0)
                {

                    paramList[12] = new SqlParameter("@P_OrderNo", _orderNo);
                }
                else
                {
                    paramList[12] = new SqlParameter("@P_OrderNo", DBNull.Value);
                }
                paramList[13] = new SqlParameter("@P_ContentFileName", _contentFileName);
                paramList[14] = new SqlParameter("@P_ContentFileType", _contentFileType);

                paramList[15] = new SqlParameter("@P_Cms_Detail_Id_Ret", SqlDbType.Int);
                paramList[15].Direction = ParameterDirection.Output;
                paramList[16] = new SqlParameter("@P_ProductId", _productType);
                paramList[17] = new SqlParameter("@P_ContentValue", _contentValue);
                if (!string.IsNullOrEmpty(_flex1))
                { 
                paramList[18] = new SqlParameter("@P_Flex1", _flex1);
                }
                else
                {
                    paramList[18] = new SqlParameter("@P_Flex1", DBNull.Value);
                }
                int count = DBGateway.ExecuteNonQuery("CMS_CONTENT_DETAIL_ADD_UPDATE", paramList);
                if (count > 0 && paramList[15].Value != DBNull.Value)
                {
                    cmsdetailId = Convert.ToInt32(paramList[15].Value);
                }
            }
            catch
            {
                throw;
            }

            return cmsdetailId;

        }

        /// <summary>
        /// Update the CMS_CONTENT_DETAILS table row against the DetailId 
        /// </summary>
        /// <param name="detailId"></param>
        /// <param name="contentFilePath"></param>
        /// <param name="fileName"></param>
        /// <param name="fileType"></param>
        public void UpdateContentFileDetails(int detailId, string contentFilePath, string fileName, string fileType)
        {
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@P_DETAIL_ID", detailId);
            paramList[1] = new SqlParameter("@P_CONTENT_FILE_PATH", contentFilePath);
            paramList[2] = new SqlParameter("@P_CONTENT_FILE_NAME", fileName);
            paramList[3] = new SqlParameter("@P_CONTENT_FILE_TYPE", fileType);
            DBGateway.ExecuteNonQuery("usp_Update_CMS_Content_FilePath", paramList);
        }
        /// <summary>
        /// Returns the Corresponding Banner Images List Corresponding to the productType,Content Type and CMSID.
        /// </summary>
        /// <param name="cmsId"></param>
        /// <param name="contentType"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        public static DataTable GetBannersList(int cmsId, string contentType, int product)
        {

            /* Different Content Types: HERO,PROMOMID,DEALSOFMONTH,
             * DEALSOFMONTHSLIDER,VIDEO,SOCIALFB,SOCIALTW,
             * SOCIALINST,SOCIALLIIN,SOCIALYOU,GOOGLETAG,
             * FACEBOOKTAG,TWITTERTAG,OTHERTAG
             * 
              Product Id'S List:  FLIGHT =1, HOTEL =2,
             * HOLIDAYS =3, VISA=12 ,SOCIALMEDIA & TAGSMANAGER = 0 */

            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_CMS_ID", cmsId);
                paramList[1] = new SqlParameter("@P_CMS_REGION", contentType);
                paramList[2] = new SqlParameter("@P_PRODUCT_ID", product);
                return DBGateway.ExecuteQuery("usp_GetBannerDetails_By_Product_Type", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         /// <summary>
        /// Returns Only SOCIALMEDIA Content Type records from CMS_CONTENT_DETAILS against the CMSID
        /// Here the   product will be always '0'(ZERO)
         /// </summary>
         /// <param name="cmsId"></param>
         /// <param name="product"></param>
         /// <returns></returns>
        public static DataTable GetSocialMediaList(int cmsId, int product)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_CMS_ID", cmsId);
                paramList[2] = new SqlParameter("@P_PRODUCT_ID", product);
                return DBGateway.ExecuteQuery("usp_GetSocialMedia_List", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
          /// <summary>
        /// Returns Only TAGSMANAGER Content Type records from CMS_CONTENT_DETAILS against the CMSID
        /// Here the   product will be always '0'(ZERO)
          /// </summary>
          /// <param name="cmsId"></param>
          /// <param name="product"></param>
          /// <returns></returns>
        public static DataTable GetTagsList(int cmsId, int product)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_CMS_ID", cmsId);
                paramList[2] = new SqlParameter("@P_PRODUCT_ID", product);
                return DBGateway.ExecuteQuery("usp_GetTags_List", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         /// <summary>
        /// Update the row status in the table CMS_CONTENT_DETAILS against the detailId
         /// </summary>
         /// <param name="detailId"></param>
         /// <param name="modifiedBy"></param>
         /// <param name="status"></param>
        public static void DeleteContentDetail(int detailId, int modifiedBy, string status)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_DETAIL_ID", detailId);
                paramList[1] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                paramList[2] = new SqlParameter("@P_STATUS", status);
                DBGateway.ExecuteNonQuery("usp_Update_Content_Detail", paramList);
            }
            catch { throw; }
        }



        #endregion
    }

    public class CMSFooterLinks
    {

        #region Member Variables
        int _linkId;
        int _cmsId;
        /*Information,OurServices,CustomerCare,resources,BecomeAnAffiliate,Affliations*/
        string _linkType;
        string _linkName;
        string _linkURL;
        int _orderNo;
        string _status;
        DateTime _effectiveDate;
        DateTime _expiryDate;
        int _createdBy;
        #endregion

        #region Properties
        public int LinkId
        {
            get { return _linkId; }
            set { _linkId = value; }
        }
        public int CmsId
        {
            get { return _cmsId; }
            set { _cmsId = value; }
        }
        public string LinkType
        {
            get { return _linkType; }
            set { _linkType = value; }
        }
        public string LinkName
        {
            get { return _linkName; }
            set { _linkName = value; }
        }
        public string LinkURL
        {
            get { return _linkURL; }
            set { _linkURL = value; }
        }
        public int OrderNo
        {
            get { return _orderNo; }
            set { _orderNo = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public DateTime EffectiveDate
        {
            get { return _effectiveDate; }
            set { _effectiveDate = value; }
        }
        public DateTime ExpiryDate
        {
            get { return _expiryDate; }
            set { _expiryDate = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public CMSFooterLinks()
        {
        }
        #endregion

        #region Methods





        /// <summary>
        /// Returns all the footer links from the table CMS_FOOTER_LINKS for the particular CMSID 
        /// </summary>
        /// <param name="cmsId"></param>
        /// <returns></returns>
        public static DataTable GetAllLinksList(int cmsId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_CMS_ID", cmsId);
                return DBGateway.ExecuteQuery("usp_GetCMSLinks", paramList).Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Saves the footer links into the table CMS_FOOTER_LINKS for the particular CMSID
        /// </summary>
        public void save()
        {

            try
            {
                SqlParameter[] paramList = new SqlParameter[10];
                paramList[0] = new SqlParameter("@P_LinkId", _linkId);
                paramList[1] = new SqlParameter("@P_CmsId", _cmsId);
                if (!string.IsNullOrEmpty(_linkType))
                {
                    paramList[2] = new SqlParameter("@P_LinkType", _linkType);
                }
                else
                {
                    paramList[2] = new SqlParameter("@P_LinkType", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(_linkName))
                {
                    paramList[3] = new SqlParameter("@P_LinkName", _linkName);
                }
                else
                {
                    paramList[3] = new SqlParameter("@P_LinkName", DBNull.Value);
                }

                if (!string.IsNullOrEmpty(_linkURL))
                {
                    paramList[4] = new SqlParameter("@P_LinkURl", _linkURL);
                }
                else
                {
                    paramList[4] = new SqlParameter("@P_LinkURl", DBNull.Value);
                }

                if (_orderNo > 0)
                {
                    paramList[5] = new SqlParameter("@P_OrderNo", _orderNo);
                }
                else
                {
                    paramList[5] = new SqlParameter("@P_OrderNo", DBNull.Value);
                }
                if (!string.IsNullOrEmpty(_status))
                {
                    paramList[6] = new SqlParameter("@P_Status", _status);
                }
                else
                {
                    paramList[6] = new SqlParameter("@P_Status", DBNull.Value);
                }
                if (_effectiveDate != DateTime.MinValue)
                {
                    paramList[7] = new SqlParameter("@P_EffectiveDate", _effectiveDate);
                }
                else
                {
                    paramList[7] = new SqlParameter("@P_EffectiveDate", DBNull.Value);
                }

                if (_expiryDate != DateTime.MinValue)
                {
                    paramList[8] = new SqlParameter("@P_ExpiryDate", _expiryDate);
                }
                else
                {
                    paramList[8] = new SqlParameter("@P_ExpiryDate", DBNull.Value);
                }
                paramList[9] = new SqlParameter("@P_CreatedBy", _createdBy);
                int count = DBGateway.ExecuteNonQuery("CMS_FOOTER_LINKS_ADD_UPDATE", paramList);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        /// <summary>
        /// Update the row status in the table CMS_FOOTER_LINKS against the LINKID
        /// </summary>
        /// <param name="detailId"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="status"></param>
        public static void DeleteLinkDetail(int detailId, int modifiedBy, string status)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_LINK_ID", detailId);
                paramList[1] = new SqlParameter("@P_MODIFIED_BY", modifiedBy);
                paramList[2] = new SqlParameter("@P_STATUS", status);
                DBGateway.ExecuteNonQuery("usp_Update_Link_Detail", paramList);
            }
            catch { throw; }
        }
        #endregion
    }
}






