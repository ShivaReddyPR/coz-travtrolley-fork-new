﻿using System;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;

/// Summary description for Feedback
/// </summary>
namespace CT.CRM
{
    public class Feedback
    {
        public Feedback()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        public static DataTable GetFeedbackDetails(DateTime fromDate, DateTime toDate)
        {

            SqlDataReader data = null;
            DataTable dt = new DataTable();
            using (SqlConnection connection = DBGateway.GetConnection())
            {

                try
                {
                    SqlParameter[] paramList = new SqlParameter[2];
                    paramList[0] = new SqlParameter("@Fromdate", fromDate);
                    paramList[1] = new SqlParameter("@Todate", toDate);
                    data = DBGateway.ExecuteReaderSP(SPNames.GetFeedbackDetails, paramList, connection);
                    if (data != null)
                    {
                        dt.Load(data);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return dt;
        }

    }
}