namespace CT.CRM
{
    internal static class SPNames
    {
        public const string GetAgencyCustomersNameAndPhoneNo = "usp_GetAgencyCustomersNameAndPhoneNo";
        public const string LoadAgencyCustomerByCustomerId = "usp_LoadAgencyCustomerByCustomerId";
        public const string UpdateHotelDetailsOfAgencyCustomer = "usp_UpdateHotelDetailsOfAgencyCustomer";  //Procedure not made yet
        public const string UpdateFlightDetailsOfAgencyCustomer = "usp_UpdateFlightDetailsOfAgencyCustomer";        
        public const string UpdateTrainDetailsOfAgencyCustomer = "usp_UpdateTrainDetailsOfAgencyCustomer";
        public const string GetAgencyCustomerSchema = "usp_GetAgencyCustomerSchema";
        public const string SaveAgencyCustomer = "usp_SaveAgencyCustomer";
        public const string GetAgencyCustomersNameAndPhoneNoByName = "usp_GetAgencyCustomersNameAndPhoneNoByName";
        public const string GetFeedbackDetails = "usp_GetFeedbackDetails";       

        
    }
}
