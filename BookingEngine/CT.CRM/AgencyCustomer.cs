using System;
using System.Collections.Generic;
using CT.BookingEngine;
using System.Data.SqlClient;
using System.Data;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;

namespace CT.CRM
{
    public class AgencyCustomer
    {
        #region members
        private int customerId;
        private string firstName;
        private string lastName;
        private string title;
        private int agencyId;
        private Nullable<DateTime> dateOfBirth;
        private short age;
        private string address1;
        private string address2;
        private Gender genderOfCustomer;
        private string email;
        private string phoneNo;
        private string countryCode;
        private string countryPhoneCode;
        private string state;
        private string zipCode;
        private string city;
        private string areaPhoneCode;
        private string flightMealCode;
        private string flightSeatCode;
        private string passportNumber;
        private List<KeyValuePair<string, string>> ffKeyValuePair;
        private string nameInTrain;
        private TrainMealPreference trainMealCode;
        private TrainSeatPreference trainSeatCode;
        private DateTime createdOn;
        private DateTime lastModifiedOn;
        private int lastModifiedBy;
        private int createdBy;       


        #endregion

        #region properties
        public Gender GenderOfCustomer
        {
            get
            {
                return genderOfCustomer;
            }
            set
            {
                genderOfCustomer = value;
            }
        }

        public int CustomerId
        {
            get
            {
               return customerId;
            }
            set
            {
                customerId = value;
            }
        }
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }
        public string Address1
        {
            get
            {
                return address1;
            }
            set
            {
                address1 = value;
            }
        }
        public string Address2
        {
            get
            {
                return address2;
            }
            set
            {
                address2 = value;
            }
        }
        public Nullable<DateTime> DateOfBirth
        {
            get
            {
                return dateOfBirth;
            }
            set
            {
                dateOfBirth = value;
            }
        }
        public short Age
        {
            get
            {
                return age;
            }
            set
            {
                age = value;
            }
        }
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }
        public string PhoneNo
        {
            get
            {
                return phoneNo;
            }
            set
            {
                phoneNo = value;
            }
        }
        public string CountryCode
        {
            get
            {
                return countryCode;
            }
            set
            {
                countryCode = value;
            }
        }
        public string CountryPhoneCode
        {
            get
            {
                return countryPhoneCode;
            }
            set
            {
                countryPhoneCode = value;
            }
        }
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }
        public string ZipCode
        {
            get
            {
                return zipCode;
            }
            set
            {
                zipCode = value;
            }
        }
        public string AreaPhoneCode
        {
            get
            {
                return areaPhoneCode;
            }
            set
            {
                areaPhoneCode = value;
            }
        }
        public string FlightMealCode
        {
            get
            {
                return flightMealCode;
            }
            set
            {
                flightMealCode = value;
            }
        }
        public string FlightSeatCode
        {
            get
            {
                return flightSeatCode;
            }
            set
            {
                flightSeatCode = value;
            }
        }
        public string PassportNumber
        {
            get
            {
                return passportNumber;
            }
            set
            {
                passportNumber = value;
            }
        }
        public List<KeyValuePair<string,string>> FFKeyValuePair
        {
            get
            {
                return ffKeyValuePair;
            }
            set
            {
                ffKeyValuePair = value;
            }
        }
        public string NameInTrain
        {
            get
            {
                return nameInTrain;
            }
            set
            {
                nameInTrain = value;
            }
        }
        public TrainMealPreference TrainMealCode
        {
            get
            {
                return trainMealCode;
            }
            set
            {
                trainMealCode = value;
            }
        }
        public TrainSeatPreference TrainSeatCode
        {
            get
            {
                return trainSeatCode;
            }
            set
            {
                trainSeatCode = value;
            }
        }
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        #endregion

        #region Functions

        public AgencyCustomer()
        {
            ffKeyValuePair = new List<KeyValuePair<string, string>>();           
        }
        public bool Save(ref int customerId)
        {
            bool saveSuccessful;
            try
            {
                SqlParameter[] paramlist = new SqlParameter[26];
                paramlist[0] = new SqlParameter("@title", title);
                paramlist[1] = new SqlParameter("@firstName", firstName);
                paramlist[2] = new SqlParameter("@lastName", lastName);
                paramlist[3] = new SqlParameter("@agencyId", agencyId);
                paramlist[4] = new SqlParameter("@dateOfbirth", dateOfBirth);
                paramlist[5] = new SqlParameter("@flightMealCode", flightMealCode);
                paramlist[6] = new SqlParameter("@createdBy", createdBy);
                paramlist[7] = new SqlParameter("@flightSeatCode", flightSeatCode);
                paramlist[8] = new SqlParameter("@ffAirlineCodeAndffNumber", ConvertFFKeyValuePairToString(ffKeyValuePair));
                paramlist[9] = new SqlParameter("@passportNumber", passportNumber);
                paramlist[10] = new SqlParameter("@countryCode", countryCode);
                paramlist[11] = new SqlParameter("@phoneNo", phoneNo);
                paramlist[12] = new SqlParameter("@email", email);
                paramlist[13] = new SqlParameter("@address1", address1);
                paramlist[14] = new SqlParameter("@address2", address2);
                paramlist[15] = new SqlParameter("@nameInTrain", nameInTrain);
                paramlist[16] = new SqlParameter("@age", age);
                paramlist[17] = new SqlParameter("@gender", genderOfCustomer);
                paramlist[18] = new SqlParameter("@trainMealCode", trainMealCode);
                paramlist[19] = new SqlParameter("@trainSeatCode", trainSeatCode);
                paramlist[20] = new SqlParameter("@countryPhoneCode", countryPhoneCode);
                paramlist[21] = new SqlParameter("@areaPhoneCode", areaPhoneCode);
                paramlist[22] = new SqlParameter("@state", state);
                paramlist[23] = new SqlParameter("@city", city);
                paramlist[24] = new SqlParameter("@zipCode", zipCode);
                paramlist[25] = new SqlParameter("@customerId", SqlDbType.Int);
                paramlist[25].Direction = ParameterDirection.Output;
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.SaveAgencyCustomer, paramlist);
                customerId = (int)paramlist[25].Value;
                if (rowsAffected == 0)
                {
                    saveSuccessful = false;
                }
                else
                {
                    saveSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgencyCustomer, Severity.High, agencyId, "Not able to Save Agency Customer " + firstName + " " + lastName + " Due to Exception " + ex.StackTrace + " Message " + ex.Message,"");
                throw new Exception("Unable to Save the Agency Customer");
            }
            return saveSuccessful;
        }
        public static bool SaveList(List<AgencyCustomer> customersList)
        {
            bool saveListSuccessful;
            try
            {
                SqlConnection connection = DBGateway.GetConnection();
                SqlCommand command = new SqlCommand(SPNames.GetAgencyCustomerSchema, connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);

                foreach (AgencyCustomer customer in customersList)
                {
                    DataRow row = dt.NewRow();
                    row["address1"] = customer.address1;
                    row["address2"] = customer.address2;
                    row["age"] = customer.age;
                    row["agencyId"] = customer.agencyId;
                    row["areaPhoneCode"] = customer.areaPhoneCode;
                    row["city"] = customer.city;
                    row["countryCode"] = customer.countryCode;
                    row["countryPhoneCode"] = customer.countryPhoneCode;
                    row["createdBy"] = customer.createdBy;
                    row["createdOn"] = DateTime.UtcNow;
                    row["dateOfBirth"] = customer.dateOfBirth;
                    row["email"] = customer.email;
                    row["ffAirlineCodeAndffNumber"] = ConvertFFKeyValuePairToString(customer.ffKeyValuePair);
                    row["firstName"] = customer.firstName;
                    row["flightMealCode"] = customer.flightMealCode;
                    row["flightSeatCode"] = customer.flightSeatCode;
                    row["gender"] = customer.genderOfCustomer;
                    row["lastModifiedBy"] = customer.lastModifiedBy;
                    row["lastModifiedOn"] = DateTime.UtcNow;
                    row["lastName"] = customer.lastName;
                    row["nameInTrain"] = customer.nameInTrain;
                    row["passportNumber"] = customer.passportNumber;
                    row["phoneNo"] = customer.phoneNo;
                    row["state"] = customer.state;
                    row["title"] = customer.title;
                    row["trainMealCode"] = customer.trainMealCode;
                    row["trainSeatCode"] = customer.trainSeatCode;
                    row["zipCode"] = customer.zipCode;
                    dt.Rows.Add(row);
                }
                SqlCommandBuilder cmd = new SqlCommandBuilder(adapter);
                int rowsAffected = adapter.Update(dt);
                if (rowsAffected == customersList.Count)
                {
                    saveListSuccessful = true;
                }
                else
                {
                    saveListSuccessful = false;
                }
                connection.Close();
                adapter.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to store List of Agency Customers Exception :"+ex.Message);
            }
            return saveListSuccessful;
        }

        public void Update()
        {
            
        }

        public bool UpdateTrainDetails()
        {
            bool updateSuccessful;
            try
            {
                SqlParameter[] paramlist = new SqlParameter[7];
                paramlist[0] = new SqlParameter("@nameInTrain", nameInTrain);
                paramlist[1] = new SqlParameter("@age", age);
                paramlist[2] = new SqlParameter("@gender", genderOfCustomer);
                paramlist[3] = new SqlParameter("@trainMealCode", trainMealCode);
                paramlist[4] = new SqlParameter("@trainSeatCode", trainSeatCode);
                if (customerId <= 0)
                {
                    throw new ArgumentException("CustomerId should be greater then zero");
                }
                paramlist[5] = new SqlParameter("@customerId", customerId);
                paramlist[6] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                paramlist[7] = new SqlParameter("@phoneNo", phoneNo);
                paramlist[8] = new SqlParameter("@email", email);
                paramlist[9] = new SqlParameter("@address1", address1);
                paramlist[10] = new SqlParameter("@address2", address2);
                
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateTrainDetailsOfAgencyCustomer, paramlist);
                if (rowsAffected == 0)
                {
                    updateSuccessful = false;
                }
                else
                {
                    updateSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgencyCustomer, Severity.High, agencyId, "Not able to Update Agency Customer details of Lead Pax" + firstName + " " + lastName + " Due to Exception " + ex.StackTrace + " Message " + ex.Message,"");
                throw new Exception("Unable to update Customer Hotel Details ");
            }
            return updateSuccessful;
        }
        
        public bool UpdateFlightDetails()
        {
            bool updateSuccessful;
            try
            {
                SqlParameter[] paramlist = new SqlParameter[18];
                paramlist[0] = new SqlParameter("@title", title);
                paramlist[1] = new SqlParameter("@firstName", firstName);
                paramlist[2] = new SqlParameter("@lastName", lastName);
                if (customerId <= 0)
                {
                    throw new ArgumentException("CustomerId should be greater then zero");
                }
                paramlist[3] = new SqlParameter("@customerId", customerId);
                paramlist[4] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                paramlist[5] = new SqlParameter("@dateOfbirth", dateOfBirth);
                paramlist[6] = new SqlParameter("@flightMealCode", flightMealCode);
                paramlist[7] = new SqlParameter("@flightSeatCode", flightSeatCode);
                paramlist[8] = new SqlParameter("@ffAirlineCodeAndffNumber", ConvertFFKeyValuePairToString(ffKeyValuePair));
                paramlist[9] = new SqlParameter("@passportNumber", passportNumber);
                paramlist[10] = new SqlParameter("@countryCode", countryCode);
                paramlist[11] = new SqlParameter("@city", City);
                paramlist[12] = new SqlParameter("@gender", genderOfCustomer);
                paramlist[13] = new SqlParameter("@pinCode", ZipCode);
                paramlist[14] = new SqlParameter("@phoneNo", phoneNo);
                paramlist[15] = new SqlParameter("@email", email);
                paramlist[16] = new SqlParameter("@address1", address1);
                paramlist[17] = new SqlParameter("@address2", address2);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateFlightDetailsOfAgencyCustomer, paramlist);
                if (rowsAffected == 0)
                {
                    updateSuccessful = false;
                }
                else
                {
                    updateSuccessful = true;
                }      
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgencyCustomer, Severity.High, agencyId, "Not able to Update Agency Customer Flight details of Lead Pax" + firstName + " " + lastName + " Due to Exception " + ex.StackTrace + " Message " + ex.Message, "");
                throw new Exception("Unable to update Customer Details");
            }
            return updateSuccessful;
        }
        
        public bool UpdateHotelDetails()
        {
            bool updateSuccessful;
            try
            {
                SqlParameter[] paramlist = new SqlParameter[15];
                paramlist[0] = new SqlParameter("@title", title);
                paramlist[1] = new SqlParameter("@firstName", firstName);
                paramlist[2] = new SqlParameter("@lastName", lastName);
                if (customerId <= 0)
                {
                    throw new ArgumentException("CustomerId should be greater then zero");
                }
                paramlist[3] = new SqlParameter("@customerId", customerId);
                paramlist[4] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                paramlist[5] = new SqlParameter("@email", email);
                paramlist[6] = new SqlParameter("@address1", address1);
                paramlist[7] = new SqlParameter("@address2", address2);
                paramlist[8] = new SqlParameter("@countryCode", countryCode);
                paramlist[9] = new SqlParameter("@countryPhoneCode", countryPhoneCode);
                paramlist[10] = new SqlParameter("@areaPhoneCode", areaPhoneCode);
                paramlist[11] = new SqlParameter("@state", state);
                paramlist[12] = new SqlParameter("@city", city);
                paramlist[13] = new SqlParameter("@zipCode", zipCode);
                paramlist[14] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                int rowsAffected = DBGateway.ExecuteNonQuerySP(SPNames.UpdateHotelDetailsOfAgencyCustomer, paramlist);
                if (rowsAffected == 0)
                {
                    updateSuccessful = false;
                }
                else
                {
                    updateSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to update Customer Details",ex);
            }
            return updateSuccessful;

        }
        
        public bool LoadByCustomerIdAndAgencyId()
        {
            bool loadSuccessful;
            using (SqlConnection conn = DBGateway.GetConnection())
            {
                try
                {
                    SqlDataReader dataReader ;
                    SqlParameter[] paramList = new SqlParameter[2];
                    paramList[0] = new SqlParameter("@customerId", customerId);
                    paramList[1] = new SqlParameter("@agencyId", agencyId);
                    dataReader = DBGateway.ExecuteReaderSP(SPNames.LoadAgencyCustomerByCustomerId, paramList, conn);
                    if (dataReader.HasRows)
                    {
                        dataReader.Read();
                        agencyId = Convert.ToInt32(dataReader["agencyId"]);
                        createdBy = Convert.ToInt32(dataReader["createdBy"]);
                        createdOn = Convert.ToDateTime(dataReader["createdOn"]);
                        customerId = Convert.ToInt32(dataReader["customerId"]);
                        firstName = Convert.ToString(dataReader["firstName"]);
                        lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
                        lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
                        phoneNo = Convert.ToString(dataReader["phoneNo"]);
                        
                        if (dataReader["age"] != DBNull.Value)
                        {
                            age = Convert.ToInt16(dataReader["age"]);
                        }
                        if (dataReader["address1"] != DBNull.Value)
                        {
                            address1 = Convert.ToString(dataReader["address1"]);
                        }
                        if (dataReader["address2"] != DBNull.Value)
                        {
                            address2 = Convert.ToString(dataReader["address2"]);
                        }
                        if (dataReader["city"] != DBNull.Value)
                        {
                            city = Convert.ToString(dataReader["city"]);
                        }
                        if (dataReader["countryCode"] != DBNull.Value)
                        {
                            countryCode = Convert.ToString(dataReader["countryCode"]);
                        }
                        if (dataReader["countryPhoneCode"] != DBNull.Value)
                        {
                            countryPhoneCode = Convert.ToString(dataReader["countryPhoneCode"]);
                        }
                        if (dataReader["dateOfBirth"] != DBNull.Value)
                        {
                            dateOfBirth = Convert.ToDateTime(dataReader["dateOfBirth"]);
                        }
                        if (dataReader["email"] != DBNull.Value)
                        {
                            email = Convert.ToString(dataReader["email"]);
                        }
                        if (dataReader["ffAirlineCodeAndffNumber"] != DBNull.Value)
                        {
                            ffKeyValuePair = ConvertFFstringToKeyValuePair(Convert.ToString(dataReader["ffAirlineCodeAndffNumber"]));
                        }

                        if (dataReader["flightMealCode"] != DBNull.Value)
                        {
                            flightMealCode = Convert.ToString(dataReader["flightMealCode"]);
                        }
                        if (dataReader["flightSeatCode"] != DBNull.Value)
                        {
                            flightSeatCode = Convert.ToString(dataReader["flightSeatCode"]);
                        }
                        if (dataReader["gender"] != DBNull.Value)
                        {
                            genderOfCustomer = (Gender)Enum.Parse(typeof(Gender), Convert.ToString(dataReader["gender"]));
                        }

                        if (dataReader["lastName"] != DBNull.Value)
                        {
                            lastName = Convert.ToString(dataReader["lastName"]);
                        }
                        if (dataReader["nameInTrain"] != DBNull.Value)
                        {
                            nameInTrain = Convert.ToString(dataReader["nameInTrain"]);
                        }
                        if (dataReader["passportNumber"] != DBNull.Value)
                        {
                            passportNumber = Convert.ToString(dataReader["passportNumber"]);
                        }
                        if (dataReader["state"] != DBNull.Value)
                        {
                            state = Convert.ToString(dataReader["state"]);
                        }
                        if (dataReader["title"] != DBNull.Value)
                        {
                            title = Convert.ToString(dataReader["title"]);
                        }
                        if (dataReader["trainMealCode"] != DBNull.Value)
                        {
                            trainMealCode = (TrainMealPreference)Enum.Parse(typeof(TrainMealPreference), Convert.ToString(dataReader["trainMealCode"]));
                        }
                        if (dataReader["trainSeatCode"] != DBNull.Value)
                        {
                            trainSeatCode = (TrainSeatPreference)Enum.Parse(typeof(TrainSeatPreference), Convert.ToString(dataReader["trainSeatCode"]));
                        }
                        if (dataReader["zipCode"] != DBNull.Value)
                        {
                            zipCode = Convert.ToString(dataReader["zipCode"]);
                        }
                        dataReader.Dispose();
                        dataReader.Close();
                        loadSuccessful = true;
                    }
                    else
                    {
                        loadSuccessful = false;
                    }                                      
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.AgencyCustomer, Severity.High, agencyId, "Not able to Load Agency Customer of AgencyId " + agencyId + " and CustomerId " + customerId + " Due to Exception " + ex.StackTrace + " Message " + ex.Message, "");
                    throw new Exception("Unable to load Agency Customer");
                }               
            }
            return loadSuccessful;
        }

        public static DataView GetCustomersNameAndPhoneNoForAgency(int agencyId, string searchBy, string searchText)
        {
           DataSet dataSet =new DataSet();
            try
           {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@searchText", searchText);
                paramList[1] = new SqlParameter("@agencyId", agencyId);
                dataSet = DBGateway.FillSP(SPNames.GetAgencyCustomersNameAndPhoneNoByName, paramList);                
           }
           catch (Exception ex)
           {
                Audit.Add(EventType.AgencyCustomer, Severity.High, agencyId, "Not able to Get Customers Name and Phone No while searching for agencyId +" + agencyId + " Search Text was " + searchText + " .Due to Exception " + ex.StackTrace + " Message " + ex.Message, "");
           }
           return new DataView(dataSet.Tables[0]);          
        }

        public static List<AgencyCustomer> GetCustomersForAgency(int agencyId,string searchBy,string searchText,int fromRecordNo,int noOfRecords)
        {
            List<AgencyCustomer> customerList = new List<AgencyCustomer>();
            return customerList;
        }

        public static int GetNoOfCustomersForAgency(int agencyId)
        {
            return 0;
        }        

        public static DataTable GetAllCustomersNameAndPhoneNo()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[0];
                DataTable customerList = DBGateway.FillDataTableSP(SPNames.GetAgencyCustomersNameAndPhoneNo, paramList);
                return customerList;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgencyCustomer, Severity.High, 0, "Not able to Get Customers Name and Phone No for Generating the Trie object .Due to Exception " + ex.StackTrace + " Message " + ex.Message, "");
                throw new Exception("Not able to get Customer details for AutoComlete");
            }
        }       

        public static List<KeyValuePair<string, string>> ConvertFFstringToKeyValuePair(string ffString)
        {
            List<KeyValuePair<string, string>> ffKeyValuePair = new List<KeyValuePair<string, string>>();
            string[] ffArray = ffString.Split('|', '|');
            string[] oneFFCodeArray ;
            foreach (string oneFFCode in ffArray)
            {
                oneFFCodeArray = oneFFCode.Split(':');
                if (oneFFCodeArray.Length == 2)
                {
                    ffKeyValuePair.Add(new KeyValuePair<string, string>(oneFFCodeArray[0], oneFFCodeArray[1]));
                }
            }
            return ffKeyValuePair;
        }
        public static string ConvertFFKeyValuePairToString(List<KeyValuePair<string, string>> ffKeyValuePair)
        {
            string ffString="";
            foreach (KeyValuePair<string, string> ffCode in ffKeyValuePair)
            {
                ffString += ffCode.Key + ":" + ffCode.Value + "||";
            }
            ffString.Trim('|','|');
            return ffString;
        }
        public static explicit operator AgencyCustomer(SqlDataReader dataReader)
        {
            AgencyCustomer agencyCustomer = new AgencyCustomer();
            try
            {

                agencyCustomer.agencyId = Convert.ToInt32(dataReader["agencyId"]);
                agencyCustomer.createdBy = Convert.ToInt32(dataReader["createdBy"]);
                agencyCustomer.createdOn = Convert.ToDateTime(dataReader["createdOn"]);
                agencyCustomer.customerId = Convert.ToInt32(dataReader["customerId"]);
                agencyCustomer.firstName = Convert.ToString(dataReader["firstName"]);
                agencyCustomer.lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
                agencyCustomer.lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);
                agencyCustomer.phoneNo = Convert.ToString(dataReader["phoneNo"]);
                
                if (dataReader["age"] != DBNull.Value)
                {
                    agencyCustomer.age = Convert.ToInt16(dataReader["age"]);
                }
                if (dataReader["address1"] != DBNull.Value)
                {
                    agencyCustomer.address1 = Convert.ToString(dataReader["address1"]);
                }
                if (dataReader["address2"] != DBNull.Value)
                {
                    agencyCustomer.address2 = Convert.ToString(dataReader["address2"]);
                }
                if (dataReader["city"] != DBNull.Value)
                {
                    agencyCustomer.city = Convert.ToString(dataReader["city"]);
                }
                if (dataReader["countryCode"] != DBNull.Value)
                {
                    agencyCustomer.countryCode = Convert.ToString(dataReader["countryCode"]);
                }
                if (dataReader["countryPhoneCode"] != DBNull.Value)
                {
                    agencyCustomer.countryPhoneCode = Convert.ToString(dataReader["countryPhoneCode"]);
                }
                if (dataReader["dateOfBirth"] != DBNull.Value)
                {
                    agencyCustomer.dateOfBirth = Convert.ToDateTime(dataReader["dateOfBirth"]);
                }
                if (dataReader["email"] != DBNull.Value)
                {
                    agencyCustomer.email = Convert.ToString(dataReader["email"]);
                }
                if (dataReader["ffAirlineCodeAndffNumber"] != DBNull.Value)
                {
                    agencyCustomer.ffKeyValuePair = ConvertFFstringToKeyValuePair(Convert.ToString(dataReader["ffAirlineCodeAndffNumber"]));
                }

                if (dataReader["flightMealCode"] != DBNull.Value)
                {
                    agencyCustomer.flightMealCode = Convert.ToString(dataReader["flightMealCode"]);
                }
                if (dataReader["flightSeatCode"] != DBNull.Value)
                {
                    agencyCustomer.flightSeatCode = Convert.ToString(dataReader["flightSeatCode"]);
                }
                if (dataReader["gender"] != DBNull.Value)
                {
                    agencyCustomer.genderOfCustomer = (Gender)Enum.Parse(typeof(Gender), Convert.ToString(dataReader["gender"]));
                }

                if (dataReader["lastName"] != DBNull.Value)
                {
                    agencyCustomer.lastName = Convert.ToString(dataReader["lastName"]);
                }
                if (dataReader["nameInTrain"] != DBNull.Value)
                {
                    agencyCustomer.nameInTrain = Convert.ToString(dataReader["nameInTrain"]);
                }
                if (dataReader["passportNumber"] != DBNull.Value)
                {
                    agencyCustomer.passportNumber = Convert.ToString(dataReader["passportNumber"]);
                }
                if (dataReader["state"] != DBNull.Value)
                {
                    agencyCustomer.state = Convert.ToString(dataReader["state"]);
                }
                if (dataReader["title"] != DBNull.Value)
                {
                    agencyCustomer.title = Convert.ToString(dataReader["title"]);
                }
                if (dataReader["trainMealCode"] != DBNull.Value)
                {
                    agencyCustomer.trainMealCode = (TrainMealPreference)Enum.Parse(typeof(TrainMealPreference), Convert.ToString(dataReader["trainMealCode"]));
                }
                if (dataReader["trainSeatCode"] != DBNull.Value)
                {
                    agencyCustomer.trainSeatCode = (TrainSeatPreference)Enum.Parse(typeof(TrainSeatPreference), Convert.ToString(dataReader["trainSeatCode"]));
                }
                if (dataReader["zipCode"] != DBNull.Value)
                {
                    agencyCustomer.zipCode = Convert.ToString(dataReader["zipCode"]);
                }                    
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AgencyCustomer, Severity.High,agencyCustomer.agencyId, "Not able to change dataReader object to Agencycustomer object.Due to Exception " + ex.StackTrace + " Message " + ex.Message, "");
                throw new Exception("Unable to extract data from Database");
            }
            return agencyCustomer;
        }
        
        #endregion
    }
}
