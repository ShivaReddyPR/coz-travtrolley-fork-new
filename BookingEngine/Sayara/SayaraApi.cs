﻿using System;
using System.Collections.Generic;
using CT.Configuration;
using Sayara.SayaraRestApi;
using CT.BookingEngine;
using System.Xml;
using System.IO;
using CT.Core;

namespace Sayara
{
    public class SayaraApi:IDisposable
    {
        protected string XmlPath = string.Empty;
        string loginName = string.Empty;
        string password = string.Empty;
        string currency = string.Empty;

        decimal rateOfExchange = 1;
        string agentCurrency = "";
        Dictionary<string, decimal> exchangeRates;
        int decimalPoint;
        string sessionId;
        int appUserId;
        CTSCRWebService sayaraService = new CTSCRWebService();
        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }

        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }

        }
        public SayaraApi()
        {
            loginName = ConfigurationSystem.SAYARAConfig["LoginName"]; ;
            password = ConfigurationSystem.SAYARAConfig["Password"];
            XmlPath = ConfigurationSystem.SAYARAConfig["XmlLogPath"];
            currency = ConfigurationSystem.SAYARAConfig["Currency"];

            //Folder existing or not checking
            try
            {
                if (!System.IO.Directory.Exists(XmlPath))
                {
                    System.IO.Directory.CreateDirectory(XmlPath);
                }
            }
            catch { }

            //Authntication checking
            sayaraService = new CTSCRWebService();
            sayaraService.Url = ConfigurationSystem.SAYARAConfig["url"];
            ServiceAuthentication authentication = new ServiceAuthentication();
            authentication.UserName = loginName;
            authentication.Password = password;
            sayaraService.ServiceAuthenticationValue = authentication;
        }

        #region
        public List<FleetCity> GetCityList(ref List<FleetLocation> locationList)
        {
            List<FleetCity> cityList = new List<FleetCity>();
            try
            {
                //Creating request Object
                WSCityRequest request = new WSCityRequest();
                request.CountryCode = "AE";
                try
                {
                    System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSCityRequest));
                    FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CityListRequest.xml", FileMode.Create, FileAccess.Write);
                    XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                    xtw.Formatting = Formatting.Indented;
                    serializer.Serialize(xtw, request);
                    fs.Close();
                }
                catch { }
                //Audit.Add(EventType.SayaraCitySearch, Severity.High, 0, "CityRequest Requested", "0");
                WSCityResponse response = new WSCityResponse();
                response = sayaraService.GetCities(request);
                //Audit.Add(EventType.SayaraCitySearch, Severity.High, 0, "CityResponse Received", "0");
                try
                {
                    System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSCityResponse));
                    FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_CityListResponse.xml", FileMode.Create, FileAccess.Write);
                    XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                    xtw.Formatting = Formatting.Indented;
                    serializer.Serialize(xtw, response);
                    fs.Close();
                }
                catch { }
                if (response != null && response.ErrorCode == "000")
                {
                    locationList = new List<FleetLocation>();
                    foreach (WSCity city in response.CityList)
                    {
                        FleetCity fCity = new FleetCity();
                        fCity.CityCode = city.CityCode;
                        fCity.CityName = city.CityName;
                        //checking duplicate values
                        if (!cityList.Exists(delegate(FleetCity f) { return f.CityCode == city.CityCode; }))
                        {
                            cityList.Add(fCity);
                        }

                        FleetLocation fLocation = new FleetLocation();
                        fLocation.AirportCharges = city.LocationAirportCharge;
                        fLocation.CityCode = city.CityCode;
                        fLocation.LocationCode = city.LocationCode;
                        fLocation.LocationName = city.LocationName;
                        locationList.Add(fLocation);
                    }
                }
                else
                {
                    Audit.Add(EventType.SayaraCitySearch, Severity.High, 0, "Exception Loading cities in GetCities Method" + response.ErrorMessage, "0");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraCitySearch, Severity.High, 0, "CityRequest Requested"+ex.ToString(), "0");
            }
            return cityList;
        }
        #endregion

        #region search
        public FleetSearchResult[] GetFleetAvailability(FleetRequest req, decimal markup, string markupType,decimal discount,string discountType)
        {
            //Audit.Add(EventType.SayaraAvailSearch, Severity.Normal, 1, "Sayara.GetFleetAvailability entered", "0");
            WSFleetTariffRequest request = GenerateGetFleetAvailabilityRequest(req);
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSFleetTariffRequest));
                FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_FleetAvailabilityRequest.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, request);
                fs.Close();
            }
            catch { }
            //Audit.Add(EventType.SayaraAvailSearch, Severity.High, 0, "FleetSearchResult Requested", "0");

            FleetSearchResult[] searchRes = new FleetSearchResult[0];
            WSFleetTariffResponse response = new WSFleetTariffResponse();
            try
            {
                response = sayaraService.GetVehicleTariffDetails(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraAvailSearch, Severity.High, 0, "Exception returned from Sayara GetFleetAvailability exception:" + ex.ToString(), "0");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Audit.Add(EventType.SayaraAvailSearch, Severity.High, 0, "WSFleetTariffResponse Received", "0");
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSFleetTariffResponse));
                FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_FleetAvailabilityResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
            try
            {
                searchRes = ReadSearchResultResponse(response, req, markup, markupType, discount,discountType);

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraAvailSearch, Severity.High, 0, "Exception returned from Sayara.ReadSearchResultResponse Error Message:" + ex.ToString(), "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return searchRes;
        }

        private FleetSearchResult[] ReadSearchResultResponse(WSFleetTariffResponse response, FleetRequest req, decimal markup, string markupType, decimal discount, string discountType)
        {
            FleetSearchResult[] fleetResults;
            int count = 0;
            //checking tariffcount
            if (response != null && response.TariffList.Length > 0)
            {
                fleetResults = new FleetSearchResult[response.TariffList.Length];
                foreach (WSFleetTariff fleetTariff in response.TariffList)
                {
                    //creating fleetResult object
                    FleetSearchResult fleetResult = new FleetSearchResult();
                    fleetResult.ID = fleetTariff.Id;
                    fleetResult.IdVehTariff = fleetTariff.VehicleTariffId;
                    fleetResult.VehId = fleetTariff.VehicleId;
                    fleetResult.TariffDoc = fleetTariff.TariffDoc;
                    fleetResult.RentType = (CT.BookingEngine.RentType)fleetTariff.RentType;
                    fleetResult.DailyTariff = fleetTariff.DailyTariff;
                    fleetResult.Tariff = fleetTariff.Tariff;
                    fleetResult.Currency = currency;
                    rateOfExchange = exchangeRates[fleetResult.Currency];

                    fleetResult.SourceServiceAmount = new SerializableDictionary<string, decimal>();
                    //Adding without rateOfExchange Amount
                    fleetResult.CDW = Math.Round(fleetTariff.Cdw*rateOfExchange,decimalPoint);
                    fleetResult.SourceServiceAmount.Add("CDW", fleetTariff.Cdw);
                    fleetResult.PAI = Math.Round(fleetTariff.Pai * rateOfExchange, decimalPoint);
                    fleetResult.SourceServiceAmount.Add("PAI", fleetTariff.Pai);
                    fleetResult.SCDW = Math.Round(fleetTariff.Scdw * rateOfExchange, decimalPoint);
                    fleetResult.SourceServiceAmount.Add("SCDW", fleetTariff.Scdw);
                    fleetResult.CSEAT = Math.Round(fleetTariff.Cseat * rateOfExchange, decimalPoint);
                    fleetResult.SourceServiceAmount.Add("CSEAT", fleetTariff.Cseat);
                    fleetResult.VMD = Math.Round(fleetTariff.Vmd*rateOfExchange,decimalPoint);
                    fleetResult.SourceServiceAmount.Add("VMD", fleetTariff.Vmd);
                    fleetResult.GPS = Math.Round(fleetTariff.GPS * rateOfExchange, decimalPoint);
                    fleetResult.SourceServiceAmount.Add("GPS", fleetTariff.GPS);
                    fleetResult.KMREST = fleetTariff.KmRest;
                    fleetResult.KMRATE = fleetTariff.KmRate;
                    fleetResult.Chauffer = Math.Round(fleetTariff.Chauffer * rateOfExchange, decimalPoint);
                    fleetResult.SourceServiceAmount.Add("CHAUFFER", fleetTariff.Chauffer);
                    fleetResult.Ins_Excess = Math.Round(fleetTariff.InsuranceExcess * rateOfExchange, decimalPoint);
                    fleetResult.FromDate = fleetTariff.FromDate;
                    fleetResult.ToDate = fleetTariff.ToDate;
                    fleetResult.SRNO = fleetTariff.SrNo;
                    fleetResult.FleetName = fleetTariff.FleetName;
                    fleetResult.CarType = (CT.BookingEngine.FleetCarType)fleetTariff.CarType;

                    fleetResult.ImgFileName = fleetTariff.ImgFileName;
                    fleetResult.SourceServiceAmount.Add("LinkCharges", fleetTariff.LinkCharges);
                    fleetResult.LinkCharges = Math.Round(fleetTariff.LinkCharges * rateOfExchange, decimalPoint);
                    fleetResult.Source = CarBookingSource.Sayara;
                   

                    #region price
                    decimal totPrice = 0m;
                    decimal calcMarkup = 0m;
                    decimal calcStrikeOutMarkup = 0m;
                    decimal ItemPrice = 0m;
                    decimal strikeOutPrice = 0m;
                    decimal totalStrikeOutPrice = 0m;
                    decimal calcDiscount = 0m;
                    decimal calcStrikeOutDiscount = 0m;
                    //calucating days
                    int days = (int)Math.Ceiling(req.ToDateTime.Subtract(req.FromDateTime).TotalDays);
                    fleetResult.Days = days;
                    switch (fleetResult.RentType)
                    {
                        case CT.BookingEngine.RentType.Daily:
                            ItemPrice = (fleetResult.Tariff * days);
                            break;
                        case CT.BookingEngine.RentType.Weekly:
                            int weeks = days / 7;
                            int remainingDays = days % 7;
                            ItemPrice = ((fleetResult.Tariff * weeks) + (fleetResult.DailyTariff * remainingDays));
                            strikeOutPrice = (fleetResult.DailyTariff * days);
                            break;
                        case CT.BookingEngine.RentType.Monthly:
                            int months = days / 30;
                            remainingDays = days % 30;
                            ItemPrice = ((fleetResult.Tariff * months) + (fleetResult.DailyTariff * remainingDays));
                            strikeOutPrice = (fleetResult.DailyTariff * days);
                            break;
                    }
                    //calucating TotalAmount
                    totPrice = Math.Round((ItemPrice) * rateOfExchange, decimalPoint);
                    totalStrikeOutPrice = Math.Round((strikeOutPrice) * rateOfExchange, decimalPoint);
                    fleetResult.PriceInfo = new PriceAccounts();
                    fleetResult.PriceInfo.SupplierCurrency = fleetResult.Currency;
                    fleetResult.PriceInfo.SupplierPrice = ItemPrice;
                    fleetResult.PriceInfo.RateOfExchange = rateOfExchange;
                    fleetResult.PriceInfo.Currency = agentCurrency;
                    fleetResult.PriceInfo.CurrencyCode = agentCurrency;
                    //caluc Markup
                    calcMarkup = ((markupType == "F") ? markup : (Convert.ToDecimal(totPrice) * (markup / 100m)));
                    //StrikeOut Markup
                    calcStrikeOutMarkup = ((markupType == "F") ? markup : (Convert.ToDecimal(totalStrikeOutPrice) * (markup / 100m)));
                    //caluc discount
                    calcDiscount = ((discountType == "F") ? discount : (Convert.ToDecimal(totPrice + (Math.Round(calcMarkup, decimalPoint))) * (discount / 100m)));
                    //StrikeOut discount
                    calcStrikeOutDiscount = ((discountType == "F") ? discount : (Convert.ToDecimal(totalStrikeOutPrice+(Math.Round(calcStrikeOutMarkup, decimalPoint))) * (discount / 100m)));

                    fleetResult.PriceInfo.Markup = Math.Round(calcMarkup, decimalPoint);
                    fleetResult.PriceInfo.MarkupType = markupType;
                    fleetResult.PriceInfo.MarkupValue = markup;
                    fleetResult.PriceInfo.Discount = Math.Round(calcDiscount, decimalPoint);
                    fleetResult.PriceInfo.DiscountType = discountType;
                    fleetResult.PriceInfo.DiscountValue = discount;
                    fleetResult.TotalPrice = Math.Round(totPrice, decimalPoint);
                    fleetResult.PriceInfo.NetFare = totPrice;
                    //StrikeOut price
                    fleetResult.PriceInfo.OtherCharges = Math.Round((totalStrikeOutPrice + calcStrikeOutMarkup - calcStrikeOutDiscount), decimalPoint);

                    #endregion
                    //loading FleetSpecs
                    if (fleetTariff.FleetSpecifications != null)
                    {
                        fleetResult.FleetSpecs = new CT.BookingEngine.FleetSpecifications[fleetTariff.FleetSpecifications.Length];
                        for (int k = 0; k < fleetTariff.FleetSpecifications.Length; k++)
                        {
                            fleetResult.FleetSpecs[k] = new CT.BookingEngine.FleetSpecifications();
                            fleetResult.FleetSpecs[k].CarType = (CT.BookingEngine.FleetCarType)fleetTariff.FleetSpecifications[k].CarType;
                            fleetResult.FleetSpecs[k].FleetName = fleetTariff.FleetSpecifications[k].FleetName;
                            fleetResult.FleetSpecs[k].SpecDetails = fleetTariff.FleetSpecifications[k].SpecificationDetails;
                            fleetResult.FleetSpecs[k].SpecId = fleetTariff.FleetSpecifications[k].SpecificationId;
                            fleetResult.FleetSpecs[k].SpecName = fleetTariff.FleetSpecifications[k].SpecificationName;
                            fleetResult.FleetSpecs[k].SrNo = fleetTariff.FleetSpecifications[k].SrNo;
                        }
                    }
                    fleetResults[count] = fleetResult;
                    fleetResult.Currency = agentCurrency;
                    count++;
                }
                if (fleetResults.Length > count)
                {
                    Array.Resize(ref fleetResults, count);
                }
                foreach (FleetSearchResult FleetResult in fleetResults)
                {
                    FleetResult.TotalPrice = FleetResult.TotalPrice + FleetResult.PriceInfo.Markup - FleetResult.PriceInfo.Discount;
                    FleetResult.PriceInfo.AccPriceType = PriceType.NetFare;
                }
            }
            else
            {
                Audit.Add(EventType.SayaraAvailSearch, Severity.High, 0, "Exception Loading VehicleTariffDetails in GetVehicleTariffDetails Method" + response.ErrorMessage, "");
                throw new BookingEngineException("Error: " + response.ErrorMessage);
            }
            return fleetResults;
        }
        private WSFleetTariffRequest GenerateGetFleetAvailabilityRequest(FleetRequest request)
        {
            WSFleetTariffRequest tariffRequest = new WSFleetTariffRequest();
            try
            {
                tariffRequest.FromCity = request.FromCityCode;
                tariffRequest.FromDateTime = request.FromDateTime;
                tariffRequest.FromLocation = request.FromLocationCode;
                tariffRequest.CarType = request.CarType;
                tariffRequest.ReturnStatus = request.ReturnStatus;
                tariffRequest.ToCity = request.ToCityCode;
                tariffRequest.ToDateTime = request.ToDateTime;
                tariffRequest.ToLocation = request.ToLocationCode;

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraAvailSearch, Severity.High, 0, "Sayara tariffRequest Genarting Exception:" + ex.ToString(), "0");
            }
            return tariffRequest;
        }
        #endregion

        #region Booking
        public BookingResponse GetSayaraBooking(ref FleetItinerary itineary)
        {
            //Audit.Add(EventType.SayaraBooking, Severity.Normal, 1, "Sayara.GetSayara entered", "0");
            WSBookingDetail bookingRequest = GenerateGetFleetBookingRequest(itineary);

            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSBookingDetail));
                FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_BookingDetailRequest.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, bookingRequest);
                fs.Close();
            }
            catch { }
            //Audit.Add(EventType.SayaraBooking, Severity.High, 0, "WSBookingDetail Requested", "0");
            WSBookingDetailResponse response = new WSBookingDetailResponse();
            BookingResponse bookingRes = default(BookingResponse);
            try
            {
                response = sayaraService.SaveBookingDetails(bookingRequest);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraBooking, Severity.High, 0, "Exception returned from Sayara SaveBookingDetails exception:" + ex.ToString(), "0");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Audit.Add(EventType.SayaraAvailSearch, Severity.High, 0, "FleetResponse Received", "0");
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSBookingDetailResponse));
                FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_bookingDetailsResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
            try
            {
                bookingRes = ReadFleetBookingResponse(response, ref itineary);

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraAvailSearch, Severity.High, 0, "Exception returned from Sayara.ReadFleetBookingResponse Error Message:" + ex.ToString(), "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return bookingRes;
        }
        private BookingResponse ReadFleetBookingResponse(WSBookingDetailResponse response, ref FleetItinerary itineary)
        {
            BookingResponse bookingRes = default(BookingResponse);
            try
            {
                if (response != null && response.ErrorCode == "000")
                {
                    itineary.BookingRefNo = response.BookingDetail.BookingRef;
                    itineary.PassengerInfo[0].ClientId = response.BookingDetail.ClientId;
                    if (response.BookingDetail.BookingStatus == "C")
                    {
                        itineary.BookingStatus = CarBookingStatus.Confirmed;
                        bookingRes.Status = BookingResponseStatus.Successful;
                    }
                    else
                    {
                        itineary.BookingStatus = CarBookingStatus.Failed;
                        bookingRes.Status = BookingResponseStatus.Failed;
                    }
                    
                }
                else
                {
                    Audit.Add(EventType.SayaraBooking, Severity.High, 0, string.Concat(new object[]
                    {
                        " SAYARA:ReadGetFleetBookingResponse,Error Message:",
                        response.ErrorMessage,
                        " | ",
                        DateTime.Now,
                        "| Response XML",
                        response
                    }), "");
                    throw new BookingEngineException("<br>" + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bookingRes;
        }
        private WSBookingDetail GenerateGetFleetBookingRequest(FleetItinerary request)
        {
            WSBookingDetail bookingRequest = new WSBookingDetail();
            try
            {
               //Genarating Booking Request
                bookingRequest.BookingNetValue = request.Price.GetAgentFleetPrice();
                bookingRequest.BookingDate = DateTime.Now;
                bookingRequest.BookingRef = string.Empty;
                bookingRequest.BookingStatus = "C";
                bookingRequest.CardCharges = 0;
                //paxDetails

                if (request.PassengerInfo != null && request.PassengerInfo.Count > 0)
                {
                    bookingRequest.Customer = new WSCustomer();
                    bookingRequest.Customer.Title = request.PassengerInfo[0].Title;
                    bookingRequest.Customer.FirstName = request.PassengerInfo[0].FirstName;
                    bookingRequest.Customer.LastName = request.PassengerInfo[0].LastName;
                    bookingRequest.Customer.Email = request.PassengerInfo[0].Email;
                    bookingRequest.Customer.LicenseExpiryDate = request.PassengerInfo[0].LicenseExpiryDate;
                    bookingRequest.Customer.LicenseNumber = request.PassengerInfo[0].LicenseNumber;
                    bookingRequest.Customer.LicenseIssuePlace = request.PassengerInfo[0].LicenseIssuePlace;
                    bookingRequest.Customer.LicensePath = request.PassengerInfo[0].DocumentsPath;
                    if (request.PassengerInfo[0].ClientId > 0)
                    {
                        bookingRequest.ClientId = request.PassengerInfo[0].ClientId;
                    }
                    else
                    {
                        bookingRequest.ClientId = -1;
                    }
                }
                //Loading without rateofexchange value
                if (request.AirportCharge > -1)
                {
                    decimal airportChatge = 0;
                    request.SourceServiceAmount.TryGetValue("AirportCharges", out airportChatge);
                    bookingRequest.AirportCharge = airportChatge;
                }
                if (request.CDW > -1)
                {
                    decimal cdw = 0;
                    request.SourceServiceAmount.TryGetValue("CDW", out cdw);
                    bookingRequest.Cdw = cdw;
                }
                
                bookingRequest.Chauffer = request.CHAUFFER;
                if (request.CSEAT > -1)
                {
                    decimal cseat = 0;
                    request.SourceServiceAmount.TryGetValue("CSEAT", out cseat);
                    bookingRequest.Cseat = cseat;
                }
                if (request.Emirates_Link_Charge > -1)
                {
                    decimal linkCharge = 0;
                    request.SourceServiceAmount.TryGetValue("LinkCharges", out linkCharge);
                    bookingRequest.EmiratesLinkCharge  = linkCharge;
                }

                bookingRequest.FromDate = request.FromDate;
                bookingRequest.FromLocation = request.FromLocationCode;
                if (request.GPS > -1)
                {
                    decimal gps = 0;
                    request.SourceServiceAmount.TryGetValue("GPS", out gps);
                    bookingRequest.Gps = gps;
                }
                if (request.CHAUFFER > -1)
                {
                    decimal chau = 0;
                    request.SourceServiceAmount.TryGetValue("CHAUFFER", out chau);
                    bookingRequest.Chauffer = chau;
                }
                bookingRequest.VehicleTariffId = request.VehTariffId;

                bookingRequest.KmRate = request.KMRATE;
                
                if (request.PAI > -1)
                {
                    decimal pai = 0;
                    request.SourceServiceAmount.TryGetValue("PAI", out pai);
                    bookingRequest.Pai = pai;
                }
                bookingRequest.RentType = request.RentType.ToString();
                if (request.SCDW > -1)
                {
                    decimal scdw = 0;
                    request.SourceServiceAmount.TryGetValue("SCDW", out scdw);
                    bookingRequest.Scdw = scdw;
                }
                bookingRequest.Status = "A";//service side mandatory default passing A
                bookingRequest.Tariff = request.Price.SupplierPrice;
                bookingRequest.ToDate = request.ToDate;
                bookingRequest.ToLocation = request.ToLocationCode;
                bookingRequest.VehicleName = request.FleetName;
                bookingRequest.VehicleType = request.FleetType;
                bookingRequest.VehicleId = request.VehId;
                bookingRequest.PayStatus = "C";//service side mandatory default passing C
                if (request.VMD > -1)
                {
                    decimal vmd = 0;
                    request.SourceServiceAmount.TryGetValue("VMD", out vmd);
                    bookingRequest.Vmd = vmd;
                }
                bookingRequest.PaymentId = "0";
               
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraBooking, Severity.High, 0, "Sayara bookingRequest Genarting Exception:" + ex.ToString(), "0");
            }
            return bookingRequest;
        }
        #endregion

        #region BookingQueue
        public List<fleetAgentBookingBO> GetSayaraBookingDetails(DateTime fromDate, DateTime toDate, string bkRefNum, string bkStatus, string paymentStatus)
        {
            //Audit.Add(EventType.SayaraBookingQueue, Severity.Normal, 1, "Sayara.GetSayaraBookingDetails entered", "0");
            WSAgentBookingQueueRequest request = GenerateGetBookingQueueRequest(fromDate, toDate, bkRefNum, bkStatus, paymentStatus);
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSAgentBookingQueueRequest));
                FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_FleetBookingQueueRequest.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, request);
                fs.Close();
            }
            catch { }
            //Audit.Add(EventType.SayaraBookingQueue, Severity.High, 0, "WSAgentBookingQueueRequest Requested", "0");
            WSAgentBookingQueueResponse response = new WSAgentBookingQueueResponse();
            List<fleetAgentBookingBO> fleetBookingQueueList = new List<fleetAgentBookingBO>();
            try
            {
                response = sayaraService.GetSCRBookingDetails(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraBookingQueue, Severity.High, 0, "Exception returned from Sayara GetSayaraBookingDetails exception:" + ex.ToString(), "0");
                throw new BookingEngineException("Error: " + ex.Message);
            }
           // Audit.Add(EventType.SayaraBookingQueue, Severity.High, 0, "WSAgentBookingQueueResponse Received", "0");
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSAgentBookingQueueResponse));
                FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_FleetBookingQueueResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
            try
            {
                fleetBookingQueueList = ReadBookingQueueResponse(response);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraBookingQueue, Severity.High, 0, "Exception returned from Sayara.ReadBookingQueueResponse Error Message:" + ex.ToString(), "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return fleetBookingQueueList;
        }
        //Loading all b2c Booking details
        private List<fleetAgentBookingBO> ReadBookingQueueResponse(WSAgentBookingQueueResponse response)
        {
            List<fleetAgentBookingBO> bookingQueueRes = new List<fleetAgentBookingBO>();
            try
            {
                if (response != null && response.ErrorCode == "000")
                {
                    if(response.BookingDetails !=null)
                    {
                        foreach (WSBookingDetail details in response.BookingDetails)
                        {
                            fleetAgentBookingBO agentBkBo = new fleetAgentBookingBO();
                            agentBkBo.BookingStatus = GetStatusValue(details.BookingStatus);
                            agentBkBo.BookingDate = details.BookingDate;
                            agentBkBo.FromLocation = details.FromLocation;
                            agentBkBo.ToLocation = details.ToLocation;
                            agentBkBo.BookingRef = details.BookingRef;
                            agentBkBo.BookingNetAmount = details.BookingNetValue;
                            agentBkBo.PayStatus = GetStatusValue(details.PayStatus);
                            if (details.Customer != null)
                            {
                                agentBkBo.FirstName = details.Customer.FirstName;
                                agentBkBo.LastName = details.Customer.LastName;
                            }
                            agentBkBo.BookingId = details.BookingId;
                            if (details.PayStatus == "C" && details.PaymentDetail != null)
                            {
                                agentBkBo.BookingId = details.PaymentDetail.BookingId;
                                agentBkBo.PayRefId = details.PaymentDetail.PayRefId;
                                agentBkBo.Amount = details.PaymentDetail.Amount;
                                agentBkBo.Source = details.PaymentDetail.Source;
                            }
                            bookingQueueRes.Add(agentBkBo);
                        }
                    }
                }
                else
                {
                    Audit.Add(EventType.SayaraBookingQueue, Severity.High, 0, string.Concat(new object[]
                    {
                        " SAYARA:ReadBookingQueueResponse,Error Message:",
                        response.ErrorMessage,
                        " | ",
                        DateTime.Now,
                        "| Response XML",
                        response
                    }), "");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bookingQueueRes;
        }
        private WSAgentBookingQueueRequest GenerateGetBookingQueueRequest(DateTime fromDate, DateTime toDate, string bkRefNum, string bkStatus, string paymentStatus)
        {
            WSAgentBookingQueueRequest request = new WSAgentBookingQueueRequest();
            try
            {
                request.FromDate = fromDate;
                request.ToDate = toDate;
                if (!string.IsNullOrEmpty(bkRefNum))
                {
                    request.BookingRefNum = bkRefNum;
                }
                if (!string.IsNullOrEmpty(bkStatus))
                {
                    request.BookingStatus = bkStatus;
                }
                if (!string.IsNullOrEmpty(paymentStatus))
                {
                    request.PaymentStatus = paymentStatus;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraBookingQueue, Severity.High, 0, "Sayara GenerateGetBookingQueueRequest Genarting Exception:" + ex.ToString(), "0");
            }
            return request;
        }
        public String GetStatusValue(String StatusCode)
        {
            string description = string.Empty;

            switch (StatusCode)
            {
                case "P":
                    description = "Pending";
                    break;
                case "F":
                    description = "Modification In Progress";
                    break;
                case "C":
                    description = "Confirmed";
                    break;
                case "A":
                    description = "Active";
                    break;
                case "M":
                    description = "Modified";
                    break;
                case "L":
                    description = "Cancellation in progress";
                    break;
                case "X":
                    description = "Cancelled";
                    break;

                default:
                    description = "Pending";
                    break;
            }
            return description;
        }
        #endregion

        #region Booking Cancel

        public Dictionary<string, string> CancelFleetBooking(FleetItinerary itineary)
        {
            //Audit.Add(EventType.SayaraCancel, Severity.Normal, 1, "Sayara.CancelFleetBooking entered", "0");
            WSSaveServiceRequest request = GenerateGetCancelRequest(itineary);
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSSaveServiceRequest));
                FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_FleetCancelRequest.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, request);
                fs.Close();
            }
            catch { }
           // Audit.Add(EventType.SayaraCancel, Severity.High, 0, "WSSaveServiceRequest Requested", "0");
            Dictionary<string, string> cancelList = new Dictionary<string, string>();
            WSSaveServiceRequestResponse response = new WSSaveServiceRequestResponse();
            try
            {
                response = sayaraService.SaveServiceRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraCancel, Severity.High, 0, "Exception returned from Sayara SaveServiceRequest exception:" + ex.ToString(), "0");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Audit.Add(EventType.SayaraBookingQueue, Severity.High, 0, "WSSaveServiceRequestResponse Received", "0");
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSSaveServiceRequestResponse));
                FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_FleetBookingQueueResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
            try
            {
                cancelList = ReadCancelResponse(response);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraCancel, Severity.High, 0, "Exception returned from Sayara.ReadCancelResponse Error Message:" + ex.ToString(), "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return cancelList;

        }
        private Dictionary<string, string> ReadCancelResponse(WSSaveServiceRequestResponse response)
        {
            Dictionary<string, string> cancelList = new Dictionary<string, string>();
            try
            {
                if (response != null && response.ErrorCode == "000")
                {
                    cancelList.Add("Status", "CANCELLED"); //True
                    cancelList.Add("ID", response.BookingId.ToString()); //Booking ID
                    cancelList.Add("Currency", "AED"); //Supplier Currency
                    cancelList.Add("Amount", "0"); //fully refund
                }
                else
                {
                    cancelList.Add("Currency", "AED");
                    cancelList.Add("Amount", "0");
                    Audit.Add(EventType.SayaraCancel, Severity.High, 0, string.Concat(new object[]
                    {
                        " Sayara:ReadCancelResponse,Error Message:",
                        response.ErrorMessage,
                        " | ",
                        DateTime.Now,
                        "| Response XML",
                        response
                    }), "");
                    throw new BookingEngineException("<br>" + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cancelList;
        }
        private WSSaveServiceRequest GenerateGetCancelRequest(FleetItinerary itineary)
        {
            WSSaveServiceRequest request = new WSSaveServiceRequest();
            try
            {
                if (!string.IsNullOrEmpty(itineary.BookingRefNo))
                {
                    string [] bookrefNo = itineary.BookingRefNo.Split('-');
                    if (bookrefNo.Length > 2)
                    {
                        request.BookingId = Convert.ToInt32(bookrefNo[1]);
                        request.ServiceRequestStatus = Sayara.SayaraRestApi.ServiceRequestStatus.Cancelled;
                        request.Remarks = itineary.Remarks;
                        request.ClientId = itineary.PassengerInfo[0].ClientId;
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraCancel, Severity.High, 0, "Sayara GenerateGetCancelRequest Genarting Exception:" + ex.ToString(), "0");
            }
            return request;
        }
        #endregion

        #region changeRequestQueue
        public List<fleetAgentBookingBO> GetB2CChangeRequestQueue(DateTime fromDate, DateTime toDate, string bkRefNum, string bkStatus, string paymentStatus)
        {
            //Audit.Add(EventType.SayaraBookingQueue, Severity.Normal, 1, "Sayara.GetB2CChangeRequestQueue entered", "0");
            WSAgentBookingQueueRequest request = GenerateGetBookingQueueRequest(fromDate, toDate, bkRefNum, bkStatus, paymentStatus);
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSAgentBookingQueueRequest));
                FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_FleetB2CServiceRequest.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, request);
                fs.Close();
            }
            catch { }
            //Audit.Add(EventType.SayaraChangeRequest, Severity.High, 0, "WSAgentBookingQueueRequest Requested", "0");
            WSServiceRequestResponse response = new WSServiceRequestResponse();
            List<fleetAgentBookingBO> fleetBookingQueueList = new List<fleetAgentBookingBO>();
            try
            {
                response = sayaraService.GetChangeServiceRequests(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraChangeRequest, Severity.High, 0, "Exception returned from Sayara GetChangeServiceRequests exception:" + ex.ToString(), "0");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Audit.Add(EventType.SayaraBookingQueue, Severity.High, 0, "WSServiceRequestResponse Received", "0");
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSServiceRequestResponse));
                FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_FleetServiceResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
            try
            {
                fleetBookingQueueList = ReadB2CChangeRequestResponse(response);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraChangeRequest, Severity.High, 0, "Exception returned from Sayara.ReadB2CChangeRequestResponse Error Message:" + ex.ToString(), "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return fleetBookingQueueList;
        }
        //Loading all b2c Service details
        private List<fleetAgentBookingBO> ReadB2CChangeRequestResponse(WSServiceRequestResponse response)
        {
            List<fleetAgentBookingBO> bookingQueueRes = new List<fleetAgentBookingBO>();
            try
            {
                if (response != null && response.ErrorCode == "000")
                {
                    if (response.ServiceRequest != null)
                    {
                        foreach (WSServiceRequest service in response.ServiceRequest)
                        {
                            WSBookingDetail details = service.BookingDetail;
                            fleetAgentBookingBO agentBkBo = new fleetAgentBookingBO();
                            agentBkBo.BookingStatus = GetStatusValue(details.BookingStatus);
                            agentBkBo.BookingDate = details.BookingDate;
                            agentBkBo.FromLocation = details.FromLocation;
                            agentBkBo.ToLocation = details.ToLocation;
                            agentBkBo.BookingRef = details.BookingRef;
                            agentBkBo.BookingNetAmount = details.BookingNetValue;
                            agentBkBo.PayStatus = GetStatusValue(details.PayStatus);
                            if (details.Customer != null)
                            {
                                agentBkBo.FirstName = details.Customer.FirstName;
                                agentBkBo.LastName = details.Customer.LastName;
                            }
                            agentBkBo.BookingId = details.BookingId;
                            if (details.PayStatus == "C" && details.PaymentDetail != null)
                            {
                                agentBkBo.BookingId = details.PaymentDetail.BookingId;
                                agentBkBo.PayRefId = details.PaymentDetail.PayRefId;
                                agentBkBo.Amount = details.PaymentDetail.Amount;
                                agentBkBo.Source = details.PaymentDetail.Source;
                            }
                            agentBkBo.Remarks = service.Remarks;
                            agentBkBo.ServiceRequestStatus = service.ServiceRequestStatus.ToString();
                            agentBkBo.ServiceReqId = service.Id;
                            bookingQueueRes.Add(agentBkBo);
                        }

                    }
                }
                else
                {
                    Audit.Add(EventType.SayaraBookingQueue, Severity.High, 0, string.Concat(new object[]
                    {
                        " SAYARA:ReadB2CChangeRequestResponse,Error Message:",
                        response.ErrorMessage,
                        " | ",
                        DateTime.Now,
                        "| Response XML",
                        response
                    }), "");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bookingQueueRes;
        }
        #endregion

        #region SaveServiceRequest
        public bool SaveServiceRequestDetails(decimal adminFee,decimal suppplierFee,int bookingId,int clientId,int serviceRequestId,string requestStatus)
        {
            //Audit.Add(EventType.SayaraCancel, Severity.Normal, 1, "Sayara.SaveServiceRequesDetails entered", "0");
            WSSaveServiceRequest request = GenerateSaveServiceRequestDetails(adminFee, suppplierFee, bookingId, clientId, serviceRequestId, requestStatus);
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSSaveServiceRequest));
                FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_FleetSaveServiceRequest.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, request);
                fs.Close();
            }
            catch { }
            //Audit.Add(EventType.SayaraCancel, Severity.High, 0, "WSSaveServiceRequest Requested", "0");
            WSSaveServiceRequestResponse response = new WSSaveServiceRequestResponse();
            bool isSaved = false;
            try
            {
                response = sayaraService.SaveServiceRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraCancel, Severity.High, 0, "Exception returned from Sayara SaveServiceRequest exception:" + ex.ToString(), "0");
                throw new BookingEngineException("Error: " + ex.Message);
            }
            //Audit.Add(EventType.SayaraCancel, Severity.High, 0, "WSSaveServiceRequestResponse Received", "0");
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(WSSaveServiceRequestResponse));
                FileStream fs = new FileStream(XmlPath + DateTime.Now.ToString("ddMMyyy_hhmmss") + "_FleetSaveServiceResponse.xml", FileMode.Create, FileAccess.Write);
                XmlTextWriter xtw = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                serializer.Serialize(xtw, response);
                fs.Close();
            }
            catch { }
            try
            {
                isSaved = ReadSaveServiceResponse(response);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraCancel, Severity.High, 0, "Exception returned from Sayara.ReadSaveServiceResponse Error Message:" + ex.ToString(), "");
                throw new BookingEngineException("Error: " + ex.ToString());
            }
            return isSaved;
        }
        private bool ReadSaveServiceResponse(WSSaveServiceRequestResponse response)
        {
            bool isValid = false;
            List<fleetAgentBookingBO> bookingQueueRes = new List<fleetAgentBookingBO>();
            try
            {
                if (response != null && response.ErrorCode == "000")
                {
                    isValid = true;
                }
                else
                {
                    isValid = false;
                    Audit.Add(EventType.SayaraCancel, Severity.High, 0, string.Concat(new object[]
                    {
                        " SAYARA:ReadSaveServiceResponse,Error Message:",
                        response.ErrorMessage,
                        " | ",
                        DateTime.Now,
                        "| Response XML",
                        response
                    }), "");
                    throw new BookingEngineException("<br>" + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isValid;
        }
        private WSSaveServiceRequest GenerateSaveServiceRequestDetails(decimal adminFee, decimal suppplierFee, int bookingId, int clientId, int serviceRequestId, string requestStatus)
        {
            WSSaveServiceRequest request = new WSSaveServiceRequest();
            try
            {
                request.AdminFee = adminFee;
                request.SuppplierFee = suppplierFee;
                request.ClientId = clientId;
                request.BookingId = bookingId;
                request.ServiceRequestId = serviceRequestId;
                if (requestStatus == "Cancel")
                {
                    request.ServiceRequestStatus=Sayara.SayaraRestApi.ServiceRequestStatus.Cancelled;
                }
                else if (requestStatus == "Modified")
                {
                    request.ServiceRequestStatus = Sayara.SayaraRestApi.ServiceRequestStatus.Modified;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SayaraBookingQueue, Severity.High, 0, "Sayara GenerateGetBookingQueueRequest Genarting Exception:" + ex.ToString(), "0");
            }
            return request;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~SayaraApi()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
        #endregion


    }
    
}
