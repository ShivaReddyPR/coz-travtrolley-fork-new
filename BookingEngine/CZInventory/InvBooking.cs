﻿using System;
using System.Collections.Generic;
using System.Data;
using CT.BookingEngine;
using System.Data.SqlClient;
using CT.Configuration;
using CT.Core;
using System.Collections;
using System.Configuration;
using System.Linq;

namespace CZInventory
{
    public class InvBooking
    {

        public InvBooking()
        {
            Connection();
        }


        private void Connection()
        {
            if (ConfigurationSystem.HotelConnectConfig["ConnectionString"] != null)
            {
                DBGateway.ConnectionString =(ConfigurationSystem.HotelConnectConfig["ConnectionString"]);
            }
        }

        /// <summary>
        /// This method returns the search results for the Hotel Search Request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DataSet GetInventoryHotels(HotelRequest request)
        {
            
            DataSet dsHotelResults = new DataSet();
            string roomPax = "";
            string childAge = "";
            //int child = -1;
            int order = 1;
            int countryId = 0;
            Dictionary<int, string> Guests = new Dictionary<int, string>();

            foreach (RoomGuestData guest in request.RoomGuest)
            {
                if (roomPax.Trim().Length > 0)
                {
                    roomPax += "|" + guest.noOfAdults + "," + guest.noOfChild;
                    Guests.Add(order, guest.noOfAdults + "," + guest.noOfChild);
                }
                else
                {
                    roomPax = guest.noOfAdults + "," + guest.noOfChild;
                    Guests.Add(order, guest.noOfAdults + "," + guest.noOfChild);
                }

                order++;
                if (guest.noOfChild > 0)
                {
                    for (int i = 0; i < guest.childAge.Count; i++)
                    {
                        int age = guest.childAge[i];
                        if (childAge.Trim().Length > 0 && !childAge.EndsWith("|"))
                        {
                            childAge += "," + age;
                        }
                        else
                        {
                            childAge += age.ToString();
                        }
                    }

                    //if (!childAge.Contains("|"))
                    {
                        childAge += "|";
                    }
                }
                else
                {
                    if (childAge.Trim().Length > 0 && childAge.EndsWith("|"))
                    {
                        childAge += "0|";
                    }
                    else
                    {
                        childAge += "0|";
                    }
                }
            }

            if (childAge.EndsWith("|"))
            {
                childAge = childAge.Remove(childAge.Length - 1, 1);
            }


            try
            {
                SqlParameter[] paramList = new SqlParameter[18];
                if (countryId > 0)
                {
                    paramList[0] = new SqlParameter("@P_Country_Id", countryId);
                }
                else
                {
                    paramList[0] = new SqlParameter("@P_Country_Id", DBNull.Value);
                }
                if(request.CityId>0)paramList[1] = new SqlParameter("@P_City_Id", request.CityId);
                else paramList[1] = new SqlParameter("@P_City_Id", DBNull.Value);
                paramList[2] = new SqlParameter("@P_CheckIn_Date", request.StartDate.ToString("yyyy-MM-dd"));
                paramList[3] = new SqlParameter("@P_CheckOut_Date", request.EndDate.ToString("yyyy-MM-dd"));
                switch (request.Rating)
                {
                    case HotelRating.All:
                        paramList[4] = new SqlParameter("@P_Star_Level", "");
                        break;
                    case HotelRating.OneStar:
                        paramList[4] = new SqlParameter("@P_Star_Level", "1S");
                        break;
                    case HotelRating.TwoStar:
                        paramList[4] = new SqlParameter("@P_Star_Level", "2S");
                        break;
                    case HotelRating.ThreeStar:
                        paramList[4] = new SqlParameter("@P_Star_Level", "3S");
                        break;
                    case HotelRating.FourStar:
                        paramList[4] = new SqlParameter("@P_Star_Level", "4S");
                        break;
                    case HotelRating.FiveStar:
                        paramList[4] = new SqlParameter("@P_Star_Level", "5S");
                        break;
                }
                paramList[5] = new SqlParameter("@P_Hotel_Name", (string.IsNullOrEmpty(request.HotelName) ? string.Empty: request.HotelName));
                paramList[6] = new SqlParameter("@P_No_Of_Rooms", request.NoOfRooms);
                paramList[7] = new SqlParameter("@P_Room_Pax", roomPax);
                paramList[8] = new SqlParameter("@P_Child_Ages", childAge);
                paramList[9] = new SqlParameter("@P_Property_Type_Id", DBNull.Value);
                paramList[10] = new SqlParameter("@P_Show_Map", DBNull.Value);
                paramList[11] = new SqlParameter("@P_Currency", request.Currency);
                paramList[12] = new SqlParameter("@P_MSG_TEXT", "");
                paramList[12].Direction = ParameterDirection.Output;
                paramList[13] = new SqlParameter("@P_MSG_TYPE", "");
                paramList[13].Direction = ParameterDirection.Output;


                if (request.PassengerNationality == null || request.PassengerNationality.Length <= 0)
                {
                    paramList[14] = new SqlParameter("@P_Nationality", DBNull.Value);
                }
                else
                {

                    string countryCode = Country.GetCountryCodeFromCountryName(request.PassengerNationality);

                    if(!string.IsNullOrEmpty(countryCode))request.PassengerNationality = countryCode;

                    paramList[14] = new SqlParameter("@P_Nationality", (request.PassengerNationality));
                }
                if (request.Longtitude != 0.0)
                {
                    paramList[15] = new SqlParameter("@P_Longitude", request.Longtitude);
                }
                else
                {
                    paramList[15] = new SqlParameter("@P_Longitude", DBNull.Value);
                }
                if (request.Latitude != 0.0)
                {
                    paramList[16] = new SqlParameter("@P_Latitude", request.Latitude);
                }
                else
                {
                    paramList[16] = new SqlParameter("@P_Latitude", DBNull.Value);
                }
                if (request.Radius > 0) paramList[17] = new SqlParameter("@P_Radius", request.Radius/1000);
                else paramList[17] = new SqlParameter("@P_Radius", DBNull.Value);
                dsHotelResults = DBGateway.FillSP("P_SEARCH_HOTELS", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, 1, "(CZInventory)Failed to get results. Error: " + ex.ToString(), "");
            }
            return dsHotelResults;
        }

        /// <summary>
        /// This method returns the available inventory quota for the particular room and date
        /// </summary>
        /// <param name="hotelId"></param>
        /// <param name="periodId"></param>
        /// <param name="roomId"></param>
        /// <param name="accomId"></param>
        /// <param name="checkInDate"></param>
        /// <param name="checkOutDate"></param>
        /// <returns></returns>
        public int GetQuota(int hotelId, int periodId, int roomId, int accomId, DateTime checkInDate, DateTime checkOutDate)
        {
            int Quota = 0;

            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@P_HotelId", hotelId);
                paramList[1] = new SqlParameter("@P_PeriodId", periodId);
                paramList[2] = new SqlParameter("@P_RoomId", roomId);
                paramList[3] = new SqlParameter("@P_AccomId", accomId);
                paramList[4] = new SqlParameter("@P_CheckInDate", checkInDate);
                paramList[5] = new SqlParameter("@P_CheckOutDate", checkOutDate);

                DataTable dt = DBGateway.FillDataTableSP("usp_GetRoomQuotaBetweenDates", paramList);

                if (dt != null && dt.Rows.Count > 0)
                {
                    Quota = Convert.ToInt32(dt.Rows[0]["Quota"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Quota;
        }


        public string GetRoomImagesAndChilds(int roomId)
        {
            string roomImages = string.Empty;

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@ID", roomId);

                DataTable dtImages = DBGateway.FillDataTableSP("P_HTL_ROOM_MASTER_GETDATA", paramList);

                if (dtImages != null && dtImages.Rows.Count > 0)
                {
                    roomImages = dtImages.Rows[0]["room_child"].ToString() + "|" + dtImages.Rows[0]["room_img1"].ToString() + "," + dtImages.Rows[0]["room_img2"].ToString() + "," + dtImages.Rows[0]["room_img3"].ToString();
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, 1, "(CZInventory)Failed to get Room images. Error : " + ex.ToString(), "");
            }

            return roomImages;
        }

        public List<string> GetHotelFacilities(int hotelId)
        {
            List<string> HotelFacilities = new List<string>();

            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@P_LIST_STATUS", 2);
                paramList[1] = new SqlParameter("@P_RECORD_STATUS", "A");
                paramList[2] = new SqlParameter("@p_hotel_id", hotelId);
                paramList[3] = new SqlParameter("@P_source", "H");
                DataTable dtFacilities = DBGateway.FillDataTableSP("P_HTL_HOTEL_Facilities_GETLIST", paramList);

                if (dtFacilities != null)
                {
                    foreach (DataRow row in dtFacilities.Rows)
                    {
                        HotelFacilities.Add(row["FacilityName"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return HotelFacilities;
        }

        public DataTable GetCancelPolicy(string hotelCode, int roomId, int periodId)
        {
            DataTable dtCancelPolicy = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@P_Hotel_Id", hotelCode);
                paramList[1] = new SqlParameter("@P_Room_Id", roomId);
                paramList[2] = new SqlParameter("@P_Period_Id", periodId);
                dtCancelPolicy = DBGateway.FillDataTableSP("P_HTL_CANCELLATION_POLICY_GET", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(CZInventory)Failed to get Cancel Policy for HotelCode. Error : " + ex.ToString(), "");
            }
            return dtCancelPolicy;
        }

        public DataTable GetHotelNorms(int hotelId)
        {
            DataTable dtNorms = new DataTable();
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@P_Hotel_Id", hotelId);
                dtNorms = DBGateway.FillDataTableSP("P_HTL_HOTEL_NORMS", parameters);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "");
            }

            return dtNorms;
        }

        public string ConfirmBooking(HotelItinerary hotelBooking)
        {
            SqlTransaction bookingTrans = null;
            string confirmationRef =string.Empty;
            int bookingId = 0;
            try
            {
                SqlConnection con = DBGateway.GetConnection();
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                if (hotelBooking.HotelPassenger == null)
                    hotelBooking.HotelPassenger = hotelBooking.Roomtype[0].PassenegerInfo[0];
                bookingTrans = con.BeginTransaction();

                //*****************************************************************************************//
                //                  Generate and Save Confirmation number for the booking
                //*****************************************************************************************//

                SqlParameter[] paramList = new SqlParameter[5];
                paramList[0] = new SqlParameter("@P_ConfirmationRef", confirmationRef);
                paramList[0].Direction = System.Data.ParameterDirection.Output;
                paramList[0].Size = 50;
                paramList[1] = new SqlParameter("@P_RoomId", Convert.ToInt32(hotelBooking.Roomtype[0].RoomTypeCode.Split('-')[1]));
                paramList[2] = new SqlParameter("@P_PropertyId", Convert.ToInt32(hotelBooking.Roomtype[0].RoomTypeCode.Split('-')[0]));
                paramList[3] = new SqlParameter("@P_CreatedOn", DateTime.Now);
                paramList[4] = new SqlParameter("@P_CreatedBy", hotelBooking.CreatedBy);
                try
                {
                    int count = DBGateway.ExecuteNonQuerySP("P_HTL_CONFIRMATION_REFERENCE_ADD", paramList);

                    if (count > 0 && paramList[0].Value != null)
                    {
                        confirmationRef = paramList[0].Value.ToString();
                        hotelBooking.ConfirmationNo = confirmationRef;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Failed to Save Confirmation", ex);
                }

                //*****************************************************************************************//
                //                          Save the Booking detail header
                //*****************************************************************************************//

                paramList = new SqlParameter[28];
                paramList[0] = new SqlParameter("@P_BookingId", -1);
                paramList[1] = new SqlParameter("@P_ClientId", hotelBooking.HotelCode);
                paramList[2] = new SqlParameter("@P_AgentReference", hotelBooking.AgencyReference);
                paramList[3] = new SqlParameter("@P_ConfirmationNo", hotelBooking.ConfirmationNo);
                paramList[4] = new SqlParameter("@P_StartDate", hotelBooking.StartDate);
                paramList[5] = new SqlParameter("@P_EndDate", hotelBooking.EndDate);
                paramList[6] = new SqlParameter("@P_BookingStatus", (int)HotelBookingStatus.Confirmed);
                paramList[7] = new SqlParameter("@P_CityCode", (hotelBooking.CityCode != null ? hotelBooking.CityCode : ""));
                //paramList[8] = new SqlParameter("@P_CountryCode", (hotelBooking.HotelPassenger.Countrycode != null ? hotelBooking.HotelPassenger.Countrycode : ""));
                paramList[8] = new SqlParameter("@P_CountryCode", (hotelBooking.Roomtype[0].PassenegerInfo[0].Countrycode != null ? hotelBooking.Roomtype[0].PassenegerInfo[0].Countrycode : ""));
                paramList[9] = new SqlParameter("@P_PropertyId", hotelBooking.HotelCode);
                paramList[10] = new SqlParameter("@P_SpecialRequest", (hotelBooking.SpecialRequest != null ? hotelBooking.SpecialRequest : ""));
                paramList[11] = new SqlParameter("@P_FlightInformation", (hotelBooking.FlightInfo != null ? hotelBooking.FlightInfo : ""));
                paramList[12] = new SqlParameter("@P_PromoId", -1);
                paramList[13] = new SqlParameter("@P_CreatedOn", hotelBooking.CreatedOn);
                paramList[14] = new SqlParameter("@P_CreatedBy", hotelBooking.CreatedBy);
                if (hotelBooking.LastModifiedBy > 0)
                {
                    paramList[15] = new SqlParameter("@P_LastModifiedOn", hotelBooking.LastModifiedOn);
                    paramList[16] = new SqlParameter("@P_LastModifiedBy", hotelBooking.LastModifiedBy);
                }
                else
                {
                    paramList[15] = new SqlParameter("@P_LastModifiedOn", DBNull.Value);
                    paramList[16] = new SqlParameter("@P_LastModifiedBy", DBNull.Value);
                }

                paramList[17] = new SqlParameter("@P_FirstName", hotelBooking.HotelPassenger.Firstname);
                paramList[18] = new SqlParameter("@P_LastName", hotelBooking.HotelPassenger.Lastname);
                paramList[19] = new SqlParameter("@P_AddressLine1", hotelBooking.HotelPassenger.Addressline1);
                paramList[20] = new SqlParameter("@P_AddressLine2", hotelBooking.HotelPassenger.Addressline2);
                paramList[21] = new SqlParameter("@P_City", hotelBooking.HotelPassenger.City);
                paramList[22] = new SqlParameter("@P_State", (hotelBooking.HotelPassenger.State != null ? hotelBooking.HotelPassenger.State : ""));
                paramList[23] = new SqlParameter("@P_Country", hotelBooking.HotelPassenger.Country);
                paramList[24] = new SqlParameter("@P_PhoneNumber", hotelBooking.HotelPassenger.Phoneno);
                paramList[25] = new SqlParameter("@P_Email", hotelBooking.HotelPassenger.Email);
                paramList[26] = new SqlParameter("@P_Currency", hotelBooking.Currency);
                paramList[27] = new SqlParameter("@P_NewId", 0);
                paramList[27].Direction = System.Data.ParameterDirection.Output;
                int recCount = 0;
                try
                {
                    recCount = DBGateway.ExecuteNonQuerySP("P_HTL_BOOKING_CONFIRMATION_ADDUPDATE", paramList);
                }
                catch (Exception ex)
                {
                    throw new Exception("(CZInventory)Failed to Save Booking Header. Error : " + ex.Message, ex);
                }

                if (recCount > 0 && paramList[27].Value != null)
                {
                    bookingId = Convert.ToInt32(paramList[27].Value);

                    bool isSameRoom = false;
                    string firstRoom = hotelBooking.Roomtype[0].RoomTypeCode.Split('-')[1];
                    try
                    {

                        //*************************************************************************
                        //                      Save Room Booking Information
                        //*************************************************************************

                        foreach (HotelRoom room in hotelBooking.Roomtype)
                        {
                            if (room.RoomTypeCode.Split('-')[1] == firstRoom)
                            {
                                isSameRoom = true;
                            }
                            else
                            {
                                isSameRoom = false;
                            }

                            string ChildAge = string.Empty;
                            if (room.ChildAge != null && room.ChildAge.Count > 0)
                            {
                                ChildAge = "";
                                foreach (int age in room.ChildAge)
                                {
                                    if (ChildAge.Length > 0)
                                    {
                                        ChildAge += "," + age.ToString();
                                    }
                                    else
                                    {
                                        ChildAge = age.ToString();
                                    }
                                }
                            }
                            else
                            {
                                ChildAge = "";
                            }
                            paramList = new SqlParameter[15];
                            paramList[0] = new SqlParameter("@P_BookingId", bookingId);
                            paramList[1] = new SqlParameter("@P_RoomConfirmationNo", confirmationRef);
                            paramList[2] = new SqlParameter("@P_RoomStatus", 0);
                            paramList[3] = new SqlParameter("@P_RoomId", Convert.ToInt32(room.RoomTypeCode.Split('-')[1]));
                            paramList[4] = new SqlParameter("@P_RoomCode", room.RoomTypeCode);
                            string roomNameWithMealPlan=room.RoomName + "-" + room.MealPlanDesc;
                            paramList[5] = new SqlParameter("@P_RoomName", roomNameWithMealPlan);
                            paramList[6] = new SqlParameter("@P_NoOfAdults", room.AdultCount);
                            paramList[7] = new SqlParameter("@P_NoOfChild", room.ChildCount);
                            paramList[8] = new SqlParameter("@P_FirstName", room.PassenegerInfo[0].Firstname);
                            paramList[9] = new SqlParameter("@P_LastName", room.PassenegerInfo[0].Lastname);
                            paramList[10] = new SqlParameter("@P_CancellationDetails", hotelBooking.HotelCancelPolicy);
                            paramList[11] = new SqlParameter("@P_Price", room.Price.SupplierPrice);
                            paramList[12] = new SqlParameter("@P_Tax", room.Price.Tax);
                            paramList[13] = new SqlParameter("@P_RoomReferenceNo", Convert.ToInt32(room.RoomTypeCode.Split('-')[1]));
                            paramList[14] = new SqlParameter("@P_ChildAge", ChildAge);

                            int count = DBGateway.ExecuteNonQuerySP("P_HTL_ROOM_BOOKING_INFORMATION_ADD", paramList);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Failed to Save Room Booking information", ex);
                    }

                    //****************************************************************
                    //                  Save Room Quota information
                    //****************************************************************

                    List<string> roomIds = new List<string>();
                    foreach (HotelRoom room in hotelBooking.Roomtype)
                    {
                        string quota = string.Empty;
                        //If Same room booked then update Quota = No of rooms
                        if (isSameRoom)
                        {
                            //RoomTypeCode format HotelId-RoomId-PeriodId-AccomId-Adults-Sequence
                            string[] vals = room.RoomTypeCode.Split('-');
                            quota = vals[0] + "," + vals[1] + "," + vals[2] + "," + hotelBooking.StartDate.ToString("yyyy-MM-dd") + "," + hotelBooking.EndDate.ToString("yyyy-MM-dd") + "," + vals[3] + ",1";
                            int sameRoomIndex = roomIds.FindIndex(delegate(string rq) { return Convert.ToInt32(rq.Split(',')[1]) == Convert.ToInt32(room.RoomTypeCode.Split('-')[1]); });

                            if (sameRoomIndex >= 0)
                            {
                                int Quota = Convert.ToInt32(roomIds[sameRoomIndex].Split(',')[6]);
                                Quota++;
                                string[] ids = roomIds[sameRoomIndex].Split(',');
                                roomIds[sameRoomIndex] = ids[0] + "," + ids[1] + "," + ids[2] + "," + ids[3] + "," + ids[4] + "," + ids[5] + "," + Quota;
                            }

                            //Add only unique rooms
                            if (!roomIds.Exists(delegate(string rq) { return Convert.ToInt32(rq.Split(',')[1]) == Convert.ToInt32(quota.Split(',')[1]) && Convert.ToInt32(rq.Split(',')[0]) == Convert.ToInt32(quota.Split(',')[0]) && Convert.ToInt32(rq.Split(',')[2]) == Convert.ToInt32(quota.Split(',')[2]) && Convert.ToInt32(rq.Split(',')[5]) == Convert.ToInt32(quota.Split(',')[5]); }))
                            {
                                roomIds.Add(quota);
                            }
                        }
                        else //Otherwise quota will be 1
                        {
                            string[] vals = room.RoomTypeCode.Split('-');
                            quota = vals[0] + "," + vals[1] + "," + vals[2] + "," + hotelBooking.StartDate.ToString("yyyy-MM-dd") + "," + hotelBooking.EndDate.ToString("yyyy-MM-dd") + "," + vals[3] + ",1";

                            //Add only unique rooms
                            if (!roomIds.Exists(delegate(string rq) { return Convert.ToInt32(rq.Split(',')[1]) == Convert.ToInt32(quota.Split(',')[1]) && Convert.ToInt32(rq.Split(',')[0]) == Convert.ToInt32(quota.Split(',')[0]) && Convert.ToInt32(rq.Split(',')[2]) == Convert.ToInt32(quota.Split(',')[2]) && Convert.ToInt32(rq.Split(',')[5]) == Convert.ToInt32(quota.Split(',')[5]); }))
                            {
                                roomIds.Add(quota);
                            }
                        }
                    }
                    foreach (string rq in roomIds)
                    {
                        try
                        {
                            paramList = new SqlParameter[7];

                            paramList[0] = new SqlParameter("@P_RoomId", Convert.ToInt32(rq.Split(',')[1]));
                            paramList[1] = new SqlParameter("@P_HotelId", Convert.ToInt32(rq.Split(',')[0]));
                            paramList[2] = new SqlParameter("@P_PeriodId", Convert.ToInt32(rq.Split(',')[2]));
                            paramList[3] = new SqlParameter("@P_Start_Date", rq.Split(',')[3]);
                            paramList[4] = new SqlParameter("@P_End_Date", rq.Split(',')[4]);
                            paramList[5] = new SqlParameter("@P_AccomId", Convert.ToInt32(rq.Split(',')[5]));
                            paramList[6] = new SqlParameter("@P_QuotaUsed", Convert.ToInt32(rq.Split(',')[6]));

                            int rec = DBGateway.ExecuteNonQuerySP("P_HTL_UPDATE_ROOM_QUOTA", paramList);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Failed to Update Room Quota", ex);
                        }
                    }

                    //*******************************************************************
                    //                     Save Room Fare Breakdown
                    //*******************************************************************

                    try
                    {
                        decimal charge = 0; Int16 rateType = (Int16)FareType.Net;
                        foreach (HotelRoom room in hotelBooking.Roomtype)
                        {
                            int days = hotelBooking.EndDate.Subtract(hotelBooking.StartDate).Days;

                            //*******************************************************************************************
                            // If more than 2 days booked and more than 1 room booked then save room fare breakdown
                            // day wise by dividing the price with total days. In this scenario RoomFareBreakDown will be
                            // only one for each room
                            //*******************************************************************************************
                            if (days > 1 && hotelBooking.Roomtype.Length > 1)
                            {
                                for (int i = 0; i < days; i++)
                                {
                                    foreach (HotelRoomFareBreakDown fare in room.RoomFareBreakDown)
                                    {
                                        paramList = new SqlParameter[14];
                                        paramList[0] = new SqlParameter("@P_BookingId", bookingId);
                                        paramList[1] = new SqlParameter("@P_RoomId", Convert.ToInt32(room.RoomTypeCode.Split('-')[1]));
                                        paramList[2] = new SqlParameter("@P_Date", hotelBooking.StartDate.AddDays(i));
                                        paramList[3] = new SqlParameter("@P_Price", (room.Price.SupplierPrice) / days);
                                        paramList[4] = new SqlParameter("@P_Tax", (room.Price.Tax / days));
                                        paramList[5] = new SqlParameter("@P_ExtraGuestCharge", charge);
                                        paramList[6] = new SqlParameter("@P_RateType", rateType);
                                        paramList[7] = new SqlParameter("@P_NetPercentage", charge);
                                        paramList[8] = new SqlParameter("@P_IncomingComm", charge);
                                        paramList[9] = new SqlParameter("@P_OutgoingComm", charge);
                                        paramList[10] = new SqlParameter("@P_ChildCharge", charge);
                                        paramList[11] = new SqlParameter("@P_AccomId", Convert.ToInt32(room.RoomTypeCode.Split('-')[3]));
                                        paramList[12] = new SqlParameter("@P_PeriodId", Convert.ToInt32(room.RoomTypeCode.Split('-')[2]));
                                        paramList[13] = new SqlParameter("@P_HotelId", Convert.ToInt32(room.RoomTypeCode.Split('-')[0]));

                                        int count = DBGateway.ExecuteNonQuerySP("P_HTL_BOOKING_ROOM_FARE_BREAK_DOWN_ADD", paramList);
                                    }
                                }

                            }
                            else
                            {
                                //************************************************************************************
                                // If only one room booked for multiple days then RoomFareBreakDown will be there for
                                // each day i.e. 5th to 9th fare break down will be for 5th, 6th, 7th and 8th
                                //************************************************************************************
                                foreach (HotelRoomFareBreakDown fare in room.RoomFareBreakDown)
                                {
                                    paramList = new SqlParameter[14];
                                    paramList[0] = new SqlParameter("@P_BookingId", bookingId);
                                    paramList[1] = new SqlParameter("@P_RoomId", Convert.ToInt32(room.RoomTypeCode.Split('-')[1]));
                                    paramList[2] = new SqlParameter("@P_Date", fare.Date);
                                    paramList[3] = new SqlParameter("@P_Price", (room.Price.NetFare + room.Price.Markup) / room.RoomFareBreakDown.Length);
                                    paramList[4] = new SqlParameter("@P_Tax", (room.Price.Tax / room.RoomFareBreakDown.Length));
                                    paramList[5] = new SqlParameter("@P_ExtraGuestCharge", charge);
                                    paramList[6] = new SqlParameter("@P_RateType", rateType);
                                    paramList[7] = new SqlParameter("@P_NetPercentage", charge);
                                    paramList[8] = new SqlParameter("@P_IncomingComm", charge);
                                    paramList[9] = new SqlParameter("@P_OutgoingComm", charge);
                                    paramList[10] = new SqlParameter("@P_ChildCharge", charge);
                                    paramList[11] = new SqlParameter("@P_AccomId", Convert.ToInt32(room.RoomTypeCode.Split('-')[3]));
                                    paramList[12] = new SqlParameter("@P_PeriodId", Convert.ToInt32(room.RoomTypeCode.Split('-')[2]));
                                    paramList[13] = new SqlParameter("@P_HotelId", Convert.ToInt32(room.RoomTypeCode.Split('-')[0]));

                                    int count = DBGateway.ExecuteNonQuerySP("P_HTL_BOOKING_ROOM_FARE_BREAK_DOWN_ADD", paramList);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Failed to Save Room Fare Breakdown", ex);
                    }
                }

                bookingTrans.Commit();


                #region Send Hotel Booking Status Mail
                try
                {

                    string from = string.Empty, mailTo = string.Empty, subject = string.Empty, message = string.Empty, eMailsTo = string.Empty;
                    int paxCount = 0;
                    List<string> toList = new List<string>();
                    Hashtable tempTable = new Hashtable();
                    AgentAppConfig clsAppCnf = new AgentAppConfig();
                    clsAppCnf.Source = "HIS";
                    List<AgentAppConfig> appConfigs = clsAppCnf.GetConfigDataBySource();



                    from = ConfigurationManager.AppSettings["fromEmail"];
                    /*-----------------------------------------------
                              Sending Mail To Inventory Email
                     -----------------------------------------------*/
                    try
                    {
                        mailTo = appConfigs.Where(x=>x.AppKey.ToUpper()== "HIS_CONFIRM_EMAIL").Select(y=>y.AppValue).FirstOrDefault();
                        subject = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_CONFIRM_SUBJECT").Select(y => y.AppValue).FirstOrDefault();
                        message = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_CONFIRM_MESSAGE").Select(y => y.AppValue).FirstOrDefault();



                        //mailTo = ConfigurationManager.AppSettings["HIS_Confirm_EMail"];
                        //subject = ConfigurationManager.AppSettings["HIS_Confirm_Subject"];
                        //message = ConfigurationManager.AppSettings["HIS_Confirm_Message"];

                        toList.Add(mailTo);

                        for (int i = 0; i < hotelBooking.Roomtype.Length; i++)
                        {
                            paxCount += hotelBooking.Roomtype[i].AdultCount + hotelBooking.Roomtype[i].ChildCount;
                        }

                        /*--------------------------------------------------------------------------------------------------------
                         *                                      Body content for mail
                         *                                    ------------------------- 
                         * \n Dear Guest, \n\n New Hotel Booking Confirmation No. %confNo% Hotel Name : %hotelName% Check In Date:  
                        %checkInDate%. Check Out Date:  %checkOutDate%. No. Od Rooms:  %rooms% No. Of Pax:  %pax% 
                         ---------------------------------------------------------------------------------------------------------*/


                        tempTable.Add("confNo", "<b>" + hotelBooking.ConfirmationNo + "</b>");
                        tempTable.Add("hotelName", "<b>" + hotelBooking.HotelName + "</b>");
                        tempTable.Add("checkInDate", hotelBooking.StartDate.ToString("dd/MM/yyy"));
                        tempTable.Add("checkOutDate", hotelBooking.EndDate.ToString("dd/MM/yyy"));
                        tempTable.Add("rooms", hotelBooking.NoOfRooms);
                        tempTable.Add("pax", paxCount);

                       Email.Send(from, from, toList, subject, message, tempTable);
                    }
                    catch (Exception exInv)
                    {

                        Audit.Add(EventType.Email, Severity.Normal, 0, "Sending Failed To Inv Email. Error : " + exInv.Message, "");
                    }


                    /*-----------------------------------------------
                              Sending Mail To Supplier Email
                     ------------------------------------------------*/
                    try
                    {
                        toList.Clear();
                        subject = string.Empty;
                        message = string.Empty;
                        subject = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_SUPP_CONFIRM_SUBJECT").Select(y => y.AppValue).FirstOrDefault();
                        message = appConfigs.Where(x => x.AppKey.ToUpper() == "HIS_SUPP_CONFIRM_MESSAGE").Select(y => y.AppValue).FirstOrDefault();

                        //subject = ConfigurationManager.AppSettings["HIS_Supp_Confirm_Subject"];
                        //message = ConfigurationManager.AppSettings["HIS_Supp_Confirm_Message"];
                        try
                        {
                            hotelBooking.ChainCode = GetHotelSupplierEmails(Convert.ToInt32(hotelBooking.HotelCode));
                        }
                        catch { }

                        if (!string.IsNullOrEmpty(hotelBooking.ChainCode))
                        {
                            toList.AddRange(hotelBooking.ChainCode.Split(','));
                        }
                        tempTable.Clear();

                        tempTable.Add("confNo", "<b>" + hotelBooking.ConfirmationNo + "</b>");
                        tempTable.Add("hotelName", "<b>" + hotelBooking.HotelName + "</b>");
                        tempTable.Add("checkInDate", hotelBooking.StartDate.ToString("dd/MM/yyy"));
                        tempTable.Add("checkOutDate", hotelBooking.EndDate.ToString("dd/MM/yyy"));
                        tempTable.Add("rooms", hotelBooking.NoOfRooms);
                        tempTable.Add("pax", paxCount);



                        Email.Send(from, from, toList, subject, message, tempTable);
                    }
                    catch (Exception exSupp)
                    {
                        Audit.Add(EventType.Email, Severity.Normal, 0, "Sending Failed To Supplier Email. Error : " + exSupp.ToString(), "");

                    }

                    /*-----------------------------------------------
                            Saving Booking Status After Mailed
                     ------------------------------------------------*/
                    eMailsTo = mailTo + "," + hotelBooking.ChainCode;
                    AddBookingStatusMail(bookingId, hotelBooking.ConfirmationNo, BookingStatus.Booked, eMailsTo);

                }
                catch (Exception exMail)
                {
                    Audit.Add(EventType.Email, Severity.Normal, 0, "Error : " + exMail.Message, "");

                }
                #endregion


            }
            catch (Exception ex)
            {
                bookingTrans.Rollback();
                throw ex;
            }
            return confirmationRef;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="confirmationNo"></param>
        /// <param name="cancel"></param>
        /// <returns></returns>
        public DataTable CancelBooking(string confirmationNo, bool cancel)
        {
            DataTable dtCancelPolicy = new DataTable();
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@P_ConfNo", confirmationNo);
                paramList[1] = new SqlParameter("@P_Cancel", (cancel ? "1" : "0"));

                dtCancelPolicy = DBGateway.FillDataTableSP("P_HTL_Cancel_Booking", paramList);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(CZInventory)Failed to cancel booking. Error : " + ex.ToString(), "");
            }

            return dtCancelPolicy;
        }

        //Save and Update Booking Status After Mailed <05052016>
        public static void AddBookingStatusMail(int hotelId, string confNo,BookingStatus status, string eMailTo)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[4];
                paramList[0] = new SqlParameter("@hotelId", hotelId);
                paramList[1] = new SqlParameter("@confirmationNo", confNo);
                paramList[2] = new SqlParameter("@status", Convert.ToString(status));
                paramList[3] = new SqlParameter("@email", eMailTo);

                DBGateway.ExecuteNonQuerySP("P_HTL_AddUpdateBookingStatusEmails", paramList);
            }
            catch (Exception ex)
            {
               throw new Exception("Failed To save Booking Status Mail. Error : " + ex.Message);
            }
        }
        
        //Getting Supplier Emails <06/05/2016>
        public static string GetSupplierEmails(string confNo)
        {
            string emailsTo = string.Empty;
            try
            {
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@confirmationNo", confNo);
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP("P_HTL_GetSupplierEmails", paramList, connection);
                if (dataReader.Read())
                {
                    emailsTo = dataReader["Email"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed To Retrieve Supplier Email. Error : " + ex.Message);
            }
            return emailsTo;
        }
        //Getting Supplier Emails <06/05/2016>
        public static string GetHotelSupplierEmails(int hotelId)
        {
            string emailsTo = string.Empty;
            try
            {
                SqlConnection connection = DBGateway.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@hotelId", hotelId);
                SqlDataReader dataReader = DBGateway.ExecuteReaderSP("P_HTL_GetHotelSupplierEmails", paramList, connection);
                if (dataReader.Read())
                {
                    emailsTo = dataReader["eMail1"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed To Retrieve Hotel Supplier Email. Error : " + ex.Message);
            }
            return emailsTo;
        }

    }
}
