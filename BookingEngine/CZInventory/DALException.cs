using System;
using System.Data.SqlClient;

namespace CZInventory
{
    public class DALException : Exception
    {
        /// <param name="message">Custom error message</param>
        /// <param name="sqEx">SqlException caught</param>
        public DALException(String message, SqlException sqEx) : base(message + " : " + sqEx.Message, sqEx) { }
    }
}
