﻿using System;
using System.Data;
using System.Collections.Generic;
using CT.Core;
using CT.BookingEngine;
using System.IO;
using CT.Configuration;
using System.Xml;
using System.Linq;

namespace CZInventory
{
    /// <summary>
    /// Summary description for SearchEngine
    /// </summary>
    public class SearchEngine
    {
        int countryId;
        string xmlPath;
        decimal rateOfExchange = 1;
        public int CountryId
        {
            get { return countryId; }
            set { countryId = value; }
        }
        public List<HotelStaticData> StaticData { get; set; }
        public SearchEngine()
        {
            xmlPath = ConfigurationSystem.HotelConnectConfig["XmlLogPath"];
            xmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";
            try
            {
                if (!Directory.Exists(xmlPath))
                {
                    Directory.CreateDirectory(xmlPath);
                }
            }
            catch { }
        }

        public SearchEngine(string connection)
        {
            //DBGateway.GetConnection().ConnectionString = connection;
            xmlPath = ConfigurationSystem.HotelConnectConfig["XmlLogPath"];

            try
            {
                if (!Directory.Exists(xmlPath))
                {
                    Directory.CreateDirectory(xmlPath);
                }
            }
            catch { }
        }

        void SerializeHotelRequest(HotelRequest request, string filePath)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            XmlWriter writer = XmlTextWriter.Create(filePath, settings);

            //writer.WriteStartDocument(true);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("HotelRequest");
            if (request.Latitude != 0 && request.Longtitude != 0)//  For City search
            {
                writer.WriteElementString("Longtitude", request.Longtitude.ToString());
                writer.WriteElementString("Latitude", request.Latitude.ToString());
                writer.WriteElementString("Radius", request.Radius.ToString());
            }
            else
            {
                writer.WriteElementString("CityId", request.CityId.ToString());
            }
            writer.WriteElementString("StartDate", request.StartDate.ToString("dd/MM/yyyy"));
            writer.WriteElementString("EndDate", request.EndDate.ToString("dd/MM/yyyy"));
            writer.WriteElementString("CityCode", request.CityCode);
            writer.WriteElementString("NoOfRooms", request.NoOfRooms.ToString());
            writer.WriteStartElement("RoomGuest");
            foreach (RoomGuestData guest in request.RoomGuest)
            {

                writer.WriteStartElement("RoomGuestData");
                writer.WriteElementString("noOfAdults", guest.noOfAdults.ToString());
                writer.WriteElementString("noOfChild", guest.noOfChild.ToString());
                writer.WriteStartElement("ChildAges");
                if (guest.childAge != null) { 
                foreach (int age in guest.childAge)
                {
                    writer.WriteElementString("ChildAge", age.ToString());
                }
                }
                writer.WriteEndElement();//ChildAge
                writer.WriteEndElement();//RoomGuestData
            }
            writer.WriteEndElement();//RoomGuest
            writer.WriteElementString("HotelName", request.HotelName);
            writer.WriteElementString("Rating", request.Rating.ToString());
            writer.WriteElementString("ExtraCots", request.ExtraCots.ToString());
            writer.WriteElementString("Currency", request.Currency);
            writer.WriteElementString("AvailabilityType", request.AvailabilityType.ToString());
            writer.WriteStartElement("Sources");
            foreach (string source in request.Sources)
            {
                writer.WriteElementString("Source", source);
            }
            writer.WriteEndElement();//Sources
            writer.WriteElementString("IsDomestic", request.IsDomestic.ToString());
            writer.WriteElementString("SearchByArea", request.SearchByArea.ToString());
            writer.WriteElementString("IsMultiRoom", request.IsMultiRoom.ToString());
            writer.WriteElementString("MinRating", request.MinRating.ToString());
            writer.WriteElementString("MaxRating", request.MaxRating.ToString());
            writer.WriteElementString("RequestMode", request.RequestMode.ToString());
            writer.WriteElementString("PassengerNationality", request.PassengerNationality);
            writer.WriteElementString("RoomRateType", request.RoomRateType.ToString());
            writer.WriteEndElement();//HotelREquest

            writer.Flush();
            writer.Close();
        }


        public List<HotelSearchResult> GetHotels(HotelRequest request,Dictionary<string ,decimal> exchangeRates, string currency, decimal markup, string markupType,string sourceCountryCode, decimal discount, string discountType)
        {

            List<HotelSearchResult> searchResults = new List<HotelSearchResult>();
            try
            {
                string roomPax = "";
                string childAge = "";
                //int child = -1;
                int order = 1;
                //int countryId = 0;
                Dictionary<int, string> Guests = new Dictionary<int, string>();

                foreach (RoomGuestData guest in request.RoomGuest)
                {
                    if (roomPax.Trim().Length > 0)
                    {
                        roomPax += "|" + guest.noOfAdults + "," + guest.noOfChild;
                        Guests.Add(order, guest.noOfAdults + "," + guest.noOfChild);
                    }
                    else
                    {
                        roomPax = guest.noOfAdults + "," + guest.noOfChild;
                        Guests.Add(order, guest.noOfAdults + "," + guest.noOfChild);
                    }

                    order++;
                    if (guest.noOfChild > 0)
                    {
                        for (int i = 0; i < guest.childAge.Count; i++)
                        {
                            int age = guest.childAge[i];
                            if (childAge.Trim().Length > 0 && !childAge.EndsWith("|"))
                            {
                                childAge += "," + age;
                            }
                            else
                            {
                                childAge += age.ToString();
                            }
                        }

                        //if (!childAge.Contains("|"))
                        {
                            childAge += "|";
                        }
                    }
                    else
                    {
                        if (childAge.Trim().Length > 0 && childAge.EndsWith("|"))
                        {
                            childAge += "0|";
                        }
                        else
                        {
                            childAge += "0|";
                        }
                    }
                }

                if (childAge.EndsWith("|"))
                {
                    childAge = childAge.Remove(childAge.Length - 1, 1);
                }

                try
                {
                    //XmlSerializer serializer = new XmlSerializer(typeof(HotelRequest));
                    string filePath = @"" + xmlPath + "HotelInventorySearchRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                    //StreamWriter sw = new StreamWriter(filePath);
                    //serializer.Serialize(sw, request);
                    //sw.Flush();
                    //sw.Close();                                       

                    SerializeHotelRequest(request, filePath);
                }
                catch { }

                InvBooking booking = new InvBooking();
                if(request.Longtitude==0 && request.Latitude==0)
                {
                     HotelCity hCity = HotelCity.Load(!string.IsNullOrEmpty(request.CityName) ? HotelCity.GetCityIdFromCityName(request.CityName) : 0);
                    request.CityId =!string.IsNullOrEmpty(hCity.HISCode)? Convert.ToInt32(hCity.HISCode):0;
                }
                DataSet dsSearchResults = booking.GetInventoryHotels(request);

                if (dsSearchResults != null && dsSearchResults.Tables.Count > 0)
                {
                    DataTable dtSearchResults = dsSearchResults.Tables[0];
                    DataTable dtHotelRooms = dsSearchResults.Tables[1];
                    DataTable dtRoomRates = dsSearchResults.Tables[2];
                    DataTable dtRoomFacilities = dsSearchResults.Tables[3];
                    DataTable dtHotelFacilities = dsSearchResults.Tables[4];
                    //DataTable dtHotelCancelPolicy = dsSearchResults.Tables[5];
                    DataTable dtChilds = dsSearchResults.Tables[5];

                    string[] ages = childAge.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    dtChilds.Clear();
                    foreach (string age in ages)
                    {
                        string[] pax = age.Split(',');
                        DataRow row = dtChilds.NewRow();
                        for (int i = 0; i < pax.Length; i++)
                        {
                            row[i] = pax[i];
                        }
                        dtChilds.Rows.Add(row);
                    }


                    HotelSearchResult result = null;

                    HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                    //HotelRoomsDetails[] roomDetails = new HotelRoomsDetails[dtSearchResults.Rows.Count];
                    //List<HotelRoomsDetails> roomDetails = new List<HotelRoomsDetails>();// ziyad
                    PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());

                    foreach (DataRow row in dtSearchResults.Rows)
                    {
                        DataRow[] rooms = null;
                        if (!FindHotel(ref searchResults, row["Hotel_Name"].ToString()))
                        {
                            result = new HotelSearchResult();
                        }
                        else
                        {
                            result = searchResults.Find(x => x.HotelName.Equals(row["Hotel_Name"].ToString()));
                            continue;
                        }
                        result.HotelName = row["Hotel_Name"].ToString();
                        result.HotelPicture = ConfigurationSystem.HotelConnectConfig["imgPathForServer"]+row["Hotel_Picture"].ToString();
                        result.HotelAddress = row["Hotel_Address"].ToString();
                        result.HotelCode = row["Hotel_Id"].ToString();
                        result.EndDate = Convert.ToDateTime(row["To_Date"]);
                        result.Currency = row["Currency"].ToString();
                        result.HotelDescription = row["Hotel_Description"].ToString();
                        result.HotelLocation = row["Hotel_Location"].ToString();
                        result.HotelMap = row["Hotel_Map"].ToString();
                        result.IsFeaturedHotel = (row["Is_Preferred"].ToString() == "Y" ? true : false);
                        result.RateType = RateType.Negotiated;
                        result.Rating = GetHotelRating(row["Star_Level"].ToString());
                        result.StartDate = Convert.ToDateTime(row["From_Date"]);
                        result.RoomGuest = request.RoomGuest;
                        result.CityCode = request.CityCode;
                        List<HotelStaticData> hotelStaticData = new List<HotelStaticData>();
                        if (!string.IsNullOrEmpty(result.HotelCode))
                        {
                            HotelStaticData staticInfo = new HotelStaticData();
                            hotelStaticData = staticInfo.StaticDataLoad(string.Empty, string.Empty, result.HotelCode, (int)HotelBookingSource.HotelConnect);
                            if (hotelStaticData != null && hotelStaticData.Count > 0)
                            {
                                result.HotelStaticID = hotelStaticData[0].HotelStaticID;
                            }
                        }


                        result.HotelFacilities = new List<string>();
                        if (dtHotelFacilities != null)
                        {
                            List<string> Facilities = new List<string>();
                            foreach (DataRow dr in dtHotelFacilities.Rows)
                            {
                                if (dr["Hotel_Id"].ToString() == row["Hotel_Id"].ToString())
                                {
                                    Facilities.Add(dr["FacilityName"].ToString());
                                }
                            }
                            result.HotelFacilities = Facilities;
                        }
                        if (exchangeRates.ContainsKey(result.Currency))
                        {
                            rateOfExchange = exchangeRates[result.Currency];
                            result.Currency = currency;
                        }
                        else
                        {
                            result.Currency = request.Currency;
                        }
                         rooms = dtHotelRooms.Select("HotelId=" + row["Hotel_Id"]+ " AND PeriodId=" + row["Period_Id"] , "RoomId");
                        //roomDetails = null;
                        List<HotelRoomsDetails> roomDetails = new List<HotelRoomsDetails>();
                        if (rooms != null && rooms.Length > 0)
                        {
                            List<DataRow> availRooms = new List<DataRow>();
                            int days = request.EndDate.Subtract(request.StartDate).Days;
                            DateTime searchDate = Convert.ToDateTime(dtSearchResults.Rows[0]["From_Date"]);
                            foreach (DataRow room in rooms)
                            {
                                int dayCount = 0;
                                for (int i = 0; i < days; i++)
                                {
                                    searchDate.AddDays(i);

                                    if (booking.GetQuota(Convert.ToInt32(room["HotelId"]), Convert.ToInt32(room["PeriodId"]), Convert.ToInt32(room["RoomId"]), Convert.ToInt32(room["AccomId"]), searchDate, Convert.ToDateTime(dtSearchResults.Rows[0]["To_Date"])) >= request.NoOfRooms)
                                    {
                                        dayCount++;
                                    }
                                    else
                                    {
                                        dayCount--;
                                    }
                                }
                                if (dayCount == days)
                                {
                                    availRooms.Add(room);
                                }
                            }

                            rooms = availRooms.ToArray();

                            
                            //foreach (DataRow room in rooms)
                            availRooms.ForEach(room =>
                            {
                                string roomImages = booking.GetRoomImagesAndChilds(Convert.ToInt32(room["RoomId"]));
                                int Childs = Convert.ToInt32(roomImages.Split('|')[0]);
                                string[] images = roomImages.Split('|')[1].Split(',');
                                List<string> HotelImages = new List<string>();
                                string imagePath = ConfigurationSystem.HotelConnectConfig["imgPathForServer"];
                                if (images.Length > 0 && images[0].Length > 0 && !result.HotelPicture.Contains(images[0]))
                                {
                                    HotelImages.Add(imagePath + "RoomImages/" + images[0]);
                                    // result.HotelPicture += ",RoomImages/" + images[0];
                                }
                                if (images.Length > 1 && images[1].Length > 0 && !result.HotelPicture.Contains(images[1]))
                                {
                                    HotelImages.Add(imagePath + "RoomImages/" + images[1]);
                                    // result.HotelPicture += ",RoomImages/" + images[1];
                                }
                                if (images.Length > 2 && images[2].Length > 0 && !result.HotelPicture.Contains(images[2]))
                                {
                                    HotelImages.Add(imagePath + "RoomImages/" + images[2]);
                                    // result.HotelPicture += ",RoomImages/" + images[2];
                                }
                                result.HotelImages = HotelImages;
                                //if (RoomQuota.GetQuota(Convert.ToInt32(room["HotelId"]), Convert.ToInt32(room["PeriodId"]), Convert.ToInt32(room["RoomId"]), Convert.ToInt32(room["AccomId"]), Convert.ToDateTime(dtSearchResults.Rows[0]["From_Date"]), Convert.ToDateTime(dtSearchResults.Rows[0]["To_Date"])) > 0)
                                {
                                    for (int i = 0; i < request.RoomGuest.Length; i++)
                                    {
                                        DataRow drChild = null;
                                        int age = 0;
                                        int age2 = 0, age3 = 0, age4 = 0, age5 = 0, age6 = 0;
                                        if (dtChilds.Rows.Count > 0)
                                        {
                                            drChild = dtChilds.Rows[i];

                                            if (drChild["Child1"] != DBNull.Value)
                                            {
                                                age = Convert.ToInt32(drChild["Child1"]);
                                            }
                                            if (drChild["Child2"] != DBNull.Value && request.RoomGuest[i].noOfChild >= 2)
                                            {
                                                age2 = Convert.ToInt32(drChild["Child2"]);
                                            }

                                            if (drChild["Child3"] != DBNull.Value && request.RoomGuest[i].noOfChild >= 3)
                                            {
                                                age3 = Convert.ToInt32(drChild["Child3"]);
                                            }
                                            if (drChild["Child4"] != DBNull.Value && request.RoomGuest[i].noOfChild >= 4)
                                            {
                                                age4 = Convert.ToInt32(drChild["Child4"]);
                                            }
                                            if (drChild["Child5"] != DBNull.Value && request.RoomGuest[i].noOfChild >= 5)
                                            {
                                                age5 = Convert.ToInt32(drChild["Child5"]);
                                            }
                                            if (drChild["Child6"] != DBNull.Value && request.RoomGuest[i].noOfChild >= 6)
                                            {
                                                age6 = Convert.ToInt32(drChild["Child6"]);
                                            }
                                        }

                                        if (Convert.ToInt32(room["room_adult"]) == request.RoomGuest[i].noOfAdults && Convert.ToInt32(room["room_child"]) == request.RoomGuest[i].noOfChild)
                                        {
                                            int fromAge1 = Convert.ToInt32(room["room_child1_age_range"].ToString().Split(',')[0]);
                                            int toAge1 = Convert.ToInt32(room["room_child1_age_range"].ToString().Split(',')[1]);

                                            int fromAge2 = 0;
                                            int toAge2 = 0;
                                            if (room["room_child2_age_range"] != DBNull.Value)
                                            {
                                                fromAge2 = Convert.ToInt32(room["room_child2_age_range"].ToString().Split(',')[0]);
                                                toAge2 = Convert.ToInt32(room["room_child2_age_range"].ToString().Split(',')[1]);
                                            }

                                            int fromAge3 = 0;
                                            int toAge3 = 0;
                                            if (room["room_child3_age_range"] != DBNull.Value)
                                            {
                                                fromAge3 = Convert.ToInt32(room["room_child3_age_range"].ToString().Split(',')[0]);
                                                toAge3 = Convert.ToInt32(room["room_child3_age_range"].ToString().Split(',')[1]);
                                            }


                                            //for (int d = 0; d < days; d++)
                                            decimal childAmount = 0, totalAmount = 0;
                                            DateTime nextDay = result.StartDate.AddDays(0);
                                            DayOfWeek weekday = nextDay.DayOfWeek;
                                            //DataRow[] rates = dtRoomRates.Select("RoomId = " + room["RoomId"] + " AND PeriodId=" + room["PeriodId"] + " AND AccomId=" + room["AccomId"] + " AND Adults=" + request.RoomGuest[i].noOfAdults + " AND Childs=" + request.RoomGuest[i].noOfChild);//+ " AND WeekDay='" + weekday.ToString().Substring(0, 3) + "'");                                            
                                            var rates = (from rate in dtRoomRates.AsEnumerable().Where(x => x.Field<int>("RoomId") == Convert.ToInt32(room["RoomId"]) 
                                                        && x.Field<int>("PeriodId") == Convert.ToInt32(room["PeriodId"]) 
                                                        && x.Field<int>("AccomId") == Convert.ToInt32(room["AccomId"])
                                                        && x.Field<int>("Adults") == request.RoomGuest[0].noOfAdults
                                                        && x.Field<int>("Childs") == request.RoomGuest[0].noOfChild) select rate).ToList();

                                            //for (int r = 0; r < rates.Length; r++)
                                            rates.ForEach(rate =>
                                            {
                                                //DataRow rate = rates[r];

                                                //if (request.RoomGuest[i].noOfAdults == Convert.ToInt32(rate["Adults"]) && request.RoomGuest[i].noOfChild == Convert.ToInt32(rate["Childs"]))
                                                //{
                                                //    if (request.RoomGuest[i].noOfChild > 0)
                                                //    {
                                                //        if (!request.RoomGuest[i].childAge.Contains(age))
                                                //        {
                                                //            if (!request.RoomGuest[i].childAge.Contains(age2))
                                                //            {
                                                //                if (!request.RoomGuest[i].childAge.Contains(age3))
                                                //                {
                                                //                    if (!request.RoomGuest[i].childAge.Contains(age4))
                                                //                    {
                                                //                        if (!request.RoomGuest[i].childAge.Contains(age5))
                                                //                        {
                                                //                            if (!request.RoomGuest[i].childAge.Contains(age6))
                                                //                            {
                                                //                                break;
                                                //                            }
                                                //                        }
                                                //                    }
                                                //                }
                                                //            }
                                                //        }
                                                //    }
                                                //    else
                                                //    {
                                                //        break;
                                                //    }
                                                //}
                                                //decimal childRate = 0;

                                                //if (rate["Weekday"].ToString() == weekday.ToString().Substring(0, 3))
                                                if (Convert.ToDecimal(rate["ChildRateApplied"]) <= 0)
                                                {
                                                    if (age > 0 && Childs >= 1)
                                                    {
                                                        if (age >= fromAge1 && age <= toAge1)
                                                        {
                                                            ReadRoomRate(1, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                        else if (age >= fromAge2 && age <= toAge2)
                                                        {
                                                            ReadRoomRate(2, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                        else if (age >= fromAge3 && age <= toAge3)
                                                        {
                                                            ReadRoomRate(3, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                    }
                                                    if (age2 > 0 && Childs >= 2)
                                                    {
                                                        if (age2 >= fromAge1 && age2 <= toAge1)
                                                        {
                                                            ReadRoomRate(1, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                        else if (age2 >= fromAge2 && age2 <= toAge2)
                                                        {
                                                            ReadRoomRate(2, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                        else if (age2 >= fromAge3 && age2 <= toAge3)
                                                        {
                                                            ReadRoomRate(3, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                    }
                                                    if (age3 > 0 && Childs >= 3)
                                                    {
                                                        if (age3 >= fromAge1 && age3 <= toAge1)
                                                        {
                                                            ReadRoomRate(1, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                        else if (age3 >= fromAge2 && age3 <= toAge2)
                                                        {
                                                            ReadRoomRate(2, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                        else if (age3 >= fromAge3 && age3 <= toAge3)
                                                        {
                                                            ReadRoomRate(3, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                    }
                                                    if (age4 > 0 && Childs >= 4)
                                                    {
                                                        if (age4 >= fromAge1 && age4 <= toAge1)
                                                        {
                                                            ReadRoomRate(1, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                        else if (age4 >= fromAge2 && age4 <= toAge2)
                                                        {
                                                            ReadRoomRate(2, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                        else if (age4 >= fromAge3 && age4 <= toAge3)
                                                        {
                                                            ReadRoomRate(3, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                    }
                                                    if (age5 > 0 && Childs >= 5)
                                                    {
                                                        if (age5 >= fromAge1 && age5 <= toAge1)
                                                        {
                                                            ReadRoomRate(1, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                        else if (age5 >= fromAge2 && age5 <= toAge2)
                                                        {
                                                            ReadRoomRate(2, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                        else if (age5 >= fromAge3 && age5 <= toAge3)
                                                        {
                                                            ReadRoomRate(3, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                    }
                                                    if (age6 > 0 && Childs >= 6)
                                                    {
                                                        if (age6 >= fromAge1 && age6 <= toAge1)
                                                        {
                                                            ReadRoomRate(1, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                        else if (age6 >= fromAge2 && age6 <= toAge2)
                                                        {
                                                            ReadRoomRate(2, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                        else if (age6 >= fromAge3 && age6 <= toAge3)
                                                        {
                                                            ReadRoomRate(3, ref rate, ref childAmount, ref totalAmount);
                                                        }
                                                    }
                                                }//ChildRateApplied

                                            });//Rates loop
                                        }//adults & childs match
                                    }//room guest loop

                                    
                                    //if (RoomQuota.GetQuota(Convert.ToInt32(row1["HotelId"]), Convert.ToInt32(row1["PeriodId"]), Convert.ToInt32(row1["RoomId"]), Convert.ToInt32(row1["AccomId"]), Convert.ToDateTime(dtSearchResults.Rows[0]["From_Date"]), Convert.ToDateTime(dtSearchResults.Rows[0]["To_Date"])) > 0)
                                    {
                                        RoomRates[] roomRates = new RoomRates[days];

                                        RoomRates roomRate = new RoomRates();
                                        int k = 0;
                                        //DataRow[] Rates = dtRoomRates.Select("HotelId = " + row["Hotel_Id"] + " AND RoomId=" + room["RoomId"] + " AND AccomId=" + room["AccomId"] + " AND Adults=" + room["room_adult"] + " AND Childs=" + room["room_child"]);
                                        var Rates = (from rate in dtRoomRates.AsEnumerable().Where(x => x.Field<int>("RoomId") == Convert.ToInt32(room["RoomId"])
                                                        && x.Field<int>("AccomId") == Convert.ToInt32(room["AccomId"])
                                                        && x.Field<int>("Adults") == Convert.ToInt32(room["room_adult"])
                                                        && x.Field<int>("Childs") == Convert.ToInt32(room["room_child"]))
                                                     select rate).ToList();
                                        //need to check for all days having rates
                                        if (days != Rates.Count)
                                        {
                                            return;
                                        }
                                        roomDetail = new HotelRoomsDetails();
                                        roomDetail.RoomTypeName = room["RoomName"].ToString();
                                        roomDetail.Amenities = new List<string>();
                                        roomDetail.CancellationPolicy = "";
                                        roomDetail.RatePlanCode = "";
                                        roomDetail.Occupancy = new Dictionary<string, int>();
                                        roomDetail.RoomTypeCode = room["HotelId"].ToString() + "-" + room["RoomId"].ToString() + "-" + room["PeriodId"].ToString() + "-" + room["AccomId"].ToString() + "-" + room["room_adult"].ToString() + "-" + room["room_child"].ToString();
                                        roomDetail.Occupancy = new Dictionary<string, int>();
                                        roomDetail.mealPlanDesc = room["accomodation"].ToString();
                                        roomDetail.SupplierName = HotelBookingSource.HotelConnect.ToString();
                                        roomDetail.SupplierId = Convert.ToString((int)HotelBookingSource.HotelConnect);
                                        roomDetail.IsIndividualSelection = true;//Here IsIndividualSelection = true means roomselection is individual.

                                        //RoomRates[] roomRates = new RoomRates[days];

                                        //RoomRates roomRate = new RoomRates();
                                        //int k = 0;
                                        //DataRow[] Rates = dtRoomRates.Select("HotelId = " + row["Hotel_Id"] + " AND RoomId=" + room["RoomId"] + " AND AccomId=" + room["AccomId"] + " AND Adults=" + room["room_adult"] + " AND Childs=" + room["room_child"]);
                                        ////need to check for all days having rates
                                        //if (days!=Rates.Length)
                                        //{
                                        //    continue;
                                        //}
                                        for (int i = 0; i < days; i++)
                                        {
                                            DateTime nextDay = result.StartDate.AddDays(i);
                                            DayOfWeek weekday = nextDay.DayOfWeek;
                                            // DataRow[] Rates = dtRoomRates.Select("HotelId = " + row["Hotel_Id"] + " AND RoomId=" + room["RoomId"] + " AND AccomId=" + room["AccomId"] + " AND Adults=" + room["room_adult"] + " AND Childs=" + room["room_child"]);// + " AND WeekDay='" + weekday.ToString().Substring(0, 3) + "'");
                                            //for (; k < Rates.Length; )
                                            {
                                                DataRow dr = Rates[i];

                                                if (!roomDetail.Occupancy.ContainsKey("ExtraBed"))
                                                {
                                                    if (dr["ExtraBed"].ToString() == "Y")
                                                    {
                                                        roomDetail.Occupancy.Add("ExtraBed", 1);
                                                    }
                                                    else
                                                    {
                                                        roomDetail.Occupancy.Add("ExtraBed", 0);
                                                    }
                                                }
                                                //if (dr["WeekDay"].ToString() == weekday.ToString().Substring(0, 3))
                                                //if(room["RoomId"].ToString() == dr["RoomId"].ToString() && room["AccomId"].ToString() == dr["AccomId"].ToString())
                                                {
                                                    decimal rate = 0;

                                                    rate = Convert.ToDecimal(dr["TotalRate"]) + Convert.ToDecimal(dr["ExtraBedRate"]) + Convert.ToDecimal(dr["ChildExtraBedRate"]);
                                                    if (rate == 0)
                                                    {
                                                        rate = Convert.ToDecimal(dr["Rate"]);
                                                    }

                                                    roomRate.SellingFare = rate * rateOfExchange;
                                                    roomRate.RateType = RateType.Negotiated;
                                                    roomRate.BaseFare = rate;
                                                    roomRate.Amount = rate;
                                                    roomRate.Totalfare = roomRate.SellingFare;
                                                    roomRate.Days = request.StartDate.AddDays(i);
                                                    roomDetail.SellingFare += roomRate.SellingFare;
                                                    roomDetail.TotalPrice += roomRate.SellingFare;
                                                    roomDetail.Markup = Convert.ToDecimal(dr["TaxRate"]);
                                                    roomDetail.MarkupType = dr["TaxType"].ToString();
                                                }
                                                //break;
                                            }
                                            k++;
                                            roomRates[i] = roomRate;
                                        }

                                        DataRow[] facilities = dtRoomFacilities.Select("Room_Id=" + room["RoomId"].ToString());

                                        if (facilities != null)
                                        {
                                            List<string> amenities = new List<string>();
                                            foreach (DataRow rowA in facilities)
                                            {
                                                amenities.Add(rowA["FacilityName"].ToString());
                                            }
                                            roomDetail.Amenities = amenities;
                                        }

                                        /*===================================================================================================================
                                         *                   If any child age combination is not present in any of the room 
                                         *                  make the Sequence of that to ZERO so that it cannot be displayed 
                                         *===================================================================================================================*/
                                        for (int m = 0; m < dtChilds.Rows.Count; m++)
                                        {
                                            DataRow drChild = null;
                                            int age = 0;
                                            int age2 = 0, age3 = 0, age4 = 0, age5 = 0, age6 = 0;
                                            
                                            
                                                drChild = dtChilds.Rows[m];

                                                if (drChild["Child1"] != DBNull.Value)
                                                {
                                                    age = Convert.ToInt32(drChild["Child1"]);
                                                }
                                                if (drChild["Child2"] != DBNull.Value)
                                                {
                                                    age2 = Convert.ToInt32(drChild["Child2"]);
                                                }

                                                if (drChild["Child3"] != DBNull.Value)
                                                {
                                                    age3 = Convert.ToInt32(drChild["Child3"]);
                                                }
                                                if (drChild["Child4"] != DBNull.Value)
                                                {
                                                    age4 = Convert.ToInt32(drChild["Child4"]);
                                                }
                                                if (drChild["Child5"] != DBNull.Value)
                                                {
                                                    age5 = Convert.ToInt32(drChild["Child5"]);
                                                }
                                                if (drChild["Child6"] != DBNull.Value)
                                                {
                                                    age6 = Convert.ToInt32(drChild["Child6"]);
                                                }
                                            


                                            int fromAge1 = Convert.ToInt32(room["room_child1_age_range"].ToString().Split(',')[0]);
                                            int toAge1 = Convert.ToInt32(room["room_child1_age_range"].ToString().Split(',')[1]);

                                            int fromAge2 = 0;
                                            int toAge2 = 0;
                                            if (room["room_child2_age_range"] != DBNull.Value)
                                            {
                                                fromAge2 = Convert.ToInt32(room["room_child2_age_range"].ToString().Split(',')[0]);
                                                toAge2 = Convert.ToInt32(room["room_child2_age_range"].ToString().Split(',')[1]);
                                            }

                                            int fromAge3 = 0;
                                            int toAge3 = 0;
                                            if (room["room_child3_age_range"] != DBNull.Value)
                                            {
                                                fromAge3 = Convert.ToInt32(room["room_child3_age_range"].ToString().Split(',')[0]);
                                                toAge3 = Convert.ToInt32(room["room_child3_age_range"].ToString().Split(',')[1]);
                                            }
                                            if (room["Rank"].ToString() == (m + 1).ToString())
                                            {
                                                if (Convert.ToInt32(room["room_child"]) > 0)
                                                {
                                                    if (age > 0 && age >= fromAge1 && age <= toAge1 || (age >= fromAge2 && age <= toAge2) || (age >= fromAge3 && age <= toAge3))
                                                    {
                                                        if (Convert.ToInt32(room["room_child"]) > 1)
                                                        {
                                                            if (age2 > 0)
                                                            {
                                                                if (age2 >= fromAge1 && age2 <= toAge1 || (age2 >= fromAge2 && age2 <= toAge2) || (age2 >= fromAge3 && age2 <= toAge3))
                                                                {
                                                                    if (Convert.ToInt32(room["room_child"]) > 2)
                                                                    {
                                                                        if (age3 > 0)
                                                                        {
                                                                            if (age3 >= fromAge1 && age3 <= toAge1 || (age3 >= fromAge2 && age3 <= toAge2) || (age3 >= fromAge3 && age3 <= toAge3))
                                                                            {
                                                                                if (Convert.ToInt32(room["room_child"]) > 3)
                                                                                {
                                                                                    if (age4 > 0)
                                                                                    {
                                                                                        if (age4 >= fromAge1 && age4 <= toAge1 || (age4 >= fromAge2 && age4 <= toAge2) || (age4 >= fromAge3 && age4 <= toAge3))
                                                                                        {
                                                                                            if (Convert.ToInt32(room["room_child"]) > 4)
                                                                                            {
                                                                                                if (age5 > 0)
                                                                                                {
                                                                                                    if (age5 >= fromAge1 && age5 <= toAge1 || (age5 >= fromAge2 && age5 <= toAge2) || (age5 >= fromAge3 && age5 <= toAge3))
                                                                                                    {
                                                                                                        if (Convert.ToInt32(room["room_child"]) > 5)
                                                                                                        {
                                                                                                            if (age6 > 0)
                                                                                                            {
                                                                                                                if (age6 >= fromAge1 && age6 <= toAge1 || (age6 >= fromAge2 && age6 <= toAge2) || (age6 >= fromAge3 && age6 <= toAge3))
                                                                                                                {
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    room["Rank"] = 0;
                                                                                                                }
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                room["Rank"] = 0;
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        room["Rank"] = 0;
                                                                                                    }
                                                                                                }
                                                                                                else if (age6 > 0)
                                                                                                {
                                                                                                    if (age6 >= fromAge1 && age6 <= toAge1 || (age6 >= fromAge2 && age6 <= toAge2) || (age6 >= fromAge3 && age6 <= toAge3))
                                                                                                    {
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        room["Rank"] = 0;
                                                                                                    }
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    room["Rank"] = 0;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            room["Rank"] = 0;
                                                                                        }
                                                                                    }
                                                                                    else if (age5 > 0)
                                                                                    {
                                                                                        if (age5 >= fromAge1 && age5 <= toAge1 || (age5 >= fromAge2 && age5 <= toAge2) || (age5 >= fromAge3 && age5 <= toAge3))
                                                                                        {
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            room["Rank"] = 0;
                                                                                        }
                                                                                    }
                                                                                    else if (age6 > 0)
                                                                                    {
                                                                                        if (age6 >= fromAge1 && age6 <= toAge1 || (age6 >= fromAge2 && age6 <= toAge2) || (age6 >= fromAge3 && age6 <= toAge3))
                                                                                        {
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            room["Rank"] = 0;
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        room["Rank"] = 0;
                                                                                    }
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                room["Rank"] = 0;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    room["Rank"] = 0;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else if (age2 > 0)
                                                    {
                                                        if (age2 >= fromAge1 && age2 <= toAge1 || (age2 >= fromAge2 && age2 <= toAge2) || (age2 >= fromAge3 && age2 <= toAge3))
                                                        {
                                                        }
                                                        else
                                                        {
                                                            room["Rank"] = 0;
                                                        }
                                                    }
                                                    else if (age3 > 0)
                                                    {
                                                        if (age3 >= fromAge1 && age3 <= toAge1 || (age3 >= fromAge2 && age3 <= toAge2) || (age3 >= fromAge3 && age3 <= toAge3))
                                                        {
                                                        }
                                                        else
                                                        {
                                                            room["Rank"] = 0;
                                                        }
                                                    }
                                                    else if (age4 > 0)
                                                    {
                                                        if (age4 >= fromAge1 && age4 <= toAge1 || (age4 >= fromAge2 && age4 <= toAge2) || (age4 >= fromAge3 && age4 <= toAge3))
                                                        {
                                                        }
                                                        else
                                                        {
                                                            room["Rank"] = 0;
                                                        }
                                                    }
                                                    else if (age5 > 0)
                                                    {
                                                        if (age5 >= fromAge1 && age5 <= toAge1 || (age5 >= fromAge2 && age5 <= toAge2) || (age5 >= fromAge3 && age5 <= toAge3))
                                                        {
                                                        }
                                                        else
                                                        {
                                                            room["Rank"] = 0;
                                                        }
                                                    }
                                                    else if (age6 > 0)
                                                    {
                                                        if (age6 >= fromAge1 && age6 <= toAge1 || (age6 >= fromAge2 && age6 <= toAge2) || (age6 >= fromAge3 && age6 <= toAge3))
                                                        {
                                                        }
                                                        else
                                                        {
                                                            room["Rank"] = 0;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        room["Rank"] = 0;
                                                    }
                                                }
                                            }
                                        }

                                        if (Convert.ToInt32(room["Rank"]) > request.NoOfRooms)
                                        {
                                            roomDetail.SequenceNo = (Convert.ToInt32(room["Rank"]) - request.NoOfRooms).ToString();
                                        }
                                        else
                                        {
                                            roomDetail.SequenceNo = room["Rank"].ToString();
                                        }

                                        roomDetail.Rates = roomRates;
                                        roomDetails.Add(roomDetail);
                                    }
                                }
                            });

                            #region Repetetive Code moved in above loop
                            //int k = 0;
                            //roomDetails = new HotelRoomsDetails[rooms.Length];
                            //int prevRoom = (rooms.Length > 0 ? Convert.ToInt32(rooms[0]["RoomId"]) : 0);

                            //Dictionary<string, int> maxRoomSequence = new Dictionary<string, int>();
                            //for (int j = 0; j < rooms.Length; j++)
                            //{
                            //    //int seq = 0;    
                            //    DataRow row1 = rooms[j];
                            //    //if (RoomQuota.GetQuota(Convert.ToInt32(row1["HotelId"]), Convert.ToInt32(row1["PeriodId"]), Convert.ToInt32(row1["RoomId"]), Convert.ToInt32(row1["AccomId"]), Convert.ToDateTime(dtSearchResults.Rows[0]["From_Date"]), Convert.ToDateTime(dtSearchResults.Rows[0]["To_Date"])) > 0)
                            //    {
                            //        RoomRates[] roomRates = new RoomRates[days];

                            //        RoomRates roomRate = new RoomRates();
                            //        int k = 0;
                            //        DataRow[] Rates = dtRoomRates.Select("HotelId = " + row["Hotel_Id"] + " AND RoomId=" + row1["RoomId"] + " AND AccomId=" + row1["AccomId"] + " AND Adults=" + row1["room_adult"] + " AND Childs=" + row1["room_child"]);
                            //        //need to check for all days having rates
                            //        if (days != Rates.Length)
                            //        {
                            //            continue;
                            //        }
                            //        roomDetail = new HotelRoomsDetails();
                            //        roomDetail.RoomTypeName = row1["RoomName"].ToString();
                            //        roomDetail.Amenities = new List<string>();
                            //        roomDetail.CancellationPolicy = "";
                            //        roomDetail.RatePlanCode = "";
                            //        roomDetail.Occupancy = new Dictionary<string, int>();
                            //        roomDetail.RoomTypeCode = row1["HotelId"].ToString() + "-" + row1["RoomId"].ToString() + "-" + row1["PeriodId"].ToString() + "-" + row1["AccomId"].ToString() + "-" + row1["room_adult"].ToString() + "-" + row1["room_child"].ToString();
                            //        roomDetail.Occupancy = new Dictionary<string, int>();
                            //        roomDetail.mealPlanDesc = row1["accomodation"].ToString();
                            //        roomDetail.SupplierName = HotelBookingSource.HotelConnect.ToString();
                            //        roomDetail.SupplierId = Convert.ToString((int)HotelBookingSource.HotelConnect);
                            //        roomDetail.IsIndividualSelection = true;//Here IsIndividualSelection = true means roomselection is individual.

                            //        //RoomRates[] roomRates = new RoomRates[days];

                            //        //RoomRates roomRate = new RoomRates();
                            //        //int k = 0;
                            //        //DataRow[] Rates = dtRoomRates.Select("HotelId = " + row["Hotel_Id"] + " AND RoomId=" + row1["RoomId"] + " AND AccomId=" + row1["AccomId"] + " AND Adults=" + row1["room_adult"] + " AND Childs=" + row1["room_child"]);
                            //        ////need to check for all days having rates
                            //        //if (days!=Rates.Length)
                            //        //{
                            //        //    continue;
                            //        //}
                            //        for (int i = 0; i < days; i++)
                            //        {
                            //            DateTime nextDay = result.StartDate.AddDays(i);
                            //            DayOfWeek weekday = nextDay.DayOfWeek;
                            //           // DataRow[] Rates = dtRoomRates.Select("HotelId = " + row["Hotel_Id"] + " AND RoomId=" + row1["RoomId"] + " AND AccomId=" + row1["AccomId"] + " AND Adults=" + row1["room_adult"] + " AND Childs=" + row1["room_child"]);// + " AND WeekDay='" + weekday.ToString().Substring(0, 3) + "'");
                            //            //for (; k < Rates.Length; )
                            //            {
                            //                DataRow dr = Rates[i];

                            //                if (!roomDetail.Occupancy.ContainsKey("ExtraBed"))
                            //                {
                            //                    if (dr["ExtraBed"].ToString() == "Y")
                            //                    {
                            //                        roomDetail.Occupancy.Add("ExtraBed", 1);
                            //                    }
                            //                    else
                            //                    {
                            //                        roomDetail.Occupancy.Add("ExtraBed", 0);
                            //                    }
                            //                }
                            //                //if (dr["WeekDay"].ToString() == weekday.ToString().Substring(0, 3))
                            //                //if(row1["RoomId"].ToString() == dr["RoomId"].ToString() && row1["AccomId"].ToString() == dr["AccomId"].ToString())
                            //                {
                            //                    decimal rate = 0;

                            //                    rate = Convert.ToDecimal(dr["TotalRate"]) + Convert.ToDecimal(dr["ExtraBedRate"]) + Convert.ToDecimal(dr["ChildExtraBedRate"]);
                            //                    if (rate == 0)
                            //                    {
                            //                        rate = Convert.ToDecimal(dr["Rate"]);
                            //                    }

                            //                    roomRate.SellingFare = rate * rateOfExchange;
                            //                    roomRate.RateType = RateType.Negotiated;
                            //                    roomRate.BaseFare = rate;
                            //                    roomRate.Amount = rate;
                            //                    roomRate.Totalfare = roomRate.SellingFare;
                            //                    roomRate.Days = request.StartDate.AddDays(i);
                            //                    roomDetail.SellingFare += roomRate.SellingFare;
                            //                    roomDetail.TotalPrice += roomRate.SellingFare;                                                
                            //                    roomDetail.Markup = Convert.ToDecimal(dr["TaxRate"]);
                            //                    roomDetail.MarkupType = dr["TaxType"].ToString();
                            //                }
                            //                //break;
                            //            }
                            //            k++;
                            //            roomRates[i] = roomRate;
                            //        }

                            //        DataRow[] facilities = dtRoomFacilities.Select("Room_Id=" + row1["RoomId"].ToString());

                            //        if (facilities != null)
                            //        {
                            //            List<string> amenities = new List<string>();
                            //            foreach (DataRow rowA in facilities)
                            //            {
                            //                amenities.Add(rowA["FacilityName"].ToString());
                            //            }
                            //            roomDetail.Amenities = amenities;
                            //        }

                            //        /*===================================================================================================================
                            //         *                   If any child age combination is not present in any of the room 
                            //         *                  make the Sequence of that to ZERO so that it cannot be displayed 
                            //         *===================================================================================================================*/
                            //        for (int m = 0; m < dtChilds.Rows.Count; m++)
                            //        {
                            //            DataRow drChild = null;
                            //            int age = 0;
                            //            int age2 = 0, age3 = 0, age4 = 0, age5 = 0, age6 = 0;
                            //            if (dtChilds.Rows.Count > 0)
                            //            {
                            //                drChild = dtChilds.Rows[m];

                            //                if (drChild["Child1"] != DBNull.Value)
                            //                {
                            //                    age = Convert.ToInt32(drChild["Child1"]);
                            //                }
                            //                if (drChild["Child2"] != DBNull.Value)
                            //                {
                            //                    age2 = Convert.ToInt32(drChild["Child2"]);
                            //                }

                            //                if (drChild["Child3"] != DBNull.Value)
                            //                {
                            //                    age3 = Convert.ToInt32(drChild["Child3"]);
                            //                }
                            //                if (drChild["Child4"] != DBNull.Value)
                            //                {
                            //                    age4 = Convert.ToInt32(drChild["Child4"]);
                            //                }
                            //                if (drChild["Child5"] != DBNull.Value)
                            //                {
                            //                    age5 = Convert.ToInt32(drChild["Child5"]);
                            //                }
                            //                if (drChild["Child6"] != DBNull.Value)
                            //                {
                            //                    age6 = Convert.ToInt32(drChild["Child6"]);
                            //                }
                            //            }


                            //            int fromAge1 = Convert.ToInt32(row1["room_child1_age_range"].ToString().Split(',')[0]);
                            //            int toAge1 = Convert.ToInt32(row1["room_child1_age_range"].ToString().Split(',')[1]);

                            //            int fromAge2 = 0;
                            //            int toAge2 = 0;
                            //            if (row1["room_child2_age_range"] != DBNull.Value)
                            //            {
                            //                fromAge2 = Convert.ToInt32(row1["room_child2_age_range"].ToString().Split(',')[0]);
                            //                toAge2 = Convert.ToInt32(row1["room_child2_age_range"].ToString().Split(',')[1]);
                            //            }

                            //            int fromAge3 = 0;
                            //            int toAge3 = 0;
                            //            if (row1["room_child3_age_range"] != DBNull.Value)
                            //            {
                            //                fromAge3 = Convert.ToInt32(row1["room_child3_age_range"].ToString().Split(',')[0]);
                            //                toAge3 = Convert.ToInt32(row1["room_child3_age_range"].ToString().Split(',')[1]);
                            //            }
                            //            if (row1["Rank"].ToString() == (m + 1).ToString())
                            //            {
                            //                if (Convert.ToInt32(row1["room_child"]) > 0)
                            //                {
                            //                    if (age > 0 && age >= fromAge1 && age <= toAge1 || (age >= fromAge2 && age <= toAge2) || (age >= fromAge3 && age <= toAge3))
                            //                    {
                            //                        if (Convert.ToInt32(row1["room_child"]) > 1)
                            //                        {
                            //                            if (age2 > 0)
                            //                            {
                            //                                if (age2 >= fromAge1 && age2 <= toAge1 || (age2 >= fromAge2 && age2 <= toAge2) || (age2 >= fromAge3 && age2 <= toAge3))
                            //                                {
                            //                                    if (Convert.ToInt32(row1["room_child"]) > 2)
                            //                                    {
                            //                                        if (age3 > 0)
                            //                                        {
                            //                                            if (age3 >= fromAge1 && age3 <= toAge1 || (age3 >= fromAge2 && age3 <= toAge2) || (age3 >= fromAge3 && age3 <= toAge3))
                            //                                            {
                            //                                                if (Convert.ToInt32(row1["room_child"]) > 3)
                            //                                                {
                            //                                                    if (age4 > 0)
                            //                                                    {
                            //                                                        if (age4 >= fromAge1 && age4 <= toAge1 || (age4 >= fromAge2 && age4 <= toAge2) || (age4 >= fromAge3 && age4 <= toAge3))
                            //                                                        {
                            //                                                            if (Convert.ToInt32(row1["room_child"]) > 4)
                            //                                                            {
                            //                                                                if (age5 > 0)
                            //                                                                {
                            //                                                                    if (age5 >= fromAge1 && age5 <= toAge1 || (age5 >= fromAge2 && age5 <= toAge2) || (age5 >= fromAge3 && age5 <= toAge3))
                            //                                                                    {
                            //                                                                        if (Convert.ToInt32(row1["room_child"]) > 5)
                            //                                                                        {
                            //                                                                            if (age6 > 0)
                            //                                                                            {
                            //                                                                                if (age6 >= fromAge1 && age6 <= toAge1 || (age6 >= fromAge2 && age6 <= toAge2) || (age6 >= fromAge3 && age6 <= toAge3))
                            //                                                                                {
                            //                                                                                }
                            //                                                                                else
                            //                                                                                {
                            //                                                                                    row1["Rank"] = 0;
                            //                                                                                }
                            //                                                                            }
                            //                                                                            else
                            //                                                                            {
                            //                                                                                row1["Rank"] = 0;
                            //                                                                            }
                            //                                                                        }
                            //                                                                    }
                            //                                                                    else
                            //                                                                    {
                            //                                                                        row1["Rank"] = 0;
                            //                                                                    }
                            //                                                                }
                            //                                                                else if (age6 > 0)
                            //                                                                {
                            //                                                                    if (age6 >= fromAge1 && age6 <= toAge1 || (age6 >= fromAge2 && age6 <= toAge2) || (age6 >= fromAge3 && age6 <= toAge3))
                            //                                                                    {
                            //                                                                    }
                            //                                                                    else
                            //                                                                    {
                            //                                                                        row1["Rank"] = 0;
                            //                                                                    }
                            //                                                                }
                            //                                                                else
                            //                                                                {
                            //                                                                    row1["Rank"] = 0;
                            //                                                                }
                            //                                                            }
                            //                                                        }
                            //                                                        else
                            //                                                        {
                            //                                                            row1["Rank"] = 0;
                            //                                                        }
                            //                                                    }
                            //                                                    else if (age5 > 0)
                            //                                                    {
                            //                                                        if (age5 >= fromAge1 && age5 <= toAge1 || (age5 >= fromAge2 && age5 <= toAge2) || (age5 >= fromAge3 && age5 <= toAge3))
                            //                                                        {
                            //                                                        }
                            //                                                        else
                            //                                                        {
                            //                                                            row1["Rank"] = 0;
                            //                                                        }
                            //                                                    }
                            //                                                    else if (age6 > 0)
                            //                                                    {
                            //                                                        if (age6 >= fromAge1 && age6 <= toAge1 || (age6 >= fromAge2 && age6 <= toAge2) || (age6 >= fromAge3 && age6 <= toAge3))
                            //                                                        {
                            //                                                        }
                            //                                                        else
                            //                                                        {
                            //                                                            row1["Rank"] = 0;
                            //                                                        }
                            //                                                    }
                            //                                                    else
                            //                                                    {
                            //                                                        row1["Rank"] = 0;
                            //                                                    }
                            //                                                }
                            //                                            }
                            //                                            else
                            //                                            {
                            //                                                row1["Rank"] = 0;
                            //                                            }
                            //                                        }
                            //                                    }
                            //                                }
                            //                                else
                            //                                {
                            //                                    row1["Rank"] = 0;
                            //                                }
                            //                            }
                            //                        }
                            //                    }
                            //                    else if (age2 > 0)
                            //                    {
                            //                        if (age2 >= fromAge1 && age2 <= toAge1 || (age2 >= fromAge2 && age2 <= toAge2) || (age2 >= fromAge3 && age2 <= toAge3))
                            //                        {
                            //                        }
                            //                        else
                            //                        {
                            //                            row1["Rank"] = 0;
                            //                        }
                            //                    }
                            //                    else if (age3 > 0)
                            //                    {
                            //                        if (age3 >= fromAge1 && age3 <= toAge1 || (age3 >= fromAge2 && age3 <= toAge2) || (age3 >= fromAge3 && age3 <= toAge3))
                            //                        {
                            //                        }
                            //                        else
                            //                        {
                            //                            row1["Rank"] = 0;
                            //                        }
                            //                    }
                            //                    else if (age4 > 0)
                            //                    {
                            //                        if (age4 >= fromAge1 && age4 <= toAge1 || (age4 >= fromAge2 && age4 <= toAge2) || (age4 >= fromAge3 && age4 <= toAge3))
                            //                        {
                            //                        }
                            //                        else
                            //                        {
                            //                            row1["Rank"] = 0;
                            //                        }
                            //                    }
                            //                    else if (age5 > 0)
                            //                    {
                            //                        if (age5 >= fromAge1 && age5 <= toAge1 || (age5 >= fromAge2 && age5 <= toAge2) || (age5 >= fromAge3 && age5 <= toAge3))
                            //                        {
                            //                        }
                            //                        else
                            //                        {
                            //                            row1["Rank"] = 0;
                            //                        }
                            //                    }
                            //                    else if (age6 > 0)
                            //                    {
                            //                        if (age6 >= fromAge1 && age6 <= toAge1 || (age6 >= fromAge2 && age6 <= toAge2) || (age6 >= fromAge3 && age6 <= toAge3))
                            //                        {
                            //                        }
                            //                        else
                            //                        {
                            //                            row1["Rank"] = 0;
                            //                        }
                            //                    }
                            //                    else
                            //                    {
                            //                        row1["Rank"] = 0;
                            //                    }
                            //                }
                            //            }
                            //        }

                            //        if (Convert.ToInt32(row1["Rank"]) > request.NoOfRooms)
                            //        {
                            //            roomDetail.SequenceNo = (Convert.ToInt32(row1["Rank"]) - request.NoOfRooms).ToString();
                            //        }
                            //        else
                            //        {
                            //            roomDetail.SequenceNo = row1["Rank"].ToString();
                            //        }

                            //        roomDetail.Rates = roomRates;
                            //        roomDetails[j] = roomDetail;
                            //    }
                            //}
                            #endregion
                        }
                        
                        if (roomDetails != null && roomDetails.Count > 0)
                        {
                            roomDetails = roomDetails.FindAll(s => s.Rates != null);
                            //Array.Sort(roomDetails, delegate (HotelRoomsDetails rd1, HotelRoomsDetails rd2) { return rd1.TotalPrice.CompareTo(rd2.TotalPrice); });
                            roomDetails = (from r in roomDetails orderby r.TotalPrice select r).ToList();

                        result.BookingSource = HotelBookingSource.HotelConnect;
                        result.RoomDetails = roomDetails.ToArray();

                        List<int> Sequences = new List<int>();

                        int roomsAvailable = result.RoomDetails.Length;
                        for (int k = 0; k < result.RoomDetails.Length; k++)
                        {

                            //VAT Calucation Changes Modified 14.12.2017
                            decimal hotelTotalPrice = 0m;
                            decimal vatAmount = 0m;
                            hotelTotalPrice = result.RoomDetails[k].TotalPrice;
                            if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                            {
                                hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, 2);
                            }
                            hotelTotalPrice = Math.Round(hotelTotalPrice, 2);
                            result.RoomDetails[k].TotalPrice = hotelTotalPrice;
                            result.RoomDetails[k].InputVATAmount = vatAmount;
                            result.RoomDetails[k].TaxDetail = priceTaxDet;

                            /*===================================================================================================================
                            *                                           Tax Calculation for Total Price
                            *                                      MarkupType is Tax Type. Markup stores Tax value.
                            *==================================================================================================================*/
                            if (result.RoomDetails[k].MarkupType == "P")
                            {
                                //result.RoomDetails[k].TotalPrice += result.RoomDetails[k].TotalPrice * (result.RoomDetails[k].Markup / 100);
                                result.RoomDetails[k].TotalTax += result.RoomDetails[k].TotalPrice * (result.RoomDetails[k].Markup / 100);
                            }
                            else
                            {
                                //result.RoomDetails[k].TotalPrice += result.RoomDetails[k].Markup;
                                result.RoomDetails[k].TotalTax += result.RoomDetails[k].Markup;
                            }

                            /*===================================================================================================================
                            *                   After Tax calcuation to Total clear Markup & MarkupType
                            *==================================================================================================================*/

                            result.RoomDetails[k].Markup = 0;
                            result.RoomDetails[k].MarkupType = "";
                            result.Price = new PriceAccounts();
                            result.Price.RateOfExchange = rateOfExchange;
                            result.Price.SupplierCurrency = "AED";
                            /*===================================================================================================================
                             *                                  Markup calculation room wise
                             * =================================================================================================================*/
                            if (markupType == "P")
                            {
                                result.RoomDetails[k].Markup = (result.RoomDetails[k].TotalPrice + result.RoomDetails[k].TotalTax) * (markup / 100);
                            }
                            else
                            {
                                result.RoomDetails[k].Markup = markup;
                            }
                            result.RoomDetails[k].MarkupValue = markup;
                            result.RoomDetails[k].MarkupType = markupType;
                            result.RoomDetails[k].Discount = 0;
                            if (markup > 0 && discount > 0)
                            {
                                result.RoomDetails[k].Discount = discountType == "F" ?  discount :  result.RoomDetails[k].Markup * (discount / 100);
                            }
                            result.RoomDetails[k].Discount = result.RoomDetails[k].Discount > 0 ? result.RoomDetails[k].Discount : 0;                            
                            result.RoomDetails[k].SellingFare = result.RoomDetails[k].TotalPrice;
                            /*===================================================================================================================
                            *  Least price is always the first price in result. Set the base price as first price
                            * ==================================================================================================================*/
                            
                            if (k == 0)
                            {
                                //result.TotalPrice += result.RoomDetails[k].TotalPrice;
                                result.SellingFare += result.RoomDetails[k].TotalPrice;
                            }
                            result.RoomDetails[k].TotalPrice += result.RoomDetails[k].Markup;
                            /*====================================================================================================================
                            *      Check if any room combination sequence no is 0. If all room sequence's are ZERO then omit that result
                            *====================================================================================================================*/
                            if (Convert.ToInt32(result.RoomDetails[k].SequenceNo) == 0)
                            {
                                roomsAvailable--;
                            }
                            else
                            {   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                //      Check whether all room combination sequences exist ex:- searched for 4 rooms 
                                //      then result should have 1,2,3 & 4 sequence nos. If any one is missing omit that result
                                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                for (int j = 0; j <= request.NoOfRooms; j++)
                                {
                                    if (Convert.ToInt32(result.RoomDetails[k].SequenceNo) == (j + 1))
                                    {
                                        if (!Sequences.Contains((j + 1)))
                                        {
                                            result.TotalPrice += result.RoomDetails[k].TotalPrice  + result.RoomDetails[k].TotalTax;
                                            Sequences.Add((j + 1));
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                        if (!searchResults.Contains(result) && roomsAvailable > 0 && Sequences.Count == request.NoOfRooms)
                        {
                            result.Price.Discount = 0;
                            if (markup > 0 && discount > 0)
                            {
                                result.Price.Discount = discountType == "F" ? discount * request.NoOfRooms : (result.RoomDetails[0].TotalPrice*request.NoOfRooms-result.RoomDetails[0].SellingFare*request.NoOfRooms) * (discount) / 100;
                            }
                            result.Price.Discount = result.Price.Discount > 0 ? result.Price.Discount : 0;
                            searchResults.Add(result);
                        }
                        }
                        //else
                        //{
                        //    searchResults[searchResults.IndexOf(result)] = result;
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to load results " + ex.ToString(), ex);
            }

            try
            {
                string filePath = @"" + xmlPath + "HotelInventorySearchResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                SerializeResult(filePath, searchResults);
            }
            catch { }

            return searchResults;
        }        

        void SerializeResult(string filePath, List<HotelSearchResult> hotelResults)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            XmlWriter writer = XmlTextWriter.Create(filePath,settings);

            //writer.WriteStartDocument(true);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("HotelResults");            
            
            foreach (HotelSearchResult result in hotelResults)
            {
                writer.WriteStartElement("Hotel");
                writer.WriteElementString("Name", result.HotelName);
                writer.WriteElementString("BookingSource", result.BookingSource.ToString());
                writer.WriteElementString("CityCode", result.CityCode);
                writer.WriteElementString("Currency", result.Currency);                
                writer.WriteElementString("Code", result.HotelCode);
                writer.WriteElementString("Description", result.HotelDescription);
                writer.WriteElementString("Address", result.HotelAddress);
                writer.WriteElementString("ContactNo", result.HotelContactNo);
                writer.WriteElementString("MainImage", ConfigurationSystem.HotelConnectConfig["imgPathForServer"]+result.HotelPicture);
                writer.WriteElementString("Location", result.HotelLocation);
                writer.WriteElementString("CheckIn", result.StartDate.ToString("dd MMM yyyy"));
                writer.WriteElementString("CheckOut", result.EndDate.ToString("dd MMM yyyy"));
                writer.WriteStartElement("HotelAmenities");
                foreach (string amenity in result.HotelFacilities)
                {
                    writer.WriteElementString("Amenity", amenity);
                }
                writer.WriteEndElement();//HotelAmenities
                writer.WriteElementString("Rating", result.Rating.ToString());
                writer.WriteElementString("RateType", result.RateType.ToString());
                writer.WriteElementString("Price", result.TotalPrice.ToString());
                writer.WriteStartElement("RoomDetails");
                foreach (HotelRoomsDetails room in result.RoomDetails)
                {
                    writer.WriteElementString("RoomName", room.RoomTypeName);
                    writer.WriteElementString("MealPlan", room.mealPlanDesc);
                    writer.WriteStartElement("RoomAmenties");
                    if (room.Amenities != null && room.Amenities.Count > 0)
                    {
                        foreach (string amenity in room.Amenities)
                        {
                            writer.WriteElementString("Amenity", amenity);
                        }
                    }
                    writer.WriteEndElement();//Amenities
                    writer.WriteElementString("EssentialInformation", room.EssentialInformation);
                    writer.WriteElementString("ExtraBed", (room.IsExtraBed ? "Allowed" : "Not Allowed"));
                    writer.WriteElementString("MaxExtraBeds", room.MaxExtraBeds.ToString());                    
                    writer.WriteElementString("NoOfCots", room.NumberOfCots.ToString());
                    writer.WriteElementString("Promo", room.PromoMessage);
                    writer.WriteElementString("RatePlanCode", room.RatePlanCode);
                    writer.WriteElementString("RoomTypeCode", room.RoomTypeCode);
                    writer.WriteElementString("SellingFare", room.SellingFare.ToString());
                    writer.WriteElementString("SequenceNo", room.SequenceNo);
                    writer.WriteElementString("SharingBedding", (room.SharingBedding ? "True" : "False"));
                    writer.WriteElementString("SupplierPrice", room.supplierPrice.ToString());
                    writer.WriteElementString("TotalPrice", room.TotalPrice.ToString());
                    writer.WriteStartElement("RateBreakup");
                    foreach (RoomRates rate in room.Rates)
                    {
                        writer.WriteStartElement("Rate");
                        writer.WriteElementString("BaseFare", rate.BaseFare.ToString());
                        writer.WriteElementString("Date", rate.Days.ToString("dd MMM yyyy"));
                        writer.WriteElementString("RateType", rate.RateType.ToString());
                        writer.WriteElementString("SellingFare", rate.SellingFare.ToString());
                        writer.WriteElementString("TotalFare", rate.Totalfare.ToString());
                        writer.WriteEndElement();//Rate
                    }
                    writer.WriteEndElement();//RateBreakup
                }
                writer.WriteEndElement();//RoomDetails
                writer.WriteEndElement();//HotelSearchResult
            }
            writer.WriteEndElement();//HotelSearchResults
            //writer.WriteEndElement();//End Document
            writer.Flush();
            writer.Close();
        }


        void SerializeItinerary(string filePath, HotelItinerary itinerary)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            XmlWriter writer = XmlTextWriter.Create(filePath, settings);

            //writer.WriteStartDocument(true);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("HotelItinerary");

            writer.WriteStartElement("Hotel");
            writer.WriteElementString("Name", itinerary.HotelName);
            writer.WriteElementString("BookingSource", itinerary.Source.ToString());
            writer.WriteElementString("CityCode", itinerary.CityCode);
            writer.WriteElementString("Currency", itinerary.Currency);
            writer.WriteElementString("Code", itinerary.HotelCode);
            writer.WriteElementString("Address", itinerary.HotelAddress1 + itinerary.HotelAddress2);
            writer.WriteElementString("CheckIn", itinerary.StartDate.ToString("dd MMM yyyy"));
            writer.WriteElementString("CheckOut", itinerary.EndDate.ToString("dd MMM yyyy"));

            writer.WriteElementString("Rating", itinerary.Rating.ToString());
            writer.WriteElementString("RateType", itinerary.Rating.ToString());
            writer.WriteElementString("Price", itinerary.TotalPrice.ToString());
            writer.WriteStartElement("RoomDetails");
            foreach (HotelRoom room in itinerary.Roomtype)
            {
                writer.WriteElementString("RoomName", room.RoomName);
                writer.WriteElementString("MealPlan", room.MealPlanDesc);
                writer.WriteStartElement("RoomAmenties");
                if (room.Ameneties!=null && room.Ameneties.Count>0) { 
                foreach (string amenity in room.Ameneties)
                {
                    writer.WriteElementString("Amenity", amenity);
                }
                }
                writer.WriteEndElement();//Amenities
                writer.WriteElementString("EssentialInformation", room.EssentialInformation);
                writer.WriteElementString("ExtraBed", (room.ExtraBed ? "Allowed" : "Not Allowed"));
                writer.WriteElementString("MaxExtraBeds", room.NoOfExtraBed.ToString());
                writer.WriteElementString("NoOfCots", room.NoOfCots.ToString());

                writer.WriteElementString("RatePlanCode", room.RatePlanCode);
                writer.WriteElementString("RoomTypeCode", room.RoomTypeCode);
                writer.WriteElementString("SellingFare", room.Price.NetFare.ToString());

                writer.WriteElementString("SharingBedding", (room.SharingBed ? "True" : "False"));
                writer.WriteElementString("SupplierPrice", room.Price.SupplierPrice.ToString());
                writer.WriteElementString("TotalPrice", room.Price.NetFare.ToString());
                writer.WriteStartElement("RateBreakup");
            }
            writer.WriteEndElement();//RoomDetails
            writer.WriteEndElement();//Hotel
            writer.WriteEndElement();//HotelItinerary
            //writer.WriteEndElement();//End Document
            writer.Flush();
            writer.Close();
        }

        bool FindHotel(ref List<HotelSearchResult> searchResults, string hotelName)
        {
            bool found = false;
            //if (searchResults.Count > 1)
            {
                foreach (HotelSearchResult result in searchResults)
                {
                    if (result != null && result.HotelName.Equals(hotelName))
                    {
                        found = true;
                        break;
                    }
                }
            }

            return found;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rating"></param>
        /// <returns></returns>
        HotelRating GetHotelRating(string rating)
        {
            HotelRating hotelRating = HotelRating.All;
            switch (rating)
            {
                case "1S":
                    hotelRating = HotelRating.OneStar;
                    break;
                case "2S":
                    hotelRating = HotelRating.TwoStar;
                    break;
                case "3S":
                    hotelRating = HotelRating.ThreeStar;
                    break;
                case "4S":
                    hotelRating = HotelRating.FourStar;
                    break;
                case "5S":
                    hotelRating = HotelRating.FiveStar;
                    break;
            }
            return hotelRating;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public List<string> GetHotelFacilities(int hotelId)
        {
            List<string> HotelFacilities = new List<string>();

            InvBooking booking = new InvBooking();

            HotelFacilities = booking.GetHotelFacilities(hotelId);

            return HotelFacilities;
        }


        /// <summary>
        /// Used to return the cancellation policies for the selected room(s) of a Hotel.
        /// </summary>
        /// <param name="resultObj">Hotel Search Result object.</param>
        /// <param name="request">Hotel Request object.</param>
        /// <param name="Rooms">List of Room Id for whom cancellation policies to be returned.</param>
        /// <returns></returns>
        public Dictionary<string, string> GetCancellationPolicy(ref HotelSearchResult resultObj, HotelRequest request, List<string> Rooms)
        {
            decimal markup = 0;string markuptype = "F";
         return  GetCancellationPolicy(ref resultObj, request, Rooms, markup, markuptype);
        }
        public Dictionary<string, string> GetCancellationPolicy(ref HotelSearchResult resultObj, HotelRequest request, List<string> Rooms, decimal markup, string markuptype)
        {
            Dictionary<string, string> CancellationPolicy = new Dictionary<string, string>();
            //Cancel Policy Updates
            List<HotelCancelPolicy> cancelPolicyList = new List<HotelCancelPolicy>();
            string cancelPolicy = "";
            string terms = "";
            int durationHours = 0;
            //Cancel Policy Updates
            int roomDetailsIndex = -1;
            try
            {
                string filePath = @"" + xmlPath + "HotelInventoryCancellationPolicyRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                try
                {                    
                    List<HotelSearchResult> result = new List<HotelSearchResult>();
                    result.Add(resultObj);
                    SerializeResult(filePath, result);
                }
                catch { }
                SimilarHotelSourceInfo sourceInfo = resultObj.SimilarHotels.Find(s => s.Source == HotelBookingSource.HotelConnect);
                foreach (string pair in Rooms)
                {
                    //Cancel Policy Updates
                    HotelCancelPolicy policy = new HotelCancelPolicy();

                    string roomTypeCode = pair;

                    InvBooking booking = new InvBooking();

                    DataTable dtCancelPolicy = booking.GetCancelPolicy(sourceInfo.HotelCode, Convert.ToInt32(pair.Split('-')[1]), Convert.ToInt32(pair.Split('-')[2]));
                    decimal totFare = 0;
                    decimal oneDayFare = 0;
                    string roomName = "";

                    for (int y = 0; y < resultObj.RoomDetails.Length; y++)
                    {
                        if (resultObj.RoomDetails[y].SupplierName == "HotelConnect") { 
                        int RoomId = Convert.ToInt32(resultObj.RoomDetails[y].RoomTypeCode.Split('-')[1]);
                        int AccomId = Convert.ToInt32(resultObj.RoomDetails[y].RoomTypeCode.Split('-')[3]);

                        if (resultObj.RoomDetails[y].RoomTypeCode == pair)
                        {
                            //Cancel Policy Updates
                            roomDetailsIndex = y;
                            totFare += resultObj.RoomDetails[y].SellingFare + resultObj.RoomDetails[y].TotalTax - resultObj.RoomDetails[y].Discount;
                            oneDayFare = resultObj.RoomDetails[y].Rates[0].SellingFare;
                            roomName = resultObj.RoomDetails[y].RoomTypeName;
                            break;
                        }
                        }
                    }
                    
                    totFare = Math.Round(totFare) + ((markuptype == "F") ? markup : ((totFare) * (markup / 100m)));
                    totFare = Math.Round(totFare, 2);
                    string msg = "";
                    if (dtCancelPolicy != null && dtCancelPolicy.Rows.Count > 0)
                    {
                        DataRow row = dtCancelPolicy.Rows[0];

                        string[] hours = row["cnx_before_hours"].ToString().Split(',');
                        string[] chargeTypes = row["cnx_charge_type"].ToString().Split(',');
                        string[] chargeValues = row["cnx_charge_value"].ToString().Split(',');                        
                        bool refundable = true;


                        if (dtCancelPolicy.Rows.Count > 1 && dtCancelPolicy.Rows[1]["cnx_terms_conditions"] != DBNull.Value)
                        {
                            terms = dtCancelPolicy.Rows[1]["cnx_terms_conditions"].ToString();
                        }

                        if (row["RPM_cnx_status"].ToString().Equals("N"))
                        {
                            refundable = false;
                        }
                  
                        if (refundable)
                        {
                            
                            for (int i = 0; i < hours.Length; i++)
                            {
                                durationHours = Convert.ToInt32(hours[i]);

                                //Cancel Policy Updates
                                policy.FromDate = resultObj.StartDate.AddHours(-durationHours);
                                policy.Amount = oneDayFare * Convert.ToInt32(chargeValues[i]);
                                policy.BufferDays = Convert.ToInt32(Math.Round(Convert.ToDecimal(durationHours) / 24));
                                policy.Todate = resultObj.StartDate.AddHours(durationHours);
                                policy.Currency = resultObj.Currency;

                                if (chargeTypes[i] == "N")
                                {
                                    if (msg.Length > 0)
                                    {
                                        if (Convert.ToInt32(chargeValues[i]) > 0)
                                        {
                                            msg += "@@Cancelling after " + resultObj.StartDate.AddHours(-durationHours).ToString("dd/MMM/yyyy") + " charge applicable : " + chargeValues[i] + " nights.";
                                        }
                                        else
                                        {
                                            msg += "@@Cancelling before " + resultObj.StartDate.AddHours(-durationHours).ToString("dd/MMM/yyyy") + " charge applicable : " + chargeValues[i] + " nights.";
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(chargeValues[i]) > 0)
                                        {
                                            msg = "Cancelling after " + resultObj.StartDate.AddHours(-durationHours).ToString("dd/MMM/yyyy") + " charge applicable : " + chargeValues[i] + " nights.";
                                        }
                                        else
                                        {
                                            msg = "Cancelling before " + resultObj.StartDate.AddHours(-durationHours).ToString("dd/MMM/yyyy") + " charge applicable : " + chargeValues[i] + " nights.";
                                        }
                                    }
                                    //Cancel Policy Updates
                                    policy.ChargeType = "Nights";
                                    policy.Remarks = msg;
                                }
                                else
                                {
                                    if (msg.Length > 0)
                                    {
                                        if (Convert.ToInt32(chargeValues[i]) > 0)
                                        {
                                            msg += "@@Amount of " + resultObj.Currency + " " + (totFare * Convert.ToInt32(chargeValues[i]) / 100) + " will be charged after " + resultObj.StartDate.AddHours(-durationHours).ToString("dd/MMM/yyyy") + ".";
                                        }
                                        else
                                        {
                                            msg += "@@Amount of " + resultObj.Currency + " " + (totFare * Convert.ToInt32(chargeValues[i]) / 100) + " will be charged before " + resultObj.StartDate.AddHours(-durationHours).ToString("dd/MMM/yyyy") + ".";
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(chargeValues[i]) > 0)
                                        {
                                            msg = "Amount of " + resultObj.Currency + " " + (totFare * Convert.ToInt32(chargeValues[i]) / 100) + " will be charged after " + resultObj.StartDate.AddHours(-durationHours).ToString("dd/MMM/yyyy") + ".";
                                        }
                                        else
                                        {
                                            msg = "Amount of " + resultObj.Currency + " " + (totFare * Convert.ToInt32(chargeValues[i]) / 100) + " will be charged before " + resultObj.StartDate.AddHours(-durationHours).ToString("dd/MMM/yyyy") + ".";
                                        }
                                    }
                                    //Cancel Policy Updates
                                    policy.ChargeType = "Amount";
                                    policy.Remarks = msg;
                                }
                            }
                        }
                        else
                        {
                            if (msg.Length > 0)
                            {
                                //cancelPolicy += "|Bookings for " + roomName + " room are Non refundable.";
                                msg += "@@Non-refundable and full charge will apply once booking is completed.";
                            }
                            else
                            {
                                //cancelPolicy = "Bookings for " + roomName + " room are Non refundable.";
                                msg = "Non-refundable and full charge will apply once booking is completed.";
                            }

                            //Cancel Policy Updates
                            policy.FromDate = resultObj.StartDate.AddHours(-durationHours);
                            policy.Amount = resultObj.TotalPrice;
                            policy.BufferDays = Convert.ToInt32(Math.Round(Convert.ToDecimal(durationHours) / 24));
                            policy.Todate = resultObj.StartDate.AddHours(durationHours);
                            policy.Currency = resultObj.Currency;
                            policy.ChargeType = "Amount";
                            policy.Remarks = msg;
                        }
                    }
                    if(cancelPolicy.Length>0)
                    {
                        cancelPolicy += "|" + msg;
                    }
                    else
                    {
                        cancelPolicy = msg;
                    }
                    //Cancel Policy Updates
                    cancelPolicyList.Add(policy);
                    if (roomDetailsIndex != -1)
                        resultObj.RoomDetails[roomDetailsIndex].CancelPolicyList = cancelPolicyList;
                    resultObj.RoomDetails[roomDetailsIndex].CancellationPolicy = cancelPolicy;
                }
                CancellationPolicy.Add("CancelPolicy", cancelPolicy);
                CancellationPolicy.Add("lastCancellationDate", resultObj.StartDate.AddHours(durationHours).ToString("dd/MMM/yyyy"));
                
                if (terms.Length > 0)
                {
                    CancellationPolicy.Add("HotelPolicy", terms + "#" + GetHotelNorms(Convert.ToInt32(resultObj.HotelCode)));
                }
                else
                {
                    CancellationPolicy.Add("HotelPolicy", GetHotelNorms(Convert.ToInt32(resultObj.HotelCode)));
                }
                if (roomDetailsIndex != -1)
                    resultObj.RoomDetails[roomDetailsIndex].EssentialInformation = CancellationPolicy["HotelPolicy"];
                    filePath = @"" + xmlPath + "HotelInventoryCancellationPolicyResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";

                try
                {
                    SerializeCancellationPolicy(filePath, CancellationPolicy, resultObj.HotelName);
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CancellationPolicy;
        }

        void SerializeCancellationPolicy(string filePath, Dictionary<string, string> cancelPolicy,string hotelName)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            XmlWriter writer = XmlTextWriter.Create(filePath, settings);

            //writer.WriteStartDocument(true);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("CancellationPolicyResponse");
            writer.WriteElementString("hotelName", hotelName);
            writer.WriteElementString("LastCancellationDate", cancelPolicy["lastCancellationDate"]);
            writer.WriteElementString("CancellationPolicy", cancelPolicy["CancelPolicy"]);
            writer.WriteElementString("HotelNorms", cancelPolicy["HotelPolicy"]);
            writer.WriteEndElement();
            
            writer.Flush();
            writer.Close();
        }

        /// <summary>
        /// Used to get standard Hotel Norms/Policies.
        /// </summary>
        /// <param name="hotelId">HotelId which can be found in first param RoomTypeCode. RoomTypeCode consists of HotelId-RoomId-PeriodId-AccomodationId-Adults-Childs ex: hh-rr-pp-aa-ad-ch hh stands for HotelId. </param>
        /// <returns></returns>
        public string GetHotelNorms(int hotelId)
        {
            string hotelNorms = "";

            try
            {
                InvBooking booking = new InvBooking();
                DataTable dtNorms = booking.GetHotelNorms(hotelId);

                if (dtNorms != null && dtNorms.Rows.Count > 0)
                {
                    DataRow dr = dtNorms.Rows[0];
                    hotelNorms = "CheckIn Time: " + Convert.ToDateTime(dr["check_in_hrs"]).ToString("hh:mm tt");
                    hotelNorms += "#CheckOut Time: " + Convert.ToDateTime(dr["check_out_hrs"]).ToString("hh:mm tt");
                    string city = dr["City"].ToString();
                    if (dr["is_early_check_in"].ToString() == "1")
                    {
                        hotelNorms += "#Early CheckIn Allowed prior " + dr["early_checkin_hrs"].ToString() + " hours";
                    }
                    if (dr["policies_and_disclaimer"].ToString().Length > 0)
                    {
                        hotelNorms += "#" + dr["policies_and_disclaimer"].ToString();
                    }
                    else
                    {
                        hotelNorms += "#All timings are based on local times only.";
                    }
                    if (city.ToLower().Contains("dubai"))
                    {
                        hotelNorms += "#Compulsory Tourism Dirham to be paid by guest directly to the hotel per room per night valid from 31st March 2014 Onward.#Normally it would be AED 20 - 40 per night.";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return hotelNorms;
        }


        /// <summary>
        /// Used to Confirm booking for a selected hotel itinerary.
        /// </summary>
        /// <param name="hotelBooking">HotelItinerary object for booking.</param>
        /// <param name="objCR">ConfirmationReference object for generating Confirmation Number.</param>
        /// <param name="rooms">List of RoomBookingInformation objects for storing room passenger information.</param>
        /// <param name="fares">List of RoomFareBreakDown objects for storing day wise fare information.</param>
        /// <param name="roomQuota">List of RoomQuota objects for updating room quota after booking.</param>
        /// <returns></returns>
        public BookingResponse BookHotel(HotelItinerary hotelBooking)
        {
            BookingResponse response = new BookingResponse();
            

            string filePath = @"" + xmlPath + "HotelInventoryBookingRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
            try
            {
                SerializeItinerary(filePath, hotelBooking);

                InvBooking booking = new InvBooking();
                hotelBooking.ConfirmationNo = booking.ConfirmBooking(hotelBooking);

                response.BookingId = hotelBooking.HotelId;
                response.ConfirmationNo = hotelBooking.ConfirmationNo;
                response.PNR = "";
                response.ProdType = ProductType.Hotel;
                response.SSRDenied = false;
                response.SSRMessage = "";
                response.Status = BookingResponseStatus.Successful;
                response.Error = "";
                hotelBooking.BookingRefNo = hotelBooking.ConfirmationNo;
                hotelBooking.Status = HotelBookingStatus.Confirmed;
            }
            catch (Exception ex)
            {
                response.Status = BookingResponseStatus.Failed;
                response.Error = ex.Message;
                hotelBooking.Status = HotelBookingStatus.Failed;                
                throw ex;
            }
            try
            {
                filePath = @"" + xmlPath + "HotelInventoryBookingResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                SerializeBookingResponse(filePath, response);
            }
            catch { }
            return response;
        }


        void SerializeBookingResponse(string filePath, BookingResponse response)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            XmlWriter writer = XmlTextWriter.Create(filePath, settings);

            //writer.WriteStartDocument(true);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("HotelBookingResponse");
            writer.WriteElementString("Status", response.Status.ToString());
            writer.WriteElementString("ConfirmationNo", response.ConfirmationNo);
            writer.WriteElementString("Error", response.Error);            
            writer.WriteEndElement();

            writer.Flush();
            writer.Close();
        }

        /// <summary>
        /// Used to cancel an Hotel booking. If you want to cancel send True in second parameter. 
        /// Otherwise to know the charges for Cancellation send False in second parameter.
        /// </summary>
        /// <param name="confirmationNo">Confirmation No received while booking.</param>
        /// <param name="cancel">Default True. Send True if you want to cancel booking otherwise send False to know cancel charges.</param>
        /// <returns></returns>
        public Dictionary<string, string> CancelBooking(string confirmationNo, bool cancel)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            string filePath = @"" + xmlPath + "HotelInventoryBookingCancellationRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
            try
            {
                SerializeCancellationCharges(filePath, cancellationCharges, (cancel ? "Cancelled" : "CancelCharges"), confirmationNo, true);

                InvBooking booking = new InvBooking();
                DataTable dtCancelPolicy = booking.CancelBooking(confirmationNo, cancel);
                if (dtCancelPolicy != null && dtCancelPolicy.Rows.Count > 0)
                {
                    int durationHours = 0, rows = dtCancelPolicy.Rows.Count;
                    cancellationCharges.Add("Currency", dtCancelPolicy.Rows[0]["currency"].ToString());
                    foreach (DataRow row in dtCancelPolicy.Rows)
                    {
                        string[] hours = row["cnx_before_hours"].ToString().Split(',');
                        string[] chargeTypes = row["cnx_charge_type"].ToString().Split(',');
                        string[] chargeValues = row["cnx_charge_value"].ToString().Split(',');
                        decimal dayRate = Math.Round(Convert.ToDecimal(row["Price"]), 2);
                        DateTime checkInDate = Convert.ToDateTime(row["StartDate"]);
                        decimal fare = Math.Round(Convert.ToDecimal(row["TotalPrice"]), 2);
                        int rooms = Convert.ToInt32(row["Rooms"]);
                        string cnxStatus = row["RPM_CNX_Status"].ToString();
                        for (int i = 0; i < hours.Length; i++)
                        {
                            if (cnxStatus != "N")
                            {
                                durationHours = Convert.ToInt32(hours[i]);
                                if (checkInDate.Subtract(DateTime.Now).TotalDays * 24 <= Convert.ToInt32(hours[i]))
                                {
                                    if (chargeTypes[i] == "N")
                                    {
                                        if (rooms > rows)//If Same room and Same Price for all rooms, one row will come
                                        {
                                            if (cancellationCharges.ContainsKey("Amount"))
                                            {
                                                decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                                                decimal roomRate = dayRate * Convert.ToInt32(chargeValues[i]);
                                                roomRate = roomRate * rooms;
                                                amount += roomRate;
                                                cancellationCharges["Amount"] = amount.ToString();
                                            }
                                            else
                                            {
                                                decimal roomRate = dayRate * Convert.ToInt32(chargeValues[i]);
                                                roomRate = roomRate * rooms;
                                                cancellationCharges.Add("Amount", (roomRate).ToString());
                                            }
                                        }
                                        else //if(rooms == rows)//If Same room and different price, three rows will return
                                        {
                                            if (cancellationCharges.ContainsKey("Amount"))
                                            {
                                                decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                                                amount += (dayRate * Convert.ToInt32(chargeValues[i]));
                                                cancellationCharges["Amount"] = amount.ToString();
                                            }
                                            else
                                            {
                                                cancellationCharges.Add("Amount", (dayRate * Convert.ToInt32(chargeValues[i])).ToString());
                                            }
                                        }
                                    }
                                    else if (chargeValues[i] == "100" && chargeTypes[i] == "P" && hours[i] == "0")
                                    {
                                        if (cancellationCharges.ContainsKey("Amount"))
                                        {
                                            decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                                            amount += fare;
                                            cancellationCharges["Amount"] = amount.ToString();
                                        }
                                        else
                                        {
                                            cancellationCharges.Add("Amount", fare.ToString());
                                        }
                                    }
                                    else if (chargeTypes[i] == "P")
                                    {
                                        if (cancellationCharges.ContainsKey("Amount"))
                                        {
                                            decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);

                                            decimal charge = ((fare / rooms) * Convert.ToInt32(chargeValues[i]) / 100);
                                            charge = charge * rooms;
                                            amount += charge;
                                            cancellationCharges["Amount"] = amount.ToString();
                                        }
                                        else
                                        {
                                            decimal charge = ((fare / rooms) * Convert.ToInt32(chargeValues[i]) / 100);
                                            charge = charge * rooms;
                                            cancellationCharges.Add("Amount", (charge).ToString());
                                        }
                                    }
                                    else
                                    {
                                        if (cancellationCharges.ContainsKey("Amount"))
                                        {
                                            decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                                            amount += fare;
                                            cancellationCharges["Amount"] = amount.ToString();
                                        }
                                        else
                                        {
                                            cancellationCharges.Add("Amount", fare.ToString());
                                        }
                                    }
                                }
                                else
                                {
                                    //if (chargeTypes[i] == "N")
                                    //{
                                    //    if (cancellationCharges.ContainsKey("Amount"))
                                    //    {
                                    //        decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                                    //        amount += (dayRate * Convert.ToInt32(chargeValues[i]));
                                    //        cancellationCharges["Amount"] = amount.ToString();
                                    //    }
                                    //    else
                                    //    {
                                    //        cancellationCharges.Add("Amount", (dayRate * Convert.ToInt32(chargeValues[i])).ToString());
                                    //    }
                                    //}
                                    //Cancelled before last cancellation date if Non Refundable then charge 100% of booking amount
                                    if (chargeValues[i] == "100" && chargeTypes[i] == "P" && hours[i] == "0")
                                    {
                                        if (cancellationCharges.ContainsKey("Amount"))
                                        {
                                            decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                                            amount += fare;
                                            cancellationCharges["Amount"] = amount.ToString();
                                        }
                                        else
                                        {
                                            cancellationCharges.Add("Amount", fare.ToString());
                                        }
                                    }
                                    //else
                                    {
                                        //No charge before last cancellation date
                                        //if (fare > 0)
                                        //{
                                        //    if (cancellationCharges.ContainsKey("Amount"))
                                        //    {
                                        //        decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                                        //        amount += fare;
                                        //        cancellationCharges["Amount"] = amount.ToString();
                                        //    }
                                        //    else
                                        //    {
                                        //        cancellationCharges.Add("Amount", fare.ToString());
                                        //    }
                                        //}
                                        //else
                                        {
                                            if (cancellationCharges.ContainsKey("Amount"))
                                            {
                                                decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                                                cancellationCharges["Amount"] = amount.ToString();
                                            }
                                            else
                                            {
                                                cancellationCharges.Add("Amount", "0");
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (cancellationCharges.ContainsKey("Amount"))
                                {
                                    decimal amount = Convert.ToDecimal(cancellationCharges["Amount"]);
                                    amount += fare;
                                    cancellationCharges["Amount"] = amount.ToString();
                                }
                                else
                                {
                                    cancellationCharges.Add("Amount", fare.ToString());
                                }
                            }
                        }                        
                    }
                    cancellationCharges.Add("Status", "Cancelled");
                }               
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Cancel booking - Confirmation No: " + confirmationNo + " Error: " + ex.Message, ex);
            }

            filePath = @"" + xmlPath + "HotelInventoryBookingCancellationResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";

            SerializeCancellationCharges(filePath, cancellationCharges, (cancel ? "Cancelled" : "CancelCharges"), confirmationNo, false);

            return cancellationCharges;
        }

        void SerializeCancellationCharges(string filePath, Dictionary<string, string> cancellationCharges,string status,string confNo, bool request)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            XmlWriter writer = XmlTextWriter.Create(filePath, settings);
                        
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            if (request)
            {
                writer.WriteStartElement("HotelBookingCancellationRequest");
                writer.WriteElementString("Status", status);
                writer.WriteElementString("ConfirmationNo", confNo);
                writer.WriteEndElement();
            }
            else
            {
                writer.WriteStartElement("HotelBookingCancellationResponse");
                writer.WriteElementString("Status", status);
                writer.WriteElementString("Charge", cancellationCharges["Amount"]);
                writer.WriteEndElement();
            }

            writer.Flush();
            writer.Close();
        }


        private int GetBookingId(string confNo)
        {
            int bookingId=0;

            try
            {
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GetBooking, Severity.High, 1, "(HotelInventory)Failed to get BookingId. Reason : " + ex.ToString(), "");
            }

            return bookingId;
        }

        /// <summary>
        /// To avoid repetetive code common logic is written in this method
        /// </summary>
        /// <param name="age"></param>
        /// <param name="rate"></param>
        /// <param name="childAmount"></param>
        /// <param name="totalAmount"></param>
        private void ReadRoomRate(int age, ref DataRow rate, ref decimal childAmount, ref decimal totalAmount)
        {
            decimal childRate = 0;
            if (rate["ChildRateType"].ToString() == "P")
            {
                decimal amount = 0;
                if (rate["ExtraBed"].ToString() == "Y")
                {
                    amount = Convert.ToDecimal(rate["Rate"]);
                }
                else
                {
                    amount = Convert.ToDecimal(rate["Rate"]);
                }
                childRate = Convert.ToDecimal(rate["ChildRate" + age]);
                rate["ChildRateApplied"] = (amount * childRate / 100);
                childAmount += (amount * childRate / 100);
                totalAmount += (amount * childRate / 100);
                rate["TotalRate"] = Convert.ToDecimal(rate["TotalRate"]) + (amount * childRate / 100);

            }
            else
            {
                decimal amount = Convert.ToDecimal(rate["Rate"]);// / Convert.ToInt32(rate["Adults"]);

                childRate = Convert.ToDecimal(rate["ChildRate" + age]);
                rate["ChildRateApplied"] = childRate;
                childAmount += childRate;
                totalAmount += childRate;
                rate["TotalRate"] = Convert.ToDecimal(rate["TotalRate"]) + childRate;
            }
        }
    }


}

