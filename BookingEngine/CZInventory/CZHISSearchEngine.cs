﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Collections.Generic;
using CT.Core;
using CT.BookingEngine;
using System.IO;
using CT.Configuration;
using System.Xml;
using System.Net;
//using System.Web.Script.Serialization;
using System.Text;
using System.Linq;
using System.Net.Http;
using System.Web;


//namespace CT.BookingEngine.GDS
namespace CZInventory
{
    public class CZHISSearchEngine
    {
        string CZfilePath = string.Empty;
        string cancelInfo;
        //string sSessionKey;
        int countryId;
        string _token;
        string xmlPath;
        decimal rateOfExchange = 1;
        int decimalPoint;
        string sourceCountryCode;
        //Dictionary<string, decimal> exchangeRates;
        public int CountryId
        {
            get { return countryId; }
            set { countryId = value; }
        }
        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }
        /// <summary>
        /// Api source countryCode
        /// </summary>
        public string SourceCountryCode
        {
            get { return sourceCountryCode; }
            set { sourceCountryCode = value; }
        }
        protected int NoofRooms = 1;
        public List<HotelStaticData> StaticData { get; set; }


        public CZHISSearchEngine()
        {
            xmlPath = ConfigurationSystem.HotelExtranetConfig["XmlLogPath"];
            xmlPath += DateTime.Now.ToString("dd-MM-yyy") + "\\";

            try
            {
                if (!Directory.Exists(xmlPath))
                {
                    Directory.CreateDirectory(xmlPath);
                }
            }
            catch { }
        }

        public CZHISSearchEngine(string connection)
        {
            //DBGateway.GetConnection().ConnectionString = connection;
            xmlPath = ConfigurationSystem.HotelExtranetConfig["XmlLogPath"];
            try
            {
                if (!Directory.Exists(xmlPath))
                {
                    Directory.CreateDirectory(xmlPath);
                }
            }
            catch { }
        }

        private string GetResponse(string requestData, string url, string sToken, string requestType)
        {
            string apiResponse = string.Empty;
            try
            {
                HttpWebRequest httpwebrequest = WebRequest.CreateHttp(url);
                httpwebrequest.ContentType = "application/json";
                httpwebrequest.Method = "POST";
                httpwebrequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + sToken);
                httpwebrequest.Accept = "application/json";

                httpwebrequest.ContentLength = requestData.Length;
                StreamWriter requestWriter = new StreamWriter(httpwebrequest.GetRequestStream());
                //request.ContentType = contentType;
                requestWriter.Write(requestData);
                requestWriter.Close();

                WebResponse webResponse = httpwebrequest.GetResponse();
                Stream webStream = webResponse.GetResponseStream();

                StreamReader responseReader = new StreamReader(webStream);

                apiResponse = responseReader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception("HIS: Failed to get response for " + requestType, ex);
            }
            return apiResponse;
        }

        //private string Authentication()
        public static string Authentication()
        {

            string sToken = string.Empty;
            //string sSessionKey = string.Empty;
            try
            {
                string apiUrl = ConfigurationSystem.HotelExtranetConfig["AuthenticationUrl"];

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(apiUrl);

                HttpResponseMessage response =
                  client.PostAsync("token",
                    new StringContent(string.Format("grant_type=password&username=cozmo&password=cozmo"), Encoding.UTF8,
                      "application/x-www-form-urlencoded")).Result;

                string resultJSON = response.Content.ReadAsStringAsync().Result;
                LoginTokenResult result = JsonConvert.DeserializeObject<LoginTokenResult>(resultJSON);

                if (result != null && !string.IsNullOrEmpty(result.ErrorDescription))
                    throw new Exception(result.Error + ": " + result.ErrorDescription);

                sToken = result.AccessToken;

                //HttpContext Context = HttpContext.Current;
                //Context.Session["sSessionKey"] = sToken;

                //HttpContext.Current.Session["sSessionKey"] = sToken;
            }
            //catch (WebException wex)
            //{
            //    throw wex;
            //}
            catch (Exception ex)
            {
                throw ex;
            }
            return sToken;
        }

        public List<HotelSearchResult> GetHotels(HotelRequest request, Dictionary<string, decimal> exchangeRates, string currency, decimal markup, string markupType, string sourceCountryCode, decimal discount, string discountType)
        {

            _token = Authentication();

            List<HotelSearchResult> searchResults = new List<HotelSearchResult>();
            try
            {
                string postData = GenerateHotelSearchRequest(request);

                string apiUrl = string.Empty;

                apiUrl = ConfigurationSystem.HotelExtranetConfig["SearchUrl"];

                string CZresponse = GetResponse(postData, apiUrl, _token, "hotelresults");

                if (CZresponse != null)
                {
                    CZfilePath = @"" + xmlPath + "CZHotelInventorySearchResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                    StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                    sw.Write(CZresponse);
                    sw.Close();

                    searchResults = ReadHotelSearchResponse(CZresponse, request, markup, markupType, discount, discountType, currency, exchangeRates);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to load results " + ex.ToString(), ex);
            }

            return searchResults;
        }

        private string GenerateHotelSearchRequest(HotelRequest hotelRequest)
        {
            string CZfilePath = string.Empty;
            HotelRequestDTO hotelrequestDTO = new HotelRequestDTO();

            HotelSearchRequest structRequest = new HotelSearchRequest();
            var request = string.Empty;
            try
            {
                if (hotelRequest.Latitude != 0 && hotelRequest.Longtitude != 0)
                {
                    structRequest.Longtitude = hotelRequest.Longtitude;
                    structRequest.Latitude = hotelRequest.Latitude;
                    structRequest.Radius = hotelRequest.Radius;
                }
                else
                {
                    structRequest.CityId = hotelRequest.CityId;
                }
                structRequest.StartDate = hotelRequest.StartDate.ToString("dd/MM/yyyy");
                structRequest.EndDate = hotelRequest.EndDate.ToString("dd/MM/yyyy");
                structRequest.CityCode = hotelRequest.CityCode;
                structRequest.NoOfRooms = hotelRequest.NoOfRooms;
                RoomGuest[] roomGuest = new RoomGuest[hotelRequest.NoOfRooms];
                for (int i = 0; i < hotelRequest.RoomGuest.Length; i++)
                {
                    roomGuest[i].NoOfAdults = hotelRequest.RoomGuest[i].noOfAdults;
                    roomGuest[i].NoOfChild = hotelRequest.RoomGuest[i].noOfChild;
                    if (hotelRequest.RoomGuest[i].noOfChild > 0)
                    {
                        roomGuest[i].ChildAge = new int[hotelRequest.RoomGuest[i].noOfChild];
                        roomGuest[i].ChildAge = hotelRequest.RoomGuest[i].childAge.ToArray();
                    }
                }
                structRequest.RoomGuestData = roomGuest;
                structRequest.HotelName = hotelRequest.HotelName;
                structRequest.Rating = hotelRequest.Rating.ToString();
                structRequest.ExtraCots = hotelRequest.ExtraCots;
                structRequest.Currency = hotelRequest.Currency;
                structRequest.AvailabilityType = hotelRequest.AvailabilityType.ToString();

                //string source = string.Empty;
                List<string> czsources = new List<string>();
                foreach (string source in hotelRequest.Sources)
                {
                    czsources.Add(source);
                }
                structRequest.Sources = czsources.ToArray();

                structRequest.IsDomestic = hotelRequest.IsDomestic.ToString();
                structRequest.SearchByArea = hotelRequest.SearchByArea.ToString();
                structRequest.IsMultiRoom = hotelRequest.IsMultiRoom.ToString();
                structRequest.MinRating = hotelRequest.MinRating;
                structRequest.MaxRating = hotelRequest.MaxRating;
                structRequest.RequestMode = hotelRequest.RequestMode.ToString();
                structRequest.PassengerNationality = hotelRequest.PassengerNationality.ToString();
                structRequest.RoomRateType = hotelRequest.RoomRateType.ToString();

                //hotelrequestDTO.Authentication = new Authentication() { UserName = "cozmo", Password = "cozmo" };
                hotelrequestDTO.HotelSearchRequest = structRequest;


                request = JsonConvert.SerializeObject(hotelrequestDTO,
                           new JsonSerializerSettings()
                           {
                               NullValueHandling = NullValueHandling.Ignore
                           });

                CZfilePath = @"" + xmlPath + "CZHotelInventorySearchRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                sw.Write(request);
                sw.Close();

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "CZ.GenerateHotelSerchRequest Err :" + ex.ToString(), "0");
                throw ex;
            }
            return request;
        }

        private List<HotelSearchResult> ReadHotelSearchResponse(string CZResponse, HotelRequest request, decimal markup, string markupType, decimal discount, string discountType, string currency, Dictionary<string, decimal> exchangeRates)
        {
            List<HotelSearchResult> searchResults = new List<HotelSearchResult>();
            HotelSearchResult result = null;

            try
            {
                var CZJresponse = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(CZResponse);

                foreach (var hotelSearchResponse in CZJresponse["result"]["hotelSearchResponse"])
                {
                    result = new HotelSearchResult();

                    result.HotelCode = Convert.ToString(hotelSearchResponse["hotelCode"].Value);
                    result.HotelName = hotelSearchResponse["hotelName"].Value;
                    result.HotelAddress = hotelSearchResponse["address"].Value;
                    result.HotelPicture = hotelSearchResponse["hotelMainImage"].Value;
                    result.HotelDescription = hotelSearchResponse["hotelDescriptions"].Value;
                    result.Rating = GetHotelRating(hotelSearchResponse["starLevel"].Value);
                    result.HotelMap = Convert.ToString(hotelSearchResponse["latitude"].Value) + "||" + Convert.ToString(hotelSearchResponse["longitude"].Value);
                    result.StartDate = request.StartDate;
                    result.EndDate = request.EndDate;
                    result.Currency = hotelSearchResponse["currency"].Value;//request.Currency;
                    result.HotelLocation = null;
                    result.IsFeaturedHotel = false;
                    result.RateType = RateType.Negotiated;
                    result.RoomGuest = request.RoomGuest;
                    result.CityCode = request.CityCode;
                    result.PropertyType = _token;//need to check later to put in session
                    List<HotelStaticData> hotelStaticData = new List<HotelStaticData>();
                    if (!string.IsNullOrEmpty(result.HotelCode))
                    {
                        HotelStaticData staticInfo = new HotelStaticData();
                        hotelStaticData = staticInfo.StaticDataLoad(string.Empty, string.Empty, result.HotelCode, (int)HotelBookingSource.HotelExtranet);
                        if (hotelStaticData != null && hotelStaticData.Count > 0)
                        {
                            result.HotelStaticID = hotelStaticData[0].HotelStaticID;
                        }
                    }
                    if (exchangeRates.ContainsKey(result.Currency))
                    {
                        rateOfExchange = exchangeRates[result.Currency];
                        result.Currency = currency;
                    }
                    else
                    {
                        result.Currency = request.Currency;
                    }
                    decimal sourcePrice = 0m;
                    decimal totalprice = sourcePrice = Convert.ToDecimal(hotelSearchResponse["totalRate"]) + Convert.ToDecimal(hotelSearchResponse["extraBedRate"]) + Convert.ToDecimal(hotelSearchResponse["childExtraBedRate"]);
                    //decimal totalprice = Convert.ToDecimal(hotelSearchResponse["totalRate"]);
                    //VAT Calucation Changes Modified 14.12.2017
                    decimal hotelTotalPrice = 0m;
                    decimal vatAmount = 0m;

                    PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                    hotelTotalPrice = Math.Round((totalprice) * rateOfExchange, decimalPoint);

                    if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                    {
                        hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, 2);
                    }
                    hotelTotalPrice = Math.Round(hotelTotalPrice, 2);

                    result.Price = new PriceAccounts();

                    result.Price.RateOfExchange = rateOfExchange;
                    result.Price.SupplierCurrency = hotelSearchResponse["currency"].Value;// "AED";
                    result.Price.SupplierPrice = sourcePrice;//Need to check with harish
                    result.SellingFare = hotelTotalPrice;
                    result.TotalPrice = result.SellingFare + (markupType == "F" ? markup * request.NoOfRooms : result.SellingFare * (markup / 100));
                    result.Price.Discount = 0;
                    if (markup > 0 && discount > 0)
                    {
                        result.Price.Discount = discountType == "F" ? discount * request.NoOfRooms : (result.TotalPrice - result.SellingFare) * (discount / 100);
                    }
                    result.Price.Discount = result.Price.Discount > 0 ? result.Price.Discount : 0;

                    result.BookingSource = HotelBookingSource.HotelExtranet;

                    searchResults.Add(result);
                }
                return searchResults;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public void GetRoomRefetchDetails(ref HotelSearchResult hotelResult, HotelRequest request, Dictionary<string, decimal> exchangeRates, decimal markup, string markupType, string sessionid, decimal discount, string discountType)
        {

            try
            {
                SimilarHotelSourceInfo[] CZHotelInventory = hotelResult.SimilarHotels.Where(f => f.Source == HotelBookingSource.HotelExtranet).ToArray();
                //hotelResult.PropertyType = CZHotelInventory[0].SourceSessionId;

                //try
                //{
                string roomData = GenerateRoomFetchRequest(CZHotelInventory[0].HotelCode, request);

                string apiUrl = string.Empty;
                apiUrl = ConfigurationSystem.HotelExtranetConfig["RoomFetchUrl"];

                string CZresponse = GetResponse(roomData, apiUrl, hotelResult.PropertyType, "RoomData");

                if (CZresponse != null)
                {
                    CZfilePath = @"" + xmlPath + "CZHotelInventoryRomFetchResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                    StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                    sw.Write(CZresponse);
                    sw.Close();

                    ReadRefetchRoomDetails(CZresponse, ref hotelResult, request, markup, markupType, discount, discountType, exchangeRates);
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GimmonixRoomDetails, Severity.Normal, 1, "Gimmoinx.ReadRefetchRoomDetails Err :" + ex.ToString(), "0");
                throw ex;
            }
        }

        private string GenerateRoomFetchRequest(string hotelcode, HotelRequest hotelRequest)
        {
            //string CZfilePath = string.Empty;
			Dictionary<string, string> Countries = null;
            DOTWCountry dotw = new DOTWCountry();
            Countries = dotw.GetAllCountries();
            Guid guid = Guid.NewGuid();
            RoomRequestDTO roomrequestDTO = new RoomRequestDTO();

            RoomFetchRequest structRoomRequest = new RoomFetchRequest();
            var request = string.Empty;
            try
            {
                structRoomRequest.StartDate = hotelRequest.StartDate.ToString("dd/MM/yyyy");
                structRoomRequest.EndDate = hotelRequest.EndDate.ToString("dd/MM/yyyy");
                structRoomRequest.HotelCode = hotelcode;
                structRoomRequest.NoOfRooms = hotelRequest.NoOfRooms;
				//structRoomRequest.PassengerNationality = hotelRequest.PassengerNationality;
				structRoomRequest.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[hotelRequest.PassengerNationality]);
                RoomGuest[] roomGuest = new RoomGuest[hotelRequest.NoOfRooms];
                for (int i = 0; i < hotelRequest.RoomGuest.Length; i++)
                {
                    roomGuest[i].NoOfAdults = hotelRequest.RoomGuest[i].noOfAdults;
                    roomGuest[i].NoOfChild = hotelRequest.RoomGuest[i].noOfChild;
                    if (hotelRequest.RoomGuest[i].noOfChild > 0)
                    {
                        roomGuest[i].ChildAge = new int[hotelRequest.RoomGuest[i].noOfChild];
                        roomGuest[i].ChildAge = hotelRequest.RoomGuest[i].childAge.ToArray();
                    }
                }
                structRoomRequest.RoomGuestData = roomGuest;
                roomrequestDTO.RoomRequest = structRoomRequest;
                //roomrequestDTO.SessionId = guid.ToString();


                request = JsonConvert.SerializeObject(roomrequestDTO,
                           new JsonSerializerSettings()
                           {
                               NullValueHandling = NullValueHandling.Ignore
                           });

                CZfilePath = @"" + xmlPath + "CZHotelInventoryRoomFetchRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                sw.Write(request);
                sw.Close();

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "CZ.GenerateRoomFetchRequest Err :" + ex.ToString(), "0");
                throw ex;
            }
            return request;
        }

        /// <summary>
        ///Read the Complete Room Details For Selected Hotel
        /// </summary>
        private void ReadRefetchRoomDetails(string CZResponse, ref HotelSearchResult hotelResult, HotelRequest request, decimal markup, string markupType, decimal discount, string discountType, Dictionary<string, decimal> exchangeRates)
        {
            string roomPax = "";
            string childAge = "";
            //int child = -1;
            int order = 1;
            //int countryId = 0;

            int days = request.EndDate.Subtract(request.StartDate).Days;

            //Dictionary<int, string> Guests = new Dictionary<int, string>();

            //foreach (RoomGuestData guest in request.RoomGuest)
            //{
            //    if (roomPax.Trim().Length > 0)
            //    {
            //        roomPax += "|" + guest.noOfAdults + "," + guest.noOfChild;
            //        Guests.Add(order, guest.noOfAdults + "," + guest.noOfChild);
            //    }
            //    else
            //    {
            //        roomPax = guest.noOfAdults + "," + guest.noOfChild;
            //        Guests.Add(order, guest.noOfAdults + "," + guest.noOfChild);
            //    }

            //    order++;
            //    if (guest.noOfChild > 0)
            //    {
            //        for (int i = 0; i < guest.childAge.Count; i++)
            //        {
            //            int age = guest.childAge[i];
            //            if (childAge.Trim().Length > 0 && !childAge.EndsWith("|"))
            //            {
            //                childAge += "," + age;
            //            }
            //            else
            //            {
            //                childAge += age.ToString();
            //            }
            //        }

            //        //if (!childAge.Contains("|"))
            //        {
            //            childAge += "|";
            //        }
            //    }
            //    else
            //    {
            //        if (childAge.Trim().Length > 0 && childAge.EndsWith("|"))
            //        {
            //            childAge += "0|";
            //        }
            //        else
            //        {
            //            childAge += "0|";
            //        }
            //    }
            //}

            //if (childAge.EndsWith("|"))
            //{
            //    childAge = childAge.Remove(childAge.Length - 1, 1);
            //}

            //DataTable dtChilds = new DataTable();

            //dtChilds.Columns.Add("Child1", typeof(string));
            //dtChilds.Columns.Add("Child2", typeof(string));
            //dtChilds.Columns.Add("Child3", typeof(string));
            //dtChilds.Columns.Add("Child4", typeof(string));
            //dtChilds.Columns.Add("Child5", typeof(string));
            //dtChilds.Columns.Add("Child6", typeof(string));

            //string[] ages = childAge.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            //dtChilds.Clear();
            //foreach (string age in ages)
            //{
            //    string[] pax = age.Split(',');
            //    DataRow row = dtChilds.NewRow();
            //    for (int i = 0; i < pax.Length; i++)
            //    {
            //        row[i] = pax[i];
            //    }
            //    dtChilds.Rows.Add(row);
            //}


            try
            {
                var CZJresponse = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(CZResponse);

                PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());

                List<HotelRoomsDetails> rooms = new List<HotelRoomsDetails>();
                //decimal totalprice = Convert.ToDecimal(hotelResult.TotalPrice);
                //decimal roomprice = totalprice / request.NoOfRooms;
                //int days = request.EndDate.Subtract(request.StartDate).Days;
                foreach (var getRoomResponse in CZJresponse["result"]["getRoomResponse"])
                {
                    int HotelId = Convert.ToInt32(getRoomResponse["hotelId"].Value);
                    foreach (var czrooms in getRoomResponse["rooms"])
                    {
                        HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                        roomDetail.SequenceNo = Convert.ToString(czrooms["roomNumber"].Value);
                        roomDetail.RoomTypeName = czrooms["roomName"].Value;
                        roomDetail.SupplierName = HotelBookingSource.HotelExtranet.ToString();
                        roomDetail.SupplierId = Convert.ToString((int)HotelBookingSource.HotelExtranet);
                        roomDetail.IsIndividualSelection = true;//Here IsIndividualSelection = true means roomselection is individual.
                        roomDetail.mealPlanDesc = czrooms["accomodation"].Value;
                        //roomDetail.RoomTypeCode = HotelId + "-" + czrooms["roomId"].Value + "-" + czrooms["periodid"].Value + "-" + czrooms["accomId"].Value + "-" + czrooms["adults"].Value + "-" + czrooms["childs"].Value;
                        roomDetail.AllocationType = czrooms["allocationType"].Value;
                        roomDetail.SellType = czrooms["sellType"].Value;
                        roomDetail.RqId = Convert.ToInt32(czrooms["rqId"].Value);
                        roomDetail.RoomTypeCode = HotelId + "-" + czrooms["roomId"].Value + "-" + czrooms["periodId"].Value + "-" + czrooms["accomId"].Value + "-" + czrooms["adults"].Value + "-" + czrooms["childs"].Value + "-" + roomDetail.SequenceNo + "-" + roomDetail.AllocationType + "-" + roomDetail.SellType + "-" + czrooms["marketId"].Value;
                        int Childs = Convert.ToInt32(czrooms["childs"].Value);
                        //int Childs = Convert.ToInt32(czrooms["noOfChild"].Value);
                        roomDetail.Occupancy = new Dictionary<string, int>();

                        //Need to extrabed column in procedure
                        if (!roomDetail.Occupancy.ContainsKey("ExtraBed"))
                        {
                            if (czrooms["extraBed"].ToString() == "Y")
                            {
                                roomDetail.Occupancy.Add("ExtraBed", 1);
                            }
                            else
                            {
                                roomDetail.Occupancy.Add("ExtraBed", 0);
                            }
                        }

                        //for (int i = 0; i < request.RoomGuest.Length; i++)
                        //{
                        //int age = 0;
                        //int age2 = 0, age3 = 0, age4 = 0, age5 = 0, age6 = 0;
                        //DataRow drChild = null;
                        //if (dtChilds.Rows.Count > 0)
                        //{
                        //    drChild = dtChilds.Rows[i];

                        //    if (drChild["Child1"] != DBNull.Value)
                        //    {
                        //        age = Convert.ToInt32(drChild["Child1"]);
                        //    }
                        //    if (drChild["Child2"] != DBNull.Value && request.RoomGuest[i].noOfChild >= 2)
                        //    {
                        //        age2 = Convert.ToInt32(drChild["Child2"]);
                        //    }

                        //    if (drChild["Child3"] != DBNull.Value && request.RoomGuest[i].noOfChild >= 3)
                        //    {
                        //        age3 = Convert.ToInt32(drChild["Child3"]);
                        //    }
                        //    if (drChild["Child4"] != DBNull.Value && request.RoomGuest[i].noOfChild >= 4)
                        //    {
                        //        age4 = Convert.ToInt32(drChild["Child4"]);
                        //    }
                        //    if (drChild["Child5"] != DBNull.Value && request.RoomGuest[i].noOfChild >= 5)
                        //    {
                        //        age5 = Convert.ToInt32(drChild["Child5"]);
                        //    }
                        //    if (drChild["Child6"] != DBNull.Value && request.RoomGuest[i].noOfChild >= 6)
                        //    {
                        //        age6 = Convert.ToInt32(drChild["Child6"]);
                        //    }
                        //}
                        //if (Convert.ToInt32(czrooms["noOfAdult"]) == request.RoomGuest[i].noOfAdults && Convert.ToInt32(czrooms["noOfChild"]) == request.RoomGuest[i].noOfChild)
                        //{
                        //    int fromAge1 = Convert.ToInt32(czrooms["roomChild1AgeRange"].ToString().Split(',')[0]);
                        //    int toAge1 = Convert.ToInt32(czrooms["roomChild1AgeRange"].ToString().Split(',')[1]);

                        //    int fromAge2 = 0;
                        //    int toAge2 = 0;
                        //    if (czrooms["roomChild2AgeRange"] != DBNull.Value)
                        //    {
                        //        fromAge2 = Convert.ToInt32(czrooms["roomChild2AgeRange"].ToString().Split(',')[0]);
                        //        toAge2 = Convert.ToInt32(czrooms["roomChild2AgeRange"].ToString().Split(',')[1]);
                        //    }

                        //    int fromAge3 = 0;
                        //    int toAge3 = 0;
                        //    if (czrooms["roomChild3AgeRange"] != DBNull.Value)
                        //    {
                        //        fromAge3 = Convert.ToInt32(czrooms["roomChild3AgeRange"].ToString().Split(',')[0]);
                        //        toAge3 = Convert.ToInt32(czrooms["roomChild3AgeRange"].ToString().Split(',')[1]);
                        //    }

                        //    //decimal childAmount = 0, totalAmount = 0;

                        //    if (Convert.ToDecimal(czrooms["childRateApplied"]) <= 0)
                        //    {
                        //        if (age > 0 && Childs >= 1)
                        //        {
                        //            if (age >= fromAge1 && age <= toAge1)
                        //            {
                        //                ReadRoomRate(1, czrooms["childRateType"].ToString(), czrooms["extraBed"].ToString(), Convert.ToDecimal(czrooms["rate"]), Convert.ToDecimal(czrooms["childRate1"]), Convert.ToDecimal(czrooms["childRate2"]), Convert.ToDecimal(czrooms["childRate3"]), Convert.ToDecimal(czrooms["totalRate"]), Convert.ToDecimal(czrooms["childRateApplied"]), ref childAmount, ref totalAmount);
                        //            }
                        //            else if (age >= fromAge2 && age <= toAge2)
                        //            {
                        //                ReadRoomRate(2, czrooms["childRateType"].ToString(), czrooms["extraBed"].ToString(), Convert.ToDecimal(czrooms["rate"]), Convert.ToDecimal(czrooms["childRate1"]), Convert.ToDecimal(czrooms["childRate2"]), Convert.ToDecimal(czrooms["childRate3"]), Convert.ToDecimal(czrooms["totalRate"]), Convert.ToDecimal(czrooms["childRateApplied"]), ref childAmount, ref totalAmount);
                        //            }
                        //            else if (age >= fromAge3 && age <= toAge3)
                        //            {
                        //                ReadRoomRate(3, czrooms["childRateType"].ToString(), czrooms["extraBed"].ToString(), Convert.ToDecimal(czrooms["rate"]), Convert.ToDecimal(czrooms["childRate1"]), Convert.ToDecimal(czrooms["childRate2"]), Convert.ToDecimal(czrooms["childRate3"]), Convert.ToDecimal(czrooms["totalRate"]), Convert.ToDecimal(czrooms["childRateApplied"]), ref childAmount, ref totalAmount);
                        //            }
                        //        }
                        //        if (age2 > 0 && Childs >= 2)
                        //        {
                        //            if (age2 >= fromAge1 && age2 <= toAge1)
                        //            {
                        //                ReadRoomRate(1, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //            else if (age2 >= fromAge2 && age2 <= toAge2)
                        //            {
                        //                ReadRoomRate(2, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //            else if (age2 >= fromAge3 && age2 <= toAge3)
                        //            {
                        //                ReadRoomRate(3, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //        }
                        //        if (age3 > 0 && Childs >= 3)
                        //        {
                        //            if (age3 >= fromAge1 && age3 <= toAge1)
                        //            {
                        //                ReadRoomRate(1, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //            else if (age3 >= fromAge2 && age3 <= toAge2)
                        //            {
                        //                ReadRoomRate(2, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //            else if (age3 >= fromAge3 && age3 <= toAge3)
                        //            {
                        //                ReadRoomRate(3, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //        }
                        //        if (age4 > 0 && Childs >= 4)
                        //        {
                        //            if (age4 >= fromAge1 && age4 <= toAge1)
                        //            {
                        //                ReadRoomRate(1, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //            else if (age4 >= fromAge2 && age4 <= toAge2)
                        //            {
                        //                ReadRoomRate(2, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //            else if (age4 >= fromAge3 && age4 <= toAge3)
                        //            {
                        //                ReadRoomRate(3, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //        }
                        //        if (age5 > 0 && Childs >= 5)
                        //        {
                        //            if (age5 >= fromAge1 && age5 <= toAge1)
                        //            {
                        //                ReadRoomRate(1, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //            else if (age5 >= fromAge2 && age5 <= toAge2)
                        //            {
                        //                ReadRoomRate(2, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //            else if (age5 >= fromAge3 && age5 <= toAge3)
                        //            {
                        //                ReadRoomRate(3, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //        }
                        //        if (age6 > 0 && Childs >= 6)
                        //        {
                        //            if (age6 >= fromAge1 && age6 <= toAge1)
                        //            {
                        //                ReadRoomRate(1, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //            else if (age6 >= fromAge2 && age6 <= toAge2)
                        //            {
                        //                ReadRoomRate(2, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //            else if (age6 >= fromAge3 && age6 <= toAge3)
                        //            {
                        //                ReadRoomRate(3, czrooms["childRateType"], czrooms["extraBed"], czrooms["rate"], czrooms["childRate1"], czrooms["childRate2"], czrooms["childRate3"], czrooms["totalRate"], czrooms["childRateApplied"], ref childAmount, ref totalAmount);
                        //            }
                        //        }
                        //    }
                        //}
                        //}

                        roomDetail.Markup = Convert.ToDecimal(czrooms["taxRate"]);
                        roomDetail.MarkupType = czrooms["taxType"].ToString();

                        decimal rate = 0m;

                        //rate = Convert.ToDecimal(czrooms["totalRate"]) + Convert.ToDecimal(czrooms["extraBedRate"]) + Convert.ToDecimal(czrooms["childExtraBedRate"]);
                        rate = Convert.ToDecimal(czrooms["totalRate"]);
                        if (rate == 0)
                        {
                            rate = Convert.ToDecimal(czrooms["rate"]);
                        }
                        roomDetail.supplierPrice = rate;
                        rateOfExchange = exchangeRates[hotelResult.Price.SupplierCurrency];

                        //VAT Calucation Changes Modified 14.12.2017
                        decimal hotelTotalPrice = 0m;
                        decimal vatAmount = 0m;
                        hotelTotalPrice = Math.Round((rate) * rateOfExchange, decimalPoint);

                        //day wise price calucation
                        System.TimeSpan diffResult = hotelResult.EndDate.Subtract(hotelResult.StartDate);
                        RoomRates[] hRoomRates = new RoomRates[diffResult.Days];
                        decimal roomAmount = roomDetail.TotalPrice;

                        for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                        {
                            decimal price = roomDetail.TotalPrice / diffResult.Days;
                            if (fareIndex == diffResult.Days - 1)
                            {
                                //price = totalprice;
                                price = roomAmount;
                            }
                            //totalprice -= price;
                            roomAmount -= price;
                            hRoomRates[fareIndex].Amount = hotelTotalPrice;
                            hRoomRates[fareIndex].BaseFare = hotelTotalPrice;
                            hRoomRates[fareIndex].SellingFare = hotelTotalPrice;
                            hRoomRates[fareIndex].Totalfare = hotelTotalPrice;
                            hRoomRates[fareIndex].RateType = RateType.Negotiated;
                            hRoomRates[fareIndex].Days = hotelResult.StartDate.AddDays(fareIndex);
                        }

                        roomDetail.Rates = hRoomRates;

                        if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                        {
                            hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, 2);
                        }
                        //hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);
                        roomDetail.TotalPrice = hotelTotalPrice;
                        roomDetail.TaxDetail = priceTaxDet;
                        roomDetail.InputVATAmount = vatAmount;

                        /*===================================================================================================================
                            *                                           Tax Calculation for Total Price
                            *                                      MarkupType is Tax Type. Markup stores Tax value.
                            *==================================================================================================================*/
                        if (roomDetail.MarkupType == "P")
                        {
                            //result.RoomDetails[k].TotalPrice += result.RoomDetails[k].TotalPrice * (result.RoomDetails[k].Markup / 100);
                            roomDetail.TotalTax = roomDetail.TotalPrice * (roomDetail.Markup / 100);
                        }
                        else
                        {
                            //result.RoomDetails[k].TotalPrice += result.RoomDetails[k].Markup;
                            roomDetail.TotalTax = roomDetail.Markup;
                        }

                        /*===================================================================================================================
                            *                   After Tax calcuation to Total clear Markup & MarkupType
                            *==================================================================================================================*/

                        roomDetail.Markup = 0;
                        roomDetail.MarkupType = "";
                        //result.Price = new PriceAccounts();
                        //result.Price.RateOfExchange = rateOfExchange;
                        //result.Price.SupplierCurrency = "AED";
                        roomDetail.SellingFare = roomDetail.TotalPrice;


                        roomDetail.Markup = ((markupType == "F") ? markup : ((roomDetail.TotalPrice + roomDetail.TotalTax) * (markup / 100m)));
                        roomDetail.TotalPrice = hotelTotalPrice + roomDetail.Markup;
                        Audit.Add(EventType.Exception, Severity.High, 1, "Room wise Markup checking:-hotel Totalprice:" + Convert.ToString(hotelResult.TotalPrice), "");
                        Audit.Add(EventType.Exception, Severity.High, 1, "Room wise Markup checking:-Markup:" + Convert.ToString(roomDetail.Markup) + " Total price:" + Convert.ToString(roomDetail.TotalPrice), "");
                        roomDetail.Discount = 0;
                        if (markup > 0 && discount > 0)
                        {
                            roomDetail.Discount = discountType == "F" ? discount : roomDetail.Markup * (discount / 100);
                        }
                        roomDetail.Discount = roomDetail.Discount > 0 ? roomDetail.Discount : 0;
                        //  roomDetail.SellingFare = roomDetail.TotalPrice;

                        List<string> HotelImages = czrooms["images"].ToObject<List<string>>();
                        hotelResult.HotelImages = HotelImages;

                        List<string> HotelFacilities = czrooms["hotelfacilities"].ToObject<List<string>>();
                        hotelResult.HotelFacilities = HotelFacilities;

                        rooms.Add(roomDetail);
                    }
                }
                rooms = rooms.OrderBy(h => h.TotalPrice).ToList();

                hotelResult.RoomDetails = rooms.ToArray();

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "CZ.GenerateRoomFetchResponse Err :" + ex.ToString(), "0");
                throw ex;
            }
        }

        /// <summary>
        /// Used to return the cancellation policies for the selected room(s) of a Hotel.
        /// </summary>
        /// <param name="resultObj">Hotel Search Result object.</param>
        /// <param name="request">Hotel Request object.</param>
        /// <param name="Rooms">List of Room Id for whom cancellation policies to be returned.</param>
        /// <returns></returns>
        public Dictionary<string, string> GetCancellationPolicy(ref HotelSearchResult resultObj, HotelRequest request, List<string> Rooms)
        {
            decimal markup = 0; string markuptype = "F";
            return GetCancellationPolicy(ref resultObj, request, Rooms, markup, markuptype);
        }

        public Dictionary<string, string> GetCancellationPolicy(ref HotelSearchResult resultObj, HotelRequest request, List<string> Rooms, decimal markup, string markuptype)
        {
            Dictionary<string, string> CancellationPolicy = new Dictionary<string, string>();

            try
            {
                string roomTypeCode = string.Empty;
                cancelInfo = string.Empty;
                foreach (string pair in Rooms)
                {

                    roomTypeCode = pair;

                    for (int i = 0; i < resultObj.RoomDetails.Length; i++)
                    {
                        if (resultObj.RoomDetails[i].RoomTypeCode == roomTypeCode)
                        {
                            CancellationPolicy = GetCancellationInfo(ref resultObj, markup, markuptype, roomTypeCode, resultObj.RoomDetails[i].AllocationType, resultObj.RoomDetails[i].SellType);
                            if (CancellationPolicy.ContainsKey("CancelPolicyJsonString"))
                            {
                                List<HotelCancelPolicy> deserializedCancelPolicyList = JsonConvert.DeserializeObject<List<HotelCancelPolicy>>(CancellationPolicy["CancelPolicyJsonString"]);
                                resultObj.RoomDetails[i].CancelPolicyList = deserializedCancelPolicyList;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CancellationPolicy;
        }

        //public Dictionary<string, string> GetCancellationInfo(ref HotelSearchResult hResult, decimal markup, string markupType, string roomTypeCode)
        public Dictionary<string, string> GetCancellationInfo(ref HotelSearchResult hResult, decimal markup, string markupType, string roomTypeCode, string AllocationType, string SellType)
        {
            Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
            //string postData = GenerateCancellationInfoRequest(ref hResult, roomTypeCode);// GETTING XML STRING...
            string postData = GenerateCancellationInfoRequest(ref hResult, roomTypeCode, AllocationType, SellType);// GETTING XML STRING...

            string apiUrl = string.Empty;
            apiUrl = ConfigurationSystem.HotelExtranetConfig["CancellationPolicyUrl"];

            string CZresponse = GetResponse(postData, apiUrl, hResult.PropertyType, "CancellationPolicy");

            if (CZresponse != null)
            {
                CZfilePath = @"" + xmlPath + "CZHotelInventoryCancellationPolicyResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                sw.Write(CZresponse);
                sw.Close();

                //rooms = ReadRoomFetchResponse(CZresponse, request, markup, markupType, discount, discountType, exchangeRates);
                cancellationInfo = ReadCancellationInfoResponse(CZresponse, ref hResult, markup, markupType, roomTypeCode);
            }

            return cancellationInfo;
        }

        //private string GenerateCancellationInfoRequest(ref HotelSearchResult hResult, string roomTypeCode)
        private string GenerateCancellationInfoRequest(ref HotelSearchResult hResult, string roomTypeCode, string AllocationType, string SellType)
        {

            SimilarHotelSourceInfo sourceInfo = hResult.SimilarHotels.Find(s => s.Source == HotelBookingSource.HotelExtranet);
            CancelPolicyRequestDto cancelPolicyRequestDTO = new CancelPolicyRequestDto();

            CancelPolicyRequest structCancelPolicyRequest = new CancelPolicyRequest();

            var request = string.Empty;
            try
            {

                //structCancelPolicyRequest.HotelCode = roomTypeCode.Split('-')[0];
                structCancelPolicyRequest.HotelCode = sourceInfo.HotelCode;
                structCancelPolicyRequest.RoomId = roomTypeCode.Split('-')[1];
                structCancelPolicyRequest.PeriodId = roomTypeCode.Split('-')[2];
                structCancelPolicyRequest.AllocationType = AllocationType;
                structCancelPolicyRequest.SellType = SellType;
                structCancelPolicyRequest.StartDate = hResult.StartDate.ToString("dd/MM/yyyy");
                structCancelPolicyRequest.EndDate = hResult.EndDate.ToString("dd/MM/yyyy");
                structCancelPolicyRequest.MarketId = roomTypeCode.Split('-')[9];
                structCancelPolicyRequest.AccomId = roomTypeCode.Split('-')[3];
                //structCancelPolicyRequest.AllocationType = hResult.RoomDetails[0].AllocationType;
                //structCancelPolicyRequest.SellType = hResult.RoomDetails[0].SellType;
                //structCancelPolicyRequest.StartDate = hResult.StartDate.ToString("dd/MM/yyyy");
                //structCancelPolicyRequest.EndDate = hResult.EndDate.ToString("dd/MM/yyyy");
                cancelPolicyRequestDTO.CancelPolicyRequest = structCancelPolicyRequest;


                request = JsonConvert.SerializeObject(cancelPolicyRequestDTO,
                           new JsonSerializerSettings()
                           {
                               NullValueHandling = NullValueHandling.Ignore
                           });

                CZfilePath = @"" + xmlPath + "CZHotelInventoryCancellationPolicyRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                sw.Write(request);
                sw.Close();

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "CZ.GenerateCancellationPolicyRequest Err :" + ex.ToString(), "0");
                throw ex;
            }
            return request;
        }

        private Dictionary<string, string> ReadCancellationInfoResponse(string CZResponse, ref HotelSearchResult hResult, decimal markup, string markupType, string roomTypeCode)
        {
            Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();

            try
            {

                var CZJresponse = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(CZResponse);

                double buffer = 0;

                if (ConfigurationSystem.HotelExtranetConfig.ContainsKey("Buffer"))
                {
                    buffer = Convert.ToDouble(ConfigurationSystem.HotelExtranetConfig["Buffer"]);
                }

                //SimilarHotelSourceInfo sourceInfo = hResult.SimilarHotels.Find(s => s.Source == HotelBookingSource.HotelConnect);

                //string endDate = "";
                string endDate = string.Empty;
                decimal amount = 0m;
                string ChargeType = "";
                string msg = "";
                int NoofRooms = 1;
                string currency = string.Empty;
                decimal CancelAmount = 0m;

                //Cancel Policy Updates
                List<HotelCancelPolicy> hotelPolicyList = new List<HotelCancelPolicy>();

                foreach (var getRoomResponse in CZJresponse["result"]["cancellationPolicyResponse"])
                {
                    if (getRoomResponse["cancellationPolicy"].Count > 0)
                    {
                        bool firstPolicy=true;
                        foreach (var cancelpolicy in getRoomResponse["cancellationPolicy"])
                        {
                            HotelCancelPolicy hotelPolicy = new HotelCancelPolicy();
                            if (cancelpolicy["refundable"].Value == true)
                            {
                                string startDate = cancelpolicy["startDate"].Value;
                                DateTime stdt = DateTime.ParseExact(startDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-(Convert.ToDouble(cancelpolicy["bufferDays"].Value)));
                                //startDate = (stdt.AddDays(-(buffer))).ToString("dd MMM yyyy HH:mm:ss");
                                startDate = stdt.ToString("dd MMM yyyy HH:mm:ss");
                                endDate = cancelpolicy["endDate"].Value;
                                DateTime eddt = DateTime.ParseExact(endDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(-(Convert.ToDouble(cancelpolicy["bufferDays"].Value)));
                                endDate = eddt.ToString("dd MMM yyyy HH:mm:ss");
                                //endDate = (eddt.AddDays(-(buffer))).ToString("dd MMM yyyy HH:mm:ss");
                                ChargeType = cancelpolicy["chargeType"].Value;
                                amount = cancelpolicy["chargeValue"].Value;

                                hotelPolicy.FromDate =Convert.ToDateTime(startDate);
                                hotelPolicy.Todate = Convert.ToDateTime(endDate);
                                hotelPolicy.BufferDays = Convert.ToInt32(cancelpolicy["bufferDays"]);
                                //hotelPolicy.BufferDays = Convert.ToInt32(Math.Round(buffer));
                                hotelPolicy.ChargeType = ChargeType;
                                hotelPolicy.Currency = hResult.Currency;

                                if (ChargeType == "P")
                                {

                                    //Cancel Policy Updates
                                    amount = Convert.ToDecimal(amount.ToString("N" + decimalPoint));
                                    hotelPolicy.Amount = amount;
                                    msg +=firstPolicy == true ? "After " + startDate + " you will be charged " + amount + "% of entire stay" : "@@After " + startDate + " you will be charged " + amount + "% of entire stay";

                                    firstPolicy = false;//if (msg.Length > 0)
                                    //{
                                    //    msg += "@@" + amount + "% of amount will be charged between " + startDate + " and " + endDate; ;
                                    //}
                                    //else
                                    //{
                                    //    msg = amount + "% of amount will be charged between " + startDate + " and " + endDate; ;
                                    //}
                                }
                                else
                                {
                                    msg += firstPolicy == true ? "After " + startDate + " you will be charged " + amount + " night(s) of Amount" : "@@After " + startDate + " you will be charged " + amount + " night(s) of Amount";

                                    firstPolicy = false;
                                    //if (msg.Length > 0)
                                    //{
                                    //    msg += "@@" + amount + " night(s) to be charged between " + startDate + " and " + endDate; ;
                                    //}
                                    //else
                                    //{
                                    //    msg = amount + " night(s) to be charged between " + startDate + " and " + endDate; ;
                                    //}
                                }
                            }
                            else
                            {
                                hotelPolicy.Amount = 0;
                                if (msg.Length > 0)
                                {
                                    msg += "@@Non-refundable and full charge will apply once booking is completed.";
                                }
                                else
                                {
                                    msg = "Non - refundable and full charge will apply once booking is completed.";
                                }
                            }
                            hotelPolicy.Remarks = msg;

                            //Cancel Policy Updates
                            hotelPolicyList.Add(hotelPolicy);


                        }
                        if (getRoomResponse["hotelNorms"] != null)
                        {
                            string terms = getRoomResponse["hotelNorms"].Value;
                            cancellationInfo.Add("HotelPolicy", terms);
                        }
                        hResult.RoomDetails[hResult.RoomDetails.ToList().IndexOf(hResult.RoomDetails.ToList().Single(h => h.RoomTypeCode == roomTypeCode))].CancellationPolicy = msg;
                        if (cancellationInfo["HotelPolicy"] != null)
                        {
                            hResult.RoomDetails[hResult.RoomDetails.ToList().IndexOf(hResult.RoomDetails.ToList().Single(h => h.RoomTypeCode == roomTypeCode))].EssentialInformation = cancellationInfo["HotelPolicy"];
                        }
                        if (!string.IsNullOrEmpty(msg))
                        {
                            if (cancelInfo.Length > 0)
                            {
                                cancelInfo += "|" + msg;
                            }
                            else
                            {
                                cancelInfo = msg;
                            }
                        }
                        cancellationInfo.Add("lastCancellationDate", !string.IsNullOrEmpty(endDate) ? endDate : Convert.ToString(hResult.EndDate));
                        cancellationInfo.Add("CancelPolicy", cancelInfo);
                        //Cancel Policy Updates
                        if (hotelPolicyList.Count > 0)
                        {
                            string cancelPolicyJsonString = JsonConvert.SerializeObject(hotelPolicyList);
                            cancellationInfo.Add("CancelPolicyJsonString", cancelPolicyJsonString);
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "CZ.ReadCancellationPolicyResponse Err :" + ex.ToString(), "0");
                throw ex;
                //throw new Exception("HIS: Failed to Read cancellation response ", ex);
            }
            return cancellationInfo;
        }

        public Dictionary<string, string> GetItemPriceCheck(HotelItinerary itinerary)
        {
            Dictionary<string, string> preBookInfo = new Dictionary<string, string>();

            string apiUrl = string.Empty;
            apiUrl = ConfigurationSystem.HotelExtranetConfig["PriceCheckUrl"];

            string postData = GenerateItemPriceCheckRequest(itinerary);// GETTING XML STRING...

            string CZresponse = GetResponse(postData, apiUrl, itinerary.PropertyType, "PriceCheck");

            if (CZresponse != null)
            {
                CZfilePath = @"" + xmlPath + "m" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                sw.Write(CZresponse);
                sw.Close();

                //rooms = ReadRoomFetchResponse(CZresponse, request, markup, markupType, discount, discountType, exchangeRates);
                preBookInfo = ReadItemPriceCheckResponse(CZresponse, itinerary);
            }
            return preBookInfo;
        }

        private string GenerateItemPriceCheckRequest(HotelItinerary itinerary)
        {
            //string CZfilePath = string.Empty;
            //Guid guid = Guid.NewGuid();
            PriceCheckRequestDTO priceCheckRequestDTO = new PriceCheckRequestDTO();

            PriceCheckRequest structPriceCheckRequest = new PriceCheckRequest();
            var request = string.Empty;
            try
            {
                structPriceCheckRequest.StartDate = itinerary.StartDate.ToString("dd/MM/yyyy");
                structPriceCheckRequest.EndDate = itinerary.EndDate.ToString("dd/MM/yyyy");
                structPriceCheckRequest.HotelCode = itinerary.HotelCode;
				structPriceCheckRequest.PassengerNationality = Country.GetCountryCodeFromCountryName(itinerary.PassengerNationality);
                RoomGuest[] roomGuest = new RoomGuest[itinerary.NoOfRooms];
                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    roomGuest[i].NoOfAdults = itinerary.Roomtype[i].AdultCount;
                    roomGuest[i].NoOfChild = itinerary.Roomtype[i].ChildCount;
                    if (itinerary.Roomtype[i].ChildCount > 0)
                    {
                        roomGuest[i].ChildAge = new int[itinerary.Roomtype[i].ChildCount];
                        roomGuest[i].ChildAge = itinerary.Roomtype[i].ChildAge.ToArray();
                    }
                    roomGuest[i].RoomId = Convert.ToInt32(itinerary.Roomtype[i].RoomTypeCode.Split('-')[1]);
                    roomGuest[i].RoomSeqNo = Convert.ToInt32(itinerary.Roomtype[i].RoomTypeCode.Split('-')[6]);
					roomGuest[i].AllocType = Convert.ToString(itinerary.Roomtype[i].RoomTypeCode.Split('-')[7]);
                    roomGuest[i].SellType = Convert.ToString(itinerary.Roomtype[i].RoomTypeCode.Split('-')[8]);
                    roomGuest[i].MarketId= Convert.ToInt32(itinerary.Roomtype[i].RoomTypeCode.Split('-')[9]);
                    roomGuest[i].AccomId= Convert.ToInt32(itinerary.Roomtype[i].RoomTypeCode.Split('-')[3]);
                }
                structPriceCheckRequest.NoOfRooms = itinerary.NoOfRooms;
                structPriceCheckRequest.RoomDetail = roomGuest;
                priceCheckRequestDTO.PriceRequest = structPriceCheckRequest;
                //roomrequestDTO.SessionId = guid.ToString();


                request = JsonConvert.SerializeObject(priceCheckRequestDTO,
                           new JsonSerializerSettings()
                           {
                               NullValueHandling = NullValueHandling.Ignore
                           });

                CZfilePath = @"" + xmlPath + "CZHotelInventoryPriceCheckRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                sw.Write(request);
                sw.Close();

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "CZ.CZHotelInventoryPriceCheckRequest Err :" + ex.ToString(), "0");
                throw ex;
            }
            return request;
        }

        /// <summary>
        /// Read preBooking Info
        /// </summary>
        /// <param name="CZZResponse"></param>
        /// <param name="hotelPrice"></param>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private Dictionary<string, string> ReadItemPriceCheckResponse(string CZResponse, HotelItinerary itinerary)
        {
            Dictionary<string, string> preBookInfo = new Dictionary<string, string>();
            string status = string.Empty;
            string difference = string.Empty;

            string cancelInfo = "";
            bool AvailQuota = true;
            decimal oldPrice = itinerary.Roomtype.Sum(x => x.Price.SupplierPrice);
            decimal newPrice = 0m;

            try
            {

                var CZJresponse = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(CZResponse);

                foreach (var getRoomResponse in CZJresponse["result"]["getRoomResponse"])
                {
                    int HotelId = Convert.ToInt32(getRoomResponse["hotelId"].Value);
                    decimal rate = 0;
                    foreach (var czrooms in getRoomResponse["rooms"])
                    {
						if(Convert.ToString(czrooms["allocationType"]) == "F")
                          {
                             //rate = Convert.ToDecimal(czrooms["totalRate"]) + Convert.ToDecimal(czrooms["extraBedRate"]) + Convert.ToDecimal(czrooms["childExtraBedRate"]) + Convert.ToDecimal(czrooms["childRate1"]) + Convert.ToDecimal(czrooms["childRate2"]) + Convert.ToDecimal(czrooms["childRate3"]) + Convert.ToDecimal(czrooms["childRate4"]);
                             rate = Convert.ToDecimal(czrooms["totalRate"]);
                             if (rate == 0)
                             {
                                  rate = Convert.ToDecimal(czrooms["rate"]);
                             }
                              newPrice += rate;
                          }
                          else
                          {
							//if (Convert.ToInt32(czrooms["quota"]) < itinerary.NoOfRooms)
							//{
							//	AvailQuota = false;
							//	break;
							//}
							//else
							//{						
								//rate = Convert.ToDecimal(czrooms["totalRate"]) + Convert.ToDecimal(czrooms["extraBedRate"]) + Convert.ToDecimal(czrooms["childExtraBedRate"]);
								rate = Convert.ToDecimal(czrooms["totalRate"]);
								if (rate == 0)
								{
									rate = Convert.ToDecimal(czrooms["rate"]);
								}
								newPrice += rate;
							//}
						}
                    }
                }

                if ((oldPrice == newPrice) && (AvailQuota = true))
                {
                    preBookInfo.Add("Status", "true");
                    preBookInfo.Add("Error", "");
                }
                else
                {
                    preBookInfo.Add("Status", "false");
                    preBookInfo.Add("Error", "Either PriceChanged (OR) Quota Unavailable");
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "CZ.CZHotelInventoryPriceCheckResponse Err :" + ex.ToString(), "0");
                throw ex;
                //throw new Exception("CZHotelInventory: Failed to Read Item Price check response ", ex);
            }
            return preBookInfo;
        }

        /// <summary>
        /// Used to Confirm booking for a selected hotel itinerary.
        /// </summary>
        /// <param name="hotelBooking">HotelItinerary object for booking.</param>
        /// <param name="objCR">ConfirmationReference object for generating Confirmation Number.</param>
        /// <param name="rooms">List of RoomBookingInformation objects for storing room passenger information.</param>
        /// <param name="fares">List of RoomFareBreakDown objects for storing day wise fare information.</param>
        /// <param name="roomQuota">List of RoomQuota objects for updating room quota after booking.</param>
        /// <returns></returns>
        public BookingResponse BookHotel(HotelItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();

            string apiUrl = string.Empty;
            apiUrl = ConfigurationSystem.HotelExtranetConfig["HotelBookUrl"];

            string postData = GenerateHotelBookingRequest(itinerary);

            string CZresponse = GetResponse(postData, apiUrl, itinerary.PropertyType, "BookHotel");

            if (CZresponse != null)
            {
                CZfilePath = @"" + xmlPath + "CZHotelInventoryBookingResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                sw.Write(CZresponse);
                sw.Close();

                bookResponse = ReadHotelBookingResponse(CZresponse, ref itinerary);
            }

            return bookResponse;
        }

        private string GenerateHotelBookingRequest(HotelItinerary itinerary)
        {
            BookHotelRequestDTO bookHotelRequestDTO = new BookHotelRequestDTO();

            BookHotelRequest structBookHotelRequest = new BookHotelRequest();
            var request = string.Empty;

            try
            {


                structBookHotelRequest.HotelName = itinerary.HotelName;
                structBookHotelRequest.BookingSource = itinerary.Source.ToString();
                structBookHotelRequest.CityCode = itinerary.CityCode;
                structBookHotelRequest.HotelCode = Convert.ToInt32(itinerary.HotelCode);
                structBookHotelRequest.AgencyReference = itinerary.AgencyReference;
                structBookHotelRequest.HotelBookingStatus = (int)HotelBookingStatus.Confirmed;
                structBookHotelRequest.StartDate = itinerary.StartDate.ToString("dd/MM/yyyy");
                structBookHotelRequest.EndDate = itinerary.EndDate.ToString("dd/MM/yyyy");
                structBookHotelRequest.Price = itinerary.Roomtype.Sum(x => x.Price.SupplierPrice);//itinerary.TotalPrice;
                structBookHotelRequest.SpecialRequest = itinerary.SpecialRequest;
                structBookHotelRequest.FlightInfo = (itinerary.FlightInfo != null ? itinerary.FlightInfo : "");
                structBookHotelRequest.CreatedBy = itinerary.CreatedBy;
                structBookHotelRequest.LastModifiedBy = itinerary.LastModifiedBy;
                structBookHotelRequest.CreatedOn = itinerary.CreatedOn;
                structBookHotelRequest.LastModifiedOn = itinerary.LastModifiedOn;
                structBookHotelRequest.Currency = itinerary.Roomtype[0].Price.SupplierCurrency;//itinerary.Currency;
                structBookHotelRequest.HotelCancelPolicy = itinerary.HotelCancelPolicy;
                structBookHotelRequest.NoOfRooms = itinerary.NoOfRooms;
                //structBookHotelRequest.SupplierCurrency = itinerary.Roomtype[0].Price.SupplierCurrency;
                HotelPassenger hotelPassenger = new HotelPassenger();
                if (itinerary.HotelPassenger != null)
                {
                    hotelPassenger.Firstname = itinerary.HotelPassenger.Title+"."+ itinerary.HotelPassenger.Firstname;
                    hotelPassenger.Lastname = itinerary.HotelPassenger.Lastname;
                    hotelPassenger.Addressline1 = itinerary.HotelPassenger.Addressline1;
                    hotelPassenger.Addressline2 = itinerary.HotelPassenger.Addressline2;
                    hotelPassenger.City = itinerary.HotelPassenger.City;
                    hotelPassenger.State = itinerary.HotelPassenger.State;
                    hotelPassenger.Country = itinerary.HotelPassenger.Country;
                    hotelPassenger.Email = itinerary.HotelPassenger.Email;
                    hotelPassenger.Phoneno = itinerary.HotelPassenger.Phoneno;
                    hotelPassenger.Countrycode = itinerary.HotelPassenger.Countrycode;
                }

                structBookHotelRequest.HotelPassenger = hotelPassenger;

                HotelRoom[] roomGuest = new HotelRoom[itinerary.NoOfRooms];
                List<HotelRoom> rooms = new List<HotelRoom>();
                //HotelRoom[] roomGuestgff;
                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    HotelRoom hotelRoom = new HotelRoom();
                    hotelRoom.RoomName = itinerary.Roomtype[i].RoomName;
                    hotelRoom.MealPlan = itinerary.Roomtype[i].MealPlanDesc;
                    hotelRoom.ExtraBed = itinerary.Roomtype[i].ExtraBed;
                    hotelRoom.MaxExtraBeds = itinerary.Roomtype[i].MaxExtraBed;
                    hotelRoom.NoOfCots = itinerary.Roomtype[i].NoOfCots;
                    hotelRoom.RoomId = itinerary.Roomtype[i].RoomId;
                    hotelRoom.PeriodId = Convert.ToInt32(itinerary.Roomtype[i].RoomTypeCode.Split('-')[2]);
                    hotelRoom.AccomId = Convert.ToInt32(itinerary.Roomtype[i].RoomTypeCode.Split('-')[3]);
                    hotelRoom.RoomSeqNo = Convert.ToInt32(itinerary.Roomtype[i].RoomTypeCode.Split('-')[6]);
                    hotelRoom.NoOfAdults = itinerary.Roomtype[i].AdultCount;
                    hotelRoom.NoOfChild = itinerary.Roomtype[i].ChildCount;
                    hotelRoom.SharingBed = itinerary.Roomtype[i].SharingBed;
                    hotelRoom.SupplierPrice = itinerary.Roomtype[i].Price.SupplierPrice;
                    hotelRoom.NetFare = itinerary.Roomtype[i].Price.NetFare;
                    hotelRoom.Tax = itinerary.Roomtype[i].Price.Tax;
                    hotelRoom.Markup = itinerary.Roomtype[i].Price.Markup;
                    hotelRoom.AllocType = itinerary.Roomtype[i].AllocType;
                    hotelRoom.SellType = itinerary.Roomtype[i].SellType;
                    hotelRoom.RoomReferenceKey = itinerary.Roomtype[i].RoomTypeCode;
                    hotelRoom.MarketId= Convert.ToInt32(itinerary.Roomtype[i].RoomTypeCode.Split('-')[9]);
                    hotelRoom.RqId = itinerary.Roomtype[i].RqId;
                    if (itinerary.Roomtype[i].ChildCount > 0)
                    {
                        hotelRoom.ChildAge = itinerary.Roomtype[i].ChildAge;
                    }

                    if (itinerary.Roomtype[i].PassenegerInfo != null)
                    {
                        List<HotelPassenger> PassengerInfo = new List<HotelPassenger>();
                        for (int j = 0; j < itinerary.Roomtype[i].PassenegerInfo.Count(); j++)
                        {
                            //i = j;

                            HotelPassenger hotelpassenger = new HotelPassenger();

                            hotelpassenger.Firstname = itinerary.Roomtype[i].PassenegerInfo[j].Firstname;
                            hotelpassenger.Lastname = itinerary.Roomtype[i].PassenegerInfo[j].Lastname;

                            PassengerInfo.Add(hotelpassenger);

                        }
                        hotelRoom.PassenegerInfo = PassengerInfo;
                        //PassengerInfo.Add(hotelPassenger);
                    }


                    HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[itinerary.Roomtype[i].RoomFareBreakDown.Length];
                    for (int k = 0; k < itinerary.Roomtype[i].RoomFareBreakDown.Length; k++)
                    {
                        HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();

                        fare.RoomId = itinerary.Roomtype[i].RoomFareBreakDown[k].RoomId;
                        fare.Date = itinerary.Roomtype[i].RoomFareBreakDown[k].Date.ToString("dd/MM/yyyy");
                        fare.RoomPrice = itinerary.Roomtype[i].RoomFareBreakDown[k].RoomPrice;
                        fareInfo[k] = fare;
                    }
                    hotelRoom.RoomFareBreakDown = fareInfo;
                    rooms.Add(hotelRoom);
                }
                roomGuest = rooms.ToArray();

                structBookHotelRequest.RoomDetail = roomGuest;
                bookHotelRequestDTO.HotelBookingRequest = structBookHotelRequest;


                request = JsonConvert.SerializeObject(bookHotelRequestDTO,
                           new JsonSerializerSettings()
                           {
                               NullValueHandling = NullValueHandling.Ignore
                           });

                CZfilePath = @"" + xmlPath + "CZHotelInventoryBookingRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                sw.Write(request);
                sw.Close();



            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "CZ.CZHotelInventoryBookHotelRequest Err :" + ex.ToString(), "0");
                throw ex;
            }

            return request;
        }

        private BookingResponse ReadHotelBookingResponse(string CZResponse, ref HotelItinerary hotelBooking)
        {
            BookingResponse bookResponse = new BookingResponse();

            try
            {

                var CZJresponse = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(CZResponse);

                foreach (var getRoomResponse in CZJresponse["result"]["hotelBookingResponse"])
                {
                    hotelBooking.ConfirmationNo = getRoomResponse["confirmationNo"];
                    bookResponse.BookingId = hotelBooking.HotelId;
                    bookResponse.ConfirmationNo = hotelBooking.ConfirmationNo;
                    bookResponse.PNR = "";
                    bookResponse.ProdType = ProductType.Hotel;
                    bookResponse.SSRDenied = false;
                    bookResponse.SSRMessage = "";
                    bookResponse.Status = BookingResponseStatus.Successful;
                    bookResponse.Error = "";
                    hotelBooking.BookingRefNo = hotelBooking.ConfirmationNo;
                    hotelBooking.Status = HotelBookingStatus.Confirmed;

                }
            }
            catch (Exception ex)
            {
                bookResponse.Status = BookingResponseStatus.Failed;
                bookResponse.Error = ex.Message;
                hotelBooking.Status = HotelBookingStatus.Failed;
                Audit.Add(EventType.Exception, Severity.Normal, 1, "CZ.CZHotelInventoryBookHotelResponse Err :" + ex.ToString(), "0");
                throw ex;
                //throw new Exception("CZHotelInventory: Failed to Read Hotel Book response ", ex);
            }
            return bookResponse;
        }

        /// <summary>
        /// Used to cancel an Hotel booking. If you want to cancel send True in second parameter. 
        /// Otherwise to know the charges for Cancellation send False in second parameter.
        /// </summary>
        /// <param name="confirmationNo">Confirmation No received while booking.</param>
        /// <param name="cancel">Default True. Send True if you want to cancel booking otherwise send False to know cancel charges.</param>
        /// <returns></returns>
        public Dictionary<string, string> CancelBooking(string confirmationNo, bool cancel)
        {

            _token = Authentication();

            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();

            string apiUrl = string.Empty;
            apiUrl = ConfigurationSystem.HotelExtranetConfig["BookingCancelUrl"];

            string postData = GenerateCancellBookingRequest(confirmationNo);

            string CZresponse = GetResponse(postData, apiUrl, _token, "CancelHotel");

            if (CZresponse != null)
            {
                CZfilePath = @"" + xmlPath + "CZHotelInventoryCancelBookingResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                sw.Write(CZresponse);
                sw.Close();

                cancellationCharges = ReadCancelBookingResponse(CZresponse);
            }

            return cancellationCharges;

        }

        private string GenerateCancellBookingRequest(string confirmationNo)
        {

            BookingCancelRequestDTO bookCancelRequestDTO = new BookingCancelRequestDTO();

            BookingCancelRequest bookcancel = new BookingCancelRequest();

            var request = string.Empty;

            try
            {
                //if(confirmationNo != "") { 
                bookcancel.ConfirmationNo = confirmationNo;
                //}

                bookCancelRequestDTO.HotelBookingCancelRequest = bookcancel;


                request = JsonConvert.SerializeObject(bookCancelRequestDTO,
                           new JsonSerializerSettings()
                           {
                               NullValueHandling = NullValueHandling.Ignore
                           });

                CZfilePath = @"" + xmlPath + "CZHotelInventoryCancelBookingRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                sw.Write(request);
                sw.Close();



            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "CZ.CZHotelInventoryCancelBookingRequest Err :" + ex.ToString(), "0");
                throw ex;
            }

            return request;
        }

        private Dictionary<string, string> ReadCancelBookingResponse(string CZResponse)
        {
            Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            decimal amount = 0m;
            string currency = string.Empty;
            string status = string.Empty;
            try
            {
                var CZJresponse = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(CZResponse);

                foreach (var getCancelBookingResponse in CZJresponse["result"]["hotelBookingCancelResponse"])
                {
                    amount = Convert.ToDecimal(getCancelBookingResponse["amount"]);
                    currency = Convert.ToString(getCancelBookingResponse["currency"]);
                    status = Convert.ToString(getCancelBookingResponse["status"]);
                    //cancellationCharges.Add("Amount", Convert.ToDecimal(getCancelBookingResponse["amount"]));
                    //cancellationCharges.Add("Currency", getCancelBookingResponse["currency"]);
                    //cancellationCharges.Add("Status", getCancelBookingResponse["status"]);
                }
                cancellationCharges.Add("Amount", amount.ToString());
                cancellationCharges.Add("Currency", currency);
                cancellationCharges.Add("Status", status);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "CZ.CZHotelInventoryReadCancelBookResponse Err :" + ex.ToString(), "0");
                throw ex;
                //throw new Exception("CZHotelInventory: Failed to Read Hotel Book response ", ex);
            }
            return cancellationCharges;
        }

        /// <summary>
        /// Used to request for cancel an Hotel booking. If you want to cancel send True in second parameter. 
        /// </summary>
        /// <param name="confirmationNo">Confirmation No received while booking.</param>
        /// <returns></returns>
        public string CancelBookingRequest(string confirmationNo, bool cancel)
        {

            _token = Authentication();

            //Dictionary<string, string> cancellationCharges = new Dictionary<string, string>();
            string message = string.Empty;

            string apiUrl = string.Empty;
            apiUrl = ConfigurationSystem.HotelExtranetConfig["CancelEmailRequestUrl"];

            string postData = GenerateEmailforCancelRequest(confirmationNo);

            string CZresponse = GetResponse(postData, apiUrl, _token, "CancelHotelEmailRequest");

            if (CZresponse != null)
            {
                CZfilePath = @"" + xmlPath + "CZHotelInventoryemailCancelBookingResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                sw.Write(CZresponse);
                sw.Close();

                //cancellationCharges = ReadCancelBookingResponse(CZresponse);
                //message = ReadEmailforCancelRequest(CZresponse);
                var CZJresponse = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(CZresponse);

                foreach (var getCancelEmailResponse in CZJresponse["result"]["hotelBookingCancelEmailResponse"])
                {
                    message = Convert.ToString(getCancelEmailResponse["message"]);
                    //cancellationCharges.Add("Amount", Convert.ToDecimal(getCancelBookingResponse["amount"]));
                    //cancellationCharges.Add("Currency", getCancelBookingResponse["currency"]);
                    //cancellationCharges.Add("Status", getCancelBookingResponse["status"]);
                }
            }

            return message;

        }

        private string GenerateEmailforCancelRequest(string confirmationNo)
        {

            BookingCancelRequestDTO bookCancelRequestDTO = new BookingCancelRequestDTO();

            BookingCancelRequest bookcancel = new BookingCancelRequest();

            var request = string.Empty;

            try
            {
                //if(confirmationNo != "") { 
                bookcancel.ConfirmationNo = confirmationNo;
                //}

                bookCancelRequestDTO.HotelBookingCancelRequest = bookcancel;


                request = JsonConvert.SerializeObject(bookCancelRequestDTO,
                           new JsonSerializerSettings()
                           {
                               NullValueHandling = NullValueHandling.Ignore
                           });

                CZfilePath = @"" + xmlPath + "CZHotelInventoryEmailforCancelBookingRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss");
                StreamWriter sw = new StreamWriter(CZfilePath + ".json");
                sw.Write(request);
                sw.Close();



            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.Normal, 1, "CZ.CZHotelInventoryEmailforCancelBookingRequest Err :" + ex.ToString(), "0");
                throw ex;
            }

            return request;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rating"></param>
        /// <returns></returns>
        HotelRating GetHotelRating(string rating)
        {
            HotelRating hotelRating = HotelRating.All;
            switch (rating)
            {
                case "1S":
                    hotelRating = HotelRating.OneStar;
                    break;
                case "2S":
                    hotelRating = HotelRating.TwoStar;
                    break;
                case "3S":
                    hotelRating = HotelRating.ThreeStar;
                    break;
                case "4S":
                    hotelRating = HotelRating.FourStar;
                    break;
                case "5S":
                    hotelRating = HotelRating.FiveStar;
                    break;
            }
            return hotelRating;
        }

        public struct HotelSearchRequest
        {
            public double Longtitude;
            public double Latitude;
            public int Radius;
            public int CityId;
            public string StartDate;
            public string EndDate;
            //public DateTime StartDate;
            //public DateTime EndDate;
            public string CityCode;
            public int NoOfRooms;
            public RoomGuest[] RoomGuestData;
            public string HotelName;
            public string Rating;
            public int ExtraCots;
            public string Currency;
            public string AvailabilityType;
            //public CZSources[] Sources;
            public string[] Sources;
            //public List<string> Sources;
            public string IsDomestic;
            public string SearchByArea;
            public string IsMultiRoom;
            public int MinRating;
            public int MaxRating;
            public string RequestMode;
            public string PassengerNationality;
            public string RoomRateType;
        }

        public struct RoomGuest
        {
            public int NoOfAdults;
            public int NoOfChild;
            public int[] ChildAge;
            public int RoomId;
            public int RoomSeqNo;
            public string AllocType;
            public string SellType;
            public int MarketId;
            public int AccomId;
        }

        public class HotelRequestDTO
        {
            public HotelSearchRequest HotelSearchRequest { get; set; }
        }

        public struct RoomFetchRequest
        {
            //public DateTime StartDate;
            //public DateTime EndDate;
            public string StartDate;
            public string EndDate;
            public string HotelCode;
            public RoomGuest[] RoomGuestData;
            public int NoOfRooms;
	    public string PassengerNationality;
        }

        public class RoomRequestDTO
        {
            public RoomFetchRequest RoomRequest { get; set; }
        }

        public struct CancelPolicyRequest
        {
            public string HotelCode;
            public string RoomId;
            public string PeriodId;
            public string AllocationType;
            public string SellType;
            public string StartDate;
            public string EndDate;
            public string MarketId;
            public string AccomId;
        }

        public class CancelPolicyRequestDto
        {
            public CancelPolicyRequest CancelPolicyRequest { get; set; }
        }

        public struct PriceCheckRequest
        {
            //public DateTime StartDate;
            //public DateTime EndDate;
            public string StartDate;
            public string EndDate;
            public string HotelCode;
            public RoomGuest[] RoomDetail;
            public int NoOfRooms;
	    public string PassengerNationality;
        }

        public class PriceCheckRequestDTO
        {
            public PriceCheckRequest PriceRequest { get; set; }
        }

        public struct HotelRoom
        {
            public int NoOfAdults;
            public int NoOfChild;
            //public int[] ChildAge;
            public List<int> ChildAge;
            public int RoomId { get; set; }
            public string RoomName { get; set; }
            public string MealPlan { get; set; }
            public bool ExtraBed { get; set; }
            public int MaxExtraBeds { get; set; }
            public int NoOfCots { get; set; }
            public int PeriodId { get; set; }
            public int AccomId { get; set; }
            public decimal SellingFare { get; set; }
            public bool SharingBed { get; set; }
            public decimal SupplierPrice { get; set; }
            public decimal TotalPrice { get; set; }
            public decimal Tax { get; set; }
            public decimal NetFare { get; set; }
            public decimal Markup { get; set; }
            public List<HotelPassenger> PassenegerInfo { get; set; }
            public HotelRoomFareBreakDown[] RoomFareBreakDown { get; set; }
            public string AllocType { get; set; }
            public string SellType { get; set; }
            public string RoomReferenceKey { get; set; }
            public int RoomSeqNo { get; set; }
            public int MarketId { get; set; }
            public int RqId { get; set; }
        }

        [Serializable]
        public class HotelPassenger
        {
            public string Firstname { get; set; }
            public string Lastname { get; set; }
            public string Addressline1 { get; set; }
            public string Addressline2 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Country { get; set; }
            public string Phoneno { get; set; }
            public string Email { get; set; }
            public string Countrycode { get; set; }
        }

        public struct HotelRoomFareBreakDown
        {
            public int RoomId { get; set; }
            public string Date { get; set; }
            public decimal RoomPrice { get; set; }
        }


        public struct BookHotelRequest
        {
            public string HotelName;
            public string BookingSource;
            public int HotelCode;
            public string CityCode;
            public string AgencyReference;
            public int HotelBookingStatus;
            public string StartDate;
            public string EndDate;
            public decimal Price;
            public string SpecialRequest;
            public string FlightInfo;
            public int CreatedBy;
            public int LastModifiedBy;
            public DateTime CreatedOn;
            public DateTime LastModifiedOn;
            public string Currency;
            public HotelRoom[] RoomDetail;
            public HotelPassenger HotelPassenger { get; set; }
            //public HotelPassenger HotelPassenger;
            //public HotelPassenger hotelPassenger;
            public string HotelCancelPolicy;
            public int NoOfRooms;
            public string ChainCode;
        }


        public class BookHotelRequestDTO
        {
            public BookHotelRequest HotelBookingRequest { get; set; }
        }

        public class BookingCancelRequest
        {
            public string ConfirmationNo { get; set; }
        }

        public class BookingCancelRequestDTO
        {
            public BookingCancelRequest HotelBookingCancelRequest { get; set; }
        }


        /// <summary>
        /// To read api token result
        /// </summary>
        public class LoginTokenResult
        {
            public override string ToString()
            {
                return AccessToken;
            }

            [JsonProperty(PropertyName = "access_token")]
            public string AccessToken { get; set; }

            [JsonProperty(PropertyName = "error")]
            public string Error { get; set; }

            [JsonProperty(PropertyName = "error_description")]
            public string ErrorDescription { get; set; }
        }
        /// <summary>
        /// To read api token request
        /// </summary>
        public class LoginTokenRequest
        {
            public string username { get; set; }

            public string password { get; set; }

            public string grant_type { get; set; }

        }

        /// <summary>
        /// To avoid repetetive code common logic is written in this method
        /// </summary>
        /// <param name="age"></param>
        /// <param name="rate"></param>
        /// <param name="childAmount"></param>
        /// <param name="totalAmount"></param>
        private void ReadRoomRate(int age, string ChildRateType, string ExtraBed, decimal Rate, decimal ChildRate1, decimal ChildRate2, decimal ChildRate3, decimal TotalRate, decimal ChildRateApplied, ref decimal childAmount, ref decimal totalAmount)
        {
            decimal childRate = 0;
            if (ChildRateType.ToString() == "P")
            {
                decimal amount = 0;
                if (ExtraBed.ToString() == "Y")
                {
                    amount = Convert.ToDecimal(Rate);
                }
                else
                {
                    amount = Convert.ToDecimal(Rate);
                }
                childRate = Convert.ToDecimal("ChildRate" + age);
                ChildRateApplied = (amount * childRate / 100);
                childAmount += (amount * childRate / 100);
                totalAmount += (amount * childRate / 100);
                TotalRate = Convert.ToDecimal(TotalRate) + (amount * childRate / 100);

            }
            else
            {
                decimal amount = Convert.ToDecimal(Rate);// / Convert.ToInt32(rate["Adults"]);

                childRate = Convert.ToDecimal("ChildRate" + age);
                ChildRateApplied = childRate;
                childAmount += childRate;
                totalAmount += childRate;
                TotalRate = Convert.ToDecimal(TotalRate) + childRate;
            }
        }

    }
}
