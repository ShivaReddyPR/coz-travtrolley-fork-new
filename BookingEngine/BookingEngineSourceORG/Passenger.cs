using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using CoreLogic;
using Technology.Configuration;

namespace Technology.BookingEngine
{
    public enum PassengerType
    {
        Adult = 1,
        Child = 2,
        Infant = 3,
        Senior = 4
    }
    [Serializable]
    public class FlightPassenger
    {
        #region Private Variables
        /// <summary>
        /// Unique Id for a passenger
        /// </summary>
        int paxId;
        /// <summary>
        /// flight id of the flight to which the pax is linked
        /// </summary>
        int flightId;
        /// <summary>
        /// First name of passenger
        /// </summary>
        string firstName;
        /// <summary>
        /// Last name of passenger
        /// </summary>
        string lastName;
        /// <summary>
        /// TODO
        /// </summary>
        string title;
        /// <summary>
        /// full Name of passenger. (Title + FName + LName).
        /// </summary>
        string fullName;
        /// <summary>
        /// Mobile phone number of passenger
        /// </summary>
        string cellPhone;
        /// <summary>
        /// Indicates if the passenger is primary (leading) passenger
        /// </summary>
        bool isLeadPax;
        /// <summary>
        /// Date of Birth of passenger
        /// </summary> 
        DateTime dateOfBirth;
        /// <summary>
        /// Type of passenger. Adult, Child, Infant or Senior
        /// </summary>
        PassengerType type;
        /// <summary>
        /// Passport no of passenger
        /// </summary>
        string passportNo;
        /// <summary>
        /// Nationality of the passenger
        /// </summary>
        Country nationality;
        /// <summary>
        /// Country issueing passport.
        /// </summary>
        Country country;
        /// <summary>
        /// City Name
        /// </summary>
        string city; 
        /// <summary>
        /// Address Line1
        /// </summary>
        string addressLine1;
        /// <summary>
        /// Address Line2
        /// </summary>
        string addressLine2;
        /// <summary>
        /// gender of the passenger
        /// </summary>
        Gender gender;
        /// <summary>
        /// EmailId of passenger
        /// </summary>
        string email;
        /// <summary>
        /// Meal Preference of passenger
        /// </summary>
        Meal meal;
        /// <summary>
        /// Seat Preference of passenger
        /// </summary>
        Seat seat;
        /// <summary>
        /// Price detail of services to passenger
        /// </summary>
        PriceAccounts price;
        /// <summary>
        /// Frequent flier airline
        /// </summary>
        string ffAirline;
        /// <summary>
        /// Frequent flier number
        /// </summary>
        string ffNumber;
        /// <summary>
        /// LastName.FirstName.Title spaces replaced with '.'.
        /// Used to identify a passenger in a booking.
        /// </summary>
        string paxKey;
        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// Expirt Date of passport
        /// </summary> 
        DateTime passportExpiry;


        #endregion

        #region Public Members
        /// <summary>
        /// Gets or Sets paxId
        /// </summary>
        public int PaxId
        {
            get
            {
                return paxId;
            }
            set
            {
                paxId = value;
            }
        }

        /// <summary>
        /// Gets of sets the Flight Id.
        /// </summary>
        public int FlightId
        {
            get
            {
                return flightId;
            }
            set
            {
                flightId = value;
            }
        }

        /// <summary>
        /// Gets or sets First name
        /// </summary>
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        /// <summary>
        /// Gets or sets Last name
        /// </summary>
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        /// <summary>
        /// Gets or sets the title of passenger
        /// </summary>
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        /// <summary>
        /// Gets full name of a passenger.
        /// </summary>
        public string FullName
        {
            get
            {
                if (fullName == null || fullName.Length == 0)
                {
                    StringBuilder fName = new StringBuilder();
                    fName.Append(title);
                    fName.Append(" ");
                    fName.Append(firstName);
                    fName.Append(" ");
                    fName.Append(lastName);
                    fullName = fName.ToString().Trim();
                }
                return fullName;
            }
        }

        /// <summary>
        /// Gets or sets the Cellphone of passenger
        /// </summary>
        public string CellPhone
        {
            get
            {
                return cellPhone;
            }
            set
            {
                cellPhone = value;
            }
        }

        /// <summary>
        /// Gets or sets the isLeadPax property.
        /// </summary>
        public bool IsLeadPax
        {
            get
            {
                return isLeadPax;
            }
            set
            {
                isLeadPax = value;
            }
        }

        /// <summary>
        /// Gets or sets Date of birth
        /// </summary>
        public DateTime DateOfBirth
        {
            get
            {
                return dateOfBirth;
            }
            set
            {
                dateOfBirth = value;
            }
        }

        /// <summary>
        /// Gets or sets the passenger type
        /// </summary>
        public PassengerType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        /// <summary>
        /// Gets or sets the passport number
        /// </summary>
        public string PassportNo
        {
            get
            {
                return passportNo;
            }
            set
            {
                passportNo = value;
            }
        }
        /// <summary>
        /// get or set the Nationality
        /// </summary>
        public Country Nationality
        {
            get
            {
                return nationality;
            }
            set
            {
                nationality = value;
            }
        }
        public Country Country
        {
            get
            {
                return country;
            }
            set
            {
                country = value;
            }
        }

        /// <summary>
        ///  gets or sets city value
        /// </summary>
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        /// <summary>
        /// Gets or sets the address line 1
        /// </summary>
        public string AddressLine1
        {
            get
            {
                return addressLine1;
            }
            set
            {
                addressLine1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the address line 2
        /// </summary>
        public string AddressLine2
        {
            get
            {
                return addressLine2;
            }
            set
            {
                addressLine2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the Gender
        /// </summary>
        public Gender Gender
        {
            get
            {
                return gender;
            }
            set
            {
                gender = value;
            }
        }

        /// <summary>
        /// Gets or sets the email id of passenger
        /// </summary>
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        /// <summary>
        /// Gets or sets the Meal preference of passenger
        /// </summary>
        public Meal Meal
        {
            get
            {
                return meal;
            }
            set
            {
                meal = value;
            }
        }

        /// <summary>
        /// Gets or sets the Seat preference
        /// </summary>
        public Seat Seat
        {
            get
            {
                return seat;
            }
            set
            {
                seat = value;
            }
        }

        /// <summary>
        /// Gets or sets the price for the services to the passenger
        /// </summary>
        public PriceAccounts Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
            }
        }

        /// <summary>
        /// Gets or sets the Frequent Flier airline of passenger
        /// </summary>
        public string FFAirline
        {
            get
            {
                return ffAirline;
            }
            set
            {
                ffAirline = value;
            }
        }

        /// <summary>
        /// Gets or sets the Frequent Flier Number of the passenger
        /// </summary>
        public string FFNumber
        {
            get
            {
                return ffNumber;
            }
            set
            {
                ffNumber = value;
            }
        }

        /// <summary>
        /// Gets PaxKey. LastName.FirstName.Title with spaces replaced with '.'.
        /// </summary>
        public string PaxKey
        {
            get
            {
                if (paxKey == null || paxKey.Length == 0)
                {
                    StringBuilder key = new StringBuilder();
                    key.Append(lastName);
                    key.Append(".");
                    key.Append(firstName);
                    if (title != null && title.Length > 0)
                    {
                        //key.Append(".");
                        key.Append(title);
                    }
                    paxKey = key.ToString().Replace(' ', '.').Trim().ToUpper();
                }
                paxKey = paxKey.Replace(".", "");
                return paxKey;
            }
        }

        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets or sets passport expiry date
        /// </summary>
        public DateTime PassportExpiry
        {
            get
            {
                return passportExpiry;
            }
            set
            {
                passportExpiry = value;
            }
        }
        #endregion

        /// <summary>
        /// Saves the information contained in Passenger object to flight passenger database
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("FlightPassenger.Save entered firstName = " + firstName + ", flightId = " + flightId);
            if (lastName == null || lastName.Length < 1)
            {
                throw new ArgumentException("Last name should be atleast 2 character long", "lastName");
            }
            if (flightId == 0)
            {
                throw new ArgumentException("Flight Id must have a value", "flightId");
            }
            if (createdBy <= 0)
            {
                throw new ArgumentException("created by must have positive integer value", "createdBy");
            }
            Trace.TraceInformation("FlightPassenger.Save : compulsory parameters verified");
            SqlParameter[] paramList = new SqlParameter[23];
            paramList[0] = new SqlParameter("@firstName", firstName);
            paramList[1] = new SqlParameter("@lastName", lastName);
            paramList[2] = new SqlParameter("@title", title);
            paramList[3] = new SqlParameter("@flightId", flightId);
            paramList[4] = new SqlParameter("@cellPhone", cellPhone);
            paramList[5] = new SqlParameter("@leadPax", isLeadPax);
            if (dateOfBirth != DateTime.MinValue)
            {
                paramList[6] = new SqlParameter("@dateOfBirth", dateOfBirth);
            }
            else
            {
                paramList[6] = new SqlParameter("@dateOfBirth", DBNull.Value);
            }
            paramList[7] = new SqlParameter("@paxType", GetPTC(type));
            paramList[8] = new SqlParameter("@passportNumber", passportNo);
            if (country != null)
            {
                paramList[9] = new SqlParameter("@countryCode", country.CountryCode);
            }
            else
            {
                paramList[9] = new SqlParameter("@countryCode", DBNull.Value);
            }
            paramList[10] = new SqlParameter("@line1", addressLine1);
            paramList[11] = new SqlParameter("@line2", addressLine2);
            paramList[12] = new SqlParameter("@email", email);
            paramList[13] = new SqlParameter("@mealCode", meal.Code);
            paramList[14] = new SqlParameter("@seatCode", seat.Code);
            paramList[15] = new SqlParameter("@priceId", price.PriceId);
            paramList[16] = new SqlParameter("@ffAirline", ffAirline);
            paramList[17] = new SqlParameter("@ffNumber", ffNumber);
            if (gender != Gender.Null)
            {
                paramList[18] = new SqlParameter("@gender", (int)gender);
            }
            else
            {
                paramList[18] = new SqlParameter("@gender", DBNull.Value);
            }
            paramList[19] = new SqlParameter("@createdBy", createdBy);
            if (city != null)
            {
                paramList[20] = new SqlParameter("@city", city);
            }
            else
            {
                paramList[20] = new SqlParameter("@city", DBNull.Value);
            }
            if (nationality != null)
            {
                paramList[21] = new SqlParameter("@nationality", nationality.CountryCode);
            }
            else
            {
                paramList[21] = new SqlParameter("@nationality", DBNull.Value);
            }


            paramList[22] = new SqlParameter("@paxId", SqlDbType.Int);
            paramList[22].Direction = ParameterDirection.Output;

            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddFlightPassenger, paramList);
            paxId = (int)paramList[22].Value;
            Trace.TraceInformation("FlightPassenger.Save exiting : paxId = " + paxId);

        }
        /// <summary>
        /// Method to load the passenger details based in the paxId
        /// </summary>
        /// <param name="paxId"></param>
        public void Load(int paxId)
        {
            Trace.TraceInformation("FlightPassenger.Load entered : paxId= " + paxId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxId", paxId);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader dr = Dal.ExecuteReaderSP(SPNames.LoadPassenger, paramList, connection);
            while (dr.Read())
            {
                addressLine1 = dr["line1"].ToString();
                if (dr["line2"] != DBNull.Value)
                {
                    addressLine2 = dr["line2"].ToString();
                }

                if (dr["city"] != DBNull.Value)
                {
                    city = dr["city"].ToString();
                }
                nationality = new Country();
                if (dr["nationality"] != DBNull.Value)
                {
                    nationality.CountryCode = dr["nationality"].ToString();
                }

                if (dr["cellPhone"] != DBNull.Value)
                {
                    cellPhone = dr["cellPhone"].ToString();
                }
                if (dr["dateOfBirth"] != DBNull.Value)
                {
                    dateOfBirth = Convert.ToDateTime(dr["dateOfBirth"]);
                }
                if (dr["gender"] != DBNull.Value)
                {
                    gender = (Gender)Enum.Parse(typeof(Gender), Convert.ToString(dr["gender"]));
                }
                email = dr["email"].ToString();
                if (dr["FFAirlineCode"] != DBNull.Value)
                {
                    ffAirline = dr["FFAirlineCode"].ToString();
                }
                if (dr["FFNumber"] != DBNull.Value)
                {
                    ffNumber = dr["FFNumber"].ToString();
                }
                firstName = dr["firstName"].ToString();
                flightId = Convert.ToInt32(dr["flightId"]);
                isLeadPax = Convert.ToBoolean(dr["leadPax"]);
                lastName = dr["lastName"].ToString();
                type = GetPassengerType(dr["paxType"].ToString());
                if (dr["title"] != DBNull.Value)
                {
                    title = dr["title"].ToString();
                }
                if (dr["seatCode"].ToString().Length != 0)
                {
                    seat = Seat.GetSeat(dr["seatCode"].ToString());
                }
                if (dr["mealCode"].ToString().Length != 0)
                {
                    meal = Meal.GetMeal(dr["mealCode"].ToString());
                }
                price = new PriceAccounts();
                price.Load(Convert.ToInt32(dr["priceId"]));
                paxId = Convert.ToInt32(dr["PaxId"]);
                if (dr["passportNumber"] != DBNull.Value)
                {
                    passportNo = dr["passportNumber"].ToString();
                }
                country = new Country();
                if (dr["countryCode"] != DBNull.Value)
                {
                    country.CountryCode = dr["countryCode"].ToString();
                }
                createdBy = Convert.ToInt32(dr["createdBy"]);
                createdOn = Convert.ToDateTime(dr["createdOn"]);
                lastModifiedBy = Convert.ToInt32(dr["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(dr["lastModifiedOn"]);
            }
            dr.Close();
            connection.Close();
        }

        /// <summary>
        /// Gets the passenger type code for a given PassengerType
        /// </summary>
        /// <param name="type">PassengerType for which PTC is needed</param>
        /// <returns>Passenger type code as string</returns>
        public static string GetPTC(PassengerType type)
        {
            switch (type)
            {
                case PassengerType.Adult:
                    return "ADT";
                case PassengerType.Child:
                    return "CNN";
                case PassengerType.Infant:
                    return "INF";
                case PassengerType.Senior:
                    return "SNN";
                default:
                    return string.Empty;
            }
        }
        /// <summary>
        /// Gets the passenger type code for a given PassengerType string
        /// </summary>
        /// <param name="type">PassengerType string for which PTC is needed</param>
        /// <returns>Passenger type code as PassengerType</returns>
        public static PassengerType GetPassengerType(string passengerType)
        {
            switch (passengerType)
            {
                case "ADT":
                    return PassengerType.Adult;
                case "CNN":
                    return PassengerType.Child;
                case "INF":
                    return PassengerType.Infant;
                case "SNN":
                    return PassengerType.Senior;
                default:
                    return PassengerType.Adult;
            }
        }

        /// <summary>
        /// Gets the list of all meal preferences with its mealCode
        /// </summary>
        /// <returns>SortedList with mealDescription as key and mealCode as value</returns>
        public static List<KeyValuePair<string, string>> GetMealPreferenceList(BookingSource source)
        {
            Trace.TraceInformation("FlightPassenger.GetMealPreferenceList entered");
            List<KeyValuePair<string, string>> mealPreferenceList = new List<KeyValuePair<string, string>>();
            // checks whether Cache contains req. details.if it contains  data retrive from the Cache.
            if (CacheData.CheckGetMealPreferenceList())
            {
                foreach (int gdsSource in CacheData.MealList.Keys)
                {
                    if (gdsSource == (int)source)
                    {
                        //mealPreferenceList = CacheData.MealList[gdsSource];
                        foreach (KeyValuePair<string, string> mealtype in CacheData.MealList[gdsSource])
                        {
                            mealPreferenceList.Add(mealtype);
                        }
                    }
                }
            }
            // If it doesnt contain in Cache retrieve data from the Database.
            else
            {
                SqlParameter[] paramList = new SqlParameter[0];
                SqlConnection connection = Dal.GetConnection();
                SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetMealPreferenceList, paramList, connection);
                while (dataReader.Read())
                {
                    if (Convert.ToInt16(dataReader["source"]) == (int)source)
                    {
                        mealPreferenceList.Add(new KeyValuePair<string, string>((string)dataReader["mealCode"], (string)dataReader["mealDescription"]));

                    }//Update the Cache.
                    if (!CacheData.MealList.ContainsKey(Convert.ToInt16(dataReader["source"])))
                    {
                        Dictionary<string, string> meal = new Dictionary<string, string>();
                        meal.Add((string)dataReader["mealCode"], (string)dataReader["mealDescription"]);
                        CacheData.MealList.Add(Convert.ToInt16(dataReader["source"]), meal);
                    }
                    else
                    {
                        CacheData.MealList[Convert.ToInt16(dataReader["source"])].Add((string)dataReader["mealCode"], (string)dataReader["mealDescription"]);
                    }
                }
                dataReader.Close();
                connection.Close();
            }
            Trace.TraceInformation("FlightPassenger.GetMealPreferenceList exiting : mealPreferenceList.Count = " + mealPreferenceList.Count);
            return mealPreferenceList;
        }

        public static SortedList GetSeatPreferenceList()
        {
            //TODO: make static properties which get loaded only once
            Trace.TraceInformation("FlightPasenger.GetSeatPreferenceList entered");
            SortedList seatPreferenceList = new SortedList();
            // checks whether Cache contains req. details.if it contains  data retrive from the Cache.
            if (CacheData.CheckGetSeatPreferenceList())
            {
                foreach (string seatName in CacheData.SeatList.Keys)
                {

                    seatPreferenceList.Add(seatName, CacheData.SeatList[seatName]);
                }
            }
            // If it doesnt contain in Cache retrieve data from the Database.
            else
            {
                SqlParameter[] paramList = new SqlParameter[0];
                SqlConnection connection = Dal.GetConnection();
                SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetSeatPreferenceList, paramList, connection);
                while (dataReader.Read())
                {
                    seatPreferenceList.Add((string)dataReader["seatDescription"], (string)dataReader["seatCode"]);
                    //Update the Cache.
                    CacheData.SeatList.Add((string)dataReader["seatDescription"], (string)dataReader["seatCode"]);
                }
                dataReader.Close();
                connection.Close();
            }
            Trace.TraceInformation("FlightPasenger.GetSeatPreferenceList exiting : seatPreferenceList.Count = " + seatPreferenceList.Count);
            return seatPreferenceList;
        }
        /// <summary>
        /// Method gets the all info about passenger corresponding to a flightId
        /// </summary>
        /// <param name="flightId">flightId</param>
        /// <returns>FlightPassenger[] for a particular flightId</returns>
        public static FlightPassenger[] GetPassengers(int flightId)
        {
            Trace.TraceInformation("FlightPassenger.GetPassengers entered : flightId = " + flightId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            DataSet dataSet = Dal.FillSP(SPNames.GetPassengersInfo, paramList);
            FlightPassenger[] passengers = new FlightPassenger[dataSet.Tables[0].Rows.Count];
            int count = 0;
            foreach (DataRow dr in dataSet.Tables[0].Rows)
            {
                passengers[count] = new FlightPassenger();
                if (dr["city"] != DBNull.Value)
                {
                    passengers[count].city = dr["city"].ToString();
                }
                passengers[count].nationality = new Country();
                if (dr["nationality"] != DBNull.Value)
                {
                    passengers[count].nationality.CountryCode  = dr["nationality"].ToString();
                }
                passengers[count].addressLine1 = dr["line1"].ToString();
                if (dr["line2"] != DBNull.Value)
                {
                    passengers[count].addressLine2 = dr["line2"].ToString();
                }
                if (dr["cellPhone"] != DBNull.Value)
                {
                    passengers[count].cellPhone = dr["cellPhone"].ToString();
                }
                if (dr["dateOfBirth"] != DBNull.Value)
                {
                    passengers[count].dateOfBirth = Convert.ToDateTime(dr["dateOfBirth"]);
                }
                if (dr["gender"] != DBNull.Value)
                {
                    passengers[count].gender = (Gender)Enum.Parse(typeof(Gender), Convert.ToString(dr["gender"]));
                }
                passengers[count].email = dr["email"].ToString();
                if (dr["FFAirlineCode"] != DBNull.Value)
                {
                    passengers[count].ffAirline = dr["FFAirlineCode"].ToString();
                }
                if (dr["FFNumber"] != DBNull.Value)
                {
                    passengers[count].ffNumber = dr["FFNumber"].ToString();
                }
                passengers[count].firstName = dr["firstName"].ToString();
                passengers[count].flightId = flightId;
                passengers[count].isLeadPax = Convert.ToBoolean(dr["leadPax"]);
                passengers[count].lastName = dr["lastName"].ToString();
                passengers[count].type = GetPassengerType(dr["paxType"].ToString());
                if (dr["title"] != DBNull.Value)
                {
                    passengers[count].Title = dr["title"].ToString();
                }
                if (dr["seatCode"].ToString().Length != 0)
                {
                    passengers[count].seat = Seat.GetSeat(dr["seatCode"].ToString());
                }
                if (dr["mealCode"].ToString().Length != 0)
                {
                    passengers[count].meal = Meal.GetMeal(dr["mealCode"].ToString());
                }
                passengers[count].Price = new PriceAccounts();
                passengers[count].Price.Load(Convert.ToInt32(dr["priceId"]));
                passengers[count].paxId = Convert.ToInt32(dr["PaxId"]);
                if (dr["passportNumber"] != DBNull.Value)
                {
                    passengers[count].passportNo = dr["passportNumber"].ToString();
                }
                passengers[count].country = new Country();
                if (dr["countryCode"] != DBNull.Value)
                {
                    passengers[count].country.CountryCode = dr["countryCode"].ToString();
                }
                passengers[count].createdBy = Convert.ToInt32(dr["createdBy"]);
                passengers[count].createdOn = Convert.ToDateTime(dr["createdOn"]);
                passengers[count].lastModifiedBy = Convert.ToInt32(dr["lastModifiedBy"]);
                passengers[count].lastModifiedOn = Convert.ToDateTime(dr["lastModifiedOn"]);
                count++;
            }
            Trace.TraceInformation("Passenger.GetPassengers exiting : Rows=" + dataSet.Tables[0].Rows.Count);
            return passengers;
        }
        public static string GetMealCode(int paxId)
        {
            string mealCode = string.Empty;
            Trace.TraceInformation("Passenger.GetMealCode entered : paxId = " + paxId);
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxId", paxId);
            SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetMealCode, paramList, connection);
            if (dataReader.Read())
            {
                mealCode = dataReader["mealCode"].ToString();
            }
            dataReader.Close();
            connection.Close();
            return mealCode;
        }

        public static FlightPassenger[] GetDummyPaxList(int adult, int child, int infant, int senior)
        {
            Trace.TraceInformation("Passenger.GettDummyPaxList entered.");
            int[] paxCount = { adult, child, infant, senior };
            FlightPassenger[] paxList = new FlightPassenger[adult + child + infant + senior];
            for (int i = 0, k = 0; i < 4; i++)
            {
                for (int j = 0; j < paxCount[i]; j++, k++)
                {
                    paxList[k] = new FlightPassenger();
                    PassengerType type = (PassengerType)(i + 1);
                    paxList[k].type = type;
                    paxList[k].title = string.Empty;
                    paxList[k].firstName = GetPTC(type) + Convert.ToChar(j + 65);
                    paxList[k].lastName = "TEST";
                    if (type == PassengerType.Infant)
                    {
                        paxList[k].dateOfBirth = DateTime.Now.AddYears(-1);
                    }
                }
            }
            return paxList;
        }

        public static string GetPaxFullName(int paxId)
        {
            Trace.TraceInformation("FlightItinerary.GetPaxFullName entered : paxId = " + paxId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxId", paxId);
            StringBuilder fullName = new StringBuilder();
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetPaxFullName, paramList, connection);
            if (dataReader.Read())
            {
                if (dataReader["title"] != DBNull.Value)
                {
                    fullName.Append((string)dataReader["title"]);
                    fullName.Append(" ");
                }
                fullName.Append((string)dataReader["firstName"]);
                fullName.Append(" ");
                fullName.Append((string)dataReader["lastName"]);
                dataReader.Close();
                connection.Close();
            }
            else
            {
                dataReader.Close();
                connection.Close();
                throw new ArgumentException("paxId not in database");
            }
            return fullName.ToString().Trim();
        }

        public override string ToString()
        {
            StringBuilder paxString = new StringBuilder(100);
            paxString.AppendFormat("{0} ", paxId);
            paxString.Append(title);
            paxString.AppendFormat(" {0} {1}", firstName, lastName);
            paxString.AppendFormat(" {0}", type.ToString());
            if (gender != Gender.Null)
            {
                paxString.AppendFormat(" {0}", gender.ToString());
            }
            if (!string.IsNullOrEmpty(email))
            {
                paxString.AppendFormat(" {0}", email);
            }
            if (!string.IsNullOrEmpty(addressLine1))
            {
                paxString.AppendFormat(" {0}", addressLine1);
            }
            if (!string.IsNullOrEmpty(addressLine2))
            {
                paxString.AppendFormat(" {0}", addressLine2);
            }
            return paxString.ToString();
        }
    }
}
