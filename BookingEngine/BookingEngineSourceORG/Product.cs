using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;
using Technology.Data;

namespace Technology.BookingEngine
{
    [Serializable]
    public class Product
    {
        /// <summary>
        /// Product Type Id
        /// </summary>
        public int ProductTypeId;
        /// <summary>
        /// Product Id
        /// </summary>
        public int ProductId;

        /// <summary>
        /// Booking Mode(Whether booking is made from Automatic,Manual or ImportPNR )
        /// </summary>
        public BookingMode BookingMode;

        public ProductType ProductType;

        public Product()
        {
            
        }
        /// <summary>
        /// Get Booking Mode of A Product
        /// </summary>
        /// <param name="productId">Unique Id of a Product</param>
        /// <param name="productType">Type of Product</param>
        /// <returns></returns>
        public static BookingMode GetBookingMode(int productId, ProductType productType)
        {
            Trace.TraceInformation("FlightItinerary.GetBookingMode entered : productId = " + productId);
            BookingMode bookingMode = new BookingMode();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@productId", productId);
            paramList[1] = new SqlParameter("@productTypeId", (int)productType);

            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetBookingMode, paramList, connection);
            if (data.Read())
            {
                bookingMode = (BookingMode)Enum.Parse(typeof(BookingMode), data["bookingModeId"].ToString());
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("FlightItinerary.GetBookingMode exiting :" + bookingMode.ToString());
            return bookingMode;
        }

        /// <summary>
        /// It will return productId 
        /// </summary>
        /// <param name="bookingId">BookingId</param>
        /// <param name="productType"> Type of product</param>
        /// <returns>return product Id</returns>
        public static int GetProductIdByBookingId(int bookingId, ProductType productType)
        {
            Trace.TraceInformation("Product.GetProductIdByBookingId entered : bookingId = " + bookingId);
            int productId = 0;
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            paramList[1] = new SqlParameter("@productTypeId", (int)productType);

            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetProductIdByBookingId, paramList, connection);
            if (data.Read())
            {
                productId = Convert.ToInt32(data["productId"]);
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("Product.GetProductIdByBookingId exiting :" + bookingId.ToString());
            return productId;
        }


        // Virtual Class for supporting multiple products
        public virtual void Save(Product product)
        {
            //No Implementation
        }
    }
}
