using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;

namespace Technology.BookingEngine
{
    public class HotelCity
    {
        /// <summary>
        /// Private variables
        /// </summary>
        private int cityId;
        private string cityCode;
        private string cityName;
        private string stateCode;
        private string stateName;
        private string countryName;
        private string countryCode;
        private string currencyCode;
        private string gtaCode;
        private string hobCode;
        private string touCode;
        private string tboCode;
        private string mikiCode;
        private string travcoCode;
        private HotelBookingSource source;
        private int index;
        private string dotwCode;
        private string wstCode;

        /// <summary>
        /// Public Properties
        /// </summary>
        public int CityId
        {
            get { return cityId; }
            set { cityId = value; }
        }
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }
        public string StateCode
        {
            get { return stateCode; }
            set { stateCode = value; }
        }
        public string StateName
        {
            get { return stateName; }
            set { stateName = value; }
        }
        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }
        public HotelBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        public int Index
        {
            get { return index; }
            set { index = value; }
        }
        public string GTACode
        {
            get { return gtaCode; }
            set { gtaCode = value; }
        }
        public string HOBCode
        {
            get { return hobCode; }
            set { hobCode = value; }
        }
        public string TOUCode
        {
            get { return touCode; }
            set { touCode = value; }
        }
        public string TBOCode
        {
            get { return tboCode; }
            set { tboCode = value; }
        }
        public string MikiCode
        {
            get { return mikiCode; }
            set { mikiCode = value; }
        }
        public string TravcoCode
        {
            get { return travcoCode; }
            set { travcoCode = value; }
        }
        public string DOTWCode
        {
            get { return dotwCode; }
            set { dotwCode = value; }
        }
        public string WSTCode
        {
            get { return wstCode; }
            set { wstCode = value; }
        }
        public static List<HotelCity> GetHotelCity(List<GTACity> gtaCityList)
        {
            List<HotelCity> hotelCityList = new List<HotelCity>();
            foreach (GTACity gtaCity in gtaCityList)
            {
                HotelCity hCity = new HotelCity();
                hCity.cityCode = gtaCity.CityCode;
                hCity.cityName = gtaCity.CityName;
                hCity.countryCode = gtaCity.CountryCode;
                hCity.countryName = gtaCity.CountryName;
                hCity.currencyCode = gtaCity.CurrencyCode;
                hCity.index = gtaCity.Index;
                hCity.source = HotelBookingSource.GTA;
            }
            return hotelCityList;
        }
        ///<summary>
        /// This method is to retrieve the result from DB corresponding to cityname
        /// </summary>
        /// <param name="cityName"> </param>
        /// <returns>A list containing the result in a list</returns>
        public List<HotelCity> GetHotelCityByCityName(string cityName, string countryName)
        {
            Trace.TraceInformation("HotelCity.GetHotelCityByCityName entered:" + cityName);
            SqlDataReader dr = null;
            SqlParameter[] paramList = new SqlParameter[0];
            List<HotelCity> cityList = new List<HotelCity>();
            SqlConnection con = Dal.GetConnection();
            if (countryName == null || countryName.Length == 0)
            {
                paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@cityName", cityName);
                dr = Dal.ExecuteReaderSP(SPNames.GetHotelCityByCityName, paramList, con);
            }
            else
            {
                paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@cityName", cityName);
                paramList[1] = new SqlParameter("@countryName", countryName);
                dr = Dal.ExecuteReaderSP(SPNames.GetHotelCityByCountryName, paramList, con);
            }
            while (dr.Read())
            {
                HotelCity city = new HotelCity();
                city.cityId = Convert.ToInt32(dr["cityId"]);
                city.cityName = Convert.ToString(dr["Destination"]);
                city.stateName = Convert.ToString(dr["stateprovince"]);
                city.countryName = Convert.ToString(dr["country"]);
                city.countryCode = Convert.ToString(dr["countryCode"]);
                city.gtaCode = Convert.ToString(dr["GTA"]);
                city.hobCode = Convert.ToString(dr["HotelBeds"]);
                city.touCode = Convert.ToString(dr["Tourico"]);
                city.tboCode = Convert.ToString(dr["TBOConnect"]);
                city.currencyCode = Convert.ToString(dr["currency"]);
                city.mikiCode = Convert.ToString(dr["Miki"]);
                city.travcoCode = Convert.ToString(dr["Travco"]);
                city.dotwCode = Convert.ToString(dr["DOTW"]);
                city.wstCode = Convert.ToString(dr["WST"]);
                cityList.Add(city);
            }
            dr.Close();
            con.Close();
            Trace.TraceInformation("HotelCity.GetHotelCityByCityName entered:" + cityName);
            return cityList;
        }
        ///<summary>
        /// This method is to save the Record into HotelCityCode DB
        /// </summary>
        /// <param name="cityId">Save a record corresponding to cityid</param>
        public void SaveHotelCity()
        {
            Trace.TraceInformation("HotelCity.SaveHotelCity entered:");
            SqlParameter[] paramList = new SqlParameter[0];
            SqlConnection con = Dal.GetConnection();
            paramList = new SqlParameter[11];
            paramList[0] = new SqlParameter("@cityId", cityId);
            paramList[1] = new SqlParameter("@gta", gtaCode);
            paramList[2] = new SqlParameter("@hotelBeds", hobCode);
            paramList[3] = new SqlParameter("@tourico", touCode);
            paramList[4] = new SqlParameter("@miki", mikiCode);
            paramList[5] = new SqlParameter("@travco", travcoCode);
            paramList[6] = new SqlParameter("@currency", currencyCode);
            paramList[7] = new SqlParameter("@tboConnect", tboCode);
            paramList[8] = new SqlParameter("@dotw", dotwCode);
            paramList[9] = new SqlParameter("@wst", wstCode);
            Dal.ExecuteNonQuerySP(SPNames.UpdateCityCode, paramList);
            con.Close();
            Trace.TraceInformation("HotelCity.SaveHotelCity exited:");
        }
        ///<summary>To get city code with bokking source</summary>
        /// <param name="cityName"></param>
        /// <param name="countryName"></param>
        /// <param name="hSource"></param>
        /// <returns></returns>
        public string GetCityCode(string cityName, string countryName, HotelBookingSource hSource)
        {
            Trace.TraceInformation("HotelCity.GetCityCode Entered");
            SqlParameter[] paramlist = new SqlParameter[2];
            paramlist[0] = new SqlParameter("@source", (int)hSource);
            paramlist[1] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = Dal.GetConnection();
            string cityCode = string.Empty;
            countryName = countryName.Trim().ToUpper();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetHotelCityCode, paramlist, connection);
            while (datareader.Read())
            {
                if (countryName.Length > 0 && countryName == datareader["countryName"].ToString().Trim().ToUpper())
                {
                    cityCode = datareader["destCode"].ToString();
                    break;
                }
                else
                {
                    cityCode = datareader["destCode"].ToString();
                }
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("HotelCity.GetCityCode exiting");
            return cityCode;
        }
        ///<summary>
        /// To retrieve information about Tourico airportcode
        /// </summary>
        /// <param name="parentCityCode"></param>
        /// <param name="cityName"></param>
        /// <returns></returns>

        public string GetTouricoAirportCode(string parentCityCode, string cityName)
        {
            Trace.TraceInformation("HotelCity.GetTouricoAirportCode Entered");
            SqlParameter[] paramlist = new SqlParameter[2];
            paramlist[0] = new SqlParameter("@parentCityCode", parentCityCode);
            paramlist[1] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = Dal.GetConnection();
            string airPortCode = string.Empty;
            cityName = cityName.Trim().ToUpper();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetTouricoAirportCode, paramlist, connection);
            while (datareader.Read())
            {
                if (cityName.Length > 0 && cityName == datareader["cityName"].ToString().Trim().ToUpper())
                {
                    airPortCode = datareader["airPortCode"].ToString();
                    break;
                }
                else
                {
                    airPortCode = datareader["airPortCode"].ToString();
                }
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("HotelCity.GetTouricoAirportCode exiting");
            return airPortCode;
        }
        /// <summary>
        /// To Retrieve GTA Code from GTACityCode DB
        /// </summary>
        /// <param name="cityName"></param>
        /// <returns></returns>
        public List<HotelCity> GetGTACode(string cityName)
        {
            List<HotelCity> cityList = new List<HotelCity>();
            Trace.TraceInformation("HotelCity.GetGTACode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityName",cityName);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetGTACode, paramlist, connection);
            while (datareader.Read())
            {
                HotelCity city = new HotelCity();
                city.cityName = Convert.ToString(datareader["cityName"]);
                city.countryName = Convert.ToString(datareader["countryName"]);
                city.gtaCode = Convert.ToString(datareader["cityCode"]);
                city.CountryCode = Convert.ToString(datareader["countryCode"]);
                cityList.Add(city);
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("HotelCity.GetHotelCityByCityName entered:" + cityName);
            return cityList;
        }
        /// <summary>
        /// To Retrieve HotelBedCode from HotelBedsDestination DB
        /// </summary>
        /// <param name="cityName"></param>
        /// <returns></returns>
        public List<HotelCity> GetHotelBedCode(string cityName)
        {
            List<HotelCity> cityList = new List<HotelCity>();
            string HobCode=null;
            Trace.TraceInformation("HotelCity.GetHotelBedCode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetHotelBedCode, paramlist, connection);
            while (datareader.Read())
            {
                HotelCity city = new HotelCity();
                city.cityName = Convert.ToString(datareader["destName"]);
                city.countryName = Convert.ToString(datareader["countryName"]);
                city.hobCode = Convert.ToString(datareader["destCode"]);
                city.CountryCode = Convert.ToString(datareader["countryCode"]);
                cityList.Add(city);
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("HotelCity.GetHotelCityByCityName entered:" + cityName);
            return cityList;
        }
        /// <summary>
        /// To Retrieve Tourico code from 
        /// </summary>
        /// <param name="cityName"></param>
        /// <returns></returns>
        public List<HotelCity> GetTouricoCode(string cityName)
        {
            List<HotelCity> cityList = new List<HotelCity>();
            string TouCode=null;
            Trace.TraceInformation("HotelCity.GetTouricoCode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetTouricoCode, paramlist, connection);
            while (datareader.Read())
            {
                HotelCity city = new HotelCity();
                city.cityName = Convert.ToString(datareader["cityName"]);
                city.countryName = Convert.ToString(datareader["country"]);
                city.touCode = Convert.ToString(datareader["airportCode"]);
                city.CountryCode = Convert.ToString(datareader["countryCode"]);
                cityList.Add(city);
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("HotelCity.GetHotelCityByCityName entered:" + cityName);
            return cityList;
        }
        ///<summary>To get citycode from IAN table</summary>
        /// <param name="parentCityCode"></param>
        /// <param name="cityName"></param>
        public void GetIANCityCode(string parentCityCode, string cityName)
        {
            Trace.TraceInformation("HotelCity.GetIANCityCode Entered");
            SqlParameter[] paramlist = new SqlParameter[2];
            paramlist[0] = new SqlParameter("@parentCityCode", parentCityCode);
            paramlist[1] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetIANCityCode, paramlist, connection);
            if (datareader.Read())
            {
                this.cityCode = (string)datareader["airPortCode"];
                this.cityName = (string)datareader["cityName"];
                if (!datareader["countryCode"].Equals(DBNull.Value))
                {
                    this.countryCode = datareader["countryCode"].ToString();
                }
                if (!datareader["stateCode"].Equals(DBNull.Value))
                {
                    this.stateCode = datareader["stateCode"].ToString();
                }
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("HotelCity.GetIANCityCode exiting");
        }
        /// <summary>
        /// To get citycode from MikiCityCode table
        /// </summary>
        /// <param name="cityName"></param>
        /// <returns></returns>
        public List<HotelCity> GetMikiCityCode(string cityName)
        {
            List<HotelCity> cityList = new List<HotelCity>();
            Trace.TraceInformation("HotelCity.GetMikiCityCode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetMikiCityCode, paramlist, connection);
            while (datareader.Read())
            {
                HotelCity city = new HotelCity();
                city.cityName = Convert.ToString(datareader["CityName"]).Trim();
                city.mikiCode = Convert.ToString(datareader["CityCode"]);
                city.CountryCode = Convert.ToString(datareader["countryCode"]);
                city.countryName = Convert.ToString(datareader["countryName"]);
                cityList.Add(city);
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("HotelCity.GetMikiCityCode exiting:" + cityName);
            return cityList;
        }
        /// <summary>
        /// To get citycode from TravcoCityCode table
        /// </summary>
        /// <param name="cityName"></param>
        /// <returns></returns>
        public List<HotelCity> GetTravcoCityCode(string cityName)
        {
            List<HotelCity> cityList = new List<HotelCity>();
            Trace.TraceInformation("HotelCity.GetTravcoCityCode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetTravcoCityCode, paramlist, connection);
            while (datareader.Read())
            {
                HotelCity city = new HotelCity();
                city.cityName = Convert.ToString(datareader["CityName"]).Trim();
                city.travcoCode = Convert.ToString(datareader["CityCode"]);
                city.countryCode = Convert.ToString(datareader["StandardCountryCodes"]);
                city.CountryName = Convert.ToString(datareader["countryName"]);
                cityList.Add(city);
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("HotelCity.GetTravcoCityCode exiting:" + cityName);
            return cityList;
        }
        ///<summary>
        /// This method is used to get DestinationId corresponding to city and country 
        /// </summary>
        /// <param name="cityName"></param>
        /// <param name="countryName"></param>
        /// <returns></returns>
        public string GetIANDestinationId(string cityName, string countryName)
        {
            Trace.TraceInformation("HotelCity.GetIANDestinationId Entered");
            SqlParameter[] paramlist = new SqlParameter[2];
            paramlist[0] = new SqlParameter("@cityName", cityName);
            paramlist[1] = new SqlParameter("@countryName", countryName);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetIANDestinationID, paramlist, connection);
            string destId = string.Empty;
            if (datareader.Read())
            {
                destId = datareader["destinationID"].ToString();
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("HotelCity.GetIANDestinationId exiting");
            return destId;
        }                
        ///<summary>
        /// To retrive state list corresponding to country
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        public static string GetIANStateList(string countryCode)
        {
            Trace.TraceInformation("HotelCity.GetIANStateList Entered");
            string stateListString = string.Empty;
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@countryCode", countryCode);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetIANStateList, paramlist, connection);
            StringBuilder stateList = new StringBuilder();
            while (datareader.Read())
            {
                if (!datareader["stateName"].Equals(DBNull.Value))
                {
                    stateList.Append(datareader["stateName"].ToString().Replace("|", "").Replace("#", ""));
                }
                if (!datareader["stateCode"].Equals(DBNull.Value))
                {
                    stateList.Append("|" + datareader["stateCode"].ToString().Replace("|", "").Replace("#", "") + "#");
                }
            }
            datareader.Close();
            connection.Close();
            stateListString = stateList.ToString();
            if (stateListString.Length > 0)
            {
                stateListString = stateListString.Remove(stateListString.Length - 1, 1);
            }
            Trace.TraceInformation("HotelCity.GetIANStateList exiting");
            return stateListString;
        }
        /// <summary>
        /// This Method is used to get currency Code
        ///  </summary>
        /// <param name="currencyCode"></param>
        /// <returns></returns>
        public static string GetCurrencySymbol(string currencyCode)
        {
            string symbol = string.Empty;
            if (currencyCode == "USD")
            {
                symbol = "$";
            }
            else if (currencyCode == "GBP")
            {
                symbol = "�";
            }
            else if (currencyCode == "EUR")
            {
                symbol = "�";
            }
            else if (currencyCode == "AUD")
            {
                symbol = "A$";
            }
            else if (currencyCode == "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
            {
                symbol = "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "";
            }
            else
            {
                symbol = currencyCode;
            }
            return symbol;
        }
        /// <summary>
        /// This method gets the list of all distinct countries in HotelCityCode table
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllHotelCountries()
        {
            Trace.TraceInformation("HotelCity.GetAllHotelCountries Entered");
            SqlParameter[] paramList = new SqlParameter[0];
            SqlConnection con = Dal.GetConnection();
            List<string> countryList = new List<string>();
            SqlDataReader dr = Dal.ExecuteReaderSP(SPNames.GetHotelCountries, paramList, con);
            while (dr.Read())
            {
                countryList.Add(Convert.ToString(dr["country"]));
            }
            dr.Close();
            con.Close();
            Trace.TraceInformation("HotelCity.GetAllHotelCountries Entered");
            return countryList;
        }

        /// <summary>
        /// This method gets the list of all cities in the given country
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        public List<HotelCity> GetHotelCities(string country)
        {
            Trace.TraceInformation("Hotelcity.GetHotelCities Entered:" + country);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@country", country);
            List<HotelCity> cityList = new List<HotelCity>();
            SqlConnection con = Dal.GetConnection();
            SqlDataReader dr = Dal.ExecuteReaderSP(SPNames.GetHotelCities, paramList, con);
            while (dr.Read())
            {
                HotelCity city = new HotelCity();
                city.cityId = Convert.ToInt32(dr["cityId"]);
                city.cityName = (dr["stateprovince"] == DBNull.Value || Convert.ToString(dr["stateprovince"]) == "") ? Convert.ToString(dr["destination"]) : Convert.ToString(dr["destination"]) + ",   " + Convert.ToString(dr["stateprovince"]);
                cityList.Add(city);
            }
            dr.Close();
            con.Close();
            Trace.TraceInformation("Hotelcity.GetHotelCities Exited:" + country);
            return cityList;
        }

        /// <summary>
        /// This method gets the a HotelCity based on cityId
        /// </summary>
        /// <param name="cityIdNo"></param>
        /// <returns></returns>
        public static HotelCity Load(int cityIdNo)
        {
            Trace.TraceInformation("HotelCity.Load entered:"+cityIdNo);
            SqlParameter[] paramList=new SqlParameter[1];
            paramList[0]=new SqlParameter("@cityId",cityIdNo) ;
            HotelCity city = null;
            SqlConnection con=Dal.GetConnection();
            SqlDataReader dr = Dal.ExecuteReaderSP(SPNames.LoadHotelCity, paramList, con);
            if (dr.Read())
            {
                city = new HotelCity();
                city.cityName = Convert.ToString(dr["destination"]);
                city.cityId = Convert.ToInt32(dr["cityId"]);
                city.countryCode = Convert.ToString(dr["countryCode"]);
                city.countryName = Convert.ToString(dr["country"]);
                city.currencyCode = Convert.ToString(dr["currency"]);
                if (dr["destinationId"] != DBNull.Value)
                {
                    city.cityCode = Convert.ToString(dr["destinationId"]);
                }
                else
                {
                    city.cityCode = string.Empty;
                }
                if (dr["stateprovince"] != DBNull.Value)
                {
                    city.stateCode = Convert.ToString(dr["stateprovince"]);
                }
                else
                {
                    city.stateCode = string.Empty;
                }
                if (dr["GTA"] != DBNull.Value)
                {
                    city.gtaCode = Convert.ToString(dr["GTA"]);
                }
                else
                {
                    city.gtaCode = string.Empty;
                }
                if (dr["HotelBeds"] != DBNull.Value)
                {
                    city.hobCode = Convert.ToString(dr["HotelBeds"]);
                }
                else
                {
                    city.hobCode = string.Empty;
                }
                if (dr["Tourico"] != DBNull.Value)
                {
                    city.touCode = Convert.ToString(dr["Tourico"]);
                }
                else
                {
                    city.touCode = string.Empty;
                }
                if (dr["TBOConnect"] != DBNull.Value)
                {
                    city.tboCode = Convert.ToString(dr["TBOConnect"]);
                }
                else
                {
                    city.tboCode = string.Empty;
                }
                if (dr["Miki"] != DBNull.Value)
                {
                    city.mikiCode = Convert.ToString(dr["Miki"]);
                }
                else
                {
                    city.mikiCode = string.Empty;
                }
                if (dr["Travco"] != DBNull.Value)
                {
                    city.travcoCode = Convert.ToString(dr["Travco"]);
                }
                else
                {
                    city.travcoCode = string.Empty;
                }
                if (dr["DOTW"] != DBNull.Value)
                {
                    city.dotwCode = Convert.ToString(dr["DOTW"]);
                }
                else
                {
                    city.dotwCode = string.Empty;
                }
                if (dr["WST"] != DBNull.Value)
                {
                    city.wstCode = Convert.ToString(dr["WST"]);
                }
                else
                {
                    city.wstCode = string.Empty;
                }
            }
            dr.Close();
            con.Close();
            Trace.TraceInformation("HotelCity.Load exited");
            return city;
        }

        /// <summary>
        /// This method returns corresponding cityId of the given city.
        /// </summary>
        /// <param name="cityName"></param>
        /// <returns></returns>
        public static int GetCityIdFromCityName(string cityName)
        {
            Trace.TraceInformation("HotelCity.GetCityIdFromCityName entered:" + cityName);
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@cityName", cityName);
            paramList[1]=new SqlParameter("@cityId",SqlDbType.Int);
            paramList[1].Direction=ParameterDirection.Output;
            int rowsAffected=Dal.ExecuteNonQuerySP(SPNames.GetCityIdFromCityName, paramList);
            Trace.TraceInformation("HotelCity.GetCityIdFromCityName exited:" + cityName);
            if (paramList[1].Value != DBNull.Value)
            {
                return Convert.ToInt32(paramList[1].Value);
            }
            else
            {
                return 0;
            }
        }        
    }
}



    
