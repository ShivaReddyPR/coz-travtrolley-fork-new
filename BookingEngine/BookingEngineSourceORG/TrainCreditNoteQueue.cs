using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using Technology.Data;
using Technology.Configuration;
using System.Xml;
using System.IO;
using CoreLogic;

namespace Technology.BookingEngine
{
   public class TrainCreditNoteQueue
    {
        /// <summary>
        /// bookingId of the booking
        /// </summary>
        private int bookingId;
        /// <summary>
        /// pnr of the booking
        /// </summary>
        private string pnr = string.Empty;
        /// <summary>
        /// Ticket Number of the credit note
        /// </summary>
        private string ticketNumber = string.Empty;
        /// <summary>
        /// Agency id of the requesting agency
        /// </summary>
        private int agencyId;
        /// <summary>
        /// passengerId of the credit note 
        /// </summary>
        private int paxId;
        /// <summary>
        /// passenger Name of the credit note
        /// </summary>
        private string paxName = string.Empty;
        /// <summary>
        /// price Id of the booking
        /// </summary>
        private int priceId;
        /// <summary>
        /// admin fee of ticket refund
        /// </summary>
        private decimal adminFee;
        /// <summary>
        /// supplier fee of ticket refund
        /// </summary>
        private decimal supplierFee;
        /// <summary>
        /// creditNoteNo
        /// </summary>
        private string creditNoteNo = string.Empty;
        /// <summary>
        /// details regarding credit note
        /// </summary>
        private string detail = string.Empty;
        /// <summary>
        /// Creation date of credit note
        /// </summary>
        private DateTime createdOn;
        /// <summary>
        /// Logged member Id,who makes credit note 
        /// </summary>
        private int createdBy;
        /// <summary>
        /// last modification date of credit note
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// last modification memberId
        /// </summary>
        private int lastModifiedBy;
        /// <summary>
        /// published fare of ticket
        /// </summary>
        private decimal totalFare;
        /// <summary>
        /// AgencyName
        /// </summary>
        private string agencyName = string.Empty;
        /// <summary>
        /// Travelling Date
        /// </summary>
        private DateTime travelDate;
        /// <summary>
        /// Booking Class of passenger
        /// </summary>
        private string bookingClass = string.Empty;
        /// <summary>
        /// Passenger type
        /// </summary>
       private TrainPaxType paxType;
       /// <summary>
       /// other charges like superfast,bedroll.
       /// </summary>
       private decimal otherCharges;


        public int BookingId
        {
            get
            {
                return bookingId;
            }
            set
            {
                bookingId = value;
            }
        }
        public string Pnr
        {
            get
            {
                return pnr;
            }
            set
            {
                pnr = value;
            }
        }
        public string TicketNumber
        {
            get
            {
                return ticketNumber;
            }
            set
            {
                ticketNumber = value;
            }
        }
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }
        public int PaxId
        {
            get
            {
                return paxId;
            }
            set
            {
                paxId = value;
            }
        }
        public string PaxName
        {
            get
            {
                return paxName;
            }
            set
            {
                paxName = value;
            }
        }
        public int PriceId
        {
            get
            {
                return priceId;
            }
            set
            {
                priceId = value;
            }
        }
        public decimal SupplierFee
        {
            get
            {
                return supplierFee;
            }
            set
            {
                supplierFee = value;
            }
        }
        public decimal AdminFee
        {
            get
            {
                return adminFee;
            }
            set
            {
                adminFee = value;
            }
        }
        public string CreditNoteNo
        {
            get
            {
                return creditNoteNo;
            }
            set
            {
                creditNoteNo = value;
            }
        }
        public string Detail
        {
            get
            {
                return detail;
            }
            set
            {
                detail = value;
            }
        }
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

       public decimal TotalFare
       {
           get
           {
               return totalFare;
           }
           set
           {
               totalFare = value;
           }
       }
       public string AgencyName
       {
           get
           {
               return agencyName;
           }
           set
           {
               agencyName = value;
           }
       }
       public DateTime TravelDate
       {
           get
           {
               return travelDate;
           }
           set
           {
               travelDate = value;
           }
       }
       public string BookingClass
       {
           get
           {
               return bookingClass;

           }
           set
           {
               bookingClass = value;

           }
       }
       public TrainPaxType PaxType
       {
           get
           {
               return paxType;

           }
           set
           {
               paxType = value;

           }
       }

       public decimal OtherCharges
       {
           get
           {
               return otherCharges;
           }
           set
           {
               otherCharges = value;
           }
       }


       /// <summary>
       /// saves train credit note queue for credit note
       /// </summary>
       public void Save()
       {
           Trace.TraceInformation("TrainCreditNoteQueue.Save entered : ");
           SqlConnection connection = Dal.GetConnection();
           SqlParameter[] paramList = new SqlParameter[17];
           paramList[0] = new SqlParameter("@bookingId", this.bookingId);
           paramList[1] = new SqlParameter("@ticketNumber", this.ticketNumber);
           paramList[2] = new SqlParameter("@pnr", this.pnr);
           paramList[3] = new SqlParameter("@agencyId", this.agencyId);
           paramList[4] = new SqlParameter("@paxId", this.paxId);
           paramList[5] = new SqlParameter("@paxName", this.paxName);
           paramList[6] = new SqlParameter("@priceId", this.priceId);
           paramList[7] = new SqlParameter("@adminFee", this.adminFee);
           paramList[8] = new SqlParameter("@supplierFee", this.supplierFee);
           paramList[9] = new SqlParameter("@creditNoteNo", this.creditNoteNo);
           paramList[10] = new SqlParameter("@detail", this.detail);
           paramList[11] = new SqlParameter("@createdOn", this.createdOn);
           paramList[12] = new SqlParameter("@createdBy", this.createdBy);
           paramList[13] = new SqlParameter("@lastModifiedOn", this.lastModifiedOn);
           paramList[14] = new SqlParameter("@lastModifiedBy", this.lastModifiedBy);
           paramList[15] = new SqlParameter("@totalPrice", this.totalFare);
           paramList[16] = new SqlParameter("@otherCharges", this.otherCharges);
           int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddTrainCreditNoteQueue, paramList);
           Trace.TraceInformation("TrainCreditNoteQueue.Save exiting rowsAffected = " + rowsAffected);
       }

       public static List<TrainCreditNoteQueue> GetTrainCreditNoteQueueData(int startRow, int endRow, string whereString)
       {
           List<TrainCreditNoteQueue> queues = new List<TrainCreditNoteQueue>();
           int i = 0;
           int bookingNo = 0;
           try
           {
               SqlParameter[] paramList = new SqlParameter[3];
               paramList[0] = new SqlParameter("@startrow", startRow);
               paramList[1] = new SqlParameter("@endrow", endRow);
               paramList[2] = new SqlParameter("@whereString", whereString); 
               using (DataSet dataSet = Dal.FillSP(SPNames.GetTrainCreditNoteQueue, paramList))
               {
                   if (dataSet.Tables.Count > 0)
                   {
                       DataTable creditnotequeue = dataSet.Tables[0];
                       foreach (DataRow reader in creditnotequeue.Rows)
                       {
                           string auditMessage = null;
                           TrainCreditNoteQueue queue = new TrainCreditNoteQueue();
                           if (bookingNo == 0)
                           {
                               bookingNo = Convert.ToInt32(reader["bookingId"]);
                               i = 1;
                           }
                           queue.BookingId = Convert.ToInt32(reader["bookingId"]);
                           queue.Pnr = Convert.ToString(reader["pnr"]);
                           queue.TicketNumber = Convert.ToString(reader["ticketNumber"]);
                           queue.AgencyId = Convert.ToInt32(reader["agencyId"]);
                           queue.PaxId = Convert.ToInt32(reader["paxId"]);
                           queue.PaxName = Convert.ToString(reader["paxName"]);
                           queue.PriceId = Convert.ToInt32(reader["priceId"]);
                           queue.AdminFee = Convert.ToDecimal(reader["adminFee"]);
                           queue.SupplierFee = Convert.ToDecimal(reader["supplierFee"]);
                           queue.CreditNoteNo = Convert.ToString(reader["creditNoteNo"]);
                           queue.AgencyName = Convert.ToString(reader["agencyName"]);
                           queue.Detail = Convert.ToString(reader["detail"]);
                           queue.TotalFare = Convert.ToDecimal(reader["totalPrice"]);
                           queue.OtherCharges = Convert.ToDecimal(reader["otherCharges"]);
                           if (bookingNo != queue.bookingId)
                           {
                               bookingNo = queue.bookingId;
                               i++;
                           }
                           queues.Add(queue);
                       }
                   }
                   dataSet.Dispose();
               }
           }
           catch (Exception ex)
           {
               CoreLogic.Audit.Add(CoreLogic.EventType.ChangeRequest, CoreLogic.Severity.High, 0, ex.Message + ex.StackTrace, "");
           }
           Trace.TraceInformation("Queue.GetChangeRequestData Exited :queuesCount=" + queues.Count);
           return queues;
       }

       public static List<TrainCreditNoteQueue> GetTrainCreditNote(string creditNoteNo)
       {
           List<TrainCreditNoteQueue> queues = new List<TrainCreditNoteQueue>();
           try
           {
               SqlParameter[] paramList = new SqlParameter[1];
               paramList[0] = new SqlParameter("@creditNoteN", creditNoteNo);
               using (DataSet dataSet = Dal.FillSP(SPNames.GetTrainCreditNote, paramList))
               {
                   if (dataSet.Tables.Count > 0)
                   {
                       DataTable creditnotequeue = dataSet.Tables[0];
                       foreach (DataRow reader in creditnotequeue.Rows)
                       {
                           TrainCreditNoteQueue queue = new TrainCreditNoteQueue();
                           queue.BookingId = Convert.ToInt32(reader["bookingId"]);
                           queue.Pnr = Convert.ToString(reader["pnr"]);
                           queue.TicketNumber = Convert.ToString(reader["ticketNumber"]);
                           queue.AgencyId = Convert.ToInt32(reader["agencyId"]);
                           queue.PaxId = Convert.ToInt32(reader["paxId"]);
                           queue.PaxName = Convert.ToString(reader["paxName"]);
                           queue.PriceId = Convert.ToInt32(reader["priceId"]);
                           queue.AdminFee = Convert.ToDecimal(reader["adminFee"]);
                           queue.SupplierFee = Convert.ToDecimal(reader["supplierFee"]);
                           queue.CreditNoteNo = Convert.ToString(reader["creditNoteNo"]);
                           queue.TotalFare = Convert.ToDecimal(reader["totalPrice"]);
                           queue.OtherCharges = Convert.ToDecimal(reader["otherCharges"]);
                           queue.AgencyName = Convert.ToString(reader["agencyName"]);
                           queue.Detail = Convert.ToString(reader["detail"]);
                           queue.CreatedOn = Convert.ToDateTime(reader["createdOn"]);
                           queue.TravelDate = Convert.ToDateTime(reader["travelDate"]);
                           queue.CreatedBy = Convert.ToInt32(reader["createdBy"]);
                           queue.BookingClass=Convert.ToString(reader["bookingClass"]);
                           queue.PaxType = (TrainPaxType)Enum.Parse(typeof(TrainPaxType), reader["paxType"].ToString());
                           queues.Add(queue);
                       }
                   }
                   
                dataSet.Dispose();
            }
          }
          catch (Exception ex)
          {
             CoreLogic.Audit.Add(CoreLogic.EventType.IRCTCCancelTicket, CoreLogic.Severity.High, 0, ex.Message + ex.StackTrace, "");
          }
        Trace.TraceInformation("Queue.GetChangeRequestData Exited..." );
        return queues;
       }

       public static int GetCreditNoteCount(int agencyId, string whereString)
       {
           Trace.TraceInformation("Queue.getCreditNoteCount entered...");
           DataSet dataset = new DataSet();
           int queueCount = 0;
           SqlParameter[] paramList = new SqlParameter[1];
           paramList[0] = new SqlParameter("@whereString", whereString);
           dataset = Dal.FillSP(SPNames.GetTrainCreditNoteCount, paramList);
           if (dataset.Tables.Count != 0)
           {
               queueCount = Convert.ToInt32(dataset.Tables[0].Rows[0]["count"]);
           }
           dataset.Dispose();
           Trace.TraceInformation("Queue.getCreditNoteCount exited: Total Credit Note = " + queueCount);
           return queueCount;
       }
    }
}
