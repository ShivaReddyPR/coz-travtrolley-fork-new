using System;
using System.Collections.Generic;
using System.Text;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Diagnostics;

namespace Technology.BookingEngine
{
    /// <summary>
    /// Enum for Type of Transfer Change Policy
    /// </summary>
    public enum TransferChangePoicyType
    {
        Amendment = 1,
        Namechange = 2,
        Other = 3
    }
    [Serializable]
   public class TransferPenalty : Technology.BookingEngine.Product
    {
        //private Variables
        int transferId;
        TransferChangePoicyType policyType;
        TransferBookingSource bookingSource;
        bool isAllowed;
        DateTime fromDate;
        DateTime toDate;
        decimal price;
        string remarks;
        //public Properties.
        public int TransferId
        {
            get { return transferId; }
            set { transferId = value; }
        }
        public TransferChangePoicyType PolicyType
        {
            get { return policyType; }
            set { policyType = value; }
        }
        public TransferBookingSource BookingSource
        {
            get { return bookingSource; }
            set { bookingSource = value; }
        }
        public bool IsAllowed
        {
            get { return isAllowed; }
            set { isAllowed = value; }
        }
        public DateTime FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }
        public DateTime ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }
        public decimal Price
        {
            get { return price; }
            set { price = value; }
        }
        /// <summary>
        /// This Method is used to Save the Transfer Penality Data
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("TransferPenality.Save entered.");
            SqlParameter[] paramList = new SqlParameter[8];
            paramList[0] = new SqlParameter("@transferID", this.transferId);
            paramList[1] = new SqlParameter("@policyType", Convert.ToInt16(this.policyType));
            paramList[2] = new SqlParameter("@bookingSource", Convert.ToInt16(this.bookingSource));
            paramList[3] = new SqlParameter("@isAllowed", this.isAllowed);
            paramList[4] = new SqlParameter("@fromDate", this.fromDate);
            paramList[5] = new SqlParameter("@toDate", this.toDate);
            paramList[6] = new SqlParameter("@price", this.price);
            paramList[7] = new SqlParameter("@remarks", this.remarks);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.SaveTransferPenality, paramList);
            Trace.TraceInformation("TransferPenality.Save exiting");
        }
        /// <summary>
        /// This Method is used to Retrive all Transfer Penality details based on Transfer ID
        /// </summary>
        /// <param name="transferId"></param>
        /// <returns></returns>
        public List<TransferPenalty> GetTransferPenality(int transferId)
        {
            Trace.TraceInformation("TransferPenality.GetTransferPenality entered.transfer ID:" + transferId);
            List<TransferPenalty> penalityList = new List<TransferPenalty>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@transferId", transferId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetTransferPenality, paramList, connection);
            while (data.Read())
            {
                TransferPenalty penalityInfo = new TransferPenalty();
                penalityInfo.TransferId = transferId;
                penalityInfo.IsAllowed = Convert.ToBoolean(data["isAllowed"]);
                penalityInfo.PolicyType = (TransferChangePoicyType)(Convert.ToInt32(data["policyType"]));
                penalityInfo.Price = Convert.ToDecimal(data["price"]);
                penalityInfo.Remarks = data["remarks"].ToString();
                penalityInfo.ToDate = Convert.ToDateTime(data["toDate"]);
                penalityInfo.FromDate = Convert.ToDateTime(data["fromDate"]);
                penalityInfo.BookingSource = (TransferBookingSource)(Convert.ToInt32(data["transferSource"]));
                penalityList.Add(penalityInfo);
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("TransferPenality.GetTransferPenality exiting");
            return penalityList;
        }

        /// <summary>
        /// This menthod is used to delete the Penality info for a Transfer.
        /// </summary>
        /// <param name="transferId"></param>
        public void DeletePenalityInfo(int transferId)
        {
            Trace.TraceInformation("TransferPenality.DeletePenalityInfo entered.Transfer ID:" + transferId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@transferId", transferId);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.DeleteTransferPenality, paramList);
            Trace.TraceInformation("TransferPenality.DeletePenalityInfo exiting  Result:" + rowsAffected);
        }
    }
}
