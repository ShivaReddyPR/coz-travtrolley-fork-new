using System;
using System.Collections.Generic;
using System.Text;

namespace Technology.BookingEngine
{
    public enum SearchType
    {
        OneWay = 1,
        Return = 2,
        MultiWay = 3
    }
    [Serializable]
    public class SearchRequest
    {
        SearchType type;
        bool? isDomestic = null;
        bool restrictAirline;
        FlightSegment[] segments;
        int adultCount;
        int childCount;
        int seniorCount;
        int infantCount;
        private List<string> sources;

        // For Unrestricted carrier
        string unrestrictedcarrier;

        public string Unrestrictedcarrier
        {
            get
            {
                return unrestrictedcarrier;
            }
            set
            {
                unrestrictedcarrier = value;
            }
        }

        public SearchType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }
        public bool RestrictAirline
        {
            get
            {
                return restrictAirline;
            }
            set
            {
                restrictAirline = value;
            }
        }
        public bool IsDomestic
        {
            get
            {
                if (!isDomestic.HasValue)
                {
                    Airport airport = new Airport();
                    if (airport.GetCountryCode(Segments[0].Origin) != airport.GetCountryCode(Segments[0].Destination) || airport.GetCountryCode(Segments[0].Origin) != "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "")
                    {
                        isDomestic = false;
                    }
                    else
                    {
                        isDomestic = true;
                    }
                }
                return isDomestic.Value;
            }
        }

        public FlightSegment[] Segments
        {
            get
            {
                return segments;
            }
            set
            {
                segments = value;
            }
        }

        public int AdultCount
        {
            get
            {
                return adultCount;
            }
            set
            {
                adultCount = value;
            }
        }

        public int ChildCount
        {
            get
            {
                return childCount;
            }
            set
            {
                childCount = value;
            }
        }

        public int SeniorCount
        {
            get
            {
                return seniorCount;
            }
            set
            {
                seniorCount = value;
            }
        }

        public int InfantCount
        {
            get
            {
                return infantCount;
            }
            set
            {
                infantCount = value;
            }
        }

        /// <summary>
        /// List of GDS Sources
        /// </summary>
        public List<string> Sources
        {
            get
            {
                return sources;
            }
            set
            {
                sources = value;
            }
        }

        /// <summary>
        /// Get the dummy SearchRequest Object
        /// </summary>
        /// <param name="airline">airline source</param>
        /// <param name="origin">origin city</param>
        /// <param name="destination">destination city</param>
        /// <returns></returns>
        public static SearchRequest GetDummySearchRequest(string airline, string origin, string destination, DateTime date)
        {
            SearchRequest req = new SearchRequest();
            req.Segments = new FlightSegment[1];
            FlightSegment fs = new FlightSegment();
            fs.Origin = origin;
            fs.Destination = destination;
            fs.PreferredAirlines = new string[0];
            fs.PreferredDepartureTime = date;
            req.AdultCount = 1;
            req.ChildCount = 0;
            req.InfantCount = 0;
            req.SeniorCount = 0;
            req.Type = SearchType.OneWay;
            req.RestrictAirline = false;
            List<string> sources = new List<string>();
            sources.Add(airline);
            fs.flightCabinClass = CabinClass.Economy;
            req.Sources = sources;
            req.Segments[0] = fs;
            return req;
        }

        public SearchRequest Copy()
        {
            SearchRequest request = new SearchRequest();
            request.adultCount = adultCount;
            request.childCount = childCount;
            request.infantCount = infantCount;
            request.seniorCount = seniorCount;
            request.isDomestic = isDomestic;
            request.restrictAirline = restrictAirline;
            request.segments = new FlightSegment[segments.Length];
            for (int i = 0; i < segments.Length; i++)
            {
                request.segments[i] = segments[i].Copy();
            }
            if (sources != null)
            {
                request.sources = new List<string>(sources);
            }   // else nothing to assign. the value will remain null.
            request.type = type;
            request.unrestrictedcarrier = unrestrictedcarrier;
            return request;
        }
    }
}
