using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Technology.Data;

namespace Technology.BookingEngine
{
    public class BApiHotelBookingDetail
    {
        #region private fields
        PaymentGatewaySource paySource;
        string paymentId;
        int referenceId;
        decimal paymentAmount;
        bool isDomestic;
        string bookingId;
        string confirmationNo;
        HotelRating starRating;
        int agencyId;
        string passengerInfo;
        string cityRef;
        string hotelName;
        DateTime checkInDate;
        DateTime checkOutDate;
        int noOfRooms;
        HotelApiBookingStatus bookingStatus;
        HotelBookingSource source;
        string bookingRefNo;
        bool voucherStatus;
        int adultCount;
        int childCount;
        string country;
        string remarks;
        DateTime createdOn;
        int createdBy;
        DateTime lastModifiedOn;
        int lastModifiedBy;
        #endregion

        # region Public Properties
        public PaymentGatewaySource PaySource
        {
            set
            {
                paySource = value;
            }
            get
            {
                return paySource;
            }
        }

        public string PaymentId
        {
            set
            {
                paymentId = value;
            }
            get
            {
                return paymentId;
            }
        }

        public int ReferenceId
        {
            set
            {
                referenceId = value;
            }
            get
            {
                return referenceId;
            }
        }

        public decimal PaymentAmount
        {
            set
            {
                paymentAmount = value;
            }
            get
            {
                return paymentAmount;
            }
        }

        public bool IsDomestic
        {
            set
            {
                isDomestic = value;
            }
            get
            {
                return isDomestic;
            }
        }

        public string BookingId
        {
            set
            {
                bookingId = value;
            }
            get
            {
                return bookingId;
            }
        }

        public string ConfirmationNo
        {
            set
            {
                confirmationNo = value;
            }
            get
            {
                return confirmationNo;
            }
        }

        public HotelRating StarRating
        {
            set
            {
                starRating = value;
            }
            get
            {
                return starRating;
            }
        }

        public int AgencyId
        {
            set
            {
                agencyId = value;
            }
            get
            {
                return agencyId;
            }
        }

        public string PassengerInfo
        {
            set
            {
                passengerInfo = value;
            }
            get
            {
                return passengerInfo;
            }
        }

        public string CityRef
        {
            set
            {
                cityRef = value;
            }
            get
            {
                return cityRef;
            }
        }

        public string HotelName
        {
            set
            {
                hotelName = value;
            }
            get
            {
                return hotelName;
            }
        }

        public DateTime CheckInDate
        {
            set
            {
                checkInDate = value;
            }
            get
            {
                return checkInDate;
            }
        }

        public DateTime CheckOutDate
        {
            set
            {
                checkOutDate = value;
            }
            get
            {
                return checkOutDate;
            }
        }

        public int NoOfRooms
        {
            set
            {
                noOfRooms = value;
            }
            get
            {
                return noOfRooms;
            }
        }

        public HotelApiBookingStatus BookingStatus
        {
            set
            {
                bookingStatus = value;
            }
            get
            {
                return bookingStatus;
            }
        }

        public HotelBookingSource Source
        {
            set
            {
                source = value;
            }
            get
            {
                return source;
            }
        }

        public string BookingRefNo
        {
            set
            {
                bookingRefNo = value;
            }
            get
            {
                return bookingRefNo;
            }
        }

        public bool VoucherStatus
        {
            set
            {
                voucherStatus = value;
            }
            get
            {
                return voucherStatus;
            }
        }

        public int AdultCount
        {
            set
            {
                adultCount = value;
            }
            get
            {
                return adultCount;
            }
        }

        public int ChildCount
        {
            set
            {
                childCount = value;
            }
            get
            {
                return childCount;
            }
        }

        public string Country
        {
            set
            {
                country = value;
            }
            get
            {
                return country;
            }
        }

        public string Remarks
        {
            set
            {
                remarks = value;
            }
            get
            {
                return remarks;
            }
        }

        public DateTime CreatedOn
        {
            set
            {
                createdOn = value;
            }
            get
            {
                return createdOn;
            }
        }

        public DateTime LastModifiedOn
        {
            set
            {
                lastModifiedOn = value;
            }
            get
            {
                return lastModifiedOn;
            }
        }

        public int CreatedBy
        {
            set
            {
                createdBy = value;
            }
            get
            {
                return createdBy;
            }
        }

        public int LastModifiedBy
        {
            set
            {
                lastModifiedBy = value;
            }
            get
            {
                return lastModifiedBy;
            }
        }
        # endregion

        #region Class Constructors
        public BApiHotelBookingDetail()
        {
        }
        #endregion

        # region Methods


        public void Load(int refId, int agencyId)
        {
            Trace.TraceInformation("BApiHotelBookingDetail.Load entered : referenceId = " + refId);
            referenceId = refId;
            if (referenceId <= 0)
            {
                throw new ArgumentException("referenceId should be positive integer", "referenceId");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@referenceId", referenceId);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetBApiHotelBookingDetailByRefId, paramList, connection);
            if (data.Read())
            {
                lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                createdBy = Convert.ToInt32(data["createdBy"]);
                createdOn = Convert.ToDateTime(data["createdOn"]);
                if (data["remarks"] != DBNull.Value)
                {
                    remarks = Convert.ToString(data["remarks"]);
                }
                else
                {
                    remarks = string.Empty;
                }
                country = Convert.ToString(data["country"]);
                childCount = Convert.ToInt32(data["childCount"]);
                adultCount = Convert.ToInt32(data["adultCount"]);
                voucherStatus = Convert.ToBoolean(data["voucherStatus"]);
                if (data["bookingRefNo"] != DBNull.Value)
                {
                    bookingRefNo = Convert.ToString(data["bookingRefNo"]);
                }
                else
                {
                    bookingRefNo = null;
                }
                source = (HotelBookingSource)Enum.Parse(typeof(HotelBookingSource), data["source"].ToString());
                bookingStatus = (HotelApiBookingStatus)Enum.Parse(typeof(HotelApiBookingStatus), Convert.ToString(data["bookingStatus"]));
                noOfRooms = Convert.ToInt32(data["noOfRooms"]);
                checkOutDate = Convert.ToDateTime(data["checkOutDate"]);
                checkInDate = Convert.ToDateTime(data["checkInDate"]);
                hotelName = Convert.ToString(data["hotelName"]);
                cityRef = Convert.ToString(data["cityRef"]);
                passengerInfo = Convert.ToString(data["passengerInfo"]);
                agencyId = Convert.ToInt32(data["agencyId"]);
                starRating = (HotelRating)Enum.Parse(typeof(HotelRating), Convert.ToString(data["starRating"]));
                if (data["confirmationNo"] != DBNull.Value)
                {
                    confirmationNo = Convert.ToString(data["confirmationNo"]);
                }
                else
                {
                    confirmationNo = string.Empty;
                }
                if (data["bookingId"] != DBNull.Value)
                {
                    bookingId = Convert.ToString(data["bookingId"]);
                }
                else
                {
                    bookingId = string.Empty;
                }
                isDomestic = Convert.ToBoolean(data["isDomestic"]);
                paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                if (data["paymentId"] != DBNull.Value)
                {
                    paymentId = Convert.ToString(data["paymentId"]);
                }
                else
                {
                    paymentId = string.Empty;
                }
                paySource = (PaymentGatewaySource)Enum.Parse(typeof(PaymentGatewaySource), Convert.ToString(data["paymentGatewaySourceId"]));                
                data.Close();
                connection.Close();
                Trace.TraceInformation("BApiHotelBookingDetail.Load exiting :" + referenceId.ToString());
            }
            else
            {
                data.Close();
                connection.Close();
                Trace.TraceInformation("BApiHotelBookingDetail.Load exiting : referenceId does not exist.referenceId = " + referenceId.ToString());
                throw new ArgumentException("referenceId does not exist in database");
            }
        }

        /// <summary>
        /// loads a single trip.
        /// </summary>
        /// <param name="hotelTripId">hotel trip id</param>
        /// <returns>ITimesHotelBookingDetails</returns>
        public void Load(int refId)
        {
            Trace.TraceInformation("BApiHotelBookingDetail.Load entered : referenceId = " + refId);
            referenceId = refId;
            if (referenceId <= 0)
            {
                throw new ArgumentException("referenceId should be positive integer", "referenceId");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@referenceId", referenceId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetBApiHotelBookingDetailByReferenceId, paramList, connection);
            if (data.Read())
            {
                lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                createdBy = Convert.ToInt32(data["createdBy"]);
                createdOn = Convert.ToDateTime(data["createdOn"]);
                if (data["remarks"] != DBNull.Value)
                {
                    remarks = Convert.ToString(data["remarks"]);
                }
                else
                {
                    remarks = string.Empty;
                }
                country = Convert.ToString(data["country"]);
                childCount = Convert.ToInt32(data["childCount"]);
                adultCount = Convert.ToInt32(data["adultCount"]);
                voucherStatus = Convert.ToBoolean(data["voucherStatus"]);
                if (data["bookingRefNo"] != DBNull.Value)
                {
                    bookingRefNo = Convert.ToString(data["bookingRefNo"]);
                }
                else
                {
                    bookingRefNo = null;
                }
                source = (HotelBookingSource)Enum.Parse(typeof(HotelBookingSource), data["source"].ToString());
                bookingStatus = (HotelApiBookingStatus)Enum.Parse(typeof(HotelApiBookingStatus), Convert.ToString(data["bookingStatus"]));
                noOfRooms = Convert.ToInt32(data["noOfRooms"]);
                checkOutDate = Convert.ToDateTime(data["checkOutDate"]);
                checkInDate = Convert.ToDateTime(data["checkInDate"]);
                hotelName = Convert.ToString(data["hotelName"]);
                cityRef = Convert.ToString(data["cityRef"]);
                passengerInfo = Convert.ToString(data["passengerInfo"]);
                agencyId = Convert.ToInt32(data["agencyId"]);
                starRating = (HotelRating)Enum.Parse(typeof(HotelRating), Convert.ToString(data["starRating"]));
                if (data["confirmationNo"] != DBNull.Value)
                {
                    confirmationNo = Convert.ToString(data["confirmationNo"]);
                }
                else
                {
                    confirmationNo = string.Empty;
                }
                if (data["bookingId"] != DBNull.Value)
                {
                    bookingId = Convert.ToString(data["bookingId"]);
                }
                else
                {
                    bookingId = string.Empty;
                }
                isDomestic = Convert.ToBoolean(data["isDomestic"]);
                paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                if (data["paymentId"] != DBNull.Value)
                {
                    paymentId = Convert.ToString(data["paymentId"]);
                }
                else
                {
                    paymentId = string.Empty;
                }
                paySource = (PaymentGatewaySource)Enum.Parse(typeof(PaymentGatewaySource), Convert.ToString(data["paymentGatewaySourceId"]));
                data.Close();
                connection.Close();
                Trace.TraceInformation("BApiHotelBookingDetail.Load exiting :" + referenceId.ToString());
            }
            else
            {
                data.Close();
                connection.Close();
                Trace.TraceInformation("BApiHotelBookingDetail.Load exiting : referenceId does not exist.referenceId = " + referenceId.ToString());
                throw new ArgumentException("referenceId does not exist in database");
            }
        }

        public static List<BApiHotelBookingDetail> GetBApiHotelDetail(int pageNumber, int noOfRecordsPerPage, int totalRecords, string whereString)
        {
            int endrow = 0;
            int startrow = ((noOfRecordsPerPage * (pageNumber - 1)) + 1);
            if ((startrow + noOfRecordsPerPage) - 1 < totalRecords)
            {
                endrow = (startrow + noOfRecordsPerPage) - 1;
            }
            else
            {
                endrow = totalRecords;
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@startrow", startrow);
            paramList[1] = new SqlParameter("@endrow", endrow);
            paramList[2] = new SqlParameter("@whereString", whereString);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetBApiHotelDetail, paramList, connection);
            List<BApiHotelBookingDetail> bookingDetail = new List<BApiHotelBookingDetail>();
            while (data.Read())
            {
                BApiHotelBookingDetail detail = new BApiHotelBookingDetail();
                detail.referenceId = Convert.ToInt32(data["referenceId"]);
                detail.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                detail.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                detail.createdBy = Convert.ToInt32(data["createdBy"]);
                detail.createdOn = Convert.ToDateTime(data["createdOn"]);
                if (data["remarks"] != DBNull.Value)
                {
                    detail.remarks = Convert.ToString(data["remarks"]);
                }
                else
                {
                    detail.remarks = string.Empty;
                }
                detail.country = Convert.ToString(data["country"]);
                detail.childCount = Convert.ToInt32(data["childCount"]);
                detail.adultCount = Convert.ToInt32(data["adultCount"]);
                detail.voucherStatus = Convert.ToBoolean(data["voucherStatus"]);
                if (data["bookingRefNo"] != DBNull.Value)
                {
                    detail.bookingRefNo = Convert.ToString(data["bookingRefNo"]);
                }
                else
                {
                    detail.bookingRefNo = null;
                }
                detail.source = (HotelBookingSource)Enum.Parse(typeof(HotelBookingSource), data["source"].ToString());
                detail.bookingStatus = (HotelApiBookingStatus)Enum.Parse(typeof(HotelApiBookingStatus), Convert.ToString(data["bookingStatus"]));
                detail.noOfRooms = Convert.ToInt32(data["noOfRooms"]);
                detail.checkOutDate = Convert.ToDateTime(data["checkOutDate"]);
                detail.checkInDate = Convert.ToDateTime(data["checkInDate"]);
                detail.hotelName = Convert.ToString(data["hotelName"]);
                detail.cityRef = Convert.ToString(data["cityRef"]);
                detail.passengerInfo = Convert.ToString(data["passengerInfo"]);
                detail.agencyId = Convert.ToInt32(data["agencyId"]);
                detail.starRating = (HotelRating)Enum.Parse(typeof(HotelRating), Convert.ToString(data["starRating"]));
                if (data["confirmationNo"] != DBNull.Value)
                {
                    detail.confirmationNo = Convert.ToString(data["confirmationNo"]);
                }
                else
                {
                    detail.confirmationNo = string.Empty;
                }
                if (data["bookingId"] != DBNull.Value)
                {
                    detail.bookingId = Convert.ToString(data["bookingId"]);
                }
                else
                {
                    detail.bookingId = string.Empty;
                }
                detail.isDomestic = Convert.ToBoolean(data["isDomestic"]);
                detail.paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                if (data["paymentId"] != DBNull.Value)
                {
                    detail.paymentId = Convert.ToString(data["paymentId"]);
                }
                else
                {
                    detail.paymentId = string.Empty;
                }
                detail.paySource = (PaymentGatewaySource)Enum.Parse(typeof(PaymentGatewaySource), Convert.ToString(data["paymentGatewaySourceId"]));                
                bookingDetail.Add(detail);
            }
            data.Close();
            connection.Close();
            return bookingDetail;
        }

        public static int GetBApiHotelDetailCount(string whereString)
        {
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@whereString", whereString);
            SqlDataReader reader = Dal.ExecuteReaderSP(SPNames.GetBApiHotelDetailCount, paramList, connection);
            int detailCount = 0;
            if (reader.Read())
            {
                detailCount = Convert.ToInt32(reader["total"]);
            }
            reader.Close();
            return detailCount;
        }

        public int Save()
        {
            if (referenceId == 0)
            {
                SqlParameter[] paramList = new SqlParameter[24];
                paramList[0] = new SqlParameter("@paymentGatewaySourceId", (int)(paySource));
                if (paymentId == null)
                {
                    paramList[1] = new SqlParameter("@paymentId", DBNull.Value);
                }
                else
                {
                    paramList[1] = new SqlParameter("@paymentId", paymentId);
                }
                paramList[2] = new SqlParameter("@paymentAmount", paymentAmount);
                paramList[3] = new SqlParameter("@isDomestic", isDomestic);
                if (bookingId == null)
                {
                    paramList[4] = new SqlParameter("@bookingId", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@bookingId", bookingId);
                }
                if (confirmationNo == null)
                {
                    paramList[5] = new SqlParameter("@confirmationNo", DBNull.Value);
                }
                else
                {
                    paramList[5] = new SqlParameter("@confirmationNo", confirmationNo);
                }
                paramList[6] = new SqlParameter("@starRating", (int)starRating);
                paramList[7] = new SqlParameter("@agencyId", agencyId);
                paramList[8] = new SqlParameter("@passengerInfo", passengerInfo);
                paramList[9] = new SqlParameter("@cityRef", cityRef);
                paramList[10] = new SqlParameter("@hotelName", hotelName);
                if (remarks == null || remarks == "")
                {
                    paramList[11] = new SqlParameter("@remarks", DBNull.Value);
                }
                else
                {
                    paramList[11] = new SqlParameter("@remarks", remarks);
                } 
                paramList[12] = new SqlParameter("@checkInDate", checkInDate);
                paramList[13] = new SqlParameter("@checkOutDate", checkOutDate);
                paramList[14] = new SqlParameter("@noOfRooms", noOfRooms);
                paramList[15] = new SqlParameter("@bookingStatus", (int)bookingStatus);
                paramList[16] = new SqlParameter("@source", (int)source);
                paramList[17] = new SqlParameter("@country", country);               
                if (bookingRefNo == null)
                {
                    paramList[18] = new SqlParameter("@bookingRefNo", DBNull.Value);
                }
                else
                {
                    paramList[18] = new SqlParameter("@bookingRefNo", bookingRefNo);
                }
                paramList[19] = new SqlParameter("@voucherStatus", voucherStatus);
                paramList[20] = new SqlParameter("@adultCount", adultCount);
                paramList[21] = new SqlParameter("@childCount", childCount);
                paramList[22] = new SqlParameter("@createdBy", createdBy);
                paramList[23] = new SqlParameter("@referenceId", SqlDbType.Int);
                paramList[23].Direction = ParameterDirection.Output;
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddBApiHotelBookingDetail, paramList);
                if (rowsAffected > 0)
                {
                    return (int)paramList[23].Value;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[7];

                if (bookingId == null || bookingId == "")
                {
                    paramList[0] = new SqlParameter("@bookingId", DBNull.Value);
                }
                else
                {
                    paramList[0] = new SqlParameter("@bookingId", bookingId);
                }
                paramList[1] = new SqlParameter("@voucherStatus", voucherStatus);
                paramList[2] = new SqlParameter("@bookingStatus", (int)bookingStatus);
                if (confirmationNo == null || confirmationNo == "")
                {
                    paramList[3] = new SqlParameter("@confirmationNo", DBNull.Value);
                }
                else
                {
                    paramList[3] = new SqlParameter("@confirmationNo", confirmationNo);
                }
                if (remarks == null || remarks == "")
                {
                    paramList[4] = new SqlParameter("@remarks", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@remarks", remarks);
                }
                paramList[5] = new SqlParameter("@referenceId", referenceId);
                paramList[6] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateBApiHotelBookingDetail, paramList);
                if (rowsAffected > 0)
                {
                    return referenceId;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Searches in hotel itinerary table
        /// </summary>
        /// <param name="confirmationNo">confirmationNo</param>
        /// <param name="source">source</param>
        /// <returns>void</returns>
        public static void IsConfExistAndVouchered(string confirmationNo, HotelBookingSource source, out bool isExists, out bool isVouchered, out int bookingId)
        {
            isExists = false;
            isVouchered = false;
            bookingId = 0;
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@confNo", confirmationNo);
            paramList[1] = new SqlParameter("@source", (int)source);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.IsConfExistAndVouchered, paramList, connection);
            if (data.Read())
            {
                isExists = true;
                isVouchered = Convert.ToBoolean(data["voucherStatus"]);
                bookingId = Convert.ToInt32(data["bookingId"]);
            }
            data.Close();
            connection.Close();
            return;
        }

        /// <summary>
        /// Searches in hotel itinerary table
        /// </summary>
        /// <param name="confirmationNo">confirmationNo</param>
        /// <param name="source">source</param>
        /// <returns>void</returns>
        public static bool IsConfirmationNoUnique(string confirmationNo)
        {
            bool status = false;
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@confNo", confirmationNo);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.IsConfirmationNoUnique, paramList, connection);
            data.Read();
            if (Convert.ToInt32(data["counter"]) > 0)
            {
                status = false;
            }
            else
            {
                status = true;
            }
            data.Close();
            connection.Close();
            return status;
        }

        /// <summary>
        /// gets hotelid from hotel itinerary table based on conformation number and source.
        /// </summary>
        /// <param name="confNo">confirmation number</param>
        /// <param name="source">source</param>
        /// <returns>hotel id</returns>
        public static int GetHotelId(string confNo, HotelBookingSource source)
        {
            int hotelId = 0;
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@confirmationNo", confNo);
            paramList[1] = new SqlParameter("@source", source);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetHotelIdFromSource, paramList, connection);
            if (data.Read())
            {
                hotelId = Convert.ToInt32(data["hotelId"]);
            }
            data.Close();
            connection.Close();
            return hotelId;
        }

        /// <summary>
        /// upadtes voucher status
        /// </summary>
        /// <param name="hotelId"></param>
        /// <param name="voucherStatus"></param>
        /// <param name="hotelTripId"></param>
        /// <returns>number of rows affected.</returns>
        public static int UpdateVoucherStatus(int hotelId, bool voucherStatus, int hotelTripId, HotelApiBookingStatus bookingStatus)
        {
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@status", voucherStatus);
            paramList[1] = new SqlParameter("@hotelId", hotelId);
            paramList[2] = new SqlParameter("@hotelTripId", hotelTripId);
            paramList[3] = new SqlParameter("@bookingStatus", (int)bookingStatus);

            int retVal = Dal.ExecuteNonQuerySP(SPNames.UpdateHotelVoucherStatus, paramList);
            return retVal;
        }
        #endregion
    }
}
