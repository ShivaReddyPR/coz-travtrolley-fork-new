using System;
using System.Collections.Generic;
using System.Text;
using Technology.BookingEngine;
using System.Xml;
using System.Data.SqlClient;
using Technology.Data;
using CoreLogic;
using CoreLogic;

namespace Technology.BookingEngine
{
    public class TrainTatkalRequest
    {
        private List<PassengerInfo> passengerInformation;
        private TatkalRequestStatus status;
        int requestId;
        private int agencyId;
        private int createdBy;               //created By
        private string trainNo;
        private int passengerCount;
        private short dayOfJourney;
        private short monthOfJourney;
        private int yearOfJourney;
        private string sourceStationCode;
        private string destinationCode;
        private string boardingPoint;
        private string reservationUpto;
        private string bookingClass;
        private string address;
        private long phoneNo;
        DateTime createdOn;
        int lastModifiedBy;
        DateTime lastModifiedOn;
        DateTime boardingDateAndTime;
        bool isWaitingAllowed;
        string agencyName;
        string remarks;

        #region properties
        public TatkalRequestStatus Status
        {
            get { return status; }
            set { status = value; }
        }
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }
        public int AgencyId
        {
            get{return agencyId;}
            set { agencyId = value; }
        }
        public string AgencyName
        {
            get { return agencyName; }
            set { agencyName = value; }
        }

        public bool IsWaitingAllowed
        {
            get { return isWaitingAllowed; }
            set { isWaitingAllowed = value; }
        }

        public int PassengerCount
        {
            get { return passengerCount; }
            set { passengerCount = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public List<PassengerInfo> PassengerInformation
        {
            get{return passengerInformation;}
            set { passengerInformation = value; }
        }

        public short DayOfJourney
        {
            get { return dayOfJourney; }
            set { dayOfJourney = value; }
        }
        public short MonthOfJourney
        {
            get { return monthOfJourney; }
            set { monthOfJourney = value; }
        }
        public int YearOfJourney
        {
            get { return yearOfJourney; }
            set { yearOfJourney = value; }
        }
        public string SourceStationCode
        {
            get { return sourceStationCode; }
            set { sourceStationCode = value; }
        }
        public string DestinationCode
        {
            get { return destinationCode; }
            set { destinationCode = value; }
        }
        public string TrainNo
        {
            get { return trainNo; }
            set {trainNo=value; }
        }
        public string BookingClass
        {
            get { return bookingClass; }
            set { bookingClass = value; }
        }
        public int RequestId
        {
            get { return requestId; }
            set { requestId = value; } 
        }
        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        public string BoardingPoint
        {
            get { return boardingPoint; }
            set { boardingPoint = value; }
        }
        public long PhoneNo
        {
            get { return phoneNo; }
            set { phoneNo = value; }
        }
        #endregion

       
        public TrainTatkalRequest()
        {
            passengerInformation = new List<PassengerInfo>();
        }
        public TrainTatkalRequest(List<PassengerInfo> passengerInformation,string bookingClass,string trainNumber,int agencyId,int createdBy,int psngrCount,int day,int month,int year,string source,string destination,string boardingPoint,string address,string phoneNo,bool isWaitingAllowed,string remarks,int modifiedBy)
        {
            this.passengerInformation = passengerInformation;
            this.agencyId = agencyId;
            passengerCount = psngrCount;
            trainNo = trainNumber;
            dayOfJourney = (short)day;
            monthOfJourney = (short)month;
            yearOfJourney = year;
            sourceStationCode = source;
            destinationCode = destination;
            this.boardingPoint = boardingPoint;
            reservationUpto = destination;
            this.bookingClass = bookingClass;
            this.address = address;
            this.phoneNo = Convert.ToInt64(phoneNo);
            status = TatkalRequestStatus.Requested;
            this.createdBy = createdBy;
            this.isWaitingAllowed = isWaitingAllowed;
            agencyName = new Agency(agencyId).AgencyName;
            this.remarks = remarks;
            this.lastModifiedBy = modifiedBy;
        }

        public void LoadByRequestId()
        {
            SqlDataReader datareader;
            using (SqlConnection connection = Dal.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[1];
                    paramList[0] = new SqlParameter("@requestId", requestId);
                    datareader = Dal.ExecuteReaderSP(SPNames.GetTatkalRequestByRequestId, paramList, connection);
                    if (datareader.HasRows)
                    {
                        datareader.Read();
                        address = Convert.ToString(datareader["address"]);
                        agencyId = Convert.ToInt32(datareader["agencyId"]);
                        agencyName = Convert.ToString(datareader["AgencyName"]);
                        boardingPoint = Convert.ToString(datareader["boardingPoint"]);
                        bookingClass = Convert.ToString(datareader["bookingClass"]);
                        dayOfJourney = Convert.ToInt16(datareader["dayOfJourney"]);
                        monthOfJourney = Convert.ToInt16(datareader["monthOfJourney"]);
                        yearOfJourney = Convert.ToInt32(datareader["yearOfJourney"]);
                        trainNo = Convert.ToString(datareader["trainNumber"]);
                        status = (TatkalRequestStatus)Enum.Parse(typeof(TatkalRequestStatus), Convert.ToString(datareader["status"]));
                        passengerCount = Convert.ToInt32(datareader["passengerCount"]);
                        destinationCode = Convert.ToString(datareader["destinationStation"]);
                        sourceStationCode = Convert.ToString(datareader["sourceStation"]);
                        phoneNo = Convert.ToInt64(datareader["phoneNo"]);
                        reservationUpto = Convert.ToString(datareader["reservationUpto"]);
                        createdBy = Convert.ToInt32(datareader["createdBy"]);
                        createdOn = Convert.ToDateTime(datareader["createdOn"]);
                        lastModifiedBy = Convert.ToInt32(datareader["lastModifiedBy"]);
                        lastModifiedOn = Convert.ToDateTime(datareader["lastModifiedOn"]);
                        requestId = Convert.ToInt32(datareader["requestId"]);
                        isWaitingAllowed = Convert.ToBoolean(datareader["isWaitingAllowed"]);
                        remarks = Convert.ToString(datareader["Remarks"]);
                        this.requestId = Convert.ToInt32(datareader["requestId"]);
                        XmlToPassengerInfo(Convert.ToString(datareader["passengerInformation"]));
                        datareader.Close();
                        datareader.Dispose();
                    }
                    else 
                    {
                        datareader.Close();
                        datareader.Dispose();
                        throw new Exception("Request not Found"); 
                    }
                }
                catch (DalException ex)
                {
                    Audit.Add(EventType.Exception, Severity.Low, 0, "Dal Exception in GetTatkalRequestByRequestId" + ex.StackTrace, "");
                    throw new Exception("");
                }
                
            }
        }        
        /// <summary>
        /// Saves the object in database
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            bool flag = true;
            try
            {
                SqlParameter[] paramlist = new SqlParameter[18];
                paramlist[0] = new SqlParameter("@sourceStation", sourceStationCode);
                paramlist[1] = new SqlParameter("@destinationStation", destinationCode);
                paramlist[2] = new SqlParameter("@trainNumber", trainNo);
                paramlist[3] = new SqlParameter("@bookingClass", bookingClass);
                paramlist[4] = new SqlParameter("@reservationUpto", reservationUpto);
                paramlist[5] = new SqlParameter("@boardingPoint", boardingPoint);
                paramlist[6] = new SqlParameter("@dayOfJourney", dayOfJourney);
                paramlist[7] = new SqlParameter("@monthOfJourney", monthOfJourney);
                paramlist[8] = new SqlParameter("@yearOfJourney", yearOfJourney);
                paramlist[9] = new SqlParameter("@passengerInformation", PassengerXmlGenerator());
                paramlist[10] = new SqlParameter("@passengerCount", passengerCount);
                paramlist[11] = new SqlParameter("@agencyId", agencyId);
                paramlist[12] = new SqlParameter("@status", status);
                paramlist[13] = new SqlParameter("@address", address);
                paramlist[14] = new SqlParameter("@phoneNo", phoneNo);
                paramlist[15] = new SqlParameter("@createdBy", createdBy);
                paramlist[16] = new SqlParameter("@isWaitingAllowed", isWaitingAllowed);
                paramlist[17] = new SqlParameter("@remarks", Remarks);
                Dal.ExecuteNonQuerySP(SPNames.SaveTatkalBookingRequest, paramlist);
            }
            catch (DalException ex)
            {
                Audit.Add(EventType.Exception, Severity.Low, 0, "Dal Exception in usp_SaveTatkalBookingRequest" + ex.StackTrace, "");
                flag = false;
            }
            return flag;
        }
        public bool Delete(int modifiedBy)
        {
            bool flag = true;
            try
            {
                SqlParameter[] paramlist = new SqlParameter[5];
                paramlist[0] = new SqlParameter("@requestId", requestId);
                paramlist[1] = new SqlParameter("@agencyId", agencyId);
                paramlist[2] = new SqlParameter("@remarks", remarks);
                paramlist[3] = new SqlParameter("@modifiedBy", modifiedBy);
                paramlist[4] = new SqlParameter("@status", Convert.ToInt32(TatkalRequestStatus.Cancelled));
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.DeleteTatkalRequest, paramlist);
                if (rowsAffected == 0)
                    flag = false;
            }
            catch (DalException ex)
            {
                Audit.Add(EventType.Exception, Severity.Low, 0, "Dal Exception in usp_DeleteTatkalRequest" + ex.StackTrace, "");
                flag = false;
            }
            return flag;
        }
        public bool UpdateRequest(int updatedBy)
        {
            bool flag = true;
            try
            {
                SqlParameter[] paramlist = new SqlParameter[19];
                paramlist[0] = new SqlParameter("@sourceStation", sourceStationCode);
                paramlist[1] = new SqlParameter("@destinationStation", destinationCode);
                paramlist[2] = new SqlParameter("@trainNumber", trainNo);
                paramlist[3] = new SqlParameter("@bookingClass", bookingClass);
                paramlist[4] = new SqlParameter("@reservationUpto", reservationUpto);
                paramlist[5] = new SqlParameter("@boardingPoint", boardingPoint);
                paramlist[6] = new SqlParameter("@dayOfJourney", dayOfJourney);
                paramlist[7] = new SqlParameter("@monthOfJourney", monthOfJourney);
                paramlist[8] = new SqlParameter("@yearOfJourney", yearOfJourney);
                paramlist[9] = new SqlParameter("@passengerInformation", PassengerXmlGenerator());
                paramlist[10] = new SqlParameter("@passengerCount", passengerCount);
                paramlist[11] = new SqlParameter("@agencyId", agencyId);
                paramlist[12] = new SqlParameter("@status", status);
                paramlist[13] = new SqlParameter("@address", address);
                paramlist[14] = new SqlParameter("@phoneNo", phoneNo);
                paramlist[15] = new SqlParameter("@modifiedBy", updatedBy);
                paramlist[16] = new SqlParameter("@isWaitingAllowed", isWaitingAllowed);
                paramlist[17] = new SqlParameter("@remarks", Remarks);
                paramlist[18] = new SqlParameter("@requestId", RequestId);
                int rowsAffected=Dal.ExecuteNonQuerySP(SPNames.UpdateTatkalRequest, paramlist);
                if (rowsAffected == 0)
                    flag = false;
            }
            catch (DalException ex)
            {
                Audit.Add(EventType.Exception, Severity.Low, 0, "Dal Exception in UpdateTatkalRequest" + ex.StackTrace, "");
                flag = false;
            }
            return flag;
        }        
        string PassengerXmlGenerator()
        {
            StringBuilder stBuilder = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(stBuilder);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            writer.WriteStartElement("Passengers");
            foreach(PassengerInfo passenger in passengerInformation)
            {
                writer.WriteStartElement("passenger");
                writer.WriteAttributeString("Name",passenger.name);
                writer.WriteAttributeString("Age",Convert.ToString(passenger.age));
                writer.WriteAttributeString("Gender",Convert.ToString(passenger.gender));
                writer.WriteAttributeString("Berth", Convert.ToString(passenger.seatPreference));
                writer.WriteAttributeString("Meal", Convert.ToString(passenger.mealPreference));
                writer.WriteAttributeString("PassengerType", Convert.ToString(passenger.passengerType));
                writer.WriteEndElement();                
            }
            writer.WriteEndElement();
            writer.Close();
            
            return stBuilder.ToString();
        }
        public void XmlToPassengerInfo(string passengerXml)
        {
            XmlTextReader obj = new XmlTextReader(passengerXml, XmlNodeType.Element, null);
                   
            while(obj.Read())
            {
                if (obj.Name=="passenger")
                {
                    PassengerInfo passenger = new PassengerInfo();
                    passenger.age = Convert.ToInt32(obj["Age"]);
                    passenger.name = Convert.ToString(obj["Name"]);
                    passenger.passengerType = (TrainPaxType)Enum.Parse(typeof(TrainPaxType), Convert.ToString(obj["PassengerType"]));
                    passenger.seatPreference = (TrainSeatPreference)Enum.Parse(typeof(TrainSeatPreference), Convert.ToString(obj["Berth"]));
                    passenger.gender = (TrainPassengerGender)Enum.Parse(typeof(TrainPassengerGender), Convert.ToString(obj["Gender"]));
                    passenger.mealPreference = (TrainMealPreference)Enum.Parse(typeof(TrainMealPreference), Convert.ToString(obj["Meal"]));
                    passengerInformation.Add(passenger);
                }
            }           
        }        
        public void UpdateRequestStatus(int modifiedBy,TatkalRequestStatus status,string remark)
        {
            int rowsAffected;
            try
            {
                SqlParameter[] paramlist = new SqlParameter[4];
                paramlist[0] = new SqlParameter("@requestId", requestId);
                paramlist[1] = new SqlParameter("@modifiedBy", modifiedBy);
                paramlist[2] = new SqlParameter("@status", status);
                paramlist[3] = new SqlParameter("@remark", remark);
                rowsAffected=Dal.ExecuteNonQuerySP(SPNames.UpdateTatkalRequestStatus, paramlist);
                if (rowsAffected == 0)
                {
                    throw new Exception("No Request with This Id");
                }
            }
            catch (DalException ex)
            {
                Audit.Add(EventType.Exception, Severity.Low, 0, "Dal Exception in UpdateTatkalRequestStatus" + ex.StackTrace, "");
                throw new Exception("Not Able To Update Request");
            }
        }                
        public static explicit operator TrainTatkalRequest(SqlDataReader datareader)
        {
            TrainTatkalRequest obj = new TrainTatkalRequest();
            obj.address = Convert.ToString(datareader["address"]);
            obj.agencyId = Convert.ToInt32(datareader["agencyId"]);
            obj.agencyName=Convert.ToString(datareader["AgencyName"]);
            obj.boardingPoint = Convert.ToString(datareader["boardingPoint"]);
            obj.bookingClass = Convert.ToString(datareader["bookingClass"]);
            obj.dayOfJourney = Convert.ToInt16(datareader["dayOfJourney"]);
            obj.monthOfJourney = Convert.ToInt16(datareader["monthOfJourney"]);
            obj.yearOfJourney = Convert.ToInt32(datareader["yearOfJourney"]);
            obj.trainNo = Convert.ToString(datareader["trainNumber"]);
            obj.status = (TatkalRequestStatus)Enum.Parse(typeof(TatkalRequestStatus),Convert.ToString(datareader["status"]));
            obj.passengerCount = Convert.ToInt32(datareader["passengerCount"]);
            obj.destinationCode = Convert.ToString(datareader["destinationStation"]);
            obj.sourceStationCode = Convert.ToString(datareader["sourceStation"]);
            obj.phoneNo = Convert.ToInt64(datareader["phoneNo"]);
            obj.reservationUpto= Convert.ToString(datareader["reservationUpto"]);
            obj.createdBy = Convert.ToInt32(datareader["createdBy"]);
            obj.createdOn = Convert.ToDateTime(datareader["createdOn"]);
            obj.lastModifiedBy = Convert.ToInt32(datareader["lastModifiedBy"]);
            obj.lastModifiedOn = Convert.ToDateTime(datareader["lastModifiedOn"]);
            obj.requestId = Convert.ToInt32(datareader["requestId"]);
            obj.isWaitingAllowed = Convert.ToBoolean(datareader["isWaitingAllowed"]);
            obj.remarks = Convert.ToString(datareader["Remarks"]);
            obj.XmlToPassengerInfo(Convert.ToString(datareader["passengerInformation"]));
            return obj;
        }
        public static explicit operator IRCTCBookRequest(TrainTatkalRequest tatkalRequest)
        {
            IRCTCBookRequest obj = new IRCTCBookRequest();
            obj.MobileNumber = Convert.ToString(tatkalRequest.phoneNo);
            obj.BoardingPoint = tatkalRequest.boardingPoint;
            obj.BookingClass = tatkalRequest.bookingClass;
            obj.DayOfJourney = tatkalRequest.dayOfJourney;
            obj.MonthOfJourney = tatkalRequest.monthOfJourney;
            obj.YearOfJourney = tatkalRequest.yearOfJourney;
            obj.TrainNumber = tatkalRequest.trainNo;
            obj.PassengerCount = tatkalRequest.passengerCount;
            
            obj.SourceStation = tatkalRequest.sourceStationCode;
            obj.ReservationUpto = tatkalRequest.reservationUpto;
            obj.DestinationStation = tatkalRequest.destinationCode;
            obj.Quota = TrainQuota.CK;
            for(int i=0;i<tatkalRequest.passengerInformation.Count;i++)
            {
                switch (i)
                {
                    case 0: 
                        obj.PassengerAge1 = Convert.ToString(tatkalRequest.passengerInformation[i].age);
                        obj.PassengerName1 = tatkalRequest.passengerInformation[i].name;
                        obj.PassengerConcessionCode1 = TrainConcessionCode.NOCONC;
                        obj.PassengerBerthPreference1 = tatkalRequest.passengerInformation[i].seatPreference;
                        obj.PassengerSex1 = tatkalRequest.passengerInformation[i].gender;
                        obj.PassengerFoodPreference1 = tatkalRequest.passengerInformation[i].mealPreference;
                        break;
                    case 1: obj.PassengerAge2 = Convert.ToString(tatkalRequest.passengerInformation[i].age);
                        obj.PassengerName2 = tatkalRequest.passengerInformation[i].name;
                        obj.PassengerConcessionCode2 = TrainConcessionCode.NOCONC;
                        obj.PassengerBerthPreference2 = tatkalRequest.passengerInformation[i].seatPreference;
                        obj.PassengerSex2 = tatkalRequest.passengerInformation[i].gender;
                        obj.PassengerFoodPreference2 = tatkalRequest.passengerInformation[i].mealPreference;
                        break;
                    case 2: obj.PassengerAge3 = Convert.ToString(tatkalRequest.passengerInformation[i].age);
                        obj.PassengerName3 = tatkalRequest.passengerInformation[i].name;
                        obj.PassengerConcessionCode3 = TrainConcessionCode.NOCONC;
                        obj.PassengerBerthPreference3 = tatkalRequest.passengerInformation[i].seatPreference;
                        obj.PassengerSex3 = tatkalRequest.passengerInformation[i].gender;
                        obj.PassengerFoodPreference3 = tatkalRequest.passengerInformation[i].mealPreference;
                        break;
                    case 3: obj.PassengerAge4 = Convert.ToString(tatkalRequest.passengerInformation[i].age);
                        obj.PassengerName4 = tatkalRequest.passengerInformation[i].name;
                        obj.PassengerConcessionCode4 = TrainConcessionCode.NOCONC;
                        obj.PassengerBerthPreference4 = tatkalRequest.passengerInformation[i].seatPreference;
                        obj.PassengerSex4 = tatkalRequest.passengerInformation[i].gender;
                        obj.PassengerFoodPreference4 = tatkalRequest.passengerInformation[i].mealPreference;
                        break;
                    case 4: obj.PassengerAge5 = Convert.ToString(tatkalRequest.passengerInformation[i].age);
                        obj.PassengerName5 = tatkalRequest.passengerInformation[i].name;
                        obj.PassengerConcessionCode5 = TrainConcessionCode.NOCONC;
                        obj.PassengerBerthPreference5 = tatkalRequest.passengerInformation[i].seatPreference;
                        obj.PassengerSex5 = tatkalRequest.passengerInformation[i].gender;
                        obj.PassengerFoodPreference5 = tatkalRequest.passengerInformation[i].mealPreference;
                        break;
                    case 5: obj.PassengerAge6 = Convert.ToString(tatkalRequest.passengerInformation[i].age);
                        obj.PassengerName6 = tatkalRequest.passengerInformation[i].name;
                        obj.PassengerConcessionCode6 = TrainConcessionCode.NOCONC;
                        obj.PassengerBerthPreference6 = tatkalRequest.passengerInformation[i].seatPreference;
                        obj.PassengerSex6 = tatkalRequest.passengerInformation[i].gender;
                        obj.PassengerFoodPreference6 = tatkalRequest.passengerInformation[i].mealPreference;
                        break;
                }
                
            }
            return obj;
        }
        public static int GetNoOfRequestsForAgency(string agencyId,params TatkalRequestStatus[] statusList)
        {
            int noOfRequests;
            SqlDataReader datareader;
            string statusCsv="";
            foreach (TatkalRequestStatus status in statusList)
            {
                statusCsv += Convert.ToString(Convert.ToInt32(status))+',';
            }
            using (SqlConnection connection = Dal.GetConnection())
            {
                try
                {
                   SqlParameter[] paramList = new SqlParameter[2];
                   paramList[0] = new SqlParameter("@agencyId", agencyId);
                   paramList[1] = new SqlParameter("@statusCsv", statusCsv);
                   datareader = Dal.ExecuteReaderSP(SPNames.GetNoOfTatkalRequestsForAgency, paramList, connection);
                   datareader.Read();
                   noOfRequests = Convert.ToInt32(datareader["noOfRequests"]);
                   datareader.Close();
                   datareader.Dispose();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.Low, 0, "Exception in GetNoOfTatkalRequestsForAgency "+ex.Message + ex.StackTrace, "");
                    throw new Exception("Not able to get No of Requests");
                }
                datareader.Dispose();
            }
            return noOfRequests;
        }
        public static int GetNoOfRequestsForAdmin(string agencyId,params TatkalRequestStatus[] statusList)
        {
            int noOfRequests;
            SqlDataReader datareader;
            string statusCsv="";
            foreach (TatkalRequestStatus status in statusList)
            {
                statusCsv += Convert.ToString(Convert.ToInt32(status))+',';
            }
            using (SqlConnection connection = Dal.GetConnection())
            {
                try
                {
                    if (agencyId == "0")
                    {
                        SqlParameter[] paramList = new SqlParameter[1];
                        paramList[0] = new SqlParameter("@statusCsv", statusCsv);
                        datareader = Dal.ExecuteReaderSP(SPNames.GetNoOfTatkalRequestsForAdmin, paramList, connection);
                        datareader.Read();
                        noOfRequests = Convert.ToInt32(datareader["noOfRequests"]);                        
                        datareader.Close();
                        datareader.Dispose();
                    }
                    else
                    {
                        noOfRequests = GetNoOfRequestsForAgency(agencyId, statusList);
                    }                    
                }
                catch (DalException ex)
                {
                    Audit.Add(EventType.Exception, Severity.Low, 0, "Dal Exception in GetNoOfTatkalRequestsForAdmin" + ex.StackTrace, "");
                    throw new Exception("");
                }                
            }
            return noOfRequests;
        }
        public static List<TrainTatkalRequest> GetTatkalRequestsForAgency(string agencyId, int fromRecordNo, int noOfRecords, params TatkalRequestStatus[] statusList)
        {
            List<TrainTatkalRequest> requestList = new List<TrainTatkalRequest>();
            SqlDataReader datareader;
            string statusCsv ="";
            foreach (TatkalRequestStatus status in statusList)
            {
                statusCsv += Convert.ToString(Convert.ToInt32(status)) + ',';
            }
            using (SqlConnection connection = Dal.GetConnection())
            {
                try
                {
                    SqlParameter[] paramList = new SqlParameter[4];
                    paramList[0] = new SqlParameter("@agencyId", agencyId);
                    paramList[1] = new SqlParameter("@statusCsv", statusCsv);
                    paramList[2] = new SqlParameter("@fromRecordNo", fromRecordNo);
                    paramList[3] = new SqlParameter("@noOfRecords", noOfRecords);
                    datareader = Dal.ExecuteReaderSP(SPNames.GetTatkalRequestsForAgency, paramList, connection);
                    while (datareader.Read())
                    {
                        TrainTatkalRequest request = new TrainTatkalRequest();
                        request = (TrainTatkalRequest)datareader;
                        requestList.Add(request);
                    }
                    datareader.Close();
                    datareader.Dispose();                    
                }
                catch (DalException ex)
                {
                    Audit.Add(EventType.Exception, Severity.Low, 0, "Dal Exception in GetTatkalRequestsForAgency" + ex.StackTrace, "");
                    throw new Exception("");
                }
                datareader.Dispose();
            }

            return requestList;
        }
        public static List<TrainTatkalRequest> GetTatkalRequestsForAdmin(string agencyId, int fromRecordNo, int noOfRecords, params TatkalRequestStatus[] statusList)
        {
            List<TrainTatkalRequest> requestList = new List<TrainTatkalRequest>();
            SqlDataReader datareader;
            string statusCsv = "";
            foreach (TatkalRequestStatus status in statusList)
            {
                statusCsv += Convert.ToString(Convert.ToInt32(status)) + ',';
            }
            using (SqlConnection connection = Dal.GetConnection())
            {
                try
                {
                    if (agencyId == "0")
                    {
                        SqlParameter[] paramList = new SqlParameter[3];
                        paramList[0] = new SqlParameter("@statusCsv", statusCsv);
                        paramList[1] = new SqlParameter("@fromRecordNo", fromRecordNo);
                        paramList[2] = new SqlParameter("@noOfRecords", noOfRecords);
                        datareader = Dal.ExecuteReaderSP(SPNames.GetTatkalRequestsForAdmin, paramList, connection);
                        while (datareader.Read())
                        {
                            TrainTatkalRequest request = new TrainTatkalRequest();
                            request = (TrainTatkalRequest)datareader;
                            requestList.Add(request);
                        }
                        datareader.Close();
                        datareader.Dispose();
                    }
                    else
                    {
                        requestList = GetTatkalRequestsForAgency(agencyId, fromRecordNo, noOfRecords, statusList);
                    }
                }
                catch (DalException ex)
                {
                    Audit.Add(EventType.Exception, Severity.Low, 0, "Dal Exception in GetTatkalRequestsForAdmin" + ex.StackTrace, "");
                    throw new Exception("");
                }                
            }
            return requestList;
        }
        public static List<int> GetTatkalRequestsIDForDateInterval(string agencyId, DateTime fromDate,DateTime toDate, params TatkalRequestStatus[] statusList)
        {
            List<int> requestsIDList = new List<int>();
            SqlDataReader datareader;
            string statusCsv = "";
            foreach (TatkalRequestStatus status in statusList)
            {
                statusCsv += Convert.ToString(Convert.ToInt32(status)) + ',';
            }
            using (SqlConnection connection = Dal.GetConnection())
            {
                try
                {
                    if (agencyId == "0")
                    {
                        SqlParameter[] paramlist = new SqlParameter[3];
                        paramlist[0] = new SqlParameter("@fromDate", fromDate);
                        paramlist[1] = new SqlParameter("@statusCsv", statusCsv);
                        paramlist[2] = new SqlParameter("@toDate", toDate);
                        datareader = Dal.ExecuteReaderSP(SPNames.GetTatkalRequestsIDForDateInterval, paramlist, connection);
                        while (datareader.Read())
                        {
                            int requestID = Convert.ToInt32(datareader["requestID"]);
                            requestsIDList.Add(requestID);
                        }                        
                        datareader.Close();
                        datareader.Dispose();
                    }                    
                }
                catch (DalException ex)
                {
                    Audit.Add(EventType.Exception, Severity.Low, 0, "Dal Exception in GetTatkalRequestsIDForDateInterval" + ex.StackTrace, "");
                    throw new Exception("GetTatkalRequestsByDate failed due to Da Exception");
                }            
            }

            return requestsIDList;
        }
   }
}
