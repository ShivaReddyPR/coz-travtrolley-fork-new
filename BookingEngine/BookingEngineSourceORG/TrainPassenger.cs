using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Technology.Data;
using System.Data.SqlClient;
using System.Data;
using Technology.BookingEngine;

namespace Technology.BookingEngine
{
    public class TrainPassenger
    {

        private int paxId;
        private int bookingId;
        private string paxName;
        private short age;
        private TrainPassengerGender gender;
        private string ticketDescription;
        private int priceId;
        private TrainMealPreference mealPreference;
        private TrainSeatPreference seatPreference;
        private TrainPaxType paxType;
        private TrainPassengerStatus passengerStatus ;
        private DateTime createdOn;
        private int createdBy;
        private DateTime lastModifiedOn;
        private int lastModifiedBy;
        private bool hasBedRoll;

        /// <summary>
        /// unique id for each passenger
        /// </summary>
        public int PaxId
        {
            get
            {
                return paxId;

            }
            set
            {
                paxId = value;

            }
        }
        /// <summary>
        /// Booking id to which this passenger belongs
        /// </summary>
        public int BookingId
        {
            get
            {
                return bookingId;

            }
            set
            {
                bookingId = value;

            }
        }
        /// <summary>
        /// Full name of the passenger
        /// </summary>
        public string PaxName
        {
            get
            {
                return paxName;

            }
            set
            {
                paxName = value;

            }
        }
        /// <summary>
        /// Age of the passenger
        /// </summary>
        public short Age
        {
            get
            {
                return age;

            }
            set
            {
                age = value;

            }
        }
        /// <summary>
        /// Gender of the passenger
        /// </summary>
        public TrainPassengerGender Gender
        {
            get
            {
                return gender;

            }
            set
            {
                gender = value;

            }
        }
        /// <summary>
        /// Status/Coach No/Seat No
        /// </summary>
        public string TicketDescription
        {
            get
            {
                return ticketDescription;

            }
            set
            {
                ticketDescription = value;

            }
        }
        /// <summary>
        /// Price id of this ticket
        /// </summary>
        public int PriceId
        {
            get
            {
                return priceId;

            }
            set
            {
                priceId = value;

            }
        }
        /// <summary>
        /// Meal preference
        /// </summary>
        public TrainMealPreference MealPreference
        {
            get
            {
                return mealPreference;

            }
            set
            {
                mealPreference = value;

            }
        }
        /// <summary>
        /// Train seat preference
        /// </summary>
        public TrainSeatPreference SeatPreference
        {
            get
            {
                return seatPreference;
            }
            set
            {
                seatPreference = value;
            }
        }
        /// <summary>
        /// Pax type of the passenger
        /// </summary>
        public TrainPaxType PaxType
        {
            get
            {
                return paxType;

            }
            set
            {
                paxType = value;

            }
        }
        public TrainPassengerStatus PassengerStatus
        {
            get
            {
                return passengerStatus;
            }
            set
            {
                passengerStatus = value;
            }
        }   
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;

            }
            set
            {
                createdOn = value;

            }
        }
        public int CreatedBy
        {
            get
            {
                return createdBy;

            }
            set
            {
                createdBy = value;

            }
        }
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;

            }
            set
            {
                lastModifiedOn = value;

            }
        }
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;

            }
            set
            {
                lastModifiedBy = value;

            }
        }
        public bool HasBedRoll
        {
            get
            {
                return hasBedRoll;

            }
            set
            {
                hasBedRoll = value;

            }
        }
        public int Save()
        {
            Trace.TraceInformation("TrainItinerary.save entered");
            ValidateData();
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[14];
            paramList[0] = new SqlParameter("@paxId",paxId);
            paramList[1] = new SqlParameter("@bookingId", bookingId);
            paramList[2] = new SqlParameter("@paxName", paxName);
            paramList[3] = new SqlParameter("@age", age);
            paramList[4] = new SqlParameter("@gender", (int)gender);
            paramList[5] = new SqlParameter("@ticketDescription", ticketDescription);
            paramList[6] = new SqlParameter("@priceId", priceId);
            paramList[7] = new SqlParameter("@mealPreference", (int)mealPreference);
            paramList[8] = new SqlParameter("@seatPreference", (int)seatPreference);
            paramList[9] = new SqlParameter("@paxType", (int)paxType );
            paramList[10] = new SqlParameter("@passengerStatus", (int)passengerStatus);
            paramList[11] = new SqlParameter("@createdBy", createdBy);
            paramList[12] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            paramList[13] = new SqlParameter("@hasBedRoll", hasBedRoll);
            if (paxId > 0)
            {
                rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateTrainPassenger, paramList);
            }
            else
            {
                paramList[0].Direction = ParameterDirection.Output;
                rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddTrainPassenger, paramList);
                paxId = Convert.ToInt32(paramList[0].Value);
            }
            Trace.TraceInformation("TrainItinerary.save exited");
            return rowsAffected;
        }
        /// <summary>
        /// This function validates all the inputs that are to be stored
        /// </summary>
        private void ValidateData()
        {
            if (bookingId  == 0)
            {
                throw new ArgumentException("bookingId cannot be zero");
            }
            if (paxName == null || paxName.Trim().Length == 0)
            {
                throw new ArgumentException("Pax Name cannot be null or empty");
            }
            if (age == 0 && paxType != TrainPaxType.Infant)
            {
                throw new ArgumentException("Age cannot be zero");
            }
            if (gender == null )
            {
                throw new ArgumentException("Gender need to be provided");
            }
            if (ticketDescription == null )
            {
                throw new ArgumentException("Ticket Description cannot be null or empty");
            }
            if (priceId == 0 && paxType != TrainPaxType.Infant)
            {
                throw new ArgumentException("Price Id cannot be zero");
            }
            if ((int)mealPreference == 0 && paxType != TrainPaxType.Infant)
            {
                throw new ArgumentException("Meal Preference need to be provided");
            }
            if ((int)seatPreference == 0 && paxType != TrainPaxType.Infant)
            {
                throw new ArgumentException("Seat Preference need to be provided");
            }
            if ((int)paxType == 0)
            {
                throw new ArgumentException("Pax Type need to be provided");
            }
            if (createdBy == 0)
            {
                throw new ArgumentException("Created By cannot be zero");
            }
        }
        public void Load(int passengerId)
        {
            Trace.TraceInformation("TrainPassenger.Load entered paxId = " + passengerId);
            if (passengerId == 0)
            {
                throw new ArgumentException("Train Passenger Id cannot be zero");
            }
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxId", passengerId);
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlDataReader reader = Dal.ExecuteReaderSP(SPNames.GetTrainPassenger, paramList, connection);
                if (reader.Read())
                {
                    ReadData(this, reader);
                }
                reader.Close();
            }
            Trace.TraceInformation("TrainPassenger.Load exited");
        }
        public static List<TrainPassenger> GetPassengerListByBookingId(int bookingId,ref List<TrainPassenger> infantList)
        {
            Trace.TraceInformation("TrainPassenger.GetPassengerListByBookingId entered booking id = " + bookingId);
            if (bookingId == 0)
            {
                throw new ArgumentException("Train Booking Id cannot be zero");
            }
            List<TrainPassenger> passengerList = new List<TrainPassenger>();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@bookingId", bookingId);
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlDataReader reader = Dal.ExecuteReaderSP(SPNames.GetTrainPassengerByBookingId, paramList, connection);
                while (reader.Read())
                {
                    TrainPassenger passenger = new TrainPassenger();
                    ReadData(passenger, reader);
                    if (passenger.paxType == TrainPaxType.Infant)
                    {
                        infantList.Add(passenger);
                    }
                    else
                    {
                        passengerList.Add(passenger);
                    }                    
                }
                reader.Close();
            }
            Trace.TraceInformation("TrainPassenger.GetPassengerListByBookingId exited");
            return passengerList;
        }
        private static void ReadData(TrainPassenger passenger, SqlDataReader reader)
        {
            Trace.TraceInformation("TrainPassenger.ReadData entered");
            passenger.paxId = Convert.ToInt32(reader["paxId"]);
            passenger.bookingId = Convert.ToInt32(reader["bookingId"]);
            passenger.paxName = Convert.ToString(reader["paxName"]);
            passenger.age = Convert.ToInt16(reader["age"]);
            passenger.gender = (TrainPassengerGender)(Convert.ToInt32(reader["gender"]));
            passenger.ticketDescription = Convert.ToString(reader["ticketDescription"]);
            passenger.priceId = Convert.ToInt32(reader["priceId"]);
            passenger.mealPreference = (TrainMealPreference)(Convert.ToInt32(reader["mealPreference"]));
            passenger.seatPreference = (TrainSeatPreference)Convert.ToInt32(reader["seatPreference"]);
            passenger.paxType = (TrainPaxType)(Convert.ToInt32(reader["paxType"]));
            passenger.passengerStatus = (TrainPassengerStatus)Convert.ToInt32(reader["passengerStatus"]);
            passenger.hasBedRoll =  Convert.ToBoolean(reader["hasBedRoll"]);
            passenger.createdOn = Convert.ToDateTime(reader["createdOn"]);
            passenger.createdBy = Convert.ToInt32(reader["createdBy"]);
            passenger.lastModifiedOn = Convert.ToDateTime(reader["lastModifiedOn"]);
            passenger.lastModifiedBy = Convert.ToInt32(reader["lastModifiedBy"]);
            Trace.TraceInformation("TrainPassenger.ReadData exited");
        }
    }
}
