using System;
using System.Collections.Generic;
using System.Text;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Diagnostics;

namespace Technology.BookingEngine
{
    [Serializable]
    public class TransferVehicle
    {
        private string vehicle;
        private int transferId;
        private string vehicleCode;
        private int vehicleMaximumPassengers;
        private int vehicleMaximumLuggage;
        private PriceAccounts itemPrice;
        private string currency;
        private int occPax;

        public string Vehicle
        {
            get { return vehicle; }
            set { vehicle = value; }
        }
        public int TransferId
        {
            get { return transferId; }
            set { transferId = value; }

        }
        public string VehicleCode
        {
            get
            {
                return vehicleCode;
            }
            set { vehicleCode = value; }
        }
        public int VehicleMaximumPassengers
        {
            get { return vehicleMaximumPassengers; }
            set { vehicleMaximumPassengers = value; }
        }

        public int VehicleMaximumLuggage
        {
            get { return vehicleMaximumLuggage; }
            set { vehicleMaximumLuggage = value; }
        }
        public PriceAccounts ItemPrice
        {
            get { return itemPrice; }
            set { itemPrice = value; }
        }
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        public int OccupiedPax
        {
            get { return occPax; }
            set { occPax = value; }
        }
        /// <summary>
        /// This Method is used to save the Transfer Vehicle Details
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("TransferVehicle.Save entered.");
            SqlParameter[] paramList = new SqlParameter[8];
            paramList[0] = new SqlParameter("@transferId", transferId);
            paramList[1] = new SqlParameter("@vehicleCode", vehicleCode);
            paramList[2] = new SqlParameter("@vehicleName", vehicle);
            paramList[3] = new SqlParameter("@maxPax", vehicleMaximumPassengers);
            paramList[4] = new SqlParameter("@maxLauggage", vehicleMaximumLuggage);
            paramList[5] = new SqlParameter("@occPax", occPax);
            paramList[6] = new SqlParameter("@currency", currency);
            paramList[7] = new SqlParameter("@priceId", itemPrice.PriceId);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddTransferVehicle, paramList);
            Trace.TraceInformation("TransferVehicle.Save exiting");    
        }

        /// <summary>
        /// This Method is used to Load the Vehicle Details based on transferID.
        /// </summary>
        /// <param name="transferId"></param>
        public List<TransferVehicle> Load(int trId)
        {
            Trace.TraceInformation("TransferVehicle.Load entered.");
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@transferId", trId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetVehicleByTransferId, paramList, connection);
            List<TransferVehicle> vehList = new List<TransferVehicle>();
             PriceAccounts pAcc = new PriceAccounts();
            while (data.Read())
            {
                TransferVehicle vehInfo = new TransferVehicle();
                vehInfo.TransferId = trId;
                vehInfo.VehicleCode = data["vehicleCode"].ToString();
                vehInfo.Vehicle = data["vehicleName"].ToString();
                vehInfo.VehicleMaximumPassengers = Convert.ToInt32(data["maxPax"]);
                vehInfo.VehicleMaximumLuggage = Convert.ToInt32(data["maxLauggage"]);
                vehInfo.OccupiedPax = Convert.ToInt32(data["occPax"]);
                vehInfo.Currency = data["currency"].ToString();
                pAcc.Load(Convert.ToInt32(data["priceId"]));
                vehInfo.ItemPrice = pAcc;
                vehList.Add(vehInfo);
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("TransferVehicle.Load exiting");
            return vehList;
        }


    }

}
