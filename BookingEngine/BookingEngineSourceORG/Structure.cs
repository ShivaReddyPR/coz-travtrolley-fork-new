using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;

namespace Technology.BookingEngine
{
    [Serializable]
    public struct Meal
    {
        private string code;
        private string description;

        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public static Meal GetMeal(string code)
        {
            Trace.TraceInformation("Meal.GetMeal entered : mealCode = " + code);
            Meal meal = new Meal();
            meal.code = code;
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@mealCode", meal.code);
            SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetMeal, paramList, connection);
            if (dataReader.Read())
            {
                meal.description = (string)dataReader["mealDescription"];
            }
            else
            {
                dataReader.Close();
                connection.Close();
                Trace.TraceError("Meal.GetMeal exiting : Meal Code " + code + " does not exist in database");
                throw new BookingEngineException("Meal Code " + code + " does not exist in database");
            }
            dataReader.Close();
            connection.Close();
            return meal;
        }
    }
    [Serializable]
    public struct Seat
    {
        private string code;
        private string description;

        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public static Seat GetSeat(string code)
        {
            Trace.TraceInformation("Seat.GetSeat entered : seatCode = " + code);
            Seat seat = new Seat();
            seat.code = code;
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@seatCode", seat.code);
            SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetSeat, paramList, connection);
            if (dataReader.Read())
            {
                seat.description = (string)dataReader["seatDescription"];
            }
            else
            {
                dataReader.Close();
                connection.Close();
                Trace.TraceError("Seat.GetSeat exiting : Seat Code " + code + " does not exist in database");
                throw new BookingEngineException("Seat Code " + code + " does not exist in database");
            }
            dataReader.Close();
            connection.Close();
            return seat;
        }
    }

    public class SegmentPTCDetail
    {
        private int segmentId;
        public int SegmentId
        {
            get { return segmentId; }
            set { segmentId = value; }
        }

        private string paxType;
        public string PaxType
        {
            get { return paxType; }
            set { paxType = value; }
        }

        private string fareBasis;
        public string FareBasis
        {
            get { return fareBasis; }
            set { fareBasis = value; }
        }

        private string nva;
        public string NVA
        {
            get { return nva; }
            set { nva = value; }
        }

        private string nvb;
        public string NVB
        {
            get { return nvb; }
            set { nvb = value; }
        }

        private string baggage;
        public string Baggage
        {
            get { return baggage; }
            set { baggage = value; }
        }

        // Airline code + flight number + date + time  eg. BA010227MAR20070710 for BA 0102 27MAR2007 0710 
        private string flightKey;
        // Gets or sets flight key. (Airline code + flight number + date + time)  eg. BA010227MAR20070710 for BA 0102 27MAR2007 0710 
        public string FlightKey
        {
            get { return flightKey; }
            set { flightKey = value; }
        }


        public void Save()
        {
            Trace.TraceInformation("SegmentPTCDetail.SaveSegmentDetail entered.");
            SqlParameter[] paramList = new SqlParameter[6];
            paramList[0] = new SqlParameter("@segmentId", segmentId);
            paramList[1] = new SqlParameter("@paxType", paxType);
            paramList[2] = new SqlParameter("@fareBasis", fareBasis);
            paramList[3] = new SqlParameter("@baggage", baggage);
            paramList[4] = new SqlParameter("@nva", nva);
            paramList[5] = new SqlParameter("@nvb", nvb);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddSegmentPTCDetail, paramList);
            Trace.TraceInformation("SegmentPTCDetail.SaveSegmentDetail exiting : rowsAffected = " + rowsAffected);
        }

        /// <summary>
        /// Gets the segmentptcdetail for a particular segment
        /// </summary>
        /// <param name="segmentId">segmentId</param>
        /// <returns>SegmentPTCDetail[] contains info abt PTC detail</returns>
        public static List<SegmentPTCDetail> GetSegmentPTCDetail(int flightId)
        {
            Trace.TraceInformation("FlightInfo.GetSegmentPTCDetail entered : flightId = " + flightId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            DataSet dataSet = Dal.FillSP(SPNames.GetSegmentPTCDetail, paramList);
            List<SegmentPTCDetail> segmentPTCDetail = new List<SegmentPTCDetail>();
            //int count = 0;
            foreach (DataRow dr in dataSet.Tables[0].Rows)
            {
                SegmentPTCDetail ptcDetail = new SegmentPTCDetail();
                ptcDetail.SegmentId = (int)dr["segmentId"];
                ptcDetail.PaxType = dr["paxType"].ToString();
                if (dr["FareBasis"] != DBNull.Value)
                {
                    ptcDetail.fareBasis = Convert.ToString(dr["FareBasis"]);
                }
                else
                {
                    ptcDetail.fareBasis = string.Empty;
                }
                if (dr["NVA"] != DBNull.Value)
                {
                    ptcDetail.nva = Convert.ToString(dr["NVA"]);
                }
                else
                {
                    ptcDetail.nva = string.Empty;
                }
                if (dr["NVB"] != DBNull.Value)
                {
                    ptcDetail.nvb = Convert.ToString(dr["NVB"]);
                }
                else
                {
                    ptcDetail.nvb = string.Empty;
                }
                if (dr["Baggage"] != DBNull.Value)
                {
                    ptcDetail.baggage = Convert.ToString(dr["Baggage"]);
                }
                else
                {
                    ptcDetail.baggage = string.Empty;
                }
                segmentPTCDetail.Add(ptcDetail);
            }
            return segmentPTCDetail;
        }
        public static List<SegmentPTCDetail> GetSegmentPTCDetail(int flightId, string paxType)
        {
            Trace.TraceInformation("FlightInfo.GetSegmentPTCDetail entered : flightId = " + flightId);
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@flightId", flightId);
            paramList[1] = new SqlParameter("@paxType", paxType);
            DataSet dataSet = Dal.FillSP(SPNames.GetSegmentPTCDetail, paramList);
            List<SegmentPTCDetail> segmentPTCDetail = new List<SegmentPTCDetail>();
            //int count = 0;
            foreach (DataRow dr in dataSet.Tables[0].Rows)
            {
                SegmentPTCDetail ptcDetail = new SegmentPTCDetail();
                ptcDetail.SegmentId = (int)dr["segmentId"];
                ptcDetail.PaxType = dr["paxType"].ToString();
                if (dr["FareBasis"] != DBNull.Value)
                {
                    ptcDetail.fareBasis = Convert.ToString(dr["FareBasis"]);
                }
                else
                {
                    ptcDetail.fareBasis = string.Empty;
                }
                if (dr["NVA"] != DBNull.Value)
                {
                    ptcDetail.nva = Convert.ToString(dr["NVA"]);
                }
                else
                {
                    ptcDetail.nva = string.Empty;
                }
                if (dr["NVB"] != DBNull.Value)
                {
                    ptcDetail.nvb = Convert.ToString(dr["NVB"]);
                }
                else
                {
                    ptcDetail.nvb = string.Empty;
                }
                if (dr["Baggage"] != DBNull.Value)
                {
                    ptcDetail.baggage = Convert.ToString(dr["Baggage"]);
                }
                else
                {
                    ptcDetail.baggage = string.Empty;
                }
                segmentPTCDetail.Add(ptcDetail);
            }
            return segmentPTCDetail;
        }
    }
    // Over Loading  for GetSegmentPTCDetail
    

    public struct FareRuleOND
    {
        private Airport origin;
        private Airport destination;
        private string airline;

        public Airport Origin
        {
            get { return origin; }
            set { origin = value; }
        }

        public Airport Destination
        {
            get { return destination; }
            set { destination = value; }
        }

        public string Airline
        {
            get { return airline; }
            set { airline = value; }
        }

    }
    public struct FareRuleOLD
    {

        private string fareBasis;
        private string fareRule;
        private FareRuleOND[] ond;

        public string FareBasis
        {
            get { return fareBasis; }
            set { fareBasis = value; }
        }

        public string Rule
        {
            get { return fareRule; }
            set { fareRule = value; }
        }

        public FareRuleOND[] OND
        {
            get { return ond; }
            set { ond = value; }
        }
    }

    public enum BookingResponseStatus
    {
        Successful = 1,
        Failed = 2,
        OtherFare = 3,
        OtherClass = 4,
        BookedOther = 5,
        ScheduleChanged = 6,// uapi schedule changes
    }

    public struct BookingResponse
    {
        private string pnr;
        private BookingResponseStatus status;
        private string error;
        private bool ssrDenied;
        private string ssrMessage;
        private ProductType prodType;
        private string confirmationNo;
        private int bookingId;

        public string PNR
        {
            get
            {
                return pnr;
            }
            set
            {
                pnr = value;
            }
        }

        public BookingResponseStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public string Error
        {
            get
            {
                return error;
            }
            set
            {
                error = value;
            }
        }

        public bool SSRDenied
        {
            get
            {
                return ssrDenied;
            }
            set
            {
                ssrDenied = value;
            }
        }

        public string SSRMessage
        {
            get
            {
                return ssrMessage;
            }
            set
            {
                ssrMessage = value;
            }
        }

        public ProductType ProdType
        {
            get
            {
                return prodType;
            }
            set
            {
                prodType = value;
            }
        }

        public string ConfirmationNo
        {
            get
            {
                return confirmationNo;
            }
            set
            {
                confirmationNo = value;
            }
        }

        public int BookingId
        {
            get
            {
                return bookingId;
            }
            set
            {
                bookingId = value;
            }
        }

        public BookingResponse(BookingResponseStatus status, string error, string pnr)
        {
            this.status = status;
            this.error = error;
            this.pnr = pnr;
            this.prodType = ProductType.Flight;
            this.confirmationNo = string.Empty;
            this.ssrDenied = false;
            this.ssrMessage = string.Empty;
            this.bookingId = 0;
        }

        public BookingResponse(BookingResponseStatus status, string error, string pnr, bool ssrDenied, string ssrMessage)
        {
            this.status = status;
            this.error = error;
            this.pnr = pnr;
            this.prodType = ProductType.Flight;
            this.confirmationNo = string.Empty;
            this.ssrDenied = ssrDenied;
            this.ssrMessage = ssrMessage;
            this.bookingId = 0;
        }

        public BookingResponse(BookingResponseStatus status, string error, string pnr, ProductType productType, string confirmationNo)
        {
            this.status = status;
            this.error = error;
            this.pnr = pnr;
            this.prodType = productType;
            this.confirmationNo = confirmationNo;
            this.ssrDenied = false;
            this.ssrMessage = string.Empty;
            this.bookingId = 0;
        }

    }

    public enum TicketingResponseStatus
    {
        Successful = 1,
        NotSaved = 2,
        NotCreated = 3,
        NotAllowed = 4,
        InProgress = 5,
        TicketedDB = 6,
        TicketedWS = 7,
        PriceChanged = 8,
        OtherError = 9
    }
    public struct TicketingResponse
    {
        private string pnr;
        public string PNR
        {
            get { return pnr; }
            set { pnr = value; }
        }

        private TicketingResponseStatus status;
        public TicketingResponseStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { message = value; }
        }
    }

    /// <summary>
    /// Booking Mode is How booking is made in the system
    /// </summary>
    public enum BookingMode
    {
        Auto = 1,
        Manual = 2,
        Import = 3,
        WhiteLabel = 4,
        BookingAPI = 5,
        Itimes = 6,
        ManualImport=7
    }
    /// <summary>
    /// Struct for Cancellation Charge for Ticket Refund
    /// </summary>
    public struct CancellationCharges
    {
        /// <summary>
        /// PaymentDetailId field(When ticket is refunded then the paymentDetailId regarding that refund)
        /// </summary>
        private int paymentDetailId;
        /// <summary>
        /// Supplier fee(like airline charges from us)
        /// </summary>
        private decimal supplierFee;
        /// <summary>
        /// admin fee(what we charge for cancellation)
        /// </summary>
        private decimal adminFee;
        /// <summary>
        /// referenceId field (it is ticket id if productType is Flight)
        /// </summary>
        private int referenceId;
        /// <summary>
        /// productType type(Flight in case of Ticket refund)
        /// </summary>
        private ProductType productType;
        /// <summary>
        /// itemTypeId represents whether booking is online or offline 
        /// </summary>
        private InvoiceItemTypeId itemTypeId;



        public int PaymentDetailId
        {
            get
            {
                return paymentDetailId;
            }
            set
            {
                this.paymentDetailId = value;
            }
        }
        /// <summary>
        /// Gets or Sets the field supplierFee
        /// </summary>
        public decimal SupplierFee
        {
            get
            {
                return supplierFee;
            }
            set
            {
                this.supplierFee = value;
            }
        }
        /// <summary>
        /// Gets or Sets the field adminFee
        /// </summary>
        public decimal AdminFee
        {
            get
            {
                return this.adminFee;
            }
            set
            {
                this.adminFee = value;
            }
        }
        /// <summary>
        /// Gets or Sets referenceId field
        /// </summary>
        public int ReferenceId
        {
            get
            {
                return this.referenceId;
            }
            set
            {
                this.referenceId = value;
            }
        }
        /// <summary>
        /// Gets or Sets ProductType field
        /// </summary>
        public ProductType ProductType
        {
            get
            {
                return this.productType;
            }
            set
            {
                this.productType = value;
            }
        }

        /// <summary>
        /// Gets or sets the itemTypeId
        /// </summary>
        public InvoiceItemTypeId ItemTypeId
        {
            get
            {
                return this.itemTypeId;
            }
            set
            {
                this.itemTypeId = value;
            }
        }
        /// <summary>
        /// Save the entry in Cancellation Charge table
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("CancellationCharges.Save entered.");
            SqlParameter[] paramList = new SqlParameter[6];
            paramList[0] = new SqlParameter("@paymentDetailId", paymentDetailId);
            paramList[1] = new SqlParameter("@supplierFee", supplierFee);
            paramList[2] = new SqlParameter("@adminFee", adminFee);
            paramList[3] = new SqlParameter("@referenceId", referenceId);
            paramList[4] = new SqlParameter("@productTypeId", (int)productType);
            paramList[5] = new SqlParameter("@itemTypeId", (Int16)itemTypeId);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddCancellationCharge, paramList);
            Trace.TraceInformation("CancellationCharges.Save exiting : rowsAffected = " + rowsAffected);
        }

        /// <summary>
        /// Update the entry of Cancellation Charge table
        /// </summary>
        public void Update()
        {
            Trace.TraceInformation("CancellationCharges.Update entered.");
            try
            {
                SqlParameter[] paramList = new SqlParameter[6];
                paramList[0] = new SqlParameter("@paymentDetailId", paymentDetailId);
                paramList[1] = new SqlParameter("@supplierFee", supplierFee);
                paramList[2] = new SqlParameter("@adminFee", adminFee);
                paramList[3] = new SqlParameter("@referenceId", referenceId);
                paramList[4] = new SqlParameter("@productTypeId", (int)productType);
                paramList[5] = new SqlParameter("@itemTypeId", (Int16)itemTypeId);
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateCancellationCharge, paramList);
                Trace.TraceInformation("CancellationCharges.Update exiting : rowsAffected = " + rowsAffected);
            }
            catch (SqlException ex)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.Account, CoreLogic.Severity.High, 0, "Error during updation of cancellation charge due to: " + ex.Message, "");
            }            
        }
        /// <summary>
        /// Load CancellationCharges odject
        /// </summary>
        /// <param name="ticketId"></param>
        public void Load(int referenceId, ProductType productType)
        {
            Trace.TraceInformation("CancellationCharges.Load entered.");
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@referenceId", referenceId);
            paramList[1] = new SqlParameter("@productTypeId", (int)productType);
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetCancellationCharge, paramList, connection);
            if (datareader.Read())
            {
                this.PaymentDetailId = Convert.ToInt32(datareader["paymentDetailId"]);
                this.SupplierFee = Convert.ToDecimal(datareader["supplierFee"]);
                this.AdminFee = Convert.ToDecimal(datareader["adminFee"]);
                this.ReferenceId = Convert.ToInt32(datareader["referenceId"]);
                this.ProductType = (ProductType)Enum.Parse(typeof(ProductType), datareader["productTypeId"].ToString());
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("CancellationCharges.Load exiting : ");
        }
        /// <summary>
        ///Overloaded Load() method to retrieve the CancellationCharges object on the basis of itemtype(online or offline) 
        /// </summary>
        /// <param name="ticketId" or "offlineBookingId"></param>
        public void Load(int referenceId, ProductType productType, InvoiceItemTypeId itemTypeId)
        {
            Trace.TraceInformation("CancellationCharges.Load entered.");
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@referenceId", referenceId);
            paramList[1] = new SqlParameter("@productTypeId", (int)productType);
            paramList[2] = new SqlParameter("@itemTypeId", (Int16)itemTypeId);
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetCancellationChargeOnItemTypeBasis, paramList, connection);
            if (datareader.Read())
            {
                this.PaymentDetailId = Convert.ToInt32(datareader["paymentDetailId"]);
                this.SupplierFee = Convert.ToDecimal(datareader["supplierFee"]);
                this.AdminFee = Convert.ToDecimal(datareader["adminFee"]);
                this.ReferenceId = Convert.ToInt32(datareader["referenceId"]);
                this.ProductType = (ProductType)Enum.Parse(typeof(ProductType), datareader["productTypeId"].ToString());
                this.ItemTypeId = (InvoiceItemTypeId)Enum.Parse(typeof(InvoiceItemTypeId), datareader["itemTypeId"].ToString());
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("CancellationCharges.Load exiting : ");
        }
    }
    /// <summary>
    /// Contains ticker advisory for certaind advisory type
    /// </summary>
    public struct Advisory
    {
        /// <summary>
        /// advisory type field(like Domesic,International)
        /// </summary>
        private string advisoryType;
        private string advisoryText;
        /// <summary>
        /// Gets or Sets the field advisory type
        /// </summary>
        public string AdvisoryType
        {
            get
            {
                return this.advisoryType;
            }
            set
            {
                this.advisoryType = value;
            }
        }
        /// <summary>
        /// Get or Sets the field advisoryText
        /// </summary>
        public string AdvisoryText
        {
            get
            {
                return this.advisoryText;
            }
            set
            {
                this.advisoryText = value;
            }
        }
        /// <summary>
        /// Saves the advisory in database(If same advisory type is present then it'll update the advisory text)
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("Advisory.Save entered.");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@advisoryType", advisoryType);
            paramList[1] = new SqlParameter("@advisoryText", advisoryText);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateAdvisory, paramList);
            Trace.TraceInformation("Advisory.Save exiting : rowsAffected = " + rowsAffected);
        }
        /// <summary>
        /// Gets List of All the advisories(of all types)
        /// </summary>
        /// <returns></returns>
        public static List<Advisory> GetAllAdvisory()
        {
            Trace.TraceInformation("Advisory.GetAllAdvisory entered.");
            List<Advisory> advisoryList = new List<Advisory>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetAllAdvisory, paramList, connection);
            while (datareader.Read())
            {
                Advisory tempAdvisory = new Advisory();
                tempAdvisory.advisoryText = datareader["advisoryText"].ToString();
                tempAdvisory.advisoryType = datareader["advisoryType"].ToString();
                advisoryList.Add(tempAdvisory);
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("Advisory.GetAllAdvisory exiting : ");
            return advisoryList;
        }
        /// <summary>
        /// Loads advisory for certain advisory type
        /// </summary>
        /// <param name="advisoryType">advisory type</param>
        public void Load(string advisoryType)
        {
            Trace.TraceInformation("Advisory.Load entered.");
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@advisoryType", advisoryType);
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetAdvisory, paramList, connection);
            if (datareader.Read())
            {
                advisoryText = datareader["advisoryText"].ToString();
                this.advisoryType = advisoryType;
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("Advisory.Load exiting :");
        }
    }

    public struct HotelBookingResponse
    {
        private string confirmationNo;
        private HotelBookingStatus status;
        private string error;

        public string ConfirmationNo
        {
            get { return confirmationNo; }
            set { confirmationNo = value; }
        }

        public HotelBookingStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public string Error
        {
            get { return error; }
            set { error = value; }
        }

        public HotelBookingResponse(HotelBookingStatus status, string error, string confirmationNo)
        {
            this.status = status;
            this.error = error;
            this.confirmationNo = confirmationNo;
        }
    }
    public struct TransferBookingResponse
    {
        private string confirmationNo;
        private TransferBookingStatus status;
        private string error;

        public string ConfirmationNo
        {
            get { return confirmationNo; }
            set { confirmationNo = value; }
        }

        public TransferBookingStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public string Error
        {
            get { return error; }
            set { error = value; }
        }

        public TransferBookingResponse(TransferBookingStatus status, string error, string confirmationNo)
        {
            this.status = status;
            this.error = error;
            this.confirmationNo = confirmationNo;
        }
    }
    public struct SightseeingBookingResponse
    {
        private string confirmationNo;
        private SightseeingBookingStatus status;
        private string error;

        public string ConfirmationNo
        {
            get { return confirmationNo; }
            set { confirmationNo = value; }
        }

        public SightseeingBookingStatus Status
        {
            get { return status; }
            set { status = value; }
        }

        public string Error
        {
            get { return error; }
            set { error = value; }
        }

        public SightseeingBookingResponse(SightseeingBookingStatus status, string error, string confirmationNo)
        {
            this.status = status;
            this.error = error;
            this.confirmationNo = confirmationNo;
        }
    }
}
