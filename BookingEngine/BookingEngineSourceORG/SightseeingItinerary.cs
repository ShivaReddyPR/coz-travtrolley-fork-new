using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;
using System.Diagnostics;
using Technology.Data;
using System.Transactions;

namespace Technology.BookingEngine
{
    public enum SightseeingBookingSource
    {
        GTA = 1
    }
    public enum SightseeingBookingStatus
    {
        Confirmed = 1,
        Cancelled = 2,
        Failed = 0,
        Pending = 3,
        Error = 4
    }
    [Serializable]
    public class SightseeingItinerary : Technology.BookingEngine.Product
    {
        private int sightseeingId;
        private string cityCode;     //City Code
        private string cityName;
        private string itemCode;     //Item Code
        private string itemName;
        private string currency;             //Price Information
        private PriceAccounts priceInfo;
        private SightseeingBookingStatus status;        //Item Status
        private string confirmationNo;    //Item Reference in Booking
        private DateTime tourDate;   //Tour Date
        private string tourLanguage;
        private List<string> paxNames; //Pax Id list according to the PaxType List
        private List<int> childAge;
        private int adultCount;
        private int childCount;
        private string bookingRef; //Random-generated Unique Identifier
        private DateTime createdOn;
        private int createdBy;
        private DateTime lastModifiedOn;
        private int lastModifiedBy;
        private bool isDomestic;
        private bool voucherStatus;
        private DateTime lastCancellationDate;
        private string cancelPolicy;
        private List<SightseeingPenalty> penalityInfo;
        private string cancelId;
        private SightseeingBookingSource source;
        private string duration;
        private string depTime;
        private string depPointInfo;
        private string specialCode;
        private string supplierInfo;
        public int SightseeingId
        {
            get { return sightseeingId; }
            set { sightseeingId = value; }
        }
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }
        public string ItemCode
        {
            get { return itemCode; }
            set { itemCode = value; }
        }
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        public List<string> PaxNames
        {
            get { return paxNames; }
            set { paxNames = value; }
        }
        public int AdultCount
        {
            get { return adultCount; }
            set { adultCount = value; }
        }
        public int ChildCount
        {
            get { return childCount; }
            set { childCount = value; }
        }
        public string CancelId
        {
            get { return cancelId; }
            set { cancelId = value; }
        }
        public string ConfirmationNo
        {
            get { return confirmationNo; }
            set { confirmationNo = value; }
        }
        public PriceAccounts Price
        {
            get { return priceInfo; }
            set { priceInfo = value; }
        }
        public SightseeingBookingStatus BookingStatus
        {
            get { return status; }
            set { status = value; }
        }
        public DateTime TourDate
        {
            get { return tourDate; }
            set { tourDate = value; }
        }
        public string Language
        {
            get { return tourLanguage; }
            set { tourLanguage = value; }
        }
        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        public List<SightseeingPenalty> PenalityInfo
        {
            get { return penalityInfo; }
            set { penalityInfo = value; }
        }
        public string CancellationPolicy
        {
            get { return cancelPolicy; }
            set { cancelPolicy = value; }
        }
        public DateTime LastCancellationDate
        {
            get { return lastCancellationDate; }
            set { lastCancellationDate = value; }
        }
        public string BookingReference
        {
            get { return bookingRef; }
            set { bookingRef = value; }
        }
        public bool IsDomestic
        {
            get { return isDomestic; }
            set { isDomestic = value; }
        }
        public bool VoucherStatus
        {
            get { return voucherStatus; }
            set { voucherStatus = value; }
        }
        public string ItemName
        {
            get { return itemName; }
            set { itemName = value; }
        }
        public List<int> ChildAge
        {
            get { return childAge; }
            set { childAge = value; }
        }
        public SightseeingBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        public string Duration
        {
            get { return duration; }
            set { duration = value; }
        }
        public string DepTime
        {
            get { return depTime; }
            set { depTime = value; }
        }
        public string DepPointInfo
        {
            get { return depPointInfo; }
            set { depPointInfo = value; }
        }
        public string SpecialCode
        {
            get { return specialCode; }
            set { specialCode = value; }
        }
        public string SupplierInfo
        {
            get { return supplierInfo; }
            set { supplierInfo = value; }
        }
        #region Methods
        /// <summary>
        /// This Method is used to Save the Sightseeing Details.
        /// </summary>
        public override void Save(Product prod)
        {
            Trace.TraceInformation("SightseeingItinerary.Save entered.");
            SightseeingItinerary itinearary = (SightseeingItinerary)prod;
            SqlParameter[] paramList = new SqlParameter[28];
            using (TransactionScope updateTransaction = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
            {
                itinearary.Price.Save();
                string paxName = string.Empty;
                foreach (string pax in itinearary.PaxNames)
                {
                    paxName += pax + "|";
                }
                string childData = string.Empty;
                foreach (int age in itinearary.ChildAge)
                {
                    childData += age.ToString() + "|";
                }
                paramList[0] = new SqlParameter("@sightseeingId", itinearary.SightseeingId);
                paramList[1] = new SqlParameter("@cityCode", itinearary.CityCode);
                paramList[2] = new SqlParameter("@cityName", itinearary.CityName);
                paramList[3] = new SqlParameter("@itemCode", itinearary.ItemCode);
                paramList[4] = new SqlParameter("@itemName", itinearary.ItemName);
                paramList[5] = new SqlParameter("@currency", itinearary.Currency);
                paramList[6] = new SqlParameter("@status", itinearary.BookingStatus);
                paramList[7] = new SqlParameter("@priceId", itinearary.Price.PriceId);
                paramList[8] = new SqlParameter("@confirmationNo", itinearary.ConfirmationNo);
                paramList[9] = new SqlParameter("@tourDate", itinearary.TourDate);
                paramList[10] = new SqlParameter("@tourLanguage", itinearary.Language);
                paramList[11] = new SqlParameter("@paxNames", paxName);
                paramList[12] = new SqlParameter("@childAge", childData);
                paramList[13] = new SqlParameter("@adultCount", itinearary.AdultCount);
                paramList[14] = new SqlParameter("@childCount", itinearary.ChildCount);
                paramList[15] = new SqlParameter("@bookingRef", itinearary.BookingReference);
                paramList[16] = new SqlParameter("@createdBy", itinearary.CreatedBy);
                paramList[17] = new SqlParameter("@isDomestic", itinearary.IsDomestic);
                paramList[18] = new SqlParameter("@voucherStatus", itinearary.VoucherStatus);
                paramList[19] = new SqlParameter("@lastCancellationDate", itinearary.LastCancellationDate);
                paramList[20] = new SqlParameter("@cancelPolicy", itinearary.CancellationPolicy);
                paramList[21] = new SqlParameter("@cancelId", itinearary.CancelId);
                paramList[22] = new SqlParameter("@source", itinearary.Source);
                paramList[23] = new SqlParameter("@duration", itinearary.Duration);
                paramList[24] = new SqlParameter("@depTime", itinearary.DepTime);
                paramList[25] = new SqlParameter("@depPointInfo", itinearary.DepPointInfo);
                paramList[26] = new SqlParameter("@specialCode", itinearary.SpecialCode);
                paramList[27] = new SqlParameter("@supplierInfo", itinearary.SupplierInfo);
                paramList[0].Direction = ParameterDirection.Output;

                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddSightseeingItinerary, paramList);
                sightseeingId = (int)paramList[0].Value;
                if (itinearary.PenalityInfo != null)
                {
                    foreach (SightseeingPenalty penality in itinearary.PenalityInfo)
                    {
                        penality.SightseeingId = sightseeingId;
                        penality.Save();
                    }
                }
                updateTransaction.Complete();
            }
            Trace.TraceInformation("SightseeingItinerary.Save Exit.");
        }
        /// <summary>
        /// This Method is used to Load the Sightseeing Details based on Transfer ID.
        /// </summary>
        /// <param name="transferId"></param>
        public void Load(int ssId)
        {
            Trace.TraceInformation("SightseeingItinerary.Load entered : Id = " + ssId);
            if (ssId <= 0)
            {
                throw new ArgumentException("Sightseeing Id Id should be positive integer");
            }
            this.sightseeingId = ssId;
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@sightseeingId", sightseeingId);
            try
            {
                PriceAccounts priceData = new PriceAccounts();
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetSightseeingItinerary, paramList, connection);
                if (data.Read())
                {
                    confirmationNo = data["confirmationNo"].ToString();
                    itemCode = data["itemCode"].ToString();
                    cityCode = data["cityCode"].ToString();
                    cityName = data["cityName"].ToString();
                    itemName = data["itemName"].ToString();
                    currency = data["currency"].ToString();
                    status = (SightseeingBookingStatus)Convert.ToInt16(data["status"]);
                    priceData.Load(Convert.ToInt32(data["priceId"]));
                    priceInfo=priceData;
                    confirmationNo = data["confirmationNo"].ToString();
                    tourDate = Convert.ToDateTime(data["tourDate"]);
                    tourLanguage = data["tourLanguage"].ToString();
                    string[] PaxList = data["paxNames"].ToString().Split('|');
                    List<string> paxData = new List<string>();
                    for (int i = 0; i < PaxList.Length - 1; i++)
                    {
                        paxData.Add(PaxList[i]);
                    }
                    paxNames = paxData;
                    string[] ageLst = data["childAge"].ToString().Split('|');
                    List<int> ageData = new List<int>();
                    for (int j = 0; j < ageLst.Length - 1; j++)
                    {
                        ageData.Add(Convert.ToInt32(ageLst[j]));
                    }
                    childAge = ageData;
                    adultCount = Convert.ToInt32(data["adultCount"]);
                    childCount = Convert.ToInt32(data["childCount"]);
                    bookingRef = data["bookingRef"].ToString();
                    createdBy = Convert.ToInt32(data["createdBy"]);
                    isDomestic = Convert.ToBoolean(data["isDomestic"]);
                    voucherStatus = Convert.ToBoolean(data["voucherStatus"]);
                    lastCancellationDate = Convert.ToDateTime(data["lastCancellationDate"]);
                    cancelPolicy = data["cancelPolicy"].ToString();
                    createdOn = Convert.ToDateTime(data["createdOn"]);
                    cancelId = data["cancelId"].ToString();
                    source = (SightseeingBookingSource)Convert.ToInt32(data["source"]);
                    duration = data["duration"].ToString();
                    depTime = data["depTime"].ToString();
                    depPointInfo = data["depPointInfo"].ToString();
                    specialCode = data["specialCode"].ToString();
                    supplierInfo = data["supplierInfo"].ToString();
                }
                else
                {
                    data.Close();
                    connection.Close();
                    Trace.TraceInformation("SightseeingItinerary.Load exiting : sightseeingId does not exist.transferId = " + ssId.ToString());
                }
                data.Close();
                connection.Close();
                SightseeingPenalty penality = new SightseeingPenalty();
                penalityInfo = penality.GetSightseeingPenality(ssId);
            }
            catch (Exception exp)
            {
                connection.Close();
                throw new ArgumentException("Sightseeing id does not exist in database");
            }
        }
        /// <summary>
        /// This Method is used to get SightseeingId using Confirmation Number
        /// </summary>
        /// <param name="confNo"></param>
        /// <returns></returns>
        public static int GetSightseeingId(string confNo)
        {
            Trace.TraceInformation("SightseeingItinerary.GetSightseeingId entered : confNo = " + confNo);
            int sightseeingId = 0;
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@confNo", confNo);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetSightseeingId, paramList, connection);
            if (data.Read())
            {
                sightseeingId = Convert.ToInt32(data["sightseeingId"]);
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("SightseeingItinerary.GetSightseeingId exiting :" + sightseeingId.ToString());
            return sightseeingId;
        }
        /// <summary>
        /// This Method is sued to Update the Voucher status
        /// </summary>
        public void UpdateVoucherStatus()
        {
            Trace.TraceInformation("TransferItinerary.UpdateVoucherStatus entered  ");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@voucherStatus", voucherStatus);
            paramList[1] = new SqlParameter("@sightseeingId", sightseeingId);

            int retVal = Dal.ExecuteNonQuerySP(SPNames.UpdateSightseeingVoucherStatus, paramList);
            Trace.TraceInformation("SightseeingItinerary.UpdateSightseeingStatus exiting count" + retVal);
        }
        /// <summary>
        /// This MEthod is used to Update the Itinerary BookingStatus
        /// </summary>
        public void UpdateBookingStatus()
        {
            Trace.TraceInformation("SightseeingItinerary.UpdateBookingStatus entered  ");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@status", status);
            paramList[1] = new SqlParameter("@cancelId", cancelId);
            paramList[2] = new SqlParameter("@sightseeingId", sightseeingId);

            int retVal = Dal.ExecuteNonQuerySP(SPNames.UpdateSightseeingItineary, paramList);
            Trace.TraceInformation("SightseeingItinerary.UpdateBookingStatus  exiting count" + retVal);
        }
        #endregion

    }
}
