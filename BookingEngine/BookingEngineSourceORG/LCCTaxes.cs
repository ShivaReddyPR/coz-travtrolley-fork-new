using System;
using System.Collections.Generic;
using System.Text;
using Technology.Data;
using System.Data.SqlClient;
using CoreLogic;
using System.Configuration;
using System.Collections;
using CoreLogic;
using Technology.BookingEngine;
using System.Threading;
using System.Data;
using System.Diagnostics;

namespace Technology.BookingEngine
{
    public class LCCTaxes
    {
        /// <summary>
        /// Origin City Code.
        /// </summary>
        private string origin;
        /// <summary>
        /// Origin City Code
        /// </summary>
        public string Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
            }
        }

        /// <summary>
        /// Destiantion City Code
        /// </summary>
        private string destination;
        /// <summary>
        /// Destiantion City Code
        /// </summary>
        public string Destination
        {
            get
            {
                return destination;
            }
            set
            {
                destination = value;
            }
        }

        /// <summary>
        ///LCC source 
        /// </summary>
        private string source;

        /// <summary>
        ///LCC source whose Tax are to be saved
        /// </summary>
        public string Source
        {
            get
            {
                return source;
            }
            set
            {
                source = value;
            }
        }

        /// <summary>
        ///Tax value
        /// </summary>
        private decimal tax;

        /// <summary>
        ///Tax Value
        /// </summary>
        public decimal Tax
        {
            get
            {
                return tax;
            }
            set
            {
                tax = value;
            }
        }

        /// <summary>
        /// Fuel Surcharge
        /// </summary>
        private decimal fuelSurcharge;
        /// <summary>
        /// Fuel Surcharge
        /// </summary>
        public decimal FuelSurcharge
        {
            get
            {
                return fuelSurcharge;
            }
            set
            {
                fuelSurcharge = value;
            }
        }
        /// <summary>
        ///Tax value
        /// </summary>
        private bool status;

        /// <summary>
        ///Tax Value
        /// </summary>
        public bool Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        /// <summary>
        /// Method to the taxes and fuel surcharge for LCC's
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("MetaSearchEngine.SaveLccTaxes entered.");
            if (origin == null || origin.Length == 0)
            {
                throw new BookingEngineException("Origin cannot be blank");
            }
            if (destination == null || destination.Length == 0)
            {
                throw new BookingEngineException("Destination cannot be blank");
            }
            if (source == null || source.Length == 0)
            {
                throw new BookingEngineException("Source cannot be blank");
            }
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("tax", tax);
            paramList[1] = new SqlParameter("fuelSurcharge", fuelSurcharge);
            paramList[2] = new SqlParameter("source", source);
            paramList[3] = new SqlParameter("origin", origin);
            paramList[4] = new SqlParameter("destination", destination);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateLccTaxes, paramList);
            Trace.TraceInformation("MetaSearchEngine.SaveLccTaxes exited.");
        }  
        public static LCCTaxes GetLccTax(string source, string origin, string destination)
        {
            Trace.TraceInformation("LCCTaxes.GetLccTax entered.");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@source", source);
            paramList[1] = new SqlParameter("@origin", origin);
            paramList[2] = new SqlParameter("destination", destination);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader reader = Dal.ExecuteReaderSP(SPNames.GetLccTax, paramList, connection);
            LCCTaxes lccTax = new LCCTaxes();
            lccTax.source = source;
            lccTax.origin = origin;
            lccTax.destination = destination;
            if (reader.Read())
            {
                lccTax.Tax = Convert.ToDecimal(reader["tax"]);
                lccTax.FuelSurcharge = Convert.ToDecimal(reader["fuelSurcharge"]);
            }
            reader.Close();
            connection.Close();
            return lccTax;
        }

        /// <summary>
        /// Gets list of LCC Taxes for a particular airline
        /// </summary>
        /// <param name="source">Source whose taxes are to be retrieved</param>
        /// <returns>List of objects of LCCTaxes</returns>
        public static List<LCCTaxes> GetList(string source)
        {
            Trace.TraceInformation("MetaSearchEngine.GetList entered : source = " + source);
            List<LCCTaxes> lccTaxes = new List<LCCTaxes>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@source", source);
            SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetListOfLcctaxes, paramList, connection);
            List<string> originDest = new List<string>();
            while (dataReader.Read())
            {
                LCCTaxes lccTax = new LCCTaxes();
                lccTax.origin = Convert.ToString(dataReader["origin"]);
                lccTax.destination = Convert.ToString(dataReader["destination"]);
                lccTax.source = Convert.ToString(dataReader["source"]);
                lccTax.tax = Convert.ToDecimal(dataReader["tax"]);
                lccTax.fuelSurcharge = Convert.ToDecimal(dataReader["fuelSurcharge"]);
                if (lccTax.origin.Trim().Length > 0 && lccTax.destination.Trim().Length > 0)
                {
                    lccTaxes.Add(lccTax);
                }
            }
            dataReader.Close();
            connection.Close();
            return lccTaxes;
        }
    }
}
