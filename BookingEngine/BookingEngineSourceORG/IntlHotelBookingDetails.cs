using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;

namespace Technology.BookingEngine
{
    public enum TypeOfDocument
    {
        PassportFront1=1,
        PassportFront2=2,
        PassportAdressPage1=3,
        PassportBack2=4,
        BTQForm=5,
        TravellerCheque=6,
        PassportRenew=7
    }
    public class IntlHotelBookingDetails
    {
        private int  hotelId;
        TypeOfDocument documentName;
        private string fileName;
            
        public int  HotelId 
        {
            get {return hotelId;}
            set {hotelId=value;}
        }

        public TypeOfDocument DocumentName
        {
            get { return documentName; }
            set { documentName = value; }
        }
       
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public List<IntlHotelBookingDetails> Load(int hotelId)
        {
            Trace.TraceInformation("IntlHotelBookingDetails.Load entered :hotelID = " + hotelId);
            if (hotelId<=0)
            {
                throw new ArgumentException("hotelId value is null", "hotelId");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            List<IntlHotelBookingDetails> listDetails = new List<IntlHotelBookingDetails>();
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetIntelHotelById, paramList, connection);
            while(data.Read())
            {
                IntlHotelBookingDetails internationalInfo = new IntlHotelBookingDetails();
                internationalInfo.hotelId = Convert.ToInt32(data["hotelId"]);
                internationalInfo.DocumentName = (TypeOfDocument)data["typeOfDocument"];
                internationalInfo.FileName = data["fileName"].ToString();
                listDetails.Add(internationalInfo);
            }
            data.Close();
            connection.Close();
        
            Trace.TraceInformation("IntlHotelBookingDetail.Load exiting.");
            return listDetails;
        }
        public void Save()
        {
            Trace.TraceInformation("IntlHotelBookingDetails.Save entered.");
            SqlParameter[] paramList = new SqlParameter[3];
            try
            {
                paramList[0] = new SqlParameter("@hotelId", hotelId);
                paramList[1] = new SqlParameter("@typeOfDocument", (TypeOfDocument)documentName);
                paramList[2] = new SqlParameter("@fileName", fileName);
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddIntlHotelBookingDetails, paramList);
            }
            catch (Exception exp)
            {
                throw new Exception(exp.Message);
            }
           
            Trace.TraceInformation("IntlHotelBookingDetails.Save exiting");
        }
    }
}
