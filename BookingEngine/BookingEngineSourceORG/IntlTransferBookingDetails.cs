using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;


namespace Technology.BookingEngine
{
   public class IntlTransferBookingDetails
    {
        private int transferId;
        TypeOfDocument documentName;
        private string fileName;

        public int TransferId
        {
            get { return transferId; }
            set { transferId = value; }
        }

        public TypeOfDocument DocumentName
        {
            get { return documentName; }
            set { documentName = value; }
        }

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public List<IntlTransferBookingDetails> Load(int transferId)
        {
            Trace.TraceInformation("IntlTransferBookingDetails.Load entered :transferID = " + transferId);
            if (transferId <= 0)
            {
                throw new ArgumentException("transferId value is null", "transferId");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@transferId", transferId);
            List<IntlTransferBookingDetails> listDetails = new List<IntlTransferBookingDetails>();
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetIntelTransferById, paramList, connection);
            while (data.Read())
            {
                IntlTransferBookingDetails internationalInfo = new IntlTransferBookingDetails();
                internationalInfo.transferId = Convert.ToInt32(data["transferId"]);
                internationalInfo.DocumentName = (TypeOfDocument)data["typeOfDocument"];
                internationalInfo.FileName = data["fileName"].ToString();
                listDetails.Add(internationalInfo);
            }
            data.Close();
            connection.Close();

            Trace.TraceInformation("IntlTransferBookingDetail.Load exiting.");
            return listDetails;
        }
        public void Save()
        {
            Trace.TraceInformation("IntlTransferBookingDetails.Save entered.");
            SqlParameter[] paramList = new SqlParameter[3];
            try
            {
                paramList[0] = new SqlParameter("@transferId", transferId);
                paramList[1] = new SqlParameter("@typeOfDocument", (TypeOfDocument)documentName);
                paramList[2] = new SqlParameter("@fileName", fileName);
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddIntlTransferBookingDetails, paramList);
            }
            catch (Exception exp)
            {
                throw new Exception(exp.Message);
            }

            Trace.TraceInformation("IntlTransferBookingDetails.Save exiting");
        }
    }
}
