using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using Technology.Data;
namespace Technology.BookingEngine
{
    public enum PriceType
    {
        PublishedFare = 1,
        NetFare = 2
    }

    [Serializable]
    public class PriceAccounts
    {
        public PriceAccounts()
        {
            publishedFare = 0;
            netFare = 0;
            markup = 0;
            agentCommission = 0;
            agentPLB = 0;
            ourCommission = 0;
            ourPLB = 0;
            tax = 0;
            otherCharges = 0;
            chargeBU = new List<ChargeBreakUp>();
            tdsCommission = 0;
            tdsPLB = 0;
            serviceTax = 0;
            whiteLabelDiscount = 0;
            transactionFee = 0;
            currency = "";
            airlineTransFee = 0;
            accPriceType = PriceType.PublishedFare;
            cessTax = 0;
            wlCharge = 0;
            discount = 0;
            reverseHandlingCharge = 0;
            commissionType = "RB";
            yqTax = 0;
            isServiceTaxOnBaseFarePlusYQ = false;
            tdsRate = 0;
        }
        public PriceAccounts(decimal pFare, decimal nFare, decimal markUP, decimal agentComm, decimal agentPlb, decimal ourComm, decimal ourPlb, decimal TAX, decimal otherCharge, decimal tdsReq, decimal serviceTaxReq, decimal whiteLabelDiscountReq, decimal transactionFeeReq, string curr)
        {
            publishedFare = pFare;
            netFare = nFare;
            markup = markUP;
            agentCommission = agentComm;
            agentPLB = agentPlb;
            ourCommission = ourComm;
            ourPLB = ourPlb;
            tax = TAX;
            otherCharges = otherCharge;
            chargeBU = new List<ChargeBreakUp>();
            tdsCommission = tdsReq;
            tdsPLB = tdsReq;
            serviceTax = serviceTaxReq;
            whiteLabelDiscount = whiteLabelDiscountReq;
            transactionFee = transactionFeeReq;
            currency = curr;
            commissionType = "RB";
            yqTax = 0;
            isServiceTaxOnBaseFarePlusYQ = false;
        }

        public PriceAccounts(decimal pFare, decimal nFare, decimal markUP, decimal agentComm, decimal agentPlb, decimal ourComm, decimal ourPlb, decimal TAX, decimal otherCharge, decimal tdsReq, decimal serviceTaxReq, decimal whiteLabelDiscountReq, decimal transactionFeeReq, string curr, PriceType pType)
        {
            publishedFare = pFare;
            netFare = nFare;
            markup = markUP;
            agentCommission = agentComm;
            agentPLB = agentPlb;
            ourCommission = ourComm;
            ourPLB = ourPlb;
            tax = TAX;
            otherCharges = otherCharge;
            chargeBU = new List<ChargeBreakUp>();
            tdsCommission = tdsReq;
            tdsPLB = tdsReq;
            serviceTax = serviceTaxReq;
            whiteLabelDiscount = whiteLabelDiscountReq;
            transactionFee = transactionFeeReq;
            currency = curr;
            accPriceType = pType;
            commissionType = "RB";
            yqTax = 0;
            isServiceTaxOnBaseFarePlusYQ = false;
        }

        /// <summary>
        /// Unique field for price
        /// </summary>
        private int priceId;
        /// <summary>
        /// Field contained Published Fare of booking
        /// </summary>
        private decimal publishedFare;
        /// <summary>
        /// Field contained netPrice of booking
        /// </summary>
        private decimal netFare;
        /// <summary>
        /// Field contained markup(incase of net fare)
        /// </summary>
        private decimal markup;
        /// <summary>
        /// Field contained tax at that price(in currency)
        /// </summary>
        private decimal tax;
        /// <summary>
        /// Field contains other charges(in currency)
        /// </summary>
        private decimal otherCharges;
        /// <summary>
        /// Field contains our commission
        /// </summary>
        private decimal ourCommission;
        /// <summary>
        /// Field contains our PLB(in currency)
        /// </summary>
        private decimal ourPLB;
        /// <summary>
        /// Commission field(for agent in currency)
        /// </summary>
        private decimal agentCommission;
        /// <summary>
        /// PLB field (for agent in rupees)
        /// </summary>
        private decimal agentPLB;
        ///// <summary>
        ///// Field contained tds of booking(e.g.in currency)
        ///// </summary>
        //private decimal tds;
        /// <summary>
        /// Field contained serviceTax of booking
        /// </summary>
        private decimal serviceTax;
        /// <summary>
        /// Currency string
        /// </summary>
        private string currency;
        private decimal tdsCommission;
        private decimal tdsPLB;
        /// <summary>
        /// Discount given to white label customer
        /// </summary>
        private decimal whiteLabelDiscount;
        /// <summary>
        /// Transaction Fees specially for LCC (based on basefare+fuelSurcharge)
        /// </summary>
        private decimal transactionFee;
        /// <summary>
        /// This contains airline transaction fee - AS Airdeccan has a seperate transaction fee term
        /// </summary>
        private decimal airlineTransFee;
        /// <summary>
        /// This list contains the charge breakup for other charges
        /// </summary>
        private List<ChargeBreakUp> chargeBU;
        /// <summary>
        /// Account Price Type - NetFare or Published fare
        /// </summary>
        private PriceType accPriceType;
        /// <summary>
        /// it will contain Rate of Exchange
        /// </summary>
        private decimal rateOfExchange;
        /// <summary>
        /// This will contain currency code - ie USD
        /// </summary>
        private string currencyCode;
        /// <summary>
        /// This fee will be a part of Published fare but not of Offered Fare
        /// </summary>
        private decimal additionalTxnFee;
        /// <summary>
        /// Charge taken by agent from WL customer
        /// </summary>
        private decimal wlCharge;
        /// <summary>
        /// This is for train concession
        /// </summary>
        private decimal discount;

        /// <summary>
        /// About to be deducted from Agent Commision for IMport PNR
        /// </summary>
        private decimal reverseHandlingCharge;
        /// <summary>
        /// CommissionType method(RB/RT/V)
        /// </summary>
        private string commissionType;

        /// <summary>
        /// Field contains tdsRate
        /// </summary>
        private decimal tdsRate;
        private bool isServiceTaxOnBaseFarePlusYQ;

        public string CommissionType
        {
            get { return commissionType; }
            set { commissionType = value; }
        }
        public decimal AirlineTransFee
        {
            get
            {
                return this.airlineTransFee;
            }
            set
            {
                this.airlineTransFee = value;
            }
        }

        public decimal TdsCommission
        {
            get
            {
                return this.tdsCommission;
            }
            set
            {
                this.tdsCommission = value;
            }
        }
        public decimal TDSPLB
        {
            get
            {
                return this.tdsPLB;
            }
            set
            {
                this.tdsPLB = value;
            }
        }
        /// <summary>
        /// Unique property for price
        /// </summary>
        public int PriceId
        {
            get
            {
                return this.priceId;
            }
            set
            {
                this.priceId = value;
            }
        }
        /// <summary>
        /// Property contained Published fare of the booking
        /// </summary>
        public decimal PublishedFare
        {
            get
            {
                return this.publishedFare;
            }
            set
            {
                this.publishedFare = value;
            }
        }
        /// <summary>
        /// Property contained Net Fare on Price
        /// </summary>
        public decimal NetFare
        {
            get
            {
                return this.netFare;
            }
            set
            {
                this.netFare = value;
            }
        }
        /// <summary>
        /// Property contained markup(incase of net fare)
        /// </summary>
        public decimal Markup
        {
            get
            {
                return this.markup;
            }
            set
            {
                this.markup = value;
            }
        }
        /// <summary>
        /// Property contains the service tax of booking
        /// </summary>
        public decimal SeviceTax
        {
            get
            {
                return this.serviceTax;
            }
            set
            {
                this.serviceTax = value;
            }
        }

        ///<summary>
        ///Field to cotain Cess Tax
        ///</summary>

        private decimal cessTax;
        /// <summary>
        /// Property to contain cessTax
        /// </summary>
        public decimal CessTax
        {
            get { return cessTax; }
            set { cessTax = value; }
        }
        ///// <summary>
        ///// property contained TDS of that booking
        ///// </summary>
        //public decimal TDS
        //{
        //    get
        //    {
        //        return this.tds;
        //    }
        //    set
        //    {
        //        this.tds = value;
        //    }
        //}
        /// <summary>
        /// Commission property for us(in currency)
        /// </summary>
        public decimal OurCommission
        {
            get
            {
                return this.ourCommission;
            }
            set
            {
                this.ourCommission = value;
            }
        }
        /// <summary>
        /// PLB property for us(in rupees)
        /// </summary>
        public decimal OurPLB
        {
            get
            {
                return this.ourPLB;
            }
            set
            {
                this.ourPLB = value;
            }
        }
        /// <summary>
        /// Commission property for agent(in rupees)
        /// </summary>
        public decimal AgentCommission
        {
            get
            {
                return this.agentCommission;
            }
            set
            {
                this.agentCommission = value;
            }
        }
        /// <summary>
        /// PLB property for agent(in rupees)
        /// </summary>
        public decimal AgentPLB
        {
            get
            {
                return this.agentPLB;
            }
            set
            {
                this.agentPLB = value;
            }
        }
        /// <summary>
        /// Other charges property for agent(in rupees)
        /// </summary>
        public decimal OtherCharges
        {
            get
            {
                return this.otherCharges;
            }
            set
            {
                this.otherCharges = value;
            }
        }
        /// <summary>
        /// Tax property at that price
        /// </summary>
        public decimal Tax
        {
            get
            {
                return this.tax;
            }
            set
            {
                this.tax = value;
            }
        }
        /// <summary>
        /// Gets or Sets the white label discount field
        /// </summary>
        public decimal WhiteLabelDiscount
        {
            get
            {
                return this.whiteLabelDiscount;
            }
            set
            {
                this.whiteLabelDiscount = value;
            }
        }
        /// <summary>
        /// Gets or sets TransactionFee field
        /// </summary>
        public decimal TransactionFee
        {
            get
            {
                return this.transactionFee;
            }
            set
            {
                this.transactionFee = value;
            }
        }
        /// <summary>
        /// Currency string for that price
        /// </summary>
        public string Currency
        {
            get
            {
                return this.currency;
            }
            set
            {
                this.currency = value;
            }
        }
        /// <summary>
        /// Gets or sets the list of charge break up for other charges
        /// </summary>
        public List<ChargeBreakUp> ChargeBU
        {
            get
            {
                return chargeBU;
            }
            set
            {
                chargeBU = value;
            }
        }

        /// <summary>
        /// Gets or sets the priceType
        /// </summary>
        public PriceType AccPriceType
        {
            get
            {
                return accPriceType;
            }
            set
            {
                accPriceType = value;
            }

        }
        /// <summary>
        /// Gets or sets the Rate of Exchange
        /// </summary>
        public decimal RateOfExchange
        {
            get { return rateOfExchange; }
            set { rateOfExchange = value; }
        }
        /// <summary>
        /// Gets or sets the Currency Code
        /// </summary>
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }

        /// <summary>
        /// This fee will be a part of Published fare but not of Offered Fare.
        /// </summary>
        public decimal AdditionalTxnFee
        {
            get
            {
                return additionalTxnFee;
            }
            set
            {
                additionalTxnFee = value;
            }
        }
        public decimal WLCharge
        {
            get
            {
                return wlCharge;
            }
            set
            {
                wlCharge = value;
            }
        }
        public decimal Discount
        {
            get
            {
                return discount;
            }
            set
            {
                discount = value;
            }
        }
        public decimal ReverseHandlingCharge
        {
            get
            {
                return reverseHandlingCharge;
            }
            set
            {
                reverseHandlingCharge = value;
            }
        }

        /// <summary>
        ///Field to contain YQTax. 
        /// </summary>
        private decimal yqTax;
        public decimal YQTax
        {
            get { return yqTax; }
            set { yqTax = value; }
        }

        ///<summary>
        /// Is the service tax calculation use YQ
        ///</summary>
        public bool IsServiceTaxOnBaseFarePlusYQ
        {
            get { return isServiceTaxOnBaseFarePlusYQ; }
            set { isServiceTaxOnBaseFarePlusYQ = value; }
        }

        /// <summary>
        /// Field contains tdsRate
        /// </summary>

        public decimal TdsRate
        {
            get
            {
                return tdsRate;
            }
            set
            {
                tdsRate = value;
            }
        }
        public void Save()
        {
            Trace.TraceInformation("PriceAccounts.Save entered : ");

            // For few airlines it contains transation fee - adding this in to published fare
            publishedFare = publishedFare + airlineTransFee;
            if (priceId > 0)
            {
                SqlParameter[] paramList = new SqlParameter[25];
                paramList[0] = new SqlParameter("@priceId", priceId);
                paramList[1] = new SqlParameter("@publishedFare", Math.Round(publishedFare, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[2] = new SqlParameter("@netFare", Math.Round(netFare, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[3] = new SqlParameter("@markup", Math.Round(markup, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[4] = new SqlParameter("@tax", Math.Round(tax, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[5] = new SqlParameter("@ourCommission", Math.Round(ourCommission, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[6] = new SqlParameter("@ourPLB", Math.Round(ourPLB, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[7] = new SqlParameter("@agentCommission", Math.Round(agentCommission, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[8] = new SqlParameter("@agentPLB", Math.Round(agentPLB, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[9] = new SqlParameter("@serviceTax", Math.Round(serviceTax, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[10] = new SqlParameter("@tdsCommission", Math.Round(tdsCommission, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[11] = new SqlParameter("@tdsPLB", Math.Round(tdsPLB, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[12] = new SqlParameter("@otherCharges", Math.Round(otherCharges, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[13] = new SqlParameter("@whiteLabelDiscount", Math.Round(whiteLabelDiscount, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[14] = new SqlParameter("@transactionFee", Math.Round(transactionFee, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[15] = new SqlParameter("@currency", currency);
                paramList[16] = new SqlParameter("@priceType", accPriceType);
                paramList[17] = new SqlParameter("@rateOfExchange", rateOfExchange);
                paramList[18] = new SqlParameter("@currencyCode", currencyCode);
                paramList[19] = new SqlParameter("@additionalTxnFee", Math.Round(additionalTxnFee, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[20] = new SqlParameter("@wlCharge", wlCharge);
                paramList[21] = new SqlParameter("@discount", discount);
                paramList[22] = new SqlParameter("@reverseHandlingCharge", Math.Round(reverseHandlingCharge, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[23] = new SqlParameter("@yqTax", Math.Round(yqTax, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[24] = new SqlParameter("@commissionType", commissionType);

                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdatePrice, paramList);
                Trace.TraceInformation("PriceAccounts.Save exiting : rowsAffected = " + rowsAffected);
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[27];
                paramList[0] = new SqlParameter("@priceId", SqlDbType.Int);
                paramList[0].Direction = ParameterDirection.Output;
                paramList[1] = new SqlParameter("@publishedFare", Math.Round(publishedFare, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                paramList[2] = new SqlParameter("@netFare", Math.Round(netFare, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[3] = new SqlParameter("@markup", Math.Round(markup, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[4] = new SqlParameter("@tax", Math.Round(tax, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[5] = new SqlParameter("@ourCommission", Math.Round(ourCommission, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[6] = new SqlParameter("@ourPLB", Math.Round(ourPLB, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[7] = new SqlParameter("@agentCommission", Math.Round(agentCommission, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[8] = new SqlParameter("@agentPLB", Math.Round(agentPLB, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[9] = new SqlParameter("@serviceTax", Math.Round(serviceTax, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[10] = new SqlParameter("@tdsCommission", Math.Round(tdsCommission, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[11] = new SqlParameter("@tdsPLB", Math.Round(tdsPLB, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[12] = new SqlParameter("@otherCharges", Math.Round(otherCharges, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[13] = new SqlParameter("@whiteLabelDiscount", Math.Round(whiteLabelDiscount, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[14] = new SqlParameter("@transactionFee", Math.Round(transactionFee, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[15] = new SqlParameter("@currency", currency);
                paramList[16] = new SqlParameter("@priceType", accPriceType);
                paramList[17] = new SqlParameter("@rateOfExchange", rateOfExchange);
                paramList[18] = new SqlParameter("@currencyCode", currencyCode);
                paramList[19] = new SqlParameter("@additionalTxnFee", Math.Round(additionalTxnFee, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[20] = new SqlParameter("@wlCharge", wlCharge);
                paramList[21] = new SqlParameter("@discount", discount);
                paramList[22] = new SqlParameter("@commissionType", commissionType);
                paramList[23] = new SqlParameter("@reverseHandlingCharge", Math.Round(reverseHandlingCharge, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[24] = new SqlParameter("@yqTax", Math.Round(yqTax, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                paramList[25] = new SqlParameter("@isServiceTaxOnBaseFarePlusYQ", isServiceTaxOnBaseFarePlusYQ);
                paramList[26] = new SqlParameter("@tdsRate", tdsRate);
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddPrice, paramList);
                priceId = (int)paramList[0].Value;
                for (int i = 0; i < chargeBU.Count; i++)
                {
                    chargeBU[i].PriceId = this.priceId;
                    chargeBU[i].Save();
                }
                Trace.TraceInformation("PriceAccounts.Save exiting : rowsAffected = " + rowsAffected);
            }
        }

        public void Load(int priceId)
        {
            Trace.TraceInformation("PriceAccounts.Load entered : ");
            this.priceId = priceId;
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@priceId", priceId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetPriceDetail, paramList, connection);
            if (data.Read())
            {
                publishedFare = Convert.ToDecimal(data["publishedFare"]);
                netFare = Convert.ToDecimal(data["netFare"]);
                tax = Convert.ToDecimal(data["tax"]);
                markup = Convert.ToDecimal(data["markup"]);
                ourCommission = Convert.ToDecimal(data["ourCommission"]);
                ourPLB = Convert.ToDecimal(data["ourPLB"]);
                agentCommission = Convert.ToDecimal(data["agentCommission"]);
                agentPLB = Convert.ToDecimal(data["agentPLB"]);
                serviceTax = Convert.ToDecimal(data["serviceTax"]);
                tdsRate = Convert.ToDecimal(data["tdsRate"]);
                //tds = Convert.ToDecimal(data["tds"]);
                if (data["tdsCommission"] != DBNull.Value)
                {
                    tdsCommission = Convert.ToDecimal(data["tdsCommission"]);
                }
                if (data["tdsPLB"] != DBNull.Value)
                {
                    tdsPLB = Convert.ToDecimal(data["tdsPLB"]);
                }
                otherCharges = Convert.ToDecimal(data["otherCharges"]);
                whiteLabelDiscount = Convert.ToDecimal(data["whiteLabelDiscount"]);
                transactionFee = Convert.ToDecimal(data["transactionFee"]);
                accPriceType = (PriceType)data["priceType"];

                currency = data["currency"].ToString();
                if (data["rateofExchange"] != DBNull.Value)
                {
                    rateOfExchange = Convert.ToDecimal(data["rateofExchange"]);
                }
                if (data["currencyCode"] != DBNull.Value)
                {
                    currencyCode = data["currencyCode"].ToString();
                }
                additionalTxnFee = Convert.ToDecimal(data["additionalTxnFee"]);
                wlCharge = Convert.ToDecimal(data["wlCharge"]);
                discount = Convert.ToDecimal(data["discount"]);
                if (data["commissionType"] != DBNull.Value)
                {
                    commissionType = data["commissionType"].ToString();
                }
                if (data["reverseHandlingCharge"] != DBNull.Value)
                {
                    reverseHandlingCharge = Convert.ToDecimal(data["reverseHandlingCharge"]);
                }
                if (data["yqTax"] != DBNull.Value)
                {
                    yqTax = Convert.ToDecimal(data["yqTax"]);
                }
                isServiceTaxOnBaseFarePlusYQ = Convert.ToBoolean(data["isServiceTaxOnBaseFarePlusYQ"]);
                ChargeBreakUp cbu = new ChargeBreakUp();
                chargeBU = cbu.Load(priceId);
            }
            else
            {
                new ArgumentException("This price id is not present in database");
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("PriceAccounts.Load exiting :");
        }

        public void LoadForCreditNote(int itemId, int itemTypeId)
        {
            Trace.TraceInformation("PriceAccounts.LoadForCreditNote entered : ");
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@itemId", itemId);
            paramList[1] = new SqlParameter("@itemTypeId", itemTypeId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetPriceDetailForCreditNote, paramList, connection);
            if (data.Read())
            {
                this.priceId = Convert.ToInt32(data["priceId"]);
                publishedFare = Convert.ToDecimal(data["publishedFare"]);
                netFare = Convert.ToDecimal(data["netFare"]);
                tax = Convert.ToDecimal(data["tax"]);
                markup = Convert.ToDecimal(data["markup"]);
                ourCommission = Convert.ToDecimal(data["ourCommission"]);
                ourPLB = Convert.ToDecimal(data["ourPLB"]);
                agentCommission = Convert.ToDecimal(data["agentCommission"]);
                agentPLB = Convert.ToDecimal(data["agentPLB"]);
                serviceTax = Convert.ToDecimal(data["serviceTax"]);
                //tds = Convert.ToDecimal(data["tds"]);
                if (data["tdsCommission"] != DBNull.Value)
                {
                    tdsCommission = Convert.ToDecimal(data["tdsCommission"]);
                }
                if (data["tdsPLB"] != DBNull.Value)
                {
                    tdsPLB = Convert.ToDecimal(data["tdsPLB"]);
                }
                otherCharges = Convert.ToDecimal(data["otherCharges"]);
                whiteLabelDiscount = Convert.ToDecimal(data["whiteLabelDiscount"]);
                transactionFee = Convert.ToDecimal(data["transactionFee"]);
                accPriceType = (PriceType)data["priceType"];

                currency = data["currency"].ToString();
                if (data["rateofExchange"] != DBNull.Value)
                {
                    rateOfExchange = Convert.ToDecimal(data["rateofExchange"]);
                }
                if (data["currencyCode"] != DBNull.Value)
                {
                    currencyCode = data["currencyCode"].ToString();
                }
                additionalTxnFee = Convert.ToDecimal(data["additionalTxnFee"]);
                wlCharge = Convert.ToDecimal(data["wlCharge"]);
                discount = Convert.ToDecimal(data["discount"]);
                ChargeBreakUp cbu = new ChargeBreakUp();
                chargeBU = cbu.Load(priceId);
                if (data["commissionType"] != DBNull.Value)
                {
                    commissionType = data["commissionType"].ToString();
                }
                if (data["reverseHandlingCharge"] != DBNull.Value)
                {
                    reverseHandlingCharge = Convert.ToDecimal(data["reverseHandlingCharge"]);
                }
                if (data["yqTax"] != DBNull.Value)
                {
                    yqTax = Convert.ToDecimal(data["yqTax"]);
                }
                isServiceTaxOnBaseFarePlusYQ = Convert.ToBoolean(data["isServiceTaxOnBaseFarePlusYQ"]);
            }
            else
            {
                new ArgumentException("This price id is not present in database");
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("PriceAccounts.Load exiting :");
        }

        public Decimal GetAgentPrice()
        {
            decimal agentPrice = 0;
            if (accPriceType == PriceType.PublishedFare)
            {
                agentPrice = (publishedFare + airlineTransFee + serviceTax + tax + tdsCommission + tdsPLB + otherCharges - agentPLB - agentCommission + transactionFee + reverseHandlingCharge);
            }
            else if (accPriceType == PriceType.NetFare)
            {
                agentPrice = (netFare + markup + serviceTax + tax + otherCharges);
            }
            return Math.Round(agentPrice, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
        }

        public Decimal GetServiceAgentPrice()
        {
            decimal agentPrice = 0;
            if (accPriceType == PriceType.PublishedFare)
            {
                agentPrice = (publishedFare + airlineTransFee + serviceTax + tax + otherCharges + transactionFee);
            }
            else if (accPriceType == PriceType.NetFare)
            {
                agentPrice = (netFare + markup + serviceTax + tax + otherCharges);
            }
            return Math.Round(agentPrice, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
        }

        public decimal GetOfferdPrice()
        {
            decimal offPrice = 0;
            if (accPriceType == PriceType.PublishedFare)
            {
                offPrice = (publishedFare + airlineTransFee + tax - agentPLB - agentCommission + transactionFee);
            }
            else if (accPriceType == PriceType.NetFare)
            {
                offPrice = (netFare + markup + tax);
            }
            return offPrice;
        }

        public Decimal GetOfferedPriceWL()
        {
            decimal offPrice = 0;
            if (accPriceType == PriceType.PublishedFare)
            {
                offPrice = (publishedFare + airlineTransFee + tax - whiteLabelDiscount);
            }
            else if (accPriceType == PriceType.NetFare)
            {
                offPrice = (netFare + markup + tax); //TODO: WhiteLabelDiscount
            }
            return offPrice;
        }
        /// <summary>
        /// Gets the list of price id's for all  the  unpaid invoices in the current fortnight
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static List<int> GetPriceIdListOfUnpaidInvoicesInCurrentFortnight(int agencyId)
        {
            Trace.TraceInformation("Price.GetPriceIdListOfUnpaidInvoicesInCurrentFortnight entered agencyId=" + agencyId);
            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
            }
            List<int> priceList = new List<int>();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetPriceIdOfUnpaidInvoicesInCurrentFortnight, paramList, connection);
            while (data.Read())
            {
                priceList.Add(Convert.ToInt32(data["priceId"]));
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("Price.GetPriceIdListOfUnpaidInvoicesInCurrentFortnight exiting");
            return priceList;
        }
        /// <summary>
        /// Gets the total unpaid amount against raised and partially paid invoices of the current fortnight
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static decimal GetTotalAmountOfUnpaidInvoicesInCurrentFortnight(int agencyId)
        {
            Trace.TraceInformation("Price.GetTotalAmountOfUnpaidInvoicesInCurrentFortnight entered agencyId=" + agencyId);
            if (agencyId <= 0)
            {
                throw new ArgumentException("agencyId must have a positive non zero integer value", "agencyId");
            }
            decimal totalAmount = 0;
            List<int> priceList = GetPriceIdListOfUnpaidInvoicesInCurrentFortnight(agencyId);
            string commaSeperatedPriceIdList = string.Empty;
            foreach (int priceId in priceList)
            {
                commaSeperatedPriceIdList = commaSeperatedPriceIdList + ", " + priceId;
            }
            if (commaSeperatedPriceIdList == string.Empty)
            {
                return 0;
            }
            commaSeperatedPriceIdList = commaSeperatedPriceIdList.TrimStart(',');
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@priceIdList", commaSeperatedPriceIdList);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetPriceForPriceIdList, paramList, connection);
            while (data.Read())
            {
                PriceAccounts price = new PriceAccounts();
                price.publishedFare = Convert.ToDecimal(data["publishedFare"]);
                price.netFare = Convert.ToDecimal(data["netFare"]);
                price.tax = Convert.ToDecimal(data["tax"]);
                price.markup = Convert.ToDecimal(data["markup"]);
                price.ourCommission = Convert.ToDecimal(data["ourCommission"]);
                price.ourPLB = Convert.ToDecimal(data["ourPLB"]);
                price.agentCommission = Convert.ToDecimal(data["agentCommission"]);
                price.agentPLB = Convert.ToDecimal(data["agentPLB"]);
                price.serviceTax = Convert.ToDecimal(data["serviceTax"]);
                if (data["tdsCommission"] != DBNull.Value)
                {
                    price.tdsCommission = Convert.ToDecimal(data["tdsCommission"]);
                }
                if (data["tdsPLB"] != DBNull.Value)
                {
                    price.tdsPLB = Convert.ToDecimal(data["tdsPLB"]);
                }
                price.otherCharges = Convert.ToDecimal(data["otherCharges"]);
                price.whiteLabelDiscount = Convert.ToDecimal(data["whiteLabelDiscount"]);
                price.transactionFee = Convert.ToDecimal(data["transactionFee"]);
                price.accPriceType = (PriceType)data["priceType"];
                price.currency = data["currency"].ToString();
                if (data["rateofExchange"] != DBNull.Value)
                {
                    price.rateOfExchange = Convert.ToDecimal(data["rateofExchange"]);
                }
                if (data["currencyCode"] != DBNull.Value)
                {
                    price.currencyCode = data["currencyCode"].ToString();
                }
                price.additionalTxnFee = Convert.ToDecimal(data["additionalTxnFee"]);
                price.wlCharge = Convert.ToDecimal(data["wlCharge"]);
                totalAmount = totalAmount + price.GetAgentPrice();
            }
            totalAmount = totalAmount - Invoice.GetAmountAgainstPartialInvoicesOfCurrentFortnight(agencyId);
            data.Close();
            connection.Close();
            Trace.TraceInformation("Price.GetTotalAmountOfUnpaidInvoicesInCurrentFortnight exiting");
            return totalAmount;
        }
        public decimal GetTrainTicketPrice()
        {
            decimal totalPrice = publishedFare - discount + markup + transactionFee;
            foreach (ChargeBreakUp breakUp in ChargeBU)
            {
                totalPrice += breakUp.Amount;
            }
            return totalPrice;
        }
        /// <summary>
        /// Gets the array of IRCTC fare 
        /// 0 -> ourPrice
        /// 1 -> irctcPrice
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static decimal[] GetIRCTCFare(IRCTCFareClass fareClass, int passengerCount, int agencyId)
        {
            Trace.TraceInformation("Price.GetIRCTCFare entered ");
            if (passengerCount <= 0 || passengerCount > 6)
            {
                throw new ArgumentException("Passenger count must be between 1 and 6");
            }
            decimal[] irctcFare = new decimal[2];
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@AgencyId", agencyId);
            paramList[1] = new SqlParameter("@class", (int)fareClass);
            paramList[2] = new SqlParameter("@passengerCount", passengerCount);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetIRCTCPriceForAgent, paramList, connection);
            if (data.Read())
            {
                irctcFare[0] = Convert.ToInt32(data["ourPrice"]);
                irctcFare[1] = Convert.ToInt32(data["irctcPrice"]);
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("Price.GetIRCTCFare entered");
            return irctcFare;
        }

        /// <summary>
        /// To get insurance price for agent
        /// </summary>
        /// <returns></returns>
        public decimal GetAgentInsurancePrice()
        {
            decimal agentPrice = 0;
            if (accPriceType == PriceType.PublishedFare)
            {
                agentPrice = (publishedFare + serviceTax - agentCommission);
            }
            return Math.Round(agentPrice, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ));
        }

        /// <summary>
        /// To get list of price of More than one passenger 
        /// </summary>
        /// <returns></returns>
        public static List<PriceAccounts> GetPriceList(string priceIdString)
        {
            Trace.TraceInformation("Price.GetPriceList entered priceIdString=" + priceIdString);
            if (priceIdString == "")
            {
                throw new ArgumentException("PriceIdString must have atleast one priceid", "priceIdString");
            }
            List<PriceAccounts> priceList = new List<PriceAccounts>();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@priceId", priceIdString);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetPriceList, paramList, connection);
            while (data.Read())
            {
                PriceAccounts price = new PriceAccounts();
                price = ReadDataReader(price, data);
                priceList.Add(price);
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("Price.GetPriceList  exited");
            return priceList;
        }

        public static PriceAccounts ReadDataReader(PriceAccounts price, SqlDataReader data)
        {
                price.PriceId = Convert.ToInt32(data["priceId"]);
                price.publishedFare = Convert.ToDecimal(data["publishedFare"]);
                price.netFare = Convert.ToDecimal(data["netFare"]);
                price.tax = Convert.ToDecimal(data["tax"]);
                price.markup = Convert.ToDecimal(data["markup"]);
                price.ourCommission = Convert.ToDecimal(data["ourCommission"]);
                price.ourPLB = Convert.ToDecimal(data["ourPLB"]);
                price.agentCommission = Convert.ToDecimal(data["agentCommission"]);
                price.agentPLB = Convert.ToDecimal(data["agentPLB"]);
                price.serviceTax = Convert.ToDecimal(data["serviceTax"]);
                price.tdsRate = Convert.ToDecimal(data["tdsRate"]);
                //tds = Convert.ToDecimal(data["tds"]);
                if (data["tdsCommission"] != DBNull.Value)
                {
                    price.tdsCommission = Convert.ToDecimal(data["tdsCommission"]);
                }
                if (data["tdsPLB"] != DBNull.Value)
                {
                    price.tdsPLB = Convert.ToDecimal(data["tdsPLB"]);
                }
                price.otherCharges = Convert.ToDecimal(data["otherCharges"]);
                price.whiteLabelDiscount = Convert.ToDecimal(data["whiteLabelDiscount"]);
                price.transactionFee = Convert.ToDecimal(data["transactionFee"]);
                price.accPriceType = (PriceType)data["priceType"];

                price.currency = data["currency"].ToString();
                if (data["rateofExchange"] != DBNull.Value)
                {
                    price.rateOfExchange = Convert.ToDecimal(data["rateofExchange"]);
                }
                if (data["currencyCode"] != DBNull.Value)
                {
                    price.currencyCode = data["currencyCode"].ToString();
                }
                price.additionalTxnFee = Convert.ToDecimal(data["additionalTxnFee"]);
                price.wlCharge = Convert.ToDecimal(data["wlCharge"]);
                price.discount = Convert.ToDecimal(data["discount"]);
                if (data["commissionType"] != DBNull.Value)
                {
                    price.commissionType = data["commissionType"].ToString();
                }
                if (data["reverseHandlingCharge"] != DBNull.Value)
                {
                    price.reverseHandlingCharge = Convert.ToDecimal(data["reverseHandlingCharge"]);
                }
                if (data["yqTax"] != DBNull.Value)
                {
                    price.yqTax = Convert.ToDecimal(data["yqTax"]);
                }
                price.isServiceTaxOnBaseFarePlusYQ = Convert.ToBoolean(data["isServiceTaxOnBaseFarePlusYQ"]);
                ChargeBreakUp cbu = new ChargeBreakUp();
                price.chargeBU = cbu.Load(price.priceId);            
                return price;
        }
    }
}
