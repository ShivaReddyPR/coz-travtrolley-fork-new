using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;


namespace Technology.BookingEngine
{
   public enum TransferType
   {
       pvtTransfer=0,
       shared=1
   }
   public class TransferStaticData
    {
       private string itemCode;
       private string cityCode;
       private string transferTime;
       private string description;
       private string meetingPoint;
       private TransferType type;
       private TransferBookingSource source;
       private string conditions;
       private string flashLinks;
       private DateTime timeStamp;

        /// <summary>
        /// Item Code
        /// </summary>
        public string ItemCode
        {
            get { return itemCode; }
            set { itemCode = value; }
        }
        /// <summary>
        /// City Code
        /// </summary>
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        /// <summary>
        /// Approximate Transfer Time
        /// </summary>
        public string TransferTime
        {
            get { return transferTime; }
            set { transferTime = value; }
        }
        /// <summary>
        /// Description about the Transfer
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        /// <summary>
        /// Meeting Point Information
        /// </summary>
        public string MeetingPoint
        {
            get { return meetingPoint; }
            set { meetingPoint = value; }
        }
        /// <summary>
        /// Type of Transfer
        /// </summary>
        public TransferType Type
        {
            get { return type; }
            set { type = value; }
        }
        /// <summary>
        /// Source of the Transfer
        /// </summary>
        public TransferBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        /// <summary>
        /// Transfer Conditions
        /// </summary>
        public string Conditions
        {
            get { return conditions; }
            set { conditions = value; }
        }
        /// <summary>
        /// Falsh Links of the Transfer
        /// </summary>
        public string FlashLinks
        {
            get { return flashLinks; }
            set { flashLinks = value; }
        }
        /// <summary>
        /// Time stamp of the static Data
        /// </summary>
        public DateTime TimeStamp
        {
            get { return timeStamp; }
            set { timeStamp = value; }
        }
    
#region Methods
       /// <summary>
       /// This Method is used to Load the Transfer Details.
       /// </summary>
       /// <param name="itemCode"></param>
       /// <param name="cityCode"></param>
       /// <param name="hSource"></param>
       public void Load(string itCode, string ctyCode, TransferBookingSource tSource)
       {
           Trace.TraceInformation("TransferStaticData.Load entered : hotelCode = " + itemCode + "HotelSource = " + tSource.ToString());
           if (itemCode != null && itemCode.Length == 0)
           {
               throw new ArgumentException("ItemCode Should be enterd !", "hotelCode");
           }
           SqlConnection connection = Dal.GetConnection();
           SqlParameter[] paramList = new SqlParameter[3];
           paramList[0] = new SqlParameter("@itemCode", itCode);
           paramList[1] = new SqlParameter("@cityCode", ctyCode);
           paramList[2] = new SqlParameter("@source", (int)tSource);
           SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetTransferStaticDataByCode, paramList, connection);
           List<HotelRoom> hotelRooms = new List<HotelRoom>();
           if (data.Read())
           {
               itemCode = itCode;
               cityCode = ctyCode;
               source = (TransferBookingSource)Convert.ToInt16(data["source"]);
               transferTime = data["transferTime"].ToString();
               description = data["description"].ToString();
               meetingPoint = data["meetingPoint"].ToString();
               conditions = data["condition"].ToString();
               type = (TransferType)Convert.ToInt16(data["type"]);
               flashLinks = data["flashLink"].ToString();
               timeStamp = Convert.ToDateTime(data["timeStamp"]);
           }
           data.Close();
           connection.Close();
           Trace.TraceInformation("HotelStaticData.Load exit :");
       }
       /// <summary>
       /// This Method is used to Save the static Details.
       /// </summary>
       public void Save()
       {
           Trace.TraceInformation("TransferStaticData.Save entered.");
           SqlParameter[] paramList = new SqlParameter[9];
           paramList[0] = new SqlParameter("@itemCode", itemCode);
           paramList[1] = new SqlParameter("@cityCode", cityCode);
           paramList[2] = new SqlParameter("@source", source);
           paramList[3] = new SqlParameter("@transfertime", transferTime);
           paramList[4] = new SqlParameter("@description", description);
           paramList[5] = new SqlParameter("@meetingPoint", meetingPoint);
           paramList[6] = new SqlParameter("@condition", conditions);
           paramList[7] = new SqlParameter("@type", type);
           paramList[8] = new SqlParameter("@flashLink", flashLinks);
           int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddTransferStaticData, paramList);
           Trace.TraceInformation("TransferStaticData.Save exiting");
       }
       /// <summary>
       /// This Method is used to Update the Static Data
       /// </summary>
       public void Update()
       {
           Trace.TraceInformation("TransferStaticData.Update entered.");
           SqlParameter[] paramList = new SqlParameter[9];
           paramList[0] = new SqlParameter("@itemCode", itemCode);
           paramList[1] = new SqlParameter("@cityCode", cityCode);
           paramList[2] = new SqlParameter("@source", source);
           paramList[3] = new SqlParameter("@transfertime", transferTime);
           paramList[4] = new SqlParameter("@description", description);
           paramList[5] = new SqlParameter("@meetingPoint", meetingPoint);
           paramList[6] = new SqlParameter("@condition", conditions);
           paramList[7] = new SqlParameter("@type", type);
           paramList[8] = new SqlParameter("@flashLink", flashLinks);
           int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateTransferStaticData, paramList);
           Trace.TraceInformation("TransferStaticData.Update exiting");

       }
#endregion
    }
}
