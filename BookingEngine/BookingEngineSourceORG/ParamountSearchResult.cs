using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace Technology.BookingEngine
{
    public class ParamountSearchResult : SearchResult
    {
        string fareCode;
        string depBookResult;
        string retBookResult = string.Empty;
        string viewstate;
        CookieContainer cookieContainerObject;

        public string FareCode
        {
            get
            {
                return fareCode;
            }
            set
            {
                fareCode = value;
            }
        }
        public string DepBookResult
        {
            get
            {
                return depBookResult;
            }
            set
            {
                depBookResult = value;
            }
        }
        public string RetBookResult
        {
            get
            {
                return retBookResult;
            }
            set
            {
                retBookResult = value;
            }
        }
        public string Viewstate
        {
            get
            {
                return viewstate;
            }
            set
            {
                viewstate = value;
            }
        }
        public CookieContainer CookieContainerObject
        {
            get
            {
                return cookieContainerObject;
            }
            set
            {
                cookieContainerObject = value;
            }
        }
    }
}
