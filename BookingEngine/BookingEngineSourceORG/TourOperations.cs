using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;

namespace Technology.BookingEngine
{
    public class TourOperations
    {
        private string itemCode;
        private string cityCode;
        private string language;
        private string commentary;
        private DateTime fromDate;
        private DateTime toDate;
        private string frequency;
        private string depTime;
        private string depCode;
        private string depName;
        private string fromTime;
        private string toTime;
        private string interval;
        private string telephone;
        private string address;
        public string ItemCode
        {
            get { return itemCode; }
            set { itemCode = value; }
        }
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        public string Language
        {
            get { return language; }
            set { language = value; }
        }
        public string Commentary
        {
            get { return commentary; }
            set { commentary = value; }
        }
        public DateTime FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }
        public DateTime ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }
        public string Frequency
        {
            get { return frequency; }
            set { frequency = value; }
        }
        public string DepCode
        {
            get { return depCode; }
            set { depCode = value; }
        }
        public string DepTime
        {
            get { return depTime; }
            set { depTime = value; }
        }
        public string DepName
        {
            get { return depName; }
            set { depName = value; }
        }
         public string FromTime
        {
            get { return fromTime; }
            set { fromTime = value; }
        }
        public string ToTime
        {
            get { return toTime; }
            set { toTime = value; }
        }
        public string Interval
        {
            get { return interval; }
            set { interval= value; }
        }
        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        /// <summary>
        /// This Method is used to save the Tour Operations.
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("TourOperations.Save entered.");
            SqlParameter[] paramList = new SqlParameter[15];
            paramList[0] = new SqlParameter("@itemCode", itemCode);
            paramList[1] = new SqlParameter("@cityCode", cityCode);
            paramList[2] = new SqlParameter("@language", language);
            paramList[3] = new SqlParameter("@commentary", commentary);
            paramList[4] = new SqlParameter("@fromDate", fromDate);
            paramList[5] = new SqlParameter("@toDate", toDate);
            paramList[6] = new SqlParameter("@frequency", frequency);
            paramList[7] = new SqlParameter("@depTime", depTime);
            paramList[8] = new SqlParameter("@depCode", depCode);
            paramList[9] = new SqlParameter("@depName", depName);
            paramList[10] = new SqlParameter("@fromTime", fromTime);
            paramList[11] = new SqlParameter("@toTime", toTime);
            paramList[12] = new SqlParameter("@interval", interval);
            paramList[13] = new SqlParameter("@telephone", telephone);
            paramList[14]=new SqlParameter("@address",address);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddTourOperations, paramList);
            Trace.TraceInformation("TourOperations.Save exiting");
        }
        public void Delete()
        {
            Trace.TraceInformation("TourOperations.Delete entered.");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@itemCode", itemCode);
            paramList[1] = new SqlParameter("@cityCode", cityCode);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.DeleteTourOperations, paramList);
            Trace.TraceInformation("TourOperations.Delete exiting");
        }
        public List<TourOperations> Load(string ctyCode, string itmCode)
        {
            Trace.TraceInformation("TourOperations.Load entered : itemCode = " + itmCode );
            if (itmCode != null && itmCode.Length == 0)
            {
                throw new ArgumentException("ItemCode Should be enterd !", "");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@itemCode", itmCode);
            paramList[1] = new SqlParameter("@cityCode", ctyCode);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetTourOperations, paramList, connection);
            List<TourOperations> tourOperations = new List<TourOperations>();
            while (data.Read())
            {
                TourOperations tourInfo = new TourOperations();
                tourInfo.CityCode = ctyCode;
                tourInfo.ItemCode = itmCode;
                tourInfo.FromDate = Convert.ToDateTime(data["fromDate"]);
                tourInfo.ToDate = Convert.ToDateTime(data["toDate"]);
                tourInfo.Language = data["language"].ToString();
                tourInfo.Frequency = data["frequency"].ToString();
                tourInfo.Commentary = data["commentary"].ToString();
                tourInfo.DepName = data["depName"].ToString();
                tourInfo.DepCode = data["depCode"].ToString();
                tourInfo.DepTime = data["depTime"].ToString();
                tourInfo.FromTime = data["fromTime"].ToString();
                tourInfo.ToTime = data["toTime"].ToString();
                tourInfo.Interval= data["interval"].ToString();
                tourInfo.Address = data["address"].ToString();
                tourInfo.Telephone = data["telephone"].ToString();
                tourOperations.Add(tourInfo);
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("TourOperations.Load exit :");
            return tourOperations;
        }


    }
}
