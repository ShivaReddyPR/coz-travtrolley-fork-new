using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Diagnostics;
using Technology.Data;
using System.Data.SqlClient;
using System.Data;
using Technology.BookingEngine;
using System.Transactions;
using Technology.Configuration;
using CoreLogic;
using System.IO;

namespace Technology.BookingEngine
{
    public class TrainItinerary
    {
        private int bookingId;
        private string pnr;
        private string origin;
        private string destination;
        private string trainName;
        private string trainNumber;
        private DateTime travelDate;
        private int agencyId;
        private  short passengerCount;
        private string transactionId;
        private DateTime dateOfBoarding;
        private string boardingStation;
        private string bookingClass;
        private decimal distance;
        private TrainBookingstatus status;
        private TrainQuota quota;
        private DateTime createdOn;
        private int createdBy;
        private DateTime lastModifiedOn;
        private int lastModifiedBy;
        private string ticketNumber;
        private string slipRouteMessage;
        private List<TrainPassenger> passengerList;
        private List<TrainPassenger> infantList;
        private Boolean hasClassUpgrade;
        private static Dictionary<string, string> irctcClass;
        /// <summary>
        /// 
        /// </summary>
        /// 
        public TrainItinerary()
        {
            infantList = new List<TrainPassenger>();
        }

        public static Dictionary<string, string> IRCTCClass
        {
            get
            {
                if (irctcClass == null)
                {
                    irctcClass = new Dictionary<string, string>();
                    SqlParameter[] paramList = new SqlParameter[0];
                    using (SqlConnection connection = Dal.GetConnection())
                    {
                        using (SqlDataReader reader = Dal.ExecuteReaderSP("usp_GetIRCTCClass", paramList, connection))
                        {
                            while (reader.Read())
                            {
                                irctcClass.Add(Convert.ToString(reader["classId"]), Convert.ToString(reader["className"]));
                            }
                            reader.Close();
                        }
                    }
                }
                return irctcClass;                           
            }        
        }
        public string SlipRouteMessage
        {
            get
            {
                return slipRouteMessage;
            }
            set
            {
                slipRouteMessage = value;
            }
        }

        /// <summary>
        /// Unique id for each booking
        /// </summary>
        public int BookingId
        {
            get
            {
                return bookingId;

            }
            set
            {
                bookingId = value;

            }
        }
        /// <summary>
        /// Pnr of the booking
        /// </summary>
        public string Pnr
        {
            get
            {
                return pnr;

            }
            set
            {
                pnr = value;

            }
        }
        /// <summary>
        /// origin station code
        /// </summary>
        public string Origin
        {
            get
            {
                return origin;

            }
            set
            {
                origin = value;

            }
        }
        /// <summary>
        /// Destination station code
        /// </summary>
        public string Destination
        {
            get
            {
                return destination;

            }
            set
            {
                destination = value;

            }
        }
        /// <summary>
        /// Name of the train 
        /// </summary>
        public string TrainName
        {
            get
            {
                return trainName;

            }
            set
            {
                trainName = value;

            }
        }
        /// <summary>
        /// Number of the train
        /// </summary>
        public string TrainNumber
        {
            get
            {
                return trainNumber;

            }
            set
            {
                trainNumber = value;

            }
        }
        /// <summary>
        /// Date of travel
        /// </summary>
        public DateTime TravelDate
        {
            get
            {
                return travelDate;

            }
            set
            {
                travelDate = value;

            }
        }
        /// <summary>
        /// Agency which is making the booking
        /// </summary>
        public int AgencyId
        {
            get
            {
                return agencyId;

            }
            set
            {
                agencyId = value;

            }
        }
        /// <summary>
        /// No of passengers in the booking
        /// </summary>
        public  short PassengerCount
        {
            get
            {
                return passengerCount;

            }
            set
            {
                passengerCount = value;

            }
        }
        /// <summary>
        /// unique id for each transaction provided by us to IRCTC
        /// </summary>
        public string TransactionId
        {
            get
            {
                return transactionId;

            }
            set
            {
                transactionId = value;

            }
        }
        /// <summary>
        /// Date on which passenger will board the train
        /// </summary>
        public DateTime DateOfBoarding
        {
            get
            {
                return dateOfBoarding;

            }
            set
            {
                dateOfBoarding = value;

            }
        }
        /// <summary>
        /// Station at which boarding is done
        /// </summary>
        public string BoardingStation
        {
            get
            {
                return boardingStation;

            }
            set
            {
                boardingStation = value;

            }
        }
        /// <summary>
        /// class of the booking
        /// </summary>
        public string BookingClass
        {
            get
            {
                return bookingClass;

            }
            set
            {
                bookingClass = value;

            }
        }
        /// <summary>
        /// Distance between origin and destination
        /// </summary>
        public decimal Distance
        {
            get
            {
                return distance;

            }
            set
            {
                distance = value;

            }
        }
        /// <summary>
        /// status of the booking
        /// </summary>
        public TrainBookingstatus Status
        {
            get
            {
                return status;

            }
            set
            {
                status = value;

            }
        }
        /// <summary>
        /// Quota under which booking is done
        /// </summary>
        public TrainQuota Quota
        {
            get
            {
                return quota;

            }
            set
            {
                quota = value;

            }
        }
        /// <summary>
        /// Booking creation date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;

            }
            set
            {
                createdOn = value;

            }
        }
        /// <summary>
        /// Member id of the member who has done the booking
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        /// <summary>
        /// Date on which booking was modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        /// <summary>
        /// Member id of the member who has modified the booking
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;

            }
            set
            {
                lastModifiedBy = value;

            }
        }
        public string TicketNumber
        {
            get
            {
                return ticketNumber;

            }
            set
            {
                ticketNumber = value;

            }
        }
         public List<TrainPassenger> PassengerList
         {
            get
            {
                if (passengerList == null)
                {
                    passengerList = TrainPassenger.GetPassengerListByBookingId(bookingId,ref infantList);
                }
                return passengerList;
               
            }
            set
            {
                passengerList = value;
            }         
         }
        public Boolean HasClassUpgrade
        {
            get
            {
                return hasClassUpgrade;

            }
            set
            {
                hasClassUpgrade = value;
            }
        }

        public List<TrainPassenger> InfantList
        {
            get
            {
                if (infantList == null)
                {
                    passengerList = TrainPassenger.GetPassengerListByBookingId(bookingId,ref infantList);
                }
                return infantList;

            }
            set
            {
                infantList = value;
            }
        }
        /// <summary>
        /// Adds or updates a entry in train itinerary table
        /// </summary>
        /// <returns></returns>
        public int Save()
        {
            Trace.TraceInformation("TrainItinerary.save entered");
            int rowsAffected = 0;
            ValidateData();
            SqlParameter[] paramList  = new SqlParameter[21];
            paramList[0] = new SqlParameter("@bookingId",bookingId);
            paramList[1] = new SqlParameter("@pnr",pnr);
            paramList[2] = new SqlParameter("@origin",origin);
            paramList[3] = new SqlParameter("@destination",destination);
            paramList[4] = new SqlParameter("@trainName",trainName);
            paramList[5] = new SqlParameter("@trainNumber",trainNumber);
            paramList[6] = new SqlParameter("@travelDate",travelDate);
            paramList[7] = new SqlParameter("@agencyId",agencyId);
            paramList[8] = new SqlParameter("@passengerCount",passengerCount);
            paramList[9] = new SqlParameter("@transactionId",transactionId);
            paramList[10] = new SqlParameter("@dateOfBoarding",dateOfBoarding);
            paramList[11] = new SqlParameter("@boardingStation",boardingStation);
            paramList[12] = new SqlParameter("@bookingClass",bookingClass);
            paramList[13] = new SqlParameter("@distance",distance);
            paramList[14] = new SqlParameter("@status",(int)status);
            paramList[15] = new SqlParameter("@quota",(int)quota);
            paramList[16] = new SqlParameter("@createdBy",createdBy);
            paramList[17] = new SqlParameter("@lastModifiedBy",LastModifiedBy);
            paramList[18] = new SqlParameter("@ticketNumber", ticketNumber);
            paramList[19] = new SqlParameter("@slipRouteMessage", slipRouteMessage);
            paramList[20] = new SqlParameter("@hasClassUpgrade", hasClassUpgrade);
            using (TransactionScope updateTransaction = new TransactionScope())
            {
                if (bookingId > 0)
                {
                    rowsAffected = Dal.ExecuteNonQuerySP("usp_UpdateTrainItinerary", paramList);
                }
                else
                {
                    paramList[0].Direction = ParameterDirection.Output;
                    rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddTrainItinerary, paramList);
                    bookingId = Convert.ToInt32(paramList[0].Value);
                }
                foreach (TrainPassenger passenger in passengerList)
                {
                    passenger.BookingId = bookingId;
                    passenger.Save();
                }
                foreach (TrainPassenger passenger in infantList)
                {
                    passenger.BookingId = bookingId;
                    passenger.Save();
                }
                updateTransaction.Complete();
            }
            Trace.TraceInformation("TrainItinerary.save exited");
            return rowsAffected;
        }
        /// <summary>
        /// This function validates all the inputs that are to be stored
        /// </summary>
        private void ValidateData()
        {
            if (pnr == null )
            {
                throw new ArgumentException("Pnr number cannot be null ");
            }
            if (origin == null || origin.Trim().Length == 0)
            {
                throw new ArgumentException("Origin cannot null or empty");
            }
            if (destination == null || destination.Trim().Length == 0)
            {
                throw new ArgumentException("Destination cannot be null or empty");
            }
            if (trainName == null )
            {
                throw new ArgumentException("Train Name cannot be null or empty");
            }
            if (trainNumber == null || trainNumber.Trim().Length == 0)
            {
                throw new ArgumentException("Train Number cannot be null or empty");
            }
            if (travelDate == null || travelDate == new DateTime())
            {
                throw new ArgumentException("Travel Date cannot be null or empty");
            }
            if (agencyId == 0)
            {
                throw new ArgumentException("Agency Id cannot be zero");
            }
            if (passengerCount == 0)
            {
                throw new ArgumentException("Passenger Count cannot be zero");
            }
            if (transactionId == null || transactionId.Trim().Length == 0)
            {
                throw new ArgumentException("Travel Date cannot be null or empty");
            }          
            if (bookingClass == null || bookingClass.Trim().Length == 0)
            {
                throw new ArgumentException("Booking Class cannot be null or empty");
            }
            if (boardingStation == null || boardingStation.Trim().Length == 0)
            {
                throw new ArgumentException("Boarding Station cannot be null or empty");
            }
            if ((int)status == 0 )
            {
                throw new ArgumentException("Status cannot be null ");
            }
            if ((int)Quota == 0)
            {
                throw new ArgumentException("Quota cannot be null ");
            }
            if (createdBy == 0)
            {
                throw new ArgumentException("Created by cannot be zero");
            }
            if(passengerList == null || passengerList.Count == 0)
            {
                throw new ArgumentException("Passenger List cannot be null or empty");
            }
             
           
        }
        public void Load(int trainBookingId)
        {
            Trace.TraceInformation("TrainItinerary.Load entered bookingId = " + trainBookingId);
            if (trainBookingId == 0)
            {
                throw new ArgumentException("Train booking id cannot be zero");
            }
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@bookingId",trainBookingId);
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlDataReader reader = Dal.ExecuteReaderSP(SPNames.GetTrainItinerary, paramList, connection);
                ReadDataReader(reader);
                reader.Close();
            }            
            Trace.TraceInformation("TrainItinerary.Load exited");
        }
        /// <summary>
        /// Reads a data reader
        /// </summary>
        /// <param name="reader"></param>
        private void ReadDataReader(SqlDataReader reader)
        {
            if (reader.Read())
            {
                 
                bookingId = Convert.ToInt32(reader["bookingId"]);
                pnr = Convert.ToString(reader["pnr"]);
                origin = Convert.ToString(reader["origin"]);
                destination = Convert.ToString(reader["destination"]);
                trainName = Convert.ToString(reader["trainName"]);
                trainNumber = Convert.ToString(reader["trainNumber"]);
                travelDate = Convert.ToDateTime(reader["travelDate"]);
                agencyId = Convert.ToInt32(reader["agencyId"]);
                passengerCount = Convert.ToInt16(reader["passengerCount"]);
                transactionId = Convert.ToString(reader["transactionId"]);
                dateOfBoarding = Convert.ToDateTime(reader["dateOfBoarding"]);
                boardingStation = Convert.ToString(reader["boardingStation"]);
                bookingClass = Convert.ToString(reader["bookingClass"]);
                if (reader["distance"] != DBNull.Value)
                {
                    distance = Convert.ToInt32(reader["distance"]);
                }
                status = (TrainBookingstatus)(Convert.ToInt32(reader["status"]));
                quota = (TrainQuota)Convert.ToInt32(reader["quota"]);
                createdOn = Convert.ToDateTime(reader["createdOn"]);
                createdBy = Convert.ToInt32(reader["createdBy"]);
                lastModifiedOn = Convert.ToDateTime(reader["lastModifiedOn"]);
                lastModifiedBy = Convert.ToInt32(reader["lastModifiedBy"]);
                ticketNumber = reader["ticketNumber"].ToString();
                if (reader["slipRouteMessage"] != DBNull.Value)
                {
                    slipRouteMessage = Convert.ToString(reader["slipRouteMessage"]);
                }
                hasClassUpgrade = Convert.ToBoolean(reader["hasClassUpgrade"]);
                passengerList = TrainPassenger.GetPassengerListByBookingId(bookingId,ref infantList);
            }
        }
        public static string GetUniqueTrainSequenceNumber()
        {
            Trace.TraceInformation("TrainItinerary.GetUniqueTrainSequenceNumber entered");
            string uniqueSequenceNumber = "";
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@key", "Train"); ;
            using (SqlConnection con = Dal.GetConnection())
            {
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetUniqueSequenceNumber, paramList, con);
                if (data.Read())
                {
                    uniqueSequenceNumber = Convert.ToString(data["value"]);
                }
                data.Close();
            }
            uniqueSequenceNumber = uniqueSequenceNumber.PadLeft(10, '0');
            Trace.TraceInformation("TrainItinerary.GetUniqueTrainSequenceNumber exited");
            return uniqueSequenceNumber;
        }
        public static bool CheckTrainSectorForInprogress(string origin, string destination, int agencyId)
        {
            Trace.TraceInformation("TrainItinerary.CheckTrainSectorForInprogress entered");
            bool sectorInProgress = false;
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@status", TrainBookingstatus.Inprogress);
            paramList[1] = new SqlParameter("@origin", origin);
            paramList[2] = new SqlParameter("@destination", destination);
            paramList[3] = new SqlParameter("@agencyId", agencyId);
            using (SqlConnection con = Dal.GetConnection())
            {
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.CheckTrainSectorForInprogress, paramList, con);
                if (data.Read())
                {
                    sectorInProgress = true;
                }
                data.Close();
            }
            Trace.TraceInformation("TrainItinerary.CheckTrainSectorForInprogress exited");
            return sectorInProgress;
        }
        public static List<int> GetInProgressTrainBookingsIDForDate(DateTime date)
        {
            Trace.TraceInformation("TrainItinerary.GetInProgressTrainBookingsIDForDate entered");
            List<int> inProgressIDList = new List<int>();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@status", TrainBookingstatus.Inprogress);
            paramList[1] = new SqlParameter("@date", date);
            using (SqlConnection con = Dal.GetConnection())
            {
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetInProgressTrainBookingIDsForDate, paramList, con);
                while(data.Read())
                {
                    int bookingId = Convert.ToInt32(data["bookingId"]);
                    inProgressIDList.Add(bookingId);
                }
                data.Close();
                con.Close();
            }
            Trace.TraceInformation("TrainItinerary.GetInProgressTrainBookingsIDForDate exited");
            return inProgressIDList;
        }
        public static List<int> GetTraversedTrainBookingIDsInWaiting()
        {
            Trace.TraceInformation("TrainItinerary.GetTraversedTrainBookingIDsInWaiting entered");
            List<int> bookingIDList = new List<int>();
            DateTime fromDate = DateTime.ParseExact(Convert.ToString(ConfigurationSystem.IRCTCConfig["refundWaitingBookingsFromDate"]), "dd/MM/yyyy", null);
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@trainPassengerStatus",TrainPassengerStatus.Ticketed);
            paramList[1] = new SqlParameter("@waitingStringPattern", "WL");
            paramList[2] = new SqlParameter("@fromDate", fromDate);
            using (SqlConnection con = Dal.GetConnection())
            {
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetTraversedTrainBookingIDsInWaiting, paramList, con);
                while (data.Read())
                {
                    int bookingId = Convert.ToInt32(data["bookingId"]);
                    bookingIDList.Add(bookingId);
                }
                data.Close();
                con.Close();
            }
            Trace.TraceInformation("TrainItinerary.GetTraversedTrainBookingIDsInWaiting exited");
            return bookingIDList;
        }
        public static bool EmailTrainEticket(int memberId, int trainItineraryId,string csvEmailAddress,string filepath)
        {
            bool emailSend = false;
            try
            {
                Agency agency;
                string logo = string.Empty;
                string cityName = string.Empty;                
                string preferenceValue = string.Empty;
                string id = string.Empty;
                TrainItinerary itinerary = new TrainItinerary();
                itinerary.Load(trainItineraryId);
                BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(trainItineraryId, ProductType.Train));
                int agencyId = booking.AgencyId;
                agency = new Agency(agencyId);
                int bookedBy = Member.GetPrimaryMemberId(agency.AgencyId);
                if (agency.Address.CityId > 0)
                {
                    cityName = RegCity.GetCity(agency.Address.CityId).CityName;
                }
                else
                {
                    cityName = agency.Address.CityOther;
                }
                logo = Agency.GetAgencyLogo(agency.AgencyId);
                string phoneString = string.Empty;
                if (agency.Address.Phone != null && agency.Address.Phone.Length > 0)
                {
                    phoneString = agency.Address.Phone;
                }
                Member PrimaryMember = new Member();
                PrimaryMember.Load(Member.GetPrimaryMemberId(agency.AgencyId));
                //This is a global hash table which we send in Email.Send which replaces 
                //hashvariables with values(code which is not in loop)
                Hashtable globalHashTable = new Hashtable();
                globalHashTable.Add("TransactionId", itinerary.TransactionId);
                globalHashTable.Add("PNR", itinerary.Pnr);
                globalHashTable.Add("TrainNumber", itinerary.TrainNumber);
                globalHashTable.Add("TravelDate", itinerary.TravelDate.ToString("dd-MM-yyyy"));
                globalHashTable.Add("CreatedOn", (itinerary.CreatedOn + new TimeSpan(3, 30, 0)).ToString("dd-MMM-yyyy hh: mm: ss tt"));
                globalHashTable.Add("Origin", itinerary.Origin);
                globalHashTable.Add("Destination", itinerary.Destination);
                globalHashTable.Add("Distance", itinerary.Distance);
                globalHashTable.Add("BoardingStation", itinerary.BoardingStation);
                if (itinerary.Quota == Technology.BookingEngine.TrainQuota.CK)
                {
                    globalHashTable.Add("Quota", "Tatkal");
                }
                else
                {
                    globalHashTable.Add("Quota", "General");
                }
                globalHashTable.Add("DepTime", itinerary.DateOfBoarding.ToString("HH:mm "));
                globalHashTable.Add("boardingDate", itinerary.DateOfBoarding.ToString("dd-MM-yyyy"));

                globalHashTable.Add("AgencyName", agency.AgencyName);
                globalHashTable.Add("agencyAddress", agency.Address.Line1 + " " + agency.Address.Line2 + " , " + agency.Address.CityName);
                globalHashTable.Add("agencyPhone", agency.Address.Phone);
                globalHashTable.Add("agencyEmail", PrimaryMember.Email);

                int adultCount = 0, childCount = 0, seniorWomenCount = 0, seniorCount = 0;
                string adultCountString = string.Empty, childCountString = string.Empty, seniorWomenCountString = string.Empty, seniorCountString = string.Empty, totalCountString = string.Empty;
                decimal totalPrice = 0;
                decimal irctcCharge = 0;
                int counter = 0;
                string paxString = string.Empty;
                bool isFullWaitingList = true;
                bool hasWaitingList = false;
                foreach (TrainPassenger passenger in itinerary.PassengerList)
                {
                    counter++;
                    PriceAccounts price = new PriceAccounts();
                    price.Load(passenger.PriceId);
                    totalPrice += price.GetTrainTicketPrice() - price.TransactionFee - price.Markup;
                    irctcCharge += price.TransactionFee;
                    paxString += "<tr>";
                    paxString += "<td>" + counter + "</td>";
                    paxString += "<td>" + passenger.PaxName + "</td>";
                    paxString += "<td>" + passenger.Age + "</td>";
                    paxString += "<td>" + passenger.Gender.ToString() + "</td>";
                    if (passenger.PassengerStatus == TrainPassengerStatus.Ticketed)
                    {
                            paxString += "<td>" + passenger.TicketDescription.Replace(",", "/").TrimStart('/') + "</td>";
                    }
                    else
                    {
                        paxString += "<td>Cancelled</td>";
                    }
                    switch (passenger.PaxType)
                    {
                        case TrainPaxType.Adult:
                            adultCount++;
                            adultCountString = " Adult = " + adultCount.ToString();
                            break;
                        case TrainPaxType.Child:
                            childCount++;
                            childCountString = " Child = " + childCount.ToString();
                            break;
                        case TrainPaxType.SeniorCitizenMen:
                            adultCount++;
                            adultCountString = " Adult = " + adultCount.ToString();
                            break;
                        case TrainPaxType.SeniorCitizenWomen:
                            adultCount++;
                            adultCountString = " Adult = " + adultCount.ToString();
                            break;
                        default:
                            adultCount++;
                            adultCountString = " Adult = " + adultCount.ToString();
                            break;
                    }
                    if (!passenger.TicketDescription.Contains("WL"))
                    {
                        isFullWaitingList = false;
                    }
                    else
                    {
                        if (!hasWaitingList)
                        {
                            hasWaitingList = true;
                        }
                    }
                }
                string waitListString = "";
                string waitListStringTop = "";
                string waitListImage = "";
                string widthForSpan = "530px";
                if (hasWaitingList)
                {
                    waitListString = GetWaitlistString();
                    waitListStringTop = GetWaitListTopString();
                    waitListImage = "<img src=\"http://www.travelboutiqueonline.com/IRCTC/Images/wait_list.gif\" alt=\"logo\" />";
                    widthForSpan = "415px";
                }
                totalCountString = (adultCountString + childCountString + seniorWomenCountString + seniorCountString);
                globalHashTable.Add("totalCount", totalCountString);
                globalHashTable.Add("irctcCharge", Math.Round(irctcCharge, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                globalHashTable.Add("widthForSpan", widthForSpan);
                globalHashTable.Add("waitlistdata", waitListString);
                globalHashTable.Add("waitListDataTop", waitListStringTop);
                globalHashTable.Add("waitlistImage", waitListImage);
                globalHashTable.Add("Price", Math.Round(totalPrice, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] )));
                globalHashTable.Add("paxString", paxString);
                globalHashTable.Add("TrainName", itinerary.TrainName);
                globalHashTable.Add("BookingClass", itinerary.BookingClass);
                StreamReader sr = new StreamReader(filepath);
                string loopString = string.Empty;

                bool loopStarts = false;
                //string contains the code before loop 
                string startString = string.Empty;
                // string contains the code after loop
                string endString = string.Empty;
                bool loopEnds = false;
                #region seperate out the loop code and the other code
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (line.IndexOf("%loopEnds%") >= 0 || loopEnds)
                    {
                        loopEnds = true;
                        loopStarts = false;
                        if (line.IndexOf("%loopEnds%") >= 0)
                        {
                            line = sr.ReadLine();
                        }
                        endString += line;
                    }
                    if (line.IndexOf("%loopStarts%") >= 0 || loopStarts)
                    {
                        if (line.IndexOf("%loopStarts%") >= 0)
                        {
                            line = sr.ReadLine();
                        }
                        loopString += line;
                        loopStarts = true;
                    }
                    else
                    {
                        if (!loopEnds)
                        {
                            startString += line;
                        }
                    }
                }
                #endregion
                string fullString = startString;
                List<string> toArray = new List<string>();
                string[] addressArray = csvEmailAddress.Split(',');

                for (int k = 0; k < addressArray.Length; k++)
                {
                    toArray.Add(addressArray[k]);
                }
                string from = new Member(memberId).Email;
                try
                {
                    Email.Send(from, from, toArray, "Train E-Ticket", fullString, globalHashTable);
                    emailSend = false;
                }
                catch (System.Net.Mail.SmtpException)
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.Email, CoreLogic.Severity.Normal, 0, "Smtp is unable to send the message", "");
                }
            }
            catch (Exception ex)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.Email, CoreLogic.Severity.Normal, 0, "Unable to send mail for itineraryId = " + trainItineraryId + " by memberId = " + memberId + " message = " + ex.Message + " stack Trace = " + ex.StackTrace, "");
            }
            return emailSend;

        }
        
        private static string GetWaitlistString()
        {
            string waitListString = "<span style=\"text-align:left;width: 700px;\"><br/><br/><b>Rules for Waitlisted E tickets passengers</b></span><br /><br /><span><b>a) Status of E- ticket after chart preparation</b></span><div><ol style=\"margin: 5px 5px 0 15px;text-align: justify; width: 745px; list-style-position: inside;\"><li>Confirmed E ticket : Wait-listed E- ticket where all passengers are confirmed at the time of charting.</li><li>Partially waitlisted/Confirmed/RAC E ticket: Wait-listed E- ticket where some passengers are confirmed/RAC and others are wait-listed at the time of charting.</li><li>Fully waitlisted E tickets: Wait-listed E- ticket where all passengers continue to be waitlisted at the time of charting.</li></ol></div><span><b>b) Authorization to board the train</b></span><div><ol style=\"margin: 5px 5px 0 15px;text-align: justify; width: 745px; list-style-position: inside;\"><li>Passengers with Confirmed E ticket are permitted to board the train. Their names will appear on the chart.</li><li>Names of passengers with Partially Waitlisted /Confirmed/RAC will appear on the chart (including the Waitlisted passengers).</li><li>PNRs having Fully waitlisted status will be dropped and names of the passengers will not appear on the chart. They are not allowed to board the train. If full waitlisted passengers are found travelling, they will be treated as without ticket and charged as per extant Railway rules.</li></ol></div><span><b>c) Cancellation rules</b></span><div><ol style=\"margin: 5px 5px 0 15px;text-align: justify; width: 745px; list-style-position: inside;\"><li>Confirmed E ticket can be cancelled by passengers through internet before chart preparation only.</li><li>Partially waitlisted/Confirmed/RAC E ticket can be cancelled by passengers through internet before chart preparation only.</li><li>Fully waitlisted E tickets can be cancelled by passenger using internet before chart preparation. After chart preparation it will be cancelled by Railways and money refunded automatically.</li></ol></div><span><b>d) Refund rules</b></span><div><ol style=\"margin: 5px 5px 0 15px;text-align: justify; width: 745px; list-style-position: inside;\"><li>After chart preparation, Refunds for Confirmed E ticket will be processed on a receipt of mail from customer on the email id : rail@travelboutiqueonline.com. It would then be processed offline and refund sanctioned by Railways would be credited back to user/agents account.</li><li>After chart preparation,refunds for Partially Waitlisted/Confirmed/RAC E ticket will be processed on a receipt of mail from customer on the email id: rail@travelboutiqueonline.com . It would then be processed offline and refund sanctioned by Railways would be credited back to user/agents account.</li><li>For refunds of Fully waitlisted E ticket after chart preparation, customer need not to apply to IRCTC for cancellation and refund. Their tickets will be cancelled online by Railways and refund shall be credited to user/agents account automatically.</li></ol></div><br/><br/><b>Thank you for using IRCTC's Services.</b><b>Other Instructions</b><div><ul style=\"margin: 5px 5px 0 15px;text-align: justify; width: 745px; list-style-position: inside;\"><li>E-ticket passenger is permitted in the train against a berth/seat only when his name appears in the reservation chart failing which he can be treated as a passenger traveling without ticket.</li><li>E-ticket cannot be cancelled after chart preparation through internet.You are requested to use the online TDR filing & Status tracking service provided by IRCTC. Select the \"File TDR\" Link in the left panel under the \"My Transactions\" Menu for filing TDR online.For additional information regarding TDR, Select the \"File TDR Procedure\" in the left panel under the \"General\" Menu to process the refund with the Railways in this case. Any further clarification please mail to rail@travelboutiqueonline.com</li><li>If train is cancelled, E-ticket can be cancelled through internet by the user upto 3 days from the date of departure of the train.</li><li>Bank charges, if any, will be payable extra. (For details of bank charges kindly refer to Terms and Conditions on www.irctc.co.in )</li><li>The compartment/cabin/coupe numbers for first ACC and first class will be allotted at the time of chart preparation.</li><li>For Train No. 2901 / 2902 (Gujarat Mail), Coach No. S2 to S6 will be attached / detached at Dadar</li><li>In case of 1-AC booking, Coach / Seat No. will be given at the time of charting.</li><li>The customer who has opted for auto-up gradation during booking of his/her e-ticket is requested to check the up-gradation chart before boarding the train.</li></ul></div>";
            return waitListString;
        }
        private static string GetWaitListTopString()
        {
            string waitListString = "<p style=\"float:left; width: 745px;\">PNRs having Fully waitlisted status will be dropped and names of the passengers will not appear on the chart. They are not allowed to board the train. If full waitlisted passengers are found travelling, they will be treated as without ticket and charged as per extant Railway rules.</p>";
            return waitListString;
        }
    }
}
 
