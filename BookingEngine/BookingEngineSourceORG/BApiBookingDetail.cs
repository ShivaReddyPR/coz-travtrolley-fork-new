using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using Technology.BookingEngine;
using CoreLogic;
using System.Diagnostics;

namespace Technology.BookingEngine
{
    public class BApiBookingDetail
    {
        # region Private Fields
        PaymentGatewaySource paySource;
        string paymentId;
        int referenceId;
        decimal paymentAmount;
        bool isDomesticReturn;
        string bookingIdOut;
        string bookingIdIn;
        string outPNR;
        string inPNR;
        bool outTicketed;
        bool inTicketed;
        int agencyId;
        string airlineCodeOut;
        string airlineCodeIn;
        string origin;
        string destination;
        string flightNumberOut;
        string flightNumberIn;
        string passengerInfo;
        DateTime depDate;
        DateTime returnDate;
        APIBookingStatus bookingStatus;
        string remarks;
        DateTime createdOn;
        int createdBy;
        DateTime lastModifiedOn;
        int lastModifiedBy;
        # endregion

        # region Public Properties
        public PaymentGatewaySource PaySource
        {
            set
            {
                paySource = value;
            }
            get
            {
                return paySource;
            }
        }

        public int ReferenceId
        {
            set
            {
                referenceId = value;
            }
            get
            {
                return referenceId;
            }
        }


        public string PaymentId
        {
            set
            {
                paymentId = value;
            }
            get
            {
                return paymentId;
            }
        }

        public decimal PaymentAmount
        {
            set
            {
                paymentAmount = value;
            }
            get
            {
                return paymentAmount;
            }
        }

        public bool IsDomesticReturn
        {
            set
            {
                isDomesticReturn = value;
            }
            get
            {
                return isDomesticReturn;
            }
        }

        public string BookingIdOut
        {
            set
            {
                bookingIdOut = value;
            }
            get
            {
                return bookingIdOut;
            }
        }

        public string BookingIdIn
        {
            set
            {
                bookingIdIn = value;
            }
            get
            {
                return bookingIdIn;
            }
        }

        public string OutPNR
        {
            set
            {
                outPNR = value;
            }
            get
            {
                return outPNR;
            }
        }

        public string InPNR
        {
            set
            {
                inPNR = value;
            }
            get
            {
                return inPNR;
            }
        }

        public bool OutTicketed
        {
            set
            {
                outTicketed = value;
            }
            get
            {
                return outTicketed;
            }
        }

        public bool InTicketed
        {
            set
            {
                inTicketed = value;
            }
            get
            {
                return inTicketed;
            }
        }

        public int AgencyId
        {
            set
            {
                agencyId = value;
            }
            get
            {
                return agencyId;
            }
        }

        public string AirlineCodeOut
        {
            set
            {
                airlineCodeOut = value;
            }
            get
            {
                return airlineCodeOut;
            }
        }

        public string AirlineCodeIn
        {
            set
            {
                airlineCodeIn = value;
            }
            get
            {
                return airlineCodeIn;
            }
        }

        public string Origin
        {
            set
            {
                origin = value;
            }
            get
            {
                return origin;
            }
        }

        public string Destination
        {
            set
            {
                destination = value;
            }
            get
            {
                return destination;
            }
        }

        public string FlightNumberOut
        {
            set
            {
                flightNumberOut = value;
            }
            get
            {
                return flightNumberOut;
            }
        }

        public string FlightNumberIn
        {
            set
            {
                flightNumberIn = value;
            }
            get
            {
                return flightNumberIn;
            }
        }

        public string PassengerInfo
        {
            set
            {
                passengerInfo = value;
            }
            get
            {
                return passengerInfo;
            }
        }

        public DateTime DepDate
        {
            set
            {
                depDate = value;
            }
            get
            {
                return depDate;
            }
        }

        public DateTime ReturnDate
        {
            set
            {
                returnDate = value;
            }
            get
            {
                return returnDate;
            }
        }

        public APIBookingStatus BookingStatus
        {
            set
            {
                bookingStatus = value;
            }
            get
            {
                return bookingStatus;
            }
        }
        public string Remarks
        {
            set
            {
                remarks = value;
            }
            get
            {
                return remarks;
            }
        }

        public DateTime CreatedOn
        {
            set
            {
                createdOn = value;
            }
            get
            {
                return createdOn;
            }
        }

        public DateTime LastModifiedOn
        {
            set
            {
                lastModifiedOn = value;
            }
            get
            {
                return lastModifiedOn;
            }
        }

        public int CreatedBy
        {
            set
            {
                createdBy = value;
            }
            get
            {
                return createdBy;
            }
        }

        public int LastModifiedBy
        {
            set
            {
                lastModifiedBy = value;
            }
            get
            {
                return lastModifiedBy;
            }
        }


        # endregion

        #region Class Constructors
        public BApiBookingDetail()
        {
        }
        public BApiBookingDetail(int bookingId, int agencyId)
        {
            Load(bookingId, agencyId);
        }
        #endregion


        # region Methods

        public void Load(int refId, int agencyId)
        {
            Trace.TraceInformation("BApiBookingDetail.Load entered : bookingId = " + referenceId);
            referenceId = refId;
            if (referenceId <= 0)
            {
                throw new ArgumentException("Reference Id should be positive integer", "referenceId");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@referenceId", referenceId);
            paramList[1] = new SqlParameter("@agencyId", agencyId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetBApiBookingDetailByRefId, paramList, connection);
            if (data.Read())
            {
                createdBy = Convert.ToInt32(data["createdBy"]);
                createdOn = Convert.ToDateTime(data["createdOn"]);
                lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                agencyId = Convert.ToInt32(data["agencyId"]);
                if (data["airlineCodeIn"] != DBNull.Value)
                {
                    airlineCodeIn = Convert.ToString(data["airlineCodeIn"]);
                }
                airlineCodeOut = Convert.ToString(data["airlineCodeOut"]);
                if (data["bookingIdIn"] != DBNull.Value)
                {
                    bookingIdIn = Convert.ToString(data["bookingIdIn"]);
                }
                if (data["bookingIdOut"] != DBNull.Value)
                {
                    bookingIdOut = Convert.ToString(data["bookingIdOut"]);
                }
                if (data["inPNR"] != DBNull.Value)
                {
                    inPNR = Convert.ToString(data["inPNR"]);
                }
                if (data["outPNR"] != DBNull.Value)
                {
                    outPNR = Convert.ToString(data["outPNR"]);
                }
                bookingStatus = (APIBookingStatus)Enum.Parse(typeof(APIBookingStatus), Convert.ToString(data["bookingStatus"]));

                depDate = Convert.ToDateTime(data["depDate"]);
                destination = Convert.ToString(data["destination"]);
                if (data["flightNumberIn"] != DBNull.Value)
                {
                    flightNumberIn = Convert.ToString(data["flightNumberIn"]);
                }
                flightNumberOut = Convert.ToString(data["flightNumberOut"]);
                if (data["inTicketed"] != DBNull.Value)
                {
                    inTicketed = Convert.ToBoolean(data["inTicketed"]);
                }
                isDomesticReturn = Convert.ToBoolean(data["isDomesticReturn"]);
                origin = Convert.ToString(data["origin"]);
                outTicketed = Convert.ToBoolean(data["outTicketed"]);
                passengerInfo = Convert.ToString(data["passengerInfo"]);
                paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                paymentId = Convert.ToString(data["paymentId"]);
                paySource = (PaymentGatewaySource)Enum.Parse(typeof(PaymentGatewaySource), Convert.ToString(data["paymentGatewaySourceId"]));
                if (data["returnDate"] != DBNull.Value)
                {
                    returnDate = Convert.ToDateTime(data["returnDate"]);
                }
                if (data["remarks"] != DBNull.Value)
                {
                    remarks = Convert.ToString(data["remarks"]);
                }
                else
                {
                    remarks = string.Empty;
                }
                data.Close();
                connection.Close();
                Trace.TraceInformation("BookingDetail.Load exiting :" + referenceId.ToString());
            }
            else
            {
                data.Close();
                connection.Close();
                Trace.TraceInformation("BApiBookingDetail.Load exiting : ReferenceId does not exist.referenceId = " + referenceId.ToString());
                throw new ArgumentException("Reference id does not exist in database");
            }

        }

        public static List<BApiBookingDetail> GetDetails(int pageNumber, int noOfRecordsPerPage, string whereString)
        {
            int totalRecords = 0;
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] parameterList = new SqlParameter[1];
            parameterList[0] = new SqlParameter("@whereString", whereString);
            SqlDataReader tempData = Dal.ExecuteReaderSP(SPNames.GetBApiDetailCount, parameterList, connection);
            if (tempData.Read())
            {
                totalRecords = Convert.ToInt32(tempData["total"]);
                tempData.Close();
            }
            else
            {
                tempData.Close();
            }

            int endrow = 0;
            int startrow = ((noOfRecordsPerPage * (pageNumber - 1)) + 1);
            if ((startrow + noOfRecordsPerPage) - 1 < totalRecords)
            {
                endrow = (startrow + noOfRecordsPerPage) - 1;
            }
            else
            {
                endrow = totalRecords;
            }
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@startrow", startrow);
            paramList[1] = new SqlParameter("@endrow", endrow);
            paramList[2] = new SqlParameter("@whereString", whereString);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetBApiDetail, paramList, connection);
            List<BApiBookingDetail> bookingDetail = new List<BApiBookingDetail>();
            while (data.Read())
            {
                BApiBookingDetail detail = new BApiBookingDetail();
                detail.createdBy = Convert.ToInt32(data["createdBy"]);
                detail.createdOn = Convert.ToDateTime(data["createdOn"]);
                detail.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                detail.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                detail.agencyId = Convert.ToInt32(data["agencyId"]);
                detail.airlineCodeOut = Convert.ToString(data["airlineCodeOut"]);
                if (data["airlineCodeIn"] != DBNull.Value)
                {
                    detail.airlineCodeIn = Convert.ToString(data["airlineCodeIn"]);
                }
                if (data["bookingIdIn"] != DBNull.Value)
                {
                    detail.bookingIdIn = Convert.ToString(data["bookingIdIn"]);
                }
                if (data["bookingIdOut"] != DBNull.Value)
                {
                    detail.bookingIdOut = Convert.ToString(data["bookingIdOut"]);
                }
                if (data["inPNR"] != DBNull.Value)
                {
                    detail.inPNR = Convert.ToString(data["inPNR"]);
                }
                if (data["outPNR"] != DBNull.Value)
                {
                    detail.outPNR = Convert.ToString(data["outPNR"]);
                }
                detail.bookingStatus = (APIBookingStatus)Enum.Parse(typeof(APIBookingStatus), Convert.ToString(data["bookingStatus"]));
                detail.depDate = Convert.ToDateTime(data["depDate"]);
                detail.destination = Convert.ToString(data["destination"]);
                if (data["flightNumberIn"] != DBNull.Value)
                {
                    detail.flightNumberIn = Convert.ToString(data["flightNumberIn"]);
                }
                detail.flightNumberOut = Convert.ToString(data["flightNumberOut"]);
                if (data["inTicketed"] != DBNull.Value)
                {
                    detail.inTicketed = Convert.ToBoolean(data["inTicketed"]);
                }
                detail.isDomesticReturn = Convert.ToBoolean(data["isDomesticReturn"]);
                detail.origin = Convert.ToString(data["origin"]);
                detail.outTicketed = Convert.ToBoolean(data["outTicketed"]);
                detail.passengerInfo = Convert.ToString(data["passengerInfo"]);
                detail.paymentAmount = Convert.ToDecimal(data["paymentAmount"]);
                detail.paymentId = Convert.ToString(data["paymentId"]);
                detail.paySource = (PaymentGatewaySource)Enum.Parse(typeof(PaymentGatewaySource), Convert.ToString(data["paymentGatewaySourceId"]));
                detail.referenceId = Convert.ToInt32(data["referenceId"]);
                detail.origin = Convert.ToString(data["origin"]);
                if (data["returnDate"] != DBNull.Value)
                {
                    detail.returnDate = Convert.ToDateTime(data["returnDate"]);
                }
                if (data["remarks"] != DBNull.Value)
                {
                    detail.remarks = data["remarks"].ToString();
                }
                else
                {
                    detail.remarks = string.Empty;
                }

                bookingDetail.Add(detail);
            }
            data.Close();
            connection.Close();
            return bookingDetail;
        }

        public static int GetDetailsCount(string whereString)
        {
            DataSet dataset = new DataSet();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@whereString", whereString);
            dataset = Dal.FillSP(SPNames.GetBApiDetailCount, paramList);
            int detailCount = 0;
            if (dataset.Tables.Count != 0)
            {
                detailCount = Convert.ToInt32(dataset.Tables[0].Rows[0]["total"]);
            }
            dataset.Dispose();
            return detailCount;
        }

        public int Save()
        {
            if (referenceId == 0)
            {
                SqlParameter[] paramList = new SqlParameter[24];

                paramList[0] = new SqlParameter("@paymentGatewaySourceId", (int)(paySource));
                paramList[1] = new SqlParameter("@paymentId", paymentId);
                paramList[2] = new SqlParameter("@paymentAmount", paymentAmount);
                paramList[3] = new SqlParameter("@isDomesticReturn", isDomesticReturn);
                if (bookingIdOut == null)
                {
                    paramList[4] = new SqlParameter("@bookingIdOut", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@bookingIdOut", bookingIdOut);
                }
                if (bookingIdIn == null)
                {
                    paramList[5] = new SqlParameter("@bookingIdIn", DBNull.Value);
                }
                else
                {
                    paramList[5] = new SqlParameter("@bookingIdIn", bookingIdIn);
                }
                paramList[6] = new SqlParameter("@outTicketed", outTicketed);
                paramList[7] = new SqlParameter("@inTicketed", inTicketed);
                paramList[8] = new SqlParameter("@agencyId", agencyId);
                paramList[9] = new SqlParameter("@airlineCodeOut", airlineCodeOut);
                if (airlineCodeIn == null)
                {
                    paramList[10] = new SqlParameter("@airlineCodeIn", DBNull.Value);
                }
                else
                {
                    paramList[10] = new SqlParameter("@airlineCodeIn", airlineCodeIn);
                }
                paramList[11] = new SqlParameter("@origin", origin);
                paramList[12] = new SqlParameter("@destination", destination);
                paramList[13] = new SqlParameter("@flightNumberOut", flightNumberOut);
                if (flightNumberIn == null)
                {
                    paramList[14] = new SqlParameter("@flightNumberIn", DBNull.Value);
                }
                else
                {
                    paramList[14] = new SqlParameter("@flightNumberIn", flightNumberIn);
                }
                paramList[15] = new SqlParameter("@passengerInfo", passengerInfo);
                paramList[16] = new SqlParameter("@depDate", depDate);
                if (returnDate.ToString() == "1/1/0001 12:00:00 AM")
                {
                    paramList[17] = new SqlParameter("@returnDate", DBNull.Value);
                }
                else
                {
                    paramList[17] = new SqlParameter("@returnDate", returnDate);
                }
                paramList[18] = new SqlParameter("@referenceId", SqlDbType.Int);
                paramList[18].Direction = ParameterDirection.Output;
                paramList[19] = new SqlParameter("@bookingStatus", bookingStatus);
                if (inPNR == null)
                {
                    paramList[20] = new SqlParameter("@inPNR", DBNull.Value);
                }
                else
                {
                    paramList[20] = new SqlParameter("@inPNR", inPNR);
                }
                if (outPNR == null)
                {
                    paramList[21] = new SqlParameter("@outPNR", DBNull.Value);
                }
                else
                {
                    paramList[21] = new SqlParameter("@outPNR", outPNR);
                }
                paramList[22] = new SqlParameter("@createdBy", createdBy);
                if (remarks == null || remarks == "")
                {
                    paramList[23] = new SqlParameter("@remarks", DBNull.Value);
                }
                else
                {
                    paramList[23] = new SqlParameter("@remarks", remarks);
                }
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddBApiBookingDetail, paramList);
                if (rowsAffected > 0)
                {
                    return (int)paramList[18].Value;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[8];

                if (bookingIdOut == null || bookingIdOut == "")
                {
                    paramList[0] = new SqlParameter("@bookingIdOut", DBNull.Value);
                }
                else
                {
                    paramList[0] = new SqlParameter("@bookingIdOut", bookingIdOut);
                }
                if (bookingIdIn == null || bookingIdIn == "")
                {
                    paramList[1] = new SqlParameter("@bookingIdIn", DBNull.Value);
                }
                else
                {
                    paramList[1] = new SqlParameter("@bookingIdIn", bookingIdIn);
                }
                paramList[2] = new SqlParameter("@bookingStatus", (int)bookingStatus);
                if (inPNR == null || inPNR == "")
                {
                    paramList[3] = new SqlParameter("@inPNR", DBNull.Value);
                }
                else
                {
                    paramList[3] = new SqlParameter("@inPNR", inPNR);
                }
                if (outPNR == null || outPNR == "")
                {
                    paramList[4] = new SqlParameter("@outPNR", DBNull.Value);
                }
                else
                {
                    paramList[4] = new SqlParameter("@outPNR", outPNR);
                }
                if (remarks == null || remarks == "")
                {
                    paramList[5] = new SqlParameter("@remarks", DBNull.Value);
                }
                else
                {
                    paramList[5] = new SqlParameter("@remarks", remarks);
                }
                paramList[6] = new SqlParameter("@referenceId", referenceId);
                paramList[7] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateBApiBookingDetail, paramList);
                return referenceId;

            }

        }
        # endregion

    }
}
