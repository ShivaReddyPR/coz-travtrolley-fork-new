using System;
using System.Net;
using System.IO;

using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;

namespace Technology.BookingEngine
{
	/// <summary>
	/// Summary description for HttpWebResponseDecompressed.
	/// </summary>
	public class HttpWebResponseDecompressed : WebResponse
	{
		HttpWebResponse _response;
		private string _contentType;
		private long _contentLength;
		private string _contentEncoding;
		private Uri _responseUri;
		private WebHeaderCollection _httpResponseHeaders;

		public override long ContentLength
		{
			set 
			{
				_contentLength = value;
			}
			get
			{
				return _contentLength;
			}
		}

		public override string ContentType
		{
			set 
			{
				_contentType = value;
			}
			get 
			{
				return _contentType;
			}
		}

		public override WebHeaderCollection Headers
		{
			get
			{
				return _httpResponseHeaders;
			}
		}

		public string ContentEncoding
		{
			set 
			{
				_contentEncoding = value;;
			}
			get 
			{
				return _contentEncoding;
			}
		}

		public HttpWebResponseDecompressed(HttpWebResponse response)
		{
			_response = response;
			_contentType = response.ContentType;
			_contentLength = response.ContentLength;
			_contentEncoding = response.ContentEncoding;
			_responseUri = response.ResponseUri;
			_httpResponseHeaders = response.Headers;
		}
        public override System.IO.Stream GetResponseStream()
        {
            // WriteResponseToFile(response);	

            Stream compressedStream = null;
            // select right decompression stream (or null if content is not compressed)	
            if (_response.ContentEncoding == "gzip")
            {
                compressedStream = new GZipInputStream(_response.GetResponseStream());
            }

            if (compressedStream != null)
            {
                // decompress
                MemoryStream decompressedStream = new MemoryStream();

                int size = 2048;
                byte[] writeData = new byte[2048];
                while (true)
                {
                    size = compressedStream.Read(writeData, 0, size);
                    if (size > 0)
                    {
                        decompressedStream.Write(writeData, 0, size);
                    }
                    else
                    {
                        break;
                    }
                }
                decompressedStream.Seek(0, SeekOrigin.Begin);
                _response.Close();
                return decompressedStream;
            }
            else
                return _response.GetResponseStream();
        }
		public override void Close()
		{
		}

 		public override Uri ResponseUri 
		{ 
			get 
			{ 
				return _responseUri; 
			} 
		}
	}
}
