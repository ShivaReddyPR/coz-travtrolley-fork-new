using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using Technology.Data;
using Technology.Configuration;
using Technology.BookingEngine;
using CoreLogic;
///
namespace Technology.BookingEngine
{
    /// <summary>
    /// This Class is used for implenting Calendar Fare Search 
    /// </summary>
    public class CalendarSearch
    {
        /// <summary>
        /// List of Private Varibales
        /// </summary>
        private string airlineCode;
        private string origin;
        private string destination;
        private double fare;
        private DateTime departureTime;
        private DateTime createdOn;

        float minFare = 9999999999999;
        // This is for storing the detils of least fare in a given month.
        public Dictionary<int, ArrayList> monthlyDetails = new Dictionary<int, ArrayList>();

        /// <summary>
        /// Name of the Airline Code
        /// </summary>
        public string AirlineCode
        {
            get
            {
                return airlineCode;
            }
            set
            {
                airlineCode = value;
            }
        }
        /// <summary>
        /// Name of the source statiion i.e Origin
        /// </summary>
        public string Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
            }
        }
        /// <summary>
        /// Name of Destination Station
        /// </summary>
        public string Destination
        {
            get
            {
                return destination;
            }
            set
            {
                destination = value;
            }
        }
        /// <summary>
        /// Amount of the fare.
        /// </summary>
        public double Fare
        {
            get
            {
                return fare;
            }
            set
            {
                fare = value;
            }
        }
        /// <summary>
        /// Departure Time for the flight .It includes Date of Journey Also
        /// </summary>
        public DateTime DepartureTime
        {
            get
            {
                return departureTime;
            }
            set
            {
                departureTime = value;
            }
        }
        /// <summary>
        /// This is used for the time stamp .
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }

        }
        /// <summary>
        /// Constructor for the class.
        /// </summary>
        public CalendarSearch()
        {

        }
        /// <summary>
        /// This Method is use to update the Data in DB.
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("CalendarSearch.Save entered");
            // if airline Code length less than 2 then give exception
            if (airlineCode.Length != 2)
            {
                throw new ArgumentException("Invalid Airline Code");
            }
            int rowsAffected;
            // Update the data base
            Trace.TraceInformation("Calendar.Save : making update");
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@airlineCode", airlineCode);
            paramList[1] = new SqlParameter("@origin", origin);
            paramList[2] = new SqlParameter("@destination", destination);
            paramList[3] = new SqlParameter("@fare", fare);
            paramList[4] = new SqlParameter("@departureTime", departureTime);
            rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateCalendar, paramList);
            Trace.TraceInformation("CalendarSearch.Save exiting : return = " + rowsAffected);

        }
        /// <summary>
        /// This menthod will get Lowest fare for a given month and year  in a calendar format and return the lowest fare in the month.
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <param name="origin"></param>
        /// <param name="dest"></param>
        public float GetLowestFareDetails(List<string> airlineList, int month, int year, string origin, string dest)
        {
            SqlConnection connection = Dal.GetConnection();//connection to DB.
            SqlDataReader dataReader;
            // counts the number of days in a given year and Month.
            int days = DateTime.DaysInMonth(year, month);
            string airCode, fullCode;
            bool addFlag = false;
            Decimal fare;
            DateTime dateofDep;
            ArrayList lpDetials;
            int i = 1;

            // Get the lowest fare on all days of a given month.
            if (month == DateTime.Now.Month && year == DateTime.Now.Year)
            {
                i = DateTime.Now.Day;
            }
            for (; i <= days; i++)
            {
                lpDetials = new ArrayList();
                DateTime dt = new DateTime(year, month, i);
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@origin", origin);
                paramList[1] = new SqlParameter("@destination", dest);
                paramList[2] = new SqlParameter("@dt", dt);
                dataReader = Dal.ExecuteReaderSP(SPNames.GetLowestFareDetails, paramList, connection);
                // Get from the Data base and add to localvariables.
                while (dataReader.Read() && addFlag == false)
                {
                    airCode = (string)dataReader["AirlineCode"];
                    if (airlineList.Contains(airCode.ToUpper()))
                    {
                        switch (airCode.ToUpper())
                        {
                            case "SG": fullCode = "Spicejet";
                                break;
                            case "6E": fullCode = "Indigo";
                                break;
                            case "IT": fullCode = "Kingfisher";
                                break;
                            case "9W": fullCode = "Jet Airways";
                                break;
                            case "IC": fullCode = "Indian";
                                break;
                            case "I7": fullCode = "Paramount";
                                break;
                            case "S2": fullCode = "Jetlite";
                                break;
                            case "DN": fullCode = "AirDeccan";
                                break;
                            case "G8": fullCode = "GoAir";
                                break;
                            case "9H": fullCode = "MDLR";
                                break;
                            default: fullCode = "default";
                                break;

                        }
                        fare = Convert.ToDecimal(dataReader["Fare"]);
                        if ((float)fare < minFare)
                        {
                            minFare = (float)fare;
                        }
                        dateofDep = (DateTime)dataReader["DepartureTime"];
                        lpDetials.Add(fullCode);
                        lpDetials.Add(fare);
                        lpDetials.Add(dateofDep);
                        monthlyDetails.Add(i, lpDetials);
                        addFlag = true;
                    }
                    // if it doesnt contain then display appropiate message,

                }
                if (addFlag != true)
                {
                    lpDetials = null;
                    monthlyDetails.Add(i, lpDetials);
                }
                addFlag = false;

                // clsoing the Data base connection on a given day.
                dataReader.Close();
            }
            connection.Close();
            return minFare;
        }
        /// <summary>
        /// This method will gets the lowest fare on a given Date.It will make the request object and return it to search the data.
        /// </summary>
        /// <param name="airlineList"></param>
        /// <param name="origin"></param>
        /// <param name="destination"></param>
        /// <param name="departureDate"></param>
        /// <returns></returns>
        public SearchRequest GetSearchRequest(List<string> airlineList, string origin, string destination, DateTime departureDate)
        {
            SearchRequest req = new SearchRequest();
            req.Segments = new FlightSegment[2];
            FlightSegment fs = new FlightSegment();
            fs.Origin = origin;
            fs.Destination = destination;
            fs.PreferredAirlines = new string[0];
            fs.PreferredDepartureTime = departureDate;

            req.AdultCount = 1;
            req.ChildCount = 0;
            req.InfantCount = 0;
            req.SeniorCount = 0;
            req.Type = SearchType.OneWay;
            req.RestrictAirline = false;
            //Adds the source airlines.
            List<string> sources = new List<string>();
            List<string> prefCarrier = new List<string>();
            foreach (string airline in airlineList)
            {
                if (airline.ToUpper() == "IC" || airline.ToUpper() == "IT" || airline.ToUpper() == "9W" || airline.ToUpper() == "S2")
                {
                    if (!sources.Contains("1P"))
                    {
                        sources.Add("1P");
                    }
                    if (!sources.Contains("1A"))
                    {
                        sources.Add("1A");
                    }
                    prefCarrier.Add(airline);
                }
                else
                {
                    sources.Add(airline);
                }
            }
            fs.flightCabinClass = CabinClass.Economy;
            fs.PreferredAirlines = prefCarrier.ToArray();
            req.Sources = sources;
            req.Segments[0] = fs;
            //returns the object.
            return req;
        }
        /// <summary>
        /// This method will get Domestic city List and return it.
        /// </summary>
        /// <returns></returns>

        public Dictionary<string, string> GetDomesticList()
        {
            string cityCode, cityName;
            Dictionary<string, string> cityList = new Dictionary<string, string>();
            SqlConnection connection = Dal.GetConnection();//connection to DB.
            SqlDataReader dataReader;
            SqlParameter[] paramList = new SqlParameter[0];
            dataReader = Dal.ExecuteReaderSP(SPNames.GetDomesticCity, paramList, connection);
            while (dataReader.Read())
            {
                cityCode = (string)dataReader["cityCode"];
                cityName = (string)dataReader["cityName"];
                cityList.Add(cityCode, cityName);
            }
            dataReader.Close();
            connection.Close();
            return cityList;
        }
    }
}
