using System;
using System.Collections.Generic;
using System.Text;

namespace Technology.BookingEngine
{

    public enum HotelRequestMode
    {
        B2B = 1,
        BookingAPI = 2,
        WhiteLabel = 3
    }
    [Serializable]
    public struct RoomGuestData
    {
        public int noOfAdults;
        public int noOfChild;
        public List<int> childAge; // ChildAge list size should be equal to noOfChild
    }
    [Serializable]
    public struct RoomInfo
    {
        public string roomTypeCode;
        public string roomTypeName;
        public int noOfRooms;
        public int noOfCots;
        public List<int> childAge;
    }
    [Serializable]
    public class HotelRequest
    {
        # region private variables
        private int cityId;
        private DateTime startDate;
        private DateTime endDate;
        private string cityName;
        private string cityCode;
        private int noOfRooms; // total no of rooms
        private RoomGuestData[] roomGuest;
        private string hotelName; // - Optional
        private string area; // - Optional
        private string attraction; // - Optional
        private HotelRating rating; // by Default - All
        private int extraCots; // - Optional
        private string currency; // Always 'INR' - Optional
        private string priceRange; //ie. 1000 & gretaed than, 2000 & greated than - Optional
        private AvailabilityType availabilityType;
        private List<string> sources;
        private bool isDomestic;
        private bool searchByArea;
        private string countryName;
        private bool isMultiRoom;
        private int minRating;
        private int maxRating;
        private HotelRequestMode requestMode;
        //private List<RoomInfo> multiRoom;

        # endregion

        # region Public properties
        public int CityId
        {
            get { return cityId; }
            set { cityId = value; }
        }
        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                startDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        public string CityName
        {
            get
            {
                return cityName;
            }
            set
            {
                cityName = value;
            }
        }

        public string CityCode
        {
            get
            {
                return cityCode;
            }
            set
            {
                cityCode = value;
            }
        }

        public int NoOfRooms
        {
            get
            {
                return noOfRooms;
            }
            set
            {
                noOfRooms = value;
            }
        }

        public RoomGuestData[] RoomGuest
        {
            get
            {
                return roomGuest;
            }
            set
            {
                roomGuest = value;
            }
        }

        public string HotelName
        {
            get
            {
                return hotelName;
            }
            set
            {
                hotelName = value;
            }
        }

        public string Area
        {
            get
            {
                return area;
            }
            set
            {
                area = value;
            }
        }

        public string Attraction
        {
            get
            {
                return attraction;
            }
            set
            {
                attraction = value;
            }
        }

        public HotelRating Rating
        {
            get
            {
                return rating;
            }
            set
            {
                rating = value;
            }
        }

        public int ExtraCots
        {
            get
            {
                return extraCots;
            }
            set
            {
                extraCots = value;
            }
        }

        public string Currency
        {
            get
            {
                return currency;
            }
            set
            {
                currency = value;
            }
        }

        public string PriceRange
        {
            get
            {
                return priceRange;
            }
            set
            {
                priceRange = value;
            }
        }

        public AvailabilityType AvailabilityType
        {
            get { return availabilityType; }
            set { availabilityType = value; }
        }

        public List<string> Sources
        {
            get { return sources; }
            set { sources = value; }
        }
        public bool IsDomestic
        {
            get { return isDomestic; }
            set { isDomestic = value; }
        }
        public bool SearchByArea
        {
            get { return searchByArea; }
            set { searchByArea = value; }
        }
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }
        public bool IsMultiRoom
        {
            get { return isMultiRoom; }
            set { isMultiRoom = value; }
        }
        //public List<RoomInfo> MultiRoom
        //{
        //    get { return multiRoom; }
        //    set { multiRoom = value; }
        //}
        public int MinRating
        {
            get { return minRating; }
            set { minRating = value; }
        }
        public int MaxRating
        {
            get { return maxRating; }
            set { maxRating = value; }
        }
        public HotelRequestMode RequestMode
        {
            get { return requestMode; }
            set { requestMode = value; }
        }
        # endregion

        #region PUBLIC METHODS
        public HotelRequest CopyByValue()
        {
            HotelRequest hReq = new HotelRequest();
            hReq.cityId = cityId;
            hReq.area = area;
            hReq.attraction = attraction;
            hReq.availabilityType = availabilityType;
            hReq.cityCode = cityCode;
            hReq.cityName = cityName;
            hReq.countryName = countryName;
            hReq.currency = currency;
            hReq.endDate = endDate;
            hReq.extraCots = extraCots;
            hReq.hotelName = hotelName;
            hReq.isDomestic = isDomestic;
            hReq.isMultiRoom = isMultiRoom;
            //hReq.multiRoom = multiRoom;
            hReq.noOfRooms = noOfRooms;
            hReq.priceRange = priceRange;
            hReq.rating = rating;
            hReq.roomGuest = roomGuest;
            hReq.searchByArea = searchByArea;
            hReq.sources = sources;
            hReq.startDate = startDate;
            hReq.minRating = minRating;
            hReq.maxRating = maxRating;
            hReq.requestMode = requestMode;
            return hReq;
        }

        #endregion
    }
}