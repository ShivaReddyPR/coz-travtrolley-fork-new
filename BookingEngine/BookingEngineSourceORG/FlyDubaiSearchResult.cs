using System;
using System.Collections.Generic;
using System.Text;
using Technology.BookingEngine;

namespace Technology.BookingEngine
{
    public class FlyDubaiSearchResult:SearchResult 
    {
        private Dictionary<PassengerType, int> fareInformationId;

        // User for fare Quote Information
        public Dictionary<PassengerType, int> FareInformationId
        {
            get { return fareInformationId; }
            set { fareInformationId = value; }
        }
        // Unique GUId generated
        private string guid;

        public string Guid
        {
            get
            {
                return guid;
            }
            set
            {
                guid = value;
            }
        }

    }
}
