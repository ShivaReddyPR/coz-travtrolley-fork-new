using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;

namespace Technology.BookingEngine
{
    public class GTACity
    {
        /// <summary>
        /// Private variables
        /// </summary>

        private string cityCode;
        private string cityName;
        private string countryName;
        private string countryCode;
        private string currencyCode;
        private int index;
        /// <summary>
        /// Public Properties
        /// </summary>
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }

        }
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }
        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }
        public int Index
        {
            get { return index; }
            set { index = value; }
        }
        /// <summary>
        /// This Method  is sued to Save the GTA city Codes
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("GTACity.Save entered.");

            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@cityName", cityName);
            paramList[1] = new SqlParameter("@cityCode", cityCode);
            paramList[2] = new SqlParameter("@countryName", countryName);
            paramList[3] = new SqlParameter("@countryCode", countryCode);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddGTACity, paramList);
            Trace.TraceInformation("GTACity.Save exiting");

        }
        /// <summary>
        /// This Method will get List of all cities by using based on country Name
        /// </summary>
        /// <param name="countryName"></param>
        /// <returns></returns>
        public List<GTACity> GetCities(string countryName)
        {
            Trace.TraceInformation("GTACity.GetCity Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@countryName", countryName);
            SqlConnection connection = Dal.GetConnection();
            List<GTACity> cityList = new List<GTACity>();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetGTACities, paramlist, connection);
            while (datareader.Read())
            {
                GTACity cityObject = new GTACity();
                cityObject.cityCode = datareader["cityCode"].ToString();
                cityObject.cityName = datareader["cityName"].ToString();
                cityObject.countryCode = datareader["countryCode"].ToString();
                cityObject.countryName = datareader["countryName"].ToString();
                cityObject.currencyCode = datareader["currencyCode"].ToString();
                cityList.Add(cityObject);
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.GetCity Exit");
            return cityList;
        }

        /// <summary>
        /// This method is intended to load all GTA cities and other information
        /// </summary>
        /// <returns></returns>
        public List<GTACity> GTAAllCities()
        {
            Trace.TraceInformation("GTACity.GTAAllCities Entered");
            SqlParameter[] paramlist = new SqlParameter[0];            
            SqlConnection connection = Dal.GetConnection();
            List<GTACity> cityList = new List<GTACity>();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetAllGTACity, paramlist, connection);
            while (datareader.Read())
            {
                GTACity cityObject = new GTACity();
                cityObject.cityCode = datareader["cityCode"].ToString();
                cityObject.cityName = datareader["cityName"].ToString();
                cityObject.countryCode = datareader["countryCode"].ToString();
                cityObject.countryName = datareader["countryName"].ToString();
                cityObject.currencyCode = datareader["currencyCode"].ToString();
                cityList.Add(cityObject);
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.GTAAllCities Exit");
            return cityList;
        }
        /// <summary>
        /// This Method is used toget All country Name of GTA
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllCountries()
        {
            Trace.TraceInformation("GTACity.GetAllCountries Entered");
            SqlParameter[] paramlist = new SqlParameter[0];
            SqlConnection connection = Dal.GetConnection();
            List<string> countryList = new List<string>();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetGTACountries, paramlist, connection);
            while (datareader.Read())
            {
                countryList.Add(datareader["countryName"].ToString());
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.GetAllCountries exiting");
            return countryList;

        }
        /// <summary>
        /// This Method is used to get List of All country codes in GTA
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllCountryCodes()
        {
            Trace.TraceInformation("GTACity.GetAllCountryCodes Entered");
            SqlParameter[] paramlist = new SqlParameter[0];
            SqlConnection connection = Dal.GetConnection();
            List<string> countryList = new List<string>();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetGTACountryCodes, paramlist, connection);
            while (datareader.Read())
            {
                countryList.Add(datareader["countryCode"].ToString());
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.GetAllCountryCodes exiting");
            return countryList;
        }
        /// <summary>
        /// This Method is used to get City Code by using cityName and country Name.
        /// </summary>
        /// <param name="cityName"></param>
        /// <param name="countryName"></param>
        /// <returns></returns>
        public string GetCityCode(string cityName,string countryName )
        {
            Trace.TraceInformation("GTACity.GetCityCode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityName", cityName);
            SqlConnection connection = Dal.GetConnection();
            string cityCode = string.Empty;
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetGTACityCode, paramlist, connection);
            while (datareader.Read())
            {
                if (countryName.Length > 0 && countryName == datareader["countryName"].ToString())
                {
                    cityCode = datareader["cityCode"].ToString();
                    break;
                }
                else
                {
                    cityCode = datareader["cityCode"].ToString();
                }
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.GetCityCode exiting");
            return cityCode;
        }
        /// <summary>
        /// This Method is used to Load GTA City by using city Code.
        /// </summary>
        /// <param name="cityCode"></param>
        public void Load(string cityCode)
        {
            Trace.TraceInformation("GTACity.Load Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityCode", cityCode);
            SqlConnection connection = Dal.GetConnection();
       
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetGTACityDataByCode, paramlist, connection);
            if (datareader.Read())
            {
                cityCode = datareader["cityCode"].ToString();
               cityName = datareader["cityName"].ToString();
                countryCode = datareader["countryCode"].ToString();
                countryName = datareader["countryName"].ToString();
                currencyCode = datareader["currencyCode"].ToString();

            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.Load exiting");
          
        }
        /// <summary>
        /// This Method is load Area code based on city code
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        public string LoadAreaCode(string cityCode)
        {
            Trace.TraceInformation("GTACity.LoadAreaCode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityCode", cityCode);
            SqlConnection connection = Dal.GetConnection();

            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetMainAreaCodeForCity, paramlist, connection);
            string mainAreaCode = string.Empty;
            if (datareader.Read())
            {
                mainAreaCode = datareader["MainAreaCode"].ToString();
                 
           }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.LoadAreaCode exiting");
            return mainAreaCode;
        }
        /// <summary>
        /// This Method checks whether given City Code contains GTA cities and returns either true or false
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        public bool IsContainsCityCode(string cityCode)
        {
            Trace.TraceInformation("GTACity.IsContainsCityCode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@cityCode", cityCode);
            bool containsCode = false;
            SqlConnection connection = Dal.GetConnection();

            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetGTAMainAreaCityCode, paramlist, connection);
         
            if (datareader.Read())
            {
                if (Convert.ToInt16(datareader["rowsCount"]) != 0)
                {
                    containsCode = true;
                }
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.IsContainsCityCode exiting");
            return containsCode;
        }
        /// <summary>
        /// This Method is used to Load AOT Number of GTA by using country code and Country Name.
        /// </summary>
        /// <param name="countryCode"></param>
        /// <param name="countryName"></param>
        /// <returns></returns>
        public Dictionary<string, string> LoadGTAAOTNumber(string countryCode, string countryName)
        {
            Trace.TraceInformation("GTACity.LoadGTAAOTNumber Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            Dictionary<string, string> aotList = new Dictionary<string, string>();
            paramlist[0] = new SqlParameter("@countryCode", countryCode);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetGTAAOTNumber, paramlist, connection);
            string mainAreaCode = string.Empty;
            if (datareader.Read())
            {
                if (datareader["city"]!=null)
                {
                    aotList.Add("City",datareader["City"].ToString());
                }
                if (datareader["language"]!=null)
                {
                    aotList.Add("Language", datareader["language"].ToString());
                }
                if (datareader["intlCall"] != null)
                {
                    aotList.Add("InternationalNo",datareader["intlCall"].ToString());
                }
                if(datareader["nationalCall"]!=null)
                {
                    aotList.Add("NationalNo",datareader["nationalCall"].ToString());
                }
                if(datareader["localCall"]!=null)
                {
                    aotList.Add("LocalNo", datareader["localCall"].ToString());
                }
                if(datareader["officeHours"]!=null)
                {
                    aotList.Add("OfficeHours",datareader["officeHours"].ToString());
                }
                if(datareader["emergencyNo"]!=null)
                {
                    aotList.Add("EmergencyNo",datareader["emergencyNo"].ToString());
                }
                aotList.Add("Country", countryName);
               
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.LLoadGTAAOTNumber exiting");
            return aotList;
        }
        /// <summary>
        /// This Method is used to GTA Area Codes/
        /// </summary>
        /// <param name="areaCode"></param>
        /// <param name="areaName"></param>
        public void SaveAreaCode(string areaCode, string areaName)
        {
            Trace.TraceInformation("GTACity.SaveAreaCode entered.");

            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@areaCode", areaCode);
            paramList[1] = new SqlParameter("@areaName", areaName);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddGTAAreaCode, paramList);
            Trace.TraceInformation("GTACity.SaveAreaCode exiting");

        }
        /// <summary>
        /// This Method is to Update Area Code like country code and Name based on area code.
        /// </summary>
        /// <param name="areaCode"></param>
        /// <param name="countryCode"></param>
        /// <param name="countryName"></param>
        public void UpdateAreaCountry(string areaCode,string countryCode,string countryName)
        {
            Trace.TraceInformation("GTACity.UpdateAreaCountry entered.");

            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@areaCode", areaCode);
            paramList[1] = new SqlParameter("@countryCode", countryCode);
            paramList[2] = new SqlParameter("@countryName", countryName);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateAreaCountry, paramList);
            Trace.TraceInformation("GTACity.UpdateAreaCountry exiting");

        }
        /// <summary>
        /// This Method is to load Area Deatails by using country Name.
        /// </summary>
        /// <param name="countryName"></param>
        /// <returns></returns>
        public Dictionary<string, string> LoadAreaByCountryName(string countryName)
        {

            Trace.TraceInformation("GTACity.LoadAreaByCountryName Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            Dictionary<string, string> areaList = new Dictionary<string, string>();
            paramlist[0] = new SqlParameter("@countryName", countryName);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetGTAAreaCodeByCountry, paramlist, connection);
           while(datareader.Read())
            {
                areaList.Add(datareader["areaCode"].ToString(), datareader["areaName"].ToString());
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.LoadAreaByCountryName exiting");
            return areaList;
        }
        /// <summary>
        /// This Method is to get Country of GTA Area codes by using Area Code
        /// </summary>
        /// <param name="areaCode"></param>
        /// <returns></returns>
        public string GetCountryNameforAreaCode(string areaCode)
        {
            Trace.TraceInformation("GTACity.GetCountryNameforAreaCode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            Dictionary<string, string> areaList = new Dictionary<string, string>();
            paramlist[0] = new SqlParameter("@areaCode", areaCode);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetCountryNameforAreaCode, paramlist, connection);
            string countryName = string.Empty;
           if (datareader.Read())
            {
                countryName = datareader["countryName"].ToString();
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.GetCountryNameforAreaCode exiting");
            return countryName;
        }
        /// <summary>
        /// This Method is used to get currency for a given country by using country Name.
        /// </summary>
        /// <param name="countryName"></param>
        /// <returns></returns>
        public string GetCurrencyForCountry(string countryName)
        {
            Trace.TraceInformation("GTACity.GetCurrencyForCountry Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            Dictionary<string, string> areaList = new Dictionary<string, string>();
            paramlist[0] = new SqlParameter("@countryName", countryName);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetCurrencyForCountry, paramlist, connection);
            string currenctCode = string.Empty;
            if (datareader.Read())
            {
                currenctCode = datareader["currencyCode"].ToString();
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.GetCurrencyForCountry  exiting");
            return currenctCode;
        }
        /// <summary>
        /// This Method is sued to get GTA top destinations.
        /// </summary>
        /// <returns></returns>
        public List<string> GetGTATopDestination()
        {
            Trace.TraceInformation("GTACity.GetGTATopDestination entered.");
            List<string> topDestList = new List<string>();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetGTATopDestination, paramList, connection);
           while (datareader.Read())
           {   // BugID : 28084 More Info is not coming due to CityID used in XML Request. Add cityOrAreaCode in last
                topDestList.Add(datareader["cityId"].ToString() + "|" + Convert.ToBoolean(datareader["Type"]).ToString() + "|" + datareader["cityOrAreaName"].ToString() + "|" + datareader["countryName"].ToString() + "|" + datareader["cityOrAreaCode"].ToString());
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.GetGTATopDestination exiting");
            return topDestList;
        }
        /// <summary>
        /// This Method is to get Country of GTA Main Area Code by using City Code
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        public string GetCountryNameforCityCode(string cityCode)
        {
            Trace.TraceInformation("GTACity.GetCountryNameforCityCode Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            Dictionary<string, string> areaList = new Dictionary<string, string>();
            paramlist[0] = new SqlParameter("@cityCode", cityCode);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetCountryNameforCityCode, paramlist, connection);
            string countryName = string.Empty;
            if (datareader.Read())
            {
                countryName = datareader["countryName"].ToString();
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.GetCountryNameforCityCode exiting");
            return countryName;
        }
        /// <summary>
        /// This Method is used to get all Train station Codes and their Names
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetStations(string cityCode)
        {
            Trace.TraceInformation("GTACity.GetStations Entered");
            SqlParameter[] paramlist = new SqlParameter[1];
            Dictionary<string, string> areaList = new Dictionary<string, string>();
            paramlist[0] = new SqlParameter("@cityCode", cityCode);
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetTrainStationsByCityCode, paramlist, connection);
            Dictionary<string, string> stationList = new Dictionary<string, string>();
            while (datareader.Read())
            {
                if (!stationList.ContainsKey(datareader["stationCode"].ToString()))
                {
                    stationList.Add(datareader["stationCode"].ToString(), datareader["stationName"].ToString());
                }

            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.GetStations exiting");
            return stationList;

        }

        /// <summary>
        /// This Method issued to get GTA top destinations list.
        /// </summary>
        /// <returns></returns>
        public List<GTACity> GetGTATopDestinationList()
        {
            Trace.TraceInformation("GTACity.GetGTATopDestinationList entered.");
            List<GTACity> topDestList = new List<GTACity>();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlConnection connection = Dal.GetConnection();
            SqlDataReader datareader = Dal.ExecuteReaderSP(SPNames.GetGTATopDestination, paramList, connection);
            while (datareader.Read())
            {
                GTACity gtaCity = new GTACity();
                gtaCity.CountryCode = datareader["countryCode"].ToString();
                gtaCity.CountryName = datareader["countryName"].ToString();
                gtaCity.CityName = datareader["cityOrAreaName"].ToString();
                gtaCity.CityCode = datareader["cityId"].ToString();
                topDestList.Add(gtaCity);
            }
            datareader.Close();
            connection.Close();
            Trace.TraceInformation("GTACity.GetGTATopDestination exiting");
            return topDestList;
        }
    }
}
