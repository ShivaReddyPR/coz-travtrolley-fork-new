using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;

namespace Technology.BookingEngine
{
    public class HotelStaticData
    {
        private string hotelCode;
        private string cityCode;
        private string hotelName;
        private string hotelPicture;
        private string hotelDescription;
        private string hotelPolicy;
        private string hotelMap;
        private string hotelLocation;
        private string hotelAddress;
        private HotelRating rating;
        private string hotelFacilities;
        private string specialAttraction;
        private string phoneNumber;
        private string faxNumber;
        private string eMail;
        private string url;
        private string pinCode;
        private HotelBookingSource source;
        private DateTime timeStamp;
        public string HotelCode
        {
            get
            {
                return hotelCode;
            }
            set
            {
                hotelCode = value;
            }
        }
        public string CityCode
        {
            get
            {
                return cityCode;
            }
            set
            {
                cityCode = value;
            }
        }

        public string HotelName
        {
            get
            {
                return hotelName;
            }
            set
            {
                hotelName = value;
            }
        }

        public string HotelPicture
        {
            get
            {
                return hotelPicture;
            }
            set
            {
                hotelPicture = value;
            }
        }

        public string HotelDescription
        {
            get
            {
                return hotelDescription;
            }
            set
            {
                hotelDescription = value;
            }
        }
        public string HotelPolicy
        {
            get { return hotelPolicy; }
            set { hotelPolicy = value; }
        }
        public string HotelMap
        {
            get
            {
                return hotelMap;
            }
            set
            {
                hotelMap = value;
            }
        }

        public string HotelLocation
        {
            get
            {
                return hotelLocation;
            }
            set
            {
                hotelLocation = value;
            }
        }

        public string HotelAddress
        {
            get
            {
                return hotelAddress;
            }
            set
            {
                hotelAddress = value;
            }
        }
        public HotelRating Rating
        {
            get
            {
                return rating;
            }
            set
            {
                rating = value;
            }
        }
        public string HotelFacilities
        {
            get
            {
                return hotelFacilities;
            }
            set
            {
                hotelFacilities = value;
            }
        }
        public string SpecialAttraction
        {
            get
            {
                return specialAttraction;
            }
            set
            {
                specialAttraction = value;
            }
        }
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }
        public string FaxNumber
        {
            get { return faxNumber; }
            set { faxNumber = value; }
        }
        public string EMail
        {
            get { return eMail; }
            set { eMail = value; }
        }
        public string URL
        {
            get { return url; }
            set { url = value; }
        }
        public string PinCode
        {
            get { return pinCode; }
            set { pinCode = value; }
        }
        public HotelBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        public DateTime TimeStamp
        {
            get { return timeStamp; }
            set { timeStamp = value; }
        }
        #region Methods
        /// <summary>
        /// This Method is used to load the Static data
        /// </summary>
        /// <param name="hotel"></param>
        /// <param name="cityCode"></param>
        /// <param name="hSource"></param>
        public void Load(string hotel, string cityCode, HotelBookingSource hSource)
        {
            Trace.TraceInformation("HotelStaticData.Load entered : hotelCode = " + hotel + "HotelSource = " + hSource.ToString());
            if (hotelCode != null && hotelCode.Length == 0)
            {
                throw new ArgumentException("hotelCode Should be enterd !", "hotelCode");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@hotelCode", hotel);
            paramList[1] = new SqlParameter("@cityCode", cityCode);
            paramList[2] = new SqlParameter("@source", (int)hSource);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetHotelDataByCode, paramList, connection);
            List<HotelRoom> hotelRooms = new List<HotelRoom>();
            if (data.Read())
            {
                hotelCode = data["hotelCode"].ToString();
                hotelName = data["hotelName"].ToString();
                cityCode = data["hotelCity"].ToString();
                rating = (HotelRating)data["hotelRating"];
                hotelLocation = data["location"].ToString();
                hotelDescription = data["description"].ToString();
                hotelPolicy = data["hotelPolicy"].ToString();
                hotelAddress = data["address"].ToString();
                specialAttraction = data["specialAttraction"].ToString();
                hotelMap = data["hotelMaps"].ToString();
                hotelFacilities = data["hotelFacility"].ToString();
                phoneNumber = data["phoneNumber"].ToString();
                faxNumber = data["faxNumber"].ToString();
                eMail = data["eMail"].ToString();
                url = data["URL"].ToString();
                pinCode = data["pinCode"].ToString();
                source = (HotelBookingSource)Convert.ToInt16(data["source"]);
                timeStamp = Convert.ToDateTime(data["lastUpdate"]);
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("HotelStaticData.Load exit :");
        }
        /// <summary>
        /// This Method is used to Save the Static Data.
        /// </summary>

        public void Save()
        {
            Trace.TraceInformation("HotelStaticData.Save entered.");

            SqlParameter[] paramList = new SqlParameter[17];
            paramList[0] = new SqlParameter("@hotelCode", hotelCode);
            paramList[1] = new SqlParameter("@hotelName", hotelName);
            paramList[2] = new SqlParameter("@hotelCity", cityCode);
            paramList[3] = new SqlParameter("@hotelMaps", hotelMap);
            paramList[4] = new SqlParameter("@location", hotelLocation);
            paramList[5] = new SqlParameter("@description", hotelDescription);
            paramList[6] = new SqlParameter("@address", hotelAddress);
            paramList[7] = new SqlParameter("@specialAttraction", specialAttraction);
            paramList[8] = new SqlParameter("@hotelFacility", hotelFacilities);
            paramList[9] = new SqlParameter("@hotelRating", Convert.ToInt16(rating));
            paramList[10] = new SqlParameter("@phoneNumber", phoneNumber);
            paramList[11] = new SqlParameter("@faxNumber", faxNumber);
            paramList[12] = new SqlParameter("@eMail", eMail);
            paramList[13] = new SqlParameter("@URL", url);
            paramList[14] = new SqlParameter("@pinCode", pinCode);
            paramList[15] = new SqlParameter("@source", (int)source);
            paramList[16] = new SqlParameter("@hotelPolicy", hotelPolicy);

            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddHotelData, paramList);
            Trace.TraceInformation("HotelstaticData.Save exiting");
        }
        /// <summary>
        /// This Method is used to Update the entire static Data.
        /// </summary>
        public void Update()
        {
            Trace.TraceInformation("HotelStaticData.Update entered.");
            SqlParameter[] paramList = new SqlParameter[17];
            paramList[0] = new SqlParameter("@hotelCode", hotelCode);
            paramList[1] = new SqlParameter("@hotelName", hotelName);
            paramList[2] = new SqlParameter("@hotelCity", cityCode);
            paramList[3] = new SqlParameter("@hotelMaps", hotelMap);
            paramList[4] = new SqlParameter("@location", hotelLocation);
            paramList[5] = new SqlParameter("@description", hotelDescription);
            paramList[6] = new SqlParameter("@address", hotelAddress);
            paramList[7] = new SqlParameter("@specialAttraction", specialAttraction);
            paramList[8] = new SqlParameter("@hotelFacility", hotelFacilities);
            paramList[9] = new SqlParameter("@hotelRating", Convert.ToInt16(rating));
            paramList[10] = new SqlParameter("@phoneNumber", phoneNumber);
            paramList[11] = new SqlParameter("@faxNumber", faxNumber);
            paramList[12] = new SqlParameter("@eMail", eMail);
            paramList[13] = new SqlParameter("@URL", url);
            paramList[14] = new SqlParameter("@pinCode", pinCode);
            paramList[15] = new SqlParameter("@source", (int)source);
            paramList[16] = new SqlParameter("@hotelPolicy", hotelPolicy);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateHotelStaticData, paramList);
            Trace.TraceInformation("HotelstaticData.Update exiting");
        }
        /// <summary>
        /// This Method is used to Update the location.
        /// </summary>
        public void UpdateLocation()
        {
            Trace.TraceInformation("HotelStaticData.UpdateLocation entered.");
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@hotelCode", hotelCode);
            paramList[1] = new SqlParameter("@hotelCity", cityCode);
            paramList[2] = new SqlParameter("@location", hotelLocation);
            paramList[3] = new SqlParameter("@source", (int)source);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateHotelStaticDataLocation, paramList);
            Trace.TraceInformation("HotelstaticData.UpdateLocation exiting");
        }
        #endregion
    }
}