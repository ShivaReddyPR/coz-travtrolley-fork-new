using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Technology.Data;

namespace Technology.BookingEngine
{
    public class CalendarViewBooking
    {
        private string firstName;
        private string lastName;
        private string pnr;
        private string itineraryString;
        private string flightNumber;
        private DateTime traveldate;
        private int flightId;
        private int bookingId;
        private int paxCount;
        private BookingStatus status;


        #region Public properties

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }
        public string PNR
        {
            get
            {
                return pnr;
            }
            set
            {
                pnr = value;
            }
        }
        public string ItineraryString
        {
            get
            {
                return itineraryString;
            }
            set
            {
                itineraryString = value;
            }
        }
        public string FlightNumber
        {
            get
            {
                return flightNumber;
            }
            set
            {
                flightNumber = value;
            }
        }
        public DateTime TravelDate
        {
            get
            {
                return traveldate;
            }
            set
            {
                traveldate = value;
            }
        }
        public int FlightId
        {
            get
            {
                return flightId;
            }
            set
            {
                flightId = value;
            }
        }
        public int BookingId
        {
            get
            {
                return bookingId;
            }
            set
            {
                bookingId = value;
            }
        }
        public int PaxCount
        {
            get
            {
                return paxCount;
            }
            set
            {
                paxCount = value;
            }
        }
        public BookingStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        #endregion

        public static List<CalendarViewBooking> GetBookingsInAMonth(int agencyId, DateTime startDate, DateTime endDate)
        {
            List<CalendarViewBooking> calendarBookingList = new List<CalendarViewBooking>();
            using (SqlConnection con = Dal.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@agencyId", agencyId);
                paramList[1] = new SqlParameter("@startDate", startDate);
                paramList[2] = new SqlParameter("@endDate", endDate);
                using (SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetBookingsInAMonthForAgency, paramList, con))
                {
                    while (dataReader.Read())
                    {
                        CalendarViewBooking booking = new CalendarViewBooking();
                        booking.pnr = Convert.ToString(dataReader["PNR"]).Trim();
                        booking.bookingId = Convert.ToInt32(dataReader["bookingId"]);
                        booking.flightId = Convert.ToInt32(dataReader["flightId"]);
                        booking.flightNumber = Convert.ToString(dataReader["flightNumber"]);
                        booking.itineraryString = Convert.ToString(dataReader["origin"]) + "-" + Convert.ToString(dataReader["destination"]);
                        booking.lastName = Convert.ToString(dataReader["lastName"]);
                        booking.firstName = Convert.ToString(dataReader["firstName"]);
                        booking.paxCount = Convert.ToInt32(dataReader["paxCount"]);
                        booking.traveldate = Convert.ToDateTime(dataReader["traveldate"]);
                        booking.status = (BookingStatus)Enum.Parse(typeof(BookingStatus), dataReader["bookingStatus"].ToString()); ;
                        calendarBookingList.Add(booking);
                    }
                    dataReader.Close();
                }
            }

            return calendarBookingList;
        }
    }
}
