using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Diagnostics;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace Technology.BookingEngine
{
    public enum Status : int
    {
        NotSaved = 0,
        Saved = 1,
        Removed = 2
    }
    public class FailedBooking
    {
        private int failedBookingId;
        private int memberId;
        private string pnr;
        /// Assignig UAPI UR
        string universalRecord=string.Empty;// For 
        /// Assignig UAPI Provider PNR (Gelileo)
        string providerPNR = string.Empty;// 
        private int agencyId;
        private DateTime createdOn;
        private Status currentStatus;
        private DateTime lastModifiedOn;
        private BookingSource source;
        private string agencyName;
        private string itineraryXML;

        public int FailedBookingId
        {
            get { return failedBookingId; }
            set { failedBookingId=value; }
        }

        public int MemberId
        {
            get { return memberId; }
            set { memberId = value; }
        }
        /// Assing UAPI Pricing Solution Key
        /// </summary>
        public string UniversalRecord
        {
            get
            {
                return universalRecord;
            }
            set
            {
                universalRecord = value;
            }
        }

        /// <summary>
        /// Assing UAPI Pricing Solution Key
        /// </summary>
        public string ProviderPNR
        {
            get
            {
                return providerPNR;
            }
            set
            {
                providerPNR = value;
            }
        }
        public string PNR
        {
            get { return pnr; }
            set { pnr = value; }
        }
        public int AgencyId
        {
            get { return agencyId; }
            set { agencyId = value; }
        }
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }
        public Status CurretnStatus
        {
            get { return currentStatus; }
            set { currentStatus = value; }
        }
        public BookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        public string AgencyName
        {
            get { return agencyName; }
            set { agencyName = value; }
        }
        public string ItineraryXML
        {
            get { return itineraryXML; }
            set { itineraryXML = value; }
        }
        public void Save()
        {
            Trace.TraceInformation("FailedBooking.Save entered:");
            SqlParameter[] paramList = new SqlParameter[9];
            paramList[0] = new SqlParameter("@memberId", memberId);
            paramList[1] = new SqlParameter("@universalRecord", universalRecord);
            paramList[2] = new SqlParameter("@providerPNR", ProviderPNR);
            paramList[3] = new SqlParameter("@pnr", pnr);
            paramList[4] = new SqlParameter("@agencyId", agencyId);
            paramList[5] = new SqlParameter("@currentStatus", (int)currentStatus);
            paramList[6] = new SqlParameter("@source", (int)source);
            paramList[7] = new SqlParameter("@agencyName", agencyName);
            paramList[8] = new SqlParameter("@itineraryXML",itineraryXML);
            Dal.ExecuteNonQuerySP(SPNames.AddFailedBooking, paramList);
            Trace.TraceInformation("FailedBooking.Save exited");
        }
        public static FailedBooking Load(string pnrNo)
        {
            Trace.TraceInformation("FailedBooking.Load entered:");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@pnr", pnrNo);
            SqlConnection con = Dal.GetConnection();
            SqlDataReader dr = Dal.ExecuteReaderSP(SPNames.GetFailedBooking, paramList, con);
            FailedBooking fb = new FailedBooking();
            if (dr.Read())
            {
                fb.failedBookingId = Convert.ToInt32(dr["failedBookingId"]);
                fb.universalRecord = Convert.ToString(dr["universalRecord"]);
                fb.providerPNR = Convert.ToString(dr["providerPNR"]);
                fb.pnr = Convert.ToString(dr["pnr"]);
                fb.pnr = Convert.ToString(dr["pnr"]);
                fb.failedBookingId = Convert.ToInt32(dr["failedBookingId"]);
                fb.agencyId = Convert.ToInt32(dr["agencyId"]);
                fb.memberId = Convert.ToInt32(dr["memberId"]);
                fb.createdOn = Convert.ToDateTime(dr["createdOn"]);
                fb.currentStatus = (Status)Enum.Parse(typeof(Status), dr["currentStatus"].ToString());
                fb.lastModifiedOn = Convert.ToDateTime(dr["lastModifiedOn"]);
                fb.agencyName = Convert.ToString(dr["agencyName"]);
                fb.source = (BookingSource)Enum.Parse(typeof(BookingSource), dr["source"].ToString());
            }
            dr.Close();
            con.Close();
            Trace.TraceInformation("FailedBooking.Load exited:");
            return fb;
        }
        public static FailedBooking Load(int agencyIdNo, string pnrNo)
        {
            Trace.TraceInformation("FailedBooking.LoadForAgency entered:");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@agencyId", agencyIdNo);
            paramList[1] = new SqlParameter("@pnr", pnrNo);
            SqlConnection con = Dal.GetConnection();
            SqlDataReader dr = Dal.ExecuteReaderSP(SPNames.GetPNRFailedBookingForAgency, paramList, con);
            FailedBooking fb = new FailedBooking();
            if (dr.Read())
            {
                fb.failedBookingId = Convert.ToInt32(dr["failedBookingId"]);
                fb.universalRecord = Convert.ToString(dr["universalRecord"]);
                fb.providerPNR = Convert.ToString(dr["providerPNR"]);
                fb.pnr = Convert.ToString(dr["pnr"]);
                fb.failedBookingId = Convert.ToInt32(dr["failedBookingId"]);
                fb.agencyId = Convert.ToInt32(dr["agencyId"]);
                fb.memberId = Convert.ToInt32(dr["memberId"]);
                fb.createdOn = Convert.ToDateTime(dr["createdOn"]);
                fb.currentStatus = (Status)Enum.Parse(typeof(Status), dr["currentStatus"].ToString());
                fb.agencyName = Convert.ToString(dr["agencyName"]);
                fb.lastModifiedOn = Convert.ToDateTime(dr["lastModifiedOn"]);
                fb.source = (BookingSource)Enum.Parse(typeof(BookingSource), dr["source"].ToString());
            }
            dr.Close();
            con.Close();
            Trace.TraceInformation("FailedBooking.LoadForAgency exited:");
            return fb;
        }

        public static FlightItinerary LoadItinerary(int pendingId)
        {
            Trace.TraceInformation("FailedBooking.LoadItinerary entered:");
            string strItinerary = string.Empty; ;
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@failedBookingId", pendingId);
            SqlConnection con = Dal.GetConnection();
            SqlDataReader dr = Dal.ExecuteReaderSP(SPNames.GetFailedBookingItinerary, paramList, con);
            if (dr.Read())
            {
              strItinerary = Convert.ToString(dr["itineraryXML"]);
            }
            dr.Close();
            con.Close();
            Trace.TraceInformation("FailedBooking.LoadItinerary exited:");
            return GetObject(strItinerary);
        }


        public static bool CheckPNR(string pnrNo)
        {
            Trace.TraceInformation("FailedBooking.CheckPNR entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@pnr", pnrNo);
            paramList[1] = new SqlParameter("@isPresent", SqlDbType.Bit);
            paramList[1].Direction = ParameterDirection.Output;
            Dal.ExecuteNonQuerySP(SPNames.CheckFailedBookingPNR, paramList);
            Trace.TraceInformation("FailedBooking.CheckPNR exited");
            return Convert.ToBoolean(paramList[1].Value);
        }

        public static void UpdateStatus(string pnrNo, Status stat, string remarks)
        {
            Trace.TraceInformation("FailedBooking.UpdateStatus entered");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@pnr", pnrNo);
            paramList[1] = new SqlParameter("@currentStatus", (int)stat);
            paramList[2] = new SqlParameter("@remarks", remarks);
            Dal.ExecuteNonQuerySP(SPNames.UpdateFailedBookingStatus, paramList);
            Trace.TraceInformation("FailedBooking.UpdateStatus exited");
        }
     
        public static List<FailedBooking> GetAllFailedBooking(int lower, int upper)
        {
            Trace.TraceInformation("FailedBooking.GetAllFailedBooking entered");
            List<FailedBooking> lstFailedBooking = new List<FailedBooking>();

            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@lower", lower);
            paramList[1] = new SqlParameter("@upper", upper);
            SqlConnection con = Dal.GetConnection();
            SqlDataReader dr = Dal.ExecuteReaderSP(SPNames.GetAllFailedBooking, paramList, con);
            while (dr.Read())
            {
                FailedBooking fb = new FailedBooking();
                fb.pnr = Convert.ToString(dr["pnr"]);
                fb.source = (BookingSource)Enum.Parse(typeof(BookingSource), dr["source"].ToString());
                fb.agencyId = Convert.ToInt32(dr["agencyId"]);
                fb.memberId = Convert.ToInt32(dr["memberId"]);
                fb.createdOn = Convert.ToDateTime(dr["createdOn"]);
                fb.currentStatus = (Status)Enum.Parse(typeof(Status), dr["currentStatus"].ToString());
                fb.lastModifiedOn = (DateTime)dr["lastModifiedOn"];
                fb.agencyName = Convert.ToString(dr["agencyName"]);
                lstFailedBooking.Add(fb);
            }
            dr.Close();
            con.Close();
            Trace.TraceInformation("FailedBooking.GetAllFailedBooking exited");
            return lstFailedBooking;
        }

        public static int GetCount()
        {
            Trace.TraceInformation("FailedBooking.GetCount entered");
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@count", SqlDbType.Int);
            paramList[0].Direction = ParameterDirection.Output;
            Technology.Data.Dal.ExecuteNonQuerySP(SPNames.CountFailedBooking, paramList);
            Trace.TraceInformation("FailedBooking.GetCount exited");
            return (int)paramList[0].Value;

        }
        public static int GetCount(int agencyIdNo)
        {
            Trace.TraceInformation("FailedBooking.GetCount entered");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@agencyId", agencyIdNo);
            paramList[1] = new SqlParameter("@count", SqlDbType.Int);
            paramList[1].Direction = ParameterDirection.Output;
            Technology.Data.Dal.ExecuteNonQuerySP(SPNames.CountFailedBookingForAgency, paramList);
            Trace.TraceInformation("FailedBooking.GetCount exited");
            return (int)paramList[1].Value;

        }

        public static List<FailedBooking> GetAllFailedBooking(int agencyIdNo, int lower, int upper)
        {
            Trace.TraceInformation("FailedBooking.GetFailedBookingForAgency entered");
            List<FailedBooking> lstFailedBooking = new List<FailedBooking>();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@agencyId", agencyIdNo);
            paramList[1] = new SqlParameter("@lower", lower);
            paramList[2] = new SqlParameter("@upper", upper);
            SqlConnection con = Dal.GetConnection();
            SqlDataReader dr = Dal.ExecuteReaderSP(SPNames.GetFailedBookingForAgency, paramList, con);
            while (dr.Read())
            {
                FailedBooking fb = new FailedBooking();
                fb.failedBookingId=Convert.ToInt32(dr["failedBookingId"]);
                fb.pnr = Convert.ToString(dr["pnr"]);
                fb.source = (BookingSource)Enum.Parse(typeof(BookingSource), dr["source"].ToString());
                fb.agencyId = Convert.ToInt32(dr["agencyId"]);
                fb.memberId = Convert.ToInt32(dr["memberId"]);
                fb.createdOn = Convert.ToDateTime(dr["createdOn"]);
                fb.currentStatus = (Status)Enum.Parse(typeof(Status), dr["currentStatus"].ToString());
                fb.lastModifiedOn = (DateTime)dr["lastModifiedOn"];
                fb.agencyName = Convert.ToString(dr["agencyName"]);
                fb.itineraryXML=Convert.ToString(dr["itineraryXML"]);
                lstFailedBooking.Add(fb);
            }
            dr.Close();
            con.Close();
            Trace.TraceInformation("FailedBooking.GetFailedBookingForAgency exited");
            return lstFailedBooking;
        }

        public static string GetXML(FlightItinerary itinerary)
        {
            Trace.TraceInformation("FailedBooking.GetXML entered:");
            XmlDocument doc = new XmlDocument();
            XmlSerializer serializer = new XmlSerializer(typeof(FlightItinerary));
            MemoryStream stream = new MemoryStream();
            try
            {
                serializer.Serialize(stream, itinerary);
                stream.Position = 0;
                doc.Load(stream);
                return doc.InnerXml.Trim();
            }
            catch(Exception ex)
            {
                try
                {
                    string messageText = "PNR = " + itinerary.PNR + "\r\n Booking Source is " + itinerary.FlightBookingSource.ToString() + "\r\n. PNR created successfully but itinerary failed to serialize.";
                    string message = Util.GetExceptionInformation(ex, "Itinerary Serialization failed.\r\n" + messageText);
                    CoreLogic.Audit.Add(CoreLogic.EventType.Exception, CoreLogic.Severity.High, 0, message, string.Empty);
                    CoreLogic.Email.Send("Itinerary Serialization Failed", message);
                }
                catch
                {
                }
                return "Object can't be serailized";
            }
            finally
            {
                stream.Close();
                Trace.TraceInformation("FailedBooking.GetXML exited");
            }
        }

        public static FlightItinerary GetObject(string itinerary)
        {
            StringReader strReader = null;
            strReader = new StringReader(itinerary);
            XmlSerializer serializer = new XmlSerializer(typeof(FlightItinerary));
            XmlReader xmlRead = new XmlTextReader(strReader);
            try
            {
                FlightItinerary fltItinerary = (FlightItinerary)serializer.Deserialize(xmlRead);
                return fltItinerary;
            }
            catch
            {
                return null;
            }
            finally
            {
                strReader.Close();
                xmlRead.Close();
            }
        }
    }
}
