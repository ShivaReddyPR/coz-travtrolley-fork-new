using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;

namespace Technology.BookingEngine
{
    public enum HotelPaxType
    {
        Adult=1,
        Child=2
    }
    [Serializable ]
    public class HotelPassenger
    {

        int paxId;
        int hotelId;
        string title;
        string firstname;
        string middlename;
        string lastname;
        string phoneno; // number node will take the same value
        string countrycode;
        string areacode;
        string email;
        string addressline1;
        string addressline2;
        string city;
        string zipcode;
        string state;
        string country;
        HotelPaxType paxType;
        int roomId;
        bool leadPasseneger;
        int age;

        # region public properties
        public int PaxId
        {
            get { return paxId; }
            set { paxId = value; }
        }

        public int HotelId
        {
            get { return hotelId; }
            set { hotelId = value; }
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string Firstname
        {
            get { return firstname; }
            set { firstname = value; }
        }

        public string Middlename
        {
            get { return middlename; }
            set { middlename = value; }
        }

        public string Lastname
        {
            get { return lastname; }
            set { lastname = value; }
        }

        public string Phoneno
        {
            get { return phoneno; }
            set { phoneno = value; }
        }

        public string Countrycode
        {
            get { return countrycode; }
            set { countrycode = value; }
        }

        public string Areacode
        {
            get { return areacode; }
            set { areacode = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string Addressline1
        {
            get { return addressline1; }
            set { addressline1 = value; }
        }

        public string Addressline2
        {
            get { return addressline2; }
            set { addressline2 = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string Zipcode
        {
            get { return zipcode; }
            set { zipcode = value; }
        }

        public string State
        {
            get { return state; }
            set { state = value; }
        }

        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        public HotelPaxType PaxType
        {
            get { return paxType; }
            set { paxType = value; }
        }
        public int RoomId
        {
            get { return roomId; }
            set { roomId = value; }
        }
        public bool LeadPassenger
        {
            get { return leadPasseneger; }
            set { leadPasseneger = value; }
        }
        public int Age
        {
            get { return age; }
            set { age = value; }
        }
        #endregion

        # region Methods
        /// <summary>
        /// This Method is used to Save the Hotel Passenger details
        /// </summary>
        /// <returns></returns>
        public int Save()
        { 
            //TODO: validate that the fields are properly set before calling the SP
            Trace.TraceInformation("HotelPassenger.Save entered.");
            SqlParameter[] paramList = new SqlParameter[19];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            paramList[1] = new SqlParameter("@firstName", firstname);
            paramList[2] = new SqlParameter("@lastName", lastname);
            paramList[3] = new SqlParameter("@email", email);
            paramList[4] = new SqlParameter("@phone", phoneno);

            paramList[5] = new SqlParameter("@countryCode", countrycode);
    
            paramList[6] = new SqlParameter("@address1", addressline1);

            if (addressline2 != null)
            {
                paramList[7] = new SqlParameter("@address2", addressline2);
            }
            else
            {
                paramList[7] = new SqlParameter("@address2", ""); 
            }

            paramList[8] = new SqlParameter("@areaCode", areacode);
            paramList[9] = new SqlParameter("@city", city);
            paramList[10] = new SqlParameter("@zipCode", zipcode);
            paramList[11] = new SqlParameter("@state", state);
            paramList[12] = new SqlParameter("@country", country);          
           
            paramList[13] = new SqlParameter("@paxId", SqlDbType.Int);
            paramList[13].Direction = ParameterDirection.Output;
            paramList[14] = new SqlParameter("@paxType", paxType);
            paramList[15] = new SqlParameter("@title", title);
            paramList[16] = new SqlParameter("@leadPassenger", leadPasseneger);
            paramList[17] = new SqlParameter("@age", age);
            paramList[18] = new SqlParameter("@roomId", roomId);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddHotelPassenger, paramList);
            PaxId = (int)paramList[13].Value;   

            Trace.TraceInformation("HotelPassenger.Save exiting");

            return PaxId;
        }
        /// <summary>
        /// This Method is used to load Lead Passenger details
        /// </summary>
        /// <param name="hotelId"></param>
        public void Load(int hotelId)
        {
            Trace.TraceInformation("HotelPassenger.Load entered : hotelId = " + hotelId);
            if (hotelId <= 0)
            {
                throw new ArgumentException("HotelId should be positive integer", "hotelId");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetHotelPassengerByHotelId, paramList, connection);
            if (data.Read())
            {
                paxId = Convert.ToInt32(data["paxId"]);
                paxType =(HotelPaxType)Convert.ToInt16(data["paxType"]);
                hotelId = Convert.ToInt32(data["hotelId"]);
                firstname = data["firstname"].ToString();
                if (data["lastname"] != null)
                {
                    lastname = data["lastname"].ToString();
                }
                email = data["email"].ToString();
                phoneno = data["phone"].ToString();
                countrycode = data["countrycode"].ToString();
                addressline1 = data["address1"].ToString();
                if (data["address2"] != DBNull.Value)
                {
                    addressline2 = data["address2"].ToString();
                }
                else
                {
                    addressline2 = "";
                }
                areacode = data["areacode"].ToString();
                city = data["city"].ToString();
                zipcode = data["zipcode"].ToString();
                state = data["state"].ToString();
                country = data["country"].ToString();
                if (data["title"] != null)
                {
                    title = data["title"].ToString();
                }
                if (data["age"] != DBNull.Value)
                {
                    age = Convert.ToInt32(data["age"]);
                }
                if (data["roomId"] != DBNull.Value)
                {
                    roomId = Convert.ToInt32(data["roomId"]);
                }
                leadPasseneger = Convert.ToBoolean(data["leadPassenger"]);
                data.Close();
                connection.Close();
                Trace.TraceInformation("HotelPassenger.Load exiting :" + hotelId.ToString());
            }
            else
            {
                data.Close();
                connection.Close();
                Trace.TraceInformation("HotelPassenger.Load exiting : hotelId does not exist.hotelId = " + hotelId.ToString());
                throw new ArgumentException("Hotel id does not exist in database");
            }          
           
            Trace.TraceInformation("HotelPassenger.Load exiting.");
        }
        /// <summary>
        /// This Method is used to load all Passenger details of a particular Room
        /// </summary>
        /// <param name="hotelId"></param>
        /// <param name="roomId"></param>
        /// <returns></returns>
        public List<HotelPassenger>  Load(int hotelId, int roomId)
        {
            Trace.TraceInformation("HotelPassenger.Load entered : hotelId = " + hotelId);
            if (hotelId <= 0)
            {
                throw new ArgumentException("HotelId should be positive integer", "hotelId");
            }
            List<HotelPassenger> hotelPaxList=new List<HotelPassenger>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            paramList[1]=new SqlParameter("@roomId",roomId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetHotelPassengerByHotelIdRoomId, paramList, connection);
            while (data.Read())
            {
                HotelPassenger passInfo=new HotelPassenger();
                passInfo.PaxId = Convert.ToInt32(data["paxId"]);
                passInfo.PaxType = (HotelPaxType)Convert.ToInt16(data["paxType"]);
                passInfo.HotelId = Convert.ToInt32(data["hotelId"]);
                if (data["firstname"] != DBNull.Value)
                {
                    passInfo.Firstname = data["firstname"].ToString();
                }
                if (data["lastname"] != DBNull.Value)
                {
                    passInfo.Lastname = data["lastname"].ToString();
                }
                if (data["email"] != DBNull.Value)
                {
                    passInfo.Email = data["email"].ToString();
                }
                if (data["phone"] != DBNull.Value)
                {
                    passInfo.Phoneno = data["phone"].ToString();
                }
                if (data["countrycode"] != DBNull.Value)
                {
                    passInfo.Countrycode = data["countrycode"].ToString();
                }
                if (data["address1"] != DBNull.Value)
                {
                    passInfo.Addressline1 = data["address1"].ToString();
                }
                if (data["address2"] != DBNull.Value)
                {
                   passInfo.Addressline2 = data["address2"].ToString();
                }
                else
                {
                   passInfo.Addressline2 = "";
                }
                if (data["areacode"] != DBNull.Value)
                {
                    passInfo.Areacode = data["areacode"].ToString();
                }
                if (data["city"] != DBNull.Value)
                {
                    passInfo.City = data["city"].ToString();
                }
                if (data["zipcode"] != DBNull.Value)
                {
                    passInfo.Zipcode = data["zipcode"].ToString();
                }
                if (data["state"] != DBNull.Value)
                {
                    passInfo.State = data["state"].ToString();
                }
                if (data["country"] != DBNull.Value)
                {
                    passInfo.Country = data["country"].ToString();
                }
                if (data["title"] != null)
                {
                    passInfo.Title = data["title"].ToString();
                }
                if (data["age"] != DBNull.Value)
                {
                    passInfo.Age = Convert.ToInt32(data["age"]);
                }
                passInfo.RoomId = Convert.ToInt32(data["roomId"]);
                passInfo.LeadPassenger = Convert.ToBoolean(data["leadPassenger"]);
                hotelPaxList.Add(passInfo);
              
            }
           data.Close();
           connection.Close();
           Trace.TraceInformation("HotelPassenger.Load exiting.");
           return hotelPaxList;
        }
        /// <summary>
        /// This menthod is used to update the Hotel Passeneger Information.
        /// </summary>
        public void Update()
        {
            Trace.TraceInformation("HotelPassenger.Update entered.");
            SqlParameter[] paramList = new SqlParameter[4];
            paramList[0] = new SqlParameter("@firstName", firstname);
            paramList[1] = new SqlParameter("@lastName", lastname);
            paramList[2] = new SqlParameter("@paxId", paxId);
            paramList[3] = new SqlParameter("@leadPassenger", leadPasseneger);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateHotelPassenger, paramList);
           Trace.TraceInformation("HotelPassenger.Update exiting");


        }
        # endregion
    }
}
