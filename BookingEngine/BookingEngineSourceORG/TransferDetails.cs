using System;
using System.Collections.Generic;
using System.Text;

namespace Technology.BookingEngine
{
    public class TransferDetails
    {
        # region private variables
        int transferId;
        string vehicleCode;
        string vechicleName;
        int maxPax;
        int maxLauggage;
        int occupiedPax;
        int leadPax;
        #endregion

        #region Public Properties
        /// <summary>
        /// Transfer Id .Its a foregin Key
        /// </summary>
        public int TransferId
        {
            get { return transferId;}
            set { transferId=value;}
        }
        /// <summary>
        /// Vehicle Code
        /// </summary>
        public string VehicleCode
        {
            get { return vehicleCode;}
            set { vehicleCode=value;}
        }
        /// <summary>
        /// Name of the Vehicle
        /// </summary>
        public string VehicleName
        {
            get { return vechicleName;}
            set { vechicleName=value;}
        }
        /// <summary>
        ///  Max Pax occupied for this Vehicle
        /// </summary>
        public int MaxPax
        {
            get { return maxPax; }
            set { maxPax = value; }
        }
        /// <summary>
        /// Max Laggauge Allowed
        /// </summary>
        public int MaxLaggauge
        {
            get { return maxLauggage; }
            set { maxLauggage = value; }
        }
        /// <summary>
        /// No of Pax for this Vehicle
        /// </summary>
        public int OccupiedPax
        {
            get { return occupiedPax; }
            set { occupiedPax = value; }
        }
        /// <summary>
        /// Lead Pax Id
        /// </summary>
        public int LeadPax
        {
            get { return leadPax; }
            set { leadPax = value; }
        }
       

        #endregion

# region Methods
        /// <summary>
        /// This Method is used to Save the Data
        /// </summary>
        public void Save()
        {


        }
        /// <summary>
        /// This Method is used to Load the Transfer Details Corresponding to TransferId
        /// </summary>
        /// <param name="transferId"></param>
        /// <returns></returns>
        public List<TransferDetails> LoadByTransferId(int transferId)
        {
            List<TransferDetails> transferDetails = new List<TransferDetails>();
            return transferDetails;
        }
#endregion

    }
}
