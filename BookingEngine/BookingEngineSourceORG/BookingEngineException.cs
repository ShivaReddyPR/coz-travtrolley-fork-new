using System;
using System.Collections.Generic;
using System.Text;

namespace Technology.BookingEngine
{
    public class BookingEngineException : Exception
    {
        /// <summary>
        /// Exception number;
        /// </summary>
        private int errorCode;
        /// <summary>
        /// Gets the exception number.
        /// </summary>
        public int ErrorCode
        {
            get { return errorCode; }
        }

        public BookingEngineException(string message)
            : base(message)
        {
            errorCode = 0;
        }

        public BookingEngineException(int errorCode, string message)
            : base(message)
        {
            this.errorCode = errorCode;
        }
    }
}
