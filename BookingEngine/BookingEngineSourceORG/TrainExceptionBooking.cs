using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Technology.Data;
using System.Data.SqlClient;
using System.Data;
using Technology.BookingEngine;

namespace Technology.BookingEngine
{
    public enum TrainExceptionBookingStatus
    { 
        Unsaved = 1,
        Saved = 2
    }
    public class TrainExceptionBooking
    {
        string uniqueTxnId;
        string pnr;
        string sector;
        int agencyId;
        string agencyName;
        DateTime createdOn;
        int createdBy;
        DateTime lastModifiedOn;
        int lastModifiedBy;
        TrainExceptionBookingStatus status;

        public string UniqueTxnId
        {
            get { return uniqueTxnId; }
            set { uniqueTxnId = value; }
        }
        public string Pnr
        {
            get { return pnr; }
            set { pnr = value; }
        }
        public int AgencyId
        {
            get { return agencyId; }
            set { agencyId = value; }
        }
        public string AgencyName
        {
            get { return agencyName; }
            set { agencyName = value; }
        }
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        public DateTime LastModifiedOn
        {
            get { return lastModifiedOn; }
            set { lastModifiedOn = value; }
        }
        public int LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }
        public TrainExceptionBookingStatus Status
        {
            get { return status; }
            set { status = value; }
        }
        public string Sector
        {
            get { return sector; }
            set { sector = value; }
        }
        public int Save()
        {
            Trace.TraceInformation("TrainExceptionBooking.save entered");
            ValidateData();
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[6];
            paramList[0] = new SqlParameter("@uniqueTxnId", uniqueTxnId.Trim());
            paramList[1] = new SqlParameter("@pnr", pnr.Trim());
            paramList[2] = new SqlParameter("@agencyId", agencyId);
            paramList[3] = new SqlParameter("@status", status);
            paramList[4] = new SqlParameter("@sector", sector);
            paramList[5] = new SqlParameter("@createdBy", createdBy);
            rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddTrainExceptionBooking, paramList);
            Trace.TraceInformation("TrainExceptionBooking.save exited");
            return rowsAffected;
        }
        public int Update()
        {
            Trace.TraceInformation("TrainExceptionBooking.Update entered");
            ValidateData();
            int rowsAffected = 0;
            SqlParameter[] paramList = new SqlParameter[6];
            paramList[0] = new SqlParameter("@uniqueTxnId", uniqueTxnId.Trim());
            paramList[1] = new SqlParameter("@pnr", pnr.Trim());
            paramList[2] = new SqlParameter("@agencyId", agencyId);
            paramList[3] = new SqlParameter("@status", status);
            paramList[4] = new SqlParameter("@sector", sector);
            paramList[5] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateTrainExceptionBooking, paramList);
            Trace.TraceInformation("TrainExceptionBooking.save exited");
            return rowsAffected;
        }
        private void ValidateData()
        {
            if (uniqueTxnId == null || uniqueTxnId.Trim().Length == 0)
            {
                throw new ArgumentException("uniqueTxnId cannot be null or empty");
            }
            if (pnr == null || pnr.Trim().Length == 0)
            {
                throw new ArgumentException("pnr cannot be null or empty");
            }
            if (agencyId == 0)
            {
                throw new ArgumentException("AgencyId cannot be zero");
            }          
            if ((int)status == 0)
            {
                throw new ArgumentException("status need to be provided");
            }             
        }
        public void Load(string uniqueTxnId)
        {
            Trace.TraceInformation("TrainExceptionBooking.Load entered uniqueTxnId = " + uniqueTxnId);
            if (uniqueTxnId == null || uniqueTxnId.Trim().Length == 0)
            {
                throw new ArgumentException("uniqueTxnId cannot be null or empty");
            }
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@uniqueTxnId", uniqueTxnId);
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlDataReader reader = Dal.ExecuteReaderSP(SPNames.GetTrainExceptionBooking, paramList, connection);
                if (reader.Read())
                {
                    ReadData(this, reader);
                }
                reader.Close();
            }
            Trace.TraceInformation("TrainExceptionBooking.Load exited");
        }
        public void LoadByPnr(string pnr)
        {
            Trace.TraceInformation("TrainExceptionBooking.LoadByPnr entered pnr = " + pnr);
            if (pnr == null || pnr.Trim().Length == 0)
            {
                throw new ArgumentException("pnr cannot be null or empty");
            }
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@pnr", pnr);
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlDataReader reader = Dal.ExecuteReaderSP(SPNames.GetTrainExceptionBookingByPnr, paramList, connection);
                if (reader.Read())
                {
                    ReadData(this, reader);
                }
                reader.Close();
            }
            Trace.TraceInformation("TrainExceptionBooking.LoadByPnr exited");
        }
        public static List<TrainExceptionBooking> GetAllTrainExceptionBooking()
        {
            Trace.TraceInformation("TrainExceptionBooking.GetAllTrainExceptionBooking entered ");
            List<TrainExceptionBooking> exceptionList = new List<TrainExceptionBooking>();
            SqlParameter[] paramList = new SqlParameter[0];            
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlDataReader reader = Dal.ExecuteReaderSP(SPNames.GetAllTrainExceptionBooking, paramList, connection);
                while (reader.Read())
                {
                    TrainExceptionBooking exceptionBooking = new TrainExceptionBooking();
                    ReadData(exceptionBooking, reader);
                    exceptionList.Add(exceptionBooking);
                }
                reader.Close();
            }
            Trace.TraceInformation("TrainExceptionBooking.GetAllTrainExceptionBooking exited");
            return exceptionList;
        }
        public static List<TrainExceptionBooking> GetAllTrainExceptionBookingByAgencyId( int agencyId)
        {
            Trace.TraceInformation("TrainExceptionBooking.GetAllTrainExceptionBooking entered ");
            List<TrainExceptionBooking> exceptionList = new List<TrainExceptionBooking>();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@agencyId", agencyId);
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlDataReader reader = Dal.ExecuteReaderSP(SPNames.GetAllTrainExceptionBookingByAgencyId, paramList, connection);
                while (reader.Read())
                {
                    TrainExceptionBooking exceptionBooking = new TrainExceptionBooking();
                    ReadData(exceptionBooking, reader);
                    exceptionList.Add(exceptionBooking);
                }
                reader.Close();
            }
            Trace.TraceInformation("TrainExceptionBooking.GetAllTrainExceptionBooking exited");
            return exceptionList;
        }
        public static List<string> GetUniqueTxnIDsForDate(DateTime date)
        {
            Trace.TraceInformation("TrainExceptionBooking.GetUniqueTxnIDsForDate entered ");
            List<string> uniqueTxnIDList = new List<string>();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@date", date);
            paramList[1] = new SqlParameter("@status", TrainExceptionBookingStatus.Unsaved);
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlDataReader reader = Dal.ExecuteReaderSP(SPNames.GetUniqueTxnIDsForDate, paramList, connection);
                while (reader.Read())
                {
                    string uniquetxnId = Convert.ToString(reader["uniqueTxnId"]);
                    uniqueTxnIDList.Add(uniquetxnId);
                }
                reader.Close();
            }
            Trace.TraceInformation("TrainExceptionBooking.GetUniqueTxnIDsForDate exited");
            return uniqueTxnIDList;
        }
        private static void ReadData(TrainExceptionBooking exceptionBooking, SqlDataReader reader)
        {
            Trace.TraceInformation("TrainExceptionBooking.ReadData entered");
            exceptionBooking.uniqueTxnId = Convert.ToString(reader["uniqueTxnId"]);
            exceptionBooking.pnr = Convert.ToString(reader["pnr"]);
            exceptionBooking.agencyId = Convert.ToInt32(reader["agencyId"]);
            if (reader["agencyName"] != DBNull.Value)
            {
                exceptionBooking.agencyName = Convert.ToString(reader["agencyName"]);
            }
            exceptionBooking.status = (TrainExceptionBookingStatus)(Convert.ToInt32(reader["status"]));
            exceptionBooking.sector = Convert.ToString(reader["sector"]);           
            exceptionBooking.createdOn = Convert.ToDateTime(reader["createdOn"]);
            exceptionBooking.createdBy = Convert.ToInt32(reader["createdBy"]);
            exceptionBooking.lastModifiedOn = Convert.ToDateTime(reader["lastModifiedOn"]);
            exceptionBooking.lastModifiedBy = Convert.ToInt32(reader["lastModifiedBy"]);
            Trace.TraceInformation("TrainExceptionBooking.ReadData exited");
        }
    }
}
