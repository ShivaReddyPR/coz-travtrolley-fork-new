using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;

namespace Technology.BookingEngine
{
    public enum HotelBookingStatus
    {
        Confirmed = 1,
        Cancelled = 2,
        Failed = 0,
        Pending = 3,
        Error = 4
    }

    public enum HotelBookingSource
    {
        Desiya = 1,
        GTA = 2,
        HotelBeds = 3,
        Tourico = 4,
        IAN = 5,
        TBOConnect = 6,
        Miki = 7,
        Travco = 8,
        DOTW = 9,
        WST = 10
    }
    [Serializable]
    public struct CreditCardInfo
    {
        public string eMail;
        public string fName;
        public string lName;
        public string homePhoneNo;
        public string workPhoneNo;
        public string cardType;
        public string cardNo;
        public string expMonth;
        public string expYear;
        public string cvv;
        public string address1;
        public string address2;
        public string city;
        public string stateCode;
        public string countryCode;
        public string postalCode;
        public string creditCardPasHttpUserAgent;
        public string creditCardPasHttpAccept;
        public string creditCardPasPaRes;
    }
    [Serializable]
    public class HotelItinerary : Technology.BookingEngine.Product
    {
        # region private variables
        string confirmationNo;
        int hotelId;
        string hotelCode;
        string hotelName;
        string chainCode;
        string hotelCategory;
        HotelRating rating;
        HotelRoom[] roomtype;
        DateTime startDate;
        DateTime endDate;
        HotelPassenger hotelPassenger;
        string hotelAddress1;
        string hotelAddress2;
        string cityCode;
        string cityRef;
        string specialRequest;
        HotelBookingStatus status;
        string hotelPolicyDetails;
        string hotelCancelPolicy;
        int noOfRooms;
        int createdBy;
        HotelBookingSource source;
        private bool isDomestic;
        string cancelId;
        string flightInfo;
        string bookingRefNo;
        string map;
        DateTime lastCancellationDate;
        bool voucherStatus;
        List<HotelPenality> penalityInfo;
        string agencyReference;
        string vatDescription;
        CreditCardInfo ccInfo;
        string propertyType;
        string supplierType;
        # endregion

        # region public properties
        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;

        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;

        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;

        public string ConfirmationNo
        {
            get { return confirmationNo; }
            set { confirmationNo = value; }
        }

        public int HotelId
        {
            get { return hotelId; }
            set { hotelId = value; }
        }

        public string HotelCode
        {
            get { return hotelCode; }
            set { hotelCode = value; }
        }

        public string HotelName
        {
            get { return hotelName; }
            set { hotelName = value; }
        }

        public string ChainCode
        {
            get { return chainCode; }
            set { chainCode = value; }
        }
        public string HotelCategory
        {
            get { return hotelCategory; }
            set { hotelCategory = value; }
        }

        public HotelRating Rating
        {
            get { return rating; }
            set { rating = value; }
        }

        public HotelRoom[] Roomtype
        {
            get { return roomtype; }
            set { roomtype = value; }
        }

        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }

        public HotelPassenger HotelPassenger
        {
            get { return hotelPassenger; }
            set { hotelPassenger = value; }
        }

        public string HotelAddress1
        {
            get { return hotelAddress1; }
            set { hotelAddress1 = value; }
        }

        public string HotelAddress2
        {
            get { return hotelAddress2; }
            set { hotelAddress2 = value; }
        }

        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        public string CityRef
        {
            get { return cityRef; }
            set { cityRef = value; }
        }
        public HotelBookingStatus Status
        {
            get { return status; }
            set { status = value; }
        }
        public string SpecialRequest
        {
            get { return specialRequest; }
            set { specialRequest = value; }
        }

        /// <summary>
        /// Hotel Policy details - will be added with some delimeter or new line.
        /// </summary>
        public string HotelPolicyDetails
        {
            get { return hotelPolicyDetails; }
            set { hotelPolicyDetails = value; }
        }
        public string HotelCancelPolicy
        {
            get { return hotelCancelPolicy; }
            set { hotelCancelPolicy = value; }
        }

        /// <summary>
        /// Total number of rooms requested
        /// </summary>
        public int NoOfRooms
        {
            get
            {
                return noOfRooms;
            }
            set
            {
                noOfRooms = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        public HotelBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        public bool IsDomestic
        {
            get { return isDomestic; }
            set { isDomestic = value; }
        }
        public string CancelId
        {
            get { return cancelId; }
            set { cancelId = value; }
        }
        public string FlightInfo
        {
            get { return flightInfo; }
            set { flightInfo = value; }
        }
        public string BookingRefNo
        {
            get { return bookingRefNo; }
            set { bookingRefNo = value; }
        }
        public string Map
        {
            get { return map; }
            set { map = value; }
        }
        public DateTime LastCancellationDate
        {
            get { return lastCancellationDate; }
            set { lastCancellationDate = value; }
        }
        public bool VoucherStatus
        {
            get { return voucherStatus; }
            set { voucherStatus = value; }
        }
        public List<HotelPenality> PenalityInfo
        {
            get { return penalityInfo; }
            set { penalityInfo = value; }
        }
        public string AgencyReference
        {
            get { return agencyReference; }
            set { agencyReference = value; }
        }
        public string VatDescription
        {
            get { return vatDescription; }
            set { vatDescription = value; }
        }
        public CreditCardInfo CCInfo
        {
            get { return ccInfo; }
            set { ccInfo = value; }
        }
        public string PropertyType
        {
            get { return propertyType; }
            set { propertyType = value; }
        }
        public string SupplierType
        {
            get { return supplierType; }
            set { supplierType = value; }
        }
        # endregion

        # region Methods

        public override void Save(Product prod)
        {
            HotelItinerary itinerary = (HotelItinerary)prod;

            //TODO: validate that the fields are properly set before calling the SP
            Trace.TraceInformation("HotelItinerary.Save entered.");
            SqlParameter[] paramList = new SqlParameter[29];
            if (itinerary.confirmationNo != null)
            {
                paramList[0] = new SqlParameter("@confirmationNo", itinerary.ConfirmationNo);
            }
            else
            {
                paramList[0] = new SqlParameter("@confirmationNo", DBNull.Value);
            }
            paramList[1] = new SqlParameter("@starRating", itinerary.Rating);
            if (itinerary.CityCode != null)
            {
                paramList[2] = new SqlParameter("@cityCode", itinerary.CityCode);
            }
            else
            {
                paramList[2] = new SqlParameter("@cityCode", DBNull.Value);
            }
            paramList[3] = new SqlParameter("@hotelName", itinerary.HotelName);
            paramList[4] = new SqlParameter("@hotelCode", itinerary.HotelCode);

            paramList[5] = new SqlParameter("@addressLine1", itinerary.HotelAddress1);

            if (itinerary.HotelAddress2 != null)
            {
                paramList[6] = new SqlParameter("@addressLine2", itinerary.HotelAddress2);
            }
            else
            {
                paramList[6] = new SqlParameter("@addressLine2", string.Empty);
            }

            paramList[7] = new SqlParameter("@checkInDate", itinerary.StartDate);
            paramList[8] = new SqlParameter("@checkOutDate", itinerary.EndDate);
            paramList[9] = new SqlParameter("@noOfRooms", itinerary.NoOfRooms);
            paramList[10] = new SqlParameter("@status", itinerary.Status);
            paramList[11] = new SqlParameter("@specialRequest", itinerary.SpecialRequest);
            paramList[12] = new SqlParameter("@createdBy", itinerary.CreatedBy);
            paramList[13] = new SqlParameter("@lastModifiedBy", itinerary.LastModifiedBy);
            paramList[14] = new SqlParameter("@hotelPolicyDetails", itinerary.HotelPolicyDetails);
            paramList[15] = new SqlParameter("@source", itinerary.Source);
            paramList[16] = new SqlParameter("@hotelId", SqlDbType.Int);
            paramList[17] = new SqlParameter("@isDomestic", itinerary.isDomestic);
            paramList[18] = new SqlParameter("@hotelCancelPolicy", itinerary.hotelCancelPolicy);
            paramList[19] = new SqlParameter("@cityRef", itinerary.cityRef);
            paramList[20] = new SqlParameter("@cancelId", itinerary.cancelId);
            paramList[21] = new SqlParameter("@flightInfo", itinerary.flightInfo);
            paramList[22] = new SqlParameter("@bookingRefNo", itinerary.bookingRefNo);
            paramList[23] = new SqlParameter("@map", itinerary.map);
            paramList[24] = new SqlParameter("@lastCancellationDate", itinerary.lastCancellationDate);
            paramList[25] = new SqlParameter("@voucherStatus", itinerary.voucherStatus);
            paramList[26] = new SqlParameter("@hotelCategory", itinerary.hotelCategory);
            paramList[27] = new SqlParameter("@agencyReference", itinerary.agencyReference);
            paramList[28] = new SqlParameter("@vatDescription", itinerary.vatDescription);
            paramList[16].Direction = ParameterDirection.Output;
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddHotelItinerary, paramList);
            hotelId = (int)paramList[16].Value;

            itinerary.HotelId = hotelId;
            // Saving rooms details
            for (int i = 0; i < itinerary.Roomtype.Length; i++)
            {
                itinerary.Roomtype[i].Price.Save();
                itinerary.Roomtype[i].HotelId = hotelId;
                itinerary.Roomtype[i].Save();
            }

            // saving Lead passenger details 
            //itinerary.HotelPassenger.RoomId = 0;  
            //itinerary.HotelPassenger.Save();
            if (itinerary.penalityInfo != null)
            {
                foreach (HotelPenality penality in itinerary.PenalityInfo)
                {
                    penality.HotelId = hotelId;
                    penality.Save();
                }
            }

            Trace.TraceInformation("HotelItinerary.Save exiting");
        }

        public void Load(int hotelId)
        {
            Trace.TraceInformation("HotelItinerary.Load entered : hotelId = " + hotelId);
            if (hotelId <= 0)
            {
                throw new ArgumentException("HotelId Id should be positive integer", "hotelId");
            }
            this.hotelId = hotelId;
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            try
            {

                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetHotelItinerary, paramList, connection);
                if (data.Read())
                {
                    confirmationNo = data["confirmationNo"].ToString();
                    rating = (HotelRating)(Convert.ToInt32(data["starRating"]));
                    cityCode = data["cityCode"].ToString();
                    hotelName = data["hotelName"].ToString();
                    hotelCode = data["hotelCode"].ToString();
                    hotelAddress1 = data["addressLine1"].ToString();
                    hotelAddress2 = data["addressLine2"].ToString();
                    startDate = Convert.ToDateTime(data["checkInDate"]);
                    endDate = Convert.ToDateTime(data["checkOutDate"]);
                    noOfRooms = Convert.ToInt16(data["noOfRooms"]);
                    hotelPolicyDetails = data["hotelPolicyDetails"].ToString();
                    status = (HotelBookingStatus)(Convert.ToInt16(data["status"]));
                    if (data["specialRequest"] != null)
                    {
                        specialRequest = data["specialRequest"].ToString();
                    }
                    isDomestic = Convert.ToBoolean(data["isDomestic"]);
                    createdBy = Convert.ToInt32(data["createdBy"]);
                    createdOn = Convert.ToDateTime(data["createdOn"]);
                    lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                    source = (HotelBookingSource)(Convert.ToInt32(data["source"]));
                    hotelCancelPolicy = data["hotelCancelPolicy"].ToString();
                    cityRef = data["cityRef"].ToString();
                    cancelId = data["cancellationId"].ToString();
                    flightInfo = data["flightInfo"].ToString();
                    bookingRefNo = data["bookingRefNo"].ToString();
                    map = data["map"].ToString();
                    voucherStatus = Convert.ToBoolean(data["voucherStatus"]);
                    if (data["lastCancellationDate"] != DBNull.Value)
                    {
                        lastCancellationDate = Convert.ToDateTime(data["lastCancellationDate"]);
                    }
                    if (!data["hotelCategory"].Equals(DBNull.Value))
                    {
                        hotelCategory = data["hotelCategory"].ToString();
                    }
                    if (!data["agencyReference"].Equals(DBNull.Value))
                    {
                        agencyReference = data["agencyReference"].ToString();
                    }
                    if (!data["vatDescription"].Equals(DBNull.Value))
                    {
                        vatDescription = data["vatDescription"].ToString();
                    }
                    data.Close();
                    connection.Close();
                    Trace.TraceInformation("HotelItinerary.Load exiting :" + hotelId.ToString());
                }
                else
                {
                    data.Close();
                    connection.Close();
                    Trace.TraceInformation("HotelItinerary.Load exiting : hotelId does not exist.hotelId = " + hotelId.ToString());

                }
                HotelPenality penality = new HotelPenality();
                penalityInfo = penality.GetHotelPenality(hotelId);
            }
            catch (Exception exp)
            {
                connection.Close();
                throw new ArgumentException("Hotel id does not exist in database");
            }

        }

        public static int GetHotelId(string confNo)
        {
            Trace.TraceInformation("HotelItinerary.GetHotelId entered : confNo = " + confNo);
            int hotelId = 0;
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@confirmationNo", confNo);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetHotelId, paramList, connection);
            if (data.Read())
            {
                hotelId = Convert.ToInt32(data["hotelId"]);
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("HotelItinerary.GetHotelId exiting :" + hotelId.ToString());
            return hotelId;
        }
        public void Update()
        {
            Trace.TraceInformation("HotelItinerary.Update entered  ");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@status", status);
            paramList[1] = new SqlParameter("@cancelId", cancelId);
            paramList[2] = new SqlParameter("@hotelId", hotelId);

            int retVal = Dal.ExecuteNonQuerySP(SPNames.UpdateHotelItineary, paramList);
            Trace.TraceInformation("HotelItinerary.Update exiting count" + retVal);
        }
        public void UpdateVoucherStatus()
        {
            Trace.TraceInformation("HotelItinerary.UpdateVoucherStatus entered  ");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@status", voucherStatus);
            paramList[1] = new SqlParameter("@hotelId", hotelId);

            int retVal = Dal.ExecuteNonQuerySP(SPNames.UpdateVoucherStatus, paramList);
            Trace.TraceInformation("HotelItinerary.UpdateVoucherStatus exiting count" + retVal);
        }
        /// <summary>
        /// This method is used for sending selected Hotel Itineararies.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="result"></param>
        /// <param name="isPublishPrice"></param>
        /// <param name="email"></param>
        /// <param name="agencyId"></param>
        /// <param name="memberId"></param>
        public static void CreateMail(HotelRequest request, HotelSearchResult[] result, Boolean isPublishPrice, string email, int agencyId, int memberId)
        {
            if (result != null)
            {

                string checkIn = string.Empty;
                string checkOut = string.Empty;
                Member member = new Member(memberId);
                Agency agency = new Agency(agencyId);
                checkIn = request.StartDate.ToShortDateString();
                checkOut = request.EndDate.ToString();
                string pax = string.Empty;
                decimal totalSellRate;
                decimal avgFare;
                string paxString = string.Empty;
                decimal rateofExchange = 1;
                Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
                StaticData staticInfo = new StaticData();
                rateOfExList = staticInfo.CurrencyROE;
                pax = Convert.ToString(request.RoomGuest[0].noOfAdults + request.RoomGuest[0].noOfChild);
                if (Convert.ToInt32(pax) > 1)
                {
                    paxString = "Passengers";
                }
                System.TimeSpan diffResult = request.EndDate.Subtract(request.StartDate);
                int noDays = diffResult.Days;
                StringBuilder sb = new StringBuilder();
                sb.Append("<body style=\"font-family:arial;font-size: 12px;margin: 0px;padding: 0px;background-color: White;color:#4c4c4b;\">");
                sb.Append("<div style=\"width:778px;margin: auto;\">");
                sb.Append("<div style=\"width:778px;float:left;\">");
                sb.Append("<div style=\"float:left;width:100%;border: solid 1px #c0c0c0;\">");
                sb.Append("<div style=\"float:left;width:97%;margin:0px;padding-top:20px;padding-left:0px;\">");
                sb.Append("<div style=\"float:left;width:100%;\">");
                sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\"><span style=\"float:left;font-size:15px;font-weight:bold;width:90px;\">City:</span><span style=\"float:left;font-weight:bold;width:0px;font-size:15px;\">" + request.CityName + "</span></p>");
                sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
                sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:90px;\">Check in:</span> ");
                sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + request.StartDate.ToString("dd MMM yy") + "</span></p>");
                sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\">");
                sb.Append("<span style=\"float:left;font-size:15px;font-weight:bold;width:90px;\">Check out:</span> ");
                sb.Append("<span style=\"float:left;width:200px;font-weight:bold;font-size:15px;\">" + request.EndDate.ToString("dd MMM yy") + " <em style=\"font-style:normal;font-size:12px;\">(" + noDays.ToString() + " Nights)</em></span></p>");
                sb.Append("<p style=\"float:left; width:100%; margin:0; padding:20px 0 0;\"><span style=\"float:left;font-size:15px;font-weight:bold;width:90px;\">Room(s):" + request.NoOfRooms.ToString() + "</span></p>");

                int adults = 0, chldren = 0;
                for (int i = 0; i < request.RoomGuest.Length; i++)
                {
                    adults += request.RoomGuest[i].noOfAdults;
                    chldren += request.RoomGuest[i].noOfChild;
                }
                sb.Append(" <p style=\"float:left; width:100%; margin:0; padding:0;\"><span style=\"float:left;font-size:15px;font-weight:bold;width:110px;\"> No.of Guest(s):</span><span style=\"float:left;width:80px;font-weight:bold;font-size:15px;\">" + (adults + chldren) + "  </span></p>");
                for (int j = 0; j < request.RoomGuest.Length; j++)
                {
                    sb.Append("<p style=\"float:left; width:100%; margin:0; padding:0;\"><span style=\"float:left;font-size:15px;font-weight:bold;width:140px;\">Room " + (j + 1) + ":");
                    sb.Append("<b style=\"padding-left:3px;\">" + request.RoomGuest[j].noOfAdults + "</b> Adult(s)</span>");
                    if (request.RoomGuest[j].noOfChild > 0)
                    {
                        sb.Append("<span style=\"float:left;width:120px;font-weight:bold;font-size:15px;\">" + request.RoomGuest[j].noOfChild.ToString() + " Children</span>");
                    }
                    sb.Append("</p>");
                }
                sb.Append("</div></div>");

                foreach (HotelSearchResult sr in result)
                {
                    sb.Append("<div style=\"float:left; width:100%; border:solid 1px #ccc; margin:10px 0 0; padding:0 0 5px 0;\"><div style=\"float:left; width:98%; padding:5px;\"><div style=\"float:left; width:500px;\"><p style=\"float:left;font-size:17px;font-weight:bold; width:500px; margin:0; padding:0;\">");
                    sb.Append(sr.HotelName);
                    sb.Append("<span style=\"float:left;font-size:13px;font-weight:bold; padding:0; width:350px;\">");
                    Technology.BookingEngine.HotelRoomsDetails room = new Technology.BookingEngine.HotelRoomsDetails();
                    room = sr.RoomDetails[0];
                    if (rateOfExList.ContainsKey(sr.Currency))
                    {
                        rateofExchange = rateOfExList[sr.Currency];
                    }
                    string symbol = "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "";
                    if (sr.Currency == "USD")
                    {
                        symbol = "$";
                    }
                    else if (sr.Currency == "GBP")
                    {
                        symbol = "�";
                    }
                    else if (sr.Currency == "EUR")
                    {
                        symbol = "�";
                    }
                    if (Convert.ToInt16(sr.Rating) > 0)
                    {
                        sb.Append("(" + sr.Rating + ")");
                    }
                    sb.Append("<br />");
                    sb.Append(sr.HotelAddress);
                    sb.Append("</span><br /><span style=\"float:left;font-size:13px;font-weight:bold; padding:0; width:350px;\">");
                    sb.Append(request.CityName);
                    sb.Append(" </span></p></div>");
                    sb.Append("<div style=\"float:right;text-align:right;font-size:13px;font-weight:bold; padding:0; width:200px;\">");

                    decimal totFare = 0;
                    //if it is an international but not ian it will have sequence no.
                    if (!request.IsDomestic && sr.BookingSource != HotelBookingSource.IAN)
                    {
                        for (int x = 0; x < request.NoOfRooms; x++)
                        {
                            for (int y = 0; y < sr.RoomDetails.Length; y++)
                            {
                                if (sr.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                                {
                                    totFare += sr.RoomDetails[y].SellingFare;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        totFare = room.TotalPrice;
                        if (sr.BookingSource == HotelBookingSource.TBOConnect)
                        {
                            totFare += (room.TotalPrice - room.ChildCharges) * sr.Price.AgentCommission / 100;
                            totFare = totFare * request.NoOfRooms;
                        }
                    }
                    if (!request.IsMultiRoom)
                    {
                        avgFare = totFare / request.NoOfRooms;
                        if (sr.BookingSource != HotelBookingSource.IAN)
                        {
                        decimal finalFare = Math.Round(avgFare * rateofExchange, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                            sb.Append("<h2 style=\"float:right;width:100%;margin:0;padding:0;\">" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "");
                            sb.Append(finalFare.ToString());
                            if (symbol != "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "")
                            {
                                sb.Append("(" + symbol + avgFare.ToString( Technology.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) + ")");
                            }
                        }
                        else
                        {
                            sb.Append("<h2 style=\"float:right;width:100%;margin:0;padding:0;\">");
                            if (room.SellingFare == room.TotalPrice)
                            {
                            sb.Append(symbol + Math.Round(room.TotalPrice, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                            }
                            else
                            {
                                sb.Append("<del style=\"font-size:13px; font-weight: bold;\">");
                                sb.Append(symbol + Math.Round(room.TotalPrice, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                                sb.Append("</del> <br />");
                                sb.Append(symbol + Math.Round(room.SellingFare, Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"])));
                            }
                        }
                        sb.Append("</h2> <p style=\"float:right;width:100%;margin:0;padding:0;\">Lowest Total Rate</p><span style=\"float:right;width:100%;margin:0;padding:0;\">(per room)");
                    }
                    else
                    {
                        //in case of generalized multi room booking
                        sb.Append((Math.Round(totFare * rateofExchange, 0)).ToString());
                        if (symbol != "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "")
                        {
                            sb.Append("(" + symbol + totFare.ToString( Technology.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) + ")");
                        }
                        sb.Append("</h2> <p style=\"float:right;width:100%;margin:0;padding:0;\">Minimum Total Price</p><span style=\"float:right;width:100%;margin:0;padding:0;\">(Incl. of All Taxes)");
                    }
                    if (symbol != "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "")
                    {
                        sb.Append("<span style=\"float:right;width:100%;margin:0;padding:0;\">(converted from " + sr.Currency + ")</span>");
                    }
                    sb.Append("</span></div>");
                    sb.Append("<div style=\"float:left;width:99.5%;margin:0;padding:10px 0 0 5px;\">");
                    sb.Append(sr.HotelDescription + "</div></div>");
                    if (sr.BookingSource != HotelBookingSource.TBOConnect || sr.BookingSource == HotelBookingSource.TBOConnect && !request.IsMultiRoom)
                    {
                        sb.Append("<div style=\"float:left;width:100%;margin:10px 0 0;padding:3px 0;background:#eee;font-weight:bold;\"><span style=\"float:left;width:400px;margin:0;padding:0 0 0 5px;\">Room Type</span>");

                        if ((sr.Price != null && sr.Price.AccPriceType == PriceType.PublishedFare) || sr.BookingSource == HotelBookingSource.IAN)
                        {
                            sb.Append("<span style=\"float:right;width:120px;margin:0;padding:0;\">Published Rate</span></div>");
                        }
                        else if (sr.Price.AccPriceType == PriceType.NetFare)
                        {
                            sb.Append("<span style=\"float:right;width:120px;margin:0;padding:0;\"> Rate</span></div>");
                        }
                        decimal tempPrice = 0;
                        foreach (HotelRoomsDetails roomInfo in sr.RoomDetails)
                        {
                            sb.Append("<p style=\"width:100%;float:left;margin:0;padding:5px 0 0;\"><span style=\"width:400px;float:left;margin:0;padding:0 0 0 5px;\">");
                            sb.Append(roomInfo.RoomTypeName);
                            sb.Append("<br /><em style=\"font-style:normal;\">");
                            if (roomInfo.Amenities != null && roomInfo.Amenities.Count > 0)
                            {
                                sb.Append("Incl:");
                                foreach (string amenity in roomInfo.Amenities)
                                {
                                    sb.Append(amenity + " ");
                                }
                            }
                            if ((sr.Price != null && sr.Price.AccPriceType == PriceType.PublishedFare) || sr.BookingSource == HotelBookingSource.IAN)
                            {
                                sb.Append("</em></span>");
                                sb.Append("<span style=\"float:right;width:120px;margin:0;padding:0;\">" + symbol + " ");
                                if (sr.BookingSource != HotelBookingSource.TBOConnect)
                                {
                                    sb.Append(roomInfo.TotalPrice.ToString( Technology.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
                                }
                                else
                                {
                                    tempPrice = roomInfo.TotalPrice + (roomInfo.TotalPrice * sr.Price.AgentCommission / 100);
                                    sb.Append(tempPrice.ToString( Technology.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ));
                                }
                                sb.Append("</span></p>");
                            }
                            else
                            {
                                decimal netSellingRate = roomInfo.SellingFare + (roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length) + roomInfo.TotalTax;
                                sb.Append("</em></span><span style=\"float:right;width:100px;margin:0;padding:0;\">" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + " ");
                                sb.Append("" + Math.Round(netSellingRate * rateofExchange, 0).ToString("N2"));
                                if (symbol != "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "")
                                {
                                    sb.Append("(" + symbol + netSellingRate.ToString("N2") + ")");
                                }
                                sb.Append("</span></p>");
                            }

                        }
                    }
                    sb.Append("</div>");
                }
                sb.Append("</div></div></div></body>");
                List<string> toArray = new List<string>();
                string addressList = email;
                string[] addressArray = addressList.Split(',');
                for (int k = 0; k < addressArray.Length; k++)
                {
                    toArray.Add(addressArray[k]);
                }
                string from = Agency.GetAgencyEmail(agency.AgencyId);
                string subjectLine = "Your itineraries for Hotels";
                try
                {
                    Email.Send(from, from, toArray, subjectLine, sb.ToString(), new Hashtable());
                }
                catch (System.Net.Mail.SmtpException)
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.Email, CoreLogic.Severity.Normal, 0, "Smtp is unable to send the message", "");
                }
            }
        }

        /// <summary>
        /// This menthod is used to update the Itinearary whille Amendments
        /// </summary>
        /// <param name="prod"></param>
        public void UpdateItinearary(Product prod, bool isItemChange, bool isPaxChange)
        {
            Trace.TraceInformation("HotelItinerary.UpdateItinearary entered  ");
            HotelItinerary itinerary = (HotelItinerary)prod;
            // Loading Previous Itineary room Data to remember the Room ID
            HotelItinerary prevItineary = new HotelItinerary();
            prevItineary.Load(itinerary.hotelId);
            HotelRoom[] prevRoomData = new HotelRoom[prevItineary.noOfRooms];
            HotelRoom roomInfo = new HotelRoom();
            prevItineary.Roomtype = roomInfo.Load(itinerary.hotelId);
            prevRoomData = prevItineary.Roomtype;
            if (isItemChange)
            {
                SqlParameter[] paramList = new SqlParameter[10];
                paramList[0] = new SqlParameter("@confirmationNo", itinerary.ConfirmationNo);
                paramList[1] = new SqlParameter("@checkInDate", itinerary.StartDate);
                paramList[2] = new SqlParameter("@checkOutDate", itinerary.EndDate);
                paramList[3] = new SqlParameter("@status", itinerary.Status);
                paramList[4] = new SqlParameter("@lastModifiedBy", itinerary.LastModifiedBy);
                paramList[5] = new SqlParameter("@HotelPolicyDetails", itinerary.HotelPolicyDetails);
                paramList[6] = new SqlParameter("@hotelId", itinerary.hotelId);
                paramList[7] = new SqlParameter("@hotelCancelPolicy", itinerary.hotelCancelPolicy);
                paramList[8] = new SqlParameter("@bookingRefNo", itinerary.bookingRefNo);
                paramList[9] = new SqlParameter("@lastCancellationDate", itinerary.lastCancellationDate);
                int retVal = Dal.ExecuteNonQuerySP(SPNames.AmendHotelItineary, paramList);

                //updating room Info
                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    itinerary.Roomtype[i].Price.PriceId = prevRoomData[i].PriceId;
                    itinerary.Roomtype[i].Price.Save();
                    itinerary.Roomtype[i].HotelId = hotelId;
                    itinerary.Roomtype[i].UpdateRoom(prevRoomData[i].RoomId);

                }
                //Deleting Old Penality Info.
                HotelPenality penalityInfo = new HotelPenality();
                penalityInfo.DeletePenalityInfo(itinerary.hotelId);
                // Adding new  Penality Info.
                if (itinerary.penalityInfo != null)
                {
                    foreach (HotelPenality penality in itinerary.PenalityInfo)
                    {
                        penality.HotelId = hotelId;
                        penality.Save();
                    }
                }
            }
            //if Pax change then update onll Pax information.
            if (isPaxChange)
            {
                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                {
                    itinerary.Roomtype[i].UpdatePax(prevRoomData[i].RoomId, prevItineary.HotelId);
                }
            }
            Trace.TraceInformation("HotelItinerary.UpdateItinearary exiting ");

        }

        /// <summary>
        /// whether booking exists against given bookingRefNo
        /// </summary>
        /// <param name="bookingRefNo"></param>
        /// <returns></returns>
        public bool IsBookingAlreadyExist(string confirmationNo, HotelBookingSource source)
        {
            bool isExists = false;
            List<int> hotelIdList = new List<int>();
            try
            {
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@confNo", confirmationNo);
                paramList[1] = new SqlParameter("@source", (int)source);
                SqlConnection sqlConn = Dal.GetConnection();
                SqlDataReader reader = Dal.ExecuteReaderSP(SPNames.IsBookingAlreadyExist, paramList, sqlConn);
                while (reader.Read())
                {
                    hotelIdList.Add(Convert.ToInt32(reader["hotelId"]));
                }
                reader.Close();
                sqlConn.Close();
                if (hotelIdList.Count >= 1)
                {
                    isExists = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
            return isExists;
        }
        #endregion
    }
}
