using System;
using System.Collections.Generic;
using System.Text;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Diagnostics;

namespace Technology.BookingEngine
{
    /// <summary>
    /// Enum for Type of Sightseeing Change Policy
    /// </summary>
    public enum SightseeingChangePoicyType
    {
        Amendment = 1,
        Namechange = 2,
        Other = 3
    }
    [Serializable]
   public class SightseeingPenalty
    {
        //private Variables
        int sightseeingId;
        SightseeingChangePoicyType policyType;
        SightseeingBookingSource bookingSource;
        bool isAllowed;
        DateTime fromDate;
        DateTime toDate;
        decimal price;
        string remarks;
        //public Properties.
        public int SightseeingId
        {
            get { return sightseeingId; }
            set { sightseeingId = value; }
        }
        public SightseeingChangePoicyType PolicyType
        {
            get { return policyType; }
            set { policyType = value; }
        }
        public SightseeingBookingSource BookingSource
        {
            get { return bookingSource; }
            set { bookingSource = value; }
        }
        public bool IsAllowed
        {
            get { return isAllowed; }
            set { isAllowed = value; }
        }
        public DateTime FromDate
        {
            get { return fromDate; }
            set { fromDate = value; }
        }
        public DateTime ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }
        public decimal Price
        {
            get { return price; }
            set { price = value; }
        }
        /// <summary>
        /// This Method is used to Save the Sightseeing Penality Data
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("SightseeingPenality.Save entered.");
            SqlParameter[] paramList = new SqlParameter[8];
            paramList[0] = new SqlParameter("@sightseeingID", this.sightseeingId);
            paramList[1] = new SqlParameter("@policyType", Convert.ToInt16(this.policyType));
            paramList[2] = new SqlParameter("@bookingSource", Convert.ToInt16(this.bookingSource));
            paramList[3] = new SqlParameter("@isAllowed", this.isAllowed);
            paramList[4] = new SqlParameter("@fromDate", this.fromDate);
            paramList[5] = new SqlParameter("@toDate", this.toDate);
            paramList[6] = new SqlParameter("@price", this.price);
            paramList[7] = new SqlParameter("@remarks", this.remarks);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.SaveSightseeingPenality, paramList);
            Trace.TraceInformation("SightseeingPenality.Save exiting");
        }
        /// <summary>
        /// This Method is used to Retrive all Sightseeing Penality details based on Sightseeing ID
        /// </summary>
        /// <param name="sightseeingId"></param>
        /// <returns></returns>
        public List<SightseeingPenalty> GetSightseeingPenality(int sightseeingId)
        {
            Trace.TraceInformation("SightseeingPenality.GetSightseeingPenality entered.sightseeing ID:" + sightseeingId);
            List<SightseeingPenalty> penalityList = new List<SightseeingPenalty>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@sightseeingId", sightseeingId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetSightseeingPenality, paramList, connection);
            while (data.Read())
            {
                SightseeingPenalty penalityInfo = new SightseeingPenalty();
                penalityInfo.SightseeingId = sightseeingId;
                penalityInfo.IsAllowed = Convert.ToBoolean(data["isAllowed"]);
                penalityInfo.PolicyType = (SightseeingChangePoicyType)(Convert.ToInt32(data["policyType"]));
                penalityInfo.Price = Convert.ToDecimal(data["price"]);
                penalityInfo.Remarks = data["remarks"].ToString();
                penalityInfo.ToDate = Convert.ToDateTime(data["toDate"]);
                penalityInfo.FromDate = Convert.ToDateTime(data["fromDate"]);
                penalityInfo.BookingSource = (SightseeingBookingSource)(Convert.ToInt32(data["sightseeingSource"]));
                penalityList.Add(penalityInfo);
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("SightseeingPenality.GetSightseeingPenality exiting");
            return penalityList;
        }

        /// <summary>
        /// This menthod is used to delete the Penality info for a Sightseeing.
        /// </summary>
        /// <param name="sightseeingId"></param>
        public void DeletePenalityInfo(int sightseeingId)
        {
            Trace.TraceInformation("SightseeingPenality.DeletePenalityInfo entered.Sightseeing ID:" + sightseeingId);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@sightseeingId", sightseeingId);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.DeleteSightseeingPenality, paramList);
            Trace.TraceInformation("SightseeingPenality.DeletePenalityInfo exiting  Result:" + rowsAffected);
        }
    }
}
