using System;
using System.Collections.Generic;
using System.Text;

namespace Technology.BookingEngine
{
    public class GoAirSearchResult : SearchResult
    {
        private Dictionary<PassengerType, int> fareInformationId;

        // User for fare Quote Information
        public Dictionary<PassengerType, int> FareInformationId
        {
            get { return fareInformationId; }
            set { fareInformationId = value; }
        }

        // Unique GUId generated
        private string guid;

        public string Guid
        {
            get
            {
                return guid;
            }
            set
            {
                guid = value;
            }
        }

        //Actual Base For - Added for retaining actual base fare value in passenger details page
        private Fare[] actualAmmount;

        public Fare[] ActualAmmount
        {
            get
            {
                return actualAmmount;
            }
            set
            {
                actualAmmount = value;
            }
        }
    }
}
