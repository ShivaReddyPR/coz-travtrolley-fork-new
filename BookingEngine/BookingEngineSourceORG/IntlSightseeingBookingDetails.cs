using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;

namespace Technology.BookingEngine
{
   public class IntlSightseeingBookingDetails
    {
        private int sightseeingId;
        TypeOfDocument documentName;
        private string fileName;

        public int SightseeingId
        {
            get { return sightseeingId; }
            set { sightseeingId = value; }
        }

        public TypeOfDocument DocumentName
        {
            get { return documentName; }
            set { documentName = value; }
        }

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public List<IntlSightseeingBookingDetails> Load(int sightseeingId)
        {
            Trace.TraceInformation("IntlSightseeingBookingDetails.Load entered :sightseeingID = " + sightseeingId);
            if (sightseeingId <= 0)
            {
                throw new ArgumentException("sightseeingId value is null", "sightseeingId");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@sightseeingId", sightseeingId);
            List<IntlSightseeingBookingDetails> listDetails = new List<IntlSightseeingBookingDetails>();
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetIntelSightseeingById, paramList, connection);
            while (data.Read())
            {
                IntlSightseeingBookingDetails internationalInfo = new IntlSightseeingBookingDetails();
                internationalInfo.sightseeingId = Convert.ToInt32(data["sightseeingId"]);
                internationalInfo.DocumentName = (TypeOfDocument)data["typeOfDocument"];
                internationalInfo.FileName = data["fileName"].ToString();
                listDetails.Add(internationalInfo);
            }
            data.Close();
            connection.Close();

            Trace.TraceInformation("IntlSightseeingBookingDetail.Load exiting.");
            return listDetails;
        }
        public void Save()
        {
            Trace.TraceInformation("IntlSightseeingBookingDetails.Save entered.");
            SqlParameter[] paramList = new SqlParameter[3];
            try
            {
                paramList[0] = new SqlParameter("@sightseeingId", sightseeingId);
                paramList[1] = new SqlParameter("@typeOfDocument", (TypeOfDocument)documentName);
                paramList[2] = new SqlParameter("@fileName", fileName);
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddIntlSightseeingBookingDetails, paramList);
            }
            catch (Exception exp)
            {
                throw new Exception(exp.Message);
            }

            Trace.TraceInformation("IntlSightseeingBookingDetails.Save exiting");
        }
    }
}
