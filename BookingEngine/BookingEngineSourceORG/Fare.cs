using System;
using System.Collections.Generic;
using System.Text;

namespace Technology.BookingEngine
{
    public class Fare
    {
        private PassengerType passengerType;
        private int passengerCount;
        private double baseFare;
        private double totalFare;
        private double sellingFare;
        private decimal airlineTransFee;
        /// <summary>
        /// This fee will be a part of Published fare but not of Offered Fare
        /// </summary>
        private decimal additionalTxnFee;

        public PassengerType PassengerType
        {
            get
            {
                return passengerType;
            }
            set
            {
                passengerType = value;
            }
        }

        public int PassengerCount
        {
            get
            {
                return passengerCount;
            }
            set
            {
                passengerCount = value;
            }
        }

        public double BaseFare
        {
            get
            {
                return baseFare;
            }
            set
            {
                baseFare = value;
            }
        }

        public double TotalFare
        {
            get
            {
                return totalFare;
            }
            set
            {
                totalFare = value;
            }
        }
        public double SellingFare
        {
            get 
            {
                return this.sellingFare;
            }
            set
            {
                this.sellingFare = value;
            }
        }

        public decimal AirlineTransFee
        {
            get
            {
                return this.airlineTransFee;
            }
            set
            {
                this.airlineTransFee = value;
            }
        }

        /// <summary>
        /// This fee will be a part of Published fare but not of Offered Fare.
        /// </summary>
        public decimal AdditionalTxnFee
        {
            get
            {
                return additionalTxnFee;
            }
            set
            {
                additionalTxnFee = value;
            }
        }

        public decimal Tax
        {
            get
            {
                return Convert.ToDecimal(totalFare - baseFare);
            }
        }
    }
}
