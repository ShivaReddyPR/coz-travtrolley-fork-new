using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using CT.TicketReceipt.DataAccessLayer;
using CT.Core;
using CT.Configuration;
using System.Globalization;

namespace Technology.BookingEngine
{
    public class Util
    {
        public static string GetHHMM(TimeSpan tspan)
        {
            string hr = Convert.ToString(tspan.Days * 24 + tspan.Hours);
            string min = Convert.ToString(tspan.Minutes);
            if (min.Length == 1)
            {
                min = "0" + min;
            }
            return (hr + min);
        }

        public static string PascalToTitle(string pascal)
        {
            string pattern = "([A-Z][a-z]+)";
            string title = string.Empty;
            string[] splitArray = Regex.Split(pascal, pattern);
            bool start = false;
            for (int i = 0; i < splitArray.Length; i++)
            {
                if (start)
                {
                    title += " " + splitArray[i];
                }
                else if (splitArray[i].Length > 0)
                {
                    title += splitArray[i];
                    start = true;
                }
            }
            return title;
        }
        /// <summary>
        /// Method returns time span and takes a HHMM string
        /// </summary>
        /// <param name="HHMM">HHMM</param>
        /// <returns></returns>
        public static TimeSpan GetTimeSpan(string HHMM)
        {
            HHMM = HHMM.Trim();
            if (HHMM.Length == 3)
            {
                HHMM = "0" + HHMM;
            }
            return new TimeSpan(Convert.ToInt32(HHMM.Substring(0, 2)), Convert.ToInt32(HHMM.Substring(2, 2)), 0);
        }
        /// <summary>
        /// Method takes dateTime and returns dateTime in numeric format
        /// </summary>
        /// <param name="dateTime">DateTime</param>
        /// <returns>(23/06/2005 types)</returns>
        public static string GetDateStringNum(DateTime dateTime)
        {
            return (dateTime.Day.ToString() + "/" + dateTime.Month.ToString() + "/" + dateTime.Year.ToString());
        }
        /// <summary>
        /// Method takes dateTime and return date in proper format
        /// </summary>
        /// <param name="dateTime">DateTime</param>
        /// <returns>Day,month and year(23 Jan 06 types)</returns>
        public static string GetDateString(DateTime dateTime)
        {
            return dateTime.ToString("dd MMM yy");
        }
        /// <summary>
        /// Method takes datetime and return date without year
        /// </summary>
        /// <param name="dateTime">Date Time</param>
        /// <returns>Day and month(23 Jan types)</returns>
        public static string GetOnlyDayMon(DateTime dateTime)
        {
            return dateTime.ToString("dd MMM");
        }
        /// <summary>
        /// Method take dateTime and returns time String with AM and PM
        /// </summary>
        /// <param name="dateTime">DateTime</param>
        /// <returns>Time string (12:30 AM types)</returns>
        public static string GetTimeString(DateTime dateTime)
        {
            return dateTime.ToShortTimeString();
        }
        /// <summary>
        /// Method takes a dateTime type and return days of week(weekdays)
        /// </summary>
        /// <param name="dateTime">datetime IST.</param>
        /// <returns>"Today or Yesterday or the day of certain date"</returns>
        public static string GetDayString(DateTime dateTime)
        {
            // Changes done due to issue in booking Queue page Booking Date 'Today' - changes GetIst added and called to convert DateTime.Now.UTC time to IST time
            if (dateTime.Day == GetIST().Day && dateTime.Month == GetIST().Month && dateTime.Year == GetIST().Year)
            {
                return "Today";
            }
            else if (dateTime.Day + 1 == GetIST().Day && dateTime.Month == GetIST().Month && dateTime.Year == GetIST().Year)
            {
                return "Yesterday";
            }
            else
            {
                return dateTime.DayOfWeek.ToString().Substring(0, 3);
            }
        }

        /// <summary>
        /// Checks if given date is today's date.
        /// </summary>
        /// <param name="utcDateTime">DateTime UTC.</param>
        /// <returns>True if given date is today's date</returns>
        public static bool IsToday(DateTime utcDateTime)
        {
            if (DateTime.UtcNow.Year == utcDateTime.Year && DateTime.UtcNow.DayOfYear == utcDateTime.DayOfYear)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets time in minute. Number of minutes passed in a particular day
        /// </summary>
        /// <param name="time">DateTime object</param>
        /// <returns>Integer indicating number of minustes passed in the day</returns>
        public static int IntTime(DateTime time)
        {
            return (time.Hour * 60 + time.Minute);
        }

        /// <summary>
        /// Gives the difference between two dates in months
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static int MonthDifference(DateTime date1, DateTime date2)
        {
            if (date1 < date2)
            {
                DateTime temp = date2;
                date2 = date1;
                date1 = temp;
            }
            int year = date1.Year - date2.Year;
            int month = date1.Month - date2.Month;
            int totalMonth = year * 12 + month;
            if (date1.Day >= date2.Day) totalMonth++;
            return totalMonth;
        }

        /// <summary>
        /// Gets CityName for a given city code.
        /// </summary>
        /// <param name="cityCode">three character code for a city</param>
        /// <returns>Name of city as string</returns>
        public static string GetCityName(string cityCode)
        {
            Trace.TraceInformation("Util.GetCityName entered : cityCode = " + cityCode);
            //SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@cityCode", cityCode);
            string cityName = string.Empty;
            SqlDataReader dataReader = null;
            try
            {
                dataReader = DBGateway.ExecuteReaderSP(SPNames.GetCityNameFromCityCode, paramList);
                if (dataReader.Read())
                {
                    cityName = (string)dataReader["cityName"];
                }
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
                //connection.Close();
            }

            if (cityName == string.Empty)
            {
                //TODO: Name space is incorrect. And what to do for memberId and IP Address.
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.Low, 1, "City code does not exist in database : cityCode = " + cityCode, "NA");
            }
            Trace.TraceInformation("Util.GetCityName exiting : cityName = " + cityName);
            return cityName;
        }


        /// <summary>
        /// Gets current date and time in IST.
        /// </summary>
        /// <returns>DateTime in IST.</returns>
        public static DateTime GetIST()
        {
            return DateTime.Now.ToUniversalTime() + new TimeSpan(4, 00, 0);
        }
        /// <summary>
        /// IST time convert into UTC time 
        /// </summary>
        /// <param name="dateTime"> datetime </param>
        /// <returns> UST time </returns>
        public static DateTime ISTToUTC(DateTime dateTime)
        {
            return dateTime - new TimeSpan(4, 00, 0);
        }
        /// <summary>
        /// UTC Time converted into IST time.
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static DateTime UTCToIST(DateTime datetime)
        {
            return datetime + new TimeSpan(4, 00, 0);
        }
        /// <summary>
        /// Get tax for LCC
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        public static decimal GetTax(string source, string origin, string destination)
        {
            Trace.TraceInformation("Util.GetTax enter");
            decimal tax = 0;

            //SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@source", source);
            paramList[1] = new SqlParameter("@origin", origin);
            paramList[2] = new SqlParameter("@destination", destination);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetTaxForSourceByOriginDest, paramList);
            if (data.Read())
            {
                if (data["tax"] != DBNull.Value)
                {
                    tax = Convert.ToDecimal(data["tax"]);
                }
                data.Close();
                //connection.Close();
            }
            else
            {
                data.Close();
                //connection.Close();
                Trace.TraceInformation("Util.GetTax : No tax found in database for origin= " + origin + " & destination = " + destination);

                // Not throwing exception because if no tax found only adding data in trace and audit. And tax value returned is "0".
            }

            Trace.TraceInformation("Util.GetTax exit");
            return tax;
        }

        /// <summary>
        /// Get Fare Rules for LCC
        /// </summary>
        /// <param name="source">Sources are any LCC like -'SG'</param>
        /// <returns>Fare Rules String</returns>
        public static string GetLCCFareRules(string source)
        {
            Trace.TraceInformation("Util.GetLCCFareRules enter");
            string fareRules = string.Empty;

            //SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@source", source);

            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetFareRulesForLcc, paramList);
            if (data.Read())
            {
                if (data["fareRules"] != DBNull.Value)
                {
                    fareRules = Convert.ToString(data["fareRules"]);
                }
                data.Close();
                //connection.Close();
            }
            else
            {
                data.Close();
                //connection.Close();
                Trace.TraceInformation("Util.GetFuelSurCharge : No Fare Rules found in database for source=" + source);
            }

            Trace.TraceInformation("Util.GetLCCFareRules exit");
            return fareRules;
        }
        /// <summary>
        /// Method gets FuelSurcharge for LCC
        /// </summary>
        /// <param name="source">Booking source(e.g. SG or 6E)</param>
        /// <param name="origin">origin</param>
        /// <param name="destination">destination</param>
        /// <returns>returns the fuel surchage for the airline</returns>
        public static decimal GetFuelSurcharge(string source, string origin, string destination)
        {
            Trace.TraceInformation("Util.GetFuelSurcharge enter");
            decimal fuelSurcharge = 0;

            //SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@source", source);
            paramList[1] = new SqlParameter("@origin", origin);
            paramList[2] = new SqlParameter("@destination", destination);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetFuelSurchargeForSource, paramList);
            if (data.Read())
            {
                if (data["fuelSurcharge"] != DBNull.Value)
                {
                    fuelSurcharge = Convert.ToDecimal(data["fuelSurcharge"]);
                }
                data.Close();
                //connection.Close();
            }
            else
            {
                data.Close();
                //connection.Close();
                Trace.TraceInformation("Util.GetFuelSurcharge : No fuelSurcharge found in database for origin= " + origin + " & destination = " + destination);

            }

            Trace.TraceInformation("GetFuelSurcharge exit");
            return fuelSurcharge;
        }

        /// <summary>
        /// Checks if the given origin and destination both are in the given country.
        /// </summary>
        /// <param name="origin">City or Airport code of the origin.</param>
        /// <param name="destination">City or Airport code of the destination</param>
        /// <returns>True is origin and destination both are in the given country.</returns>
        public static bool IsDomestic(string origin, string destination, string countryCode)
        {
            //SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@origin", origin);
            paramList[1] = new SqlParameter("@destination", destination);
            paramList[2] = new SqlParameter("@countryCode", countryCode);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.IsDomestic, paramList);
            data.Read();
            bool isDomestic = Convert.ToBoolean(data["isDomestic"]);
            data.Close();
            //connection.Close();
            return isDomestic;
        }

        /// <summary>
        /// Checks if the airline is in the list of Net Remittance airlines in booking engine config.
        /// </summary>
        /// <param name="validatingAirline">Two character airline code which is to be checked for Net Remittance.</param>
        /// <returns>True if airline is in the list of Net Remittance airlines.</returns>
        public static bool IsNetRemmitance(string validatingAirline)
        {
            bool netRemmitance = false;
            string[] netRemittanceAirlines = ConfigurationSystem.BookingEngineConfig["netRemittanceAirlines"].Split(',');
            for (int i = 0; i < netRemittanceAirlines.Length; i++)
            {
                if (validatingAirline == netRemittanceAirlines[i])
                {
                    netRemmitance = true;
                    break;
                }
            }
            return netRemmitance;
        }

        /// <summary>
        /// Gets exception information and stack trace from exception in string format.
        /// </summary>
        /// <param name="ex">exception</param>
        /// <param name="message">this message will be prefixed with the information.</param>
        /// <returns></returns>
        public static string GetExceptionInformation(Exception ex, string message)
        {
            StringBuilder messageText = new StringBuilder(2048);
            messageText.Append("\r\n");
            messageText.Append(message);                    // prefixing the message given.
            messageText.Append("\r\n--------------------------\r\nError: ");
            messageText.Append(ex.Message);                 // appending exception message.
            messageText.Append("\r\nTargetsite: ");
            messageText.Append(ex.TargetSite);              // appending the method from where the exception originated.
            messageText.Append("\r\nSource: ");
            messageText.Append(ex.Source);                  // appending the name of application that caused exception.
            messageText.Append("\r\n\nStackTrace:\r\n");
            messageText.Append(ex.StackTrace);              // appending the stack trace.

            // checking if additional data is there in exception.
            if (ex.Data != null)
            {
                messageText.Append("\r\n\nAdditional Data: \r\n\t");
                // appending the data infromation with exception.
                foreach (DictionaryEntry de in ex.Data)
                {
                    messageText.Append("\r\n");
                    messageText.Append(de.Key);
                    messageText.Append(": ");
                    messageText.Append(de.Value);
                }
            }
            string excepMessage = messageText.ToString();
            if (ex.InnerException != null)
            {
                excepMessage = GetExceptionInformation(ex.InnerException, excepMessage + "\r\nInner Exception\r\n");
            }
            return excepMessage;
        }
        /// <summary>
        /// check whether the trip is domestic or international
        /// </summary>
        /// <param name="segment"></param>
        /// <param name="country"></param>
        /// <returns></returns>
       /* public static bool IsDomestic(FlightInfo[] segment, string country)
        {
            bool result = true;
            if (country != segment[0].Origin.CountryCode)
            {
                result = false;
            }
            else
            {
                for (int i = 0; i < segment.Length; i++)
                {
                    if (country != segment[i].Destination.CountryCode)
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }*/

        /// <summary>
        /// This Method will get currency symbol by taking currency code
        /// </summary>
        /// <param name="currencyCode"></param>
        /// <returns></returns>
        public static string GetCurrencySymbol(string currencyCode)
        {
            string symbol = string.Empty;
            if (currencyCode == "USD")
            {
                symbol = "$";
            }
            else if (currencyCode == "GBP")
            {
                symbol = "�";
            }
            else if (currencyCode == "EUR")
            {
                symbol = "�";
            }
            else if (currencyCode == "AUD")
            {
                symbol = "A$";
            }
            else if (currencyCode == "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
            {
                symbol = "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "";
            }
            else
            {
                symbol = currencyCode;
            }
            return symbol;
        }


        /// <summary>
        /// This method compares the switch string passed to against the value in config file
        /// </summary>
        /// <param name="strSource"></param>
        /// <param name="strDBSwitch"></param>
        /// <returns></returns>
        public static bool CheckSwitch(string source, SwitchType switchType)
        {
            string strDBSwitch = string.Empty;
            if (switchType == SwitchType.Air)
            {
                strDBSwitch = ConfigurationSystem.SwitchConfig["Air"];
            }
            else if (switchType == SwitchType.Hotel)
            {
                strDBSwitch = ConfigurationSystem.SwitchConfig["Hotel"];
            }
            //Convert the Hexadecimal value into uint.
            uint i = GetHexValue(strDBSwitch.ToString());
            uint b = GetHexValue(source.ToString());
            bool bExists = (i & b) > 0 ? true : false;
            return bExists;
        }


        /// <summary>
        /// This is the function used for returning uint value from hexadecimal value.?
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private static uint GetHexValue(String text)
        {
            uint result = 0;
            String parseText = text;
            if (parseText.StartsWith("0x", true, CultureInfo.InvariantCulture))
            {
                parseText = text.Substring(2);
            }


            if (UInt32.TryParse(parseText, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out result) == false)
            {
                throw new ArgumentException("Text value must be a valid hexidecimal number" + text);
            }

            return result;
        }
    }
    public enum SwitchType : int
    {
        Air = 1,
        Hotel = 2
    }
}