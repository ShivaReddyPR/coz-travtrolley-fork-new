using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;
using Technology.Data;
using System.Data;

namespace Technology.BookingEngine
{
    public class MiscBooking
    {
        #region Private Members

        /// <summary>
        ///  Unique identity number for misc booking
        /// </summary>
        int miscBookingId=0;
        /// <summary>
        /// Agency id of the agency who made the booking
        /// </summary>
        int agencyId=0;
        /// <summary>
        /// 
        /// </summary>
        string referenceNo = string.Empty;
        /// <summary>
        /// Product Type Id
        /// </summary>
        int productTypeId;
        decimal rateOfExchange;
        string description = string.Empty;
        string supplierBillNo = string.Empty;
        int supplierId = 0;
        string supplierCurrency = string.Empty;
        string clientCurrency = string.Empty;
        string glAccountCode = string.Empty;
        bool isDomestic = false;
        private PriceAccounts price = new PriceAccounts();
        int priceId = 0;
        /// <summary>
        /// Detail of the passenger
        /// </summary>
        string paxDetail;
        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;

        #endregion

        #region Public Properties

        /// <summary>
        ///  Unique identity number for misc booking
        /// </summary>
        public int MiscBookingId
        {
            set { miscBookingId = value; }
            get { return miscBookingId; }
        }
        /// <summary>
        /// Agency id of the agency who made the booking
        /// </summary>
        public int AgencyId
        {
            set { agencyId = value; }
            get { return agencyId; }
        }
        public bool IsDomestic
        {
            get
            {
                return isDomestic;
            }
            set
            {
                isDomestic = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ReferenceNo
        {
            set { referenceNo = value; }
            get { return referenceNo; }
        }
        /// <summary>
        /// Product Type Id
        /// </summary>
        public int ProductTypeId
        {
            set { productTypeId = value; }
            get { return productTypeId; }
        }

        public decimal RateOfExchange
        {
            set { rateOfExchange = value; }
            get { return rateOfExchange; }
        }

        public PriceAccounts Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
            }
        }
        public string Description
        {
            set { description = value; }
            get { return description; }
        }

        public string SupplierBillNo
        {
            set { supplierBillNo = value; }
            get { return supplierBillNo; }
        }

        public int SupplierId
        {
            set { supplierId = value; }
            get { return supplierId; }
        }

        public string SupplierCurrency
        {
            set { supplierCurrency = value; }
            get { return supplierCurrency; }
        }

        public string ClientCurrency
        {
            set { clientCurrency = value; }
            get { return clientCurrency; }
        }

        public string GlAccountCode
        {
            set { glAccountCode = value; }
            get { return glAccountCode; }
        }

        public int PriceId
        {
            set { priceId = value; }
            get { return priceId; }
        }
        /// <summary>
        /// Detail of the passenger
        /// </summary>
        public string PaxDetail
        {
            set { paxDetail = value; }
            get { return paxDetail; }
        }
        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        #endregion

        #region Public Methods

        public void Save()
        {
            Trace.TraceInformation("MiscBooking.Save entered : ");

            if (miscBookingId > 0)
            {
                SqlParameter[] paramList = new SqlParameter[15];
                paramList[0] = new SqlParameter("@miscInvoiceBookingId", miscBookingId);
                paramList[1] = new SqlParameter("@agencyId", agencyId);
                paramList[2] = new SqlParameter("@referenceNo", referenceNo);
                paramList[3] = new SqlParameter("@productType", productTypeId);
                paramList[4] = new SqlParameter("@rateOfExchange", rateOfExchange);
                paramList[5] = new SqlParameter("@paxDetail", paxDetail);
                paramList[6] = new SqlParameter("@description", description);
                paramList[7] = new SqlParameter("@supplierBillNo", supplierBillNo);
                paramList[8] = new SqlParameter("@supplierId", supplierId);
                paramList[9] = new SqlParameter("@supplierCurrency", supplierCurrency);
                paramList[10] = new SqlParameter("@clientCurrency", clientCurrency);
                paramList[11] = new SqlParameter("@glAccountCode", glAccountCode);
                paramList[12] = new SqlParameter("@isDomestic", isDomestic);
                paramList[13] = new SqlParameter("@priceId", priceId);
                paramList[14] = new SqlParameter("@loggedMemberId", createdBy);
                
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateMiscBooking, paramList);
                Trace.TraceInformation("MiscBooking.Update exiting : rowsAffected = " + rowsAffected);
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[15];
                paramList[0] = new SqlParameter("@miscInvoiceBookingId", SqlDbType.Int);
                paramList[0].Direction = ParameterDirection.Output;
                paramList[1] = new SqlParameter("@agencyId", agencyId);
                paramList[2] = new SqlParameter("@referenceNo", referenceNo);
                paramList[3] = new SqlParameter("@productType", productTypeId);
                paramList[4] = new SqlParameter("@rateOfExchange", rateOfExchange);
                paramList[5] = new SqlParameter("@paxDetail", paxDetail);
                paramList[6] = new SqlParameter("@description", description);
                paramList[7] = new SqlParameter("@supplierBillNo", supplierBillNo);
                paramList[8] = new SqlParameter("@supplierId", supplierId);
                paramList[9] = new SqlParameter("@supplierCurrency", supplierCurrency);
                paramList[10] = new SqlParameter("@clientCurrency", clientCurrency);
                paramList[11] = new SqlParameter("@glAccountCode", glAccountCode);
                paramList[12] = new SqlParameter("@isDomestic", isDomestic);
                paramList[13] = new SqlParameter("@priceId", priceId);
                paramList[14] = new SqlParameter("@loggedMemberId", createdBy);

                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddMiscBooking, paramList);
                miscBookingId = (int)paramList[0].Value;
                Trace.TraceInformation("MiscBooking.Save exiting : rowsAffected = " + rowsAffected);
            }
        }

        public void Load(int miscBookingId)
        {
            Trace.TraceInformation("MiscInvoiceBooking.Load entered: miscBookingId" + miscBookingId);
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                using (SqlConnection connection = Dal.GetConnection())
                {
                    paramList[0] = new SqlParameter("@miscBookingId", miscBookingId);
                    SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetMiscBookingNyId, paramList, connection);
                    if (dataReader.Read())
                    {
                        this.miscBookingId = miscBookingId;
                        agencyId = Convert.ToInt32(dataReader["agencyId"]);
                        referenceNo = dataReader["referenceNo"].ToString();
                        productTypeId = Convert.ToInt32(dataReader["productType"]);
                        rateOfExchange = Convert.ToDecimal(dataReader["rateOfExchange"]);
                        paxDetail = dataReader["paxDetail"].ToString();
                        priceId = Convert.ToInt32(dataReader["priceId"]);
                        price.Load(priceId);
                        description = dataReader["description"].ToString();
                        supplierBillNo = dataReader["supplierBillNo"].ToString();
                        supplierId = Convert.ToInt32(dataReader["supplierId"]);
                        supplierCurrency = dataReader["supplierCurrency"].ToString();
                        clientCurrency = dataReader["clientCurrency"].ToString();
                        glAccountCode = dataReader["glAccountCode"].ToString();
                        isDomestic = (bool)dataReader["isDomestic"];
                        createdBy = Convert.ToInt32(dataReader["supplierId"]);
                        createdOn = Convert.ToDateTime(dataReader["createdOn"]);
                        lastModifiedBy = Convert.ToInt32(dataReader["lastModifiedBy"]);
                        lastModifiedOn = Convert.ToDateTime(dataReader["lastModifiedOn"]);

                    }
                    dataReader.Close();
                    connection.Close();
                }
            }
            catch (SqlException ex)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.Account, CoreLogic.Severity.High, 0, ex.Message, "");
            }
            Trace.TraceInformation("MiscBookingInvoice.Load exited: miscBookingId" + miscBookingId);
        }

        #endregion
    }
}
