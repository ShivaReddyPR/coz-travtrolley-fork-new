using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;

namespace Technology.BookingEngine
{
    [Serializable]
    public class HotelRoom
    {
        int roomId;
        int hotelId;
        string roomTypeCode;
        string ratePlanCode;
        string noOfUnits;
        int adultCount;
        int childCount; // - Optional
        List<int> childAge; // It will contain Child Age 
        string roomName;
        List<string> ameneties;
        int priceId;
        HotelRoomFareBreakDown[] roomFareBreakDown;
        decimal extraGuestCharge;
        decimal childCharge;
        bool extraBed;
        int noOfExtraBed;
        int noOfCots;
        bool sharingBed;
        List<HotelPassenger> paasengerInfo;

        /// <summary>
        /// Price detail of one hotel booking
        /// </summary>
        PriceAccounts price;
        decimal previousFare;

        public int RoomId
        {
            get { return roomId; }
            set { roomId = value; }
        }

        public int HotelId
        {
            get { return hotelId; }
            set { hotelId = value; }
        }

        public string RoomTypeCode
        {
            get { return roomTypeCode; }
            set { roomTypeCode = value; }
        }

        public string RatePlanCode
        {
            get { return ratePlanCode; }
            set { ratePlanCode = value; }
        }

        public string NoOfUnits
        {
            get { return noOfUnits; }
            set { noOfUnits = value; }
        }

        public int AdultCount
        {
            get { return adultCount; }
            set { adultCount = value; }
        }

        public int ChildCount
        {
            get { return childCount; }
            set { childCount = value; }
        }

        public List<int> ChildAge
        {
            get { return childAge; }
            set { childAge = value; }
        }

        public string RoomName
        {
            get { return roomName; }
            set { roomName = value; }
        }

        public List<string> Ameneties
        {
            get { return ameneties; }
            set { ameneties = value; }
        }

        public int PriceId
        {
            get { return priceId; }
            set { priceId = value; }
        }

        public HotelRoomFareBreakDown[] RoomFareBreakDown
        {
            get { return roomFareBreakDown; }
            set { roomFareBreakDown = value; }
        }

        public decimal ExtraGuestCharge
        {
            get { return extraGuestCharge; }
            set { extraGuestCharge = value; }
        }
        public decimal ChildCharge
        {
            get { return childCharge; }
            set { childCharge = value; }
        }
        public PriceAccounts Price
        {
            get { return price; }
            set { price = value; }
        }

        public decimal PreviousFare
        {
            get { return previousFare; }
            set { previousFare = value; }
        }
        public bool ExtraBed
        {
            get { return extraBed; }
            set { extraBed = value; }
        }
        public int NoOfExtraBed
        {
            get { return noOfExtraBed; }
            set { noOfExtraBed = value; }
        }
        public int NoOfCots
        {
            get { return noOfCots; }
            set { noOfCots = value; }
        }
        public bool SharingBed
        {
            get { return sharingBed; }
            set { sharingBed = value; }
        }

        public List<HotelPassenger> PassenegerInfo
        {
            get { return paasengerInfo; }
            set { paasengerInfo = value; }
        }
        # region Method

        public int Save()
        {
            //TODO: validate that the fields are properly set before calling the SP
            Trace.TraceInformation("HotelRoom.Save entered.");

            SqlParameter[] paramList = new SqlParameter[17];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            paramList[1] = new SqlParameter("@adultCount", adultCount);
            paramList[2] = new SqlParameter("@childCount", childCount);
            string childAgeString = string.Empty;
            if (childAge != null)
            {
                for (int i = 0; i < childAge.Count; i++)
                {
                    childAgeString = childAgeString + "|" + childAge[i];
                }
            }
            paramList[3] = new SqlParameter("@childAge", childAgeString);
            paramList[4] = new SqlParameter("@roomName", roomName);
            paramList[5] = new SqlParameter("@roomTypeCode", roomTypeCode);
            paramList[6] = new SqlParameter("@ratePlanCode", ratePlanCode);
            string amenetiesString = string.Empty;
            if (ameneties != null)
            {
                for (int i = 0; i < ameneties.Count; i++)
                {
                    amenetiesString = amenetiesString + "|" + ameneties[i];
                }
            }
            paramList[7] = new SqlParameter("@amenities", amenetiesString);
            paramList[8] = new SqlParameter("@noOfUnit", noOfUnits);
            paramList[9] = new SqlParameter("@extraGuestCharge", extraGuestCharge);
            paramList[10] = new SqlParameter("@priceId", price.PriceId);
            paramList[11] = new SqlParameter("@roomId", SqlDbType.Int);
            paramList[12] = new SqlParameter("@extraBed", extraBed);
            paramList[13] = new SqlParameter("@noOfCots", noOfCots);
            paramList[14] = new SqlParameter("@sharingBed", sharingBed);
            if (noOfExtraBed == 0)
            {
                if (extraBed)//if true in the previous booking default 1 
                {
                    noOfExtraBed = 1;
                }
                else
                {
                    noOfExtraBed = 0;
                }
            }
            paramList[15] = new SqlParameter("@noOfExtraBed", noOfExtraBed);
            paramList[16] = new SqlParameter("@childCharge", childCharge);
            paramList[11].Direction = ParameterDirection.Output;

            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddHotelRoom, paramList);
            roomId = (int)paramList[11].Value;

            // saving room fare break
            for (int i = 0; i < roomFareBreakDown.Length; i++)
            {
                roomFareBreakDown[i].RoomId = roomId;
                roomFareBreakDown[i].Save();
            }
            foreach (HotelPassenger passInfo in paasengerInfo)
            {
                passInfo.RoomId = roomId;
                passInfo.HotelId = hotelId;
                passInfo.Save();
            }
            Trace.TraceInformation("HotelRoom.Save exiting");

            return roomId;
        }

        public HotelRoom[] Load(int hotelId)
        {
            Trace.TraceInformation("HotelRoom.Load entered : hotelId = " + hotelId);
            if (hotelId <= 0)
            {
                throw new ArgumentException("HotelId Id should be positive integer", "hotelId");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@hotelId", hotelId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetHotelRoomByHotelId, paramList, connection);
            List<HotelRoom> hotelRooms = new List<HotelRoom>();
            while (data.Read())
            {
                HotelRoom hroom = new HotelRoom();
                hroom.roomId = Convert.ToInt32(data["roomId"]);
                hroom.hotelId = Convert.ToInt32(data["hotelId"]);
                hroom.adultCount = Convert.ToInt16(data["adultCount"]);
                string ameneties = Convert.ToString(data["amenities"]);
                string[] amenetiesArray = ameneties.Split('|');
                hroom.ameneties = new List<string>();
                for (int i = 0; i < amenetiesArray.Length; i++)
                {
                    if (amenetiesArray[i] != "")
                    {
                        hroom.ameneties.Add(amenetiesArray[i]);
                    }
                }

                //hroom.childAge
                string childAge = Convert.ToString(data["childAge"]);
                string[] childAgeList = childAge.Split('|');
                hroom.childAge = new List<int>();
                for (int i = 0; i < childAgeList.Length; i++)
                {
                    if (childAgeList[i] != "" && childAgeList[i] != null)
                    {
                        hroom.childAge.Add(Convert.ToInt16(childAgeList[i]));
                    }
                }

                hroom.childCount = Convert.ToInt32(data["childCount"]);
                hroom.noOfUnits = Convert.ToString(data["noOfUnit"]);
                hroom.priceId = Convert.ToInt32(data["priceId"]);
                hroom.ratePlanCode = data["ratePlanCode"].ToString();
                hroom.extraGuestCharge = Convert.ToDecimal(data["extraGuestCharge"]);
                hroom.childCharge = Convert.ToDecimal(data["childCharge"]);
                hroom.roomFareBreakDown = HotelRoomFareBreakDown.Load(hroom.roomId);

                hroom.roomName = data["roomName"].ToString();
                hroom.roomTypeCode = data["roomTypeCode"].ToString();
                if (!Convert.IsDBNull(data["extraBed"]))
                {
                    hroom.extraBed = Convert.ToBoolean(data["extraBed"]);
                }
                if (!Convert.IsDBNull(data["noOfExtraBed"]))
                {
                    hroom.noOfExtraBed = Convert.ToInt32(data["noOfExtraBed"]);
                }
                if (!Convert.IsDBNull(data["noOfCots"]))
                {
                    hroom.noOfCots = Convert.ToInt32(data["noOfCots"]);
                }
                if (!Convert.IsDBNull(data["sharingBed"]))
                {
                    hroom.sharingBed = Convert.ToBoolean(data["sharingBed"]);
                }
                //price information
                PriceAccounts pAcc = new PriceAccounts();
                pAcc.Load(hroom.priceId);
                hroom.price = pAcc;
                HotelPassenger passInfo = new HotelPassenger();
                hroom.paasengerInfo = passInfo.Load(hroom.hotelId, hroom.roomId);


                hotelRooms.Add(hroom);
            }
            data.Close();
            connection.Close();

            HotelRoom[] hotelRoomArray = hotelRooms.ToArray();
            Trace.TraceInformation("HotelRoom.Load exiting.");

            return hotelRoomArray;

        }

        public HotelRoom LoadByRoomId(int roomId)
        {
            Trace.TraceInformation("HotelRoom.LoadByRoomId entered : roomId = " + roomId);
            if (roomId <= 0)
            {
                throw new ArgumentException("RoomId Id should be positive integer", "roomId");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@roomId", roomId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetHotelRoomByRoomId, paramList, connection);
            HotelRoom hroom = new HotelRoom();
            if (data.Read())
            {
                hroom.roomId = Convert.ToInt32(data["roomId"]);
                hroom.hotelId = Convert.ToInt32(data["hotelId"]);
                hroom.adultCount = Convert.ToInt16(data["adultCount"]);
                string ameneties = Convert.ToString(data["amenities"]);
                string[] amenetiesArray = ameneties.Split('|');
                hroom.ameneties = new List<string>();
                for (int i = 0; i < amenetiesArray.Length; i++)
                {
                    if (amenetiesArray[i] != "")
                    {
                        hroom.ameneties.Add(amenetiesArray[i]);
                    }
                }

                //hroom.childAge
                string childAge = Convert.ToString(data["childAge"]);
                string[] childAgeList = childAge.Split('|');
                hroom.childAge = new List<int>();
                for (int i = 0; i < childAgeList.Length; i++)
                {
                    if (childAgeList[i] != "" && childAgeList[i] != null)
                    {
                        hroom.childAge.Add(Convert.ToInt16(childAgeList[i]));
                    }
                }

                hroom.childCount = Convert.ToInt32(data["childCount"]);
                hroom.noOfUnits = Convert.ToString(data["noOfUnit"]);
                hroom.priceId = Convert.ToInt32(data["priceId"]);
                hroom.ratePlanCode = data["ratePlanCode"].ToString();
                hroom.extraGuestCharge = Convert.ToDecimal(data["extraGuestCharge"]);

                hroom.roomFareBreakDown = HotelRoomFareBreakDown.Load(hroom.roomId);

                hroom.roomName = data["roomName"].ToString();
                hroom.roomTypeCode = data["roomTypeCode"].ToString();
                if (data["extraBed"] != DBNull.Value)
                {
                    hroom.extraBed = Convert.ToBoolean(data["extraBed"]);
                }
                if (!Convert.IsDBNull(data["noOfExtraBed"]))
                {
                    hroom.noOfExtraBed = Convert.ToInt32(data["noOfExtraBed"]);
                }
                if (data["noOfCots"] != DBNull.Value)
                {
                    hroom.noOfCots = Convert.ToInt32(data["noOfCots"]);
                }
                if (data["sharingBed"] != DBNull.Value)
                {
                    hroom.sharingBed = Convert.ToBoolean(data["sharingBed"]);
                }
                //price information
                PriceAccounts pAcc = new PriceAccounts();
                pAcc.Load(hroom.priceId);
                hroom.price = pAcc;
            }
            data.Close();
            connection.Close();

            Trace.TraceInformation("HotelRoom.LoadByRoomId exiting.");

            return hroom;
        }
        /// <summary>
        /// This Method is used to update the room information.
        /// </summary>
        /// <param name="roomId"></param>
        public void UpdateRoom(int roomId)
        {
            Trace.TraceInformation("HotelRoom.UpdateRoom entered : roomId = " + roomId);
            //first delete the fare breakup information.
            HotelRoomFareBreakDown roomFareBreakUp = new HotelRoomFareBreakDown();
            roomFareBreakUp.DeleteFareBreakDown(roomId);
            // Add the latest farebreak up Information.
            for (int i = 0; i < roomFareBreakDown.Length; i++)
            {
                roomFareBreakDown[i].RoomId = roomId;
                roomFareBreakDown[i].Save();
            }
            Trace.TraceInformation("HotelRoom.UpdateRoom Exited : roomId = " + roomId);
        }
        /// <summary>
        /// This method is used to Update the Only Pax Info
        /// </summary>
        /// <param name="prevroomId"></param>
        /// <param name="prevHotelId"></param>
        public void UpdatePax(int prevroomId, int prevHotelId)
        {
            Trace.TraceInformation("HotelRoom.UpdatePax entered : roomId = " + prevroomId);
            List<HotelPassenger> prevPaxInfo = new List<HotelPassenger>();
            HotelPassenger paxInfo = new HotelPassenger();
            prevPaxInfo = paxInfo.Load(prevHotelId, roomId);
            int i = 0;
            //update the Passenger information.
            foreach (HotelPassenger passInfo in paasengerInfo)
            {
                passInfo.PaxId = prevPaxInfo[i++].PaxId;
                passInfo.Update();
            }
            Trace.TraceInformation("HotelRoom.UpdatePax exit : roomId = " + prevroomId);
        }

        # endregion

    }
}
