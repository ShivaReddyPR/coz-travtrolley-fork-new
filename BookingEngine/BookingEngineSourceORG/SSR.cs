using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Technology.Data;

namespace Technology.BookingEngine
{
    public enum SSRStatus
    {
        Accepted = 1,
        Denied = 2,
        Unknown = 3,
        Deleted = 4
    }
    [Serializable]
    public class SSR
    {
        /// <summary>
        /// 
        /// </summary>
        private int ssrId;
        public int SsrId
        {
            get
            {
                return ssrId;
            }
            set
            {
                ssrId = value;
            }
        }
        
        //FlightId of the itinerary to which the SSR belongs
        private int flightId;
        public int FlightId
        {
            get
            {
                return flightId;
            }
            set
            {
                flightId = value;
            }
        }
        
        //Id of the Pax corresponding to whom this particular SSR is being Stored
        private int paxId;
        public int PaxId
        {
            get
            {
                return paxId;
            }
            set
            {
                paxId = value;
            }
        }
        
        //SSR Code E.g. - FQTV for frequent flyer no., Meal for meal request
        private string ssrCode;
        public string SsrCode
        {
            get
            {
                return ssrCode;
            }
            set
            {
                ssrCode = value;
            }
        }
        
        private string detail;
        public string Detail
        {
            get
            {
                return detail;
            }
            set
            {
                detail = value;
            }
        }
        
        private SSRStatus status;
        public SSRStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        
        private string ssrStatus;
        public string SsrStatus
        {
            get
            {
                return ssrStatus;
            }
            set
            {
                ssrStatus = value;
            }
        }

        public void Save()
        {
            Trace.TraceInformation("SSR.Save entered.");
            SqlParameter[] paramList = new SqlParameter[6];
            paramList[0] = new SqlParameter("paxId", paxId);
            paramList[1] = new SqlParameter("flightId", flightId);
            paramList[2] = new SqlParameter("ssrCode", ssrCode);
            paramList[3] = new SqlParameter("detail", detail);
            paramList[4] = new SqlParameter("status", (int)status);
            if (ssrStatus == null || ssrStatus.Length == 0)
            {
                paramList[5] = new SqlParameter("ssrStatus", DBNull.Value);
            }
            else
            {
                paramList[5] = new SqlParameter("ssrStatus", ssrStatus);
            } 
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddSSR, paramList);            
            Trace.TraceInformation("SSR.Save exited.");
        }

        public static List<SSR> GetList(int flightId)
        {
            Trace.TraceInformation("SSR.GetList entered.");
            if (flightId <= 0)
            {
                throw new ArgumentException("FlightId should be positive integer", "flightId");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetSSRDetail, paramList, connection);

            List<SSR> ssrList = new List<SSR>();
            while (data.Read())
            {
                SSR ssr = new SSR();
                ssr.ssrId = Convert.ToInt32(data["ssrId"]);
                ssr.paxId = Convert.ToInt32(data["paxId"]);
                ssr.flightId = Convert.ToInt32(data["flightId"]);
                ssr.paxId = Convert.ToInt32(data["paxId"]);
                ssr.ssrCode = Convert.ToString(data["ssrCode"]);
                ssr.detail = Convert.ToString(data["detail"]);
                ssr.status = (SSRStatus)(Convert.ToInt32(data["status"]));
                if (data["ssrStatus"] == DBNull.Value)
                {
                    ssr.ssrStatus = string.Empty;
                }
                else
                {
                    ssr.ssrStatus = Convert.ToString(data["ssrStatus"]);
                }

                ssrList.Add(ssr);
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("SSR.GetList exited.");
            return ssrList;
        }
        public static void Delete(int flightId)
        {
            Trace.TraceInformation("SSR.Load entered.");
            if (flightId <= 0)
            {
                throw new ArgumentException("FlightId should be positive integer", "flightId");
            }
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@flightId", flightId);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.DeleteSSR, paramList);
            Trace.TraceInformation("SSR.Delete exited.");
        }

        public static SSR Copy(SSR ssr)
        {
            SSR copy = new SSR();
            copy.ssrId = ssr.ssrId;
            copy.paxId = ssr.paxId;
            copy.flightId = ssr.flightId;
            copy.ssrCode = ssr.ssrCode;
            copy.ssrStatus = ssr.ssrStatus;
            copy.status = ssr.status;
            copy.detail = ssr.detail;
            return copy;
        }
    }
}
