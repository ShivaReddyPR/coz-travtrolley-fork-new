using System;
using System.Collections.Generic;
using System.Text;
using Technology.Configuration;
using CoreLogic;
using Technology.Data;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Technology.BookingEngine
{
   public enum TransferLocationType
    {
        PickUp=0,
        Dropoff=1
    }
    [Serializable] 
    public class TransferRequest
    {
        private bool immediateConfirmationOnly;
        private string itemName;
        private string itemCode;
        private DateTime trasnferDate;
        private int numberOfPassengers;
        private string preferredLanguage;
        private string alternateLanguage;
        private string pickUpCityCode;
        private string pickUpCode;
        private string pickUpPointCode;
        private string dropOffCityCode;
        private string dropOffCode;
        private string dropOffPointCode;
       

        public bool ImmediateConfirmationOnly
        {
            get { return immediateConfirmationOnly; }
            set { immediateConfirmationOnly = value; }
        }
        public string ItemName
        {
            get { return itemName; }
            set { itemName = value; }
        }
        public string ItemCode
        {
            get { return itemCode; }
            set { itemCode = value; }
        }
        public DateTime TrasnferDate
        {
            get {return trasnferDate; }
            set { trasnferDate = value; }
        }
        public int NumberOfPassengers
        {
            get { return numberOfPassengers; }
            set { numberOfPassengers = value; }
        }
        public string PreferredLanguage
        {
            get { return preferredLanguage; }
            set { preferredLanguage = value; }
        }
        public string AlternateLanguage
        {
            get { return alternateLanguage; }
            set { alternateLanguage = value; }
        }
        public string PickUpCityCode
        {
            get { return pickUpCityCode; }
            set { pickUpCityCode = value; }
        }
        public string PickUpCode
        {
            get { return pickUpCode; }
            set { pickUpCode = value; }
        }
        public string PickUpPointCode
        {
            get { return pickUpPointCode; }
            set { pickUpPointCode = value; }
        }
        public string DropOffCityCode
        {
            get { return dropOffCityCode; }
            set { dropOffCityCode = value; }
        }
        public string DropOffCode
        {
            get { return dropOffCode; }
            set { dropOffCode = value; }
        }
        public string DropOffPointCode
        {
            get { return dropOffPointCode; }
            set { dropOffPointCode = value; }
        }

        /// <summary>
        /// This Method is used to get All Transfer Locations
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetTransferLocation()
        {
            Trace.TraceInformation("TransferRequest.GetTransferLocation entered ");
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            Dictionary<string, string> locList = new Dictionary<string, string>();
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetTransferLocations, paramList, connection);
            try
            {
                while(data.Read())
                {
                    locList.Add(data["locationCode"].ToString(), data["locationName"].ToString());
                }
                data.Close();
                connection.Close();
            }
            catch (Exception exp)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.Exception, CoreLogic.Severity.High, 1, "Exception in Loading Transfer Location details , Message: " + exp.Message, "");
                data.Close();
                connection.Close();
            }
            Trace.TraceInformation("TransferRequest.GetTransferLocation exit ");
            return locList;
          
        }


    }
   
}
