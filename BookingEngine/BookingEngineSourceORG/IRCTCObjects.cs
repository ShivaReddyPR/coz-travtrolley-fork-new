using System;
using System.Collections.Generic;
using System.Text;
using Technology.BookingEngine;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Diagnostics;
using CoreLogic;

namespace Technology.BookingEngine
{
    public struct Train
    {
        private string trainNumber;
        private string trainName;
        private string source;
        private string destination;

        #region public properties
        public string TrainNumber
        {
            get { return trainNumber; }
            set { trainNumber = value; }
        }      

        public string TrainName
        {
            get { return trainName; }
            set { trainName = value; }
        }       

        public string Source 
        {
            get { return source; }
            set { source = value; }
        }        

        public string Destination
        {
            get { return destination; }
            set { destination = value; }
        }
        #endregion

        public static List<string> GetTrainNumbers()
        {
            List<string> trainNos = new List<string>();
            SqlDataReader datareader;
            SqlConnection connection = Dal.GetConnection();
            try
            {
                SqlParameter[] paramlist = new SqlParameter[0];
                datareader=Dal.ExecuteReaderSP(SPNames.GetTrainNumbers, paramlist, Dal.GetConnection());
                while (datareader.Read())
                {
                    trainNos.Add(Convert.ToString(datareader["trainNumber"]));
                }
                datareader.Close();
                datareader.Dispose();
            }
            catch (DalException ex)
            {
                Audit.Add(EventType.Exception, Severity.Low, 0, "Unable to receive trainnos from database " + ex.Message, "");
                throw new Exception();
            }
            finally
            {
                connection.Close();
            }
            return trainNos;
        }

        public static DataTable GetIRCTCTrainList()
        {
            SqlParameter[] paramList = new SqlParameter[0];
            DataTable trainList = Dal.FillDataTableSP(SPNames.GetTrainList, paramList);
            return trainList;
        }
    }    

    public class TrainRoutes
    {
        public static DataTable AddTrainRoute(List<TrainRouteInfo> routeInfoList)
        {
            Trace.TraceInformation("IRCTC.AddTrainRoute entered");
            SqlConnection connection = Dal.GetConnection();
            SqlCommand command = new SqlCommand(SPNames.GetIRCTCTrainRouteSchema, connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable dt = new DataTable();
            adapter.Fill(dt);

            foreach (TrainRouteInfo response in routeInfoList)
            {
                DataRow row = dt.NewRow();
                row["sno"] = response.Slno;
                row["stnCode"] = response.StationCode;
                row["stnName"] = response.StationName;
                row["routeNo"] = response.RouteNo;
                row["arrTime"] = response.ArrTime;
                row["depTime"] = response.DepTime;
                row["distance"] = response.Distance;
                row["day"] = response.Day;
                row["remarks"] = response.Remarks;
                row["trainNumber"] = response.TrainNumber;
                row["trainName"] = response.TrainName;
                dt.Rows.Add(row);
            }
            SqlCommandBuilder cmd = new SqlCommandBuilder(adapter);
            adapter.Update(dt);
            connection.Close();
            adapter.Dispose();
            Trace.TraceInformation("IRCTC.AddTrainRoute exited");
            return dt;
        
        }

        public static int DeleteTrainRoutes(string trainNos)
        {
            SqlParameter[] paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@trainNos", trainNos);
            int rowsaffected = 0;
            rowsaffected = Dal.ExecuteNonQuerySP(SPNames.DeleteTrainRoutes, paramlist);
            return rowsaffected;
        }

        public static int DeleteAllTrainRoutes()
        {
            SqlParameter[] paramlist = new SqlParameter[0];
            int rowsaffected=0;
            try
            {
               rowsaffected = Dal.ExecuteNonQuerySP(SPNames.DeleteAllTrainRoutes, paramlist);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Unable to Delete AllTrainRoutes.Exception "+ex.Message,"");
                throw new Exception();
            }
            return rowsaffected;
        }
        public static int GetTrainsDayOfArrival(string trainNumber, string StationCode)
        {
            Trace.TraceInformation("TrainRoutes.GetTrainsDayOfArrival entered");
            int day = 0;
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@trainNumber", trainNumber);
            paramList[1] = new SqlParameter("@statonCode", StationCode);           
            using (SqlConnection con = Dal.GetConnection())
            {
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetTrainsDayOfArrival, paramList, con);
                if (data.Read())
                {
                    day = Convert.ToInt32(data["day"]);
                }
                data.Close();
            }
            Trace.TraceInformation("TrainRoutes.GetTrainsDayOfArrival exited");
            return day;
        }
        public static DataTable GetTrainRoute(string trainNumber)
        {
            Trace.TraceInformation("IRCTCStation.GetTrainRoute entered");            
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@trainNumber", trainNumber);
            DataTable stationList = Dal.FillDataTableSP(SPNames.GetTrainRoute, paramList);
            Trace.TraceInformation("IRCTCStation.GetTrainRoute exited");
            return stationList;
        }
        public static decimal GetDistanceBetweenStation(string sourceStation, string destinationStation, string trainNumber)
        {
            Trace.TraceInformation("IRCTCStation.GetDistanceBetweenStation entered");
            decimal distance = 0;
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@sourceStation",sourceStation);
            paramList[1] = new SqlParameter("@destinationStation", destinationStation);
            paramList[2] = new SqlParameter("@trainNumber", trainNumber);
            using(SqlConnection con = Dal.GetConnection())
            {
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetDistanceBetweenStations, paramList, con);
                if (data.Read())
                {
                    distance = Convert.ToDecimal(data["distance"]);                
                }
                data.Close();
            }
            Trace.TraceInformation("IRCTCStation.GetDistanceBetweenStation exited");
            return distance;
        }
        public static List<string> GetTrainSourceAndDestinationStation(string trainNumber)
        {
            Trace.TraceInformation("IRCTCStation.GetDistanceBetweenStation entered");
            List<string> sourceDestinationList = new List<string>();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@trainNumber", trainNumber);
            using (SqlConnection con = Dal.GetConnection())
            {
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetTrainSourceAndDestinationStation, paramList, con);
                while (data.Read())
                {
                    sourceDestinationList.Add(Convert.ToString(data["stnCode"]));
                }
                data.Close();
            }
            Trace.TraceInformation("IRCTCStation.GetDistanceBetweenStation exited");
            return sourceDestinationList;  
        
        }
        /// <summary>
        /// Gets the starting time and ending time of a train at source and destination station
        /// </summary>
        /// <param name="sourceStation"></param>
        /// <param name="destinationStation"></param>
        /// <param name="trainNumber"></param>
        /// <returns></returns>
        public static string[,] GetTrainTimeAtStation(string sourceStation, string destinationStation, string trainNumber)
        {
            Trace.TraceInformation("IRCTCStation.GetTrainTimeAtStation entered");
            string[,] timeAndDay = new string[2,2];
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@sourceStnCode", sourceStation);
            paramList[1] = new SqlParameter("@destStnCode", destinationStation);
            paramList[2] = new SqlParameter("@trainNumber", trainNumber);
            using (SqlConnection con = Dal.GetConnection())
            {
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetTrainTimeAtStation, paramList, con);
                int i = 0;
                while (data.Read() && i < 2)
                {
                    if (data["time"] == DBNull.Value || data["day"] == DBNull.Value)
                    {
                        Audit.Add(EventType.IRCTCSearch, Severity.High, 0, "Train source and destination time was not found for train = " + trainNumber + " for source = " + sourceStation + "and destination = " + destinationStation, "");
                    }
                    else
                    {
                        timeAndDay[i, 0] = data["time"].ToString();
                        timeAndDay[i, 1] = data["day"].ToString();
                    }
                    i++;
                }
                data.Close();
            }
            Trace.TraceInformation("IRCTCStation.GetTrainTimeAtStation exited");
            return timeAndDay;
        }
    }
    /// <summary>
    /// IRCTC station object for maintaining station list
    /// </summary>
    public struct IRCTCStation
    {
        public string StationName;
        public string StationCode;
        public string CityName;
        /// <summary>
        /// Returns the data table that contains all the trainStaion
        /// </summary>
        /// <returns></returns>
        public static DataTable GetIRCTCStationList()
        {
            Trace.TraceInformation("IRCTCStation.GetIRCTCStationList entered");
            SqlParameter[] paramList = new SqlParameter[0];
            DataTable stationList = Dal.FillDataTableSP(SPNames.GetTrainStation, paramList);            
            Trace.TraceInformation("IRCTCStation.GetIRCTCStationList exited");
            return stationList;
        }
        public static IRCTCStation GetStationByStationCode(string stationCode)
        {
            Trace.TraceInformation("IRCTCStation.GetStationByStationCode entered");
            IRCTCStation station = new IRCTCStation();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@stationCode", stationCode);            
            using (SqlConnection con = Dal.GetConnection())
            {
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetTrainStationByStationCode, paramList, con);
                if (data.Read())
                {
                    station.StationName = Convert.ToString(data["StationName"]);
                    station.StationCode = Convert.ToString(data["StationCode"]);
                    station.CityName = Convert.ToString(data["StationCity"]);
                }
                data.Close();
            }
            Trace.TraceInformation("IRCTCStation.GetStationByStationCode exited");
            return station;
        
        }
    }
    /// <summary>
    /// IRCTC search request object
    /// </summary>
    ///     
    public class IRCTCSeachRequest
    {
        string sourceStaionCode;
        string destinationStationCode;
        string irctcSearchClass;
        short dayOfJourney;
        short monthOfJourney;
        int yearOfJourney;
        int childCount;
        int adultCount;
        int seniorMenCount;
        int seniorWomenCount;
        int agencyId;
        /// <summary>
        /// Station code of the source station
        /// </summary>
        public string SourceStaionCode
        {
            get
            {
                return sourceStaionCode;
            }
            set
            {
                sourceStaionCode = value.Trim();
            }
        }
        /// <summary>
        /// Station code of the destination satation
        /// </summary>
        public string DestinationStationCode
        {
            get
            {
                return destinationStationCode;
            }
            set
            {
                destinationStationCode = value.Trim();
            }
        }
        /// <summary>
        /// ClassId of the class for which search is to be made
        /// </summary>
        public string IRCTCSearchClass
        {
            get
            {
                return irctcSearchClass;
            }
            set
            {
                if (!TrainItinerary.IRCTCClass.ContainsKey(value))
                {
                    throw new ArgumentException("Class value is not valid");
                }
                else
                {
                    irctcSearchClass = value;
                }
            }
        }
        /// <summary>
        /// Month day of the journey 
        /// </summary>
        public short DayOfJourney
        {
            get
            {
                return dayOfJourney;
            }
            set
            {
                if (value > 31 || value < 1)
                {
                    throw new ArgumentException("Day Of Journey value is not valid");
                }
                else
                {
                    dayOfJourney = value;
                }
            }
        }
        /// <summary>
        /// Month of the journey
        /// </summary>
        public short MonthOfJourney
        {
            get
            {
                return monthOfJourney;
            }
            set
            {
                if (value > 12 || value < 1)
                {
                    throw new ArgumentException("MonthOfJourney value is not valid");
                }
                else
                {
                    monthOfJourney = value;
                }
            }
        }
        /// <summary>
        /// Year of the journey
        /// </summary>
        public int YearOfJourney
        {
            get
            {
                return yearOfJourney;
            }
            set
            {
                if (value < DateTime.Now.Year || value > (DateTime.Now.Year + 1))
                {
                    throw new ArgumentException("YearOfJourney value is not valid");
                }
                else
                {
                    yearOfJourney = value;
                }
            }
        }
        public int ChildCount
        {
            get
            {
                return childCount;
            }
            set
            {
                childCount = value;
            }
        }
        public int AdultCount
        {
            get
            {
                return adultCount;
            }
            set
            {
                adultCount = value;

            }
        }
        public int SeniorMenCount
        {
            get
            {
                return seniorMenCount;
            }
            set
            {
                seniorMenCount = value;
            }
        }
        public int SeniorWomenCount
        {
            get
            {
                return seniorWomenCount;
            }
            set
            {
                seniorWomenCount = value;
            }
        }
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }     

    }
    public class IRCTCSeachResponse
    {
        private string trainNumber;
        private string trainName;
        private string originatingStationCode;
        private string originatingStationName;
        private string sourceStationCode;
        private string sourceStationName;
        private string destinationStationCode;
        private string destinationStationName;
        private string departureTimeAtSource;
        private string arrivalTimeAtDestination;
        private bool runsOnMonday;
        private bool runsOnTuesday;
        private bool runsOnWednesday;
        private bool runsOnThursday;
        private bool runsOnFriday;
        private bool runsOnSaturday;
        private bool runsOnSunday;
        private bool isTemporaryTrain;
        private TrainType irctcTrainType;
        private bool mealPreferenceAvailable;
        private bool isOriginStartingStation;
        private bool available1A;
        private bool available2A;
        private bool availableFC;
        private bool available3A;
        private bool availableCC;
        private bool availableSL;
        private bool available2S;
        private bool available3E;
        private TimeSpan duration;
        private string errorCode;
        private string errorDescription;
       
        public TimeSpan Duration
        {
            get
            {
                if (duration == new TimeSpan())
                {
                    string[,] trainTimeAtStation = TrainRoutes.GetTrainTimeAtStation(sourceStationCode, destinationStationCode, trainNumber);
                    if (trainTimeAtStation[0, 0] != null && trainTimeAtStation[0, 1] != null && trainTimeAtStation[1, 0] != null && trainTimeAtStation[1, 1] != null )
                    {
                        string timeAtSource = trainTimeAtStation[0,0];
                        string timeAtDestination = trainTimeAtStation[1,0];
                        int dayAtSource = Convert.ToInt32(trainTimeAtStation[0,1]) - 1;
                        int dayAtDestination = Convert.ToInt32(trainTimeAtStation[1,1]) - 1;
                        string[] splittedTimeAtSource = departureTimeAtSource.Split(':');
                        string[] splittedTimeAtDestination = arrivalTimeAtDestination.Split(':');
                        DateTime time1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day , Convert.ToInt32(splittedTimeAtSource[0]), Convert.ToInt32(splittedTimeAtSource[1]), 0).AddDays(dayAtSource);
                        DateTime time2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day , Convert.ToInt32(splittedTimeAtDestination[0]), Convert.ToInt32(splittedTimeAtDestination[1]), 0).AddDays(dayAtDestination);                        
                        duration = time2 - time1;
                        return duration;
                    }
                    else
                    {                        
                        return new TimeSpan(0);
                    }
                }
                else
                {
                    return duration;
                }
            }
        }
        /// <summary>
        /// Number of the train to which this result belongs
        /// </summary>
        public string TrainNumber
        {
            get
            {
                return trainNumber;
            }
            set
            {
                trainNumber = value;
            }
        }
        /// <summary>
        /// Name of the train to which this result belongs
        /// </summary>
        public string TrainName
        {
            get
            {
                return trainName;
            }
            set
            {
                trainName = value;
            }
        }
        /// <summary>
        /// Train's origin station code
        /// </summary>
        public string OriginatingStationCode
        {
            get
            {
                return originatingStationCode;
            }
            set
            {
                originatingStationCode = value;
            }
        }
        /// <summary>
        /// Train's origin station name
        /// </summary>
        public string OriginatingStationName
        {
            get
            {
                return originatingStationName;
            }
            set
            {
                originatingStationName = value;
            }
        }
        /// <summary>
        /// Source station code
        /// </summary>
        public string SourceStationCode
        {
            get
            {
                return sourceStationCode;
            }
            set
            {
                sourceStationCode = value;
            }
        }
        /// <summary>
        /// Source station name
        /// </summary>
        public string SourceStationName
        {
            get
            {
                return sourceStationName;
            }
            set
            {
                sourceStationName = value;
            }
        }
        /// <summary>
        /// Station  code of the destination
        /// </summary>
        public string DestinationStationCode
        {
            get
            {
                return destinationStationCode;
            }
            set
            {
                destinationStationCode = value;
            }
        }
        /// <summary>
        /// Station name of the destinatioon
        /// </summary>
        public string DestinationStationName
        {
            get
            {
                return destinationStationName;
            }
            set
            {
                destinationStationName = value;
            }
        }
        /// <summary>
        /// Time at which train departs from source
        /// </summary>
        public string DepartureTimeAtSource
        {
            get
            {
                return departureTimeAtSource;
            }
            set
            {
                departureTimeAtSource = value;
            }
        }
        /// <summary>
        /// Time at which train arrives at destination
        /// </summary>
        public string ArrivalTimeAtDestination
        {
            get
            {
                return arrivalTimeAtDestination;
            }
            set
            {
                arrivalTimeAtDestination = value;
            }
        }
        /// <summary>
        /// Whether the train runs on monday or not
        /// </summary>
        public bool RunsOnMonday
        {
            get
            {
                return runsOnMonday;
            }
            set
            {
                runsOnMonday = value;
            }
        }
        /// <summary>
        /// Whether the train runs on tusday or not
        /// </summary>
        public bool RunsOnTuesday
        {
            get
            {
                return runsOnTuesday;
            }
            set
            {
                runsOnTuesday = value;
            }
        }
        /// <summary>
        /// Whether the train runs on tusday or not
        /// </summary>
        public bool RunsOnWednesday
        {
            get
            {
                return runsOnWednesday;
            }
            set
            {
                runsOnWednesday = value;
            }
        }
        /// <summary>
        /// Whether the train runs on tusday or not
        /// </summary>
        public bool RunsOnThursday
        {
            get
            {
                return runsOnThursday;
            }
            set
            {
                runsOnThursday = value;
            }
        }
        /// <summary>
        /// Whether the train runs on tusday or not
        /// </summary>
        public bool RunsOnFriday
        {
            get
            {
                return runsOnFriday;
            }
            set
            {
                runsOnFriday = value;
            }
        }
        /// <summary>
        /// Whether the train runs on tusday or not
        /// </summary>
        public bool RunsOnSaturday
        {
            get
            {
                return runsOnSaturday;
            }
            set
            {
                runsOnSaturday = value;
            }
        }
        /// <summary>
        /// Whether the train runs on tusday or not
        /// </summary>
        public bool RunsOnSunday
        {
            get
            {
                return runsOnSunday;
            }
            set
            {
                runsOnSunday = value;
            }
        }
        /// <summary>
        /// Tells whether the train is a temporary train or not
        /// </summary>
        public bool IsTemporaryTrain
        {
            get
            {
                return isTemporaryTrain;
            }
            set
            {
                isTemporaryTrain = value;
            }
        }
        /// <summary>
        /// Type of the train
        /// R is Rajadhani
        /// S is Shathabdi
        /// A is Ordinary
        /// </summary>
        public TrainType IRCTCTrainType
        {
            get
            {
                return irctcTrainType;
            }
            set
            {
                irctcTrainType = value;
            }
        }
        /// <summary>
        /// Meal preference is available or not with this train
        /// </summary>
        public bool MealPreferenceAvailable
        {
            get
            {
                return mealPreferenceAvailable;
            }
            set
            {
                mealPreferenceAvailable = value;
            }
        }
        /// <summary>
        /// Tells whether the sorce station is an originating station of this train or not
        /// </summary>
        public bool IsOriginStartingStation
        {
            get
            {
                return isOriginStartingStation;
            }
            set
            {
                isOriginStartingStation = value;
            }
        }
        /// <summary>
        /// Is 1A class available
        /// </summary>
        public bool Available1A
        {
            get
            {
                return available1A;
            }
            set
            {
                available1A = value;
            }
        }
        /// <summary>
        /// Is 2A class available
        /// </summary>
        public bool Available2A
        {
            get
            {
                return available2A;
            }
            set
            {
                available2A = value;
            }
        }
        /// <summary>
        /// Is FC class available
        /// </summary>
        public bool AvailableFC
        {
            get
            {
                return availableFC;
            }
            set
            {
                availableFC = value;
            }
        }
        /// <summary>
        /// Is 3A class available
        /// </summary>
        public bool Available3A
        {
            get
            {
                return available3A;
            }
            set
            {
                available3A = value;
            }
        }
        /// <summary>
        /// Is CC class available
        /// </summary>
        public bool AvailableCC
        {
            get
            {
                return availableCC;
            }
            set
            {
                availableCC = value;
            }
        }
        /// <summary>
        /// Is SL class available
        /// </summary>
        public bool AvailableSL
        {
            get
            {
                return availableSL;
            }
            set
            {
                availableSL = value;
            }
        }
        /// <summary>
        /// Is 2S class available
        /// </summary>
        public bool Available2S
        {
            get
            {
                return available2S;
            }
            set
            {
                available2S = value;
            }
        }

        /// <summary>
        /// Is 3E class available
        /// </summary>
        public bool Available3E
        {
            get
            {
                return available3E;
            }
            set
            {
                available3E = value;
            }
        }

        public string ErrorCode
        {
            get
            {
                return errorCode;
            }
            set
            {
                errorCode = value;
            }
        }
        public string ErrorDescription
        {
            get
            {
                return errorDescription;
            }
            set
            {
                errorDescription = value;
            }
        }

    }
    public class IRCTCFareAndAvailabilityRequest
    {
        private string trainNumber;
        private string sourceCode;
        private string destinationCode;
        private short monthOfJourney;
        private short dayOfJourney;
        private int yearOfJourney;
        private int age;
        private TrainConcessionCode concessionCode;
        private string classOpted;
        private const string frclass1 = "ZZ";
        private const string frclass2 = "ZZ";
        private const string frclass3 = "ZZ";
        private const string frclass4 = "ZZ";
        private const string frclass5 = "ZZ";
        private const string frclass6 = "ZZ";
        private const string frclass7 = "ZZ";
        private const string enroute = "NONE";
        private const string viacode = "NONE";
        private TrainQuota quota;
        private string class1;
        private const string class2 = "ZZ";
        private const string class3 = "ZZ";
        private const string class4 = "ZZ";
        private const string class5 = "ZZ";
        private const string class6 = "ZZ";
        private const string class7 = "ZZ";
         

        public string TrainNumber
        {
            get
            {
                return trainNumber;
            }
            set
            {
                trainNumber = value;
            }
        }
        public string SourceCode
        {
            get
            {
                return sourceCode;
            }
            set
            {
                if (value.Length > 4)
                {
                    throw new ArgumentException("Source code can not have length greater than 4");
                }
                else
                {
                    sourceCode = value;
                }
            }
        }
        public string DestinationCode
        {
            get
            {
                return destinationCode;
            }
            set
            {
                if (value.Length > 4)
                {
                    throw new ArgumentException("Destination code can not have length greater than 4");
                }
                else
                {
                    destinationCode = value;
                }
            }
        }
        public short MonthOfJourney
        {
            get
            {
                return monthOfJourney;
            }
            set
            {
                if (value > 12 || value < 1)
                {
                    throw new ArgumentException("Month Of Journey value is not valid");
                }
                else
                {
                    monthOfJourney = value;
                }
            }
        }
        public short DayOfJourney
        {
            get
            {
                return dayOfJourney;
            }
            set
            {
                if (value > 31 || value < 1)
                {
                    throw new ArgumentException("Day Of Journey value is not valid");
                }
                else
                {
                    dayOfJourney = value;
                }
            }
        }
        public int YearOfJourney
        {
            get
            {
                return yearOfJourney;
            }
            set
            {
                if (value < DateTime.Now.Year || value > (DateTime.Now.Year + 1))
                {
                    throw new ArgumentException("Year Of Journey value is not valid");
                }
                else
                {
                    yearOfJourney = value;
                }
            }
        }
        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if (value != 0)
                {
                    age = value;
                }
                else
                {
                    throw new ArgumentException("Age cannot be zero or less than zero");
                }
            }
        }
        public TrainConcessionCode ConcessionCode
        {
            get
            {
                return concessionCode;
            }
            set
            {
                if ((int)value == 0)
                {
                    throw new ArgumentException("concession Code is not provided");
                }
                else
                {
                    concessionCode = value;
                }
            }
        }
        public string ClassOpted
        {
            get
            {
                return classOpted;
            }
            set
            {
                if (TrainItinerary.IRCTCClass.ContainsKey(value.Trim()))
                {
                    class1 = classOpted = value.Trim();
                }
            }
        }
        public string Frclass1
        {
            get
            {
                return frclass1;
            }
        }
        public string Frclass2
        {
            get
            {
                return frclass2;
            }
        }
        public string Frclass3
        {
            get
            {
                return frclass3;
            }
        }
        public string Frclass4
        {
            get
            {
                return frclass4;
            }
        }
        public string Frclass5
        {
            get
            {
                return frclass5;
            }
        }
        public string Frclass6
        {
            get
            {
                return frclass6;
            }
        }
        public string Frclass7
        {
            get
            {
                return frclass7;
            }
        }
        public string Enroute
        {
            get
            {
                return enroute;
            }
        }
        public string Viacode
        {
            get
            {
                return viacode;
            }
        }
        public TrainQuota Quota
        {
            get
            {
                return quota;
            }
            set
            {
                quota = value;
            }
        }
        public string Class1
        {
            get
            {
                return class1;
            }
        }
        public string Class2
        {
            get
            {
                return class2;
            }
        }
        public string Class3
        {
            get
            {
                return class3;
            }
        }
        public string Class4
        {
            get
            {
                return class4;
            }
        }
        public string Class5
        {
            get
            {
                return class5;
            }

        }
        public string Class6
        {
            get
            {
                return class6;
            }
        }
        public string Class7
        {
            get
            {
                return class7;
            }
        }

    
        
    }
    public class IRCTCFareAndAvailabilityResponse
    {
        private string trainName;
        private string sourceStationName;
        private string destinationStationName;        
        private string trainType;
        private decimal distance;
        private int noOfClassCodes;
        private string quotaName;
        private int noOfAvailDays;
        private string classCode;
        private decimal baseFare;
        private decimal superFastCharge;
        private decimal otherCharge;
        private decimal total;
        private decimal concessionAmount;
        private decimal netAmount;
        private decimal resvCharge;
        private Dictionary<DateTime, string> availabilityList;
        private string errorCode;
        private string errorDescription;

        #region properties
        /// <summary>
        /// Name of the train
        /// </summary>
        public string TrainName
        {
            get
            {
                return trainName;            
            }
            set
            {
                trainName = value;
            }        
        }
        /// <summary>
        /// Source station name
        /// </summary>
        public string SourceStationName
        {
            get
            {
                return sourceStationName;
            }
            set
            {
                sourceStationName = value;
            }
        }
        public string DestinationStationName
        {
            get
            {
                return destinationStationName;
            }
            set
            {
                destinationStationName = value;
            }
        }
        /// <summary>
        /// Type of the train
        /// </summary>
        public string TrainType
        {
            get
            {
                return trainType;
            }
            set
            {
                trainType = value;
            }
        }
        /// <summary>
        /// Diatance between origin and destination
        /// </summary>
        public decimal Distance
        {
            get
            {
                return distance;
            }
            set
            {
                distance = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int NoOfClassCodes
        {
            get
            {
                return noOfClassCodes;
            }
            set
            {
                noOfClassCodes = value;
            }
        }
        /// <summary>
        /// Name of the quota for which price and availability is there
        /// </summary>
        public string QuotaName
        {
            get
            {
                return quotaName;
            }
            set
            {
                quotaName = value;
            }
        }
        /// <summary>
        /// Number of days for which availability record is coming
        /// </summary>
        public int NoOfAvailDays
        {
            get
            {
                return noOfAvailDays;
            }
            set
            {
                noOfAvailDays = value;
            }
        }
        /// <summary>
        /// Code of the class for which result is coming
        /// </summary>
        public string ClassCode
        {
            get
            {
                return classCode;
            }
            set
            {
                classCode = value;
            }
        }
        public decimal BaseFare
        {
            get
            {
                return baseFare;
            }
            set
            {
                baseFare = value;
            }
        }
        public decimal SuperFastCharge
        {
            get
            {
                return superFastCharge;            
            }
            set
            {
                superFastCharge = value;
            }        
        }
        public decimal OtherCharge
        {
            get
            {
                return otherCharge;
            }
            set
            {
                otherCharge = value;
            }
        }
        public decimal Total
        {
            get
            {
                return total;
            }
            set
            {
                total = value;
            }
        }
        public decimal ConcessionAmount
        {
            get
            {
                return concessionAmount;
            }
            set
            {
                concessionAmount = value;
            }
        }
        public decimal NetAmount
        {
            get
            {
                return netAmount;
            }
            set
            {
                netAmount = value;
            }
        }
        public decimal ResvCharge
        {
            get
            {
                return resvCharge;
            }
            set
            {
                resvCharge = value;
            }
        }
        /// <summary>
        /// List containing date and availability on that date 
        /// </summary>
        public Dictionary<DateTime, string> AvailabilityList
        {
            get
            {
                return availabilityList;
            }
            set
            {
                availabilityList = value;
            }
        }
        public string ErrorCode
        {
            get
            {
                return errorCode;
            }
            set
            {
                errorCode = value;
            }
        }
        public string ErrorDescription
        {
            get
            {
                return errorDescription;
            }
            set
            {
                errorDescription = value;
            }
        }
#endregion
    }
    public class IRCTCBookRequest
    {
        #region members
        private string sourceStation;
        private string destinationStation;
        private string trainNumber;
        private string bookingClass;
        private string reservationUpto;
        private string boardingPoint;
        short dayOfJourney;
        short monthOfJourney;
        int yearOfJourney;
        TrainQuota quota;
        private string passengerName1;
        private string passengerName2 = "";
        private string passengerName3 = "";
        private string passengerName4 = "";
        private string passengerName5 = "";
        private string passengerName6 = "";
        private string childPassengerName1 = "";
        private string childPassengerName2 = "";
        private TrainPassengerGender passengerSex1;
        private TrainPassengerGender passengerSex2;
        private TrainPassengerGender passengerSex3;
        private TrainPassengerGender passengerSex4;
        private TrainPassengerGender passengerSex5;
        private TrainPassengerGender passengerSex6;
        private TrainPassengerGender childPassengerSex1;
        private TrainPassengerGender childPassengerSex2;
        private string passengerAge1 = "";
        private string passengerAge2 = "";
        private string passengerAge3 = "";
        private string passengerAge4 = "";
        private string passengerAge5 = "";
        private string passengerAge6 = "";
        private string childPassengerAge1 = "";
        private string childPassengerAge2 = "";
        private TrainSeatPreference passengerBerthPreference1;
        private TrainSeatPreference passengerBerthPreference2;
        private TrainSeatPreference passengerBerthPreference3;
        private TrainSeatPreference passengerBerthPreference4;
        private TrainSeatPreference passengerBerthPreference5;
        private TrainSeatPreference passengerBerthPreference6;
        private TrainMealPreference passengerFoodPreference1;
        private TrainMealPreference passengerFoodPreference2;
        private TrainMealPreference passengerFoodPreference3;
        private TrainMealPreference passengerFoodPreference4;
        private TrainMealPreference passengerFoodPreference5;
        private TrainMealPreference passengerFoodPreference6;
        private TrainConcessionCode passengerConcessionCode1;
        private TrainConcessionCode passengerConcessionCode2;
        private TrainConcessionCode passengerConcessionCode3;
        private TrainConcessionCode passengerConcessionCode4;
        private TrainConcessionCode passengerConcessionCode5;
        private TrainConcessionCode passengerConcessionCode6;
        private bool hasBedRoll1;
        private bool hasBedRoll2;
        private bool hasBedRoll3;
        private bool hasBedRoll4;
        private bool hasBedRoll5;
        private bool hasBedRoll6;
        private DateTime boardingDateAndTime;
        private TrainReservationChoice resChoice;
        //private IRCTCPaymentGateway paymentGatewayName;
        //IRCTCCardType astrCardType;
        //IRCTCOperatorCode operatorCode;
        private string mobileNumber;
        private const string eFlag = "eticket";
        private int passengerCount;
        //private string accountNumber ;
        //private string accountPassword ;
        //private string userid;
        //private string password;
        private string uniqueTxnID;
        private Boolean hasClassUpgrade;
         
        #endregion
        #region properties
       
        public TrainReservationChoice ResChoice
        {
            get
            {
                return resChoice;
            }
            set
            {
                resChoice = value;
            }
        }
        public string SourceStation
        {
            get
            {
                return sourceStation;
            }
            set
            {
                if (value.Length > 4)
                {
                    throw new ArgumentException("Source code can not have length greater than 4");
                }
                else
                {
                    sourceStation = value.Trim();
                }
            }
        }
        public string DestinationStation
        {
            get
            {
                return destinationStation;
            }
            set
            {
                if (value.Length > 4)
                {
                    throw new ArgumentException("Destination code can not have length greater than 4");
                }
                else
                {
                    destinationStation = value.Trim();
                }
            }
        }
        public string TrainNumber
        {
            get
            {
                return trainNumber;
            }
            set
            {
                if (value.Length > 5)
                {
                    throw new ArgumentException("Train number can not have length greater than 4");
                }
                else
                {
                    trainNumber = value.Trim();
                }
            }
        }
        /// <summary>
        /// Key of TrainItinerary.IRCTCClass
        /// </summary>
        public string BookingClass
        {
            get
            {
                return bookingClass;
            }
            set
            {
                if (!TrainItinerary.IRCTCClass.ContainsKey(value))
                {
                    throw new ArgumentException("Class value is not valid");
                }
                else
                {
                    bookingClass = value.Trim();
                }
            }
        }
        public string ReservationUpto
        {
            get
            {
                return reservationUpto;
            }
            set
            {
                if (value.Length > 4)
                {
                    throw new ArgumentException("Reservation code can not have length greater than 4");
                }
                else
                {
                    reservationUpto = value.Trim();
                }
            }
        }
        public string BoardingPoint
        {
            get
            {
                return boardingPoint;
            }
            set
            {
                if (value.Length > 4)
                {
                    throw new ArgumentException("Boarding code can not have length greater than 4");
                }
                else
                {
                    boardingPoint = value.Trim();
                }
            }
        }
        public short DayOfJourney
        {
            get
            {
                return dayOfJourney;
            }
            set
            {
                if (value > 31 || value < 1)
                {
                    throw new ArgumentException("Day Of Journey value is not valid");
                }
                else
                {
                    dayOfJourney = value;
                }
            }
        }
        public short MonthOfJourney
        {
            get
            {
                return monthOfJourney;
            }
            set
            {
                if (value > 12 || value < 1)
                {
                    throw new ArgumentException("Month Of Journey value is not valid");
                }
                else
                {
                    monthOfJourney = value;
                }
            }
        }
        public int YearOfJourney
        {
            get
            {
                return yearOfJourney;
            }
            set
            {
                if (value < DateTime.Now.Year || value > (DateTime.Now.Year + 1))
                {
                    throw new ArgumentException("YearOfJourney value is not valid");
                }
                else
                {
                    yearOfJourney = value;
                }
            }
        }
        public TrainQuota Quota
        {
            get
            {
                return quota;
            }
            set
            {
                quota = value;
            }
        }
        public string PassengerName1
        {
            get
            {
                return passengerName1;
            }
            set
            {
                if (value.Length < 17)
                {
                    passengerName1 = value.Trim();
                }
                else
                {
                    throw new ArgumentException("Length of name can not be greater than 16");
                }
            }
        }
        public string PassengerName2
        {
            get
            {
                return passengerName2;
            }
            set
            {
                if (value.Length < 17)
                {
                    passengerName2 = value.Trim();
                }
                else
                {
                    throw new ArgumentException("Length of name can not be greater than 16");
                }
            }
        }
        public string PassengerName3
        {
            get
            {
                return passengerName3;
            }
            set
            {
                if (value.Length < 17)
                {
                    passengerName3 = value.Trim();
                }
                else
                {
                    throw new ArgumentException("Length of name can not be greater than 16");
                }
            }
        }
        public string PassengerName4
        {
            get
            {
                return passengerName4;
            }
            set
            {
                if (value.Length < 17)
                {
                    passengerName4 = value.Trim();
                }
                else
                {
                    throw new ArgumentException("Length of name can not be greater than 16");
                }
            }
        }
        public string PassengerName5
        {
            get
            {
                return passengerName5;
            }
            set
            {
                if (value.Length < 17)
                {
                    passengerName5 = value.Trim();
                }
                else
                {
                    throw new ArgumentException("Length of name can not be greater than 16");
                }
            }
        }
        public string PassengerName6
        {
            get
            {
                return passengerName6;
            }
            set
            {
                if (value.Length < 17)
                {
                    passengerName6 = value.Trim();
                }
                else
                {
                    throw new ArgumentException("Length of name can not be greater than 16");
                }
            }
        }
        public TrainPassengerGender PassengerSex1
        {
            get
            {
                return passengerSex1;
            }
            set
            {
                passengerSex1 = value;
            }
        }
        public TrainPassengerGender PassengerSex2
        {
            get
            {
                return passengerSex2;
            }
            set
            {
                passengerSex2 = value;
            }
        }
        public TrainPassengerGender PassengerSex3
        {
            get
            {
                return passengerSex3;
            }
            set
            {
                passengerSex3 = value;
            }
        }
        public TrainPassengerGender PassengerSex4
        {
            get
            {
                return passengerSex4;
            }
            set
            {
                passengerSex4 = value;
            }
        }
        public TrainPassengerGender PassengerSex5
        {
            get
            {
                return passengerSex5;
            }
            set
            {
                passengerSex5 = value;
            }
        }
        public TrainPassengerGender PassengerSex6
        {
            get
            {
                return passengerSex6;
            }
            set
            {
                passengerSex6 = value;
            }
        }
        public string PassengerAge1
        {
            get
            {
                return passengerAge1;
            }
            set
            {
                passengerAge1 = value.Trim();
            }
        }
        public string PassengerAge2
        {
            get
            {
                return passengerAge2;
            }
            set
            {
                passengerAge2 = value.Trim();
            }
        }
        public string PassengerAge3
        {
            get
            {
                return passengerAge3;
            }
            set
            {
                passengerAge3 = value.Trim();
            }
        }
        public string PassengerAge4
        {
            get
            {
                return passengerAge4;
            }
            set
            {
                passengerAge4 = value.Trim();
            }
        }
        public string PassengerAge5
        {
            get
            {
                return passengerAge5;
            }
            set
            {
                passengerAge5 = value.Trim();
            }
        }
        public string PassengerAge6
        {
            get
            {
                return passengerAge6;
            }
            set
            {
                passengerAge6 = value;
            }
        }
        public TrainSeatPreference PassengerBerthPreference1
        {
            get
            {
                return passengerBerthPreference1;
            }
            set
            {
                passengerBerthPreference1 = value;
            }
        }
        public TrainSeatPreference PassengerBerthPreference2
        {
            get
            {
                return passengerBerthPreference2;
            }
            set
            {
                passengerBerthPreference2 = value;
            }
        }
        public TrainSeatPreference PassengerBerthPreference3
        {
            get
            {
                return passengerBerthPreference3;
            }
            set
            {
                passengerBerthPreference3 = value;
            }
        }
        public TrainSeatPreference PassengerBerthPreference4
        {
            get
            {
                return passengerBerthPreference4;
            }
            set
            {
                passengerBerthPreference4 = value;
            }
        }
        public TrainSeatPreference PassengerBerthPreference5
        {
            get
            {
                return passengerBerthPreference5;
            }
            set
            {
                passengerBerthPreference5 = value;
            }
        }
        public TrainSeatPreference PassengerBerthPreference6
        {
            get
            {
                return passengerBerthPreference6;
            }
            set
            {
                passengerBerthPreference6 = value;
            }
        }
        public TrainMealPreference PassengerFoodPreference1
        {
            get
            {
                return passengerFoodPreference1;
            }
            set
            {
                passengerFoodPreference1 = value;
            }
        }
        public TrainMealPreference PassengerFoodPreference2
        {
            get
            {
                return passengerFoodPreference2;
            }
            set
            {
                passengerFoodPreference2 = value;
            }
        }
        public TrainMealPreference PassengerFoodPreference3
        {
            get
            {
                return passengerFoodPreference3;
            }
            set
            {
                passengerFoodPreference3 = value;
            }
        }
        public TrainMealPreference PassengerFoodPreference4
        {
            get
            {
                return passengerFoodPreference4;
            }
            set
            {
                passengerFoodPreference4 = value;
            }
        }
        public TrainMealPreference PassengerFoodPreference5
        {
            get
            {
                return passengerFoodPreference5;
            }
            set
            {
                passengerFoodPreference5 = value;
            }
        }
        public TrainMealPreference PassengerFoodPreference6
        {
            get
            {
                return passengerFoodPreference6;
            }
            set
            {
                passengerFoodPreference6 = value;
            }
        }
        public TrainConcessionCode PassengerConcessionCode1
        {
            get
            {
                return passengerConcessionCode1;
            }
            set
            {
                passengerConcessionCode1 = value;
            }
        }
        public TrainConcessionCode PassengerConcessionCode2
        {
            get
            {
                return passengerConcessionCode2;
            }
            set
            {
                passengerConcessionCode2 = value;
            }
        }
        public TrainConcessionCode PassengerConcessionCode3
        {
            get
            {
                return passengerConcessionCode3;
            }
            set
            {
                passengerConcessionCode3 = value;
            }
        }
        public TrainConcessionCode PassengerConcessionCode4
        {
            get
            {
                return passengerConcessionCode4;
            }
            set
            {
                passengerConcessionCode4 = value;
            }
        }
        public TrainConcessionCode PassengerConcessionCode5
        {
            get
            {
                return passengerConcessionCode5;
            }
            set
            {
                passengerConcessionCode5 = value;
            }
        }
        public TrainConcessionCode PassengerConcessionCode6
        {
            get
            {
                return passengerConcessionCode6;
            }
            set
            {
                passengerConcessionCode6 = value;
            }
        }
        public bool HasBedRoll1
        {
            get
            {
                return hasBedRoll1;
            }
            set
            {
                hasBedRoll1 = value;
            }
        }
        public bool HasBedRoll2
        {
            get
            {
                return hasBedRoll2;
            }
            set
            {
                hasBedRoll2 = value;
            }
        }
        public bool HasBedRoll3
        {
            get
            {
                return hasBedRoll3;
            }
            set
            {
                hasBedRoll3 = value;
            }
        }
        public bool HasBedRoll4
        {
            get
            {
                return hasBedRoll4;
            }
            set
            {
                hasBedRoll4 = value;
            }
        }
        public bool HasBedRoll5
        {
            get
            {
                return hasBedRoll5;
            }
            set
            {
                hasBedRoll5 = value;
            }
        }
        public bool HasBedRoll6
        {
            get
            {
                return hasBedRoll6;
            }
            set
            {
                hasBedRoll6 = value;
            }
        }        
        //public IRCTCPaymentGateway PaymentGatewayName
        //{
        //    get
        //    {
        //        return paymentGatewayName; 
        //    }
        //    set
        //    {
        //       paymentGatewayName = value;
        //    }       
        //}
        //public IRCTCCardType AstrCardType
        //{
        //    get
        //    {
        //        return astrCardType; 
        //    }
        //    set
        //    {
        //       astrCardType = value;
        //    }       
        //}
        //public IRCTCOperatorCode OperatorCode
        //{
        //    get
        //    {
        //        return operatorCode; 
        //    }
        //    set
        //    {
        //       operatorCode = value;
        //    }       
        //}
        public string MobileNumber
        {
            get
            {
                return mobileNumber;
            }
            set
            {
                mobileNumber = value.Trim();
            }
        }
        public string EFlag
        {
            get
            {
                return eFlag;
            }
        }
        /// <summary>
        /// Sub user id
        /// </summary>
        //public string AccountNumber
        //{
        //    get
        //    {
        //        return accountNumber;
        //    }
        //    set
        //    {
        //        accountNumber = value;
        //    }
        //}
        ///// <summary>
        ///// Transaction password
        ///// </summary>
        //public string AccountPassword
        //{
        //    get
        //    {
        //        return accountPassword;
        //    }
        //    set
        //    {
        //        accountPassword = value;
        //    }
        //}
        ///// <summary>
        ///// sub user id
        ///// </summary>
        //public string Userid
        //{
        //    get
        //    {
        //        return userid;
        //    }
        //    set
        //    {
        //        userid = value;
        //    }
        //}
        ///// <summary>
        ///// Subuser password
        ///// </summary>
        //public string Password
        //{
        //    get
        //    {
        //        return password;
        //    }
        //    set
        //    {
        //        password = value;
        //    }
        //}
        /// <summary>
        /// 7 digit id provided by IRCTC + 10 digit unique id generated by us
        /// </summary>
        public string UniqueTxnID
        {
            get
            {
                return uniqueTxnID;
            }
            set
            {
                uniqueTxnID = value;
            }
        }
        public int PassengerCount
        {
            get
            {
                return passengerCount;
            }
            set
            {
                passengerCount = value;
            }
        }
        public DateTime BoardingDateAndTime
        {
            get
            {
                return boardingDateAndTime;
            }
            set
            {
                boardingDateAndTime = value;
            }
        }
        public string ChildPassengerName1
        {
            get
            {
                return childPassengerName1;
            }
            set
            {
                childPassengerName1 = value;
            }
        }
        public string ChildPassengerName2
        {
            get
            {
                return childPassengerName2;
            }
            set
            {
                childPassengerName2 = value;
            }
        }
        public TrainPassengerGender ChildPassengerSex1
        {
            get
            {
                return childPassengerSex1;
            }
            set
            {
                childPassengerSex1 = value;
            }
        }
        public TrainPassengerGender ChildPassengerSex2
        {
            get
            {
                return childPassengerSex2;
            }
            set
            {
                childPassengerSex2 = value;
            }
        }
        public string ChildPassengerAge1
        {
            get
            {
                return childPassengerAge1;
            }
            set
            {
                childPassengerAge1 = value;
            }
        }
        public string ChildPassengerAge2
        {
            get
            {
                return childPassengerAge2;
            }
            set
            {
                childPassengerAge2 = value;
            }
        }
        public Boolean HasClassUpgrade
        {
            get
            {
                return hasClassUpgrade;
            }
            set
            {
                hasClassUpgrade = value;
            }
        }
        #endregion

    }
    public class IRCTCBookResponse
    {
        public string TicketNumber;
        public string PnrNumber;
        public string TrainNumber;
        public string TrainName;
        public TrainQuota Quota;
        public string TrainbookingClass;
        public string SourceStation;
        public string DestinationStation;
        public string BoardingStation;
        public DateTime BoardingDate;
        public string ReservUptoStation;
        public short DayOfJourney;
        public short MonthOfJourney;
        public int YearOfJourney;
        public decimal Distance;
        public decimal ServiceCharge;
        public decimal TotalFare;
        public decimal TicketFare;
        public DateTime BookedOn;
        public string Departure;
        public List<BookedPaxInfo> passengerInfo;
        public string errorCode;
        public string errorDescription;
        public string slipRouteMessage;
    }
    public struct BookedPaxInfo
    {
        public string Name;
        public TrainPassengerGender Sex;
        public string Age;
        public TrainConcessionCode ConcessionCode;
        //Ask IRCTC and change to enum
        public string BookingStatus;
        public string CurrentStatus;
        public string Coach;
        public string Seat;
        public string Berth;
    }
    [Serializable]
    public struct PassengerInfo
    {
        public int age;
        public string name;
        public TrainPassengerGender gender;
        public TrainSeatPreference seatPreference;
        public string email;
        public string address;
        public string phone;
        public TrainPaxType passengerType;    //for concession code
        public TrainMealPreference mealPreference;
    }
    [Serializable]
    public class IRCTCFare
    {
        public decimal baseFare;
        public decimal superFastCharge;
        public decimal otherCharge;
        public decimal total;
        public decimal concessionAmount;
        public decimal netAmount;
        public decimal resvCharge;
        public decimal serviceFee;
        public decimal irctcCharge;
        public decimal bedRollCharge;
        public TrainPaxType paxType;
        public TrainConcessionCode concessionCode;
        public IRCTCFare Copy()
        {
            return (IRCTCFare)this.MemberwiseClone();
        }
    }
    public class IRCTCItineraryDetailsRequest
    {
        private string sourceStation;
        private string destinationStation;
        private string trainNumber;
        private string bookingClass;
        private string reservationUpto;
        private string boardingPoint;
        short dayOfJourney;
        short monthOfJourney;
        int yearOfJourney;
        TrainQuota quota;
        private string passengerName1;
        private string passengerName2 = "";
        private string passengerName3 = "";
        private string passengerName4 = "";
        private string passengerName5 = "";
        private string passengerName6 = "";
        private TrainPassengerGender passengerSex1;
        private TrainPassengerGender passengerSex2;
        private TrainPassengerGender passengerSex3;
        private TrainPassengerGender passengerSex4;
        private TrainPassengerGender passengerSex5;
        private TrainPassengerGender passengerSex6;
        private string passengerAge1 = "";
        private string passengerAge2 = "";
        private string passengerAge3 = "";
        private string passengerAge4 = "";
        private string passengerAge5 = "";
        private string passengerAge6 = "";
        private TrainSeatPreference passengerBerthPreference1;
        private TrainSeatPreference passengerBerthPreference2;
        private TrainSeatPreference passengerBerthPreference3;
        private TrainSeatPreference passengerBerthPreference4;
        private TrainSeatPreference passengerBerthPreference5;
        private TrainSeatPreference passengerBerthPreference6;
        private TrainMealPreference passengerFoodPreference1;
        private TrainMealPreference passengerFoodPreference2;
        private TrainMealPreference passengerFoodPreference3;
        private TrainMealPreference passengerFoodPreference4;
        private TrainMealPreference passengerFoodPreference5;
        private TrainMealPreference passengerFoodPreference6;
        private TrainConcessionCode passengerConcessionCode1;
        private TrainConcessionCode passengerConcessionCode2;
        private TrainConcessionCode passengerConcessionCode3;
        private TrainConcessionCode passengerConcessionCode4;
        private TrainConcessionCode passengerConcessionCode5;
        private TrainConcessionCode passengerConcessionCode6;
        private bool hasBedRoll1;
        private bool hasBedRoll2;
        private bool hasBedRoll3;
        private bool hasBedRoll4;
        private bool hasBedRoll5;
        private bool hasBedRoll6;
        public string SourceStation
        {
            get
            {
                return sourceStation;
            }
            set
            {
                if (value.Length > 4)
                {
                    throw new ArgumentException("Source code can not have length greater than 4");
                }
                else
                {
                    sourceStation = value.Trim();
                }
            }
        }
        public string DestinationStation
        {
            get
            {
                return destinationStation;
            }
            set
            {
                if (value.Length > 4)
                {
                    throw new ArgumentException("Destination code can not have length greater than 4");
                }
                else
                {
                    destinationStation = value.Trim();
                }
            }
        }
        public string TrainNumber
        {
            get
            {
                return trainNumber;
            }
            set
            {
                if (value.Length > 5)
                {
                    throw new ArgumentException("Train number can not have length greater than 4");
                }
                else
                {
                    trainNumber = value.Trim();
                }
            }
        }
        /// <summary>
        /// Key of TrainItinerary.IRCTCClass
        /// </summary>
        public string BookingClass
        {
            get
            {
                return bookingClass;
            }
            set
            {
                if (!TrainItinerary.IRCTCClass.ContainsKey(value))
                {
                    throw new ArgumentException("Class value is not valid");
                }
                else
                {
                    bookingClass = value.Trim();
                }
            }
        }
        public string ReservationUpto
        {
            get
            {
                return reservationUpto;
            }
            set
            {
                if (value.Length > 4)
                {
                    throw new ArgumentException("Reservation code can not have length greater than 4");
                }
                else
                {
                    reservationUpto = value.Trim();
                }
            }
        }
        public string BoardingPoint
        {
            get
            {
                return boardingPoint;
            }
            set
            {
                if (value.Length > 4)
                {
                    throw new ArgumentException("Boarding code can not have length greater than 4");
                }
                else
                {
                    boardingPoint = value.Trim();
                }
            }
        }
        public short DayOfJourney
        {
            get
            {
                return dayOfJourney;
            }
            set
            {
                if (value > 31 || value < 1)
                {
                    throw new ArgumentException("Day Of Journey value is not valid");
                }
                else
                {
                    dayOfJourney = value;
                }
            }
        }
        public short MonthOfJourney
        {
            get
            {
                return monthOfJourney;
            }
            set
            {
                if (value > 12 || value < 1)
                {
                    throw new ArgumentException("Month Of Journey value is not valid");
                }
                else
                {
                    monthOfJourney = value;
                }
            }
        }
        public int YearOfJourney
        {
            get
            {
                return yearOfJourney;
            }
            set
            {
                if (value < DateTime.Now.Year || value > (DateTime.Now.Year + 1))
                {
                    throw new ArgumentException("YearOfJourney value is not valid");
                }
                else
                {
                    yearOfJourney = value;
                }
            }
        }
        public TrainQuota Quota
        {
            get
            {
                return quota;
            }
            set
            {
                quota = value;
            }
        }
        public string PassengerName1
        {
            get
            {
                return passengerName1;
            }
            set
            {
                if (value.Length < 17)
                {
                    passengerName1 = value.Trim();
                }
                else
                {
                    throw new ArgumentException("Length of name can not be greater than 16");
                }
            }
        }
        public string PassengerName2
        {
            get
            {
                return passengerName2;
            }
            set
            {
                if (value.Length < 17)
                {
                    passengerName2 = value.Trim();
                }
                else
                {
                    throw new ArgumentException("Length of name can not be greater than 16");
                }
            }
        }
        public string PassengerName3
        {
            get
            {
                return passengerName3;
            }
            set
            {
                if (value.Length < 17)
                {
                    passengerName3 = value.Trim();
                }
                else
                {
                    throw new ArgumentException("Length of name can not be greater than 16");
                }
            }
        }
        public string PassengerName4
        {
            get
            {
                return passengerName4;
            }
            set
            {
                if (value.Length < 17)
                {
                    passengerName4 = value.Trim();
                }
                else
                {
                    throw new ArgumentException("Length of name can not be greater than 16");
                }
            }
        }
        public string PassengerName5
        {
            get
            {
                return passengerName5;
            }
            set
            {
                if (value.Length < 17)
                {
                    passengerName5 = value.Trim();
                }
                else
                {
                    throw new ArgumentException("Length of name can not be greater than 16");
                }
            }
        }
        public string PassengerName6
        {
            get
            {
                return passengerName6;
            }
            set
            {
                if (value.Length < 17)
                {
                    passengerName6 = value.Trim();
                }
                else
                {
                    throw new ArgumentException("Length of name can not be greater than 16");
                }
            }
        }
        public TrainPassengerGender PassengerSex1
        {
            get
            {
                return passengerSex1;
            }
            set
            {
                passengerSex1 = value;
            }
        }
        public TrainPassengerGender PassengerSex2
        {
            get
            {
                return passengerSex2;
            }
            set
            {
                passengerSex2 = value;
            }
        }
        public TrainPassengerGender PassengerSex3
        {
            get
            {
                return passengerSex3;
            }
            set
            {
                passengerSex3 = value;
            }
        }
        public TrainPassengerGender PassengerSex4
        {
            get
            {
                return passengerSex4;
            }
            set
            {
                passengerSex4 = value;
            }
        }
        public TrainPassengerGender PassengerSex5
        {
            get
            {
                return passengerSex5;
            }
            set
            {
                passengerSex5 = value;
            }
        }
        public TrainPassengerGender PassengerSex6
        {
            get
            {
                return passengerSex6;
            }
            set
            {
                passengerSex6 = value;
            }
        }
        public string PassengerAge1
        {
            get
            {
                return passengerAge1;
            }
            set
            {
                passengerAge1 = value.Trim();
            }
        }
        public string PassengerAge2
        {
            get
            {
                return passengerAge2;
            }
            set
            {
                passengerAge2 = value.Trim();
            }
        }
        public string PassengerAge3
        {
            get
            {
                return passengerAge3;
            }
            set
            {
                passengerAge3 = value.Trim();
            }
        }
        public string PassengerAge4
        {
            get
            {
                return passengerAge4;
            }
            set
            {
                passengerAge4 = value.Trim();
            }
        }
        public string PassengerAge5
        {
            get
            {
                return passengerAge5;
            }
            set
            {
                passengerAge5 = value.Trim();
            }
        }
        public string PassengerAge6
        {
            get
            {
                return passengerAge6;
            }
            set
            {
                passengerAge6 = value;
            }
        }
        public TrainSeatPreference PassengerBerthPreference1
        {
            get
            {
                return passengerBerthPreference1;
            }
            set
            {
                passengerBerthPreference1 = value;
            }
        }
        public TrainSeatPreference PassengerBerthPreference2
        {
            get
            {
                return passengerBerthPreference2;
            }
            set
            {
                passengerBerthPreference2 = value;
            }
        }
        public TrainSeatPreference PassengerBerthPreference3
        {
            get
            {
                return passengerBerthPreference3;
            }
            set
            {
                passengerBerthPreference3 = value;
            }
        }
        public TrainSeatPreference PassengerBerthPreference4
        {
            get
            {
                return passengerBerthPreference4;
            }
            set
            {
                passengerBerthPreference4 = value;
            }
        }
        public TrainSeatPreference PassengerBerthPreference5
        {
            get
            {
                return passengerBerthPreference5;
            }
            set
            {
                passengerBerthPreference5 = value;
            }
        }
        public TrainSeatPreference PassengerBerthPreference6
        {
            get
            {
                return passengerBerthPreference6;
            }
            set
            {
                passengerBerthPreference6 = value;
            }
        }
        public TrainMealPreference PassengerFoodPreference1
        {
            get
            {
                return passengerFoodPreference1;
            }
            set
            {
                passengerFoodPreference1 = value;
            }
        }
        public TrainMealPreference PassengerFoodPreference2
        {
            get
            {
                return passengerFoodPreference2;
            }
            set
            {
                passengerFoodPreference2 = value;
            }
        }
        public TrainMealPreference PassengerFoodPreference3
        {
            get
            {
                return passengerFoodPreference3;
            }
            set
            {
                passengerFoodPreference3 = value;
            }
        }
        public TrainMealPreference PassengerFoodPreference4
        {
            get
            {
                return passengerFoodPreference4;
            }
            set
            {
                passengerFoodPreference4 = value;
            }
        }
        public TrainMealPreference PassengerFoodPreference5
        {
            get
            {
                return passengerFoodPreference5;
            }
            set
            {
                passengerFoodPreference5 = value;
            }
        }
        public TrainMealPreference PassengerFoodPreference6
        {
            get
            {
                return passengerFoodPreference6;
            }
            set
            {
                passengerFoodPreference6 = value;
            }
        }
        public TrainConcessionCode PassengerConcessionCode1
        {
            get
            {
                return passengerConcessionCode1;
            }
            set
            {
                passengerConcessionCode1 = value;
            }
        }
        public TrainConcessionCode PassengerConcessionCode2
        {
            get
            {
                return passengerConcessionCode2;
            }
            set
            {
                passengerConcessionCode2 = value;
            }
        }
        public TrainConcessionCode PassengerConcessionCode3
        {
            get
            {
                return passengerConcessionCode3;
            }
            set
            {
                passengerConcessionCode3 = value;
            }
        }
        public TrainConcessionCode PassengerConcessionCode4
        {
            get
            {
                return passengerConcessionCode4;
            }
            set
            {
                passengerConcessionCode4 = value;
            }
        }
        public TrainConcessionCode PassengerConcessionCode5
        {
            get
            {
                return passengerConcessionCode5;
            }
            set
            {
                passengerConcessionCode5 = value;
            }
        }
        public TrainConcessionCode PassengerConcessionCode6
        {
            get
            {
                return passengerConcessionCode6;
            }
            set
            {
                passengerConcessionCode6 = value;
            }
        }
        public bool HasBedRoll1
        {
            get
            {
                return hasBedRoll1;
            }
            set
            {
                hasBedRoll1 = value;
            }
        }
        public bool HasBedRoll2
        {
            get
            {
                return hasBedRoll2;
            }
            set
            {
                hasBedRoll2 = value;
            }
        }
        public bool HasBedRoll3
        {
            get
            {
                return hasBedRoll3;
            }
            set
            {
                hasBedRoll3 = value;
            }
        }
        public bool HasBedRoll4
        {
            get
            {
                return hasBedRoll4;
            }
            set
            {
                hasBedRoll4 = value;
            }
        }
        public bool HasBedRoll5
        {
            get
            {
                return hasBedRoll5;
            }
            set
            {
                hasBedRoll5 = value;
            }
        }
        public bool HasBedRoll6
        {
            get
            {
                return hasBedRoll6;
            }
            set
            {
                hasBedRoll6 = value;
            }
        }



    }
    public class IRCTCItineraryDetailsResponse
    {
        public decimal TicketAmount;
        public decimal ServiceCharges;
        public decimal TotalAmount;
        public string AvailabilityDetails;
    }
    public class IRCTCBookingSearchResponse
    {
        public string TicketNumber;
        public string PnrNumber;
        public string TrainNumber;
        public string TrainName;
        public TrainQuota Quota;
        public string TrainbookingClass;
        public string SourceStation;
        public string DestinationStation;
        public string BoardingStation;
        public DateTime BoardingDate;
        public string ReservUptoStation;
        public short DayOfJourney;
        public short MonthOfJourney;
        public int YearOfJourney;
        public decimal Distance;
        public decimal ServiceCharge;
        public decimal TotalFare;
        public decimal TicketFare;
        public DateTime BookedOn;
        public string Departure;
        public List<BookedPaxInfo> passengerInfo;
        public string errorCode;
        public string errorDescription;
        public int noOfTickets;
        public string message;
        public string slipRouteMessage;
    }
    public struct IRCTCCancelRequest
    {
        private string transactionNo;
        private string cancelString;        

        /// <summary>
        /// Transaction number
        /// </summary>
        public string TransactionNo
        {
            get
            {
                return transactionNo;
            }
            set
            {
                if (value.Trim().Length != 10)
                {
                    throw new ArgumentException("Transaction number must be 10 characters long");
                }
                else
                {
                    transactionNo = value.Trim();
                }
            }
        }
        /// <summary>
        /// Cancelation string
        /// </summary>
        public string CancelString
        {
            get
            {
                return cancelString;
            }
            set
            {
                if (value.Trim().Length != 6)
                {
                    throw new ArgumentException("cancelString must be 6 characters long");
                }
                else
                {
                    cancelString = value.Trim();
                }
            }
        }
      
    }
    public struct IRCTCCancelResponse
    {

        public bool Success;
        public decimal RefundAmount;
        public decimal AmountCollected;
        public decimal CashDeducted;
        public decimal CashCollected;
        public string ErrorCode;
        public string ErrorDescription;
    
    }
    public class IRCTCPnrStatusResponse
    {
        public string TrainNo;
        public string TrainName;
        public int Day;
        public int Month;
        public int Year;
        public string From;
        public string To;
        public string BoardingPoint;
        public string ReservationUpto;
        public string ClassCode;
        public int NoOfPassenger;
        public string ChartingStatus;
        public List<PassengerPnrStatus> PassengerPnrStatusList;
        public string ErrorCode;
        public string ErrorDescription;
    }
    public struct PassengerPnrStatus
    {
        public string Slno;
        public string CurrentStatus;
        public string RunningStatus;    
    }
    public struct IRCTCTrainRouteRequest
    {
        private string trainNumber;
        private int day;
        private int month;
        private const string daycnt = "0";
        
        public string Daycnt
        {
            get
            {
                return daycnt;
            }
        }

        public string TrainNumber
        {
            get
            {
                return trainNumber;
            }
            set
            {
                if (value.Trim().Length > 5)
                {
                    throw new ArgumentException("trainNumber must be less than 5 characters");
                }
                else
                {
                    trainNumber = value;
                }
            }
        }
        public int Day
        {
            get
            {
                return day;
            }
            set
            {
                if (value > 31 || value < 1)
                {
                    throw new ArgumentException("Day value is not valid");
                }
                else
                {                    
                    day = value;
                }
            }
        }
        public int Month
        {
            get
            {
                return month;
            }
            set
            {
                if (value > 12 || value < 1)
                {
                    throw new ArgumentException("Month value is not valid");
                }
                else
                {
                    month = value;
                }
            }
        }
    }
    
    public struct TrainRouteInfo
    {
        public string Slno;
        public string StationCode;
        public string StationName;
        public string RouteNo;
        public string ArrTime;
        public string DepTime;
        public decimal Distance;
        public int Day;
        public string Remarks;
        public string TrainNumber;
        public string TrainName;
    }
    public struct IRCTCRefundDetailOutput
    {
        public string TicketNumber;
        public string PnrNumber;
        public DateTime Date;
        public string TrainNumber;
        public string TrainFrom;
        public string TrainTo;
        public string Class;
        public string PaymentGatewayName;
        public decimal TotalAmount;
        public decimal ServiceCharge;
        public List<RefundHistory> refundHistoryList;
        public string ErrorCode;
        public string ErrorDescription;
        public List<IRCTCCancelStatus> cancelStatusList;      

    }
    public struct IRCTCCancelStatus
    {
        public string CancelStatus;
        public decimal CashPaid;
        public decimal AmountRefund;
        public decimal AmountDeducted;
        public decimal CashCollected;
        public string CancelationString;
    }
    public struct RefundHistory
    {
        public DateTime RefundDate;
        public decimal RefundAmount;
        public string RefundStatus;
        public string PaymentGateWayName;
    }
    public struct TrainItineraryDetail
    {
        public int AdultCount;
        public int ChildCount;
        public int SeniorMenCount;
        public int SeniorWomenCount;
        public string FromCode;
        public string ToCode;
        public string FromName;
        public string ToName;
        public DateTime DateOfTravel;
        public string TrainClass;
        public string TrainNumber;
        public string TrainName;
        public int TotalPassenger;
        public TrainType TrainType;
        public bool isTatkal;
        public decimal TotalPrice;
    }

}
