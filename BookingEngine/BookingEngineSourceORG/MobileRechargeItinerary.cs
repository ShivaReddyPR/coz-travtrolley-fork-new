using System;
using System.Collections.Generic;
using System.Text;
using Technology.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Technology.BookingEngine
{
    public class MobileRechargeItinerary : Technology.BookingEngine.Product
    {
        private string operatorCode;
        private string circleCode;
        private string amount;
        private string mobileNumber;
        private string denomination;
        private string transactionId;
        private string clientId;
        private DateTime createdOn;
        private int createdBy;
        private DateTime lastModifiedOn;
        private int lastModifiedBy;
        private int rechargeId;
        private decimal agentCommission;
        private int agencyId;
        private decimal ourCommission;
        private int priceId;
        public string OperatorCode
        {
            get
            {
                return operatorCode;
            }
            set
            {
                operatorCode = value;
            }
        }
        public string CircleCode
        {
            get
            {
                return circleCode;
            }
            set
            {
                circleCode = value;
            }
        }
        public string MobileNumber
        {
            get
            {
                return mobileNumber;
            }
            set
            {
                mobileNumber = value;
            }
        }
        public string Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }
        public string TransactionId
        {
            get
            {
                return transactionId;
            }
            set
            {
                transactionId = value;
            }
        }
        public string ClientId
        {
            get
            {
                return clientId;
            }
            set
            {
                clientId = value;
            }
        }
        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        public int RechargeId
        {
            get
            {
                return rechargeId;
            }
            set
            {
                rechargeId = value;
            }
        }
        public decimal AgentCommission
        {
            get
            {
                return agentCommission;
            }
            set
            {
                agentCommission = value;
            }
        }
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }
        public decimal OurCommission
        {
            get
            {
                return ourCommission;
            }
            set
            {
                ourCommission = value;
            }
        }
        public int PriceId
        {
            get { return priceId; }
            set { priceId = value; }
        }
        public override void Save(Product prod)
        {
            MobileRechargeItinerary itinerary = (MobileRechargeItinerary)prod;
            SqlParameter[] paramList = new SqlParameter[11];
            paramList[0] = new SqlParameter("@operatorCode", itinerary.OperatorCode);
            paramList[1] = new SqlParameter("@circleCode", itinerary.CircleCode);
            paramList[2] = new SqlParameter("@mobileNumber", itinerary.MobileNumber);
            paramList[3] = new SqlParameter("@amount", itinerary.Amount);
            paramList[4] = new SqlParameter("@transactionId", itinerary.TransactionId);
            paramList[5] = new SqlParameter("@clientId", itinerary.ClientId);
            paramList[6] = new SqlParameter("@createdBy", itinerary.CreatedBy);
            paramList[7] = new SqlParameter("@agentCommission", itinerary.AgentCommission);
            paramList[8] = new SqlParameter("@rechargeId", itinerary.RechargeId);
            paramList[9] = new SqlParameter("@agencyId", itinerary.AgencyId);
            paramList[8].Direction = ParameterDirection.Output;
            
            PriceAccounts price = new PriceAccounts();
            price.Currency = "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "";
            price.OurCommission = itinerary.OurCommission;
            price.AgentCommission = itinerary.AgentCommission;
            price.CurrencyCode = "" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "";
            price.RateOfExchange = 1;
            price.PublishedFare = Convert.ToDecimal(itinerary.Amount);
            price.Save();

            paramList[10] = new SqlParameter("@priceId", price.PriceId);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddMobileRecharge, paramList);
            rechargeId = Convert.ToInt32(paramList[8].Value);
        }
        public void Load(int productId)
        {
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@rechargeId", productId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.LoadMobileRechargeItinerary, paramList, connection);
            if (data.Read())
            {
                operatorCode = data["operatorCode"].ToString();
                transactionId = data["transactionId"].ToString();
                circleCode = data["circleCode"].ToString();
                mobileNumber = data["mobileNumber"].ToString();
                amount = data["amount"].ToString();
                agentCommission = Convert.ToDecimal(data["agentCommission"]);
                rechargeId = productId;
                createdBy = Convert.ToInt16(data["createdBy"]);
                createdOn = Convert.ToDateTime(data["createdOn"]);
                clientId = data["clientId"].ToString();
                if (data["agencyId"] != DBNull.Value)
                    agencyId = Convert.ToInt16(data["agencyId"]);
                else
                    agencyId = 0;
                lastModifiedBy = Convert.ToInt16(data["lastModifiedBy"]);
                lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                priceId = Convert.ToInt32(data["priceId"]);
                rechargeId = Convert.ToInt16(data["rechargeId"]);
            }
            data.Close();
            connection.Close();
        }
        /// <summary>
        /// This Method is used for getting all the mobile recharges
        /// </summary>
        /// <returns></returns>
        public static List<MobileRechargeItinerary> GetTotalRecordsforMobileRecharge(int lower, int upper, int agencyId)
        {
            Trace.TraceInformation("GetTotalRecordsforMobileRecharge entered");
            List<MobileRechargeItinerary> rechargeList = new List<MobileRechargeItinerary>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@lower", lower);
            paramList[1] = new SqlParameter("@upper", upper);
            paramList[2] = new SqlParameter("@createdBy", agencyId);

            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetTotalRecordsforMobileRecharge, paramList, connection);
            while (data.Read())
            {
                MobileRechargeItinerary recharge = new MobileRechargeItinerary();
                recharge.Amount = data["amount"].ToString();
                recharge.agentCommission = Convert.ToDecimal(data["agentCommission"]);
                recharge.TransactionId = data["transactionId"].ToString();
                recharge.MobileNumber = data["mobileNumber"].ToString();
                recharge.CreatedOn = Convert.ToDateTime(data["createdOn"]);
                recharge.RechargeId = Convert.ToInt32(data["rechargeId"]);
                rechargeList.Add(recharge);
            }
            data.Close();
            connection.Close();

            return rechargeList;
        }

        public static List<MobileRechargeItinerary> GetRecordsOnFilterBasis(string filter, string filterText,int agencyId)
        {
            Trace.TraceInformation("GetRecordsOnFilterBasis entered");
            List<MobileRechargeItinerary> rechargeList = new List<MobileRechargeItinerary>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@filter", filter);
            paramList[1] = new SqlParameter("@filterText", filterText);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetMobileFilteredResults, paramList, connection);
            while (data.Read())
            {
                MobileRechargeItinerary recharge = new MobileRechargeItinerary();
                recharge.Amount = data["amount"].ToString();
                recharge.agentCommission = Convert.ToDecimal(data["agentCommission"]);
                recharge.TransactionId = data["transactionId"].ToString();
                recharge.MobileNumber = data["mobileNumber"].ToString();
                recharge.CreatedOn = Convert.ToDateTime(data["createdOn"]);
                recharge.RechargeId = Convert.ToInt32(data["rechargeId"]);
                recharge.createdBy = Convert.ToInt32(data["createdBy"]);
                recharge.agencyId = Convert.ToInt32(data["agencyId"]);
                //Add All Records if admin is searching
                if (agencyId == 0)
                {
                    rechargeList.Add(recharge);
                }
                else
                {
                    //Add only those records created for/by the agency
                    if (agencyId == recharge.agencyId)
                    {
                        rechargeList.Add(recharge);
                    }
                }
            }
            return rechargeList;
        }

        public static int GetTotalNoOfRecords(int agencyId)
        {
            Trace.TraceInformation("GetTotalNoOfRecords entered");
            int count = 0;
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@createdBy", agencyId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetMobileRecordsCount, paramList, connection);
            if (data.Read())
            {
                count = Convert.ToInt32(data["CountRows"]);
            }
            Trace.TraceInformation("GetTotalNoOfRecords exiting");
            return count;
        }
    }
}
