using System;
using System.Collections.Generic;
using System.Text;

namespace Technology.BookingEngine
{
    public class SpiceJetSearchResult : SearchResult
    {
        #region Private Members               

        // for fare item
        string [] fareString = new string[2];

        // for flight item
        string[] flightString = new string[2];    
        
        // fare basis code
        string[] selectedFareBasisCode = new string[2];
       
        #endregion

        #region Public Properties
        public string [] FareString
        {
            get
            {
                return fareString;
            }
            set
            {
                fareString = value;
            }
        }

        public string [] FlightString
        {
            get
            {
                return flightString;
            }
            set
            {
                flightString = value;
            }
        }

        public string [] SelectedFareBasisCode
        {
            get
            {
                return selectedFareBasisCode;
            }
            set
            {
                selectedFareBasisCode = value;
            }
        }
        #endregion

    }
}
