using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Technology.Data;
using Technology.Configuration;
using CoreLogic;

namespace Technology.BookingEngine
{
    [Serializable]
    public class Airport
    {
        /// <summary>
        /// Airport Code
        /// </summary>
        private string airportCode;
        /// <summary>
        /// Name of Ariport
        /// </summary>
        private string airportName;
        /// <summary>
        /// Code of city where the airport is located
        /// </summary>
        private string cityCode;
        /// <summary>
        /// Name of city where the airport is located
        /// </summary>
        private string cityName;
        /// <summary>
        /// Code of country where the airport is located
        /// </summary>
        private string countryCode;
        /// <summary>
        /// Name of country where the airport is located
        /// </summary>
        private string countryName;

        /// <summary>
        /// Default Constructor
        /// </summary>

       
        
        public Airport() { }

        /// <summary>
        /// Constructor that loads details according to airportCode
        /// </summary>
        public Airport(string arpCode)
        {
            airportCode = arpCode;
            try
            {
                Load();
            }
            catch (ArgumentException)
            {
                airportName = string.Empty;
                cityCode = arpCode;
                cityName = string.Empty;
                countryCode = string.Empty;
                countryName = string.Empty;
            }
        }

        /// <summary>
        /// Gets the airport code
        /// </summary>
        public string AirportCode
        {
            get { return airportCode; }
            set { airportCode = value; }
        }

        /// <summary>
        /// Gets the airport name
        /// </summary>
        public string AirportName
        {
            get { return airportName; }
            set { airportName = value; }
        }

        /// <summary>
        /// Gets the code of city where the airport is located
        /// </summary>
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }

        /// <summary>
        /// Gets the name of city where the airport is located
        /// </summary>
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }

        /// <summary>
        /// Gets the code of country where the airport is located
        /// </summary>
        public string CountryCode
        {
            get { return countryCode; }
            set { countryCode = value; }
        }

        /// <summary>
        /// Gets the name of country where the airport is located
        /// </summary>
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }
      
       
        /// <summary>
        /// Loads the airport object detail corresponding to airport code.
        /// </summary>
        public void Load()
        {       
            Trace.TraceInformation("Airport.Load entered airportCode = " + airportCode);
            if (airportCode == null || airportCode.Length == 0)
            {
                throw new ArgumentException("Airport Code can not be null or empty", "airportCode");
            }
            // checks whether Cache contains req. details.if it contains  data retrive from the Cache.
            if (CacheData.CheckAirportLoad(airportCode))
            {
                List<string> airList = new List<string>();
                airList = CacheData.AirportList[airportCode];
                airportName = (string)airList[0];
                cityCode = (string)airList[1];
                countryCode = (string)airList[2];
                cityName = CacheData.CityList[cityCode];
                countryName = CacheData.CountryList[countryCode];
                return;
            }
            else
            {
                List<string> tempList = new List<string>();
                SqlConnection connection = Dal.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@airportCode", airportCode);
                SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetAirport, paramList, connection);
                // If it doesnt contain in Cache retrieve data from the Database.
                if (dataReader.Read())
                {
                    airportName = (string)dataReader["airportName"];

                    cityCode = Convert.ToString(dataReader["cityCode"]).ToUpper();
                    cityName = (string)dataReader["cityName"];
                    countryCode = (string)dataReader["countryCode"];
                    countryName = (string)dataReader["countryName"];
                    //Update the Cache.
                    tempList.Add(airportName);
                    tempList.Add(cityCode);
                    tempList.Add(countryCode);
                    CacheData.AirportList.Add(airportCode, tempList);
                    if (!CacheData.CheckGetCityName(cityCode))
                    {
                        CacheData.CityList.Add(cityCode, cityName);
                    }
                    if (!CacheData.CheckCountryName(countryCode))
                    {
                        CacheData.CountryList.Add(countryCode, countryName);
                    }
                    dataReader.Close();
                    connection.Close();
                }
                else
                {
                    dataReader.Close();
                    connection.Close();
                    Trace.TraceInformation("Airport.Load exiting : Airport does not exist. airportCode = " + airportCode);

                    throw new ArgumentException("Airport code does not exist in database");
                }
            }
        }

        /// <summary>
        /// Get valid city code - as if city name entered then corresponding city code and if airport code is entered then city code corresponding airport code.
        /// </summary>
        public ArrayList GetAirportCodesForCity(string city)
        {
            if (city == null && city.Length == 0)
            {
                Trace.TraceInformation("city parameter must have a value");
                throw new ArgumentException("city parameter must have a value");
            }

            Trace.TraceInformation("Airport.GetAirportCodesForCity entered  city" + city);
            ArrayList cityCodesArray = new ArrayList();
            
           
          
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@to", city);
            SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetValidCityCode, paramList, connection);
            
            while (dataReader.Read())
            {
                string cityName;
                string countryName;
                //TODO: why are we doing this?
                if (dataReader.FieldCount == 1)
                {
                    cityName = "";
                    countryName = "";
                }
                else
                {
                    cityName = (string)dataReader["cityName"];
                    countryName = (string)dataReader["countryName"];
                    
                }
                string[] temp = { Convert.ToString(dataReader["cityCode"]).ToUpper(), cityName, countryName };
                cityCodesArray.Add(temp);
                
            }

            dataReader.Close();
            connection.Close();

            if (cityCodesArray.Count == 0)
            {
                Trace.TraceError("City Code does not exist for city (" + city + ")");
                
                string subjectMessage = "TBO City/Airport not in Database";
                string bodyMessage ="Dear Executive,\n\nA City/Airport searched by user does not exist in our Database.\nCity/Airport searched : "+city+"\n\nThis is automated mail alert generated by TBO system.\n\n---\nTBO System";
                Email.Send(ConfigurationSystem.Email["fromEmail"], ConfigurationSystem.Email["TechSupportMailingId"], subjectMessage, bodyMessage);
                
                throw new ArgumentException("City Code does not exist for city (" + city + ")");
            }

            Trace.TraceInformation("Airport.GetAirportCodesForCity exiting city" + city);
     
           return cityCodesArray;
            
        }
        /// <summary>
        /// Gets the complete information about any city or airport name searched for 
        /// </summary>
        /// <param name="city">ciy name or the airport name to be searched for</param>
        /// <returns>An object of type Airport </returns>
        public static List<Airport> GetInfoForCityOrAirport(string city)
        {
            if (city == null && city.Length == 0)
            {
                Trace.TraceInformation("city parameter must have a value");
                throw new ArgumentException("city parameter must have a value");
            }

            Trace.TraceInformation("Airport.GetAirportCodesForCity entered  city" + city);
            List<Airport> airportList = new List<Airport>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@to", city);
            SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetAirportInfo, paramList, connection);

            while (dataReader.Read())
            {
                Airport airport = new Airport();
                airport.cityName = (string)dataReader["cityName"];
                airport.countryName = (string)dataReader["countryName"];
                airport.cityCode = Convert.ToString(dataReader["cityCode"]).ToUpper();
                airport.countryCode = (string)dataReader["countryCode"];
                airport.airportCode = Convert.ToString(dataReader["airportCode"]).ToUpper();
                airport.airportName = (string)dataReader["airportName"];
                airportList.Add(airport);
            }
            dataReader.Close();
            connection.Close();
            Trace.TraceInformation("Airport.GetAirportCodesForCity exiting city" + city);
            return airportList;
        }
        /// <summary>
        /// Gets the list of airport using city code
        /// </summary>
        /// <param name="cityCode"></param>
        /// <returns></returns>
        public static List<Airport> GetAirports(string cityCode)
        {
            Trace.TraceInformation("Airport.GetAirports entered  city code" + cityCode);
            if (cityCode == null  || cityCode.Length != 3)
            {
                Trace.TraceInformation("City Code parameter must have a value");
                throw new ArgumentException("city Code sholud be of length 3");
            }
            List<Airport> airportList = new List<Airport>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@cityCode", cityCode);
            SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetAirports, paramList, connection);
            while (dataReader.Read())
            {
                Airport airport = new Airport();
                airport.cityName = dataReader["cityName"].ToString();
                airport.countryName = dataReader["countryName"].ToString();
                airport.cityCode = dataReader["cityCode"].ToString().ToUpper();
                airport.countryCode = dataReader["countryCode"].ToString();
                airport.airportCode = dataReader["airportCode"].ToString().ToUpper();
                airport.airportName = dataReader["airportName"].ToString();
                airportList.Add(airport);
            }
            dataReader.Close();
            connection.Close();
            Trace.TraceInformation("Airport.GetAirports exiting city code" + cityCode);
            return airportList;
        }

        // for getting city name
        public static ArrayList GetCityName(string citycode)
        {
            Trace.TraceInformation("Airport.GetCityName entered  citycode" + citycode);
            ArrayList cityArray = new ArrayList();
            if (citycode == null)
            {
                Trace.TraceInformation("citycode parameter must have a value");
                throw new ArgumentException("citycode parameter must have a value");
            }
            if (citycode.Length != 3)
            {
                Trace.TraceInformation("citycode can only be three characters");
                throw new ArgumentException("citycode can only be three characters");
            }
            if (CacheData.CheckGetCityName(citycode))
            {
                string cName = CacheData.CityList[citycode];
                string[] temp = { citycode, cName };
                cityArray.Add(temp);
                return cityArray;
            }
            else
            {

                SqlConnection connection = Dal.GetConnection();
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@citycode", citycode);
                SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetCityName, paramList, connection);
                while (dataReader.Read())
                {
                    string cityName;
                    string cityCode;
                    cityName = (string)dataReader["cityName"];
                    cityCode = Convert.ToString(dataReader["cityCode"]).ToUpper();
                    if (!CacheData.CheckGetCityName(cityCode))
                    {
                        CacheData.CityList.Add(cityCode, cityName);
                    }
                    string[] temp = { cityCode, cityName };
                    cityArray.Add(temp);
                }
                dataReader.Close();
                connection.Close();
                if (cityArray.Count == 0)
                {
                    Trace.TraceError("City does not exist for given city code (" + citycode + ")");
                    throw new ArgumentException("City does not exist for given city code(" + citycode + ")");
                }
                Trace.TraceInformation("Airport.GetCityName exiting citycode" + citycode);
                return cityArray;
            }
        }

        /// <summary>
        /// getting arrival and departure citi's airportcodes
        /// </summary>
        /// <param name="citycode"></param>
        /// <returns></returns>
        public static ArrayList GetAirportCode(string citycode)
        {
            Trace.TraceInformation("Airport.GetAirportCode entered  citycode" + citycode);
            ArrayList airportCodes = new ArrayList();
            List<string> airList = new List<string>();
            string airCode = string.Empty;
           
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@citycode", citycode);
            SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetAirportCodesByCity, paramList, connection);

            while (dataReader.Read())
            {
                string airportCode;
                string airportName;
                string cityCode;

                airportCode = Convert.ToString(dataReader["airportCode"]).ToUpper();
                airportName = (string)dataReader["airportName"];
                cityCode = Convert.ToString(dataReader["cityCode"]).ToUpper();
                //airList.Add(airportCode);
                string[] temp = { airportCode, airportName, cityCode };
                airportCodes.Add(temp);
            }
            //CacheData.cityCodeList.Add(citycode, airList);
            dataReader.Close();
            connection.Close();

            if (airportCodes.Count == 0)
            {
                Trace.TraceInformation("Airports does not exist for given city code (" + citycode + ")");
                throw new ArgumentException("Airports does not exist for given city code(" + citycode + ")");
            }

            Trace.TraceInformation("Airport.GetAirportCode exiting citycode" + citycode);
           
            return airportCodes;
        }

        /// <summary>
        /// Get country code for given city code or airport code
        /// </summary>
        /// <param name="cityCodeOrAirportCode"></param>
        public string GetCountryCode(string cityCodeOrAirportCode)
        {
            Trace.TraceInformation("Airport.GetCountryCode Enter citycode" + cityCodeOrAirportCode);
            ArrayList cityCodeArray = new ArrayList();
            try
            {
                cityCodeArray = GetAirportCodesForCity(cityCodeOrAirportCode);
            }
            catch (ArgumentException excep)
            {
                Trace.TraceInformation("No City Code exist for given City Code or Airport Code =" + cityCodeOrAirportCode + " | error = " + excep.Message);
                throw new ArgumentException("No City Code exist for given City Code or Airport Code =" + cityCodeOrAirportCode + " | error = " + excep.Message);
            }

            // get country code for cityCode
            Array a = (Array)cityCodeArray[0];
            cityCode = (string)a.GetValue(0);

            countryCode = (string)a.GetValue(2);

            if ((countryCode == null) || (countryCode != null && countryCode.Length == 0))
            {
                // get data from data base
                ArrayList airportData = GetAirportCode(cityCode);
                Array b = (Array)airportData[0];
                airportCode = (string)b.GetValue(0);
                Load();
            }

            Trace.TraceInformation("Airport.GetCountryCode exiting citycode" + cityCodeOrAirportCode);
            return countryCode;
        }
        public static List<string> GetInvalidAirports(string csvAirports)
        {
            Trace.TraceInformation("Airport.GetInvalidAirports entered  csvAirports" + csvAirports);
            List<string> tempList = new List<string>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@airportString", csvAirports);
            SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetAirportPresent, paramList, connection);
            string[] splitAirports = csvAirports.Split(',');
            List<string> dbArray = new List<string>();
            while (dataReader.Read())
            {
                dbArray.Add(dataReader["airportCode"].ToString());
            }
            foreach (string s in splitAirports)
            {
                foreach (string dbString in dbArray)
                {
                    if (s.IndexOf(dbString) < 0)
                    {
                        tempList.Add(s);
                        break;
                    }
                }
            }
            dataReader.Close();
            connection.Close();
            Trace.TraceInformation("Airport.GetInvalidAirports exiting count" + tempList.Count);
            return tempList;
        }
        /// <summary>
        /// Adds new airport using airport code,airport name and city code 
        /// </summary>
        public void SaveAirport()
        {
            Trace.TraceInformation("Airport.SaveAirport entered  " );
            SqlParameter[] paramList = new SqlParameter[3];
             paramList[0] = new SqlParameter("@airportCode", airportCode.ToUpper() );
             paramList[1] = new SqlParameter("@airportName", airportName) ;
             paramList[2] = new SqlParameter("@cityCode", cityCode.ToUpper() );
             int retVal = Dal.ExecuteNonQuerySP(SPNames.AddAirport, paramList);
             Trace.TraceInformation("Airport.SaveAirport exiting count" + retVal);
        
        }
        /// <summary>
        /// Updates the airport name through airport code
        /// </summary>
        public void Update()
        {
            Trace.TraceInformation("Airport.Update entered  ");
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@airportCode", airportCode.ToUpper());
            paramList[1] = new SqlParameter("@airportName", airportName);
            int retVal = Dal.ExecuteNonQuerySP(SPNames.UpdateAirport, paramList);
            if (CacheData.CheckAirportLoad(airportCode))
            {
                CacheData.AirportList.Remove(airportCode);
                List<string> updateList = new List<string>();
                updateList.Add(airportName);
                updateList.Add(cityCode);
                updateList.Add(countryCode);
                CacheData.AirportList.Add(airportCode, updateList);
            }
            else
            { 
            //No need to update the cache
            }
            Trace.TraceInformation("Airport.Update exiting count" + retVal);
        }

        public static List<Airport> GetAirportsForCountry(string countryCode)
        {
            List<Airport> airportList = new List<Airport>();
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@countryCode", countryCode);
                SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetAirportsForCountry, paramList, connection);

                while (dataReader.Read())
                {
                    //Todo : Initialize countryNAME.Not Done because eextra join on DB
                    Airport airport = new Airport();
                    airport.cityName = (string)dataReader["cityName"];
                    airport.cityCode = Convert.ToString(dataReader["cityCode"]).ToUpper();
                    airport.countryCode = (string)dataReader["countryCode"];
                    airport.airportCode = Convert.ToString(dataReader["airportCode"]).ToUpper();
                    airport.airportName = (string)dataReader["airportName"];
                    airportList.Add(airport);
                }
                dataReader.Close();
                connection.Close();
            }
            return airportList;
        }
    }
}
