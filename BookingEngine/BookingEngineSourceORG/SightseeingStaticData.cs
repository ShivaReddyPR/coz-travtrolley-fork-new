using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Technology.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Configuration;
using System.IO;
using System.Net;
using System.Transactions;
namespace Technology.BookingEngine
{
    public class SightseeingStaticData
    {
        private string itemCode;
        private string cityCode;
        private string description;
        private string tourInfo;
        private string includes;
        private string departurePoint;
        private string flashLink;
        private string notes;
        private List<TourOperations> tourOperation;
        private SightseeingBookingSource source;
        private DateTime timeStamp;
        private string itemName;
        private string imageInfo;
        private string closedDates;
        public string ItemCode
        {
            get { return itemCode; }
            set { itemCode = value; }
        }
        public string ItemName
        {
            get { return itemName; }
            set { itemName = value; }
        }
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public SightseeingBookingSource Source
        {
            get { return source; }
            set { source = value; }
        }
        public string ImageInfo
        {
            get { return imageInfo; }
            set { imageInfo = value; }
        }
        public string TourInfo
        {
            get { return tourInfo; }
            set { tourInfo = value; }
        }
        public string Includes
        {
            get { return includes; }
            set { includes = value; }
        }
        public string DeparturePoint
        {
            get { return departurePoint; }
            set { departurePoint = value; }
        }
        public string FlashLink
        {
            get { return flashLink; }
            set { flashLink = value; }
        }
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }
        public DateTime TimeStamp
        {
            get { return timeStamp; }
            set { timeStamp = value; }
        }
        public List<TourOperations> TourOperationInfo
        {
            get { return tourOperation; }
            set { tourOperation = value; }
        }
        public string ClosedDates
        {
            get { return closedDates; }
            set { closedDates = value; }
        }
        /// <summary>
        /// This Method is used to save the Sightseeing Static Data
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("SightseeingStaticData.Save entered.");
            try
            {
                using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                {
                    SqlParameter[] paramList = new SqlParameter[12];
                    paramList[0] = new SqlParameter("@itemCode", itemCode);
                    paramList[1] = new SqlParameter("@cityCode", cityCode);
                    paramList[2] = new SqlParameter("@description", description);
                    paramList[3] = new SqlParameter("@tour", tourInfo);
                    paramList[4] = new SqlParameter("@includes", includes);
                    paramList[5] = new SqlParameter("@departurePoint", departurePoint);
                    paramList[6] = new SqlParameter("@flashLink", flashLink);
                    paramList[7] = new SqlParameter("@notes", notes);
                    paramList[8] = new SqlParameter("@source", source);
                    paramList[9] = new SqlParameter("@itemName", itemName);
                    paramList[10] = new SqlParameter("@imageInfo", imageInfo);
                    paramList[11] = new SqlParameter("@closedDates", closedDates);
                    int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddSightseeingStaticData, paramList);
                    foreach (TourOperations tour in tourOperation)
                    {
                        tour.CityCode = cityCode;
                        tour.ItemCode = itemCode;
                        tour.Save();
                    }
                    updateTransaction.Complete();
                }
            }
            catch (Exception exp)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.SightseeingSearch, CoreLogic.Severity.High, 1, "Exception in Saving Static Data. Message:" + exp.Message, "");
            }
            Trace.TraceInformation("SightseeingStaticData.Save exiting");
        }

        public void Load(string itmCode, string ctyCode, SightseeingBookingSource src)
        {
            Trace.TraceInformation("SightseeingStaticData.Load entered : hotelCode = " + itmCode + "|Source = " + src.ToString());
            if (itmCode != null && itmCode.Length == 0)
            {
                throw new ArgumentException("ItemCode Should be enterd !", "");
            }
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@itemCode", itmCode);
            paramList[1] = new SqlParameter("@cityCode", ctyCode);
            paramList[2] = new SqlParameter("@source", (int)src);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetSightseeingStaticDataByCode, paramList, connection);
            if (data.Read())
            {
                itemCode = itmCode;
                cityCode = ctyCode;
                source = (SightseeingBookingSource)Convert.ToInt16(data["source"]);
                timeStamp = Convert.ToDateTime(data["timeStamp"]);
                description = data["description"].ToString();
                tourInfo = data["tour"].ToString();
                includes = data["includes"].ToString();
                departurePoint = data["departurePoint"].ToString();
                flashLink = data["flashLink"].ToString();
                notes = data["notes"].ToString();
                itemName = data["itemName"].ToString();
                imageInfo = data["imageInfo"].ToString();
                closedDates = data["closedDates"].ToString();
            }
            data.Close();
            connection.Close();
            TourOperations tour = new TourOperations();
            tourOperation = tour.Load(ctyCode, itmCode);
            Trace.TraceInformation("SightseeingStaticData.Load exit :");
        }
        public void Update()
        {
            Trace.TraceInformation("SightseeingStaticData.Update entered.");
            try
            {
                using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                {
                    SqlParameter[] paramList = new SqlParameter[12];
                    paramList[0] = new SqlParameter("@itemCode", itemCode);
                    paramList[1] = new SqlParameter("@cityCode", cityCode);
                    paramList[2] = new SqlParameter("@description", description);
                    paramList[3] = new SqlParameter("@tour", tourInfo);
                    paramList[4] = new SqlParameter("@includes", includes);
                    paramList[5] = new SqlParameter("@departurePoint", departurePoint);
                    paramList[6] = new SqlParameter("@flashLink", flashLink);
                    paramList[7] = new SqlParameter("@notes", notes);
                    paramList[8] = new SqlParameter("@source", source);
                    paramList[9] = new SqlParameter("@itemName", itemName);
                    paramList[10] = new SqlParameter("@imageInfo", imageInfo);
                    paramList[11] = new SqlParameter("@closedDates", closedDates);
                    int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateSightseeingStaticData, paramList);
                    TourOperations tourDetails = new TourOperations();
                    tourDetails.CityCode = cityCode;
                    tourDetails.ItemCode = itemCode;
                    tourDetails.Delete();
                    foreach (TourOperations tour in tourOperation)
                    {
                        tour.CityCode = cityCode;
                        tour.ItemCode = itemCode;
                        tour.Save();
                    }
                    updateTransaction.Complete();
                }
            }
            catch (Exception exp)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.SightseeingSearch, CoreLogic.Severity.High, 1, "Exception in Updating Static Data. Message:" + exp.Message, "");
            }
            Trace.TraceInformation("SightseeingStaticData.Update exiting");

        }
        public string DownloadImage(string images, SightseeingBookingSource Source)
        {
            Trace.TraceInformation("SightseeingStaticData.DownloadImage entered.");
            // Download image from given URL and save image name only in database.
            string imagesURL = string.Empty;
            string[] imgURL = images.Replace("\\", "/").Split('|');
            string imgPath = string.Empty;
            if (Source == SightseeingBookingSource.GTA)
            {
                imgPath = Configuration.ConfigurationSystem.GTAConfig["downloadImagesPath"].ToString() + "\\";
            }
           
            for (int k = 0; k < imgURL.Length - 1; k++)
            {
                WebClient Client = new WebClient();
                string guidValue = Guid.NewGuid().ToString();
                string[] fileName = imgURL[k].Split('/');
                string imageFileName = fileName[fileName.Length - 1];
                try
                {
                    Client.DownloadFile(imgURL[k], imgPath + guidValue + imageFileName);
                    imagesURL += guidValue + imageFileName + "|";
                }
                catch (Exception excep)
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.Exception, CoreLogic.Severity.Low, 0, "HotelImage.DownloadImage Downloading Image Failed Message : " + excep.Message, "");
                }
            }
            Trace.TraceInformation("SightseeingStaticData.DownloadImage Exit.");
            return imagesURL;
        }
    }


}
