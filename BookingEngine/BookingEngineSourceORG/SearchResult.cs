using System;
using System.Collections.Generic;
using System.Text;


namespace Technology.BookingEngine
{
    [Serializable]
    public class SearchResult//:Air20.AirPricingSolution
    {
        #region Private Members
        double baseFare;
        double tax;
        double totalFare;
        string currency;
        string fareType; //TODO: this can be an enum
        Fare[] fareBreakdown;
        FlightInfo[][] flights;
        List<FareRule> fareRules;
        string lastTicketDate;
        string ticketAdvisory;
        PriceAccounts price;
        /// <summary>
        /// validating airline code of the flight.
        /// </summary>
        string validatingAirline;
        /// <summary>
        /// True if the fare is non refundable.
        /// </summary>
        bool nonRefundable;
        /// <summary>
        /// True if E-ticket is allowed.
        /// </summary>
        bool eticketEligible;
        string airline;
        /// <summary>
        /// To mentain unique Id of results when shown on return search page after filtering and sorting.
        /// </summary>
        int resultId;
        /// <summary>
        /// stores the alias airlinecode of the flight name like ITR in case of ITRed. 
        /// </summary>
        string aliasAirlineCode;

        private BookingSource resultBookingSource;
        private Air20.AirPricingSolution uapiPricingSolution;

        //TODO: Fare Basis Code ?
        #endregion

        #region Public Properties
        // For UAPI to get price in Create Reservation Res
        public Air20.AirPricingSolution UapiPricingSolution
        {
            get
            {
                return uapiPricingSolution;
            }
            set
            {
                uapiPricingSolution = value;
            }
        }
        public FlightInfo[][] Flights
        {
            get
            {
                return flights;
            }
            set
            {
                flights = value;
            }
        }

        public List<FareRule> FareRules
        {
            get
            {
                return fareRules;
            }
            set
            {
                fareRules = value;
            }
        }

        public Fare[] FareBreakdown
        {
            get
            {
                return fareBreakdown;
            }
            set
            {
                fareBreakdown = value;
            }
        }

        public double BaseFare
        {
            get
            {
                return baseFare;
            }
            set
            {
                baseFare = value;
            }
        }

        public double Tax
        {
            get
            {
                return tax;
            }
            set
            {
                tax = value;
            }
        }

        public double TotalFare
        {
            get
            {
                return totalFare;
            }
            set
            {
                totalFare = value;
            }
        }

        public string Currency
        {
            get
            {
                return currency;
            }
            set
            {
                currency = value;
            }
        }

        public string FareType
        {
            get
            {
                return fareType;
            }
            set
            {
                fareType = value;
            }
        }
        public string LastTicketDate
        {
            get
            {
                return lastTicketDate;
            }
            set
            {
                lastTicketDate = value;
            }
        }

        public string TicketAdvisory
        {
            get
            {
                return ticketAdvisory;
            }
            set
            {
                ticketAdvisory = value;
            }
        }

        public PriceAccounts Price
        {
            get 
            {
                return this.price;
            }
            set
            {
                this.price = value;
            }
        }

        /// <summary>
        /// get the alias airlinecode of the flight name like ITR in case of ITRed. 
        /// </summary>
        public string AliasAirlineCode
        {
            get
            {
                if (aliasAirlineCode != null && aliasAirlineCode.Length > 0)
                {
                    return aliasAirlineCode;
                }
                else
                {
                    for (int g = 0; g < flights.Length; g++)
                    {
                        for (int f = 0; f < flights[g].Length; f++)
                        {
                            if (flights[g][f].Airline == "IT")
                            {

                                foreach (KeyValuePair<int, int> s in Configuration.ConfigurationSystem.ITRedConfig)
                                {
                                    if (s.Key <= Convert.ToInt32(flights[g][f].FlightNumber) && s.Value >= Convert.ToInt32(flights[g][f].FlightNumber))
                                    {
                                        aliasAirlineCode = "ITR";
                                        break;
                                    }
                                }
                            }
                            if (aliasAirlineCode == "ITR")
                            {
                                break;
                            }
                        }
                        if (aliasAirlineCode == "ITR")
                        {
                            break;
                        }
                    }
                    if (aliasAirlineCode != "ITR")
                    {
                        aliasAirlineCode = ValidatingAirline;
                    }
                    return aliasAirlineCode;
                }
            }
        }

        /// <summary>
        /// Gets the validating airline of the flight.
        /// </summary>
        public string ValidatingAirline
        {
            get
            {
                if (validatingAirline == null || validatingAirline.Length == 0)
                {
                    validatingAirline = flights[0][0].Airline;
                    string countryKey = flights[0][0].Origin.CountryCode;
                    bool done = false;
                    for (int g = 0; g < flights.Length; g++)
                    {
                        for (int f = 0; f < flights[g].Length; f++)
                        {
                            if (flights[g][f].Destination.CountryCode != countryKey)
                            {
                                validatingAirline = flights[g][f].Airline;
                                done = true;
                                break;
                            }
                        }
                        if (done)
                        {
                            break;
                        }
                    }
                }
                return validatingAirline;
            }
            set
            {
                validatingAirline = value;
            }
        }

        /// <summary>
        /// True if fare is non refundable.
        /// </summary>
        public bool NonRefundable
        {
            get
            {
                return nonRefundable;
            }
            set
            {
                nonRefundable = value;
            }
        }
        /// <summary>
        /// True if eticket is allowed.
        /// </summary>
        public bool EticketEligible
        {
            get
            {
                return eticketEligible;
            }
            set
            {
                eticketEligible = value;
            }
        }
        public BookingSource ResultBookingSource
        {
            get
            {
                return resultBookingSource;
            }
            set
            {
                resultBookingSource = value;
            }
        }
        public string Airline
        {
            get
            {
                return airline;
            }
            set
            {
                airline = value;
            }
        }
        /// <summary>
        /// To mentain unique Id of results when shown on return search page after filtering and sorting.
        /// </summary>
        public int ResultId
        {
            get
            {
                return resultId;
            }
            set
            {
                resultId = value;
            }
        }
        
        #endregion

        /// <summary>
        /// Gets an array of flight which includes flights from all flight groups
        /// </summary>
        /// <param name="result">Search result object</param>
        /// <returns>Array of Flightinfo (segments)</returns>
        /// 
        public static FlightInfo[] GetSegments(SearchResult result)
        {
            int flightCount = 0;
            for (int g = 0; g < result.Flights.Length; g++)
            {
                flightCount += result.Flights[g].Length;
            }
            FlightInfo[] flightSegments = new FlightInfo[flightCount];
            int i = 0;
            for (int g = 0; g < result.Flights.Length; g++)
            {
                for (int f = 0; f < result.Flights[g].Length; f++)
                {
                    flightSegments[i] = result.Flights[g][f];
                    i++;
                }
            }
            return flightSegments;
        }
    }
}
