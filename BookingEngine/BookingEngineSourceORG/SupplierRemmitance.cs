using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using Technology.Data;

namespace Technology.BookingEngine
{
    public class SupplierRemmitance
    {
        public SupplierRemmitance()
        {
            publishedFare = 0;
            netFare = 0;
            markup = 0;
            ourCommission = 0;
            tax = 0;
            otherCharges = 0;
            chargeBU = new List<ChargeBreakUp>();
            tdsCommission = 0;
            tdsPLB = 0;
            serviceTax = 0;
            currency = "";
            accpriceType = PriceType.PublishedFare;
            cessTax = 0;
            ourPlb = 0;
            supplierCode = string.Empty;

        }
        public SupplierRemmitance(decimal pFare, decimal nFare, decimal markUP, decimal ourComm, decimal TAX, decimal otherCharge, decimal tdsReq, decimal serviceTaxReq,string curr)
        {
            publishedFare = pFare;
            netFare = nFare;
            markup = markUP;
            ourCommission = ourComm;
            tax = TAX;
            otherCharges = otherCharge;
            chargeBU = new List<ChargeBreakUp>();
            tdsCommission = tdsReq;
            tdsPLB = tdsReq;
            serviceTax = serviceTaxReq;
            currency = curr;
        }

        public SupplierRemmitance(decimal pFare, decimal nFare, decimal markUP, decimal ourComm,decimal TAX, decimal otherCharge, decimal tdsReq, decimal serviceTaxReq, string curr, PriceType pType)
        {
            publishedFare = pFare;
            netFare = nFare;
            markup = markUP;
            ourCommission = ourComm;
            tax = TAX;
            otherCharges = otherCharge;
            chargeBU = new List<ChargeBreakUp>();
            tdsCommission = tdsReq;
            tdsPLB = tdsReq;
            serviceTax = serviceTaxReq;
            currency = curr;
            accpriceType = pType;
        }

        /// <summary>
        /// Unique field for SupplierRemmitance
        /// </summary>
        private int supplierRemmitanceId;
        ///<summary>
        /// Property to store supplierRemmitanceId
        ///</summary>
        public int SupplierRemmitanceId
        {
            get { return supplierRemmitanceId; }
            set { supplierRemmitanceId = value; }
        }

        /// <summary>
        /// Field contained supplier Code
        /// </summary>
        private string supplierCode;
        ///<summary>
        /// Property to store supplier code
        ///</summary>
        public string SupplierCode
        {
            get { return supplierCode;}
            set { supplierCode=value;}
        }
        ///<summary>
        /// Field to store ticketid
        ///</summary>

        private int ticketid;

        ///<summary>
        /// Property to store ticketid
        ///</summary>
        public int TicketId
        {
            get { return ticketid; }
            set { ticketid = value; }
        }
        /// <summary>
        /// Field contained Published Fare of booking
        /// </summary>

        private decimal publishedFare;

        ///<summary>
        /// Property to published Fare
        ///</summary>
        public decimal PublishedFare
        {
            get { return publishedFare; }
            set { publishedFare = value; }
        }

        /// <summary>
        /// Field contained netPrice of booking
        /// </summary>
        private decimal netFare;

        ///<summary>
        /// Property to store net Fare
        ///</summary>
        public decimal NetFare
        {
            get { return netFare; }
            set { netFare = value; }
        }
        /// <summary>
        /// Field contained markup(incase of net fare)
        /// </summary>
        private decimal markup;

        ///<summary>
        /// Property to store markup
        ///</summary>
        public decimal Markup
        {
            get { return markup; }
            set { markup = value; }
        }

        /// <summary>
        /// Field contained tax at that price(in currency)
        /// </summary>

        private decimal tax;
        ///<summary>
        /// Property to store tax
        ///</summary>
        public decimal Tax
        {
            get { return tax; }
            set { tax = value; }
        }
        

        /// <summary>
        /// Field contains other charges(in currency)
        /// </summary>

        private decimal otherCharges;

        ///<summary>
        /// Property to store otherCharges
        ///</summary>
        public decimal OtherCharges
        {
            get { return otherCharges; }
            set { otherCharges = value; }
        }

        /// <summary>
        /// Field contains our commission(supplier commision)
        /// </summary>

        private decimal ourCommission;
        ///<summary>
        /// Property to store ourCommission
        ///</summary>
        public decimal OurCommission
        {
            get { return ourCommission; }
            set { ourCommission = value; }
        }

        ///<summary>
        /// Property to store OurPlb
        ///</summary>
        private decimal ourPlb;

        ///<summary>
        /// Property to store OurPlb
        ///</summary>
        public decimal OurPlb
        {
            get { return ourPlb; }
            set { ourPlb = value; }
        }

        /// <summary>
        /// Field contained serviceTax of booking
        /// </summary>

        private decimal serviceTax;

        ///<summary>
        /// Property to store serviceTax
        ///</summary>
        public decimal ServiceTax
        {
            get { return serviceTax; }
            set { serviceTax = value; }
        }

        private decimal cessTax;

        ///<summary>
        /// Property to store serviceTax
        ///</summary>
        public decimal CessTax
        {
            get { return cessTax; }
            set { cessTax = value; }
        }

        /// <summary>
        /// Currency string
        /// </summary>
        private string currency;
        ///<summary>
        /// Property to store currency
        ///</summary>
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        /// <summary>
        /// Field to store tds Commission
        /// </summary>
        private decimal tdsCommission;
        ///<summary>
        /// Property to store tdsCommission
        ///</summary>
        public decimal TdsCommission
        {
            get { return tdsCommission; }
            set { tdsCommission = value; }
        }

        /// <summary>
        /// Field to store tdsPLB
        /// </summary>
        private decimal tdsPLB;
        ///<summary>
        /// Property to store tdsPLB
        ///</summary>
        public decimal TdsPLB
        {
            get { return tdsPLB; }
            set { tdsPLB = value; }
        }

        /// <summary>
        /// Field to store priceType
        /// </summary>
        private PriceType accpriceType;
        ///<summary>
        /// Property to store priceType
        ///</summary>
        public PriceType AccpriceType
        {
            get { return accpriceType; }
            set { accpriceType = value; }
        }

        /// <summary>
        /// field to store charge breakup
        /// </summary>
        private List<ChargeBreakUp> chargeBU;

        ///<summary>
        /// Property to  store chargeBU
        ///</summary>
        public List<ChargeBreakUp> ChargeBU
        {
            get { return chargeBU; }
            set { chargeBU = value; }
        }
        /// <summary>
        /// rate of exchange
        /// </summary>
        /// 
        private decimal rateOfExchange;

        ///<summary>
        /// Property to store rateOfExchange
        ///</summary>
        public decimal RateOfExchange
        {
            get { return rateOfExchange; }
            set { rateOfExchange = value; }
        }

        /// <summary>
        /// This will contain currency code - ie USD
        /// </summary>
        private string currencyCode;

        ///<summary>
        /// Property to currencyCode
        ///</summary>
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }

        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get { return createdOn; }
            set { createdOn = value; }
        }

        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get { return lastModifiedOn; }
            set { lastModifiedOn = value; }
        }

        /// <summary>
        /// Gets or sets CommissionType
        /// </summary>
        private string commissionType;
        public string CommissionType
        {
            get { return commissionType;}
            set {commissionType=value ;}
        }

        public void Save()
        {
            Trace.TraceInformation("SupplierRemmittance.Save entered : ");
           
            if (supplierRemmitanceId > 0)
            {
                SqlParameter[] paramList = new SqlParameter[18];
                paramList[0] = new SqlParameter("@supplierRemmitanceId", supplierRemmitanceId);
                paramList[1] = new SqlParameter("@supplierCode", supplierCode);
                paramList[2] = new SqlParameter("@ticketId", ticketid);

                paramList[3] = new SqlParameter("@publishedFare", Math.Round(publishedFare));
                paramList[4] = new SqlParameter("@netFare", Math.Round(netFare));
                paramList[5] = new SqlParameter("@markup", Math.Round(markup));


                paramList[6] = new SqlParameter("@tax", Math.Round(tax));
                paramList[7] = new SqlParameter("@ourCommission", Math.Round(ourCommission));
                paramList[8] = new SqlParameter("@serviceTax", Math.Round(serviceTax));

                paramList[9] = new SqlParameter("@currency", currency);

                paramList[10] = new SqlParameter("@tdsCommission", Math.Round(tdsCommission));
                paramList[11] = new SqlParameter("@tdsPLB", Math.Round(tdsPLB));
                paramList[12] = new SqlParameter("@otherCharges", Math.Round(otherCharges));
            
                paramList[13] = new SqlParameter("@priceType", accpriceType);
                paramList[14] = new SqlParameter("@rateOfExchange", rateOfExchange);
                paramList[15] = new SqlParameter("@ourPlb", ourPlb);
                paramList[16] = new SqlParameter("@cessTax", cessTax);
                paramList[17] = new SqlParameter("@createdBy", createdBy);
               
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateSupplierRemmittance, paramList);
                Trace.TraceInformation("SupplierRemmittance.Save exiting : rowsAffected = " + rowsAffected);
            }
            else
            {
                SqlParameter[] paramList = new SqlParameter[20];
                paramList[0] = new SqlParameter("@supplierRemmitanceId", SqlDbType.Int);
                paramList[0].Direction = ParameterDirection.Output;
                paramList[1] = new SqlParameter("@supplierCode", supplierCode);
                paramList[2] = new SqlParameter("@ticketId", ticketid);

                paramList[3] = new SqlParameter("@publishedFare", Math.Round(publishedFare));
                paramList[4] = new SqlParameter("@netFare", Math.Round(netFare));
                paramList[5] = new SqlParameter("@markup", Math.Round(markup));


                paramList[6] = new SqlParameter("@tax", Math.Round(tax));
                paramList[7] = new SqlParameter("@ourCommission", Math.Round(ourCommission));
                paramList[8] = new SqlParameter("@serviceTax", Math.Round(serviceTax));

                paramList[9] = new SqlParameter("@currency", currency);

                paramList[10] = new SqlParameter("@tdsCommission", Math.Round(tdsCommission));
                paramList[11] = new SqlParameter("@tdsPLB", Math.Round(tdsPLB));
                paramList[12] = new SqlParameter("@otherCharges", Math.Round(otherCharges));

                paramList[13] = new SqlParameter("@priceType", accpriceType);
                paramList[14] = new SqlParameter("@rateOfExchange", rateOfExchange);
                paramList[15] = new SqlParameter("@ourPlb", ourPlb);
                paramList[16] = new SqlParameter("@cessTax", cessTax);
                paramList[17] = new SqlParameter("@createdOn", createdOn);
                paramList[18] = new SqlParameter("@createdBy", createdBy);
                paramList[19] = new SqlParameter("@commissionType", commissionType);

                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddSupplierRemmittance, paramList);
                supplierRemmitanceId = (int)paramList[0].Value;
                //for (int i = 0; i < chargeBU.Count; i++)
                //{
                //    chargeBU[i].PriceId = this.supplierRemmitanceId;
                //    chargeBU[i].Save();
                //}

                Trace.TraceInformation("SupplierRemmittance.Save exiting : rowsAffected = " + rowsAffected);
            }
        }
        public static SupplierRemmitance LoadSupplierRemmitance( int ticketId)
        {
            Trace.TraceInformation("SupplierRemmitance.LoadSupplierRemmitance entered : ticketId = " + ticketId);
            SupplierRemmitance remmittance = new SupplierRemmitance();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ticketId", ticketId);

            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetSupplierRemmitance, paramList, connection);
            if (data.Read())
            {
               remmittance.SupplierRemmitanceId=Convert.ToInt32(data["SupplierRemmitanceId"]);
               remmittance.supplierCode =Convert.ToString(data["SupplierCode"]);
               remmittance.ticketid= Convert.ToInt32(data["TicketId"]);
               remmittance.publishedFare =Convert.ToDecimal(data["PublishedFare"]);
               remmittance.netFare = Convert.ToDecimal(data["NetFare"]);
               remmittance.markup =Convert.ToDecimal(data["Markup"]);
               remmittance.tax =Convert.ToDecimal(data["Tax"]);
               remmittance.otherCharges =Convert.ToDecimal(data["OtherCharges"]);
               remmittance.ourCommission =Convert.ToDecimal(data["OurCommission"]);
               remmittance.serviceTax =Convert.ToDecimal(data["ServiceTax"]);
               if (data["TdsCommision"] != DBNull.Value)
               {
                   remmittance.tdsCommission =Convert.ToDecimal(data["TdsCommision"]);
               }
               if (data["TdsPlb"] != DBNull.Value)
               {
                   remmittance.tdsPLB = Convert.ToDecimal(data["TdsPlb"]);
               }
              
               remmittance.rateOfExchange =Convert.ToDecimal(data["RateOfExchange"]);
               remmittance.currencyCode = data["CurrencyCode"].ToString();
               remmittance.OurPlb = Convert.ToDecimal(data["OurPlb"]);
               remmittance.cessTax = Convert.ToDecimal(data["CessTax"]);
               remmittance.createdBy = Convert.ToInt32(data["CreatedBy"]);
               remmittance.commissionType = data["commissionType"].ToString();
            }
            data.Close();
            connection.Close();
            Trace.TraceInformation("SupplierRemmitance.LoadSupplierRemmitance exiting");
            return remmittance;
        }
    }
}
