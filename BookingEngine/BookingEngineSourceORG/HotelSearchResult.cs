using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Formatters.Binary;

using Technology.Data;


namespace Technology.BookingEngine
{
    public struct HotelDetails
    {
        public string HotelCode;
        public string HotelName;
        public string Description;
        //public Dictionary<string, string> Rooms; // Keys will be RoomName, RoomTypeId and Description
        public Room[] RoomData;
        public List<string> HotelFacilities;
        public string HotelPolicy; // CheckOut time, check-in time
        public List<string> RoomFacilities;
        public string Services;
        public Dictionary<string, string> Attractions; // key will be Description and Distance + Unit
        public string Image;
        public List<string> Images;
        public string Address;
        public int PinCode;
        public string CountryName;
        public string PhoneNumber;
        public string Email;
        public string URL;
        public string FaxNumber;
        public string Map;
        public HotelRating hotelRating;
        public List<string> Location;
    }

    public struct Room
    {
        public string RoomName;
        public string RoomTypeId;
        public string Description;
        public List<string> Images;
    }

    [Serializable]
    public struct HotelRoomsDetails
    {
        public string RoomTypeCode;
        public string RoomTypeName;
        public string RatePlanCode;
        public string CancellationPolicy;
        public RoomRates[] Rates;
        public decimal SellExtraGuestCharges;
        public decimal PubExtraGuestCharges;
        public decimal TotalPrice; // sum of roomrates + totaltax
        public decimal TotalTax;
        public decimal SellingFare;
        public List<string> Amenities;
        public Dictionary<string, int> Occupancy; // Keys are - MaxAdult, MaxChild, MaxInfant, MaxGuest, BaseAdult, BaseChild
        public string SequenceNo;//for which room it is associated 1|2|3 means this room type is for 1st ,2nd & 3rd room.
        public decimal Discount;
        public decimal ChildCharges;
    }
    [Serializable]
    public struct RoomRates
    {
        //public WeekDays Days;
        public DateTime Days; // for 0 it will be first day, for 1 it will be second day and so on
        public RateType RateType;
        public decimal Amount;
        public decimal BaseFare;
        public decimal Totalfare;
        public decimal SellingFare;
    }

    public enum HotelRating
    {
        All = 0,
        OneStar = 1,
        TwoStar = 2,
        ThreeStar = 3,
        FourStar = 4,
        FiveStar = 5

    }

    public enum RateType
    {
        Published = 1, //SellRate == published
        Negotiated = 2  // Negotiated == NetRate
    }

    public enum RoomType
    {
        Single = 1,
        Double = 2,
        Triple = 3,
        Quad = 4
    }

    public enum AvailabilityType
    {
        OnRequest = 1,
        Confirmed = 2
    }

    [Serializable]
    public class HotelSearchResult
    {
        # region private variables

        private int rPH; // re4lated key to related hotel - used for identifying fare and hotel relationship
        private string hotelCode;
        private string cityCode;
        private string hotelName;
        private string hotelCategory;
        private string hotelPicture;
        private string hotelDescription;
        private string hotelMap;
        private string hotelLocation;
        private string hotelAddress;
        private string hotelContactNo;

        private HotelRating rating;
        private DateTime startDate;
        private DateTime endDate;
        private RateType rateType;
        private RoomGuestData[] roomGuest;
        private HotelRoomsDetails[] roomDetails;

        private decimal totalPrice;
        private decimal totalTax;
        private decimal sellingFare;
        private AvailabilityType availType;
        private HotelBookingSource bookingSource;
        private PriceAccounts price;
        private string currency;
        private string propertyType;
        private string supplierType;
        private string promoMessage = string.Empty;
        private bool isFeaturedHotel;
        #endregion

        # region public properties

        public int RPH
        {
            get
            {
                return rPH;
            }
            set
            {
                rPH = value;
            }
        }

        public string HotelCode
        {
            get
            {
                return hotelCode;
            }
            set
            {
                hotelCode = value;
            }
        }
        public RoomGuestData[] RoomGuest
        {
            get
            {
                return roomGuest;
            }
            set
            {
                roomGuest = value;
            }
        }
        public string CityCode
        {
            get
            {
                return cityCode;
            }
            set
            {
                cityCode = value;
            }
        }

        public string HotelName
        {
            get
            {
                return hotelName;
            }
            set
            {
                hotelName = value;
            }
        }
        public string HotelCategory
        {
            get
            {
                return hotelCategory;
            }
            set
            {
                hotelCategory = value;
            }
        }

        public string HotelPicture
        {
            get
            {
                return hotelPicture;
            }
            set
            {
                hotelPicture = value;
            }
        }

        public string HotelDescription
        {
            get
            {
                return hotelDescription;
            }
            set
            {
                hotelDescription = value;
            }
        }

        public string HotelMap
        {
            get
            {
                return hotelMap;
            }
            set
            {
                hotelMap = value;
            }
        }

        public string HotelLocation
        {
            get
            {
                return hotelLocation;
            }
            set
            {
                hotelLocation = value;
            }
        }

        public string HotelAddress
        {
            get
            {
                return hotelAddress;
            }
            set
            {
                hotelAddress = value;
            }
        }

        public string HotelContactNo
        {
            get
            {
                return hotelContactNo;
            }
            set
            {
                hotelContactNo = value;
            }
        }

        public HotelRating Rating
        {
            get
            {
                return rating;
            }
            set
            {
                rating = value;
            }
        }

        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }

        public RateType RateType
        {
            get { return rateType; }
            set { rateType = value; }
        }

        public HotelRoomsDetails[] RoomDetails
        {
            get { return roomDetails; }
            set { roomDetails = value; }
        }

        public decimal TotalPrice
        {
            get { return totalPrice; }
            set { totalPrice = value; }
        }

        public decimal TotalTax
        {
            get { return totalTax; }
            set { totalTax = value; }
        }

        public decimal SellingFare
        {
            get { return sellingFare; }
            set { sellingFare = value; }
        }

        public AvailabilityType AvailType
        {
            get { return availType; }
            set { availType = value; }
        }
        public HotelBookingSource BookingSource
        {
            get { return bookingSource; }
            set { bookingSource = value; }
        }
        public PriceAccounts Price
        {
            get { return price; }
            set { price = value; }
        }
        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }
        public string PropertyType
        {
            get { return propertyType; }
            set { propertyType = value; }
        }
        public string SupplierType
        {
            get { return supplierType; }
            set { supplierType = value; }
        }
        public string PromoMessage
        {
            get { return promoMessage; }
            set { promoMessage = value; }
        }
        public bool IsFeaturedHotel
        {
            get { return isFeaturedHotel; }
            set { isFeaturedHotel = value; }
        }
        #endregion

        #region METHODS

        /// <summary>
        /// Save the search result into DB for the given session
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="searchResult"></param>
        public void Save(string sessionId, HotelSearchResult[] searchResult, int noOfRooms)
        {
            Trace.TraceInformation("HotelSearchResult[].Save entered : sessionId = " + sessionId);
            if (sessionId == null)
            {
                throw new ArgumentException("The current session expired");
            }
            //order the results on the selling price
            SortResultsByPrice(ref searchResult, noOfRooms);
            searchResult = FilterByTBOConnect(searchResult);
            if (searchResult != null && searchResult.Length > 0)
            {
                byte[] data = GetByteArrayWithObject(searchResult);
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@sessionId", sessionId);
                paramList[1] = new SqlParameter("@resultData", data);

                Dal.ExecuteNonQuerySP(SPNames.AddHotelSearchResultCache, paramList);
            }  // ELSE no data to save.

            Trace.TraceInformation("HotelSearchResult.Save exited : sessionId = " + sessionId);
        }
        /// <summary>
        /// Save the TBOConnect results into database
        /// </summary>
        /// <param name="searchResult">HotelSearchResult object</param>
        /// <param name="city">City Name</param>
        public void Save(HotelSearchResult[] searchResult, string cityName)
        {
            Trace.TraceInformation("HotelSearchResult.Save entered:city=" + cityName);
            List<HotelSearchResult> lstResults = new List<HotelSearchResult>();
            //Check for search result exists or not
            if (searchResult != null && searchResult.Length > 0)
            {
                foreach (HotelSearchResult result in searchResult)
                {
                    if (result.IsFeaturedHotel)
                    {
                        lstResults.Add(result);
                    }
                }

                decimal minutes = Convert.ToDecimal(Technology.Configuration.ConfigurationSystem.HotelConnectConfig["cacheTimeinMinutes"]);
                byte[] data = GetByteArrayWithObject(lstResults.ToArray());
                SqlParameter[] paramList = new SqlParameter[3];
                paramList[0] = new SqlParameter("@cityName", cityName);
                paramList[1] = new SqlParameter("@resultData", data);
                paramList[2] = new SqlParameter("@timeToExpire", minutes);
                if (lstResults.Count > 0)
                {
                    Dal.ExecuteNonQuerySP(SPNames.AddTBOConnectSearchResultCache, paramList);
                }
            }
            Trace.TraceInformation("HotelSearchResult.Save exited:city=" + cityName);
        }
        /// <summary>
        /// Loads the tboconnect results from the db for displaying on the e-ticket
        /// </summary>
        /// <param name="cityName">City Name</param>
        /// <returns></returns>
        public HotelSearchResult[] LoadTBOResults(string cityName)
        {
            Trace.TraceInformation("HotelSearchResult.Load entered:cityName=" + cityName);
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@cityName", cityName);
            SqlConnection con = Dal.GetConnection();
            SqlDataReader reader = Dal.ExecuteReaderSP(SPNames.GetTBOConnectSearchResultCache, paramList, con);
            byte[] data = new byte[0];
            HotelSearchResult[] searchResultCache = new HotelSearchResult[0];
            if (reader.Read())
            {
                data = (byte[])reader["resultData"];
            }
            reader.Close();
            con.Close();
            if (data.Length > 0)
            {
                searchResultCache = (HotelSearchResult[])GetObjectWithByteArray(data);
            }
            Trace.TraceInformation("HotelSearchResult.Load exited:cityName=" + cityName);
            return searchResultCache;
        }
        private void SortResultsByPrice(ref HotelSearchResult[] finalResult, int noOfRooms)
        {
            //since all the sources does not give price sorted results
            //rate of exchange to compare the price in INR
            CoreLogic.StaticData staticInfo = new CoreLogic.StaticData();
            Dictionary<string, decimal> rateOfEx = staticInfo.CurrencyROE;
            for (int n = 0; n < finalResult.Length - 1; n++)
            {
                for (int m = n + 1; m < finalResult.Length; m++)
                {
                    decimal exRateN = 1, exRateM = 1;
                    if (rateOfEx.ContainsKey(finalResult[n].Currency))
                    {
                        exRateN = rateOfEx[finalResult[n].Currency];
                    }
                    if (rateOfEx.ContainsKey(finalResult[m].Currency))
                    {
                        exRateM = rateOfEx[finalResult[m].Currency];
                    }
                    decimal priceN = 0, priceM = 0;
                    if (noOfRooms > 1)
                    {
                        //decimal price = 0;
                        //decimal sellPrice = 0;
                        decimal tempPrice = 0;
                        if (finalResult[n].BookingSource == HotelBookingSource.Desiya)
                        {
                            Technology.BookingEngine.HotelRoomsDetails roomInfo = finalResult[n].RoomDetails[0];
                            priceN = roomInfo.SellingFare + roomInfo.TotalTax + roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length;
                            priceN *= exRateN;
                        }
                        else if (finalResult[n].BookingSource == HotelBookingSource.IAN)
                        {
                            Technology.BookingEngine.HotelRoomsDetails roomInfo = finalResult[n].RoomDetails[0];
                            //price = Math.Round((roomInfo.TotalPrice + roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length), 2);
                            priceN = Math.Round((roomInfo.SellingFare + roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length), Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                            priceN *= exRateN;
                        }
                        else
                        {
                            Technology.BookingEngine.HotelRoomsDetails[] roomInfo = finalResult[n].RoomDetails;
                            for (int x = 0; x < noOfRooms; x++)
                            {
                                for (int y = 0; y < finalResult[n].RoomDetails.Length; y++)
                                {
                                    tempPrice = 0;
                                    if ((finalResult[n].RoomDetails[y].SequenceNo != null && finalResult[n].RoomDetails[y].SequenceNo.Contains((x + 1).ToString())))
                                    {
                                        if (finalResult[n].BookingSource != HotelBookingSource.TBOConnect)
                                        {
                                            priceN += finalResult[n].RoomDetails[y].SellingFare + (finalResult[n].RoomDetails[y].SellExtraGuestCharges * finalResult[n].RoomDetails[y].Rates.Length) + finalResult[n].RoomDetails[y].TotalTax - finalResult[n].RoomDetails[y].Discount;
                                        }
                                        else
                                        {
                                            tempPrice += finalResult[n].RoomDetails[y].SellingFare + finalResult[n].RoomDetails[y].SellExtraGuestCharges + finalResult[n].RoomDetails[y].TotalTax - finalResult[n].RoomDetails[y].Discount;
                                            //tempPrice += tempPrice * finalResult[n].Price.AgentCommission / 100;
                                            priceN += tempPrice;
                                        }
                                        break;
                                    }
                                }
                            }
                            priceN *= exRateN;
                        }

                        if (finalResult[m].BookingSource == HotelBookingSource.Desiya)
                        {
                            Technology.BookingEngine.HotelRoomsDetails roomInfo = finalResult[m].RoomDetails[0];
                            priceM = roomInfo.SellingFare + roomInfo.TotalTax + roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length;
                            priceM *= exRateM;
                        }
                        else if (finalResult[m].BookingSource == HotelBookingSource.IAN)
                        {
                            Technology.BookingEngine.HotelRoomsDetails roomInfo = finalResult[m].RoomDetails[0];
                            //price = Math.Round((roomInfo.TotalPrice + roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length), 2);
                            priceM = Math.Round((roomInfo.SellingFare + roomInfo.SellExtraGuestCharges * roomInfo.Rates.Length), Convert.ToInt32(Technology.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"]));
                            priceM *= exRateM;
                        }
                        else
                        {
                            Technology.BookingEngine.HotelRoomsDetails[] roomInfo = finalResult[m].RoomDetails;
                            for (int x = 0; x < noOfRooms; x++)
                            {
                                for (int y = 0; y < finalResult[m].RoomDetails.Length; y++)
                                {
                                    tempPrice = 0;
                                    if ((finalResult[m].RoomDetails[y].SequenceNo != null && finalResult[m].RoomDetails[y].SequenceNo.Contains((x + 1).ToString())))
                                    {
                                        if (finalResult[m].BookingSource != HotelBookingSource.TBOConnect)
                                        {
                                            priceM += finalResult[m].RoomDetails[y].SellingFare + (finalResult[m].RoomDetails[y].SellExtraGuestCharges * finalResult[m].RoomDetails[y].Rates.Length) + finalResult[m].RoomDetails[y].TotalTax - finalResult[m].RoomDetails[y].Discount;
                                        }
                                        else
                                        {
                                            tempPrice += finalResult[m].RoomDetails[y].SellingFare + finalResult[m].RoomDetails[y].SellExtraGuestCharges + finalResult[m].RoomDetails[y].TotalTax - finalResult[m].RoomDetails[y].Discount;
                                            //tempPrice += tempPrice * finalResult[n].Price.AgentCommission / 100;
                                            priceM += tempPrice;
                                        }
                                        break;
                                    }
                                }
                            }
                            priceM *= exRateM;
                        }
                    }
                    else
                    {
                        if (finalResult[n].bookingSource == HotelBookingSource.IAN)
                        {
                            priceN = (finalResult[n].RoomDetails[0].SellingFare) * exRateN;
                            //priceN /= noOfRooms;
                        }
                        else if (finalResult[n].bookingSource == HotelBookingSource.TBOConnect)
                        {
                            priceN = (finalResult[n].RoomDetails[0].SellingFare + finalResult[n].RoomDetails[0].TotalTax - finalResult[n].RoomDetails[0].Discount + finalResult[n].RoomDetails[0].SellExtraGuestCharges) * exRateN;
                        }
                        else
                        {
                            priceN = (finalResult[n].RoomDetails[0].SellingFare + finalResult[n].RoomDetails[0].TotalTax + finalResult[n].RoomDetails[0].SellExtraGuestCharges * finalResult[n].RoomDetails[0].Rates.Length) * exRateN;
                        }
                        if (finalResult[m].bookingSource == HotelBookingSource.IAN)
                        {
                            priceM = (finalResult[m].RoomDetails[0].SellingFare) * exRateM;
                            //priceM /= noOfRooms;
                        }
                        else if (finalResult[m].bookingSource == HotelBookingSource.TBOConnect)
                        {
                            priceM = (finalResult[m].RoomDetails[0].SellingFare + finalResult[m].RoomDetails[0].TotalTax - finalResult[m].RoomDetails[0].Discount + finalResult[m].RoomDetails[0].SellExtraGuestCharges) * exRateN;
                        }
                        else
                        {
                            priceM = (finalResult[m].RoomDetails[0].SellingFare + finalResult[m].RoomDetails[0].TotalTax + finalResult[m].RoomDetails[0].SellExtraGuestCharges * finalResult[m].RoomDetails[0].Rates.Length) * exRateM;
                        }
                    }
                    if (priceN > priceM)
                    {
                        HotelSearchResult temp = finalResult[n];
                        finalResult[n] = finalResult[m];
                        finalResult[m] = temp;
                    }
                }
            }
        }

        /// <summary>
        /// Load the data from DB for the given session
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        private HotelSearchResult[] Load(string sessionId)
        {
            Trace.TraceInformation("HotelSearchResult.Load entered : sessionId = " + sessionId);

            if (sessionId == null)
            {
                throw new ArgumentException("The current session expired");
            }

            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@sessionId", sessionId);

            SqlConnection connection = Dal.GetConnection();
            SqlDataReader dReader = Dal.ExecuteReaderSP(SPNames.GetHotelSearchResultCache, paramList, connection);
            byte[] data = new byte[0];
            if (dReader.Read())
            {
                data = (byte[])dReader["resultData"];
            }
            dReader.Close();
            connection.Close();

            HotelSearchResult[] searchResultCache = new HotelSearchResult[0];
            if (data.Length > 0)
            {
                searchResultCache = (HotelSearchResult[])GetObjectWithByteArray(data);
            }
            return searchResultCache;
        }

        /// <summary>
        /// Return the HotelResult for the sessionId on selected filter criteria
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="filterCriteria"></param>
        /// <param name="noOfPages"></param>
        /// <returns></returns>
        public HotelSearchResult[] GetFilteredResult(string sessionId, Dictionary<string, string> filterCriteria, ref int noOfPages)
        {
            HotelSearchResult[] noResult = new HotelSearchResult[0];
            if (string.IsNullOrEmpty(sessionId))
            {
                return noResult;
            }
            int recordsPerPage;

            if (filterCriteria.ContainsKey("recordsPerPage"))
            {
                recordsPerPage = Convert.ToInt32(filterCriteria["recordsPerPage"]);
            }
            else
            {
                throw new ArgumentException("Incomplete filter criteria. Need value of recordsPerPage.");
            }
            HotelSearchResult[] searchResultCache = Load(sessionId);
            if (searchResultCache.Length > 0)
            {
                #region get the filter criteria

                string hAddress = filterCriteria.ContainsKey("hotelAddress") ? filterCriteria["hotelAddress"] : string.Empty;
                string hName = filterCriteria.ContainsKey("hotelName") ? filterCriteria["hotelName"] : string.Empty;
                int hRating = filterCriteria.ContainsKey("hotelRating") ? Convert.ToInt32(filterCriteria["hotelRating"]) : 0;
                int orderBy = filterCriteria.ContainsKey("orderBy") ? Convert.ToInt32(filterCriteria["orderBy"]) : 0;
                int pageNo = filterCriteria.ContainsKey("pageNo") ? Convert.ToInt32(filterCriteria["pageNo"]) : 1;

                #endregion

                HotelSearchResult[] sRFiltered = new HotelSearchResult[searchResultCache.Length];
                if (hAddress.Length > 0 || hName.Length > 0 || hRating != 0)
                {
                    int count = 0;
                    for (int k = 0; k < searchResultCache.Length; k++)
                    {
                        HotelSearchResult hotel = new HotelSearchResult();
                        hotel = searchResultCache[k];
                        //only the hotel with rating equal to the requested one
                        if (hRating == 0 || Convert.ToInt32(hotel.Rating) == hRating)
                        {
                            if (hotel.bookingSource == HotelBookingSource.Desiya)
                            {
                                if ((hAddress.Length > 0 && hotel.HotelLocation != null && hotel.HotelLocation.Length > 0 && hAddress == hotel.HotelLocation) || (hotel.hotelName != null && hotel.hotelAddress != null && hotel.hotelName.ToUpper().Contains(hName.ToUpper()) && hotel.hotelAddress.ToUpper().Contains(hAddress.ToUpper())))
                                {
                                    sRFiltered[count++] = hotel;
                                }
                            }
                            else if (hotel.bookingSource == HotelBookingSource.GTA && hotel.hotelLocation != null && hotel.hotelLocation.Length > 0)
                            {
                                if (hotel.hotelName != null && hotel.hotelName.ToUpper().Contains(hName.ToUpper()) && hotel.hotelLocation.ToUpper().Contains(hAddress.ToUpper()))
                                {
                                    sRFiltered[count++] = hotel;
                                }
                            }
                            else if (hotel.hotelName != null && hotel.hotelAddress != null && hotel.hotelName.ToUpper().Contains(hName.ToUpper()) && hotel.hotelAddress.ToUpper().Contains(hAddress.ToUpper()))
                            {
                                sRFiltered[count++] = hotel;
                            }
                        }
                    }
                    if (sRFiltered.Length > count)
                    {
                        Array.Resize(ref sRFiltered, count);
                    }
                }
                else
                {
                    sRFiltered = searchResultCache;
                }
                //update the no of pages
                int totalFilteredRes = sRFiltered.Length;

                noOfPages = totalFilteredRes / recordsPerPage + Convert.ToInt16(totalFilteredRes % recordsPerPage > 0);

                //sorting the data by the given parameter
                switch (orderBy)
                {
                    case 0: sRFiltered = SortByPrice(sRFiltered, 0);
                        break;
                    case 1: sRFiltered = SortByPrice(sRFiltered, 1);
                        break;
                    case 2: sRFiltered = SortByName(sRFiltered, 0);
                        break;
                    case 3: sRFiltered = SortByName(sRFiltered, 1);
                        break;
                    case 4: sRFiltered = SortByRating(sRFiltered, 0);
                        break;
                    case 5: sRFiltered = SortByRating(sRFiltered, 1);
                        break;
                    case 6: sRFiltered = SortByDiscounts(sRFiltered, 0);
                        break;
                    default:
                        break;
                }
                sRFiltered = FilterByTBOConnect(sRFiltered);
                int endIndex = 0;
                int startIndex = recordsPerPage * (pageNo - 1);
                if ((startIndex + recordsPerPage) - 1 < totalFilteredRes)
                {
                    endIndex = (startIndex + recordsPerPage) - 1;
                }
                else
                {
                    endIndex = totalFilteredRes - 1;
                }
                //copy the required result for the requested page
                HotelSearchResult[] returnSearchResult = new HotelSearchResult[endIndex - startIndex + 1];
                for (int iX = 0; iX <= endIndex - startIndex; iX++)
                {
                    returnSearchResult[iX] = sRFiltered[startIndex + iX];
                }
                return returnSearchResult;
            }
            else
            {
                return noResult;
            }
        }

        /// <summary>
        /// Deserialize the ByteArray to object
        /// </summary>
        /// <param name="theByteArray"></param>
        /// <returns></returns>
        private object GetObjectWithByteArray(byte[] theByteArray)
        {
            MemoryStream ms = new MemoryStream(theByteArray);
            BinaryFormatter bf1 = new BinaryFormatter();
            ms.Position = 0;
            return bf1.Deserialize(ms);
        }

        /// <summary>
        /// Serialize the object to ByteArray
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private byte[] GetByteArrayWithObject(Object o)
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf1 = new BinaryFormatter();
            bf1.Serialize(ms, o);
            return ms.ToArray();
        }

        /// <summary>
        /// Sort the result set order by price
        /// </summary>
        /// <param name="result"></param>
        /// <param name="order">if 0 low to high,1 high to low</param>
        /// <returns></returns>
        private HotelSearchResult[] SortByPrice(HotelSearchResult[] result, int order)
        {
            //Assumption : data from actual source comes sorted on Price in Ascending order
            if (order == 1)
            {
                Array.Reverse(result);
            }
            return result;
        }
        /// <summary>
        /// This Method is used to sort the results based on discounts.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        private HotelSearchResult[] SortByDiscounts(HotelSearchResult[] result, int order)
        {
            result = SortByPrice(result, 0);
            int count = 0, k = 0;
            for (int n = 0; n < result.Length - 1; n++)
            {
                if (result[n].PromoMessage.Length > 0)
                {
                    count = n - 1;
                    k = n;
                    while (count >= 0)
                    {
                        if (result[count].PromoMessage.Length > 0)
                        {
                            break;
                        }
                        else
                        {
                            HotelSearchResult temp = result[k];
                            result[k] = result[count];
                            result[count] = temp;
                            count--;
                            k--;
                        }
                    }

                }

            }

            return result;

        }

        /// <summary>
        /// sort the result order by name
        /// </summary>
        /// <param name="result"></param>
        /// <param name="order">if 0 A-Z,1 Z-A</param>
        /// <returns></returns>
        private HotelSearchResult[] SortByName(HotelSearchResult[] result, int order)
        {
            for (int n = 0; n < result.Length - 1; n++)
            {
                for (int m = n + 1; m < result.Length; m++)
                {
                    if (result[n].hotelName.CompareTo(result[m].hotelName) > 0)
                    {
                        HotelSearchResult temp = result[n];
                        result[n] = result[m];
                        result[m] = temp;
                    }
                }
            }
            if (order == 1)
            {
                Array.Reverse(result);
            }
            return result;
        }

        /// <summary>
        /// order by star rating
        /// </summary>
        /// <param name="result"></param>
        /// <param name="order">if 0 0-5,1 5-0</param>
        /// <returns></returns>
        private HotelSearchResult[] SortByRating(HotelSearchResult[] result, int order)
        {
            for (int n = 0; n < result.Length - 1; n++)
            {
                for (int m = n + 1; m < result.Length; m++)
                {
                    if ((int)result[n].rating > (int)result[m].rating)
                    {
                        HotelSearchResult temp = result[n];
                        result[n] = result[m];
                        result[m] = temp;
                    }
                }
            }
            if (order == 1)
            {
                Array.Reverse(result);
            }
            return result;
        }
        private HotelSearchResult[] FilterByTBOConnect(HotelSearchResult[] searchResult)
        {
            HotelSearchResult[] tempRes = new HotelSearchResult[searchResult.Length];
            int j = 0;
            for (int i = 0; i < searchResult.Length; i++)
            {
                if (searchResult[i].BookingSource == HotelBookingSource.TBOConnect)
                {
                    tempRes[j++] = searchResult[i];
                }
            }
            for (int k = 0; k < searchResult.Length; k++)
            {
                if (searchResult[k].BookingSource != HotelBookingSource.TBOConnect)
                {
                    tempRes[j++] = searchResult[k];
                }
            }
            return tempRes;
        }

        #endregion

    }
}