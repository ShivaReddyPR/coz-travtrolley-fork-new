using System;
using System.Collections.Generic;
using System.Text;


namespace Technology.BookingEngine
{
    [Serializable]
    public class SightseeingRequest
    {

        public string CountryName;                      //Country Code
        public string DestinationType;                  //Destination type, can be "city" or "area"
        public string DestinationCode;                  //Destination code according to the DestinationType
        public string ItemName;                         //filter by itemName
        public string ItemCode;                         //filter by itemCode
        public DateTime TourDate;                       //Tour date
        public int NoOfAdults;                          //number of adults
        public List<int> ChildrenList;                  //children age list
        public List<string> TypeCodeList;               //filter by types
        public List<string> CategoryCodeList;           //filter by category
        public int ChildCount;
        //Not Needed for Search
        public string SpecialCode;
        public string CityName;
        public string Language; //Tour Language
    }
}
