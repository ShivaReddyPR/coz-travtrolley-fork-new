using System;
using System.Collections.Generic;
using System.Text;
using CT.TicketReceipt.DataAccessLayer;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace CT.BookingEngine
{
    public class MobileRechargeRequest
    {
        private string operatorCode;
        private string circleCode;
        private string amount;
        private string mobileNumber;
        private string denomination;
        private string transactionId;
        private string product;

        public string OperatorCode
        {
            get
            {
                return operatorCode;
            }
            set
            {
                operatorCode = value;
            }
        }
        public string CircleCode
        {
            get
            {
                return circleCode;
            }
            set
            {
                circleCode = value;
            }
        }
        public string MobileNumber
        {
            get
            {
                return mobileNumber;
            }
            set
            {
                mobileNumber = value;
            }
        }
        public string Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }
        public string TransactionId
        {
            get
            {
                return transactionId;
            }
            set
            {
                transactionId = value;
            }
        }
        public string Product
        {
            get
            {
                return product;
            }
            set
            {
                product = value;
            }
        }
        public string Denomination
        {
            get
            {
                return denomination;
            }
            set
            {
                denomination = value;
            }
        }

        public static Dictionary<string, string> GetOperators()
        {
            Trace.TraceInformation("TMobileRechargeRequest.GetOperators entered");
            Dictionary<string, string> operatorList = new Dictionary<string, string>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetMobileOperators, paramList, connection);
            while (data.Read())
            {
                operatorList.Add(data["operatorcode"].ToString(), data["operatorname"].ToString());
            }
            Trace.TraceInformation("TMobileRechargeRequest.GetOperators Exit");
            return operatorList;
        }

        public static Dictionary<string, string> GetCirclesForOperator(string operatorCode)
        {
            Trace.TraceInformation("MobileRechargeRequest.GetCirclesForOperator entered");
            Dictionary<string, string> cirList = new Dictionary<string, string>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@operatorCode", operatorCode);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetCirclesForOperator, paramList, connection);
            while (data.Read())
            {
               cirList.Add(data["circode"].ToString(), data["cirname"].ToString());
            }
            connection.Close();
            data.Close();
            Trace.TraceInformation("MobileRechargeRequest.GetCirclesForOperator Exit");
            return cirList;
        }
        public static Dictionary<string, string> GetProdcutTypes(string operatorCode,string circle)
        {
            Trace.TraceInformation("MobileRechargeRequest.GetProdcutTypes entered");
            Dictionary<string, string> productType = new Dictionary<string, string>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[2];
            paramList[0] = new SqlParameter("@operatorCode", operatorCode);
            paramList[1] = new SqlParameter("@circle", circle);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetProcductTypes, paramList, connection);
            while (data.Read())
            {
                productType.Add(data["product"].ToString(), data["productdescription"].ToString());
            }
            connection.Close();
            data.Close();
            Trace.TraceInformation("MobileRechargeRequest.GetProdcutTypes Exit");
            return productType;
        }
        public static Dictionary<string, string> GetDenomination(string operatorCode, string circle, string product)
        {
            Trace.TraceInformation("MobileRechargeRequest.GetDenomination entered");
            Dictionary<string, string> denomination = new Dictionary<string, string>();
            //SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@operatorCode", operatorCode);
            paramList[1] = new SqlParameter("@circle", circle);
            paramList[2] = new SqlParameter("@product", product);
            SqlDataReader data = DBGateway.ExecuteReaderSP(SPNames.GetDenomination, paramList);
            while (data.Read())
            {
                denomination.Add(data["denomination"].ToString(), data["commission"].ToString());
            }
            connection.Close();
            data.Close();

            Trace.TraceInformation("MobileRechargeRequest.GetDenomination exit");
            return denomination;
        }

        
        //public static bool IsValidMobileNumber(string operatorCode, string circleCode, string mobileNumber)
        //{
        //    SqlConnection connection = Dal.GetConnection();
        //    SqlParameter[] paramList = new SqlParameter[3];
        //    paramList[0] = new SqlParameter("@operatorCode", operatorCode);
        //    paramList[1] = new SqlParameter("@circleCode", circleCode);
        //    paramList[2] = new SqlParameter("@mobileNumber", mobileNumber);
        //    SqlDataReader data = Dal.ExecuteReaderSP(SPNames.IsValidMobileNumber, paramList, connection);
        //    if (data.Read())
        //    {
        //        connection.Close();
        //        data.Close();
        //        return true;
        //    }
        //    else
        //    {
        //        connection.Close();
        //        data.Close();
        //        return false;
        //    }
        //} 
           

    }
}
