using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Collections;
using Technology.Data;
using CoreLogic;
using System.Xml;

namespace Technology.BookingEngine
{
    /// <summary>
    /// This is an Enum of Request Type(For change Request Queue)
    /// </summary>
    public enum RequestType
    {
        Void = 1,
        Reissuance =2,      
        Refund = 3,
        HotelCancel=4,
        HotelAmendment=5
    }
    /// <summary>
    /// This is an Enum of Service Request Status
    /// </summary>
    public enum ServiceRequestStatus
    {
        Unassigned = 1,
        Assigned = 2,
        Acknowledged = 3,
        Completed = 4,
        Rejected = 5,
        Closed = 6,
        Pending=7
    }
    public enum RequestSource
    {
        TBO = 1,     
        WhiteLabel = 2,
        BookingAPI = 3,
         
    }
    public class ServiceRequest
    {
        #region Class private fields
        /// <summary>
        /// Unique id for a request
        /// </summary>
        private int requestId;
        /// <summary>
        /// Request type field(Of Enum Type)
        /// </summary>
        private RequestType requestType;
        /// <summary>
        /// BookingId field against a request
        /// </summary>
        private int bookingId;
        /// <summary>
        /// TicketId in case of Flight
        /// </summary>
        private int referenceId;
        /// <summary>
        /// UniqueId of product
        /// </summary>
        private ProductType productType;
        /// <summary>
        /// data field od a request
        /// </summary>
        private string data;
        /// <summary>
        /// Current status of Service Request
        /// </summary>

        private string docName=string.Empty;

        private ServiceRequestStatus serviceRequestStatusId;
        /// <summary>
        /// Created by field comtains the memberId who created the new entry
        /// </summary>
        private int createdBy;

        /// <summary>
        /// Modified by field comtains the memberId who modified the entry 
        /// </summary>
        private int lastModifiedBy;

        /// <summary>
        /// Date when entry is created
        /// </summary>
        private DateTime createdOn;

        /// <summary>
        /// Date when entry is modified
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// flag which is set whether we want to Update or Creating new
        /// </summary>
        private bool isUpdate;
        /// <summary>
        /// Id of item type
        /// </summary>
        private InvoiceItemTypeId itemTypeId;
        private bool isDomestic;
        private int source;
        private string supplierName;
        private int agencyId;
        private string paxName;
        private string pnr;
        private string referenceNumber;
        private Agencytype agencyTypeId;
        private string details;
        private int priceId;
        private DateTime startDate;
        private RequestSource requestSourceId;        
        private string creditNoteNumber = null;
        private decimal adminFee;
        private decimal supplierFee ;
        private string ticketStatus;
        private decimal publishedFare;
        private decimal tax ;
        private string comments;
        private string airlineCode;
 

        #endregion

        #region Class public properies
        /// <summary>
        /// Unique Id property for a request
        /// </summary>
        public int RequestId
        {
            get
            {
                return this.requestId;
            }
            set 
            {
                this.requestId = value;
            }
        }
        /// <summary>
        /// Request type property
        /// </summary>
        public RequestType RequestType
        {
            get 
            {
                return this.requestType;
            }
            set 
            {
                this.requestType = value;
            }
        }
        /// <summary>
        /// Bookig Id property against a booking
        /// </summary>
        public int BookingId
        {
            get 
            {
                return this.bookingId;
            }
            set 
            {
                this.bookingId = value;
            }
        }
        /// <summary>
        /// Ticket Id property against a booking
        /// </summary>
        public int ReferenceId
        {
            get
            {
                return this.referenceId;
            }
            set
            {
                this.referenceId = value;
            }
        }
        /// <summary>
        /// Unique Id of product
        /// </summary>
        public ProductType ProductType
        {
            get
            {
                return this.productType;
            }
            set
            {
                this.productType = value;
            }
        }
        /// <summary>
        /// Data property against a request
        /// </summary>
        public string Data
        {
            get 
            {
                return this.data;
            }
            set 
            {
                this.data = value;
            }
        }
        public ServiceRequestStatus ServiceRequestStatusId
        {
            get
            {
                return this.serviceRequestStatusId;
            }
            set
            {
                this.serviceRequestStatusId = value;
            }
        }

        public string DocName
        {
            get
            {
                return this.docName;
            }
            set
            {
                this.docName = value;
            }
        }

        /// <summary>
        /// Created by property comtains the memberId who created the new entry
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return this.createdBy;
            }
            set
            {
                this.createdBy = value;
            }
        }

        /// <summary>
        /// Modified by property comtains the memberId who modified the entry 
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return this.lastModifiedBy;
            }
            set
            {
                this.lastModifiedBy = value;
            }
        }

        /// <summary>
        ///  Date when entry is created
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return this.createdOn;
            }
            set
            {
                this.createdOn = value;
            }
        }

        /// <summary>
        ///  Date when entry is modified
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return this.lastModifiedOn;
            }
            set
            {
                this.lastModifiedOn = value;
            }
        }
        /// <summary>
        /// Gets or sets the Id of item type
        /// </summary>
        //public int ItemTypeId
        //{
        //    get { return itemTypeId; }
        //    set { itemTypeId = value; }
        //}
         public InvoiceItemTypeId ItemTypeId
        {
            get 
            {
                return this.itemTypeId;
            }
            set 
            {
                this.itemTypeId = value;
            }
        }
        /// <summary>
        /// Flag indicating whether this is domestic or international
        /// </summary>
        public bool IsDomestic
        {
            get
            {
                return this.isDomestic;
            }
            set
            {
                this.isDomestic = value;
            }
        }
        /// <summary>
        /// Source of the booking
        /// </summary>
        public int Source
        {
            get
            {
                return this.source;
            }
            set
            {
                this.source = value;
            }
        }
        /// <summary>
        /// Airline code in case of flights and hotel name in case of hotels
        /// </summary>
        public string SupplierName
        {
            get
            {
                return this.supplierName;
            }
            set
            {
                this.supplierName = value.Trim();
            }
        }
        /// <summary>
        /// AgncyId of the booking agency
        /// </summary>
        public int AgencyId
        {
            get
            {
                return this.agencyId;
            }
            set
            {
                this.agencyId = value;
            }
        }
        /// <summary>
        /// Name of the passenger related to this request
        /// </summary>
        public string PaxName
        {
            get
            {
                return this.paxName;
            }
            set
            {
                this.paxName = value.Trim();
            }
        }
        /// <summary>
        /// Pnr number related to a flight.
        /// </summary>
        public string Pnr
        {
            get
            {
                return this.pnr;
            }
            set
            {
                this.pnr = value.Trim();
            }
        }
        /// <summary>
        /// Ticket number in case of flights
        /// </summary>
        public string ReferenceNumber
        {
            get
            {
                return this.referenceNumber;
            }
            set
            {
                this.referenceNumber = value.Trim();
            }
        }
        /// <summary>
        /// Type of the agency making the booking
        /// </summary>
        public Agencytype AgencyTypeId
        {
            get
            {
                return this.agencyTypeId;
            }
            set
            {
                this.agencyTypeId = value;
            }
        }

        public string Details
        {
            get
            {
                return this.details;
            }
            set
            {
                this.details = value.Trim();
            }
        }
        public int PriceId
        {
            get
            {
                return this.priceId;
            }
            set
            {
                this.priceId = value;
            }
        }
        public DateTime StartDate
        {
            get
            {
                return this.startDate;
            }
            set
            {
                this.startDate = value;
            }
        }
        public RequestSource RequestSourceId
        {
            get
            {
                return this.requestSourceId;
            }
            set
            {
                this.requestSourceId = value;
            }
        }
        public decimal AdminFee
        {
            get
            {
                return this.adminFee;
            }
            set
            {
                this.adminFee = value;
            }
        }
        public decimal SupplierFee
        {
            get
            {
                return this.supplierFee;
            }
            set
            {
                this.supplierFee = value;
            }
        }
        public decimal PublishedFare
        {
            get
            {
                return this.publishedFare;
            }
            set
            {
                this.publishedFare = value;
            }
        }
        public decimal Tax
        {
            get
            {
                return this.tax;
            }
            set
            {
                this.tax = value;
            }
        }
        public string TicketStatus
        {
            get
            {
                return this.ticketStatus;
            }
            set
            {
                this.ticketStatus = value;
            }
        }
        public string AirlineCode
        {
            get
            {
                return this.airlineCode;
            }
            set
            {
                this.airlineCode = value;
            }
        }
        public string Comments
        {
            get
            {
                return this.comments;
            }
            set
            {
                this.comments = value;
            }
        }
        public string CreditNoteNumber
        {
            get {return this.creditNoteNumber ;}
            set {this.creditNoteNumber=value ;}
        }

        #endregion

        #region Class Constructors

        public ServiceRequest()
        {
            isUpdate = false;
        }
        public ServiceRequest(int requestId)
        {
            Load(requestId);
            isUpdate = true;
        }
        #endregion

        #region Class methods
        /// <summary>
        /// Method load the values for a request
        /// </summary>
        /// <param name="requestId">requestId</param>
        private void Load(int requestId)
        {
            Trace.TraceInformation("ServiceRequest.Load entered : requestId = " + requestId);
            if (requestId <= 0)
            {
                throw new ArgumentException("Request Id should be positive integer", "requestId");
            }
            this.requestId = requestId;
            using (SqlConnection connection = Dal.GetConnection())
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@requestId", requestId);
                SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetServiceRequest, paramList, connection);
                if (data.Read())
                {
                    this.RequestType = (RequestType)Enum.Parse(typeof(RequestType), data["requestTypeId"].ToString());
                    this.bookingId = Convert.ToInt32(data["bookingId"]);
                    if (data["referenceId"] != DBNull.Value)
                    {
                        this.referenceId = Convert.ToInt32(data["referenceId"]);
                    }
                    else
                    {
                        this.referenceId = 0;
                    }
                    this.productType = (ProductType)Enum.Parse(typeof(ProductType), data["productTypeId"].ToString());
                    this.data = data["data"].ToString();
                    this.serviceRequestStatusId = (ServiceRequestStatus)Enum.Parse(typeof(ServiceRequestStatus), data["serviceRequestStatusId"].ToString());
                    if (data["docName"] != DBNull.Value)
                    {
                        this.docName = Convert.ToString(data["docName"]);
                    }
                    this.createdBy = Convert.ToInt32(data["createdBy"]);
                    this.createdOn = Convert.ToDateTime(data["createdOn"]);
                    this.lastModifiedBy = Convert.ToInt32(data["lastModifiedBy"]);
                    this.lastModifiedOn = Convert.ToDateTime(data["lastModifiedOn"]);
                    // this.itemTypeId = Convert.ToInt32(data["itemTypeId"]);
                    this.itemTypeId = (InvoiceItemTypeId)Enum.Parse(typeof(InvoiceItemTypeId), data["itemTypeId"].ToString());
                    if (data["isDomestic"] != DBNull.Value)
                    {
                        this.isDomestic = Convert.ToBoolean(data["isDomestic"]);
                    }
                    if (data["source"] != DBNull.Value)
                    {
                        this.source = Convert.ToInt32(data["source"]);
                    }
                    if (data["supplierName"] != DBNull.Value)
                    {
                        this.supplierName = data["supplierName"].ToString();
                    }
                    if (data["agencyId"] != DBNull.Value)
                    {
                        this.agencyId = Convert.ToInt32(data["agencyId"]);
                    }
                    if (data["paxName"] != DBNull.Value)
                    {
                        this.paxName = Convert.ToString(data["paxName"]);
                    }
                    if (data["pnr"] != DBNull.Value)
                    {
                        this.pnr = Convert.ToString(data["pnr"]);
                    }
                    if (data["referenceNumber"] != DBNull.Value)
                    {
                        this.referenceNumber = Convert.ToString(data["referenceNumber"]);
                    }
                    if (data["agencyType"] != DBNull.Value)
                    {
                        this.agencyTypeId = (Agencytype)Enum.Parse(typeof(Agencytype), data["agencyType"].ToString());
                    }
                    if (data["details"] != DBNull.Value)
                    {
                        this.details = data["details"].ToString();
                    }
                    if (data["startDate"] != DBNull.Value)
                    {
                        this.startDate = Convert.ToDateTime(data["startDate"]);
                    }
                    if (data["priceId"] != DBNull.Value)
                    {
                        this.priceId = Convert.ToInt32(data["priceId"]);
                    }
                    if (data["requestSourceId"] != DBNull.Value)
                    {
                        this.requestSourceId = (RequestSource)Enum.Parse(typeof(RequestSource), data["requestSourceId"].ToString());
                    }
                    if (data["tax"] != DBNull.Value)
                    {
                        this.tax = Convert.ToDecimal(data["tax"]);
                    } 
                    if (data["publishedFare"] != DBNull.Value)
                    {
                        this.publishedFare = Convert.ToDecimal(data["publishedFare"]);
                    }
                    if (data["adminFee"] != DBNull.Value)
                    {
                        this.adminFee = Convert.ToDecimal(data["adminFee"]);
                    }
                    if (data["supplierFee"] != DBNull.Value)
                    {
                        this.supplierFee = Convert.ToDecimal(data["supplierFee"]);
                    }
                    if (data["ticketStatus"] != DBNull.Value)
                    {
                        this.ticketStatus = Convert.ToString(data["ticketStatus"]);
                    }
                    if (data["airlineCode"] != DBNull.Value)
                    {
                        this.airlineCode = Convert.ToString(data["airlineCode"]);
                    }
                    if (data["comments"] != DBNull.Value)
                    {
                        this.comments = Convert.ToString(data["comments"]);
                    }
                    data.Close();
                    connection.Close();
                    Trace.TraceInformation("ServiceRequest.Load exiting :" + requestId.ToString());
                }
                else
                {
                    data.Close();
                    connection.Close();
                    Trace.TraceInformation("ServiceRequest.Load exiting : requestId does not exist.requestId = " + requestId.ToString());
                    throw new ArgumentException("Request id does not exist in database");
                }
            }
            
        }
        /// <summary>
        /// Method Adds new enrty or Updates the existing entryin Service request
        /// </summary>
        public void Save()
        {
            if (isUpdate)
            {

            }
            else
            {
                Trace.TraceInformation("ServiceRequest.Save entered : ");

                #region Adds entry in Request Queue
                //SqlParameter[] paramList = new SqlParameter[8];
                SqlParameter[] paramList = new SqlParameter[29];
                paramList[0] = new SqlParameter("@requestTypeId", (int)this.requestType);
                paramList[1] = new SqlParameter("@bookingId", this.bookingId);
                paramList[2] = new SqlParameter("@data", this.data);
                paramList[3] = new SqlParameter("@createdBy", this.createdBy);
                paramList[4] = new SqlParameter("@serviceRequestStatusId", (int)this.ServiceRequestStatusId);
                paramList[5] = new SqlParameter("@referenceId", this.referenceId);
                paramList[6] = new SqlParameter("@productTypeId", (int)productType);
                paramList[7] = new SqlParameter("@requestId", this.requestId);
                paramList[7].Direction = System.Data.ParameterDirection.Output;
                paramList[8] = new SqlParameter("@itemTypeId", (int)this.itemTypeId);
                paramList[9] = new SqlParameter("@isDomestic", this.isDomestic);
                if (this.source == 0)
                {
                    paramList[10] = new SqlParameter("@source", DBNull.Value);
                }
                else
                {
                    paramList[10] = new SqlParameter("@source", this.source);
                }
                if (this.supplierName == null || this.supplierName == "")
                {
                    paramList[11] = new SqlParameter("@supplierName", DBNull.Value);
                }
                else
                {
                    paramList[11] = new SqlParameter("@supplierName", this.supplierName);
                }
                if (this.agencyId == 0)
                {
                    paramList[12] = new SqlParameter("@agencyId", DBNull.Value);
                }
                else
                {
                    paramList[12] = new SqlParameter("@agencyId", this.agencyId);
                }
                if (this.paxName == null || this.paxName == "")
                {
                    paramList[13] = new SqlParameter("@paxName", DBNull.Value);
                }
                else
                {
                    paramList[13] = new SqlParameter("@paxName", this.paxName);
                }
                if (this.pnr == null || this.pnr == "")
                {
                    paramList[14] = new SqlParameter("@pnr", DBNull.Value);
                }
                else
                {
                    paramList[14] = new SqlParameter("@pnr", this.pnr);
                }
                if (this.referenceNumber == null || this.referenceNumber == "")
                {
                    paramList[15] = new SqlParameter("@referenceNumber", DBNull.Value);
                }
                else
                {
                    paramList[15] = new SqlParameter("@referenceNumber", this.referenceNumber);
                }
                if ((int)agencyTypeId == 0)
                {
                    paramList[16] = new SqlParameter("@agencyType", DBNull.Value);
                }
                else
                {
                    paramList[16] = new SqlParameter("@agencyType", this.agencyTypeId);
                }
                if (this.details == null || this.details == "")
                {
                    paramList[17] = new SqlParameter("@details", DBNull.Value);
                }
                else
                {
                    paramList[17] = new SqlParameter("@details", this.details);
                }
                paramList[18] = new SqlParameter("@priceId", priceId);  
                if (this.startDate == null || this.startDate == new DateTime())
                {
                    paramList[19] = new SqlParameter("@startdate", DBNull.Value);
                }
                else
                {
                    paramList[19] = new SqlParameter("@startdate", this.startDate);
                }
                if ((int)this.requestSourceId ==  0)
                {
                    paramList[20] = new SqlParameter("@requestSourceId", DBNull.Value);
                }
                else
                {
                    paramList[20] = new SqlParameter("@requestSourceId", this.requestSourceId);
                }   
              

                if (this.creditNoteNumber == null)
                {
                    paramList[21] = new SqlParameter("@creditNoteNumber", DBNull.Value);
                }
                else
                {
                    paramList[21] = new SqlParameter("@creditNoteNumber",this.creditNoteNumber);
                }
                paramList[22] = new SqlParameter("@publishedFare", this.publishedFare);
                paramList[23] = new SqlParameter("@tax", this.tax);
                paramList[24] = new SqlParameter("@adminFee", this.adminFee);
                paramList[25] = new SqlParameter("@supplierFee", this.supplierFee);
                paramList[26] = new SqlParameter("@ticketStatus", this.ticketStatus);
                paramList[27] = new SqlParameter("@airlineCode", this.airlineCode);
                paramList[28] = new SqlParameter("@comments", this.comments);
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddServiceRequest, paramList);
                requestId = (int)paramList[7].Value;                
                #endregion

                #region Adds entry in Queue table
                CoreLogic.Queue queue = new CoreLogic.Queue();
                queue.ItemId = requestId;
                queue.QueueTypeId = (int)QueueType.Request;
                queue.CreatedBy = this.CreatedBy;
                queue.StatusId = (int)QueueStatus.Assigned;
                queue.AssignedDate = DateTime.Now;
                //TODO: make 7 a constant
                queue.CompletionDate = DateTime.Now.AddDays(7);
                queue.Save();
                #endregion

                Trace.TraceInformation("ServiceRequest.Save exiting rowsAffected = " + rowsAffected);
            }
        }
        /// <summary>
        /// It gets the List of all request Type from RequestType table
        /// </summary>
        /// <returns>Sorted list contains key value pair(RequestType,RequestTypeId)</returns>
        public static SortedList GetRequestTypes()
        {
            Trace.TraceInformation("ServiceRequest.GetRequestTypes Entered ");
            SortedList requestType = new SortedList();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[0];
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetRequestTypeList, paramList, connection);
            while (data.Read())
            {
                requestType.Add((String)data["requestType"], (int)data["requestTypeId"]);
            }            
            data.Close();
            connection.Close();
            requestType.TrimToSize();
            Trace.TraceInformation("ServiceRequest.GetRequestTypes Exited : number of request types = " + requestType.Count);
            return requestType;
        }
        /// <summary>
        /// Get Service Request Staus
        /// </summary>
        /// <returns></returns>
        public static SortedList GetServiceRequestStatus()
        {
            Trace.TraceInformation("ServiceRequest.GetServiceRequestStatusForAdmin Entered ");
            SortedList serviceRequestStatusforAdmin = new SortedList();
            if(Technology.Configuration.CacheData.ServiceRequestStatus!=null)
            {
                serviceRequestStatusforAdmin = Technology.Configuration.CacheData.ServiceRequestStatus;
            }
            else
            {
                try
                {
                    using (SqlConnection connection = Dal.GetConnection())
                    {
                        SqlParameter[] paramList = new SqlParameter[0];
                        SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetServiceRequestStatus, paramList, connection);
                        while (data.Read())
                        {
                            serviceRequestStatusforAdmin.Add(Convert.ToInt32(data["serviceRequestStatusId"]), Convert.ToString(data["status"]));
                        }
                        data.Close();
                        connection.Close();
                    }
                    serviceRequestStatusforAdmin.TrimToSize();
                    Technology.Configuration.CacheData.ServiceRequestStatus = serviceRequestStatusforAdmin;
                }
                catch (SqlException ex)
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.ServiceRequest, CoreLogic.Severity.High, 0, ex.Message, "");
                }
            }
            Trace.TraceInformation("ServiceRequest.GetServiceRequestStatusForAdmin Exited : number of Service Request = " + serviceRequestStatusforAdmin.Count);
            return serviceRequestStatusforAdmin;
        }

        /// <summary>
        /// Get Current Status of Service Request
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public static int GetStatusOfCurrentRequestId(int requestId)
        {
            Trace.TraceInformation("ServiceRequest.GetStatusOfCurrentRequest Entered ");            
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            int currentServiceRequestStatusId = 0;
            paramList[0] = new SqlParameter("@requestId", requestId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetStatusOfCurrentRequest, paramList, connection);
            if (data.Read() && data["serviceRequestStatusId"] != DBNull.Value)
            {
                currentServiceRequestStatusId = Convert.ToInt32(data["serviceRequestStatusId"]);               
            }            
            data.Close();
            connection.Close();
            Trace.TraceInformation("ServiceRequest.GetStatusOfCurrentRequest Exited");
            return currentServiceRequestStatusId;
        }
        /// <summary>
        /// To Update Assignto and Assign by field in ServiceRequest table 
        /// </summary>
        /// <param name="requestId"></param>
        public void UpdateServiceRequestAssignment(int requestId, int serviceRequestStatusId, int lastModifiedBy, int requestStatus,string tempCreditNoteNumber)
        {
            Trace.TraceInformation("ServiceRequest.GetStatusOfCurrentRequest Entered Request Id= " + requestId);                        
            SqlParameter[] paramList = new SqlParameter[6];
            paramList[0] = new SqlParameter("@requestId", requestId);            
            paramList[1] = new SqlParameter("@serviceRequestStatusId", serviceRequestStatusId);
            paramList[2] = new SqlParameter("@lastModifiedBy", lastModifiedBy);
            paramList[3] = new SqlParameter("@requestStatus", requestStatus);
            if (string.IsNullOrEmpty(tempCreditNoteNumber))
            {
                paramList[4] = new SqlParameter("@creditNoteNumber", DBNull.Value);
            }
            else
            {
                paramList[4] = new SqlParameter("@creditNoteNumber", tempCreditNoteNumber);
            }
            if (string.IsNullOrEmpty(comments))
            {
                paramList[5] = new SqlParameter("@comments", DBNull.Value);            
            }
            else
            {
                paramList[5] = new SqlParameter("@comments", comments);            
            }             
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateServiceRequestAssignment, paramList);
            Trace.TraceInformation("ServiceRequest.UpdateServiceRequestAssignment exiting : rowsAffected = " + rowsAffected);            
        }
        /// <summary>
        /// To change the status of service requet to complete 
        /// </summary>
        /// <param name="requestId"></param>
        public void CompleteAirServiceRequest(int requestId,  int lastModifiedBy, string tempCreditNoteNumber,string status,decimal adminFee,decimal supplierFee )
        {
            Trace.TraceInformation("ServiceRequest.CompleteAirServiceRequest Entered Request Id= " + requestId);
            SqlParameter[] paramList = new SqlParameter[7];
            paramList[0] = new SqlParameter("@requestId", requestId);
            paramList[1] = new SqlParameter("@serviceRequestStatusId", ServiceRequestStatus.Completed);
            paramList[2] = new SqlParameter("@lastModifiedBy", lastModifiedBy);            
            if (string.IsNullOrEmpty(tempCreditNoteNumber))
            {
                paramList[3] = new SqlParameter("@creditNoteNumber", DBNull.Value);
            }
            else
            {
                paramList[3] = new SqlParameter("@creditNoteNumber", tempCreditNoteNumber);
            }
            paramList[4] = new SqlParameter("@ticketStatus", ticketStatus);
            paramList[5] = new SqlParameter("@adminFee", adminFee);
            paramList[6] = new SqlParameter("@supplierFee", supplierFee);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.CompleteAirServiceRequest, paramList);
            Trace.TraceInformation("ServiceRequest.CompleteAirServiceRequest exiting : rowsAffected = " + rowsAffected);
        }
        /// <summary>
        /// To update admin fee and supplier for a particular ticket
        /// </summary>
        /// <param name="requestId"></param>
        public static void UpdateCancellationCharge(int referenceId,decimal adminFee, decimal supplierFee)
        {
            Trace.TraceInformation("ServiceRequest.UpdateCancellationCharge Entered referenceId  = " + referenceId);
            if (referenceId < 1)
            {
                throw new ArgumentException("referenceId should be a positive non negative integer");
            }
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@referenceId", referenceId);          
            paramList[1] = new SqlParameter("@adminFee", adminFee);
            paramList[2] = new SqlParameter("@supplierFee", supplierFee);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateServiceRequestCancellationCharge, paramList);
            Trace.TraceInformation("ServiceRequest.UpdateCancellationCharge exiting : rowsAffected = " + rowsAffected);
        }
        public static void UpdateFileNameInServiceRequest(int itemId, int itemTypeId, string docName)
        {
            Trace.TraceInformation("ServiceRequest.UpdateFileNameInServiceRequest Entered with itemId:" + itemId +
                ", itemTypeId:" + itemTypeId + ", fileName:" + docName);
            CoreLogic.Audit.Add(CoreLogic.EventType.ServiceRequest, CoreLogic.Severity.Low, 0,
                "ServiceRequest.UpdateFileNameInServiceRequest Entered with itemId:" + itemId + 
                ", itemTypeId:" + itemTypeId + ", fileName:" + docName, "");
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@itemId", itemId);
            paramList[1] = new SqlParameter("@itemTypeId", itemTypeId);
            paramList[2] = new SqlParameter("@docName", docName);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdateFileNameInServiceRequest, paramList);
            Trace.TraceInformation("ServiceRequest.UpdateServiceRequestAssignment exiting : rowsAffected = " + rowsAffected);
            CoreLogic.Audit.Add(CoreLogic.EventType.ServiceRequest, CoreLogic.Severity.Low, 0,
                "ServiceRequest.UpdateFileNameInServiceRequest exiting : rowsAffected = " + rowsAffected,"");
        }
        /// <summary>
        /// To Know That Service Request is assigned to which member
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public static int GetAssignedToServiceRequest(int requestId)
        {
            Trace.TraceInformation("ServiceRequest.GetAssignedToServiceRequest Entered ");
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@requestId", requestId);
            SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetAssignedToServiceRequest, paramList, connection);
            int assignedTo = 1;
            if (dataReader.Read())
            {              
                    assignedTo = Convert.ToInt32(dataReader["assignedTo"]);
            }
            dataReader.Close();
            connection.Close();
            Trace.TraceInformation("ServiceRequest.GetAssignedToServiceRequest exiting : assignedTo = " + assignedTo.ToString());
            return assignedTo;             
        }

        /// <summary>
        /// Get the serviceRequestId against TicketIds
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public static int GetServiceRequestIdByTicketId(int ticketId)
        {
            Trace.TraceInformation("ServiceRequest.GetServiceRequestIdByPriceId Entered ");
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@ticketId", ticketId);
            SqlDataReader dataReader = Dal.ExecuteReaderSP(SPNames.GetServiceRequestIdByTicketId, paramList, connection);
            int requestId = 0;
            if (dataReader.Read())
            {
                requestId = Convert.ToInt32(dataReader["requestid"]);
            }
            dataReader.Close();
            connection.Close();
            Trace.TraceInformation("ServiceRequest.GetServiceRequestIdByPriceId exiting : requestId = " + requestId.ToString());
            return requestId;
        }

        public static string GenerateFlightDetailXML(Ticket tempTicket, FlightItinerary itinerary,Agency agency)
        {
            string itineraryString = itinerary.Segments[0].Origin.AirportCode;
            foreach (FlightInfo info in itinerary.Segments)
            {
                itineraryString += "-" + info.Destination.AirportCode;
            }
            StringBuilder stBuilder = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(stBuilder);             
            writer.WriteStartElement("ChangeRequestDetail");
            writer.WriteElementString("TicketId", tempTicket.TicketId.ToString());
            writer.WriteElementString("FlightId", tempTicket.FlightId.ToString());
            writer.WriteElementString("PaxType", ((int)tempTicket.PaxType).ToString());            
            writer.WriteElementString("BookingDate", itinerary.CreatedOn.ToString());
            writer.WriteElementString("FlightNumber", itinerary.Segments[0].FlightNumber);
            writer.WriteElementString("Class",  itinerary.Segments[0].BookingClass);
            writer.WriteElementString("ItineraryString", itineraryString);
            writer.WriteElementString("AgencyName", agency.AgencyName);
            writer.WriteElementString("BookingId", itinerary.BookingId.ToString());
            writer.WriteElementString("SupplierName", Supplier.GetSupplierNameById(tempTicket.SupplierId));
            writer.WriteElementString("NonRefundable", itinerary.NonRefundable.ToString());
            writer.WriteElementString("ValidatingAirline", tempTicket.ValidatingAriline);
            writer.WriteEndElement();
            writer.Close();
            return stBuilder.ToString();
        }

        #endregion
    }

    #region ServiceRequestAssignmentLog structure for keeping Log of Service request

    public struct ServiceRequestAssignmentLog
    {
        #region Private Members
        /// <summary>
        /// unique id of ServiceRequestAssignmentLog
        /// </summary>
        private int serviceRequestAssignmentLogId;
        /// <summary>
        /// Primary Key of ServiceRequest table
        /// </summary>
        private int requestId;
        /// <summary>
        /// UniqueId of service request Status
        /// </summary>
        private int serviceRequestStatusId;
        /// <summary>
        /// memberId by which Service Request is assign
        /// </summary>
        private int assignedBy;
        /// <summary>
        /// memberId to which Service Request is assign
        /// </summary>
        private int assignedTo;
        /// <summary>
        /// memberId of that Member Change Request is last Modified.
        /// </summary>
        private int modifiedBy;
        /// <summary>
        /// Date Time of assignment 
        /// </summary>
        private DateTime assignedOn;

        #endregion
#region Public Members

        /// <summary>
        /// Gets the serviceRequestAssignmentLogId field
        /// </summary>
        public int ServiceRequestAssignmentLogId
        {
            get
            {
                return this.serviceRequestAssignmentLogId;
            }
        }
        /// <summary>
        /// Gets or Sets the RequestId field
        /// </summary>
        public int RequestId
        {
            get
            {
                return this.requestId;
            }
            set
            {
                this.requestId = value;
            }
        }
        /// <summary>
        /// Gets or Sets the ServiceRequestStatusId field
        /// </summary>
        public int ServiceRequestStatusId
        {
            get
            {
                return this.serviceRequestStatusId;
            }
            set
            {
                this.serviceRequestStatusId = value;
            }
        }
        /// <summary>
        /// Gets or Sets the AssignedBy field
        /// </summary>
        public int AssignedBy
        {
            get
            {
                return this.assignedBy;
            }
            set
            {
                this.assignedBy = value;
            }
        }
        /// <summary>
        /// Gets or Sets the AssignedTo field
        /// </summary>        
        public int AssignedTo
        {
            get
            {
                return this.assignedTo;
            }
            set
            {
                this.assignedTo = value;
            }
        }
        /// <summary>
        /// Gets or Sets the ModifiedBy field
        /// </summary>
        public int ModifiedBy
        {
            get
            {
                return this.modifiedBy;
            }
            set
            {
                this.modifiedBy = value;
            }
        }
        /// <summary>
        /// Gets or Sets the AssignedOn field
        /// </summary>
        public DateTime AssignedOn
        {
            get
            {
                return this.assignedOn;
            }
            set
            {
                this.assignedOn = value;
            }
        }


        #endregion
        

        #region Public Mehods
        /// <summary>
        /// Save Region 
        /// </summary>
        public void Save()
        {
            Trace.TraceInformation("ServiceRequestAssignmentLog.Save entered : requestId = " + requestId);            
            if (assignedBy <= 0)
            {
                throw new ArgumentException("assignedBy must have a value " + assignedBy);
            }
            if (assignedTo <= 0)
            {
                throw new ArgumentException("assignedTo must have a value " + assignedTo);
            }           
            SqlParameter[] paramList = new SqlParameter[5];
            paramList[0] = new SqlParameter("@requestId", requestId);
            paramList[1] = new SqlParameter("@serviceRequestStatusId", serviceRequestStatusId);
            paramList[2] = new SqlParameter("@assignedBy", assignedBy);
            paramList[3] = new SqlParameter("@assignedTo", assignedTo);
            paramList[4] = new SqlParameter("@modifiedBy", modifiedBy);
            int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddServiceRequestAssignmentLog, paramList);
            Trace.TraceInformation("ServiceRequestAssignmentLog.Save exiting : rowsAffected = " + rowsAffected);
        }

        #endregion
    }
    #endregion
}
