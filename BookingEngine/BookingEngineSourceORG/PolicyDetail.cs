using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using Technology.BookingEngine;
using CoreLogic;
using Technology.Data;
using Technology.Configuration;
namespace Technology.BookingEngine
{
    public class PolicyDetail
    {
        private int policyId;
        public int PolicyId
        {
            get
            {
                return policyId;
            }
            set
            {
                policyId = value;
            }
        }
        private int insuranceId;
        public int InsuranceId
        {
            get
            {
                return insuranceId;
            }
            set
            {
                insuranceId = value;
            }
        }
        private int paxId;
        public int PaxId
        {
            get
            {
                return paxId;
            }
            set
            {
                paxId = value;
            }
        }
        private string policyNumber;
        public string PolicyNumber
        {
            get
            {
                return policyNumber;
            }
            set
            {
                policyNumber = value;
            }
        }
        private string remarks;
        public string Remarks
        {
            get
            {
                return remarks;
            }
            set
            {
                remarks = value;
            }
        }
        private string status;
        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        private PriceAccounts price;
        public PriceAccounts Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
            }
        }
        private decimal displayPrice;
        public decimal DisplayPrice
        {
            get
            {
                return displayPrice;
            }
            set
            {
                displayPrice = value;
            }
        }
        public void Save()
        {
            if (policyId > 0)
            {
                SqlParameter[] paramList = new SqlParameter[8];
                paramList[0] = new SqlParameter("@insuranceId", insuranceId);
                paramList[1] = new SqlParameter("@paxId", paxId);
                if (policyNumber != null)
                {
                    paramList[2] = new SqlParameter("@policyNumber", policyNumber);
                }
                else
                {
                    paramList[2] = new SqlParameter("@policyNumber", DBNull.Value);
                }
                if (remarks != null)
                {
                    paramList[3] = new SqlParameter("@remarks", remarks);
                }
                else
                {
                    paramList[3] = new SqlParameter("@remarks", DBNull.Value);
                }
                if (status != null)
                {
                    paramList[4] = new SqlParameter("@status", status);
                }
                else
                {
                    paramList[4] = new SqlParameter("@status", DBNull.Value);
                }
                paramList[5] = new SqlParameter("@priceId", price.PriceId);
                paramList[6] = new SqlParameter("@policyId", policyId);
                paramList[7] = new SqlParameter("@displayPrice", displayPrice);
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.UpdatePolicyDetail, paramList);
            }
            else
            {
                //price = new PriceAccounts();
                price.Save();
                SqlParameter[] paramList = new SqlParameter[8];
                paramList[0] = new SqlParameter("@insuranceId", insuranceId);
                paramList[1] = new SqlParameter("@paxId", paxId);
                if (policyNumber != null)
                {
                    paramList[2] = new SqlParameter("@policyNumber", policyNumber);
                }
                else
                {
                    paramList[2] = new SqlParameter("@policyNumber", DBNull.Value);
                }
                if (remarks != null)
                {
                    paramList[3] = new SqlParameter("@remarks", remarks);
                }
                else
                {
                    paramList[3] = new SqlParameter("@remarks", DBNull.Value);
                }
                if (status != null)
                {
                    paramList[4] = new SqlParameter("@status", status);
                }
                else
                {
                    paramList[4] = new SqlParameter("@status", DBNull.Value);
                }
                paramList[5] = new SqlParameter("@priceId", price.PriceId);
                paramList[6] = new SqlParameter("@displayPrice", displayPrice);
                paramList[7] = new SqlParameter("@policyId", SqlDbType.Int);
                paramList[7].Direction = ParameterDirection.Output;
                int rowsAffected = Dal.ExecuteNonQuerySP(SPNames.AddPolicyDetail, paramList);
                policyId = (int)paramList[7].Value;
            }
        }
        public void Load(int policyId)
        {
            Trace.TraceInformation("PolicyDetail.Load entered : policyId= " + policyId);
            if (policyId <= 0)
            {
                throw new ArgumentException("policyId should be positive integer", "policyId");
            }
            PolicyDetail policy = new PolicyDetail();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@policyId", policyId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.LoadPolicyDetail, paramList, connection);
            while (data.Read())
            {
                insuranceId = Convert.ToInt32(data["insuranceId"]);
                paxId = Convert.ToInt32(data["paxId"]);
                this.policyId = Convert.ToInt32(data["policyId"]);
                if (data["policyNumber"] != DBNull.Value)
                {
                    policyNumber = Convert.ToString(data["policyNumber"]);
                }
                price = new PriceAccounts();
                //policy.Price.PriceId = Convert.ToInt32(data["priceId"]);
                price.Load(Convert.ToInt32(data["priceId"]));
                if (data["remarks"] != DBNull.Value)
                {
                    remarks = Convert.ToString(data["remarks"]);
                }
                displayPrice = Convert.ToDecimal(data["displayPrice"]);
            }
            data.Close();
            connection.Close();
        }

        public static PolicyDetail[] GetPolicyDetails(int insuranceId)
        {
            Trace.TraceInformation("PolicyDetail.GetPolicyDetails entered : insuranceId= " + insuranceId);
            if (insuranceId <= 0)
            {
                throw new ArgumentException("Insurance Id should be positive integer", "insuranceId");
            }
            List<PolicyDetail> policies = new List<PolicyDetail>();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@insuranceId", insuranceId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetPolicyDetail, paramList, connection);
            while(data.Read())
            {
                PolicyDetail policy = new PolicyDetail();
                policy.insuranceId = Convert.ToInt32(data["insuranceId"]);
                policy.PaxId = Convert.ToInt32(data["paxId"]);
                policy.PolicyId = Convert.ToInt32(data["policyId"]);
                if (data["policyNumber"] != DBNull.Value)
                {
                    policy.PolicyNumber = Convert.ToString(data["policyNumber"]);
                }
                policy.Price = new PriceAccounts();
                //policy.Price.PriceId = Convert.ToInt32(data["priceId"]);
                policy.Price.Load(Convert.ToInt32(data["priceId"]));
                if (data["remarks"] != DBNull.Value)
                {
                    policy.Remarks = Convert.ToString(data["remarks"]);
                }
                policy.displayPrice = Convert.ToDecimal(data["displayPrice"]);
                policies.Add(policy);
            }
            data.Close();
            connection.Close();
            return policies.ToArray();
        }
        public static PolicyDetail GetPolicyDetailByPaxId(int paxId)
        {
            Trace.TraceInformation("PolicyDetail.GetPolicyDetails entered : insuranceId= " + paxId);
            if (paxId <= 0)
            {
                throw new ArgumentException("Pax Id should be positive integer", "paxId");
            }
            PolicyDetail policy = new PolicyDetail();
            SqlConnection connection = Dal.GetConnection();
            SqlParameter[] paramList = new SqlParameter[1];
            paramList[0] = new SqlParameter("@paxId", paxId);
            SqlDataReader data = Dal.ExecuteReaderSP(SPNames.GetPolicyDetailByPaxId, paramList, connection);
            while (data.Read())
            {
                policy.insuranceId = Convert.ToInt32(data["insuranceId"]);
                policy.PaxId = Convert.ToInt32(data["paxId"]);
                policy.PolicyId = Convert.ToInt32(data["policyId"]);
                if (data["policyNumber"] != DBNull.Value)
                {
                    policy.PolicyNumber = Convert.ToString(data["policyNumber"]);
                }
                policy.Price = new PriceAccounts();
                //policy.Price.PriceId = Convert.ToInt32(data["priceId"]);
                policy.Price.Load(Convert.ToInt32(data["priceId"]));
                if (data["remarks"] != DBNull.Value)
                {
                    policy.Remarks = Convert.ToString(data["remarks"]);
                }
                policy.displayPrice = Convert.ToDecimal(data["displayPrice"]);
            }
            data.Close();
            connection.Close();
            return policy;
        }
        public void GetPrice()
        {
            InsuranceSource insuranceSource = new InsuranceSource();
            insuranceSource.Load(1);
            price = new PriceAccounts();
            price.NetFare = insuranceSource.Price;
            price.Markup = insuranceSource.OurCommission;
            price.AccPriceType = PriceType.NetFare;
            displayPrice = insuranceSource.DisplayPrice;
        }
    }
}
