﻿using System;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;

namespace TBOAir
{
    class ChangeRequestStatus
    {
        long cancelId;
        long ticketId;
        string status;
        decimal cancelAmount;
        string remarks;
        int createdBy;
        long bookingId;

        public long CancelId
        {
            set { cancelId = value; }
            get { return cancelId; }
        }
        public long TicketId
        {
            set { ticketId = value; }
            get { return ticketId; }
        }
        public string Status
        {
            set { status = value; }
            get { return status; }
        }
        public decimal CancelAmount
        {
            set { cancelAmount = value; }
            get { return cancelAmount; }
        }
        public string Remarks
        {
            set { remarks = value; }
            get { return remarks; }
        }
        public int CreatedBy
        {
            set { createdBy = value; }
            get { return createdBy; }
        }
        public long BookingId
        {
            set { bookingId = value; }
            get { return bookingId; }
        }
        public void Save()
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[7];
                paramList[0] = new SqlParameter("@P_changeRequestId", cancelId);
                paramList[1] = new SqlParameter("@P_ticketId", ticketId);
                paramList[2] = new SqlParameter("@P_cancelStatus", status);
                if(cancelAmount > 0) paramList[3] = new SqlParameter("@P_cancelAmount", cancelAmount);
                if(!string.IsNullOrEmpty(remarks)) paramList[4] = new SqlParameter("@P_remarks", remarks);
                paramList[5] = new SqlParameter("@P_createdBy", 1);
                paramList[6] = new SqlParameter("@P_BookingId", bookingId);
                DBGateway.ExecuteNonQuerySP("usp_FLIGHT_CANCELSTATUS_ADDUPDATE", paramList);
            }
            catch { throw; }
        }
        public static DataTable LoadCancelIds(long bookingId)
        {
            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@P_bookingId", bookingId);
                return DBGateway.FillDataTableSP("usp_GetCancelIds", paramList);
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
    }
}
