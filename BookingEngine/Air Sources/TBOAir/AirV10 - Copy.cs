﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CT.Configuration;
using System.IO;
using System.Net;
using System.IO.Compression;
using Newtonsoft.Json;
using CT.BookingEngine;
using CT.Core;
using System.Xml;
using System.Data;
using System.Threading;


namespace TBOAir
{
    public class AirV10
    {
        
        protected string XmlPath = string.Empty;
        string loginName = string.Empty;
        string password = string.Empty;
        string clientId = string.Empty;
        string authenticateUrl = string.Empty;
        string logoutUrl = string.Empty;
        string getAgencyBalanceUrl = string.Empty;
        string searchUrl = string.Empty;
        string priceRBDUrl = string.Empty;
        string fareRuleUrl = string.Empty;
        string fareQuoteUrl = string.Empty;
        string ssrUrl = string.Empty;
        string bookUrl = string.Empty;
        string ticketUrl = string.Empty;
        string changeRequestUrl = string.Empty;
        string getBookingDetailUrl = string.Empty;
        string releasePNRUrl = string.Empty;
        string getChangeRequestStatusUrl = string.Empty;
        int Twoway = 0;
        string agentBaseCurrency;
        Dictionary<string, decimal> exchangeRates;
        decimal rateOfExchange = 1;
        int decimalValue = 3;
        bool testMode = true;
        string sessionId;
        int appUserId;
        string endUserIp = "183.82.111.104";
        DataTable dtBaggage = new DataTable();
        static string BaggageType = string.Empty;

        List<SearchResult> CombineResults = new List<SearchResult>();
        static SearchRequest Request = new SearchRequest();
        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        AuthenticationResponse loginResponse = null;
        /// <summary>

        /// </summary>
        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }

        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }

        public string LoginName
        {
            get { return loginName; }
            set { loginName = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string AgentBaseCurrency
        {
            get { return agentBaseCurrency; }
            set { agentBaseCurrency = value; }
        }

        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public int AgentDecimalValue
        {
            get { return decimalValue; }
            set { decimalValue = value; }
        }

        public string ClientId
        {
            get { return clientId; }
            set { clientId = value; }
        }

        public AirV10()
        {
            try
            {
                loginName = ConfigurationSystem.TBOAirConfig["UserName"];
                password = ConfigurationSystem.TBOAirConfig["Password"];
                XmlPath = ConfigurationSystem.TBOAirConfig["XmlLogPath"];
                if (ConfigurationSystem.TBOAirConfig.ContainsKey("ClientId"))
                {
                    clientId = ConfigurationSystem.TBOAirConfig["ClientId"];
                }
                else
                {
                    //ApiIntegration for test
                    //"tboprod" for Live
                    clientId = "ApiIntegration";
                }
                if (!Directory.Exists(XmlPath))
                {
                    Directory.CreateDirectory(XmlPath);
                }
                authenticateUrl = ConfigurationSystem.TBOAirConfig["Authenticate"];
                logoutUrl = ConfigurationSystem.TBOAirConfig["Logout"];
                getAgencyBalanceUrl = ConfigurationSystem.TBOAirConfig["GetAgencyBalance"];
                searchUrl = ConfigurationSystem.TBOAirConfig["Search"];
                priceRBDUrl = ConfigurationSystem.TBOAirConfig["PriceRBD"];
                fareRuleUrl = ConfigurationSystem.TBOAirConfig["FareRule"];
                fareQuoteUrl = ConfigurationSystem.TBOAirConfig["FareQuote"];
                ssrUrl = ConfigurationSystem.TBOAirConfig["SSR"];
                bookUrl = ConfigurationSystem.TBOAirConfig["Book"];
                ticketUrl = ConfigurationSystem.TBOAirConfig["Ticket"];
                changeRequestUrl = ConfigurationSystem.TBOAirConfig["SendChangeRequest"];
                getBookingDetailUrl = ConfigurationSystem.TBOAirConfig["GetBookingDetails"];
                releasePNRUrl = ConfigurationSystem.TBOAirConfig["ReleasePNR"];
                getChangeRequestStatusUrl = ConfigurationSystem.TBOAirConfig["GetChangeRequestStatus"];

                if (dtBaggage.Columns.Count == 0)
                {
                    dtBaggage.Columns.Add("WayType", typeof(string));
                    dtBaggage.Columns.Add("Group", typeof(int));
                    dtBaggage.Columns.Add("Code", typeof(string));
                    dtBaggage.Columns.Add("Description", typeof(string));
                    dtBaggage.Columns.Add("Weight", typeof(int));
                    dtBaggage.Columns.Add("Price", typeof(decimal));
                    dtBaggage.Columns.Add("SupplierPrice", typeof(decimal));
                    dtBaggage.Columns.Add("Currency", typeof(string));
                    dtBaggage.Columns.Add("Origin", typeof(string));
                    dtBaggage.Columns.Add("Destination", typeof(string));
                }
                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir) Failed to read config xml. Error " + ex.ToString (), "");
            }
        }


        private string GetResponse(string requestData, string url)
        {
            string responseFromServer = string.Empty;
            string responseXML = string.Empty;
            try
            {
                string contentType = "application/json";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST"; //Using POST method       
                string postData = requestData;// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();
            }
            catch (WebException webEx)
            {
                //get the response stream
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                String responseMessage = new StreamReader(stream).ReadToEnd();
            }
            return responseFromServer;
        }

        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        } 

        private AuthenticationResponse Authenticate()
        {
            AuthenticationResponse authResponse = new AuthenticationResponse();

            try
            {
                StringBuilder requestData = new StringBuilder();
                requestData.AppendLine("{");
                requestData.AppendLine("\"ClientId\": \"" + clientId + "\",");
                requestData.AppendLine("\"UserName\": \"" + loginName + "\",");
                requestData.AppendLine("\"Password\": \"" + password + "\",");
                requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                requestData.AppendLine("}");

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.ObjectCreationHandling = ObjectCreationHandling.Auto;
                settings.StringEscapeHandling = StringEscapeHandling.Default;
                settings.Formatting = Newtonsoft.Json.Formatting.Indented;

                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOAirAuthenticateRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePath);
                        sw.Write(requestData.ToString());
                        sw.Close();
                        
                        //XmlDocument doc = JsonConvert.DeserializeXmlNode(requestData.ToString(), "Authenticate", true);
                        //filePath = XmlPath + "TBOAirAuthenticateRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        //doc.Save(filePath);
                        TextReader reader = new StringReader(requestData.ToString());
                        AuthenticationRequest authRequest = (AuthenticationRequest)JsonSerializer.Create(settings).Deserialize(reader, typeof(AuthenticationRequest));
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AuthenticationRequest));
                        filePath = XmlPath + "TBOAirAuthenticationRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        sw = new StreamWriter(filePath);
                        ser.Serialize(sw, authRequest);
                        sw.Close();
                    }
                }
                catch (Exception ex)
                {
                    //Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Authenticate Request. Error" + ex.ToString(), "");
                }

                string response = string.Empty;
                
                
                response = GetResponse(requestData.ToString(), authenticateUrl);


                //authResponse.Error.ErrorMessage = "";
                //authResponse.Member.AgencyId = 594;
                //authResponse.Member.MemberId = 530;
                //authResponse.TokenId = "ed8bb691-fee9-473f-a0df-f9355c701b86";
                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        string filePath = XmlPath + "TBOAirAuthenticateResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                        StreamWriter sw = new StreamWriter(filePath);
                        sw.Write(response);
                        sw.Close();

                        TextReader reader = new StringReader(response);
                        authResponse = (AuthenticationResponse)JsonSerializer.Create(settings).Deserialize(reader, typeof(AuthenticationResponse));
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AuthenticationResponse));
                        filePath = XmlPath + "TBOAirAuthenticationResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        sw = new StreamWriter(filePath);
                        ser.Serialize(sw, authResponse);
                        sw.Close();                        
                    }
                }
                catch (Exception ex)
                {
                    //Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Authenticate Response. Error " + ex.ToString(), "");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate. Reason : " + ex.ToString(), "");
            }
            return authResponse;
        }

        private AgencyBalance GetAgencyBalance(AuthenticationResponse ar)
        {
            AgencyBalance balance = new AgencyBalance();

            try
            {
                if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                {
                    StringBuilder requestData = new StringBuilder();
                    requestData.AppendLine("{");
                    requestData.AppendLine("\"ClientId\": \"" + clientId + "\",");
                    requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                    requestData.AppendLine("\"TokenAgencyId\": " + ar.Member.AgencyId + ",");
                    requestData.AppendLine("\"TokenMemberId\": " + ar.Member.MemberId + ",");
                    requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                    requestData.AppendLine("}");

                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirGetAgencyBalanceRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(requestData.ToString());
                            sw.Close();

                            XmlDocument doc = JsonConvert.DeserializeXmlNode(requestData.ToString());
                            filePath = XmlPath + "TBOAirGetAgencyBalanceRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        // Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetAgencyBalance Request. Error" + ex.ToString(), "");
                    }

                    string response = GetResponse(requestData.ToString(), getAgencyBalanceUrl);

                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            JsonSerializerSettings settings = new JsonSerializerSettings();
                            settings.ObjectCreationHandling = ObjectCreationHandling.Auto;
                            settings.StringEscapeHandling = StringEscapeHandling.Default;
                            settings.Formatting = Newtonsoft.Json.Formatting.Indented;

                            string filePath = XmlPath + "TBOAirGetAgencyBalanceResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(response);
                            sw.Close();

                            TextReader reader = new StringReader(response);
                            balance = (AgencyBalance)JsonSerializer.Create(settings).Deserialize(reader, typeof(AgencyBalance));

                            if (balance != null)
                            {
                                Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Balance remaining " + balance.CashBalance, "");
                            }

                            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AgencyBalance));
                            filePath = XmlPath + "TBOAirGetAgencyBalanceResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            sw = new StreamWriter(filePath);
                            ser.Serialize(sw, balance);
                            sw.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        //Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetAgencyBalance Response. Error" + ex.ToString(), "");
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Get Agency Balance. Error : " + ex.ToString(), "");
            }

            return balance;
        }

        /// <summary>
        /// Get search results.Merging of two search type results. if the journey type is return.
        /// 1.Normal Return . Getting normal return results and combining GDS to GDS and LCC to LCC
        /// 2.Special Return . Combining results with  same airline.
        /// 3.GDS Special Return. Getting results as a combinations.
        /// For one way journey showing results with display fares.
        /// Type 
        /// 1 - OneWay 2 - Return 3 - Multi Stop 4 - AdvanceSearch 5 - Special Return
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchResult[] Search(SearchRequest request)
        {
            Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Search Started" + DateTime.Now, "");

            if (request.Type == SearchType.Return)
            {
                Dictionary<string, int> readySources = new Dictionary<string, int>();
                Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();
                Request = request;

                
                //listOfThreads.Add("TBO1", new WaitCallback(SearchNormalReturn)); // Normal return results Combination Commented for Time being
                if (request.RestrictAirline = true  && request.Segments[0].PreferredAirlines.Length > 0) //If preferred airline is mentioned then hit for only gds
                {
                    listOfThreads.Add("TBO3", new WaitCallback(SearchSpecialReturnGDS)); // GDS special return 
                    eventFlag = new AutoResetEvent[1];
                    readySources.Add("TBO3", 1200);
                }
                else// No preferred airline mentioned.
                {
                    listOfThreads.Add("TBO3", new WaitCallback(SearchSpecialReturnGDS));
                    listOfThreads.Add("TBO2", new WaitCallback(SearchSpecialReturnLCC)); // Special return with LCC. Here combining only same airline
                    eventFlag = new AutoResetEvent[2];                                      
                    readySources.Add("TBO2", 1200);
                    readySources.Add("TBO3", 1200);
                }
                
               

                int j = 0;
                //Start each fare search within a Thread which will automatically terminate after the results are received.
                foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                {
                    if (readySources.ContainsKey(deThread.Key))
                    {
                        ThreadPool.QueueUserWorkItem(deThread.Value, j);
                        eventFlag[j] = new AutoResetEvent(false);
                        j++;
                    }
                }

                if (j != 0)
                {
                    if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 0, 5000), true))
                    {
                        //TODO: audit which thread is timed out                
                    }
                }
            }
            else
            {

                List<SearchResult> results = new List<SearchResult>();
                try
                {
                    AuthenticationResponse ar = Authenticate();
                    if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                    {
                        //AgencyBalance balance = GetAgencyBalance(ar);

                        #region JSON Search Request
                        int type = 2;//Return
                        switch (request.Type)
                        {
                            case SearchType.MultiWay:
                                type = 3;
                                break;
                            case SearchType.OneWay:
                                type = 1;
                                break;
                            case SearchType.Return:
                                type = 2;
                                break;
                        }


                        StringBuilder requestData = new StringBuilder();
                        requestData.AppendLine("{");
                        requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        requestData.AppendLine("\"AdultCount\": \"" + request.AdultCount + "\",");
                        requestData.AppendLine("\"ChildCount\": \"" + request.ChildCount + "\",");
                        requestData.AppendLine("\"InfantCount\": \"" + request.InfantCount + "\",");
                        requestData.AppendLine("\"DirectFlight\": \"false\",");
                        requestData.AppendLine("\"OneStopFlight\": \"false\",");
                        requestData.AppendLine("\"JourneyType\": \"" + type + "\",");
                        if (request.Segments[0].PreferredAirlines.Length == 0)
                        {
                            requestData.AppendLine("\"PreferredAirlines\": null,");
                        }
                        else
                        {
                            requestData.AppendLine("\"PreferredAirlines\": [");
                            //requestData.AppendLine("{");
                            string pa = string.Empty;
                            foreach (string airline in request.Segments[0].PreferredAirlines)
                            {
                                if (pa.Length > 0)
                                {
                                    pa += "," + "\"" + airline + "\"";
                                }
                                else
                                {
                                    pa = "\"" + airline + "\"";
                                }
                            }
                            requestData.AppendLine(pa);
                            //requestData.AppendLine("}");
                            requestData.AppendLine("],");
                        }
                        requestData.AppendLine("\"Segments\": [");

                        foreach (FlightSegment segment in request.Segments)
                        {
                            if (segment.Origin != null)
                            {
                                requestData.AppendLine("{");
                                requestData.AppendLine("\"Origin\": \"" + segment.Origin + "\",");
                                requestData.AppendLine("\"Destination\": \"" + segment.Destination + "\",");
                                requestData.AppendLine("\"FlightCabinClass\": \"" + (int)segment.flightCabinClass + "\",");
                                requestData.AppendLine("\"PreferredDepartureTime\": \"" + segment.PreferredDepartureTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\",");
                                requestData.AppendLine("\"PreferredArrivalTime\": \"" + segment.PreferredArrivalTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\",");
                                requestData.AppendLine("}");
                            }
                        }
                        requestData.AppendLine("],");
                        //requestData.AppendLine("\"Sources\": [");
                        //Sources are optional for Normal one-way, so all airlines will be returned
                        //if we do not specify any sources
                        if (type < 5)  // Normal one way, International one way ,round trip
                        {
                            //requestData.AppendLine("\"6E\",");
                            //requestData.AppendLine("\"G8\",");
                            //requestData.AppendLine("\"SG\",");
                            //requestData.AppendLine("\"G9\",");
                            //requestData.AppendLine("\"GDS\"");
                        }
                        else
                        {
                            //requestData.AppendLine("\"SG\",");
                            //requestData.AppendLine("\"6E\",");
                            //requestData.AppendLine("\"G8\",");
                            //requestData.AppendLine("\"GDS\"");

                        }
                        //If preferred airline is mentioned and restrict airline is true then hit for only gds
                        if (request.RestrictAirline = true && request.Segments[0].PreferredAirlines.Length > 0) 
                        {
                            requestData.AppendLine("\"Sources\": [");
                            requestData.AppendLine("\"GDS\"");
                            requestData.AppendLine("]");
                        }
                        //requestData.AppendLine("]");
                        requestData.AppendLine("}");
                        #endregion

                        //Response resp = null;
                        XmlDocument doc = null;
                        try
                        {
                            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                            {
                                string filePath = XmlPath + "TBOAirSearchRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(requestData.ToString());
                                sw.Close();

                                doc = JsonConvert.DeserializeXmlNode(requestData.ToString(), "Request");
                                filePath = XmlPath + "TBOAirSearchRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                doc.Save(filePath);
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Search Request. Error" + ex.ToString(), "");
                        }

                        doc = null;

                        string response = GetResponse(requestData.ToString(), searchUrl);

                        try
                        {
                            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                            {
                                string filePath = XmlPath + "TBOAirSearchResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(response);
                                sw.Close();

                                TextReader reader = new StringReader(response);
                                doc = JsonConvert.DeserializeXmlNode(response);

                                filePath = XmlPath + "TBOAirSearchResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                doc.Save(filePath);
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Search Response.Error" + ex.ToString(), "");
                        }

                        if (doc != null)
                        {
                            XmlNode Error = doc.SelectSingleNode("Response/Error");
                            string errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                            if (errorMessage.Length <= 0)
                            {
                                XmlNodeList ResultNodes = doc.SelectNodes("Response/Results/Results");

                                if ((request.IsDomestic && type == 2))
                                {
                                    List<XmlNode> outboundResults = new List<XmlNode>();
                                    List<XmlNode> inboundResults = new List<XmlNode>();

                                    foreach (XmlNode result in ResultNodes)
                                    {
                                        if (result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Origin/Airport/AirportCode").InnerText == request.Segments[0].Origin)
                                        {
                                            outboundResults.Add(result);
                                        }
                                        if (request.Segments.Length > 1)
                                        {
                                            if (result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Origin/Airport/AirportCode").InnerText == request.Segments[1].Origin)
                                            {
                                                inboundResults.Add(result);
                                            }
                                        }
                                    }
                                    if (exchangeRates != null && exchangeRates.Count > 0)
                                    {
                                        rateOfExchange = exchangeRates[outboundResults[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];
                                    }
                                    else
                                    {
                                        rateOfExchange = 1;
                                    }

                                    #region Domestic Results

                                    foreach (XmlNode obResult in outboundResults)
                                    {
                                        foreach (XmlNode ibResult in inboundResults)
                                        {
                                            Airline airlineOut = new Airline();
                                            airlineOut.Load(obResult.SelectSingleNode("ValidatingAirline").InnerText);

                                            Airline airlineIn = new Airline();
                                            airlineIn.Load(ibResult.SelectSingleNode("ValidatingAirline").InnerText);

                                            if (airlineOut.TBOAirAllowed && airlineIn.TBOAirAllowed)
                                            {
                                                SearchResult sr = new SearchResult();

                                                sr.Currency = agentBaseCurrency;
                                                if (obResult.SelectSingleNode("BaggageAllowance") != null)
                                                {
                                                    sr.BaggageIncludedInFare = obResult.SelectSingleNode("BaggageAllowance").InnerText;
                                                }
                                                sr.FareType = "OneWay LCC";
                                                sr.ResultBookingSource = BookingSource.TBOAir;
                                                //=====================================================Outbound begin===============================================//
                                                #region Outbound Result
                                                sr.GUID = obResult.SelectSingleNode("ResultIndex").InnerText;
                                                sr.JourneySellKey = obResult.SelectSingleNode("Source").InnerText;
                                                XmlNodeList fareBreakDownNodes = obResult.SelectNodes("FareBreakdown");
                                                List<Fare> fbdList = new List<Fare>();
                                                List<Fare> tbofbdList = new List<Fare>();
                                                foreach (XmlNode fbdNode in fareBreakDownNodes)
                                                {
                                                    Fare fbd = new Fare();
                                                    fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                    fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                    fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                    fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                    fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                    fbd.SellingFare = fbd.TotalFare;
                                                    fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);
                                                    fbdList.Add(fbd);

                                                    Fare tbofbd = new Fare();
                                                    tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                    tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                    tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                    tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                    tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                    tbofbdList.Add(tbofbd);

                                                }
                                                sr.FareBreakdown = fbdList.ToArray();
                                                sr.TBOFareBreakdown = tbofbdList.ToArray();

                                                XmlNodeList fareRuleList = obResult.SelectNodes("FareRules");
                                                sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                                foreach (XmlNode fare in fareRuleList)
                                                {
                                                    CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                    fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                    fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                    fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                    fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                    fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                    fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                    sr.FareRules.Add(fr);
                                                }

                                                if (request.Type == SearchType.Return)
                                                {
                                                    sr.Flights = new FlightInfo[2][];
                                                }
                                                else
                                                {
                                                    sr.Flights = new FlightInfo[1][];
                                                }

                                                XmlNodeList segmentNodes = obResult.SelectNodes("Segments");



                                                sr.IsLCC = Convert.ToBoolean(obResult.SelectSingleNode("IsLCC").InnerText);
                                                sr.NonRefundable = (Convert.ToBoolean(obResult.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                                sr.ResultBookingSource = BookingSource.TBOAir;
                                                sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                                sr.ValidatingAirline = obResult.SelectSingleNode("ValidatingAirline").InnerText;

                                                BindSegments(segmentNodes, ref sr, obResult, null);

                                                int outbound = 0, inbound = 0;
                                                for (int i = 0; i < sr.Flights.Length; i++)
                                                {
                                                    for (int j = 0; j < sr.Flights[i].Length; j++)
                                                    {
                                                        if (sr.Flights[i][j].Group == 0)
                                                        {
                                                            outbound++;
                                                        }
                                                        else
                                                        {
                                                            inbound++;
                                                        }
                                                    }
                                                }

                                                for (int i = 0; i < sr.Flights.Length; i++)
                                                {
                                                    for (int j = 0; j < sr.Flights[i].Length; j++)
                                                    {
                                                        if (sr.Flights[i][j].Group == 0)
                                                        {
                                                            sr.Flights[i][j].Stops = outbound - 1;
                                                        }
                                                        else
                                                        {
                                                            sr.Flights[i][j].Stops = inbound - 1;
                                                        }
                                                    }
                                                }

                                                sr.Price = new PriceAccounts();
                                                sr.Price.AccPriceType = PriceType.PublishedFare;
                                                sr.Price.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                sr.Price.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                sr.Price.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                sr.Price.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                                sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                                XmlNodeList chargeNodes = obResult.SelectNodes("Fare/ChargeBU");
                                                foreach (XmlNode cb in chargeNodes)
                                                {
                                                    ChargeBreakUp cbu = new ChargeBreakUp();
                                                    cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                    switch (cb.SelectSingleNode("key").InnerText)
                                                    {
                                                        case "TBOMARKUP":
                                                            cbu.ChargeType = ChargeType.TBOMarkup;
                                                            break;
                                                        case "OTHERCHARGES":
                                                            cbu.ChargeType = ChargeType.OtherCharges;
                                                            break;
                                                        case "CONVENIENCECHARGES":
                                                            cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                            break;
                                                    }
                                                }
                                                sr.Price.Currency = agentBaseCurrency;
                                                sr.Price.CurrencyCode = agentBaseCurrency;
                                                sr.Price.DecimalPoint = decimalValue;
                                                sr.Price.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                                sr.Price.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                                sr.Price.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                sr.Price.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                                sr.Price.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                                sr.Price.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                                sr.Price.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                                sr.Price.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                sr.Price.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                sr.Price.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                {
                                                    sr.Price.TransactionFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                }
                                                else
                                                {
                                                    sr.Price.TransactionFee = 0.0M;
                                                }

                                                sr.Price.SupplierPrice = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                sr.Price.SupplierCurrency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;


                                                sr.TBOPrice = new PriceAccounts();
                                                sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                                sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                sr.TBOPrice.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                sr.TBOPrice.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                sr.TBOPrice.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                                sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                                XmlNodeList chargeNodes2 = ibResult.SelectNodes("Fare/ChargeBU");
                                                foreach (XmlNode cb in chargeNodes2)
                                                {
                                                    ChargeBreakUp cbu = new ChargeBreakUp();
                                                    cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                    switch (cb.SelectSingleNode("key").InnerText)
                                                    {
                                                        case "TBOMARKUP":
                                                            cbu.ChargeType = ChargeType.TBOMarkup;
                                                            break;
                                                        case "OTHERCHARGES":
                                                            cbu.ChargeType = ChargeType.OtherCharges;
                                                            break;
                                                        case "CONVENIENCECHARGES":
                                                            cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                            break;
                                                    }
                                                }
                                                sr.TBOPrice.Currency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                sr.TBOPrice.CurrencyCode = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                sr.TBOPrice.DecimalPoint = decimalValue;
                                                sr.TBOPrice.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                                sr.TBOPrice.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                                sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                sr.TBOPrice.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                                sr.TBOPrice.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                                sr.TBOPrice.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                sr.TBOPrice.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                sr.TBOPrice.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                sr.TBOPrice.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                sr.TBOPrice.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                {
                                                    sr.TBOPrice.TransactionFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                }

                                                sr.BaseFare = (double)sr.Price.BaseFare;
                                                sr.Tax = (double)(Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;
                                                #endregion
                                                //======================================================Outbound end================================================//

                                                //=====================================================Inbound Begin================================================//
                                                #region Inbound Result

                                                if (ibResult.SelectSingleNode("BaggageAllowance") != null)
                                                {
                                                    sr.BaggageIncludedInFare += "," + ibResult.SelectSingleNode("BaggageAllowance").InnerText;
                                                }
                                                XmlNodeList fareBreakDownNodes20 = ibResult.SelectNodes("FareBreakdown");
                                                sr.GUID += "," + ibResult.SelectSingleNode("ResultIndex").InnerText;
                                                sr.JourneySellKey += "|" + ibResult.SelectSingleNode("Source").InnerText;
                                                foreach (XmlNode fbdNode in fareBreakDownNodes20)
                                                {
                                                    foreach (Fare fbd in sr.FareBreakdown)
                                                    {
                                                        if (fbd.PassengerType == (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText))
                                                        {
                                                            fbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                            fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                            fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                            fbd.TotalFare += fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                            fbd.SellingFare += fbd.TotalFare;
                                                            fbd.SupplierFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);
                                                            break;
                                                        }
                                                    }

                                                    foreach (Fare tbofbd in sr.TBOFareBreakdown)
                                                    {
                                                        if (tbofbd.PassengerType == (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText))
                                                        {
                                                            tbofbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                            tbofbd.BaseFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                            tbofbd.PassengerCount += Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                            tbofbd.TotalFare += tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                            break;
                                                        }
                                                    }
                                                }

                                                XmlNodeList fareRuleList20 = ibResult.SelectNodes("FareRules");

                                                foreach (XmlNode fare in fareRuleList20)
                                                {
                                                    CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                    fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                    fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                    fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                    fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                    fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                    fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                    sr.FareRules.Add(fr);
                                                }

                                                XmlNodeList segmentNodes20 = ibResult.SelectNodes("Segments");

                                                BindSegments(segmentNodes20, ref sr, null, ibResult);

                                                sr.IsLCC = Convert.ToBoolean(ibResult.SelectSingleNode("IsLCC").InnerText);
                                                sr.NonRefundable = (Convert.ToBoolean(ibResult.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                                sr.ResultBookingSource = BookingSource.TBOAir;
                                                sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                                sr.ValidatingAirline = ibResult.SelectSingleNode("ValidatingAirline").InnerText;

                                                sr.Price.AdditionalTxnFee += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                sr.Price.AgentCommission += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                sr.Price.AgentPLB += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                sr.Price.BaseFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;

                                                XmlNodeList chargeNodes20 = ibResult.SelectNodes("Fare/ChargeBU");
                                                foreach (XmlNode cb in chargeNodes20)
                                                {
                                                    foreach (ChargeBreakUp cbu in sr.Price.ChargeBU)
                                                    {

                                                        switch (cb.SelectSingleNode("key").InnerText)
                                                        {
                                                            case "TBOMARKUP":
                                                                if (cbu.ChargeType == ChargeType.TBOMarkup)
                                                                {
                                                                    cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                                }
                                                                break;
                                                            case "OTHERCHARGES":
                                                                if (cbu.ChargeType == ChargeType.OtherCharges)
                                                                {
                                                                    cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                                }
                                                                break;
                                                            case "CONVENIENCECHARGES":
                                                                if (cbu.ChargeType == ChargeType.ConvenienceCharge)
                                                                {
                                                                    cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                                }
                                                                break;
                                                        }
                                                    }
                                                }
                                                sr.Price.Currency = agentBaseCurrency;
                                                sr.Price.CurrencyCode = agentBaseCurrency;
                                                sr.Price.DecimalPoint = decimalValue;
                                                sr.Price.Tax += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                                sr.Price.YQTax += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                                sr.Price.IncentiveEarned += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                sr.Price.NetFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                                sr.Price.AgentCommission += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                sr.Price.PublishedFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                                sr.Price.SServiceFee += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                                sr.Price.OtherCharges += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                                sr.Price.TdsCommission += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                sr.Price.TDSPLB += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                sr.Price.TDSIncentive += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                if (ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                {
                                                    sr.Price.TransactionFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                }
                                                else
                                                {
                                                    sr.Price.TransactionFee = 0.0M;
                                                }
                                                sr.Price.SupplierPrice += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                sr.Price.SupplierCurrency = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;

                                                sr.TBOPrice = new PriceAccounts();
                                                sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                                sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                sr.TBOPrice.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                sr.TBOPrice.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                sr.TBOPrice.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                                sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                                XmlNodeList chargeNodes3 = ibResult.SelectNodes("Fare/ChargeBU");
                                                foreach (XmlNode cb in chargeNodes3)
                                                {
                                                    ChargeBreakUp cbu = new ChargeBreakUp();
                                                    cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                    switch (cb.SelectSingleNode("key").InnerText)
                                                    {
                                                        case "TBOMARKUP":
                                                            cbu.ChargeType = ChargeType.TBOMarkup;
                                                            break;
                                                        case "OTHERCHARGES":
                                                            cbu.ChargeType = ChargeType.OtherCharges;
                                                            break;
                                                        case "CONVENIENCECHARGES":
                                                            cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                            break;
                                                    }
                                                }
                                                sr.TBOPrice.Currency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                sr.TBOPrice.CurrencyCode = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                sr.TBOPrice.DecimalPoint = decimalValue;
                                                sr.TBOPrice.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                                sr.TBOPrice.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                                sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                sr.TBOPrice.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                                sr.TBOPrice.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                                sr.TBOPrice.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                sr.TBOPrice.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                sr.TBOPrice.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                sr.TBOPrice.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                sr.TBOPrice.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                {
                                                    sr.TBOPrice.TransactionFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                }
                                                sr.BaseFare += (double)sr.Price.BaseFare;
                                                sr.Tax += (double)(Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;

                                                #endregion
                                                //======================================================Inbound end=================================================//
                                                //Adding logic not to bind LCC to GDS
                                                Airline oAirline = new Airline();
                                                Airline iAirline = new Airline();
                                                if (sr.Flights.Length == 2)
                                                {

                                                    oAirline.Load(sr.Flights[0][0].Airline);

                                                    iAirline.Load(sr.Flights[1][0].Airline);

                                                }

                                                if (oAirline.IsLCC == iAirline.IsLCC) // Normal retrun domestic. Should only combine GDS-GDS and LCC-LCC                                        
                                                {
                                                    results.Add(sr);
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    bool isAllowed = true;
                                    #region GDS and SpecialReturn Results
                                    foreach (XmlNode result in ResultNodes)
                                    {
                                        isAllowed = true;
                                        XmlNodeList segmentNodes = result.SelectNodes("Segments");

                                        foreach (XmlNode segment in segmentNodes)
                                        {
                                            XmlNodeList segments = segment.SelectNodes("Segments");

                                            foreach (XmlNode seg in segments)
                                            {
                                               Airline segAirline = new Airline();
                                               segAirline.Load(seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText);

                                               if (!segAirline.TBOAirAllowed)
                                               {
                                                   isAllowed = false;
                                                   break;
                                               }
                                            }
                                            if (!isAllowed)
                                                break;
                                        }

                                        Airline airline = new Airline();
                                        airline.Load(result.SelectSingleNode("ValidatingAirline").InnerText);

                                        if (airline.TBOAirAllowed && isAllowed)
                                        {
                                            SearchResult sr = new SearchResult();
                                            if (type == 5)
                                            {
                                                sr.IsLCC = true;
                                                sr.FareType = "LCCSpecialReturn";
                                            }
                                            else
                                            {
                                                sr.FareType = "Normal (GDS)";
                                            }
                                            if (result.SelectSingleNode("BaggageAllowance") != null)
                                            {
                                                sr.BaggageIncludedInFare = result.SelectSingleNode("BaggageAllowance").InnerText;
                                            }
                                            sr.Currency = agentBaseCurrency;
                                            rateOfExchange = exchangeRates[result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];
                                            sr.GUID = result.SelectSingleNode("ResultIndex").InnerText;
                                            sr.JourneySellKey = result.SelectSingleNode("Source").InnerText;
                                            XmlNodeList fareBreakDownNodes = result.SelectNodes("FareBreakdown");
                                            double TransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * (double)rateOfExchange : 0;
                                            double TBOTransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) : 0;
                                            double TBOotherCharges = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);

                                            double OtherCharges = (request.IsDomestic) ? 0 : Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * (double)rateOfExchange;
                                            List<Fare> fbdList = new List<Fare>();
                                            List<Fare> tbofbdList = new List<Fare>();
                                            foreach (XmlNode fbdNode in fareBreakDownNodes)
                                            {
                                                Fare fbd = new Fare();
                                                fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                fbd.SellingFare = fbd.TotalFare;
                                                fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);

                                                fbdList.Add(fbd);

                                                Fare tbofbd = new Fare();
                                                tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                tbofbdList.Add(tbofbd);

                                            }
                                            sr.FareBreakdown = fbdList.ToArray();
                                            sr.TBOFareBreakdown = tbofbdList.ToArray();

                                            XmlNodeList fareRuleList = result.SelectNodes("FareRules");
                                            sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                            foreach (XmlNode fare in fareRuleList)
                                            {
                                                CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                sr.FareRules.Add(fr);
                                            }

                                            if (request.Type == SearchType.Return)
                                            {
                                                sr.Flights = new FlightInfo[2][];
                                            }
                                            else if (request.Type == SearchType.MultiWay)
                                            {
                                                sr.Flights = new FlightInfo[segmentNodes.Count][];
                                            }
                                            else
                                            {
                                                sr.Flights = new FlightInfo[1][];
                                            }

                                            if (request.Type != SearchType.MultiWay)
                                            {
                                                

                                                foreach (XmlNode segment in segmentNodes)
                                                {
                                                    XmlNodeList segments = segment.SelectNodes("Segments");
                                                    int tripIndicator = 0, segmentIndicator = 0;
                                                    tripIndicator = Convert.ToInt32(segments[0].SelectSingleNode("TripIndicator").InnerText);
                                                    sr.Flights[tripIndicator - 1] = new FlightInfo[segments.Count];
                                                    foreach (XmlNode seg in segments)
                                                    {
                                                        segmentIndicator = Convert.ToInt32(seg.SelectSingleNode("SegmentIndicator").InnerText);
                                                        tripIndicator = Convert.ToInt32(seg.SelectSingleNode("TripIndicator").InnerText);

                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1] = new FlightInfo();
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Airline = seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrivalTime = Convert.ToDateTime(seg.SelectSingleNode("Destination").SelectSingleNode("ArrTime").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrTerminal = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].BookingClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Craft = seg.SelectSingleNode("Craft").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepartureTime = Convert.ToDateTime(seg.SelectSingleNode("Origin").SelectSingleNode("DepTime").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepTerminal = seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                                        //Reading Airport code directly from DB
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination = new CT.BookingEngine.Airport(seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);                                                        
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = new TimeSpan(0, Convert.ToInt32(seg.SelectSingleNode("Duration").InnerText), 0);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].ETicketEligible = Convert.ToBoolean(seg.SelectSingleNode("IsETicketEligible").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightNumber = seg.SelectSingleNode("Airline").SelectSingleNode("FlightNumber").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightStatus = FlightStatus.Confirmed;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].GroundTime = new TimeSpan(Convert.ToInt32(seg.SelectSingleNode("GroundTime").InnerText));
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Group = tripIndicator - 1;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Mile = Convert.ToInt32(seg.SelectSingleNode("Mile").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].OperatingCarrier = seg.SelectSingleNode("Airline").SelectSingleNode("OperatingCarrier").InnerText;
                                                        //Reading Airport code directly from DB
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin = new CT.BookingEngine.Airport(seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);                                                        
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Status = seg.SelectSingleNode("Status").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].StopOver = Convert.ToBoolean(seg.SelectSingleNode("StopOver").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentFareType = result.SelectSingleNode("AirlineRemark").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice = new PriceAccounts();
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.Currency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.CurrencyCode = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.DecimalPoint = decimalValue;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);

                                                    }
                                                }

                                                int outbound = 0, inbound = 0;
                                                for (int i = 0; i < sr.Flights.Length; i++)
                                                {
                                                    for (int j = 0; j < sr.Flights[i].Length; j++)
                                                    {
                                                        if (sr.Flights[i][j].Group == 0)
                                                        {
                                                            outbound++;
                                                        }
                                                        else
                                                        {
                                                            inbound++;
                                                        }
                                                    }
                                                }

                                                for (int i = 0; i < sr.Flights.Length; i++)
                                                {
                                                    for (int j = 0; j < sr.Flights[i].Length; j++)
                                                    {
                                                        if (sr.Flights[i][j].Group == 0)
                                                        {
                                                            sr.Flights[i][j].Stops = outbound - 1;
                                                        }
                                                        else
                                                        {
                                                            sr.Flights[i][j].Stops = inbound - 1;
                                                        }
                                                    }
                                                }
                                            }
                                            else //MultiCity 
                                            {
                                                
                                                int tripIndicator = 1;
                                                foreach (XmlNode segment in segmentNodes)
                                                {
                                                    XmlNodeList segments = segment.SelectNodes("Segments");
                                                    int segmentIndicator = 0;
                                                    //tripIndicator = Convert.ToInt32(segments[0].SelectSingleNode("TripIndicator").InnerText);
                                                    sr.Flights[tripIndicator - 1] = new FlightInfo[segments.Count];
                                                    foreach (XmlNode seg in segments)
                                                    {
                                                        segmentIndicator = Convert.ToInt32(seg.SelectSingleNode("SegmentIndicator").InnerText);
                                                        //tripIndicator = Convert.ToInt32(seg.SelectSingleNode("TripIndicator").InnerText);

                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1] = new FlightInfo();
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Airline = seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrivalTime = Convert.ToDateTime(seg.SelectSingleNode("Destination").SelectSingleNode("ArrTime").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrTerminal = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].BookingClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Craft = seg.SelectSingleNode("Craft").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepartureTime = Convert.ToDateTime(seg.SelectSingleNode("Origin").SelectSingleNode("DepTime").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepTerminal = seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                                        //Reading Airport code directly from DB
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination = new CT.BookingEngine.Airport(seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);                                                        
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = new TimeSpan(0, Convert.ToInt32(seg.SelectSingleNode("Duration").InnerText), 0);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].ETicketEligible = Convert.ToBoolean(seg.SelectSingleNode("IsETicketEligible").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightNumber = seg.SelectSingleNode("Airline").SelectSingleNode("FlightNumber").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightStatus = FlightStatus.Confirmed;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].GroundTime = new TimeSpan(Convert.ToInt32(seg.SelectSingleNode("GroundTime").InnerText));
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Group = tripIndicator - 1;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Mile = Convert.ToInt32(seg.SelectSingleNode("Mile").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].OperatingCarrier = seg.SelectSingleNode("Airline").SelectSingleNode("OperatingCarrier").InnerText;
                                                        //Reading Airport code directly from DB
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin = new CT.BookingEngine.Airport(seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Status = seg.SelectSingleNode("Status").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].StopOver = Convert.ToBoolean(seg.SelectSingleNode("StopOver").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops = 1;//Added by Shiva
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentFareType = result.SelectSingleNode("AirlineRemark").InnerText;

                                                    }
                                                    tripIndicator++;
                                                }
                                            }

                                            sr.IsLCC = Convert.ToBoolean(result.SelectSingleNode("IsLCC").InnerText);
                                            sr.NonRefundable = (Convert.ToBoolean(result.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                            sr.ResultBookingSource = BookingSource.TBOAir;
                                            sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                            sr.ValidatingAirline = result.SelectSingleNode("ValidatingAirline").InnerText;

                                            sr.Price = new PriceAccounts();
                                            sr.Price.AccPriceType = PriceType.PublishedFare;
                                            sr.Price.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                            sr.Price.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.Price.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.Price.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                            sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes = result.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.Price.Currency = agentBaseCurrency;
                                            sr.Price.CurrencyCode = agentBaseCurrency;
                                            sr.Price.DecimalPoint = decimalValue;
                                            sr.Price.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                            sr.Price.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.Price.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                            sr.Price.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.Price.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                            sr.Price.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                            sr.Price.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                            sr.Price.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                            sr.Price.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.Price.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.Price.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.Price.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                            }
                                            sr.Price.SupplierPrice = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                            sr.Price.SupplierCurrency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;



                                            sr.TBOPrice = new PriceAccounts();
                                            sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                            sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                            sr.TBOPrice.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.TBOPrice.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.TBOPrice.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                            sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes2 = result.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes2)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.TBOPrice.Currency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.CurrencyCode = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.DecimalPoint = decimalValue;
                                            sr.TBOPrice.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                            sr.TBOPrice.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                            sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.TBOPrice.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                            sr.TBOPrice.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                            sr.TBOPrice.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                            sr.TBOPrice.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                            sr.TBOPrice.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.TBOPrice.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.TBOPrice.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.TBOPrice.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            }

                                            sr.BaseFare = (double)sr.Price.BaseFare;
                                            sr.Tax = (double)(Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                            sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;

                                            if (isAllowed)
                                            {
                                                results.Add(sr);
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Search. Error : " + ex.ToString(), "");
                }
                CombineResults.AddRange(results);
            }
            Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Search Ended" + DateTime.Now, "");
            return CombineResults.ToArray();
        }

        /// <summary>
        /// Binds GDS and Multi-way segments. Optimized for readability and Performance by Shiva
        /// </summary>
        /// <param name="segmentNodes"></param>
        /// <param name="sr"></param>
        /// <param name="result"></param>
        private bool BindSegments(XmlNodeList segmentNodes, ref SearchResult sr, XmlNode result)
        {
            bool isAirlineAllowed = true;
            try
            {
                int tripIndicator = 0, segmentIndicator = 0;
                if (Request.Type == SearchType.MultiWay)
                {
                    tripIndicator = 1;
                }
            
                foreach (XmlNode segment in segmentNodes)
                {
                    XmlNodeList segments = segment.SelectNodes("Segments");

                    foreach (XmlNode seg in segments)
                    {
                        Airline airline = new Airline();
                        airline.Load(seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText);

                        if (airline.TBOAirAllowed)
                        {
                            isAirlineAllowed = true;
                        }
                        else
                        {
                            isAirlineAllowed = false;
                            break;
                        }
                    }
                }
                if (isAirlineAllowed)
                {
                    foreach (XmlNode segment in segmentNodes)
                    {
                        XmlNodeList segments = segment.SelectNodes("Segments");

                        if (Request.Type != SearchType.MultiWay)
                        {
                            tripIndicator = Convert.ToInt32(segments[0].SelectSingleNode("TripIndicator").InnerText);
                        }
                        sr.Flights[tripIndicator - 1] = new FlightInfo[segments.Count];
                        int onwardStops = 0, returnStops = 0;
                        
                        tripIndicator = 0;
                        foreach (XmlNode seg in segments)
                        {
                            segmentIndicator = Convert.ToInt32(seg.SelectSingleNode("SegmentIndicator").InnerText);
                            tripIndicator = Convert.ToInt32(seg.SelectSingleNode("TripIndicator").InnerText);

                            sr.Flights[tripIndicator - 1][segmentIndicator - 1] = new FlightInfo();
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Airline = seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrivalTime = Convert.ToDateTime(seg.SelectSingleNode("Destination").SelectSingleNode("ArrTime").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrTerminal = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].BookingClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                            if (Request.Segments[0].flightCabinClass == CabinClass.Business)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Business.ToString();
                            }
                            else if (Request.Segments[0].flightCabinClass == CabinClass.First)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.First.ToString();
                            }
                            else if (Request.Segments[0].flightCabinClass == CabinClass.Economy)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Economy.ToString();
                            }
                            else
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                            }
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Craft = seg.SelectSingleNode("Craft").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepartureTime = Convert.ToDateTime(seg.SelectSingleNode("Origin").SelectSingleNode("DepTime").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepTerminal = seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination = new CT.BookingEngine.Airport(seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.AirportCode = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText;
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.AirportName = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportName").InnerText;
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("CityCode").InnerText;
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityName = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("CityName").InnerText;
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CountryCode = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("CountryCode").InnerText;
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CountryName = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("CountryName").InnerText;
                            if (seg.SelectSingleNode("Duration") != null)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = new TimeSpan(0, Convert.ToInt32(seg.SelectSingleNode("Duration").InnerText), 0);
                            }
                            else if (seg.SelectSingleNode("AccumulatedDuration") != null)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = new TimeSpan(0, Convert.ToInt32(seg.SelectSingleNode("AccumulatedDuration").InnerText), 0);
                            }
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ETicketEligible = Convert.ToBoolean(seg.SelectSingleNode("IsETicketEligible").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightNumber = seg.SelectSingleNode("Airline").SelectSingleNode("FlightNumber").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightStatus = FlightStatus.Confirmed;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].GroundTime = new TimeSpan(Convert.ToInt32(seg.SelectSingleNode("GroundTime").InnerText));
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Group = tripIndicator - 1;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Mile = Convert.ToInt32(seg.SelectSingleNode("Mile").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].OperatingCarrier = seg.SelectSingleNode("Airline").SelectSingleNode("OperatingCarrier").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin = new CT.BookingEngine.Airport(seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Status = seg.SelectSingleNode("Status").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].StopOver = Convert.ToBoolean(seg.SelectSingleNode("StopOver").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops = (tripIndicator == 1 ? onwardStops - 1 : returnStops - 1);
                            //if (tripIndicator == 1)
                            //{
                            //    if (sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode == Request.Segments[0].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[0].Destination ||
                            //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[0].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode == Request.Segments[0].Destination ||
                            //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[0].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[0].Destination)
                            //    {
                            //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops++;
                            //    }
                            //}
                            //else
                            //{
                            //    if (sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode == Request.Segments[1].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[1].Destination ||
                            //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[1].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode == Request.Segments[1].Destination ||
                            //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[1].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[1].Destination)
                            //    {
                            //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops++;
                            //    }
                            //}
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentFareType = result.SelectSingleNode("AirlineRemark").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice = new PriceAccounts();
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.Currency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.CurrencyCode = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.DecimalPoint = decimalValue;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);

                        }
                        if (Request.Type == SearchType.MultiWay)
                        {
                            tripIndicator++;
                        }
                    }

                    int outbound = 0, inbound = 0;
                    for (int i = 0; i < sr.Flights.Length; i++)
                    {
                        for (int j = 0; j < sr.Flights[i].Length; j++)
                        {
                            if (sr.Flights[i][j].Group == 0)
                            {
                                outbound++;
                            }
                            else
                            {
                                inbound++;
                            }
                        }
                    }

                    for (int i = 0; i < sr.Flights.Length; i++)
                    {
                        for (int j = 0; j < sr.Flights[i].Length; j++)
                        {
                            if (sr.Flights[i][j].Group == 0)
                            {
                                sr.Flights[i][j].Stops = outbound - 1;
                            }
                            else
                            {
                                sr.Flights[i][j].Stops = inbound - 1;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir)Failed to Bind Segments. " + ex.ToString(), "");
            }

            return isAirlineAllowed;
        }

        /// <summary>
        /// Binds domestic LCC or normal segments. Optimized for readability and Performance by Shiva
        /// </summary>
        /// <param name="segmentNodes"></param>
        /// <param name="sr"></param>
        /// <param name="obResult"></param>
        /// <param name="ibResult"></param>
        private void BindSegments(XmlNodeList segmentNodes, ref SearchResult sr, XmlNode obResult, XmlNode ibResult)
        {
            try
            {
                //Bind onward segments
                if (obResult != null && ibResult == null)
                {
                    foreach (XmlNode segment in segmentNodes)
                    {
                        XmlNodeList segments = segment.SelectNodes("Segments");
                        int tripIndicator = 0, segmentIndicator = 0, onwardStops = 0;
                        tripIndicator = Convert.ToInt32(segments[0].SelectSingleNode("TripIndicator").InnerText);
                        sr.Flights[tripIndicator - 1] = new FlightInfo[segments.Count];
                       
                        foreach (XmlNode seg in segments)
                        {

                            segmentIndicator = Convert.ToInt32(seg.SelectSingleNode("SegmentIndicator").InnerText);
                            tripIndicator = Convert.ToInt32(seg.SelectSingleNode("TripIndicator").InnerText);

                            sr.Flights[tripIndicator - 1][segmentIndicator - 1] = new FlightInfo();
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Airline = seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrivalTime = Convert.ToDateTime(seg.SelectSingleNode("Destination").SelectSingleNode("ArrTime").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrTerminal = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].BookingClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                            if (Request.Segments[0].flightCabinClass == CabinClass.Business)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Business.ToString();
                            }
                            else if (Request.Segments[0].flightCabinClass == CabinClass.First)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.First.ToString();
                            }
                            else if (Request.Segments[0].flightCabinClass == CabinClass.Economy)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Economy.ToString();
                            }
                            else
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                            }
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Craft = seg.SelectSingleNode("Craft").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepartureTime = Convert.ToDateTime(seg.SelectSingleNode("Origin").SelectSingleNode("DepTime").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepTerminal = seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination = new CT.BookingEngine.Airport(seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);

                            if (seg.SelectSingleNode("Duration") != null)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = TimeSpan.FromMinutes(Convert.ToInt32(seg.SelectSingleNode("Duration").InnerText));
                            }
                            else if (seg.SelectSingleNode("AccumulatedDuration") != null)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = TimeSpan.FromMinutes(Convert.ToInt32(seg.SelectSingleNode("AccumulatedDuration").InnerText));
                            }
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ETicketEligible = Convert.ToBoolean(seg.SelectSingleNode("IsETicketEligible").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightNumber = seg.SelectSingleNode("Airline").SelectSingleNode("FlightNumber").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightStatus = FlightStatus.Confirmed;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].GroundTime = new TimeSpan(Convert.ToInt32(seg.SelectSingleNode("GroundTime").InnerText));
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Group = tripIndicator - 1;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Mile = Convert.ToInt32(seg.SelectSingleNode("Mile").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].OperatingCarrier = seg.SelectSingleNode("Airline").SelectSingleNode("OperatingCarrier").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin = new CT.BookingEngine.Airport(seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Status = seg.SelectSingleNode("Status").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].StopOver = Convert.ToBoolean(seg.SelectSingleNode("StopOver").InnerText);
                        //    if (sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode == Request.Segments[0].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[0].Destination ||
                        //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[0].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode == Request.Segments[0].Destination)
                        //    {
                        //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops++;
                        //    }
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops = onwardStops-1;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice = new PriceAccounts();
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.AccPriceType = PriceType.PublishedFare;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.ChargeBU = new List<ChargeBreakUp>();
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentFareType = !string.IsNullOrEmpty ( obResult.SelectSingleNode("AirlineRemark").InnerText) ? obResult.SelectSingleNode("AirlineRemark").InnerText : string.Empty;
                            XmlNodeList chargeNodes1 = obResult.SelectNodes("Fare/ChargeBU");
                            foreach (XmlNode cb in chargeNodes1)
                            {
                                ChargeBreakUp cbu = new ChargeBreakUp();
                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                switch (cb.SelectSingleNode("key").InnerText)
                                {
                                    case "TBOMARKUP":
                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                        break;
                                    case "OTHERCHARGES":
                                        cbu.ChargeType = ChargeType.OtherCharges;
                                        break;
                                    case "CONVENIENCECHARGES":
                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                        break;
                                }
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.ChargeBU.Add(cbu);
                            }
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.Currency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.CurrencyCode = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.DecimalPoint = decimalValue;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir)Failed to bind onward segments." + ex.ToString(), "");
            }

            try{
            //Bind return segments
                if (ibResult != null && obResult == null)
                {
                    foreach (XmlNode segment in segmentNodes)
                    {
                            
                        XmlNodeList segments = segment.SelectNodes("Segments");
                        int tripIndicator = 0, segmentIndicator = 0, returnStops = 0;
                        tripIndicator = Convert.ToInt32(segments[0].SelectSingleNode("TripIndicator").InnerText);
                        sr.Flights[tripIndicator] = new FlightInfo[segments.Count];
                        
                        foreach (XmlNode seg in segments)
                        {

                            segmentIndicator = Convert.ToInt32(seg.SelectSingleNode("SegmentIndicator").InnerText);
                            tripIndicator = Convert.ToInt32(seg.SelectSingleNode("TripIndicator").InnerText);

                            sr.Flights[tripIndicator][segmentIndicator - 1] = new FlightInfo();
                            sr.Flights[tripIndicator][segmentIndicator - 1].Airline = seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].ArrivalTime = Convert.ToDateTime(seg.SelectSingleNode("Destination").SelectSingleNode("ArrTime").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].ArrTerminal = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].BookingClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                            if (Request.Segments[0].flightCabinClass == CabinClass.Business)
                            {
                                sr.Flights[tripIndicator][segmentIndicator - 1].CabinClass = CabinClass.Business.ToString();
                            }
                            else if (Request.Segments[0].flightCabinClass == CabinClass.First)
                            {
                                sr.Flights[tripIndicator][segmentIndicator - 1].CabinClass = CabinClass.First.ToString();
                            }
                            else if (Request.Segments[0].flightCabinClass == CabinClass.Economy)
                            {
                                sr.Flights[tripIndicator][segmentIndicator - 1].CabinClass = CabinClass.Economy.ToString();
                            }
                            else
                            {
                                sr.Flights[tripIndicator][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                            }
                            sr.Flights[tripIndicator][segmentIndicator - 1].Craft = seg.SelectSingleNode("Craft").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].DepartureTime = Convert.ToDateTime(seg.SelectSingleNode("Origin").SelectSingleNode("DepTime").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].DepTerminal = seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].Destination = new CT.BookingEngine.Airport(seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                            
                            if (seg.SelectSingleNode("Duration") != null)
                            {
                                sr.Flights[tripIndicator][segmentIndicator - 1].Duration = TimeSpan.FromMinutes(Convert.ToInt32(seg.SelectSingleNode("Duration").InnerText));
                            }
                            else if (seg.SelectSingleNode("AccumulatedDuration") != null)
                            {
                                sr.Flights[tripIndicator][segmentIndicator - 1].Duration = TimeSpan.FromMinutes(Convert.ToInt32(seg.SelectSingleNode("AccumulatedDuration").InnerText));
                            }
                            sr.Flights[tripIndicator][segmentIndicator - 1].ETicketEligible = Convert.ToBoolean(seg.SelectSingleNode("IsETicketEligible").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].FlightNumber = seg.SelectSingleNode("Airline").SelectSingleNode("FlightNumber").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].FlightStatus = FlightStatus.Confirmed;
                            sr.Flights[tripIndicator][segmentIndicator - 1].GroundTime = TimeSpan.FromMinutes(Convert.ToInt32(seg.SelectSingleNode("GroundTime").InnerText));
                            sr.Flights[tripIndicator][segmentIndicator - 1].Group = tripIndicator;
                            sr.Flights[tripIndicator][segmentIndicator - 1].Mile = Convert.ToInt32(seg.SelectSingleNode("Mile").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].OperatingCarrier = seg.SelectSingleNode("Airline").SelectSingleNode("OperatingCarrier").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].Origin = new CT.BookingEngine.Airport(seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].Status = seg.SelectSingleNode("Status").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].StopOver = Convert.ToBoolean(seg.SelectSingleNode("StopOver").InnerText);
                        //    if (sr.Flights[tripIndicator][segmentIndicator - 1].Origin.CityCode == Request.Segments[1].Origin && sr.Flights[tripIndicator][segmentIndicator - 1].Destination.CityCode != Request.Segments[1].Destination ||
                        //sr.Flights[tripIndicator][segmentIndicator - 1].Origin.CityCode != Request.Segments[1].Origin && sr.Flights[tripIndicator][segmentIndicator - 1].Destination.CityCode == Request.Segments[1].Destination)
                        //    {
                        //        sr.Flights[tripIndicator][segmentIndicator - 1].Stops++;
                        //    }
                            sr.Flights[tripIndicator][segmentIndicator - 1].Stops = returnStops - 1;
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice = new PriceAccounts();
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.AccPriceType = PriceType.PublishedFare;
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.AdditionalTxnFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.AgentCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.AgentPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.BaseFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.Tax = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.ChargeBU = new List<ChargeBreakUp>();
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentFareType = !string.IsNullOrEmpty ( ibResult.SelectSingleNode("AirlineRemark").InnerText) ? ibResult.SelectSingleNode("AirlineRemark").InnerText : string.Empty;
                            XmlNodeList chargeNodes1 = ibResult.SelectNodes("Fare/ChargeBU");
                            foreach (XmlNode cb in chargeNodes1)
                            {
                                ChargeBreakUp cbu = new ChargeBreakUp();
                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                switch (cb.SelectSingleNode("key").InnerText)
                                {
                                    case "TBOMARKUP":
                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                        break;
                                    case "OTHERCHARGES":
                                        cbu.ChargeType = ChargeType.OtherCharges;
                                        break;
                                    case "CONVENIENCECHARGES":
                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                        break;
                                }
                                sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.ChargeBU.Add(cbu);
                            }

                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.Currency = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.CurrencyCode = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.DecimalPoint = decimalValue;
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.YQTax = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.IncentiveEarned = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.NetFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.PublishedFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.SServiceFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.OtherCharges = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.TdsCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.TDSPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.TDSIncentive = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                            
                        }
                    }
                }

                int outbound = 0, inbound = 0;
                for (int i = 0; i < sr.Flights.Length; i++)
                {
                    for (int j = 0; j < sr.Flights[i].Length; j++)
                    {
                        if (sr.Flights[i][j].Group == 0)
                        {
                            outbound++;
                        }
                        else
                        {
                            inbound++;
                        }
                    }
                }

                for (int i = 0; i < sr.Flights.Length; i++)
                {
                    for (int j = 0; j < sr.Flights[i].Length; j++)
                    {
                        if (sr.Flights[i][j].Group == 0)
                        {
                            sr.Flights[i][j].Stops = outbound - 1;
                        }
                        else
                        {
                            sr.Flights[i][j].Stops = inbound - 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir)Failed to bind return segments. " + ex.ToString(), "");
            }
        }
        
        /// <summary>
        /// To search Special Return fares for LCC. result will be mixed of LCC and GDS. Combining same airline results.
        /// and Displaying fares with breakdown.
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchSpecialReturnLCC(object eventNumber)
        {
            Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Special Return  LCC Started" + DateTime.Now, "");
            SearchRequest request = new SearchRequest();
            request = Request;
            List<SearchResult> results = new List<SearchResult>();
            try
            {
                AuthenticationResponse ar = Authenticate();
                if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                {
                    //AgencyBalance balance = GetAgencyBalance(ar);

                    #region JSON Search Request
                    int type = 5;//Return

                    StringBuilder requestData = new StringBuilder();
                    requestData.AppendLine("{");
                    requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                    requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                    requestData.AppendLine("\"AdultCount\": \"" + request.AdultCount + "\",");
                    requestData.AppendLine("\"ChildCount\": \"" + request.ChildCount + "\",");
                    requestData.AppendLine("\"InfantCount\": \"" + request.InfantCount + "\",");
                    if (request.MaxStops == "-1")
                    {
                        requestData.AppendLine("\"DirectFlight\": \"false\",");
                        requestData.AppendLine("\"OneStopFlight\": \"false\",");
                    }
                    else if (request.MaxStops == "0")
                    {
                        requestData.AppendLine("\"DirectFlight\": \"true\",");
                        requestData.AppendLine("\"OneStopFlight\": \"false\",");
                    }
                    else if (request.MaxStops == "1")
                    {
                        requestData.AppendLine("\"DirectFlight\": \"false\",");
                        requestData.AppendLine("\"OneStopFlight\": \"true\",");
                    }
                    else
                    {
                        requestData.AppendLine("\"DirectFlight\": \"false\",");
                        requestData.AppendLine("\"OneStopFlight\": \"false\",");
                    }
                    requestData.AppendLine("\"JourneyType\": \"" + type + "\",");
                    if (request.Segments[0].PreferredAirlines.Length == 0)
                    {
                        requestData.AppendLine("\"PreferredAirlines\": null,");
                    }
                    else
                    {
                        //requestData.AppendLine("\"PreferredAirlines\": [");
                        ////requestData.AppendLine("{");
                        //string pa = string.Empty;
                        //foreach (string airline in request.Segments[0].PreferredAirlines)
                        //{
                        //    if (pa.Length > 0)
                        //    {
                        //        pa += "," + "\"" + airline + "\"";
                        //    }
                        //    else
                        //    {
                        //        pa = "\"" + airline + "\"";
                        //    }
                        //}
                        //requestData.AppendLine(pa);
                        ////requestData.AppendLine("}");
                        //requestData.AppendLine("],");
                    }
                    requestData.AppendLine("\"Segments\": [");

                    foreach (FlightSegment segment in request.Segments)
                    {
                        if (segment.Origin != null)
                        {
                            requestData.AppendLine("{");
                            requestData.AppendLine("\"Origin\": \"" + segment.Origin + "\",");
                            requestData.AppendLine("\"Destination\": \"" + segment.Destination + "\",");
                            requestData.AppendLine("\"FlightCabinClass\": \"" + (int)segment.flightCabinClass + "\",");
                            requestData.AppendLine("\"PreferredDepartureTime\": \"" + segment.PreferredDepartureTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\",");
                            requestData.AppendLine("\"PreferredArrivalTime\": \"" + segment.PreferredArrivalTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\"");
                            if (segment == request.Segments[request.Segments.Length - 1])
                            {
                                requestData.AppendLine("}");
                            }
                            else
                            {
                                requestData.AppendLine("},");
                            }
                        }
                    }
                    requestData.AppendLine("],");
                    requestData.AppendLine("\"Sources\": [");

                    if (request.Segments[0].PreferredAirlines.Length > 0)
                    {
                        if (Request.Segments[0].PreferredAirlines[0] == "6E")
                        {
                            requestData.AppendLine("\"6E\"");
                        }
                        else if (Request.Segments[0].PreferredAirlines[0] == "G8")
                        {
                            requestData.AppendLine("\"G8\"");
                        }
                        else if (Request.Segments[0].PreferredAirlines[0] == "SG")
                        {
                            requestData.AppendLine("\"SG\"");
                        }
                        else
                        {
                            // Passing only LCC sources.
                            requestData.AppendLine("\"6E\","); //Indigo
                            requestData.AppendLine("\"G8\","); //Go Air
                            requestData.AppendLine("\"SG\","); //SpiceJet
                            requestData.AppendLine("\"IX\""); //Airindia Express
                        }
                    }
                    else
                    {
                        // Passing only LCC sources.
                        requestData.AppendLine("\"6E\","); //Indigo
                        requestData.AppendLine("\"G8\","); //Go Air
                        requestData.AppendLine("\"SG\","); //SpiceJet
                        requestData.AppendLine("\"IX\""); //Airindia Express
                    }
                    requestData.AppendLine("]");
                    requestData.AppendLine("}");
                    #endregion

                    //Response resp = null;
                    XmlDocument doc = null;
                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirSearchRequestReturnLCCJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(requestData.ToString());
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(requestData.ToString(), "Request");
                            filePath = XmlPath + "TBOAirSearchRequestReturnLCCJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save SpecialReturnLCC Request. Error" + ex.ToString(), "");
                    }

                    doc = null;
                    Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "Special Return LCC Request Started", "");
                    string response = GetResponse(requestData.ToString(), searchUrl);
                    Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "Special Return LCC Got REsponse", "");
                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirSearchResponseReturnLCC_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(response);
                            sw.Close();

                            TextReader reader = new StringReader(response);
                            doc = JsonConvert.DeserializeXmlNode(response);

                            filePath = XmlPath + "TBOAirSearchResponseRTNLCC_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save SpecialReturnLCC Response. Error" + ex.ToString(), "");
                    }

                    if (doc != null)
                    {
                        XmlNode Error = doc.SelectSingleNode("Response/Error");
                        string errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                        if (errorMessage.Length <= 0)
                        {
                            XmlNodeList ResultNodes = doc.SelectNodes("Response/Results/Results");

                            if (request.IsDomestic)
                            {
                                List<XmlNode> outboundResults = new List<XmlNode>();
                                List<XmlNode> inboundResults = new List<XmlNode>();

                                foreach (XmlNode result in ResultNodes)
                                {
                                    if (result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Origin/Airport/AirportCode").InnerText == request.Segments[0].Origin)
                                    {
                                        outboundResults.Add(result);
                                    }
                                    if (request.Segments.Length > 1)
                                    {
                                        if (result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Origin/Airport/AirportCode").InnerText == request.Segments[1].Origin)
                                        {
                                            inboundResults.Add(result);
                                        }
                                    }
                                }
                                rateOfExchange = exchangeRates[outboundResults[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];

                                #region Domestic Results

                                foreach (XmlNode obResult in outboundResults)
                                {
                                    foreach (XmlNode ibResult in inboundResults)
                                    {
                                        Airline airlineOut = new Airline();
                                        airlineOut.Load(obResult.SelectSingleNode("ValidatingAirline").InnerText);

                                        Airline airlineIn = new Airline();
                                        airlineIn.Load(ibResult.SelectSingleNode("ValidatingAirline").InnerText);                                            

                                        if (airlineOut.TBOAirAllowed && airlineIn.TBOAirAllowed)
                                        {
                                            SearchResult sr = new SearchResult();
                                            sr.IsLCC = true;
                                            sr.Currency = agentBaseCurrency;

                                            if (obResult.SelectSingleNode("BaggageAllowance") != null)
                                            {
                                                sr.BaggageIncludedInFare = obResult.SelectSingleNode("BaggageAllowance").InnerText;
                                            }
                                            //=====================================================Outbound begin===============================================//
                                            #region Outbound Result
                                            sr.GUID = obResult.SelectSingleNode("ResultIndex").InnerText;
                                            sr.JourneySellKey = obResult.SelectSingleNode("Source").InnerText;
                                            XmlNodeList fareBreakDownNodes = obResult.SelectNodes("FareBreakdown");
                                            List<Fare> fbdList = new List<Fare>();
                                            List<Fare> tbofbdList = new List<Fare>();
                                            foreach (XmlNode fbdNode in fareBreakDownNodes)
                                            {
                                                Fare fbd = new Fare();
                                                fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                fbd.SellingFare = fbd.TotalFare;
                                                fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);
                                                fbdList.Add(fbd);

                                                Fare tbofbd = new Fare();
                                                tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                tbofbdList.Add(tbofbd);

                                            }
                                            sr.FareBreakdown = fbdList.ToArray();
                                            sr.TBOFareBreakdown = tbofbdList.ToArray();

                                            XmlNodeList fareRuleList = obResult.SelectNodes("FareRules");
                                            sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                            foreach (XmlNode fare in fareRuleList)
                                            {
                                                CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                sr.FareRules.Add(fr);
                                            }

                                            if (request.Type == SearchType.Return)
                                            {
                                                sr.Flights = new FlightInfo[2][];
                                            }
                                            else
                                            {
                                                sr.Flights = new FlightInfo[1][];
                                            }

                                            XmlNodeList segmentNodes = obResult.SelectNodes("Segments");



                                            sr.IsLCC = Convert.ToBoolean(obResult.SelectSingleNode("IsLCC").InnerText);
                                            sr.NonRefundable = (Convert.ToBoolean(obResult.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                            sr.ResultBookingSource = BookingSource.TBOAir;
                                            sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                            sr.ValidatingAirline = obResult.SelectSingleNode("ValidatingAirline").InnerText;

                                            BindSegments(segmentNodes, ref sr, obResult, null);

                                            sr.Price = new PriceAccounts();
                                            sr.Price.AccPriceType = PriceType.PublishedFare;
                                            sr.Price.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                            sr.Price.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.Price.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.Price.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                            sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes = obResult.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.Price.Currency = agentBaseCurrency;
                                            sr.Price.CurrencyCode = agentBaseCurrency;
                                            sr.Price.DecimalPoint = decimalValue;
                                            sr.Price.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                            sr.Price.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                            sr.Price.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.Price.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                            sr.Price.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                            sr.Price.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                            sr.Price.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                            sr.Price.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.Price.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.Price.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            sr.Price.SupplierPrice = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                            sr.Price.SupplierCurrency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;

                                            sr.TBOPrice = new PriceAccounts();
                                            sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                            sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                            sr.TBOPrice.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.TBOPrice.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.TBOPrice.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                            sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes2 = ibResult.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes2)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.TBOPrice.Currency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.CurrencyCode = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.DecimalPoint = decimalValue;
                                            sr.TBOPrice.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                            sr.TBOPrice.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                            sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.TBOPrice.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                            sr.TBOPrice.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                            sr.TBOPrice.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                            sr.TBOPrice.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                            sr.TBOPrice.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.TBOPrice.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.TBOPrice.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.TBOPrice.TransactionFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            }


                                            sr.BaseFare = (double)sr.Price.BaseFare;
                                            sr.Tax = (double)(Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                            sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.AdditionalTxnFee;
                                            #endregion
                                            //======================================================Outbound end================================================//


                                            //=====================================================Inbound Begin================================================//
                                            #region Inbound Result
                                            //If inbound segment is having restricted airline then skip outbound result binding


                                            XmlNodeList fareBreakDownNodes20 = ibResult.SelectNodes("FareBreakdown");
                                            sr.GUID += "," + ibResult.SelectSingleNode("ResultIndex").InnerText;
                                            sr.JourneySellKey += "|" + obResult.SelectSingleNode("Source").InnerText;
                                            if (ibResult.SelectSingleNode("BaggageAllowance") != null)
                                            {
                                                sr.BaggageIncludedInFare += "," + ibResult.SelectSingleNode("BaggageAllowance").InnerText;
                                            }
                                            foreach (XmlNode fbdNode in fareBreakDownNodes20)
                                            {
                                                foreach (Fare fbd in sr.FareBreakdown)
                                                {
                                                    if (fbd.PassengerType == (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText))
                                                    {
                                                        fbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                        fbd.BaseFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                        double baseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                        fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                        fbd.TotalFare += baseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                        fbd.SellingFare += fbd.TotalFare;
                                                        fbd.SupplierFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);
                                                        break;
                                                    }
                                                }

                                                foreach (Fare tbofbd in sr.TBOFareBreakdown)
                                                {
                                                    if (tbofbd.PassengerType == (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText))
                                                    {
                                                        tbofbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                        tbofbd.BaseFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                        tbofbd.PassengerCount += Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                        tbofbd.TotalFare += tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                        break;
                                                    }
                                                }
                                            }

                                            XmlNodeList fareRuleList20 = ibResult.SelectNodes("FareRules");

                                            foreach (XmlNode fare in fareRuleList20)
                                            {
                                                CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                sr.FareRules.Add(fr);
                                            }

                                            XmlNodeList segmentNodes20 = ibResult.SelectNodes("Segments");



                                            sr.IsLCC = Convert.ToBoolean(ibResult.SelectSingleNode("IsLCC").InnerText);
                                            sr.NonRefundable = (Convert.ToBoolean(ibResult.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                            sr.ResultBookingSource = BookingSource.TBOAir;
                                            sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                            sr.ValidatingAirline = ibResult.SelectSingleNode("ValidatingAirline").InnerText;

                                            BindSegments(segmentNodes20, ref sr, null, ibResult);

                                            sr.Price.AdditionalTxnFee += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                            sr.Price.AgentCommission += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.Price.AgentPLB += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.Price.BaseFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;

                                            XmlNodeList chargeNodes20 = ibResult.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes20)
                                            {
                                                foreach (ChargeBreakUp cbu in sr.Price.ChargeBU)
                                                {

                                                    switch (cb.SelectSingleNode("key").InnerText)
                                                    {
                                                        case "TBOMARKUP":
                                                            if (cbu.ChargeType == ChargeType.TBOMarkup)
                                                            {
                                                                cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                            }
                                                            break;
                                                        case "OTHERCHARGES":
                                                            if (cbu.ChargeType == ChargeType.OtherCharges)
                                                            {
                                                                cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                            }
                                                            break;
                                                        case "CONVENIENCECHARGES":
                                                            if (cbu.ChargeType == ChargeType.ConvenienceCharge)
                                                            {
                                                                cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                            }
                                                            break;
                                                    }
                                                }
                                            }
                                            sr.Price.Currency = agentBaseCurrency;
                                            sr.Price.CurrencyCode = agentBaseCurrency;
                                            sr.Price.DecimalPoint = decimalValue;
                                            sr.Price.Tax += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                            sr.Price.YQTax += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                            sr.Price.IncentiveEarned += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.Price.NetFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                            sr.Price.PublishedFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                            sr.Price.SServiceFee += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                            sr.Price.OtherCharges += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                            sr.Price.TdsCommission += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.Price.TDSPLB += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.Price.TDSIncentive += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            sr.Price.SupplierPrice += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);

                                            sr.TBOPrice = new PriceAccounts();
                                            sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                            sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                            sr.TBOPrice.AgentCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.TBOPrice.AgentPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.TBOPrice.BaseFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                            sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes3 = ibResult.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes3)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.TBOPrice.Currency = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.CurrencyCode = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.DecimalPoint = decimalValue;
                                            sr.TBOPrice.Tax = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                            sr.TBOPrice.YQTax = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                            sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.TBOPrice.NetFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                            sr.TBOPrice.PublishedFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                            sr.TBOPrice.SServiceFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                            sr.TBOPrice.OtherCharges = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                            sr.TBOPrice.TdsCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.TBOPrice.TDSPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.TBOPrice.TDSIncentive = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            if (ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.TBOPrice.TransactionFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            }


                                            sr.BaseFare += (double)sr.Price.BaseFare;
                                            sr.Tax += (double)(Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                            sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;

                                            #endregion
                                            //======================================================Inbound end=================================================//
                                            sr.FareType = "SpecialReturnLCC (Domestic)";
                                            //Adding logic not to bind LCC to GDS
                                            Airline oAirline = new Airline();
                                            Airline iAirline = new Airline();
                                            if (sr.Flights.Length == 2)
                                            {

                                                //oAirline.Load(sr.Flights[0][0].Airline);
                                                //iAirline.Load(sr.Flights[1][0].Airline);

                                            }

                                            if (sr.Flights[0][0].Airline == sr.Flights[1][0].Airline && sr.ValidatingAirline.Length > 0)
                                            {
                                                results.Add(sr);
                                            }

                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                
                                #region GDS and SpecialReturn Results
                                foreach (XmlNode result in ResultNodes)
                                {   
                                    Airline airline = new Airline();
                                    airline.Load(result.SelectSingleNode("ValidatingAirline").InnerText);

                                    if (airline.TBOAirAllowed )
                                    {
                                        SearchResult sr = new SearchResult();
                                        if (type == 5)
                                        {
                                            sr.IsLCC = true;
                                            sr.FareType = "LCCSpecialReturn";
                                        }
                                        else
                                        {
                                            sr.FareType = "Normal (GDS)";
                                        }
                                        if (result.SelectSingleNode("BaggageAllowance") != null)
                                        {
                                            sr.BaggageIncludedInFare = result.SelectSingleNode("BaggageAllowance").InnerText;
                                        }
                                        sr.Currency = agentBaseCurrency;
                                        rateOfExchange = exchangeRates[result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];
                                        sr.GUID = result.SelectSingleNode("ResultIndex").InnerText;
                                        sr.JourneySellKey = result.SelectSingleNode("Source").InnerText;
                                        XmlNodeList fareBreakDownNodes = result.SelectNodes("FareBreakdown");
                                        double TransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * (double)rateOfExchange : 0;
                                        double TBOTransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) : 0;
                                        double TBOotherCharges = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);

                                        double OtherCharges = (request.IsDomestic) ? 0 : Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * (double)rateOfExchange;
                                        List<Fare> fbdList = new List<Fare>();
                                        List<Fare> tbofbdList = new List<Fare>();
                                        foreach (XmlNode fbdNode in fareBreakDownNodes)
                                        {
                                            Fare fbd = new Fare();
                                            fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                            fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                            fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                            fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                            fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                            fbd.SellingFare = fbd.TotalFare;
                                            fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);

                                            fbdList.Add(fbd);

                                            Fare tbofbd = new Fare();
                                            tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                            tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                            tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                            tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                            tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                            tbofbdList.Add(tbofbd);

                                        }
                                        sr.FareBreakdown = fbdList.ToArray();
                                        sr.TBOFareBreakdown = tbofbdList.ToArray();

                                        XmlNodeList fareRuleList = result.SelectNodes("FareRules");
                                        sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                        foreach (XmlNode fare in fareRuleList)
                                        {
                                            CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                            fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                            fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                            fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                            fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                            fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                            fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                            sr.FareRules.Add(fr);
                                        }
                                        XmlNodeList segmentNodes = result.SelectNodes("Segments");
                                        if (request.Type == SearchType.Return)
                                        {
                                            sr.Flights = new FlightInfo[2][];
                                        }
                                        else if (request.Type == SearchType.MultiWay)
                                        {
                                            sr.Flights = new FlightInfo[segmentNodes.Count][];
                                        }
                                        else
                                        {
                                            sr.Flights = new FlightInfo[1][];
                                        }

                                        if (request.Type != SearchType.MultiWay)
                                        {
                                            BindSegments(segmentNodes, ref sr, result);
                                        }
                                        else //MultiCity 
                                        {
                                            //int tripIndicator = 1;
                                            BindSegments(segmentNodes, ref sr, result);
                                        }

                                        sr.IsLCC = Convert.ToBoolean(result.SelectSingleNode("IsLCC").InnerText);
                                        sr.NonRefundable = (Convert.ToBoolean(result.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                        sr.ResultBookingSource = BookingSource.TBOAir;
                                        sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                        sr.ValidatingAirline = result.SelectSingleNode("ValidatingAirline").InnerText;

                                        sr.Price = new PriceAccounts();
                                        sr.Price.AccPriceType = PriceType.PublishedFare;
                                        sr.Price.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                        sr.Price.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                        sr.Price.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                        sr.Price.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                        sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                        XmlNodeList chargeNodes = result.SelectNodes("Fare/ChargeBU");
                                        foreach (XmlNode cb in chargeNodes)
                                        {
                                            ChargeBreakUp cbu = new ChargeBreakUp();
                                            cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                            switch (cb.SelectSingleNode("key").InnerText)
                                            {
                                                case "TBOMARKUP":
                                                    cbu.ChargeType = ChargeType.TBOMarkup;
                                                    break;
                                                case "OTHERCHARGES":
                                                    cbu.ChargeType = ChargeType.OtherCharges;
                                                    break;
                                                case "CONVENIENCECHARGES":
                                                    cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                    break;
                                            }
                                        }
                                        sr.Price.Currency = agentBaseCurrency;
                                        sr.Price.CurrencyCode = agentBaseCurrency;
                                        sr.Price.DecimalPoint = decimalValue;
                                        sr.Price.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;

                                        sr.Price.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                        sr.Price.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                        sr.Price.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                        sr.Price.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                        sr.Price.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                        sr.Price.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                        sr.Price.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                        sr.Price.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                        sr.Price.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                        if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                        {
                                            sr.Price.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                        }
                                        //sr.Price.Tax += sr.Price.OtherCharges + sr.Price.TransactionFee;
                                        sr.Price.SupplierPrice = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                        sr.Price.SupplierCurrency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;

                                        sr.TBOPrice = new PriceAccounts();
                                        sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                        sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                        sr.TBOPrice.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                        sr.TBOPrice.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                        sr.TBOPrice.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                        sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                        XmlNodeList chargeNodes2 = result.SelectNodes("Fare/ChargeBU");
                                        foreach (XmlNode cb in chargeNodes2)
                                        {
                                            ChargeBreakUp cbu = new ChargeBreakUp();
                                            cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                            switch (cb.SelectSingleNode("key").InnerText)
                                            {
                                                case "TBOMARKUP":
                                                    cbu.ChargeType = ChargeType.TBOMarkup;
                                                    break;
                                                case "OTHERCHARGES":
                                                    cbu.ChargeType = ChargeType.OtherCharges;
                                                    break;
                                                case "CONVENIENCECHARGES":
                                                    cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                    break;
                                            }
                                        }
                                        sr.TBOPrice.Currency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                        sr.TBOPrice.CurrencyCode = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                        sr.TBOPrice.DecimalPoint = decimalValue;
                                        sr.TBOPrice.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                        sr.TBOPrice.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                        sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                        sr.TBOPrice.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                        sr.TBOPrice.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                        sr.TBOPrice.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                        sr.TBOPrice.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                        sr.TBOPrice.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                        sr.TBOPrice.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                        sr.TBOPrice.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                        if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                        {
                                            sr.TBOPrice.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                        }

                                        sr.BaseFare = (double)sr.Price.BaseFare;
                                        sr.Tax = (double)(Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                        sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee;
                                        
                                        results.Add(sr);
                                    }
                                }
                                #endregion
                            }
                            SearchResult[] finalResult = results.ToArray();
                            if (finalResult.Length > 0)
                            {
                                Array.Sort<SearchResult>(finalResult, finalResult[0]);
                            }
                        }
                    }
                    
                    CombineResults.AddRange(results);
                }
                eventFlag[(int)eventNumber].Set();
                Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Special Return  LCC Ended" + DateTime.Now, "");
            }
            catch (Exception ex)
            {
                eventFlag[(int)eventNumber].Set();
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Search. Error : " + ex.ToString(), "");
            }

        }
        /// <summary>
        /// To fetch normal return fares. result will be mixed of LCC and GDS. Combining LCC to LCC and GDS to GDS 
        /// and Displaying fares with breakdown.
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchNormalReturn(object eventNumber)
        {
            Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Normal Return Started" + DateTime.Now, "");
            SearchRequest request = Request;
            List<SearchResult> results = new List<SearchResult>();
            try
            {
                AuthenticationResponse ar = Authenticate();
                if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                {
                    //AgencyBalance balance = GetAgencyBalance(ar);

                    #region JSON Search Request
                    int type = 2;//Return

                    StringBuilder requestData = new StringBuilder();
                    requestData.AppendLine("{");
                    requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                    requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                    requestData.AppendLine("\"AdultCount\": \"" + request.AdultCount + "\",");
                    requestData.AppendLine("\"ChildCount\": \"" + request.ChildCount + "\",");
                    requestData.AppendLine("\"InfantCount\": \"" + request.InfantCount + "\",");
                    if (request.MaxStops == "-1")
                    {
                        requestData.AppendLine("\"DirectFlight\": \"false\",");
                        requestData.AppendLine("\"OneStopFlight\": \"false\",");
                    }
                    else if (request.MaxStops == "0")
                    {
                        requestData.AppendLine("\"DirectFlight\": \"true\",");
                        requestData.AppendLine("\"OneStopFlight\": \"false\",");
                    }
                    else if (request.MaxStops == "1")
                    {
                        requestData.AppendLine("\"DirectFlight\": \"false\",");
                        requestData.AppendLine("\"OneStopFlight\": \"true\",");
                    }
                    else
                    {
                        requestData.AppendLine("\"DirectFlight\": \"false\",");
                        requestData.AppendLine("\"OneStopFlight\": \"false\",");
                    }
                    requestData.AppendLine("\"JourneyType\": \"" + type + "\",");
                    if (request.Segments[0].PreferredAirlines.Length == 0)
                    {
                        requestData.AppendLine("\"PreferredAirlines\": null,");
                    }
                    //else
                    //{
                    //    requestData.AppendLine("\"PreferredAirlines\": [");
                    //    //requestData.AppendLine("{");
                    //    string pa = string.Empty;
                    //    foreach (string airline in request.Segments[0].PreferredAirlines)
                    //    {
                    //        if (pa.Length > 0)
                    //        {
                    //            pa += "," + "\"" + airline + "\"";
                    //        }
                    //        else
                    //        {
                    //            pa = "\"" + airline + "\"";
                    //        }
                    //    }
                    //    requestData.AppendLine(pa);
                    //    //requestData.AppendLine("}");
                    //    requestData.AppendLine("],");
                    //}
                    requestData.AppendLine("\"Segments\": [");

                    foreach (FlightSegment segment in request.Segments)
                    {
                        if (segment.Origin != null)
                        {
                            requestData.AppendLine("{");
                            requestData.AppendLine("\"Origin\": \"" + segment.Origin + "\",");
                            requestData.AppendLine("\"Destination\": \"" + segment.Destination + "\",");
                            requestData.AppendLine("\"FlightCabinClass\": \"" + (int)segment.flightCabinClass + "\",");
                            requestData.AppendLine("\"PreferredDepartureTime\": \"" + segment.PreferredDepartureTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\",");
                            requestData.AppendLine("\"PreferredArrivalTime\": \"" + segment.PreferredArrivalTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\"");
                            if (segment == request.Segments[request.Segments.Length - 1])
                            {
                                requestData.AppendLine("}");
                            }
                            else
                            {
                                requestData.AppendLine("},");
                            }
                        }
                    }
                    requestData.AppendLine("]");
                    requestData.AppendLine("\"Sources\": [");
                    //Passing both LCC and GDS Sources . Mix match results will come.
                    //if (request.Segments[0].PreferredAirlines.Length > 0)
                    //{
                    //    //if (Request.Segments[0].PreferredAirlines[0] == "6E")
                    //    //{
                    //    //    requestData.AppendLine("\"6E\"");
                    //    //}
                    //    //else if (Request.Segments[0].PreferredAirlines[0] == "G8")
                    //    //{
                    //    //    requestData.AppendLine("\"G8\"");
                    //    //}
                    //    //else if (Request.Segments[0].PreferredAirlines[0] == "SG")
                    //    //{
                    //    //    requestData.AppendLine("\"SG\"");
                    //    //}
                    //    //if (Request.Segments[0].PreferredAirlines[0] == "AK")
                    //    //{
                    //    //    requestData.AppendLine("\"AK\"");
                    //    //}
                    //    //else if (Request.Segments[0].PreferredAirlines[0] == "LB")
                    //    //{
                    //    //    requestData.AppendLine("\"LB\"");
                    //    //}
                    //    //else if (Request.Segments[0].PreferredAirlines[0] == "IX")
                    //    //{
                    //    //    requestData.AppendLine("\"IX\"");
                    //    //}
                    //    //else if (Request.Segments[0].PreferredAirlines[0] == "G9")
                    //    //{
                    //    //    requestData.AppendLine("\"G9\"");
                    //    //}
                    //    //else if (Request.Segments[0].PreferredAirlines[0] == "FZ")
                    //    //{
                    //    //    requestData.AppendLine("\"FZ\"");
                    //    //}
                    //    //else
                    //    //{
                    //    //    requestData.AppendLine("\"GDS\"");
                    //    //}
                    //}
                    //else
                    {
                        //requestData.AppendLine("\"6E\","); //Indigo
                        //requestData.AppendLine("\"G8\","); //Go Air
                        //requestData.AppendLine("\"SG\","); //SpiceJet
                        requestData.AppendLine("\"AK\","); //Air Asia
                        //requestData.AppendLine("\"LB\","); //Air Costa
                        requestData.AppendLine("\"IX\","); //AirIndia Express
                        requestData.AppendLine("\"G9\","); //Air Arabia
                        requestData.AppendLine("\"FZ\""); //FlyDubai
                        //requestData.AppendLine("\"GDS\""); //All other GDS airlines
                    }
                    requestData.AppendLine("]");
                    requestData.AppendLine("}");
                    #endregion

                    List<string> allowedSources = new List<string>();
                    allowedSources.Add("AK");
                    allowedSources.Add("IX");
                    allowedSources.Add("G9");
                    allowedSources.Add("FZ");

                    //Response resp = null;
                    XmlDocument doc = null;
                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirSearchRequestJSONRTN_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(requestData.ToString());
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(requestData.ToString(), "Request");
                            filePath = XmlPath + "TBOAirSearchRequestRTN_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Normal return Request. Error" + ex.ToString(), "");
                    }

                    doc = null;

                    Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "NormalReturn Request Started", "");
                    string response = GetResponse(requestData.ToString(), searchUrl);
                    Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "NormalReturn Got Response", "");

                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirSearchResponseJSONRTN_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(response);
                            sw.Close();

                            TextReader reader = new StringReader(response);
                            doc = JsonConvert.DeserializeXmlNode(response);

                            filePath = XmlPath + "TBOAirSearchResponseRTN_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Normal return Response. Error" + ex.ToString(), "");
                    }

                    if (doc != null)
                    {
                        XmlNode Error = doc.SelectSingleNode("Response/Error");
                        string errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                        if (errorMessage.Length <= 0)
                        {
                            XmlNodeList ResultNodes = doc.SelectNodes("Response/Results/Results");

                            List<string> prefAirlines = new List<string>();
                            if (Request.Segments[0].PreferredAirlines.Length > 0)
                            {
                                prefAirlines.AddRange(Request.Segments[0].PreferredAirlines);
                            }
                            if ((request.IsDomestic && type == 2))
                            {
                                List<XmlNode> outboundResults = new List<XmlNode>();
                                List<XmlNode> inboundResults = new List<XmlNode>();

                                List<XmlNode> filteredNodes = new List<XmlNode>();
                                foreach (XmlNode result in ResultNodes)
                                {
                                    string source = allowedSources.Find(delegate(string s) { return s == result.SelectSingleNode("AirlineCode").InnerText; });

                                    if (source != null && source.Length > 0)
                                    {
                                        filteredNodes.Add(result);
                                    }
                                }

                                foreach (XmlNode result in filteredNodes)
                                {
                                    if (result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Origin/Airport/AirportCode").InnerText == request.Segments[0].Origin)
                                    {
                                        if (Request.Segments[0].PreferredAirlines.Length > 0)
                                        {
                                            if (Request.Segments[0].PreferredAirlines[0] == result.SelectSingleNode("AirlineCode").InnerText)
                                            {
                                                outboundResults.Add(result);
                                            }
                                        }
                                        else
                                        {
                                            outboundResults.Add(result);
                                        }
                                    }
                                    if (request.Segments.Length > 1)
                                    {
                                        if (result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Origin/Airport/AirportCode").InnerText == request.Segments[1].Origin)
                                        {
                                            if (Request.Segments[0].PreferredAirlines.Length > 0)
                                            {
                                                if (Request.Segments[0].PreferredAirlines[0] == result.SelectSingleNode("AirlineCode").InnerText)
                                                {
                                                    inboundResults.Add(result);
                                                }
                                            }
                                            else
                                            {
                                                inboundResults.Add(result);
                                            }
                                        }
                                    }
                                }
                                if (outboundResults.Count > 0)
                                {
                                    rateOfExchange = exchangeRates[outboundResults[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];
                                }
                                try
                                {
                                    #region Domestic Results

                                    foreach (XmlNode obResult in outboundResults)
                                    {
                                        foreach (XmlNode ibResult in inboundResults)
                                        {
                                            SearchResult sr = new SearchResult();
                                            sr.IsLCC = true;
                                            sr.Currency = agentBaseCurrency;
                                            sr.JourneySellKey = obResult.SelectSingleNode("Source").InnerText;
                                            if (obResult.SelectSingleNode("BaggageAllowance") != null)
                                            {
                                                sr.BaggageIncludedInFare = obResult.SelectSingleNode("BaggageAllowance").InnerText;
                                            }
                                            //=====================================================Outbound begin===============================================//
                                            #region Outbound Result
                                            sr.GUID = obResult.SelectSingleNode("ResultIndex").InnerText;
                                            XmlNodeList fareBreakDownNodes = obResult.SelectNodes("FareBreakdown");
                                            List<Fare> fbdList = new List<Fare>();
                                            List<Fare> tbofbdList = new List<Fare>();
                                            foreach (XmlNode fbdNode in fareBreakDownNodes)
                                            {
                                                Fare fbd = new Fare();
                                                fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                fbd.SellingFare = fbd.TotalFare;
                                                fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                {
                                                    fbd.SupplierFare += Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                }
                                                fbdList.Add(fbd);

                                                Fare tbofbd = new Fare();
                                                tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                tbofbdList.Add(tbofbd);

                                            }
                                            sr.FareBreakdown = fbdList.ToArray();
                                            sr.TBOFareBreakdown = tbofbdList.ToArray();

                                            XmlNodeList fareRuleList = obResult.SelectNodes("FareRules");
                                            sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                            foreach (XmlNode fare in fareRuleList)
                                            {
                                                CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                sr.FareRules.Add(fr);
                                            }

                                            if (request.Type == SearchType.Return)
                                            {
                                                sr.Flights = new FlightInfo[2][];
                                            }
                                            else
                                            {
                                                sr.Flights = new FlightInfo[1][];
                                            }

                                            XmlNodeList segmentNodes = obResult.SelectNodes("Segments");

                                            BindSegments(segmentNodes, ref sr, obResult, null);

                                            sr.IsLCC = Convert.ToBoolean(obResult.SelectSingleNode("IsLCC").InnerText);
                                            sr.NonRefundable = (Convert.ToBoolean(obResult.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                            sr.ResultBookingSource = BookingSource.TBOAir;
                                            sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                            sr.ValidatingAirline = obResult.SelectSingleNode("ValidatingAirline").InnerText;
                                            sr.Price = new PriceAccounts();
                                            sr.Price.AccPriceType = PriceType.PublishedFare;
                                            sr.Price.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                            sr.Price.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.Price.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.Price.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                            sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes = obResult.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.Price.Currency = agentBaseCurrency;
                                            sr.Price.CurrencyCode = agentBaseCurrency;
                                            sr.Price.DecimalPoint = decimalValue;
                                            sr.Price.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                            sr.Price.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                            sr.Price.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.Price.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                            sr.Price.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                            sr.Price.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                            sr.Price.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                            sr.Price.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.Price.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.Price.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            sr.Price.SupplierPrice = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);

                                            sr.TBOPrice = new PriceAccounts();
                                            sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                            sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                            sr.TBOPrice.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.TBOPrice.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.TBOPrice.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                            sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes2 = ibResult.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes2)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.TBOPrice.Currency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.CurrencyCode = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.DecimalPoint = decimalValue;
                                            sr.TBOPrice.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                            sr.TBOPrice.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                            sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.TBOPrice.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                            sr.TBOPrice.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                            sr.TBOPrice.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                            sr.TBOPrice.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                            sr.TBOPrice.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.TBOPrice.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.TBOPrice.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.TBOPrice.TransactionFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            }

                                            if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.TBOPrice.TransactionFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            }
                                            else
                                            {
                                                sr.Price.TransactionFee = 0.0M;
                                            }

                                            sr.BaseFare = (double)sr.Price.BaseFare;
                                            sr.Tax = (double)(Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                            sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;
                                            #endregion
                                            //======================================================Outbound end================================================//

                                            //=====================================================Inbound Begin================================================//
                                            #region Inbound Result
                                            XmlNodeList fareBreakDownNodes20 = ibResult.SelectNodes("FareBreakdown");
                                            sr.GUID += "," + ibResult.SelectSingleNode("ResultIndex").InnerText;
                                            sr.JourneySellKey += "|" + ibResult.SelectSingleNode("Source").InnerText;
                                            if (ibResult.SelectSingleNode("BaggageAllowance") != null)
                                            {
                                                sr.BaggageIncludedInFare += "," + ibResult.SelectSingleNode("BaggageAllowance").InnerText;
                                            }
                                            foreach (XmlNode fbdNode in fareBreakDownNodes20)
                                            {
                                                foreach (Fare fbd in sr.FareBreakdown)
                                                {
                                                    if (fbd.PassengerType == (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText))
                                                    {
                                                        fbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                        fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                        fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                        fbd.TotalFare += fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                        fbd.SellingFare += fbd.TotalFare;                                                        
                                                        fbd.SupplierFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                        if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                        {
                                                            fbd.SupplierFare += Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                        }
                                                        break;
                                                    }
                                                }

                                                foreach (Fare tbofbd in sr.TBOFareBreakdown)
                                                {
                                                    if (tbofbd.PassengerType == (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText))
                                                    {
                                                        tbofbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                        tbofbd.BaseFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                        tbofbd.PassengerCount += Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                        tbofbd.TotalFare += tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                        break;
                                                    }
                                                }
                                            }

                                            XmlNodeList fareRuleList20 = ibResult.SelectNodes("FareRules");

                                            foreach (XmlNode fare in fareRuleList20)
                                            {
                                                CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                sr.FareRules.Add(fr);
                                            }

                                            XmlNodeList segmentNodes20 = ibResult.SelectNodes("Segments");

                                            BindSegments(segmentNodes20, ref sr, null, ibResult);

                                            sr.IsLCC = Convert.ToBoolean(ibResult.SelectSingleNode("IsLCC").InnerText);
                                            sr.NonRefundable = (Convert.ToBoolean(ibResult.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                            sr.ResultBookingSource = BookingSource.TBOAir;
                                            sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                            sr.ValidatingAirline = ibResult.SelectSingleNode("ValidatingAirline").InnerText;

                                            sr.Price.AdditionalTxnFee += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                            sr.Price.AgentCommission += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.Price.AgentPLB += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.Price.BaseFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;

                                            XmlNodeList chargeNodes20 = ibResult.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes20)
                                            {
                                                foreach (ChargeBreakUp cbu in sr.Price.ChargeBU)
                                                {

                                                    switch (cb.SelectSingleNode("key").InnerText)
                                                    {
                                                        case "TBOMARKUP":
                                                            if (cbu.ChargeType == ChargeType.TBOMarkup)
                                                            {
                                                                cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                            }
                                                            break;
                                                        case "OTHERCHARGES":
                                                            if (cbu.ChargeType == ChargeType.OtherCharges)
                                                            {
                                                                cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                            }
                                                            break;
                                                        case "CONVENIENCECHARGES":
                                                            if (cbu.ChargeType == ChargeType.ConvenienceCharge)
                                                            {
                                                                cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                            }
                                                            break;
                                                    }
                                                }
                                            }
                                            sr.Price.Currency = agentBaseCurrency;
                                            sr.Price.CurrencyCode = agentBaseCurrency;
                                            sr.Price.DecimalPoint = decimalValue;
                                            sr.Price.Tax += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                            sr.Price.YQTax += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                            sr.Price.IncentiveEarned += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.Price.NetFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                            sr.Price.PublishedFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                            sr.Price.SServiceFee += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                            sr.Price.OtherCharges += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                            sr.Price.TdsCommission += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.Price.TDSPLB += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.Price.TDSIncentive += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            sr.Price.SupplierPrice += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);

                                            sr.TBOPrice = new PriceAccounts();
                                            sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                            sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                            sr.TBOPrice.AgentCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.TBOPrice.AgentPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.TBOPrice.BaseFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                            sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes3 = ibResult.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes3)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.TBOPrice.Currency = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.CurrencyCode = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.DecimalPoint = decimalValue;
                                            sr.TBOPrice.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                            sr.TBOPrice.YQTax = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                            sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.TBOPrice.NetFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                            sr.TBOPrice.PublishedFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                            sr.TBOPrice.SServiceFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                            sr.TBOPrice.OtherCharges = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                            sr.TBOPrice.TdsCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.TBOPrice.TDSPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.TBOPrice.TDSIncentive = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            if (ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.TBOPrice.TransactionFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            }

                                            if (ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.Price.TransactionFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            }
                                            else
                                            {
                                                sr.Price.TransactionFee = 0.0M;
                                            }
                                            sr.BaseFare += (double)sr.Price.BaseFare;
                                            sr.Tax += (double)(Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                            sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;
                                            #endregion
                                            //======================================================Inbound end=================================================//
                                            //Adding logic not to bind LCC to GDS
                                            sr.FareType = "NormalReturn";
                                            
                                            Airline oAirline = new Airline();
                                            Airline iAirline = new Airline();
                                            try
                                            {
                                                if (sr.Flights.Length == 2)
                                                {
                                                    oAirline.Load(sr.Flights[0][0].Airline);
                                                    iAirline.Load(sr.Flights[1][0].Airline);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir)Failed to load airline. Onward : " + sr.Flights[0][0].Airline + " Return : " + sr.Flights[1][0].Airline + " Error : " + ex.ToString(), "");
                                            }

                                            if (oAirline.IsLCC == iAirline.IsLCC) // Normal retrun domestic. Should only combine GDS-GDS and LCC-LCC
                                            {
                                                results.Add(sr);
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                catch (Exception ex)
                                {
                                    Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir)Failed to bind Domestic results. " + ex.ToString(), "");
                                }
                            }
                            else
                            {
                                #region GDS and SpecialReturn Results

                                List<XmlNode> filteredNodes = new List<XmlNode>();
                                foreach (XmlNode result in ResultNodes)
                                {
                                    string source = allowedSources.Find(delegate(string s) { return s == result.SelectSingleNode("AirlineCode").InnerText; });

                                    if (source != null && source.Length > 0)
                                    {
                                        filteredNodes.Add(result);
                                    }
                                }

                                //foreach (XmlNode result in ResultNodes)
                                foreach(XmlNode result in filteredNodes)
                                {
                                    SearchResult sr = null;
                                    if (Request.Segments[0].PreferredAirlines.Length > 0)
                                    {
                                        if (Request.Segments[0].PreferredAirlines[0] == result.SelectSingleNode("AirlineCode").InnerText)
                                        {
                                            sr = new SearchResult();
                                        }
                                    }
                                    else
                                    {
                                        sr = new SearchResult();
                                    }
                                    if (sr != null)
                                    {
                                        if (type == 5)
                                        {
                                            sr.IsLCC = true;
                                            sr.FareType = "LCCSpecialReturn";
                                        }
                                        else
                                        {
                                            sr.FareType = "Normal (GDS)";
                                        }
                                        if (result.SelectSingleNode("BaggageAllowance") != null)
                                        {
                                            sr.BaggageIncludedInFare = result.SelectSingleNode("BaggageAllowance").InnerText;
                                        }
                                        sr.JourneySellKey = result.SelectSingleNode("Source").InnerText;
                                        sr.Currency = agentBaseCurrency;
                                        rateOfExchange = exchangeRates[result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];
                                        sr.GUID = result.SelectSingleNode("ResultIndex").InnerText;
                                        XmlNodeList fareBreakDownNodes = result.SelectNodes("FareBreakdown");
                                        double TransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * (double)rateOfExchange : 0;
                                        double TBOTransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) : 0;
                                        double TBOotherCharges = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);

                                        double OtherCharges = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * (double)rateOfExchange;
                                        List<Fare> fbdList = new List<Fare>();
                                        List<Fare> tbofbdList = new List<Fare>();
                                        foreach (XmlNode fbdNode in fareBreakDownNodes)
                                        {
                                            Fare fbd = new Fare();
                                            fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                            fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                            fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                            fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                            fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                            fbd.SellingFare = fbd.TotalFare;
                                            fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);

                                            fbdList.Add(fbd);

                                            Fare tbofbd = new Fare();
                                            tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                            tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                            tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                            tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                            tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                            tbofbdList.Add(tbofbd);
                                        }
                                        sr.FareBreakdown = fbdList.ToArray();
                                        sr.TBOFareBreakdown = tbofbdList.ToArray();

                                        XmlNodeList fareRuleList = result.SelectNodes("FareRules");
                                        sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                        foreach (XmlNode fare in fareRuleList)
                                        {
                                            CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                            fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                            fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                            fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                            fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                            fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                            fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                            sr.FareRules.Add(fr);
                                        }
                                        XmlNodeList segmentNodes = result.SelectNodes("Segments");
                                        if (request.Type == SearchType.Return)
                                        {
                                            sr.Flights = new FlightInfo[2][];
                                        }
                                        else if (request.Type == SearchType.MultiWay)
                                        {
                                            sr.Flights = new FlightInfo[segmentNodes.Count][];
                                        }
                                        else
                                        {
                                            sr.Flights = new FlightInfo[1][];
                                        }


                                        if (request.Type != SearchType.MultiWay)
                                        {
                                            BindSegments(segmentNodes, ref sr, result);
                                        }
                                        else //MultiCity 
                                        {
                                            //int tripIndicator = 1;

                                            BindSegments(segmentNodes, ref sr, result);
                                        }
                                        sr.IsLCC = Convert.ToBoolean(result.SelectSingleNode("IsLCC").InnerText);
                                        sr.NonRefundable = (Convert.ToBoolean(result.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                        sr.ResultBookingSource = BookingSource.TBOAir;
                                        sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                        sr.ValidatingAirline = result.SelectSingleNode("ValidatingAirline").InnerText;
                                        sr.Price = new PriceAccounts();
                                        sr.Price.AccPriceType = PriceType.PublishedFare;
                                        sr.Price.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                        sr.Price.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                        sr.Price.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                        sr.Price.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                        sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                        XmlNodeList chargeNodes = result.SelectNodes("Fare/ChargeBU");
                                        foreach (XmlNode cb in chargeNodes)
                                        {
                                            ChargeBreakUp cbu = new ChargeBreakUp();
                                            cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                            switch (cb.SelectSingleNode("key").InnerText)
                                            {
                                                case "TBOMARKUP":
                                                    cbu.ChargeType = ChargeType.TBOMarkup;
                                                    break;
                                                case "OTHERCHARGES":
                                                    cbu.ChargeType = ChargeType.OtherCharges;
                                                    break;
                                                case "CONVENIENCECHARGES":
                                                    cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                    break;
                                            }
                                        }
                                        sr.Price.Currency = agentBaseCurrency;
                                        sr.Price.CurrencyCode = agentBaseCurrency;
                                        sr.Price.DecimalPoint = decimalValue;
                                        sr.Price.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                        sr.Price.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                        sr.Price.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                        sr.Price.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                        sr.Price.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                        sr.Price.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                        sr.Price.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                        sr.Price.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                        sr.Price.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                        sr.Price.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                        sr.Price.SupplierPrice = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                        if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                        {
                                            sr.Price.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                        }
                                        //sr.Price.Tax += sr.Price.OtherCharges + sr.Price.TransactionFee;

                                        sr.TBOPrice = new PriceAccounts();
                                        sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                        sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                        sr.TBOPrice.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                        sr.TBOPrice.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                        sr.TBOPrice.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                        sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                        XmlNodeList chargeNodes2 = result.SelectNodes("Fare/ChargeBU");
                                        foreach (XmlNode cb in chargeNodes2)
                                        {
                                            ChargeBreakUp cbu = new ChargeBreakUp();
                                            cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                            switch (cb.SelectSingleNode("key").InnerText)
                                            {
                                                case "TBOMARKUP":
                                                    cbu.ChargeType = ChargeType.TBOMarkup;
                                                    break;
                                                case "OTHERCHARGES":
                                                    cbu.ChargeType = ChargeType.OtherCharges;
                                                    break;
                                                case "CONVENIENCECHARGES":
                                                    cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                    break;
                                            }
                                        }
                                        sr.TBOPrice.Currency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                        sr.TBOPrice.CurrencyCode = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                        sr.TBOPrice.DecimalPoint = decimalValue;
                                        sr.TBOPrice.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                        sr.TBOPrice.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                        sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                        sr.TBOPrice.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                        sr.TBOPrice.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                        sr.TBOPrice.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                        sr.TBOPrice.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                        sr.TBOPrice.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                        sr.TBOPrice.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                        sr.TBOPrice.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                        if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                        {
                                            sr.TBOPrice.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                        }

                                        sr.BaseFare = (double)sr.Price.BaseFare;
                                        sr.Tax = (double)(Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                        sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;

                                        results.Add(sr);
                                    }
                                }
                                #endregion
                            }

                        }
                        SearchResult[] finalResult = results.ToArray();
                        if (finalResult.Length > 0)
                        {
                            Array.Sort<SearchResult>(finalResult, finalResult[0]);
                        }
                        CombineResults.AddRange(results);
                    }
                    
                }
                eventFlag[(int)eventNumber].Set();
                Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Normal Return Ended" + DateTime.Now, "");
            }
            catch (Exception ex)
            {
                eventFlag[(int)eventNumber].Set();
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Search. Error : " + ex.ToString(), "");
            }

        }

        /// <summary>
        /// To fetch special return fares for GDS. Results will come as combinations. 
        /// Displaying fares with breakdown.
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchSpecialReturnGDS(object eventNumber)
        {
            Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Special Return  GDS Started" + DateTime.Now, "");
            SearchRequest request = new SearchRequest();
            request = Request;
            List<SearchResult> results = new List<SearchResult>();
            try
            {
                AuthenticationResponse ar = Authenticate();
                if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                {
                    //AgencyBalance balance = GetAgencyBalance(ar);

                    #region JSON Search Request
                    int type = 5;//Return


                    StringBuilder requestData = new StringBuilder();
                    requestData.AppendLine("{");
                    requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                    requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                    requestData.AppendLine("\"AdultCount\": \"" + request.AdultCount + "\",");
                    requestData.AppendLine("\"ChildCount\": \"" + request.ChildCount + "\",");
                    requestData.AppendLine("\"InfantCount\": \"" + request.InfantCount + "\",");
                    if (request.MaxStops == "-1")
                    {
                        requestData.AppendLine("\"DirectFlight\": \"false\",");
                        requestData.AppendLine("\"OneStopFlight\": \"false\",");
                    }
                    else if (request.MaxStops == "0")
                    {
                        requestData.AppendLine("\"DirectFlight\": \"true\",");
                        requestData.AppendLine("\"OneStopFlight\": \"false\",");
                    }
                    else if (request.MaxStops == "1")
                    {
                        requestData.AppendLine("\"DirectFlight\": \"false\",");
                        requestData.AppendLine("\"OneStopFlight\": \"true\",");
                    }
                    else
                    {
                        requestData.AppendLine("\"DirectFlight\": \"false\",");
                        requestData.AppendLine("\"OneStopFlight\": \"false\",");
                    }
                    requestData.AppendLine("\"JourneyType\": \"" + type + "\",");
                    if (request.Segments[0].PreferredAirlines.Length == 0)
                    {
                        requestData.AppendLine("\"PreferredAirlines\": null,");
                    }
                    else
                    {
                        requestData.AppendLine("\"PreferredAirlines\": [");
                        //requestData.AppendLine("{");
                        string pa = string.Empty;
                        foreach (string airline in request.Segments[0].PreferredAirlines)
                        {
                            if (pa.Length > 0)
                            {
                                pa += "," + "\"" + airline + "\"";
                            }
                            else
                            {
                                pa = "\"" + airline + "\"";
                            }
                        }
                        requestData.AppendLine(pa);
                        //requestData.AppendLine("}");
                        requestData.AppendLine("],");
                    }
                    requestData.AppendLine("\"Segments\": [");

                    foreach (FlightSegment segment in request.Segments)
                    {
                        if (segment.Origin != null)
                        {
                            requestData.AppendLine("{");
                            requestData.AppendLine("\"Origin\": \"" + segment.Origin + "\",");
                            requestData.AppendLine("\"Destination\": \"" + segment.Destination + "\",");
                            requestData.AppendLine("\"FlightCabinClass\": \"" + (int)segment.flightCabinClass + "\",");
                            requestData.AppendLine("\"PreferredDepartureTime\": \"" + segment.PreferredDepartureTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\",");
                            requestData.AppendLine("\"PreferredArrivalTime\": \"" + segment.PreferredArrivalTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\"");
                            if (segment == request.Segments[request.Segments.Length - 1])
                            {
                                requestData.AppendLine("}");
                            }
                            else
                            {
                                requestData.AppendLine("},");
                            }
                        }
                    }
                    requestData.AppendLine("],");
                    requestData.AppendLine("\"Sources\": [");

                    //Passing only GDS Sources.
                    requestData.AppendLine("\"GDS\"");

                    requestData.AppendLine("]");
                    requestData.AppendLine("}");
                    #endregion

                    //Response resp = null;
                    XmlDocument doc = null;
                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirSearchRequestJSONRTNGDS_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(requestData.ToString());
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(requestData.ToString(), "Request");
                            filePath = XmlPath + "TBOAirSearchRequestRTNGDS_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save SpecialReturnGDS Request. Error" + ex.ToString(), "");
                    }
                    doc = null;
                    Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "Special Return GDS Request Started", "");
                    string response = GetResponse(requestData.ToString(), searchUrl);
                    Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "Special Return GDS Got Response", "");
                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirSearchResponseJSONRTNGDS_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(response);
                            sw.Close();

                            TextReader reader = new StringReader(response);
                            doc = JsonConvert.DeserializeXmlNode(response);

                            filePath = XmlPath + "TBOAirSearchResponseRTNGDS_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save SpecialReturnGDS Response. Error" + ex.ToString(), "");
                    }
                    if (doc != null)
                    {
                        XmlNode Error = doc.SelectSingleNode("Response/Error");
                        string errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                        if (errorMessage.Length <= 0)
                        {
                            XmlNodeList ResultNodes = doc.SelectNodes("Response/Results/Results");
                            
                            #region GDS and SpecialReturn Results
                            foreach (XmlNode result in ResultNodes)
                            {                              
                                Airline airline = new Airline();
                                airline.Load(result.SelectSingleNode("ValidatingAirline").InnerText);

                                if (airline.TBOAirAllowed)
                                {
                                    SearchResult sr = new SearchResult();
                                    if (type == 5)
                                    {
                                        sr.IsLCC = true;
                                        sr.FareType = "LCCSpecialReturn";
                                    }
                                    else
                                    {
                                        sr.FareType = "Normal (GDS)";
                                    }
                                    sr.Currency = agentBaseCurrency;
                                    sr.JourneySellKey = result.SelectSingleNode("Source").InnerText;
                                    rateOfExchange = exchangeRates[result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];
                                    sr.GUID = result.SelectSingleNode("ResultIndex").InnerText;
                                    double TransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * (double)rateOfExchange : 0;
                                    double TBOTransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) : 0;
                                    double TBOotherCharges = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);

                                    double OtherCharges = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * (double)rateOfExchange;
                                    XmlNodeList fareBreakDownNodes = result.SelectNodes("FareBreakdown");
                                    List<Fare> fbdList = new List<Fare>();
                                    List<Fare> tbofbdList = new List<Fare>();
                                    foreach (XmlNode fbdNode in fareBreakDownNodes)
                                    {
                                        Fare fbd = new Fare();
                                        fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                        fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                        fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                        fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                        fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                        fbd.SellingFare = fbd.TotalFare;
                                        fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);

                                        fbdList.Add(fbd);

                                        Fare tbofbd = new Fare();
                                        tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                        tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                        tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                        tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                        tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                        tbofbdList.Add(tbofbd);

                                    }
                                    sr.FareBreakdown = fbdList.ToArray();
                                    sr.TBOFareBreakdown = tbofbdList.ToArray();

                                    XmlNodeList fareRuleList = result.SelectNodes("FareRules");
                                    sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                    foreach (XmlNode fare in fareRuleList)
                                    {
                                        CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                        fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                        fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                        fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                        fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                        fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                        fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                        sr.FareRules.Add(fr);
                                    }

                                    if (request.Type == SearchType.Return)
                                    {
                                        sr.Flights = new FlightInfo[2][];
                                    }
                                    else
                                    {
                                        sr.Flights = new FlightInfo[1][];
                                    }
                                    XmlNodeList segmentNodes = result.SelectNodes("Segments");

                                    BindSegments(segmentNodes, ref sr, result);

                                    if (result.SelectSingleNode("BaggageAllowance") != null)
                                    {
                                        sr.BaggageIncludedInFare = result.SelectSingleNode("BaggageAllowance").InnerText;
                                    }
                                    sr.IsLCC = Convert.ToBoolean(result.SelectSingleNode("IsLCC").InnerText);
                                    sr.NonRefundable = (Convert.ToBoolean(result.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                    sr.ResultBookingSource = BookingSource.TBOAir;
                                    sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                    sr.ValidatingAirline = result.SelectSingleNode("ValidatingAirline").InnerText;

                                    sr.Price = new PriceAccounts();
                                    sr.Price.AccPriceType = PriceType.PublishedFare;
                                    sr.Price.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                    sr.Price.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                    sr.Price.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                    sr.Price.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                    sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                    XmlNodeList chargeNodes = result.SelectNodes("Fare/ChargeBU");
                                    foreach (XmlNode cb in chargeNodes)
                                    {
                                        ChargeBreakUp cbu = new ChargeBreakUp();
                                        cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                        switch (cb.SelectSingleNode("key").InnerText)
                                        {
                                            case "TBOMARKUP":
                                                cbu.ChargeType = ChargeType.TBOMarkup;
                                                break;
                                            case "OTHERCHARGES":
                                                cbu.ChargeType = ChargeType.OtherCharges;
                                                break;
                                            case "CONVENIENCECHARGES":
                                                cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                break;
                                        }
                                    }
                                    sr.Price.Currency = agentBaseCurrency;
                                    sr.Price.CurrencyCode = agentBaseCurrency;
                                    sr.Price.DecimalPoint = decimalValue;
                                    sr.Price.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                    sr.Price.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                    sr.Price.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                    sr.Price.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                    sr.Price.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                    sr.Price.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                    sr.Price.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                    sr.Price.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                    sr.Price.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                    sr.Price.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                    if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                    {
                                        sr.Price.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                    }
                                    //sr.Price.Tax += sr.Price.OtherCharges + sr.Price.TransactionFee;
                                    sr.Price.SupplierPrice = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                    sr.Price.SupplierCurrency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;

                                    sr.TBOPrice = new PriceAccounts();
                                    sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                    sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                    sr.TBOPrice.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                    sr.TBOPrice.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                    sr.TBOPrice.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                    sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                    XmlNodeList chargeNodes2 = result.SelectNodes("Fare/ChargeBU");
                                    foreach (XmlNode cb in chargeNodes2)
                                    {
                                        ChargeBreakUp cbu = new ChargeBreakUp();
                                        cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                        switch (cb.SelectSingleNode("key").InnerText)
                                        {
                                            case "TBOMARKUP":
                                                cbu.ChargeType = ChargeType.TBOMarkup;
                                                break;
                                            case "OTHERCHARGES":
                                                cbu.ChargeType = ChargeType.OtherCharges;
                                                break;
                                            case "CONVENIENCECHARGES":
                                                cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                break;
                                        }
                                    }
                                    sr.TBOPrice.Currency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                    sr.TBOPrice.CurrencyCode = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                    sr.TBOPrice.DecimalPoint = decimalValue;
                                    sr.TBOPrice.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                    sr.TBOPrice.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                    sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                    sr.TBOPrice.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                    sr.TBOPrice.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                    sr.TBOPrice.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                    sr.TBOPrice.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                    sr.TBOPrice.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                    sr.TBOPrice.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                    sr.TBOPrice.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                    if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                    {
                                        sr.TBOPrice.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                    }
                                    sr.FareType = "SpecialReturnGDS";
                                    sr.BaseFare = (double)sr.Price.BaseFare;
                                    sr.Tax = (double)(Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                    sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;
                                    
                                    results.Add(sr);
                                }
                            }
                            #endregion
                            SearchResult[] finalResult = results.ToArray();
                            if (finalResult.Length > 0)
                            {
                                Array.Sort<SearchResult>(finalResult, finalResult[0]);
                            }
                            CombineResults.AddRange(results);
                        }
                    }
                }
                eventFlag[(int)eventNumber].Set();
                Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Special Return GDS Ended" + DateTime.Now, "");
            }
            catch (Exception ex)
            {
                eventFlag[(int)eventNumber].Set();
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Search. Error : " + ex.ToString(), "");
            }
        }


        /// <summary>
        /// To get Farerules
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public List<FareRule> GetFareRule(SearchResult result)
        {
            List<FareRule> fareRuleList = new List<FareRule>();

            AuthenticationResponse ar = Authenticate();            

            try
            {
                if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                {
                    if (result.GUID.Split(',').Length <= 1)
                    {
                        StringBuilder requestData = new StringBuilder();
                        requestData.AppendLine("{");
                        requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        requestData.AppendLine("\"TraceId\": \"" + result.FareSellKey + "\",");
                        requestData.AppendLine("\"ResultIndex\": \"" + result.GUID + "\"");
                        requestData.AppendLine("}");

                        try
                        {
                            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                            {
                                string filePath = XmlPath + "TBOAirGetFareRuleRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(requestData.ToString());
                                sw.Close();

                                XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString());
                                filePath = XmlPath + "TBOAirGetRuleRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                doc1.Save(filePath);
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetFareRule Request. Error" + ex.ToString(), "");
                        }

                        string response = GetResponse(requestData.ToString(), fareRuleUrl);

                        XmlDocument doc = null;

                        try
                        {
                            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                            {
                                string filePath = XmlPath + "TBOAirGetFareRuleResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(response);
                                sw.Close();

                                doc = JsonConvert.DeserializeXmlNode(response);
                                filePath = XmlPath + "TBOAirGetRuleResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                doc.Save(filePath);
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetFareRule Response. Error" + ex.ToString(), "");
                        }

                        if (doc != null)
                        {
                            XmlNode Error = doc.SelectSingleNode("Response/Error");
                            string errorMessage = "";
                            errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                            if (errorMessage.Length <= 0)
                            {
                                XmlNodeList fareRuleNodes = doc.SelectNodes("Response/FareRules");

                                foreach (XmlNode fareRule in fareRuleNodes)
                                {
                                    FareRule fr = new FareRule();
                                    fr.Airline = fareRule.SelectSingleNode("Airline").InnerText;
                                    fr.Destination = fareRule.SelectSingleNode("Destination").InnerText;
                                    fr.FareBasisCode = fareRule.SelectSingleNode("FareBasisCode").InnerText;
                                    fr.FareRestriction = fareRule.SelectSingleNode("FareRestriction").InnerText;
                                    fr.FareRuleDetail = fareRule.SelectSingleNode("FareRuleDetail").InnerText;
                                    fr.Origin = fareRule.SelectSingleNode("Origin").InnerText;

                                    fareRuleList.Add(fr);
                                }
                            }
                        }
                    }
                    else
                    {
                        string[] guids = result.GUID.Split(',');

                        foreach (string guid in guids)
                        {
                            StringBuilder requestData = new StringBuilder();
                            requestData.AppendLine("{");
                            requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                            requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                            requestData.AppendLine("\"TraceId\": \"" + result.FareSellKey + "\",");
                            requestData.AppendLine("\"ResultIndex\": \"" + guid + "\",");
                            requestData.AppendLine("}");

                            try
                            {
                                if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                                {
                                    string filePath = XmlPath + "TBOAirGetFareRuleRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                                    StreamWriter sw = new StreamWriter(filePath);
                                    sw.Write(requestData.ToString());
                                    sw.Close();

                                    XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString());
                                    filePath = XmlPath + "TBOAirGetRuleRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                    doc1.Save(filePath);
                                }
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetFareRule Request. Error" + ex.ToString(), "");
                            }

                            string response = GetResponse(requestData.ToString(), fareRuleUrl);

                            XmlDocument doc = null;

                            try
                            {
                                if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                                {
                                    string filePath = XmlPath + "TBOAirGetFareRuleResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                                    StreamWriter sw = new StreamWriter(filePath);
                                    sw.Write(response);
                                    sw.Close();

                                    doc = JsonConvert.DeserializeXmlNode(response);
                                    filePath = XmlPath + "TBOAirGetRuleResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                    doc.Save(filePath);
                                }
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetFareRule Response. Error" + ex.ToString(), "");
                            }

                            if (doc != null)
                            {
                                XmlNode Error = doc.SelectSingleNode("Response/Error");
                                string errorMessage = "";
                                errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                                if (errorMessage.Length <= 0)
                                {
                                    XmlNodeList fareRuleNodes = doc.SelectNodes("Response/FareRules");

                                    foreach (XmlNode fareRule in fareRuleNodes)
                                    {
                                        FareRule fr = new FareRule();
                                        fr.Airline = fareRule.SelectSingleNode("Airline").InnerText;
                                        fr.Destination = fareRule.SelectSingleNode("Destination").InnerText;
                                        fr.FareBasisCode = fareRule.SelectSingleNode("FareBasisCode").InnerText;
                                        fr.FareRestriction = fareRule.SelectSingleNode("FareRestriction").InnerText;
                                        fr.FareRuleDetail = fareRule.SelectSingleNode("FareRuleDetail").InnerText;
                                        fr.Origin = fareRule.SelectSingleNode("Origin").InnerText;

                                        fareRuleList.Add(fr);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to get Fare Rule. Error : " + ex.ToString(), "");
                throw ex;
            }

            return fareRuleList;
        }

        /// <summary>
        /// Method used to check Price changed or not
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public Fare[] GetFareQuote(ref SearchResult result, ref string errorMessage)
        {
            List<Fare> fareList = new List<Fare>();
            List<Fare> tboFares = new List<Fare>();
            XmlDocument doc = null;
            //Sending to fare Quote requests for Combined results. and merging the responses.

            if (result.GUID.Contains(',') && result.FareType.ToLower() == "normalreturn") // Normal return combined result set
            {
                string[] Guids = result.GUID.Split(','); //Search Result indecies.                                

                PriceAccounts Price = new PriceAccounts();
                PriceAccounts TBOPrice = new PriceAccounts();
                int priceChanged = 0;
                for (int i = 0; i < Guids.Length;i++ )
                {                    
                    doc = TempGetFareQuote(Guids[i], result.FareSellKey);

                    if (doc != null)
                    {
                        XmlNode Error = doc.SelectSingleNode("Response/Error");                       
                        
                        if (Error.SelectSingleNode("ErrorCode").InnerText == "0")
                        {
                            XmlNodeList fareNodes = doc.SelectNodes("Response/Results/FareBreakdown");

                            rateOfExchange = exchangeRates[fareNodes[0].SelectSingleNode("Currency").InnerText];

                            if (doc.SelectSingleNode("Response/IsPriceChanged").InnerText.ToLower() == "true")
                            {
                                priceChanged++;
                                foreach (XmlNode fbdNode in fareNodes)
                                {
                                    bool used = false;
                                    Fare fbd = null;
                                    if (fareList.Count > 0)
                                    {
                                        fbd = fareList.Find(delegate(Fare f) { return Convert.ToInt32(f.PassengerType) == Convert.ToInt32(fbdNode.SelectSingleNode("PassengerType").InnerText); });

                                        if (fbd == null)
                                        {
                                            fbd = new Fare();
                                        }
                                        else
                                        {
                                            used = true;
                                        }
                                    }
                                    else
                                    {
                                        fbd = new Fare();
                                    }
                                    fbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                    fbd.BaseFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                    double baseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                    fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                    fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                    fbd.TotalFare += baseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                    fbd.SellingFare += fbd.TotalFare;
                                    fbd.SupplierFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + (double)(Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText));
                                    if (!used)
                                    {
                                        fareList.Add(fbd);
                                    }

                                    Fare tbofbd = new Fare();
                                    if (tboFares.Count > 0)
                                    {
                                        tbofbd = tboFares.Find(delegate(Fare f) { return Convert.ToInt32(f.PassengerType) == Convert.ToInt32(fbdNode.SelectSingleNode("PassengerType").InnerText); });

                                        if (tbofbd == null)
                                        {
                                            tbofbd = new Fare();
                                        }
                                    }
                                    else
                                    {
                                        tbofbd = new Fare();
                                    }
                                    tbofbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                    tbofbd.BaseFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                    tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                    tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                    tbofbd.TotalFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                    tbofbd.SellingFare += tbofbd.TotalFare;
                                    tbofbd.SupplierFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + (double)(Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText));
                                    if (!used)
                                    {
                                        tboFares.Add(tbofbd);
                                    }
                                }
                                result.TBOFareBreakdown = tboFares.ToArray();

                                XmlNodeList fares = doc.SelectNodes("Response/Results/Fare");

                                if (fares != null)
                                {
                                    foreach (XmlNode fare in fares)
                                    {
                                        if (i == 1)
                                        {
                                            Price.OtherCharges += Convert.ToDecimal(fare.SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                            Price.TdsCommission += Convert.ToDecimal(fare.SelectSingleNode("TdsOnCommission").InnerText);
                                            Price.AgentCommission += Convert.ToDecimal(fare.SelectSingleNode("CommissionEarned").InnerText);
                                            Price.IncentiveEarned += Convert.ToDecimal(fare.SelectSingleNode("IncentiveEarned").InnerText);
                                            Price.TDSIncentive += Convert.ToDecimal(fare.SelectSingleNode("TdsOnIncentive").InnerText);
                                            Price.SServiceFee += Convert.ToDecimal(fare.SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                            Price.SupplierPrice += Convert.ToDecimal(fare.SelectSingleNode("PublishedFare").InnerText);
                                            Price.SupplierCurrency = fare.SelectSingleNode("Currency").InnerText;
                                            Price.YQTax += Convert.ToDecimal(fare.SelectSingleNode("YQTax").InnerText);
                                            Price.AgentPLB += Convert.ToDecimal(fare.SelectSingleNode("PLBEarned").InnerText);
                                            Price.TDSPLB += Convert.ToDecimal(fare.SelectSingleNode("TdsOnPLB").InnerText);
                                            if (fare.SelectSingleNode("TransactionFee") != null)
                                            {
                                                Price.TransactionFee += Convert.ToDecimal(fare.SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                            }

                                            TBOPrice.OtherCharges += Convert.ToDecimal(fare.SelectSingleNode("OtherCharges").InnerText);
                                            TBOPrice.TdsCommission += Convert.ToDecimal(fare.SelectSingleNode("TdsOnCommission").InnerText);
                                            TBOPrice.AgentCommission += Convert.ToDecimal(fare.SelectSingleNode("CommissionEarned").InnerText);
                                            TBOPrice.IncentiveEarned += Convert.ToDecimal(fare.SelectSingleNode("IncentiveEarned").InnerText);
                                            TBOPrice.TDSIncentive += Convert.ToDecimal(fare.SelectSingleNode("TdsOnIncentive").InnerText);
                                            TBOPrice.SeviceTax += Convert.ToDecimal(fare.SelectSingleNode("ServiceFee").InnerText);
                                        }
                                        else
                                        {
                                            Price.OtherCharges = Convert.ToDecimal(fare.SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                            Price.TdsCommission = Convert.ToDecimal(fare.SelectSingleNode("TdsOnCommission").InnerText);
                                            Price.AgentCommission = Convert.ToDecimal(fare.SelectSingleNode("CommissionEarned").InnerText);
                                            Price.IncentiveEarned = Convert.ToDecimal(fare.SelectSingleNode("IncentiveEarned").InnerText);
                                            Price.TDSIncentive = Convert.ToDecimal(fare.SelectSingleNode("TdsOnIncentive").InnerText);
                                            Price.SServiceFee = Convert.ToDecimal(fare.SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                            Price.SupplierPrice = Convert.ToDecimal(fare.SelectSingleNode("PublishedFare").InnerText);
                                            Price.SupplierCurrency = fare.SelectSingleNode("Currency").InnerText;
                                            Price.YQTax = Convert.ToDecimal(fare.SelectSingleNode("YQTax").InnerText);
                                            Price.AgentPLB = Convert.ToDecimal(fare.SelectSingleNode("PLBEarned").InnerText);
                                            Price.TDSPLB = Convert.ToDecimal(fare.SelectSingleNode("TdsOnPLB").InnerText);
                                            if (fare.SelectSingleNode("TransactionFee") != null)
                                            {
                                                Price.TransactionFee = Convert.ToDecimal(fare.SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                            }

                                            TBOPrice.OtherCharges = Convert.ToDecimal(fare.SelectSingleNode("OtherCharges").InnerText);
                                            TBOPrice.TdsCommission = Convert.ToDecimal(fare.SelectSingleNode("TdsOnCommission").InnerText);
                                            TBOPrice.AgentCommission = Convert.ToDecimal(fare.SelectSingleNode("CommissionEarned").InnerText);
                                            TBOPrice.IncentiveEarned = Convert.ToDecimal(fare.SelectSingleNode("IncentiveEarned").InnerText);
                                            TBOPrice.TDSIncentive = Convert.ToDecimal(fare.SelectSingleNode("TdsOnIncentive").InnerText);
                                            TBOPrice.SeviceTax = Convert.ToDecimal(fare.SelectSingleNode("ServiceFee").InnerText);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                fareList = new List<Fare>(); 
                            }
                        }
                        else
                        {
                            fareList = new List<Fare>();
                            errorMessage = "Price has been revised from Supplier end. Please search again";
                            //When fareQuote failed from Supplier side. Throwing an exception .
                            Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir)" + errorMessage, "");
                            return fareList.ToArray();
                        }
                    }
                }
                if (priceChanged != Guids.Length)
                {
                    fareList = new List<Fare>();
                }
                else
                {
                    result.Price.OtherCharges = Price.OtherCharges;
                    result.Price.TdsCommission = Price.TdsCommission;
                    result.Price.AgentCommission = Price.AgentCommission;
                    result.Price.IncentiveEarned = Price.IncentiveEarned;
                    result.Price.TDSIncentive = Price.TDSIncentive;
                    result.Price.SServiceFee = Price.SServiceFee;
                    result.Price.SupplierPrice = Price.SupplierPrice;
                    result.Price.YQTax = Price.YQTax;
                    result.Price.AgentPLB = Price.AgentPLB;
                    result.Price.TDSPLB = Price.TDSPLB;
                    result.Price.TransactionFee = Price.TransactionFee;

                    result.TBOPrice.OtherCharges = TBOPrice.OtherCharges;
                    result.TBOPrice.TdsCommission = TBOPrice.TdsCommission;
                    result.TBOPrice.AgentCommission = TBOPrice.AgentCommission;
                    result.TBOPrice.IncentiveEarned = TBOPrice.IncentiveEarned;
                    result.TBOPrice.TDSIncentive = TBOPrice.TDSIncentive;
                    result.TBOPrice.SeviceTax = TBOPrice.SeviceTax;
                }
            }
            else //Special retuns and internation oneway, round trip
            {
                doc = TempGetFareQuote(result.GUID, result.FareSellKey);
                if (doc != null)
                {
                    XmlNode Error = doc.SelectSingleNode("Response/Error");
                    
                    errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                    if (errorMessage.Length <= 0)
                    {
                        if (doc.SelectSingleNode("Response/IsPriceChanged").InnerText.ToLower() == "true")
                        {
                            XmlNodeList fareNodes = doc.SelectNodes("Response/Results/FareBreakdown");

                            rateOfExchange = exchangeRates[fareNodes[0].SelectSingleNode("Currency").InnerText];


                            foreach (XmlNode fbdNode in fareNodes)
                            {
                                Fare fbd = new Fare();
                                fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);

                                fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                fbd.SellingFare = fbd.TotalFare;
                                fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + (double)(Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText));
                                fareList.Add(fbd);

                                Fare tboFbd = new Fare();
                                tboFbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                tboFbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                tboFbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);

                                tboFbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                tboFbd.TotalFare = tboFbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                tboFbd.SellingFare = tboFbd.TotalFare;
                                tboFbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + (double)(Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText));

                                tboFares.Add(tboFbd);
                            }
                            result.TBOFareBreakdown = fareList.ToArray();

                            XmlNodeList fares = doc.SelectNodes("Response/Results/Fare");

                            if (fares != null)
                            {
                                foreach (XmlNode fare in fares)
                                {
                                    result.Price.OtherCharges = Convert.ToDecimal(fare.SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                    result.Price.TdsCommission = Convert.ToDecimal(fare.SelectSingleNode("TdsOnCommission").InnerText);
                                    result.Price.AgentCommission = Convert.ToDecimal(fare.SelectSingleNode("CommissionEarned").InnerText);
                                    result.Price.IncentiveEarned = Convert.ToDecimal(fare.SelectSingleNode("IncentiveEarned").InnerText);
                                    result.Price.TDSIncentive = Convert.ToDecimal(fare.SelectSingleNode("TdsOnIncentive").InnerText);
                                    result.Price.SServiceFee = Convert.ToDecimal(fare.SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                    result.Price.SupplierPrice = Convert.ToDecimal(fare.SelectSingleNode("PublishedFare").InnerText);
                                    result.Price.SupplierCurrency = fare.SelectSingleNode("Currency").InnerText;
                                    result.Price.YQTax = Convert.ToDecimal(fare.SelectSingleNode("YQTax").InnerText);
                                    result.Price.AgentPLB = Convert.ToDecimal(fare.SelectSingleNode("PLBEarned").InnerText);
                                    result.Price.TDSPLB = Convert.ToDecimal(fare.SelectSingleNode("TdsOnPLB").InnerText);
                                    if (fare.SelectSingleNode("TransactionFee") != null)
                                    {
                                        result.Price.TransactionFee = Convert.ToDecimal(fare.SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                    }

                                    result.TBOPrice.OtherCharges = Convert.ToDecimal(fare.SelectSingleNode("OtherCharges").InnerText);
                                    result.TBOPrice.TdsCommission = Convert.ToDecimal(fare.SelectSingleNode("TdsOnCommission").InnerText);
                                    result.TBOPrice.AgentCommission = Convert.ToDecimal(fare.SelectSingleNode("CommissionEarned").InnerText);
                                    result.TBOPrice.IncentiveEarned = Convert.ToDecimal(fare.SelectSingleNode("IncentiveEarned").InnerText);
                                    result.TBOPrice.TDSIncentive = Convert.ToDecimal(fare.SelectSingleNode("TdsOnIncentive").InnerText);
                                    result.TBOPrice.SeviceTax = Convert.ToDecimal(fare.SelectSingleNode("ServiceFee").InnerText);
                                }
                            }
                        }
                    }
                    else
                    {
                        fareList = new List<Fare>();
                        //TWhen fareQuote failed from Supplier side. Throwing an exception .
                        Audit.Add(EventType.TBOAir, Severity.High, 1, errorMessage, "");
                        return fareList.ToArray();
                    }
                }
            }

            return fareList.ToArray();
        }
        /// <summary>
        /// To send request and get fare calculation response.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private XmlDocument TempGetFareQuote(string guid, string sessionId)
        {

            XmlDocument doc = null;
            AuthenticationResponse ar = Authenticate();

            if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
            {
                try
                {
                    StringBuilder requestData = new StringBuilder();
                    requestData.AppendLine("{");
                    requestData.AppendLine("\"EndUserIp\": \"127.0.0.1\",");
                    requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                    requestData.AppendLine("\"TraceId\": \"" + sessionId + "\",");
                    requestData.AppendLine("\"ResultIndex\": \"" + guid + "\",");
                    requestData.AppendLine("}");

                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirGetFareQuoteRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(requestData.ToString());
                            sw.Close();

                            XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString());
                            filePath = XmlPath + "TBOAirGetFareQuoteRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc1.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetFareQuote Request. Error" + ex.ToString(), "");
                    }

                    string response = GetResponse(requestData.ToString(), fareQuoteUrl);



                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirGetFareQuoteResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(response);
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(response);
                            filePath = XmlPath + "TBOAirGetFareQuoteResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetFareQuote Response. Error" + ex.ToString(), "");
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to get Fare Quote. Reason : " + ex.ToString(), "");
                }
            }

            return doc;
        }
        /// <summary>
        /// To get baggage code for LCC. Sending two baggage requests and merging response for Combined results.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public DataTable GetBaggage(ref SearchResult result)
        {
            XmlDocument doc = null;
            //Sending two baggage requests and merging response for Combined results.
            if (result.GUID.Contains(','))
            {
                string[] Guids = result.GUID.Split(',');
                int i = 0;
                foreach (string Guid in Guids)
                {
                    result.GUID = Guid;
                    doc = TempGetBaggage(result);

                    if (doc != null)
                    {
                        XmlNodeList BaggageNodes = doc.SelectNodes("Response/Baggage");

                        if (BaggageNodes != null)
                        {
                            foreach (XmlNode baggage in BaggageNodes)
                            {
                                XmlNodeList nodes = baggage.SelectNodes("Baggage");

                                if (nodes != null)
                                {
                                    rateOfExchange = exchangeRates[nodes[0].SelectSingleNode("Currency").InnerText];
                                    foreach (XmlNode node in nodes)
                                    {
                                        DataRow dr = dtBaggage.NewRow();
                                        dr["WayType"] = node.SelectSingleNode("WayType").InnerText;
                                        dr["Code"] = node.SelectSingleNode("Code").InnerText;
                                        dr["Description"] = node.SelectSingleNode("Description").InnerText;
                                        dr["Weight"] = Convert.ToInt32(node.SelectSingleNode("Weight").InnerText);
                                        dr["Price"] = Convert.ToDecimal(node.SelectSingleNode("Price").InnerText) * rateOfExchange;
                                        dr["SupplierPrice"] = Convert.ToDecimal(node.SelectSingleNode("Price").InnerText);
                                        dr["Currency"] = agentBaseCurrency;
                                        dr["Origin"] = node.SelectSingleNode("Origin").InnerText;
                                        dr["Destination"] = node.SelectSingleNode("Destination").InnerText;
                                        if (i == 0)
                                        {
                                            dr["Group"] = 0;
                                        }
                                        else if (i == 1)
                                        {
                                            dr["Group"] = 1;
                                        }
                                        dtBaggage.Rows.Add(dr);

                                    }
                                }
                                i++;
                            }
                        }
                    }
                }
                result.GUID = Guids[0] + "," + Guids[1];
            }
            else
            {
                doc = TempGetBaggage(result);
                if (doc != null)
                {
                    XmlNodeList BaggageNodes = doc.SelectNodes("Response/Baggage");

                    if (BaggageNodes != null)
                    {
                        foreach (XmlNode baggage in BaggageNodes)
                        {
                            XmlNodeList nodes = baggage.SelectNodes("Baggage");

                            if (nodes != null)
                            {
                                rateOfExchange = exchangeRates[nodes[0].SelectSingleNode("Currency").InnerText];
                                if (result.Flights[0].Length > 1 || (result.Flights.Length > 1 && result.Flights[1].Length > 1))
                                {
                                    List<XmlNode> BagNodes = new List<XmlNode>();
                                    foreach (XmlNode node in nodes)
                                    {
                                        BagNodes.Add(node);
                                    }
                                    for (int i = 0; i < BagNodes.Count / 2; i++)
                                    {
                                        XmlNode node = BagNodes[i];
                                        List<XmlNode> bags = BagNodes.FindAll(delegate(XmlNode nd) { return nd.SelectSingleNode("Code").InnerText == node.SelectSingleNode("Code").InnerText; });
                                        if (bags != null)
                                        {
                                            DataRow dr = dtBaggage.NewRow();
                                            dr["WayType"] = node.SelectSingleNode("WayType").InnerText;
                                            dr["Code"] = node.SelectSingleNode("Code").InnerText;
                                            dr["Description"] = node.SelectSingleNode("Description").InnerText;
                                            dr["Weight"] = Convert.ToInt32(node.SelectSingleNode("Weight").InnerText);
                                            dr["Price"] = Convert.ToDecimal(node.SelectSingleNode("Price").InnerText) * rateOfExchange;
                                            dr["SupplierPrice"] = Convert.ToDecimal(node.SelectSingleNode("Price").InnerText);
                                            if (bags.Count > 1)
                                            {
                                                dr["Price"] = Convert.ToDecimal(dr["Price"]) + (Convert.ToDecimal(bags[1].SelectSingleNode("Price").InnerText) * rateOfExchange);
                                                dr["SupplierPrice"] = Convert.ToDecimal(dr["SupplierPrice"]) + Convert.ToDecimal(node.SelectSingleNode("Price").InnerText);
                                            }
                                            
                                            dr["Currency"] = agentBaseCurrency;
                                            dr["Origin"] = node.SelectSingleNode("Origin").InnerText;
                                            dr["Destination"] = node.SelectSingleNode("Destination").InnerText;
                                            if (node.SelectSingleNode("Origin").InnerText == result.Flights[0][0].Origin.AirportCode)
                                            {
                                                dr["Group"] = 0;
                                            }
                                            else if (node.SelectSingleNode("Destination").InnerText == result.Flights[0][result.Flights[0].Length - 1].Origin.AirportCode)
                                            {
                                                dr["Group"] = 1;
                                            }
                                            dtBaggage.Rows.Add(dr);
                                        }
                                    }
                                }
                                else
                                {                                    
                                    foreach (XmlNode node in nodes)
                                    {
                                        DataRow dr = dtBaggage.NewRow();
                                        dr["WayType"] = node.SelectSingleNode("WayType").InnerText;
                                        dr["Code"] = node.SelectSingleNode("Code").InnerText;
                                        dr["Description"] = node.SelectSingleNode("Description").InnerText;
                                        dr["Weight"] = Convert.ToInt32(node.SelectSingleNode("Weight").InnerText);
                                        dr["Price"] = Convert.ToDecimal(node.SelectSingleNode("Price").InnerText) * rateOfExchange;
                                        dr["SupplierPrice"] = Convert.ToDecimal(node.SelectSingleNode("Price").InnerText);
                                        dr["Currency"] = agentBaseCurrency;
                                        dr["Origin"] = node.SelectSingleNode("Origin").InnerText;
                                        dr["Destination"] = node.SelectSingleNode("Destination").InnerText;
                                        if (node.SelectSingleNode("Origin").InnerText == result.Flights[0][0].Origin.AirportCode )
                                        {
                                            dr["Group"] = 0;
                                        }
                                        else
                                        {
                                            dr["Group"] = 1;
                                        }
                                        dtBaggage.Rows.Add(dr);
                                    }
                                }
                            }
                        }
                    }
                }

            }

            return dtBaggage;

        }
        /// <summary>
        /// To send baggage request and get response 
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private XmlDocument TempGetBaggage(SearchResult result)
        {
            XmlDocument doc = null;

            try
            {
                AuthenticationResponse ar = Authenticate();

                if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                {
                    StringBuilder requestData = new StringBuilder();
                    requestData.AppendLine("{");
                    requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                    requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                    requestData.AppendLine("\"TraceId\": \"" + result.FareSellKey + "\",");
                    requestData.AppendLine("\"ResultIndex\": \"" + result.GUID + "\",");
                    requestData.AppendLine("}");
                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirBaggageRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(requestData.ToString());
                            sw.Close();

                            XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString());
                            filePath = XmlPath + "TBOAirBaggageRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc1.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetBaggage Request. Error" + ex.ToString(), "");
                    }

                    string response = GetResponse(requestData.ToString(), ssrUrl);

                    try
                    {

                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirBaggageResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(response);
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(response);
                            filePath = XmlPath + "TBOAirBaggageResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetBaggage Response. Error" + ex.ToString(), "");
                    }

                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to get Baggage details. Error : " + ex.ToString(), "");
                throw ex;
            }

            return doc;
        }
        /// <summary>
        /// To book/Hold itinerary. Sending two booking requests by  spliting them and finally combining the response. this method is only for GDS.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public BookingResponse Book(ref FlightItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();
            try
            {
                //Sending two booking requests by  spliting them and finally combining the response.
                if (itinerary.GUID.Contains(',') && itinerary.SupplierLocatorCode.ToLower() == "normalreturn") //Normal return combine result set
                {
                    decimal onWardPublishedFare = 0.0M;
                    decimal onwardTax = 0.0M;
                    string[] Guids = itinerary.GUID.Split(',');
                    foreach (string Guid in Guids)
                    {
                        itinerary.GUID = Guid;
                        XmlDocument doc = TempBook(ref itinerary);
                        if (doc != null)
                        {
                            XmlNode Error = doc.SelectSingleNode("Response/Error");

                            if (Error != null)
                            {
                                if (Error.SelectSingleNode("ErrorCode").InnerText == "0")
                                {
                                    XmlNode Response = doc.SelectSingleNode("Response/Response");
                                    if (bookResponse.PNR != null && bookResponse.PNR.Length > 0)
                                    {
                                        bookResponse.PNR += "|" + Response.SelectSingleNode("PNR").InnerText;
                                    }
                                    else
                                    {
                                        bookResponse.PNR = Response.SelectSingleNode("PNR").InnerText;
                                    }
                                    bookResponse.ProdType = ProductType.Flight;
                                    bookResponse.ConfirmationNo = bookResponse.PNR;
                                    bookResponse.SSRDenied = Convert.ToBoolean(Response.SelectSingleNode("SSRDenied").InnerText);
                                    itinerary.PNR = bookResponse.PNR;
                                    if (itinerary.AirLocatorCode != null && itinerary.AirLocatorCode.Length > 0)
                                    {
                                        itinerary.AirLocatorCode += "|" + Response.SelectSingleNode("BookingId").InnerText;//TEMP Store TBO BookingId for Ticketing.
                                    }
                                    else
                                    {
                                        itinerary.AirLocatorCode = Response.SelectSingleNode("BookingId").InnerText ;//TEMP Store TBO BookingId for Ticketing.
                                    }
                                    onWardPublishedFare = Convert.ToDecimal(doc.SelectSingleNode("Response/Response/FlightItinerary/Fare/BaseFare").InnerText) * (0.056M);
                                    onwardTax = Convert.ToDecimal(doc.SelectSingleNode("Response/Response/FlightItinerary/Fare/Tax").InnerText) * (0.056M);

                                    XmlNodeList Segments = doc.SelectNodes("Response/Response/FlightItinerary/Segments");

                                    foreach (FlightInfo seg in itinerary.Segments)
                                    {
                                        foreach (XmlNode segment in Segments)
                                        {
                                            if (seg.Origin.AirportCode == segment.SelectSingleNode("Origin/Airport/AirportCode").InnerText)
                                            {
                                                if (segment.SelectSingleNode("AirlinePNR") != null)
                                                {
                                                    seg.AirlinePNR = segment.SelectSingleNode("AirlinePNR").InnerText;                                                    
                                                }
                                                break;
                                            }
                                        }
                                    }

                                    if (Response.SelectSingleNode("IsPriceChanged").InnerText == "true" || Response.SelectSingleNode("IsTimeChanged").InnerText == "true")
                                    {
                                        if (itinerary.PNR == null)
                                        {
                                            itinerary.PNR = "";
                                        }
                                        //Update Price or Time for the Itinerary and alert user.
                                        bookResponse.Status = BookingResponseStatus.Failed;
                                        bookResponse.Error = "Price has been revised for this booking.";                                        
                                    }
                                    else
                                    {
                                        Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Booking Successful PNR " + bookResponse.PNR + " for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
                                        bookResponse.Status = BookingResponseStatus.Successful;
                                    }
                                }
                                else if (Error.SelectSingleNode("ErrorCode").InnerText == "3")
                                {
                                    if (itinerary.PNR == null)
                                    {
                                        itinerary.PNR = "";
                                    }
                                    bookResponse.Status = BookingResponseStatus.OtherFare;
                                    Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir)" + Error.SelectSingleNode("ErrorMessage").InnerText, "");
                                }
                                else
                                {
                                    Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir)"+Error.SelectSingleNode("ErrorMessage").InnerText, "");
                                    bookResponse.Error = Error.SelectSingleNode("ErrorMessage").InnerText;
                                    bookResponse.Status = BookingResponseStatus.Failed;
                                    throw new ArgumentException(bookResponse.Error);
                                }
                            }
                            else
                            {
                                if (itinerary.PNR == null)
                                {
                                    itinerary.PNR = "";
                                }
                                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir)Failed to Book", "");
                                bookResponse.Status = BookingResponseStatus.Failed;
                            }
                        }
                    }
                    if (itinerary.PNR != null)
                    {
                        int pnrCount = itinerary.PNR.Split('|').Length;
                        //Failed booking. In the combined result if return booking failed. Continue booking onward segment by showing an alert
                        if (pnrCount == 1)
                        {
                            FlightInfo[] f = itinerary.Segments;
                            itinerary.Segments = new FlightInfo[1];
                            itinerary.Segments[0] = f[0];
                            itinerary.PNR = itinerary.PNR;
                            itinerary.AirLocatorCode = itinerary.AirLocatorCode.Remove(itinerary.AirLocatorCode.Length - 1);

                            itinerary.Passenger[0].Price.PublishedFare = onWardPublishedFare;
                            itinerary.Passenger[0].Price.Tax = onwardTax;
                            bookResponse.Status = BookingResponseStatus.ReturnFailed;
                        }
                    }
                }
                else
                {
                    XmlDocument doc = TempBook(ref itinerary);

                    if (doc != null)
                    {
                        XmlNode Error = doc.SelectSingleNode("Response/Error");

                        if (Error != null)
                        {
                            if (Error.SelectSingleNode("ErrorCode").InnerText == "0")
                            {
                                XmlNode Response = doc.SelectSingleNode("Response/Response");

                                bookResponse.PNR = Response.SelectSingleNode("PNR").InnerText;
                                bookResponse.ProdType = ProductType.Flight;
                                bookResponse.ConfirmationNo = bookResponse.PNR;
                                bookResponse.SSRDenied = Convert.ToBoolean(Response.SelectSingleNode("SSRDenied").InnerText);
                                itinerary.PNR = bookResponse.PNR;
                                itinerary.AirLocatorCode = Response.SelectSingleNode("BookingId").InnerText;//TEMP Store TBO BookingId for Ticketing.

                                XmlNodeList Segments = doc.SelectNodes("Response/Response/FlightItinerary/Segments");

                                foreach (FlightInfo seg in itinerary.Segments)
                                {
                                    foreach (XmlNode segment in Segments)
                                    {
                                        if (seg.Origin.AirportCode == segment.SelectSingleNode("Origin/Airport/AirportCode").InnerText)
                                        {
                                            if (segment.SelectSingleNode("AirlinePNR") != null && segment.SelectSingleNode("AirlinePNR").InnerText.Length > 0)
                                            {
                                                seg.AirlinePNR = segment.SelectSingleNode("AirlinePNR").InnerText;
                                            }
                                            break;
                                        }
                                    }
                                }

                                bookResponse.Status = BookingResponseStatus.Successful;

                            }
                            else if (Error.SelectSingleNode("ErrorCode").InnerText == "3")
                            {
                                bookResponse.Status = BookingResponseStatus.OtherFare;
                                bookResponse.Error = Error.SelectSingleNode("ErrorMessage").InnerText;
                            }
                            else
                            {
                                bookResponse.Error = Error.SelectSingleNode("ErrorMessage").InnerText;
                                bookResponse.Status = BookingResponseStatus.Failed;
                                throw new ArgumentException(bookResponse.Error);

                            }
                        }
                        else
                        {
                            bookResponse.Status = BookingResponseStatus.Failed;
                        }
                    }
                    else
                    {
                        bookResponse.Status = BookingResponseStatus.Failed;
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to Book itinerary. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return bookResponse;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private XmlDocument TempBook(ref FlightItinerary itinerary)
        {

            XmlDocument doc = null;
            try
            {
                AuthenticationResponse ar = Authenticate();
                if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                {
                    //AgencyBalance balance = GetAgencyBalance(ar); Not required to HOLD

                    decimal bookingAmount = 0;

                    foreach (FlightPassenger pax in itinerary.Passenger)
                    {
                        bookingAmount = pax.TBOPrice.PublishedFare;
                    }

                    //if (balance != null && balance.CashBalance > bookingAmount)not to check balance for HOLD
                    {
                        Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Booking started for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
                        StringBuilder requestData = new StringBuilder();
                        requestData.AppendLine("{");
                        requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        requestData.AppendLine("\"TraceId\": \"" + itinerary.TicketAdvisory + "\",");
                        requestData.AppendLine("\"ResultIndex\": \"" + itinerary.GUID + "\",");
                        requestData.AppendLine("\"Passengers\": [");

                        for (int i = 0; i < itinerary.Passenger.Length; i++)
                        {
                            FlightPassenger pax = itinerary.Passenger[i];
                            requestData.AppendLine("{");
                            switch (pax.Type)
                            {
                                case PassengerType.Adult:
                                    requestData.AppendLine("\"Title\":\"" + pax.Title + "\",");
                                    break;
                                case PassengerType.Child:
                                case PassengerType.Infant:
                                    if (pax.Gender == Gender.Male)
                                    {
                                        requestData.AppendLine("\"Title\":\"Mstr\",");
                                    }
                                    else
                                    {
                                        requestData.AppendLine("\"Title\":\"Miss\",");
                                    }
                                    break;
                            }
                            requestData.AppendLine("\"FirstName\": \"" + pax.FirstName + "\",");
                            requestData.AppendLine("\"LastName\": \"" + pax.LastName + "\",");
                            requestData.AppendLine("\"PaxType\": " + (int)pax.Type + ",");
                            if (pax.DateOfBirth != DateTime.MinValue) //we will not pass the default date value
                            {
                                requestData.AppendLine("\"DateOfBirth\": \"" + pax.DateOfBirth.ToString("yyyy-MM-ddThh:mm:ss") + "\",");
                            }
                            requestData.AppendLine("\"Gender\": " + (int)pax.Gender + ",");
                            if (!string.IsNullOrEmpty(pax.PassportNo))
                            {
                                requestData.AppendLine("\"PassportNo\": \"" + pax.PassportNo + "\",");
                            }
                            if (pax.PassportExpiry != DateTime.MinValue)
                            {
                                requestData.AppendLine("\"PassportExpiry\": \"" + pax.PassportExpiry.ToString("yyyy-MM-ddThh:mm:ss") + "\",");
                            }
                            if (!string.IsNullOrEmpty(pax.AddressLine1))//if AddressLine1 is empty then pass dummy
                            {
                                requestData.AppendLine("\"AddressLine1\": \"" + pax.AddressLine1 + "\",");
                            }
                            else
                            {
                                requestData.AppendLine("\"AddressLine1\": \"" + "AddressLine1" + "\",");
                            }
                            requestData.AppendLine("\"AddressLine2\": \"\",");
                            if (!string.IsNullOrEmpty(pax.City))
                            {
                                requestData.AppendLine("\"City\": \"" + pax.City + "\",");
                            }
                            else
                            {
                                requestData.AppendLine("\"City\": \"" + "City" + "\",");
                            }
                            if (pax.Country != null)
                            {
                                if (!string.IsNullOrEmpty(pax.Country.CountryCode))
                                {
                                    requestData.AppendLine("\"CountryCode\": \"" + pax.Country.CountryCode + "\",");
                                }
                                if (!string.IsNullOrEmpty(pax.Country.CountryName))
                                {
                                    requestData.AppendLine("\"CountryName\": \"" + pax.Country.CountryName + "\",");
                                }
               
                            }
                            
                            
                            requestData.AppendLine("\"ContactNo\": \"" + pax.CellPhone.Replace("-", "") + "\",");
                            requestData.AppendLine("\"Email\": \"" + pax.Email + "\",");
                            requestData.AppendLine("\"IsLeadPax\": \"" + (pax.IsLeadPax ? "true" : "false") + "\",");
                            requestData.AppendLine("\"FFAirline\": \"\",");
                            requestData.AppendLine("\"FFNumber\": \"\",");
                            requestData.AppendLine("\"Fare\": {");
                            requestData.AppendLine("\"BaseFare\":" + pax.TBOPrice.BaseFare + ",");
                            requestData.AppendLine("\"Tax\": " + pax.TBOPrice.Tax + ",");
                            requestData.AppendLine("\"TransactionFee\": " + pax.TBOPrice.TransactionFee + ",");
                            requestData.AppendLine("\"YQTax\": " + pax.TBOPrice.YQTax + ",");
                            requestData.AppendLine("\"AdditionalTxnFee\": " + pax.TBOPrice.AdditionalTxnFee + ",");
                            requestData.AppendLine("\"AirTransFee\": " + pax.TBOPrice.AirlineTransFee);
                            requestData.AppendLine("}");

                            if (i == 0)
                            {
                                if (pax.Meal.Code != null)
                                {
                                    requestData.Append(",");
                                    requestData.AppendLine("\"Meal\": {");
                                    requestData.AppendLine("\"Code\": \"" + (pax.Meal.Code != null ? pax.Meal.Code : null) + "\",");
                                    requestData.AppendLine("\"Description\": \"" + (pax.Meal.Description != null ? pax.Meal.Description : null) + "\",");
                                    requestData.AppendLine("}");
                                }
                                else if (pax.Seat.Code != null)
                                {
                                    requestData.Append(",");
                                    requestData.AppendLine("\"Seat\": {");
                                    requestData.AppendLine("\"Code\": \"" + (pax.Seat.Code != null ? pax.Seat.Code : null) + "\",");
                                    requestData.AppendLine("\"Description\": \"" + (pax.Seat.Description != null ? pax.Seat.Description : null) + "\",");
                                    requestData.AppendLine("}");//Seat End
                                }
                            }

                            if (i < itinerary.Passenger.Length - 1)
                            {
                                requestData.AppendLine("},");//Pax End
                            }
                            else
                            {
                                requestData.AppendLine("}");//Pax End
                            }
                        }

                        requestData.AppendLine("]");//Passengers End
                        requestData.AppendLine("}");//Request End

                        try
                        {
                            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                            {
                                string filePath = XmlPath + "TBOAirBookRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(requestData.ToString());
                                sw.Close();

                                XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "Request");
                                filePath = XmlPath + "TBOAirBookRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                doc1.Save(filePath);
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Booking Request. Error" + ex.ToString(), "");
                        }

                        Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Booking request created for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                        string response = GetResponse(requestData.ToString(), bookUrl);

                        Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Booking response created for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                        try
                        {
                            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                            {
                                string filePath = XmlPath + "TBOAirBookResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(response);
                                sw.Close();

                                doc = JsonConvert.DeserializeXmlNode(response);
                                filePath = XmlPath + "TBOAirBookResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                doc.Save(filePath);
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Booking Response. Error" + ex.ToString(), "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to Book itinerary. Error : " + ex.ToString(), "");
                throw ex;
            }

            return doc;
        }

        /// <summary>
        /// To create Ticket. Sending to ticketing requests and merging responses for combined results. this method is only for  LCC.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public BookingResponse Ticket(ref FlightItinerary itinerary)
        {
            BookingResponse bookingResponse = new BookingResponse();
            XmlDocument doc = null;
            int RowCount = 1;
            decimal onWardPublishedFare = 0.0M;
            decimal onwardTax = 0.0M;
            try
            {
                //Sending to ticketing requests and merging responses for combined results.
                if (itinerary.GUID.Contains(',') && itinerary.SupplierLocatorCode.ToLower() == "normalreturn") //Normal Return for domestic
                {
                    string[] Guids = itinerary.GUID.Split(',');
                    for (int i = 0; i < Guids.Length; i++)
                    {
                        string Guid = Guids[i];
                        Twoway = RowCount;
                        itinerary.GUID = Guid;
                        doc = TempTicket(ref itinerary);

                        if (doc != null)
                        {
                            if (itinerary.IsLCC || itinerary.AirLocatorCode.Contains("LCC"))
                            {
                                XmlNode errorNode = doc.SelectSingleNode("Request/Response/Error");
                                if (errorNode != null && errorNode.SelectSingleNode("ErrorCode").InnerText == "0")
                                {
                                    if (bookingResponse.PNR != null && bookingResponse.PNR.Length > 0)
                                    {
                                        bookingResponse.PNR += "|" + doc.SelectSingleNode("Request/Response/Response/PNR").InnerText;
                                    }
                                    else
                                    {
                                        bookingResponse.PNR = doc.SelectSingleNode("Request/Response/Response/PNR").InnerText;
                                    }
                                    itinerary.PNR = bookingResponse.PNR;
                                    bookingResponse.ProdType = ProductType.Flight;
                                    bookingResponse.Error = "";
                                    bookingResponse.Status = BookingResponseStatus.Successful;
                                    if (itinerary.AirLocatorCode != null && itinerary.AirLocatorCode.Length > 0)
                                    {
                                        itinerary.AirLocatorCode += "|" + doc.SelectSingleNode("Request/Response/Response/BookingId").InnerText;
                                    }
                                    else
                                    {
                                        itinerary.AirLocatorCode = doc.SelectSingleNode("Request/Response/Response/BookingId").InnerText;
                                    }

                                    Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing Successful PNR " + itinerary.PNR + " started for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                                    onWardPublishedFare = Convert.ToDecimal(doc.SelectSingleNode("Request/Response/Response/FlightItinerary/Fare/BaseFare").InnerText) * rateOfExchange;
                                    onwardTax = (Convert.ToDecimal(doc.SelectSingleNode("Request/Response/Response/FlightItinerary/Fare/Tax").InnerText) + Convert.ToDecimal(doc.SelectSingleNode("Request/Response/Response/FlightItinerary/Fare/OtherCharges").InnerText)) * rateOfExchange;
                                    XmlNodeList Passengers = doc.SelectNodes("Request/Response/Response/FlightItinerary/Passenger");

                                    //Set Airline PNR
                                    XmlNodeList Segments = doc.SelectNodes("Request/Response/Response/FlightItinerary/Segments");

                                    foreach (FlightInfo seg in itinerary.Segments)
                                    {
                                        foreach (XmlNode segment in Segments)
                                        {
                                            if (seg.Origin.AirportCode == segment.SelectSingleNode("Origin/Airport/AirportCode").InnerText)
                                            {
                                                if (segment.SelectSingleNode("AirlinePNR") != null && segment.SelectSingleNode("AirlinePNR").InnerText.Length > 0)
                                                {
                                                    seg.AirlinePNR = segment.SelectSingleNode("AirlinePNR").InnerText;
                                                }
                                                break;
                                            }
                                        }
                                    }


                                    if (i == 0)
                                    {
                                        //Remove previous assigned baggage codes
                                        foreach (FlightPassenger fpax in itinerary.Passenger)
                                        {
                                            fpax.BaggageCode = "";
                                        }
                                    }

                                    foreach (FlightPassenger fpax in itinerary.Passenger)
                                    {
                                        foreach (XmlNode pax in Passengers)
                                        {
                                            if (fpax.PassportNo == pax.SelectSingleNode("PassportNo").InnerText)
                                            {

                                                if (fpax.CategoryId != null && fpax.CategoryId.Length > 0)
                                                {
                                                    fpax.CategoryId += "|" + pax.SelectSingleNode("Ticket").SelectSingleNode("TicketId").InnerText;
                                                }
                                                else
                                                {
                                                    fpax.CategoryId = pax.SelectSingleNode("Ticket").SelectSingleNode("TicketId").InnerText;
                                                }
                                                XmlNodeList Baggages = pax.SelectNodes("SegmentAdditionalInfo");

                                                foreach (XmlNode baggage in Baggages)
                                                {
                                                    switch (pax.SelectSingleNode("PaxType").InnerText)
                                                    {
                                                        case "1":
                                                            if (fpax.Type == PassengerType.Adult)
                                                            {
                                                                string bags = "";
                                                                if (fpax.BaggageCode.Length > 0)
                                                                {
                                                                    string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                    if (bag.Contains("|"))
                                                                    {
                                                                        bag = bag.TrimEnd('|');
                                                                        foreach (string b in bag.Split('|'))
                                                                        {
                                                                            if (b != "0")
                                                                            {
                                                                                if (bags.Length > 0)
                                                                                {
                                                                                    bags += "+" + (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                                else
                                                                                {
                                                                                    bags = (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        bags = bag;
                                                                    }
                                                                    fpax.BaggageCode += "," + bags;
                                                                }
                                                                else
                                                                {
                                                                    string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                    if (bag.Contains("|"))
                                                                    {
                                                                        bag = bag.TrimEnd('|');
                                                                        foreach (string b in bag.Split('|'))
                                                                        {
                                                                            if (b != "0")
                                                                            {
                                                                                if (bags.Length > 0)
                                                                                {
                                                                                    bags += "+" + (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                                else
                                                                                {
                                                                                    bags = (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        bags = bag;
                                                                    }
                                                                    fpax.BaggageCode = bags;
                                                                }

                                                            }
                                                            break;
                                                        case "2":
                                                            if (fpax.Type == PassengerType.Child)
                                                            {
                                                                string bags = "";
                                                                if (fpax.BaggageCode.Length > 0)
                                                                {
                                                                    string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                    if (bag.Contains("|"))
                                                                    {
                                                                        bag = bag.TrimEnd('|');
                                                                        foreach (string b in bag.Split('|'))
                                                                        {
                                                                            if (b != "0")
                                                                            {
                                                                                if (bags.Length > 0)
                                                                                {
                                                                                    bags += "+" + (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                                else
                                                                                {
                                                                                    bags = (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        bags = bag;
                                                                    }
                                                                    fpax.BaggageCode += "," + bags;
                                                                }
                                                                else
                                                                {
                                                                    string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                    if (bag.Contains("|"))
                                                                    {
                                                                        bag = bag.TrimEnd('|');
                                                                        foreach (string b in bag.Split('|'))
                                                                        {
                                                                            if (b != "0")
                                                                            {
                                                                                if (bags.Length > 0)
                                                                                {
                                                                                    bags += "+" + (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                                else
                                                                                {
                                                                                    bags = (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        bags = bag;
                                                                    }
                                                                    fpax.BaggageCode = bags;
                                                                }

                                                            }
                                                            break;
                                                        //case "3":
                                                        //    if (fpax.Type == PassengerType.Infant)
                                                        //    {
                                                        //        if (Baggages.Count > 1)
                                                        //        {
                                                        //            fpax.BaggageCode = Baggages[0].SelectSingleNode("Baggage").InnerText + "," + Baggages[1].SelectSingleNode("Baggage").InnerText;
                                                        //        }
                                                        //        else
                                                        //        {
                                                        //            fpax.BaggageCode = Baggages[0].SelectSingleNode("Baggage").InnerText;
                                                        //        }
                                                        //    }
                                                        //    break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (itinerary.PNR == null)
                                    {
                                        itinerary.PNR = "";
                                        bookingResponse.PNR = "";
                                    }
                                    bookingResponse.Error = errorNode.SelectSingleNode("ErrorMessage").InnerText;
                                    bookingResponse.Status = BookingResponseStatus.Failed;
                                }
                            }
                        }
                        RowCount++;
                    }
                    itinerary.GUID = Guids[0] + "," + Guids[1];
                    if (itinerary.PNR != null)
                    {
                        int pnrCount = itinerary.PNR.Split('|').Length;  //Proceed to ticket in return ticketing got failed . Showing an alert .
                        //if (pnrCount == 2)
                        if (pnrCount == 1)
                        {
                            FlightInfo[] f = itinerary.Segments;
                            itinerary.Segments = new FlightInfo[1];
                            itinerary.Segments[0] = f[0];

                            itinerary.Passenger[0].Price.PublishedFare = onWardPublishedFare;
                            itinerary.Passenger[0].Price.Tax = onwardTax;
                            bookingResponse.Status = BookingResponseStatus.ReturnFailed;

                        }
                    }
                }
                else
                {
                    doc = TempTicket(ref itinerary);

                    if (doc != null)
                    {
                        if (itinerary.IsLCC || itinerary.AirLocatorCode.Contains("LCC"))
                        {
                            XmlNode errorNode = doc.SelectSingleNode("Request/Response/Error");
                            if (errorNode != null && errorNode.SelectSingleNode("ErrorCode").InnerText == "0")
                            {
                                bookingResponse.PNR = doc.SelectSingleNode("Request/Response/Response/PNR").InnerText;
                                itinerary.PNR = bookingResponse.PNR;
                                bookingResponse.ProdType = ProductType.Flight;
                                bookingResponse.Error = "";
                                bookingResponse.Status = BookingResponseStatus.Successful;
                                if (itinerary.AirLocatorCode != null && itinerary.AirLocatorCode.Length > 0)
                                {
                                    itinerary.AirLocatorCode += "|" + doc.SelectSingleNode("Request/Response/Response/BookingId").InnerText;
                                }
                                else
                                {
                                    itinerary.AirLocatorCode = doc.SelectSingleNode("Request/Response/Response/BookingId").InnerText;
                                }

                                Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing Successful PNR " + itinerary.PNR + " for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                                XmlNodeList Passengers = doc.SelectNodes("Request/Response/Response/FlightItinerary/Passenger");

                                //Set Airline PNR
                                XmlNodeList Segments = doc.SelectNodes("Request/Response/Response/FlightItinerary/Segments");

                                foreach (FlightInfo seg in itinerary.Segments)
                                {
                                    foreach (XmlNode segment in Segments)
                                    {
                                        if (seg.Origin.AirportCode == segment.SelectSingleNode("Origin/Airport/AirportCode").InnerText)
                                        {
                                            if (segment.SelectSingleNode("AirlinePNR") != null && segment.SelectSingleNode("AirlinePNR").InnerText.Length > 0)
                                            {
                                                seg.AirlinePNR = segment.SelectSingleNode("AirlinePNR").InnerText;
                                            }
                                            break;
                                        }

                                    }
                                }

                                //Remove previous assigned baggage codes
                                foreach (FlightPassenger fpax in itinerary.Passenger)
                                {
                                    fpax.BaggageCode = "";
                                }

                                foreach (FlightPassenger fpax in itinerary.Passenger)
                                {
                                    foreach (XmlNode pax in Passengers)
                                    {
                                        if (fpax.PassportNo == pax.SelectSingleNode("PassportNo").InnerText)
                                        {
                                            if (fpax.CategoryId != null && fpax.CategoryId.Length > 0)
                                            {
                                                fpax.CategoryId += "|" + pax.SelectSingleNode("Ticket").SelectSingleNode("TicketId").InnerText;
                                            }
                                            else
                                            {
                                                fpax.CategoryId = pax.SelectSingleNode("Ticket").SelectSingleNode("TicketId").InnerText;
                                            }

                                            XmlNodeList Baggages = pax.SelectNodes("SegmentAdditionalInfo");

                                            foreach (XmlNode baggage in Baggages)
                                            {
                                                switch (pax.SelectSingleNode("PaxType").InnerText)
                                                {
                                                    case "1":
                                                        if (fpax.Type == PassengerType.Adult)
                                                        {
                                                            string bags = "";
                                                            if (fpax.BaggageCode.Length > 0)
                                                            {
                                                                string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                if (bag.Contains("|"))
                                                                {
                                                                    bag = bag.TrimEnd('|');
                                                                    foreach (string b in bag.Split('|'))
                                                                    {
                                                                        if (b != "0")
                                                                        {
                                                                            if (bags.Length > 0)
                                                                            {
                                                                                bags += "+" + (b.EndsWith("Kg") ? b : b + "Kg");
                                                                            }
                                                                            else
                                                                            {
                                                                                bags = (b.EndsWith("Kg") ? b : b + "Kg");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    bags = bag;
                                                                }
                                                                fpax.BaggageCode += "," + bags;
                                                            }
                                                            else
                                                            {
                                                                string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                if (bag.Contains("|"))
                                                                {
                                                                    bag = bag.TrimEnd('|');

                                                                    foreach (string b in bag.Split('|'))
                                                                    {
                                                                        if (b != "0")
                                                                        {
                                                                            if (bags.Length > 0)
                                                                            {
                                                                                bags += "+" + (b.EndsWith("Kg") ? b : b + "Kg");
                                                                            }
                                                                            else
                                                                            {
                                                                                bags = (b.EndsWith("Kg") ? b : b + "Kg");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    bags = bag;
                                                                }
                                                                fpax.BaggageCode = bags;
                                                            }

                                                        }
                                                        break;
                                                    case "2":
                                                        if (fpax.Type == PassengerType.Child)
                                                        {
                                                            string bags = "";
                                                            if (fpax.BaggageCode.Length > 0)
                                                            {
                                                                string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                if (bag.Contains("|"))
                                                                {
                                                                    bag = bag.TrimEnd('|');
                                                                    foreach (string b in bag.Split('|'))
                                                                    {
                                                                        if (b != "0")
                                                                        {
                                                                            if (bags.Length > 0)
                                                                            {
                                                                                bags += "+" + (b.EndsWith("Kg") ? b : b + "Kg");
                                                                            }
                                                                            else
                                                                            {
                                                                                bags = (b.EndsWith("Kg") ? b : b + "Kg");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    bags = bag;
                                                                }
                                                                fpax.BaggageCode += "," + bags;
                                                            }
                                                            else
                                                            {
                                                                string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                if (bag.Contains("|"))
                                                                {
                                                                    bag = bag.TrimEnd('|');
                                                                    foreach (string b in bag.Split('|'))
                                                                    {
                                                                        if (b != "0")
                                                                        {
                                                                            if (bags.Length > 0)
                                                                            {
                                                                                bags += "+" + (b.EndsWith("Kg") ? b : b + "Kg");
                                                                            }
                                                                            else
                                                                            {
                                                                                bags = (b.EndsWith("Kg") ? b : b + "Kg");
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    bags = bag;
                                                                }
                                                                fpax.BaggageCode = bags;
                                                            }

                                                        }
                                                        break;
                                                    //case "3":
                                                    //    if (fpax.Type == PassengerType.Infant)
                                                    //    {
                                                    //        if (Baggages.Count > 1)
                                                    //        {
                                                    //            fpax.BaggageCode = Baggages[0].SelectSingleNode("Baggage").InnerText + "," + Baggages[1].SelectSingleNode("Baggage").InnerText;
                                                    //        }
                                                    //        else
                                                    //        {
                                                    //            fpax.BaggageCode = Baggages[0].SelectSingleNode("Baggage").InnerText;
                                                    //        }
                                                    //    }
                                                    //    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (itinerary.PNR == null)
                                {
                                    itinerary.PNR = "";
                                    bookingResponse.PNR = "";
                                }
                                bookingResponse.Error = errorNode.SelectSingleNode("ErrorMessage").InnerText;
                                bookingResponse.Status = BookingResponseStatus.Failed;
                                throw new ArgumentException(bookingResponse.Error);
                            }
                        }
                    }
                    else
                    {
                        bookingResponse.Error = "Ticketing response returned empty or corrupted from TBO Air.";
                        bookingResponse.Status = BookingResponseStatus.Failed;
                        throw new ArgumentException(bookingResponse.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                bookingResponse.Status = BookingResponseStatus.Failed;
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to generate Ticket. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return bookingResponse;
        }
        /// <summary>
        /// To send a ticketing requeat and response for LCC.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private XmlDocument TempTicket(ref FlightItinerary itinerary)
        {

            XmlDocument doc = null;
            if (itinerary.GUID.Contains(',') && itinerary.SupplierLocatorCode.ToLower() != "normalreturn")
            {
                itinerary.GUID = itinerary.GUID.Split(',')[0];
            }
            try
            {
                AuthenticationResponse ar = Authenticate();
                AgencyBalance balance = GetAgencyBalance(ar);
                
                decimal bookingAmount = 0;

                foreach (FlightPassenger pax in itinerary.Passenger)
                {
                    bookingAmount += pax.TBOPrice.PublishedFare;
                }

                if (balance != null && balance.CashBalance > bookingAmount)
                {
                    Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing started for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
                    StringBuilder requestData = new StringBuilder();
                    requestData.AppendLine("{");
                    requestData.AppendLine("\"PreferredCurrency\": \"INR\",");
                    requestData.AppendLine("\"IsBaseCurrencyRequired\":\"true\",");
                    requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                    requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                    requestData.AppendLine("\"TraceId\": \"" + itinerary.TicketAdvisory + "\",");
                    requestData.AppendLine("\"ResultIndex\": \"" + itinerary.GUID + "\",");
                    requestData.AppendLine("\"Passengers\":[");

                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        FlightPassenger pax = itinerary.Passenger[i];
                        requestData.AppendLine("{");
                        switch (pax.Type)
                        {
                            case PassengerType.Adult:
                                requestData.AppendLine("\"Title\":\"" + pax.Title + "\",");
                                break;
                            case PassengerType.Child:
                            case PassengerType.Infant:
                                if (pax.Gender == Gender.Male)
                                {
                                    requestData.AppendLine("\"Title\":\"" + "Mstr" + "\",");
                                }
                                else if (pax.Gender == Gender.Female)
                                {
                                    requestData.AppendLine("\"Title\":\"" + "Miss" + "\",");
                                }
                                break;
                        }
                        requestData.AppendLine("\"FirstName\": \"" + pax.FirstName + "\",");
                        requestData.AppendLine("\"LastName\": \"" + pax.LastName + "\",");
                        requestData.AppendLine("\"PaxType\": " + (int)pax.Type + ",");
                        if (pax.DateOfBirth != DateTime.MinValue)
                        {

                            requestData.AppendLine("\"DateOfBirth\": \"" + pax.DateOfBirth.ToString("yyyy-MM-ddThh:mm:ss") + "\",");
                        }
                        requestData.AppendLine("\"Gender\": " + (int)pax.Gender + ",");

                        if (!string.IsNullOrEmpty(pax.PassportNo))
                        {
                            requestData.AppendLine("\"PassportNo\": \"" + pax.PassportNo + "\",");
                        }

                        if (pax.PassportExpiry != DateTime.MinValue)
                        {
                            requestData.AppendLine("\"PassportExpiry\": \"" + pax.PassportExpiry.ToString("yyyy-MM-ddThh:mm:ss") + "\",");
                        }
                        if (!string.IsNullOrEmpty(pax.AddressLine1))//If Address is emtpy then pass dummy
                        {
                            requestData.AppendLine("\"AddressLine1\": \"" + pax.AddressLine1 + "\",");
                        }
                        else
                        {
                            requestData.AppendLine("\"AddressLine1\": \"" + "AddressLine1" + "\",");
                        }
                        requestData.AppendLine("\"AddressLine2\": \"\",");
                        if (!string.IsNullOrEmpty(pax.City))//If City is empty then pass dummy
                        {
                            requestData.AppendLine("\"City\": \"" + pax.City + "\",");
                        }
                        else
                        {
                            requestData.AppendLine("\"City\": \"" + "City" + "\",");
                        }
                        if (pax.Country != null)
                        {

                            if (!string.IsNullOrEmpty(pax.Country.CountryCode))
                            {
                                requestData.AppendLine("\"CountryCode\": \"" + pax.Country.CountryCode + "\",");
                            }
                            if (!string.IsNullOrEmpty(pax.Country.CountryName))
                            {
                                requestData.AppendLine("\"CountryName\": \"" + pax.Country.CountryName + "\",");
                            }
                        }
                       
                        requestData.AppendLine("\"ContactNo\": \"" + pax.CellPhone.Replace("-", "") + "\",");
                        requestData.AppendLine("\"Email\": \"" + pax.Email + "\",");
                        requestData.AppendLine("\"IsLeadPax\": \"" + (pax.IsLeadPax ? "true" : "false") + "\",");
                        requestData.AppendLine("\"FFAirline\": \"\",");
                        requestData.AppendLine("\"FFNumber\": \"\",");
                        requestData.AppendLine("\"Fare\": {");
                        requestData.AppendLine("\"BaseFare\":" + pax.TBOPrice.BaseFare + ",");
                        requestData.AppendLine("\"Tax\": " + pax.TBOPrice.Tax + ",");
                        requestData.AppendLine("\"TransactionFee\": " + pax.TBOPrice.TransactionFee + ",");
                        requestData.AppendLine("\"YQTax\": " + pax.TBOPrice.YQTax + ",");
                        requestData.AppendLine("\"AdditionalTxnFee\": " + pax.TBOPrice.AdditionalTxnFee + ",");
                        requestData.AppendLine("\"AirTransFee\": " + pax.TBOPrice.AirlineTransFee);
                        requestData.AppendLine("}");

                        if (pax.BaggageCode != null && pax.BaggageCode.Length > 0 && pax.BaggageType != null)
                        {
                            requestData.Append(",");
                            requestData.AppendLine("\"Baggage\":[");

                            //if (Twoway != 0)
                            //{
                            //    string[] str = pax.BaggageType.Split('|');
                            //    if (Twoway == 1)
                            //    {
                            //        BaggageType = str[1];
                            //    }
                            //    pax.BaggageType = (Twoway == 1) ? str[0] : BaggageType;
                            //}
                            if (pax.BaggageType != null)
                            {
                                string[] baggageData = pax.BaggageType.Split('|');//Split according to Way-Type (One-Way/Return)
                                for (int b = 0; b < baggageData.Length; b++)
                                {
                                    if (baggageData[b].Length > 0)
                                    {
                                        requestData.AppendLine("{");
                                        string[] data = baggageData[b].Split(',');
                                        for (int d = 0; d < data.Length; d++)
                                        {
                                            string bd = data[d];
                                            switch (bd.Split(':')[0])
                                            {
                                                case "WayType":
                                                    requestData.AppendLine("\"WayType\":" + bd.Split(':')[1] + ",");
                                                    break;
                                                case "Code":
                                                    requestData.AppendLine("\"Code\":\"" + bd.Split(':')[1] + "\",");
                                                    break;
                                                case "Description":
                                                    requestData.AppendLine("\"Description\":" + bd.Split(':')[1] + ",");
                                                    break;
                                                case "Weight":
                                                    requestData.AppendLine("\"Weight\":" + bd.Split(':')[1] + ",");
                                                    break;
                                                case "Currency":
                                                    requestData.AppendLine("\"Currency\":\"" + itinerary.Passenger[0].Price.SupplierCurrency + "\",");
                                                    break;
                                                case "Price":
                                                    requestData.AppendLine("\"Price\":" + bd.Split(':')[1] + ",");
                                                    break;
                                                case "Origin":
                                                    requestData.AppendLine("\"Origin\":\"" + bd.Split(':')[1] + "\",");
                                                    break;
                                                case "Destination":
                                                    requestData.AppendLine("\"Destination\":\"" + bd.Split(':')[1] + "\"");
                                                    break;
                                            }
                                        }
                                        if (b < baggageData.Length - 1)
                                        {
                                            requestData.AppendLine("},");
                                        }
                                        else
                                        {
                                            requestData.AppendLine("}");
                                        }
                                    }
                                }
                            }
                            requestData.AppendLine("]");
                        }


                        if (i < itinerary.Passenger.Length - 1)
                        {
                            requestData.AppendLine("},");//Pax End
                        }
                        else
                        {
                            requestData.AppendLine("}");
                        }
                    }

                    requestData.AppendLine("]");//Passengers End
                    requestData.AppendLine("}");


                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirTicketRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(requestData.ToString());
                            sw.Close();

                            XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "Request");
                            filePath = XmlPath + "TBOAirTicketRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc1.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Ticket Request. Error" + ex.ToString(), "");
                    }

                    string response = GetResponse(requestData.ToString(), ticketUrl);

                    Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing got response for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirTicketResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(response);
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(response, "Request");
                            filePath = XmlPath + "TBOAirTicketResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Ticket Response. Error" + ex.ToString(), "");
                    }
                }
                else
                {
                    throw new Exception("Agent balance and booking amount mismatch. Agent Bal: " + balance.CashBalance + " " + " Booking Amount: " + bookingAmount);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to generate Ticket. Error : " + ex.ToString(), "");
                throw ex;
            }
            return doc;
        }
        /// <summary>
        /// To create ticket for Blocked PNR.Sending two ticket requests and merging response for combined results. this block option is only for
        /// GDS airlines.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="tickets"></param>
        /// <returns></returns>
        public TicketingResponse CreateTicket(ref FlightItinerary itinerary, out Ticket[] tickets)
        {
           
                TicketingResponse ticketingResponse = new TicketingResponse();
                tickets = new Ticket[0];
                try
                {
                    //Sending two ticket requests and merging response for combined results.
                    if (itinerary.PNR.Contains('|') && itinerary.AirLocatorCode.Contains('|')) //Nomral return for GDS ticket whick was hold. 
                    {
                        string[] PNRs = itinerary.PNR.Split('|');
                        string[] AirLocatorCodes = itinerary.AirLocatorCode.Split('|');
                        List<Ticket> tempTicket = new List<Ticket>();
                        for (int i = 0; i < 2; i++)
                        {
                            itinerary.PNR = PNRs[i];
                            itinerary.AirLocatorCode = AirLocatorCodes[i];

                            ticketingResponse = TempCreateTicket(ref itinerary, ref tempTicket);

                        }
                        tickets = tempTicket.ToArray();
                        itinerary.PNR = PNRs[0] + "|" + PNRs[1];
                        itinerary.AirLocatorCode = AirLocatorCodes[0] + "|" + AirLocatorCodes[1];
                    }
                    else
                    {
                        List<Ticket> tempTicket = new List<Ticket>();
                        ticketingResponse = TempCreateTicket(ref itinerary, ref tempTicket);
                        tickets = tempTicket.ToArray();
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to generate Ticket. Error : " + ex.ToString(), "");
                    throw ex;
                }
            return ticketingResponse;
        }

        /// <summary>
        /// To Create block PNR request and get response for GDS airlines.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="tickets"></param>
        /// <returns></returns>
        private TicketingResponse TempCreateTicket(ref FlightItinerary itinerary, ref List<Ticket> tickets)
        {
            TicketingResponse ticketingResponse = new TicketingResponse();


            try
            {
                AuthenticationResponse ar = Authenticate();
                if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                {
                    AgencyBalance balance = GetAgencyBalance(ar);

                    decimal bookingAmount = 0;

                    foreach (FlightPassenger pax in itinerary.Passenger)
                    {
                        if (pax.TBOPrice != null)
                        {
                            bookingAmount += pax.TBOPrice.PublishedFare;
                        }
                        else
                        {
                            bookingAmount += pax.Price.PublishedFare;
                        }
                    }

                    Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing balance checking started for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                    if (balance != null && balance.CashBalance > bookingAmount)
                    {
                        Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing started for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
                        StringBuilder requestData = new StringBuilder();
                        requestData.AppendLine("{");
                        requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        requestData.AppendLine("\"TraceId\": \"" + itinerary.TicketAdvisory + "\",");
                        requestData.AppendLine("\"PNR\": \"" + itinerary.PNR + "\",");

                        requestData.AppendLine("\"BookingId\":" + itinerary.AirLocatorCode);
                        requestData.AppendLine("}");

                        XmlDocument doc = null;

                        try
                        {
                            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                            {
                                string filePath = XmlPath + "TBOAirTicketRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(requestData.ToString());
                                sw.Close();

                                XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "Request");
                                filePath = XmlPath + "TBOAirTicketRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                doc1.Save(filePath);
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Create Ticket Request. Error" + ex.ToString(), "");
                        }

                        Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing request created for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                        string response = GetResponse(requestData.ToString(), ticketUrl);

                        Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing response created for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                        try
                        {
                            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                            {
                                string filePath = XmlPath + "TBOAirTicketResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(response);
                                sw.Close();

                                doc = JsonConvert.DeserializeXmlNode(response, "Request");
                                filePath = XmlPath + "TBOAirTicketResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                doc.Save(filePath);
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Create Ticket Response. Error" + ex.ToString(), "");
                        }

                        if (doc != null)
                        {

                            XmlNode errorNode = doc.SelectSingleNode("Request/Response/Error");
                            if (errorNode != null && errorNode.SelectSingleNode("ErrorMessage").InnerText.Length <= 0)
                            {                                
                                XmlNode ResponseNode = doc.SelectSingleNode("Request/Response/Response");
                                //itinerary.TourCode = ResponseNode.SelectSingleNode("BookingId").InnerText;                                
                                if (ResponseNode.SelectSingleNode("PNR") != null && ResponseNode.SelectSingleNode("FlightItinerary") != null)
                                {
                                    itinerary.PNR = ResponseNode.SelectSingleNode("PNR").InnerText;//Read PNR from TicketResponse
                                    ticketingResponse.Status = TicketingResponseStatus.Successful;

                                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                                    {
                                        bool used = false;
                                        FlightPassenger pax = itinerary.Passenger[i];
                                        Ticket ticket = null;
                                        if (tickets.Count > 0)
                                        {
                                            ticket = tickets.Find(delegate(Ticket t) { return t.PaxFirstName == pax.FirstName && t.PaxLastName == pax.LastName && t.Price.PriceId == pax.Price.PriceId; });
                                            if (ticket == null)
                                            {
                                                ticket = new Ticket();
                                            }
                                            else
                                            {
                                                used = true;
                                            }
                                        }
                                        else
                                        {
                                            ticket = new Ticket();
                                        }

                                        ticket.ConjunctionNumber = "";
                                        ticket.CorporateCode = "";
                                        ticket.CreatedBy = itinerary.CreatedBy;
                                        ticket.CreatedOn = DateTime.Now;
                                        ticket.Endorsement = "";
                                        ticket.ETicket = true;
                                        ticket.FareCalculation = "";
                                        ticket.FareRule = "";
                                        ticket.FlightId = itinerary.FlightId;
                                        ticket.IssueDate = DateTime.Now;
                                        ticket.LastModifiedBy = itinerary.CreatedBy;
                                        ticket.PaxFirstName = itinerary.Passenger[i].FirstName;
                                        ticket.PaxId = itinerary.Passenger[i].PaxId;
                                        ticket.PaxLastName = itinerary.Passenger[i].LastName;
                                        ticket.PaxType = itinerary.Passenger[i].Type;
                                        ticket.Price = itinerary.Passenger[i].Price;
                                        ticket.Status = "Ticketed";
                                        ticket.TicketAdvisory = "";
                                        ticket.TicketDesignator = "";

                                        XmlNode PaxNode = ResponseNode.SelectNodes("FlightItinerary/Passenger")[i];
                                        if (PaxNode.SelectSingleNode("Ticket") != null && PaxNode.SelectSingleNode("Ticket").SelectSingleNode("TicketNumber") != null)
                                        {
                                            if (ticket.TicketNumber != null && ticket.TicketNumber.Length > 0)
                                            {
                                                if (PaxNode.SelectSingleNode("Ticket").SelectSingleNode("ValidatingAirline") != null)
                                                {
                                                    ticket.TicketNumber += "|" + PaxNode.SelectSingleNode("Ticket").SelectSingleNode("ValidatingAirline").InnerText + PaxNode.SelectSingleNode("Ticket").SelectSingleNode("TicketNumber").InnerText;
                                                }
                                                else
                                                {
                                                    ticket.TicketNumber += "|" + PaxNode.SelectSingleNode("Ticket").SelectSingleNode("TicketNumber").InnerText;
                                                }
                                            }
                                            else
                                            {
                                                if (PaxNode.SelectSingleNode("Ticket").SelectSingleNode("ValidatingAirline") != null)
                                                {
                                                    ticket.TicketNumber = PaxNode.SelectSingleNode("Ticket").SelectSingleNode("ValidatingAirline").InnerText + PaxNode.SelectSingleNode("Ticket").SelectSingleNode("TicketNumber").InnerText;
                                                }
                                                else
                                                {
                                                    ticket.TicketNumber = PaxNode.SelectSingleNode("Ticket").SelectSingleNode("TicketNumber").InnerText;
                                                }
                                            }
                                            if (ticket.StockType != null && ticket.StockType.Length > 0)
                                            {
                                                ticket.StockType += "|" + PaxNode.SelectSingleNode("Ticket").SelectSingleNode("TicketId").InnerText;//Store TicketId for Cancellation
                                            }
                                            else
                                            {
                                                ticket.StockType = PaxNode.SelectSingleNode("Ticket").SelectSingleNode("TicketId").InnerText;//Store TicketId for Cancellation
                                            }
                                        }
                                        else
                                        {
                                            ticket.TicketNumber = itinerary.PNR;
                                        }

                                        ticket.TicketType = "";
                                        ticket.Title = itinerary.Passenger[i].Title;

                                        ticket.PtcDetail = new List<SegmentPTCDetail>();

                                        for (int j = 0; j < itinerary.Segments.Length; j++)
                                        {
                                            SegmentPTCDetail ptcDetail = new SegmentPTCDetail();
                                            FlightInfo segment = itinerary.Segments[j];
                                            XmlNodeList BaggageNodes = ResponseNode.SelectNodes("FlightItinerary/Passenger")[i].SelectNodes("SegmentAdditionalInfo");

                                            if (BaggageNodes != null)
                                            {
                                                if (BaggageNodes[0] != null && BaggageNodes[0].SelectSingleNode("Baggage").InnerText.Length == 0)
                                                {
                                                    ptcDetail.Baggage = "";
                                                }
                                                else
                                                {
                                                    ptcDetail.Baggage = (BaggageNodes[0] != null) ? BaggageNodes[0].SelectSingleNode("Baggage").InnerText : string.Empty;
                                                }
                                                ptcDetail.FareBasis = (BaggageNodes[0] != null) ? BaggageNodes[0].SelectSingleNode("FareBasis").InnerText : string.Empty;
                                            }
                                            else
                                            {
                                                ptcDetail.Baggage = "";
                                                ptcDetail.FareBasis = "";
                                            }
                                            ptcDetail.FlightKey = segment.FlightKey;
                                            ptcDetail.NVA = "";
                                            ptcDetail.NVB = "";
                                            ptcDetail.SegmentId = segment.SegmentId;
                                            switch (itinerary.Passenger[i].Type)
                                            {
                                                case PassengerType.Adult:
                                                    ptcDetail.PaxType = "ADT";
                                                    break;
                                                case PassengerType.Child:
                                                    ptcDetail.PaxType = "CNN";
                                                    break;
                                                case PassengerType.Infant:
                                                    ptcDetail.PaxType = "INF";
                                                    break;
                                            }

                                            ticket.PtcDetail.Add(ptcDetail);
                                        }

                                        //Update AirlinePNR
                                        XmlNodeList Segments = ResponseNode.SelectNodes("FlightItinerary/Segments");

                                        foreach (FlightInfo seg in itinerary.Segments)
                                        {
                                            foreach (XmlNode segment in Segments)
                                            {
                                                if (seg.Origin.AirportCode == segment.SelectSingleNode("Origin/Airport/AirportCode").InnerText)
                                                {
                                                    if (segment.SelectSingleNode("AirlinePNR") != null && segment.SelectSingleNode("AirlinePNR").InnerText.Length > 0)
                                                    {
                                                        seg.AirlinePNR = segment.SelectSingleNode("AirlinePNR").InnerText;
                                                        seg.Save();
                                                    }
                                                    break;
                                                }
                                            }
                                        }

                                        if (!used)
                                        {
                                            tickets.Add(ticket);
                                        }
                                    }
                                    Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing Successful PNR " + itinerary.PNR + " for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
                                }
                                else
                                {
                                    ticketingResponse.Status = TicketingResponseStatus.OtherError;
                                    ticketingResponse.Message = "Ticket details missing from Ticket Response";
                                    Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing Failed for PNR " + itinerary.PNR + " as Ticket details are missing in response for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
                                }                                
                            }
                            else
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir)Failed to Generate Ticket. " + (errorNode != null ? errorNode.SelectSingleNode("ErrorMessage").InnerText : ""), "");
                                ticketingResponse.Status = TicketingResponseStatus.OtherError;
                                ticketingResponse.Message = errorNode.SelectSingleNode("ErrorMessage").InnerText;
                            }
                        }
                        else
                        {
                            ticketingResponse.Status = TicketingResponseStatus.OtherError;
                            ticketingResponse.Message = "Ticketed response returned empty or corrupted from TBO Air.";
                        }
                    }
                    else
                    {
                        ticketingResponse.Status = TicketingResponseStatus.OtherError;
                        ticketingResponse.Message = "Agent balance and booking amount mismatch. Agent Bal: " + balance.CashBalance + " " + "Booking Amount: " + bookingAmount;
                    }
                }
                else
                {
                    ticketingResponse.Message = "Agent profile retrieve failed from TBO Air.";
                    ticketingResponse.Status = TicketingResponseStatus.OtherError;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to generate Ticket. Reason : " + ex.ToString(), "");
                ticketingResponse.Message = ex.Message;
                ticketingResponse.Status = TicketingResponseStatus.OtherError;
                throw ex;
            }
            return ticketingResponse;
        }

        /// <summary>
        /// Get Booking Details
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="tickets"></param>
        protected void GetBookingDetails(FlightItinerary itinerary, ref Ticket[] tickets)
        {
            try
            {
                AuthenticationResponse ar = Authenticate();

                if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                {
                    StringBuilder requestData = new StringBuilder();
                    requestData.AppendLine("{");
                    requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                    requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                    requestData.AppendLine("\"TraceId\": \"" + itinerary.TicketAdvisory + "\",");
                    requestData.AppendLine("\"PNR\": \"" + itinerary.PNR + "\",");
                    requestData.AppendLine("\"BookingId\":" + itinerary.AirLocatorCode);
                    requestData.AppendLine("}");

                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirGetBookingDetailsRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(requestData.ToString());
                            sw.Close();

                            XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "Request");
                            filePath = XmlPath + "TBOAirGetBookingDetailsRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc1.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetBooking Request. Error" + ex.ToString(), "");
                    }
                    string response = GetResponse(requestData.ToString(), getBookingDetailUrl);
                    XmlDocument doc = null;

                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirGetBookingDetailsResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(requestData.ToString());
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(requestData.ToString(), "Request");
                            filePath = XmlPath + "TBOAirGetBookingDetailsResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetBooking Response. Error" + ex.ToString(), "");
                    }

                    if (doc != null)
                    {
                        XmlNode ErrorNode = doc.SelectSingleNode("Response/Error");

                        if (ErrorNode != null && ErrorNode.SelectSingleNode("ErrorCode").InnerText == "0")
                        {
                            XmlNode ItineraryNode = doc.SelectSingleNode("Response/FlightItinerary");

                            for (int i = 0; i < ItineraryNode.SelectNodes("Passenger").Count; i++)
                            {
                                XmlNode BaggageNode = ItineraryNode.SelectNodes("Passenger")[i].SelectSingleNode("Baggage");
                                foreach (SegmentPTCDetail ptcDetail in tickets[i].PtcDetail)
                                {
                                    ptcDetail.Baggage = BaggageNode.SelectSingleNode("Weight").InnerText + "Kg," + BaggageNode.SelectSingleNode("Weight").InnerText + "Kg";
                                    ptcDetail.FareBasis = BaggageNode.SelectSingleNode("Code").InnerText;
                                    ptcDetail.FlightKey = "";
                                    switch (ItineraryNode.SelectNodes("Passenger")[i].SelectSingleNode("PaxType").InnerText)
                                    {
                                        case "1":
                                            ptcDetail.PaxType = "ADT";
                                            break;
                                        case "2":
                                            ptcDetail.PaxType = "CNN";
                                            break;
                                        case "3":
                                            ptcDetail.PaxType = "INF";
                                            break;
                                    }
                                    ptcDetail.NVA = "";
                                    ptcDetail.NVB = "";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to get Booking details. Error : " + ex.ToString(), "");
                throw ex;
            }
        }


        /// <summary>
        /// This method is used to Release PNR for HOLD bookings (GDS).
        /// </summary>
        /// <param name="bookingId"></param>
        /// <param name="sources"></param>
        /// <returns></returns>
        public bool ReleasePNR(int bookingId, string sources)
        {
            bool Released = false;
            try
            {
                AuthenticationResponse ar = Authenticate();

                if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                {
                    StringBuilder requestData = new StringBuilder();
                    requestData.AppendLine("{");
                    requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                    requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                    requestData.AppendLine("\"BookingId\": \"" + bookingId + "\",");
                    requestData.AppendLine("\"Source\":\"" + sources + "\"");
                    requestData.AppendLine("}");

                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirReleasePNRRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(requestData.ToString());
                            sw.Close();

                            XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "Request");
                            filePath = XmlPath + "TBOAirReleasePNRRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc1.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to Release PNR Request. Error" + ex.ToString(), "");
                    }
                    string response = GetResponse(requestData.ToString(), releasePNRUrl);
                    XmlDocument doc = null;

                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirReleasePNRResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(response.ToString());
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(response.ToString(), "Request");
                            filePath = XmlPath + "TBOAirReleasePNRResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to Release PNR Response. Error" + ex.ToString(), "");
                    }

                    if (doc != null)
                    {
                        XmlNode errorNode = doc.SelectSingleNode("Request/Response/Error");
                        if (errorNode != null && errorNode.SelectSingleNode("ErrorCode").InnerText == "0")
                        {
                            if (doc.SelectSingleNode("Request/Response/ResponseStatus").InnerText == "1")//Successful
                            {
                                Released = true;
                            }
                            else //Failed
                            {
                                Released = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir)Failed to Release PNR for BookingId : " + bookingId + ". Error : " + ex.ToString(), "");
            }
            return Released;
        }

        /// <summary>
        /// Method is incomplete. still coding need to done
        /// </summary>
        /// <param name="itinerary"></param>
        public Dictionary<string, string> SendChangeRequest(FlightItinerary itinerary)
        {
            Dictionary<string, string> cancellationData = new Dictionary<string, string>();
            bool mainCancelStatus = true;
            decimal totalCancelAmount = 0;
            try
            {
                AuthenticationResponse ar = Authenticate();
                if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                {
                    string[] bookingIds = new string[0];
                    if (!string.IsNullOrEmpty(itinerary.AirLocatorCode))//MultipleBookind id's
                    {
                        bookingIds = itinerary.AirLocatorCode.TrimEnd('|').Split('|');
                    }
                    List<Ticket> ticketList = CT.BookingEngine.Ticket.GetTicketList(itinerary.FlightId);
                    ///////////////////////////////////////////////////////////////////////////////////////////
                    /// ////////      Booking Id's wise need send Changes Request    //////////////////////////
                    ////////////      TicketId Need To send Comma Separted Values     /////////////////////////
                    ////////////      All Booking id's confirm that time only Booking stats changed  /////////
                    //////////////////////////////////////////////////////////////////////////////////////////
                    for (int i = 0; i < bookingIds.Length; i++)
                    {
                        string ticketId = string.Empty;
                        for (int k = 0; k < ticketList.Count; k++)
                        {
                            string[] ticketIds = new string[0];
                            if (!string.IsNullOrEmpty(ticketList[k].StockType))
                            {
                                ticketIds = ticketList[k].StockType.Split('|');
                            }

                            if (bookingIds.Length == ticketIds.Length)
                            {
                                if (string.IsNullOrEmpty(ticketId))
                                {
                                    ticketId = ticketIds[i];
                                }
                                else
                                {
                                    ticketId = ticketId + "," + ticketIds[i];
                                }
                            }
                        }
                        string bookingId = bookingIds[i];
                        DataTable dtCancelIds = ChangeRequestStatus.LoadCancelIds(Convert.ToInt64(bookingId));
                        if (dtCancelIds == null || dtCancelIds.Rows.Count <= 0)
                        {
                            StringBuilder requestData = new StringBuilder();
                            requestData.AppendLine("{");
                            requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                            requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                            requestData.AppendLine("\"BookingId\": " + bookingId + ",");
                            requestData.AppendLine("\"RequestType\":" + 1 + ",");
                            requestData.AppendLine("\"CancellationType\":" + 2 + ",");
                            //requestData.AppendLine("\"TicketId\":[" + ticketId + "],");
                            requestData.AppendLine("\"Remarks\":\"" + "Test" + "\"");
                            requestData.AppendLine("}");

                            try
                            {
                                if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                                {
                                    string filePath = XmlPath + "TBOAirChangeRequestJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                                    StreamWriter sw = new StreamWriter(filePath);
                                    sw.Write(requestData.ToString());
                                    sw.Close();

                                    XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "Request");
                                    filePath = XmlPath + "TBOAirChangeRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                    doc1.Save(filePath);
                                }
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to Get Change Request. Error" + ex.ToString(), "");
                            }
                            string response = GetResponse(requestData.ToString(), changeRequestUrl);
                            XmlDocument doc = null;

                            try
                            {
                                if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                                {
                                    string filePath = XmlPath + "TBOChangeResponseJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                                    StreamWriter sw = new StreamWriter(filePath);
                                    sw.Write(response.ToString());
                                    sw.Close();

                                    doc = JsonConvert.DeserializeXmlNode(response.ToString(), "Request");
                                    filePath = XmlPath + "TBOAirChangeResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                    doc.Save(filePath);
                                }
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to Changerequest Response. Error" + ex.ToString(), "");
                            }
                            if (doc != null)
                            {
                                if (doc.SelectSingleNode("Request/Response/ResponseStatus").InnerText == "1")//Successful
                                {
                                    XmlNodeList changeList = doc.SelectNodes("Request/Response/TicketCRInfo");
                                    if (changeList != null)
                                    {
                                        //ChangeRequestIds giving multiples but need to send and one by one
                                        int k = 0;
                                        foreach (XmlNode change in changeList)//ChangeRequestId's giving  List only
                                        {
                                            XmlNode ChangeStatus = change.SelectSingleNode("ChangeRequestStatus");
                                            if (ChangeStatus == null)
                                            {
                                                ChangeStatus = change.SelectSingleNode("Status");
                                            }
                                            //if (ChangeStatus != null && ChangeStatus.InnerText == "1") //1 is success
                                            if (ChangeStatus != null && (ChangeStatus.InnerText == "1" || ChangeStatus.InnerText == "4")) //1 is success, 4 is completed (LCC)
                                            {
                                                long cancelId = 0;
                                                XmlNode changeRequestId = change.SelectSingleNode("ChangeRequestId");
                                                if (changeRequestId != null)
                                                {
                                                    cancelId = Convert.ToInt64(changeRequestId.InnerText);
                                                }
                                                string[] tkts = ticketId.Split(',');
                                                long tickId = 0;
                                                if (tkts != null && tkts.Length > 0)
                                                {
                                                    tickId = Convert.ToInt64(tkts[k]);
                                                }
                                                bool canceled = false;
                                                decimal canceledAmount = 0;
                                                ChangeRequestStatus changeRequestobj = new ChangeRequestStatus();
                                                changeRequestobj.CancelId = cancelId;
                                                changeRequestobj.TicketId = tickId;
                                                changeRequestobj.Status = "P";
                                                changeRequestobj.BookingId = Convert.ToInt64(bookingId);
                                                changeRequestobj.Save();
                                                canceled = GetChangeRequestStatus(cancelId, tickId, ref canceledAmount);
                                                if (!canceled)
                                                {
                                                    mainCancelStatus = false;
                                                }
                                                else
                                                {
                                                    totalCancelAmount += canceledAmount;
                                                }
                                            }
                                            else
                                            {
                                                mainCancelStatus = false;
                                            }
                                            k++;
                                        }
                                    }
                                    else
                                    {
                                        mainCancelStatus = false;
                                    }
                                }
                                else //Failed
                                {
                                    mainCancelStatus = false;
                                }
                            }
                            else
                            {
                                mainCancelStatus = false;
                            }
                        }
                        else
                        {
                            foreach (DataRow dr in dtCancelIds.Rows)
                            {
                                decimal canceledAmount = 0;
                                if (Convert.ToString(dr["cancelStatus"]) != "C")
                                {
                                    bool canceled = false;
                                    canceled = GetChangeRequestStatus(Convert.ToInt64(dr["changeRequestId"]), Convert.ToInt64(dr["ticketId"]), ref canceledAmount);
                                    if (!canceled)
                                    {
                                        mainCancelStatus = false;
                                    }
                                    else
                                    {
                                        totalCancelAmount += canceledAmount;
                                    }
                                }
                                else
                                {
                                    totalCancelAmount += Convert.ToDecimal(dr["cancelAmount"]);
                                }
                            }
                        }
                    }
                    if (mainCancelStatus)
                    {
                        rateOfExchange = exchangeRates["INR"];
                        cancellationData.Add("Cancelled", "True");
                        cancellationData.Add("Charges", (Convert.ToDecimal(totalCancelAmount) * rateOfExchange).ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                cancellationData = new Dictionary<string, string>();
                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir)Failed to SendChangeRequest for PNR : " + itinerary.PNR + ". Error : " + ex.ToString(), "");
            }
            return cancellationData;
        }


        public bool GetChangeRequestStatus(long cancelId, long tickId, ref decimal canceledAmount)
        {
            bool canceled = false;
            try
            {
                AuthenticationResponse ar = Authenticate();

                if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                {
                    StringBuilder requestData = new StringBuilder();
                    requestData.AppendLine("{");
                    requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                    requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                    requestData.AppendLine("\"ChangeRequestId\": \"" + cancelId + "\"");
                    requestData.AppendLine("}");

                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOAirChangeRequestStstusJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(requestData.ToString());
                            sw.Close();

                            XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "Request");
                            filePath = XmlPath + "TBOAirChangeRequestStatus_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc1.Save(filePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to Get Change RequestStstus. Error" + ex.ToString(), "");
                    }
                    string response = GetResponse(requestData.ToString(), getChangeRequestStatusUrl);
                    XmlDocument doc = null;

                    try
                    {
                        if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                        {
                            string filePath = XmlPath + "TBOChangeResponseStatusJSON_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".txt";
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(response.ToString());
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(response.ToString(), "Request");
                            filePath = XmlPath + "TBOAirChangeResponseStatus_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }

                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to Changerequest Response Status. Error" + ex.ToString(), "");
                    }
                    if (doc != null)
                    {
                        if (doc.SelectSingleNode("Request/Response/ResponseStatus").InnerText == "1")//Successful
                        {
                            XmlNode requestStatus = doc.SelectSingleNode("Request/Response/ChangeRequestStatus");
                            if (requestStatus != null && requestStatus.InnerText == "4")//Completed
                            {
                                canceled = true;
                                XmlNode cancelCharge = doc.SelectSingleNode("Request/Response/CancellationCharge");
                                if (cancelCharge != null)
                                {
                                    canceledAmount += Convert.ToDecimal(cancelCharge.InnerText);
                                }
                                XmlNode serviceTax = doc.SelectSingleNode("Request/Response/ServiceTaxOnRAF");
                                if (serviceTax != null)
                                {
                                    canceledAmount += Convert.ToDecimal(serviceTax.InnerText);
                                }
                                XmlNode swachhBharatCess = doc.SelectSingleNode("Request/Response/SwachhBharatCess");
                                if (swachhBharatCess != null)
                                {
                                    canceledAmount += Convert.ToDecimal(swachhBharatCess.InnerText);
                                }
                                ChangeRequestStatus changeRequestobj = new ChangeRequestStatus();
                                changeRequestobj.CancelId = cancelId;
                                changeRequestobj.TicketId = tickId;
                                changeRequestobj.Status = "C";
                                changeRequestobj.CancelAmount = canceledAmount;
                                changeRequestobj.Remarks = "Completed";

                                changeRequestobj.Save();
                            }
                            else
                            {
                                string remarks = string.Empty;
                                if (requestStatus != null)
                                {
                                    if (requestStatus.InnerText == "0")
                                    {
                                        remarks = "NotSet";
                                    }
                                    else if (requestStatus.InnerText == "1")
                                    {
                                        remarks = "Unassigned";
                                    }
                                    else if (requestStatus.InnerText == "2")
                                    {
                                        remarks = "Assigned";
                                    }
                                    else if (requestStatus.InnerText == "3")
                                    {
                                        remarks = "Acknowledged";
                                    }
                                    else if (requestStatus.InnerText == "5")
                                    {
                                        remarks = "Rejected";
                                    }
                                    else if (requestStatus.InnerText == "6")
                                    {
                                        remarks = "Closed";
                                    }
                                    else if (requestStatus.InnerText == "7")
                                    {
                                        remarks = "Pending";
                                    }
                                    else if (requestStatus.InnerText == "8")
                                    {
                                        remarks = "Other";
                                    }
                                }
                                ChangeRequestStatus changeRequestobj = new ChangeRequestStatus();
                                changeRequestobj.CancelId = cancelId;
                                changeRequestobj.TicketId = tickId;
                                changeRequestobj.Status = "P";
                                changeRequestobj.CancelAmount = canceledAmount;
                                changeRequestobj.Remarks = remarks;
                                changeRequestobj.Save();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                canceled = false;
                Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBOAir)Failed to SendChangeRequestStatus for TicketId : " + tickId + ". Error : " + ex.ToString(), "");
            }
            return canceled;
        }

    }

    public class AuthenticationRequest
    {
        public string ClientId;
        public string UserName;
        public string Password;
        public string EndUserIP;
    }

    public class AuthenticationResponse
    {
        public string Status;
        public string TokenId;
        public Error Error;
        public Member Member;
    }

    public struct Error
    {
        public string ErrorCode;
        public string ErrorMessage;
    }

    public struct Member
    {
        public string FirstName;
        public string LastName;
        public string Email;
        public int MemberId;
        public int AgencyId;
        public string LoginName;
        public string LoginDetails;
        public bool IsPrimaryAgent;
    }

    public class AgencyBalance
    {
        public int AgencyType;
        public decimal CashBalance;
        public decimal CreditBalance;
        public Error Error;
        public int Status;
    }


    
}
