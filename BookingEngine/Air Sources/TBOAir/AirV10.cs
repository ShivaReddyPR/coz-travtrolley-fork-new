﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CT.Configuration;
using System.IO;
using System.Net;
using System.IO.Compression;
using Newtonsoft.Json;
using CT.BookingEngine;
using CT.Core;
using System.Xml;
using System.Data;
using System.Threading;
using System.Web.Script.Serialization;

/// <summary>
/// Common Namespace for all Products (Flight, Hotel)
/// </summary>
namespace TBOAir
{
    /// <summary>
    /// Provides Flight services related operations
    /// </summary>
    public class AirV10:IDisposable
    {
        AuthenticationResponse ar = new AuthenticationResponse();
        /// <summary>
        /// Path for writing xml/json files
        /// </summary>
        protected string XmlPath = string.Empty;
        /// <summary>
        /// LoginName for API
        /// </summary>
        string loginName = string.Empty;
        /// <summary>
        /// Password for API
        /// </summary>
        string password = string.Empty;
        /// <summary>
        /// ClientID for API
        /// </summary>
        string clientId = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string authenticateUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string logoutUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string getAgencyBalanceUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string searchUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string priceRBDUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string fareRuleUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string fareQuoteUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string ssrUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string bookUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string ticketUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string changeRequestUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string getBookingDetailUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string releasePNRUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        string getChangeRequestStatusUrl = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        int Twoway = 0;
        /// <summary>
        /// 
        /// </summary>
        string agentBaseCurrency;
        /// <summary>
        /// 
        /// </summary>
        Dictionary<string, decimal> exchangeRates;
        /// <summary>
        /// 
        /// </summary>
        decimal rateOfExchange = 1;
        /// <summary>
        /// Default decimal value to be rounded off when not assigned
        /// </summary>
        int decimalValue = 3;
        //bool testMode = true;
        /// <summary>
        /// Global sessionId
        /// </summary>
        string sessionId;
        /// <summary>
        /// Login UserID
        /// </summary>
        int appUserId;
        /// <summary>
        /// 
        /// </summary>
        string endUserIp = "183.82.111.104";
        /// <summary>
        /// 
        /// </summary>
        DataTable dtBaggage = new DataTable();
        /// <summary>
        /// 
        /// </summary>
        static string BaggageType = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        List<SearchResult> CombineResults = new List<SearchResult>();
        /// <summary>
        /// 
        /// </summary>
        static SearchRequest Request = new SearchRequest();
        /// <summary>
        /// AutoEvent for creating Search threads in order to run simultaneous searches
        /// </summary>
        AutoResetEvent[] eventFlag = new AutoResetEvent[0];

        /// <summary>
        /// Showing PublishedFare for Corp. Assigning showPubFaresForCorp will enable corp.
        /// </summary>
        bool showPubFaresForCorp;

        bool searchBySegments;

        /// <summary>
        /// To hold agent level prefered airlines
        /// </summary>
        string _privateFareCodes;

        List<SearchRequest> searchRequests = new List<SearchRequest>();
        #region Properties
        //AuthenticationResponse loginResponse = null;
        /// <summary>
        /// Global sessionId of the search assigned from MSE
        /// </summary>
        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }

        /// <summary>
        /// Login UserId under the Agent
        /// </summary>
        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }

        /// <summary>
        /// UserName for the API
        /// </summary>
        public string LoginName
        {
            get { return loginName; }
            set { loginName = value; }
        }
        /// <summary>
        /// Password for the API
        /// </summary>
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        /// <summary>
        /// Base currency used by the Agent
        /// </summary>
        public string AgentBaseCurrency
        {
            get { return agentBaseCurrency; }
            set { agentBaseCurrency = value; }
        }
        /// <summary>
        /// Exchange rates to be used for conversion in Agent currency from suplier currency
        /// </summary>
        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }
        /// <summary>
        /// Decimal value to be rounded off
        /// </summary>
        public int AgentDecimalValue
        {
            get { return decimalValue; }
            set { decimalValue = value; }
        }
        /// <summary>
        /// ClientID for the API
        /// </summary>
        public string ClientId
        {
            get { return clientId; }
            set { clientId = value; }
        }

        /// <summary>
        /// Showing PublishedFare for Corp. Assigning showPubFaresForCorp will enable corp
        /// </summary>
        public bool ShowPubFaresForCorp { get => showPubFaresForCorp; set => showPubFaresForCorp = value; }
        public bool SearchBySegments { get => searchBySegments; set => searchBySegments = value; }
        public string PrivateFareCodes { get => _privateFareCodes; set => _privateFareCodes = value; }
        #endregion
        /// <summary>
        /// Default constructor
        /// </summary>
        public AirV10()
        {
            try
            {
                //Load credentials from AgentSourceDetails
                //loginName = ConfigurationSystem.TBOAirConfig["UserName"];
                //password = ConfigurationSystem.TBOAirConfig["Password"];
                XmlPath = ConfigurationSystem.TBOAirConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMMyyyy") + "\\";
                if (ConfigurationSystem.TBOAirConfig.ContainsKey("ClientId"))
                {
                    clientId = ConfigurationSystem.TBOAirConfig["ClientId"];
                }
                else
                {
                    //ApiIntegration for test
                    //"tboprod" for Live
                    clientId = "ApiIntegration";
                }
                if (!Directory.Exists(XmlPath))
                {
                    Directory.CreateDirectory(XmlPath);
                }
                authenticateUrl = ConfigurationSystem.TBOAirConfig["Authenticate"];
                logoutUrl = ConfigurationSystem.TBOAirConfig["Logout"];
                getAgencyBalanceUrl = ConfigurationSystem.TBOAirConfig["GetAgencyBalance"];
                searchUrl = ConfigurationSystem.TBOAirConfig["Search"];
                priceRBDUrl = ConfigurationSystem.TBOAirConfig["PriceRBD"];
                fareRuleUrl = ConfigurationSystem.TBOAirConfig["FareRule"];
                fareQuoteUrl = ConfigurationSystem.TBOAirConfig["FareQuote"];
                ssrUrl = ConfigurationSystem.TBOAirConfig["SSR"];
                bookUrl = ConfigurationSystem.TBOAirConfig["Book"];
                ticketUrl = ConfigurationSystem.TBOAirConfig["Ticket"];
                changeRequestUrl = ConfigurationSystem.TBOAirConfig["SendChangeRequest"];
                getBookingDetailUrl = ConfigurationSystem.TBOAirConfig["GetBookingDetails"];
                releasePNRUrl = ConfigurationSystem.TBOAirConfig["ReleasePNR"];
                getChangeRequestStatusUrl = ConfigurationSystem.TBOAirConfig["GetChangeRequestStatus"];

                if (dtBaggage.Columns.Count == 0)
                {
                    dtBaggage.Columns.Add("WayType", typeof(string));
                    dtBaggage.Columns.Add("Group", typeof(int));
                    dtBaggage.Columns.Add("Code", typeof(string));
                    dtBaggage.Columns.Add("Description", typeof(string));
                    dtBaggage.Columns.Add("Weight", typeof(int));
                    dtBaggage.Columns.Add("Price", typeof(decimal));
                    dtBaggage.Columns.Add("SupplierPrice", typeof(decimal));
                    dtBaggage.Columns.Add("Currency", typeof(string));
                    dtBaggage.Columns.Add("Origin", typeof(string));
                    dtBaggage.Columns.Add("Destination", typeof(string));
                }
                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir) Failed to read config xml. Error " + ex.ToString (), "");
            }
        }

       /// <summary>
       /// Method for retrieving response based on the request and url
       /// </summary>
       /// <param name="requestData"></param>
       /// <param name="url"></param>
       /// <returns></returns>
        private string GetResponse(string requestData, string url)
        {
            string responseFromServer = string.Empty;
            string responseXML = string.Empty;
            try
            {
                string contentType = "application/json";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST"; //Using POST method       
                string postData = requestData;// GETTING XML STRING...
                request.ContentType = contentType;
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                // request for compressed response. This does not seem to have any effect now.
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                request.ContentLength = bytes.Length;

                Stream requestWriter = (request.GetRequestStream());

                requestWriter.Write(bytes, 0, bytes.Length);
                requestWriter.Close();

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();
            }
            catch (WebException webEx)
            {
                //get the response stream
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                String responseMessage = new StreamReader(stream).ReadToEnd();
            }
            return responseFromServer;
        }
        /// <summary>
        /// Method for decrypting response compression i.e GZIP, Deflate, default
        /// </summary>
        /// <param name="webResponse"></param>
        /// <returns></returns>
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        } 

        /// <summary>
        /// Method for loggging in to API
        /// </summary>
        /// <returns></returns>
        private void Authenticate()
        {
            try
            {
                //if (string.IsNullOrEmpty(ar.TokenId))
                {
                    StringBuilder requestData = new StringBuilder();
                    requestData.AppendLine("{");
                    requestData.AppendLine("\"ClientId\": \"" + clientId + "\",");
                    requestData.AppendLine("\"UserName\": \"" + loginName + "\",");
                    requestData.AppendLine("\"Password\": \"" + password + "\",");
                    requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\"");
                    requestData.AppendLine("}");

                    JsonSerializerSettings settings = new JsonSerializerSettings();
                    settings.ObjectCreationHandling = ObjectCreationHandling.Auto;
                    settings.StringEscapeHandling = StringEscapeHandling.Default;
                    settings.Formatting = Newtonsoft.Json.Formatting.Indented;

                    try
                    {
                        try
                        {
                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirAuthenticateRequestJSON.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic searchRequest = js.Deserialize<dynamic>(requestData.ToString());
                            string rsp = JsonConvert.SerializeObject(searchRequest, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            TextReader reader = new StringReader(requestData.ToString());
                            AuthenticationRequest authRequest = (AuthenticationRequest)JsonSerializer.Create(settings).Deserialize(reader, typeof(AuthenticationRequest));
                            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AuthenticationRequest));
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirAuthenticationRequest.xml";
                            sw = new StreamWriter(filePath);
                            ser.Serialize(sw, authRequest);
                            sw.Close();

                        }
                        catch { }
                    }
                    catch
                    {
                        //Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Authenticate Request. Error" + ex.ToString(), "");
                    }

                    string response = string.Empty;


                    response = GetResponse(requestData.ToString(), authenticateUrl);


                    try
                    {
                        string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirAuthenticateResponseJSON.json";
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        dynamic searchResponse = js.Deserialize<dynamic>(response);
                        string rsp = JsonConvert.SerializeObject(searchResponse, Newtonsoft.Json.Formatting.Indented);
                        StreamWriter sw = new StreamWriter(filePath);
                        sw.Write(rsp);
                        sw.Close();

                        TextReader reader = new StringReader(response);
                        ar = (AuthenticationResponse)JsonSerializer.Create(settings).Deserialize(reader, typeof(AuthenticationResponse));
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AuthenticationResponse));
                        filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirAuthenticationResponse.xml";
                        sw = new StreamWriter(filePath);
                        ser.Serialize(sw, ar);
                        sw.Close();
                    }
                    catch
                    {
                        //Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save Authenticate Response. Error " + ex.ToString(), "");
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate. Reason : " + ex.ToString(), "");
            }            
        }

        /// <summary>
        /// Method for checking our balance before Book or Ticketing
        /// </summary>
        /// <param name="ar"></param>
        /// <returns></returns>
        private AgencyBalance GetAgencyBalance(AuthenticationResponse ar)
        {
            AgencyBalance balance = new AgencyBalance();

            try
            {
                if (ar != null)
                {
                    if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                    {
                        StringBuilder requestData = new StringBuilder();
                        requestData.AppendLine("{");
                        requestData.AppendLine("\"ClientId\": \"" + clientId + "\",");
                        requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        requestData.AppendLine("\"TokenAgencyId\": " + ar.Member.AgencyId + ",");
                        requestData.AppendLine("\"TokenMemberId\": " + ar.Member.MemberId + ",");
                        requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        requestData.AppendLine("}");

                        try
                        {

                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetAgencyBalanceRequestJSON.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic agencyBalRequest = js.Deserialize<dynamic>(requestData.ToString());
                            string rsp = JsonConvert.SerializeObject(agencyBalRequest, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            XmlDocument doc = JsonConvert.DeserializeXmlNode(requestData.ToString(), "AgencyBalanceRequest");
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetAgencyBalanceRequest.xml";
                            doc.Save(filePath);
                        }
                        catch
                        {
                            // Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetAgencyBalance Request. Error" + ex.ToString(), "");
                        }

                        string response = GetResponse(requestData.ToString(), getAgencyBalanceUrl);

                        try
                        {

                            JsonSerializerSettings settings = new JsonSerializerSettings();
                            settings.ObjectCreationHandling = ObjectCreationHandling.Auto;
                            settings.StringEscapeHandling = StringEscapeHandling.Default;
                            settings.Formatting = Newtonsoft.Json.Formatting.Indented;

                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetAgencyBalanceResponseJSON.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic agencyBalResponse = js.Deserialize<dynamic>(response);
                            string rsp = JsonConvert.SerializeObject(agencyBalResponse, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            TextReader reader = new StringReader(response);
                            balance = (AgencyBalance)JsonSerializer.Create(settings).Deserialize(reader, typeof(AgencyBalance));

                            if (balance != null)
                            {
                                Audit.Add(EventType.TBOAir, Severity.Normal, appUserId, "(TBOAir)Balance remaining " + balance.CashBalance, "");
                            }

                            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AgencyBalance));
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetAgencyBalanceResponse.xml";
                            sw = new StreamWriter(filePath);
                            ser.Serialize(sw, balance);
                            sw.Close();
                        }
                        catch
                        {
                            //Audit.Add(EventType.TBOAir, Severity.High, 1, "(TBO Air) Failed to save GetAgencyBalance Response. Error" + ex.ToString(), "");
                        }
                    }

                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Get Agency Balance. Error : " + ex.ToString(), "");
            }

            return balance;
        }

        /// <summary>
        /// Get search results.Merging of two search type results. if the journey type is return.
        /// 1.Normal Return . Getting normal return results and combining GDS to GDS and LCC to LCC
        /// 2.Special Return . Combining results with  same airline.
        /// 3.GDS Special Return. Getting results as a combinations.
        /// For one way journey showing results with display fares.
        /// Type 
        /// 1 - OneWay 2 - Return 3 - Multi Stop 4 - AdvanceSearch 5 - Special Return
        /// 
        /// </summary>
        /// <exception cref="System.NullReferenceException">Throws when any xml nodes from the response missing</exception>
        /// <exception cref="System.IndexOutOfRangeException">Throws when any node count is not as represented in schema</exception>
        /// <param name="request">Search Request object containing the criteria</param>
        /// <returns></returns>
        public SearchResult[] Search(SearchRequest request)
        {
            //Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Search Started" + DateTime.Now, "");
            Authenticate();
            
            if (request.Type == SearchType.Return)
            {
                Dictionary<string, int> readySources = new Dictionary<string, int>();
                Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();
                Request = request;
                

                //listOfThreads.Add("TBO1", new WaitCallback(SearchNormalReturn)); // Normal return results Combination Commented for Time being
                if (request.RestrictAirline = true && request.Segments[0].PreferredAirlines.Length > 0) //If preferred airline is mentioned then hit for only gds
                {
                    if (!searchBySegments)
                    {
                        listOfThreads.Add("TBO3", new WaitCallback(SearchSpecialReturnGDS)); // GDS special return 
                        eventFlag = new AutoResetEvent[1];
                        readySources.Add("TBO3", 1200);
                    }
                    else
                    {
                        listOfThreads.Add("TBO1", new WaitCallback(SearchRoutingReturn));
                        listOfThreads.Add("TBO2", new WaitCallback(SearchRoutingReturn));

                        SearchRequest onwardRequest = request.Copy();
                        onwardRequest.Type = SearchType.OneWay;
                        onwardRequest.Segments = new FlightSegment[1];
                        onwardRequest.Segments[0] = request.Segments[0];

                        SearchRequest returnRequest = request.Copy();
                        returnRequest.Type = SearchType.Return;
                        returnRequest.Segments = new FlightSegment[1];
                        returnRequest.Segments[0] = request.Segments[1];

                        searchRequests.Add(onwardRequest);
                        searchRequests.Add(returnRequest);

                        eventFlag = new AutoResetEvent[2];
                        readySources.Add("TBO1", 1200);
                        readySources.Add("TBO2", 1200);
                    }
                }
                else// No preferred airline mentioned.
                {
                    if (!searchBySegments)
                    {
                        listOfThreads.Add("TBO3", new WaitCallback(SearchSpecialReturnGDS));
                        listOfThreads.Add("TBO2", new WaitCallback(SearchSpecialReturnLCC)); // Special return with LCC. Here combining only same airline

                        eventFlag = new AutoResetEvent[2];
                        readySources.Add("TBO2", 1200);
                        readySources.Add("TBO3", 1200);
                    }
                    else
                    {
                        listOfThreads.Add("TBO1", new WaitCallback(SearchRoutingReturn));
                        listOfThreads.Add("TBO2", new WaitCallback(SearchRoutingReturn));

                        SearchRequest onwardRequest = request.Copy();
                        onwardRequest.Type = SearchType.OneWay;
                        onwardRequest.Segments = new FlightSegment[1];
                        onwardRequest.Segments[0] = request.Segments[0];

                        SearchRequest returnRequest = request.Copy();
                        returnRequest.Type = SearchType.Return;
                        returnRequest.Segments = new FlightSegment[1];
                        returnRequest.Segments[0] = request.Segments[1];

                        searchRequests.Add(onwardRequest);
                        searchRequests.Add(returnRequest);

                        eventFlag = new AutoResetEvent[2];
                        readySources.Add("TBO1", 1200);
                        readySources.Add("TBO2", 1200);
                    }
                }              
               

                int j = 0;
                //Start each fare search within a Thread which will automatically terminate after the results are received.
                foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                {
                    if (readySources.ContainsKey(deThread.Key))
                    {
                        ThreadPool.QueueUserWorkItem(deThread.Value, j);
                        eventFlag[j] = new AutoResetEvent(false);
                        j++;
                    }
                }

                if (j != 0)
                {
                    if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 2, 0), true))
                    {
                        //TODO: audit which thread is timed out                
                    }
                }
            }
            else
            {
                List<SearchResult> results = new List<SearchResult>();
                try
                {
                    string tokenId = string.Empty;
                    if (ar != null)
                    {
                        if (string.IsNullOrEmpty(ar.Error.ErrorMessage) )
                        {
                            if (!string.IsNullOrEmpty(ar.TokenId))
                                tokenId = ar.TokenId;
                            else
                            {
                                Authenticate();
                                tokenId = ar.TokenId;
                            }
                        }
                        else
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir. Reason: " + ar.Error.ErrorMessage, "");
                            throw new Exception(ar.Error.ErrorMessage);
                        }

                        if (tokenId.Length > 0)
                        {
                            //AgencyBalance balance = GetAgencyBalance(ar);

                            #region JSON Search Request
                            int type = 2;//Return
                            switch (request.Type)
                            {
                                case SearchType.MultiWay:
                                    type = 3;
                                    break;
                                case SearchType.OneWay:
                                    type = 1;
                                    break;
                                case SearchType.Return:
                                    type = 2;
                                    break;
                            }

                            TBOSearchRequest searchRequest = new TBOSearchRequest();
                            searchRequest.EndUserIp = endUserIp;
                            searchRequest.TokenId = ar.TokenId;
                            searchRequest.AdultCount = request.AdultCount;
                            searchRequest.ChildCount = request.ChildCount;
                            searchRequest.InfantCount = request.InfantCount;
                            switch (request.MaxStops)
                            {
                                case "-1":
                                    searchRequest.DirectFlight = false;
                                    searchRequest.OneStopFlight = false;
                                    break;
                                case "0":
                                    searchRequest.DirectFlight = true;
                                    searchRequest.OneStopFlight = false;
                                    break;
                                case "1":
                                    searchRequest.DirectFlight = false;
                                    searchRequest.OneStopFlight = true;
                                    break;
                            }
                            searchRequest.JourneyType = type;

                            searchRequest.PreferredAirlines = //!string.IsNullOrEmpty(PrivateFareCodes) ? PrivateFareCodes.Split(',') :
                                (request.Segments[0].PreferredAirlines.Length > 0 ? request.Segments[0].PreferredAirlines : null);

                            List<Segment> Segments = new List<Segment>();
                            foreach (FlightSegment segment in request.Segments)
                            {
                                if (segment.Origin != null)
                                {
                                    Segment Seg = new Segment();
                                    Seg.Origin = segment.Origin;
                                    Seg.Destination = segment.Destination;
                                    Seg.FlightCabinClass = segment.flightCabinClass;
                                    Seg.PreferredDepartureTime = segment.PreferredDepartureTime.ToString("yyyy-MM-ddT00:00:00");//Pass default time 
                                    Seg.PreferredArrivalTime = segment.PreferredArrivalTime.ToString("yyyy-MM-ddT00:00:00");//Pass default time 
                                    Segments.Add(Seg);
                                }
                            }
                            searchRequest.Segments = Segments.ToArray();
                            searchRequest.Sources = ((searchRequest.PreferredAirlines != null) && searchRequest.PreferredAirlines.Length > 0 ? new string[] { "GDS" } : null);


                            //StringBuilder requestData = new StringBuilder();
                            //requestData.AppendLine("{");
                            //requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                            //requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                            //requestData.AppendLine("\"AdultCount\": \"" + request.AdultCount + "\",");
                            //requestData.AppendLine("\"ChildCount\": \"" + request.ChildCount + "\",");
                            //requestData.AppendLine("\"InfantCount\": \"" + request.InfantCount + "\",");
                            //requestData.AppendLine("\"DirectFlight\": \"false\",");
                            //requestData.AppendLine("\"OneStopFlight\": \"false\",");
                            //requestData.AppendLine("\"JourneyType\": \"" + type + "\",");
                            //if (request.Segments[0].PreferredAirlines.Length == 0)
                            //{
                            //    requestData.AppendLine("\"PreferredAirlines\": null,");
                            //}
                            //else
                            //{
                            //    requestData.AppendLine("\"PreferredAirlines\": [");
                            //    //requestData.AppendLine("{");
                            //    string pa = string.Empty;
                            //    foreach (string airline in request.Segments[0].PreferredAirlines)
                            //    {
                            //        if (pa.Length > 0)
                            //        {
                            //            pa += "," + "\"" + airline + "\"";
                            //        }
                            //        else
                            //        {
                            //            pa = "\"" + airline + "\"";
                            //        }
                            //    }
                            //    requestData.AppendLine(pa);
                            //    //requestData.AppendLine("}");
                            //    requestData.AppendLine("],");
                            //}
                            //requestData.AppendLine("\"Segments\": [");

                            //foreach (FlightSegment segment in request.Segments)
                            //{
                            //    if (segment.Origin != null)
                            //    {
                            //        requestData.AppendLine("{");
                            //        requestData.AppendLine("\"Origin\": \"" + segment.Origin + "\",");
                            //        requestData.AppendLine("\"Destination\": \"" + segment.Destination + "\",");
                            //        requestData.AppendLine("\"FlightCabinClass\": \"" + (int)segment.flightCabinClass + "\",");
                            //        requestData.AppendLine("\"PreferredDepartureTime\": \"" + segment.PreferredDepartureTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\",");
                            //        requestData.AppendLine("\"PreferredArrivalTime\": \"" + segment.PreferredArrivalTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\",");
                            //        requestData.AppendLine("}");
                            //    }
                            //}
                            //requestData.AppendLine("],");
                            ////requestData.AppendLine("\"Sources\": [");
                            ////Sources are optional for Normal one-way, so all airlines will be returned
                            ////if we do not specify any sources
                            //if (type < 5)  // Normal one way, International one way ,round trip
                            //{
                            //    //requestData.AppendLine("\"6E\",");
                            //    //requestData.AppendLine("\"G8\",");
                            //    //requestData.AppendLine("\"SG\",");
                            //    //requestData.AppendLine("\"G9\",");
                            //    //requestData.AppendLine("\"GDS\"");
                            //}
                            //else
                            //{
                            //    //requestData.AppendLine("\"SG\",");
                            //    //requestData.AppendLine("\"6E\",");
                            //    //requestData.AppendLine("\"G8\",");
                            //    //requestData.AppendLine("\"GDS\"");

                            //}
                            ////If preferred airline is mentioned and restrict airline is true then hit for only gds
                            //if (request.RestrictAirline = true && request.Segments[0].PreferredAirlines.Length > 0) 
                            //{
                            //    requestData.AppendLine("\"Sources\": [");
                            //    requestData.AppendLine("\"GDS\"");
                            //    requestData.AppendLine("]");
                            //}
                            ////requestData.AppendLine("]");
                            //requestData.AppendLine("}");
                            #endregion

                            string rsp = string.Empty;
                            //Response resp = null;
                            XmlDocument doc = null;
                            try
                            {
                                string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchRequestJSON.json";
                                //JavaScriptSerializer js = new JavaScriptSerializer();
                                //dynamic searchRequest = js.Deserialize<dynamic>(requestData.ToString());
                                rsp = JsonConvert.SerializeObject(searchRequest, Newtonsoft.Json.Formatting.Indented);
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(rsp);
                                sw.Close();

                                doc = JsonConvert.DeserializeXmlNode(rsp, "SearchRequest");
                                filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchRequest.xml";
                                doc.Save(filePath);
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save Search Request. Error" + ex.ToString(), "");
                            }

                            doc = null;

                            string response = GetResponse(searchRequest.ToJSON(), searchUrl);

                            try
                            {
                                string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchResponseJSON.json";
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                dynamic searchResponse = js.Deserialize<dynamic>(response);
                                rsp = JsonConvert.SerializeObject(searchResponse, Newtonsoft.Json.Formatting.Indented);
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(rsp);
                                sw.Close();

                                TextReader reader = new StringReader(response);
                                doc = JsonConvert.DeserializeXmlNode(response);

                                filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchResponse.xml";
                                doc.Save(filePath);
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save Search Response.Error" + ex.ToString(), "");
                            }

                            if (doc != null)
                            {
                                XmlNode Error = doc.SelectSingleNode("Response/Error");
                                string errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                                if (errorMessage.Length <= 0)
                                {
                                    XmlNodeList ResultNodes = doc.SelectNodes("Response/Results/Results");

                                    if ((request.IsDomestic && type == 2))
                                    {
                                        List<XmlNode> outboundResults = new List<XmlNode>();
                                        List<XmlNode> inboundResults = new List<XmlNode>();

                                        foreach (XmlNode result in ResultNodes)
                                        {
                                            if (!ValidatePrefAirline(PrivateFareCodes, request.Segments[0].PreferredAirlines, 
                                                result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Airline/AirlineCode").InnerText))
                                                continue;

                                            if (result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Origin/Airport/AirportCode").InnerText == request.Segments[0].Origin)
                                            {
                                                outboundResults.Add(result);
                                            }
                                            if (request.Segments.Length > 1)
                                            {
                                                if (result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Origin/Airport/AirportCode").InnerText == request.Segments[1].Origin)
                                                {
                                                    inboundResults.Add(result);
                                                }
                                            }
                                        }
                                        if (exchangeRates != null && exchangeRates.Count > 0 && exchangeRates.ContainsKey(outboundResults[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText))
                                        {
                                            rateOfExchange = exchangeRates[outboundResults[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];
                                        }
                                        else
                                        {
                                            rateOfExchange = 1;
                                        }

                                        #region Domestic Results

                                        foreach (XmlNode obResult in outboundResults)
                                        {
                                            foreach (XmlNode ibResult in inboundResults)
                                            {
                                                Airline airlineOut = new Airline();
                                                airlineOut.Load(obResult.SelectSingleNode("ValidatingAirline").InnerText);

                                                Airline airlineIn = new Airline();
                                                airlineIn.Load(ibResult.SelectSingleNode("ValidatingAirline").InnerText);

                                                if (airlineOut.TBOAirAllowed && airlineIn.TBOAirAllowed)
                                                {
                                                    SearchResult sr = new SearchResult();

                                                    sr.Currency = agentBaseCurrency;
                                                    //if (obResult.SelectSingleNode("BaggageAllowance") != null)
                                                    //{
                                                    //    sr.BaggageIncludedInFare = obResult.SelectSingleNode("BaggageAllowance").InnerText;
                                                    //}
                                                    sr.FareType = "OneWay LCC";
                                                    sr.ResultBookingSource = BookingSource.TBOAir;
                                                    //=====================================================Outbound begin===============================================//
                                                    #region Outbound Result
                                                    sr.GUID = obResult.SelectSingleNode("ResultIndex").InnerText;
                                                    sr.JourneySellKey = obResult.SelectSingleNode("Source").InnerText;
                                                    XmlNodeList fareBreakDownNodes = obResult.SelectNodes("FareBreakdown");
                                                    //Read CommissionEarned, PLBEarned & IncentiveEarned and deduct it from Adult fare
                                                    double offeredFareAdjustment = Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                    //Deduct TDS Charged on incentives and discount the remaining amount
                                                    offeredFareAdjustment -= (Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText));
                                                    offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                                    int fareCounter = 0;
                                                    List<Fare> fbdList = new List<Fare>();
                                                    List<Fare> tbofbdList = new List<Fare>();
                                                    foreach (XmlNode fbdNode in fareBreakDownNodes)
                                                    {
                                                        Fare fbd = new Fare();
                                                        fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                        fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                        //if(fareCounter == 0 && !showPubFaresForCorp)
                                                        //{
                                                        //    if(fbd.BaseFare > offeredFareAdjustment)
                                                        //    {
                                                        //        fbd.BaseFare -= offeredFareAdjustment;
                                                        //    }
                                                        //}
                                                        fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                        fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                        fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                        if (fareCounter == 0 && !showPubFaresForCorp)
                                                        {
                                                            if (fbd.TotalFare > offeredFareAdjustment)
                                                            {
                                                                fbd.TotalFare -= offeredFareAdjustment;
                                                            }
                                                        }
                                                        fbd.SellingFare = fbd.TotalFare;
                                                        fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);
                                                        fbdList.Add(fbd);
                                                        fareCounter++;

                                                        Fare tbofbd = new Fare();
                                                        tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                        tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                        tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                        tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                        tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                        tbofbdList.Add(tbofbd);

                                                    }
                                                    sr.FareBreakdown = fbdList.ToArray();
                                                    sr.TBOFareBreakdown = tbofbdList.ToArray();

                                                    XmlNodeList fareRuleList = obResult.SelectNodes("FareRules");
                                                    sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                                    foreach (XmlNode fare in fareRuleList)
                                                    {
                                                        CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                        fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                        fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                        fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                        fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                        fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                        fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                        sr.FareRules.Add(fr);
                                                    }

                                                    if (request.Type == SearchType.Return)
                                                    {
                                                        sr.Flights = new FlightInfo[2][];
                                                    }
                                                    else
                                                    {
                                                        sr.Flights = new FlightInfo[1][];
                                                    }

                                                    XmlNodeList segmentNodes = obResult.SelectNodes("Segments");



                                                    sr.IsLCC = Convert.ToBoolean(obResult.SelectSingleNode("IsLCC").InnerText);
                                                    sr.NonRefundable = (Convert.ToBoolean(obResult.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                                    sr.ResultBookingSource = BookingSource.TBOAir;
                                                    sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                                    sr.ValidatingAirline = obResult.SelectSingleNode("ValidatingAirline").InnerText;

                                                    BindSegments(segmentNodes, ref sr, obResult, null);

                                                    int outbound = 0, inbound = 0;
                                                    for (int i = 0; i < sr.Flights.Length; i++)
                                                    {
                                                        for (int j = 0; j < sr.Flights[i].Length; j++)
                                                        {
                                                            if (sr.Flights[i][j].Group == 0)
                                                            {
                                                                outbound++;
                                                            }
                                                            else
                                                            {
                                                                inbound++;
                                                            }
                                                        }
                                                    }

                                                    for (int i = 0; i < sr.Flights.Length; i++)
                                                    {
                                                        for (int j = 0; j < sr.Flights[i].Length; j++)
                                                        {
                                                            if (sr.Flights[i][j].Group == 0)
                                                            {
                                                                sr.Flights[i][j].Stops = outbound - 1;
                                                            }
                                                            else
                                                            {
                                                                sr.Flights[i][j].Stops = inbound - 1;
                                                            }
                                                        }
                                                    }

                                                    sr.Price = new PriceAccounts();
                                                    sr.Price.AccPriceType = PriceType.PublishedFare;
                                                    sr.Price.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                    sr.Price.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                    sr.Price.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                    sr.Price.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                                    sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                                    XmlNodeList chargeNodes = obResult.SelectNodes("Fare/ChargeBU");
                                                    foreach (XmlNode cb in chargeNodes)
                                                    {
                                                        ChargeBreakUp cbu = new ChargeBreakUp();
                                                        cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                        switch (cb.SelectSingleNode("key").InnerText)
                                                        {
                                                            case "TBOMARKUP":
                                                                cbu.ChargeType = ChargeType.TBOMarkup;
                                                                break;
                                                            case "OTHERCHARGES":
                                                                cbu.ChargeType = ChargeType.OtherCharges;
                                                                break;
                                                            case "CONVENIENCECHARGES":
                                                                cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                                break;
                                                        }
                                                    }
                                                    sr.Price.Currency = agentBaseCurrency;
                                                    sr.Price.CurrencyCode = agentBaseCurrency;
                                                    sr.Price.DecimalPoint = decimalValue;
                                                    sr.Price.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                                    sr.Price.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                                    sr.Price.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                    sr.Price.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                                    sr.Price.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                                    //sr.Price.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                                    //sr.Price.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                                    sr.Price.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                                    sr.Price.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                                    sr.Price.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                    sr.Price.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                    sr.Price.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                    if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                    {
                                                        sr.Price.TransactionFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                    }
                                                    else
                                                    {
                                                        sr.Price.TransactionFee = 0.0M;
                                                    }

                                                    sr.Price.SupplierPrice = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                    sr.Price.SupplierCurrency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                    XmlNodeList taxNodes = obResult.SelectSingleNode("Fare").SelectNodes("TaxBreakup");

                                                    if(taxNodes != null)
                                                    {
                                                        sr.Price.TaxBreakups = new List<TaxBreakup>();
                                                        foreach(XmlNode taxNode in taxNodes)
                                                        {
                                                            if (taxNode.SelectSingleNode("key").InnerText != "OtherTaxes")
                                                            {
                                                                TaxBreakup tax = new TaxBreakup();
                                                                tax.TaxCode = taxNode.SelectSingleNode("key").InnerText;
                                                                tax.TaxValue = Convert.ToDecimal(taxNode.SelectSingleNode("value").InnerText) * rateOfExchange; ;
                                                                if (tax.TaxCode == "K3")
                                                                {
                                                                    sr.Price.K3Tax = tax.TaxValue;
                                                                }
                                                                sr.Price.TaxBreakups.Add(tax);
                                                            }
                                                        }
                                                    }

                                                    sr.TBOPrice = new PriceAccounts();
                                                    sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                                    sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                    sr.TBOPrice.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                    sr.TBOPrice.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                    sr.TBOPrice.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                                    sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                                    XmlNodeList chargeNodes2 = ibResult.SelectNodes("Fare/ChargeBU");
                                                    foreach (XmlNode cb in chargeNodes2)
                                                    {
                                                        ChargeBreakUp cbu = new ChargeBreakUp();
                                                        cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                        switch (cb.SelectSingleNode("key").InnerText)
                                                        {
                                                            case "TBOMARKUP":
                                                                cbu.ChargeType = ChargeType.TBOMarkup;
                                                                break;
                                                            case "OTHERCHARGES":
                                                                cbu.ChargeType = ChargeType.OtherCharges;
                                                                break;
                                                            case "CONVENIENCECHARGES":
                                                                cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                                break;
                                                        }
                                                    }
                                                    sr.TBOPrice.Currency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                    sr.TBOPrice.CurrencyCode = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                    sr.TBOPrice.DecimalPoint = decimalValue;
                                                    sr.TBOPrice.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                                    sr.TBOPrice.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                                    sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                    sr.TBOPrice.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                                    sr.TBOPrice.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                                    sr.TBOPrice.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                    sr.TBOPrice.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                    sr.TBOPrice.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                    sr.TBOPrice.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                    sr.TBOPrice.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                    if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                    {
                                                        sr.TBOPrice.TransactionFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                    }

                                                    sr.BaseFare = (double)sr.Price.BaseFare;
                                                    sr.Tax = (double)(Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                    if (!showPubFaresForCorp)
                                                    {
                                                        if (sr.BaseFare > offeredFareAdjustment)
                                                        {
                                                            sr.BaseFare -= offeredFareAdjustment;
                                                        }
                                                        else if (sr.BaseFare < offeredFareAdjustment && sr.Tax > offeredFareAdjustment)
                                                        {
                                                            sr.Tax -= offeredFareAdjustment;
                                                        }
                                                    }
                                                    sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;
                                                    #endregion
                                                    //======================================================Outbound end================================================//

                                                    //=====================================================Inbound Begin================================================//
                                                    #region Inbound Result

                                                    //if (ibResult.SelectSingleNode("BaggageAllowance") != null)
                                                    //{
                                                    //    sr.BaggageIncludedInFare += "," + ibResult.SelectSingleNode("BaggageAllowance").InnerText;
                                                    //}
                                                    XmlNodeList fareBreakDownNodes20 = ibResult.SelectNodes("FareBreakdown");
                                                    sr.GUID += "," + ibResult.SelectSingleNode("ResultIndex").InnerText;
                                                    sr.JourneySellKey += "|" + ibResult.SelectSingleNode("Source").InnerText;
                                                    //Read CommissionEarned, PLBEarned & IncentiveEarned and deduct it from Adult fare
                                                    offeredFareAdjustment = Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                    //Deduct TDS Charged on incentives and discount the remaining amount
                                                    offeredFareAdjustment -= (Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText));
                                                    offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                                    fareCounter = 0;
                                                    foreach (XmlNode fbdNode in fareBreakDownNodes20)
                                                    {
                                                        foreach (Fare fbd in sr.FareBreakdown)
                                                        {
                                                            if (fbd.PassengerType == (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText))
                                                            {
                                                                fbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                                fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                                //if(fareCounter == 0 && !showPubFaresForCorp)
                                                                //{
                                                                //    if(fbd.BaseFare > offeredFareAdjustment)
                                                                //    {
                                                                //        fbd.BaseFare -= offeredFareAdjustment;
                                                                //    }
                                                                //}
                                                                fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                                fbd.TotalFare += fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                                if (fareCounter == 0 && !showPubFaresForCorp)
                                                                {
                                                                    if (fbd.TotalFare > offeredFareAdjustment)
                                                                    {
                                                                        fbd.TotalFare -= offeredFareAdjustment;
                                                                    }
                                                                }
                                                                fbd.SellingFare += fbd.TotalFare;
                                                                fbd.SupplierFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);
                                                                fareCounter++;
                                                                break;
                                                            }
                                                        }

                                                        foreach (Fare tbofbd in sr.TBOFareBreakdown)
                                                        {
                                                            if (tbofbd.PassengerType == (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText))
                                                            {
                                                                tbofbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                                tbofbd.BaseFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                                tbofbd.PassengerCount += Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                                tbofbd.TotalFare += tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                                break;
                                                            }
                                                        }
                                                    }

                                                    XmlNodeList fareRuleList20 = ibResult.SelectNodes("FareRules");

                                                    foreach (XmlNode fare in fareRuleList20)
                                                    {
                                                        CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                        fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                        fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                        fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                        fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                        fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                        fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                        sr.FareRules.Add(fr);
                                                    }

                                                    XmlNodeList segmentNodes20 = ibResult.SelectNodes("Segments");

                                                    BindSegments(segmentNodes20, ref sr, null, ibResult);

                                                    sr.IsLCC = Convert.ToBoolean(ibResult.SelectSingleNode("IsLCC").InnerText);
                                                    sr.NonRefundable = (Convert.ToBoolean(ibResult.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                                    sr.ResultBookingSource = BookingSource.TBOAir;
                                                    sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                                    sr.ValidatingAirline = ibResult.SelectSingleNode("ValidatingAirline").InnerText;

                                                    sr.Price.AdditionalTxnFee += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                    sr.Price.AgentCommission += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                    sr.Price.AgentPLB += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                    sr.Price.BaseFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;

                                                    XmlNodeList chargeNodes20 = ibResult.SelectNodes("Fare/ChargeBU");
                                                    foreach (XmlNode cb in chargeNodes20)
                                                    {
                                                        foreach (ChargeBreakUp cbu in sr.Price.ChargeBU)
                                                        {

                                                            switch (cb.SelectSingleNode("key").InnerText)
                                                            {
                                                                case "TBOMARKUP":
                                                                    if (cbu.ChargeType == ChargeType.TBOMarkup)
                                                                    {
                                                                        cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                                    }
                                                                    break;
                                                                case "OTHERCHARGES":
                                                                    if (cbu.ChargeType == ChargeType.OtherCharges)
                                                                    {
                                                                        cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                                    }
                                                                    break;
                                                                case "CONVENIENCECHARGES":
                                                                    if (cbu.ChargeType == ChargeType.ConvenienceCharge)
                                                                    {
                                                                        cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                    sr.Price.Currency = agentBaseCurrency;
                                                    sr.Price.CurrencyCode = agentBaseCurrency;
                                                    sr.Price.DecimalPoint = decimalValue;
                                                    sr.Price.Tax += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                                    sr.Price.YQTax += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                                    sr.Price.IncentiveEarned += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                    sr.Price.PublishedFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                                    sr.Price.NetFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                                    //sr.Price.NetFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;                                                
                                                    //sr.Price.PublishedFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                                    sr.Price.AgentCommission += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                    sr.Price.SServiceFee += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                                    sr.Price.OtherCharges += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                                    sr.Price.TdsCommission += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                    sr.Price.TDSPLB += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                    sr.Price.TDSIncentive += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                    if (ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                    {
                                                        sr.Price.TransactionFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                    }
                                                    else
                                                    {
                                                        sr.Price.TransactionFee = 0.0M;
                                                    }
                                                    sr.Price.SupplierPrice += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                    sr.Price.SupplierCurrency = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;

                                                    taxNodes = ibResult.SelectSingleNode("Fare").SelectNodes("TaxBreakup");

                                                    if (taxNodes != null)
                                                    {                                                        
                                                        foreach (XmlNode taxNode in taxNodes)
                                                        {
                                                            if (taxNode.SelectSingleNode("key").InnerText != "OtherTaxes")
                                                            {
                                                                TaxBreakup tax = new TaxBreakup();
                                                                tax.TaxCode = taxNode.SelectSingleNode("key").InnerText;
                                                                tax.TaxValue = Convert.ToDecimal(taxNode.SelectSingleNode("value").InnerText) * rateOfExchange; ;
                                                                if (tax.TaxCode == "K3")
                                                                {
                                                                    sr.Price.K3Tax += tax.TaxValue;
                                                                }
                                                                sr.Price.TaxBreakups.Add(tax);
                                                            }
                                                        }
                                                    }

                                                    sr.TBOPrice = new PriceAccounts();
                                                    sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                                    sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                    sr.TBOPrice.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                    sr.TBOPrice.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                    sr.TBOPrice.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                                    sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                                    XmlNodeList chargeNodes3 = ibResult.SelectNodes("Fare/ChargeBU");
                                                    foreach (XmlNode cb in chargeNodes3)
                                                    {
                                                        ChargeBreakUp cbu = new ChargeBreakUp();
                                                        cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                        switch (cb.SelectSingleNode("key").InnerText)
                                                        {
                                                            case "TBOMARKUP":
                                                                cbu.ChargeType = ChargeType.TBOMarkup;
                                                                break;
                                                            case "OTHERCHARGES":
                                                                cbu.ChargeType = ChargeType.OtherCharges;
                                                                break;
                                                            case "CONVENIENCECHARGES":
                                                                cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                                break;
                                                        }
                                                    }
                                                    sr.TBOPrice.Currency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                    sr.TBOPrice.CurrencyCode = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                    sr.TBOPrice.DecimalPoint = decimalValue;
                                                    sr.TBOPrice.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                                    sr.TBOPrice.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                                    sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                    sr.TBOPrice.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                                    sr.TBOPrice.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                                    sr.TBOPrice.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                    sr.TBOPrice.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                    sr.TBOPrice.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                    sr.TBOPrice.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                    sr.TBOPrice.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                    if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                    {
                                                        sr.TBOPrice.TransactionFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                    }
                                                    sr.BaseFare += (double)sr.Price.BaseFare;
                                                    sr.Tax += (double)(Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                    if (!showPubFaresForCorp)
                                                    {
                                                        if (sr.BaseFare > offeredFareAdjustment)
                                                        {
                                                            sr.BaseFare -= offeredFareAdjustment;
                                                        }
                                                        else if (sr.BaseFare < offeredFareAdjustment && sr.Tax > offeredFareAdjustment)
                                                        {
                                                            sr.Tax -= offeredFareAdjustment;
                                                        }
                                                    }
                                                    sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;

                                                    #endregion
                                                    //======================================================Inbound end=================================================//
                                                    //Adding logic not to bind LCC to GDS
                                                    Airline oAirline = new Airline();
                                                    Airline iAirline = new Airline();
                                                    if (sr.Flights.Length == 2)
                                                    {

                                                        oAirline.Load(sr.Flights[0][0].Airline);

                                                        iAirline.Load(sr.Flights[1][0].Airline);

                                                    }

                                                    if (oAirline.IsLCC == iAirline.IsLCC) // Normal retrun domestic. Should only combine GDS-GDS and LCC-LCC                                        
                                                    {
                                                        results.Add(sr);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        bool isAllowed = true;
                                        #region GDS and SpecialReturn Results

                                        if (exchangeRates.ContainsKey(ResultNodes[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText))
                                            rateOfExchange = exchangeRates[ResultNodes[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];

                                        foreach (XmlNode result in ResultNodes)
                                        {
                                            if (!ValidatePrefAirline(PrivateFareCodes, request.Segments[0].PreferredAirlines,
                                                result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Airline/AirlineCode").InnerText))
                                                continue;

                                            isAllowed = true;
                                            XmlNodeList segmentNodes = result.SelectNodes("Segments");

                                            foreach (XmlNode segment in segmentNodes)
                                            {
                                                XmlNodeList segments = segment.SelectNodes("Segments");

                                                foreach (XmlNode seg in segments)
                                                {
                                                    Airline segAirline = new Airline();
                                                    segAirline.Load(seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText);

                                                    if (!segAirline.TBOAirAllowed)
                                                    {
                                                        isAllowed = false;
                                                        break;
                                                    }
                                                }
                                                if (!isAllowed)
                                                    break;
                                            }

                                            Airline airline = new Airline();
                                            airline.Load(result.SelectSingleNode("ValidatingAirline").InnerText);

                                            if (airline.TBOAirAllowed && isAllowed)
                                            {
                                                SearchResult sr = new SearchResult();
                                                if (type == 5)
                                                {
                                                    sr.IsLCC = true;
                                                    sr.FareType = "LCCSpecialReturn";
                                                }
                                                else
                                                {
                                                    sr.FareType = "Normal (GDS)";
                                                }
                                                //if (result.SelectSingleNode("BaggageAllowance") != null)
                                                //{
                                                //    sr.BaggageIncludedInFare = result.SelectSingleNode("BaggageAllowance").InnerText;
                                                //}
                                                sr.Currency = agentBaseCurrency;
                                                

                                                sr.GUID = result.SelectSingleNode("ResultIndex").InnerText;
                                                sr.JourneySellKey = result.SelectSingleNode("Source").InnerText;
                                                XmlNodeList fareBreakDownNodes = result.SelectNodes("FareBreakdown");
                                                double TransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * (double)rateOfExchange : 0;
                                                double TBOTransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) : 0;
                                                double TBOotherCharges = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);

                                                double OtherCharges = (request.IsDomestic) ? 0 : Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * (double)rateOfExchange;

                                                //Read CommissionEarned, PLBEarned & IncentiveEarned and deduct it from Adult fare
                                                double offeredFareAdjustment = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                //Deduct TDS Charged on incentives and discount the remaining amount
                                                offeredFareAdjustment -= (Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText));
                                                offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                                int fareCounter = 0;

                                                List<Fare> fbdList = new List<Fare>();
                                                List<Fare> tbofbdList = new List<Fare>();
                                                foreach (XmlNode fbdNode in fareBreakDownNodes)
                                                {
                                                    Fare fbd = new Fare();
                                                    fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                    fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                    //if(fareCounter == 0 && !showPubFaresForCorp)
                                                    //{
                                                    //    if(fbd.BaseFare > offeredFareAdjustment)
                                                    //    {
                                                    //        fbd.BaseFare -= offeredFareAdjustment;
                                                    //    }
                                                    //}
                                                    fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                    fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                    fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                    if (fareCounter == 0 && !showPubFaresForCorp)
                                                    {
                                                        if (fbd.TotalFare > offeredFareAdjustment)
                                                        {
                                                            fbd.TotalFare -= offeredFareAdjustment;
                                                        }
                                                    }
                                                    fbd.SellingFare = fbd.TotalFare;
                                                    fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);

                                                    fbdList.Add(fbd);
                                                    fareCounter++;

                                                    Fare tbofbd = new Fare();
                                                    tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                    tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                    tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                    tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                    tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                    tbofbdList.Add(tbofbd);

                                                }
                                                sr.FareBreakdown = fbdList.ToArray();
                                                sr.TBOFareBreakdown = tbofbdList.ToArray();

                                                XmlNodeList fareRuleList = result.SelectNodes("FareRules");
                                                sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                                foreach (XmlNode fare in fareRuleList)
                                                {
                                                    CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                    fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                    fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                    fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                    fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                    fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                    fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                    sr.FareRules.Add(fr);
                                                }

                                                if (request.Type == SearchType.Return)
                                                {
                                                    sr.Flights = new FlightInfo[2][];
                                                }
                                                else if (request.Type == SearchType.MultiWay)
                                                {
                                                    sr.Flights = new FlightInfo[segmentNodes.Count][];
                                                }
                                                else
                                                {
                                                    sr.Flights = new FlightInfo[1][];
                                                }

                                                if (request.Type != SearchType.MultiWay)
                                                {
                                                    foreach (XmlNode segment in segmentNodes)
                                                    {
                                                        XmlNodeList segments = segment.SelectNodes("Segments");
                                                        int tripIndicator = 0, segmentIndicator = 0;
                                                        tripIndicator = Convert.ToInt32(segments[0].SelectSingleNode("TripIndicator").InnerText);
                                                        sr.Flights[tripIndicator - 1] = new FlightInfo[segments.Count];
                                                        foreach (XmlNode seg in segments)
                                                        {
                                                            //Read default baggage from search
                                                            if (seg.SelectSingleNode("Baggage") != null)
                                                            {
                                                                if (string.IsNullOrEmpty(sr.BaggageIncludedInFare))
                                                                {
                                                                    sr.BaggageIncludedInFare = seg.SelectSingleNode("Baggage").InnerText;
                                                                }
                                                                else
                                                                {
                                                                    sr.BaggageIncludedInFare += "," + seg.SelectSingleNode("Baggage").InnerText;
                                                                }
                                                            }
                                                            segmentIndicator = Convert.ToInt32(seg.SelectSingleNode("SegmentIndicator").InnerText);
                                                            tripIndicator = Convert.ToInt32(seg.SelectSingleNode("TripIndicator").InnerText);

                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1] = new FlightInfo();
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Airline = seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrivalTime = Convert.ToDateTime(seg.SelectSingleNode("Destination").SelectSingleNode("ArrTime").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrTerminal = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].BookingClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                            //if (request.Segments[0].flightCabinClass == CabinClass.Business)
                                                            //{
                                                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Business.ToString();
                                                            //}
                                                            //else if (request.Segments[0].flightCabinClass == CabinClass.First)
                                                            //{
                                                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.First.ToString();
                                                            //}
                                                            //else if (request.Segments[0].flightCabinClass == CabinClass.Economy)
                                                            //{
                                                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Economy.ToString();
                                                            //}
                                                            //else
                                                            //{
                                                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                            //}
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = string.Empty;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Craft = seg.SelectSingleNode("Craft").InnerText;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepartureTime = Convert.ToDateTime(seg.SelectSingleNode("Origin").SelectSingleNode("DepTime").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepTerminal = seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                                            //Reading Airport code directly from DB
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination = new CT.BookingEngine.Airport(seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = new TimeSpan(0, Convert.ToInt32(seg.SelectSingleNode("Duration").InnerText), 0);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ETicketEligible = Convert.ToBoolean(seg.SelectSingleNode("IsETicketEligible").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightNumber = seg.SelectSingleNode("Airline").SelectSingleNode("FlightNumber").InnerText;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightStatus = FlightStatus.Confirmed;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].GroundTime = new TimeSpan(Convert.ToInt32(seg.SelectSingleNode("GroundTime").InnerText));
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Group = tripIndicator - 1;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Mile = Convert.ToInt32(seg.SelectSingleNode("Mile").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].OperatingCarrier = seg.SelectSingleNode("Airline").SelectSingleNode("OperatingCarrier").InnerText;
                                                            //Reading Airport code directly from DB
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin = new CT.BookingEngine.Airport(seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Status = seg.SelectSingleNode("Status").InnerText;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].StopOver = Convert.ToBoolean(seg.SelectSingleNode("StopOver").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentFareType = result.SelectSingleNode("AirlineRemark").InnerText;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice = new PriceAccounts();
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.Currency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.CurrencyCode = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.DecimalPoint = decimalValue;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                        }
                                                    }

                                                    int outbound = 0, inbound = 0;
                                                    for (int i = 0; i < sr.Flights.Length; i++)
                                                    {
                                                        for (int j = 0; j < sr.Flights[i].Length; j++)
                                                        {
                                                            if (sr.Flights[i][j].Group == 0)
                                                            {
                                                                outbound++;
                                                            }
                                                            else
                                                            {
                                                                inbound++;
                                                            }
                                                        }
                                                    }

                                                    for (int i = 0; i < sr.Flights.Length; i++)
                                                    {
                                                        for (int j = 0; j < sr.Flights[i].Length; j++)
                                                        {
                                                            if (sr.Flights[i][j].Group == 0)
                                                            {
                                                                sr.Flights[i][j].Stops = outbound - 1;
                                                            }
                                                            else
                                                            {
                                                                sr.Flights[i][j].Stops = inbound - 1;
                                                            }
                                                        }
                                                    }
                                                }
                                                else //MultiCity 
                                                {

                                                    int tripIndicator = 1;
                                                    foreach (XmlNode segment in segmentNodes)
                                                    {
                                                        XmlNodeList segments = segment.SelectNodes("Segments");
                                                        int segmentIndicator = 0;
                                                        //tripIndicator = Convert.ToInt32(segments[0].SelectSingleNode("TripIndicator").InnerText);
                                                        sr.Flights[tripIndicator - 1] = new FlightInfo[segments.Count];
                                                        foreach (XmlNode seg in segments)
                                                        {
                                                            //Read default baggage from search
                                                            if (seg.SelectSingleNode("Baggage") != null)
                                                            {
                                                                if (string.IsNullOrEmpty(sr.BaggageIncludedInFare))
                                                                {
                                                                    sr.BaggageIncludedInFare = seg.SelectSingleNode("Baggage").InnerText;
                                                                }
                                                                else
                                                                {
                                                                    sr.BaggageIncludedInFare += "," + seg.SelectSingleNode("Baggage").InnerText;
                                                                }
                                                            }
                                                            segmentIndicator = Convert.ToInt32(seg.SelectSingleNode("SegmentIndicator").InnerText);
                                                            //tripIndicator = Convert.ToInt32(seg.SelectSingleNode("TripIndicator").InnerText);

                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1] = new FlightInfo();
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Airline = seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrivalTime = Convert.ToDateTime(seg.SelectSingleNode("Destination").SelectSingleNode("ArrTime").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrTerminal = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].BookingClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                            //if (request.Segments[0].flightCabinClass == CabinClass.Business)
                                                            //{
                                                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Business.ToString();
                                                            //}
                                                            //else if (request.Segments[0].flightCabinClass == CabinClass.First)
                                                            //{
                                                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.First.ToString();
                                                            //}
                                                            //else if (request.Segments[0].flightCabinClass == CabinClass.Economy)
                                                            //{
                                                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Economy.ToString();
                                                            //}
                                                            //else
                                                            //{
                                                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                            //}
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = string.Empty;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Craft = seg.SelectSingleNode("Craft").InnerText;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepartureTime = Convert.ToDateTime(seg.SelectSingleNode("Origin").SelectSingleNode("DepTime").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepTerminal = seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                                            //Reading Airport code directly from DB
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination = new CT.BookingEngine.Airport(seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = new TimeSpan(0, Convert.ToInt32(seg.SelectSingleNode("Duration").InnerText), 0);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ETicketEligible = Convert.ToBoolean(seg.SelectSingleNode("IsETicketEligible").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightNumber = seg.SelectSingleNode("Airline").SelectSingleNode("FlightNumber").InnerText;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightStatus = FlightStatus.Confirmed;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].GroundTime = new TimeSpan(Convert.ToInt32(seg.SelectSingleNode("GroundTime").InnerText));
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Group = tripIndicator - 1;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Mile = Convert.ToInt32(seg.SelectSingleNode("Mile").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].OperatingCarrier = seg.SelectSingleNode("Airline").SelectSingleNode("OperatingCarrier").InnerText;
                                                            //Reading Airport code directly from DB
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin = new CT.BookingEngine.Airport(seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Status = seg.SelectSingleNode("Status").InnerText;
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].StopOver = Convert.ToBoolean(seg.SelectSingleNode("StopOver").InnerText);
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops = 1;//Added by Shiva
                                                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentFareType = result.SelectSingleNode("AirlineRemark").InnerText;

                                                        }
                                                        tripIndicator++;
                                                    }
                                                }

                                                sr.IsLCC = Convert.ToBoolean(result.SelectSingleNode("IsLCC").InnerText);
                                                sr.NonRefundable = (Convert.ToBoolean(result.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                                sr.ResultBookingSource = BookingSource.TBOAir;
                                                sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                                sr.ValidatingAirline = result.SelectSingleNode("ValidatingAirline").InnerText;

                                                sr.Price = new PriceAccounts();
                                                sr.Price.AccPriceType = PriceType.PublishedFare;
                                                sr.Price.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                sr.Price.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                sr.Price.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                sr.Price.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                                sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                                XmlNodeList chargeNodes = result.SelectNodes("Fare/ChargeBU");
                                                foreach (XmlNode cb in chargeNodes)
                                                {
                                                    ChargeBreakUp cbu = new ChargeBreakUp();
                                                    cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                    switch (cb.SelectSingleNode("key").InnerText)
                                                    {
                                                        case "TBOMARKUP":
                                                            cbu.ChargeType = ChargeType.TBOMarkup;
                                                            break;
                                                        case "OTHERCHARGES":
                                                            cbu.ChargeType = ChargeType.OtherCharges;
                                                            break;
                                                        case "CONVENIENCECHARGES":
                                                            cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                            break;
                                                    }
                                                }
                                                sr.Price.Currency = agentBaseCurrency;
                                                sr.Price.CurrencyCode = agentBaseCurrency;
                                                sr.Price.DecimalPoint = decimalValue;
                                                sr.Price.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                                sr.Price.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                sr.Price.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                                sr.Price.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                sr.Price.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                                sr.Price.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                                //sr.Price.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                                //sr.Price.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                                sr.Price.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                                sr.Price.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                                sr.Price.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                sr.Price.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                sr.Price.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                {
                                                    sr.Price.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                                }
                                                sr.Price.SupplierPrice = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                sr.Price.SupplierCurrency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;

                                                XmlNodeList taxNodes = result.SelectSingleNode("Fare").SelectNodes("TaxBreakup");

                                                if (taxNodes != null)
                                                {
                                                    sr.Price.TaxBreakups = new List<TaxBreakup>();
                                                    foreach (XmlNode taxNode in taxNodes)
                                                    {
                                                        if (taxNode.SelectSingleNode("key").InnerText != "OtherTaxes")
                                                        {
                                                            TaxBreakup tax = new TaxBreakup();
                                                            tax.TaxCode = taxNode.SelectSingleNode("key").InnerText;
                                                            tax.TaxValue = Convert.ToDecimal(taxNode.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                            if (tax.TaxCode == "K3")
                                                            {
                                                                sr.Price.K3Tax = tax.TaxValue;
                                                            }
                                                            sr.Price.TaxBreakups.Add(tax);
                                                        }
                                                    }
                                                }

                                                sr.TBOPrice = new PriceAccounts();
                                                sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                                sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                sr.TBOPrice.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                sr.TBOPrice.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                sr.TBOPrice.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                                sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                                XmlNodeList chargeNodes2 = result.SelectNodes("Fare/ChargeBU");
                                                foreach (XmlNode cb in chargeNodes2)
                                                {
                                                    ChargeBreakUp cbu = new ChargeBreakUp();
                                                    cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                    switch (cb.SelectSingleNode("key").InnerText)
                                                    {
                                                        case "TBOMARKUP":
                                                            cbu.ChargeType = ChargeType.TBOMarkup;
                                                            break;
                                                        case "OTHERCHARGES":
                                                            cbu.ChargeType = ChargeType.OtherCharges;
                                                            break;
                                                        case "CONVENIENCECHARGES":
                                                            cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                            break;
                                                    }
                                                }
                                                sr.TBOPrice.Currency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                sr.TBOPrice.CurrencyCode = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                sr.TBOPrice.DecimalPoint = decimalValue;
                                                sr.TBOPrice.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                                sr.TBOPrice.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                                sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                sr.TBOPrice.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                                sr.TBOPrice.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                                sr.TBOPrice.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                sr.TBOPrice.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                sr.TBOPrice.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                sr.TBOPrice.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                sr.TBOPrice.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                {
                                                    sr.TBOPrice.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                }

                                                sr.BaseFare = (double)sr.Price.BaseFare;
                                                sr.Tax = (double)(Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                if (!showPubFaresForCorp)
                                                {
                                                    if (sr.BaseFare > offeredFareAdjustment)
                                                    {
                                                        sr.BaseFare -= offeredFareAdjustment;
                                                    }
                                                    else if (sr.BaseFare < offeredFareAdjustment && sr.Tax > offeredFareAdjustment)
                                                    {
                                                        sr.Tax -= offeredFareAdjustment;
                                                    }
                                                }
                                                sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;

                                                if (isAllowed)
                                                {
                                                    results.Add(sr);
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Search. Error : " + ex.ToString(), "");
                }
                CombineResults.AddRange(results);
            }
            //Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Search Ended" + DateTime.Now, "");
            if (searchBySegments)
            {
                if (CombineResults.FindAll(x => x.Flights[0].Any(f => f.Group == 0)).Count == 0 || (request.Type == SearchType.Return && CombineResults.FindAll(x => x.Flights[0].Any(f => f.Group == 1)).Count == 0))                
                {
                    CombineResults.Clear();
                }
            }
            return CombineResults.ToArray();
            
        }

        /// <summary>
        /// Binds GDS and Multi-way segments. Optimized for readability and Performance by Shiva
        /// </summary>
        /// <param name="segmentNodes"></param>
        /// <param name="sr"></param>
        /// <param name="result"></param>
        private bool BindSegments(XmlNodeList segmentNodes, ref SearchResult sr, XmlNode result)
        {
            bool isAirlineAllowed = true;
            try
            {
                int tripIndicator = 0, segmentIndicator = 0;
                if (Request.Type == SearchType.MultiWay)
                {
                    tripIndicator = 1;
                }

                List<XmlNode> onwardNodeList = new List<XmlNode>();
                List<XmlNode> returnNodeList = new List<XmlNode>();
                foreach (XmlNode segment in segmentNodes)
                {
                    XmlNodeList segments = segment.SelectNodes("Segments");

                    foreach (XmlNode seg in segments)
                    {
                        if (seg.SelectSingleNode("TripIndicator").InnerText == "1")
                        {
                            onwardNodeList.Add(seg);
                        }
                        else if (seg.SelectSingleNode("TripIndicator").InnerText == "2")
                        {
                            returnNodeList.Add(seg);
                        }                        
                    }
                    foreach (XmlNode seg in segments)
                    {
                        Airline airline = new Airline();
                        airline.Load(seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText);

                        if (airline.TBOAirAllowed)
                        {
                            isAirlineAllowed = true;
                        }
                        else
                        {
                            isAirlineAllowed = false;
                            break;
                        }
                    }
                }
                if (isAirlineAllowed)
                {
                    if (Request.Type != SearchType.MultiWay)
                    {
                        tripIndicator = 1;
                    }
                    sr.Flights[tripIndicator - 1] = new FlightInfo[onwardNodeList.Count];
                    foreach (XmlNode seg in onwardNodeList)
                    {
                        
                        int onwardStops = 0, returnStops = 0;
                        
                        
                        //foreach (XmlNode seg in segments)
                        {
                            //Read default baggage from search
                            if (seg.SelectSingleNode("Baggage") != null)
                            {
                                if (string.IsNullOrEmpty(sr.BaggageIncludedInFare))
                                {
                                    sr.BaggageIncludedInFare = seg.SelectSingleNode("Baggage").InnerText;
                                }
                                else
                                {
                                    sr.BaggageIncludedInFare += "," + seg.SelectSingleNode("Baggage").InnerText;
                                }
                            }
                            segmentIndicator = Convert.ToInt32(seg.SelectSingleNode("SegmentIndicator").InnerText);
                            

                            sr.Flights[tripIndicator - 1][segmentIndicator - 1] = new FlightInfo();
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Airline = seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrivalTime = Convert.ToDateTime(seg.SelectSingleNode("Destination").SelectSingleNode("ArrTime").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrTerminal = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].BookingClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                            //if (Request.Segments[0].flightCabinClass == CabinClass.Business)
                            //{
                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Business.ToString();
                            //}
                            //else if (Request.Segments[0].flightCabinClass == CabinClass.First)
                            //{
                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.First.ToString();
                            //}
                            //else if (Request.Segments[0].flightCabinClass == CabinClass.Economy)
                            //{
                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Economy.ToString();
                            //}
                            //else
                            //{
                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                            //}
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = string.Empty;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Craft = seg.SelectSingleNode("Craft").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepartureTime = Convert.ToDateTime(seg.SelectSingleNode("Origin").SelectSingleNode("DepTime").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepTerminal = seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination = new CT.BookingEngine.Airport(seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.AirportCode = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText;
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.AirportName = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportName").InnerText;
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("CityCode").InnerText;
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityName = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("CityName").InnerText;
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CountryCode = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("CountryCode").InnerText;
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CountryName = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("CountryName").InnerText;
                            if (seg.SelectSingleNode("Duration") != null)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = new TimeSpan(0, Convert.ToInt32(seg.SelectSingleNode("Duration").InnerText), 0);
                            }
                            else if (seg.SelectSingleNode("AccumulatedDuration") != null)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = new TimeSpan(0, Convert.ToInt32(seg.SelectSingleNode("AccumulatedDuration").InnerText), 0);
                            }
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ETicketEligible = Convert.ToBoolean(seg.SelectSingleNode("IsETicketEligible").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightNumber = seg.SelectSingleNode("Airline").SelectSingleNode("FlightNumber").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightStatus = FlightStatus.Confirmed;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].GroundTime = new TimeSpan(Convert.ToInt32(seg.SelectSingleNode("GroundTime").InnerText));
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Group = tripIndicator - 1;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Mile = Convert.ToInt32(seg.SelectSingleNode("Mile").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].OperatingCarrier = seg.SelectSingleNode("Airline").SelectSingleNode("OperatingCarrier").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin = new CT.BookingEngine.Airport(seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Status = seg.SelectSingleNode("Status").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].StopOver = Convert.ToBoolean(seg.SelectSingleNode("StopOver").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops = (tripIndicator == 1 ? onwardStops - 1 : returnStops - 1);
                            //if (tripIndicator == 1)
                            //{
                            //    if (sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode == Request.Segments[0].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[0].Destination ||
                            //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[0].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode == Request.Segments[0].Destination ||
                            //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[0].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[0].Destination)
                            //    {
                            //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops++;
                            //    }
                            //}
                            //else
                            //{
                            //    if (sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode == Request.Segments[1].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[1].Destination ||
                            //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[1].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode == Request.Segments[1].Destination ||
                            //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[1].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[1].Destination)
                            //    {
                            //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops++;
                            //    }
                            //}
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentFareType = result.SelectSingleNode("AirlineRemark").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice = new PriceAccounts();
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.Currency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.CurrencyCode = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.DecimalPoint = decimalValue;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);

                        }
                        if (Request.Type == SearchType.MultiWay)
                        {
                            tripIndicator++;
                        }
                    }

                    if (Request.Type == SearchType.Return)
                    {
                        if (Request.Type != SearchType.MultiWay)
                        {
                            tripIndicator = 2;
                        }
                        sr.Flights[tripIndicator - 1] = new FlightInfo[returnNodeList.Count];
                        foreach (XmlNode seg in returnNodeList)
                        {  
                            


                            
                            {
                                //Read default baggage from search
                                if (seg.SelectSingleNode("Baggage") != null)
                                {
                                    if (string.IsNullOrEmpty(sr.BaggageIncludedInFare))
                                    {
                                        sr.BaggageIncludedInFare = seg.SelectSingleNode("Baggage").InnerText;
                                    }
                                    else
                                    {
                                        sr.BaggageIncludedInFare += "," + seg.SelectSingleNode("Baggage").InnerText;
                                    }
                                }
                                segmentIndicator = Convert.ToInt32(seg.SelectSingleNode("SegmentIndicator").InnerText);


                                sr.Flights[tripIndicator - 1][segmentIndicator - 1] = new FlightInfo();
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Airline = seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrivalTime = Convert.ToDateTime(seg.SelectSingleNode("Destination").SelectSingleNode("ArrTime").InnerText);
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrTerminal = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].BookingClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                //if (Request.Segments[0].flightCabinClass == CabinClass.Business)
                                //{
                                //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Business.ToString();
                                //}
                                //else if (Request.Segments[0].flightCabinClass == CabinClass.First)
                                //{
                                //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.First.ToString();
                                //}
                                //else if (Request.Segments[0].flightCabinClass == CabinClass.Economy)
                                //{
                                //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Economy.ToString();
                                //}
                                //else
                                //{
                                //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                //}
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = string.Empty;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Craft = seg.SelectSingleNode("Craft").InnerText;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepartureTime = Convert.ToDateTime(seg.SelectSingleNode("Origin").SelectSingleNode("DepTime").InnerText);
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepTerminal = seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination = new CT.BookingEngine.Airport(seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                                //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.AirportCode = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText;
                                //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.AirportName = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportName").InnerText;
                                //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("CityCode").InnerText;
                                //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityName = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("CityName").InnerText;
                                //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CountryCode = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("CountryCode").InnerText;
                                //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CountryName = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("CountryName").InnerText;
                                if (seg.SelectSingleNode("Duration") != null)
                                {
                                    sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = new TimeSpan(0, Convert.ToInt32(seg.SelectSingleNode("Duration").InnerText), 0);
                                }
                                else if (seg.SelectSingleNode("AccumulatedDuration") != null)
                                {
                                    sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = new TimeSpan(0, Convert.ToInt32(seg.SelectSingleNode("AccumulatedDuration").InnerText), 0);
                                }
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].ETicketEligible = Convert.ToBoolean(seg.SelectSingleNode("IsETicketEligible").InnerText);
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightNumber = seg.SelectSingleNode("Airline").SelectSingleNode("FlightNumber").InnerText;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightStatus = FlightStatus.Confirmed;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].GroundTime = new TimeSpan(Convert.ToInt32(seg.SelectSingleNode("GroundTime").InnerText));
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Group = tripIndicator - 1;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Mile = Convert.ToInt32(seg.SelectSingleNode("Mile").InnerText);
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].OperatingCarrier = seg.SelectSingleNode("Airline").SelectSingleNode("OperatingCarrier").InnerText;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin = new CT.BookingEngine.Airport(seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Status = seg.SelectSingleNode("Status").InnerText;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].StopOver = Convert.ToBoolean(seg.SelectSingleNode("StopOver").InnerText);
                                //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops = (tripIndicator == 1 ? onwardStops - 1 : returnStops - 1);
                                //if (tripIndicator == 1)
                                //{
                                //    if (sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode == Request.Segments[0].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[0].Destination ||
                                //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[0].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode == Request.Segments[0].Destination ||
                                //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[0].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[0].Destination)
                                //    {
                                //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops++;
                                //    }
                                //}
                                //else
                                //{
                                //    if (sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode == Request.Segments[1].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[1].Destination ||
                                //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[1].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode == Request.Segments[1].Destination ||
                                //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[1].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[1].Destination)
                                //    {
                                //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops++;
                                //    }
                                //}
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentFareType = result.SelectSingleNode("AirlineRemark").InnerText;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice = new PriceAccounts();
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.Currency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.CurrencyCode = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.DecimalPoint = decimalValue;
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);

                            }

                        }
                    }

                    SearchResult res = sr;
                    sr.Flights[0].AsQueryable().ToList().ForEach(f => f.Stops = res.Flights[0].Length - 1);
                    if (Request.Type == SearchType.Return)
                        sr.Flights[1].AsQueryable().ToList().ForEach(f => f.Stops = res.Flights[1].Length - 1);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Bind Segments for " + sr.FareType + ". Reason: " + ex.ToString(), "");
            }

            return isAirlineAllowed;
        }

        /// <summary>
        /// Binds domestic LCC or normal segments. Optimized for readability and Performance by Shiva
        /// </summary>
        /// <param name="segmentNodes"></param>
        /// <param name="sr"></param>
        /// <param name="obResult"></param>
        /// <param name="ibResult"></param>
        private void BindSegments(XmlNodeList segmentNodes, ref SearchResult sr, XmlNode obResult, XmlNode ibResult)
        {
            try
            {
                //Bind onward segments
                if (obResult != null && ibResult == null)
                {
                    foreach (XmlNode segment in segmentNodes)
                    {
                        XmlNodeList segments = segment.SelectNodes("Segments");
                        int tripIndicator = 0, segmentIndicator = 0, onwardStops = 0;
                        tripIndicator = 1;//Convert.ToInt32(segments[0].SelectSingleNode("TripIndicator").InnerText);
                        sr.Flights[tripIndicator - 1] = new FlightInfo[segments.Count];
                        
                        foreach (XmlNode seg in segments)
                        {
                            //Read default baggage from search
                            if (seg.SelectSingleNode("Baggage") != null)
                            {
                                if (string.IsNullOrEmpty(sr.BaggageIncludedInFare))
                                {
                                    sr.BaggageIncludedInFare = seg.SelectSingleNode("Baggage").InnerText;
                                }
                                else
                                {
                                    sr.BaggageIncludedInFare += "," + seg.SelectSingleNode("Baggage").InnerText;
                                }
                            }
                            
                            segmentIndicator = Convert.ToInt32(seg.SelectSingleNode("SegmentIndicator").InnerText);
                            tripIndicator = Convert.ToInt32(seg.SelectSingleNode("TripIndicator").InnerText);

                            sr.Flights[tripIndicator - 1][segmentIndicator - 1] = new FlightInfo();
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Airline = seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrivalTime = Convert.ToDateTime(seg.SelectSingleNode("Destination").SelectSingleNode("ArrTime").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrTerminal = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].BookingClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                            //if (Request.Segments[0].flightCabinClass == CabinClass.Business)
                            //{
                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Business.ToString();
                            //}
                            //else if (Request.Segments[0].flightCabinClass == CabinClass.First)
                            //{
                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.First.ToString();
                            //}
                            //else if (Request.Segments[0].flightCabinClass == CabinClass.Economy)
                            //{
                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Economy.ToString();
                            //}
                            //else
                            //{
                            //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                            //}
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = string.Empty;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Craft = seg.SelectSingleNode("Craft").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepartureTime = Convert.ToDateTime(seg.SelectSingleNode("Origin").SelectSingleNode("DepTime").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepTerminal = seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination = new CT.BookingEngine.Airport(seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].CreatedOn = DateTime.Now;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].LastModifiedOn = DateTime.Now;
                            
                            if (seg.SelectSingleNode("Duration") != null)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = TimeSpan.FromMinutes(Convert.ToInt32(seg.SelectSingleNode("Duration").InnerText));
                            }
                            else if (seg.SelectSingleNode("AccumulatedDuration") != null)
                            {
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = TimeSpan.FromMinutes(Convert.ToInt32(seg.SelectSingleNode("AccumulatedDuration").InnerText));
                                
                            }
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].AccumulatedDuration = sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration;                            
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].ETicketEligible = Convert.ToBoolean(seg.SelectSingleNode("IsETicketEligible").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightNumber = seg.SelectSingleNode("Airline").SelectSingleNode("FlightNumber").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightStatus = FlightStatus.Confirmed;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].GroundTime = new TimeSpan(Convert.ToInt32(seg.SelectSingleNode("GroundTime").InnerText));
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Group = tripIndicator - 1;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Mile = Convert.ToInt32(seg.SelectSingleNode("Mile").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].OperatingCarrier = seg.SelectSingleNode("Airline").SelectSingleNode("OperatingCarrier").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin = new CT.BookingEngine.Airport(seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Status = seg.SelectSingleNode("Status").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].StopOver = Convert.ToBoolean(seg.SelectSingleNode("StopOver").InnerText);
                        //    if (sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode == Request.Segments[0].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode != Request.Segments[0].Destination ||
                        //sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin.CityCode != Request.Segments[0].Origin && sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination.CityCode == Request.Segments[0].Destination)
                        //    {
                        //        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops++;
                        //    }
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops = onwardStops-1;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice = new PriceAccounts();
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.AccPriceType = PriceType.PublishedFare;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.ChargeBU = new List<ChargeBreakUp>();
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentFareType = !string.IsNullOrEmpty ( obResult.SelectSingleNode("AirlineRemark").InnerText) ? obResult.SelectSingleNode("AirlineRemark").InnerText : string.Empty;
                            XmlNodeList chargeNodes1 = obResult.SelectNodes("Fare/ChargeBU");
                            foreach (XmlNode cb in chargeNodes1)
                            {
                                ChargeBreakUp cbu = new ChargeBreakUp();
                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                switch (cb.SelectSingleNode("key").InnerText)
                                {
                                    case "TBOMARKUP":
                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                        break;
                                    case "OTHERCHARGES":
                                        cbu.ChargeType = ChargeType.OtherCharges;
                                        break;
                                    case "CONVENIENCECHARGES":
                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                        break;
                                }
                                sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.ChargeBU.Add(cbu);
                            }
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.Currency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.CurrencyCode = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.DecimalPoint = decimalValue;
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                            sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Bind Onward Segments for " + sr.FareType + ". Reason: " + ex.ToString(), "");
            }

            try{
            //Bind return segments
                if (ibResult != null && obResult == null)
                {
                    foreach (XmlNode segment in segmentNodes)
                    {
                            
                        XmlNodeList segments = segment.SelectNodes("Segments");
                        int tripIndicator = 1, segmentIndicator = 0, returnStops = 0;
                        //tripIndicator = Convert.ToInt32(segments[0].SelectSingleNode("TripIndicator").InnerText);
                        sr.Flights[tripIndicator] = new FlightInfo[segments.Count];
                        
                        foreach (XmlNode seg in segments)
                        {
                            //Read default baggage from search
                            if (seg.SelectSingleNode("Baggage") != null)
                            {
                                if (string.IsNullOrEmpty(sr.BaggageIncludedInFare))
                                {
                                    sr.BaggageIncludedInFare = seg.SelectSingleNode("Baggage").InnerText;
                                }
                                else
                                {
                                    sr.BaggageIncludedInFare += "," + seg.SelectSingleNode("Baggage").InnerText;
                                }
                            }
                            segmentIndicator = Convert.ToInt32(seg.SelectSingleNode("SegmentIndicator").InnerText);
                            //tripIndicator = Convert.ToInt32(seg.SelectSingleNode("TripIndicator").InnerText);

                            sr.Flights[tripIndicator][segmentIndicator - 1] = new FlightInfo();
                            sr.Flights[tripIndicator][segmentIndicator - 1].Airline = seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].ArrivalTime = Convert.ToDateTime(seg.SelectSingleNode("Destination").SelectSingleNode("ArrTime").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].ArrTerminal = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].BookingClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                            //if (Request.Segments[0].flightCabinClass == CabinClass.Business)
                            //{
                            //    sr.Flights[tripIndicator][segmentIndicator - 1].CabinClass = CabinClass.Business.ToString();
                            //}
                            //else if (Request.Segments[0].flightCabinClass == CabinClass.First)
                            //{
                            //    sr.Flights[tripIndicator][segmentIndicator - 1].CabinClass = CabinClass.First.ToString();
                            //}
                            //else if (Request.Segments[0].flightCabinClass == CabinClass.Economy)
                            //{
                            //    sr.Flights[tripIndicator][segmentIndicator - 1].CabinClass = CabinClass.Economy.ToString();
                            //}
                            //else
                            //{
                            //    sr.Flights[tripIndicator][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                            //}
                            sr.Flights[tripIndicator][segmentIndicator - 1].CabinClass = string.Empty;
                            sr.Flights[tripIndicator][segmentIndicator - 1].Craft = seg.SelectSingleNode("Craft").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].DepartureTime = Convert.ToDateTime(seg.SelectSingleNode("Origin").SelectSingleNode("DepTime").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].DepTerminal = seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].Destination = new CT.BookingEngine.Airport(seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                            
                            if (seg.SelectSingleNode("Duration") != null)
                            {
                                sr.Flights[tripIndicator][segmentIndicator - 1].Duration = TimeSpan.FromMinutes(Convert.ToInt32(seg.SelectSingleNode("Duration").InnerText));
                            }
                            else if (seg.SelectSingleNode("AccumulatedDuration") != null)
                            {
                                sr.Flights[tripIndicator][segmentIndicator - 1].Duration = TimeSpan.FromMinutes(Convert.ToInt32(seg.SelectSingleNode("AccumulatedDuration").InnerText));
                            }
                            sr.Flights[tripIndicator][segmentIndicator - 1].ETicketEligible = Convert.ToBoolean(seg.SelectSingleNode("IsETicketEligible").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].FlightNumber = seg.SelectSingleNode("Airline").SelectSingleNode("FlightNumber").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].FlightStatus = FlightStatus.Confirmed;
                            sr.Flights[tripIndicator][segmentIndicator - 1].GroundTime = TimeSpan.FromMinutes(Convert.ToInt32(seg.SelectSingleNode("GroundTime").InnerText));
                            sr.Flights[tripIndicator][segmentIndicator - 1].Group = tripIndicator;
                            sr.Flights[tripIndicator][segmentIndicator - 1].Mile = Convert.ToInt32(seg.SelectSingleNode("Mile").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].OperatingCarrier = seg.SelectSingleNode("Airline").SelectSingleNode("OperatingCarrier").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].Origin = new CT.BookingEngine.Airport(seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].Status = seg.SelectSingleNode("Status").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].StopOver = Convert.ToBoolean(seg.SelectSingleNode("StopOver").InnerText);
                        //    if (sr.Flights[tripIndicator][segmentIndicator - 1].Origin.CityCode == Request.Segments[1].Origin && sr.Flights[tripIndicator][segmentIndicator - 1].Destination.CityCode != Request.Segments[1].Destination ||
                        //sr.Flights[tripIndicator][segmentIndicator - 1].Origin.CityCode != Request.Segments[1].Origin && sr.Flights[tripIndicator][segmentIndicator - 1].Destination.CityCode == Request.Segments[1].Destination)
                        //    {
                        //        sr.Flights[tripIndicator][segmentIndicator - 1].Stops++;
                        //    }
                            sr.Flights[tripIndicator][segmentIndicator - 1].Stops = returnStops - 1;
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice = new PriceAccounts();
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.AccPriceType = PriceType.PublishedFare;
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.AdditionalTxnFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.AgentCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.AgentPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.BaseFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.Tax = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.ChargeBU = new List<ChargeBreakUp>();
                            //sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentFareType = !string.IsNullOrEmpty ( ibResult.SelectSingleNode("AirlineRemark").InnerText) ? ibResult.SelectSingleNode("AirlineRemark").InnerText : string.Empty;
                            XmlNodeList chargeNodes1 = ibResult.SelectNodes("Fare/ChargeBU");
                            foreach (XmlNode cb in chargeNodes1)
                            {
                                ChargeBreakUp cbu = new ChargeBreakUp();
                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                switch (cb.SelectSingleNode("key").InnerText)
                                {
                                    case "TBOMARKUP":
                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                        break;
                                    case "OTHERCHARGES":
                                        cbu.ChargeType = ChargeType.OtherCharges;
                                        break;
                                    case "CONVENIENCECHARGES":
                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                        break;
                                }
                                sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.ChargeBU.Add(cbu);
                            }

                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.Currency = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.CurrencyCode = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.DecimalPoint = decimalValue;
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.YQTax = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.IncentiveEarned = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.NetFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.PublishedFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.SServiceFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.OtherCharges = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.TdsCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.TDSPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                            sr.Flights[tripIndicator][segmentIndicator - 1].SegmentPrice.TDSIncentive = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                            
                        }
                    }

                    
                    SearchResult res = sr;
                    sr.Flights[0].AsQueryable().ToList().ForEach(f => f.Stops = res.Flights[0].Length - 1);
                    if (Request.Type == SearchType.Return)
                        sr.Flights[1].AsQueryable().ToList().ForEach(f => f.Stops = res.Flights[1].Length - 1);

                }

                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to bind return segments for " + sr.FareType + ". Reason : " + ex.ToString(), "");
            }
        }
        
        /// <summary>
        /// To search Special Return fares for LCC. result will be mixed of LCC and GDS. Combining same airline results.
        /// and Displaying fares with breakdown.
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchSpecialReturnLCC(object eventNumber)
        {
            string tokenId = string.Empty;
            //Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Special Return  LCC Started" + DateTime.Now, "");
            SearchRequest request = new SearchRequest();
            request = Request;
            List<SearchResult> results = new List<SearchResult>();
            try
            {                
                if (ar!= null)
                {
                    if (string.IsNullOrEmpty(ar.Error.ErrorMessage))
                    {
                        if (!string.IsNullOrEmpty(ar.TokenId))
                            tokenId = ar.TokenId;
                        else
                        {
                            Authenticate();
                            tokenId = ar.TokenId;
                        }
                    }
                    else
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir. Reason: " + ar.Error.ErrorMessage, "");
                        throw new Exception(ar.Error.ErrorMessage);
                    }
                    //AgencyBalance balance = GetAgencyBalance(ar);
                    if (tokenId.Length > 0)
                    {
                        #region JSON Search Request
                        int type = 5;//Return

                        TBOSearchRequest searchRequest = new TBOSearchRequest();
                        searchRequest.EndUserIp = endUserIp;
                        searchRequest.TokenId = tokenId;
                        searchRequest.AdultCount = request.AdultCount;
                        searchRequest.ChildCount = request.ChildCount;
                        searchRequest.InfantCount = request.InfantCount;
                        switch (request.MaxStops)
                        {
                            case "-1":
                                searchRequest.DirectFlight = false;
                                searchRequest.OneStopFlight = false;
                                break;
                            case "0":
                                searchRequest.DirectFlight = true;
                                searchRequest.OneStopFlight = false;
                                break;
                            case "1":
                                searchRequest.DirectFlight = false;
                                searchRequest.OneStopFlight = true;
                                break;
                        }
                        searchRequest.JourneyType = type;
                        searchRequest.PreferredAirlines = //!string.IsNullOrEmpty(PrivateFareCodes) ? PrivateFareCodes.Split(',') :
                                (request.Segments[0].PreferredAirlines.Length > 0 ? request.Segments[0].PreferredAirlines : null);

                        List<Segment> Segments = new List<Segment>();
                        foreach (FlightSegment segment in request.Segments)
                        {
                            if (segment.Origin != null)
                            {
                                Segment Seg = new Segment();
                                Seg.Origin = segment.Origin;
                                Seg.Destination = segment.Destination;
                                Seg.FlightCabinClass = segment.flightCabinClass;
                                Seg.PreferredDepartureTime = segment.PreferredDepartureTime.ToString("yyyy-MM-ddT00:00:00");//Pass default time 
                                Seg.PreferredArrivalTime = segment.PreferredArrivalTime.ToString("yyyy-MM-ddT00:00:00");//Pass default time 
                                Segments.Add(Seg);
                            }
                        }
                        searchRequest.Segments = Segments.ToArray();
                        searchRequest.Sources = new string[] { "6E", "G8", "SG", "IX" };

                        //StringBuilder requestData = new StringBuilder();
                        //requestData.AppendLine("{");
                        //requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        //requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        //requestData.AppendLine("\"AdultCount\": \"" + request.AdultCount + "\",");
                        //requestData.AppendLine("\"ChildCount\": \"" + request.ChildCount + "\",");
                        //requestData.AppendLine("\"InfantCount\": \"" + request.InfantCount + "\",");
                        //if (request.MaxStops == "-1")
                        //{
                        //    requestData.AppendLine("\"DirectFlight\": \"false\",");
                        //    requestData.AppendLine("\"OneStopFlight\": \"false\",");
                        //}
                        //else if (request.MaxStops == "0")
                        //{
                        //    requestData.AppendLine("\"DirectFlight\": \"true\",");
                        //    requestData.AppendLine("\"OneStopFlight\": \"false\",");
                        //}
                        //else if (request.MaxStops == "1")
                        //{
                        //    requestData.AppendLine("\"DirectFlight\": \"false\",");
                        //    requestData.AppendLine("\"OneStopFlight\": \"true\",");
                        //}
                        //else
                        //{
                        //    requestData.AppendLine("\"DirectFlight\": \"false\",");
                        //    requestData.AppendLine("\"OneStopFlight\": \"false\",");
                        //}
                        //requestData.AppendLine("\"JourneyType\": \"" + type + "\",");
                        //if (request.Segments[0].PreferredAirlines.Length == 0)
                        //{
                        //    requestData.AppendLine("\"PreferredAirlines\": null,");
                        //}
                        //else
                        //{
                        //    //requestData.AppendLine("\"PreferredAirlines\": [");
                        //    ////requestData.AppendLine("{");
                        //    //string pa = string.Empty;
                        //    //foreach (string airline in request.Segments[0].PreferredAirlines)
                        //    //{
                        //    //    if (pa.Length > 0)
                        //    //    {
                        //    //        pa += "," + "\"" + airline + "\"";
                        //    //    }
                        //    //    else
                        //    //    {
                        //    //        pa = "\"" + airline + "\"";
                        //    //    }
                        //    //}
                        //    //requestData.AppendLine(pa);
                        //    ////requestData.AppendLine("}");
                        //    //requestData.AppendLine("],");
                        //}
                        //requestData.AppendLine("\"Segments\": [");

                        //foreach (FlightSegment segment in request.Segments)
                        //{
                        //    if (segment.Origin != null)
                        //    {
                        //        requestData.AppendLine("{");
                        //        requestData.AppendLine("\"Origin\": \"" + segment.Origin + "\",");
                        //        requestData.AppendLine("\"Destination\": \"" + segment.Destination + "\",");
                        //        requestData.AppendLine("\"FlightCabinClass\": \"" + (int)segment.flightCabinClass + "\",");
                        //        requestData.AppendLine("\"PreferredDepartureTime\": \"" + segment.PreferredDepartureTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\",");
                        //        requestData.AppendLine("\"PreferredArrivalTime\": \"" + segment.PreferredArrivalTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\"");
                        //        if (segment == request.Segments[request.Segments.Length - 1])
                        //        {
                        //            requestData.AppendLine("}");
                        //        }
                        //        else
                        //        {
                        //            requestData.AppendLine("},");
                        //        }
                        //    }
                        //}
                        //requestData.AppendLine("],");
                        //requestData.AppendLine("\"Sources\": [");

                        //if (request.Segments[0].PreferredAirlines.Length > 0)
                        //{
                        //    if (Request.Segments[0].PreferredAirlines[0] == "6E")
                        //    {
                        //        requestData.AppendLine("\"6E\"");
                        //    }
                        //    else if (Request.Segments[0].PreferredAirlines[0] == "G8")
                        //    {
                        //        requestData.AppendLine("\"G8\"");
                        //    }
                        //    else if (Request.Segments[0].PreferredAirlines[0] == "SG")
                        //    {
                        //        requestData.AppendLine("\"SG\"");
                        //    }
                        //    else
                        //    {
                        //        // Passing only LCC sources.
                        //        requestData.AppendLine("\"6E\","); //Indigo
                        //        requestData.AppendLine("\"G8\","); //Go Air
                        //        requestData.AppendLine("\"SG\","); //SpiceJet
                        //        requestData.AppendLine("\"IX\""); //Airindia Express
                        //    }
                        //}
                        //else
                        //{
                        //    // Passing only LCC sources.
                        //    requestData.AppendLine("\"6E\","); //Indigo
                        //    requestData.AppendLine("\"G8\","); //Go Air
                        //    requestData.AppendLine("\"SG\","); //SpiceJet
                        //    requestData.AppendLine("\"IX\""); //Airindia Express
                        //}
                        //requestData.AppendLine("]");
                        //requestData.AppendLine("}");
                        #endregion

                        string rsp = string.Empty;
                        //Response resp = null;
                        XmlDocument doc = null;
                        try
                        {

                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchRequestReturnLCCJSON.json";
                            //JavaScriptSerializer js = new JavaScriptSerializer();
                            //dynamic searchRequest = js.Deserialize<dynamic>(requestData.ToString());
                            rsp = JsonConvert.SerializeObject(searchRequest, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(rsp, "SearchRequest");
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchRequestReturnLCCJSON.xml";
                            doc.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save SpecialReturnLCC Request. Error" + ex.ToString(), "");
                        }

                        doc = null;
                        //Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "Special Return LCC Request Started", "");
                        string response = GetResponse(searchRequest.ToJSON(), searchUrl);
                        //Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "Special Return LCC Got REsponse", "");
                        try
                        {

                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchResponseReturnLCC.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic searchResponse = js.Deserialize<dynamic>(response);
                            rsp = JsonConvert.SerializeObject(searchResponse, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            TextReader reader = new StringReader(response);
                            doc = JsonConvert.DeserializeXmlNode(response);

                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchResponseRTNLCC.xml";
                            doc.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save SpecialReturnLCC Response. Error" + ex.ToString(), "");
                        }

                        if (doc != null)
                        {
                            XmlNode Error = doc.SelectSingleNode("Response/Error");
                            string errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                            if (errorMessage.Length <= 0)
                            {
                                XmlNodeList ResultNodes = doc.SelectNodes("Response/Results/Results");

                                if (request.IsDomestic)
                                {
                                    List<XmlNode> outboundResults = new List<XmlNode>();
                                    List<XmlNode> inboundResults = new List<XmlNode>();

                                    foreach (XmlNode result in ResultNodes)
                                    {
                                        if (!ValidatePrefAirline(PrivateFareCodes, request.Segments[0].PreferredAirlines, 
                                            result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Airline/AirlineCode").InnerText))
                                            continue;

                                        if (result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Origin/Airport/AirportCode").InnerText == request.Segments[0].Origin)
                                        {
                                            outboundResults.Add(result);
                                        }
                                        if (request.Segments.Length > 1)
                                        {
                                            if (result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Origin/Airport/AirportCode").InnerText == request.Segments[1].Origin)
                                            {
                                                inboundResults.Add(result);
                                            }
                                        }
                                    }
                                    if (exchangeRates.ContainsKey(outboundResults[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText))
                                        rateOfExchange = exchangeRates[outboundResults[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];

                                    #region Domestic Results

                                    foreach (XmlNode obResult in outboundResults)
                                    {
                                        foreach (XmlNode ibResult in inboundResults)
                                        {
                                            Airline airlineOut = new Airline();
                                            airlineOut.Load(obResult.SelectSingleNode("ValidatingAirline").InnerText);

                                            Airline airlineIn = new Airline();
                                            airlineIn.Load(ibResult.SelectSingleNode("ValidatingAirline").InnerText);

                                            if (airlineOut.TBOAirAllowed && airlineIn.TBOAirAllowed)
                                            {
                                                SearchResult sr = new SearchResult();
                                                sr.IsLCC = true;
                                                sr.Currency = agentBaseCurrency;
                                                sr.FareType = "SpecialReturnLCC (Domestic)";
                                                //if (obResult.SelectSingleNode("BaggageAllowance") != null)
                                                //{
                                                //    sr.BaggageIncludedInFare = obResult.SelectSingleNode("BaggageAllowance").InnerText;
                                                //}
                                                //=====================================================Outbound begin===============================================//
                                                #region Outbound Result
                                                sr.GUID = obResult.SelectSingleNode("ResultIndex").InnerText;
                                                sr.JourneySellKey = obResult.SelectSingleNode("Source").InnerText;
                                                XmlNodeList fareBreakDownNodes = obResult.SelectNodes("FareBreakdown");

                                                //Read CommissionEarned, PLBEarned & IncentiveEarned and deduct it from Adult fare
                                                double offeredFareAdjustment = Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                //Deduct TDS Charged on incentives and discount the remaining amount
                                                offeredFareAdjustment -= (Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText));
                                                offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                                int fareCounter = 0;
                                                List<Fare> fbdList = new List<Fare>();
                                                List<Fare> tbofbdList = new List<Fare>();
                                                foreach (XmlNode fbdNode in fareBreakDownNodes)
                                                {
                                                    Fare fbd = new Fare();
                                                    fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                    fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                    //if (fareCounter == 0 && !showPubFaresForCorp)
                                                    //{
                                                    //    if (fbd.BaseFare > offeredFareAdjustment)
                                                    //    {
                                                    //        fbd.BaseFare -= offeredFareAdjustment;
                                                    //    }
                                                    //}
                                                    fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                    fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                    fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                    if (fareCounter == 0 && !showPubFaresForCorp)
                                                    {
                                                        if (fbd.TotalFare > offeredFareAdjustment)
                                                        {
                                                            fbd.TotalFare -= offeredFareAdjustment;
                                                        }
                                                    }
                                                    fbd.SellingFare = fbd.TotalFare;
                                                    fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);
                                                    fbdList.Add(fbd);
                                                    fareCounter++;

                                                    Fare tbofbd = new Fare();
                                                    tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                    tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                    tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                    tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                    tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                    tbofbdList.Add(tbofbd);

                                                }
                                                sr.FareBreakdown = fbdList.ToArray();
                                                sr.TBOFareBreakdown = tbofbdList.ToArray();

                                                XmlNodeList fareRuleList = obResult.SelectNodes("FareRules");
                                                sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                                foreach (XmlNode fare in fareRuleList)
                                                {
                                                    CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                    fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                    fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                    fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                    fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                    fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                    fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                    sr.FareRules.Add(fr);
                                                }

                                                if (request.Type == SearchType.Return)
                                                {
                                                    sr.Flights = new FlightInfo[2][];
                                                }
                                                else
                                                {
                                                    sr.Flights = new FlightInfo[1][];
                                                }

                                                XmlNodeList segmentNodes = obResult.SelectNodes("Segments");

                                                sr.IsLCC = Convert.ToBoolean(obResult.SelectSingleNode("IsLCC").InnerText);
                                                sr.NonRefundable = (Convert.ToBoolean(obResult.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                                sr.ResultBookingSource = BookingSource.TBOAir;
                                                sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                                sr.ValidatingAirline = obResult.SelectSingleNode("ValidatingAirline").InnerText;

                                                BindSegments(segmentNodes, ref sr, obResult, null);

                                                sr.Price = new PriceAccounts();
                                                sr.Price.AccPriceType = PriceType.PublishedFare;
                                                sr.Price.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                sr.Price.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                sr.Price.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                sr.Price.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                                sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                                XmlNodeList chargeNodes = obResult.SelectNodes("Fare/ChargeBU");
                                                foreach (XmlNode cb in chargeNodes)
                                                {
                                                    ChargeBreakUp cbu = new ChargeBreakUp();
                                                    cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                    switch (cb.SelectSingleNode("key").InnerText)
                                                    {
                                                        case "TBOMARKUP":
                                                            cbu.ChargeType = ChargeType.TBOMarkup;
                                                            break;
                                                        case "OTHERCHARGES":
                                                            cbu.ChargeType = ChargeType.OtherCharges;
                                                            break;
                                                        case "CONVENIENCECHARGES":
                                                            cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                            break;
                                                    }
                                                }
                                                sr.Price.Currency = agentBaseCurrency;
                                                sr.Price.CurrencyCode = agentBaseCurrency;
                                                sr.Price.DecimalPoint = decimalValue;
                                                sr.Price.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                                sr.Price.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                                sr.Price.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                sr.Price.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                                sr.Price.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                                //sr.Price.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                                //sr.Price.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                                sr.Price.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                                sr.Price.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                                sr.Price.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                sr.Price.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                sr.Price.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                sr.Price.SupplierPrice = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                sr.Price.SupplierCurrency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                XmlNodeList taxNodes = obResult.SelectSingleNode("Fare").SelectNodes("TaxBreakup");

                                                if (taxNodes != null)
                                                {
                                                    sr.Price.TaxBreakups = new List<TaxBreakup>();
                                                    foreach (XmlNode taxNode in taxNodes)
                                                    {
                                                        if (taxNode.SelectSingleNode("key").InnerText != "OtherTaxes")
                                                        {
                                                            TaxBreakup tax = new TaxBreakup();
                                                            tax.TaxCode = taxNode.SelectSingleNode("key").InnerText;
                                                            tax.TaxValue = Convert.ToDecimal(taxNode.SelectSingleNode("value").InnerText) * rateOfExchange; ;
                                                            if (tax.TaxCode == "K3")
                                                            {
                                                                sr.Price.K3Tax = tax.TaxValue;
                                                            }
                                                            sr.Price.TaxBreakups.Add(tax);
                                                        }
                                                    }
                                                }
                                                sr.TBOPrice = new PriceAccounts();
                                                sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                                sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                sr.TBOPrice.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                sr.TBOPrice.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                sr.TBOPrice.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                                sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                                XmlNodeList chargeNodes2 = ibResult.SelectNodes("Fare/ChargeBU");
                                                foreach (XmlNode cb in chargeNodes2)
                                                {
                                                    ChargeBreakUp cbu = new ChargeBreakUp();
                                                    cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                    switch (cb.SelectSingleNode("key").InnerText)
                                                    {
                                                        case "TBOMARKUP":
                                                            cbu.ChargeType = ChargeType.TBOMarkup;
                                                            break;
                                                        case "OTHERCHARGES":
                                                            cbu.ChargeType = ChargeType.OtherCharges;
                                                            break;
                                                        case "CONVENIENCECHARGES":
                                                            cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                            break;
                                                    }
                                                }
                                                sr.TBOPrice.Currency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                sr.TBOPrice.CurrencyCode = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                sr.TBOPrice.DecimalPoint = decimalValue;
                                                sr.TBOPrice.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                                sr.TBOPrice.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                                sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                sr.TBOPrice.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                                sr.TBOPrice.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                                sr.TBOPrice.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                sr.TBOPrice.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                sr.TBOPrice.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                sr.TBOPrice.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                sr.TBOPrice.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                {
                                                    sr.TBOPrice.TransactionFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                }

                                                sr.BaseFare = (double)sr.Price.BaseFare;
                                                sr.Tax = (double)(Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                if (!showPubFaresForCorp)
                                                {
                                                    if (sr.BaseFare > offeredFareAdjustment)
                                                    {
                                                        sr.BaseFare -= offeredFareAdjustment;
                                                    }
                                                    else if (sr.BaseFare < offeredFareAdjustment && sr.Tax > offeredFareAdjustment)
                                                    {
                                                        sr.Tax -= offeredFareAdjustment;
                                                    }
                                                }
                                                sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.AdditionalTxnFee;
                                                #endregion
                                                //======================================================Outbound end================================================//


                                                //=====================================================Inbound Begin================================================//
                                                #region Inbound Result
                                                //If inbound segment is having restricted airline then skip outbound result binding


                                                XmlNodeList fareBreakDownNodes20 = ibResult.SelectNodes("FareBreakdown");
                                                sr.GUID += "," + ibResult.SelectSingleNode("ResultIndex").InnerText;
                                                sr.JourneySellKey += "|" + ibResult.SelectSingleNode("Source").InnerText;

                                                //Read CommissionEarned, PLBEarned & IncentiveEarned and deduct it from Adult fare
                                                offeredFareAdjustment = Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                //Deduct TDS Charged on incentives and discount the remaining amount
                                                offeredFareAdjustment -= (Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText));
                                                offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                                fareCounter = 0;
                                                foreach (XmlNode fbdNode in fareBreakDownNodes20)
                                                {
                                                    foreach (Fare fbd in sr.FareBreakdown)
                                                    {
                                                        if (fbd.PassengerType == (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText))
                                                        {
                                                            fbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                            fbd.BaseFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                            double baseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                            //if (fareCounter == 0 && !showPubFaresForCorp)
                                                            //{
                                                            //    if (fbd.BaseFare > offeredFareAdjustment)
                                                            //    {
                                                            //        fbd.BaseFare -= offeredFareAdjustment;
                                                            //    }
                                                            //    if (baseFare > offeredFareAdjustment)
                                                            //    {
                                                            //        baseFare -= offeredFareAdjustment;
                                                            //    }
                                                            //}

                                                            fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                            fbd.TotalFare += baseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                            if (fareCounter == 0 && !showPubFaresForCorp)
                                                            {
                                                                if (fbd.TotalFare > offeredFareAdjustment)
                                                                {
                                                                    fbd.TotalFare -= offeredFareAdjustment;
                                                                }
                                                            }
                                                            fbd.SellingFare += fbd.TotalFare;
                                                            fbd.SupplierFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);
                                                            fareCounter++;
                                                            break;
                                                        }
                                                    }

                                                    foreach (Fare tbofbd in sr.TBOFareBreakdown)
                                                    {
                                                        if (tbofbd.PassengerType == (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText))
                                                        {
                                                            tbofbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                            tbofbd.BaseFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                            tbofbd.PassengerCount += Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                            tbofbd.TotalFare += tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                            break;
                                                        }
                                                    }
                                                }

                                                XmlNodeList fareRuleList20 = ibResult.SelectNodes("FareRules");

                                                foreach (XmlNode fare in fareRuleList20)
                                                {
                                                    CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                    fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                    fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                    fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                    fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                    fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                    fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                    sr.FareRules.Add(fr);
                                                }

                                                XmlNodeList segmentNodes20 = ibResult.SelectNodes("Segments");



                                                sr.IsLCC = Convert.ToBoolean(ibResult.SelectSingleNode("IsLCC").InnerText);
                                                sr.NonRefundable = (Convert.ToBoolean(ibResult.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                                sr.ResultBookingSource = BookingSource.TBOAir;
                                                sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                                sr.ValidatingAirline = ibResult.SelectSingleNode("ValidatingAirline").InnerText;

                                                BindSegments(segmentNodes20, ref sr, null, ibResult);

                                                sr.Price.AdditionalTxnFee += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                sr.Price.AgentCommission += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                sr.Price.AgentPLB += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                sr.Price.BaseFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;

                                                XmlNodeList chargeNodes20 = ibResult.SelectNodes("Fare/ChargeBU");
                                                foreach (XmlNode cb in chargeNodes20)
                                                {
                                                    foreach (ChargeBreakUp cbu in sr.Price.ChargeBU)
                                                    {

                                                        switch (cb.SelectSingleNode("key").InnerText)
                                                        {
                                                            case "TBOMARKUP":
                                                                if (cbu.ChargeType == ChargeType.TBOMarkup)
                                                                {
                                                                    cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                                }
                                                                break;
                                                            case "OTHERCHARGES":
                                                                if (cbu.ChargeType == ChargeType.OtherCharges)
                                                                {
                                                                    cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                                }
                                                                break;
                                                            case "CONVENIENCECHARGES":
                                                                if (cbu.ChargeType == ChargeType.ConvenienceCharge)
                                                                {
                                                                    cbu.Amount += Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                                }
                                                                break;
                                                        }
                                                    }
                                                }
                                                sr.Price.Currency = agentBaseCurrency;
                                                sr.Price.CurrencyCode = agentBaseCurrency;
                                                sr.Price.DecimalPoint = decimalValue;
                                                sr.Price.Tax += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                                sr.Price.YQTax += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                                sr.Price.IncentiveEarned += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                sr.Price.PublishedFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                                sr.Price.NetFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                                //sr.Price.NetFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                                //sr.Price.PublishedFare += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                                sr.Price.SServiceFee += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                                sr.Price.OtherCharges += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                                sr.Price.TdsCommission += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                sr.Price.TDSPLB += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                sr.Price.TDSIncentive += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                sr.Price.SupplierPrice += Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);

                                                taxNodes = ibResult.SelectSingleNode("Fare").SelectNodes("TaxBreakup");

                                                if (taxNodes != null)
                                                {                                                    
                                                    foreach (XmlNode taxNode in taxNodes)
                                                    {
                                                        if (taxNode.SelectSingleNode("key").InnerText != "OtherTaxes")
                                                        {
                                                            TaxBreakup tax = new TaxBreakup();
                                                            tax.TaxCode = taxNode.SelectSingleNode("key").InnerText;
                                                            tax.TaxValue = Convert.ToDecimal(taxNode.SelectSingleNode("value").InnerText) * rateOfExchange; ;
                                                            if (tax.TaxCode == "K3")
                                                            {
                                                                sr.Price.K3Tax += tax.TaxValue;
                                                            }
                                                            sr.Price.TaxBreakups.Add(tax);
                                                        }
                                                    }
                                                }

                                                sr.TBOPrice = new PriceAccounts();
                                                sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                                sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                sr.TBOPrice.AgentCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                sr.TBOPrice.AgentPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                sr.TBOPrice.BaseFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                                sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                                XmlNodeList chargeNodes3 = ibResult.SelectNodes("Fare/ChargeBU");
                                                foreach (XmlNode cb in chargeNodes3)
                                                {
                                                    ChargeBreakUp cbu = new ChargeBreakUp();
                                                    cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                    switch (cb.SelectSingleNode("key").InnerText)
                                                    {
                                                        case "TBOMARKUP":
                                                            cbu.ChargeType = ChargeType.TBOMarkup;
                                                            break;
                                                        case "OTHERCHARGES":
                                                            cbu.ChargeType = ChargeType.OtherCharges;
                                                            break;
                                                        case "CONVENIENCECHARGES":
                                                            cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                            break;
                                                    }
                                                }
                                                sr.TBOPrice.Currency = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                sr.TBOPrice.CurrencyCode = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                sr.TBOPrice.DecimalPoint = decimalValue;
                                                sr.TBOPrice.Tax = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                                sr.TBOPrice.YQTax = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                                sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                sr.TBOPrice.NetFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                                sr.TBOPrice.PublishedFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                                sr.TBOPrice.SServiceFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                sr.TBOPrice.OtherCharges = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                sr.TBOPrice.TdsCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                sr.TBOPrice.TDSPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                sr.TBOPrice.TDSIncentive = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                if (ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                {
                                                    sr.TBOPrice.TransactionFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                }


                                                sr.BaseFare += (double)sr.Price.BaseFare;
                                                sr.Tax += (double)(Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                if (!showPubFaresForCorp)
                                                {
                                                    if (sr.BaseFare > offeredFareAdjustment)
                                                    {
                                                        sr.BaseFare -= offeredFareAdjustment;
                                                    }
                                                    else if (sr.BaseFare < offeredFareAdjustment && sr.Tax > offeredFareAdjustment)
                                                    {
                                                        sr.Tax -= offeredFareAdjustment;
                                                    }
                                                }
                                                sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;

                                                #endregion
                                                //======================================================Inbound end=================================================//

                                                //Adding logic not to bind LCC to GDS
                                                Airline oAirline = new Airline();
                                                Airline iAirline = new Airline();
                                                if (sr.Flights.Length == 2)
                                                {

                                                    //oAirline.Load(sr.Flights[0][0].Airline);
                                                    //iAirline.Load(sr.Flights[1][0].Airline);

                                                }

                                                if (sr.Flights[0][0].Airline == sr.Flights[1][0].Airline && sr.ValidatingAirline.Length > 0)
                                                {
                                                    results.Add(sr);
                                                }

                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    if (exchangeRates.ContainsKey(ResultNodes[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText))
                                        rateOfExchange = exchangeRates[ResultNodes[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];

                                    #region GDS and SpecialReturn Results
                                    foreach (XmlNode result in ResultNodes)
                                    {
                                        Airline airline = new Airline();
                                        airline.Load(result.SelectSingleNode("ValidatingAirline").InnerText);

                                        if (!ValidatePrefAirline(PrivateFareCodes, request.Segments[0].PreferredAirlines,
                                            result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Airline/AirlineCode").InnerText))
                                            continue;

                                        if (airline.TBOAirAllowed)
                                        {
                                            SearchResult sr = new SearchResult();
                                            if (type == 5)
                                            {
                                                sr.IsLCC = true;
                                                sr.FareType = "LCCSpecialReturn";
                                            }
                                            else
                                            {
                                                sr.FareType = "Normal (GDS)";
                                            }
                                            //if (result.SelectSingleNode("BaggageAllowance") != null)
                                            //{
                                            //    sr.BaggageIncludedInFare = result.SelectSingleNode("BaggageAllowance").InnerText;
                                            //}
                                            sr.Currency = agentBaseCurrency;
                                            

                                            sr.GUID = result.SelectSingleNode("ResultIndex").InnerText;
                                            sr.JourneySellKey = result.SelectSingleNode("Source").InnerText;
                                            XmlNodeList fareBreakDownNodes = result.SelectNodes("FareBreakdown");
                                            double TransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * (double)rateOfExchange : 0;
                                            double TBOTransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) : 0;
                                            double TBOotherCharges = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);

                                            double OtherCharges = (request.IsDomestic) ? 0 : Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * (double)rateOfExchange;

                                            //Read CommissionEarned, PLBEarned & IncentiveEarned and deduct it from Adult fare
                                            double offeredFareAdjustment = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            //Deduct TDS Charged on incentives and discount the remaining amount
                                            offeredFareAdjustment -= (Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText));
                                            offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                            int fareCounter = 0;
                                            List<Fare> fbdList = new List<Fare>();
                                            List<Fare> tbofbdList = new List<Fare>();
                                            foreach (XmlNode fbdNode in fareBreakDownNodes)
                                            {
                                                Fare fbd = new Fare();
                                                fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                //if(fareCounter == 0 && !showPubFaresForCorp)
                                                //{
                                                //    if(fbd.BaseFare > offeredFareAdjustment)
                                                //    {
                                                //        fbd.BaseFare -= offeredFareAdjustment;
                                                //    }
                                                //}
                                                fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                if (fareCounter == 0 && !showPubFaresForCorp)
                                                {
                                                    if (fbd.TotalFare > offeredFareAdjustment)
                                                    {
                                                        fbd.TotalFare -= offeredFareAdjustment;
                                                    }
                                                }
                                                fbd.SellingFare = fbd.TotalFare;
                                                fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);

                                                fbdList.Add(fbd);
                                                fareCounter++;

                                                Fare tbofbd = new Fare();
                                                tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                tbofbdList.Add(tbofbd);

                                            }
                                            sr.FareBreakdown = fbdList.ToArray();
                                            sr.TBOFareBreakdown = tbofbdList.ToArray();

                                            XmlNodeList fareRuleList = result.SelectNodes("FareRules");
                                            sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                            foreach (XmlNode fare in fareRuleList)
                                            {
                                                CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                sr.FareRules.Add(fr);
                                            }
                                            XmlNodeList segmentNodes = result.SelectNodes("Segments");
                                            if (request.Type == SearchType.Return)
                                            {
                                                sr.Flights = new FlightInfo[2][];
                                            }
                                            else if (request.Type == SearchType.MultiWay)
                                            {
                                                sr.Flights = new FlightInfo[segmentNodes.Count][];
                                            }
                                            else
                                            {
                                                sr.Flights = new FlightInfo[1][];
                                            }

                                            if (request.Type != SearchType.MultiWay)
                                            {
                                                BindSegments(segmentNodes, ref sr, result);
                                            }
                                            else //MultiCity 
                                            {
                                                //int tripIndicator = 1;
                                                BindSegments(segmentNodes, ref sr, result);
                                            }

                                            sr.IsLCC = Convert.ToBoolean(result.SelectSingleNode("IsLCC").InnerText);
                                            sr.NonRefundable = (Convert.ToBoolean(result.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                            sr.ResultBookingSource = BookingSource.TBOAir;
                                            sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                            sr.ValidatingAirline = result.SelectSingleNode("ValidatingAirline").InnerText;

                                            sr.Price = new PriceAccounts();
                                            sr.Price.AccPriceType = PriceType.PublishedFare;
                                            sr.Price.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                            sr.Price.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.Price.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.Price.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                            sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes = result.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.Price.Currency = agentBaseCurrency;
                                            sr.Price.CurrencyCode = agentBaseCurrency;
                                            sr.Price.DecimalPoint = decimalValue;
                                            sr.Price.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;

                                            sr.Price.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                            sr.Price.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.Price.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                            sr.Price.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                            //sr.Price.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                            //sr.Price.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                            sr.Price.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                            sr.Price.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                            sr.Price.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.Price.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.Price.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.Price.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                            }
                                            //sr.Price.Tax += sr.Price.OtherCharges + sr.Price.TransactionFee;
                                            sr.Price.SupplierPrice = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                            sr.Price.SupplierCurrency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;

                                            XmlNodeList taxNodes = result.SelectSingleNode("Fare").SelectNodes("TaxBreakup");

                                            if (taxNodes != null)
                                            {
                                                sr.Price.TaxBreakups = new List<TaxBreakup>();
                                                foreach (XmlNode taxNode in taxNodes)
                                                {
                                                    if (taxNode.SelectSingleNode("key").InnerText != "OtherTaxes")
                                                    {
                                                        TaxBreakup tax = new TaxBreakup();
                                                        tax.TaxCode = taxNode.SelectSingleNode("key").InnerText;
                                                        tax.TaxValue = Convert.ToDecimal(taxNode.SelectSingleNode("value").InnerText) * rateOfExchange; ;
                                                        if (tax.TaxCode == "K3")
                                                        {
                                                            sr.Price.K3Tax = tax.TaxValue;
                                                        }
                                                        sr.Price.TaxBreakups.Add(tax);
                                                    }
                                                }
                                            }

                                            sr.TBOPrice = new PriceAccounts();
                                            sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                            sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                            sr.TBOPrice.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.TBOPrice.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.TBOPrice.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                            sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes2 = result.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes2)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.TBOPrice.Currency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.CurrencyCode = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.DecimalPoint = decimalValue;
                                            sr.TBOPrice.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                            sr.TBOPrice.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                            sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.TBOPrice.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                            sr.TBOPrice.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                            sr.TBOPrice.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                            sr.TBOPrice.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                            sr.TBOPrice.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.TBOPrice.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.TBOPrice.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.TBOPrice.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            }

                                            sr.BaseFare = (double)sr.Price.BaseFare;
                                            sr.Tax = (double)(Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                            if (!showPubFaresForCorp)
                                            {
                                                if (sr.BaseFare > offeredFareAdjustment)
                                                {
                                                    sr.BaseFare -= offeredFareAdjustment;
                                                }
                                                else if (sr.BaseFare < offeredFareAdjustment && sr.Tax > offeredFareAdjustment)
                                                {
                                                    sr.Tax -= offeredFareAdjustment;
                                                }
                                            }
                                            sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee;

                                            results.Add(sr);
                                        }
                                    }
                                    #endregion
                                }
                                SearchResult[] finalResult = results.ToArray();
                                if (finalResult.Length > 0)
                                {
                                    Array.Sort<SearchResult>(finalResult, finalResult[0]);
                                }
                            }
                        }

                        CombineResults.AddRange(results);
                    }
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                }
                eventFlag[(int)eventNumber].Set();
                //Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Special Return  LCC Ended" + DateTime.Now, "");
            }
            catch (Exception ex)
            {
                eventFlag[(int)eventNumber].Set();
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Search. Error : " + ex.ToString(), "");
            }

        }
        /// <summary>
        /// To fetch normal return fares. result will be mixed of LCC and GDS. Combining LCC to LCC and GDS to GDS 
        /// and Displaying fares with breakdown.
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchNormalReturn(object eventNumber)
        {
            //Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Normal Return Started" + DateTime.Now, "");
            SearchRequest request = Request;
            List<SearchResult> results = new List<SearchResult>();
            try
            {
                Authenticate();
                if (ar != null)
                {
                    if (ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                    {
                        //AgencyBalance balance = GetAgencyBalance(ar);

                        #region JSON Search Request
                        int type = 2;//Return

                        TBOSearchRequest searchRequest = new TBOSearchRequest();
                        searchRequest.EndUserIp = endUserIp;
                        searchRequest.TokenId = ar.TokenId;
                        searchRequest.AdultCount = request.AdultCount;
                        searchRequest.ChildCount = request.ChildCount;
                        searchRequest.InfantCount = request.InfantCount;
                        switch (request.MaxStops)
                        {
                            case "-1":
                                searchRequest.DirectFlight = false;
                                searchRequest.OneStopFlight = false;
                                break;
                            case "0":
                                searchRequest.DirectFlight = true;
                                searchRequest.OneStopFlight = false;
                                break;
                            case "1":
                                searchRequest.DirectFlight = false;
                                searchRequest.OneStopFlight = true;
                                break;
                        }
                        searchRequest.JourneyType = type;
                        searchRequest.PreferredAirlines = (request.Segments[0].PreferredAirlines.Length > 0 ? request.Segments[0].PreferredAirlines : null);

                        List<Segment> Segments = new List<Segment>();
                        foreach (FlightSegment segment in request.Segments)
                        {
                            if (segment.Origin != null)
                            {
                                Segment Seg = new Segment();
                                Seg.Origin = segment.Origin;
                                Seg.Destination = segment.Destination;
                                Seg.FlightCabinClass = segment.flightCabinClass;
                                Seg.PreferredDepartureTime = segment.PreferredDepartureTime.ToString("yyyy-MM-ddT00:00:00");//Pass default time 
                                Seg.PreferredArrivalTime = segment.PreferredArrivalTime.ToString("yyyy-MM-ddT00:00:00");//Pass default time 
                                Segments.Add(Seg);
                            }
                        }
                        searchRequest.Segments = Segments.ToArray();
                        searchRequest.Sources = new string[] { "GDS", "AK", "IX", "G9", "FZ" };

                        //StringBuilder requestData = new StringBuilder();
                        //requestData.AppendLine("{");
                        //requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        //requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        //requestData.AppendLine("\"AdultCount\": \"" + request.AdultCount + "\",");
                        //requestData.AppendLine("\"ChildCount\": \"" + request.ChildCount + "\",");
                        //requestData.AppendLine("\"InfantCount\": \"" + request.InfantCount + "\",");
                        //if (request.MaxStops == "-1")
                        //{
                        //    requestData.AppendLine("\"DirectFlight\": \"false\",");
                        //    requestData.AppendLine("\"OneStopFlight\": \"false\",");
                        //}
                        //else if (request.MaxStops == "0")
                        //{
                        //    requestData.AppendLine("\"DirectFlight\": \"true\",");
                        //    requestData.AppendLine("\"OneStopFlight\": \"false\",");
                        //}
                        //else if (request.MaxStops == "1")
                        //{
                        //    requestData.AppendLine("\"DirectFlight\": \"false\",");
                        //    requestData.AppendLine("\"OneStopFlight\": \"true\",");
                        //}
                        //else
                        //{
                        //    requestData.AppendLine("\"DirectFlight\": \"false\",");
                        //    requestData.AppendLine("\"OneStopFlight\": \"false\",");
                        //}
                        //requestData.AppendLine("\"JourneyType\": \"" + type + "\",");
                        //if (request.Segments[0].PreferredAirlines.Length == 0)
                        //{
                        //    requestData.AppendLine("\"PreferredAirlines\": null,");
                        //}
                        ////else
                        ////{
                        ////    requestData.AppendLine("\"PreferredAirlines\": [");
                        ////    //requestData.AppendLine("{");
                        ////    string pa = string.Empty;
                        ////    foreach (string airline in request.Segments[0].PreferredAirlines)
                        ////    {
                        ////        if (pa.Length > 0)
                        ////        {
                        ////            pa += "," + "\"" + airline + "\"";
                        ////        }
                        ////        else
                        ////        {
                        ////            pa = "\"" + airline + "\"";
                        ////        }
                        ////    }
                        ////    requestData.AppendLine(pa);
                        ////    //requestData.AppendLine("}");
                        ////    requestData.AppendLine("],");
                        ////}
                        //requestData.AppendLine("\"Segments\": [");

                        //foreach (CT.BookingEngine.FlightSegment segment in request.Segments)
                        //{
                        //    if (segment.Origin != null)
                        //    {
                        //        requestData.AppendLine("{");
                        //        requestData.AppendLine("\"Origin\": \"" + segment.Origin + "\",");
                        //        requestData.AppendLine("\"Destination\": \"" + segment.Destination + "\",");
                        //        requestData.AppendLine("\"FlightCabinClass\": \"" + (int)segment.flightCabinClass + "\",");
                        //        requestData.AppendLine("\"PreferredDepartureTime\": \"" + segment.PreferredDepartureTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\",");
                        //        requestData.AppendLine("\"PreferredArrivalTime\": \"" + segment.PreferredArrivalTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\"");
                        //        if (segment == request.Segments[request.Segments.Length - 1])
                        //        {
                        //            requestData.AppendLine("}");
                        //        }
                        //        else
                        //        {
                        //            requestData.AppendLine("},");
                        //        }
                        //    }
                        //}
                        //searchRequest.Segments = segments.ToArray();
                        //requestData.AppendLine("]");
                        //requestData.AppendLine("\"Sources\": [");
                        ////Passing both LCC and GDS Sources . Mix match results will come.
                        ////if (request.Segments[0].PreferredAirlines.Length > 0)
                        ////{
                        ////    //if (Request.Segments[0].PreferredAirlines[0] == "6E")
                        ////    //{
                        ////    //    requestData.AppendLine("\"6E\"");
                        ////    //}
                        ////    //else if (Request.Segments[0].PreferredAirlines[0] == "G8")
                        ////    //{
                        ////    //    requestData.AppendLine("\"G8\"");
                        ////    //}
                        ////    //else if (Request.Segments[0].PreferredAirlines[0] == "SG")
                        ////    //{
                        ////    //    requestData.AppendLine("\"SG\"");
                        ////    //}
                        ////    //if (Request.Segments[0].PreferredAirlines[0] == "AK")
                        ////    //{
                        ////    //    requestData.AppendLine("\"AK\"");
                        ////    //}
                        ////    //else if (Request.Segments[0].PreferredAirlines[0] == "LB")
                        ////    //{
                        ////    //    requestData.AppendLine("\"LB\"");
                        ////    //}
                        ////    //else if (Request.Segments[0].PreferredAirlines[0] == "IX")
                        ////    //{
                        ////    //    requestData.AppendLine("\"IX\"");
                        ////    //}
                        ////    //else if (Request.Segments[0].PreferredAirlines[0] == "G9")
                        ////    //{
                        ////    //    requestData.AppendLine("\"G9\"");
                        ////    //}
                        ////    //else if (Request.Segments[0].PreferredAirlines[0] == "FZ")
                        ////    //{
                        ////    //    requestData.AppendLine("\"FZ\"");
                        ////    //}
                        ////    //else
                        ////    //{
                        ////    //    requestData.AppendLine("\"GDS\"");
                        ////    //}
                        ////}
                        ////else
                        //{
                        //    //requestData.AppendLine("\"6E\","); //Indigo
                        //    //requestData.AppendLine("\"G8\","); //Go Air
                        //    //requestData.AppendLine("\"SG\","); //SpiceJet
                        //    requestData.AppendLine("\"AK\","); //Air Asia
                        //    //requestData.AppendLine("\"LB\","); //Air Costa
                        //    requestData.AppendLine("\"IX\","); //AirIndia Express
                        //    requestData.AppendLine("\"G9\","); //Air Arabia
                        //    requestData.AppendLine("\"FZ\""); //FlyDubai
                        //    //requestData.AppendLine("\"GDS\""); //All other GDS airlines
                        //}
                        //requestData.AppendLine("]");
                        //requestData.AppendLine("}");
                        #endregion

                        List<string> allowedSources = new List<string>();
                        allowedSources.Add("AK");
                        allowedSources.Add("IX");
                        allowedSources.Add("G9");
                        allowedSources.Add("FZ");

                        //Response resp = null;
                        XmlDocument doc = null;
                        string rsp = string.Empty;
                        try
                        {
                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchRequestJSON_NormalGDS.json";
                            //JavaScriptSerializer js = new JavaScriptSerializer();
                            //dynamic searchRequest = js.Deserialize<dynamic>(requestData.ToString());
                            rsp = JsonConvert.SerializeObject(searchRequest, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(rsp, "SearchRequest");
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchRequest_NormalGDS.xml";
                            doc.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save Normal return Request. Error" + ex.ToString(), "");
                        }

                        doc = null;

                        //Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "NormalReturn Request Started", "");
                        string response = GetResponse(searchRequest.ToJSON(), searchUrl);
                        // Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "NormalReturn Got Response", "");

                        try
                        {

                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchResponseJSON_NormalGDS.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic searchResponse = js.Deserialize<dynamic>(response);
                            rsp = JsonConvert.SerializeObject(searchResponse, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            TextReader reader = new StringReader(response);
                            doc = JsonConvert.DeserializeXmlNode(response);

                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchResponse_NormalGDS.xml";
                            doc.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save Normal return Response. Error" + ex.ToString(), "");
                        }

                        if (doc != null)
                        {
                            XmlNode Error = doc.SelectSingleNode("Response/Error");
                            string errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                            if (errorMessage.Length <= 0)
                            {
                                XmlNodeList ResultNodes = doc.SelectNodes("Response/Results/Results");

                                List<string> prefAirlines = new List<string>();
                                if (Request.Segments[0].PreferredAirlines.Length > 0)
                                {
                                    prefAirlines.AddRange(Request.Segments[0].PreferredAirlines);
                                }
                                if ((request.IsDomestic && type == 2))
                                {
                                    List<XmlNode> outboundResults = new List<XmlNode>();
                                    List<XmlNode> inboundResults = new List<XmlNode>();

                                    List<XmlNode> filteredNodes = new List<XmlNode>();
                                    foreach (XmlNode result in ResultNodes)
                                    {
                                        //string source = allowedSources.Find(delegate (string s) { return s == result.SelectSingleNode("AirlineCode").InnerText; });

                                        //if (source != null && source.Length > 0)
                                        {
                                            filteredNodes.Add(result);
                                        }
                                    }

                                    foreach (XmlNode result in filteredNodes)
                                    {
                                        if (result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Origin/Airport/AirportCode").InnerText == request.Segments[0].Origin)
                                        {
                                            if (Request.Segments[0].PreferredAirlines.Length > 0)
                                            {
                                                if (Request.Segments[0].PreferredAirlines[0] == result.SelectSingleNode("AirlineCode").InnerText)
                                                {
                                                    outboundResults.Add(result);
                                                }
                                            }
                                            else
                                            {
                                                outboundResults.Add(result);
                                            }
                                        }
                                        if (request.Segments.Length > 1)
                                        {
                                            if (result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Origin/Airport/AirportCode").InnerText == request.Segments[1].Origin)
                                            {
                                                if (Request.Segments[0].PreferredAirlines.Length > 0)
                                                {
                                                    if (Request.Segments[0].PreferredAirlines[0] == result.SelectSingleNode("AirlineCode").InnerText)
                                                    {
                                                        inboundResults.Add(result);
                                                    }
                                                }
                                                else
                                                {
                                                    inboundResults.Add(result);
                                                }
                                            }
                                        }
                                    }
                                    if (outboundResults.Count > 0 && exchangeRates.ContainsKey(outboundResults[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText))
                                    {
                                        rateOfExchange = exchangeRates[outboundResults[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];
                                    }
                                    try
                                    {
                                        #region Domestic Results
                                        #region Outbound Result
                                        foreach (XmlNode obResult in outboundResults)
                                        {
                                            SearchResult sr = new SearchResult();
                                            sr.IsLCC = Convert.ToBoolean(obResult.SelectSingleNode("IsLCC").InnerText);
                                            sr.Currency = agentBaseCurrency;
                                            sr.JourneySellKey = obResult.SelectSingleNode("Source").InnerText;
                                            if (obResult.SelectSingleNode("BaggageAllowance") != null)
                                            {
                                                sr.BaggageIncludedInFare = obResult.SelectSingleNode("BaggageAllowance").InnerText;
                                            }
                                            //=====================================================Outbound begin===============================================//

                                            sr.GUID = obResult.SelectSingleNode("ResultIndex").InnerText;
                                            XmlNodeList fareBreakDownNodes = obResult.SelectNodes("FareBreakdown");
                                            List<Fare> fbdList = new List<Fare>();
                                            List<Fare> tbofbdList = new List<Fare>();

                                            //Read CommissionEarned, PLBEarned & IncentiveEarned and deduct it from Adult fare
                                            double offeredFareAdjustment = Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            //Deduct TDS Charged on incentives and discount the remaining amount
                                            offeredFareAdjustment -= (Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText));
                                            offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                            int fareCounter = 0;
                                            foreach (XmlNode fbdNode in fareBreakDownNodes)
                                            {
                                                Fare fbd = new Fare();
                                                fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                //if (fareCounter == 0 && !showPubFaresForCorp)
                                                //{
                                                //    if (fbd.BaseFare > offeredFareAdjustment)
                                                //    {
                                                //        fbd.BaseFare -= offeredFareAdjustment;
                                                //    }
                                                //}
                                                fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                if (fareCounter == 0 && !showPubFaresForCorp)//Only for Adult
                                                {
                                                    //if Adj amount is greater than base fare then deduct it from total amount
                                                    if (fbd.TotalFare > offeredFareAdjustment)
                                                    {
                                                        fbd.TotalFare -= offeredFareAdjustment;
                                                    }
                                                }
                                                fbd.SellingFare = fbd.TotalFare;
                                                fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                {
                                                    fbd.SupplierFare += Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                }
                                                fbdList.Add(fbd);
                                                fareCounter++;
                                                Fare tbofbd = new Fare();
                                                tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                tbofbdList.Add(tbofbd);

                                            }
                                            sr.FareBreakdown = fbdList.ToArray();
                                            sr.TBOFareBreakdown = tbofbdList.ToArray();

                                            XmlNodeList fareRuleList = obResult.SelectNodes("FareRules");
                                            sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                            foreach (XmlNode fare in fareRuleList)
                                            {
                                                CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                sr.FareRules.Add(fr);
                                            }


                                            sr.Flights = new FlightInfo[1][];

                                            XmlNodeList segmentNodes = obResult.SelectNodes("Segments");

                                            BindSegments(segmentNodes, ref sr, obResult, null);

                                            sr.IsLCC = Convert.ToBoolean(obResult.SelectSingleNode("IsLCC").InnerText);
                                            sr.NonRefundable = (Convert.ToBoolean(obResult.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                            sr.ResultBookingSource = BookingSource.TBOAir;
                                            sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                            sr.ValidatingAirline = obResult.SelectSingleNode("ValidatingAirline").InnerText;
                                            sr.Price = new PriceAccounts();
                                            sr.Price.AccPriceType = PriceType.PublishedFare;
                                            sr.Price.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                            sr.Price.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.Price.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.Price.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                            sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes = obResult.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.Price.Currency = agentBaseCurrency;
                                            sr.Price.CurrencyCode = agentBaseCurrency;
                                            sr.Price.DecimalPoint = decimalValue;
                                            sr.Price.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                            sr.Price.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                            sr.Price.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.Price.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                            sr.Price.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                            sr.Price.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                            sr.Price.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                            sr.Price.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.Price.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.Price.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            sr.Price.SupplierPrice = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);

                                            sr.TBOPrice = new PriceAccounts();
                                            sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                            sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                            sr.TBOPrice.AgentCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.TBOPrice.AgentPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.TBOPrice.BaseFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                            sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes2 = obResult.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes2)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.TBOPrice.Currency = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.CurrencyCode = obResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.DecimalPoint = decimalValue;
                                            sr.TBOPrice.Tax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                            sr.TBOPrice.YQTax = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                            sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.TBOPrice.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                            sr.TBOPrice.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                            sr.TBOPrice.SServiceFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                            sr.TBOPrice.OtherCharges = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                            sr.TBOPrice.TdsCommission = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.TBOPrice.TDSPLB = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.TBOPrice.TDSIncentive = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.TBOPrice.TransactionFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            }

                                            if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.TBOPrice.TransactionFee = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            }
                                            else
                                            {
                                                sr.Price.TransactionFee = 0.0M;
                                            }

                                            sr.BaseFare = (double)sr.Price.BaseFare;
                                            sr.Tax = (double)(Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                            if (!showPubFaresForCorp)
                                            {
                                                if (sr.BaseFare > offeredFareAdjustment)
                                                {
                                                    sr.BaseFare -= offeredFareAdjustment;
                                                }
                                                else if (sr.BaseFare < offeredFareAdjustment && sr.Tax > offeredFareAdjustment)
                                                {
                                                    sr.Tax -= offeredFareAdjustment;
                                                }
                                            }
                                            sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;
                                            sr.FareType = "NormalReturn";
                                            results.Add(sr);

                                        }
                                        #endregion
                                        //======================================================Outbound end================================================//

                                        //=====================================================Inbound Begin================================================//
                                        #region Inbound Result
                                        foreach (XmlNode ibResult in inboundResults)
                                        {
                                            SearchResult sr = new SearchResult();
                                            sr.IsLCC = Convert.ToBoolean(ibResult.SelectSingleNode("IsLCC").InnerText);
                                            sr.Currency = agentBaseCurrency;
                                            sr.JourneySellKey = ibResult.SelectSingleNode("Source").InnerText;
                                            XmlNodeList fareBreakDownNodes20 = ibResult.SelectNodes("FareBreakdown");
                                            sr.GUID = ibResult.SelectSingleNode("ResultIndex").InnerText;
                                            if (ibResult.SelectSingleNode("BaggageAllowance") != null)
                                            {
                                                sr.BaggageIncludedInFare = ibResult.SelectSingleNode("BaggageAllowance").InnerText;
                                            }
                                            //Read CommissionEarned, PLBEarned & IncentiveEarned and deduct it from Adult fare
                                            double offeredFareAdjustment = Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            //Deduct TDS Charged on incentives and discount the remaining amount
                                            offeredFareAdjustment -= (Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText));
                                            offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                            int fareCounter = 0;
                                            List<Fare> fbdList = new List<Fare>();
                                            List<Fare> tbofbdList = new List<Fare>();
                                            foreach (XmlNode fbdNode in fareBreakDownNodes20)
                                            {
                                                Fare fbd = new Fare();
                                                fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                //if (fareCounter == 0 && !showPubFaresForCorp)
                                                //{
                                                //    if (fbd.BaseFare > offeredFareAdjustment)
                                                //    {
                                                //        fbd.BaseFare -= offeredFareAdjustment;
                                                //    }
                                                //}
                                                fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                if (fareCounter == 0 && !showPubFaresForCorp)//Only for Adult
                                                {
                                                    //if Adj amount is greater than base fare then deduct it from total amount
                                                    if (fbd.TotalFare > offeredFareAdjustment)
                                                    {
                                                        fbd.TotalFare -= offeredFareAdjustment;
                                                    }
                                                }
                                                fbd.SellingFare = fbd.TotalFare;
                                                fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) + Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                if (ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                {
                                                    fbd.SupplierFare += Convert.ToDouble(ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                }
                                                fbdList.Add(fbd);
                                                fareCounter++;
                                                Fare tbofbd = new Fare();
                                                tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                tbofbdList.Add(tbofbd);

                                            }
                                            sr.FareBreakdown = fbdList.ToArray();
                                            sr.TBOFareBreakdown = tbofbdList.ToArray();
                                            #region Old FareBreakdown Code
                                            //foreach (XmlNode fbdNode in fareBreakDownNodes20)
                                            //{
                                            //    //foreach (Fare fbd in sr.FareBreakdown)
                                            //    {
                                            //        if (fbd.PassengerType == (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText))
                                            //        {
                                            //            fbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                            //            fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                            //            if (fareCounter == 0 && !showPubFaresForCorp)
                                            //            {
                                            //                if (fbd.BaseFare > offeredFareAdjustment)
                                            //                {
                                            //                    fbd.BaseFare -= offeredFareAdjustment;
                                            //                }
                                            //            }
                                            //            fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                            //            fbd.TotalFare += fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                            //            if (fareCounter == 0 && !showPubFaresForCorp)//Only for Adult
                                            //            {
                                            //                //if Adj amount is greater than base fare then deduct it from total amount
                                            //                if (fbd.BaseFare < offeredFareAdjustment && fbd.TotalFare > offeredFareAdjustment)
                                            //                {
                                            //                    fbd.TotalFare -= offeredFareAdjustment;
                                            //                }
                                            //            }
                                            //            fbd.SellingFare += fbd.TotalFare;                                                        
                                            //            fbd.SupplierFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) + Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                            //            if (obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            //            {
                                            //                fbd.SupplierFare += Convert.ToDouble(obResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            //            }
                                            //            fareCounter++;
                                            //            break;
                                            //        }
                                            //    }

                                            //    foreach (Fare tbofbd in sr.TBOFareBreakdown)
                                            //    {
                                            //        if (tbofbd.PassengerType == (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText))
                                            //        {
                                            //            tbofbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                            //            tbofbd.BaseFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                            //            tbofbd.PassengerCount += Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                            //            tbofbd.TotalFare += tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                            //            break;
                                            //        }
                                            //    }
                                            //} 
                                            #endregion
                                            sr.FareRules = new List<FareRule>();
                                            XmlNodeList fareRuleList20 = ibResult.SelectNodes("FareRules");

                                            foreach (XmlNode fare in fareRuleList20)
                                            {
                                                CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                                sr.FareRules.Add(fr);
                                            }
                                            sr.Flights = new FlightInfo[1][];

                                            XmlNodeList segmentNodes20 = ibResult.SelectNodes("Segments");

                                            BindSegments(segmentNodes20, ref sr, ibResult, null);

                                            foreach (FlightInfo seg in sr.Flights[0])
                                            {
                                                seg.Group = 1;
                                            }
                                            sr.IsLCC = Convert.ToBoolean(ibResult.SelectSingleNode("IsLCC").InnerText);
                                            sr.NonRefundable = (Convert.ToBoolean(ibResult.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                            sr.ResultBookingSource = BookingSource.TBOAir;
                                            sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                            sr.ValidatingAirline = ibResult.SelectSingleNode("ValidatingAirline").InnerText;

                                            sr.Price = new PriceAccounts();
                                            sr.Price.AdditionalTxnFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                            sr.Price.AgentCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.Price.AgentPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.Price.BaseFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                            sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes20 = ibResult.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes20)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                                sr.Price.ChargeBU.Add(cbu);
                                            }

                                            sr.Price.Currency = agentBaseCurrency;
                                            sr.Price.CurrencyCode = agentBaseCurrency;
                                            sr.Price.DecimalPoint = decimalValue;
                                            sr.Price.Tax = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                            sr.Price.YQTax = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                            sr.Price.IncentiveEarned = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.Price.NetFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                            sr.Price.PublishedFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                            sr.Price.SServiceFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                            sr.Price.OtherCharges = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                            sr.Price.TdsCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.Price.TDSPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.Price.TDSIncentive = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            sr.Price.SupplierPrice = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);

                                            sr.TBOPrice = new PriceAccounts();
                                            sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                            sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                            sr.TBOPrice.AgentCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.TBOPrice.AgentPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.TBOPrice.BaseFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                            sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes3 = ibResult.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes3)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.TBOPrice.Currency = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.CurrencyCode = ibResult.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.DecimalPoint = decimalValue;
                                            sr.TBOPrice.Tax = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                            sr.TBOPrice.YQTax = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                            sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.TBOPrice.NetFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                            sr.TBOPrice.PublishedFare = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                            sr.TBOPrice.SServiceFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                            sr.TBOPrice.OtherCharges = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                            sr.TBOPrice.TdsCommission = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.TBOPrice.TDSPLB = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.TBOPrice.TDSIncentive = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            if (ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.TBOPrice.TransactionFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            }

                                            if (ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.Price.TransactionFee = Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            }
                                            else
                                            {
                                                sr.Price.TransactionFee = 0.0M;
                                            }
                                            sr.BaseFare += (double)sr.Price.BaseFare;
                                            sr.Tax += (double)(Convert.ToDecimal(ibResult.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                            if (!showPubFaresForCorp)
                                            {
                                                if (sr.BaseFare > offeredFareAdjustment)
                                                {
                                                    sr.BaseFare -= offeredFareAdjustment;
                                                }
                                                else if (sr.BaseFare < offeredFareAdjustment && sr.Tax > offeredFareAdjustment)
                                                {
                                                    sr.Tax -= offeredFareAdjustment;
                                                }
                                            }
                                            sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;
                                            sr.FareType = "NormalReturn";
                                            results.Add(sr);

                                            //Adding logic not to bind LCC to GDS


                                            //Airline oAirline = new Airline();
                                            //Airline iAirline = new Airline();
                                            //try
                                            //{
                                            //    if (sr.Flights.Length == 2)
                                            //    {
                                            //        oAirline.Load(sr.Flights[0][0].Airline);
                                            //        iAirline.Load(sr.Flights[1][0].Airline);
                                            //    }
                                            //}
                                            //catch (Exception ex)
                                            //{
                                            //    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to load airline. Onward : " + sr.Flights[0][0].Airline + " Return : " + sr.Flights[1][0].Airline + " Error : " + ex.ToString(), "");
                                            //}

                                            //if (oAirline.IsLCC == iAirline.IsLCC) // Normal retrun domestic. Should only combine GDS-GDS and LCC-LCC
                                            //{
                                            //    results.Add(sr);
                                            //}

                                        }
                                        #endregion
                                        //======================================================Inbound end=================================================//
                                        #endregion
                                    }
                                    catch (Exception ex)
                                    {
                                        Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to bind Domestic results. " + ex.ToString(), "");
                                    }
                                }
                                else
                                {
                                    //#region GDS and SpecialReturn Results

                                    //List<XmlNode> filteredNodes = new List<XmlNode>();
                                    //foreach (XmlNode result in ResultNodes)
                                    //{
                                    //    string source = allowedSources.Find(delegate(string s) { return s == result.SelectSingleNode("AirlineCode").InnerText; });

                                    //    if (source != null && source.Length > 0)
                                    //    {
                                    //        filteredNodes.Add(result);
                                    //    }
                                    //}

                                    ////foreach (XmlNode result in ResultNodes)
                                    //foreach(XmlNode result in filteredNodes)
                                    //{
                                    //    SearchResult sr = null;
                                    //    if (Request.Segments[0].PreferredAirlines.Length > 0)
                                    //    {
                                    //        if (Request.Segments[0].PreferredAirlines[0] == result.SelectSingleNode("AirlineCode").InnerText)
                                    //        {
                                    //            sr = new SearchResult();
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        sr = new SearchResult();
                                    //    }
                                    //    if (sr != null)
                                    //    {
                                    //        if (type == 5)
                                    //        {
                                    //            sr.IsLCC = true;
                                    //            sr.FareType = "LCCSpecialReturn";
                                    //        }
                                    //        else
                                    //        {
                                    //            sr.FareType = "Normal (GDS)";
                                    //        }
                                    //        if (result.SelectSingleNode("BaggageAllowance") != null)
                                    //        {
                                    //            sr.BaggageIncludedInFare = result.SelectSingleNode("BaggageAllowance").InnerText;
                                    //        }
                                    //        sr.JourneySellKey = result.SelectSingleNode("Source").InnerText;
                                    //        sr.Currency = agentBaseCurrency;
                                    //        rateOfExchange = exchangeRates[result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];
                                    //        sr.GUID = result.SelectSingleNode("ResultIndex").InnerText;
                                    //        XmlNodeList fareBreakDownNodes = result.SelectNodes("FareBreakdown");
                                    //        double TransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * (double)rateOfExchange : 0;
                                    //        double TBOTransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) : 0;
                                    //        double TBOotherCharges = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                    //        //Read CommissionEarned, PLBEarned & IncentiveEarned and deduct it from Adult fare
                                    //        double offeredFareAdjustment = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                    //        //Deduct TDS Charged on incentives and discount the remaining amount
                                    //        offeredFareAdjustment -= (Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText));
                                    //        offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                    //        int fareCounter = 0;
                                    //        double OtherCharges = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * (double)rateOfExchange;
                                    //        List<Fare> fbdList = new List<Fare>();
                                    //        List<Fare> tbofbdList = new List<Fare>();
                                    //        foreach (XmlNode fbdNode in fareBreakDownNodes)
                                    //        {
                                    //            Fare fbd = new Fare();
                                    //            fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                    //            fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                    //            if (fareCounter == 0 && !showPubFaresForCorp)
                                    //            {
                                    //                if (fbd.BaseFare > offeredFareAdjustment)
                                    //                {
                                    //                    fbd.BaseFare -= offeredFareAdjustment;
                                    //                }
                                    //            }
                                    //            fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                    //            fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                    //            fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                    //            if (fareCounter == 0 && !showPubFaresForCorp)//Only for Adult
                                    //            {
                                    //                //if Adj amount is greater than base fare then deduct it from total amount
                                    //                if (fbd.BaseFare < offeredFareAdjustment && fbd.TotalFare > offeredFareAdjustment)
                                    //                {
                                    //                    fbd.TotalFare -= offeredFareAdjustment;
                                    //                }
                                    //            }
                                    //            fbd.SellingFare = fbd.TotalFare;
                                    //            fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);

                                    //            fbdList.Add(fbd);
                                    //            fareCounter++;
                                    //            Fare tbofbd = new Fare();
                                    //            tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                    //            tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                    //            tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                    //            tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                    //            tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                    //            tbofbdList.Add(tbofbd);
                                    //        }
                                    //        sr.FareBreakdown = fbdList.ToArray();
                                    //        sr.TBOFareBreakdown = tbofbdList.ToArray();

                                    //        XmlNodeList fareRuleList = result.SelectNodes("FareRules");
                                    //        sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                    //        foreach (XmlNode fare in fareRuleList)
                                    //        {
                                    //            CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                    //            fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                    //            fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                    //            fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                    //            fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                    //            fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                    //            fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                    //            sr.FareRules.Add(fr);
                                    //        }
                                    //        XmlNodeList segmentNodes = result.SelectNodes("Segments");
                                    //        if (request.Type == SearchType.Return)
                                    //        {
                                    //            sr.Flights = new FlightInfo[2][];
                                    //        }
                                    //        else if (request.Type == SearchType.MultiWay)
                                    //        {
                                    //            sr.Flights = new FlightInfo[segmentNodes.Count][];
                                    //        }
                                    //        else
                                    //        {
                                    //            sr.Flights = new FlightInfo[1][];
                                    //        }


                                    //        if (request.Type != SearchType.MultiWay)
                                    //        {
                                    //            BindSegments(segmentNodes, ref sr, result);
                                    //        }
                                    //        else //MultiCity 
                                    //        {
                                    //            //int tripIndicator = 1;

                                    //            BindSegments(segmentNodes, ref sr, result);
                                    //        }
                                    //        sr.IsLCC = Convert.ToBoolean(result.SelectSingleNode("IsLCC").InnerText);
                                    //        sr.NonRefundable = (Convert.ToBoolean(result.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                    //        sr.ResultBookingSource = BookingSource.TBOAir;
                                    //        sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                    //        sr.ValidatingAirline = result.SelectSingleNode("ValidatingAirline").InnerText;
                                    //        sr.Price = new PriceAccounts();
                                    //        sr.Price.AccPriceType = PriceType.PublishedFare;
                                    //        sr.Price.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                    //        sr.Price.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                    //        sr.Price.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                    //        sr.Price.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                    //        sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                    //        XmlNodeList chargeNodes = result.SelectNodes("Fare/ChargeBU");
                                    //        foreach (XmlNode cb in chargeNodes)
                                    //        {
                                    //            ChargeBreakUp cbu = new ChargeBreakUp();
                                    //            cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                    //            switch (cb.SelectSingleNode("key").InnerText)
                                    //            {
                                    //                case "TBOMARKUP":
                                    //                    cbu.ChargeType = ChargeType.TBOMarkup;
                                    //                    break;
                                    //                case "OTHERCHARGES":
                                    //                    cbu.ChargeType = ChargeType.OtherCharges;
                                    //                    break;
                                    //                case "CONVENIENCECHARGES":
                                    //                    cbu.ChargeType = ChargeType.ConvenienceCharge;
                                    //                    break;
                                    //            }
                                    //        }
                                    //        sr.Price.Currency = agentBaseCurrency;
                                    //        sr.Price.CurrencyCode = agentBaseCurrency;
                                    //        sr.Price.DecimalPoint = decimalValue;
                                    //        sr.Price.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                    //        sr.Price.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                    //        sr.Price.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                    //        sr.Price.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                    //        sr.Price.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                    //        sr.Price.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                    //        sr.Price.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                    //        sr.Price.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                    //        sr.Price.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                    //        sr.Price.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                    //        sr.Price.SupplierPrice = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                    //        if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                    //        {
                                    //            sr.Price.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                    //        }
                                    //        //sr.Price.Tax += sr.Price.OtherCharges + sr.Price.TransactionFee;

                                    //        sr.TBOPrice = new PriceAccounts();
                                    //        sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                    //        sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                    //        sr.TBOPrice.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                    //        sr.TBOPrice.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                    //        sr.TBOPrice.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                    //        sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                    //        XmlNodeList chargeNodes2 = result.SelectNodes("Fare/ChargeBU");
                                    //        foreach (XmlNode cb in chargeNodes2)
                                    //        {
                                    //            ChargeBreakUp cbu = new ChargeBreakUp();
                                    //            cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                    //            switch (cb.SelectSingleNode("key").InnerText)
                                    //            {
                                    //                case "TBOMARKUP":
                                    //                    cbu.ChargeType = ChargeType.TBOMarkup;
                                    //                    break;
                                    //                case "OTHERCHARGES":
                                    //                    cbu.ChargeType = ChargeType.OtherCharges;
                                    //                    break;
                                    //                case "CONVENIENCECHARGES":
                                    //                    cbu.ChargeType = ChargeType.ConvenienceCharge;
                                    //                    break;
                                    //            }
                                    //        }
                                    //        sr.TBOPrice.Currency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                    //        sr.TBOPrice.CurrencyCode = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                    //        sr.TBOPrice.DecimalPoint = decimalValue;
                                    //        sr.TBOPrice.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                    //        sr.TBOPrice.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                    //        sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                    //        sr.TBOPrice.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                    //        sr.TBOPrice.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                    //        sr.TBOPrice.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                    //        sr.TBOPrice.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                    //        sr.TBOPrice.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                    //        sr.TBOPrice.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                    //        sr.TBOPrice.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                    //        if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                    //        {
                                    //            sr.TBOPrice.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                    //        }

                                    //        sr.BaseFare = (double)sr.Price.BaseFare;
                                    //        sr.Tax = (double)(Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                    //        if (!showPubFaresForCorp)
                                    //        {
                                    //            if (sr.BaseFare > offeredFareAdjustment)
                                    //            {
                                    //                sr.BaseFare -= offeredFareAdjustment;
                                    //            }
                                    //            else if (sr.Tax > offeredFareAdjustment)
                                    //            {
                                    //                sr.Tax -= offeredFareAdjustment;
                                    //            }
                                    //        }
                                    //        sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;
                                    //        results.Add(sr);
                                    //    }
                                    //}
                                    //#endregion
                                }
                            }
                            SearchResult[] finalResult = results.ToArray();
                            if (finalResult.Length > 0)
                            {
                                Array.Sort<SearchResult>(finalResult, finalResult[0]);
                            }
                            CombineResults.AddRange(results);
                        }

                    }

                    eventFlag[(int)eventNumber].Set();
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                }
                // Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Normal Return Ended" + DateTime.Now, "");
            }
            catch (Exception ex)
            {
                eventFlag[(int)eventNumber].Set();
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Search. Error : " + ex.ToString(), "");
            }

        }

        /// <summary>
        /// To fetch special return fares for GDS. Results will come as combinations. 
        /// Displaying fares with breakdown.
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchSpecialReturnGDS(object eventNumber)
        {
            //Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Special Return  GDS Started" + DateTime.Now, "");
            string tokenId = string.Empty;
            SearchRequest request = new SearchRequest();
            request = Request;
            List<SearchResult> results = new List<SearchResult>();
            try
            {                
                if (ar != null)
                {
                    if (string.IsNullOrEmpty(ar.Error.ErrorMessage))
                    {
                        if (!string.IsNullOrEmpty(ar.TokenId))
                            tokenId = ar.TokenId;
                        else
                        {
                            Authenticate();
                            tokenId = ar.TokenId;
                        }
                    }
                    else
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir. Reason: " + ar.Error.ErrorMessage, "");
                        throw new Exception(ar.Error.ErrorMessage);
                    }
                    //AgencyBalance balance = GetAgencyBalance(ar);
                    if (tokenId.Length > 0)
                    {
                        #region JSON Search Request
                        int type = 5;//Return

                        TBOSearchRequest searchRequest = new TBOSearchRequest();
                        searchRequest.EndUserIp = endUserIp;
                        searchRequest.TokenId = tokenId;
                        searchRequest.AdultCount = request.AdultCount;
                        searchRequest.ChildCount = request.ChildCount;
                        searchRequest.InfantCount = request.InfantCount;
                        switch (request.MaxStops)
                        {
                            case "-1":
                                searchRequest.DirectFlight = false;
                                searchRequest.OneStopFlight = false;
                                break;
                            case "0":
                                searchRequest.DirectFlight = true;
                                searchRequest.OneStopFlight = false;
                                break;
                            case "1":
                                searchRequest.DirectFlight = false;
                                searchRequest.OneStopFlight = true;
                                break;
                        }
                        searchRequest.JourneyType = type;
                        searchRequest.PreferredAirlines = //!string.IsNullOrEmpty(PrivateFareCodes) ? PrivateFareCodes.Split(',') :
                                (request.Segments[0].PreferredAirlines.Length > 0 ? request.Segments[0].PreferredAirlines : null);

                        List<Segment> Segments = new List<Segment>();
                        foreach (FlightSegment segment in request.Segments)
                        {
                            if (segment.Origin != null)
                            {
                                Segment Seg = new Segment();
                                Seg.Origin = segment.Origin;
                                Seg.Destination = segment.Destination;
                                Seg.FlightCabinClass = segment.flightCabinClass;
                                Seg.PreferredDepartureTime = segment.PreferredDepartureTime.ToString("yyyy-MM-ddT00:00:00");//Pass default time 
                                Seg.PreferredArrivalTime = segment.PreferredArrivalTime.ToString("yyyy-MM-ddT00:00:00");//Pass default time 
                                Segments.Add(Seg);
                            }
                        }
                        searchRequest.Segments = Segments.ToArray();
                        searchRequest.Sources = new string[] { "GDS" };

                        //StringBuilder requestData = new StringBuilder();
                        //requestData.AppendLine("{");
                        //requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        //requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        //requestData.AppendLine("\"AdultCount\": \"" + request.AdultCount + "\",");
                        //requestData.AppendLine("\"ChildCount\": \"" + request.ChildCount + "\",");
                        //requestData.AppendLine("\"InfantCount\": \"" + request.InfantCount + "\",");
                        //if (request.MaxStops == "-1")
                        //{
                        //    requestData.AppendLine("\"DirectFlight\": \"false\",");
                        //    requestData.AppendLine("\"OneStopFlight\": \"false\",");
                        //}
                        //else if (request.MaxStops == "0")
                        //{
                        //    requestData.AppendLine("\"DirectFlight\": \"true\",");
                        //    requestData.AppendLine("\"OneStopFlight\": \"false\",");
                        //}
                        //else if (request.MaxStops == "1")
                        //{
                        //    requestData.AppendLine("\"DirectFlight\": \"false\",");
                        //    requestData.AppendLine("\"OneStopFlight\": \"true\",");
                        //}
                        //else
                        //{
                        //    requestData.AppendLine("\"DirectFlight\": \"false\",");
                        //    requestData.AppendLine("\"OneStopFlight\": \"false\",");
                        //}
                        //requestData.AppendLine("\"JourneyType\": \"" + type + "\",");
                        //if (request.Segments[0].PreferredAirlines.Length == 0)
                        //{
                        //    requestData.AppendLine("\"PreferredAirlines\": null,");
                        //}
                        //else
                        //{
                        //    requestData.AppendLine("\"PreferredAirlines\": [");
                        //    //requestData.AppendLine("{");
                        //    string pa = string.Empty;
                        //    foreach (string airline in request.Segments[0].PreferredAirlines)
                        //    {
                        //        if (pa.Length > 0)
                        //        {
                        //            pa += "," + "\"" + airline + "\"";
                        //        }
                        //        else
                        //        {
                        //            pa = "\"" + airline + "\"";
                        //        }
                        //    }
                        //    requestData.AppendLine(pa);
                        //    //requestData.AppendLine("}");
                        //    requestData.AppendLine("],");
                        //}
                        //requestData.AppendLine("\"Segments\": [");

                        //foreach (CT.BookingEngine.FlightSegment segment in request.Segments)
                        //{
                        //    if (segment.Origin != null)
                        //    {
                        //        requestData.AppendLine("{");
                        //        requestData.AppendLine("\"Origin\": \"" + segment.Origin + "\",");
                        //        requestData.AppendLine("\"Destination\": \"" + segment.Destination + "\",");
                        //        requestData.AppendLine("\"FlightCabinClass\": \"" + (int)segment.flightCabinClass + "\",");
                        //        requestData.AppendLine("\"PreferredDepartureTime\": \"" + segment.PreferredDepartureTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\",");
                        //        requestData.AppendLine("\"PreferredArrivalTime\": \"" + segment.PreferredArrivalTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\"");
                        //        if (segment == request.Segments[request.Segments.Length - 1])
                        //        {
                        //            requestData.AppendLine("}");
                        //        }
                        //        else
                        //        {
                        //            requestData.AppendLine("},");
                        //        }
                        //    }
                        //}
                        //requestData.AppendLine("],");
                        //requestData.AppendLine("\"Sources\": [");

                        ////Passing only GDS Sources.
                        //requestData.AppendLine("\"GDS\"");

                        //requestData.AppendLine("]");
                        //requestData.AppendLine("}");
                        #endregion
                        string rsp = string.Empty;
                        //Response resp = null;
                        XmlDocument doc = null;
                        try
                        {

                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchRequestJSONRTNGDS.json";
                            //JavaScriptSerializer js = new JavaScriptSerializer();
                            //dynamic searchRequest = js.Deserialize<dynamic>(requestData.ToString());
                            rsp = JsonConvert.SerializeObject(searchRequest, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(rsp.ToString(), "SearchRequest");
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchRequestRTNGDS.xml";
                            doc.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save SpecialReturnGDS Request. Error" + ex.ToString(), "");
                        }
                        doc = null;
                        //Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "Special Return GDS Request Started", "");
                        string response = GetResponse(searchRequest.ToJSON(), searchUrl);
                        //Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "Special Return GDS Got Response", "");
                        try
                        {

                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchResponseJSONRTNGDS.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic searchResponse = js.Deserialize<dynamic>(response);
                            rsp = JsonConvert.SerializeObject(searchResponse, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            TextReader reader = new StringReader(response);
                            doc = JsonConvert.DeserializeXmlNode(response);

                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirSearchResponseRTNGDS.xml";
                            doc.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save SpecialReturnGDS Response. Error" + ex.ToString(), "");
                        }
                        if (doc != null)
                        {
                            XmlNode Error = doc.SelectSingleNode("Response/Error");
                            string errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                            if (errorMessage.Length <= 0)
                            {
                                XmlNodeList ResultNodes = doc.SelectNodes("Response/Results/Results");

                                if (exchangeRates.ContainsKey(ResultNodes[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText))
                                    rateOfExchange = exchangeRates[ResultNodes[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];

                                #region GDS and SpecialReturn Results
                                foreach (XmlNode result in ResultNodes)
                                {
                                    if (!ValidatePrefAirline(PrivateFareCodes, request.Segments[0].PreferredAirlines, 
                                        result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Airline/AirlineCode").InnerText))
                                        continue;

                                    Airline airline = new Airline();
                                    airline.Load(result.SelectSingleNode("ValidatingAirline").InnerText);

                                    if (airline.TBOAirAllowed)
                                    {
                                        SearchResult sr = new SearchResult();
                                        if (type == 5)
                                        {
                                            sr.IsLCC = true;
                                            sr.FareType = "LCCSpecialReturn";
                                        }
                                        else
                                        {
                                            sr.FareType = "Normal (GDS)";
                                        }
                                        sr.Currency = agentBaseCurrency;
                                        sr.JourneySellKey = result.SelectSingleNode("Source").InnerText;
                                        

                                        sr.GUID = result.SelectSingleNode("ResultIndex").InnerText;
                                        double TransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * (double)rateOfExchange : 0;
                                        double TBOTransactionFee = result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) : 0;
                                        double TBOotherCharges = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);

                                        //Read CommissionEarned, PLBEarned & IncentiveEarned and deduct it from Adult fare
                                        double offeredFareAdjustment = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                        //Deduct TDS Charged on incentives and discount the remaining amount
                                        offeredFareAdjustment -= (Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText));
                                        offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                        int fareCounter = 0;
                                        double OtherCharges = Convert.ToDouble(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * (double)rateOfExchange;
                                        XmlNodeList fareBreakDownNodes = result.SelectNodes("FareBreakdown");
                                        List<Fare> fbdList = new List<Fare>();
                                        List<Fare> tbofbdList = new List<Fare>();

                                        foreach (XmlNode fbdNode in fareBreakDownNodes)
                                        {
                                            Fare fbd = new Fare();
                                            fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                            fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                            //if(fareCounter == 0 && !showPubFaresForCorp)
                                            //{
                                            //    if(fbd.BaseFare > offeredFareAdjustment)
                                            //    {
                                            //        fbd.BaseFare -= offeredFareAdjustment;
                                            //    }                                           
                                            //}
                                            fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                            fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                            fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                            if (fareCounter == 0 && !showPubFaresForCorp)//Only for Adult
                                            {
                                                //if Adj amount is greater than base fare then deduct it from total amount
                                                if (fbd.TotalFare > offeredFareAdjustment)
                                                {
                                                    fbd.TotalFare -= offeredFareAdjustment;
                                                }
                                            }
                                            fbd.SellingFare = fbd.TotalFare;
                                            fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);

                                            fbdList.Add(fbd);
                                            fareCounter++;

                                            Fare tbofbd = new Fare();
                                            tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                            tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                            tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                            tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                            tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                            tbofbdList.Add(tbofbd);

                                        }
                                        sr.FareBreakdown = fbdList.ToArray();
                                        sr.TBOFareBreakdown = tbofbdList.ToArray();

                                        XmlNodeList fareRuleList = result.SelectNodes("FareRules");
                                        sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                        foreach (XmlNode fare in fareRuleList)
                                        {
                                            CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                            fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                            fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                            fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                            fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                            fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                            fr.Origin = fare.SelectSingleNode("Origin").InnerText;

                                            sr.FareRules.Add(fr);
                                        }

                                        if (request.Type == SearchType.Return)
                                        {
                                            sr.Flights = new FlightInfo[2][];
                                        }
                                        else
                                        {
                                            sr.Flights = new FlightInfo[1][];
                                        }
                                        XmlNodeList segmentNodes = result.SelectNodes("Segments");

                                        BindSegments(segmentNodes, ref sr, result);


                                        sr.IsLCC = Convert.ToBoolean(result.SelectSingleNode("IsLCC").InnerText);
                                        sr.NonRefundable = (Convert.ToBoolean(result.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                        sr.ResultBookingSource = BookingSource.TBOAir;
                                        sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                        sr.ValidatingAirline = result.SelectSingleNode("ValidatingAirline").InnerText;

                                        sr.Price = new PriceAccounts();
                                        sr.Price.AccPriceType = PriceType.PublishedFare;
                                        sr.Price.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                        sr.Price.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                        sr.Price.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                        sr.Price.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                        sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                        XmlNodeList chargeNodes = result.SelectNodes("Fare/ChargeBU");
                                        foreach (XmlNode cb in chargeNodes)
                                        {
                                            ChargeBreakUp cbu = new ChargeBreakUp();
                                            cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                            switch (cb.SelectSingleNode("key").InnerText)
                                            {
                                                case "TBOMARKUP":
                                                    cbu.ChargeType = ChargeType.TBOMarkup;
                                                    break;
                                                case "OTHERCHARGES":
                                                    cbu.ChargeType = ChargeType.OtherCharges;
                                                    break;
                                                case "CONVENIENCECHARGES":
                                                    cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                    break;
                                            }
                                        }
                                        sr.Price.Currency = agentBaseCurrency;
                                        sr.Price.CurrencyCode = agentBaseCurrency;
                                        sr.Price.DecimalPoint = decimalValue;
                                        sr.Price.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                        sr.Price.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                        sr.Price.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                        //sr.Price.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                        //sr.Price.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                        sr.Price.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                        sr.Price.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                        sr.Price.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                        sr.Price.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                        sr.Price.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                        sr.Price.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                        sr.Price.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                        if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                        {
                                            sr.Price.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                        }
                                        //sr.Price.Tax += sr.Price.OtherCharges + sr.Price.TransactionFee;
                                        sr.Price.SupplierPrice = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                        sr.Price.SupplierCurrency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;

                                        XmlNodeList taxNodes = result.SelectSingleNode("Fare").SelectNodes("TaxBreakup");

                                        if (taxNodes != null)
                                        {
                                            sr.Price.TaxBreakups = new List<TaxBreakup>();
                                            foreach (XmlNode taxNode in taxNodes)
                                            {
                                                if (taxNode.SelectSingleNode("key").InnerText != "OtherTaxes")
                                                {
                                                    TaxBreakup tax = new TaxBreakup();
                                                    tax.TaxCode = taxNode.SelectSingleNode("key").InnerText;
                                                    tax.TaxValue = Convert.ToDecimal(taxNode.SelectSingleNode("value").InnerText) * rateOfExchange; ;
                                                    if (tax.TaxCode == "K3")
                                                    {
                                                        sr.Price.K3Tax = tax.TaxValue;
                                                    }
                                                    sr.Price.TaxBreakups.Add(tax);
                                                }
                                            }
                                        }

                                        sr.TBOPrice = new PriceAccounts();
                                        sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                        sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                        sr.TBOPrice.AgentCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                        sr.TBOPrice.AgentPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                        sr.TBOPrice.BaseFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                        sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                        XmlNodeList chargeNodes2 = result.SelectNodes("Fare/ChargeBU");
                                        foreach (XmlNode cb in chargeNodes2)
                                        {
                                            ChargeBreakUp cbu = new ChargeBreakUp();
                                            cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                            switch (cb.SelectSingleNode("key").InnerText)
                                            {
                                                case "TBOMARKUP":
                                                    cbu.ChargeType = ChargeType.TBOMarkup;
                                                    break;
                                                case "OTHERCHARGES":
                                                    cbu.ChargeType = ChargeType.OtherCharges;
                                                    break;
                                                case "CONVENIENCECHARGES":
                                                    cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                    break;
                                            }
                                        }
                                        sr.TBOPrice.Currency = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                        sr.TBOPrice.CurrencyCode = result.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                        sr.TBOPrice.DecimalPoint = decimalValue;
                                        sr.TBOPrice.Tax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                        sr.TBOPrice.YQTax = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                        sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                        sr.TBOPrice.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                        sr.TBOPrice.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                        sr.TBOPrice.SServiceFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                        sr.TBOPrice.OtherCharges = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                        sr.TBOPrice.TdsCommission = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                        sr.TBOPrice.TDSPLB = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                        sr.TBOPrice.TDSIncentive = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                        if (result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                        {
                                            sr.TBOPrice.TransactionFee = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                        }
                                        sr.FareType = "SpecialReturnGDS";
                                        sr.BaseFare = (double)sr.Price.BaseFare;
                                        sr.Tax = (double)(Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                        if (!showPubFaresForCorp)
                                        {
                                            if (sr.BaseFare > offeredFareAdjustment)
                                            {
                                                sr.BaseFare -= offeredFareAdjustment;
                                            }
                                            else if (sr.BaseFare < offeredFareAdjustment && sr.Tax > offeredFareAdjustment)
                                            {
                                                sr.Tax -= offeredFareAdjustment;
                                            }
                                        }
                                        sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;

                                        results.Add(sr);
                                    }
                                }
                                #endregion
                                SearchResult[] finalResult = results.ToArray();
                                if (finalResult.Length > 0)
                                {
                                    Array.Sort<SearchResult>(finalResult, finalResult[0]);
                                }
                                CombineResults.AddRange(results);
                            }
                        }
                    }
                    
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                }
                eventFlag[(int)eventNumber].Set();
                // Audit.Add(EventType.TBOAir, Severity.Normal, 1003, "(TBO Air) Special Return GDS Ended" + DateTime.Now, "");
            }
            catch (Exception ex)
            {
                eventFlag[(int)eventNumber].Set();
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Search. Error : " + ex.ToString(), "");
            }
        }

        
        /// <summary>
        /// This method will do Routing search for Return. This method will be called two times one for Onward segment and two for Return segment.
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchRoutingReturn(object eventNumber)
        {
            List<SearchResult> results = new List<SearchResult>();
            try
            {
                string tokenId = string.Empty;
                SearchRequest request = searchRequests[Convert.ToInt32(eventNumber)];
                
                if (ar != null)
                {
                    if (string.IsNullOrEmpty(ar.Error.ErrorMessage))
                    {
                        if (!string.IsNullOrEmpty(ar.TokenId))
                            tokenId = ar.TokenId;
                        else
                        {
                            Authenticate();
                            tokenId = ar.TokenId;
                        }
                    }
                    else
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir. Reason: " + ar.Error.ErrorMessage, "");
                        throw new Exception(ar.Error.ErrorMessage);
                    }

                    if (tokenId.Length > 0)
                    {
                        #region Search Routine
                        #region JSON Search Request
                        int type = 1;//OneWay                       

                        TBOSearchRequest searchRequest = new TBOSearchRequest();
                        searchRequest.EndUserIp = endUserIp;
                        searchRequest.TokenId = tokenId;
                        searchRequest.AdultCount = request.AdultCount;
                        searchRequest.ChildCount = request.ChildCount;
                        searchRequest.InfantCount = request.InfantCount;
                        switch (request.MaxStops)
                        {
                            case "-1":
                                searchRequest.DirectFlight = false;
                                searchRequest.OneStopFlight = false;
                                break;
                            case "0":
                                searchRequest.DirectFlight = true;
                                searchRequest.OneStopFlight = false;
                                break;
                            case "1":
                                searchRequest.DirectFlight = false;
                                searchRequest.OneStopFlight = true;
                                break;
                        }
                        searchRequest.JourneyType = type;
                        searchRequest.PreferredAirlines = //!string.IsNullOrEmpty(PrivateFareCodes) ? PrivateFareCodes.Split(',') :
                                (request.Segments[0].PreferredAirlines.Length > 0 ? request.Segments[0].PreferredAirlines : null);
                        /*                     
                        SG - SpiceJet
                       6E - Indigo
                       G8 – Go Air
                       G9 – Air Arabia
                       FZ – Fly Dubai
                       IX – Air India Express
                       AK – Air Asia
                       LB – Air Costa
                        * */
                        string allowedLCCSources = "SG,6E,G8,G9,FZ,IX,AK,LB";

                        List<Segment> Segments = new List<Segment>();
                        foreach (FlightSegment segment in request.Segments)
                        {
                            if (segment.Origin != null)
                            {
                                Segment Seg = new Segment();
                                Seg.Origin = segment.Origin;
                                Seg.Destination = segment.Destination;
                                Seg.FlightCabinClass = segment.flightCabinClass;
                                Seg.PreferredDepartureTime = segment.PreferredDepartureTime.ToString("yyyy-MM-ddT00:00:00");//Pass default time 
                                Seg.PreferredArrivalTime = segment.PreferredArrivalTime.ToString("yyyy-MM-ddT00:00:00");//Pass default time 
                                Segments.Add(Seg);
                            }
                        }
                        searchRequest.Segments = Segments.ToArray();
                        if (request.Segments[0].PreferredAirlines.Length > 0)
                        {
                            List<string> PreferredAirlines = new List<string>(request.Segments[0].PreferredAirlines);
                            List<string> AllowedLCCSources = new List<string>();

                            PreferredAirlines.ToList().ForEach(a => { if (allowedLCCSources.ToLower().Contains(a.ToLower())) AllowedLCCSources.Add(a.ToUpper()); });

                            //If Preferred Airlines are passed and no LCC airlines then pass GDS as source
                            //Else if LCC airlines are passed and those are allowed by TBOAir then pass LCC airlines as sources otherwise pass null
                            searchRequest.Sources = ((request.Segments[0].PreferredAirlines.Length > 0 && AllowedLCCSources.Count == 0) ? new string[] { "GDS" } : (AllowedLCCSources.Count > 0 ? AllowedLCCSources.ToArray() : null));
                        }
                        else
                        {
                            searchRequest.Sources = null;
                        }


                        #region Old Request Code
                        //StringBuilder requestData = new StringBuilder();
                        //requestData.AppendLine("{");
                        //requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        //requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        //requestData.AppendLine("\"AdultCount\": \"" + request.AdultCount + "\",");
                        //requestData.AppendLine("\"ChildCount\": \"" + request.ChildCount + "\",");
                        //requestData.AppendLine("\"InfantCount\": \"" + request.InfantCount + "\",");
                        //requestData.AppendLine("\"DirectFlight\": \"false\",");
                        //requestData.AppendLine("\"OneStopFlight\": \"false\",");
                        //requestData.AppendLine("\"JourneyType\": \"" + type + "\",");
                        //if (request.Segments[0].PreferredAirlines.Length == 0)
                        //{
                        //    requestData.AppendLine("\"PreferredAirlines\": null,");
                        //}
                        //else
                        //{
                        //    requestData.AppendLine("\"PreferredAirlines\": [");
                        //    //requestData.AppendLine("{");
                        //    string pa = string.Empty;
                        //    foreach (string airline in request.Segments[0].PreferredAirlines)
                        //    {
                        //        if (pa.Length > 0)
                        //        {
                        //            pa += "," + "\"" + airline + "\"";
                        //        }
                        //        else
                        //        {
                        //            pa = "\"" + airline + "\"";
                        //        }
                        //    }
                        //    requestData.AppendLine(pa);
                        //    //requestData.AppendLine("}");
                        //    requestData.AppendLine("],");
                        //}
                        //requestData.AppendLine("\"Segments\": [");

                        //foreach (FlightSegment segment in request.Segments)
                        //{
                        //    if (segment.Origin != null)
                        //    {
                        //        requestData.AppendLine("{");
                        //        requestData.AppendLine("\"Origin\": \"" + segment.Origin + "\",");
                        //        requestData.AppendLine("\"Destination\": \"" + segment.Destination + "\",");
                        //        requestData.AppendLine("\"FlightCabinClass\": \"" + (int)segment.flightCabinClass + "\",");
                        //        requestData.AppendLine("\"PreferredDepartureTime\": \"" + segment.PreferredDepartureTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\",");
                        //        requestData.AppendLine("\"PreferredArrivalTime\": \"" + segment.PreferredArrivalTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\",");
                        //        requestData.AppendLine("}");
                        //    }
                        //}
                        //requestData.AppendLine("],");
                        ////requestData.AppendLine("\"Sources\": [");
                        ////Sources are optional for Normal one-way, so all airlines will be returned
                        ////if we do not specify any sources
                        //if (type < 5)  // Normal one way, International one way ,round trip
                        //{
                        //    //requestData.AppendLine("\"6E\",");
                        //    //requestData.AppendLine("\"G8\",");
                        //    //requestData.AppendLine("\"SG\",");
                        //    //requestData.AppendLine("\"G9\",");
                        //    //requestData.AppendLine("\"GDS\"");
                        //}
                        //else
                        //{
                        //    //requestData.AppendLine("\"SG\",");
                        //    //requestData.AppendLine("\"6E\",");
                        //    //requestData.AppendLine("\"G8\",");
                        //    //requestData.AppendLine("\"GDS\"");

                        //}
                        ////If preferred airline is mentioned and restrict airline is true then hit for only gds
                        //if (request.RestrictAirline = true && request.Segments[0].PreferredAirlines.Length > 0) 
                        //{
                        //    requestData.AppendLine("\"Sources\": [");
                        //    requestData.AppendLine("\"GDS\"");
                        //    requestData.AppendLine("]");
                        //}
                        ////requestData.AppendLine("]");
                        //requestData.AppendLine("}"); 
                        #endregion
                        #endregion

                        string rsp = string.Empty;
                        //Response resp = null;
                        XmlDocument doc = null;
                        try
                        {

                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirRouting(" + request.Segments[0].Origin + "_" + request.Segments[0].Destination + ")SearchRequest.json";
                            //JavaScriptSerializer js = new JavaScriptSerializer();
                            //dynamic searchRequest = js.Deserialize<dynamic>(requestData.ToString());
                            rsp = JsonConvert.SerializeObject(searchRequest, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(rsp, "SearchRequest");
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirRouting(" + request.Segments[0].Origin + "_" + request.Segments[0].Destination + ")SearchRequest.xml";
                            doc.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save Routing Search Request. Error" + ex.ToString(), "");
                        }

                        doc = null;

                        string response = GetResponse(searchRequest.ToJSON(), searchUrl);

                        try
                        {
                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirRouting(" + request.Segments[0].Origin + "_" + request.Segments[0].Destination + ")SearchResponse.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic searchResponse = js.Deserialize<dynamic>(response);
                            rsp = JsonConvert.SerializeObject(searchResponse, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            TextReader reader = new StringReader(response);
                            doc = JsonConvert.DeserializeXmlNode(response);

                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirRouting(" + request.Segments[0].Origin + "_" + request.Segments[0].Destination + ")SearchResponse.xml";
                            doc.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save Search Response.Error" + ex.ToString(), "");
                        }

                        if (doc != null)
                        {
                            XmlNode Error = doc.SelectSingleNode("Response/Error");
                            string errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                            if (errorMessage.Length <= 0)
                            {
                                XmlNodeList ResultNodes = doc.SelectNodes("Response/Results/Results");

                                if ((request.IsDomestic))
                                {
                                    List<XmlNode> outboundResults = new List<XmlNode>();


                                    foreach (XmlNode result in ResultNodes)
                                    {
                                        if (!ValidatePrefAirline(PrivateFareCodes, request.Segments[0].PreferredAirlines, 
                                            result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Airline/AirlineCode").InnerText))
                                            continue;

                                        if (result.SelectNodes("Segments/Segments")[0].SelectSingleNode("Origin/Airport/AirportCode").InnerText == request.Segments[0].Origin)
                                        {
                                            outboundResults.Add(result);
                                        }

                                    }
                                    if (exchangeRates != null && exchangeRates.Count > 0 && exchangeRates.ContainsKey(outboundResults[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText))
                                    {
                                        rateOfExchange = exchangeRates[outboundResults[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];
                                    }
                                    else
                                    {
                                        rateOfExchange = 1;
                                    }

                                    #region Domestic Results

                                    foreach (XmlNode obResultNode in outboundResults)
                                    {
                                        //foreach (XmlNode ibResult in inboundResults)
                                        {
                                            Airline airlineOut = new Airline();
                                            airlineOut.Load(obResultNode.SelectSingleNode("ValidatingAirline").InnerText);


                                            if (airlineOut.TBOAirAllowed)
                                            {
                                                SearchResult sr = new SearchResult();

                                                sr.Currency = agentBaseCurrency;
                                                //if (obResult.SelectSingleNode("BaggageAllowance") != null)
                                                //{
                                                //    sr.BaggageIncludedInFare = obResult.SelectSingleNode("BaggageAllowance").InnerText;
                                                //}
                                                sr.FareType = (Convert.ToBoolean(obResultNode.SelectSingleNode("IsLCC").InnerText) ? "Normal (LCC)" : "Normal (GDS)");
                                                sr.ResultBookingSource = BookingSource.TBOAir;
                                                //=====================================================Outbound begin===============================================//
                                                #region Outbound Result
                                                sr.GUID = obResultNode.SelectSingleNode("ResultIndex").InnerText;
                                                sr.JourneySellKey = obResultNode.SelectSingleNode("Source").InnerText;
                                                XmlNodeList fareBreakDownNodes = obResultNode.SelectNodes("FareBreakdown");
                                                //Read CommissionEarned, PLBEarned & IncentiveEarned and deduct it from Adult fare
                                                double offeredFareAdjustment = Convert.ToDouble(obResultNode.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText) + Convert.ToDouble(obResultNode.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(obResultNode.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                //Deduct TDS Charged on incentives and discount the remaining amount
                                                offeredFareAdjustment -= (Convert.ToDouble(obResultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(obResultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(obResultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText));
                                                offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                                int fareCounter = 0;
                                                List<Fare> fbdList = new List<Fare>();
                                                List<Fare> tbofbdList = new List<Fare>();
                                                foreach (XmlNode fbdNode in fareBreakDownNodes)
                                                {
                                                    Fare fbd = new Fare();
                                                    fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                    fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                    //if (fareCounter == 0 && !showPubFaresForCorp)
                                                    //{
                                                    //    if (fbd.BaseFare > offeredFareAdjustment)
                                                    //    {
                                                    //        fbd.BaseFare -= offeredFareAdjustment;
                                                    //    }
                                                    //}
                                                    fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                    fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                    fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                    if (fareCounter == 0 && !showPubFaresForCorp)
                                                    {
                                                        if (fbd.TotalFare > offeredFareAdjustment)
                                                        {
                                                            fbd.TotalFare -= offeredFareAdjustment;
                                                        }
                                                    }
                                                    fbd.SellingFare = fbd.TotalFare;
                                                    fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);
                                                    fbdList.Add(fbd);
                                                    fareCounter++;

                                                    Fare tbofbd = new Fare();
                                                    tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                    tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                    tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                    tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                    tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                    tbofbdList.Add(tbofbd);

                                                }
                                                sr.FareBreakdown = fbdList.ToArray();
                                                sr.TBOFareBreakdown = tbofbdList.ToArray();

                                                XmlNodeList fareRuleList = obResultNode.SelectNodes("FareRules");
                                                sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                                foreach (XmlNode fare in fareRuleList)
                                                {
                                                    CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                    fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                    fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                    fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                    fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                    fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                    fr.Origin = fare.SelectSingleNode("Origin").InnerText;
                                                    fr.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                                    fr.ReturnDate = request.Segments[0].PreferredDepartureTime;

                                                    sr.FareRules.Add(fr);
                                                }

                                                sr.Flights = new FlightInfo[1][];

                                                XmlNodeList segmentNodes = obResultNode.SelectNodes("Segments");
                                                sr.IsLCC = Convert.ToBoolean(obResultNode.SelectSingleNode("IsLCC").InnerText);
                                                sr.NonRefundable = (Convert.ToBoolean(obResultNode.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                                sr.ResultBookingSource = BookingSource.TBOAir;
                                                sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                                sr.ValidatingAirline = obResultNode.SelectSingleNode("ValidatingAirline").InnerText;

                                                BindSegments(segmentNodes, ref sr, obResultNode, null);

                                                SearchResult res = sr;
                                                sr.Flights[0].AsQueryable().ToList().ForEach(f => f.Stops = res.Flights[0].Length - 1);
                                                if (request.Type == SearchType.Return)
                                                    sr.Flights[0].AsQueryable().ToList().ForEach(f => f.Group = 1);
                                                else
                                                    sr.Flights[0].AsQueryable().ToList().ForEach(f => f.Group = 0);

                                                sr.Price = new PriceAccounts();
                                                sr.Price.AccPriceType = PriceType.PublishedFare;
                                                sr.Price.AdditionalTxnFee = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                sr.Price.AgentCommission = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                sr.Price.AgentPLB = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                sr.Price.BaseFare = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                                sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                                XmlNodeList chargeNodes = obResultNode.SelectNodes("Fare/ChargeBU");
                                                foreach (XmlNode cb in chargeNodes)
                                                {
                                                    ChargeBreakUp cbu = new ChargeBreakUp();
                                                    cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                    switch (cb.SelectSingleNode("key").InnerText)
                                                    {
                                                        case "TBOMARKUP":
                                                            cbu.ChargeType = ChargeType.TBOMarkup;
                                                            break;
                                                        case "OTHERCHARGES":
                                                            cbu.ChargeType = ChargeType.OtherCharges;
                                                            break;
                                                        case "CONVENIENCECHARGES":
                                                            cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                            break;
                                                    }
                                                }
                                                sr.Price.Currency = agentBaseCurrency;
                                                sr.Price.CurrencyCode = agentBaseCurrency;
                                                sr.Price.DecimalPoint = decimalValue;
                                                sr.Price.Tax = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                                sr.Price.YQTax = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                                sr.Price.IncentiveEarned = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                sr.Price.PublishedFare = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                                sr.Price.NetFare = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                                //sr.Price.NetFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                                //sr.Price.PublishedFare = Convert.ToDecimal(obResult.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText) * rateOfExchange;
                                                sr.Price.SServiceFee = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                                sr.Price.OtherCharges = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                                sr.Price.TdsCommission = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                sr.Price.TDSPLB = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                sr.Price.TDSIncentive = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                if (obResultNode.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                {
                                                    sr.Price.TransactionFee = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                }
                                                else
                                                {
                                                    sr.Price.TransactionFee = 0.0M;
                                                }

                                                sr.Price.SupplierPrice = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                sr.Price.SupplierCurrency = obResultNode.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;

                                                XmlNodeList taxNodes = obResultNode.SelectSingleNode("Fare").SelectNodes("TaxBreakup");

                                                if (taxNodes != null)
                                                {
                                                    sr.Price.TaxBreakups = new List<TaxBreakup>();
                                                    foreach (XmlNode taxNode in taxNodes)
                                                    {
                                                        if (taxNode.SelectSingleNode("key").InnerText != "OtherTaxes")
                                                        {
                                                            TaxBreakup tax = new TaxBreakup();
                                                            tax.TaxCode = taxNode.SelectSingleNode("key").InnerText;
                                                            tax.TaxValue = Convert.ToDecimal(taxNode.SelectSingleNode("value").InnerText) * rateOfExchange; ;
                                                            if(tax.TaxCode=="K3")
                                                            {
                                                                sr.Price.K3Tax = tax.TaxValue;
                                                            }
                                                            sr.Price.TaxBreakups.Add(tax);
                                                        }
                                                    }
                                                }

                                                sr.TBOPrice = new PriceAccounts();
                                                sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                                sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                sr.TBOPrice.AgentCommission = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                                sr.TBOPrice.AgentPLB = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                                sr.TBOPrice.BaseFare = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                                sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                                XmlNodeList chargeNodes2 = obResultNode.SelectNodes("Fare/ChargeBU");
                                                foreach (XmlNode cb in chargeNodes2)
                                                {
                                                    ChargeBreakUp cbu = new ChargeBreakUp();
                                                    cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                    switch (cb.SelectSingleNode("key").InnerText)
                                                    {
                                                        case "TBOMARKUP":
                                                            cbu.ChargeType = ChargeType.TBOMarkup;
                                                            break;
                                                        case "OTHERCHARGES":
                                                            cbu.ChargeType = ChargeType.OtherCharges;
                                                            break;
                                                        case "CONVENIENCECHARGES":
                                                            cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                            break;
                                                    }
                                                }
                                                sr.TBOPrice.Currency = obResultNode.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                sr.TBOPrice.CurrencyCode = obResultNode.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                sr.TBOPrice.DecimalPoint = decimalValue;
                                                sr.TBOPrice.Tax = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                                sr.TBOPrice.YQTax = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                                sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                sr.TBOPrice.NetFare = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                                sr.TBOPrice.PublishedFare = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                                sr.TBOPrice.SServiceFee = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                sr.TBOPrice.OtherCharges = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                sr.TBOPrice.TdsCommission = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                sr.TBOPrice.TDSPLB = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                sr.TBOPrice.TDSIncentive = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                if (obResultNode.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                                {
                                                    sr.TBOPrice.TransactionFee = Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                                }

                                                sr.BaseFare = (double)sr.Price.BaseFare;
                                                sr.Tax = (double)(Convert.ToDecimal(obResultNode.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                if (!showPubFaresForCorp)
                                                {
                                                    if (sr.BaseFare > offeredFareAdjustment)
                                                    {
                                                        sr.BaseFare -= offeredFareAdjustment;
                                                    }
                                                    else if (sr.BaseFare < offeredFareAdjustment && sr.Tax > offeredFareAdjustment)
                                                    {
                                                        sr.Tax -= offeredFareAdjustment;
                                                    }
                                                }
                                                sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;
                                                #endregion
                                                //======================================================Outbound end================================================//

                                                results.Add(sr);
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    bool isAllowed = true;
                                    #region GDS and SpecialReturn Results

                                    if (exchangeRates.ContainsKey(ResultNodes[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText))
                                        rateOfExchange = exchangeRates[ResultNodes[0].SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText];

                                    foreach (XmlNode resultNode in ResultNodes)
                                    {
                                        isAllowed = true;
                                        XmlNodeList segmentNodes = resultNode.SelectNodes("Segments");

                                        if (!ValidatePrefAirline(PrivateFareCodes, request.Segments[0].PreferredAirlines, 
                                            resultNode.SelectNodes("Segments/Segments")[0].SelectSingleNode("Airline/AirlineCode").InnerText))
                                            continue;

                                        foreach (XmlNode segment in segmentNodes)
                                        {
                                            XmlNodeList segments = segment.SelectNodes("Segments");

                                            foreach (XmlNode seg in segments)
                                            {
                                                Airline segAirline = new Airline();
                                                segAirline.Load(seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText);

                                                if (!segAirline.TBOAirAllowed)
                                                {
                                                    isAllowed = false;
                                                    break;
                                                }
                                            }
                                            if (!isAllowed)
                                                break;
                                        }

                                        Airline airline = new Airline();
                                        airline.Load(resultNode.SelectSingleNode("ValidatingAirline").InnerText);

                                        if (airline.TBOAirAllowed && isAllowed)
                                        {
                                            SearchResult sr = new SearchResult();
                                            //if (type == 5)
                                            //{
                                            //    sr.IsLCC = true;
                                            //    sr.FareType = "LCCSpecialReturn";
                                            //}
                                            //else
                                            //{
                                            //    sr.FareType = "Normal (GDS)";
                                            //}
                                            sr.FareType = (Convert.ToBoolean(resultNode.SelectSingleNode("IsLCC").InnerText) ? "Normal (LCC)" : "Normal (GDS)");
                                            //if (result.SelectSingleNode("BaggageAllowance") != null)
                                            //{
                                            //    sr.BaggageIncludedInFare = result.SelectSingleNode("BaggageAllowance").InnerText;
                                            //}
                                            sr.Currency = agentBaseCurrency;
                                            

                                            sr.GUID = resultNode.SelectSingleNode("ResultIndex").InnerText;
                                            sr.JourneySellKey = resultNode.SelectSingleNode("Source").InnerText;
                                            XmlNodeList fareBreakDownNodes = resultNode.SelectNodes("FareBreakdown");
                                            double TransactionFee = resultNode.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(resultNode.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * (double)rateOfExchange : 0;
                                            double TBOTransactionFee = resultNode.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null ? Convert.ToDouble(resultNode.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) : 0;
                                            double TBOotherCharges = Convert.ToDouble(resultNode.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);

                                            double OtherCharges = (request.IsDomestic) ? 0 : Convert.ToDouble(resultNode.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * (double)rateOfExchange;

                                            //Read CommissionEarned, PLBEarned & IncentiveEarned and deduct it from Adult fare
                                            double offeredFareAdjustment = Convert.ToDouble(resultNode.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText) + Convert.ToDouble(resultNode.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(resultNode.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            //Deduct TDS Charged on incentives and discount the remaining amount
                                            offeredFareAdjustment -= (Convert.ToDouble(resultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(resultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(resultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText));
                                            offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                            int fareCounter = 0;

                                            List<Fare> fbdList = new List<Fare>();
                                            List<Fare> tbofbdList = new List<Fare>();
                                            foreach (XmlNode fbdNode in fareBreakDownNodes)
                                            {
                                                Fare fbd = new Fare();
                                                fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                                fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                                //if (fareCounter == 0 && !showPubFaresForCorp)
                                                //{
                                                //    if (fbd.BaseFare > offeredFareAdjustment)
                                                //    {
                                                //        fbd.BaseFare -= offeredFareAdjustment;
                                                //    }
                                                //}
                                                fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                                if (fareCounter == 0 && !showPubFaresForCorp)
                                                {
                                                    if (fbd.TotalFare > offeredFareAdjustment)
                                                    {
                                                        fbd.TotalFare -= offeredFareAdjustment;
                                                    }
                                                }
                                                fbd.SellingFare = fbd.TotalFare;
                                                fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText);

                                                fbdList.Add(fbd);
                                                fareCounter++;

                                                Fare tbofbd = new Fare();
                                                tbofbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                                tbofbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                                tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                                tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                                tbofbd.TotalFare = tbofbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                                tbofbdList.Add(tbofbd);

                                            }
                                            sr.FareBreakdown = fbdList.ToArray();
                                            sr.TBOFareBreakdown = tbofbdList.ToArray();

                                            XmlNodeList fareRuleList = resultNode.SelectNodes("FareRules");
                                            sr.FareRules = new List<CT.BookingEngine.FareRule>();
                                            foreach (XmlNode fare in fareRuleList)
                                            {
                                                CT.BookingEngine.FareRule fr = new CT.BookingEngine.FareRule();
                                                fr.Airline = fare.SelectSingleNode("Airline").InnerText;
                                                fr.Destination = fare.SelectSingleNode("Destination").InnerText;
                                                fr.FareBasisCode = fare.SelectSingleNode("FareBasisCode").InnerText;
                                                fr.FareRestriction = fare.SelectSingleNode("FareRestriction").InnerText;
                                                fr.FareRuleDetail = fare.SelectSingleNode("FareRuleDetail").InnerText;
                                                fr.Origin = fare.SelectSingleNode("Origin").InnerText;
                                                fr.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                                fr.ReturnDate = request.Segments[0].PreferredDepartureTime;

                                                sr.FareRules.Add(fr);
                                            }

                                            sr.Flights = new FlightInfo[1][];

                                            if (request.Type != SearchType.MultiWay)
                                            {
                                                foreach (XmlNode segment in segmentNodes)
                                                {
                                                    XmlNodeList segments = segment.SelectNodes("Segments");
                                                    int tripIndicator = 0, segmentIndicator = 0;
                                                    tripIndicator = Convert.ToInt32(segments[0].SelectSingleNode("TripIndicator").InnerText);
                                                    sr.Flights[tripIndicator - 1] = new FlightInfo[segments.Count];
                                                    foreach (XmlNode seg in segments)
                                                    {
                                                        //Read default baggage from search
                                                        if (seg.SelectSingleNode("Baggage") != null)
                                                        {
                                                            if (string.IsNullOrEmpty(sr.BaggageIncludedInFare))
                                                            {
                                                                sr.BaggageIncludedInFare = seg.SelectSingleNode("Baggage").InnerText;
                                                            }
                                                            else
                                                            {
                                                                sr.BaggageIncludedInFare += "," + seg.SelectSingleNode("Baggage").InnerText;
                                                            }
                                                        }
                                                        segmentIndicator = Convert.ToInt32(seg.SelectSingleNode("SegmentIndicator").InnerText);
                                                        tripIndicator = Convert.ToInt32(seg.SelectSingleNode("TripIndicator").InnerText);

                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1] = new FlightInfo();
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Airline = seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrivalTime = Convert.ToDateTime(seg.SelectSingleNode("Destination").SelectSingleNode("ArrTime").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrTerminal = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].BookingClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                        //sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                        //if (request.Segments[0].flightCabinClass == CabinClass.Business)
                                                        //{
                                                        //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Business.ToString();
                                                        //}
                                                        //else if (request.Segments[0].flightCabinClass == CabinClass.First)
                                                        //{
                                                        //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.First.ToString();
                                                        //}
                                                        //else if (request.Segments[0].flightCabinClass == CabinClass.Economy)
                                                        //{
                                                        //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Economy.ToString();
                                                        //}
                                                        //else
                                                        //{
                                                        //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                        //}                                                    
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = string.Empty;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].CreatedOn = DateTime.Now;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].LastModifiedOn = DateTime.Now;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Craft = seg.SelectSingleNode("Craft").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepartureTime = Convert.ToDateTime(seg.SelectSingleNode("Origin").SelectSingleNode("DepTime").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepTerminal = seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                                        //Reading Airport code directly from DB
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination = new CT.BookingEngine.Airport(seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = new TimeSpan(0, Convert.ToInt32(seg.SelectSingleNode("Duration").InnerText), 0);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].ETicketEligible = Convert.ToBoolean(seg.SelectSingleNode("IsETicketEligible").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightNumber = seg.SelectSingleNode("Airline").SelectSingleNode("FlightNumber").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightStatus = FlightStatus.Confirmed;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].GroundTime = new TimeSpan(Convert.ToInt32(seg.SelectSingleNode("GroundTime").InnerText));
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Group = (request.Type == SearchType.OneWay ? 0 : 1);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Mile = Convert.ToInt32(seg.SelectSingleNode("Mile").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].OperatingCarrier = seg.SelectSingleNode("Airline").SelectSingleNode("OperatingCarrier").InnerText;
                                                        //Reading Airport code directly from DB
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin = new CT.BookingEngine.Airport(seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Status = seg.SelectSingleNode("Status").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].StopOver = Convert.ToBoolean(seg.SelectSingleNode("StopOver").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentFareType = resultNode.SelectSingleNode("AirlineRemark").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice = new PriceAccounts();
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.Currency = resultNode.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.CurrencyCode = resultNode.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.DecimalPoint = decimalValue;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.YQTax = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.IncentiveEarned = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.NetFare = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.PublishedFare = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.SServiceFee = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.OtherCharges = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TdsCommission = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSPLB = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentPrice.TDSIncentive = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                                    }
                                                }

                                                //TODO: Bind stops information
                                                SearchResult res = sr;
                                                sr.Flights[0].AsQueryable().ToList().ForEach(f => f.Stops = res.Flights[0].Length - 1);
                                            }
                                            else //MultiCity 
                                            {

                                                int tripIndicator = 1;
                                                foreach (XmlNode segment in segmentNodes)
                                                {
                                                    XmlNodeList segments = segment.SelectNodes("Segments");
                                                    int segmentIndicator = 0;
                                                    //tripIndicator = Convert.ToInt32(segments[0].SelectSingleNode("TripIndicator").InnerText);
                                                    sr.Flights[tripIndicator - 1] = new FlightInfo[segments.Count];
                                                    foreach (XmlNode seg in segments)
                                                    {
                                                        //Read default baggage from search
                                                        if (seg.SelectSingleNode("Baggage") != null)
                                                        {
                                                            if (string.IsNullOrEmpty(sr.BaggageIncludedInFare))
                                                            {
                                                                sr.BaggageIncludedInFare = seg.SelectSingleNode("Baggage").InnerText;
                                                            }
                                                            else
                                                            {
                                                                sr.BaggageIncludedInFare += "," + seg.SelectSingleNode("Baggage").InnerText;
                                                            }
                                                        }
                                                        segmentIndicator = Convert.ToInt32(seg.SelectSingleNode("SegmentIndicator").InnerText);
                                                        //tripIndicator = Convert.ToInt32(seg.SelectSingleNode("TripIndicator").InnerText);

                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1] = new FlightInfo();
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Airline = seg.SelectSingleNode("Airline").SelectSingleNode("AirlineCode").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrivalTime = Convert.ToDateTime(seg.SelectSingleNode("Destination").SelectSingleNode("ArrTime").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].ArrTerminal = seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].BookingClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                        //sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                        //if (Request.Segments[0].flightCabinClass == CabinClass.Business)
                                                        //{
                                                        //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Business.ToString();
                                                        //}
                                                        //else if (Request.Segments[0].flightCabinClass == CabinClass.First)
                                                        //{
                                                        //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.First.ToString();
                                                        //}
                                                        //else if (Request.Segments[0].flightCabinClass == CabinClass.Economy)
                                                        //{
                                                        //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = CabinClass.Economy.ToString();
                                                        //}
                                                        //else
                                                        //{
                                                        //    sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = seg.SelectSingleNode("Airline").SelectSingleNode("FareClass").InnerText;
                                                        //}
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].CabinClass = string.Empty;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Craft = seg.SelectSingleNode("Craft").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepartureTime = Convert.ToDateTime(seg.SelectSingleNode("Origin").SelectSingleNode("DepTime").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].DepTerminal = seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("Terminal").InnerText;
                                                        //Reading Airport code directly from DB
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Destination = new CT.BookingEngine.Airport(seg.SelectSingleNode("Destination").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Duration = new TimeSpan(0, Convert.ToInt32(seg.SelectSingleNode("Duration").InnerText), 0);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].ETicketEligible = Convert.ToBoolean(seg.SelectSingleNode("IsETicketEligible").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightNumber = seg.SelectSingleNode("Airline").SelectSingleNode("FlightNumber").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].FlightStatus = FlightStatus.Confirmed;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].GroundTime = new TimeSpan(Convert.ToInt32(seg.SelectSingleNode("GroundTime").InnerText));
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Group = tripIndicator - 1;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Mile = Convert.ToInt32(seg.SelectSingleNode("Mile").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].OperatingCarrier = seg.SelectSingleNode("Airline").SelectSingleNode("OperatingCarrier").InnerText;
                                                        //Reading Airport code directly from DB
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Origin = new CT.BookingEngine.Airport(seg.SelectSingleNode("Origin").SelectSingleNode("Airport").SelectSingleNode("AirportCode").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Status = seg.SelectSingleNode("Status").InnerText;
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].StopOver = Convert.ToBoolean(seg.SelectSingleNode("StopOver").InnerText);
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].Stops = 1;//Added by Shiva
                                                        sr.Flights[tripIndicator - 1][segmentIndicator - 1].SegmentFareType = resultNode.SelectSingleNode("AirlineRemark").InnerText;

                                                    }
                                                    tripIndicator++;
                                                }
                                            }

                                            sr.IsLCC = Convert.ToBoolean(resultNode.SelectSingleNode("IsLCC").InnerText);
                                            sr.NonRefundable = (Convert.ToBoolean(resultNode.SelectSingleNode("IsRefundable").InnerText) ? false : true);
                                            sr.ResultBookingSource = BookingSource.TBOAir;
                                            sr.FareSellKey = doc.SelectSingleNode("Response/TraceId").InnerText;
                                            sr.ValidatingAirline = resultNode.SelectSingleNode("ValidatingAirline").InnerText;

                                            sr.Price = new PriceAccounts();
                                            sr.Price.AccPriceType = PriceType.PublishedFare;
                                            sr.Price.AdditionalTxnFee = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                            sr.Price.AgentCommission = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.Price.AgentPLB = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.Price.BaseFare = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                            sr.Price.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes = resultNode.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.Price.Currency = agentBaseCurrency;
                                            sr.Price.CurrencyCode = agentBaseCurrency;
                                            sr.Price.DecimalPoint = decimalValue;
                                            sr.Price.Tax = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange;
                                            sr.Price.AgentCommission = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.Price.YQTax = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText) * rateOfExchange;
                                            sr.Price.IncentiveEarned = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.Price.PublishedFare = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                            sr.Price.NetFare = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                            //sr.Price.NetFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) * rateOfExchange;
                                            //sr.Price.PublishedFare = Convert.ToDecimal(result.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText) * rateOfExchange;
                                            sr.Price.SServiceFee = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                            sr.Price.OtherCharges = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                            sr.Price.TdsCommission = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.Price.TDSPLB = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.Price.TDSIncentive = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            if (resultNode.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.Price.TransactionFee = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                            }
                                            sr.Price.SupplierPrice = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText) + Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                            sr.Price.SupplierCurrency = resultNode.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;

                                            XmlNodeList taxNodes = resultNode.SelectSingleNode("Fare").SelectNodes("TaxBreakup");

                                            if (taxNodes != null)
                                            {
                                                sr.Price.TaxBreakups = new List<TaxBreakup>();
                                                foreach (XmlNode taxNode in taxNodes)
                                                {
                                                    if (taxNode.SelectSingleNode("key").InnerText != "OtherTaxes")
                                                    {
                                                        TaxBreakup tax = new TaxBreakup();
                                                        tax.TaxCode = taxNode.SelectSingleNode("key").InnerText;
                                                        tax.TaxValue = Convert.ToDecimal(taxNode.SelectSingleNode("value").InnerText) * rateOfExchange; ;
                                                        if(tax.TaxCode=="K3")
                                                        {
                                                            sr.Price.K3Tax = tax.TaxValue;
                                                        }
                                                        sr.Price.TaxBreakups.Add(tax);
                                                    }
                                                }
                                            }

                                            sr.TBOPrice = new PriceAccounts();
                                            sr.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                            sr.TBOPrice.AdditionalTxnFee = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                            sr.TBOPrice.AgentCommission = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("CommissionEarned").InnerText);
                                            sr.TBOPrice.AgentPLB = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("PLBEarned").InnerText);
                                            sr.TBOPrice.BaseFare = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("BaseFare").InnerText);
                                            sr.TBOPrice.ChargeBU = new List<ChargeBreakUp>();
                                            XmlNodeList chargeNodes2 = resultNode.SelectNodes("Fare/ChargeBU");
                                            foreach (XmlNode cb in chargeNodes2)
                                            {
                                                ChargeBreakUp cbu = new ChargeBreakUp();
                                                cbu.Amount = Convert.ToDecimal(cb.SelectSingleNode("value").InnerText);
                                                switch (cb.SelectSingleNode("key").InnerText)
                                                {
                                                    case "TBOMARKUP":
                                                        cbu.ChargeType = ChargeType.TBOMarkup;
                                                        break;
                                                    case "OTHERCHARGES":
                                                        cbu.ChargeType = ChargeType.OtherCharges;
                                                        break;
                                                    case "CONVENIENCECHARGES":
                                                        cbu.ChargeType = ChargeType.ConvenienceCharge;
                                                        break;
                                                }
                                            }
                                            sr.TBOPrice.Currency = resultNode.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.CurrencyCode = resultNode.SelectSingleNode("Fare").SelectSingleNode("Currency").InnerText;
                                            sr.TBOPrice.DecimalPoint = decimalValue;
                                            sr.TBOPrice.Tax = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText);
                                            sr.TBOPrice.YQTax = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("YQTax").InnerText);
                                            sr.TBOPrice.IncentiveEarned = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("IncentiveEarned").InnerText);
                                            sr.TBOPrice.NetFare = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("PublishedFare").InnerText);
                                            sr.TBOPrice.PublishedFare = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("OfferedFare").InnerText);
                                            sr.TBOPrice.SServiceFee = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("ServiceFee").InnerText);
                                            sr.TBOPrice.OtherCharges = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("OtherCharges").InnerText);
                                            sr.TBOPrice.TdsCommission = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnCommission").InnerText);
                                            sr.TBOPrice.TDSPLB = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnPLB").InnerText);
                                            sr.TBOPrice.TDSIncentive = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("TdsOnIncentive").InnerText);
                                            if (resultNode.SelectSingleNode("Fare").SelectSingleNode("TransactionFee") != null)
                                            {
                                                sr.TBOPrice.TransactionFee = Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("TransactionFee").InnerText);
                                            }

                                            sr.BaseFare = (double)sr.Price.BaseFare;
                                            sr.Tax = (double)(Convert.ToDecimal(resultNode.SelectSingleNode("Fare").SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                            if (!showPubFaresForCorp)
                                            {
                                                if (sr.BaseFare > offeredFareAdjustment)
                                                {
                                                    sr.BaseFare -= offeredFareAdjustment;
                                                }
                                                else if (sr.BaseFare < offeredFareAdjustment && sr.Tax > offeredFareAdjustment)
                                                {
                                                    sr.Tax -= offeredFareAdjustment;
                                                }
                                            }
                                            sr.TotalFare = sr.BaseFare + sr.Tax + (double)sr.Price.OtherCharges + (double)sr.Price.TransactionFee + (double)sr.Price.AdditionalTxnFee + (double)sr.Price.SServiceFee;

                                            if (isAllowed)
                                            {
                                                results.Add(sr);
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }
                        CombineResults.AddRange(results);
                        eventFlag[(int)eventNumber].Set();
                        #endregion
                    }
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Search. Error : " + ex.ToString(), "");
                eventFlag[(int)eventNumber].Set();
            }
        }
        /// <summary>
        /// To get Farerules
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public List<FareRule> GetFareRule(SearchResult result)
        {
            List<FareRule> fareRuleList = new List<FareRule>();

            Authenticate();            

            try
            {  if (ar != null)
                {
                    if (ar.Error.ErrorMessage == null || ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                    {
                        if (result.GUID.Split(',').Length <= 1)
                        {
                            StringBuilder requestData = new StringBuilder();
                            requestData.AppendLine("{");
                            requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                            requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                            requestData.AppendLine("\"TraceId\": \"" + result.FareSellKey + "\",");
                            requestData.AppendLine("\"ResultIndex\": \"" + result.GUID + "\"");
                            requestData.AppendLine("}");

                            try
                            {

                                string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetFareRuleRequestJSON.json";
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                dynamic fareRuleRequest = js.Deserialize<dynamic>(requestData.ToString());
                                string rsp = JsonConvert.SerializeObject(fareRuleRequest, Newtonsoft.Json.Formatting.Indented);
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(rsp);
                                sw.Close();

                                XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "FareRuleRequest");
                                filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetRuleRequest.xml";
                                doc1.Save(filePath);
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save GetFareRule Request. Error" + ex.ToString(), "");
                            }

                            string response = GetResponse(requestData.ToString(), fareRuleUrl);

                            XmlDocument doc = null;

                            try
                            {
                                string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetFareRuleResponseJSON.json";
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                dynamic fareRuleResponse = js.Deserialize<dynamic>(response);
                                string rsp = JsonConvert.SerializeObject(fareRuleResponse, Newtonsoft.Json.Formatting.Indented);
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(rsp);
                                sw.Close();

                                doc = JsonConvert.DeserializeXmlNode(response);
                                filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetRuleResponse.xml";
                                doc.Save(filePath);
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save GetFareRule Response. Error" + ex.ToString(), "");
                            }

                            if (doc != null)
                            {
                                XmlNode Error = doc.SelectSingleNode("Response/Error");
                                string errorMessage = "";
                                errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                                if (errorMessage.Length <= 0)
                                {
                                    XmlNodeList fareRuleNodes = doc.SelectNodes("Response/FareRules");

                                    foreach (XmlNode fareRule in fareRuleNodes)
                                    {
                                        FareRule fr = new FareRule();
                                        fr.Airline = fareRule.SelectSingleNode("Airline").InnerText;
                                        fr.Destination = fareRule.SelectSingleNode("Destination").InnerText;
                                        fr.FareBasisCode = fareRule.SelectSingleNode("FareBasisCode").InnerText;
                                        fr.FareRestriction = fareRule.SelectSingleNode("FareRestriction").InnerText;
                                        fr.FareRuleDetail = fareRule.SelectSingleNode("FareRuleDetail").InnerText;
                                        fr.Origin = fareRule.SelectSingleNode("Origin").InnerText;

                                        fareRuleList.Add(fr);
                                    }
                                }
                            }
                        }
                        else
                        {
                            string[] guids = result.GUID.Split(',');

                            foreach (string guid in guids)
                            {
                                StringBuilder requestData = new StringBuilder();
                                requestData.AppendLine("{");
                                requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                                requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                                requestData.AppendLine("\"TraceId\": \"" + result.FareSellKey + "\",");
                                requestData.AppendLine("\"ResultIndex\": \"" + guid + "\",");
                                requestData.AppendLine("}");

                                try
                                {

                                    string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetFareRuleRequestJSON.json";
                                    JavaScriptSerializer js = new JavaScriptSerializer();
                                    dynamic fareRuleRequest = js.Deserialize<dynamic>(requestData.ToString());
                                    string rsp = JsonConvert.SerializeObject(fareRuleRequest, Newtonsoft.Json.Formatting.Indented);
                                    StreamWriter sw = new StreamWriter(filePath);
                                    sw.Write(rsp);
                                    sw.Close();

                                    XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "FareRuleRequest");
                                    filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetRuleRequest.xml";
                                    doc1.Save(filePath);
                                }
                                catch (Exception ex)
                                {
                                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save GetFareRule Request. Error" + ex.ToString(), "");
                                }

                                string response = GetResponse(requestData.ToString(), fareRuleUrl);

                                XmlDocument doc = null;

                                try
                                {

                                    string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetFareRuleResponseJSON.json";
                                    JavaScriptSerializer js = new JavaScriptSerializer();
                                    dynamic fareRuleResponse = js.Deserialize<dynamic>(response);
                                    string rsp = JsonConvert.SerializeObject(fareRuleResponse, Newtonsoft.Json.Formatting.Indented);
                                    StreamWriter sw = new StreamWriter(filePath);
                                    sw.Write(rsp);
                                    sw.Close();

                                    doc = JsonConvert.DeserializeXmlNode(response);
                                    filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetRuleResponse.xml";
                                    doc.Save(filePath);
                                }
                                catch (Exception ex)
                                {
                                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save GetFareRule Response. Error" + ex.ToString(), "");
                                }

                                if (doc != null)
                                {
                                    XmlNode Error = doc.SelectSingleNode("Response/Error");
                                    string errorMessage = "";
                                    errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                                    if (errorMessage.Length <= 0)
                                    {
                                        XmlNodeList fareRuleNodes = doc.SelectNodes("Response/FareRules");

                                        foreach (XmlNode fareRule in fareRuleNodes)
                                        {
                                            FareRule fr = new FareRule();
                                            fr.Airline = fareRule.SelectSingleNode("Airline").InnerText;
                                            fr.Destination = fareRule.SelectSingleNode("Destination").InnerText;
                                            fr.FareBasisCode = fareRule.SelectSingleNode("FareBasisCode").InnerText;
                                            fr.FareRestriction = fareRule.SelectSingleNode("FareRestriction").InnerText;
                                            fr.FareRuleDetail = fareRule.SelectSingleNode("FareRuleDetail").InnerText;
                                            fr.Origin = fareRule.SelectSingleNode("Origin").InnerText;

                                            fareRuleList.Add(fr);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to get Fare Rule. Error : " + ex.ToString(), "");
                throw ex;
            }

            return fareRuleList;
        }

        /// <summary>
        /// Method for checking for PriceChange or TimeChange
        /// </summary>
        /// <param name="result"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public Fare[] GetFareQuote(ref SearchResult result, ref string errorMessage)
        {
            List<Fare> fareList = new List<Fare>();
            List<Fare> tboFares = new List<Fare>();
            XmlDocument doc = null;
            //Sending to fare Quote requests for Combined results. and merging the responses.
            try
            {
                if (result.GUID.Contains(',') && result.FareType.ToLower() == "normalreturn") // Normal return combined result set
                {
                    string[] Guids = result.GUID.Split(','); //Search Result indecies.                                

                    PriceAccounts Price = new PriceAccounts();
                    PriceAccounts TBOPrice = new PriceAccounts();
                    int priceChanged = 0;
                    for (int i = 0; i < Guids.Length; i++)
                    {
                        doc = TempGetFareQuote(Guids[i], result.FareSellKey);

                        if (doc != null)
                        {
                            XmlNode Error = doc.SelectSingleNode("Response/Error");

                            if (Error.SelectSingleNode("ErrorCode").InnerText == "0")
                            {
                                XmlNodeList fareNodes = doc.SelectNodes("Response/Results/FareBreakdown");
                                XmlNodeList fares = doc.SelectNodes("Response/Results/Fare");

                                if (exchangeRates.ContainsKey(fareNodes[0].SelectSingleNode("Currency").InnerText))
                                    rateOfExchange = exchangeRates[fareNodes[0].SelectSingleNode("Currency").InnerText];

                                if (doc.SelectSingleNode("Response/IsPriceChanged").InnerText.ToLower() == "true")
                                {
                                    double offeredFareAdjustment = Convert.ToDouble(fares[0].SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(fares[0].SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(fares[0].SelectSingleNode("IncentiveEarned").InnerText);
                                    //Deduct TDS Charged on incentives and discount the remaining amount
                                    offeredFareAdjustment -= (Convert.ToDouble(fares[0].SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(fares[0].SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(fares[0].SelectSingleNode("TdsOnIncentive").InnerText));
                                    offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                    priceChanged++;
                                    int fareCounter = 0;
                                    foreach (XmlNode fbdNode in fareNodes)
                                    {
                                        bool used = false;
                                        Fare fbd = null;
                                        if (fareList.Count > 0)
                                        {
                                            fbd = fareList.Find(delegate (Fare f) { return Convert.ToInt32(f.PassengerType) == Convert.ToInt32(fbdNode.SelectSingleNode("PassengerType").InnerText); });

                                            if (fbd == null)
                                            {
                                                fbd = new Fare();
                                            }
                                            else
                                            {
                                                used = true;
                                            }
                                        }
                                        else
                                        {
                                            fbd = new Fare();
                                        }
                                        fbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                        fbd.BaseFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                        double baseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                        //if (fareCounter == 0 && !showPubFaresForCorp)
                                        //{
                                        //    if (fbd.BaseFare > offeredFareAdjustment)
                                        //    {
                                        //        fbd.BaseFare -= offeredFareAdjustment;
                                        //    }
                                        //    if (baseFare > offeredFareAdjustment)
                                        //    {
                                        //        baseFare -= offeredFareAdjustment;
                                        //    }
                                        //}
                                        fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                        fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                        fbd.TotalFare += baseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                        if (fareCounter == 0 && !showPubFaresForCorp)
                                        {
                                            if (fbd.TotalFare > offeredFareAdjustment)
                                            {
                                                fbd.TotalFare -= offeredFareAdjustment;
                                            }
                                        }
                                        fbd.SellingFare += fbd.TotalFare;
                                        fbd.SupplierFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + (double)(Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText));
                                        if (!used)
                                        {
                                            fareList.Add(fbd);
                                        }
                                        fareCounter++;
                                        Fare tbofbd = new Fare();
                                        if (tboFares.Count > 0)
                                        {
                                            tbofbd = tboFares.Find(delegate (Fare f) { return Convert.ToInt32(f.PassengerType) == Convert.ToInt32(fbdNode.SelectSingleNode("PassengerType").InnerText); });

                                            if (tbofbd == null)
                                            {
                                                tbofbd = new Fare();
                                            }
                                        }
                                        else
                                        {
                                            tbofbd = new Fare();
                                        }
                                        tbofbd.AdditionalTxnFee += Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                        tbofbd.BaseFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                        tbofbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                        tbofbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                        tbofbd.TotalFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                        tbofbd.SellingFare += tbofbd.TotalFare;
                                        tbofbd.SupplierFare += Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + (double)(Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText));
                                        if (!used)
                                        {
                                            tboFares.Add(tbofbd);
                                        }
                                    }
                                    result.TBOFareBreakdown = tboFares.ToArray();

                                    if (fares != null)
                                    {
                                        foreach (XmlNode fare in fares)
                                        {
                                            if (i == 1)
                                            {
                                                Price.OtherCharges += Convert.ToDecimal(fare.SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                                Price.TdsCommission += Convert.ToDecimal(fare.SelectSingleNode("TdsOnCommission").InnerText);
                                                Price.AgentCommission += Convert.ToDecimal(fare.SelectSingleNode("CommissionEarned").InnerText);
                                                Price.IncentiveEarned += Convert.ToDecimal(fare.SelectSingleNode("IncentiveEarned").InnerText);
                                                Price.TDSIncentive += Convert.ToDecimal(fare.SelectSingleNode("TdsOnIncentive").InnerText);
                                                Price.SServiceFee += Convert.ToDecimal(fare.SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                                Price.SupplierPrice += Convert.ToDecimal(fare.SelectSingleNode("PublishedFare").InnerText);
                                                Price.SupplierCurrency = fare.SelectSingleNode("Currency").InnerText;
                                                Price.YQTax += Convert.ToDecimal(fare.SelectSingleNode("YQTax").InnerText);
                                                Price.AgentPLB += Convert.ToDecimal(fare.SelectSingleNode("PLBEarned").InnerText);
                                                Price.TDSPLB += Convert.ToDecimal(fare.SelectSingleNode("TdsOnPLB").InnerText);
                                                if (fare.SelectSingleNode("TransactionFee") != null)
                                                {
                                                    Price.TransactionFee += Convert.ToDecimal(fare.SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                                }

                                                TBOPrice.OtherCharges += Convert.ToDecimal(fare.SelectSingleNode("OtherCharges").InnerText);
                                                TBOPrice.TdsCommission += Convert.ToDecimal(fare.SelectSingleNode("TdsOnCommission").InnerText);
                                                TBOPrice.AgentCommission += Convert.ToDecimal(fare.SelectSingleNode("CommissionEarned").InnerText);
                                                TBOPrice.IncentiveEarned += Convert.ToDecimal(fare.SelectSingleNode("IncentiveEarned").InnerText);
                                                TBOPrice.TDSIncentive += Convert.ToDecimal(fare.SelectSingleNode("TdsOnIncentive").InnerText);
                                                TBOPrice.SeviceTax += Convert.ToDecimal(fare.SelectSingleNode("ServiceFee").InnerText);
                                            }
                                            else
                                            {
                                                Price.OtherCharges = Convert.ToDecimal(fare.SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                                Price.TdsCommission = Convert.ToDecimal(fare.SelectSingleNode("TdsOnCommission").InnerText);
                                                Price.AgentCommission = Convert.ToDecimal(fare.SelectSingleNode("CommissionEarned").InnerText);
                                                Price.IncentiveEarned = Convert.ToDecimal(fare.SelectSingleNode("IncentiveEarned").InnerText);
                                                Price.TDSIncentive = Convert.ToDecimal(fare.SelectSingleNode("TdsOnIncentive").InnerText);
                                                Price.SServiceFee = Convert.ToDecimal(fare.SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                                Price.SupplierPrice = Convert.ToDecimal(fare.SelectSingleNode("PublishedFare").InnerText);
                                                Price.SupplierCurrency = fare.SelectSingleNode("Currency").InnerText;
                                                Price.YQTax = Convert.ToDecimal(fare.SelectSingleNode("YQTax").InnerText);
                                                Price.AgentPLB = Convert.ToDecimal(fare.SelectSingleNode("PLBEarned").InnerText);
                                                Price.TDSPLB = Convert.ToDecimal(fare.SelectSingleNode("TdsOnPLB").InnerText);
                                                if (fare.SelectSingleNode("TransactionFee") != null)
                                                {
                                                    Price.TransactionFee = Convert.ToDecimal(fare.SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                                }

                                                TBOPrice.PublishedFare = Convert.ToDecimal(fare.SelectSingleNode("OfferedFare").InnerText);
                                                TBOPrice.OtherCharges = Convert.ToDecimal(fare.SelectSingleNode("OtherCharges").InnerText);
                                                TBOPrice.TdsCommission = Convert.ToDecimal(fare.SelectSingleNode("TdsOnCommission").InnerText);
                                                TBOPrice.AgentCommission = Convert.ToDecimal(fare.SelectSingleNode("CommissionEarned").InnerText);
                                                TBOPrice.IncentiveEarned = Convert.ToDecimal(fare.SelectSingleNode("IncentiveEarned").InnerText);
                                                TBOPrice.TDSIncentive = Convert.ToDecimal(fare.SelectSingleNode("TdsOnIncentive").InnerText);
                                                TBOPrice.SeviceTax = Convert.ToDecimal(fare.SelectSingleNode("ServiceFee").InnerText);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    fareList = new List<Fare>();
                                }
                            }
                            else
                            {
                                fareList = new List<Fare>();
                                errorMessage = "Price has been revised from Supplier end. Please search again";
                                //When fareQuote failed from Supplier side. Throwing an exception .
                                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)" + errorMessage, "");
                                return fareList.ToArray();
                            }
                        }
                    }
                    if (priceChanged != Guids.Length)
                    {
                        fareList = new List<Fare>();
                    }
                    else
                    {
                        result.Price.OtherCharges = Price.OtherCharges;
                        result.Price.TdsCommission = Price.TdsCommission;
                        result.Price.AgentCommission = Price.AgentCommission;
                        result.Price.IncentiveEarned = Price.IncentiveEarned;
                        result.Price.TDSIncentive = Price.TDSIncentive;
                        result.Price.SServiceFee = Price.SServiceFee;
                        result.Price.SupplierPrice = Price.SupplierPrice;
                        result.Price.YQTax = Price.YQTax;
                        result.Price.AgentPLB = Price.AgentPLB;
                        result.Price.TDSPLB = Price.TDSPLB;
                        result.Price.TransactionFee = Price.TransactionFee;

                        result.TBOPrice.PublishedFare = TBOPrice.PublishedFare;
                        result.TBOPrice.OtherCharges = TBOPrice.OtherCharges;
                        result.TBOPrice.TdsCommission = TBOPrice.TdsCommission;
                        result.TBOPrice.AgentCommission = TBOPrice.AgentCommission;
                        result.TBOPrice.IncentiveEarned = TBOPrice.IncentiveEarned;
                        result.TBOPrice.TDSIncentive = TBOPrice.TDSIncentive;
                        result.TBOPrice.SeviceTax = TBOPrice.SeviceTax;
                    }
                }
                else //Special retuns and internation oneway, round trip
                {
                    doc = TempGetFareQuote(result.GUID, result.FareSellKey);
                    if (doc != null)
                    {
                        XmlNode Error = doc.SelectSingleNode("Response/Error");

                        errorMessage = Error.SelectSingleNode("ErrorMessage").InnerText;
                        if (errorMessage.Length <= 0)
                        {
                            //Check whether GST is mandatory or not. if true we need to pass GST fields while booking
                            XmlNode gstMandatory = doc.SelectSingleNode("Response/Results/IsGSTMandatory");
                            if (gstMandatory != null)
                            {
                                result.IsGSTMandatory = Convert.ToBoolean(gstMandatory.InnerText);
                            }

                            if (doc.SelectSingleNode("Response/IsPriceChanged").InnerText.ToLower() == "true")
                            {
                                XmlNodeList fareNodes = doc.SelectNodes("Response/Results/FareBreakdown");
                                XmlNodeList fares = doc.SelectNodes("Response/Results/Fare");
                                if (exchangeRates.ContainsKey(fareNodes[0].SelectSingleNode("Currency").InnerText))
                                    rateOfExchange = exchangeRates[fareNodes[0].SelectSingleNode("Currency").InnerText];

                                double offeredFareAdjustment = Convert.ToDouble(fares[0].SelectSingleNode("CommissionEarned").InnerText) + Convert.ToDouble(fares[0].SelectSingleNode("PLBEarned").InnerText) + Convert.ToDouble(fares[0].SelectSingleNode("IncentiveEarned").InnerText);
                                //Deduct TDS Charged on incentives and discount the remaining amount
                                offeredFareAdjustment -= (Convert.ToDouble(fares[0].SelectSingleNode("TdsOnCommission").InnerText) + Convert.ToDouble(fares[0].SelectSingleNode("TdsOnPLB").InnerText) + Convert.ToDouble(fares[0].SelectSingleNode("TdsOnIncentive").InnerText));
                                offeredFareAdjustment = offeredFareAdjustment * (double)rateOfExchange;
                                int fareCounter = 0;
                                foreach (XmlNode fbdNode in fareNodes)
                                {
                                    Fare fbd = new Fare();
                                    fbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText) * rateOfExchange;
                                    fbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) * (double)rateOfExchange;
                                    fbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);
                                    //if (fareCounter == 0 && !showPubFaresForCorp)
                                    //{
                                    //    if (fbd.BaseFare > offeredFareAdjustment)
                                    //    {
                                    //        fbd.BaseFare -= offeredFareAdjustment;
                                    //    }
                                    //}
                                    fbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                    fbd.TotalFare = fbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText) * rateOfExchange);
                                    if (fareCounter == 0 && !showPubFaresForCorp)
                                    {
                                        if (fbd.TotalFare > offeredFareAdjustment)
                                        {
                                            fbd.TotalFare -= offeredFareAdjustment;
                                        }
                                    }
                                    fbd.SellingFare = fbd.TotalFare;
                                    fbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + (double)(Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText));
                                    fareList.Add(fbd);
                                    fareCounter++;
                                    Fare tboFbd = new Fare();
                                    tboFbd.AdditionalTxnFee = Convert.ToDecimal(fbdNode.SelectSingleNode("AdditionalTxnFeePub").InnerText);
                                    tboFbd.BaseFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText);
                                    tboFbd.PassengerCount = Convert.ToInt32(fbdNode.SelectSingleNode("PassengerCount").InnerText);

                                    tboFbd.PassengerType = (PassengerType)Enum.Parse(typeof(PassengerType), fbdNode.SelectSingleNode("PassengerType").InnerText);
                                    tboFbd.TotalFare = tboFbd.BaseFare + (double)(Convert.ToDecimal(fbdNode.SelectSingleNode("Tax").InnerText));
                                    tboFbd.SellingFare = tboFbd.TotalFare;
                                    tboFbd.SupplierFare = Convert.ToDouble(fbdNode.SelectSingleNode("BaseFare").InnerText) + (double)(Convert.ToDouble(fbdNode.SelectSingleNode("Tax").InnerText));

                                    tboFares.Add(tboFbd);
                                }
                                result.TBOFareBreakdown = fareList.ToArray();

                                if (fares != null)
                                {
                                    foreach (XmlNode fare in fares)
                                    {
                                        result.Price.OtherCharges = Convert.ToDecimal(fare.SelectSingleNode("OtherCharges").InnerText) * rateOfExchange;
                                        result.Price.TdsCommission = Convert.ToDecimal(fare.SelectSingleNode("TdsOnCommission").InnerText);
                                        result.Price.AgentCommission = Convert.ToDecimal(fare.SelectSingleNode("CommissionEarned").InnerText);
                                        result.Price.IncentiveEarned = Convert.ToDecimal(fare.SelectSingleNode("IncentiveEarned").InnerText);
                                        result.Price.TDSIncentive = Convert.ToDecimal(fare.SelectSingleNode("TdsOnIncentive").InnerText);
                                        result.Price.SServiceFee = Convert.ToDecimal(fare.SelectSingleNode("ServiceFee").InnerText) * rateOfExchange;
                                        result.Price.SupplierPrice = Convert.ToDecimal(fare.SelectSingleNode("PublishedFare").InnerText);
                                        result.Price.SupplierCurrency = fare.SelectSingleNode("Currency").InnerText;
                                        result.Price.YQTax = Convert.ToDecimal(fare.SelectSingleNode("YQTax").InnerText);
                                        result.Price.AgentPLB = Convert.ToDecimal(fare.SelectSingleNode("PLBEarned").InnerText);
                                        result.Price.TDSPLB = Convert.ToDecimal(fare.SelectSingleNode("TdsOnPLB").InnerText);
                                        if (fare.SelectSingleNode("TransactionFee") != null)
                                        {
                                            result.Price.TransactionFee = Convert.ToDecimal(fare.SelectSingleNode("TransactionFee").InnerText) * rateOfExchange;
                                        }

                                        result.TBOPrice.PublishedFare = Convert.ToDecimal(fare.SelectSingleNode("OfferedFare").InnerText);
                                        result.TBOPrice.OtherCharges = Convert.ToDecimal(fare.SelectSingleNode("OtherCharges").InnerText);
                                        result.TBOPrice.TdsCommission = Convert.ToDecimal(fare.SelectSingleNode("TdsOnCommission").InnerText);
                                        result.TBOPrice.AgentCommission = Convert.ToDecimal(fare.SelectSingleNode("CommissionEarned").InnerText);
                                        result.TBOPrice.IncentiveEarned = Convert.ToDecimal(fare.SelectSingleNode("IncentiveEarned").InnerText);
                                        result.TBOPrice.TDSIncentive = Convert.ToDecimal(fare.SelectSingleNode("TdsOnIncentive").InnerText);
                                        result.TBOPrice.SeviceTax = Convert.ToDecimal(fare.SelectSingleNode("ServiceFee").InnerText);
                                    }

                                    XmlNodeList taxNodes = doc.SelectNodes("Response/Results/Fare/TaxBreakup");
                                    if (taxNodes != null)
                                    {                                        
                                        result.Price.TaxBreakups = new List<TaxBreakup>();

                                        foreach (XmlNode taxNode in taxNodes)
                                        {
                                            if (taxNode.SelectSingleNode("key").InnerText != "OtherTaxes")
                                            {
                                                if (result.Price.TaxBreakups.Exists(t => t.TaxCode == taxNode.SelectSingleNode("key").InnerText))
                                                {
                                                    TaxBreakup tax = result.Price.TaxBreakups.Find(t => t.TaxCode == taxNode.SelectSingleNode("key").InnerText);
                                                    tax.TaxValue = Convert.ToDecimal(taxNode.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                    if (tax.TaxCode == "K3")
                                                    {
                                                        result.Price.K3Tax = tax.TaxValue;
                                                    }
                                                }
                                                else
                                                {
                                                    TaxBreakup tax = new TaxBreakup();
                                                    tax.TaxCode = taxNode.SelectSingleNode("key").InnerText;
                                                    tax.TaxValue = Convert.ToDecimal(taxNode.SelectSingleNode("value").InnerText) * rateOfExchange;
                                                    if (tax.TaxCode == "K3")
                                                    {
                                                        result.Price.K3Tax = tax.TaxValue;
                                                    }
                                                    result.Price.TaxBreakups.Add(tax);
                                                }
                                            }
                                        }
                                    }
                                }
                                //Reading default baggage after Reprice
                                XmlNodeList segments = doc.SelectNodes("Response/Results/Segments/Segments");
                                result.BaggageIncludedInFare = string.Empty;
                                foreach (XmlNode seg in segments)
                                {
                                    if (seg.SelectSingleNode("Baggage") != null)
                                    {
                                        if (string.IsNullOrEmpty(result.BaggageIncludedInFare))
                                        {
                                            result.BaggageIncludedInFare = seg.SelectSingleNode("Baggage").InnerText;
                                        }
                                        else
                                        {
                                            result.BaggageIncludedInFare += "," + seg.SelectSingleNode("Baggage").InnerText;
                                        }
                                    }
                                }
                               
                            }
                            else
                            {
                                fareList = new List<Fare>();
                            }
                        }
                        else
                        {                            
                            //TWhen fareQuote failed from Supplier side. Throwing an exception .
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, errorMessage, "");                            
                            throw new Exception(errorMessage);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return fareList.ToArray();
        }
        /// <summary>
        /// To send request and get fare calculation response.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="traceId"></param>
        /// <returns></returns>
        private XmlDocument TempGetFareQuote(string guid, string traceId)
        {

            XmlDocument doc = null;
            Authenticate();
            if (ar != null)
            {
                if (ar.Error.ErrorMessage == null || ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                {
                    try
                    {
                        StringBuilder requestData = new StringBuilder();
                        requestData.AppendLine("{");
                        requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        requestData.AppendLine("\"TraceId\": \"" + traceId + "\",");
                        requestData.AppendLine("\"ResultIndex\": \"" + guid + "\"");
                        requestData.AppendLine("}");

                        try
                        {

                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetFareQuoteRequestJSON.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic fareQuoteRequest = js.Deserialize<dynamic>(requestData.ToString());
                            string rsp = JsonConvert.SerializeObject(fareQuoteRequest, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "FareQuoteRequest");
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetFareQuoteRequest.xml";
                            doc1.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save GetFareQuote Request. Error" + ex.ToString(), "");
                        }

                        string response = GetResponse(requestData.ToString(), fareQuoteUrl);



                        try
                        {

                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetFareQuoteResponseJSON.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic fareQuoteResponse = js.Deserialize<dynamic>(response);
                            string rsp = JsonConvert.SerializeObject(fareQuoteResponse, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(response);
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetFareQuoteResponse.xml";
                            doc.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save GetFareQuote Response. Error" + ex.ToString(), "");
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to get Fare Quote. Reason : " + ex.ToString(), "");
                    }
                }
            }
            else
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
            }

            return doc;
        }
        /// <summary>
        /// To get baggage code for LCC. Sending two baggage requests and merging response for Combined results.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public DataTable GetBaggage(ref SearchResult result)
        {
            XmlDocument doc = null;
            //Sending two baggage requests and merging response for Combined results.
            if (result.GUID.Contains(','))
            {
                string[] Guids = result.GUID.Split(',');
                int i = 0;
                foreach (string Guid in Guids)
                {
                    result.GUID = Guid;
                    doc = TempGetBaggage(result);

                    if (doc != null)
                    {
                        XmlNodeList BaggageNodes = doc.SelectNodes("Response/Baggage");

                        if (BaggageNodes != null)
                        {
                            foreach (XmlNode baggage in BaggageNodes)
                            {
                                XmlNodeList nodes = baggage.SelectNodes("Baggage");

                                if (nodes != null)
                                {
                                    if (exchangeRates.ContainsKey(nodes[0].SelectSingleNode("Currency").InnerText))
                                        rateOfExchange = exchangeRates[nodes[0].SelectSingleNode("Currency").InnerText];
                                    foreach (XmlNode node in nodes)
                                    {
                                        DataRow dr = dtBaggage.NewRow();
                                        dr["WayType"] = node.SelectSingleNode("WayType").InnerText;
                                        dr["Code"] = node.SelectSingleNode("Code").InnerText;
                                        dr["Description"] = node.SelectSingleNode("Description").InnerText;
                                        dr["Weight"] = Convert.ToInt32(node.SelectSingleNode("Weight").InnerText);
                                        dr["Price"] = Convert.ToDecimal(node.SelectSingleNode("Price").InnerText) * rateOfExchange;
                                        dr["SupplierPrice"] = Convert.ToDecimal(node.SelectSingleNode("Price").InnerText);
                                        dr["Currency"] = agentBaseCurrency;
                                        dr["Origin"] = node.SelectSingleNode("Origin").InnerText;
                                        dr["Destination"] = node.SelectSingleNode("Destination").InnerText;
                                        if (i == 0)
                                        {
                                            dr["Group"] = 0;
                                        }
                                        else if (i == 1)
                                        {
                                            dr["Group"] = 1;
                                        }
                                        dtBaggage.Rows.Add(dr);

                                    }
                                }
                                i++;
                            }
                        }
                    }
                }
                result.GUID = Guids[0] + "," + Guids[1];
            }
            else
            {
                doc = TempGetBaggage(result);
                if (doc != null)
                {
                    XmlNodeList BaggageNodes = doc.SelectNodes("Response/Baggage");

                    if (BaggageNodes != null)
                    {
                        for (int i = 0; i < BaggageNodes.Count; i++)
                        {
                            XmlNodeList nodes = BaggageNodes[i].SelectNodes("Baggage");

                            if (nodes != null)
                            {
                                if (exchangeRates.ContainsKey(nodes[0].SelectSingleNode("Currency").InnerText))
                                    rateOfExchange = exchangeRates[nodes[0].SelectSingleNode("Currency").InnerText];
                                if (BaggageNodes.Count > 1)
                                {
                                    for (int j = 0; j < nodes.Count; j++)
                                    {
                                        XmlNode node = nodes[j];

                                        if (node != null)
                                        {
                                            DataRow dr = dtBaggage.NewRow();
                                            dr["WayType"] = node.SelectSingleNode("WayType").InnerText;
                                            dr["Code"] = node.SelectSingleNode("Code").InnerText;
                                            dr["Description"] = node.SelectSingleNode("Description").InnerText;
                                            dr["Weight"] = Convert.ToInt32(node.SelectSingleNode("Weight").InnerText);
                                            dr["Price"] = Convert.ToDecimal(node.SelectSingleNode("Price").InnerText) * rateOfExchange;
                                            dr["SupplierPrice"] = Convert.ToDecimal(node.SelectSingleNode("Price").InnerText);
                                            //if (bags.Count > 1)
                                            //{
                                            //    dr["Price"] = Convert.ToDecimal(dr["Price"]) + (Convert.ToDecimal(bags[1].SelectSingleNode("Price").InnerText) * rateOfExchange);
                                            //    dr["SupplierPrice"] = Convert.ToDecimal(dr["SupplierPrice"]) + Convert.ToDecimal(node.SelectSingleNode("Price").InnerText);
                                            //}

                                            dr["Currency"] = agentBaseCurrency;
                                            dr["Origin"] = node.SelectSingleNode("Origin").InnerText;
                                            dr["Destination"] = node.SelectSingleNode("Destination").InnerText;
                                            //if (node.SelectSingleNode("Origin").InnerText == result.Flights[0][0].Origin.AirportCode)
                                            if (i == 0)
                                            {
                                                dr["Group"] = 0;
                                            }
                                            //else if (node.SelectSingleNode("Destination").InnerText == result.Flights[0][result.Flights[0].Length - 1].Origin.AirportCode)
                                            else
                                            {
                                                dr["Group"] = 1;
                                            }
                                            dtBaggage.Rows.Add(dr);
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (XmlNode node in nodes)
                                    {
                                        DataRow dr = dtBaggage.NewRow();
                                        dr["WayType"] = node.SelectSingleNode("WayType").InnerText;
                                        dr["Code"] = node.SelectSingleNode("Code").InnerText;
                                        dr["Description"] = node.SelectSingleNode("Description").InnerText;
                                        dr["Weight"] = Convert.ToInt32(node.SelectSingleNode("Weight").InnerText);
                                        dr["Price"] = Convert.ToDecimal(node.SelectSingleNode("Price").InnerText) * rateOfExchange;
                                        dr["SupplierPrice"] = Convert.ToDecimal(node.SelectSingleNode("Price").InnerText);
                                        dr["Currency"] = agentBaseCurrency;
                                        dr["Origin"] = node.SelectSingleNode("Origin").InnerText;
                                        dr["Destination"] = node.SelectSingleNode("Destination").InnerText;
                                        //if (node.SelectSingleNode("Origin").InnerText == result.Flights[0][0].Origin.AirportCode)
                                        {
                                            dr["Group"] = 0;
                                        }

                                        dtBaggage.Rows.Add(dr);
                                    }
                                }
                            }
                        }
                    }
                }

            }

            return dtBaggage;

        }
        /// <summary>
        /// To send baggage request and get response 
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private XmlDocument TempGetBaggage(SearchResult result)
        {
            XmlDocument doc = null;

            try
            {
                Authenticate();
                if (ar != null)
                {
                    if (ar.Error.ErrorMessage == null || ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                    {
                        StringBuilder requestData = new StringBuilder();
                        requestData.AppendLine("{");
                        requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        requestData.AppendLine("\"TraceId\": \"" + result.FareSellKey + "\",");
                        requestData.AppendLine("\"ResultIndex\": \"" + result.GUID + "\"");
                        requestData.AppendLine("}");
                        try
                        {

                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirBaggageRequestJSON.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic baggageRequest = js.Deserialize<dynamic>(requestData.ToString());
                            string rsp = JsonConvert.SerializeObject(baggageRequest, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "BaggageRequest");
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirBaggageRequest.xml";
                            doc1.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save GetBaggage Request. Error" + ex.ToString(), "");
                        }

                        string response = GetResponse(requestData.ToString(), ssrUrl);

                        try
                        {


                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirBaggageResponseJSON.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic baggageResponse = js.Deserialize<dynamic>(response);
                            string rsp = JsonConvert.SerializeObject(baggageResponse, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(response);
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirBaggageResponse.xml";
                            doc.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save GetBaggage Response. Error" + ex.ToString(), "");
                        }

                    }
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to get Baggage details. Error : " + ex.ToString(), "");
                throw ex;
            }

            return doc;
        }
        /// <summary>
        /// To book/Hold itinerary. Sending two booking requests by  spliting them and finally combining the response. this method is only for GDS.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public BookingResponse Book(ref FlightItinerary itinerary)
        {
            BookingResponse bookResponse = new BookingResponse();
            try
            {
                //Sending two booking requests by  spliting them and finally combining the response.
                if (itinerary.GUID.Contains(',') && itinerary.SupplierLocatorCode.ToLower() == "normalreturn") //Normal return combine result set
                {
                    decimal onWardPublishedFare = 0.0M;
                    decimal onwardTax = 0.0M;
                    string[] Guids = itinerary.GUID.Split(',');
                    foreach (string Guid in Guids)
                    {
                        itinerary.GUID = Guid;
                        XmlDocument doc = TempBook(ref itinerary);
                        if (doc != null)
                        {
                            XmlNode Error = doc.SelectSingleNode("Response/Error");

                            if (Error != null)
                            {
                                if (Error.SelectSingleNode("ErrorCode").InnerText == "0")
                                {
                                    XmlNode Response = doc.SelectSingleNode("Response/Response");
                                    if (bookResponse.PNR != null && bookResponse.PNR.Length > 0)
                                    {
                                        bookResponse.PNR += "|" + Response.SelectSingleNode("PNR").InnerText;
                                    }
                                    else
                                    {
                                        bookResponse.PNR = Response.SelectSingleNode("PNR").InnerText;
                                    }
                                    bookResponse.ProdType = ProductType.Flight;
                                    bookResponse.ConfirmationNo = bookResponse.PNR;
                                    bookResponse.SSRDenied = Convert.ToBoolean(Response.SelectSingleNode("SSRDenied").InnerText);
                                    itinerary.PNR = bookResponse.PNR;
                                    if (itinerary.AirLocatorCode != null && itinerary.AirLocatorCode.Length > 0)
                                    {
                                        itinerary.AirLocatorCode += "|" + Response.SelectSingleNode("BookingId").InnerText;//TEMP Store TBO BookingId for Ticketing.
                                    }
                                    else
                                    {
                                        itinerary.AirLocatorCode = Response.SelectSingleNode("BookingId").InnerText ;//TEMP Store TBO BookingId for Ticketing.
                                    }
                                    onWardPublishedFare = Convert.ToDecimal(doc.SelectSingleNode("Response/Response/FlightItinerary/Fare/BaseFare").InnerText) * (0.056M);
                                    onwardTax = Convert.ToDecimal(doc.SelectSingleNode("Response/Response/FlightItinerary/Fare/Tax").InnerText) * (0.056M);

                                    XmlNodeList Segments = doc.SelectNodes("Response/Response/FlightItinerary/Segments");

                                    foreach (FlightInfo seg in itinerary.Segments)
                                    {
                                        foreach (XmlNode segment in Segments)
                                        {
                                            if (seg.Origin.AirportCode == segment.SelectSingleNode("Origin/Airport/AirportCode").InnerText)
                                            {
                                                if (segment.SelectSingleNode("AirlinePNR") != null)
                                                {
                                                    seg.AirlinePNR = segment.SelectSingleNode("AirlinePNR").InnerText;                                                    
                                                }
                                                break;
                                            }
                                        }
                                    }

                                    if (Response.SelectSingleNode("IsPriceChanged").InnerText == "true" || Response.SelectSingleNode("IsTimeChanged").InnerText == "true")
                                    {
                                        if (itinerary.PNR == null)
                                        {
                                            itinerary.PNR = "";
                                        }
                                        //Update Price or Time for the Itinerary and alert user.
                                        bookResponse.Status = BookingResponseStatus.Failed;
                                        bookResponse.Error = "Price has been revised for this booking.";                                        
                                    }
                                    else
                                    {
                                        //Assign true to IsServiceTaxOnBaseFarePlusYQ in order not to consider Commission & Incentive values in Invoice & Reports
                                        if (showPubFaresForCorp)
                                        {
                                            itinerary.Passenger[0].Price.IsServiceTaxOnBaseFarePlusYQ = true;
                                        }
                                        Audit.Add(EventType.TBOAir, Severity.Normal, appUserId, "(TBOAir)Booking Successful PNR " + bookResponse.PNR + " for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
                                        bookResponse.Status = BookingResponseStatus.Successful;
                                    }
                                }
                                else if (Error.SelectSingleNode("ErrorCode").InnerText == "3")
                                {
                                    if (itinerary.PNR == null)
                                    {
                                        itinerary.PNR = "";
                                    }
                                    bookResponse.Status = BookingResponseStatus.OtherFare;
                                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)" + Error.SelectSingleNode("ErrorMessage").InnerText, "");
                                }
                                else
                                {
                                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)"+Error.SelectSingleNode("ErrorMessage").InnerText, "");
                                    bookResponse.Error = Error.SelectSingleNode("ErrorMessage").InnerText;
                                    bookResponse.Status = BookingResponseStatus.Failed;
                                    throw new ArgumentException(bookResponse.Error);
                                }
                            }
                            else
                            {
                                if (itinerary.PNR == null)
                                {
                                    itinerary.PNR = "";
                                }
                                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Book", "");
                                bookResponse.Status = BookingResponseStatus.Failed;
                            }
                        }
                    }
                    if (itinerary.PNR != null)
                    {
                        int pnrCount = itinerary.PNR.Split('|').Length;
                        //Failed booking. In the combined result if return booking failed. Continue booking onward segment by showing an alert
                        if (pnrCount == 1)
                        {
                            FlightInfo[] f = itinerary.Segments;
                            itinerary.Segments = new FlightInfo[1];
                            itinerary.Segments[0] = f[0];
                            itinerary.PNR = itinerary.PNR;
                            itinerary.AirLocatorCode = itinerary.AirLocatorCode.Remove(itinerary.AirLocatorCode.Length - 1);

                            itinerary.Passenger[0].Price.PublishedFare = onWardPublishedFare;
                            itinerary.Passenger[0].Price.Tax = onwardTax;
                            bookResponse.Status = BookingResponseStatus.ReturnFailed;
                        }
                    }
                }
                else
                {
                    XmlDocument doc = TempBook(ref itinerary);

                    if (doc != null)
                    {
                        XmlNode Error = doc.SelectSingleNode("Response/Error");

                        if (Error != null)
                        {
                            if (Error.SelectSingleNode("ErrorCode").InnerText == "0")
                            {
                                XmlNode Response = doc.SelectSingleNode("Response/Response");

                                bookResponse.PNR = Response.SelectSingleNode("PNR").InnerText;
                                bookResponse.ProdType = ProductType.Flight;
                                bookResponse.ConfirmationNo = bookResponse.PNR;
                                bookResponse.SSRDenied = Convert.ToBoolean(Response.SelectSingleNode("SSRDenied").InnerText);
                                itinerary.PNR = bookResponse.PNR;
                                itinerary.AirLocatorCode = Response.SelectSingleNode("BookingId").InnerText;//TEMP Store TBO BookingId for Ticketing.

                                XmlNodeList Segments = doc.SelectNodes("Response/Response/FlightItinerary/Segments");
                                XmlNode timeChanged = doc.SelectSingleNode("Response/Response/IsTimeChanged");
                                bool isTimeChanged = false;
                                if (timeChanged != null)
                                {
                                    isTimeChanged = Convert.ToBoolean(timeChanged.InnerText);
                                }

                                XmlNodeList FareRules = doc.SelectNodes("Response/Response/FareRules");

                                //Assign true to IsServiceTaxOnBaseFarePlusYQ in order not to consider Commission & Incentive values in Invoice & Reports
                                if (showPubFaresForCorp)
                                {
                                    itinerary.Passenger[0].Price.IsServiceTaxOnBaseFarePlusYQ = true;
                                }


                                foreach (FlightInfo seg in itinerary.Segments)
                                {
                                    foreach (XmlNode segment in Segments)
                                    {
                                        if (seg.Origin.AirportCode == segment.SelectSingleNode("Origin/Airport/AirportCode").InnerText)
                                        {
                                            if (segment.SelectSingleNode("Origin/Airport/Terminal") != null)
                                            {
                                                seg.DepTerminal = segment.SelectSingleNode("Origin/Airport/Terminal").InnerText;
                                            }
                                            if (segment.SelectSingleNode("AirlinePNR") != null && segment.SelectSingleNode("AirlinePNR").InnerText.Length > 0)
                                            {
                                                seg.AirlinePNR = segment.SelectSingleNode("AirlinePNR").InnerText;
                                            }
                                            if (isTimeChanged && segment.SelectSingleNode("Origin/DepTime") != null)
                                            {
                                                seg.DepartureTime = Convert.ToDateTime(segment.SelectSingleNode("Origin/DepTime").InnerText);
                                            }
                                            //break;
                                        }
                                        if (seg.Destination.AirportCode == segment.SelectSingleNode("Destination/Airport/AirportCode").InnerText)
                                        {
                                            if (segment.SelectSingleNode("Destination/Airport/Terminal") != null)
                                            {
                                                seg.ArrTerminal = segment.SelectSingleNode("Destination/Airport/Terminal").InnerText;
                                            }
                                            if (segment.SelectSingleNode("AirlinePNR") != null && segment.SelectSingleNode("AirlinePNR").InnerText.Length > 0)
                                            {
                                                seg.AirlinePNR = segment.SelectSingleNode("AirlinePNR").InnerText;
                                            }
                                            if (isTimeChanged && segment.SelectSingleNode("Destination/ArrTime") != null)
                                            {
                                                seg.ArrivalTime = Convert.ToDateTime(segment.SelectSingleNode("Destination/ArrTime").InnerText);
                                            }
                                        }
                                    }

                                    if (FareRules != null)
                                    {
                                        itinerary.FareRules = new List<CT.BookingEngine.FareRule>();
                                        foreach (XmlNode farerule in FareRules)
                                        {
                                            if (seg.Origin.AirportCode == farerule.SelectSingleNode("Origin").InnerText)
                                            {
                                                FareRule rule = new FareRule();
                                                rule.Origin = seg.Origin.AirportCode;
                                                rule.Airline = seg.Airline;
                                                rule.DepartureTime = seg.DepartureTime;
                                                rule.Destination = seg.Destination.AirportCode;
                                                rule.FareBasisCode = farerule.SelectSingleNode("FareBasisCode").InnerText;
                                                rule.FareRuleDetail = farerule.SelectSingleNode("FareRuleDetail").InnerText;
                                                rule.FareRestriction = farerule.SelectSingleNode("FareRestriction").InnerText;

                                                itinerary.FareRules.Add(rule);
                                            }
                                        }
                                    }
                                }

                                bookResponse.Status = BookingResponseStatus.Successful;

                            }
                            //else if (Error.SelectSingleNode("ErrorCode").InnerText == "3")
                            //{
                            //    bookResponse.Status = BookingResponseStatus.Failed;
                            //    bookResponse.Error = Error.SelectSingleNode("ErrorMessage").InnerText;
                            //}
                            else
                            {
                                bookResponse.Error = Error.SelectSingleNode("ErrorMessage").InnerText;
                                bookResponse.Status = BookingResponseStatus.Failed;
                                throw new ArgumentException(bookResponse.Error);

                            }
                        }
                        else
                        {
                            bookResponse.Status = BookingResponseStatus.Failed;
                        }
                    }
                    else
                    {
                        bookResponse.Status = BookingResponseStatus.Failed;
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to Book itinerary. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return bookResponse;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private XmlDocument TempBook(ref FlightItinerary itinerary)
        {
            XmlDocument doc = null;
            try
            {
                Authenticate();
                if (ar != null)
                {
                    if (ar.Error.ErrorMessage == null || ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                    {
                        decimal bookingAmount = 0;

                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            bookingAmount = pax.TBOPrice.PublishedFare;
                        }

                        //if (balance != null && balance.CashBalance > bookingAmount)not to check balance for HOLD
                        {
                            //Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Booking started for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
                            StringBuilder bookReq = new StringBuilder();
                            bookReq.Append("{");
                            bookReq.Append("\"EndUserIp\": \"" + endUserIp + "\",");
                            bookReq.Append("\"TokenId\": \"" + ar.TokenId + "\",");
                            bookReq.Append("\"TraceId\": \"" + itinerary.TicketAdvisory + "\",");
                            bookReq.Append("\"ResultIndex\": \"" + itinerary.GUID + "\",");
                            bookReq.Append("\"Passengers\": [");

                            for (int i = 0; i < itinerary.Passenger.Length; i++)
                            {
                                FlightPassenger pax = itinerary.Passenger[i];
                                bookReq.Append("{");
                                switch (pax.Type)
                                {
                                    case PassengerType.Adult:
                                        bookReq.Append("\"Title\":\"" + pax.Title + "\",");
                                        break;
                                    case PassengerType.Child:
                                    case PassengerType.Infant:
                                        if (pax.Gender == Gender.Male)
                                        {
                                            bookReq.Append("\"Title\":\"Mstr\",");
                                        }
                                        else
                                        {
                                            bookReq.Append("\"Title\":\"Miss\",");
                                        }
                                        break;
                                }
                                bookReq.Append("\"FirstName\": \"" + pax.FirstName + "\",");
                                bookReq.Append("\"LastName\": \"" + pax.LastName + "\",");
                                bookReq.Append("\"PaxType\": " + (int)pax.Type + ",");
                                if (pax.DateOfBirth != DateTime.MinValue) //we will not pass the default date value
                                {
                                    bookReq.Append("\"DateOfBirth\": \"" + pax.DateOfBirth.ToString("yyyy-MM-ddThh:mm:ss") + "\",");
                                }
                                bookReq.Append("\"Gender\": " + (int)pax.Gender + ",");
                                if (!string.IsNullOrEmpty(pax.PassportNo))
                                {
                                    bookReq.Append("\"PassportNo\": \"" + pax.PassportNo + "\",");
                                }
                                if (pax.PassportExpiry != DateTime.MinValue)
                                {
                                    bookReq.Append("\"PassportExpiry\": \"" + pax.PassportExpiry.ToString("yyyy-MM-ddThh:mm:ss") + "\",");
                                }
                                if (!string.IsNullOrEmpty(pax.AddressLine1))//if AddressLine1 is empty then pass dummy
                                {
                                    bookReq.Append("\"AddressLine1\": \"" + pax.AddressLine1 + "\",");
                                }
                                else
                                {
                                    bookReq.Append("\"AddressLine1\": \"" + "AddressLine1" + "\",");
                                }
                                bookReq.Append("\"AddressLine2\": \"\",");
                                if (!string.IsNullOrEmpty(pax.City))
                                {
                                    bookReq.Append("\"City\": \"" + pax.City + "\",");
                                }
                                else
                                {
                                    bookReq.Append("\"City\": \"" + "City" + "\",");
                                }
                                if (pax.Country != null)
                                {
                                    if (!string.IsNullOrEmpty(pax.Country.CountryCode))
                                    {
                                        bookReq.Append("\"CountryCode\": \"" + pax.Country.CountryCode + "\",");
                                    }
                                    if (!string.IsNullOrEmpty(pax.Country.CountryName))
                                    {
                                        bookReq.Append("\"CountryName\": \"" + pax.Country.CountryName + "\",");
                                    }

                                }


                                bookReq.Append("\"ContactNo\": \"" + pax.CellPhone.Replace("-", "") + "\",");
                                bookReq.Append("\"Email\": \"" + pax.Email + "\",");
                                bookReq.Append("\"IsLeadPax\": \"" + (pax.IsLeadPax ? "true" : "false") + "\",");
                                bookReq.Append("\"FFAirline\": \"\",");
                                bookReq.Append("\"FFNumber\": \"\",");
                                if (i == 0)//Pass GST fields for Lead pax only
                                {
                                    if (string.IsNullOrEmpty(itinerary.GstNumber))
                                    {
                                        bookReq.Append("\"GSTCompanyAddress\": \"\",");
                                        bookReq.Append("\"GSTCompanyContactNumber\": \"\",");
                                        bookReq.Append("\"GSTCompanyEmail\": \"\",");
                                        bookReq.Append("\"GSTCompanyName\": \"\",");
                                        bookReq.Append("\"GSTNumber\": \" \",");
                                    }
                                    else
                                    {
                                        bookReq.Append("\"GSTCompanyAddress\": \"" + itinerary.GstCompanyAddress + "\",");
                                        bookReq.Append("\"GSTCompanyContactNumber\": \"" + itinerary.GstCompanyContactNumber + "\",");
                                        bookReq.Append("\"GSTCompanyEmail\": \"" + itinerary.GstCompanyEmail + "\",");
                                        bookReq.Append("\"GSTCompanyName\": \"" + itinerary.GstCompanyName + "\",");
                                        bookReq.Append("\"GSTNumber\": \"" + itinerary.GstNumber + "\",");
                                        pax.GSTTaxRegNo = itinerary.GstNumber;//Assign GST Number to the lead pax
                                    }
                                }
                                bookReq.Append("\"Fare\": {");
                                bookReq.Append("\"BaseFare\":" + pax.TBOPrice.BaseFare + ",");
                                bookReq.Append("\"Tax\": " + pax.TBOPrice.Tax + ",");
                                bookReq.Append("\"TransactionFee\": " + pax.TBOPrice.TransactionFee + ",");
                                bookReq.Append("\"YQTax\": " + pax.TBOPrice.YQTax + ",");
                                bookReq.Append("\"AdditionalTxnFee\": " + pax.TBOPrice.AdditionalTxnFee + ",");
                                bookReq.Append("\"AirTransFee\": " + pax.TBOPrice.AirlineTransFee + "");

                                bookReq.Append("}");

                                if (i == 0)
                                {
                                    if (pax.Meal.Code != null)
                                    {
                                        bookReq.Append(",");
                                        bookReq.Append("\"Meal\": {");
                                        bookReq.Append("\"Code\": \"" + (pax.Meal.Code != null ? pax.Meal.Code : null) + "\",");
                                        bookReq.Append("\"Description\": \"" + (pax.Meal.Description != null ? pax.Meal.Description : null) + "\",");
                                        bookReq.Append("}");
                                    }
                                    else if (pax.Seat.Code != null)
                                    {
                                        bookReq.Append(",");
                                        bookReq.Append("\"Seat\": {");
                                        bookReq.Append("\"Code\": \"" + (pax.Seat.Code != null ? pax.Seat.Code : null) + "\",");
                                        bookReq.Append("\"Description\": \"" + (pax.Seat.Description != null ? pax.Seat.Description : null) + "\",");
                                        bookReq.Append("}");//Seat End
                                    }
                                }

                                if (i < itinerary.Passenger.Length - 1)
                                {
                                    bookReq.Append("},");//Pax End
                                }
                                else
                                {
                                    bookReq.Append("}");//Pax End
                                }
                            }

                            bookReq.Append("]");//Passengers End
                            bookReq.Append("}");//Request End

                            try
                            {

                                string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirBookRequestJSON.json";
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                dynamic bookRequest = js.Deserialize<dynamic>(bookReq.ToString());
                                string rsp = JsonConvert.SerializeObject(bookRequest, Newtonsoft.Json.Formatting.Indented);
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(rsp);
                                sw.Close();

                                XmlDocument doc1 = JsonConvert.DeserializeXmlNode(bookReq.ToString(), "BookRequest");
                                filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirBookRequest.xml";
                                doc1.Save(filePath);
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save Booking Request. Error" + ex.ToString(), "");
                            }

                            //Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Booking request created for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                            string response = GetResponse(bookReq.ToString(), bookUrl);

                            //Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Booking response created for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                            try
                            {

                                string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirBookResponseJSON.json";
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                dynamic bookResponse = js.Deserialize<dynamic>(response);
                                string rsp = JsonConvert.SerializeObject(bookResponse, Newtonsoft.Json.Formatting.Indented);
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(rsp);
                                sw.Close();

                                doc = JsonConvert.DeserializeXmlNode(response);
                                filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirBookResponse.xml";
                                doc.Save(filePath);
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save Booking Response. Error" + ex.ToString(), "");
                            }
                        }
                    }
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                    throw new Exception("Failed to Authenticate for TBOAir");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to Book itinerary. Error : " + ex.ToString(), "");
                throw ex;
            }

            return doc;
        }

        /// <summary>
        /// To create Ticket. Sending to ticketing requests and merging responses for combined results. this method is only for  LCC.
        /// </summary>
        /// <param name="itinerary">FlightItinerary reference</param>
        /// <remarks>This method is used to generate Tickets for LCC Airlines. 
        /// This method does not return any ticket related information as LCC tickets does not have ticket numbers in particular. 
        /// PNR will be treated as Ticket Number for all LCC Airlines. Tickets are manually generated using the Passenger details from MSE. 
        /// BookingResponse contains <c>PNR</c> value if ticketing is successful with <c>Status</c> Successful otherwise a failure reason is assigned in <c>Error</c></remarks>        
        /// <returns>BookingResponse with PNR information</returns>
        public BookingResponse Ticket(ref FlightItinerary itinerary)
        {
            BookingResponse bookingResponse = new BookingResponse();
            XmlDocument doc = null;
            int RowCount = 1;
            decimal onWardPublishedFare = 0.0M;
            decimal onwardTax = 0.0M;
            try
            {
                //Sending to ticketing requests and merging responses for combined results.
                if (itinerary.GUID.Contains(',') && itinerary.SupplierLocatorCode.ToLower() == "normalreturn") //Normal Return for domestic
                {
                    string[] Guids = itinerary.GUID.Split(',');
                    for (int i = 0; i < Guids.Length; i++)
                    {
                        string Guid = Guids[i];
                        Twoway = RowCount;
                        itinerary.GUID = Guid;
                        doc = TempTicket(ref itinerary);

                        if (doc != null)
                        {
                            if (itinerary.IsLCC || itinerary.AirLocatorCode.Contains("LCC"))
                            {
                                XmlNode errorNode = doc.SelectSingleNode("TicketResponse/Response/Error");
                                if (errorNode != null && errorNode.SelectSingleNode("ErrorCode").InnerText == "0")
                                {
                                    if (bookingResponse.PNR != null && bookingResponse.PNR.Length > 0)
                                    {
                                        bookingResponse.PNR += "|" + doc.SelectSingleNode("TicketResponse/Response/Response/PNR").InnerText;
                                    }
                                    else
                                    {
                                        bookingResponse.PNR = doc.SelectSingleNode("TicketResponse/Response/Response/PNR").InnerText;
                                    }
                                    itinerary.PNR = bookingResponse.PNR;
                                    bookingResponse.ProdType = ProductType.Flight;
                                    bookingResponse.Error = "";
                                    bookingResponse.Status = BookingResponseStatus.Successful;
                                    if (itinerary.AirLocatorCode != null && itinerary.AirLocatorCode.Length > 0)
                                    {
                                        itinerary.AirLocatorCode += "|" + doc.SelectSingleNode("TicketResponse/Response/Response/BookingId").InnerText;
                                    }
                                    else
                                    {
                                        itinerary.AirLocatorCode = doc.SelectSingleNode("TicketResponse/Response/Response/BookingId").InnerText;
                                    }

                                    //Assign true to IsServiceTaxOnBaseFarePlusYQ in order not to consider Commission & Incentive values in Invoice & Reports
                                    if (showPubFaresForCorp)
                                    {
                                        itinerary.Passenger[0].Price.IsServiceTaxOnBaseFarePlusYQ = true;
                                    }

                                    //Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing Successful PNR " + itinerary.PNR + " started for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                                    onWardPublishedFare = Convert.ToDecimal(doc.SelectSingleNode("TicketResponse/Response/Response/FlightItinerary/Fare/BaseFare").InnerText) * rateOfExchange;
                                    onwardTax = (Convert.ToDecimal(doc.SelectSingleNode("TicketResponse/Response/Response/FlightItinerary/Fare/Tax").InnerText) + Convert.ToDecimal(doc.SelectSingleNode("Request/Response/Response/FlightItinerary/Fare/OtherCharges").InnerText)) * rateOfExchange;
                                    XmlNodeList Passengers = doc.SelectNodes("TicketResponse/Response/Response/FlightItinerary/Passenger");

                                    //Set Airline PNR
                                    XmlNodeList Segments = doc.SelectNodes("TicketResponse/Response/Response/FlightItinerary/Segments");

                                    foreach (FlightInfo seg in itinerary.Segments)
                                    {
                                        foreach (XmlNode segment in Segments)
                                        {
                                            if (seg.Origin.AirportCode == segment.SelectSingleNode("Origin/Airport/AirportCode").InnerText)
                                            {
                                                if (segment.SelectSingleNode("AirlinePNR") != null && segment.SelectSingleNode("AirlinePNR").InnerText.Length > 0)
                                                {
                                                    seg.AirlinePNR = segment.SelectSingleNode("AirlinePNR").InnerText;
                                                }
                                                break;
                                            }
                                        }
                                    }


                                    if (i == 0)
                                    {
                                        //Remove previous assigned baggage codes
                                        foreach (FlightPassenger fpax in itinerary.Passenger)
                                        {
                                            fpax.BaggageCode = "";
                                        }
                                    }

                                    foreach (FlightPassenger fpax in itinerary.Passenger)
                                    {
                                        foreach (XmlNode pax in Passengers)
                                        {
                                            if (fpax.PassportNo == pax.SelectSingleNode("PassportNo").InnerText)
                                            {

                                                if (fpax.CategoryId != null && fpax.CategoryId.Length > 0)
                                                {
                                                    fpax.CategoryId += "|" + pax.SelectSingleNode("Ticket").SelectSingleNode("TicketId").InnerText;
                                                }
                                                else
                                                {
                                                    fpax.CategoryId = pax.SelectSingleNode("Ticket").SelectSingleNode("TicketId").InnerText;
                                                }
                                                XmlNodeList Baggages = pax.SelectNodes("SegmentAdditionalInfo");

                                                if (Baggages != null)
                                                {
                                                    foreach (XmlNode baggage in Baggages)
                                                    {
                                                        switch (pax.SelectSingleNode("PaxType").InnerText)
                                                        {
                                                            case "1":
                                                                if (fpax.Type == PassengerType.Adult)
                                                                {
                                                                    string bags = "";
                                                                    if (fpax.BaggageCode.Length > 0)
                                                                    {
                                                                        string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                        if (bag.Contains("|"))
                                                                        {
                                                                            bag = bag.TrimEnd('|');
                                                                            foreach (string b in bag.Split('|'))
                                                                            {
                                                                                if (b != "0")
                                                                                {
                                                                                    if (bags.Length > 0)
                                                                                    {
                                                                                        bags += "+" + (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        bags = (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            bags = bag;
                                                                        }
                                                                        fpax.BaggageCode += "," + bags;
                                                                    }
                                                                    else
                                                                    {
                                                                        string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                        if (bag.Contains("|"))
                                                                        {
                                                                            bag = bag.TrimEnd('|');
                                                                            foreach (string b in bag.Split('|'))
                                                                            {
                                                                                if (b != "0")
                                                                                {
                                                                                    if (bags.Length > 0)
                                                                                    {
                                                                                        bags += "+" + (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        bags = (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            bags = bag;
                                                                        }
                                                                        fpax.BaggageCode = bags;
                                                                    }

                                                                }
                                                                break;
                                                            case "2":
                                                                if (fpax.Type == PassengerType.Child)
                                                                {
                                                                    string bags = "";
                                                                    if (fpax.BaggageCode.Length > 0)
                                                                    {
                                                                        string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                        if (bag.Contains("|"))
                                                                        {
                                                                            bag = bag.TrimEnd('|');
                                                                            foreach (string b in bag.Split('|'))
                                                                            {
                                                                                if (b != "0")
                                                                                {
                                                                                    if (bags.Length > 0)
                                                                                    {
                                                                                        bags += "+" + (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        bags = (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            bags = bag;
                                                                        }
                                                                        fpax.BaggageCode += "," + bags;
                                                                    }
                                                                    else
                                                                    {
                                                                        string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                        if (bag.Contains("|"))
                                                                        {
                                                                            bag = bag.TrimEnd('|');
                                                                            foreach (string b in bag.Split('|'))
                                                                            {
                                                                                if (b != "0")
                                                                                {
                                                                                    if (bags.Length > 0)
                                                                                    {
                                                                                        bags += "+" + (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        bags = (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            bags = bag;
                                                                        }
                                                                        fpax.BaggageCode = bags;
                                                                    }

                                                                }
                                                                break;
                                                                //case "3":
                                                                //    if (fpax.Type == PassengerType.Infant)
                                                                //    {
                                                                //        if (Baggages.Count > 1)
                                                                //        {
                                                                //            fpax.BaggageCode = Baggages[0].SelectSingleNode("Baggage").InnerText + "," + Baggages[1].SelectSingleNode("Baggage").InnerText;
                                                                //        }
                                                                //        else
                                                                //        {
                                                                //            fpax.BaggageCode = Baggages[0].SelectSingleNode("Baggage").InnerText;
                                                                //        }
                                                                //    }
                                                                //    break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (itinerary.PNR == null)
                                    {
                                        itinerary.PNR = "";
                                        bookingResponse.PNR = "";
                                    }
                                    bookingResponse.Error = errorNode.SelectSingleNode("ErrorMessage").InnerText;
                                    bookingResponse.Status = BookingResponseStatus.Failed;
                                }
                            }
                        }
                        RowCount++;
                    }
                    itinerary.GUID = Guids[0] + "," + Guids[1];
                    if (itinerary.PNR != null)
                    {
                        int pnrCount = itinerary.PNR.Split('|').Length;  //Proceed to ticket in return ticketing got failed . Showing an alert .
                        //if (pnrCount == 2)
                        if (pnrCount == 1)
                        {
                            FlightInfo[] f = itinerary.Segments;
                            itinerary.Segments = new FlightInfo[1];
                            itinerary.Segments[0] = f[0];

                            itinerary.Passenger[0].Price.PublishedFare = onWardPublishedFare;
                            itinerary.Passenger[0].Price.Tax = onwardTax;
                            bookingResponse.Status = BookingResponseStatus.ReturnFailed;

                        }
                    }
                }
                else
                {
                    doc = TempTicket(ref itinerary);

                    if (doc != null)
                    {
                        if (itinerary.IsLCC || itinerary.AirLocatorCode.Contains("LCC"))
                        {
                            XmlNode errorNode = doc.SelectSingleNode("TicketResponse/Response/Error");
                            if (errorNode != null && errorNode.SelectSingleNode("ErrorCode").InnerText == "0")
                            {
                                bookingResponse.PNR = doc.SelectSingleNode("TicketResponse/Response/Response/PNR").InnerText;
                                itinerary.PNR = bookingResponse.PNR;
                                bookingResponse.ProdType = ProductType.Flight;
                                bookingResponse.Error = "";
                                bookingResponse.Status = BookingResponseStatus.Successful;
                                if (itinerary.AirLocatorCode != null && itinerary.AirLocatorCode.Length > 0)
                                {
                                    itinerary.AirLocatorCode += "|" + doc.SelectSingleNode("TicketResponse/Response/Response/BookingId").InnerText;
                                }
                                else
                                {
                                    itinerary.AirLocatorCode = doc.SelectSingleNode("TicketResponse/Response/Response/BookingId").InnerText;
                                }

                                //Assign true to IsServiceTaxOnBaseFarePlusYQ in order not to consider Commission & Incentive values in Invoice & Reports
                                if (showPubFaresForCorp)
                                {
                                    itinerary.Passenger[0].Price.IsServiceTaxOnBaseFarePlusYQ = true;
                                }

                                Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing Successful for PNR: " + itinerary.PNR, "");

                                XmlNodeList Passengers = doc.SelectNodes("TicketResponse/Response/Response/FlightItinerary/Passenger");

                                //Set Airline PNR
                                XmlNodeList Segments = doc.SelectNodes("TicketResponse/Response/Response/FlightItinerary/Segments");

                                foreach (FlightInfo seg in itinerary.Segments)
                                {
                                    foreach (XmlNode segment in Segments)
                                    {
                                        if (seg.Origin.AirportCode == segment.SelectSingleNode("Origin/Airport/AirportCode").InnerText)
                                        {
                                            if (segment.SelectSingleNode("AirlinePNR") != null && segment.SelectSingleNode("AirlinePNR").InnerText.Length > 0)
                                            {
                                                seg.AirlinePNR = segment.SelectSingleNode("AirlinePNR").InnerText;
                                            }
                                            break;
                                        }

                                    }
                                }

                                //Remove previous assigned baggage codes
                                foreach (FlightPassenger fpax in itinerary.Passenger)
                                {
                                    fpax.BaggageCode = "";                                   
                                }

                                foreach (FlightPassenger fpax in itinerary.Passenger)
                                {
                                    foreach (XmlNode pax in Passengers)
                                    {
                                        //PaxName checking added bcoz PassportNo is not mandatory for Domestic itinerary
                                        if (fpax.FirstName.Trim().ToLower() + fpax.LastName.Trim().ToLower() == pax.SelectSingleNode("FirstName").InnerText.Trim().ToLower() + pax.SelectSingleNode("LastName").InnerText.Trim().ToLower())
                                        {                                           
                                            fpax.CategoryId = pax.SelectSingleNode("Ticket").SelectSingleNode("TicketId").InnerText;                                            

                                            try
                                            {
                                                string currency = pax.SelectSingleNode("Fare/Currency").InnerText;
                                                if (exchangeRates.ContainsKey(currency))
                                                {
                                                    rateOfExchange = exchangeRates[currency];
                                                }
                                                else { rateOfExchange = 1; }
                                                List<KeyValuePair<string, decimal>> taxBreakUp = new List<KeyValuePair<string, decimal>>();

                                                XmlNodeList TaxBreakups = pax.SelectNodes("Fare/TaxBreakup");
                                                foreach (XmlNode taxBreakup in TaxBreakups)
                                                {
                                                    if (taxBreakup.SelectSingleNode("key").InnerText.ToLower() != "totaltax")
                                                    {
                                                        taxBreakUp.Add(new KeyValuePair<string, decimal>((taxBreakup.SelectSingleNode("key").InnerText.ToLower() == "yqtax" ? "YQ" : taxBreakup.SelectSingleNode("key").InnerText), Convert.ToDecimal(taxBreakup.SelectSingleNode("value").InnerText) * rateOfExchange));
                                                    }
                                                }

                                                fpax.TaxBreakup = taxBreakUp;
                                            }
                                            catch (Exception ex)
                                            {
                                                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to read TaxBreakup from Ticket response. Reason: " + ex.ToString(), "");
                                            }

                                            XmlNodeList Baggages = pax.SelectNodes("SegmentAdditionalInfo");
                                            if (Baggages != null)
                                            {
                                                foreach (XmlNode baggage in Baggages)
                                                {
                                                    switch (pax.SelectSingleNode("PaxType").InnerText)
                                                    {
                                                        case "1":
                                                            if (fpax.Type == PassengerType.Adult)
                                                            {
                                                                string bags = "";
                                                                if (fpax.BaggageCode.Length > 0)
                                                                {
                                                                    string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                    if (bag.Contains("|"))
                                                                    {
                                                                        bag = bag.TrimEnd('|');
                                                                        foreach (string b in bag.Split('|'))
                                                                        {
                                                                            if (b != "0")
                                                                            {
                                                                                if (bags.Length > 0)
                                                                                {
                                                                                    bags += "+" + (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                                else
                                                                                {
                                                                                    bags = (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        bags = bag;
                                                                    }
                                                                    fpax.BaggageCode += "," + bags;
                                                                }
                                                                else
                                                                {
                                                                    string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                    if (bag.Contains("|"))
                                                                    {
                                                                        bag = bag.TrimEnd('|');

                                                                        foreach (string b in bag.Split('|'))
                                                                        {
                                                                            if (b != "0")
                                                                            {
                                                                                if (bags.Length > 0)
                                                                                {
                                                                                    bags += "+" + (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                                else
                                                                                {
                                                                                    bags = (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        bags = bag;
                                                                    }
                                                                    fpax.BaggageCode = bags;
                                                                }

                                                            }
                                                            break;
                                                        case "2":
                                                            if (fpax.Type == PassengerType.Child)
                                                            {
                                                                string bags = "";
                                                                if (fpax.BaggageCode.Length > 0)
                                                                {
                                                                    string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                    if (bag.Contains("|"))
                                                                    {
                                                                        bag = bag.TrimEnd('|');
                                                                        foreach (string b in bag.Split('|'))
                                                                        {
                                                                            if (b != "0")
                                                                            {
                                                                                if (bags.Length > 0)
                                                                                {
                                                                                    bags += "+" + (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                                else
                                                                                {
                                                                                    bags = (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        bags = bag;
                                                                    }
                                                                    fpax.BaggageCode += "," + bags;
                                                                }
                                                                else
                                                                {
                                                                    string bag = baggage.SelectSingleNode("Baggage").InnerText;
                                                                    if (bag.Contains("|"))
                                                                    {
                                                                        bag = bag.TrimEnd('|');
                                                                        foreach (string b in bag.Split('|'))
                                                                        {
                                                                            if (b != "0")
                                                                            {
                                                                                if (bags.Length > 0)
                                                                                {
                                                                                    bags += "+" + (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                                else
                                                                                {
                                                                                    bags = (b.ToLower().EndsWith("kg") ? b : b + "Kg");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        bags = bag;
                                                                    }
                                                                    fpax.BaggageCode = bags;
                                                                }

                                                            }
                                                            break;
                                                            //case "3":
                                                            //    if (fpax.Type == PassengerType.Infant)
                                                            //    {
                                                            //        if (Baggages.Count > 1)
                                                            //        {
                                                            //            fpax.BaggageCode = Baggages[0].SelectSingleNode("Baggage").InnerText + "," + Baggages[1].SelectSingleNode("Baggage").InnerText;
                                                            //        }
                                                            //        else
                                                            //        {
                                                            //            fpax.BaggageCode = Baggages[0].SelectSingleNode("Baggage").InnerText;
                                                            //        }
                                                            //    }
                                                            //    break;
                                                    }
                                                }
                                            }

                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //if (itinerary.PNR == null)
                                {
                                    itinerary.PNR = "";
                                    bookingResponse.PNR = "";
                                }
                                if (!string.IsNullOrEmpty(errorNode.SelectSingleNode("ErrorMessage").InnerText))
                                {
                                    bookingResponse.Error = errorNode.SelectSingleNode("ErrorMessage").InnerText;
                                }
                                else
                                {
                                    bookingResponse.Error = "Failed to Generate Ticket";
                                }
                                bookingResponse.Status = BookingResponseStatus.Failed;
                                throw new ArgumentException(bookingResponse.Error);
                            }
                        }
                    }
                    else
                    {
                        bookingResponse.Error = "Ticketing response returned empty or corrupted from TBO Air.";
                        bookingResponse.Status = BookingResponseStatus.Failed;
                        throw new ArgumentException(bookingResponse.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                bookingResponse.Status = BookingResponseStatus.Failed;
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to generate Ticket. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return bookingResponse;
        }
        /// <summary>
        /// To send a ticketing requeat and response for LCC.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private XmlDocument TempTicket(ref FlightItinerary itinerary)
        {
            XmlDocument doc = null;
            if (itinerary.GUID.Contains(',') && itinerary.SupplierLocatorCode.ToLower() != "normalreturn")
            {
                itinerary.GUID = itinerary.GUID.Split(',')[0];
            }
            try
            {
                Authenticate();
                if (ar != null)
                {
                    if (ar.Error.ErrorMessage == null || ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                    {
                        AgencyBalance balance = GetAgencyBalance(ar);

                        decimal bookingAmount = 0;

                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            bookingAmount += pax.TBOPrice.PublishedFare;
                        }

                        if (balance != null && balance.CashBalance > bookingAmount)
                        {
                            //Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing started for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
                            StringBuilder ticketReq = new StringBuilder();
                            ticketReq.Append("{");
                            ticketReq.Append("\"PreferredCurrency\": \"INR\",");
                            ticketReq.Append("\"IsBaseCurrencyRequired\":\"true\",");
                            ticketReq.Append("\"EndUserIp\": \"" + endUserIp + "\",");
                            ticketReq.Append("\"TokenId\": \"" + ar.TokenId + "\",");
                            ticketReq.Append("\"TraceId\": \"" + itinerary.TicketAdvisory + "\",");
                            ticketReq.Append("\"ResultIndex\": \"" + itinerary.GUID + "\",");
                            ticketReq.Append("\"Passengers\":[");

                            for (int i = 0; i < itinerary.Passenger.Length; i++)
                            {
                                FlightPassenger pax = itinerary.Passenger[i];
                                ticketReq.Append("{");
                                switch (pax.Type)
                                {
                                    case PassengerType.Adult:
                                        ticketReq.Append("\"Title\":\"" + pax.Title + "\",");
                                        break;
                                    case PassengerType.Child:
                                    case PassengerType.Infant:
                                        if (pax.Gender == Gender.Male)
                                        {
                                            ticketReq.Append("\"Title\":\"" + "Mstr" + "\",");
                                        }
                                        else if (pax.Gender == Gender.Female)
                                        {
                                            ticketReq.Append("\"Title\":\"" + "Miss" + "\",");
                                        }
                                        break;
                                }
                                ticketReq.Append("\"FirstName\": \"" + pax.FirstName + "\",");
                                ticketReq.Append("\"LastName\": \"" + pax.LastName + "\",");
                                ticketReq.Append("\"PaxType\": " + (int)pax.Type + ",");
                                if (pax.DateOfBirth != DateTime.MinValue)
                                {

                                    ticketReq.Append("\"DateOfBirth\": \"" + pax.DateOfBirth.ToString("yyyy-MM-ddThh:mm:ss") + "\",");
                                }
                                ticketReq.Append("\"Gender\": " + (int)pax.Gender + ",");

                                if (!string.IsNullOrEmpty(pax.PassportNo))
                                {
                                    ticketReq.Append("\"PassportNo\": \"" + pax.PassportNo + "\",");
                                }

                                if (pax.PassportExpiry != DateTime.MinValue)
                                {
                                    ticketReq.Append("\"PassportExpiry\": \"" + pax.PassportExpiry.ToString("yyyy-MM-ddThh:mm:ss") + "\",");
                                }
                                if (!string.IsNullOrEmpty(pax.AddressLine1))//If Address is emtpy then pass dummy
                                {
                                    ticketReq.Append("\"AddressLine1\": \"" + pax.AddressLine1 + "\",");
                                }
                                else
                                {
                                    ticketReq.Append("\"AddressLine1\": \"" + "AddressLine1" + "\",");
                                }
                                ticketReq.Append("\"AddressLine2\": \"\",");
                                if (!string.IsNullOrEmpty(pax.City))//If City is empty then pass dummy
                                {
                                    ticketReq.Append("\"City\": \"" + pax.City + "\",");
                                }
                                else
                                {
                                    ticketReq.Append("\"City\": \"" + "City" + "\",");
                                }
                                if (pax.Country != null)
                                {

                                    if (!string.IsNullOrEmpty(pax.Country.CountryCode))
                                    {
                                        ticketReq.Append("\"CountryCode\": \"" + pax.Country.CountryCode + "\",");
                                    }
                                    if (!string.IsNullOrEmpty(pax.Country.CountryName))
                                    {
                                        ticketReq.Append("\"CountryName\": \"" + pax.Country.CountryName + "\",");
                                    }
                                }

                                ticketReq.Append("\"ContactNo\": \"" + pax.CellPhone.Replace("-", "") + "\",");
                                ticketReq.Append("\"Email\": \"" + pax.Email + "\",");
                                ticketReq.Append("\"IsLeadPax\": \"" + (pax.IsLeadPax ? "true" : "false") + "\",");
                                ticketReq.Append("\"FFAirline\": \"\",");
                                ticketReq.Append("\"FFNumber\": \"\",");
                                if (i == 0)//Pass GST fields only for Lead pax
                                {
                                    if (string.IsNullOrEmpty(itinerary.GstNumber))
                                    {
                                        ticketReq.Append("\"GSTCompanyAddress\": \"\",");
                                        ticketReq.Append("\"GSTCompanyContactNumber\": \"\",");
                                        ticketReq.Append("\"GSTCompanyEmail\": \"\",");
                                        ticketReq.Append("\"GSTCompanyName\": \"\",");
                                        ticketReq.Append("\"GSTNumber\": \" \",");
                                    }
                                    else
                                    {
                                        ticketReq.Append("\"GSTCompanyAddress\": \"" + itinerary.GstCompanyAddress + "\",");
                                        ticketReq.Append("\"GSTCompanyContactNumber\": \"" + itinerary.GstCompanyContactNumber + "\",");
                                        ticketReq.Append("\"GSTCompanyEmail\": \"" + itinerary.GstCompanyEmail + "\",");
                                        ticketReq.Append("\"GSTCompanyName\": \"" + itinerary.GstCompanyName + "\",");
                                        ticketReq.Append("\"GSTNumber\": \"" + itinerary.GstNumber + "\",");
                                        pax.GSTTaxRegNo = itinerary.GstNumber;//Assign GSTNumber for lead pax
                                    }
                                }
                                ticketReq.Append("\"Fare\": {");
                                ticketReq.Append("\"BaseFare\":" + pax.TBOPrice.BaseFare + ",");
                                ticketReq.Append("\"Tax\": " + pax.TBOPrice.Tax + ",");
                                ticketReq.Append("\"TransactionFee\": " + pax.TBOPrice.TransactionFee + ",");
                                ticketReq.Append("\"YQTax\": " + pax.TBOPrice.YQTax + ",");
                                ticketReq.Append("\"AdditionalTxnFee\": " + pax.TBOPrice.AdditionalTxnFee + ",");
                                ticketReq.Append("\"AirTransFee\": " + pax.TBOPrice.AirlineTransFee + "");
                                ticketReq.Append("}");

                                if (pax.BaggageCode != null && pax.BaggageCode.Length > 0 && pax.BaggageType != null)
                                {
                                    ticketReq.Append(",");
                                    ticketReq.Append("\"Baggage\":[");

                                    //if (Twoway != 0)
                                    //{
                                    //    string[] str = pax.BaggageType.Split('|');
                                    //    if (Twoway == 1)
                                    //    {
                                    //        BaggageType = str[1];
                                    //    }
                                    //    pax.BaggageType = (Twoway == 1) ? str[0] : BaggageType;
                                    //}
                                    if (pax.BaggageType != null)
                                    {
                                        string[] baggageData = pax.BaggageType.Split('|');//Split according to Way-Type (One-Way/Return)
                                        for (int b = 0; b < baggageData.Length; b++)
                                        {
                                            if (baggageData[b].Length > 0)
                                            {
                                                ticketReq.Append("{");
                                                string[] data = baggageData[b].Split(',');
                                                for (int d = 0; d < data.Length; d++)
                                                {
                                                    string bd = data[d];
                                                    switch (bd.Split(':')[0])
                                                    {
                                                        case "WayType":
                                                            ticketReq.Append("\"WayType\":" + bd.Split(':')[1] + ",");
                                                            break;
                                                        case "Code":
                                                            ticketReq.Append("\"Code\":\"" + bd.Split(':')[1] + "\",");
                                                            break;
                                                        case "Description":
                                                            ticketReq.Append("\"Description\":" + bd.Split(':')[1] + ",");
                                                            break;
                                                        case "Weight":
                                                            ticketReq.Append("\"Weight\":" + bd.Split(':')[1] + ",");
                                                            break;
                                                        case "Currency":
                                                            ticketReq.Append("\"Currency\":\"" + itinerary.Passenger[0].Price.SupplierCurrency + "\",");
                                                            break;
                                                        case "Price":
                                                            ticketReq.Append("\"Price\":" + bd.Split(':')[1] + ",");
                                                            break;
                                                        case "Origin":
                                                            ticketReq.Append("\"Origin\":\"" + bd.Split(':')[1] + "\",");
                                                            break;
                                                        case "Destination":
                                                            ticketReq.Append("\"Destination\":\"" + bd.Split(':')[1] + "\"");
                                                            break;
                                                    }
                                                }
                                                if (b < baggageData.Length - 1)
                                                {
                                                    ticketReq.Append("},");
                                                }
                                                else
                                                {
                                                    ticketReq.Append("}");
                                                }
                                            }
                                        }
                                    }
                                    ticketReq.Append("]");
                                }


                                if (i < itinerary.Passenger.Length - 1)
                                {
                                    ticketReq.Append("},");//Pax End
                                }
                                else
                                {
                                    ticketReq.Append("}");
                                }
                            }

                            ticketReq.Append("]");//Passengers End
                            ticketReq.Append("}");


                            try
                            {

                                string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirTicketRequestJSON.json";
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                dynamic ticketRequest = js.Deserialize<dynamic>(ticketReq.ToString());
                                string rsp = JsonConvert.SerializeObject(ticketRequest, Newtonsoft.Json.Formatting.Indented);
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(rsp);
                                sw.Close();

                                XmlDocument doc1 = JsonConvert.DeserializeXmlNode(ticketReq.ToString(), "TicketRequest");
                                filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirTicketRequest.xml";
                                doc1.Save(filePath);
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save Ticket Request. Error" + ex.ToString(), "");
                            }

                            string response = GetResponse(ticketReq.ToString(), ticketUrl);

                            // Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing got response for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
                            try
                            {
                                string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirTicketResponseJSON.json";
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                dynamic ticketResponse = js.Deserialize<dynamic>(response);
                                string rsp = JsonConvert.SerializeObject(ticketResponse, Newtonsoft.Json.Formatting.Indented);
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(rsp);
                                sw.Close();

                                doc = JsonConvert.DeserializeXmlNode(response, "TicketResponse");
                                filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirTicketResponse.xml";
                                doc.Save(filePath);
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save Ticket Response. Error" + ex.ToString(), "");
                            }
                        }
                        else
                        {
                            throw new Exception("Agent balance and booking amount mismatch. Agent Bal: " + balance.CashBalance + " " + " Booking Amount: " + bookingAmount);
                        }
                    }
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                    throw new Exception("Failed to Authenticate for TBOAir");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to generate Ticket. Error : " + ex.ToString(), "");
                throw ex;
            }
            return doc;
        }
        /// <summary>
        /// To create ticket for Blocked PNR.Sending two ticket requests and merging response for combined results. this block option is only for
        /// GDS airlines.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="tickets"></param>
        /// <returns></returns>
        public TicketingResponse CreateTicket(ref FlightItinerary itinerary, out Ticket[] tickets)
        {
            TicketingResponse ticketingResponse = new TicketingResponse();
            tickets = new Ticket[0];
            try
            {
                //Sending two ticket requests and merging response for combined results.
                if (itinerary.PNR.Contains('|') && itinerary.AirLocatorCode.Contains('|')) //Nomral return for GDS ticket whick was hold. 
                {
                    string[] PNRs = itinerary.PNR.Split('|');
                    string[] AirLocatorCodes = itinerary.AirLocatorCode.Split('|');
                    List<Ticket> tempTicket = new List<Ticket>();
                    for (int i = 0; i < 2; i++)
                    {
                        itinerary.PNR = PNRs[i];
                        itinerary.AirLocatorCode = AirLocatorCodes[i];

                        ticketingResponse = TempCreateTicket(ref itinerary, ref tempTicket);

                    }
                    tickets = tempTicket.ToArray();
                    itinerary.PNR = PNRs[0] + "|" + PNRs[1];
                    itinerary.AirLocatorCode = AirLocatorCodes[0] + "|" + AirLocatorCodes[1];
                }
                else
                {
                    List<Ticket> tempTicket = new List<Ticket>();
                    ticketingResponse = TempCreateTicket(ref itinerary, ref tempTicket);
                    tickets = tempTicket.ToArray();
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to generate Ticket. Error : " + ex.ToString(), "");
                throw ex;
            }
            return ticketingResponse;
        }

        /// <summary>
        /// To Create block PNR request and get response for GDS airlines.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="tickets"></param>
        /// <returns></returns>
        private TicketingResponse TempCreateTicket(ref FlightItinerary itinerary, ref List<Ticket> tickets)
        {
            TicketingResponse ticketingResponse = new TicketingResponse();
            string errorMessage = string.Empty;

            try
            {
                Authenticate();
                if (ar != null)
                {
                    if (ar.Error.ErrorMessage == null || ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                    {
                        AgencyBalance balance = GetAgencyBalance(ar);

                        decimal bookingAmount = 0;

                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            if (pax.TBOPrice != null)
                            {
                                bookingAmount += pax.TBOPrice.PublishedFare;
                            }
                            else
                            {
                                bookingAmount += pax.Price.PublishedFare;
                            }
                        }

                        // Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing balance checking started for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                        if (balance != null && balance.CashBalance > bookingAmount)
                        {
                            //Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing started for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
                            StringBuilder requestData = new StringBuilder();
                            requestData.AppendLine("{");
                            requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                            requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                            requestData.AppendLine("\"TraceId\": \"" + itinerary.TicketAdvisory + "\",");
                            requestData.AppendLine("\"PNR\": \"" + itinerary.PNR + "\",");

                            requestData.AppendLine("\"BookingId\":" + itinerary.AirLocatorCode);
                            requestData.AppendLine("}");

                            XmlDocument doc = null;

                            try
                            {

                                string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirTicketRequestJSON.json";
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                dynamic ticketRequest = js.Deserialize<dynamic>(requestData.ToString());
                                string rsp = JsonConvert.SerializeObject(ticketRequest, Newtonsoft.Json.Formatting.Indented);
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(rsp);
                                sw.Close();

                                XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "TicketRequest");
                                filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirTicketRequest.xml";
                                doc1.Save(filePath);
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save Create Ticket Request. Error" + ex.ToString(), "");
                            }


                            string response = GetResponse(requestData.ToString(), ticketUrl);


                            //string response = File.ReadAllText(@"D:\temp\xmlLogs\tboair\b85055e7-21bc-4916-a84d-226c111bc1ee_4532_16022019_113124_TBOAirTicketResponseJSON.json");
                            //// end

                            //Audit.Add(EventType.TBOAir, Severity.Normal, 1, "(TBOAir)Ticketing response created for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                            try
                            {

                                string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirTicketResponseJSON.json";
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                dynamic ticketResponse = js.Deserialize<dynamic>(response);
                                string rsp = JsonConvert.SerializeObject(ticketResponse, Newtonsoft.Json.Formatting.Indented);
                                StreamWriter sw = new StreamWriter(filePath);
                                sw.Write(rsp);
                                sw.Close();

                                doc = JsonConvert.DeserializeXmlNode(response, "TicketResponse");//Temp ziyad test

                                filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirTicketResponse.xml";
                                doc.Save(filePath);
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save Create Ticket Response. Error" + ex.ToString(), "");
                            }

                            if (doc != null)
                            {

                                XmlNode errorNode = doc.SelectSingleNode("TicketResponse/Response/Error");
                                if (errorNode != null && errorNode.SelectSingleNode("ErrorMessage").InnerText.Length <= 0)
                                {
                                    XmlNode ResponseNode = doc.SelectSingleNode("TicketResponse/Response/Response");
                                    //itinerary.TourCode = ResponseNode.SelectSingleNode("BookingId").InnerText;  
                                    if (ResponseNode.SelectSingleNode("Message") != null)
                                    {
                                        errorMessage = ResponseNode.SelectSingleNode("Message").InnerText;
                                    }
                                    if (string.IsNullOrEmpty(errorMessage))
                                    {
                                        //Update segment timings and terminals. Added by shiva 14 Jan 2019
                                        XmlNode timeChanged = ResponseNode.SelectSingleNode("IsTimeChanged");
                                        bool isTimeChanged = false;
                                        if (timeChanged != null)
                                        {
                                            isTimeChanged = Convert.ToBoolean(timeChanged.InnerText);
                                        }
                                        //Lokesh:29-May-2017
                                        //Verify IsPriceChanged node -- if true (fail the ticketing response)
                                        //Verify the <Ticket> node also -- if null fail the ticketing response.
                                        if (ResponseNode.SelectSingleNode("IsPriceChanged") != null && ResponseNode.SelectSingleNode("IsPriceChanged").InnerText.Length > 0 && ResponseNode.SelectSingleNode("IsPriceChanged").InnerText.ToLower() == "false")
                                        {
                                            if (ResponseNode.SelectSingleNode("PNR") != null && ResponseNode.SelectSingleNode("FlightItinerary") != null)
                                            {
                                                itinerary.PNR = ResponseNode.SelectSingleNode("PNR").InnerText;//Read PNR from TicketResponse
                                                ticketingResponse.Status = TicketingResponseStatus.Successful;

                                                for (int i = 0; i < itinerary.Passenger.Length; i++)
                                                {
                                                    bool used = false;
                                                    FlightPassenger pax = itinerary.Passenger[i];
                                                    Ticket ticket = null;
                                                    if (tickets.Count > 0)
                                                    {
                                                        ticket = tickets.Find(delegate (Ticket t) { return t.PaxFirstName == pax.FirstName && t.PaxLastName == pax.LastName && t.Price.PriceId == pax.Price.PriceId; });
                                                        if (ticket == null)
                                                        {
                                                            ticket = new Ticket();
                                                        }
                                                        else
                                                        {
                                                            used = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ticket = new Ticket();
                                                    }

                                                    ticket.ConjunctionNumber = "";
                                                    ticket.CorporateCode = "";
                                                    ticket.CreatedBy = itinerary.CreatedBy;
                                                    ticket.CreatedOn = DateTime.Now;
                                                    ticket.Endorsement = "";
                                                    ticket.ETicket = true;
                                                    ticket.FareCalculation = "";
                                                    ticket.FareRule = "";
                                                    ticket.FlightId = itinerary.FlightId;
                                                    ticket.IssueDate = DateTime.Now;
                                                    ticket.LastModifiedBy = itinerary.CreatedBy;
                                                    ticket.PaxFirstName = itinerary.Passenger[i].FirstName;
                                                    ticket.PaxId = itinerary.Passenger[i].PaxId;
                                                    ticket.PaxLastName = itinerary.Passenger[i].LastName;
                                                    ticket.PaxType = itinerary.Passenger[i].Type;
                                                    ticket.Price = itinerary.Passenger[i].Price;
                                                    ticket.Status = "Ticketed";
                                                    ticket.TicketAdvisory = "";
                                                    ticket.TicketDesignator = "";

                                                    try
                                                    {
                                                        string currency = ResponseNode.SelectSingleNode("FlightItinerary/Fare/Currency").InnerText;
                                                        if (exchangeRates.ContainsKey(currency))
                                                        {
                                                            rateOfExchange = exchangeRates[currency];
                                                        }
                                                        else { rateOfExchange = 1; }
                                                        List<KeyValuePair<string, decimal>> taxBreakUp = new List<KeyValuePair<string, decimal>>();
                                                        XmlNodeList Passengers = ResponseNode.SelectNodes("FlightItinerary/Passenger");
                                                        foreach (XmlNode fpax in Passengers)
                                                        {
                                                            if (pax.PassportNo == fpax.SelectSingleNode("PassportNo").InnerText || pax.FirstName + pax.LastName == fpax.SelectSingleNode("FirstName").InnerText + fpax.SelectSingleNode("LastName").InnerText)
                                                            {
                                                                XmlNodeList TaxBreakups = fpax.SelectNodes("Fare/TaxBreakup");
                                                                foreach (XmlNode taxBreakup in TaxBreakups)
                                                                {
                                                                    if (taxBreakup.SelectSingleNode("key").InnerText.ToLower() != "totaltax")
                                                                    {
                                                                        taxBreakUp.Add(new KeyValuePair<string, decimal>((taxBreakup.SelectSingleNode("key").InnerText.ToLower() == "yqtax" ? "YQ" : taxBreakup.SelectSingleNode("key").InnerText), Convert.ToDecimal(taxBreakup.SelectSingleNode("value").InnerText) * rateOfExchange));
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ticket.TaxBreakup = taxBreakUp;
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to read TaxBreakup from Ticket response. Reason: " + ex.ToString(), "");
                                                    }

                                                    XmlNode PaxNode = ResponseNode.SelectNodes("FlightItinerary/Passenger")[i];
                                                    if (PaxNode != null && PaxNode.SelectSingleNode("Ticket") != null && PaxNode.SelectSingleNode("Ticket").SelectSingleNode("TicketNumber") != null)
                                                    {
                                                        if (ticket.TicketNumber != null && ticket.TicketNumber.Length > 0)
                                                        {
                                                            if (PaxNode.SelectSingleNode("Ticket").SelectSingleNode("ValidatingAirline") != null)
                                                            {
                                                                ticket.TicketNumber += "|" + PaxNode.SelectSingleNode("Ticket").SelectSingleNode("ValidatingAirline").InnerText + PaxNode.SelectSingleNode("Ticket").SelectSingleNode("TicketNumber").InnerText;
                                                            }
                                                            else
                                                            {
                                                                ticket.TicketNumber += "|" + PaxNode.SelectSingleNode("Ticket").SelectSingleNode("TicketNumber").InnerText;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (PaxNode.SelectSingleNode("Ticket").SelectSingleNode("ValidatingAirline") != null)
                                                            {
                                                                ticket.TicketNumber = PaxNode.SelectSingleNode("Ticket").SelectSingleNode("ValidatingAirline").InnerText + PaxNode.SelectSingleNode("Ticket").SelectSingleNode("TicketNumber").InnerText;
                                                            }
                                                            else
                                                            {
                                                                ticket.TicketNumber = PaxNode.SelectSingleNode("Ticket").SelectSingleNode("TicketNumber").InnerText;
                                                            }
                                                        }

                                                        {
                                                            ticket.StockType = PaxNode.SelectSingleNode("Ticket").SelectSingleNode("TicketId").InnerText;//Store TicketId for Cancellation
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //ticket.TicketNumber = itinerary.PNR;

                                                        //Lokesh : 29-May-2017
                                                        //For a particular pax if there are no ticket details means ticket node null then fail the ticketing response and break.
                                                        ticket = new Ticket();
                                                        ticketingResponse.Status = TicketingResponseStatus.OtherError;
                                                        ticketingResponse.Message = "Ticket details missing from Ticket Response";

                                                        break;
                                                    }

                                                    ticket.TicketType = "";
                                                    ticket.Title = itinerary.Passenger[i].Title;

                                                    ticket.PtcDetail = new List<SegmentPTCDetail>();

                                                    for (int j = 0; j < itinerary.Segments.Length; j++)
                                                    {
                                                        SegmentPTCDetail ptcDetail = new SegmentPTCDetail();
                                                        FlightInfo segment = itinerary.Segments[j];
                                                        XmlNodeList BaggageNodes = ResponseNode.SelectNodes("FlightItinerary/Passenger")[i].SelectNodes("SegmentAdditionalInfo");

                                                        if (BaggageNodes != null)
                                                        {
                                                            if (BaggageNodes[0] != null && BaggageNodes[0].SelectSingleNode("Baggage").InnerText.Length == 0)
                                                            {
                                                                ptcDetail.Baggage = "";
                                                            }
                                                            else
                                                            {
                                                                ptcDetail.Baggage = (BaggageNodes[0] != null) ? BaggageNodes[0].SelectSingleNode("Baggage").InnerText : string.Empty;
                                                            }
                                                            ptcDetail.FareBasis = (BaggageNodes[0] != null) ? BaggageNodes[0].SelectSingleNode("FareBasis").InnerText : string.Empty;
                                                        }
                                                        else
                                                        {
                                                            ptcDetail.Baggage = "";
                                                            ptcDetail.FareBasis = "";
                                                        }
                                                        ptcDetail.FlightKey = segment.FlightKey;
                                                        ptcDetail.NVA = "";
                                                        ptcDetail.NVB = "";
                                                        ptcDetail.SegmentId = segment.SegmentId;
                                                        switch (itinerary.Passenger[i].Type)
                                                        {
                                                            case PassengerType.Adult:
                                                                ptcDetail.PaxType = "ADT";
                                                                break;
                                                            case PassengerType.Child:
                                                                ptcDetail.PaxType = "CNN";
                                                                break;
                                                            case PassengerType.Infant:
                                                                ptcDetail.PaxType = "INF";
                                                                break;
                                                        }

                                                        ticket.PtcDetail.Add(ptcDetail);
                                                    }

                                                    //Update AirlinePNR
                                                    XmlNodeList Segments = ResponseNode.SelectNodes("FlightItinerary/Segments");
                                                    //Update AirlinePNR, segment timings and terminals. Added by shiva 14 Jan 2019
                                                    foreach (FlightInfo seg in itinerary.Segments)
                                                    {
                                                        foreach (XmlNode segment in Segments)
                                                        {
                                                            if (seg.Origin.AirportCode == segment.SelectSingleNode("Origin/Airport/AirportCode").InnerText)
                                                            {
                                                                if (segment.SelectSingleNode("Origin/Airport/Terminal") != null)
                                                                {
                                                                    seg.DepTerminal = segment.SelectSingleNode("Origin/Airport/Terminal").InnerText;
                                                                }
                                                                if (segment.SelectSingleNode("AirlinePNR") != null && segment.SelectSingleNode("AirlinePNR").InnerText.Length > 0)
                                                                {
                                                                    seg.AirlinePNR = segment.SelectSingleNode("AirlinePNR").InnerText;
                                                                }
                                                                if (isTimeChanged && segment.SelectSingleNode("Origin/DepTime") != null)
                                                                {
                                                                    seg.DepartureTime = Convert.ToDateTime(segment.SelectSingleNode("Origin/DepTime").InnerText);
                                                                }
                                                                seg.Save();
                                                            }
                                                            if (seg.Destination.AirportCode == segment.SelectSingleNode("Destination/Airport/AirportCode").InnerText)
                                                            {
                                                                if (segment.SelectSingleNode("Destination/Airport/Terminal") != null)
                                                                {
                                                                    seg.ArrTerminal = segment.SelectSingleNode("Destination/Airport/Terminal").InnerText;
                                                                }
                                                                if (segment.SelectSingleNode("AirlinePNR") != null && segment.SelectSingleNode("AirlinePNR").InnerText.Length > 0)
                                                                {
                                                                    seg.AirlinePNR = segment.SelectSingleNode("AirlinePNR").InnerText;
                                                                }
                                                                if (isTimeChanged && segment.SelectSingleNode("Destination/ArrTime") != null)
                                                                {
                                                                    seg.ArrivalTime = Convert.ToDateTime(segment.SelectSingleNode("Destination/ArrTime").InnerText);
                                                                }
                                                                seg.Save();
                                                            }
                                                        }
                                                    }

                                                    if (!used)
                                                    {
                                                        tickets.Add(ticket);
                                                    }
                                                }
                                                Audit.Add(EventType.TBOAir, Severity.Normal, appUserId, "(TBOAir)Ticketing Successful PNR " + itinerary.PNR + " for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
                                            }
                                        }
                                        else
                                        {
                                            ticketingResponse.Status = TicketingResponseStatus.PriceChanged;
                                            ticketingResponse.Message = "Price has been changed for the ticket.";

                                            Audit.Add(EventType.TBOAir, Severity.Normal, appUserId, "(TBOAir)Ticketing Failed for PNR " + itinerary.PNR + " as Ticket details are missing in response for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                                        }
                                    }
                                    else
                                    {
                                        if (errorMessage.Contains("ticket") && errorMessage.Contains("not allowed"))
                                        {
                                            ticketingResponse.Status = TicketingResponseStatus.NotAllowed;
                                        }
                                        else
                                        {
                                            ticketingResponse.Status = TicketingResponseStatus.NotCreated;
                                        }
                                        ticketingResponse.Message = errorMessage;
                                        Audit.Add(EventType.TBOAir, Severity.Normal, appUserId, "(TBOAir)Ticketing Failed for PNR " + itinerary.PNR + " as " + errorMessage + " for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");

                                    }
                                }
                                else
                                {
                                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Generate Ticket. " + (errorNode != null ? errorNode.SelectSingleNode("ErrorMessage").InnerText : ""), "");
                                    ticketingResponse.Status = TicketingResponseStatus.NotCreated;
                                    ticketingResponse.Message = errorNode.SelectSingleNode("ErrorMessage").InnerText;
                                }
                            }
                            else
                            {
                                ticketingResponse.Status = TicketingResponseStatus.NotSaved;
                                ticketingResponse.Message = "Ticketed response returned empty or corrupted from TBO Air.";
                            }
                        }
                        else
                        {
                            ticketingResponse.Status = TicketingResponseStatus.OtherError;
                            ticketingResponse.Message = "Agent balance and booking amount mismatch. Agent Bal: " + balance.CashBalance + " " + "Booking Amount: " + bookingAmount;
                        }
                    }
                }
                else
                {
                    ticketingResponse.Message = "Agent profile retrieve failed from TBO Air.";
                    ticketingResponse.Status = TicketingResponseStatus.OtherError;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to generate Ticket. Reason : " + ex.ToString(), "");
                ticketingResponse.Message = ex.Message;
                ticketingResponse.Status = TicketingResponseStatus.OtherError;
                throw ex;
            }
            return ticketingResponse;
        }

        /// <summary>
        /// Get Booking Details
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="tickets"></param>
        protected void GetBookingDetails(FlightItinerary itinerary, ref Ticket[] tickets)
        {
            try
            {
                Authenticate();
                if (ar != null)
                {
                    if (ar.Error.ErrorMessage == null || ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                    {
                        StringBuilder requestData = new StringBuilder();
                        requestData.AppendLine("{");
                        requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        requestData.AppendLine("\"TraceId\": \"" + itinerary.TicketAdvisory + "\",");
                        requestData.AppendLine("\"PNR\": \"" + itinerary.PNR + "\",");
                        requestData.AppendLine("\"BookingId\":" + itinerary.AirLocatorCode);
                        requestData.AppendLine("}");

                        try
                        {
                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetBookingDetailsRequestJSON.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic getBookRequest = js.Deserialize<dynamic>(requestData.ToString());
                            string rsp = JsonConvert.SerializeObject(getBookRequest, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "GetBookingRequest");
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetBookingDetailsRequest.xml";
                            doc1.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save GetBooking Request. Error" + ex.ToString(), "");
                        }
                        string response = GetResponse(requestData.ToString(), getBookingDetailUrl);
                        XmlDocument doc = null;

                        try
                        {
                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetBookingDetailsResponseJSON.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic getBookResponse = js.Deserialize<dynamic>(response);
                            string rsp = JsonConvert.SerializeObject(getBookResponse, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(requestData.ToString(), "GetBookingDetailRequest");
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirGetBookingDetailsResponse.xml";
                            doc.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to save GetBooking Response. Error" + ex.ToString(), "");
                        }

                        if (doc != null)
                        {
                            XmlNode ErrorNode = doc.SelectSingleNode("Response/Error");

                            if (ErrorNode != null && ErrorNode.SelectSingleNode("ErrorCode").InnerText == "0")
                            {
                                XmlNode ItineraryNode = doc.SelectSingleNode("Response/FlightItinerary");

                                for (int i = 0; i < ItineraryNode.SelectNodes("Passenger").Count; i++)
                                {
                                    XmlNode BaggageNode = ItineraryNode.SelectNodes("Passenger")[i].SelectSingleNode("Baggage");
                                    foreach (SegmentPTCDetail ptcDetail in tickets[i].PtcDetail)
                                    {
                                        ptcDetail.Baggage = BaggageNode.SelectSingleNode("Weight").InnerText + "Kg," + BaggageNode.SelectSingleNode("Weight").InnerText + "Kg";
                                        ptcDetail.FareBasis = BaggageNode.SelectSingleNode("Code").InnerText;
                                        ptcDetail.FlightKey = "";
                                        switch (ItineraryNode.SelectNodes("Passenger")[i].SelectSingleNode("PaxType").InnerText)
                                        {
                                            case "1":
                                                ptcDetail.PaxType = "ADT";
                                                break;
                                            case "2":
                                                ptcDetail.PaxType = "CNN";
                                                break;
                                            case "3":
                                                ptcDetail.PaxType = "INF";
                                                break;
                                        }
                                        ptcDetail.NVA = "";
                                        ptcDetail.NVB = "";
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air)Failed to get Booking details. Error : " + ex.ToString(), "");
                throw ex;
            }
        }


        /// <summary>
        /// This method is used to Release PNR for HOLD bookings (GDS).
        /// </summary>
        /// <param name="bookingId"></param>
        /// <param name="sources"></param>
        /// <returns></returns>
        public bool ReleasePNR(int bookingId, string sources)
        {
            bool Released = false;
            try
            {
                Authenticate();
                if (ar != null)
                {
                    if (ar.Error.ErrorMessage == null || ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                    {
                        StringBuilder requestData = new StringBuilder();
                        requestData.AppendLine("{");
                        requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        requestData.AppendLine("\"BookingId\": \"" + bookingId + "\",");
                        requestData.AppendLine("\"Source\":\"" + sources + "\"");
                        requestData.AppendLine("}");

                        try
                        {

                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirReleasePNRRequestJSON.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic releaseRequest = js.Deserialize<dynamic>(requestData.ToString());
                            string rsp = JsonConvert.SerializeObject(releaseRequest, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "ReleasePNRRequest");
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirReleasePNRRequest.xml";
                            doc1.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to Release PNR Request. Error" + ex.ToString(), "");
                        }
                        string response = GetResponse(requestData.ToString(), releasePNRUrl);
                        XmlDocument doc = null;

                        try
                        {

                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirReleasePNRResponseJSON.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic releaseResponse = js.Deserialize<dynamic>(response);
                            string rsp = JsonConvert.SerializeObject(releaseResponse, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(response.ToString(), "ReleasePNRResponse");
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirReleasePNRResponse.xml";
                            doc.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to Release PNR Response. Error" + ex.ToString(), "");
                        }

                        if (doc != null)
                        {
                            XmlNode errorNode = doc.SelectSingleNode("ReleasePNRResponse/Response/Error");
                            if (errorNode != null && errorNode.SelectSingleNode("ErrorCode").InnerText == "0")
                            {
                                if (doc.SelectSingleNode("ReleasePNRResponse/Response/ResponseStatus").InnerText == "1")//Successful
                                {
                                    Released = true;
                                }
                                else //Failed
                                {
                                    Released = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Release PNR for BookingId : " + bookingId + ". Error : " + ex.ToString(), "");
            }
            return Released;
        }

        /// <summary>
        /// Method is incomplete. still coding need to done
        /// </summary>
        /// <param name="itinerary"></param>
        public Dictionary<string, string> SendChangeRequest(FlightItinerary itinerary)
        {
            Dictionary<string, string> cancellationData = new Dictionary<string, string>();
            bool mainCancelStatus = true;
            decimal totalCancelAmount = 0;
            try
            {
                Authenticate();
                if (ar != null)
                {
                    if (ar.Error.ErrorMessage == null || ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                    {
                        string[] bookingIds = new string[0];
                        if (!string.IsNullOrEmpty(itinerary.AirLocatorCode))//MultipleBookind id's
                        {
                            bookingIds = itinerary.AirLocatorCode.TrimEnd('|').Split('|');
                        }
                        List<Ticket> ticketList = CT.BookingEngine.Ticket.GetTicketList(itinerary.FlightId);
                        ///////////////////////////////////////////////////////////////////////////////////////////
                        ////////////      Booking Id's wise need send Changes Request    //////////////////////////
                        ////////////      TicketId Need To send Comma Separted Values     /////////////////////////
                        ////////////      All Booking id's confirm that time only Booking stats changed  /////////
                        //////////////////////////////////////////////////////////////////////////////////////////

                        string[] ticketIds = new string[0];
                        string ticketId = string.Empty;
                        for (int i = 0; i < bookingIds.Length; i++)
                        {

                            for (int k = 0; k < ticketList.Count; k++)
                            {
                                if (!string.IsNullOrEmpty(ticketList[k].StockType))
                                {
                                    ticketIds = ticketList[k].StockType.Split('|');
                                }

                                if (bookingIds.Length == ticketIds.Length)
                                {
                                    if (string.IsNullOrEmpty(ticketId))
                                    {
                                        ticketId = ticketIds[i];
                                    }
                                    else
                                    {
                                        ticketId = ticketId + "," + ticketIds[i];
                                    }
                                }
                            }
                            string bookingId = bookingIds[i];
                            DataTable dtCancelIds = ChangeRequestStatus.LoadCancelIds(Convert.ToInt64(bookingId));
                            if (dtCancelIds == null || dtCancelIds.Rows.Count <= 0)
                            {
                                StringBuilder requestData = new StringBuilder();
                                requestData.AppendLine("{");
                                requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                                requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                                requestData.AppendLine("\"BookingId\": " + bookingId + ",");
                                requestData.AppendLine("\"RequestType\":" + 1 + ",");
                                requestData.AppendLine("\"CancellationType\":" + 2 + ",");
                                //requestData.AppendLine("\"TicketId\":[" + ticketId + "],");
                                requestData.AppendLine("\"Remarks\":\"" + "Test" + "\"");
                                requestData.AppendLine("}");

                                try
                                {

                                    string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirChangeRequestJSON.json";
                                    JavaScriptSerializer js = new JavaScriptSerializer();
                                    dynamic changeRequest = js.Deserialize<dynamic>(requestData.ToString());
                                    string rsp = JsonConvert.SerializeObject(changeRequest, Newtonsoft.Json.Formatting.Indented);
                                    StreamWriter sw = new StreamWriter(filePath);
                                    sw.Write(rsp);
                                    sw.Close();

                                    XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "SendChangeRequest");
                                    filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirChangeRequest.xml";
                                    doc1.Save(filePath);
                                }
                                catch (Exception ex)
                                {
                                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to Get Change Request. Error" + ex.ToString(), "");
                                }
                                string response = GetResponse(requestData.ToString(), changeRequestUrl);
                                XmlDocument doc = null;

                                try
                                {

                                    string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOChangeResponseJSON.json";
                                    JavaScriptSerializer js = new JavaScriptSerializer();
                                    dynamic changeRequest = js.Deserialize<dynamic>(response);
                                    string rsp = JsonConvert.SerializeObject(changeRequest, Newtonsoft.Json.Formatting.Indented);
                                    StreamWriter sw = new StreamWriter(filePath);
                                    sw.Write(rsp);
                                    sw.Close();

                                    doc = JsonConvert.DeserializeXmlNode(response.ToString(), "SendChangeResponse");
                                    filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirChangeResponse.xml";
                                    doc.Save(filePath);
                                }
                                catch (Exception ex)
                                {
                                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to Changerequest Response. Error" + ex.ToString(), "");
                                }
                                if (doc != null)
                                {
                                    if (doc.SelectSingleNode("SendChangeResponse/Response/ResponseStatus").InnerText == "1")//Successful
                                    {
                                        XmlNodeList changeList = doc.SelectNodes("SendChangeResponse/Response/TicketCRInfo");
                                        if (changeList != null)
                                        {
                                            //ChangeRequestIds giving multiples but need to send and one by one
                                            int k = 0;
                                            foreach (XmlNode change in changeList)//ChangeRequestId's giving  List only
                                            {
                                                XmlNode ChangeStatus = change.SelectSingleNode("ChangeRequestStatus");
                                                if (ChangeStatus == null)
                                                {
                                                    ChangeStatus = change.SelectSingleNode("Status");
                                                }
                                                //if (ChangeStatus != null && ChangeStatus.InnerText == "1") //1 is success
                                                if (ChangeStatus != null && (ChangeStatus.InnerText == "1" || ChangeStatus.InnerText == "4")) //1 is success, 4 is completed (LCC)
                                                {
                                                    long cancelId = 0;
                                                    XmlNode changeRequestId = change.SelectSingleNode("ChangeRequestId");
                                                    if (changeRequestId != null)
                                                    {
                                                        cancelId = Convert.ToInt64(changeRequestId.InnerText);
                                                    }
                                                    string[] tkts = ticketId.Split(',');
                                                    long tickId = 0;
                                                    if (tkts != null && tkts.Length > 0)
                                                    {
                                                        tickId = Convert.ToInt64(tkts[k]);
                                                    }
                                                    if (ticketList.Find(t => t.StockType == change.SelectSingleNode("TicketId").InnerText) == null)
                                                    {
                                                        ticketList[k].StockType = change.SelectSingleNode("TicketId").InnerText;
                                                        ticketList[k].Save();
                                                    }
                                                    bool canceled = false;
                                                    decimal canceledAmount = 0;
                                                    ChangeRequestStatus changeRequestobj = new ChangeRequestStatus();
                                                    changeRequestobj.CancelId = cancelId;
                                                    changeRequestobj.TicketId = tickId;
                                                    changeRequestobj.Status = "P";
                                                    changeRequestobj.BookingId = Convert.ToInt64(bookingId);
                                                    changeRequestobj.Save();
                                                    canceled = GetChangeRequestStatus(cancelId, tickId, ref canceledAmount);
                                                    if (!canceled)
                                                    {
                                                        mainCancelStatus = false;
                                                    }
                                                    else
                                                    {
                                                        totalCancelAmount += canceledAmount;
                                                    }
                                                }
                                                else
                                                {
                                                    mainCancelStatus = false;
                                                }
                                                k++;
                                            }
                                        }
                                        else
                                        {
                                            mainCancelStatus = false;
                                        }
                                    }
                                    else //Failed
                                    {
                                        mainCancelStatus = false;
                                    }
                                }
                                else
                                {
                                    mainCancelStatus = false;
                                }
                            }
                            else
                            {
                                foreach (DataRow dr in dtCancelIds.Rows)
                                {
                                    decimal canceledAmount = 0;
                                    if (Convert.ToString(dr["cancelStatus"]) != "C")
                                    {
                                        bool canceled = false;
                                        canceled = GetChangeRequestStatus(Convert.ToInt64(dr["changeRequestId"]), Convert.ToInt64(dr["ticketId"]), ref canceledAmount);
                                        if (!canceled)
                                        {
                                            mainCancelStatus = false;
                                        }
                                        else
                                        {
                                            totalCancelAmount += canceledAmount;
                                        }
                                    }
                                    else
                                    {
                                        totalCancelAmount += Convert.ToDecimal(dr["cancelAmount"]);
                                    }
                                }
                            }
                        }
                        if (mainCancelStatus)
                        {
                            rateOfExchange = exchangeRates["INR"];
                            cancellationData.Add("Cancelled", "True");
                            cancellationData.Add("Charges", (Convert.ToDecimal(totalCancelAmount) * rateOfExchange).ToString());
                        }
                    }
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                }
            }
            catch (Exception ex)
            {
                cancellationData = new Dictionary<string, string>();
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to SendChangeRequest for PNR : " + itinerary.PNR + ". Error : " + ex.ToString(), "");
            }
            return cancellationData;
        }

        /// <summary>
        /// Method to return the ChangeRequest status requested earlier
        /// </summary>
        /// <param name="cancelId"></param>
        /// <param name="tickId"></param>
        /// <param name="canceledAmount"></param>
        /// <returns></returns>
        public bool GetChangeRequestStatus(long cancelId, long tickId, ref decimal canceledAmount)
        {
            bool canceled = false;
            try
            {
                Authenticate();
                if (ar != null)
                {
                    if (ar.Error.ErrorMessage == null || ar.Error.ErrorMessage != null && ar.Error.ErrorMessage.Length <= 0)
                    {
                        StringBuilder requestData = new StringBuilder();
                        requestData.AppendLine("{");
                        requestData.AppendLine("\"EndUserIp\": \"" + endUserIp + "\",");
                        requestData.AppendLine("\"TokenId\": \"" + ar.TokenId + "\",");
                        requestData.AppendLine("\"ChangeRequestId\": \"" + cancelId + "\"");
                        requestData.AppendLine("}");

                        try
                        {
                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirChangeRequestStstusJSON.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic changeRequest = js.Deserialize<dynamic>(requestData.ToString());
                            string rsp = JsonConvert.SerializeObject(changeRequest, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            XmlDocument doc1 = JsonConvert.DeserializeXmlNode(requestData.ToString(), "GetChangeRequest");
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirChangeRequestStatus.xml";
                            doc1.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to Get Change RequestStstus. Error" + ex.ToString(), "");
                        }
                        string response = GetResponse(requestData.ToString(), getChangeRequestStatusUrl);
                        XmlDocument doc = null;

                        try
                        {
                            string filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOChangeResponseStatusJSON.json";
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            dynamic changeResponse = js.Deserialize<dynamic>(response);
                            string rsp = JsonConvert.SerializeObject(changeResponse, Newtonsoft.Json.Formatting.Indented);
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.Write(rsp);
                            sw.Close();

                            doc = JsonConvert.DeserializeXmlNode(response.ToString(), "GetChangeResponse");
                            filePath = XmlPath + sessionId + "_" + appUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + "_TBOAirChangeResponseStatus.xml";
                            doc.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBO Air) Failed to Changerequest Response Status. Error" + ex.ToString(), "");
                        }
                        if (doc != null)
                        {
                            if (doc.SelectSingleNode("GetChangeResponse/Response/ResponseStatus").InnerText == "1")//Successful
                            {
                                XmlNode requestStatus = doc.SelectSingleNode("GetChangeResponse/Response/ChangeRequestStatus");
                                if (requestStatus != null && requestStatus.InnerText == "4")//Completed
                                {
                                    canceled = true;
                                    XmlNode cancelCharge = doc.SelectSingleNode("GetChangeResponse/Response/CancellationCharge");
                                    if (cancelCharge != null)
                                    {
                                        canceledAmount += Convert.ToDecimal(cancelCharge.InnerText);
                                    }
                                    XmlNode serviceTax = doc.SelectSingleNode("GetChangeResponse/Response/ServiceTaxOnRAF");
                                    if (serviceTax != null)
                                    {
                                        canceledAmount += Convert.ToDecimal(serviceTax.InnerText);
                                    }
                                    XmlNode swachhBharatCess = doc.SelectSingleNode("GetChangeResponse/Response/SwachhBharatCess");
                                    if (swachhBharatCess != null)
                                    {
                                        canceledAmount += Convert.ToDecimal(swachhBharatCess.InnerText);
                                    }
                                    ChangeRequestStatus changeRequestobj = new ChangeRequestStatus();
                                    changeRequestobj.CancelId = cancelId;
                                    changeRequestobj.TicketId = tickId;
                                    changeRequestobj.Status = "C";
                                    changeRequestobj.CancelAmount = canceledAmount;
                                    changeRequestobj.Remarks = "Completed";

                                    changeRequestobj.Save();
                                }
                                else
                                {
                                    string remarks = string.Empty;
                                    if (requestStatus != null)
                                    {
                                        if (requestStatus.InnerText == "0")
                                        {
                                            remarks = "NotSet";
                                        }
                                        else if (requestStatus.InnerText == "1")
                                        {
                                            remarks = "Unassigned";
                                        }
                                        else if (requestStatus.InnerText == "2")
                                        {
                                            remarks = "Assigned";
                                        }
                                        else if (requestStatus.InnerText == "3")
                                        {
                                            remarks = "Acknowledged";
                                        }
                                        else if (requestStatus.InnerText == "5")
                                        {
                                            remarks = "Rejected";
                                        }
                                        else if (requestStatus.InnerText == "6")
                                        {
                                            remarks = "Closed";
                                        }
                                        else if (requestStatus.InnerText == "7")
                                        {
                                            remarks = "Pending";
                                        }
                                        else if (requestStatus.InnerText == "8")
                                        {
                                            remarks = "Other";
                                        }
                                    }
                                    ChangeRequestStatus changeRequestobj = new ChangeRequestStatus();
                                    changeRequestobj.CancelId = cancelId;
                                    changeRequestobj.TicketId = tickId;
                                    changeRequestobj.Status = "P";
                                    changeRequestobj.CancelAmount = canceledAmount;
                                    changeRequestobj.Remarks = remarks;
                                    changeRequestobj.Save();
                                }
                            }
                        }
                    }
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to Authenticate for TBOAir", "");
                }
            }
            catch (Exception ex)
            {
                canceled = false;
                Audit.Add(EventType.TBOAir, Severity.High, appUserId, "(TBOAir)Failed to SendChangeRequestStatus for TicketId : " + tickId + ". Error : " + ex.ToString(), "");
            }
            return canceled;
        }

        public bool ValidatePrefAirline(string sAgentPref, string[] sUserPref, string sAirline)
        {
            bool isValid = true;

            if (!string.IsNullOrEmpty(sAgentPref))
                isValid = sAgentPref.Split(',').Where(x => x.ToUpper() == sAirline.ToUpper()).Count() > 0;

            if (isValid && sUserPref != null && sUserPref.Length > 0)
                isValid = sUserPref.Where(x => x.ToUpper() == sAirline.ToUpper()).Count() > 0;
            
            return isValid;
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls
        /// <summary>
        /// Base Overridden method for dispose
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }
        /// <summary>
        /// Disposer for Object
        /// </summary>
        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~AirV10()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        /// <summary>
        /// Dispose implementation
        /// </summary>
        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }

    /// <summary>
    /// Class for generating xml/json authenticate request
    /// </summary>
    public class AuthenticationRequest
    {
        /// <summary>
        /// ClientId
        /// </summary>
        public string ClientId;
        /// <summary>
        /// UserName
        /// </summary>
        public string UserName;
        /// <summary>
        /// Password
        /// </summary>
        public string Password;
        /// <summary>
        /// EndUserIP
        /// </summary>
        public string EndUserIP;
    }

    /// <summary>
    /// Class for reading Authentication response and to serialize xml/json.
    /// </summary>
    public class AuthenticationResponse
    {
        /// <summary>
        /// Status
        /// </summary>
        public string Status;
        /// <summary>
        /// TokenId
        /// </summary>
        public string TokenId;
        /// <summary>
        /// Error
        /// </summary>
        public Error Error;
        /// <summary>
        /// Member
        /// </summary>
        public Member Member;
    }

    /// <summary>
    /// Struct for reading Error information from the response
    /// </summary>
    public struct Error
    {
        /// <summary>
        /// 
        /// </summary>
        public string ErrorCode;
        /// <summary>
        /// 
        /// </summary>
        public string ErrorMessage;
    }

    /// <summary>
    /// Internal class of AuthenticationRequest or Authentication response
    /// </summary>
    public struct Member
    {
        /// <summary>
        /// FirstName
        /// </summary>
        public string FirstName;
        /// <summary>
        /// LastName
        /// </summary>
        public string LastName;
        /// <summary>
        /// Email
        /// </summary>
        public string Email;
        /// <summary>
        /// MemberId
        /// </summary>
        public int MemberId;
        /// <summary>
        /// AgencyId
        /// </summary>
        public int AgencyId;
        /// <summary>
        /// LoginName
        /// </summary>
        public string LoginName;
        /// <summary>
        /// LoginDetails
        /// </summary>
        public string LoginDetails;
        /// <summary>
        /// IsPrimaryAgent
        /// </summary>
        public bool IsPrimaryAgent;
    }

    /// <summary>
    /// 
    /// </summary>
    public class AgencyBalance
    {
        /// <summary>
        /// AgencyType
        /// </summary>
        public int AgencyType;
        /// <summary>
        /// CashBalance
        /// </summary>
        public decimal CashBalance;
        /// <summary>
        /// CreditBalance
        /// </summary>
        public decimal CreditBalance;
        /// <summary>
        /// Error
        /// </summary>
        public Error Error;
        /// <summary>
        /// Status
        /// </summary>
        public int Status;
    }


    
}
