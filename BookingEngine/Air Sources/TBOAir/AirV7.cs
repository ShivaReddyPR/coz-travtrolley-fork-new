﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBOAir.TekTravelAir;
using CT.Configuration;
using CT.BookingEngine;
using CT.Core;
using System.IO;
using System.Web;
using System.Net;
using System.IO.Compression;
using Newtonsoft.Json;
using System.Xml;
using System.Xml.Serialization;

namespace TBOAir
{
    public class AirV7
    {
        protected BookingAPI ba = new BookingAPI();
        protected AuthenticationData authData = new AuthenticationData();
        protected string XmlPath = string.Empty;
        string loginName = string.Empty;
        string password = string.Empty;
       

        string agentBaseCurrency;
        Dictionary<string, decimal> exchangeRates;
        decimal rateOfExchange = 1;
        int decimalValue = 3;
        bool testMode = true;
        string sessionId;
        int appUserId;

        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }

        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }

        public string LoginName
        {
            get { return loginName; }
            set { loginName = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string AgentBaseCurrency
        {
            get { return agentBaseCurrency; }
            set { agentBaseCurrency = value; }
        }

        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public int AgentDecimalValue
        {
            get { return decimalValue; }
            set { decimalValue = value; }
        }

        public AirV7()
        {
            #region V7.0 Code
            //authData.UserName = ConfigurationSystem.TBOAirConfig["UserName"];
            //authData.Password = ConfigurationSystem.TBOAirConfig["Password"];
            //authData.SiteName = "";
            //authData.AccountCode = "";
            //ba.AuthenticationDataValue = authData;
            #endregion
            XmlPath = ConfigurationSystem.TBOAirConfig["XmlLogPath"];
            if (!Directory.Exists(XmlPath))
            {
                Directory.CreateDirectory(XmlPath);
            }
           
        }




        #region V7.0 Code
        /// <summary>
        /// Returns the results for Flights
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchResult[] GetResultsForTBO(SearchRequest request)
        {
            SearchResult[] response = new SearchResult[0];
            WSSearchResponse resp = new WSSearchResponse();
            try
            {
                WSSearchRequest req = PrepareSearchRequest(request);

                if (request.IsDomestic)
                {
                    req.PromotionalPlanType = PromotionalPlanType.LCCSpecialReturn;
                    req.CabinClass = TBOAir.TekTravelAir.CabinClass.Economy;
                    req.FlightSegments = new TBOAir.TekTravelAir.FlightSegment[0];
                }

                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSSearchRequest));
                        StreamWriter sw = new StreamWriter(XmlPath + "TBOAirSearchRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, req);
                        sw.Close();
                    }
                }
                catch { }

                resp = ba.Search(req);

                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSSearchResponse));
                        StreamWriter sw = new StreamWriter(XmlPath + "TBOAirSearchResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, resp);
                        sw.Close();
                    }
                }
                catch { }


                if (resp.Result != null && resp.Result.Length > 0)
                {
                    rateOfExchange = exchangeRates[resp.Result[0].Fare.Currency];

                    if (request.IsDomestic && req.PromotionalPlanType == PromotionalPlanType.LCCSpecialReturn)
                    {
                        int obSeg = 0, inSeg = 0;
                        List<WSResult> obResults = new List<WSResult>();
                        List<WSResult> ibResults = new List<WSResult>();

                        for (int i = 0; i < resp.Result.Length; i++)
                        {
                            if (resp.Result[i].TripIndicator == 1)
                            {
                                obSeg++;
                                obResults.Add(resp.Result[i]);
                            }
                            else if (resp.Result[i].TripIndicator == 2)
                            {
                                ibResults.Add(resp.Result[i]);
                                inSeg++;
                            }
                        }

                        List<SearchResult> results = new List<SearchResult>();

                        for (int i = 0; i < obResults.Count; i++)
                        {
                            for (int j = 0; j < ibResults.Count; j++)
                            {
                                SearchResult result = new SearchResult();

                                #region Outbound Segment
                                result.Currency = agentBaseCurrency;

                                if (request.Type == CT.BookingEngine.SearchType.Return)
                                {
                                    result.Flights = new FlightInfo[2][];
                                    result.Flights[0] = new FlightInfo[1];
                                    result.Flights[1] = new FlightInfo[1];
                                }
                                else if (request.Type == CT.BookingEngine.SearchType.OneWay)
                                {
                                    result.Flights = new FlightInfo[1][];
                                    result.Flights[0] = new FlightInfo[1];
                                }
                                int segment = obResults[i].TripIndicator - 1;
                                result.Flights[segment][0] = new FlightInfo();
                                result.Flights[segment][0].Airline = obResults[i].Segment[0].Airline.AirlineCode;
                                result.Flights[segment][0].ArrivalTime = obResults[i].Segment[0].ArrTime;
                                result.Flights[segment][0].AirlinePNR = obResults[i].Segment[0].AirlinePNR;
                                result.Flights[segment][0].Craft = obResults[i].Segment[0].Craft;
                                result.Flights[segment][0].DepartureTime = obResults[i].Segment[0].DepTIme;

                                result.Flights[segment][0].Destination = new CT.BookingEngine.Airport();
                                result.Flights[segment][0].Destination.AirportCode = obResults[i].Segment[0].Destination.AirportCode;
                                result.Flights[segment][0].Destination.AirportName = obResults[i].Segment[0].Destination.AirportName;
                                result.Flights[segment][0].Destination.CityCode = obResults[i].Segment[0].Destination.CityCode;
                                result.Flights[segment][0].Destination.CityName = obResults[i].Segment[0].Destination.CityName;
                                result.Flights[segment][0].Destination.CountryCode = obResults[i].Segment[0].Destination.CountryCode;
                                result.Flights[segment][0].Destination.CountryName = obResults[i].Segment[0].Destination.CountryName;

                                result.Flights[segment][0].Origin = new CT.BookingEngine.Airport();
                                result.Flights[segment][0].Origin.AirportCode = obResults[i].Segment[0].Origin.AirportCode;
                                result.Flights[segment][0].Origin.AirportName = obResults[i].Segment[0].Origin.AirportName;
                                result.Flights[segment][0].Origin.CityCode = obResults[i].Segment[0].Origin.CityCode;
                                result.Flights[segment][0].Origin.CityName = obResults[i].Segment[0].Origin.CityName;
                                result.Flights[segment][0].Origin.CountryCode = obResults[i].Segment[0].Origin.CountryCode;
                                result.Flights[segment][0].Origin.CountryName = obResults[i].Segment[0].Origin.CountryName;

                                result.Flights[segment][0].Duration = TimeSpan.Parse(obResults[i].Segment[0].Duration);
                                result.Flights[segment][0].ETicketEligible = obResults[i].Segment[0].ETicketEligible;
                                result.Flights[segment][0].FareInfoKey = obResults[i].Segment[0].FareClass;
                                result.Flights[segment][0].FlightNumber = obResults[i].Segment[0].FlightNumber;
                                result.Flights[segment][0].OperatingCarrier = obResults[i].Segment[0].OperatingCarrier;
                                result.Flights[segment][0].Group = obResults[i].Segment[0].SegmentIndicator;
                                result.Flights[segment][0].Status = obResults[i].Segment[0].Status;
                                result.Flights[segment][0].Stops = obResults[i].Segment[0].Stop;
                                result.Flights[segment][0].BookingClass = obResults[i].Segment[0].FareClass;
                                result.Flights[segment][0].CabinClass = obResults[i].Segment[0].FareClass;


                                result.NonRefundable = obResults[i].NonRefundable;
                                result.ResultBookingSource = CT.BookingEngine.BookingSource.TBOAir;
                                result.Price = new PriceAccounts();
                                result.Price.AdditionalTxnFee = obResults[i].Fare.AdditionalTxnFee * rateOfExchange;
                                result.Price.AccPriceType = PriceType.PublishedFare;
                                result.Price.AgentCommission = obResults[i].Fare.AgentCommission * rateOfExchange;
                                result.Price.AgentPLB = obResults[i].Fare.PLBEarned * rateOfExchange;
                                result.Price.AirlineTransFee = obResults[i].Fare.AirTransFee * rateOfExchange;
                                result.Price.Currency = agentBaseCurrency;
                                result.Price.PublishedFare = obResults[i].Fare.PublishedPrice * rateOfExchange;
                                result.Price.RateOfExchange = rateOfExchange;
                                result.Price.ReverseHandlingCharge = obResults[i].Fare.ReverseHandlingCharge * rateOfExchange;
                                result.Price.SeviceTax = obResults[i].Fare.ServiceTax * rateOfExchange;
                                result.Price.SupplierCurrency = obResults[i].Fare.Currency;
                                result.Price.SupplierPrice = obResults[i].Fare.PublishedPrice * rateOfExchange;
                                result.Price.OtherCharges = obResults[i].Fare.OtherCharges * rateOfExchange;
                                result.Price.Tax = obResults[i].Fare.Tax * rateOfExchange;
                                result.Price.TdsCommission = obResults[i].Fare.TdsOnCommission * rateOfExchange;
                                result.Price.OurPLB = obResults[i].Fare.PLBEarned * rateOfExchange;
                                result.Price.IncentiveEarned = obResults[i].Fare.IncentiveEarned * rateOfExchange;//TODO: discuss with ziyad.
                                result.Price.TDSPLB = obResults[i].Fare.TdsOnPLB * rateOfExchange;
                                result.Price.TransactionFee = obResults[i].Fare.TransactionFee * rateOfExchange;

                                result.Price.NetFare = obResults[i].Fare.OfferedFare;

                                result.Price.ChargeBU = new List<CT.BookingEngine.ChargeBreakUp>();
                                foreach (TekTravelAir.ChargeBreakUp charge in obResults[i].Fare.ChargeBU)
                                {
                                    CT.BookingEngine.ChargeBreakUp cb = new CT.BookingEngine.ChargeBreakUp();
                                    cb.Amount = charge.Amount * rateOfExchange;
                                    switch (charge.ChargeType)
                                    {
                                        case TBOAir.TekTravelAir.ChargeType.CreditCardCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.CreditCardCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.BankHandlingCharges:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.CurrencyConversionCharges:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.OtherCharges:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.OtherCharges;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.RevHandlingCharge:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.ServiceCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.ServiceCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.SupplierMarkup:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TboMarkup:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainBedRollCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainBedRollCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainOtherCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainOtherCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainReservationCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainReservationCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainSuperFastCharge:
                                            break;
                                    }
                                    cb.PriceId = charge.PriceId;
                                    result.Price.ChargeBU.Add(cb);
                                }// End charge breakup


                                result.TBOPrice = new PriceAccounts();
                                result.TBOPrice.AdditionalTxnFee = obResults[i].Fare.AdditionalTxnFee;
                                result.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                result.TBOPrice.AgentCommission = obResults[i].Fare.AgentCommission;
                                result.TBOPrice.AgentPLB = obResults[i].Fare.PLBEarned;
                                result.TBOPrice.AirlineTransFee = obResults[i].Fare.AirTransFee;
                                result.TBOPrice.Currency = agentBaseCurrency;
                                result.TBOPrice.PublishedFare = obResults[i].Fare.PublishedPrice;
                                result.TBOPrice.RateOfExchange = rateOfExchange;
                                result.TBOPrice.ReverseHandlingCharge = obResults[i].Fare.ReverseHandlingCharge;
                                result.TBOPrice.SeviceTax = obResults[i].Fare.ServiceTax;
                                result.TBOPrice.SupplierCurrency = obResults[i].Fare.Currency;
                                result.TBOPrice.SupplierPrice = obResults[i].Fare.PublishedPrice;
                                result.TBOPrice.OtherCharges = obResults[i].Fare.OtherCharges;
                                result.TBOPrice.Tax = obResults[i].Fare.Tax;
                                result.TBOPrice.TdsCommission = obResults[i].Fare.TdsOnCommission;
                                result.TBOPrice.OurPLB = obResults[i].Fare.PLBEarned;
                                result.TBOPrice.IncentiveEarned = obResults[i].Fare.IncentiveEarned;//TODO: discuss with ziyad.
                                result.TBOPrice.TDSPLB = obResults[i].Fare.TdsOnPLB;
                                result.TBOPrice.TransactionFee = obResults[i].Fare.TransactionFee;

                                result.TBOPrice.NetFare = obResults[i].Fare.OfferedFare;

                                result.TBOPrice.ChargeBU = new List<CT.BookingEngine.ChargeBreakUp>();
                                foreach (TekTravelAir.ChargeBreakUp charge in obResults[i].Fare.ChargeBU)
                                {
                                    CT.BookingEngine.ChargeBreakUp cb = new CT.BookingEngine.ChargeBreakUp();
                                    cb.Amount = charge.Amount;
                                    switch (charge.ChargeType)
                                    {
                                        case TBOAir.TekTravelAir.ChargeType.CreditCardCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.CreditCardCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.BankHandlingCharges:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.CurrencyConversionCharges:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.OtherCharges:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.OtherCharges;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.RevHandlingCharge:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.ServiceCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.ServiceCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.SupplierMarkup:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TboMarkup:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainBedRollCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainBedRollCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainOtherCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainOtherCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainReservationCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainReservationCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainSuperFastCharge:
                                            break;
                                    }
                                    cb.PriceId = charge.PriceId;
                                    result.TBOPrice.ChargeBU.Add(cb);
                                }// End charge breakup

                                result.Flights[segment][0].SegmentPrice = result.TBOPrice;

                                result.FareBreakdown = new Fare[obResults[i].FareBreakdown.Length];
                                result.TBOFareBreakdown = new Fare[obResults[i].FareBreakdown.Length];
                                for (int k = 0; k < obResults[i].FareBreakdown.Length; k++)
                                {
                                    result.FareBreakdown[k] = new Fare();
                                    result.FareBreakdown[k].AdditionalTxnFee = obResults[i].FareBreakdown[k].AdditionalTxnFee * rateOfExchange;
                                    result.FareBreakdown[k].AgentMarkup = obResults[i].FareBreakdown[k].AgentServiceCharge * rateOfExchange;
                                    result.FareBreakdown[k].AirlineTransFee = obResults[i].FareBreakdown[k].AirlineTransFee * rateOfExchange;
                                    result.FareBreakdown[k].BaseFare = (double)(obResults[i].FareBreakdown[k].BaseFare * rateOfExchange);
                                    result.FareBreakdown[k].PassengerCount = obResults[i].FareBreakdown[k].PassengerCount;

                                    switch (obResults[i].FareBreakdown[k].PassengerType)
                                    {
                                        case TBOAir.TekTravelAir.PassengerType.Adult:
                                        case TBOAir.TekTravelAir.PassengerType.Senior:
                                            result.FareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Adult;
                                            break;
                                        case TBOAir.TekTravelAir.PassengerType.Child:
                                            result.FareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Child;
                                            break;
                                        case TBOAir.TekTravelAir.PassengerType.Infant:
                                            result.FareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Infant;
                                            break;
                                    }

                                    result.FareBreakdown[k].TotalFare = (double)(result.FareBreakdown[k].BaseFare + (double)((obResults[i].FareBreakdown[k].Tax * rateOfExchange) + result.FareBreakdown[k].AdditionalTxnFee + result.FareBreakdown[k].AirlineTransFee));
                                    result.FareBreakdown[k].SellingFare = result.FareBreakdown[k].TotalFare;
                                    result.FareBreakdown[k].SupplierFare = (double)(obResults[i].FareBreakdown[k].BaseFare + obResults[i].FareBreakdown[k].Tax);

                                    result.TBOFareBreakdown[k] = new Fare();
                                    result.TBOFareBreakdown[k].AdditionalTxnFee = obResults[i].FareBreakdown[k].AdditionalTxnFee;
                                    result.TBOFareBreakdown[k].AgentMarkup = obResults[i].FareBreakdown[k].AgentServiceCharge * rateOfExchange;
                                    result.TBOFareBreakdown[k].AirlineTransFee = obResults[i].FareBreakdown[k].AirlineTransFee * rateOfExchange;
                                    result.TBOFareBreakdown[k].BaseFare = (double)(obResults[i].FareBreakdown[k].BaseFare * rateOfExchange);
                                    result.TBOFareBreakdown[k].PassengerCount = obResults[i].FareBreakdown[k].PassengerCount;

                                    switch (obResults[i].FareBreakdown[k].PassengerType)
                                    {
                                        case TBOAir.TekTravelAir.PassengerType.Adult:
                                        case TBOAir.TekTravelAir.PassengerType.Senior:
                                            result.TBOFareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Adult;
                                            break;
                                        case TBOAir.TekTravelAir.PassengerType.Child:
                                            result.TBOFareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Child;
                                            break;
                                        case TBOAir.TekTravelAir.PassengerType.Infant:
                                            result.TBOFareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Infant;
                                            break;
                                    }

                                    result.TBOFareBreakdown[k].TotalFare = (double)(result.TBOFareBreakdown[k].BaseFare + (double)((obResults[i].FareBreakdown[k].Tax * rateOfExchange) + result.TBOFareBreakdown[k].AdditionalTxnFee + result.TBOFareBreakdown[k].AirlineTransFee));
                                    result.TBOFareBreakdown[k].SellingFare = result.TBOFareBreakdown[k].TotalFare;
                                    result.TBOFareBreakdown[k].SupplierFare = (double)(obResults[i].FareBreakdown[k].BaseFare + obResults[i].FareBreakdown[k].Tax);
                                }
                                result.Flights[segment][0].SegmentFareBreakdown = new List<Fare>();
                                result.Flights[segment][0].SegmentFareBreakdown.AddRange(result.TBOFareBreakdown);
                                result.FareRules = new List<FareRule>();

                                foreach (WSFareRule fare in obResults[i].FareRule)
                                {
                                    FareRule fr = new FareRule();
                                    fr.Airline = fare.Airline;
                                    fr.DepartureTime = fare.DepartureDate;
                                    fr.Destination = fare.Destination;
                                    fr.FareBasisCode = fare.FareBasisCode;
                                    fr.FareRestriction = fare.FareRestriction;
                                    fr.FareRuleDetail = fare.FareRuleDetail;
                                    fr.Origin = fare.Origin;
                                    fr.ReturnDate = fare.ReturnDate;
                                    fr.SupplierBookingSource = (TBOBookingSource)Enum.Parse(typeof(TBOAir.TekTravelAir.BookingSource), fare.Source.ToString());
                                    result.FareRules.Add(fr);
                                }
                                result.Flights[segment][0].SegmentFareRule = result.FareRules;

                                result.BaseFare = (double)(obResults[i].Fare.BaseFare * rateOfExchange);
                                result.Tax = (double)((obResults[i].Fare.Tax * rateOfExchange) + result.Price.AdditionalTxnFee + result.Price.AirlineTransFee + result.Price.OtherCharges + result.Price.SeviceTax);
                                result.TotalFare = result.BaseFare + result.Tax;
                                #endregion

                                segment = ibResults[j].TripIndicator - 1;
                                result.Flights[segment][0] = new FlightInfo();
                                result.Flights[segment][0].Airline = ibResults[j].Segment[0].Airline.AirlineCode;
                                result.Flights[segment][0].ArrivalTime = ibResults[j].Segment[0].ArrTime;
                                result.Flights[segment][0].AirlinePNR = ibResults[j].Segment[0].AirlinePNR;
                                result.Flights[segment][0].Craft = ibResults[j].Segment[0].Craft;
                                result.Flights[segment][0].DepartureTime = ibResults[j].Segment[0].DepTIme;

                                result.Flights[segment][0].Destination = new CT.BookingEngine.Airport();
                                result.Flights[segment][0].Destination.AirportCode = ibResults[j].Segment[0].Destination.AirportCode;
                                result.Flights[segment][0].Destination.AirportName = ibResults[j].Segment[0].Destination.AirportName;
                                result.Flights[segment][0].Destination.CityCode = ibResults[j].Segment[0].Destination.CityCode;
                                result.Flights[segment][0].Destination.CityName = ibResults[j].Segment[0].Destination.CityName;
                                result.Flights[segment][0].Destination.CountryCode = ibResults[j].Segment[0].Destination.CountryCode;
                                result.Flights[segment][0].Destination.CountryName = ibResults[j].Segment[0].Destination.CountryName;

                                result.Flights[segment][0].Origin = new CT.BookingEngine.Airport();
                                result.Flights[segment][0].Origin.AirportCode = ibResults[j].Segment[0].Origin.AirportCode;
                                result.Flights[segment][0].Origin.AirportName = ibResults[j].Segment[0].Origin.AirportName;
                                result.Flights[segment][0].Origin.CityCode = ibResults[j].Segment[0].Origin.CityCode;
                                result.Flights[segment][0].Origin.CityName = ibResults[j].Segment[0].Origin.CityName;
                                result.Flights[segment][0].Origin.CountryCode = ibResults[j].Segment[0].Origin.CountryCode;
                                result.Flights[segment][0].Origin.CountryName = ibResults[j].Segment[0].Origin.CountryName;

                                result.Flights[segment][0].Duration = TimeSpan.Parse(ibResults[j].Segment[0].Duration);
                                result.Flights[segment][0].ETicketEligible = ibResults[j].Segment[0].ETicketEligible;
                                result.Flights[segment][0].FareInfoKey = ibResults[j].Segment[0].FareClass;
                                result.Flights[segment][0].FlightNumber = ibResults[j].Segment[0].FlightNumber;
                                result.Flights[segment][0].OperatingCarrier = ibResults[j].Segment[0].OperatingCarrier;
                                result.Flights[segment][0].Group = ibResults[j].Segment[0].SegmentIndicator;
                                result.Flights[segment][0].Status = ibResults[j].Segment[0].Status;
                                result.Flights[segment][0].Stops = ibResults[j].Segment[0].Stop;
                                result.Flights[segment][0].BookingClass = ibResults[j].Segment[0].FareClass;
                                result.Flights[segment][0].CabinClass = ibResults[j].Segment[0].FareClass;

                                result.Price.AdditionalTxnFee += ibResults[j].Fare.AdditionalTxnFee * rateOfExchange;
                                result.Price.AccPriceType = PriceType.PublishedFare;
                                result.Price.AgentCommission += ibResults[j].Fare.AgentCommission * rateOfExchange;
                                result.Price.AgentPLB += ibResults[j].Fare.PLBEarned * rateOfExchange;
                                result.Price.AirlineTransFee += ibResults[j].Fare.AirTransFee * rateOfExchange;
                                result.Price.Currency = agentBaseCurrency;
                                result.Price.PublishedFare += ibResults[j].Fare.PublishedPrice * rateOfExchange;
                                result.Price.RateOfExchange = rateOfExchange;
                                result.Price.ReverseHandlingCharge += ibResults[j].Fare.ReverseHandlingCharge * rateOfExchange;
                                result.Price.SeviceTax += ibResults[j].Fare.ServiceTax * rateOfExchange;
                                result.Price.SupplierCurrency = ibResults[j].Fare.Currency;
                                result.Price.SupplierPrice += ibResults[j].Fare.PublishedPrice * rateOfExchange;
                                result.Price.OtherCharges += ibResults[j].Fare.OtherCharges * rateOfExchange;
                                result.Price.Tax += ibResults[j].Fare.Tax * rateOfExchange;
                                result.Price.TdsCommission += ibResults[j].Fare.TdsOnCommission * rateOfExchange;
                                result.Price.OurPLB += ibResults[j].Fare.PLBEarned * rateOfExchange;
                                //result.Price.TdsRate = ibResults[j].Fare.IncentiveEarned * rateOfExchange;//TODO: discuss with ziyad.
                                result.Price.TDSPLB += ibResults[j].Fare.TdsOnPLB * rateOfExchange;
                                result.Price.TransactionFee += ibResults[j].Fare.TransactionFee * rateOfExchange;

                                result.Price.NetFare += ibResults[j].Fare.OfferedFare;

                                foreach (TekTravelAir.ChargeBreakUp charge in resp.Result[i].Fare.ChargeBU)
                                {
                                    CT.BookingEngine.ChargeBreakUp cb = new CT.BookingEngine.ChargeBreakUp();
                                    cb.Amount = charge.Amount * rateOfExchange;
                                    switch (charge.ChargeType)
                                    {
                                        case TBOAir.TekTravelAir.ChargeType.CreditCardCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.CreditCardCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.BankHandlingCharges:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.CurrencyConversionCharges:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.OtherCharges:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.OtherCharges;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.RevHandlingCharge:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.ServiceCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.ServiceCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.SupplierMarkup:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TboMarkup:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainBedRollCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainBedRollCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainOtherCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainOtherCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainReservationCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainReservationCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainSuperFastCharge:
                                            break;
                                    }
                                    cb.PriceId = charge.PriceId;
                                    result.Price.ChargeBU.Add(cb);
                                }// End charge breakup

                                result.TBOPrice.AdditionalTxnFee += ibResults[j].Fare.AdditionalTxnFee;
                                result.TBOPrice.AccPriceType = PriceType.PublishedFare;
                                result.TBOPrice.AgentCommission += ibResults[j].Fare.AgentCommission;
                                result.TBOPrice.AgentPLB += ibResults[j].Fare.PLBEarned;
                                result.TBOPrice.AirlineTransFee += ibResults[j].Fare.AirTransFee;
                                result.TBOPrice.Currency = agentBaseCurrency;
                                result.TBOPrice.PublishedFare += ibResults[j].Fare.PublishedPrice;
                                result.TBOPrice.RateOfExchange = rateOfExchange;
                                result.TBOPrice.ReverseHandlingCharge += ibResults[j].Fare.ReverseHandlingCharge;
                                result.TBOPrice.SeviceTax += ibResults[j].Fare.ServiceTax;
                                result.TBOPrice.SupplierCurrency = ibResults[j].Fare.Currency;
                                result.TBOPrice.SupplierPrice += ibResults[j].Fare.PublishedPrice;
                                result.TBOPrice.OtherCharges += ibResults[j].Fare.OtherCharges;
                                result.TBOPrice.Tax += ibResults[j].Fare.Tax;
                                result.TBOPrice.TdsCommission += ibResults[j].Fare.TdsOnCommission;
                                result.TBOPrice.OurPLB += ibResults[j].Fare.PLBEarned;
                                result.TBOPrice.IncentiveEarned += ibResults[j].Fare.IncentiveEarned;//TODO: discuss with ziyad.
                                result.TBOPrice.TDSPLB += ibResults[j].Fare.TdsOnPLB;
                                result.TBOPrice.TransactionFee += ibResults[j].Fare.TransactionFee;
                                result.TBOPrice.TDSIncentive += ibResults[j].Fare.TdsOnIncentive;
                                result.TBOPrice.AgentConvienceCharges += ibResults[j].Fare.AgentConvienceCharges;
                                result.TBOPrice.AgentServiceCharge += ibResults[j].Fare.AgentServiceCharge;
                                result.TBOPrice.NetFare += ibResults[j].Fare.OfferedFare;

                                foreach (TekTravelAir.ChargeBreakUp charge in resp.Result[i].Fare.ChargeBU)
                                {
                                    CT.BookingEngine.ChargeBreakUp cb = new CT.BookingEngine.ChargeBreakUp();
                                    cb.Amount = charge.Amount;
                                    switch (charge.ChargeType)
                                    {
                                        case TBOAir.TekTravelAir.ChargeType.CreditCardCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.CreditCardCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.BankHandlingCharges:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.CurrencyConversionCharges:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.OtherCharges:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.OtherCharges;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.RevHandlingCharge:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.ServiceCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.ServiceCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.SupplierMarkup:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TboMarkup:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainBedRollCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainBedRollCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainOtherCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainOtherCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainReservationCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainReservationCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainSuperFastCharge:
                                            break;
                                    }
                                    cb.PriceId = charge.PriceId;
                                    result.TBOPrice.ChargeBU.Add(cb);
                                }// End charge breakup

                                result.Flights[segment][0].SegmentPrice.AdditionalTxnFee = ibResults[j].Fare.AdditionalTxnFee;
                                result.Flights[segment][0].SegmentPrice.AccPriceType = PriceType.PublishedFare;
                                result.Flights[segment][0].SegmentPrice.AgentCommission = ibResults[j].Fare.AgentCommission;
                                result.Flights[segment][0].SegmentPrice.AgentPLB = ibResults[j].Fare.PLBEarned;
                                result.Flights[segment][0].SegmentPrice.AirlineTransFee = ibResults[j].Fare.AirTransFee;
                                result.Flights[segment][0].SegmentPrice.Currency = agentBaseCurrency;
                                result.Flights[segment][0].SegmentPrice.PublishedFare = ibResults[j].Fare.PublishedPrice;
                                result.Flights[segment][0].SegmentPrice.RateOfExchange = rateOfExchange;
                                result.Flights[segment][0].SegmentPrice.ReverseHandlingCharge = ibResults[j].Fare.ReverseHandlingCharge;
                                result.Flights[segment][0].SegmentPrice.SeviceTax = ibResults[j].Fare.ServiceTax;
                                result.Flights[segment][0].SegmentPrice.SupplierCurrency = ibResults[j].Fare.Currency;
                                result.Flights[segment][0].SegmentPrice.SupplierPrice = ibResults[j].Fare.PublishedPrice;
                                result.Flights[segment][0].SegmentPrice.OtherCharges = ibResults[j].Fare.OtherCharges;
                                result.Flights[segment][0].SegmentPrice.Tax = ibResults[j].Fare.Tax;
                                result.Flights[segment][0].SegmentPrice.TdsCommission = ibResults[j].Fare.TdsOnCommission;
                                result.Flights[segment][0].SegmentPrice.OurPLB = ibResults[j].Fare.PLBEarned;
                                result.Flights[segment][0].SegmentPrice.IncentiveEarned = ibResults[j].Fare.IncentiveEarned;//TODO: discuss with ziyad.
                                result.Flights[segment][0].SegmentPrice.TDSPLB = ibResults[j].Fare.TdsOnPLB;
                                result.Flights[segment][0].SegmentPrice.TransactionFee = ibResults[j].Fare.TransactionFee;
                                result.Flights[segment][0].SegmentPrice.TDSIncentive = ibResults[j].Fare.TdsOnIncentive;
                                result.Flights[segment][0].SegmentPrice.AgentConvienceCharges = ibResults[j].Fare.AgentConvienceCharges;
                                result.Flights[segment][0].SegmentPrice.AgentServiceCharge = ibResults[j].Fare.AgentServiceCharge;
                                result.Flights[segment][0].SegmentPrice.NetFare = ibResults[j].Fare.OfferedFare;

                                foreach (TekTravelAir.ChargeBreakUp charge in resp.Result[i].Fare.ChargeBU)
                                {
                                    CT.BookingEngine.ChargeBreakUp cb = new CT.BookingEngine.ChargeBreakUp();
                                    cb.Amount = charge.Amount;
                                    switch (charge.ChargeType)
                                    {
                                        case TBOAir.TekTravelAir.ChargeType.CreditCardCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.CreditCardCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.BankHandlingCharges:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.CurrencyConversionCharges:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.OtherCharges:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.OtherCharges;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.RevHandlingCharge:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.ServiceCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.ServiceCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.SupplierMarkup:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TboMarkup:
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainBedRollCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainBedRollCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainOtherCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainOtherCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainReservationCharge:
                                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainReservationCharge;
                                            break;
                                        case TBOAir.TekTravelAir.ChargeType.TrainSuperFastCharge:
                                            break;
                                    }
                                    cb.PriceId = charge.PriceId;
                                    result.Flights[segment][0].SegmentPrice.ChargeBU.Add(cb);
                                }// End charge breakup

                                result.Flights[segment][0].SegmentFareBreakdown = new List<Fare>();
                                for (int k = 0; k < ibResults[j].FareBreakdown.Length; k++)
                                {
                                    result.FareBreakdown[k].AdditionalTxnFee += ibResults[j].FareBreakdown[k].AdditionalTxnFee * rateOfExchange;
                                    result.FareBreakdown[k].AgentMarkup += ibResults[j].FareBreakdown[k].AgentServiceCharge * rateOfExchange;
                                    result.FareBreakdown[k].AirlineTransFee += ibResults[j].FareBreakdown[k].AirlineTransFee * rateOfExchange;
                                    result.FareBreakdown[k].BaseFare += (double)(ibResults[j].FareBreakdown[k].BaseFare * rateOfExchange);
                                    result.FareBreakdown[k].PassengerCount = ibResults[j].FareBreakdown[k].PassengerCount;
                                    switch (ibResults[j].FareBreakdown[k].PassengerType)
                                    {
                                        case TBOAir.TekTravelAir.PassengerType.Adult:
                                        case TBOAir.TekTravelAir.PassengerType.Senior:
                                            result.FareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Adult;
                                            break;
                                        case TBOAir.TekTravelAir.PassengerType.Child:
                                            result.FareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Child;
                                            break;
                                        case TBOAir.TekTravelAir.PassengerType.Infant:
                                            result.FareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Infant;
                                            break;
                                    }

                                    result.FareBreakdown[k].TotalFare += (double)(result.FareBreakdown[k].BaseFare + (double)((ibResults[j].FareBreakdown[k].Tax * rateOfExchange) + result.FareBreakdown[k].AdditionalTxnFee + result.FareBreakdown[k].AirlineTransFee));
                                    result.FareBreakdown[k].SellingFare += result.FareBreakdown[k].TotalFare;
                                    result.FareBreakdown[k].SupplierFare += (double)(ibResults[j].FareBreakdown[k].BaseFare + ibResults[j].FareBreakdown[k].Tax);

                                    result.TBOFareBreakdown[k].AdditionalTxnFee += ibResults[j].FareBreakdown[k].AdditionalTxnFee;
                                    result.TBOFareBreakdown[k].AgentMarkup += ibResults[j].FareBreakdown[k].AgentServiceCharge;
                                    result.TBOFareBreakdown[k].AirlineTransFee += ibResults[j].FareBreakdown[k].AirlineTransFee;
                                    result.TBOFareBreakdown[k].BaseFare += (double)(ibResults[j].FareBreakdown[k].BaseFare);
                                    result.TBOFareBreakdown[k].PassengerCount = ibResults[j].FareBreakdown[k].PassengerCount;
                                    switch (ibResults[j].FareBreakdown[k].PassengerType)
                                    {
                                        case TBOAir.TekTravelAir.PassengerType.Adult:
                                        case TBOAir.TekTravelAir.PassengerType.Senior:
                                            result.TBOFareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Adult;
                                            break;
                                        case TBOAir.TekTravelAir.PassengerType.Child:
                                            result.TBOFareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Child;
                                            break;
                                        case TBOAir.TekTravelAir.PassengerType.Infant:
                                            result.TBOFareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Infant;
                                            break;
                                    }

                                    result.TBOFareBreakdown[k].TotalFare += (double)(result.TBOFareBreakdown[k].BaseFare + (double)((ibResults[j].FareBreakdown[k].Tax * rateOfExchange) + result.TBOFareBreakdown[k].AdditionalTxnFee + result.TBOFareBreakdown[k].AirlineTransFee));
                                    result.TBOFareBreakdown[k].SellingFare += result.TBOFareBreakdown[k].TotalFare;
                                    result.TBOFareBreakdown[k].SupplierFare += (double)(ibResults[j].FareBreakdown[k].BaseFare + ibResults[j].FareBreakdown[k].Tax);

                                    result.Flights[segment][0].SegmentFareBreakdown[k].AdditionalTxnFee += ibResults[j].FareBreakdown[k].AdditionalTxnFee;
                                    result.Flights[segment][0].SegmentFareBreakdown[k].AgentMarkup += ibResults[j].FareBreakdown[k].AgentServiceCharge;
                                    result.Flights[segment][0].SegmentFareBreakdown[k].AirlineTransFee += ibResults[j].FareBreakdown[k].AirlineTransFee;
                                    result.Flights[segment][0].SegmentFareBreakdown[k].BaseFare += (double)(ibResults[j].FareBreakdown[k].BaseFare);
                                    result.Flights[segment][0].SegmentFareBreakdown[k].PassengerCount = ibResults[j].FareBreakdown[k].PassengerCount;
                                    switch (ibResults[j].FareBreakdown[k].PassengerType)
                                    {
                                        case TBOAir.TekTravelAir.PassengerType.Adult:
                                        case TBOAir.TekTravelAir.PassengerType.Senior:
                                            result.Flights[segment][0].SegmentFareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Adult;
                                            break;
                                        case TBOAir.TekTravelAir.PassengerType.Child:
                                            result.Flights[segment][0].SegmentFareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Child;
                                            break;
                                        case TBOAir.TekTravelAir.PassengerType.Infant:
                                            result.Flights[segment][0].SegmentFareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Infant;
                                            break;
                                    }

                                    result.Flights[segment][0].SegmentFareBreakdown[k].TotalFare += (double)(result.Flights[segment][0].SegmentFareBreakdown[k].BaseFare + (double)((ibResults[j].FareBreakdown[k].Tax * rateOfExchange) + result.Flights[segment][0].SegmentFareBreakdown[k].AdditionalTxnFee + result.Flights[segment][0].SegmentFareBreakdown[k].AirlineTransFee));
                                    result.Flights[segment][0].SegmentFareBreakdown[k].SellingFare += result.Flights[segment][0].SegmentFareBreakdown[k].TotalFare;
                                    result.Flights[segment][0].SegmentFareBreakdown[k].SupplierFare += (double)(ibResults[j].FareBreakdown[k].BaseFare + ibResults[j].FareBreakdown[k].Tax);
                                }

                                result.Flights[segment][0].SegmentFareRule = new List<FareRule>();
                                foreach (WSFareRule fare in ibResults[j].FareRule)
                                {
                                    FareRule fr = new FareRule();
                                    fr.Airline = fare.Airline;
                                    fr.DepartureTime = fare.DepartureDate;
                                    fr.Destination = fare.Destination;
                                    fr.FareBasisCode = fare.FareBasisCode;
                                    fr.FareRestriction = fare.FareRestriction;
                                    fr.FareRuleDetail = fare.FareRuleDetail;
                                    fr.Origin = fare.Origin;
                                    fr.ReturnDate = fare.ReturnDate;
                                    fr.SupplierBookingSource = (TBOBookingSource)Enum.Parse(typeof(TBOAir.TekTravelAir.BookingSource), fare.Source.ToString());
                                    result.FareRules.Add(fr);
                                    result.Flights[segment][0].SegmentFareRule.Add(fr);
                                }



                                result.BaseFare += (double)(ibResults[j].Fare.BaseFare * rateOfExchange);
                                result.Tax += (double)((ibResults[j].Fare.Tax * rateOfExchange) + result.Price.AdditionalTxnFee + result.Price.AirlineTransFee + result.Price.OtherCharges + result.Price.SeviceTax);
                                result.TotalFare += result.BaseFare + result.Tax;
                                result.ValidatingAirline = "";
                                result.SupplierBookingSource = (TBOBookingSource)Enum.Parse(typeof(TBOAir.TekTravelAir.BookingSource), ibResults[j].Source.ToString());
                                result.TBOAirSessionId = resp.SessionId;
                                result.PromotionalPlanType = ibResults[j].PromotionalPlanType.ToString();
                                result.SegmentKey = ibResults[j].SegmentKey;

                                results.Add(result);
                            }
                        }

                        response = results.ToArray();
                    }
                    else
                    {
                        response = new SearchResult[resp.Result.Length];
                        for (int i = 0; i < resp.Result.Length; i++)
                        {
                            response[i] = new SearchResult();
                            response[i].Currency = agentBaseCurrency;
                            response[i].FareType = "PUB";
                            int obSeg = 0, inSeg = 0;
                            if (req.PromotionalPlanType == PromotionalPlanType.Normal)
                            {
                                for (int j = 0; j < resp.Result[i].Segment.Length; j++)
                                {
                                    if (resp.Result[i].Segment[j].SegmentIndicator == 1)
                                    {
                                        obSeg++;
                                    }
                                    else
                                    {
                                        inSeg++;
                                    }
                                }
                            }
                            else
                            {
                                for (int j = 0; j < resp.Result.Length; j++)
                                {
                                    if (resp.Result[j].TripIndicator == 1)
                                    {
                                        obSeg++;
                                    }
                                    else
                                    {
                                        inSeg++;
                                    }
                                }
                            }
                            if (req.PromotionalPlanType != PromotionalPlanType.LCCSpecialReturn)
                            {
                                if (request.Type == CT.BookingEngine.SearchType.Return)
                                {
                                    response[i].Flights = new FlightInfo[2][];
                                    response[i].Flights[0] = new FlightInfo[obSeg];
                                    response[i].Flights[1] = new FlightInfo[inSeg];
                                }
                                else if (request.Type == CT.BookingEngine.SearchType.OneWay)
                                {
                                    response[i].Flights = new FlightInfo[1][];
                                    response[i].Flights[0] = new FlightInfo[obSeg];
                                }
                            }
                            else
                            {
                                if (request.Type == CT.BookingEngine.SearchType.Return)
                                {
                                    response[i].Flights = new FlightInfo[2][];
                                    response[i].Flights[0] = new FlightInfo[1];
                                    response[i].Flights[1] = new FlightInfo[1];
                                }
                                else if (request.Type == CT.BookingEngine.SearchType.OneWay)
                                {
                                    response[i].Flights = new FlightInfo[1][];
                                    response[i].Flights[0] = new FlightInfo[1];
                                }
                            }
                            for (int j = 0; j < obSeg; j++)
                            {
                                int segment = 0;
                                if (req.PromotionalPlanType != PromotionalPlanType.LCCSpecialReturn)
                                {
                                    segment = resp.Result[i].Segment[j].SegmentIndicator - 1;
                                }
                                else
                                {
                                    segment = resp.Result[i].TripIndicator - 1;
                                }
                                response[i].Flights[segment][j] = new FlightInfo();
                                response[i].Flights[segment][j].Airline = resp.Result[i].Segment[j].Airline.AirlineCode;
                                response[i].Flights[segment][j].ArrivalTime = resp.Result[i].Segment[j].ArrTime;
                                response[i].Flights[segment][j].AirlinePNR = resp.Result[i].Segment[j].AirlinePNR;
                                response[i].Flights[segment][j].Craft = resp.Result[i].Segment[j].Craft;
                                response[i].Flights[segment][j].DepartureTime = resp.Result[i].Segment[j].DepTIme;

                                response[i].Flights[segment][j].Destination = new CT.BookingEngine.Airport();
                                response[i].Flights[segment][j].Destination.AirportCode = resp.Result[i].Segment[j].Destination.AirportCode;
                                response[i].Flights[segment][j].Destination.AirportName = resp.Result[i].Segment[j].Destination.AirportName;
                                response[i].Flights[segment][j].Destination.CityCode = resp.Result[i].Segment[j].Destination.CityCode;
                                response[i].Flights[segment][j].Destination.CityName = resp.Result[i].Segment[j].Destination.CityName;
                                response[i].Flights[segment][j].Destination.CountryCode = resp.Result[i].Segment[j].Destination.CountryCode;
                                response[i].Flights[segment][j].Destination.CountryName = resp.Result[i].Segment[j].Destination.CountryName;

                                response[i].Flights[segment][j].Origin = new CT.BookingEngine.Airport();
                                response[i].Flights[segment][j].Origin.AirportCode = resp.Result[i].Segment[j].Origin.AirportCode;
                                response[i].Flights[segment][j].Origin.AirportName = resp.Result[i].Segment[j].Origin.AirportName;
                                response[i].Flights[segment][j].Origin.CityCode = resp.Result[i].Segment[j].Origin.CityCode;
                                response[i].Flights[segment][j].Origin.CityName = resp.Result[i].Segment[j].Origin.CityName;
                                response[i].Flights[segment][j].Origin.CountryCode = resp.Result[i].Segment[j].Origin.CountryCode;
                                response[i].Flights[segment][j].Origin.CountryName = resp.Result[i].Segment[j].Origin.CountryName;

                                response[i].Flights[segment][j].Duration = TimeSpan.Parse(resp.Result[i].Segment[j].Duration);
                                response[i].Flights[segment][j].ETicketEligible = resp.Result[i].Segment[j].ETicketEligible;
                                response[i].Flights[segment][j].FareInfoKey = resp.Result[i].Segment[j].FareClass;
                                response[i].Flights[segment][j].FlightNumber = resp.Result[i].Segment[j].FlightNumber;
                                response[i].Flights[segment][j].OperatingCarrier = resp.Result[i].Segment[j].OperatingCarrier;
                                response[i].Flights[segment][j].Group = resp.Result[i].Segment[j].SegmentIndicator;
                                response[i].Flights[segment][j].Status = resp.Result[i].Segment[j].Status;
                                response[i].Flights[segment][j].Stops = resp.Result[i].Segment[j].Stop;
                                response[i].Flights[segment][j].BookingClass = resp.Result[i].Segment[j].FareClass;
                                response[i].Flights[segment][j].CabinClass = resp.Result[i].Segment[j].FareClass;
                            }

                            if (inSeg > 0)
                            {
                                for (int j = obSeg; j < obSeg + inSeg; j++)
                                {
                                    int segment = 0;
                                    if (req.PromotionalPlanType != PromotionalPlanType.LCCSpecialReturn)
                                    {
                                        segment = resp.Result[i].Segment[j].SegmentIndicator - 1;
                                    }
                                    else
                                    {
                                        segment = resp.Result[i].TripIndicator - 1;
                                    }
                                    response[i].Flights[segment][j - obSeg] = new FlightInfo();
                                    response[i].Flights[segment][j - obSeg].Airline = resp.Result[i].Segment[j].Airline.AirlineCode;
                                    response[i].Flights[segment][j - obSeg].ArrivalTime = resp.Result[i].Segment[j].ArrTime;
                                    response[i].Flights[segment][j - obSeg].AirlinePNR = resp.Result[i].Segment[j].AirlinePNR;
                                    response[i].Flights[segment][j - obSeg].Craft = resp.Result[i].Segment[j].Craft;
                                    response[i].Flights[segment][j - obSeg].DepartureTime = resp.Result[i].Segment[j].DepTIme;

                                    response[i].Flights[segment][j - obSeg].Destination = new CT.BookingEngine.Airport();
                                    response[i].Flights[segment][j - obSeg].Destination.AirportCode = resp.Result[i].Segment[j].Destination.AirportCode;
                                    response[i].Flights[segment][j - obSeg].Destination.AirportName = resp.Result[i].Segment[j].Destination.AirportName;
                                    response[i].Flights[segment][j - obSeg].Destination.CityCode = resp.Result[i].Segment[j].Destination.CityCode;
                                    response[i].Flights[segment][j - obSeg].Destination.CityName = resp.Result[i].Segment[j].Destination.CityName;
                                    response[i].Flights[segment][j - obSeg].Destination.CountryCode = resp.Result[i].Segment[j].Destination.CountryCode;
                                    response[i].Flights[segment][j - obSeg].Destination.CountryName = resp.Result[i].Segment[j].Destination.CountryName;

                                    response[i].Flights[segment][j - obSeg].Origin = new CT.BookingEngine.Airport();
                                    response[i].Flights[segment][j - obSeg].Origin.AirportCode = resp.Result[i].Segment[j].Origin.AirportCode;
                                    response[i].Flights[segment][j - obSeg].Origin.AirportName = resp.Result[i].Segment[j].Origin.AirportName;
                                    response[i].Flights[segment][j - obSeg].Origin.CityCode = resp.Result[i].Segment[j].Origin.CityCode;
                                    response[i].Flights[segment][j - obSeg].Origin.CityName = resp.Result[i].Segment[j].Origin.CityName;
                                    response[i].Flights[segment][j - obSeg].Origin.CountryCode = resp.Result[i].Segment[j].Origin.CountryCode;
                                    response[i].Flights[segment][j - obSeg].Origin.CountryName = resp.Result[i].Segment[j].Origin.CountryName;

                                    response[i].Flights[segment][j - obSeg].Duration = TimeSpan.Parse(resp.Result[i].Segment[j].Duration);
                                    response[i].Flights[segment][j - obSeg].ETicketEligible = resp.Result[i].Segment[j].ETicketEligible;
                                    response[i].Flights[segment][j - obSeg].FareInfoKey = resp.Result[i].Segment[j].FareClass;
                                    response[i].Flights[segment][j - obSeg].FlightNumber = resp.Result[i].Segment[j].FlightNumber;
                                    response[i].Flights[segment][j - obSeg].OperatingCarrier = resp.Result[i].Segment[j].OperatingCarrier;
                                    response[i].Flights[segment][j - obSeg].Group = resp.Result[i].Segment[j].SegmentIndicator;
                                    response[i].Flights[segment][j - obSeg].Status = resp.Result[i].Segment[j].Status;
                                    response[i].Flights[segment][j - obSeg].Stops = resp.Result[i].Segment[j].Stop;
                                    response[i].Flights[segment][j - obSeg].BookingClass = resp.Result[i].Segment[j].FareClass;
                                    response[i].Flights[segment][j - obSeg].CabinClass = resp.Result[i].Segment[j].FareClass;
                                }
                            }


                            response[i].NonRefundable = resp.Result[i].NonRefundable;
                            response[i].ResultBookingSource = CT.BookingEngine.BookingSource.TBOAir;
                            response[i].Price = new PriceAccounts();
                            response[i].Price.AdditionalTxnFee = resp.Result[i].Fare.AdditionalTxnFee * rateOfExchange;
                            response[i].Price.AccPriceType = PriceType.PublishedFare;
                            response[i].Price.AgentCommission = resp.Result[i].Fare.AgentCommission * rateOfExchange;
                            response[i].Price.AgentPLB = resp.Result[i].Fare.PLBEarned * rateOfExchange;
                            response[i].Price.AirlineTransFee = resp.Result[i].Fare.AirTransFee * rateOfExchange;
                            response[i].Price.Currency = agentBaseCurrency;
                            response[i].Price.PublishedFare = resp.Result[i].Fare.PublishedPrice * rateOfExchange;
                            response[i].Price.NetFare = resp.Result[i].Fare.OfferedFare * rateOfExchange;
                            response[i].Price.RateOfExchange = rateOfExchange;
                            response[i].Price.FuelSurcharge = resp.Result[i].Fare.FuelSurcharge;//TEMP
                            response[i].Price.ReverseHandlingCharge = resp.Result[i].Fare.ReverseHandlingCharge * rateOfExchange;
                            response[i].Price.SeviceTax = resp.Result[i].Fare.ServiceTax * rateOfExchange;
                            response[i].Price.SupplierCurrency = resp.Result[i].Fare.Currency;
                            response[i].Price.SupplierPrice = resp.Result[i].Fare.PublishedPrice * rateOfExchange;
                            response[i].Price.OtherCharges = resp.Result[i].Fare.OtherCharges * rateOfExchange;
                            response[i].Price.Tax = resp.Result[i].Fare.Tax * rateOfExchange;
                            response[i].Price.TdsCommission = resp.Result[i].Fare.TdsOnCommission * rateOfExchange;
                            response[i].Price.OurPLB = resp.Result[i].Fare.PLBEarned * rateOfExchange;
                            //response[i].Price.TdsRate = resp.Result[i].Fare.IncentiveEarned * rateOfExchange;//TODO: discuss with ziyad.
                            response[i].Price.TDSPLB = resp.Result[i].Fare.TdsOnPLB * rateOfExchange;
                            response[i].Price.TransactionFee = resp.Result[i].Fare.TransactionFee * rateOfExchange;
                            response[i].Price.ChargeBU = new List<CT.BookingEngine.ChargeBreakUp>();
                            foreach (TekTravelAir.ChargeBreakUp charge in resp.Result[i].Fare.ChargeBU)
                            {
                                CT.BookingEngine.ChargeBreakUp cb = new CT.BookingEngine.ChargeBreakUp();
                                cb.Amount = charge.Amount * rateOfExchange;
                                switch (charge.ChargeType)
                                {
                                    case TBOAir.TekTravelAir.ChargeType.CreditCardCharge:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.CreditCardCharge;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.BankHandlingCharges:
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.CurrencyConversionCharges:
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.OtherCharges:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.OtherCharges;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.RevHandlingCharge:
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.ServiceCharge:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.ServiceCharge;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.SupplierMarkup:
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.TboMarkup:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.TBOMarkup;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.TrainBedRollCharge:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.TrainBedRollCharge;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.TrainOtherCharge:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.TrainOtherCharge;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.TrainReservationCharge:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.TrainReservationCharge;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.TrainSuperFastCharge:
                                        break;
                                }
                                cb.PriceId = charge.PriceId;
                                response[i].Price.ChargeBU.Add(cb);
                            }// End charge breakup

                            //Original values will be used in Booking and Ticketing.
                            response[i].TBOPrice = new PriceAccounts();
                            response[i].TBOPrice.AdditionalTxnFee = resp.Result[i].Fare.AdditionalTxnFee;
                            response[i].TBOPrice.AccPriceType = PriceType.PublishedFare;
                            response[i].TBOPrice.AgentCommission = resp.Result[i].Fare.AgentCommission;
                            response[i].TBOPrice.AgentPLB = resp.Result[i].Fare.PLBEarned;
                            response[i].TBOPrice.AirlineTransFee = resp.Result[i].Fare.AirTransFee;
                            response[i].TBOPrice.Currency = resp.Result[i].Fare.Currency;
                            response[i].TBOPrice.PublishedFare = resp.Result[i].Fare.PublishedPrice;
                            response[i].TBOPrice.NetFare = resp.Result[i].Fare.OfferedFare;
                            response[i].TBOPrice.BaseFare = resp.Result[i].Fare.BaseFare;
                            response[i].TBOPrice.RateOfExchange = rateOfExchange;
                            response[i].TBOPrice.FuelSurcharge = resp.Result[i].Fare.FuelSurcharge;//TEMP
                            response[i].TBOPrice.ReverseHandlingCharge = resp.Result[i].Fare.ReverseHandlingCharge;
                            response[i].TBOPrice.SeviceTax = resp.Result[i].Fare.ServiceTax;
                            response[i].TBOPrice.SupplierCurrency = resp.Result[i].Fare.Currency;
                            response[i].TBOPrice.SupplierPrice = resp.Result[i].Fare.PublishedPrice;
                            response[i].TBOPrice.OtherCharges = resp.Result[i].Fare.OtherCharges;
                            response[i].TBOPrice.Tax = resp.Result[i].Fare.Tax;
                            response[i].TBOPrice.TdsCommission = resp.Result[i].Fare.TdsOnCommission;
                            response[i].TBOPrice.OurPLB = resp.Result[i].Fare.PLBEarned;
                            response[i].TBOPrice.IncentiveEarned = resp.Result[i].Fare.IncentiveEarned;//TODO: discuss with ziyad.
                            response[i].TBOPrice.TDSIncentive = resp.Result[i].Fare.TdsOnIncentive;//TODO: discuss with ziyad.
                            response[i].TBOPrice.TDSPLB = resp.Result[i].Fare.TdsOnPLB;
                            response[i].TBOPrice.TransactionFee = resp.Result[i].Fare.TransactionFee;
                            response[i].TBOPrice.AgentServiceCharge = resp.Result[i].Fare.AgentServiceCharge;//TODO: discuss with ziyad.
                            response[i].TBOPrice.AgentConvienceCharges = resp.Result[i].Fare.AgentConvienceCharges;//TODO: discuss with ziyad.
                            response[i].TBOPrice.ChargeBU = new List<CT.BookingEngine.ChargeBreakUp>();
                            foreach (TekTravelAir.ChargeBreakUp charge in resp.Result[i].Fare.ChargeBU)
                            {
                                CT.BookingEngine.ChargeBreakUp cb = new CT.BookingEngine.ChargeBreakUp();
                                cb.Amount = charge.Amount;
                                switch (charge.ChargeType)
                                {
                                    case TBOAir.TekTravelAir.ChargeType.CreditCardCharge:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.CreditCardCharge;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.BankHandlingCharges:
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.CurrencyConversionCharges:
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.OtherCharges:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.OtherCharges;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.RevHandlingCharge:
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.ServiceCharge:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.ServiceCharge;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.SupplierMarkup:
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.TboMarkup:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.TBOMarkup;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.TrainBedRollCharge:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.TrainBedRollCharge;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.TrainOtherCharge:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.TrainOtherCharge;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.TrainReservationCharge:
                                        cb.ChargeType = CT.BookingEngine.ChargeType.TrainReservationCharge;
                                        break;
                                    case TBOAir.TekTravelAir.ChargeType.TrainSuperFastCharge:
                                        break;
                                }
                                cb.PriceId = charge.PriceId;
                                response[i].TBOPrice.ChargeBU.Add(cb);
                            }// End charge breakup

                            response[i].FareBreakdown = new Fare[resp.Result[i].FareBreakdown.Length];
                            response[i].TBOFareBreakdown = new Fare[resp.Result[i].FareBreakdown.Length];
                            for (int k = 0; k < resp.Result[i].FareBreakdown.Length; k++)
                            {
                                response[i].FareBreakdown[k] = new Fare();
                                response[i].FareBreakdown[k].AdditionalTxnFee = resp.Result[i].FareBreakdown[k].AdditionalTxnFee * rateOfExchange;
                                response[i].FareBreakdown[k].AgentMarkup = resp.Result[i].FareBreakdown[k].AgentServiceCharge * rateOfExchange;
                                response[i].FareBreakdown[k].AirlineTransFee = resp.Result[i].FareBreakdown[k].AirlineTransFee * rateOfExchange;
                                response[i].FareBreakdown[k].BaseFare = (double)(resp.Result[i].FareBreakdown[k].BaseFare * rateOfExchange);
                                response[i].FareBreakdown[k].PassengerCount = resp.Result[i].FareBreakdown[k].PassengerCount;
                                switch (resp.Result[i].FareBreakdown[k].PassengerType)
                                {
                                    case TBOAir.TekTravelAir.PassengerType.Adult:
                                    case TBOAir.TekTravelAir.PassengerType.Senior:
                                        response[i].FareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Adult;
                                        break;
                                    case TBOAir.TekTravelAir.PassengerType.Child:
                                        response[i].FareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Child;
                                        break;
                                    case TBOAir.TekTravelAir.PassengerType.Infant:
                                        response[i].FareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Infant;
                                        break;
                                }

                                response[i].FareBreakdown[k].TotalFare = (double)(response[i].FareBreakdown[k].BaseFare + (double)((resp.Result[i].FareBreakdown[k].Tax * rateOfExchange) + response[i].FareBreakdown[k].AdditionalTxnFee + response[i].FareBreakdown[k].AirlineTransFee));
                                response[i].FareBreakdown[k].SellingFare = response[i].FareBreakdown[k].TotalFare;
                                response[i].FareBreakdown[k].SupplierFare = (double)(resp.Result[i].FareBreakdown[k].BaseFare + resp.Result[i].FareBreakdown[k].Tax);

                                //TBO FareBreakdown is used in Booking and Ticketing.
                                response[i].TBOFareBreakdown[k] = new Fare();
                                response[i].TBOFareBreakdown[k].AdditionalTxnFee = resp.Result[i].FareBreakdown[k].AdditionalTxnFee;
                                response[i].TBOFareBreakdown[k].AgentMarkup = resp.Result[i].FareBreakdown[k].AgentServiceCharge;
                                response[i].TBOFareBreakdown[k].AirlineTransFee = resp.Result[i].FareBreakdown[k].AirlineTransFee;
                                response[i].TBOFareBreakdown[k].BaseFare = (double)(resp.Result[i].FareBreakdown[k].BaseFare);
                                response[i].TBOFareBreakdown[k].PassengerCount = resp.Result[i].FareBreakdown[k].PassengerCount;
                                switch (resp.Result[i].FareBreakdown[k].PassengerType)
                                {
                                    case TBOAir.TekTravelAir.PassengerType.Adult:
                                    case TBOAir.TekTravelAir.PassengerType.Senior:
                                        response[i].TBOFareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Adult;
                                        break;
                                    case TBOAir.TekTravelAir.PassengerType.Child:
                                        response[i].TBOFareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Child;
                                        break;
                                    case TBOAir.TekTravelAir.PassengerType.Infant:
                                        response[i].TBOFareBreakdown[k].PassengerType = CT.BookingEngine.PassengerType.Infant;
                                        break;
                                }

                                response[i].TBOFareBreakdown[k].TotalFare = (double)(response[i].TBOFareBreakdown[k].BaseFare + (double)resp.Result[i].FareBreakdown[k].Tax);
                                response[i].TBOFareBreakdown[k].SellingFare = response[i].TBOFareBreakdown[k].TotalFare;
                                response[i].TBOFareBreakdown[k].SupplierFare = (double)(resp.Result[i].FareBreakdown[k].BaseFare + resp.Result[i].FareBreakdown[k].Tax);
                            }

                            response[i].FareRules = new List<FareRule>();

                            foreach (WSFareRule fare in resp.Result[i].FareRule)
                            {
                                FareRule fr = new FareRule();
                                fr.Airline = fare.Airline;
                                fr.DepartureTime = fare.DepartureDate;
                                fr.Destination = fare.Destination;
                                fr.FareBasisCode = fare.FareBasisCode;
                                fr.FareRestriction = fare.FareRestriction;
                                fr.FareRuleDetail = fare.FareRuleDetail;
                                fr.Origin = fare.Origin;
                                fr.ReturnDate = fare.ReturnDate;
                                fr.SupplierBookingSource = (TBOBookingSource)Enum.Parse(typeof(TBOAir.TekTravelAir.BookingSource), fare.Source.ToString());
                                response[i].FareRules.Add(fr);
                            }

                            response[i].BaseFare = (double)((resp.Result[i].Fare.BaseFare * rateOfExchange));
                            response[i].Tax = (double)((resp.Result[i].Fare.Tax * rateOfExchange));
                            response[i].TotalFare = response[i].BaseFare + response[i].Tax + (double)(response[i].Price.AdditionalTxnFee + response[i].Price.AirlineTransFee + response[i].Price.OtherCharges + response[i].Price.SeviceTax);
                            response[i].ValidatingAirline = "";
                            response[i].SupplierBookingSource = (TBOBookingSource)Enum.Parse(typeof(TBOAir.TekTravelAir.BookingSource), resp.Result[i].Source.ToString());
                            response[i].TBOAirSessionId = resp.SessionId;
                            response[i].PromotionalPlanType = resp.Result[i].PromotionalPlanType.ToString();
                            response[i].SegmentKey = resp.Result[i].SegmentKey;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, 0, "(TBOAir)Failed to get SearchResponse from TBOAir. Reason : " + ex.ToString(), "");
            }

            return response;
        }

        private WSSearchRequest PrepareSearchRequest(SearchRequest request)
        {
            WSSearchRequest searchRequest = new WSSearchRequest();

            searchRequest.AdultCount = request.AdultCount;
            searchRequest.ChildCount = request.ChildCount;
            searchRequest.InfantCount = request.InfantCount;
            searchRequest.DepartureDate = request.Segments[0].PreferredDepartureTime;
            searchRequest.Destination = request.Segments[0].Destination;
            searchRequest.Origin = request.Segments[0].Origin;
            searchRequest.PreferredCarrier = "";
            searchRequest.PromotionalCode = new PromotionalCode[0];
            searchRequest.PromotionalPlanType = PromotionalPlanType.Normal;
            searchRequest.ReturnDate = request.Segments[request.Segments.Length - 1].PreferredDepartureTime;
            switch (request.Type)
            {
                case CT.BookingEngine.SearchType.OneWay:
                    searchRequest.Type = TBOAir.TekTravelAir.SearchType.OneWay;
                    break;
                case CT.BookingEngine.SearchType.Return:
                    searchRequest.Type = TBOAir.TekTravelAir.SearchType.Return;
                    break;
                case CT.BookingEngine.SearchType.MultiWay:
                    searchRequest.Type = TBOAir.TekTravelAir.SearchType.MultiWay;
                    break;
            }
            searchRequest.CabinClass = TBOAir.TekTravelAir.CabinClass.All;
            searchRequest.FlightSegments = new TBOAir.TekTravelAir.FlightSegment[request.Segments.Length];

            for (int i = 0; i < request.Segments.Length; i++)
            {
                searchRequest.FlightSegments[i] = new TBOAir.TekTravelAir.FlightSegment();
                searchRequest.FlightSegments[i].Destination = request.Segments[i].Destination;
                switch (request.Segments[i].flightCabinClass)
                {
                    case CT.BookingEngine.CabinClass.All:
                        searchRequest.FlightSegments[i].flightCabinClass = TBOAir.TekTravelAir.CabinClass.All;
                        break;
                    case CT.BookingEngine.CabinClass.Business:
                        searchRequest.FlightSegments[i].flightCabinClass = TBOAir.TekTravelAir.CabinClass.Business;
                        break;
                    case CT.BookingEngine.CabinClass.Economy:
                        searchRequest.FlightSegments[i].flightCabinClass = TBOAir.TekTravelAir.CabinClass.Economy;
                        break;
                    case CT.BookingEngine.CabinClass.First:
                        searchRequest.FlightSegments[i].flightCabinClass = TBOAir.TekTravelAir.CabinClass.First;
                        break;
                }
                searchRequest.FlightSegments[i].Origin = request.Segments[i].Origin;
                searchRequest.FlightSegments[i].PreferredAirlines = request.Segments[i].PreferredAirlines;
                searchRequest.FlightSegments[i].PreferredArrivalTime = request.Segments[i].PreferredArrivalTime;
                searchRequest.FlightSegments[i].PreferredDepartureTime = request.Segments[i].PreferredDepartureTime;
            }
            return searchRequest;
        }

        /// <summary>
        /// Returns the Fare Rules for the Itinerary.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<FareRule> GetFareRuleForTBO(SearchResult result)
        {
            WSFareRuleResponse response = new WSFareRuleResponse();
            List<FareRule> fareRules = new List<FareRule>();
            try
            {
                WSFareRuleRequest request = new WSFareRuleRequest();
                request.Result = GetResultObject(result);
                request.SessionId = result.TBOAirSessionId;

                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSFareRuleRequest));
                        StreamWriter sw = new StreamWriter(XmlPath + "TBOAirFareRuleRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, request);
                        sw.Close();
                    }
                }
                catch { }


                response = ba.GetFareRule(request);

                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSFareRuleResponse));
                        StreamWriter sw = new StreamWriter(XmlPath + "TBOAirFareRuleResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, response);
                        sw.Close();
                    }
                }
                catch { }


                if (response.FareRules != null)
                {
                    foreach (WSFareRule rule in response.FareRules)
                    {
                        FareRule fr = new FareRule();
                        fr.Airline = rule.Airline;
                        fr.DepartureTime = rule.DepartureDate;
                        fr.Destination = rule.Destination;
                        fr.FareBasisCode = rule.FareBasisCode;
                        fr.FareRestriction = rule.FareRestriction;
                        fr.FareRuleDetail = rule.FareRuleDetail;
                        fr.FareRuleKeyValue = "";
                        fr.Origin = rule.Origin;
                        fr.ReturnDate = rule.ReturnDate;
                        fareRules.Add(fr);
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, 0, "(TBOAir)Failed to Get FareRule for " + result.ResultBookingSource.ToString() + ". Reason : " + ex.ToString(), "");
            }

            return fareRules;
        }

        WSResult GetResultObject(SearchResult result)
        {
            WSResult resultObj = new WSResult();

            StaticData sd = new StaticData();
            sd.BaseCurrency = result.Price.SupplierCurrency;
            rateOfExchange = sd.CurrencyROE[agentBaseCurrency];

            resultObj.Fare = new WSFare();
            resultObj.Fare.AdditionalTxnFee = result.Price.AdditionalTxnFee * rateOfExchange;
            resultObj.Fare.AgentCommission = result.Price.AgentCommission * rateOfExchange;
            resultObj.Fare.AirTransFee = result.Price.AirlineTransFee * rateOfExchange;
            resultObj.Fare.BaseFare = (result.Price.PublishedFare * rateOfExchange) - (result.Price.Tax * rateOfExchange);
            resultObj.Fare.Currency = result.Price.SupplierCurrency;
            resultObj.Fare.Discount = result.Price.Discount * rateOfExchange;
            resultObj.Fare.OtherCharges = result.Price.OtherCharges * rateOfExchange;
            resultObj.Fare.PublishedPrice = result.Price.PublishedFare * rateOfExchange;
            resultObj.Fare.ReverseHandlingCharge = result.Price.ReverseHandlingCharge * rateOfExchange;
            resultObj.Fare.ServiceTax = result.Price.SeviceTax * rateOfExchange;
            resultObj.Fare.Tax = result.Price.Tax * rateOfExchange;
            resultObj.Fare.TransactionFee = result.Price.TransactionFee * rateOfExchange;
            resultObj.Fare.TdsOnCommission = result.Price.TdsCommission * rateOfExchange;
            resultObj.Fare.TdsOnPLB = result.Price.TDSPLB * rateOfExchange;

            resultObj.FareBreakdown = new WSPTCFare[result.FareBreakdown.Length];

            for (int i = 0; i < result.FareBreakdown.Length; i++)
            {
                resultObj.FareBreakdown[i] = new WSPTCFare();
                resultObj.FareBreakdown[i].AdditionalTxnFee = result.FareBreakdown[i].AdditionalTxnFee * rateOfExchange;
                resultObj.FareBreakdown[i].AirlineTransFee = result.FareBreakdown[i].AirlineTransFee * rateOfExchange;
                resultObj.FareBreakdown[i].BaseFare = (decimal)result.FareBreakdown[i].BaseFare * rateOfExchange;
                resultObj.FareBreakdown[i].PassengerCount = result.FareBreakdown[i].PassengerCount;
                switch (result.FareBreakdown[i].PassengerType)
                {
                    case CT.BookingEngine.PassengerType.Adult:
                    case CT.BookingEngine.PassengerType.Senior:
                        resultObj.FareBreakdown[i].PassengerType = TBOAir.TekTravelAir.PassengerType.Adult;
                        break;
                    case CT.BookingEngine.PassengerType.Child:
                        resultObj.FareBreakdown[i].PassengerType = TBOAir.TekTravelAir.PassengerType.Child;
                        break;
                    case CT.BookingEngine.PassengerType.Infant:
                        resultObj.FareBreakdown[i].PassengerType = TBOAir.TekTravelAir.PassengerType.Infant;
                        break;
                }
                resultObj.FareBreakdown[i].Tax = result.FareBreakdown[i].Tax * rateOfExchange;

            }

            if (result.Flights.Length > 1)
            {
                resultObj.Segment = new WSSegment[result.Flights[0].Length + result.Flights[1].Length];
            }
            else
            {
                resultObj.Segment = new WSSegment[result.Flights[0].Length];
            }

            int counter = 0;
            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    resultObj.Segment[counter] = new WSSegment();
                    resultObj.Segment[counter].Airline = new WSAirline();
                    resultObj.Segment[counter].Airline.AirlineCode = result.Flights[i][j].Airline;
                    resultObj.Segment[counter].AirlinePNR = result.Flights[i][j].AirlinePNR;
                    resultObj.Segment[counter].ArrTime = result.Flights[i][j].ArrivalTime;
                    resultObj.Segment[counter].Craft = result.Flights[i][j].Craft;
                    resultObj.Segment[counter].DepTIme = result.Flights[i][j].DepartureTime;

                    resultObj.Segment[counter].Destination = new WSAirport();
                    resultObj.Segment[counter].Destination.AirportCode = result.Flights[i][j].Destination.AirportCode;
                    resultObj.Segment[counter].Destination.AirportName = result.Flights[i][j].Destination.AirportName;
                    resultObj.Segment[counter].Destination.CityCode = result.Flights[i][j].Destination.CityCode;
                    resultObj.Segment[counter].Destination.CityName = result.Flights[i][j].Destination.CityName;
                    resultObj.Segment[counter].Destination.CountryCode = result.Flights[i][j].Destination.CountryCode;
                    resultObj.Segment[counter].Destination.CountryName = result.Flights[i][j].Destination.CountryName;
                    resultObj.Segment[counter].Destination.Terminal = result.Flights[i][j].ArrTerminal;

                    resultObj.Segment[counter].Duration = result.Flights[i][j].Duration.ToString();
                    resultObj.Segment[counter].ETicketEligible = result.Flights[i][j].ETicketEligible;
                    resultObj.Segment[counter].FareClass = result.Flights[i][j].FareInfoKey;
                    resultObj.Segment[counter].FlightNumber = result.Flights[i][j].FlightNumber;
                    resultObj.Segment[counter].OperatingCarrier = result.Flights[i][j].OperatingCarrier;
                    resultObj.Segment[counter].SegmentIndicator = result.Flights[i][j].Group;
                    resultObj.Segment[counter].Status = result.Flights[i][j].Status;
                    resultObj.Segment[counter].Stop = result.Flights[i][j].Stops;

                    resultObj.Segment[counter].Origin = new WSAirport();
                    resultObj.Segment[counter].Origin.AirportCode = result.Flights[i][j].Origin.AirportCode;
                    resultObj.Segment[counter].Origin.AirportName = result.Flights[i][j].Origin.AirportName;
                    resultObj.Segment[counter].Origin.CityCode = result.Flights[i][j].Origin.CityCode;
                    resultObj.Segment[counter].Origin.CityName = result.Flights[i][j].Origin.CityName;
                    resultObj.Segment[counter].Origin.CountryCode = result.Flights[i][j].Origin.CountryCode;
                    resultObj.Segment[counter].Origin.CountryName = result.Flights[i][j].Origin.CountryName;
                    resultObj.Segment[counter].Origin.Terminal = result.Flights[i][j].DepTerminal;
                    counter++;
                }
            }

            resultObj.IbSegCount = counter;
            resultObj.ObSegCount = counter;

            resultObj.FareRule = new WSFareRule[result.FareRules.Count];

            for (int i = 0; i < result.FareRules.Count; i++)
            {
                resultObj.FareRule[i] = new WSFareRule();
                resultObj.FareRule[i].Airline = result.FareRules[i].Airline;
                resultObj.FareRule[i].DepartureDate = result.FareRules[i].DepartureTime;
                resultObj.FareRule[i].Destination = result.FareRules[i].Destination;
                resultObj.FareRule[i].FareBasisCode = result.FareRules[i].FareBasisCode;
                resultObj.FareRule[i].FareRestriction = result.FareRules[i].FareRestriction;
                resultObj.FareRule[i].FareRuleDetail = result.FareRules[i].FareRuleDetail;
                resultObj.FareRule[i].Origin = result.FareRules[i].Origin;
                resultObj.FareRule[i].ReturnDate = result.FareRules[i].ReturnDate;
                resultObj.FareRule[i].Source = (TBOAir.TekTravelAir.BookingSource)Enum.Parse(typeof(TBOBookingSource), result.FareRules[i].SupplierBookingSource.ToString());
            }

            resultObj.NonRefundable = result.NonRefundable;
            switch (result.PromotionalPlanType)
            {
                case "Normal":
                    resultObj.PromotionalPlanType = PromotionalPlanType.Normal;
                    break;
                case "LCCSpecialReturn":
                    resultObj.PromotionalPlanType = PromotionalPlanType.LCCSpecialReturn;
                    break;
                case "GDSSpecialReturn":
                    resultObj.PromotionalPlanType = PromotionalPlanType.GDSSpecialReturn;
                    break;
            }

            resultObj.TripIndicator = 1;//TEMP
            resultObj.SegmentKey = result.SegmentKey;
            resultObj.Source = (TBOAir.TekTravelAir.BookingSource)Enum.Parse(typeof(TBOBookingSource), result.SupplierBookingSource.ToString());

            return resultObj;
        }

        /// <summary>
        /// Returns the updated Fare Quotes for the Itinerary.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public void GetFareQuoteForTBO(ref SearchResult result)
        {
            WSGetFareQuoteResponse response = new WSGetFareQuoteResponse();
            WSGetFareQuoteRequest request = new WSGetFareQuoteRequest();

            try
            {
                #region Prepare Outbound and Inbound Results

                List<WSResult> results = new List<WSResult>();

                for (int i = 0; i < result.Flights.Length; i++)
                {
                    FlightInfo fi = result.Flights[i][0];

                    WSResult res = new WSResult();
                    res.Destination = fi.Destination.AirportCode;
                    res.IbDuration = fi.Duration.ToString();
                    res.IbSegCount = 1;
                    res.IsLcc = true;
                    res.NonRefundable = result.NonRefundable;
                    res.ObDuration = fi.Duration.ToString();
                    res.ObSegCount = 1;
                    res.Origin = fi.Origin.AirportCode;
                    res.PromotionalPlanType = (PromotionalPlanType)Enum.Parse(typeof(PromotionalPlanType), result.PromotionalPlanType);
                    res.SegmentKey = result.SegmentKey;
                    res.Source = (TBOAir.TekTravelAir.BookingSource)Enum.Parse(typeof(CT.BookingEngine.TBOBookingSource), result.SupplierBookingSource.ToString());
                    res.TripIndicator = fi.Group + 1;

                    res.Segment = new WSSegment[1];
                    res.Segment[0] = new WSSegment();
                    res.Segment[0].Airline = new WSAirline();
                    res.Segment[0].Airline.AirlineCode = fi.Airline;
                    res.Segment[0].ArrTime = fi.ArrivalTime;
                    res.Segment[0].Craft = fi.Craft;
                    res.Segment[0].DepTIme = fi.DepartureTime;
                    res.Segment[0].Destination = new WSAirport();
                    res.Segment[0].Destination.AirportCode = fi.Destination.AirportCode;
                    res.Segment[0].Destination.AirportName = fi.Destination.AirportName;
                    res.Segment[0].Destination.CityCode = fi.Destination.CityCode;
                    res.Segment[0].Destination.CityName = fi.Destination.CityName;
                    res.Segment[0].Destination.CountryCode = fi.Destination.CountryCode;
                    res.Segment[0].Destination.CountryName = fi.Destination.CountryName;
                    res.Segment[0].Destination.Terminal = fi.DepTerminal;
                    res.Segment[0].Duration = fi.Duration.ToString();
                    res.Segment[0].ETicketEligible = fi.ETicketEligible;
                    res.Segment[0].FlightNumber = fi.FlightNumber;
                    res.Segment[0].OperatingCarrier = fi.OperatingCarrier;
                    res.Segment[0].Origin = new WSAirport();
                    res.Segment[0].Origin.AirportCode = fi.Origin.AirportCode;
                    res.Segment[0].Origin.AirportName = fi.Origin.AirportName;
                    res.Segment[0].Origin.CityCode = fi.Origin.CityCode;
                    res.Segment[0].Origin.CityName = fi.Origin.CityName;
                    res.Segment[0].Origin.CountryCode = fi.Origin.CountryCode;
                    res.Segment[0].Origin.CountryName = fi.Origin.CountryName;
                    res.Segment[0].Origin.Terminal = fi.ArrTerminal;
                    res.Segment[0].SegmentIndicator = fi.Group + 1;
                    res.Segment[0].Status = fi.Status;
                    res.Segment[0].Stop = fi.Stops;


                    res.Fare = new WSFare();
                    res.Fare.AdditionalTxnFee = fi.SegmentPrice.AdditionalTxnFee;
                    res.Fare.AgentCommission = fi.SegmentPrice.AgentCommission;
                    res.Fare.AgentConvienceCharges = fi.SegmentPrice.AgentConvienceCharges;
                    res.Fare.AgentServiceCharge = fi.SegmentPrice.AgentServiceCharge;
                    res.Fare.AirTransFee = fi.SegmentPrice.AirlineTransFee;
                    res.Fare.BaseFare = fi.SegmentPrice.BaseFare;
                    res.Fare.ChargeBU = new TBOAir.TekTravelAir.ChargeBreakUp[fi.SegmentPrice.ChargeBU.Count];
                    res.Fare.Currency = "";
                    res.Fare.FuelSurcharge = fi.SegmentPrice.FuelSurcharge;
                    res.Fare.IncentiveEarned = fi.SegmentPrice.IncentiveEarned;
                    res.Fare.OfferedFare = fi.SegmentPrice.NetFare;
                    res.Fare.OtherCharges = fi.SegmentPrice.OtherCharges;
                    res.Fare.PLBEarned = fi.SegmentPrice.OurPLB;
                    res.Fare.PublishedPrice = fi.SegmentPrice.PublishedFare;
                    res.Fare.ReverseHandlingCharge = fi.SegmentPrice.ReverseHandlingCharge;
                    res.Fare.ServiceTax = fi.SegmentPrice.SeviceTax;
                    res.Fare.Tax = fi.SegmentPrice.Tax;
                    res.Fare.TdsOnCommission = fi.SegmentPrice.TdsCommission;
                    res.Fare.TdsOnIncentive = fi.SegmentPrice.TDSIncentive;
                    res.Fare.TdsOnPLB = fi.SegmentPrice.TDSPLB;
                    res.Fare.TransactionFee = fi.SegmentPrice.TransactionFee;

                    res.FareBreakdown = new WSPTCFare[fi.SegmentFareBreakdown.Count];
                    for (int j = 0; j < fi.SegmentFareBreakdown.Count; j++)
                    {
                        res.FareBreakdown[j] = new WSPTCFare();
                        res.FareBreakdown[j].AdditionalTxnFee = fi.SegmentFareBreakdown[j].AdditionalTxnFee;
                        res.FareBreakdown[j].AirlineTransFee = fi.SegmentFareBreakdown[j].AirlineTransFee;
                        res.FareBreakdown[j].BaseFare = (decimal)fi.SegmentFareBreakdown[j].BaseFare;
                        res.FareBreakdown[j].PassengerCount = fi.SegmentFareBreakdown[j].PassengerCount;
                        res.FareBreakdown[j].PassengerType = (TBOAir.TekTravelAir.PassengerType)Enum.Parse(typeof(CT.BookingEngine.PassengerType), fi.SegmentFareBreakdown[j].PassengerType.ToString());
                        res.FareBreakdown[j].Tax = fi.SegmentFareBreakdown[j].Tax;
                    }

                    res.FareRule = new WSFareRule[fi.SegmentFareRule.Count];
                    for (int k = 0; k < fi.SegmentFareRule.Count; k++)
                    {
                        FareRule fare = fi.SegmentFareRule[k];
                        res.FareRule[k] = new WSFareRule();
                        res.FareRule[k].Airline = fare.Airline;
                        res.FareRule[k].DepartureDate = fare.DepartureTime;
                        res.FareRule[k].Destination = fare.Destination;
                        res.FareRule[k].FareBasisCode = fare.FareBasisCode;
                        res.FareRule[k].FareRestriction = fare.FareRestriction;
                        res.FareRule[k].FareRuleDetail = fare.FareRuleDetail;
                        res.FareRule[k].Origin = fare.Origin;
                        res.FareRule[k].ReturnDate = fare.ReturnDate;
                        res.FareRule[k].Source = (TBOAir.TekTravelAir.BookingSource)Enum.Parse(typeof(TBOBookingSource), fare.SupplierBookingSource.ToString());
                    }
                    results.Add(res);
                }
                #endregion

                request.Result = results.ToArray();
                request.SessionId = result.TBOAirSessionId;

                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSGetFareQuoteRequest));
                        StreamWriter sw = new StreamWriter(XmlPath + "TBOAirFareQuoteRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, request);
                        sw.Close();
                    }
                }
                catch { }

                response = ba.GetFareQuote(request);

                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSGetFareQuoteResponse));
                        StreamWriter sw = new StreamWriter(XmlPath + "TBOAirFareQuoteResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, response);
                        sw.Close();
                    }
                }
                catch { }

                rateOfExchange = exchangeRates[response.Result.Fare.Currency];

                result.Price.AdditionalTxnFee = response.Result.Fare.AdditionalTxnFee * rateOfExchange;
                result.Price.AgentCommission = response.Result.Fare.AgentCommission * rateOfExchange;
                result.Price.AgentConvienceCharges = response.Result.Fare.AgentConvienceCharges * rateOfExchange;
                result.Price.AgentServiceCharge = response.Result.Fare.AgentServiceCharge * rateOfExchange;
                result.Price.AirlineTransFee = response.Result.Fare.AirTransFee * rateOfExchange;
                result.Price.BaseFare = response.Result.Fare.BaseFare * rateOfExchange;
                result.Price.FuelSurcharge = response.Result.Fare.FuelSurcharge * rateOfExchange;
                result.Price.IncentiveEarned = response.Result.Fare.IncentiveEarned * rateOfExchange;
                result.Price.NetFare = response.Result.Fare.OfferedFare * rateOfExchange;
                result.Price.OtherCharges = response.Result.Fare.OtherCharges * rateOfExchange;
                result.Price.OurPLB = response.Result.Fare.PLBEarned * rateOfExchange;
                result.Price.PublishedFare = response.Result.Fare.PublishedPrice * rateOfExchange;
                result.Price.ReverseHandlingCharge = response.Result.Fare.ReverseHandlingCharge * rateOfExchange;
                result.Price.SeviceTax = response.Result.Fare.ServiceTax * rateOfExchange;
                result.Price.Tax = response.Result.Fare.Tax * rateOfExchange;
                result.Price.TdsCommission = response.Result.Fare.TdsOnCommission * rateOfExchange;
                result.Price.TDSIncentive = response.Result.Fare.TdsOnIncentive * rateOfExchange;
                result.Price.TDSPLB = response.Result.Fare.TdsOnPLB * rateOfExchange;
                result.Price.TransactionFee = response.Result.Fare.TransactionFee * rateOfExchange;
                result.Price.ChargeBU = new List<CT.BookingEngine.ChargeBreakUp>();
                foreach (TBOAir.TekTravelAir.ChargeBreakUp charge in response.Result.Fare.ChargeBU)
                {
                    CT.BookingEngine.ChargeBreakUp cb = new CT.BookingEngine.ChargeBreakUp();
                    cb.Amount = charge.Amount * rateOfExchange;
                    switch (charge.ChargeType)
                    {
                        case TBOAir.TekTravelAir.ChargeType.CreditCardCharge:
                            cb.ChargeType = CT.BookingEngine.ChargeType.CreditCardCharge;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.BankHandlingCharges:
                            cb.ChargeType = CT.BookingEngine.ChargeType.BankHandlingCharges;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.CurrencyConversionCharges:
                            cb.ChargeType = CT.BookingEngine.ChargeType.CurrencyConversionCharges;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.OtherCharges:
                            cb.ChargeType = CT.BookingEngine.ChargeType.OtherCharges;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.RevHandlingCharge:
                            cb.ChargeType = CT.BookingEngine.ChargeType.RevHandlingCharge;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.ServiceCharge:
                            cb.ChargeType = CT.BookingEngine.ChargeType.ServiceCharge;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.SupplierMarkup:
                            cb.ChargeType = CT.BookingEngine.ChargeType.SupplierMarkup;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.TboMarkup:
                            cb.ChargeType = CT.BookingEngine.ChargeType.TBOMarkup;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.TrainBedRollCharge:
                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainBedRollCharge;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.TrainOtherCharge:
                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainOtherCharge;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.TrainReservationCharge:
                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainReservationCharge;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.TrainSuperFastCharge:
                            break;
                    }
                    cb.PriceId = charge.PriceId;
                    result.Price.ChargeBU.Add(cb);
                }



                result.TBOPrice.AdditionalTxnFee = response.Result.Fare.AdditionalTxnFee;
                result.TBOPrice.AgentCommission = response.Result.Fare.AgentCommission;
                result.TBOPrice.AgentConvienceCharges = response.Result.Fare.AgentConvienceCharges;
                result.TBOPrice.AgentServiceCharge = response.Result.Fare.AgentServiceCharge;
                result.TBOPrice.AirlineTransFee = response.Result.Fare.AirTransFee;
                result.TBOPrice.BaseFare = response.Result.Fare.BaseFare;
                result.TBOPrice.FuelSurcharge = response.Result.Fare.FuelSurcharge;
                result.TBOPrice.IncentiveEarned = response.Result.Fare.IncentiveEarned;
                result.TBOPrice.NetFare = response.Result.Fare.OfferedFare;
                result.TBOPrice.OtherCharges = response.Result.Fare.OtherCharges;
                result.TBOPrice.OurPLB = response.Result.Fare.PLBEarned;
                result.TBOPrice.PublishedFare = response.Result.Fare.PublishedPrice;
                result.TBOPrice.ReverseHandlingCharge = response.Result.Fare.ReverseHandlingCharge;
                result.TBOPrice.SeviceTax = response.Result.Fare.ServiceTax;
                result.TBOPrice.Tax = response.Result.Fare.Tax;
                result.TBOPrice.TdsCommission = response.Result.Fare.TdsOnCommission;
                result.TBOPrice.TDSIncentive = response.Result.Fare.TdsOnIncentive;
                result.TBOPrice.TDSPLB = response.Result.Fare.TdsOnPLB;
                result.TBOPrice.TransactionFee = response.Result.Fare.TransactionFee;
                result.TBOPrice.ChargeBU = new List<CT.BookingEngine.ChargeBreakUp>();
                foreach (TBOAir.TekTravelAir.ChargeBreakUp charge in response.Result.Fare.ChargeBU)
                {
                    CT.BookingEngine.ChargeBreakUp cb = new CT.BookingEngine.ChargeBreakUp();
                    cb.Amount = charge.Amount;
                    switch (charge.ChargeType)
                    {
                        case TBOAir.TekTravelAir.ChargeType.CreditCardCharge:
                            cb.ChargeType = CT.BookingEngine.ChargeType.CreditCardCharge;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.BankHandlingCharges:
                            cb.ChargeType = CT.BookingEngine.ChargeType.BankHandlingCharges;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.CurrencyConversionCharges:
                            cb.ChargeType = CT.BookingEngine.ChargeType.CurrencyConversionCharges;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.OtherCharges:
                            cb.ChargeType = CT.BookingEngine.ChargeType.OtherCharges;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.RevHandlingCharge:
                            cb.ChargeType = CT.BookingEngine.ChargeType.RevHandlingCharge;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.ServiceCharge:
                            cb.ChargeType = CT.BookingEngine.ChargeType.ServiceCharge;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.SupplierMarkup:
                            cb.ChargeType = CT.BookingEngine.ChargeType.SupplierMarkup;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.TboMarkup:
                            cb.ChargeType = CT.BookingEngine.ChargeType.TBOMarkup;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.TrainBedRollCharge:
                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainBedRollCharge;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.TrainOtherCharge:
                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainOtherCharge;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.TrainReservationCharge:
                            cb.ChargeType = CT.BookingEngine.ChargeType.TrainReservationCharge;
                            break;
                        case TBOAir.TekTravelAir.ChargeType.TrainSuperFastCharge:
                            break;
                    }
                    cb.PriceId = charge.PriceId;
                    result.TBOPrice.ChargeBU.Add(cb);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, 0, "(TBOAir)Failed to Get Fare Quote. Reason : " + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Creates HOLD booking for Non-LCC only. The booking will be Ready state. 
        /// You need to call Ticket() for creating Ticket if the response is success for this method.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public BookingResponse BookForTBO(ref FlightItinerary itinerary)
        {
            WSBookResponse wsResponse = new WSBookResponse();
            BookingResponse response = new BookingResponse();
            try
            {
                StaticData sd = new StaticData();
                sd.BaseCurrency = itinerary.Passenger[0].Price.SupplierCurrency;
                rateOfExchange = sd.CurrencyROE[agentBaseCurrency];

                WSBookRequest request = new WSBookRequest();

                FillBookingRequest(ref request, itinerary);

                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSBookRequest));
                        StreamWriter sw = new StreamWriter(XmlPath + "TBOAirBookRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, request);
                        sw.Close();
                    }
                }
                catch { }

                wsResponse = ba.Book(request);

                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSBookResponse));
                        StreamWriter sw = new StreamWriter(XmlPath + "TBOAirBookingResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, response);
                        sw.Close();
                    }
                }
                catch { }

                if (wsResponse.PNR != null && wsResponse.Status != null)
                {
                    Audit.Add(EventType.TBOAir, Severity.Low, 0, "(TBO Air)Booking Response success : " + wsResponse.PNR, HttpContext.Current.Request["REMOTE_ADDR"]);
                    //wsResponse.Status.StatusCode 06 for Success
                    response.ConfirmationNo = wsResponse.PNR;
                    response.PNR = wsResponse.PNR;
                    response.SSRDenied = wsResponse.SSRDenied;
                    response.SSRMessage = wsResponse.SSRMessage;
                    response.Status = BookingResponseStatus.Successful;

                    itinerary.PNR = wsResponse.PNR;
                    itinerary.AirLocatorCode = wsResponse.BookingId;
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.Low, 0, "(TBO Air)Booking Response Failed", HttpContext.Current.Request["REMOTE_ADDR"]);
                    response.Status = BookingResponseStatus.Failed;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, 0, "(TBOAir)Failed to Book. Reason : " + ex.ToString(), "");
            }

            return response;
        }

        void FillBookingRequest(ref WSBookRequest request, FlightItinerary itinerary)
        {
            Audit.Add(EventType.TBOAir, Severity.Low, 0, "(TBO Air)Start preparing Booking Request", HttpContext.Current.Request["REMOTE_ADDR"]);
            request.Destination = itinerary.Destination;
            request.Fare = new WSFare();
            request.FareType = "PUB";
            request.InstantTicket = true;
            request.Origin = itinerary.Origin;
            request.PromotionalPlanType = (PromotionalPlanType)Enum.Parse(typeof(PromotionalPlanType), itinerary.PromotionalPlanType);
            request.SegmentKey = itinerary.SegmentKey;
            request.SessionId = itinerary.TBOAirSessionId;
            request.Source = (TBOAir.TekTravelAir.BookingSource)Enum.Parse(typeof(TBOBookingSource), itinerary.SupplierBookingSource.ToString());
            request.Remarks = "";

            decimal bookingAmount = 0;

            int cntr = 0, paxCount = 0;
            foreach (FlightPassenger pax in itinerary.Passenger)
            {
                if (pax.Type == CT.BookingEngine.PassengerType.Adult || pax.Type == CT.BookingEngine.PassengerType.Child)
                {
                    paxCount++;
                }
            }
            bookingAmount = itinerary.Passenger[0].TBOPrice.OtherCharges;
            request.Passenger = new WSPassenger[itinerary.Passenger.Length];
            foreach (FlightPassenger pax in itinerary.Passenger)
            {
                bookingAmount = (pax.TBOPrice.PublishedFare);
                //Setting Combined Fare for Itinerary
                request.Fare.AdditionalTxnFee = pax.TBOPrice.AdditionalTxnFee;
                request.Fare.AgentCommission = pax.TBOPrice.AgentCommission;
                request.Fare.AirTransFee = pax.TBOPrice.AirlineTransFee;
                request.Fare.BaseFare = pax.TBOPrice.BaseFare;
                request.Fare.Currency = pax.TBOPrice.Currency;
                request.Fare.PublishedPrice = pax.TBOPrice.PublishedFare;
                request.Fare.ReverseHandlingCharge = pax.TBOPrice.ReverseHandlingCharge;
                request.Fare.ServiceTax = pax.TBOPrice.SeviceTax;
                request.Fare.Tax = (pax.TBOPrice.Tax);
                request.Fare.TdsOnCommission = pax.TBOPrice.TdsCommission;
                request.Fare.TdsOnPLB = pax.TBOPrice.TDSPLB;
                request.Fare.TransactionFee = pax.TBOPrice.TransactionFee;
                request.Fare.OtherCharges = pax.TBOPrice.OtherCharges;
                request.Fare.FuelSurcharge = pax.TBOPrice.FuelSurcharge;
                request.Fare.OfferedFare = pax.TBOPrice.NetFare;
                request.Fare.AgentConvienceCharges = pax.TBOPrice.AgentConvienceCharges;
                request.Fare.AgentServiceCharge = pax.TBOPrice.AgentServiceCharge;
                request.Fare.IncentiveEarned = pax.TBOPrice.IncentiveEarned;
                request.Fare.TdsOnIncentive = pax.TBOPrice.TDSIncentive;

                request.Fare.ChargeBU = new TBOAir.TekTravelAir.ChargeBreakUp[pax.TBOPrice.ChargeBU.Count];
                for (int i = 0; i < pax.TBOPrice.ChargeBU.Count; i++)
                {
                    request.Fare.ChargeBU[i] = new TBOAir.TekTravelAir.ChargeBreakUp();
                    request.Fare.ChargeBU[i].Amount = pax.TBOPrice.ChargeBU[i].Amount;
                    switch (pax.TBOPrice.ChargeBU[i].ChargeType)
                    {
                        case CT.BookingEngine.ChargeType.TBOMarkup:
                            request.Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.TboMarkup;
                            break;
                        case CT.BookingEngine.ChargeType.OtherCharges:
                            request.Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.OtherCharges;
                            break;
                        case CT.BookingEngine.ChargeType.CreditCardCharge:
                            request.Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.CreditCardCharge;
                            break;
                        case CT.BookingEngine.ChargeType.ServiceCharge:
                            request.Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.ServiceCharge;
                            break;
                        case CT.BookingEngine.ChargeType.RevHandlingCharge:
                            request.Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.RevHandlingCharge;
                            break;
                        case CT.BookingEngine.ChargeType.BankHandlingCharges:
                            request.Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.BankHandlingCharges;
                            break;
                        case CT.BookingEngine.ChargeType.SupplierMarkup:
                            request.Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.SupplierMarkup;
                            break;
                        case CT.BookingEngine.ChargeType.CurrencyConversionCharges:
                            request.Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.CurrencyConversionCharges;
                            break;
                    }
                    request.Fare.ChargeBU[i].PriceId = pax.TBOPrice.ChargeBU[i].PriceId;
                }

                //Setting Pax
                request.Passenger[cntr] = new WSPassenger();
                request.Passenger[cntr].AddressLine1 = itinerary.Passenger[cntr].AddressLine1;
                request.Passenger[cntr].AddressLine2 = itinerary.Passenger[cntr].AddressLine2;
                request.Passenger[cntr].Country = itinerary.Passenger[cntr].Country.CountryCode;
                request.Passenger[cntr].DateOfBirth = itinerary.Passenger[cntr].DateOfBirth;
                request.Passenger[cntr].Email = itinerary.Passenger[cntr].Email;
                request.Passenger[cntr].FFAirline = itinerary.Passenger[cntr].FFAirline;
                request.Passenger[cntr].FFNumber = itinerary.Passenger[cntr].FFNumber;
                request.Passenger[cntr].FirstName = itinerary.Passenger[cntr].FirstName;
                request.Passenger[cntr].Gender = (TBOAir.TekTravelAir.Gender)Enum.Parse(typeof(CT.BookingEngine.Gender), itinerary.Passenger[cntr].Gender.ToString());
                request.Passenger[cntr].LastName = itinerary.Passenger[cntr].LastName;
                request.Passenger[cntr].PassportExpiry = itinerary.Passenger[cntr].PassportExpiry;
                request.Passenger[cntr].PassportNumber = itinerary.Passenger[cntr].PassportNo;
                request.Passenger[cntr].Phone = itinerary.Passenger[cntr].CellPhone;
                request.Passenger[cntr].Title = itinerary.Passenger[cntr].Title;
                request.Passenger[cntr].Meal = new TBOAir.TekTravelAir.Meal();
                request.Passenger[cntr].Meal.Code = "";
                request.Passenger[cntr].Meal.Description = "";
                request.Passenger[cntr].Seat = new TBOAir.TekTravelAir.Seat();
                request.Passenger[cntr].Seat.Code = "";
                request.Passenger[cntr].Seat.Description = "";
                request.Passenger[cntr].Ssr = new WSSSR();
                request.Passenger[cntr].PinCode = "";

                foreach (Fare fbd in itinerary.TBOFareBreakdown)
                {
                    if (itinerary.Passenger[cntr].Type == fbd.PassengerType)
                    {
                        //Individual Pax Fare
                        request.Passenger[cntr].Fare = new WSFare();
                        request.Passenger[cntr].Fare.AdditionalTxnFee = fbd.AdditionalTxnFee / paxCount;
                        //request.Passenger[cntr].Fare.AgentCommission = itinerary.Passenger[cntr].TBOPrice.AgentCommission;
                        //request.Passenger[cntr].Fare.AirTransFee = itinerary.Passenger[cntr].TBOPrice.AirlineTransFee;
                        request.Passenger[cntr].Fare.BaseFare = (decimal)fbd.BaseFare / fbd.PassengerCount;
                        //request.Passenger[cntr].Fare.Currency = itinerary.Passenger[cntr].TBOPrice.Currency;

                        //request.Passenger[cntr].Fare.ReverseHandlingCharge = itinerary.Passenger[cntr].TBOPrice.ReverseHandlingCharge;
                        //request.Passenger[cntr].Fare.ServiceTax = itinerary.Passenger[cntr].TBOPrice.SeviceTax;
                        request.Passenger[cntr].Fare.Tax = fbd.Tax / fbd.PassengerCount;
                        //request.Passenger[cntr].Fare.TdsOnCommission = itinerary.Passenger[cntr].TBOPrice.TdsCommission;
                        //request.Passenger[cntr].Fare.TdsOnPLB = itinerary.Passenger[cntr].TBOPrice.TDSPLB;
                        //request.Passenger[cntr].Fare.TransactionFee = itinerary.Passenger[cntr].TBOPrice.TransactionFee;

                        //request.Passenger[cntr].Fare.AgentServiceCharge = itinerary.Passenger[cntr].TBOPrice.AgentServiceCharge;
                        //request.Passenger[cntr].Fare.AgentConvienceCharges = itinerary.Passenger[cntr].TBOPrice.AgentConvienceCharges;
                        if (pax.Type != CT.BookingEngine.PassengerType.Infant)
                        {
                            //request.Passenger[cntr].Fare.PublishedPrice = (decimal)((decimal)fbd.TotalFare + fbd.AdditionalTxnFee);

                            request.Passenger[cntr].Fare.FuelSurcharge = (itinerary.Passenger[cntr].TBOPrice.FuelSurcharge / paxCount);
                            //request.Passenger[cntr].Fare.OfferedFare = (itinerary.Passenger[cntr].TBOPrice.NetFare / paxCount);
                        }
                        else
                        {
                            //request.Passenger[cntr].Fare.PublishedPrice = (decimal)((decimal)fbd.TotalFare + fbd.AdditionalTxnFee);
                        }
                        break;
                    }
                }
                //if (cntr == 0)
                //{
                //    request.Passenger[cntr].Fare.ChargeBU = new TBOAir.TekTravelAir.ChargeBreakUp[itinerary.Passenger[cntr].TBOPrice.ChargeBU.Count];
                //    for (int i = 0; i < itinerary.Passenger[cntr].TBOPrice.ChargeBU.Count; i++)
                //    {
                //        request.Passenger[cntr].Fare.ChargeBU[i] = new TBOAir.TekTravelAir.ChargeBreakUp();
                //        request.Passenger[cntr].Fare.ChargeBU[i].Amount = itinerary.Passenger[cntr].TBOPrice.ChargeBU[i].Amount;
                //        switch (itinerary.Passenger[cntr].TBOPrice.ChargeBU[i].ChargeType)
                //        {
                //            case CT.BookingEngine.ChargeType.TBOMarkup:
                //                request.Passenger[cntr].Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.TboMarkup;
                //                break;
                //            case CT.BookingEngine.ChargeType.OtherCharges:
                //                request.Passenger[cntr].Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.OtherCharges;
                //                break;
                //            case CT.BookingEngine.ChargeType.CreditCardCharge:
                //                request.Passenger[cntr].Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.CreditCardCharge;
                //                break;
                //            case CT.BookingEngine.ChargeType.ServiceCharge:
                //                request.Passenger[cntr].Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.ServiceCharge;
                //                break;
                //            case CT.BookingEngine.ChargeType.RevHandlingCharge:
                //                request.Passenger[cntr].Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.RevHandlingCharge;
                //                break;
                //            case CT.BookingEngine.ChargeType.BankHandlingCharges:
                //                request.Passenger[cntr].Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.BankHandlingCharges;
                //                break;
                //            case CT.BookingEngine.ChargeType.SupplierMarkup:
                //                request.Passenger[cntr].Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.SupplierMarkup;
                //                break;
                //            case CT.BookingEngine.ChargeType.CurrencyConversionCharges:
                //                request.Passenger[cntr].Fare.ChargeBU[i].ChargeType = TBOAir.TekTravelAir.ChargeType.CurrencyConversionCharges;
                //                break;
                //        }
                //        request.Passenger[cntr].Fare.ChargeBU[i].PriceId = itinerary.Passenger[cntr].TBOPrice.ChargeBU[i].PriceId;
                //    }
                //}
                //else
                {
                    request.Passenger[cntr].Fare.ChargeBU = new TBOAir.TekTravelAir.ChargeBreakUp[0];
                }

                cntr++;
            }

            request.FareRule = new WSFareRule[itinerary.FareRules.Count];
            for (int i = 0; i < itinerary.FareRules.Count; i++)
            {
                request.FareRule[i] = new WSFareRule();
                request.FareRule[i].Airline = itinerary.FareRules[i].Airline;
                request.FareRule[i].DepartureDate = itinerary.FareRules[i].DepartureTime;
                request.FareRule[i].Destination = itinerary.FareRules[i].Destination;
                request.FareRule[i].FareBasisCode = itinerary.FareRules[i].FareBasisCode;
                request.FareRule[i].FareRestriction = itinerary.FareRules[i].FareRestriction;
                request.FareRule[i].FareRuleDetail = itinerary.FareRules[i].FareRuleDetail;
                request.FareRule[i].Origin = itinerary.FareRules[i].Origin;
                request.FareRule[i].ReturnDate = itinerary.FareRules[i].ReturnDate;
                request.FareRule[i].Source = (TBOAir.TekTravelAir.BookingSource)Enum.Parse(typeof(TBOBookingSource), itinerary.FareRules[i].SupplierBookingSource.ToString());
            }

            request.PaymentInformation = new WSPaymentInformation();
            request.PaymentInformation.Amount = bookingAmount;
            request.PaymentInformation.InvoiceNumber = 0;
            request.PaymentInformation.IPAddress = HttpContext.Current.Request["REMOTE_ADDR"];
            request.PaymentInformation.PaymentGateway = TBOAir.TekTravelAir.PaymentGatewaySource.APICustomer;
            request.PaymentInformation.PaymentId = "0";
            request.PaymentInformation.PaymentInformationId = 0;
            request.PaymentInformation.PaymentModeType = PaymentModeType.Deposited;
            request.PaymentInformation.TrackId = new Random(214758).Next(214758, 999999);

            request.SegmentKey = itinerary.SegmentKey;
            request.Segment = new WSSegment[itinerary.Segments.Length];
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                request.Segment[i] = new WSSegment();
                request.Segment[i].Airline = new WSAirline();
                request.Segment[i].Airline.AirlineCode = itinerary.Segments[i].Airline;
                request.Segment[i].AirlinePNR = itinerary.Segments[i].AirlinePNR;
                request.Segment[i].ArrTime = itinerary.Segments[i].ArrivalTime;
                request.Segment[i].Craft = itinerary.Segments[i].Craft;
                request.Segment[i].DepTIme = itinerary.Segments[i].DepartureTime;

                request.Segment[i].Destination = new WSAirport();
                request.Segment[i].Destination.AirportCode = itinerary.Segments[i].Destination.AirportCode;
                request.Segment[i].Destination.AirportName = itinerary.Segments[i].Destination.AirportName;
                request.Segment[i].Destination.CityCode = itinerary.Segments[i].Destination.CityCode;
                request.Segment[i].Destination.CityName = itinerary.Segments[i].Destination.CityName;
                request.Segment[i].Destination.CountryCode = itinerary.Segments[i].Destination.CountryCode;
                request.Segment[i].Destination.CountryName = itinerary.Segments[i].Destination.CountryName;
                request.Segment[i].Destination.Terminal = itinerary.Segments[i].DepTerminal;

                request.Segment[i].Duration = itinerary.Segments[i].Duration.ToString();
                request.Segment[i].ETicketEligible = itinerary.Segments[i].ETicketEligible;
                request.Segment[i].FareClass = itinerary.Segments[i].FareInfoKey;
                request.Segment[i].FlightNumber = itinerary.Segments[i].FlightNumber;
                request.Segment[i].OperatingCarrier = itinerary.Segments[i].OperatingCarrier;

                request.Segment[i].Origin = new WSAirport();
                request.Segment[i].Origin.AirportCode = itinerary.Segments[i].Origin.AirportCode;
                request.Segment[i].Origin.AirportName = itinerary.Segments[i].Origin.AirportName;
                request.Segment[i].Origin.CityCode = itinerary.Segments[i].Origin.CityCode;
                request.Segment[i].Origin.CityName = itinerary.Segments[i].Origin.CityName;
                request.Segment[i].Origin.CountryCode = itinerary.Segments[i].Origin.CountryCode;
                request.Segment[i].Origin.CountryName = itinerary.Segments[i].Origin.CountryName;
                request.Segment[i].Origin.Terminal = itinerary.Segments[i].ArrTerminal;

                request.Segment[i].SegmentIndicator = itinerary.Segments[i].Group;
                request.Segment[i].Status = itinerary.Segments[i].Status;
                request.Segment[i].Stop = itinerary.Segments[i].Stops;
            }
        }

        /// <summary>
        /// This method is used to release booked seat (Hold Bookings) for Non LCC flights. 
        /// CancelItinerary will work only for those booking those are not ticketed. 
        /// This method will not work for LCC flights because they can't be booked they only ticketed.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public WSCancelItineraryResponse CancelItineraryForTBO(WSCancelItineraryRequest request)
        {
            WSCancelItineraryResponse response = new WSCancelItineraryResponse();

            try
            {
                response = ba.CancelItinerary(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, 0, "(TBOAir)Failed to Cancel Itinerary. Reason : " + ex.ToString(), "");
            }

            return response;
        }

        /// <summary>
        /// This method can be called explicitly to generate ticket any airline 
        /// and will also be used to ticket the already booked itinerary. 
        /// So, there will be two overloads of this method first will be used 
        /// for the itineraries which are already booked and other will first 
        /// book and also make ticket for the requested itinerary.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public TicketingResponse TicketForTBO(ref FlightItinerary itinerary, out Ticket[] tickets)
        {
            WSTicketResponse wsResponse = new WSTicketResponse();
            TicketingResponse response = new TicketingResponse();
            tickets = new Ticket[0];
            try
            {
                WSTicketRequest request = new WSTicketRequest();

                FillTicketRequest(ref request, itinerary);

                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSTicketRequest));
                        StreamWriter sw = new StreamWriter(XmlPath + "TBOAirTicketRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, request);
                        sw.Close();
                    }
                }
                catch { }

                wsResponse = ba.Ticket(request);

                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSTicketResponse));
                        StreamWriter sw = new StreamWriter(XmlPath + "TBOAirTicketResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, response);
                        sw.Close();
                    }
                }
                catch { }

                if (wsResponse.Status != null && wsResponse.Status.Description.ToLower().Contains("succes"))
                {
                    Audit.Add(EventType.TBOAir, Severity.Low, 0, "(TBO Air)Ticket Response successful : " + wsResponse.PNR, HttpContext.Current.Request["REMOTE_ADDR"]);
                    //StatusCode 09 for Non LCC and 14 for LCC
                    response.Message = wsResponse.Status.StatusCode + " - " + wsResponse.Status.Description;
                    response.PNR = wsResponse.PNR;
                    response.Status = TicketingResponseStatus.Successful;

                    Audit.Add(EventType.TBOAir, Severity.Low, 0, "(TBO Air)Adding Ticket Detail : " + wsResponse.PNR, HttpContext.Current.Request["REMOTE_ADDR"]);

                    AddBookingDetail(wsResponse.RefId, TBOAir.TekTravelAir.APIBookingStatus.Ticketed);

                    //Retrieve Tickets
                    WSGetBookingRequest bookReq = new WSGetBookingRequest();
                    bookReq.BookingId = wsResponse.BookingId;
                    bookReq.Pnr = wsResponse.PNR;
                    bookReq.Source = (TBOAir.TekTravelAir.BookingSource)Enum.Parse(typeof(TBOBookingSource), itinerary.SupplierBookingSource.ToString());

                    Audit.Add(EventType.TBOAir, Severity.Low, 0, "(TBO Air)Retrieving Ticket details : " + wsResponse.PNR, HttpContext.Current.Request["REMOTE_ADDR"]);

                    WSGetBookingResponse bookRes = GetBookingForTBO(bookReq);

                    Audit.Add(EventType.TBOAir, Severity.Low, 0, "(TBO Air)Start preparing Tickets : " + wsResponse.PNR, HttpContext.Current.Request["REMOTE_ADDR"]);
                    //Prepare tickets
                    tickets = PrepareTickets(bookRes);
                    Audit.Add(EventType.TBOAir, Severity.Low, 0, "(TBO Air)Prepared Tickets success : " + wsResponse.PNR, HttpContext.Current.Request["REMOTE_ADDR"]);
                }
                else
                {
                    Audit.Add(EventType.TBOAir, Severity.Low, 0, "(TBO Air)Ticket Response Failed : " + wsResponse.PNR, HttpContext.Current.Request["REMOTE_ADDR"]);
                    response.Message = wsResponse.Status.StatusCode + " - " + wsResponse.Status.Description;
                    response.PNR = wsResponse.PNR;
                    response.Status = TicketingResponseStatus.OtherError;

                    AddBookingDetail(wsResponse.RefId, TBOAir.TekTravelAir.APIBookingStatus.Failed);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, 0, "(TBOAir)Failed to issue Ticket. Reason : " + ex.ToString(), "");
            }

            return response;
        }

        void FillTicketRequest(ref WSTicketRequest request, FlightItinerary itinerary)
        {
            Audit.Add(EventType.TBOAir, Severity.Low, 0, "(TBO Air)Start preparing Ticket Request", HttpContext.Current.Request["REMOTE_ADDR"]);
            request.Destination = itinerary.Destination;
            request.Fare = new WSFare();
            request.FareType = "PUB";
            request.InstantTicket = true;
            request.Origin = itinerary.Origin;
            request.PromotionalPlanType = (PromotionalPlanType)Enum.Parse(typeof(PromotionalPlanType), itinerary.PromotionalPlanType);
            request.CorporateCode = "1";
            request.Endorsement = "1";
            request.TourCode = "1";
            request.Remarks = "1";
            request.BookingID = itinerary.AirLocatorCode;
            request.IsOneWayBooking = (itinerary.IsDomestic ? true : false); //If Domestic booking true otherwise false.            
            request.SessionId = itinerary.TicketAdvisory;
            request.Source = (TBOAir.TekTravelAir.BookingSource)Enum.Parse(typeof(TBOBookingSource), itinerary.UniversalRecord.ToString());
            request.Remarks = "";

            decimal bookingAmount = 0;

            int cntr = 0, paxCount = 0;
            foreach (FlightPassenger pax in itinerary.Passenger)
            {
                if (pax.Type == CT.BookingEngine.PassengerType.Adult || pax.Type == CT.BookingEngine.PassengerType.Child)
                {
                    paxCount++;
                }
            }

            request.Passenger = new WSPassenger[itinerary.Passenger.Length];
            foreach (FlightPassenger pax in itinerary.Passenger)
            {
                bookingAmount += (pax.TBOPrice.PublishedFare);
                //Setting Combined Fare for Itinerary
                request.Fare.AdditionalTxnFee = pax.TBOPrice.AdditionalTxnFee;
                request.Fare.AgentCommission = pax.TBOPrice.AgentCommission;
                request.Fare.AirTransFee = pax.TBOPrice.AirlineTransFee;
                request.Fare.BaseFare = (pax.TBOPrice.BaseFare);
                request.Fare.Currency = pax.TBOPrice.Currency;
                request.Fare.PublishedPrice = (pax.TBOPrice.PublishedFare);
                request.Fare.ReverseHandlingCharge += pax.TBOPrice.ReverseHandlingCharge;
                request.Fare.ServiceTax = pax.TBOPrice.SeviceTax;
                request.Fare.Tax = pax.TBOPrice.Tax;
                request.Fare.TdsOnCommission = pax.TBOPrice.TdsCommission;
                request.Fare.TdsOnPLB = pax.TBOPrice.TDSPLB;
                request.Fare.TransactionFee = pax.TBOPrice.TransactionFee;
                request.Fare.OtherCharges = pax.TBOPrice.OtherCharges;
                request.Fare.FuelSurcharge = pax.TBOPrice.FuelSurcharge;
                request.Fare.AgentConvienceCharges = pax.TBOPrice.AgentConvienceCharges;
                request.Fare.AgentServiceCharge = pax.TBOPrice.AgentServiceCharge;
                request.Fare.IncentiveEarned = pax.TBOPrice.IncentiveEarned;
                request.Fare.TdsOnIncentive = pax.TBOPrice.TDSIncentive;
                request.Fare.ChargeBU = new TBOAir.TekTravelAir.ChargeBreakUp[pax.TBOPrice.ChargeBU.Count];
                for (int i = 0; i < pax.TBOPrice.ChargeBU.Count; i++)
                {
                    request.Fare.ChargeBU[i] = new TBOAir.TekTravelAir.ChargeBreakUp();
                    request.Fare.ChargeBU[i].Amount = pax.TBOPrice.ChargeBU[i].Amount;
                    request.Fare.ChargeBU[i].ChargeType = (TBOAir.TekTravelAir.ChargeType)Enum.Parse(typeof(CT.BookingEngine.ChargeType), pax.TBOPrice.ChargeBU[i].ChargeType.ToString());
                    request.Fare.ChargeBU[i].PriceId = pax.TBOPrice.ChargeBU[i].PriceId;
                }

                //Setting Pax
                request.Passenger[cntr] = new WSPassenger();
                request.Passenger[cntr].AddressLine1 = itinerary.Passenger[cntr].AddressLine1;
                request.Passenger[cntr].AddressLine2 = itinerary.Passenger[cntr].AddressLine2;
                request.Passenger[cntr].Country = itinerary.Passenger[cntr].Country.CountryCode;
                request.Passenger[cntr].DateOfBirth = itinerary.Passenger[cntr].DateOfBirth;
                request.Passenger[cntr].Email = itinerary.Passenger[cntr].Email;
                request.Passenger[cntr].FFAirline = itinerary.Passenger[cntr].FFAirline;
                request.Passenger[cntr].FFNumber = itinerary.Passenger[cntr].FFNumber;
                request.Passenger[cntr].FirstName = itinerary.Passenger[cntr].FirstName;
                request.Passenger[cntr].Gender = (TBOAir.TekTravelAir.Gender)Enum.Parse(typeof(CT.BookingEngine.Gender), itinerary.Passenger[cntr].Gender.ToString());
                request.Passenger[cntr].LastName = itinerary.Passenger[cntr].LastName;
                request.Passenger[cntr].PassportExpiry = itinerary.Passenger[cntr].PassportExpiry;
                request.Passenger[cntr].PassportNumber = itinerary.Passenger[cntr].PassportNo;
                request.Passenger[cntr].Phone = itinerary.Passenger[cntr].CellPhone;
                request.Passenger[cntr].Title = itinerary.Passenger[cntr].Title;
                request.Passenger[cntr].Meal = new TBOAir.TekTravelAir.Meal();
                request.Passenger[cntr].Seat = new TBOAir.TekTravelAir.Seat();
                request.Passenger[cntr].Ssr = new WSSSR();
                request.Passenger[cntr].PinCode = "";

                foreach (Fare fbd in itinerary.TBOFareBreakdown)
                {
                    if (itinerary.Passenger[cntr].Type == fbd.PassengerType)
                    {
                        //Individual Pax Fare
                        request.Passenger[cntr].Fare = new WSFare();
                        request.Passenger[cntr].Fare.AdditionalTxnFee = fbd.AdditionalTxnFee / paxCount;

                        request.Passenger[cntr].Fare.BaseFare = (decimal)fbd.BaseFare / fbd.PassengerCount;

                        //request.Passenger[cntr].Fare.PublishedPrice = (decimal)(fbd.TotalFare);

                        request.Passenger[cntr].Fare.Tax = fbd.Tax / fbd.PassengerCount;

                        if (pax.Type != CT.BookingEngine.PassengerType.Infant)
                        {
                            request.Passenger[cntr].Fare.FuelSurcharge = (itinerary.Passenger[cntr].TBOPrice.FuelSurcharge / fbd.PassengerCount);
                        }
                        break;
                    }
                }
                request.Passenger[cntr].Fare.ChargeBU = new TBOAir.TekTravelAir.ChargeBreakUp[0];

                cntr++;
            }

            request.FareRule = new WSFareRule[itinerary.FareRules.Count];

            for (int i = 0; i < itinerary.FareRules.Count; i++)
            {
                request.FareRule[i] = new WSFareRule();
                request.FareRule[i].Airline = itinerary.FareRules[i].Airline;
                request.FareRule[i].DepartureDate = itinerary.FareRules[i].DepartureTime;
                request.FareRule[i].Destination = itinerary.FareRules[i].Destination;
                request.FareRule[i].FareBasisCode = itinerary.FareRules[i].FareBasisCode;
                request.FareRule[i].FareRestriction = itinerary.FareRules[i].FareRestriction;
                request.FareRule[i].FareRuleDetail = itinerary.FareRules[i].FareRuleDetail;
                request.FareRule[i].Origin = itinerary.FareRules[i].Origin;
                request.FareRule[i].ReturnDate = itinerary.FareRules[i].ReturnDate;
                request.FareRule[i].Source = (TBOAir.TekTravelAir.BookingSource)Enum.Parse(typeof(TBOBookingSource), itinerary.FareRules[i].SupplierBookingSource.ToString());
            }


            request.PaymentInformation = new WSPaymentInformation();
            request.PaymentInformation.Amount = bookingAmount;
            request.PaymentInformation.InvoiceNumber = 0;
            request.PaymentInformation.IPAddress = HttpContext.Current.Request["REMOTE_ADDR"];
            request.PaymentInformation.PaymentGateway = TBOAir.TekTravelAir.PaymentGatewaySource.APICustomer;
            request.PaymentInformation.PaymentId = "0";
            request.PaymentInformation.PaymentInformationId = 0;
            request.PaymentInformation.PaymentModeType = PaymentModeType.Deposited;
            request.PaymentInformation.TrackId = 0;

            request.Segment = new WSSegment[itinerary.Segments.Length];
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                request.Segment[i] = new WSSegment();
                request.Segment[i].Airline = new WSAirline();
                request.Segment[i].Airline.AirlineCode = itinerary.Segments[i].Airline;
                request.Segment[i].AirlinePNR = itinerary.Segments[i].AirlinePNR;
                request.Segment[i].ArrTime = itinerary.Segments[i].ArrivalTime;
                request.Segment[i].Craft = itinerary.Segments[i].Craft;
                request.Segment[i].DepTIme = itinerary.Segments[i].DepartureTime;

                request.Segment[i].Destination = new WSAirport();
                request.Segment[i].Destination.AirportCode = itinerary.Segments[i].Destination.AirportCode;
                request.Segment[i].Destination.AirportName = itinerary.Segments[i].Destination.AirportName;
                request.Segment[i].Destination.CityCode = itinerary.Segments[i].Destination.CityCode;
                request.Segment[i].Destination.CityName = itinerary.Segments[i].Destination.CityName;
                request.Segment[i].Destination.CountryCode = itinerary.Segments[i].Destination.CountryCode;
                request.Segment[i].Destination.CountryName = itinerary.Segments[i].Destination.CountryName;
                request.Segment[i].Destination.Terminal = itinerary.Segments[i].DepTerminal;

                request.Segment[i].Duration = itinerary.Segments[i].Duration.ToString();
                request.Segment[i].ETicketEligible = itinerary.Segments[i].ETicketEligible;
                request.Segment[i].FareClass = itinerary.Segments[i].FareInfoKey;
                request.Segment[i].FlightNumber = itinerary.Segments[i].FlightNumber;
                request.Segment[i].OperatingCarrier = itinerary.Segments[i].OperatingCarrier;

                request.Segment[i].Origin = new WSAirport();
                request.Segment[i].Origin.AirportCode = itinerary.Segments[i].Origin.AirportCode;
                request.Segment[i].Origin.AirportName = itinerary.Segments[i].Origin.AirportName;
                request.Segment[i].Origin.CityCode = itinerary.Segments[i].Origin.CityCode;
                request.Segment[i].Origin.CityName = itinerary.Segments[i].Origin.CityName;
                request.Segment[i].Origin.CountryCode = itinerary.Segments[i].Origin.CountryCode;
                request.Segment[i].Origin.CountryName = itinerary.Segments[i].Origin.CountryName;
                request.Segment[i].Origin.Terminal = itinerary.Segments[i].ArrTerminal;

                request.Segment[i].SegmentIndicator = itinerary.Segments[i].Group;
                request.Segment[i].Status = itinerary.Segments[i].Status;
                request.Segment[i].Stop = itinerary.Segments[i].Stops;
            }
        }

        void AddBookingDetail(int refId, TBOAir.TekTravelAir.APIBookingStatus status)
        {
            WSAddBookingRequest request = new WSAddBookingRequest();
            WSAddBookingResponse response = new WSAddBookingResponse();

            request.BookingStatus = status;
            request.RefId = refId;

            try
            {
                if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSAddBookingRequest));
                    StreamWriter sw = new StreamWriter(XmlPath + "TBOAirAddBookingDetailRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                    ser.Serialize(sw, request);
                    sw.Close();
                }
            }
            catch { }

            response = ba.AddBookingDetail(request);

            try
            {
                if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSAddBookingResponse));
                    StreamWriter sw = new StreamWriter(XmlPath + "TBOAirAddBookingDetailResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                    ser.Serialize(sw, response);
                    sw.Close();
                }
            }
            catch { }

        }

        /// <summary>
        /// This method is used to retrieve detailed information about an existing booking.         
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public WSGetBookingResponse GetBookingForTBO(WSGetBookingRequest request)
        {
            WSGetBookingResponse response = new WSGetBookingResponse();

            try
            {

                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSGetBookingRequest));
                        StreamWriter sw = new StreamWriter(XmlPath + "TBOAirGetBookingRequest_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, request);
                        sw.Close();
                    }
                }
                catch { }

                response = ba.GetBooking(request);


                try
                {
                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(WSGetBookingResponse));
                        StreamWriter sw = new StreamWriter(XmlPath + "TBOAirGetBookingResponse_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, response);
                        sw.Close();
                    }
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, 0, "(TBOAir)Failed to Get Booking. Reason : " + ex.ToString(), "");
            }

            return response;
        }

        /// <summary>
        /// Prepares Tickets for the Booking.
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private Ticket[] PrepareTickets(WSGetBookingResponse response)
        {
            List<Ticket> tickets = new List<Ticket>();

            foreach (WSTicket ticket in response.Ticket)
            {
                Ticket tkt = new Ticket();

                tkt.CorporateCode = ticket.CorporateCode;
                tkt.Endorsement = ticket.Endorsement;
                tkt.ETicket = true;
                tkt.IssueDate = ticket.IssueDate;
                tkt.PaxFirstName = ticket.FirstName;
                tkt.PaxLastName = ticket.LastName;
                tkt.PaxType = (CT.BookingEngine.PassengerType)Enum.Parse(typeof(TBOAir.TekTravelAir.PassengerType), ticket.PaxType.ToString());
                tkt.Price = new PriceAccounts();
                tkt.Price.AccPriceType = PriceType.PublishedFare;
                tkt.Price.AdditionalTxnFee = ticket.Fare.AdditionalTxnFee * rateOfExchange;
                tkt.Price.AgentCommission = ticket.Fare.AgentCommission * rateOfExchange;
                tkt.Price.AirlineTransFee = ticket.Fare.AirTransFee * rateOfExchange;
                tkt.Price.Currency = agentBaseCurrency;
                tkt.Price.CurrencyCode = agentBaseCurrency;
                tkt.Price.DecimalPoint = decimalValue;
                tkt.Price.NetFare = ticket.Fare.OfferedFare * rateOfExchange;
                tkt.Price.OtherCharges = ticket.Fare.OtherCharges * rateOfExchange;
                tkt.Price.OurPLB = ticket.Fare.PLBEarned * rateOfExchange;
                tkt.Price.PublishedFare = ticket.Fare.PublishedPrice * rateOfExchange;
                tkt.Price.RateOfExchange = rateOfExchange;
                tkt.Price.ReverseHandlingCharge = ticket.Fare.ReverseHandlingCharge * rateOfExchange;
                tkt.Price.SeviceTax = ticket.Fare.ServiceTax * rateOfExchange;
                tkt.Price.SupplierCurrency = "INR";
                tkt.Price.SupplierPrice = ticket.Fare.PublishedPrice;
                tkt.Price.Tax = ticket.Fare.Tax * rateOfExchange;
                tkt.Price.TdsCommission = ticket.Fare.TdsOnCommission * rateOfExchange;
                tkt.Price.TDSPLB = ticket.Fare.TdsOnPLB * rateOfExchange;
                tkt.Price.TransactionFee = ticket.Fare.TransactionFee * rateOfExchange;
                tkt.Price.WLCharge = 0;

                tkt.PtcDetail = new List<SegmentPTCDetail>();
                foreach (WSSegAdditionalInfo ptc in ticket.SegmentAdditionalInfo)
                {
                    SegmentPTCDetail baggage = new SegmentPTCDetail();
                    baggage.Baggage = ptc.Baggage;
                    baggage.FareBasis = ptc.FareBasis;
                    baggage.FlightKey = ptc.FlightKey;
                    baggage.NVA = ptc.NVA;
                    baggage.NVB = ptc.NVB;
                    switch (ticket.PaxType)
                    {
                        case TBOAir.TekTravelAir.PassengerType.Adult:
                            baggage.PaxType = "ADT";
                            break;
                        case TBOAir.TekTravelAir.PassengerType.Child:
                            baggage.PaxType = "CNN";
                            break;
                        case TBOAir.TekTravelAir.PassengerType.Infant:
                            baggage.PaxType = "INF";
                            break;
                    }

                    tkt.PtcDetail.Add(baggage);
                }
                for (int k = 0; k < response.Segment.Length; k++)
                {
                    string flightKey = string.Empty;
                    flightKey = response.Segment[k].Airline.AirlineCode;
                    flightKey += response.Segment[k].FlightNumber.PadLeft(4, '0');
                    flightKey += response.Segment[k].DepTIme.ToString("ddMMMyyyyHHmm").ToUpper();

                    tkt.PtcDetail[k].FlightKey = flightKey;
                }
                tkt.Remarks = ticket.Remarks;
                tkt.TicketNumber = ticket.TicketNumber;
                tkt.Title = ticket.Title;
                tkt.TourCode = ticket.TourCode;
                tkt.ValidatingAriline = ticket.ValidatingAirline;

                tickets.Add(tkt);
            }

            return tickets.ToArray();
        }

        /// <summary>
        /// This method is used to either Cancel or Modify the booking. Cancellation is done either entire booking or ticket wise.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public WSSendChangeRequestResponse[] SendChangeRequestForTBO(WSSendChangeRequest request)
        {
            WSSendChangeRequestResponse[] response = new WSSendChangeRequestResponse[0];

            try
            {
                response = ba.SendChangeRequest(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOAir, Severity.High, 0, "(TBOAir)Failed to Send Change Request. Reason : " + ex.ToString(), "");
            }

            return response;
        } 
        #endregion
    }

    
}
