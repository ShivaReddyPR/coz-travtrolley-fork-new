﻿using System;
using System.Runtime.Serialization;
using CT.BookingEngine;

namespace TBOAir
{
    [DataContract]
    class TBOSearchRequest
    {
        [DataMember]
        public string EndUserIp;
        [DataMember]
        public string TokenId;
        [DataMember]
        public int AdultCount;
        [DataMember]
        public int ChildCount;
        [DataMember]
        public int InfantCount;
        [DataMember]
        public bool DirectFlight;
        [DataMember]
        public bool OneStopFlight;
        [DataMember]
        public int JourneyType;
        [DataMember]
        public string[] PreferredAirlines;
        [DataMember]
        public Segment[] Segments;
        [DataMember]
        public string[] Sources;
    }

    public class Segment
    {
        //TODO: change this to use citycodes
        string origin;
        string destination;
        string preferredDepartureTime;
        string preferredArrivalTime;
        CabinClass cabinClass;
       

        [DataMember]
        public string Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
            }
        }

        [DataMember]
        public string Destination
        {
            get
            {
                return destination;
            }
            set
            {
                destination = value;
            }
        }

        [DataMember]
        public string PreferredDepartureTime
        {
            get
            {
                return preferredDepartureTime;
            }
            set
            {
                preferredDepartureTime = value;
            }
        }

        [DataMember]
        public string PreferredArrivalTime
        {
            get
            {
                return preferredArrivalTime;
            }
            set
            {
                preferredArrivalTime = value;
            }
        }

        [DataMember]
        public CabinClass FlightCabinClass
        {
            get
            {
                return cabinClass;
            }
            set
            {
                cabinClass = value;
            }
        }


    }
}
