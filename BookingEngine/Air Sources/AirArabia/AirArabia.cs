using System;
using System.Collections.Generic;
using System.Text;
using CT.Configuration;
using System.Xml;
using System.IO;
using System.Net;
using System.Data;
using CT.Core;
using System.Threading;
using System.Linq;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace CT.BookingEngine.GDS
{
    public class AirArabia:IDisposable
    {
        public class SessionData
        {
            public CookieContainer cookie;
            public string transactionID;
            public Dictionary<string, string> fare;//fare for every flight
            public string SelectedResultInfo;
            public Dictionary<string, string> totalPrice;
            public string SelectedTotalPrice;
            public Dictionary<string, string> SelectedResultXml; //To Store the Selected result fareXml
        }
        public string[][] RPH;
        string airlineCode = "G9";
        string userName = string.Empty;
        string password = string.Empty;
        string code = string.Empty;
        string url = string.Empty;
        string nearByOrigin = string.Empty;
        string nearByDestination = string.Empty;
        string fareXml;
        DataTable dtBaggageDetails = new DataTable();
        DataTable dtMealDetails = new DataTable(); //Added for Meals
        Dictionary<string, DataTable> flightBaggages;
        private string agentCurrency;
        private Dictionary<string, decimal> exchangeRates;
        private int decimalPoint;
        private decimal roe = 1;
        private string appUserId;
        private string sessionId;
        private string xmlPath;
        private string sSeatAvailResponseXSLT;
        private string version = string.Empty;
        XmlNamespaceManager nsmgr = null;
        #region Added for CombineSearch
        List<SearchRequest> searchRequests = new List<SearchRequest>();
        private bool searchBySegments;

        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        List<SearchResult> liSearchResult = new List<SearchResult>();
        
        private string searchAvaibilityResponse = string.Empty;

        List<string> lstBundlesOn = new List<string>();
        List<string> lstBundlesRet = new List<string>();
        List<SearchResult> lstBasicResults = new List<SearchResult>();
       
        List<SearchResult> lstCombinationResults = new List<SearchResult>();
        SessionData sessionDta = null;
        SearchRequest request = null;
        private string basicRPHValues = string.Empty;
        XmlDocument xmlDoc = null;

        #endregion

        public string AgentCurrency
        {
            get { return agentCurrency; }
            set { agentCurrency = value; }
        }

        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public int DecimalPoint
        {
            get { return decimalPoint; }
            set { decimalPoint = value; }
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        /// <summary>
        /// XML_HAP value.
        /// </summary>
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        public string AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }

        public string SessionID
        {
            get { return sessionId; }
            set { sessionId = value; }
        }

        /// <summary>
        /// for sector list added by bangar
        /// </summary>
        public List<SectorList> Sectors { get; set; }


        public AirArabia()
        {
            //ServicePointManager.Expect100Continue = false;// to sort out  new g9 error "The remote server returned an error: (417) Expectation Failed."
            //userName = ConfigurationSystem.AirArabiaConfig["UserName"];
            //password = ConfigurationSystem.AirArabiaConfig["Password"];
            //code = ConfigurationSystem.AirArabiaConfig["Code"];
            url = ConfigurationSystem.AirArabiaConfig["Url"];
            dtBaggageDetails.Columns.Add("baggageCode", typeof(string));
            dtBaggageDetails.Columns.Add("baggageDescription", typeof(string));
            dtBaggageDetails.Columns.Add("baggageCharge", typeof(decimal));
            dtBaggageDetails.Columns.Add("currencyCode", typeof(string));
            dtBaggageDetails.Columns.Add("SegmentId", typeof(int));

            dtMealDetails.Columns.Add("mealCode", typeof(string));
            dtMealDetails.Columns.Add("mealDescription", typeof(string));
            dtMealDetails.Columns.Add("mealCharge", typeof(decimal));
            dtMealDetails.Columns.Add("currencyCode", typeof(string));
            dtMealDetails.Columns.Add("SegmentId", typeof(int));
            flightBaggages = new Dictionary<string, DataTable>();
            roe = 1;
            xmlPath = ConfigurationSystem.AirArabiaConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMMyyyy") + "\\";
            sSeatAvailResponseXSLT = ConfigurationSystem.XslconfigPath + "\\AirArabiaSeatMap.xslt";

            try
            {
                if (!System.IO.Directory.Exists(xmlPath))
                {
                    System.IO.Directory.CreateDirectory(xmlPath);
                }
            }
            catch { }
        }

        public static string BuildResultKey(SearchResult result)
        {
            int obLength = result.Flights[0].Length;
            StringBuilder key = new StringBuilder();
            key.Append(result.Flights[0][0].DepartureTime.DayOfYear.ToString());
            key.Append(result.Flights[0][0].DepartureTime.ToString("HHmm"));
            key.Append(result.Flights[0][obLength - 1].ArrivalTime.DayOfYear.ToString());
            key.Append(result.Flights[0][obLength - 1].ArrivalTime.ToString("HHmm"));
            if (result.Flights.Length > 1)
            {
                int ibLength = result.Flights[1].Length;
                key.Append(result.Flights[1][0].DepartureTime.DayOfYear.ToString());
                key.Append(result.Flights[1][0].DepartureTime.ToString("HHmm"));
                key.Append(result.Flights[1][ibLength - 1].ArrivalTime.DayOfYear.ToString());
                key.Append(result.Flights[1][ibLength - 1].ArrivalTime.ToString("HHmm"));
            }
            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    key.Append(result.Flights[i][j].FlightNumber + result.Flights[i][j].SegmentFareType);
                    key.Append(result.Flights[i][j].BookingClass);
                    //if (result.Flights[i][j].ServiceBundleId > 0) //Adding ServiceId Faretype to form as unique key.
                    //{
                    //    key.Append(result.Flights[i][j].ServiceBundleId);
                    //}
                    ////if (result.FareType.Contains("Value") || result.FareType.Contains("Extra"))
                    //{
                    //    key.Append(result.Flights[i][j].SegmentFareType);
                    //}
                }
            }
            return key.ToString();
        }
        public void GetFareQuotes(SearchResult result, string sessionId)
        {
            //Trace.TraceInformation("AirArabia.GetFareQuotes entered");
            SessionData sData = (SessionData)Basket.BookingSession[sessionId][airlineCode + "-" + result.ResultKey];//getting fares of selected flight
            if (sData.fare.ContainsKey(BuildResultKey(result)))
            {
                sData.SelectedResultInfo = sData.fare[BuildResultKey(result)];//inserting fare details of selected flight in session for Booking
                sData.SelectedTotalPrice = sData.totalPrice[BuildResultKey(result)];//getting selected total fare for inserting into xml while booking
            }
        }

        public void SearchRoutingReturn(object eventNumber)
        {
            try
            {
                SearchRequest request = searchRequests[Convert.ToInt32(eventNumber)];
                liSearchResult.AddRange(GetSearchResults(request, sessionId));
                eventFlag[(int)eventNumber].Set();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "Failed to get G9 Search response. Error: " + ex.ToString(), "");
                eventFlag[(int)eventNumber].Set();
            }
        }

        private void BuildBundleResults(int resIndex, int onBundleIndex, int retBundleIndex)
        {
            try
            {
                SearchResult basicResult = lstBasicResults[resIndex];//Retrieve Result
                string resultRPHvalues = string.Empty;
                string[] rph = null;
                string keyfare = string.Empty;
                sessionDta = (SessionData)Basket.BookingSession[sessionId][basicResult.AliasAirlineCode + "-" + basicResult.Flights[0].Select(x => x.Origin.AirportCode).FirstOrDefault() + "-" +
                        basicResult.Flights[0].Select(x => x.Destination.AirportCode).Last()]; //To Retrive the Transaction Id from session to pass in the price requests.
                if (basicResult.TotalFare == 0)
                {                    
                    GetFare(ref basicResult, request, ref sessionDta, ref rph, ref fareXml, sessionId, ref sessionDta.cookie, true);

                    string farexml = "<Root>" + fareXml + "</Root>";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(farexml);

                    PrepareBundleServices(xmldoc);

                    keyfare = BuildResultKey(basicResult);
                    if (!sessionDta.fare.Keys.Contains(keyfare))
                        sessionDta.fare.Add(keyfare, fareXml);
                    else
                        sessionDta.fare[keyfare] = fareXml;
                    string resultKey = basicResult.Flights[0].Select(x => x.Origin.AirportCode).FirstOrDefault() + "-" +
                        basicResult.Flights[0].Select(x => x.Destination.AirportCode).Last();
                    basicResult.ResultKey = resultKey;
                    if (!Basket.BookingSession[sessionId].ContainsKey(airlineCode + "-" + resultKey))
                        Basket.BookingSession[sessionId].Add(airlineCode + "-" + resultKey, (object)sessionDta);
                    else
                        Basket.BookingSession[sessionId][airlineCode + "-" + resultKey] = (object)sessionDta;
                }

                int segmentIndex = 1;
                if (searchBySegments)
                    segmentIndex = 0;

                SearchResult searchResult = new SearchResult();

                searchResult.IsLCC = true;
                searchResult.TicketAdvisory = basicResult.TicketAdvisory;
                searchResult.Currency = agentCurrency;
                searchResult.ResultBookingSource = BookingSource.AirArabia;
                searchResult.FareRules = basicResult.FareRules;

                List<Fare> farebreakdowns = new List<Fare>();

                if (request.AdultCount > 0)
                {
                    Fare fare = new Fare();
                    fare.PassengerCount = request.AdultCount;
                    fare.PassengerType = PassengerType.Adult;
                    farebreakdowns.Add(fare);
                }
                if (request.ChildCount > 0)
                {
                    Fare fare = new Fare();
                    fare.PassengerCount = request.ChildCount;
                    fare.PassengerType = PassengerType.Child;
                    farebreakdowns.Add(fare);
                }
                if (request.InfantCount > 0)
                {
                    Fare fare = new Fare();
                    fare.PassengerCount = request.InfantCount;
                    fare.PassengerType = PassengerType.Infant;
                    farebreakdowns.Add(fare);
                }
                searchResult.FareBreakdown = farebreakdowns.ToArray();
                searchResult.Price = new PriceAccounts();//basicResults[0].Price;
                searchResult.Price.SupplierCurrency = basicResult.Price.SupplierCurrency;

                searchResult.Flights = (request.Type == SearchType.Return && !searchBySegments) ? new FlightInfo[2][] : new FlightInfo[1][];

                searchResult.Flights[0] = new FlightInfo[basicResult.Flights[0].Length];
               
                for (int k = 0; k < basicResult.Flights[0].Length; k++)
                {
                    searchResult.Flights[0][k] = new FlightInfo();
                    searchResult.Flights[0][k] = FlightInfo.Copy(basicResult.Flights[0][k]);
                }
                if (request.Type == SearchType.Return)
                {
                    searchResult.Flights[segmentIndex] = new FlightInfo[basicResult.Flights[segmentIndex].Length];
                    for (int k = 0; k < basicResult.Flights[segmentIndex].Length; k++)
                    {
                        searchResult.Flights[segmentIndex][k] = new FlightInfo();
                        searchResult.Flights[segmentIndex][k] = FlightInfo.Copy(basicResult.Flights[segmentIndex][k]);
                    }
                }
                //For Bundle Results we need to assign the bundle Detailes
                if (request.Type == SearchType.Return) 
                {
                    //DO THE BASIC WITH BUNDLE COMBINATION RESULTS
                    if (onBundleIndex >= 0 && retBundleIndex >= 0)
                    {
                        //Combine Two Bundle Fares
                        string[] bundleData = lstBundlesOn[onBundleIndex].Split('#');
                        searchResult.Flights[0].ToList().ForEach(x => x.ServiceBundleId = Convert.ToInt32(bundleData[0]));
                        searchResult.Flights[0].ToList().ForEach(x => x.SegmentFareType = bundleData[1]);
                        searchResult.Flights[0].ToList().ForEach(x => x.BookingClass = bundleData[2]);
                        searchResult.Flights[0].ToList().ForEach(x => x.UapiSegmentRefKey = bundleData[3]);
                        searchResult.Flights[0].ToList().ForEach(x => x.DefaultBaggage = bundleData[4]);

                        bundleData = lstBundlesRet[retBundleIndex].Split('#');
                        searchResult.Flights[segmentIndex].ToList().ForEach(x => x.ServiceBundleId = Convert.ToInt32(bundleData[0]));
                        searchResult.Flights[segmentIndex].ToList().ForEach(x => x.SegmentFareType = bundleData[1]);
                        searchResult.Flights[segmentIndex].ToList().ForEach(x => x.BookingClass = bundleData[2]);
                        searchResult.Flights[segmentIndex].ToList().ForEach(x => x.UapiSegmentRefKey = bundleData[3]);
                        searchResult.Flights[segmentIndex].ToList().ForEach(x => x.DefaultBaggage = bundleData[4]);

                        searchResult.FareType = searchResult.Flights[0][0].SegmentFareType + "," + searchResult.Flights[segmentIndex][0].SegmentFareType;
                        searchResult.BaggageIncludedInFare = (string.IsNullOrEmpty(searchResult.Flights[0][0].DefaultBaggage) ? "0" : searchResult.Flights[0][0].DefaultBaggage) + "," + (string.IsNullOrEmpty(searchResult.Flights[segmentIndex][0].DefaultBaggage) ? "0" : searchResult.Flights[segmentIndex][0].DefaultBaggage);

                    }
                    else if (onBundleIndex >= 0) //COMBINATION OF BASIC-VALUE,VALUE-BASIC,BASIC-EXTRA ETC
                    {
                        //TAKING THE ONWARD BUNDLE SERVICE DETAILS BundleID, FareType, BookingClass,Description,DefaultBaggage in bun.
                        string[] bundleData = lstBundlesOn[onBundleIndex].Split('#');
                        searchResult.Flights[0].ToList().ForEach(x => x.ServiceBundleId = Convert.ToInt32(bundleData[0]));
                        searchResult.Flights[0].ToList().ForEach(x => x.SegmentFareType = bundleData[1]);
                        searchResult.Flights[0].ToList().ForEach(x => x.BookingClass = bundleData[2]);
                        searchResult.Flights[0].ToList().ForEach(x => x.UapiSegmentRefKey = bundleData[3]);
                        searchResult.Flights[0].ToList().ForEach(x => x.DefaultBaggage = bundleData[4]);
                        if (!searchBySegments)
                        {
                            searchResult.Flights[segmentIndex].ToList().ForEach(x => x.SegmentFareType = "Basic");
                            searchResult.BaggageIncludedInFare = (string.IsNullOrEmpty(searchResult.Flights[0][0].DefaultBaggage) ? "0" : searchResult.Flights[0][0].DefaultBaggage) + "," + (string.IsNullOrEmpty(searchResult.Flights[segmentIndex][0].DefaultBaggage) ? "0" : searchResult.Flights[segmentIndex][0].DefaultBaggage);
                            searchResult.FareType = searchResult.Flights[0][0].SegmentFareType + "," + "Basic";
                        }
                        else
                        {
                            searchResult.BaggageIncludedInFare = (string.IsNullOrEmpty(searchResult.Flights[0][0].DefaultBaggage) ? "0" : searchResult.Flights[0][0].DefaultBaggage);
                            searchResult.FareType = searchResult.Flights[0][0].SegmentFareType;
                        }

                    }
                    else if (retBundleIndex >= 0)
                    {

                        //TAKING THE RETURN BUNDLE SERVICE DETAILS BundleID, FareType, BookingClass,Description,DefaultBaggage in bun.
                        string[] bundleData = lstBundlesRet[retBundleIndex].Split('#');
                        searchResult.Flights[segmentIndex].ToList().ForEach(x => x.ServiceBundleId = Convert.ToInt32(bundleData[0]));
                        searchResult.Flights[segmentIndex].ToList().ForEach(x => x.SegmentFareType = bundleData[1]);
                        searchResult.Flights[segmentIndex].ToList().ForEach(x => x.BookingClass = bundleData[2]);
                        searchResult.Flights[segmentIndex].ToList().ForEach(x => x.UapiSegmentRefKey = bundleData[3]);
                        searchResult.Flights[segmentIndex].ToList().ForEach(x => x.DefaultBaggage = bundleData[4]);
                        if (!searchBySegments)
                        {
                            searchResult.Flights[0].ToList().ForEach(x => x.SegmentFareType = "Basic");
                            searchResult.FareType = "Basic" + "," + searchResult.Flights[segmentIndex][0].SegmentFareType;
                            searchResult.BaggageIncludedInFare = (string.IsNullOrEmpty(searchResult.Flights[0][0].DefaultBaggage) ? "0" : searchResult.Flights[0][0].DefaultBaggage) + "," + (string.IsNullOrEmpty(searchResult.Flights[segmentIndex][0].DefaultBaggage) ? "0" : searchResult.Flights[segmentIndex][0].DefaultBaggage);
                        }
                        else
                        {
                            searchResult.BaggageIncludedInFare = (string.IsNullOrEmpty(searchResult.Flights[0][0].DefaultBaggage) ? "0" : searchResult.Flights[0][0].DefaultBaggage);
                            searchResult.FareType= searchResult.Flights[0][0].SegmentFareType;
                        }
                    }
                    else
                    {
                        searchResult.Flights[0].ToList().ForEach(x => x.SegmentFareType = "Basic");
                        if (!searchBySegments && request.Type == SearchType.Return)
                        {
                            searchResult.Flights[1].ToList().ForEach(x => x.SegmentFareType = "Basic");
                            searchResult.FareType = "Basic,Basic";
                            searchResult.BaggageIncludedInFare = (string.IsNullOrEmpty(searchResult.Flights[0][0].DefaultBaggage) ? "0" : searchResult.Flights[0][0].DefaultBaggage) + "," + (string.IsNullOrEmpty(searchResult.Flights[1][0].DefaultBaggage) ? "0" : searchResult.Flights[1][0].DefaultBaggage);
                        }
                        else
                        {
                            searchResult.FareType = "Basic";
                            searchResult.BaggageIncludedInFare = (string.IsNullOrEmpty(searchResult.Flights[0][0].DefaultBaggage) ? "0" : searchResult.Flights[0][0].DefaultBaggage);
                        }
                    }
                }
                else  //FOR ONWARD BUNDLE RESULTS
                {                    
                    if (onBundleIndex >= 0) //COMBINATION OF BASIC-VALUE,VALUE-BASIC,BASIC-EXTRA ETC
                    {
                        //TAKING THE ONWARD BUNDLE SERVICE DETAILS BundleID, FareType, BookingClass,Description,DefaultBaggage in bun.
                        string[] bundleData = lstBundlesOn[onBundleIndex].Split('#');
                        searchResult.Flights[0].ToList().ForEach(x => x.ServiceBundleId = Convert.ToInt32(bundleData[0]));
                        searchResult.Flights[0].ToList().ForEach(x => x.SegmentFareType = bundleData[1]);
                        searchResult.Flights[0].ToList().ForEach(x => x.BookingClass = bundleData[2]);
                        searchResult.Flights[0].ToList().ForEach(x => x.UapiSegmentRefKey = bundleData[3]);
                        searchResult.Flights[0].ToList().ForEach(x => x.DefaultBaggage = bundleData[4]);
                        searchResult.FareType = searchResult.Flights[0][0].SegmentFareType;
                    }
                    else
                    {
                        searchResult.Flights[0].ToList().ForEach(x => x.SegmentFareType = "Basic");
                        searchResult.FareType = "Basic";
                    }
                    searchResult.BaggageIncludedInFare = (string.IsNullOrEmpty(searchResult.Flights[0][0].DefaultBaggage) ? "0" : searchResult.Flights[0][0].DefaultBaggage);
                }
                //Appending default Baggage 
                searchResult.ResultKey = request.Segments[0].Origin + "-" + request.Segments[0].Destination;
                keyfare = BuildResultKey(searchResult);

                if (!sessionDta.fare.ContainsKey(keyfare))                
                    GetFare(ref searchResult, request, ref sessionDta, ref rph, ref fareXml, sessionId, ref sessionDta.cookie, true);

                if (searchResult.TotalFare > 0)
                {
                    searchResult.ResultKey = request.Segments[0].Origin + "-" + request.Segments[0].Destination;
                    keyfare = BuildResultKey(searchResult);

                    if (!sessionDta.fare.ContainsKey(keyfare))
                    {
                        sessionDta.fare.Add(keyfare, fareXml);
                        lstCombinationResults.Add(searchResult);
                        string resultKey = searchResult.Flights[0].Select(x => x.Origin.AirportCode).FirstOrDefault() + "-" +
                        searchResult.Flights[0].Select(x => x.Destination.AirportCode).Last();
                        if (!Basket.BookingSession[sessionId].ContainsKey(airlineCode + "-" + resultKey))
                            Basket.BookingSession[sessionId].Add(airlineCode + "-" + resultKey, (object)sessionDta);
                        else
                            Basket.BookingSession[sessionId][airlineCode + "-" + resultKey] = (object)sessionDta;
                    }
                }                 
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "Failed to get G9 Search response. Error: " + ex.ToString(), "");                
            }
        }

        public SearchResult[] Search(SearchRequest searchRequest, string sessionId)
        {
            //bool allowSearch = false;

            ///* Checking if near by airport city is available using config for maping near by city */
            //if (ConfigurationSystem.SectorListConfig.ContainsKey("AirArabiaCityMapping"))
            //{
            //    Dictionary<string, string> sectors = ConfigurationSystem.SectorListConfig["AirArabiaCityMapping"];

            //    nearByDestination = searchRequest.Segments[0].Destination = sectors.ContainsKey(searchRequest.Segments[0].Destination) ? 
            //        sectors[searchRequest.Segments[0].Destination] : searchRequest.Segments[0].Destination;

            //    nearByOrigin = searchRequest.Segments[0].Origin = sectors.ContainsKey(searchRequest.Segments[0].Origin) ?
            //        sectors[searchRequest.Segments[0].Origin] : searchRequest.Segments[0].Origin;
            //}
            //else
            //    Core.Audit.Add(Core.EventType.AirArabiaSearch, Core.Severity.High, 1, "AirArabia: Error While reading sector from AirArabiaconfig : Error" + DateTime.Now, "");

            //if (ConfigurationSystem.SectorListConfig.ContainsKey("G9"))
            //{
            //    Dictionary<string, string> sectors = ConfigurationSystem.SectorListConfig["G9"];
            //    if (!string.IsNullOrEmpty(nearByOrigin) || !string.IsNullOrEmpty(nearByDestination))
            //        allowSearch = sectors.ContainsKey(nearByOrigin) && sectors[nearByOrigin].Contains(nearByDestination);
            //    else
            //        allowSearch = sectors.ContainsKey(searchRequest.Segments[0].Origin) && sectors[searchRequest.Segments[0].Origin].Contains(searchRequest.Segments[0].Destination);
            //}
            //else
            //    allowSearch = true;

            //if (allowSearch)
            //{
            searchBySegments = searchRequest.SearchBySegments;
            if (searchBySegments && searchRequest.Type == SearchType.Return)
            {
                Dictionary<string, int> readySources = new Dictionary<string, int>();
                Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();
                this.sessionId = sessionId;
                listOfThreads.Add("G9O1", new WaitCallback(SearchRoutingReturn));
                listOfThreads.Add("G9O2", new WaitCallback(SearchRoutingReturn));

                SearchRequest onwardRequest = searchRequest.Copy();
                onwardRequest.Type = SearchType.OneWay;
                onwardRequest.Segments = new FlightSegment[1];
                onwardRequest.Segments[0] = searchRequest.Segments[0];

                SearchRequest returnRequest = searchRequest.Copy();
                returnRequest.Type = SearchType.Return;
                returnRequest.Segments = new FlightSegment[1];
                returnRequest.Segments[0] = searchRequest.Segments[1];

                searchRequests.Add(onwardRequest);
                searchRequests.Add(returnRequest);

                eventFlag = new AutoResetEvent[2];
                readySources.Add("G9O1", 1200);
                readySources.Add("G9O2", 1200);

                int t = 0;
                //Start each fare search within a Thread which will automatically terminate after the results are received.
                foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                {
                    if (readySources.ContainsKey(deThread.Key))
                    {
                        ThreadPool.QueueUserWorkItem(deThread.Value, t);
                        eventFlag[t] = new AutoResetEvent(false);
                        t++;
                    }
                }

                if (t != 0)
                {
                    if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 2, 0), true))
                    {
                        //TODO: audit which thread is timed out                
                    }
                }
            }
            else
            {
                liSearchResult.AddRange(GetSearchResults(searchRequest, sessionId));
            }
            request = searchRequest;
            lstBasicResults.AddRange(liSearchResult);//Add search results from response for combination            

            //Write Logic For Clubing Bundle Fares with Basic Fares
            Dictionary<string, WaitCallback> lstOfThreads = new Dictionary<string, WaitCallback>();
            for (int i = 0; i < lstBasicResults.Count; i++)
            {
                //formation of basic & bundle results .
                for (int j = 0; j < lstBundlesOn.Count; j++)
                {                    
                    Parallel.Invoke(
                       () => { BuildBundleResults(i, j, -1); }//VALUE-BASIC, EXTRA-BASIC
                    );
                    if (searchRequest.Type == SearchType.Return)
                    {
                        for (int k = 0; k < lstBundlesRet.Count; k++)
                        {                            
                            Parallel.Invoke(
                                () => { BuildBundleResults(i, -1, k); }//BASIC-VALUE,BASIC-EXTRA
                            );
                        }
                    }
                }
                //Formation of bundle Combinations
                for (int j = 0; j < lstBundlesOn.Count; j++) // VALUE-EXTRA,VALUE-VALUE, EXTRA-VALUE,EXTRA-EXTRA 
                {
                    for (int k = 0; k < lstBundlesRet.Count; k++)
                    {                        
                        Parallel.Invoke(
                            () => { BuildBundleResults(i, j, k); }
                        );
                    }
                }
            }

            lstCombinationResults.AddRange(lstBasicResults);
            //Remove any result which is having ZERO price
            lstCombinationResults.ForEach(r =>
            {
                int index = lstCombinationResults.FindIndex(x => x.FareBreakdown != null && x.FareBreakdown[0].BaseFare == 0);
                if (index >= 0)
                    lstCombinationResults.RemoveAt(index);
            });

            if (lstCombinationResults.FindAll(x => x.Flights[0].Any(f => f.Group == 0)).Count == 0 || (searchRequest.Type == SearchType.Return && lstCombinationResults.FindAll(x => x.Flights[searchBySegments ? 0 : 1].Any(f => f.Group == 1)).Count == 0))
                lstCombinationResults.Clear();

            return lstCombinationResults.Where(x => x.TotalFare > 0).ToArray();
        }


        /// <summary>
        /// Method to get the search results
        /// </summary>
        /// <param name="searchRequest">Search request Object</param>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public SearchResult[] GetSearchResults(SearchRequest searchRequest, string sessionId)
        {
            //Trace.TraceInformation("AirArabia.Search Enter");
            bool allowSearch = false; string nearByOrigin = string.Empty; string nearByDestination = string.Empty;
            SearchResult[] searchresult = new SearchResult[0];
            Dictionary<string, string> sectors = new Dictionary<string, string>();
            /*Checking if near by airport city is available using config for maping near by city*/

            List<SectorList> locSectorlist = new List<SectorList>();
            locSectorlist = Sectors.FindAll(x => x.Supplier == "AirArabiaCityMapping").ToList();

           // if (ConfigurationSystem.SectorListConfig.ContainsKey("AirArabiaCityMapping")) //commented by bangar
            if (locSectorlist.Count() > 0)
            {
               // sectors = ConfigurationSystem.SectorListConfig["AirArabiaCityMapping"];//commented by bangar
                sectors = locSectorlist.ToDictionary(r => r.Origin, r => r.Sectors);

                if (sectors.ContainsKey(searchRequest.Segments[0].Destination))
                {
                    nearByDestination = sectors[searchRequest.Segments[0].Destination];
                    searchRequest.Segments[0].Destination = nearByDestination;
                }
                else
                {
                    nearByDestination = searchRequest.Segments[0].Destination;
                }
                if (sectors.ContainsKey(searchRequest.Segments[0].Origin) && searchRequest.Segments[0].NearByOriginPort)
                {
                    nearByOrigin = sectors[searchRequest.Segments[0].Origin];

                    searchRequest.Segments[0].Origin = nearByOrigin;
                }
                else
                {
                    nearByOrigin = searchRequest.Segments[0].Origin;
                }
            }
            else
            {
                Core.Audit.Add(Core.EventType.AirArabiaSearch, Core.Severity.High, 1, "AirArabia: Error While reading sector from AirArabiaconfig : Error" + DateTime.Now, "");
                //Trace.TraceError("AirArabia: Error While reading sector from AirArabiaconfig");
            }

            locSectorlist = Sectors.FindAll(x => x.Supplier == "G9").ToList();

            //if (ConfigurationSystem.SectorListConfig.ContainsKey("G9"))//commented by bangar
            if (locSectorlist.Count() > 0)
            {

                // sectors = ConfigurationSystem.SectorListConfig["G9"];//commented by bangar
                sectors = locSectorlist.ToDictionary(r => r.Origin, r => r.Sectors);

                if (nearByOrigin != string.Empty || nearByDestination != null)
                {
                    if (sectors.ContainsKey(nearByOrigin))
                    {
                        if (sectors[nearByOrigin].Contains(nearByDestination))
                        {
                            allowSearch = true;
                        }
                    }
                }
                else if (sectors.ContainsKey(searchRequest.Segments[0].Origin))
                {
                    if (sectors[searchRequest.Segments[0].Origin].Contains(searchRequest.Segments[0].Destination))
                    {
                        allowSearch = true;
                    }
                }
            }
            else
            {
                allowSearch = true;
            }
            if (allowSearch)
            {
                string requestforAvailability = GetRequestStringForAvailability(searchRequest, nearByOrigin, nearByDestination);
                string responseOfAvailability = string.Empty;

                CookieContainer cookie = new CookieContainer();
                try
                {
                    try
                    {
                        //StreamWriter writer = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaSearchREQ.xml");
                        //writer.Write(requestforAvailability);
                        //writer.Close();
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(requestforAvailability);
                        string filePath = xmlPath + "AirArabiaSearchRequest_" + sessionId + "_" + appUserId + "_" + searchRequest.Segments[0].Origin + "_" + searchRequest.Segments[0].Destination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        doc.Save(filePath);
                    }
                    catch { }
                    searchAvaibilityResponse = responseOfAvailability = GetResponse(requestforAvailability, ref cookie);
                    
                    try
                    {
                        //StreamWriter writer = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaSearchRes.xml");
                        //writer.Write(responseOfAvailability);
                        //writer.Close();
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(responseOfAvailability);
                        string filePath = xmlPath + "AirArabiaSearchResponse_" + sessionId + "_" + appUserId + "_" + searchRequest.Segments[0].Origin + "_" + searchRequest.Segments[0].Destination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        doc.Save(filePath);
                    }
                    catch { }
                    //Core.Audit.Add(Core.EventType.AirArabiaSearch, Core.Severity.Normal, 0, "AirArabia SearchResponse:" + responseOfAvailability + " Request XML - " + requestforAvailability + "| " + DateTime.Now, "");
                }
                catch (WebException excep)
                {
                    Core.Audit.Add(Core.EventType.AirArabiaSearch, Core.Severity.High, 0, "Web Exception returned from AirArabia. Error Message:" + excep.Message + " | " + DateTime.Now + "| request XML" + requestforAvailability, "");
                    //Trace.TraceError("Error: " + excep.Message);
                    throw excep;
                }
                if (responseOfAvailability != null && responseOfAvailability.Length > 0)
                {
                    searchresult = GetSearchResult(responseOfAvailability, searchRequest, sessionId, ref cookie);
                }
                else
                {
                    Core.Audit.Add(Core.EventType.AirArabiaSearch, Core.Severity.High, 0, "AirArabia No Result found | Request XML - " + requestforAvailability + DateTime.Now, "");
                    //Trace.TraceError("AirArabia search No result found");
                    throw new BookingEngineException("AirArabia search No result found");
                }
            }
            else
            {
                Core.Audit.Add(Core.EventType.AirArabiaSearch, Core.Severity.Normal, 1, "AirArabia sector Not Available for : " + nearByOrigin + "-" + nearByDestination, "0");
            }
            //Trace.TraceInformation("AirArabia.Search Exit");
            return searchresult;
        }

        private SearchResult[] GetSearchResult(string resultSt, SearchRequest request, string sessionId, ref CookieContainer cookie)
        {
            //Trace.TraceInformation("AirArabia.GetSearchResult Entered");
            SearchResult[] result;
            if(xmlDoc==null)
               xmlDoc = new XmlDocument();
            TextReader stringRead = new StringReader(resultSt);
            xmlDoc.Load(stringRead);
            nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
            XmlNode xAvailRS = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirAvailRS", nsmgr);
            XmlNode xErrors = xAvailRS.SelectSingleNode("ns1:Errors", nsmgr);
            if (xErrors != null)
            {
                XmlNodeList xError = xErrors.SelectNodes("ns1:Error", nsmgr);
                foreach (XmlNode xErr in xError)
                {
                    //Trace.TraceError("Error due to " + xErr.Attributes["ShortText"].Value.ToString());
                    Core.Audit.Add(Core.EventType.AirArabiaPriceRetrieve, Core.Severity.High, 0, "AirArabia: Get Search Results Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString() + "Response :- " + resultSt, "");
                    throw new BookingEngineException("AirArabia: Get Search Results Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString());
                }
            }
            sessionDta = new SessionData();
            sessionDta.transactionID = xAvailRS.Attributes["TransactionIdentifier"].Value.ToString();
            sessionDta.cookie = cookie;
            sessionDta.fare = new Dictionary<string, string>();
            sessionDta.totalPrice = new Dictionary<string, string>();
            sessionDta.SelectedResultXml = new Dictionary<string, string>();
            
            //Storing the Session Data in Basket (Used to Send the respetive TransactionId  in Combination search)
            if(!Basket.BookingSession[sessionId].ContainsKey(airlineCode + "-" + request.Segments[0].Origin + "-" + request.Segments[0].Destination))
               Basket.BookingSession[sessionId].Add(airlineCode + "-" + request.Segments[0].Origin + "-" + request.Segments[0].Destination, (object)sessionDta);
            else
               Basket.BookingSession[sessionId][airlineCode + "-" + request.Segments[0].Origin + "-" + request.Segments[0].Destination]= (object)sessionDta;

            //BundleServiceExt
            XmlNodeList onwardBundleService = null;
            XmlNodeList returnBundleService = null;
            XmlNode origindestInfo = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirAvailRS/ns1:AAAirAvailRSExt/ns1:PricedItineraries/ns1:PricedItinerary/ns1:AirItinerary/ns1:OriginDestinationOptions", nsmgr);

            //Bind the Onward & Return Bundle Services to lstBundlesOn & lstBundlesRet
            foreach (XmlNode node in origindestInfo)
            {
                List<XmlNodeList> bundleServices = new List<XmlNodeList>();

                if (node.Name == "ns1:AABundledServiceExt" && node.Attributes["applicableOndSequence"].Value == "0")
                {
                    onwardBundleService = node.SelectNodes("ns1:bundledService", nsmgr);
                }
                if (node.Name == "ns1:AABundledServiceExt" && node.Attributes["applicableOndSequence"].Value == "1")
                {
                    returnBundleService = node.SelectNodes("ns1:bundledService", nsmgr);
                }
            }

            if (onwardBundleService != null && onwardBundleService.Count > 0)
            {
                foreach (XmlNode onwardBundle in onwardBundleService)
                {
                    int onwServiceId = Convert.ToInt32(onwardBundle.SelectSingleNode("ns1:bunldedServiceId", nsmgr).InnerText);
                    string onwardFareType = (onwardBundle.SelectSingleNode("ns1:bundledServiceName", nsmgr) != null) ? onwardBundle.SelectSingleNode("ns1:bundledServiceName", nsmgr).InnerText : string.Empty;
                    string onwardDescription = (onwardBundle.SelectSingleNode("ns1:description", nsmgr) != null) ? onwardBundle.SelectSingleNode("ns1:description", nsmgr).InnerText : string.Empty;
                    string onwbookingClass = (onwardBundle.SelectSingleNode("ns1:bookingClasses", nsmgr) != null) ? onwardBundle.SelectSingleNode("ns1:bookingClasses", nsmgr).InnerText : string.Empty;
                    string onwardbaggage = !string.IsNullOrEmpty(onwardDescription) ? onwardDescription.Split(',').ToList().FirstOrDefault(x => x.Contains("baggage")).Replace("checked baggage", "").Replace("Includes", "").Replace("Kg", "").Trim() : string.Empty;

                    lstBundlesOn.Add(onwServiceId + "#" + onwardFareType + "#" + onwbookingClass + "#" + onwardDescription + "#" + onwardbaggage);
                }
            }
            if (returnBundleService != null && returnBundleService.Count > 0)
            {
                foreach (XmlNode returnBundle in returnBundleService)
                {
                    int retServiceId = Convert.ToInt32(returnBundle.SelectSingleNode("ns1:bunldedServiceId", nsmgr).InnerText);
                    string retFareType = (returnBundle.SelectSingleNode("ns1:bundledServiceName", nsmgr) != null) ? returnBundle.SelectSingleNode("ns1:bundledServiceName", nsmgr).InnerText : string.Empty;
                    string retDescription = (returnBundle.SelectSingleNode("ns1:description", nsmgr) != null) ? returnBundle.SelectSingleNode("ns1:description", nsmgr).InnerText : string.Empty;
                    string retbookingClass = (returnBundle.SelectSingleNode("ns1:bookingClasses", nsmgr) != null) ? returnBundle.SelectSingleNode("ns1:bookingClasses", nsmgr).InnerText : string.Empty;
                    string retbaggage = !string.IsNullOrEmpty(retDescription) ? retDescription.Split(',').ToList().FirstOrDefault(x => x.Contains("baggage")).Replace("checked baggage", "").Replace("Includes", "").Replace("Kg", "").Trim() : string.Empty;

                    lstBundlesRet.Add(retServiceId + "#" + retFareType + "#" + retbookingClass + "#" + retDescription + "#" + retbaggage);
                }
            }


            int i = 0;
            XmlNodeList xnodeInfo = xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_AirAvailRS/ns1:OriginDestinationInformation", nsmgr);
            int count = xnodeInfo.Count;
            result = new SearchResult[count];
            RPH = new string[count][];

            foreach (XmlNode xnodeOridinDesiInfo in xnodeInfo)
            {
                bool isReturn = false;
                result[i] = new SearchResult();
                result[i].IsLCC = true;
                result[i].TicketAdvisory = xAvailRS.Attributes["Version"].Value;
                result[i].Price = new PriceAccounts();
                result[i].ResultBookingSource = BookingSource.AirArabia;
                XmlNode xFlightInfo = xnodeOridinDesiInfo.SelectSingleNode("ns1:OriginDestinationOptions", nsmgr);
                XmlNodeList flightNodes = xFlightInfo.SelectNodes("ns1:OriginDestinationOption", nsmgr);
                RPH[i] = new string[flightNodes.Count];
                result[i].FareRules = new List<FareRule>();
                int c = 0;
                List<FlightInfo> outboundFlights = new List<FlightInfo>();
                List<FlightInfo> inboundFlights = new List<FlightInfo>();
                foreach (XmlNode flightNodex in flightNodes)
                {
                    FlightInfo flight = new FlightInfo();
                    flight.ETicketEligible = true;
                    XmlNode flightNode = flightNodex.SelectSingleNode("ns1:FlightSegment", nsmgr);
                    flight.DepartureTime = Convert.ToDateTime(flightNode.Attributes["DepartureDateTime"].Value);
                    flight.ArrivalTime = Convert.ToDateTime(flightNode.Attributes["ArrivalDateTime"].Value);
                    flight.FlightNumber = flightNode.Attributes["FlightNumber"].Value.Substring(2);
                    flight.Airline = flightNode.Attributes["FlightNumber"].Value.Substring(0, 2);
                    try
                    {
                        flight.Duration = GetDuration(flightNode.Attributes["JourneyDuration"].Value);
                    }
                    catch { }
                    flight.Status = "HK";
                    FareRule newFareRule = new FareRule();
                    newFareRule.Airline = Convert.ToString(flightNode.Attributes["FlightNumber"].Value.Substring(0, 2));
                    newFareRule.DepartureTime = Convert.ToDateTime(flightNode.Attributes["DepartureDateTime"].Value);
                    XmlNode FareBasisCodeNode = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirAvailRS/ns1:AAAirAvailRSExt/ns1:PricedItineraries/ns1:PricedItinerary/ns1:AirItineraryPricingInfo/ns1:PTC_FareBreakdowns/ns1:PTC_FareBreakdown/ns1:FareBasisCodes/ns1:FareBasisCode", nsmgr);
                    if (FareBasisCodeNode != null)
                    {
                        newFareRule.FareBasisCode = FareBasisCodeNode.InnerText.ToString();
                    }

                    flight.BookingClass = newFareRule.FareBasisCode.Substring(0, 1);
                    flight.CabinClass = "Economy";
                    string rphKey = Convert.ToString(flightNode.Attributes["RPH"].Value);
                    //string rph = "G9$DEL/SHJ$1571238$20140927044000$20140927063500";
                    //rph = rph.Remove(rph.LastIndexOf('$'),11);
                    ////rph = rph.Remove(rph.LastIndexOf('$'));
                    //rph = rph.Substring(rph.LastIndexOf('$') + 1, (rph.Length - (rph.LastIndexOf('$') + 1)));
                    rphKey = rphKey.Substring(rphKey.LastIndexOf('$', 10) + 1, rphKey.LastIndexOf('$', 13) - 3);
                    flight.FareInfoKey = (rphKey);
                    flight.UapiDepartureTime = Convert.ToString(flightNode.Attributes["RPH"].Value);//RPH is storing in 'UapiDepartureTime' variable
                    //flight.SegmentId= Convert.ToInt32(flightNode.Attributes["RPH"].Value);
                    RPH[i][c] = flightNode.Attributes["RPH"].Value.ToString();
                    result[i].FareType = (request.Type == SearchType.OneWay || request.SearchBySegments) ? "Basic" : "Basic,Basic";

                    if (flightNode.SelectSingleNode("ns1:DepartureAirport", nsmgr) != null)
                    {
                        flight.Origin = new Airport(flightNode.SelectSingleNode("ns1:DepartureAirport", nsmgr).Attributes["LocationCode"].Value.ToString());
                        newFareRule.Origin = flightNode.SelectSingleNode("ns1:DepartureAirport", nsmgr).Attributes["LocationCode"].Value.ToString();
                        flight.DepTerminal = flightNode.SelectSingleNode("ns1:DepartureAirport", nsmgr).Attributes["Terminal"].Value.ToString();
                    }
                    if (flightNode.SelectSingleNode("ns1:ArrivalAirport", nsmgr) != null)
                    {
                        flight.Destination = new Airport(flightNode.SelectSingleNode("ns1:ArrivalAirport", nsmgr).Attributes["LocationCode"].Value.ToString());
                        flight.ArrTerminal = flightNode.SelectSingleNode("ns1:ArrivalAirport", nsmgr).Attributes["Terminal"].Value.ToString();
                        newFareRule.Destination = flightNode.SelectSingleNode("ns1:ArrivalAirport", nsmgr).Attributes["LocationCode"].Value.ToString();
                    }
                    if (flight.Origin.AirportCode == request.Segments[0].Destination)
                    {
                        isReturn = true;
                    }
                    if (isReturn)
                    {                        
                        flight.Group = 1;//Set the Group for Return flight in order to display baggage
                        inboundFlights.Add(flight);
                    }
                    else
                    {
                        outboundFlights.Add(flight);
                    }
                    flight.Group = (searchBySegments && request.Type == SearchType.Return) ? 1 : flight.Group;
                    result[i].FareRules.Add(newFareRule);
                    c++;
                }
                if (inboundFlights.Count > 0)
                {
                    result[i].Flights = new FlightInfo[2][];
                    result[i].Flights[0] = outboundFlights.ToArray();
                    result[i].Flights[1] = inboundFlights.ToArray();
                    foreach (FlightInfo seg in result[i].Flights[0])
                    {
                        seg.Stops = outboundFlights.Count - 1;
                    }
                    foreach (FlightInfo seg in result[i].Flights[1])
                    {
                        seg.Stops = inboundFlights.Count - 1;
                    }
                    result[i].Flights[0].ToList().ForEach(x => x.SegmentFareType = "Basic");
                    result[i].Flights[1].ToList().ForEach(x => x.SegmentFareType = "Basic");
                    result[i].FareType = "Basic,Basic";
                }
                else
                {
                    result[i].Flights = new FlightInfo[1][];
                    result[i].Flights[0] = outboundFlights.ToArray();

                    foreach (FlightInfo seg in result[i].Flights[0])
                    {
                        seg.Stops = outboundFlights.Count - 1;
                    }
                    result[i].Flights[0].ToList().ForEach(x => x.SegmentFareType = "Basic");
                    result[i].FareType = "Basic";
                }
                i++;
            }
            XmlNodeList xPriceItinerary = xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_AirAvailRS/ns1:AAAirAvailRSExt/ns1:PricedItineraries/ns1:PricedItinerary", nsmgr);
            string[] sessionRPH = new string[0];
            foreach (XmlNode xPriceItin in xPriceItinerary)
            {
                //int g = 0;
                XmlNodeList xPriceFlightoption = xPriceItin.SelectNodes("ns1:AirItinerary/ns1:OriginDestinationOptions/ns1:OriginDestinationOption/ns1:FlightSegment", nsmgr);
                int seg = 0;
                sessionRPH = new string[xPriceFlightoption.Count];
                string[] flightnoInprice = new string[xPriceFlightoption.Count];
                string[] arrtime = new string[xPriceFlightoption.Count];
                string[] deptime = new string[xPriceFlightoption.Count];
                string segmentsKey = string.Empty;
                foreach (XmlNode segnode in xPriceFlightoption)
                {
                    if (string.IsNullOrEmpty(basicRPHValues))
                        basicRPHValues = segnode.Attributes["RPH"].Value.ToString() + "Basic";
                    else
                        basicRPHValues += "#" + segnode.Attributes["RPH"].Value.ToString() + "Basic";

                    sessionRPH[seg] = segnode.Attributes["RPH"].Value.ToString();
                    flightnoInprice[seg] = segnode.Attributes["FlightNumber"].Value.ToString();
                    arrtime[seg] = segnode.Attributes["ArrivalDateTime"].Value.ToString();
                    deptime[seg] = segnode.Attributes["DepartureDateTime"].Value.ToString();
                    seg++;
                    segmentsKey += segnode.Attributes["FlightNumber"].Value.ToString() + segnode.Attributes["DepartureDateTime"].Value.ToString() + segnode.Attributes["ArrivalDateTime"].Value.ToString();
                }
                for (int j = 0; j < result.Length; j++)//result count
                {
                    //bool st = true;
                    string resultSegmentKey = string.Empty;
                    result[j].EticketEligible = true;

                    //commented by lokesh on 29-Mar-2018.
                    //Need to generate All AirArabiaPriceQTRequest for all the results.
                    //So,now all the results are repriced.

                    //for (int f = 0; f < result[j].Flights.Length; f++)
                    //{
                    //    for (int k = 0; k < result[j].Flights[f].Length; k++)
                    //    {
                    //        resultSegmentKey += result[j].Flights[f][k].Airline + result[j].Flights[f][k].FlightNumber + result[j].Flights[f][k].DepartureTime.ToString("yyyy-MM-ddTHH:mm:ss") + result[j].Flights[f][k].ArrivalTime.ToString("yyyy-MM-ddTHH:mm:ss");
                    //    }
                    //}
                    //if (segmentsKey == resultSegmentKey)
                    //{
                    //    XmlNode xPrice = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirAvailRS/ns1:AAAirAvailRSExt/ns1:PricedItineraries/ns1:PricedItinerary", nsmgr);
                    //    fareXml = xPrice.InnerXml;
                    //    XmlNode ItinTotalFareInfo = xPrice.SelectSingleNode("ns1:AirItineraryPricingInfo/ns1:ItinTotalFare", nsmgr);
                    //    XmlElement ItinTotalFareElement = (XmlElement)ItinTotalFareInfo;
                    //    //StaticData staticDta = new StaticData();
                    //    //staticDta.Load("AED");//Loading rate of Exchange of United Arab
                    //    //double roe = Convert.ToDouble(ConfigurationSystem.AirArabiaConfig["ROE"]);                        
                    //    if (exchangeRates.ContainsKey(ItinTotalFareElement["ns1:TotalFare"].Attributes["CurrencyCode"].Value))
                    //    {
                    //        roe = exchangeRates[ItinTotalFareElement["ns1:TotalFare"].Attributes["CurrencyCode"].Value];
                    //    }

                    //    string key = BuildResultKey(result[j]);
                    //    sessionDta.totalPrice.Add(key, ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount").ToString());
                    //    result[j].BaseFare = Convert.ToDouble(ItinTotalFareElement["ns1:BaseFare"].GetAttribute("Amount")) * (double)roe;
                    //    result[j].TotalFare = Convert.ToDouble(ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount")) * (double)roe;
                    //    result[j].Currency = agentCurrency;
                    //    result[j].Tax = result[j].TotalFare - result[j].BaseFare;
                    //    result[j].Price = new PriceAccounts();
                    //    result[j].Price.SupplierCurrency = ItinTotalFareElement["ns1:TotalFare"].Attributes["CurrencyCode"].Value;
                    //    result[j].Price.SupplierPrice = Convert.ToDecimal(ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount"));
                    //    XmlNodeList priceDetail = xPrice.SelectNodes("ns1:AirItineraryPricingInfo/ns1:PTC_FareBreakdowns/ns1:PTC_FareBreakdown", nsmgr);
                    //    int paxType = 0;
                    //    if (request.AdultCount > 0)
                    //    {
                    //        paxType++;
                    //    }
                    //    if (request.ChildCount > 0)
                    //    {
                    //        paxType++;
                    //    }
                    //    if (request.SeniorCount > 0)
                    //    {
                    //        paxType++;
                    //    }
                    //    if (request.InfantCount > 0)
                    //    {
                    //        paxType++;
                    //    }
                    //    result[j].FareBreakdown = new Fare[paxType];
                    //    int f = 0;
                    //    foreach (XmlNode paxtype in priceDetail)
                    //    {
                    //        XmlNode xPassengerFare = paxtype.SelectSingleNode("ns1:PassengerFare", nsmgr);
                    //        XmlNode xPassengerType = paxtype.SelectSingleNode("ns1:PassengerTypeQuantity", nsmgr);
                    //        if ((request.AdultCount != 0 || request.SeniorCount != 0) && xPassengerType.Attributes["Code"].Value.ToString() == "ADT")
                    //        {
                    //            //StaticData staticDta = new StaticData();
                    //            //staticDta.Load("AED");//Loading rate of Exchange of United Arab
                    //            if (request.AdultCount != 0)
                    //            {
                    //                result[j].FareBreakdown[f] = new Fare();
                    //                result[j].FareBreakdown[f].PassengerType = PassengerType.Adult;
                    //                result[j].FareBreakdown[f].PassengerCount = request.AdultCount;

                    //                XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                    //                result[j].FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * (double)roe;
                    //                XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                    //                result[j].FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * (double)roe;
                    //                result[j].FareBreakdown[f].SupplierFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount);
                    //                f++;
                    //            }
                    //            if (request.SeniorCount != 0)
                    //            {
                    //                result[j].FareBreakdown[f] = new Fare();
                    //                result[j].FareBreakdown[f].PassengerType = PassengerType.Senior;
                    //                result[j].FareBreakdown[f].PassengerCount = request.SeniorCount;
                    //                XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                    //                result[j].FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * (double)roe;
                    //                XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                    //                result[j].FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * (double)roe;
                    //                result[j].FareBreakdown[f].SupplierFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount);
                    //                f++;
                    //            }
                    //        }
                    //        else if (request.ChildCount != 0 && xPassengerType.Attributes["Code"].Value.ToString() == "CHD")
                    //        {
                    //            result[j].FareBreakdown[f] = new Fare();
                    //            result[j].FareBreakdown[f].PassengerType = PassengerType.Child;
                    //            result[j].FareBreakdown[f].PassengerCount = request.ChildCount;
                    //            XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                    //            result[j].FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * (double)roe;
                    //            XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                    //            result[j].FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * (double)roe;
                    //            result[j].FareBreakdown[f].SupplierFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount);
                    //            f++;
                    //        }
                    //        else if (request.InfantCount != 0 && xPassengerType.Attributes["Code"].Value.ToString() == "INF")
                    //        {
                    //            result[j].FareBreakdown[f] = new Fare();
                    //            result[j].FareBreakdown[f].PassengerType = PassengerType.Infant;
                    //            result[j].FareBreakdown[f].PassengerCount = request.InfantCount;
                    //            XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                    //            result[j].FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * (double)roe;
                    //            XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                    //            result[j].FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * (double)roe;
                    //            result[j].FareBreakdown[f].SupplierFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount);
                    //            f++;
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    sessionRPH = GetFare(ref result[j], request, ref sessionDta, ref RPH[j], ref fareXml, sessionId, ref cookie);
                    //}
                    result[j].ResultKey = request.Segments[0].Origin + "-" + request.Segments[0].Destination;
                    //string keyfare = BuildResultKey(result[j]);
                    //sessionDta.fare.Add(keyfare, fareXml);
                }
            }
            //Trace.TraceInformation("AirArabia.GetSearchResult Exit");
            //Basket.BookingSession[sessionId].Add(airlineCode+"-"+request.Segments[0].Origin+"-"+ request.Segments[0].Destination, (object)sessionDta);
            return result;
        }

        private TimeSpan GetDuration(string stime)
        {
            string hours = "";
            if (stime.Split('.')[0].Contains("H"))
            {
                hours = stime.Split('.')[0].Split('H')[0].Remove(0, 2);
            }
            else
            {
                hours = "00";
            }
            string mins = "";
            if (stime.Split('.')[0].Contains("H"))
            {
                mins = stime.Split('.')[0].Split('H')[1].Split('M')[0];
            }
            else
            {
                mins = stime.Split('.')[0].Split('M')[0].Remove(0, 2);
            }
            TimeSpan time = new TimeSpan(Convert.ToInt32(hours), Convert.ToInt32(mins), 0);
            return time;
        }

        /// <summary>
        /// This method returns the Seat Availability response based on the segments.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public SeatAvailabilityResp GetSeatAvailableSSR(SearchResult result, string sOrigin, string sDestination,
            SeatAvailabilityResp clsSeatAvailabilityResp, string session)
        {   
            try
            {
                SegmentSeats clsSegmentSeats = new SegmentSeats();
                SegmentSeats seg = clsSeatAvailabilityResp.SegmentSeat.Where(x => x.Origin == sOrigin && x.Destination == sDestination).FirstOrDefault();
                SessionData data = (SessionData)Basket.BookingSession[sessionId][result.Airline + "-" + result.ResultKey];
                string sTravelRPH = (result.Flights.Length > 1 && result.Flights[1].Where(x => x.FlightNumber == seg.FlightNo).Count() > 0) ?
                    result.Flights[1].Where(x => x.FlightNumber == seg.FlightNo).Select(y => y.UapiDepartureTime).FirstOrDefault() :
                    result.Flights[0].Where(x => x.FlightNumber == seg.FlightNo).Select(y => y.UapiDepartureTime).FirstOrDefault(); 

                StringBuilder SeatAvailRequest = new StringBuilder();
                XmlWriter writer = XmlTextWriter.Create(SeatAvailRequest);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                #region Header
                writer.WriteStartElement("soap", "Header", null);
                writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
                writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
                writer.WriteStartElement("wsse", "UsernameToken", null);
                writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
                writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-16755466");
                //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
                writer.WriteStartElement("wsse", "Username", null);
                //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
                writer.WriteValue(userName);
                writer.WriteEndElement();
                writer.WriteStartElement("wsse", "Password", null);
                writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
                //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
                writer.WriteValue(password);
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                #endregion

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteAttributeString("xmlns", "ns1", null, "http://www.opentravel.org/OTA/2003/05");
                writer.WriteStartElement("ns1", "OTA_AirSeatMapRQ", null);
                writer.WriteAttributeString("EchoToken", "11835513529845-916011621");//check later
                writer.WriteAttributeString("PrimaryLangID", "en-us");
                writer.WriteAttributeString("SequenceNmbr", "1");
                writer.WriteAttributeString("TransactionIdentifier", data.transactionID);
                writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                writer.WriteAttributeString("Version", "2006.01");//fix
                #region POSDetailFix
                writer.WriteStartElement("ns1", "POS", null);

                writer.WriteStartElement("ns1", "Source", null);
                writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");

                writer.WriteStartElement("ns1", "RequestorID", null);
                writer.WriteAttributeString("Type", "4");
                writer.WriteAttributeString("ID", userName);
                writer.WriteEndElement();

                writer.WriteStartElement("ns1", "BookingChannel", null);
                writer.WriteAttributeString("Type", "12");
                writer.WriteEndElement();

                writer.WriteEndElement();//Source
                writer.WriteEndElement();//POS
                #endregion


                writer.WriteStartElement("ns1", "SeatMapRequests", null);

                writer.WriteStartElement("ns1", "SeatMapRequest", null);
                writer.WriteStartElement("ns1", "FlightSegmentInfo", null);
                writer.WriteAttributeString("DepartureDateTime", seg.STD.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                writer.WriteAttributeString("ArrivalDateTime", seg.STA.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                //writer.WriteAttributeString("FlightNumber", Flights[i][j].Airline + Flights[i][j].FlightNumber);
                writer.WriteAttributeString("RPH", sTravelRPH);

                writer.WriteStartElement("ns1", "DepartureAirport", null);
                writer.WriteAttributeString("LocationCode", seg.Origin);
                writer.WriteEndElement();

                writer.WriteStartElement("ns1", "ArrivalAirport", null);
                writer.WriteAttributeString("LocationCode", seg.Destination);
                writer.WriteEndElement();

                writer.WriteEndElement();//Flight Segment Info
                writer.WriteEndElement();//BaggageDetailsRequest                

                writer.WriteEndElement();//BaggageDetailsRequests
                writer.WriteEndElement();//AA_OTA_AirBaggageDetailsRQ
                writer.WriteEndElement();

                writer.Flush();
                writer.Close();


                GenericStatic.WriteLogFileXML(Convert.ToString(SeatAvailRequest),
                    xmlPath + "AirArabiaSeatAvailabilityRequest_" + sessionId + "_" + appUserId + "_" + seg.Origin + "_" + seg.Destination + "_");

                string response = GetResponse(Convert.ToString(SeatAvailRequest), ref data.cookie);

                string filePath = GenericStatic.WriteLogFileXML(response,
                    xmlPath + "AirArabiaSeatAvailabilityResponse_" + sessionId + "_" + appUserId + "_" + seg.Origin + "_" + seg.Destination + "_");

                clsSegmentSeats = GenericStatic.TransformXMLAndDeserialize(clsSegmentSeats, File.ReadAllText(filePath), sSeatAvailResponseXSLT);
                clsSegmentSeats.SeatInfoDetails = clsSegmentSeats.SeatInfoDetails.GroupBy(x => x.SeatNo).Select(y => y.First()).ToList();
                clsSegmentSeats.FlightNo = seg.FlightNo;

                var itemIndex = clsSeatAvailabilityResp.SegmentSeat.FindIndex(x => x.Origin == sOrigin && x.Destination == sDestination);
                clsSeatAvailabilityResp.SegmentSeat.RemoveAt(itemIndex);
                clsSeatAvailabilityResp.SegmentSeat.Insert(itemIndex, clsSegmentSeats);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(Indigo)Failed to get seat availability response. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return clsSeatAvailabilityResp;
        }

        public DataTable GetBaggageDetails(SearchResult result,string sessionId)
        {
            TextReader stringRead = null;
            XmlDocument requestXmlDoc = null;
            XmlDocument responseXmlDoc = null;
            try
            {
                //for (int i = 0; i < result.Flights.Length; i++)
                {

                    {

                        result.ResultKey = result.Flights[0].Select(x => x.Origin.AirportCode).FirstOrDefault() + "-" +
                            result.Flights[0].Select(x => x.Destination.AirportCode).Last();

                        SessionData data = (SessionData)Basket.BookingSession[sessionId][result.Airline + "-" + result.ResultKey];

                        //Prepare the Baggage request for the flight segment
                        string request = "";
                        request = GetBaggageRequestString(result.Flights, data, false);
                        string sOrigin = result.Flights[0].Select(x => x.Origin.AirportCode).FirstOrDefault();
                        string sDestination = result.Flights[0].Select(x => x.Destination.AirportCode).Last();
                        try
                        {
                            //StreamWriter writer = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaOnwardBaggageReq.xml");
                            //writer.Write(request);
                            //writer.Close();

                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(request);
                            string filePath = xmlPath + "AirArabiaOnwardBaggageRequest_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                        catch { }

                        //CookieContainer cookie = new CookieContainer();
                        //Get the baggage response
                        string response = GetResponse(request, ref data.cookie);
                        try
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(response);
                            //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaOnwardBaggageRes.xml");
                            string filePath = xmlPath + "AirArabiaOnwardBaggageResponse_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                            doc.Save(filePath);
                        }
                        catch { }

                        //Validate RPH & Flight Number for Onward Baggage

                        requestXmlDoc = new XmlDocument();
                        stringRead = new StringReader(request);
                        requestXmlDoc.Load(stringRead);

                        responseXmlDoc = new XmlDocument();
                        stringRead = new StringReader(response);
                        responseXmlDoc.Load(stringRead);

                        XmlNamespaceManager nsmgr = new XmlNamespaceManager(requestXmlDoc.NameTable);
                        nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                        nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                        nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
                        nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

                        XmlNode requestRPH = requestXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AirBaggageDetailsRQ/ns1:BaggageDetailsRequests", nsmgr);
                        XmlNode responseRPH = responseXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AirBaggageDetailsRS/ns1:BaggageDetailsResponses", nsmgr);

                        XmlNodeList requestFlightNodes = requestRPH.SelectNodes("ns1:BaggageDetailsRequest/ns1:FlightSegmentInfo", nsmgr);
                        XmlNodeList responseFlightNodes = responseRPH.SelectNodes("ns1:OnDBaggageDetailsResponse/ns1:OnDFlightSegmentInfo", nsmgr);
                        if (requestFlightNodes != null && responseFlightNodes != null && ValidateResponse(requestFlightNodes, responseFlightNodes, nsmgr))
                        {
                            dtBaggageDetails = ReadBaggageResponse(response);
                        }
                        if (result.Flights.Length > 1)
                        {
                            //------------------------------------------------------------------------------------------------
                            //                      Generate Return Baggage Request and Response                            //
                            //------------------------------------------------------------------------------------------------
                            request = GetBaggageRequestString(result.Flights, data, true);
                            sOrigin = result.Flights[1].Select(x => x.Origin.AirportCode).FirstOrDefault();
                            sDestination = result.Flights[1].Select(x => x.Destination.AirportCode).Last();
                            try
                            {
                                //StreamWriter writer = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaReturnBaggageReq.xml");
                                //writer.Write(request);
                                //writer.Close();
                                XmlDocument doc = new XmlDocument();
                                doc.LoadXml(request);
                                string filePath = xmlPath + "AirArabiaReturnBaggageRequest_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                doc.Save(filePath);
                            }
                            catch { }

                            //Get the baggage response
                            response = GetResponse(request, ref data.cookie);
                            try
                            {
                                XmlDocument doc = new XmlDocument();
                                doc.LoadXml(response);
                                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaReturnBaggageRes.xml");                            
                                string filePath = xmlPath + "AirArabiaReturnBaggageResponse_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                                doc.Save(filePath);
                            }
                            catch { }

                            //Validate RPH & Flight Number for Return Baggage

                            requestXmlDoc = new XmlDocument();
                            stringRead = new StringReader(request);
                            requestXmlDoc.Load(stringRead);

                            responseXmlDoc = new XmlDocument();
                            stringRead = new StringReader(response);
                            responseXmlDoc.Load(stringRead);

                            requestRPH = requestXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AirBaggageDetailsRQ/ns1:BaggageDetailsRequests", nsmgr);
                            responseRPH = responseXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AirBaggageDetailsRS/ns1:BaggageDetailsResponses", nsmgr);

                            requestFlightNodes = requestRPH.SelectNodes("ns1:BaggageDetailsRequest/ns1:FlightSegmentInfo", nsmgr);
                            responseFlightNodes = responseRPH.SelectNodes("ns1:OnDBaggageDetailsResponse/ns1:OnDFlightSegmentInfo", nsmgr);
                            if (requestFlightNodes != null && responseFlightNodes != null && ValidateResponse(requestFlightNodes, responseFlightNodes, nsmgr))
                            {
                                dtBaggageDetails = ReadBaggageResponse(response);
                            }
                        }
                        //flightBaggages.Add(segmentId, dtBD);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtBaggageDetails;
        }

        private string GetBaggageRequestString(FlightInfo[][] Flights,SessionData data,bool isReturn)
        {          

            StringBuilder baggageRequest = new StringBuilder();
            //StreamWriter sw = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaBaggageReq.xml");
            //XmlTextWriter writer = new XmlTextWriter(sw);
            //writer.Indentation = 4;
            //writer.Formatting = Formatting.Indented;
            XmlWriter writer = XmlTextWriter.Create(baggageRequest);            
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-16755466");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns1", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns1", "AA_OTA_AirBaggageDetailsRQ", null);
            writer.WriteAttributeString("EchoToken", "11835513529845-916011621");//check later
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("TransactionIdentifier", data.transactionID);
            writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
            writer.WriteAttributeString("Version", "2006.01");//fix
            #region POSDetailFix
            writer.WriteStartElement("ns1", "POS", null);
           
            writer.WriteStartElement("ns1", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
            
            writer.WriteStartElement("ns1", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", userName);
            writer.WriteEndElement();
            
            writer.WriteStartElement("ns1", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();

            writer.WriteEndElement();//Source
            writer.WriteEndElement();//POS
            #endregion


            writer.WriteStartElement("ns1", "BaggageDetailsRequests",null);
            
            
            //for (int i = 0; i < Flights.Length; i++)
            int i = 0;
            if (!isReturn)
            {
                for (int j = 0; j < Flights[i].Length; j++)
                {
                    writer.WriteStartElement("ns1", "BaggageDetailsRequest", null);
                    writer.WriteAttributeString("TravelerRefNumberRPHs", "");
                    writer.WriteStartElement("ns1", "FlightSegmentInfo", null);
                    writer.WriteAttributeString("ArrivalDateTime", Flights[i][j].ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                    writer.WriteAttributeString("DepartureDateTime", Flights[i][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                    writer.WriteAttributeString("FlightNumber", Flights[i][j].Airline + Flights[i][j].FlightNumber);
                    //writer.WriteAttributeString("RPH", result.Flights[i][0].SegmentId.ToString());
                    writer.WriteAttributeString("RPH", Flights[i][j].UapiDepartureTime);

                    writer.WriteStartElement("ns1", "DepartureAirport", null);
                    writer.WriteAttributeString("LocationCode", Flights[i][j].Origin.AirportCode);
                    writer.WriteAttributeString("CodeContext", "IATA");
                    writer.WriteAttributeString("Terminal", Flights[i][j].DepTerminal);
                    //writer.WriteAttributeString("Gate", "1");
                    writer.WriteEndElement();

                    writer.WriteStartElement("ns1", "ArrivalAirport", null);
                    writer.WriteAttributeString("LocationCode", Flights[i][j].Destination.AirportCode);
                    writer.WriteAttributeString("CodeContext", "IATA");
                    writer.WriteAttributeString("Terminal", Flights[i][j].ArrTerminal);
                    //writer.WriteAttributeString("Gate", "1");
                    writer.WriteEndElement();

                    writer.WriteStartElement("ns1", "OperatingAirline", null);
                    writer.WriteAttributeString("Code", "G9");
                    writer.WriteEndElement();

                    writer.WriteEndElement();//Flight Segment Info
                    writer.WriteEndElement();//BaggageDetailsRequest
                }
            }
            else //return
            {
                i = 1;
                for (int j = 0; j < Flights[i].Length; j++)
                {
                    writer.WriteStartElement("ns1", "BaggageDetailsRequest", null);
                    writer.WriteAttributeString("TravelerRefNumberRPHs", "");
                    writer.WriteStartElement("ns1", "FlightSegmentInfo", null);
                    writer.WriteAttributeString("ArrivalDateTime", Flights[i][j].ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                    writer.WriteAttributeString("DepartureDateTime", Flights[i][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                    writer.WriteAttributeString("FlightNumber", Flights[i][j].Airline + Flights[i][j].FlightNumber);
                    //writer.WriteAttributeString("RPH", result.Flights[i][0].SegmentId.ToString());
                    writer.WriteAttributeString("RPH", Flights[i][j].UapiDepartureTime);

                    writer.WriteStartElement("ns1", "DepartureAirport", null);
                    writer.WriteAttributeString("LocationCode", Flights[i][j].Origin.AirportCode);
                    writer.WriteAttributeString("CodeContext", "IATA");
                    writer.WriteAttributeString("Terminal", Flights[i][j].DepTerminal);
                    //writer.WriteAttributeString("Gate", "1");
                    writer.WriteEndElement();

                    writer.WriteStartElement("ns1", "ArrivalAirport", null);
                    writer.WriteAttributeString("LocationCode", Flights[i][j].Destination.AirportCode);
                    writer.WriteAttributeString("CodeContext", "IATA");
                    writer.WriteAttributeString("Terminal", Flights[i][j].ArrTerminal);
                    //writer.WriteAttributeString("Gate", "1");
                    writer.WriteEndElement();

                writer.WriteStartElement("ns1", "OperatingAirline", null);
                writer.WriteAttributeString("Code", "G9");
                writer.WriteEndElement();

                writer.WriteEndElement();//Flight Segment Info
                writer.WriteEndElement();//BaggageDetailsRequest
            }
}
            writer.WriteEndElement();//BaggageDetailsRequests
            writer.WriteEndElement();//AA_OTA_AirBaggageDetailsRQ
            writer.WriteEndElement();
            
            
            writer.Flush();
            writer.Close();


            return baggageRequest.ToString();
        }

        private DataTable ReadBaggageResponse(string response)
        {
            
            //Trace.TraceInformation("AirArabia.readBaggageResponse Enter");
            FlightItinerary itinerary = new FlightItinerary();
            XmlDocument xmlDoc = new XmlDocument();
            TextReader stringRead = new StringReader(response);
            xmlDoc.Load(stringRead);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
            nsmgr.AddNamespace("ns2", "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05");
            XmlNode xerror = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AirBaggageDetailsRS/ns1:Errors/ns1:Error", nsmgr);
            if (xerror != null && xerror.Attributes["ShortText"] != null)
            {
                Core.Audit.Add(Core.EventType.AirArabiaRetrieveByPNR, Core.Severity.High, 0, "Exception returned from AirArabia. Error Message:" + xerror.Attributes["ShortText"].Value.ToString() + " | " + DateTime.Now + "| request XML" + response, "");
                //Trace.TraceError("Error: " + xerror.Attributes["ShortText"].Value.ToString());
                //throw new BookingEngineException(" Error: " + xerror.Attributes["ShortText"].Value.ToString());
                //Audit.Add(EventType.AirArabiaSearch, Severity.High, 0, xerror.Attributes["ShortText"].Value, "0");
            }
            else
            {
                XmlNode xsuccess = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AirBaggageDetailsRS/ns1:Success", nsmgr);
                XmlNodeList xBaggages = xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:AA_OTA_AirBaggageDetailsRS/ns1:BaggageDetailsResponses/ns1:OnDBaggageDetailsResponse", nsmgr);
                
                foreach (XmlNode node in xBaggages)
                {
                    XmlNode segmentNode = null;
                    foreach (XmlNode baggage in node.ChildNodes)
                    {                        
                        if (baggage.Name == "ns1:OnDFlightSegmentInfo")
                        {
                            segmentNode = baggage;
                        }
                        if (baggage.Name == "ns1:Baggage")
                        {
                            DataRow dr = dtBaggageDetails.NewRow();

                            if (exchangeRates.ContainsKey(baggage.SelectSingleNode("ns1:currencyCode", nsmgr).InnerText))
                            {
                                roe = exchangeRates[baggage.SelectSingleNode("ns1:currencyCode", nsmgr).InnerText];
                            }

                            dr["baggageCode"] = baggage.SelectSingleNode("ns1:baggageCode", nsmgr).InnerText;
                            dr["baggageDescription"] = baggage.SelectSingleNode("ns1:baggageDescription", nsmgr).InnerText;
                            dr["baggageCharge"] = Convert.ToDecimal(baggage.SelectSingleNode("ns1:baggageCharge", nsmgr).InnerText) * roe;
                            dr["currencyCode"] = agentCurrency; //baggage.SelectSingleNode("ns1:currencyCode", nsmgr).InnerText;

                            //dr["SegmentId"] = Convert.ToInt32(segmentNode.Attributes["RPH"].Value);// old one
                            string rphKey = Convert.ToString(segmentNode.Attributes["RPH"].Value);
                            rphKey = rphKey.Substring(rphKey.LastIndexOf('$', 10) + 1, rphKey.LastIndexOf('$', 13) - 3);
                            dr["SegmentId"] = Convert.ToInt32(rphKey);
                            dtBaggageDetails.Rows.Add(dr);
                        }
                    }
                }
            }

            return dtBaggageDetails;
        }

        public string[] GetFare(ref SearchResult result, SearchRequest request, ref SessionData data, ref string[] rph, ref string fareXml, string sessionId, ref CookieContainer cookie,bool addBundle)
        {
            string[] st = new string[0];
            int arrlen = rph != null ? rph.Length : 0;
            string requestPrice = GetPriceString(result, request, data, rph, addBundle);
            string response = string.Empty;
            try
            {
                string sOrigin = result.Flights[0].Select(x => x.FlightNumber + "_" + x.Origin.AirportCode).FirstOrDefault();
                string sDestination = result.Flights.Length > 1 ? result.Flights[1].Select(x => x.FlightNumber + "_" + x.Origin.AirportCode).Last() : result.Flights[0].Select(x => x.FlightNumber + "_" + x.Destination.AirportCode).Last();
                string fareTypes = string.Empty;
                
                result.Flights.ToList().ForEach(f => f.ToList().ForEach(s => fareTypes += string.IsNullOrEmpty(fareTypes) ? s.SegmentFareType : "_" + s.SegmentFareType));
                try
                {
                    //StreamWriter writer = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaPriceQTReq.xml");
                    //writer.Write(requestPrice);
                    //writer.Close();

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(requestPrice);
                    string filePath = xmlPath + "AirArabiaPriceQTRequest_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + fareTypes + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmssfff") + "_" + Convert.ToString(Guid.NewGuid()).Substring(0, 5) + ".xml";
                    doc.Save(filePath);
                }
                catch { }
                response = GetResponse(requestPrice, ref data.cookie);

                try
                {
                    //StreamWriter writer = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaPriceQTRes.xml");
                    //writer.Write(response);
                    //writer.Close();
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(response);
                    string filePath = xmlPath + "AirArabiaPriceQTResponse_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + fareTypes + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmssfff") + "_" + Convert.ToString(Guid.NewGuid()).Substring(0, 5) + ".xml";
                    doc.Save(filePath);

                }
                catch { }
                //Core.Audit.Add(Core.EventType.AirArabiaPriceRetrieve, Core.Severity.Normal, 0, "AirArabia GetPrice Response. Request: " + requestPrice + "| Response: " + response + " | Time: " + DateTime.Now, "");
            }
            catch (WebException excep)
            {
                Core.Audit.Add(Core.EventType.AirArabiaPriceRetrieve, Core.Severity.High, 0, "Web Exception returned from AirArabia in GetPrice. Error Message:" + excep.Message + " | " + DateTime.Now + "| request XML" + requestPrice, "");
                //Trace.TraceError("Error: " + excep.Message);
            }
            //Validate RPH & Flight Number
            TextReader stringRead = null;
            XmlDocument requestXmlDoc = new XmlDocument();
            stringRead = new StringReader(requestPrice);
            requestXmlDoc.Load(stringRead);

            XmlDocument responseXmlDoc = new XmlDocument();
            stringRead = new StringReader(response);
            responseXmlDoc.Load(stringRead);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(requestXmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

            XmlNode requestRPH = requestXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRQ/ns1:AirItinerary/ns1:OriginDestinationOptions", nsmgr);
            XmlNode xerror = responseXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS/ns1:Errors/ns1:Error", nsmgr);
            if (xerror != null && xerror.Attributes["ShortText"] != null)
            {
                Core.Audit.Add(Core.EventType.AirArabiaPriceRetrieve, Core.Severity.High, 0, "Exception returned from AirArabia. Error Message:" + xerror.Attributes["ShortText"].Value.ToString() + " | " + DateTime.Now + "| request XML" + response, "");
                result.ValidatingAirline= "AirArabia: Failed to Retrive Fare" + xerror.Attributes["ShortText"].Value.ToString();
                //throw new BookingEngineException("AirArabia: Failed to Retrive Fare" + xerror.Attributes["ShortText"].Value.ToString());
            }
            else
            {
                XmlNode responseRPH = responseXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS/ns1:PricedItineraries/ns1:PricedItinerary/ns1:AirItinerary/ns1:OriginDestinationOptions", nsmgr);
                XmlNodeList requestFlightNodes = requestRPH.SelectNodes("ns1:OriginDestinationOption/ns1:FlightSegment", nsmgr);
                XmlNodeList responseFlightNodes = responseRPH.SelectNodes("ns1:OriginDestinationOption/ns1:FlightSegment", nsmgr);
                if (requestFlightNodes != null && responseFlightNodes != null && ValidateResponse(requestFlightNodes, responseFlightNodes, nsmgr))
                {
                    st = ReadPrice(response, ref result, request, ref data, ref fareXml);
                }
            }
            return st;
        }

        /// <summary>
        /// Retrieves the update Itinerary fare
        /// </summary>
        /// <param name="result">SearchResult object reference</param>
        /// <param name="request">SearchRequest object</param>
        /// <param name="data">SessionData object</param>
        /// <param name="rph">Segment wise unique string</param>
        /// <param name="fareXml">Updated fare xml</param>
        /// <param name="sessionId">Storing the updated price xml</param>
        /// <param name="cookie">Retrieving updated price xml</param>
        /// <param name="paxBaggages">Segment wise pax wise Baggage code</param>
        /// <param name="stateCode">Lead Pax GST State Code</param>
        /// <param name="taxRegNo">Lead Pax GSTRegistration No</param>
        /// <param name="paxBagFares">Stores the udpated pax wise Baggage fares</param>
        /// <returns></returns>
        public string[] GetBaggageFare(ref SearchResult result, SearchRequest request, ref SessionData data, ref string[] rph, ref string fareXml, string sessionId, ref CookieContainer cookie, ref List<string> paxBaggages,string stateCode,string taxRegNo, ref Dictionary<string,decimal> paxBagFares)
        {
            string[] fares = new string[0];
            int arrayLen = rph.Length;
            
            string requestXML = GetBaggagePriceString(result, request, data, rph,paxBaggages, stateCode, taxRegNo);
            string sOrigin = result.Flights[0].Select(x => x.Origin.AirportCode).FirstOrDefault();
            string sDestination = result.Flights.Length == 1 ? result.Flights[0].Select(x => x.Destination.AirportCode).Last() :
                result.Flights[1].Select(x => x.Destination.AirportCode).Last();

            try
            {
                //StreamWriter writer = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaBaggagePriceReq.xml");
                //writer.Write(requestXML);
                //writer.Close();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(requestXML);
                string filePath = xmlPath + "AirArabiaBaggagePriceRequest_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
            }
            catch { }
            string responseXML = string.Empty;
            try
            {
                responseXML = GetResponse(requestXML, ref data.cookie);
                try
                {
                    //StreamWriter writer = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaBaggagePriceRes.xml");
                    //writer.Write(responseXML);
                    //writer.Close();
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(responseXML);
                    string filePath = xmlPath + "AirArabiaBaggagePriceResponse_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    doc.Save(filePath);
                }
                catch { }
                //Core.Audit.Add(Core.EventType.AirArabiaPriceRetrieve, Core.Severity.Normal, 0, "AirArabia GetBaggagePrice Response. Request: " + requestXML + "| Response: " + responseXML + " | Time: " + DateTime.Now, "");
            }
            catch (WebException ex)
            {
                Core.Audit.Add(Core.EventType.AirArabiaPriceRetrieve, Core.Severity.High, 0, "Web Exception returned from AirArabia in GetBaggagePrice. Error Message:" + ex.Message + " | " + DateTime.Now + "| request XML" + requestXML, "");
                //Trace.TraceError("Error: " + ex.Message);
                throw ex;
            }
            //Validate RPH & Flight Number
            TextReader stringRead = null;
            XmlDocument requestXmlDoc = new XmlDocument();
            stringRead = new StringReader(requestXML);
            requestXmlDoc.Load(stringRead);

            XmlDocument responseXmlDoc = new XmlDocument();
            stringRead = new StringReader(responseXML);
            responseXmlDoc.Load(stringRead);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(requestXmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

            XmlNode requestRPH = requestXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRQ/ns1:AirItinerary/ns1:OriginDestinationOptions", nsmgr);

            XmlNode xBookRS = responseXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS", nsmgr);

            XmlNode xErrors = xBookRS.SelectSingleNode("ns1:Errors", nsmgr);
            XmlNodeList xError = xErrors.SelectNodes("ns1:Error", nsmgr);
            if (xError != null && xError.Count > 0)
            {

                foreach (XmlNode xErr in xError)
                {
                    Core.Audit.Add(Core.EventType.AirArabiaPriceRetrieve, Core.Severity.High, 0, "AirArabia Booking Failed.Error " + xErr.Attributes["ShortText"].Value.ToString(), "");
                    throw new BookingEngineException("AirArabia baggage Failed :" + xErr.Attributes["ShortText"].Value.ToString());
                }
            }
            else
            {

                XmlNode responseRPH = responseXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS/ns1:PricedItineraries/ns1:PricedItinerary/ns1:AirItinerary/ns1:OriginDestinationOptions", nsmgr);

                XmlNodeList requestFlightNodes = requestRPH.SelectNodes("ns1:OriginDestinationOption/ns1:FlightSegment", nsmgr);
                XmlNodeList responseFlightNodes = responseRPH.SelectNodes("ns1:OriginDestinationOption/ns1:FlightSegment", nsmgr);
                if (requestFlightNodes != null && responseFlightNodes != null && ValidateResponse(requestFlightNodes, responseFlightNodes, nsmgr))
                {
                    fares = ReadBaggagePrice(responseXML, ref result, request, ref data, ref fareXml, ref paxBagFares);
                }
            }
            return fares;
        }

        private string[] ReadPrice(string responseStr, ref SearchResult result, SearchRequest request, ref SessionData data, ref string fareXml)//reading for price if not given
        {
            string[] sessionRPH = new string[0];
            XmlDocument xmlDoc = new XmlDocument();
            TextReader stringRead = new StringReader(responseStr);
            xmlDoc.Load(stringRead);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

            //In case of error assign ZERO price farebreakdown to avoid error in MSE
            int paxType = 0;
            if (request.AdultCount > 0)
            {
                paxType++;
            }
            if (request.ChildCount > 0)
            {
                paxType++;
            }
            if (request.SeniorCount > 0)
            {
                paxType++;
            }
            if (request.InfantCount > 0)
            {
                paxType++;
            }

            //Assign dummy fare only if it is not having earlier fare (avoid ZERO price update in reprice)
            if (result.FareBreakdown == null)
            {
                result.FareBreakdown = new Fare[paxType];
                for (int i = 0; i < result.FareBreakdown.Length; i++)
                {
                    result.FareBreakdown[i] = new Fare();
                    result.FareBreakdown[i].FareType = string.Empty;
                    if (i == 0)
                        result.FareBreakdown[i].PassengerType = PassengerType.Adult;
                    else if (i == 1)
                        result.FareBreakdown[i].PassengerType = PassengerType.Child;
                    else
                        result.FareBreakdown[i].PassengerType = PassengerType.Infant;
                }
            }

            XmlNode xAvailRS = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS", nsmgr);
            XmlNode xErrors = xAvailRS.SelectSingleNode("ns1:Errors", nsmgr);
            bool isError = false;
            if (xErrors != null)
            {
                XmlNodeList xError = xErrors.SelectNodes("ns1:Error", nsmgr);
                foreach (XmlNode xErr in xError)
                {
                    //Trace.TraceError("Error due to " + xErr.Attributes["ShortText"].Value.ToString());
                    Core.Audit.Add(Core.EventType.AirArabiaPriceRetrieve, Core.Severity.High, 0, "AirArabia: Get Tax Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString() + "Response :- " + responseStr, "");
                    //throw new BookingEngineException("AirArabia: Get Tax Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString());
                    result.RepriceErrorMessage = "AirArabia: Get Tax Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString();
                    isError = true;
                }
            }
            if(!isError)//Ignore reading fares in case of any error
            {
                XmlNodeList xPriceSegments = xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS/ns1:PricedItineraries/ns1:PricedItinerary/ns1:OriginDestinationOptions/ns1:OriginDestinationOption", nsmgr);

                sessionRPH = new string[xPriceSegments.Count];
                int s = 0;
                foreach (XmlNode segment in xPriceSegments)
                {
                    XmlNode flightSegment = segment.SelectSingleNode("ns1:FlightSegment", nsmgr);
                    sessionRPH[s] = (string)flightSegment.Attributes["RPH"].Value.ToString();
                }

                XmlNode xPrice = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS/ns1:PricedItineraries/ns1:PricedItinerary", nsmgr);
                fareXml = xPrice.InnerXml.ToString();
                XmlNode ItinTotalFareInfo = xPrice.SelectSingleNode("ns1:AirItineraryPricingInfo/ns1:ItinTotalFare", nsmgr);
                XmlElement ItinTotalFareElement = (XmlElement)ItinTotalFareInfo;
                //StaticData staticDta = new StaticData();
                //staticDta.Load("AED");//Loading rate of Exchange of United Arab
                //double roe = Convert.ToDouble(ConfigurationSystem.AirArabiaConfig["ROE"]);
                result.Currency = agentCurrency;
                //result.Price = new PriceAccounts();
                result.Price.SupplierCurrency = ItinTotalFareElement["ns1:TotalFare"].Attributes["CurrencyCode"].Value;
                if (exchangeRates.ContainsKey(ItinTotalFareElement["ns1:TotalFare"].Attributes["CurrencyCode"].Value))
                {
                    roe = exchangeRates[ItinTotalFareElement["ns1:TotalFare"].Attributes["CurrencyCode"].Value];
                }
                result.BaseFare = Convert.ToDouble(ItinTotalFareElement["ns1:BaseFare"].GetAttribute("Amount")) * (double)roe;
                result.TotalFare = Convert.ToDouble(ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount")) * (double)roe;
                result.Price.SupplierPrice = Convert.ToDecimal(ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount"));
                string key = BuildResultKey(result);
                if (data.totalPrice.ContainsKey(key))
                {
                    data.totalPrice[key] = ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount").ToString();
                }
                else
                {
                    data.totalPrice.Add(key, ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount").ToString());
                }
                result.Tax = result.TotalFare - result.BaseFare;
                XmlNodeList priceDetail = xPrice.SelectNodes("ns1:AirItineraryPricingInfo/ns1:PTC_FareBreakdowns/ns1:PTC_FareBreakdown", nsmgr);

                
                int f = 0;
                //Added by lokesh on 4-Apr-2018 to resolve multiple pax issue in farebreakdown.
                bool priceCalculatedForAdult = false;//Variable which holds whether the price for adult is calculated or not
                bool priceCalculatedForChild = false;//Variable which holds whether the price for child is calculated or not
                bool priceCalculatedForInfant = false;//Variable which holds whether the price for infant is calculated or not
                foreach (XmlNode paxtype in priceDetail)
                {
                    XmlNode xPassengerFare = paxtype.SelectSingleNode("ns1:PassengerFare", nsmgr);
                    XmlNode xPassengerType = paxtype.SelectSingleNode("ns1:PassengerTypeQuantity", nsmgr);

                    if ((request.AdultCount != 0 || request.SeniorCount != 0) && xPassengerType.Attributes["Code"].Value.ToString() == "ADT")
                    {

                        if (request.AdultCount != 0 && !priceCalculatedForAdult)//for adult
                        {
                            result.FareBreakdown[f] = new Fare();
                            result.FareBreakdown[f].PassengerType = PassengerType.Adult;
                            result.FareBreakdown[f].PassengerCount = request.AdultCount;

                            XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                            result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                            XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                            result.FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                            result.FareBreakdown[f].SupplierFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount;
                            priceCalculatedForAdult = true;
                            f++;
                        }
                        if (request.SeniorCount != 0)
                        {
                            result.FareBreakdown[f] = new Fare();
                            result.FareBreakdown[f].PassengerType = PassengerType.Adult;
                            result.FareBreakdown[f].PassengerCount = request.AdultCount;

                            XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                            result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                            XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                            result.FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                            result.FareBreakdown[f].SupplierFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount;
                            f++;
                        }
                    }
                    else if (request.ChildCount != 0 && xPassengerType.Attributes["Code"].Value.ToString() == "CHD" && !priceCalculatedForChild)
                    {
                        result.FareBreakdown[f] = new Fare();
                        result.FareBreakdown[f].PassengerType = PassengerType.Child;
                        result.FareBreakdown[f].PassengerCount = request.ChildCount;

                        XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                        result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                        XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                        result.FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                        result.FareBreakdown[f].SupplierFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount;
                        priceCalculatedForChild = true;
                        f++;
                    }
                    else if (request.InfantCount != 0 && xPassengerType.Attributes["Code"].Value.ToString() == "INF" && !priceCalculatedForInfant)
                    {
                        result.FareBreakdown[f] = new Fare();
                        result.FareBreakdown[f].PassengerType = PassengerType.Infant;
                        result.FareBreakdown[f].PassengerCount = request.InfantCount;

                        XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                        result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                        XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                        result.FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                        //result.FareBreakdown[f].Tax = Convert.ToDouble(result.FareBreakdown[f].TotalFare - result.FareBreakdown[f].BaseFare);
                        result.FareBreakdown[f].SupplierFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount;
                        priceCalculatedForInfant = true;
                        f++;
                    }
                }
            }
            return sessionRPH;
        }

        /// <summary>
        /// Reads and updates revised Itinerary price.
        /// </summary>
        /// <param name="responseStr"></param>
        /// <param name="result"></param>
        /// <param name="request"></param>
        /// <param name="data">SessionData object</param>
        /// <param name="fareXml">Updated fare xml to be returned for storing session</param>
        /// <param name="paxBagFares">Passenger wise baggage fares</param>
        /// <returns></returns>
        private string[] ReadBaggagePrice(string responseStr, ref SearchResult result, SearchRequest request, ref SessionData data, ref string fareXml, ref Dictionary<string, decimal> paxBagFares)//reading for price if not given
        {
            XmlDocument xmlDoc = new XmlDocument();
            TextReader stringRead = new StringReader(responseStr);
            xmlDoc.Load(stringRead);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

            XmlNode xAvailRS = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS", nsmgr);
            XmlNode xErrors = xAvailRS.SelectSingleNode("ns1:Errors", nsmgr);
            if (xErrors != null)
            {
                XmlNodeList xError = xErrors.SelectNodes("ns1:Error", nsmgr);
                foreach (XmlNode xErr in xError)
                {
                    //Trace.TraceError("Error due to " + xErr.Attributes["ShortText"].Value.ToString());
                    Core.Audit.Add(Core.EventType.AirArabiaPriceRetrieve, Core.Severity.High, 0, "AirArabia: Get Tax Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString() + "Response :- " + responseStr, "");
                    throw new BookingEngineException("AirArabia: Get Tax Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString());
                }
            }
            XmlNodeList xPriceSegments = xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS/ns1:PricedItineraries/ns1:PricedItinerary/ns1:AirItinerary/ns1:OriginDestinationOptions/ns1:OriginDestinationOption", nsmgr);

            string[] sessionRPH = new string[xPriceSegments.Count];
            int s = 0;
            foreach (XmlNode segment in xPriceSegments)
            {

                XmlNode flightSegment = segment.SelectSingleNode("ns1:FlightSegment", nsmgr);
                sessionRPH[s] = (string)flightSegment.Attributes["RPH"].Value.ToString();
                s++;
            }

            XmlNode xPrice = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS/ns1:PricedItineraries/ns1:PricedItinerary", nsmgr);
            fareXml = xPrice.InnerXml.ToString();
            //Updating The fare xml of selected Result.
            if (data.SelectedResultXml != null && data.SelectedResultXml.ContainsKey(result.ResultKey))
                data.SelectedResultXml[result.ResultKey] = xPrice.InnerXml.ToString();
            XmlNode ItinTotalFareInfo = xPrice.SelectSingleNode("ns1:AirItineraryPricingInfo/ns1:ItinTotalFare", nsmgr);
            XmlElement ItinTotalFareElement = (XmlElement)ItinTotalFareInfo;
            //StaticData staticDta = new StaticData();
            //staticDta.Load("AED");//Loading rate of Exchange of United Arab
            //double roe = Convert.ToDouble(ConfigurationSystem.AirArabiaConfig["ROE"]);
            if (exchangeRates.ContainsKey(ItinTotalFareElement["ns1:TotalFare"].Attributes["CurrencyCode"].Value))
            {
                roe = exchangeRates[ItinTotalFareElement["ns1:TotalFare"].Attributes["CurrencyCode"].Value];
            }
            result.BaseFare = Convert.ToDouble(ItinTotalFareElement["ns1:BaseFare"].GetAttribute("Amount")) * (double)roe;
            result.TotalFare = Convert.ToDouble(ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount")) * (double)roe;
            string key = BuildResultKey(result);
            data.totalPrice[key] = ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount").ToString();
            data.SelectedTotalPrice = ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount").ToString();
            result.Tax = result.TotalFare - result.BaseFare;
            XmlNodeList priceDetail = xPrice.SelectNodes("ns1:AirItineraryPricingInfo/ns1:PTC_FareBreakdowns/ns1:PTC_FareBreakdown", nsmgr);


            // changed for baggage chnages
            int paxType = 0;
            if (request.AdultCount > 0)
            {
                paxType++;
            }
            if (request.AdultCount > 1)
            {                
                paxType = request.AdultCount;
            }
            
            if (request.ChildCount > 1)
            {
                paxType += request.ChildCount;
            }
            else if (request.ChildCount > 0)
            {
                paxType++;
            }
            if (request.SeniorCount > 1)
            {
                paxType += request.SeniorCount;
            }
            else if (request.SeniorCount > 0)
            {
                paxType++;
            }
            if (request.InfantCount > 1)
            {
                paxType += request.InfantCount;
            }
            else if (request.InfantCount > 0)
            {
                paxType++;
            }

            result.FareBreakdown = new Fare[paxType];
            int f = 0;
            double allPaxBaggageCharge = 0;
            foreach (XmlNode paxtype in priceDetail)
            {
                XmlNode xPassengerFare = paxtype.SelectSingleNode("ns1:PassengerFare", nsmgr);
                XmlNode xPassengerType = paxtype.SelectSingleNode("ns1:PassengerTypeQuantity", nsmgr);
                XmlNode xTravellerRef = paxtype.SelectSingleNode("ns1:TravelerRefNumber", nsmgr);//Read the TravellerRef node for updating pax wise baggage fares
                if ((request.AdultCount != 0 || request.SeniorCount != 0) && xPassengerType.Attributes["Code"].Value.ToString() == "ADT")
                {
                    if (request.AdultCount != 0)//for adult
                    {
                        result.FareBreakdown[f] = new Fare();
                        result.FareBreakdown[f].PassengerType = PassengerType.Adult;
                        //result.FareBreakdown[f].PassengerCount = request.AdultCount; for new g9 ticket fare details
                        result.FareBreakdown[f].PassengerCount = 1;
                        //Added by Lokesh on 5-April-2018
                        //In the total fare component we need to exclude the baggage charge
                        
                        double bagggageFare = 0, mealCharge = 0,seatCharge = 0;
                        XmlNodeList feesNodeList = xPassengerFare.SelectNodes("ns1:Fees/ns1:Fee", nsmgr);
                        if (feesNodeList != null && feesNodeList.Count > 0)
                        {
                            foreach (XmlNode baggageNode in feesNodeList)
                            {
                                if (Convert.ToString(baggageNode.Attributes["FeeCode"].Value) == "BG/Baggage Selection")
                                {
                                    bagggageFare += Convert.ToDouble(baggageNode.Attributes["Amount"].Value);
                                    allPaxBaggageCharge += bagggageFare;
                                    paxBagFares.Add("BAG~" + xTravellerRef.Attributes["RPH"].Value, (decimal)(bagggageFare) * roe);
                                }
                                else if (Convert.ToString(baggageNode.Attributes["FeeCode"].Value) == "ML/Meal Selection")
                                {
                                    mealCharge += Convert.ToDouble(baggageNode.Attributes["Amount"].Value);
                                    allPaxBaggageCharge += mealCharge;
                                    paxBagFares.Add("MEAL~" + xTravellerRef.Attributes["RPH"].Value, (decimal)(mealCharge) * roe);
                                }
                                else if (Convert.ToString(baggageNode.Attributes["FeeCode"].Value) == "SM/Seat Selection")
                                {
                                    seatCharge += Convert.ToDouble(baggageNode.Attributes["Amount"].Value);
                                    allPaxBaggageCharge += seatCharge;
                                    paxBagFares.Add("SEAT~" + xTravellerRef.Attributes["RPH"].Value, (decimal)(seatCharge) * roe);
                                }
                            }
        
                        }
                        XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                        result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                        XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                        result.FareBreakdown[f].TotalFare = (Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) - (bagggageFare + mealCharge + seatCharge)) * result.FareBreakdown[f].PassengerCount * (double)roe;
                        result.FareBreakdown[f].SupplierFare = (Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) - (bagggageFare + mealCharge + seatCharge)) * result.FareBreakdown[f].PassengerCount;
                        f++;
                    }
			// commented for  baggage changes
                    //if (request.SeniorCount != 0)
                    //{
                    //    result.FareBreakdown[f] = new Fare();
                    //    result.FareBreakdown[f].PassengerType = PassengerType.Adult;
                    //    result.FareBreakdown[f].PassengerCount = request.AdultCount;

                    //    XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                    //    result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * roe;
                    //    XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                    //    result.FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * roe;
                    //    f++;
                    //}
                }
                else if (request.ChildCount != 0 && xPassengerType.Attributes["Code"].Value.ToString() == "CHD")
                {
                    result.FareBreakdown[f] = new Fare();
                    result.FareBreakdown[f].PassengerType = PassengerType.Child;
                    //result.FareBreakdown[f].PassengerCount = request.ChildCount;
                    result.FareBreakdown[f].PassengerCount = 1;
                    //Added by Lokesh on 5-April-2018
                    //In the total fare component we need to exclude the baggage charge
                    double bagggageFare = 0, mealCharge = 0, seatCharge = 0;
                    XmlNodeList feesNodeList = xPassengerFare.SelectNodes("ns1:Fees/ns1:Fee", nsmgr);
                    if (feesNodeList != null && feesNodeList.Count > 0)
                    {
                        foreach (XmlNode baggageNode in feesNodeList)
                        {
                            if (Convert.ToString(baggageNode.Attributes["FeeCode"].Value) == "BG/Baggage Selection")
                            {
                                bagggageFare += Convert.ToDouble(baggageNode.Attributes["Amount"].Value);
                                allPaxBaggageCharge += bagggageFare;
                                paxBagFares.Add("BAG~" + xTravellerRef.Attributes["RPH"].Value, (decimal)(bagggageFare) * roe);
                            }
                            else if (Convert.ToString(baggageNode.Attributes["FeeCode"].Value) == "ML/Meal Selection")
                            {
                                mealCharge += Convert.ToDouble(baggageNode.Attributes["Amount"].Value);
                                allPaxBaggageCharge += mealCharge;
                                paxBagFares.Add("MEAL~" + xTravellerRef.Attributes["RPH"].Value, (decimal)(mealCharge) * roe);
                            }
                            else if (Convert.ToString(baggageNode.Attributes["FeeCode"].Value) == "SM/Seat Selection")
                            {
                                seatCharge += Convert.ToDouble(baggageNode.Attributes["Amount"].Value);
                                allPaxBaggageCharge += seatCharge;
                                paxBagFares.Add("SEAT~" + xTravellerRef.Attributes["RPH"].Value, (decimal)(seatCharge) * roe);
                            }
                        }
                        
                    }

                    XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                    result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                    XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                    result.FareBreakdown[f].TotalFare = (Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) - (bagggageFare + mealCharge + seatCharge)) * result.FareBreakdown[f].PassengerCount * (double)roe;
                    result.FareBreakdown[f].SupplierFare = (Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) - (bagggageFare + mealCharge + seatCharge)) * result.FareBreakdown[f].PassengerCount;
                    
                    f++;
                }
                else if (request.InfantCount != 0 && xPassengerType.Attributes["Code"].Value.ToString() == "INF")
                {
                    result.FareBreakdown[f] = new Fare();
                    result.FareBreakdown[f].PassengerType = PassengerType.Infant;
                    //result.FareBreakdown[f].PassengerCount = request.InfantCount;
                    result.FareBreakdown[f].PassengerCount = 1;

                    //Added by Lokesh on 5-April-2018
                    //In the total fare component we need to exclude the baggage charge
                    double bagggageFare = 0, mealCharge = 0, seatCharge = 0;
                    XmlNodeList feesNodeList = xPassengerFare.SelectNodes("ns1:Fees/ns1:Fee", nsmgr);
                    if (feesNodeList != null && feesNodeList.Count > 0)
                    {
                        foreach (XmlNode baggageNode in feesNodeList)
                        {
                            if (Convert.ToString(baggageNode.Attributes["FeeCode"].Value) == "BG/Baggage Selection")
                            {
                                bagggageFare += Convert.ToDouble(baggageNode.Attributes["Amount"].Value);
                                allPaxBaggageCharge += bagggageFare;
                            }
                            else if (Convert.ToString(baggageNode.Attributes["FeeCode"].Value) == "ML/Meal Selection")
                            {
                                mealCharge += Convert.ToDouble(baggageNode.Attributes["Amount"].Value);
                                allPaxBaggageCharge += mealCharge;
                            }
                            else if (Convert.ToString(baggageNode.Attributes["FeeCode"].Value) == "SM/Seat Selection")
                            {
                                seatCharge += Convert.ToDouble(baggageNode.Attributes["Amount"].Value);
                                allPaxBaggageCharge += seatCharge;
                            }
                        }
                        //Add Pax wise baggage fare for total journey
                        paxBagFares.Add(xTravellerRef.Attributes["RPH"].Value, (decimal)bagggageFare*roe);
                    }

                    XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                    result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                    XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                    result.FareBreakdown[f].TotalFare = (Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) - (bagggageFare + mealCharge + seatCharge)) * result.FareBreakdown[f].PassengerCount * (double)roe;
                    //result.FareBreakdown[f].Tax = Convert.ToDouble(result.FareBreakdown[f].TotalFare - result.FareBreakdown[f].BaseFare);
                    result.FareBreakdown[f].SupplierFare = (Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) - (bagggageFare + mealCharge + seatCharge)) * result.FareBreakdown[f].PassengerCount;
                  
                    f++;
                }
            }
            result.Tax = result.Tax - (allPaxBaggageCharge * (double)roe);
            result.TotalFare = result.TotalFare - (allPaxBaggageCharge * (double)roe);
            return sessionRPH;
        }

        private string GetPriceString(SearchResult result, SearchRequest request, SessionData data, string[] rph, bool addBundle)//getting price string if not available
        {
            //Creating Xml For price here using result object
            StringBuilder priceReq = new StringBuilder();

            XmlWriter writer = XmlTextWriter.Create(priceReq);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");
            //getting commom header

            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-26506823");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns1", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns1", "OTA_AirPriceRQ", null);
            writer.WriteAttributeString("EchoToken", "11810113621255-577396696");
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
            //writer.WriteAttributeString("Version", "20061.00");
            writer.WriteAttributeString("Version", result.TicketAdvisory);//TODO: Stores result Version Number. Create another prop
            writer.WriteAttributeString("TransactionIdentifier", data.transactionID);
            #region POSDetailFix
            writer.WriteStartElement("ns1", "POS", null);
            writer.WriteStartElement("ns1", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
            writer.WriteStartElement("ns1", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", userName);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("ns1", "AirItinerary", null);
            if (result.Flights.Length > 1)
            {
                writer.WriteAttributeString("DirectionInd", "Return");
            }
            else
            {
                writer.WriteAttributeString("DirectionInd", "OneWay");
            }
            writer.WriteStartElement("ns1", "OriginDestinationOptions", null);
            int countRPH = 0;
            for (int f = 0; f < result.Flights.Length; f++)
            {
                writer.WriteStartElement("ns1", "OriginDestinationOption", null);
                if (result.Flights[f] != null)
                {
                    for (int r = 0; r < result.Flights[f].Length; r++)
                    {
                        writer.WriteStartElement("ns1", "FlightSegment", null);
                        writer.WriteAttributeString("ArrivalDateTime", result.Flights[f][r].ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                        writer.WriteAttributeString("DepartureDateTime", result.Flights[f][r].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                        writer.WriteAttributeString("FlightNumber", result.Flights[f][r].Airline + result.Flights[f][r].FlightNumber);
                        //writer.WriteAttributeString("RPH", rph[countRPH]);
                        writer.WriteAttributeString("RPH", result.Flights[f][r].UapiDepartureTime);
                        writer.WriteStartElement("ns1", "DepartureAirport", null);
                        writer.WriteAttributeString("LocationCode", result.Flights[f][r].Origin.AirportCode.ToString());
                        writer.WriteAttributeString("Terminal", result.Flights[f][r].DepTerminal.ToString());
                        writer.WriteEndElement();
                        writer.WriteStartElement("ns1", "ArrivalAirport", null);
                        writer.WriteAttributeString("LocationCode", result.Flights[f][r].Destination.AirportCode.ToString());
                        writer.WriteAttributeString("Terminal", result.Flights[f][r].ArrTerminal.ToString());
                        writer.WriteEndElement();
                        writer.WriteEndElement();
                        countRPH++;
                    }
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "TravelerInfoSummary", null);
            writer.WriteStartElement("ns1", "AirTravelerAvail", null);
            int count = request.AdultCount + request.SeniorCount;
            if (count != 0)
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "ADT");
                writer.WriteAttributeString("Quantity", count.ToString());
                writer.WriteEndElement();
            }
            if (request.ChildCount != 0)
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "CHD");
                writer.WriteAttributeString("Quantity", request.ChildCount.ToString());
                writer.WriteEndElement();
            }
            if (request.InfantCount != 0 && request.AdultCount + request.SeniorCount >= request.InfantCount)
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "INF");
                writer.WriteAttributeString("Quantity", request.InfantCount.ToString());
                writer.WriteEndElement();
            }
            if (request.AdultCount + request.SeniorCount < request.InfantCount)
            {
                throw new BookingEngineException("AirArabia Search Infant Count Can't greater than Adult Count");
            }
            writer.WriteEndElement();
            writer.WriteEndElement();

            writer.WriteStartElement("ns1", "BundledServiceSelectionOptions", null);
            if (result.Flights[0][0].ServiceBundleId > 0 && addBundle)
            {
                writer.WriteStartElement("ns1", "OutBoundBunldedServiceId", null);
                writer.WriteValue(result.Flights[0][0].ServiceBundleId);
                writer.WriteEndElement();
            }
            if (result.Flights.Length > 1 && result.Flights[1][0].ServiceBundleId > 0  && addBundle)
            {
                writer.WriteStartElement("ns1", "InBoundBunldedServiceId", null);
                writer.WriteValue(result.Flights[1][0].ServiceBundleId);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();

            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
            return priceReq.ToString();
        }

        private string RequestSSRPrice(ref FlightItinerary clsFlightItinerary, SessionData data)
        {
            string response = string.Empty;
            try
            {
                #region Prepare price req xml
                StringBuilder priceReq = new StringBuilder();
                string sOrigin = clsFlightItinerary.Segments.Select(x => x.Origin.AirportCode).FirstOrDefault();
                string sDestination = clsFlightItinerary.Segments.Select(x => x.Destination.AirportCode).Last();

                XmlWriter writer = XmlTextWriter.Create(priceReq);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                #region Header
                writer.WriteStartElement("soap", "Header", null);
                writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
                writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
                writer.WriteStartElement("wsse", "UsernameToken", null);
                writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
                writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-26506823");
                writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
                writer.WriteStartElement("wsse", "Username", null);
                writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
                writer.WriteValue(userName);
                writer.WriteEndElement();
                writer.WriteStartElement("wsse", "Password", null);
                writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
                writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
                writer.WriteValue(password);
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                #endregion

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteAttributeString("xmlns", "ns1", null, "http://www.opentravel.org/OTA/2003/05");
                writer.WriteStartElement("ns1", "OTA_AirPriceRQ", null);
                writer.WriteAttributeString("EchoToken", "11810113621255-577396696");
                writer.WriteAttributeString("PrimaryLangID", "en-us");
                writer.WriteAttributeString("SequenceNmbr", "1");
                writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                writer.WriteAttributeString("Version", "2006.01");
                writer.WriteAttributeString("TransactionIdentifier", data.transactionID);
                #region POSDetailFix
                writer.WriteStartElement("ns1", "POS", null);
                writer.WriteStartElement("ns1", "Source", null);
                writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
                writer.WriteStartElement("ns1", "RequestorID", null);
                writer.WriteAttributeString("Type", "4");
                writer.WriteAttributeString("ID", userName);
                writer.WriteEndElement();
                writer.WriteStartElement("ns1", "BookingChannel", null);
                writer.WriteAttributeString("Type", "12");
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                #endregion

                writer.WriteStartElement("ns1", "AirItinerary", null);
                writer.WriteAttributeString("DirectionInd", sOrigin == sDestination ? "Return" : "OneWay");
                writer.WriteStartElement("ns1", "OriginDestinationOptions", null);

                List<string> liSegmntInfo = new List<string>();

                if (clsFlightItinerary.Segments.Where(x => x.Group == 0).Count() > 0)
                {

                    writer.WriteStartElement("ns1", "OriginDestinationOption", null);
                    clsFlightItinerary.Segments.Where(x => x.Group == 0).ToList().ForEach(y =>
                    {

                        writer.WriteStartElement("ns1", "FlightSegment", null);
                        writer.WriteAttributeString("ArrivalDateTime", y.ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                        writer.WriteAttributeString("DepartureDateTime", y.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                        writer.WriteAttributeString("FlightNumber", y.Airline + y.FlightNumber);
                        writer.WriteAttributeString("RPH", y.UapiDepartureTime );
                        writer.WriteStartElement("ns1", "DepartureAirport", null);
                        writer.WriteAttributeString("LocationCode", y.Origin.AirportCode);
                        writer.WriteAttributeString("Terminal", y.DepTerminal);
                        writer.WriteEndElement();
                        writer.WriteStartElement("ns1", "ArrivalAirport", null);
                        writer.WriteAttributeString("LocationCode", y.Destination.AirportCode);
                        writer.WriteAttributeString("Terminal", y.ArrTerminal);
                        writer.WriteEndElement();
                        writer.WriteEndElement();
                        liSegmntInfo.Add(y.Origin.AirportCode + "-" + y.Destination.AirportCode + "|" + y.UapiDepartureTime);
                    });
                    writer.WriteEndElement();
                }

                if (clsFlightItinerary.Segments.Where(x => x.Group == 1).Count() > 0)
                {
                    writer.WriteStartElement("ns1", "OriginDestinationOption", null);

                    clsFlightItinerary.Segments.Where(x => x.Group == 1).ToList().ForEach(y =>
                    {
                        writer.WriteStartElement("ns1", "FlightSegment", null);
                        writer.WriteAttributeString("ArrivalDateTime", y.ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                        writer.WriteAttributeString("DepartureDateTime", y.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                        writer.WriteAttributeString("FlightNumber", y.Airline + y.FlightNumber);
                        writer.WriteAttributeString("RPH", y.UapiDepartureTime );
                        writer.WriteStartElement("ns1", "DepartureAirport", null);
                        writer.WriteAttributeString("LocationCode", y.Origin.AirportCode);
                        writer.WriteAttributeString("Terminal", y.DepTerminal);
                        writer.WriteEndElement();
                        writer.WriteStartElement("ns1", "ArrivalAirport", null);
                        writer.WriteAttributeString("LocationCode", y.Destination.AirportCode);
                        writer.WriteAttributeString("Terminal", y.ArrTerminal);
                        writer.WriteEndElement();
                        writer.WriteEndElement();
                        liSegmntInfo.Add(y.Origin.AirportCode + "-" + y.Destination.AirportCode + "|" + y.UapiDepartureTime);
                    });
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteStartElement("ns1", "TravelerInfoSummary", null);
                writer.WriteStartElement("ns1", "AirTravelerAvail", null);

                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "ADT");
                writer.WriteAttributeString("Quantity", clsFlightItinerary.Passenger.Where(x => x.Type == PassengerType.Adult).Count().ToString());
                writer.WriteEndElement();

                if (clsFlightItinerary.Passenger.Where(x => x.Type == PassengerType.Child).Count() > 0)
                {
                    writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                    writer.WriteAttributeString("Code", "CHD");
                    writer.WriteAttributeString("Quantity", clsFlightItinerary.Passenger.Where(x => x.Type == PassengerType.Child).Count().ToString());
                    writer.WriteEndElement();
                }

                if (clsFlightItinerary.Passenger.Where(x => x.Type == PassengerType.Infant).Count() > 0)
                {
                    writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                    writer.WriteAttributeString("Code", "INF");
                    writer.WriteAttributeString("Quantity", clsFlightItinerary.Passenger.Where(x => x.Type == PassengerType.Infant).Count().ToString());
                    writer.WriteEndElement();
                }

                if (clsFlightItinerary.Passenger.Where(x => x.Type == PassengerType.Adult).Count() < clsFlightItinerary.Passenger.Where(x => x.Type == PassengerType.Infant).Count())
                    throw new BookingEngineException("AirArabia Search Infant Count Can't greater than Adult Count");
                writer.WriteEndElement();

                var SSRinfo = clsFlightItinerary.Passenger.Where(x => x.Type != PassengerType.Infant && x.liPaxSeatInfo != null && !string.IsNullOrEmpty(x.SeatInfo)).ToList();

                if (SSRinfo.Count() > 0)
                {
                    //Include Seat Details
                    writer.WriteStartElement("ns1", "SpecialReqDetails", null);
                    writer.WriteStartElement("ns1", "SeatRequests", null);

                    clsFlightItinerary.Passenger.Where(x => x.Type != PassengerType.Infant && x.liPaxSeatInfo != null && !string.IsNullOrEmpty(x.SeatInfo)).ToList().ForEach(y =>
                    {

                        y.liPaxSeatInfo.Where(v => v.SeatStatus == "A").ToList().ForEach(s =>
                        {
                            string flightref = liSegmntInfo.Where(j => j.Split('|')[0] == s.Segment).Select(f => f.Split('|')[1]).FirstOrDefault();
                            writer.WriteStartElement("ns1", "SeatRequest", null);
                            writer.WriteAttributeString("SeatNumber", s.SeatNo);
                            writer.WriteAttributeString("TravelerRefNumberRPHList", (y.Type == PassengerType.Adult ? "A" : "C") + Convert.ToString(s.PaxNo + 1));
                            writer.WriteAttributeString("FlightRefNumberRPHList", flightref);
                            writer.WriteEndElement();
                        });
                    });

                    writer.WriteEndElement();//SeatRequests
                }

                var paxinfo = clsFlightItinerary.Passenger.Where(x => x.Type != PassengerType.Infant && !string.IsNullOrEmpty(x.BaggageType)).ToList();

                if (paxinfo.Count() > 0)
                {
                    if(SSRinfo.Count()==0)
                        writer.WriteStartElement("ns1", "SpecialReqDetails", null);

                    //Include Baggage Details
                    writer.WriteStartElement("ns1", "BaggageRequests", null);
                    for (int i = 0; i < paxinfo.Count(); i++)
                    {
                        clsFlightItinerary.Segments.Where(y => y.Group == 0).ToList().ForEach(x =>
                        {
                            if (paxinfo[i].BaggageType.Split(',')[0].Trim().ToLower() != "no bag")
                            {
                                
                                    writer.WriteStartElement("ns1", "BaggageRequest", null);
                                    writer.WriteAttributeString("baggageCode", paxinfo[i].BaggageType.Split(',')[0].Trim());
                                    writer.WriteAttributeString("TravelerRefNumberRPHList", (paxinfo[i].Type == PassengerType.Adult ? "A" : "C") + Convert.ToString(i + 1));
                                    writer.WriteAttributeString("FlightRefNumberRPHList", x.UapiDepartureTime);
                                    writer.WriteAttributeString("DepartureDate", x.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                                    writer.WriteAttributeString("FlightNumber", x.Airline + x.FlightNumber);
                                    writer.WriteEndElement();
                                
                                
                            }
                        });

                        if (clsFlightItinerary.Segments.Where(y => y.Group == 0).Count() == 0 && clsFlightItinerary.Segments.Where(y => y.Group == 1).Count() > 0)
                        {
                            clsFlightItinerary.Segments.Where(y => y.Group == 1).ToList().ForEach(x =>
                            {
                                if (paxinfo[i].BaggageType.Split(',')[0].Trim().ToLower() != "no bag")
                                {
                                    
                                        writer.WriteStartElement("ns1", "BaggageRequest", null);
                                        writer.WriteAttributeString("baggageCode", paxinfo[i].BaggageType.Split(',')[0].Trim());
                                        writer.WriteAttributeString("TravelerRefNumberRPHList", (paxinfo[i].Type == PassengerType.Adult ? "A" : "C") + Convert.ToString(i + 1));
                                        writer.WriteAttributeString("FlightRefNumberRPHList", x.UapiDepartureTime);
                                        writer.WriteAttributeString("DepartureDate", x.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                                        writer.WriteAttributeString("FlightNumber", x.Airline + x.FlightNumber);
                                        writer.WriteEndElement();
                                    
                                    
                                }
                            });
                        }

                        if (paxinfo[i].BaggageType.Split(',').Length > 1 && !string.IsNullOrEmpty(paxinfo[i].BaggageType.Split(',')[1]))
                        {
                            clsFlightItinerary.Segments.Where(y => y.Group == 1).ToList().ForEach(x =>
                            {
                                if (paxinfo[i].BaggageType.Split(',')[1].Trim().ToLower() != "no bag")
                                {
                                    
                                        writer.WriteStartElement("ns1", "BaggageRequest", null);
                                        writer.WriteAttributeString("baggageCode", paxinfo[i].BaggageType.Split(',')[1].Trim());
                                        writer.WriteAttributeString("TravelerRefNumberRPHList", (paxinfo[i].Type == PassengerType.Adult ? "A" : "C") + Convert.ToString(i + 1));
                                        writer.WriteAttributeString("FlightRefNumberRPHList", x.UapiDepartureTime);
                                        writer.WriteAttributeString("DepartureDate", x.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                                        writer.WriteAttributeString("FlightNumber", x.Airline + x.FlightNumber);
                                        writer.WriteEndElement();
                                    
                                    
                                }
                            });
                        }
                    }
                    writer.WriteEndElement();//BaggageRequests
                }

                var mealInfo = clsFlightItinerary.Passenger.Where(x => x.Type != PassengerType.Infant && !string.IsNullOrEmpty(x.MealType)).ToList();

                //Including meals

                if (mealInfo.Count() > 0)
                {
                    if (paxinfo.Count() == 0 && SSRinfo.Count()==0)
                        writer.WriteStartElement("ns1", "SpecialReqDetails", null);

                    //Include Meal Details
                    writer.WriteStartElement("ns1", "MealRequests", null);
                    for (int i = 0; i < paxinfo.Count(); i++)
                    {
                        clsFlightItinerary.Segments.Where(y => y.Group == 0).ToList().ForEach(x =>
                        {
                            if (!string.IsNullOrEmpty(paxinfo[i].MealType) && paxinfo[i].MealType.Split(',')[0].Trim().ToLower() != "no meal")
                            {
                                writer.WriteStartElement("ns1", "MealRequest", null);
                                writer.WriteAttributeString("mealCode", paxinfo[i].MealType.Split(',')[0].Trim());
                                writer.WriteAttributeString("mealQuantity", "1");
                                writer.WriteAttributeString("TravelerRefNumberRPHList", (paxinfo[i].Type == PassengerType.Adult ? "A" : "C") + Convert.ToString(i + 1));
                                writer.WriteAttributeString("FlightRefNumberRPHList", x.UapiDepartureTime);
                                writer.WriteAttributeString("DepartureDate", x.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                                writer.WriteAttributeString("FlightNumber", x.Airline + x.FlightNumber);
                                writer.WriteEndElement();
                            }
                        });

                        if (clsFlightItinerary.Segments.Where(y => y.Group == 0).Count() == 0 && clsFlightItinerary.Segments.Where(y => y.Group == 1).Count() > 0)
                        {
                            clsFlightItinerary.Segments.Where(y => y.Group == 1).ToList().ForEach(x =>
                            {
                                if (!string.IsNullOrEmpty(paxinfo[i].MealType) && paxinfo[i].MealType.Split(',')[0].Trim().ToLower() != "no meal")
                                {
                                    writer.WriteStartElement("ns1", "MealRequest", null);
                                    writer.WriteAttributeString("mealCode", paxinfo[i].MealType.Split(',')[0].Trim());
                                    writer.WriteAttributeString("mealQuantity", "1");
                                    writer.WriteAttributeString("TravelerRefNumberRPHList", (paxinfo[i].Type == PassengerType.Adult ? "A" : "C") + Convert.ToString(i + 1));
                                    writer.WriteAttributeString("FlightRefNumberRPHList", x.UapiDepartureTime);
                                    writer.WriteAttributeString("DepartureDate", x.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                                    writer.WriteAttributeString("FlightNumber", x.Airline + x.FlightNumber);
                                    writer.WriteEndElement();
                                }
                            });
                        }

                        if (!string.IsNullOrEmpty(paxinfo[i].MealType) && paxinfo[i].MealType.Split(',').Length > 1 && !string.IsNullOrEmpty(paxinfo[i].MealType.Split(',')[1]))
                        {
                            clsFlightItinerary.Segments.Where(y => y.Group == 1).ToList().ForEach(x =>
                            {
                                if (!string.IsNullOrEmpty(paxinfo[i].MealType) && paxinfo[i].MealType.Split(',')[1].Trim().ToLower() != "no meal")
                                {
                                    writer.WriteStartElement("ns1", "MealRequest", null);
                                    writer.WriteAttributeString("mealCode", paxinfo[i].MealType.Split(',')[1].Trim());
                                    writer.WriteAttributeString("mealQuantity", "1");
                                    writer.WriteAttributeString("TravelerRefNumberRPHList", (paxinfo[i].Type == PassengerType.Adult ? "A" : "C") + Convert.ToString(i + 1));
                                    writer.WriteAttributeString("FlightRefNumberRPHList", x.UapiDepartureTime);
                                    writer.WriteAttributeString("DepartureDate", x.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                                    writer.WriteAttributeString("FlightNumber", x.Airline + x.FlightNumber);
                                    writer.WriteEndElement();
                                }
                            });
                        }
                    }
                    writer.WriteEndElement();//MealRequests
                }

                if (paxinfo.Count() > 0 || SSRinfo.Count() > 0 || mealInfo.Count()>0)
                    writer.WriteEndElement();//SpecialReqDetails
                writer.WriteEndElement();//TravelerInfoSummary

                if (!string.IsNullOrEmpty(clsFlightItinerary.Passenger[0].GSTStateCode))
                {
                    writer.WriteStartElement("ns1", "ServiceTaxCriteriaOptions", null);//ServiceTaxCriteriaOptions

                    writer.WriteStartElement("ns1", "CountryCode", null);//CountryCode
                    writer.WriteValue("IN");
                    writer.WriteEndElement();//CountryCode

                    writer.WriteStartElement("ns1", "StateCode", null);//StateCode
                    writer.WriteValue(clsFlightItinerary.Passenger[0].GSTStateCode);
                    writer.WriteEndElement();//StateCode

                    if (!string.IsNullOrEmpty(clsFlightItinerary.Passenger[0].GSTTaxRegNo))
                    {
                        writer.WriteStartElement("ns1", "TaxRegistrationNo", null);//TaxRegistrationNo
                        writer.WriteValue(clsFlightItinerary.Passenger[0].GSTTaxRegNo);
                        writer.WriteEndElement();//TaxRegistrationNo
                    }
                    writer.WriteEndElement();//ServiceTaxCriteriaOptions
                }

                //Send the BundleService Id in Book SSR 
                writer.WriteStartElement("ns1", "BundledServiceSelectionOptions", null);
                if (clsFlightItinerary.Segments.Length > 0 && clsFlightItinerary.Segments[0].ServiceBundleId > 0)
                {
                    writer.WriteStartElement("ns1", "OutBoundBunldedServiceId", null);
                    writer.WriteValue(clsFlightItinerary.Segments[0].ServiceBundleId);
                    writer.WriteEndElement();
                }
                if (clsFlightItinerary.Segments.Where(y => y.Group == 0).ToList().Count >0 && clsFlightItinerary.Segments.Where(y => y.Group == 1).ToList().Count >0 && clsFlightItinerary.Segments.Where(y => y.Group == 1).ToList().First().ServiceBundleId >0)
                {
                    writer.WriteStartElement("ns1", "InBoundBunldedServiceId", null);
                    writer.WriteValue(clsFlightItinerary.Segments.Where(y => y.Group == 1).ToList().First().ServiceBundleId);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement(); // end BundledServiceSelectionOptions


                writer.WriteEndElement();
                writer.WriteEndElement();
                //writer.WriteEndElement();
                writer.Flush();
                writer.Close();

                #endregion

                sDestination = sOrigin == sDestination ? clsFlightItinerary.Segments.Where(y => y.Group == 0).Select(x => x.Destination.AirportCode).Last() : sDestination;
                GenericStatic.WriteLogFileXML(Convert.ToString(priceReq.ToString()),
                        xmlPath + "AirArabiaSSRRequest_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_");

                response = GetResponse(Convert.ToString(priceReq.ToString()), ref data.cookie);

                string filePath = GenericStatic.WriteLogFileXML(response,
                    xmlPath + "AirArabiaSSRResponse_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_");

                XmlDocument xmlDoc = new XmlDocument();
                TextReader stringRead = new StringReader(response);
                xmlDoc.Load(stringRead);
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
                nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
                XmlNode xSSR = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS", nsmgr);
                XmlNode xErrors = xSSR.SelectSingleNode("ns1:Errors", nsmgr);
                if (xErrors != null)
                {
                    XmlNodeList xError = xErrors.SelectNodes("ns1:Error", nsmgr);
                    foreach (XmlNode xErr in xError)
                    {
                        Core.Audit.Add(Core.EventType.AirArabiaBooking, Core.Severity.High, 0, "G9 Failed in RequestSSR Error :" + xErr.Attributes["ShortText"].Value.ToString() + "Response XML" + response, "");
                        throw new BookingEngineException("AirArabia: "+ xErr.Attributes["ShortText"].Value.ToString());
                    }

                }
                else
                {

                    XmlDocument requestXmlDoc = new XmlDocument();
                    stringRead = new StringReader(priceReq.ToString());
                    requestXmlDoc.Load(stringRead);

                    XmlDocument responseXmlDoc = new XmlDocument();
                    stringRead = new StringReader(response);
                    responseXmlDoc.Load(stringRead);

                    XmlNode requestRPH = requestXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRQ/ns1:AirItinerary/ns1:OriginDestinationOptions", nsmgr);
                    XmlNode responseRPH = responseXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS/ns1:PricedItineraries/ns1:PricedItinerary/ns1:AirItinerary/ns1:OriginDestinationOptions", nsmgr);

                    XmlNodeList requestFlightNodes = requestRPH.SelectNodes("ns1:OriginDestinationOption/ns1:FlightSegment", nsmgr);
                    XmlNodeList responseFlightNodes = responseRPH.SelectNodes("ns1:OriginDestinationOption/ns1:FlightSegment", nsmgr);
                    if (requestFlightNodes != null && responseFlightNodes != null && ValidateResponse(requestFlightNodes, responseFlightNodes, nsmgr))
                    {
                        return response;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
            
        }

        public BookingResponse BookSSR(ref FlightItinerary clsFlightItinerary, int agencyId, string sSessionId, ref SearchResult result)
        {
            BookingResponse clsBookingResponse = new BookingResponse();
            try
            {                
                SessionData data = (SessionData)Basket.BookingSession[sessionId][clsFlightItinerary.AirlineCode + "-" + clsFlightItinerary.AliasAirlineCode];

                string response = RequestSSRPrice(ref clsFlightItinerary, data);

                XmlDocument xmlDoc = new XmlDocument();
                TextReader stringRead = new StringReader(response);
                xmlDoc.Load(stringRead);
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
                nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

                XmlNode xAvailRS = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS", nsmgr);
                XmlNode xErrors = xAvailRS.SelectSingleNode("ns1:Errors", nsmgr);
                XmlNodeList xError = xErrors != null ? xErrors.SelectNodes("ns1:Error", nsmgr) : null;

                string err = string.Empty;
                if (xError != null && xError.Count > 0)
                {
                    foreach (XmlNode xErr in xError)
                    {
                        if (!xErr.Attributes["ShortText"].Value.Contains("Seat is not available at the moment"))
                            err += xErr.Attributes["ShortText"].Value;
                        Audit.Add(EventType.AirArabiaPriceRetrieve, Severity.High, 0, "AirArabia: SSR Request Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString() + "Response :- " + response, "");
                        throw new BookingEngineException(err);
                    }
                    if (string.IsNullOrEmpty(err))
                    {
                        clsFlightItinerary.Passenger.Where(b => !string.IsNullOrEmpty(b.SeatInfo)).ToList().
                            ForEach(y => y.liPaxSeatInfo.Where(a => a.SeatStatus == "A" && !string.IsNullOrEmpty(a.SeatNo)).ToList().ForEach(s =>
                            {
                                string msg = "Failed to assign seat for passenger " + y.FirstName + " " + y.LastName + " for segment (" + s.Segment + ")";
                                err = string.IsNullOrEmpty(err) ? msg : err + "@" + msg;
                                s.SeatNo = "NoSeat"; y.Price.SeatPrice -= s.Price; s.Price = 0; s.SeatStatus = "F";
                            }));

                        clsBookingResponse.Status = BookingResponseStatus.Assignseatfailed;
                        clsBookingResponse.Error = err;
                    }
                    else
                    {
                        err = "SSR request failed. Error : " + err;
                        clsBookingResponse.Status = BookingResponseStatus.Failed;
                        clsBookingResponse.Error = err;
                    }
                }
                else
                {
                    XmlNode xPrice = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS/ns1:PricedItineraries/ns1:PricedItinerary", nsmgr);
                    XmlNodeList priceDetail = xPrice.SelectNodes("ns1:AirItineraryPricingInfo/ns1:PTC_FareBreakdowns/ns1:PTC_FareBreakdown", nsmgr);

                    string plainXml = xPrice.OuterXml.Replace("ns1:", "");
                    TextReader textReader = new StringReader(plainXml);

                    XElement co = XElement.Load(textReader);

                    var sortedElements =
                        from c in co.Elements("AirItineraryPricingInfo").
                        Elements("PTC_FareBreakdowns").Elements("PTC_FareBreakdown").Elements("TravelerRefNumber")
                        orderby c.Attribute("RPH").Value                        
                        select c.Parent;

                    plainXml = "<PTC_FareBreakdowns>";
                    foreach (var r in sortedElements.Distinct())
                    {
                        plainXml += r;
                    }

                    plainXml += "</PTC_FareBreakdowns>";
                    XmlDocument sortedDoc = new XmlDocument();
                    sortedDoc.LoadXml(plainXml);

                    XmlNode ItinTotalFareInfo = xPrice.SelectSingleNode("ns1:AirItineraryPricingInfo/ns1:ItinTotalFare", nsmgr);
                    XmlElement ItinTotalFareElement = (XmlElement)ItinTotalFareInfo;
                    if (exchangeRates.ContainsKey(ItinTotalFareElement["ns1:TotalFare"].Attributes["CurrencyCode"].Value))
                        roe = exchangeRates[ItinTotalFareElement["ns1:TotalFare"].Attributes["CurrencyCode"].Value];

                    data.SelectedTotalPrice = ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount").ToString();
                    data.totalPrice[BuildResultKey(result)] = ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount").ToString();
                    data.SelectedResultInfo = xPrice.InnerXml.ToString();

                    

                    result.BaseFare = Convert.ToDouble(ItinTotalFareElement["ns1:BaseFare"].GetAttribute("Amount")) * (double)roe;
                    result.TotalFare = Convert.ToDouble(ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount")) * (double)roe;
                    result.Tax = result.TotalFare - result.BaseFare;
                    //Updating The fare xml of selected Result.
                    if (data.SelectedResultXml != null && data.SelectedResultXml.ContainsKey(result.ResultKey))
                        data.SelectedResultXml[result.ResultKey] = xPrice.InnerXml.ToString();

                    result.FareBreakdown = new Fare[clsFlightItinerary.Passenger.Length];

                    int f = 0; double doTotBagPrice = 0; double doTotSeatPrice = 0; double doTotMealPrice = 0;
                    List<string> liPaxSeatdtls = new List<string>();
                    XmlNodeList fares = sortedDoc.SelectNodes("PTC_FareBreakdowns/PTC_FareBreakdown");
                    foreach (XmlNode paxtype in fares)
                    {
                        double doPaxBagPrice = 0; double doPaxSeatPrice = 0; double doPaxMealPrice = 0;
                        XmlNode xPassengerFare = paxtype.SelectSingleNode("PassengerFare");
                        XmlNode xPassengerType = paxtype.SelectSingleNode("PassengerTypeQuantity");
                        XmlNode xTravellerRef = paxtype.SelectSingleNode("TravelerRefNumber");
                        int paxcnt = Convert.ToInt32(xPassengerType.Attributes["Quantity"].Value);

                        XmlNodeList feesNodeList = xPassengerFare.SelectNodes("Fees/Fee");

                        for (int i = 0; i < paxcnt; i++)
                        {
                            doPaxBagPrice = 0; doPaxSeatPrice = 0; doPaxMealPrice = 0;
                            string passngrtype = Convert.ToString(xPassengerType.Attributes["Code"].Value);
                            result.FareBreakdown[f] = new Fare();
                            result.FareBreakdown[f].PassengerType = passngrtype == "ADT" ? PassengerType.Adult : passngrtype == "CHD" ? PassengerType.Child : PassengerType.Infant;
                            result.FareBreakdown[f].PassengerCount = 1;

                            foreach (XmlNode FeeNode in feesNodeList)
                            {
                                if (Convert.ToString(FeeNode.Attributes["FeeCode"].Value) == "SM/Seat Selection")
                                {
                                    doPaxSeatPrice += Convert.ToDouble(FeeNode.Attributes["Amount"].Value);
                                    liPaxSeatdtls.Add(xTravellerRef.Attributes["RPH"].Value);
                                }

                                doPaxBagPrice = Convert.ToString(FeeNode.Attributes["FeeCode"].Value) == "BG/Baggage Selection" ?
                                    doPaxBagPrice + Convert.ToDouble(FeeNode.Attributes["Amount"].Value) : doPaxBagPrice;

                                doPaxMealPrice = Convert.ToString(FeeNode.Attributes["FeeCode"].Value) == "ML/Meal Selection" ?
                                    doPaxMealPrice + Convert.ToDouble(FeeNode.Attributes["Amount"].Value) : doPaxMealPrice;
                            }

                            XmlNode xBaseFare = xPassengerFare.SelectSingleNode("BaseFare");
                            result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * (double)roe;
                            XmlNode xTotalFare = xPassengerFare.SelectSingleNode("TotalFare");
                            result.FareBreakdown[f].TotalFare = (Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) - doPaxSeatPrice - doPaxBagPrice- doPaxMealPrice) * (double)roe;
                            result.FareBreakdown[f].SupplierFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) - doPaxSeatPrice - doPaxBagPrice- doPaxMealPrice;
                            doTotBagPrice += doPaxBagPrice; doTotSeatPrice += doPaxSeatPrice; doTotMealPrice += doPaxMealPrice;
                            //Update prices
                            clsFlightItinerary.Passenger[f].Price.BaggageCharge = (decimal)doPaxBagPrice * (roe);
                            clsFlightItinerary.Passenger[f].Price.BaseFare = (decimal)result.FareBreakdown[f].BaseFare;
                            clsFlightItinerary.Passenger[f].Price.Tax = (decimal)result.FareBreakdown[f].Tax; f++;
                        }
                    }

                    doTotBagPrice = doTotBagPrice * Convert.ToDouble(roe);
                    doTotSeatPrice = doTotSeatPrice * Convert.ToDouble(roe);
                    doTotMealPrice = doTotMealPrice * Convert.ToDouble(roe);
                    result.BaseFare = result.FareBreakdown.ToList().Sum(fb => fb.BaseFare);//Update basefare also
                    result.Tax = result.Tax - (doTotBagPrice + doTotSeatPrice + doTotMealPrice);
                    result.TotalFare = result.TotalFare - (doTotBagPrice + doTotSeatPrice+ doTotMealPrice);

                    clsFlightItinerary.Passenger.Where(b => !string.IsNullOrEmpty(b.SeatInfo)).ToList().
                        ForEach(y => y.liPaxSeatInfo.Where(a => a.SeatStatus == "A" && !string.IsNullOrEmpty(a.SeatNo)).ToList().ForEach(s =>
                        {
                            if (liPaxSeatdtls.Where(p => p == (y.Type == PassengerType.Adult ? "A" : "C") + (s.PaxNo + 1).ToString()).Count() == 0)
                            {
                                string msg = "Failed to assign seat for passenger " + y.FirstName + " " + y.LastName + " for segment (" + s.Segment + ")";
                                err = string.IsNullOrEmpty(err) ? msg : err + "@" + msg;
                                s.SeatNo = "NoSeat"; y.Price.SeatPrice -= s.Price; s.Price = 0; s.SeatStatus = "F";
                            }
                            else
                                s.SeatStatus = "S";
                        }
                    ));

                    if (string.IsNullOrEmpty(err))
                    {
                        //Update pax only if any SSR is selected and prices are updated otherwise keep open for browser back handling
                        clsFlightItinerary.BookRequestType = (doTotBagPrice + doTotSeatPrice+doTotMealPrice) > 0 ? BookingFlowStatus.PaxUpdated:BookingFlowStatus.NoStatus;
                        clsBookingResponse.Status = BookingResponseStatus.Successful;
                    }
                    else
                    {
                        clsFlightItinerary.BookRequestType = BookingFlowStatus.AssignSeatFailed;
                        clsBookingResponse.Status = BookingResponseStatus.Assignseatfailed;
                        clsBookingResponse.Error = err;
                    }
                }
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(appUserId), "(AirArabia)Failed to BookSSR. Reason: " + ex.ToString(), "");
                throw ex;
            }

            return clsBookingResponse;
        }

        private string GetBaggagePriceString(SearchResult result, SearchRequest request, SessionData data, string[] rph, List<string> paxBaggages, string stateCode, string taxRegNo)//getting price string if not available
        {
            //Creating Xml For price here using result object
            StringBuilder priceReq = new StringBuilder();            
            //StreamWriter sw = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaBaggagePriceReq.xml");
            //XmlTextWriter writer = new XmlTextWriter(sw);
            //writer.Indentation = 4;


            //writer.Formatting = Formatting.Indented;
            XmlWriter writer = XmlTextWriter.Create(priceReq);  
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");
            //getting commom header

            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-26506823");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns1", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns1", "OTA_AirPriceRQ", null);
            writer.WriteAttributeString("EchoToken", "11810113621255-577396696");
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
            writer.WriteAttributeString("Version", "2006.01");
            writer.WriteAttributeString("TransactionIdentifier", data.transactionID);
            #region POSDetailFix
            writer.WriteStartElement("ns1", "POS", null);
            writer.WriteStartElement("ns1", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
            writer.WriteStartElement("ns1", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", userName);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("ns1", "AirItinerary", null);
            if (result.Flights.Length > 1)
            {
                writer.WriteAttributeString("DirectionInd", "Return");
            }
            else
            {
                writer.WriteAttributeString("DirectionInd", "OneWay");
            }
            writer.WriteStartElement("ns1", "OriginDestinationOptions", null);
            int countRPH = 0;
            for (int f = 0; f < result.Flights.Length; f++)
            {
                writer.WriteStartElement("ns1", "OriginDestinationOption", null);
                for (int r = 0; r < result.Flights[f].Length; r++)
                {
                    writer.WriteStartElement("ns1", "FlightSegment", null);
                    writer.WriteAttributeString("ArrivalDateTime", result.Flights[f][r].ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                    writer.WriteAttributeString("DepartureDateTime", result.Flights[f][r].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                    writer.WriteAttributeString("FlightNumber", result.Flights[f][r].Airline + result.Flights[f][r].FlightNumber);
                    writer.WriteAttributeString("RPH", rph[countRPH].Split('|')[0]);
                    writer.WriteStartElement("ns1", "DepartureAirport", null);
                    writer.WriteAttributeString("LocationCode", result.Flights[f][r].Origin.AirportCode.ToString());
                    writer.WriteAttributeString("Terminal", result.Flights[f][r].DepTerminal.ToString());
                    writer.WriteEndElement();
                    writer.WriteStartElement("ns1", "ArrivalAirport", null);
                    writer.WriteAttributeString("LocationCode", result.Flights[f][r].Destination.AirportCode.ToString());
                    writer.WriteAttributeString("Terminal", result.Flights[f][r].ArrTerminal.ToString());
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    countRPH++;
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "TravelerInfoSummary", null);
            writer.WriteStartElement("ns1", "AirTravelerAvail", null);
            countRPH = 0;
            int count = request.AdultCount + request.SeniorCount;
            
            if (count != 0)
            {               
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "ADT");
                writer.WriteAttributeString("Quantity", count.ToString());
                writer.WriteEndElement();
            }
            if (request.ChildCount != 0)
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "CHD");
                writer.WriteAttributeString("Quantity", request.ChildCount.ToString());
                writer.WriteEndElement();
            }
            if (request.InfantCount != 0 && request.AdultCount + request.SeniorCount >= request.InfantCount)
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "INF");
                writer.WriteAttributeString("Quantity", request.InfantCount.ToString());
                writer.WriteEndElement();
            }
            if (request.AdultCount + request.SeniorCount < request.InfantCount)
            {
                throw new BookingEngineException("AirArabia Search Infant Count Can't greater than Adult Count");
            }
            writer.WriteEndElement();
            //Include Baggage Details
            writer.WriteStartElement("ns1", "SpecialReqDetails", null);
            writer.WriteStartElement("ns1", "BaggageRequests", null);
            foreach (string paxBaggage in paxBaggages.Where(x => x.Contains("BAG")).ToList())
            {
                string bagCode = paxBaggage.Split('|')[0].Split('-')[0];
                string travRPH = paxBaggage.Split('|')[1];
                string segment = paxBaggage.Split('|')[2];
                string depTime = paxBaggage.Split('|')[3];
                string flightNum = paxBaggage.Split('|')[4];


                    writer.WriteStartElement("ns1", "BaggageRequest", null);
                    writer.WriteAttributeString("baggageCode", bagCode);
                    writer.WriteAttributeString("TravelerRefNumberRPHList", travRPH);
                    writer.WriteAttributeString("FlightRefNumberRPHList", segment);
                    writer.WriteAttributeString("DepartureDate", depTime);
                    writer.WriteAttributeString("FlightNumber", flightNum);
                    writer.WriteEndElement();
                
            }
            writer.WriteEndElement();//BaggageRequests

            writer.WriteStartElement("ns1", "MealRequests", null);
            foreach (string paxBaggage in paxBaggages.Where(x => x.Contains("MEAL")).ToList())
            {
                string bagCode = paxBaggage.Split('|')[0].Split('-')[0];
                string travRPH = paxBaggage.Split('|')[1];
                string segment = paxBaggage.Split('|')[2];
                string depTime = paxBaggage.Split('|')[3];
                string flightNum = paxBaggage.Split('|')[4];
                if (bagCode.ToLower() != "no meal")
                {
                    writer.WriteStartElement("ns1", "MealRequest", null);
                    writer.WriteAttributeString("mealCode", bagCode.Trim());
                    writer.WriteAttributeString("mealQuantity", "1");
                    writer.WriteAttributeString("TravelerRefNumberRPHList", travRPH);
                    writer.WriteAttributeString("FlightRefNumberRPHList", segment);
                    writer.WriteAttributeString("DepartureDate", depTime);
                    writer.WriteAttributeString("FlightNumber", flightNum);
                    writer.WriteEndElement();
                }
            }
            writer.WriteEndElement();//Meal Requests


            writer.WriteStartElement("ns1", "SeatRequests", null);
            paxBaggages = paxBaggages.Where(x => x.Contains("SEAT")).ToList();
            int paxCount = 0;
            foreach (string paxseat in  paxBaggages.Where(x => x.Contains("SEAT")).ToList())
            {
                string seatNo = paxseat.Split('|')[0].Split('-')[0];
                string travRPH = paxseat.Split('|')[1];
                string segment = paxseat.Split('|')[2];
                string depTime = paxseat.Split('|')[3];
                string flightNum = paxseat.Split('|')[4];
                
                writer.WriteStartElement("ns1", "SeatRequest", null);
                writer.WriteAttributeString("SeatNumber", seatNo.Trim());
                writer.WriteAttributeString("TravelerRefNumberRPHList", travRPH);
                writer.WriteAttributeString("FlightRefNumberRPHList", segment);
                writer.WriteAttributeString("DepartureDate", depTime);
                writer.WriteAttributeString("FlightNumber", flightNum);
                writer.WriteEndElement();
                paxCount++;
            }
            writer.WriteEndElement();//Seat Requests

            writer.WriteEndElement();//SpecialReqDetails
            writer.WriteEndElement();//TravelerInfoSummary

            writer.WriteStartElement("ns1", "BundledServiceSelectionOptions", null);
            if (result.Flights[0][0].ServiceBundleId > 0)
            {
                writer.WriteStartElement("ns1", "OutBoundBunldedServiceId", null);
                writer.WriteValue(result.Flights[0][0].ServiceBundleId);
                writer.WriteEndElement();
            }
            if (result.Flights.Length > 1 && result.Flights[1][0].ServiceBundleId > 0)
            {
                writer.WriteStartElement("ns1", "InBoundBunldedServiceId", null);
                writer.WriteValue(result.Flights[1][0].ServiceBundleId);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();

            if (!string.IsNullOrEmpty(stateCode))//Added by lokesh on 29-03-2018.
            {
                // <ns1:ServiceTaxCriteriaOptions>
                //<ns1:CountryCode > IN </ ns1:CountryCode>
                //<ns1:StateCode > 22 </ ns1:StateCode>
                //<ns1:TaxRegistrationNo> 221234567891235 </ns1:TaxRegistrationNo>
                //</ ns1:ServiceTaxCriteriaOptions>
                writer.WriteStartElement("ns1", "ServiceTaxCriteriaOptions", null);//ServiceTaxCriteriaOptions

                writer.WriteStartElement("ns1", "CountryCode", null);//CountryCode
                writer.WriteValue("IN");
                writer.WriteEndElement();//CountryCode

                writer.WriteStartElement("ns1", "StateCode", null);//StateCode
                writer.WriteValue(stateCode);
                writer.WriteEndElement();//StateCode

                if (!string.IsNullOrEmpty(taxRegNo))
                {
                    writer.WriteStartElement("ns1", "TaxRegistrationNo", null);//TaxRegistrationNo
                    writer.WriteValue(taxRegNo);
                    writer.WriteEndElement();//TaxRegistrationNo
                }
                writer.WriteEndElement();//ServiceTaxCriteriaOptions
            }

            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();

            //StreamReader reader = new StreamReader(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaBaggagePriceReq.xml");
            //priceReq.Append(reader.ReadToEnd());
            return priceReq.ToString();
        }

        private string GetResponse(string Request, ref CookieContainer cookie)
        {
            string strResult;
            int txnRetryCount = 10;
            int numberRetry = 0;
            bool txnSucceded = false;
            HttpWebResponse objResponse = null;
            while (!txnSucceded && numberRetry < txnRetryCount)
            {
                numberRetry++;
                string request = Request;
                //ServicePointManager.SecurityProtocol.
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //string requestUrl = "http://airarabia29.isaaviations.com/webservices/services/AAWebServicesForReceiptUpdate";
                string requestUrl = url;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(requestUrl);
                req.Method = "POST";
                //
                //ASCIIEncoding encoding = new ASCIIEncoding();
                //byte[] byte1 = encoding.GetBytes(postData);
                //byte[] reqBytes = Encoding.ASCII.GetBytes(request);
                byte[] reqBytes =  Encoding.UTF8.GetBytes(request);
                req.ContentLength = reqBytes.Length;
                //req.SendChunked = false;
                //
                //req.ContentLength = request.Length;
                req.ContentType = "text/xml;charset=\"utf-8\"";
                req.Accept = "text/xml";
                req.KeepAlive = true;
                req.CookieContainer = cookie;
                req.ProtocolVersion = HttpVersion.Version11;// To avoid closing the connection

                //Stream str = req.GetRequestStream();
                //StreamWriter writer = new StreamWriter(str);
                //writer.Write(request);
                //writer.Flush();
                //Stream resp = Stream.Null;
                //WebResponse response = null;
                //HttpWebResponse objResponse=null;
                // to retry if error occure

                try
                {
                    Stream str = req.GetRequestStream();
                    StreamWriter writer = new StreamWriter(str);
                    writer.Write(request);
                    writer.Flush();
                    Stream resp = Stream.Null;
                    objResponse = (HttpWebResponse)req.GetResponse();
                    txnSucceded = true;
                    str.Close();
                    if(objResponse.Cookies.Count>0)
                    {
                        
                        string jSessionId = objResponse.Cookies[0].Value;
                        Core.Audit.Add(Core.EventType.AirArabiaRetrieveByPNR, Core.Severity.High, 0, "Jsession ID "+jSessionId+",request->"+request+",  Status Code:"+objResponse.StatusCode, "");

                    }
                }
                catch (Exception ex)
                {
                    if (numberRetry < txnRetryCount)
                    {
                        continue;
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                strResult = sr.ReadToEnd();
                sr.Close();
            }
            return strResult;

        }

        private string GetResponseORG(string Request, ref CookieContainer cookie)
        {
            string strResult;
            int txnRetryCount = 10;
            int numberRetry = 0;
            bool txnSucceded = false;
            HttpWebResponse objResponse = null;
            while (!txnSucceded && numberRetry < txnRetryCount)
            {
                numberRetry++;
                string request = Request;
                //ServicePointManager.SecurityProtocol.
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //string requestUrl = "http://airarabia29.isaaviations.com/webservices/services/AAWebServicesForReceiptUpdate";
                string requestUrl = url;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(requestUrl);
                req.Method = "POST";
                req.ContentLength = request.Length;
                req.ContentType = "text/xml;charset=\"utf-8\"";
                req.Accept = "text/xml";
                req.KeepAlive = true;
                req.CookieContainer = cookie;
                req.ProtocolVersion = HttpVersion.Version11;// To avoid closing the connection

                //Stream str = req.GetRequestStream();
                //StreamWriter writer = new StreamWriter(str);
                //writer.Write(request);
                //writer.Flush();
                //Stream resp = Stream.Null;
                //WebResponse response = null;
                //HttpWebResponse objResponse=null;
                // to retry if error occure

                try
                {
                    Stream str = req.GetRequestStream();
                    StreamWriter writer = new StreamWriter(str);
                    writer.Write(request);
                    writer.Flush();
                    Stream resp = Stream.Null;
                    objResponse = (HttpWebResponse)req.GetResponse();
                    txnSucceded = true;
                    if (objResponse.Cookies.Count > 0)
                    {

                        string jSessionId = objResponse.Cookies[0].Value;
                        Core.Audit.Add(Core.EventType.AirArabiaRetrieveByPNR, Core.Severity.High, 0, "Jsession ID " + jSessionId + ",request->" + request + ",  Status Code:" + objResponse.StatusCode, "");

                    }
                }
                catch (Exception ex)
                {
                    if (numberRetry < txnRetryCount)
                    {
                        continue;
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                strResult = sr.ReadToEnd();
                sr.Close();
            }
            return strResult;

        }

        private string GetResponseOLD(string Request, ref CookieContainer cookie)
        {
            string request = Request;
            string requestUrl = url;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(requestUrl);
            req.Method = "POST";
            req.ContentLength = request.Length;
            req.ContentType = "text/xml;charset=\"utf-8\"";
            req.Accept = "text/xml";
            req.KeepAlive = true;
            req.CookieContainer = cookie;
            Stream str = req.GetRequestStream();
            StreamWriter writer = new StreamWriter(str);
            writer.Write(request);
            writer.Flush();
            Stream resp = Stream.Null;
            //WebResponse response = null;
            HttpWebResponse objResponse;
            objResponse = (HttpWebResponse)req.GetResponse();
            string strResult;
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                strResult = sr.ReadToEnd();
                sr.Close();
            }
            return strResult;
        }

        private string GetRequestStringForAvailability(SearchRequest searchRequest, string nearByOrigin, string nearByDestination)//done
        {
            StringBuilder availReq = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(availReq);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-26506823");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns1", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns1", "OTA_AirAvailRQ", null);
            writer.WriteAttributeString("EchoToken", "11810113621255-577396696");//check later
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("Target", "TEST");
            writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
            writer.WriteAttributeString("Version", "2006.01");//fix
            #region POSDetailFix
            writer.WriteStartElement("ns1", "POS", null);
            writer.WriteStartElement("ns1", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
            writer.WriteStartElement("ns1", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", userName);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("ns1", "OriginDestinationInformation", null);
            writer.WriteStartElement("ns1", "DepartureDateTime", null);
            writer.WriteValue(searchRequest.Segments[0].PreferredDepartureTime);//to be inserted if return
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "OriginLocation", null);
            writer.WriteAttributeString("LocationCode", nearByOrigin);//To be inserted if return
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "DestinationLocation", null);
            writer.WriteAttributeString("LocationCode", nearByDestination);//TO be inserted if return
            writer.WriteEndElement();
            writer.WriteEndElement();

            if (searchRequest.Segments.Length > 1 && searchRequest.Segments[1] != null && searchRequest.Segments[1].PreferredDepartureTime > DateTime.MinValue)
            {
                writer.WriteStartElement("ns1", "OriginDestinationInformation", null);
                writer.WriteStartElement("ns1", "DepartureDateTime", null);
                writer.WriteValue(searchRequest.Segments[1].PreferredDepartureTime);//to be inserted if return
                writer.WriteEndElement();
                writer.WriteStartElement("ns1", "OriginLocation", null);
                writer.WriteAttributeString("LocationCode",nearByDestination);//To be inserted if return
                writer.WriteEndElement();
                writer.WriteStartElement("ns1", "DestinationLocation", null);
                writer.WriteAttributeString("LocationCode", nearByOrigin);//TO be inserted if return
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            writer.WriteStartElement("ns1", "TravelerInfoSummary", null);
            writer.WriteStartElement("ns1", "AirTravelerAvail", null);
            if (searchRequest.AdultCount != 0 || searchRequest.SeniorCount != 0)
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "ADT");
                int adult = searchRequest.AdultCount + searchRequest.SeniorCount;// both seniors and adults are considered as adults
                writer.WriteAttributeString("Quantity", adult.ToString());
                writer.WriteEndElement();
            }
            if (searchRequest.ChildCount != 0)
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "CHD");
                writer.WriteAttributeString("Quantity", searchRequest.ChildCount.ToString());//for chlidern
                writer.WriteEndElement();
            }
            if (searchRequest.InfantCount != 0 && (searchRequest.AdultCount + searchRequest.SeniorCount >= searchRequest.InfantCount))
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "INF");
                writer.WriteAttributeString("Quantity", searchRequest.InfantCount.ToString());// for infants
                writer.WriteEndElement();
            }
            if ((searchRequest.AdultCount + searchRequest.SeniorCount) < searchRequest.InfantCount)
            {
                throw new BookingEngineException("AirArabia Search Infant Count Can't greater than Adult Count");
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
            return availReq.ToString();
        }

        public FlightItinerary RetrieveItinerary(string pnr)
        {
            //Trace.TraceInformation("AirArabia.RetrieveItinerary Enter");
            FlightItinerary itinerary = new FlightItinerary();
            CookieContainer cookie = new CookieContainer();
            string reqPNR = GetRetrieveMessage(pnr);
            string resPNR = GetResponse(reqPNR, ref cookie);
            if (resPNR != null)
            {
                itinerary = ReadRetrieveResponse(resPNR, pnr);
            }
            else
            {
                Core.Audit.Add(Core.EventType.AirArabiaRetrieveByPNR, Core.Severity.High, 0, "Retrieve PNR failed| " + DateTime.Now, "");
                //Trace.TraceError("AirArabia Booking No result found");
                throw new Exception("AirArabia Retreive Failed");
                //throw new BookingEngineException("AirArabia Retreive Failed");
            }
            //Trace.TraceInformation("AirArabia.RetrieveItinerary Exit");
            return itinerary;
        }

        public FlightItinerary ReadRetrieveResponse(string response, string pnr)
        {
            //Trace.TraceInformation("AirArabia.readimportPnrres Enter");
            FlightItinerary itinerary = new FlightItinerary();
            XmlDocument xmlDoc = new XmlDocument();
            TextReader stringRead = new StringReader(response);
            xmlDoc.Load(stringRead);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
            nsmgr.AddNamespace("ns2", "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05");
            //XmlNode xerror = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirBookRS/ns1:Errors/ns1:Error", nsmgr);
            XmlNode xerror = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_ResRetrieveRS/ns1:Errors/ns1:Error", nsmgr);

            if (xerror != null)
            {
                Core.Audit.Add(Core.EventType.AirArabiaRetrieveByPNR, Core.Severity.High, 0, "Exception returned from AirArabia. Error Message:" + xerror.Attributes["ShortText"].Value.ToString() + " | " + DateTime.Now + "| request XML" + response, "");
                //Trace.TraceError("Error: " + xerror.Attributes["ShortText"].Value.ToString());
                throw new BookingEngineException(" Error: " + xerror.Attributes["ShortText"].Value.ToString());
            }
            else
            {
                itinerary.AirlineCode = airlineCode;
                itinerary.PNR = pnr;
                XmlNode xPnrRS = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirBookRS/ns1:AirReservation", nsmgr);
                XmlNode flightInfo = xPnrRS.SelectSingleNode("ns1:AirItinerary/ns1:OriginDestinationOptions", nsmgr);
                XmlNodeList flights = flightInfo.SelectNodes("ns1:OriginDestinationOption", nsmgr);
                XmlNodeList flightWithSegments = flightInfo.SelectNodes("ns1:OriginDestinationOption/ns1:FlightSegment", nsmgr);
                int count = flightWithSegments.Count;
                itinerary.Segments = new FlightInfo[count];
                int i = 0;
                bool setOriDes = true;
                foreach (XmlNode FlightSeg in flights)
                {
                    itinerary.FlightBookingSource = BookingSource.AirArabia;//Added later by Ritika
                    XmlNodeList segment = FlightSeg.SelectNodes("ns1:FlightSegment", nsmgr);
                    foreach (XmlNode seg in segment)
                    {
                        itinerary.Segments[i] = new FlightInfo();
                        itinerary.Segments[i].BookingClass = "P";//AS observed most of the time Booking Class from AirArabia Side is "P"
                        itinerary.Segments[i].CabinClass = "Economy";//AS observed most of the time Booking Class from AirArabia Side is "P"
                        if (seg.HasChildNodes)
                        {
                            itinerary.Segments[i].ArrivalTime = Convert.ToDateTime(seg.Attributes["ArrivalDateTime"].Value.ToString());
                            itinerary.Segments[i].DepartureTime = Convert.ToDateTime(seg.Attributes["DepartureDateTime"].Value.ToString());
                            itinerary.Segments[i].FlightNumber = seg.Attributes["FlightNumber"].Value.Substring(2);
                            itinerary.Segments[i].Airline = seg.Attributes["FlightNumber"].Value.Substring(0, 2);
                            itinerary.AirlineCode = seg.Attributes["FlightNumber"].Value.Substring(0, 2);
                            if (seg.SelectSingleNode("ns1:DepartureAirport", nsmgr) != null)
                            {
                                itinerary.Segments[i].Origin = new Airport(seg.SelectSingleNode("ns1:DepartureAirport", nsmgr).Attributes["LocationCode"].Value.ToString());
                                itinerary.Segments[i].DepTerminal = seg.SelectSingleNode("ns1:DepartureAirport", nsmgr).Attributes["Terminal"].Value.ToString();
                            }
                            if (seg.SelectSingleNode("ns1:ArrivalAirport", nsmgr) != null)
                            {
                                itinerary.Segments[i].Destination = new Airport(seg.SelectSingleNode("ns1:ArrivalAirport", nsmgr).Attributes["LocationCode"].Value.ToString());
                                itinerary.Segments[i].ArrTerminal = seg.SelectSingleNode("ns1:ArrivalAirport", nsmgr).Attributes["Terminal"].Value.ToString();
                            }
                        }
                        i++;
                    }
                    if (setOriDes)
                    {
                        setOriDes = false;
                        itinerary.Origin = Convert.ToString(itinerary.Segments[0].Origin.CityCode);
                        itinerary.Destination = Convert.ToString(itinerary.Segments[i - 1].Destination.CityCode);
                    }
                }
                itinerary.FareRules = GetFareRule(itinerary.Origin, itinerary.Destination, "P");
                XmlNode price = xPnrRS.SelectSingleNode("ns1:PriceInfo/ns1:ItinTotalFare", nsmgr);
                XmlNode travelersinfo = xPnrRS.SelectSingleNode("ns1:TravelerInfo", nsmgr);
                XmlNodeList travelers = travelersinfo.SelectNodes("ns1:AirTraveler", nsmgr);
                int paxcount = travelers.Count;
                int pax = 0;
                itinerary.Passenger = new FlightPassenger[paxcount];
                foreach (XmlNode paxInfo in travelers)
                {
                    itinerary.Passenger[pax] = new FlightPassenger();
                    if (paxInfo.Attributes["PassengerTypeCode"].Value.ToString() == "ADT")
                    {
                        itinerary.Passenger[pax].Type = PassengerType.Adult;
                    }
                    if (paxInfo.Attributes["PassengerTypeCode"].Value.ToString() == "CHD")
                    {
                        itinerary.Passenger[pax].Type = PassengerType.Child;
                    }
                    if (paxInfo.Attributes["PassengerTypeCode"].Value.ToString() == "INF")
                    {
                        itinerary.Passenger[pax].Type = PassengerType.Infant;
                    }
                    itinerary.Passenger[pax].Price = new PriceAccounts();
                    //itinerary.Passenger[pax].Price.PublishedFare = Convert.ToDecimal(price.SelectSingleNode("ns1:BaseFare", nsmgr).Attributes["Amount"].Value);
                    //itinerary.Passenger[pax].Price.Tax = Convert.ToDecimal(price.SelectSingleNode("ns1:Taxes/ns1:Tax", nsmgr).Attributes["Amount"].Value);

                    XmlNode paxDetail = paxInfo.SelectSingleNode("ns1:PersonName", nsmgr);
                    if (paxDetail.HasChildNodes)
                    {
                        itinerary.Passenger[pax].FirstName = paxDetail.SelectSingleNode("ns1:GivenName", nsmgr).InnerText.ToString();
                        itinerary.Passenger[pax].LastName = paxDetail.SelectSingleNode("ns1:Surname", nsmgr).InnerText.ToString();
                        if (paxDetail.SelectSingleNode("ns1:NameTitle", nsmgr) != null)
                        {
                            itinerary.Passenger[pax].Title = paxDetail.SelectSingleNode("ns1:NameTitle", nsmgr).InnerText.ToString();
                        }
                        else
                        {
                            itinerary.Passenger[pax].Title = "";
                        }
                    }
                    if (paxInfo.SelectSingleNode("ns1:Telephone", nsmgr) != null)
                    {
                        itinerary.Passenger[pax].CellPhone = paxInfo.SelectSingleNode("ns1:Telephone", nsmgr).Attributes["PhoneNumber"].Value.ToString();
                    }
                    pax++;
                }
                XmlNode ticketinfo = xPnrRS.SelectSingleNode("ns1:Ticketing", nsmgr);
                //itinerary.PaymentMode = ModeOfPayment.Cash;
                if (ticketinfo.SelectSingleNode("ns1:TicketAdvisory", nsmgr) != null)
                {
                    itinerary.TicketAdvisory = ticketinfo.SelectSingleNode("ns1:TicketAdvisory", nsmgr).InnerText.ToString();
                }
            }
            //Trace.TraceInformation("AirArabia.readimportPnrres Exit");
            return itinerary;
        }

        private string GetRetrieveMessage(string pnr)
        {
            //Trace.TraceInformation("AirArabia.resByPNRstring Enter");
            StringBuilder ReqByPNR = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(ReqByPNR);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");
            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-26506823");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion
            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns1", null, "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05");
            writer.WriteAttributeString("xmlns", "ns2", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns2", "OTA_ReadRQ", null);
            writer.WriteAttributeString("EchoToken", "11810128220460-413253889");
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
            writer.WriteAttributeString("Version", "2006.01");//fix
            #region POSDetailFix
            writer.WriteStartElement("ns2", "POS", null);
            writer.WriteStartElement("ns2", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
            writer.WriteStartElement("ns2", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", "WSCOZMO");
            writer.WriteEndElement();
            writer.WriteStartElement("ns2", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion
            writer.WriteStartElement("ns2", "ReadRequests", null);
            writer.WriteStartElement("ns2", "ReadRequest", null);
            writer.WriteStartElement("ns2", "UniqueID", null);
            writer.WriteAttributeString("ID", pnr);
            writer.WriteAttributeString("Type", "14");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "AAReadRQExt", null);
            writer.WriteStartElement("ns1", "AALoadDataOptions", null);
            writer.WriteStartElement("ns1", "LoadTravelerInfo", null);
            writer.WriteValue("true");
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "LoadAirItinery", null);
            writer.WriteValue("true");
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "LoadPriceInfoTotals", null);
            writer.WriteValue("true");
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "LoadFullFilment", null);
            writer.WriteValue("true");
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "LoadPTC_FareBreakdowns", null);
            writer.WriteValue("true");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
            //Trace.TraceInformation("AirArabia.resByPNRstring Exit");
            return ReqByPNR.ToString();
        }

        public BookingResponse Booking(FlightItinerary itinerary, int bookingAgencyId, string sessionId)
        {
            BookingResponse bookingResponse = new BookingResponse();
            try
            {
                //Trace.TraceInformation("AirArabia.Booking Enter");
               
                SessionData sessiondta = (SessionData)Basket.BookingSession[sessionId][airlineCode + "-" + itinerary.AliasAirlineCode];
                string request = GetBookingRequest(itinerary, bookingAgencyId, sessionId);
                //StreamWriter writer = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaBookingReq.xml");
                //writer.Write(request);
                //writer.Close();

                string sOrigin = itinerary.Segments.Select(x => x.Origin.AirportCode).FirstOrDefault();
                string sDestination = itinerary.Segments.Select(x => x.Destination.AirportCode).Last();
                sDestination = sOrigin == sDestination ? itinerary.Segments.Where(g => g.Group == 0).Select(x => x.Destination.AirportCode).Last() : sDestination;
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(request);
                    string filePath = xmlPath + "AirArabiaBookingRequest_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    doc.Save(filePath);
                }
                catch { }
                string response = string.Empty;
                try
                {
                    response = GetResponse(request, ref sessiondta.cookie);
                    //writer = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaBookingRes.xml");
                    //writer.Write(response);
                    //writer.Close();
                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(response);
                        string filePath = xmlPath + "AirArabiaBookingResponse_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        doc.Save(filePath);
                    }
                    catch { }
                    //Core.Audit.Add(Core.EventType.AirArabiaBooking, Core.Severity.High, 0, "AirArabia Booking | Request : " + request + "| Response :-" + response + "| Time" + DateTime.Now, "");
                }
                catch (WebException ex)
                {
                    Core.Audit.Add(Core.EventType.AirArabiaBooking, Core.Severity.High, 0, "Web Exception Returned from AirArabia | Error = " + ex.Message + "| Request XML" + request + "| Time= " + DateTime.Now, "");
                    throw ex;
                }
                if (response != null && response.Length > 0)
                {
                    //Validate RPH & Flight Number
                    TextReader stringRead = null;
                    XmlDocument requestXmlDoc = new XmlDocument();
                    stringRead = new StringReader(request);
                    requestXmlDoc.Load(stringRead);

                    XmlDocument responseXmlDoc = new XmlDocument();
                    stringRead = new StringReader(response);
                    responseXmlDoc.Load(stringRead);

                    XmlNamespaceManager nsmgr = new XmlNamespaceManager(requestXmlDoc.NameTable);
                    nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                    nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                    nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
                    nsmgr.AddNamespace("ns2", "http://www.opentravel.org/OTA/2003/05");
                    nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

                    XmlNode requestRPH = requestXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns2:OTA_AirBookRQ/ns2:AirItinerary/ns2:OriginDestinationOptions", nsmgr);
                    XmlNode xBookRS = responseXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirBookRS", nsmgr);

                    XmlNode xErrors = xBookRS.SelectSingleNode("ns1:Errors", nsmgr);
                    XmlNodeList xError = xErrors.SelectNodes("ns1:Error", nsmgr);
                    if (xError != null && xError.Count > 0)
                    {
                        foreach (XmlNode xErr in xError)
                        {
                            Core.Audit.Add(Core.EventType.AirArabiaBooking, Core.Severity.High, 0, "AirArabia Booking Failed. Error :" + xErr.Attributes["ShortText"].Value.ToString() + "Response XML" + response, "");
                            bookingResponse = new BookingResponse(BookingResponseStatus.Failed, xErr.Attributes["ShortText"].Value.ToString(), string.Empty);
                        }
                    }
                    else
                    {
                        XmlNode responseRPH = responseXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirBookRS/ns1:AirReservation/ns1:AirItinerary/ns1:OriginDestinationOptions", nsmgr);

                        XmlNodeList requestFlightNodes = requestRPH.SelectNodes("ns2:OriginDestinationOption/ns2:FlightSegment", nsmgr);
                        XmlNodeList responseFlightNodes = responseRPH.SelectNodes("ns1:OriginDestinationOption/ns1:FlightSegment", nsmgr);
                        if (requestFlightNodes != null && responseFlightNodes != null && ValidateResponse(requestFlightNodes, responseFlightNodes, nsmgr))
                        {
                            bookingResponse = ReadBookingResponse(response, itinerary);
                        }
                    }
                }
                else
                {
                    Core.Audit.Add(Core.EventType.AirArabiaBooking, Core.Severity.High, 0, "AirArabia Booking Failed| Null Response from AirArabia| Time" + DateTime.Now, "");
                    bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "AirArabia Booking Failed| Null Response from AirArabia", string.Empty);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //Trace.TraceInformation("AirArabia.Booking Exit");
            return bookingResponse;
        }

        private string GetBookingRequest(FlightItinerary itinerary, int bookingAgencyId, string sessionId)//not complete yet
        {
            //Trace.TraceInformation("AirArabia.GetRequestStringForBooking Entered");
            StringBuilder bookingReq = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(bookingReq);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");
            SessionData sessiondta = (SessionData)Basket.BookingSession[sessionId][airlineCode + "-" + itinerary.AliasAirlineCode];
            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            //writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-26506823");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion
            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns1", null, "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05");
            writer.WriteAttributeString("xmlns", "ns2", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns2", "OTA_AirBookRQ", null);
            writer.WriteAttributeString("EchoToken", "11810120749379731256589");
            writer.WriteAttributeString("TransactionIdentifier", sessiondta.transactionID);//new for every transaction
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
            writer.WriteAttributeString("Version", "2006.01");
            #region POS
            writer.WriteStartElement("ns2", "POS", null);
            writer.WriteStartElement("ns2", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
            writer.WriteStartElement("ns2", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", userName);
            writer.WriteEndElement();
            writer.WriteStartElement("ns2", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion
            string pricedetail = string.Empty;
            
            //Reading The price from selected Fare xml .
            if (sessiondta.SelectedResultXml != null) 
            {
                string fareXml = "<Root>"+sessiondta.SelectedResultXml[itinerary.AliasAirlineCode] +"</Root>";
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(fareXml);
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmldoc.NameTable);
                nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
                XmlNode xPrice = xmldoc.SelectSingleNode("/Root/ns1:AirItineraryPricingInfo/ns1:ItinTotalFare", nsmgr);
                sessiondta.SelectedTotalPrice = xPrice["ns1:TotalFare"].GetAttribute("Amount");

                if (fareXml.IndexOf("<ns1:AABundledServiceExt") > 0)
                {
                    int index = fareXml.IndexOf("<ns1:AABundledServiceExt");
                    int endindex = fareXml.LastIndexOf("</ns1:AABundledServiceExt>");
                    string result = fareXml.Substring(0, index).Replace("<ns1:AABundledServiceExt", "");
                    result += fareXml.Substring(endindex).Replace("</ns1:AABundledServiceExt>", "");
                    sessiondta.SelectedResultInfo = result.Replace("<Root>","").Replace("</Root>","");
                }
            }
            pricedetail = sessiondta.SelectedResultInfo;
            pricedetail = pricedetail.Replace("ns1", "ns2");
            pricedetail = pricedetail.Replace("<ns2:AirItinerary xmlns:ns2=\"http://www.opentravel.org/OTA/2003/05\">", "<ns2:AirItinerary>");
            pricedetail = pricedetail.Replace("<ns2:AirItineraryPricingInfo PricingSource=\"Published\" xmlns:ns2=\"http://www.opentravel.org/OTA/2003/05\">", "<ns2:PriceInfo>");
            pricedetail = pricedetail.Replace("<ns2:AirItineraryPricingInfo PricingSource=\"Published\">", "<ns2:PriceInfo>");
            pricedetail = pricedetail.Replace("AirItineraryPricingInfo", "PriceInfo");
            writer.WriteElementString("PriceDetail", "");
            int noOfPax = itinerary.Passenger.Length;
            //Travler Info
            writer.WriteStartElement("ns2", "TravelerInfo", null);
            int noOfTravlers = itinerary.Passenger.Length;
            int adult = 0;//Counting No of Adults for assgning to infants
            //int inf = 0;
            // Added by hari malla on 2019-11-06
            // assigning the default values for srea code ,phone number and country code.            
            string areaCode = "06", countryCode = "971", phoneNumber = "5074444";
            string[] phone = itinerary.Passenger[0].CellPhone.Split('-');
            for (int pax = 0; pax < noOfTravlers; pax++)
            {
                writer.WriteStartElement("ns2", "AirTraveler", null);
                if (itinerary.Passenger[pax].DateOfBirth > DateTime.MinValue && itinerary.Passenger[pax].Type!=PassengerType.Infant)
                {
                    writer.WriteAttributeString("BirthDate", itinerary.Passenger[pax].DateOfBirth.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                }
                if (itinerary.Passenger[pax].Type == PassengerType.Adult || itinerary.Passenger[pax].Type == PassengerType.Senior)
                {
                    writer.WriteAttributeString("PassengerTypeCode", "ADT");
                }
                if (itinerary.Passenger[pax].Type == PassengerType.Child)
                {
                    writer.WriteAttributeString("PassengerTypeCode", "CHD");
                    //writer.WriteAttributeString("BirthDate", itinerary.Passenger[pax].DateOfBirth.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                }
                if (itinerary.Passenger[pax].Type == PassengerType.Infant)
                {
                    writer.WriteAttributeString("PassengerTypeCode", "INF");

                }                
                writer.WriteStartElement("ns2", "PersonName", null);
                writer.WriteStartElement("ns2", "GivenName", null);
                writer.WriteValue(itinerary.Passenger[pax].FirstName);
                writer.WriteEndElement();
                writer.WriteStartElement("ns2", "Surname", null);
                writer.WriteValue(itinerary.Passenger[pax].LastName);
                writer.WriteEndElement();

                writer.WriteStartElement("ns2", "NameTitle", null);
                if (itinerary.Passenger[pax].Title.ToUpper() == "MR" || itinerary.Passenger[pax].Title.ToUpper() == "MS" || itinerary.Passenger[pax].Title.ToUpper() == "MRS" || itinerary.Passenger[pax].Title.ToUpper() == "MSTR" || itinerary.Passenger[pax].Title.ToUpper() == "MISS")
                {
                    writer.WriteValue(itinerary.Passenger[pax].Title.ToUpper());
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
                if (itinerary.Passenger[pax].Type == PassengerType.Adult)
                {
                    //string areaCode = "", countryCode = "", phoneNumber = "";
                    // Added by hari malla on 2019-11-06
                    // Passing the default area code,country code and phone number when cell phone is not getting.
                    areaCode = phone.Length > 1 ? phone[1].Substring(0, 2) : areaCode;
                    countryCode = phone.Length > 1? phone[0] : countryCode;
                    phoneNumber = phone.Length > 1 ? phone[1] : phoneNumber;
                    phoneNumber = phoneNumber.Length > 2 ? phoneNumber.Substring(2, (phoneNumber.Length) - 2) : phoneNumber;

                    writer.WriteStartElement("ns2", "Telephone", null);
                    writer.WriteAttributeString("AreaCityCode", areaCode);//TO be inserted
                    writer.WriteAttributeString("CountryAccessCode", countryCode.Replace("+", ""));// needed access code here
                    writer.WriteAttributeString("PhoneNumber", phoneNumber);
                    //passing destination phone number for lead pax.
                    //added by lokesh on 5-Oct-2017.
                    //if (itinerary.Passenger[pax].IsLeadPax && !string.IsNullOrEmpty(itinerary.Passenger[pax].DestinationPhone))
                    //{
                    //    string workPhone = itinerary.Passenger[pax].DestinationPhone.Split('-')[1];
                    //    workPhone = workPhone.Substring(2, (workPhone.Length) - 2);
                    //    writer.WriteAttributeString("MobileNumber", workPhone);
                    //}

                    if (itinerary.Passenger[pax].IsLeadPax && !string.IsNullOrEmpty(itinerary.Passenger[pax].DestinationPhone))
                    {
                        writer.WriteAttributeString("MobileNumber", itinerary.Passenger[pax].DestinationPhone.Replace('-', ' '));
                    }

                    writer.WriteEndElement();
                }
                writer.WriteStartElement("ns2", "Address", null);
                writer.WriteStartElement("ns2", "CountryName", null);
                if (itinerary.Passenger[0].Country.CountryCode != null && itinerary.Passenger[0].Country.CountryCode.Length != 0)
                {
                    writer.WriteAttributeString("Code", itinerary.Passenger[pax].Country.CountryCode);
                }
                else
                {
                    writer.WriteAttributeString("Code", "IN");
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
                //if (itinerary.Passenger[pax].Type == PassengerType.Adult)
                {
                    writer.WriteStartElement("ns2", "Document", null);
                    ////writer.WriteAttributeString("DocID", itinerary.Passenger[pax].PassportNo);
                    ////writer.WriteAttributeString("DocType", "PSPT");

                    //Lokesh 10May2017 : Changed DocHolderNationality as per passenger wise.
                    if (itinerary.Passenger[pax].Nationality != null && !string.IsNullOrEmpty(itinerary.Passenger[pax].Nationality.CountryCode))
                    {
                        writer.WriteAttributeString("DocHolderNationality", itinerary.Passenger[pax].Nationality.CountryCode);
                    }
                    
                    //writer.WriteStartElement("ns2", "DocHolderName", null);
                    //writer.WriteValue(itinerary.Passenger[pax].FirstName);
                    //writer.WriteEndElement();
                    writer.WriteEndElement();
                }
                if (itinerary.Passenger[pax].Type == PassengerType.Adult || itinerary.Passenger[pax].Type == PassengerType.Senior)
                {
                    adult++;
                    writer.WriteStartElement("ns2", "TravelerRefNumber", null);
                    writer.WriteAttributeString("RPH", "A" + (pax + 1).ToString());
                    writer.WriteEndElement();
                }
                if (itinerary.Passenger[pax].Type == PassengerType.Child)
                {
                    writer.WriteStartElement("ns2", "TravelerRefNumber", null);
                    writer.WriteAttributeString("RPH", "C" + (pax + 1).ToString());
                    writer.WriteEndElement();
                }

                if (itinerary.Passenger[pax].Type == PassengerType.Infant)
                {
                    writer.WriteStartElement("ns2", "TravelerRefNumber", null);
                    writer.WriteAttributeString("RPH", "I" + (pax + 1).ToString() + "/A" + adult.ToString());
                    adult--;
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            //fullfillment
            writer.WriteStartElement("ns2", "Fulfillment", null);
            writer.WriteStartElement("ns2", "PaymentDetails", null);
            writer.WriteStartElement("ns2", "PaymentDetail", null);
            writer.WriteStartElement("ns2", "DirectBill", null);
            writer.WriteStartElement("ns2", "CompanyName", null);
            writer.WriteAttributeString("Code", code);//Of Travel Boutique Online
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("ns2", "PaymentAmount", null);
            XmlDocument xmlDoc = new XmlDocument();
            writer.WriteAttributeString("Amount", sessiondta.SelectedTotalPrice);//TOtal amt be inserted
            writer.WriteAttributeString("CurrencyCode", "AED");
            writer.WriteAttributeString("DecimalPlaces", "2");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #region AAAirBookRQExt
            // Agency agency = new Agency(bookingAgencyId); 
            // Added by hari malla on 2019-11-06 
            // Passing the default area code,country code and phone number when cell phone is not getting.
            areaCode = "06"; countryCode = "971"; phoneNumber = "5074444";
            areaCode = phone.Length > 1 ? phone[1].Substring(0, 2) : areaCode;
            countryCode = phone.Length > 1 ? phone[0] : countryCode;
            phoneNumber = phone.Length > 1 ? phone[1] : phoneNumber;             
            phoneNumber = phoneNumber.Length > 2 ?phoneNumber.Substring(2, (phoneNumber.Length) - 2): phoneNumber;            

            writer.WriteStartElement("ns1", "AAAirBookRQExt", null);
            writer.WriteStartElement("ns1", "ContactInfo", null);
            writer.WriteStartElement("ns1", "PersonName", null);
            writer.WriteStartElement("ns1", "Title", null);
            writer.WriteValue(itinerary.Passenger[0].Title.ToUpper());
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "FirstName", null);
            writer.WriteValue(itinerary.Passenger[0].FirstName);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "LastName", null);
            writer.WriteValue(itinerary.Passenger[0].LastName);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "Telephone", null);
            writer.WriteStartElement("ns1", "PhoneNumber", null);
            //string phone = "41646222";
            //if (!string.IsNullOrEmpty(itinerary.Passenger[0].CellPhone))
            //{
            //    phone = itinerary.Passenger[0].CellPhone;
            //}
            writer.WriteValue(phoneNumber);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "CountryCode", null);
            writer.WriteValue(countryCode.Replace("+", ""));//code of india
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "AreaCode", null);
            writer.WriteValue(areaCode);//code of delhi
            writer.WriteEndElement();
            writer.WriteEndElement();//Telephone
            string email = "tech@cozmotravel.com";
            if (!string.IsNullOrEmpty(itinerary.Passenger[0].Email))
            {
                email = itinerary.Passenger[0].Email;
            }
            writer.WriteStartElement("ns1", "Email", null);
            writer.WriteValue(email);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "Address", null);
            writer.WriteStartElement("ns1", "CountryName", null);
            writer.WriteStartElement("ns1", "CountryName", null);
            string country = "IN";
            if (!string.IsNullOrEmpty(itinerary.Passenger[0].Country.CountryCode))
            {
                country = itinerary.Passenger[0].Country.CountryCode;
            }
            if (!string.IsNullOrEmpty(itinerary.Passenger[0].GSTStateCode))//Added by lokesh on 29-03-2018.
            {
                country = "IN";
            }
            if (country == "IN")
            {
                writer.WriteValue("INDIA");
            }
            else
            {
              writer.WriteValue(itinerary.Passenger[0].Country.CountryName);
            }
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "CountryCode", null);         
            writer.WriteValue(country);
            writer.WriteEndElement();
            writer.WriteEndElement();//CountryName
            if (!string.IsNullOrEmpty(itinerary.Passenger[0].GSTStateCode))//Added by lokesh on 29-03-2018.
            {
                //<ns1:StateCode>22</ns1:StateCode>
                writer.WriteStartElement("ns1", "StateCode", null);//StateCode
                writer.WriteValue(itinerary.Passenger[0].GSTStateCode);
                writer.WriteEndElement();//StateCode
            }
            writer.WriteStartElement("ns1", "CityName", null);
            string cityName = "Delhi";
            if (!string.IsNullOrEmpty(itinerary.Passenger[0].City))
            {
                cityName = itinerary.Passenger[0].City;
            }
            writer.WriteValue(cityName);
            writer.WriteEndElement();//City
            writer.WriteEndElement();//Address
            if (!string.IsNullOrEmpty(itinerary.Passenger[0].GSTTaxRegNo))//Added by lokesh on 29-03-2018.
            {
                //<ns1:taxRegNo>221234567891235</ns1:taxRegNo>
                writer.WriteStartElement("ns1", "taxRegNo", null);//taxRegNo
                writer.WriteValue(itinerary.Passenger[0].GSTTaxRegNo);
                writer.WriteEndElement();//taxRegNo
            }
            writer.WriteEndElement();//ContactInfo
            writer.WriteEndElement();//AAAirBookRQExt
            #endregion
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
            bookingReq = bookingReq.Replace("<PriceDetail />", pricedetail);
            
            //Trace.TraceInformation("AirArabia.GetRequestStringForBooking Exit");
            return bookingReq.ToString();
        }

        private BookingResponse ReadBookingResponse(string response, FlightItinerary itinerary)
        {
            //Trace.TraceError("AirArabia ReadBookingResponse entered");
            BookingResponse bResponse = new BookingResponse();
            XmlDocument xmlDoc = new XmlDocument();
            TextReader stringRead = new StringReader(response);
            xmlDoc.Load(stringRead);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
            nsmgr.AddNamespace("ns2", "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05");
            XmlNode xBookRS = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirBookRS", nsmgr);
           
            //if (ticketing == null)
            //{
                XmlNode xErrors = xBookRS.SelectSingleNode("ns1:Errors", nsmgr);
                XmlNodeList xError = xErrors.SelectNodes("ns1:Error", nsmgr);
                if (xError != null && xError.Count>0) 
                {
                   
                    foreach (XmlNode xErr in xError)
                    {
                        Core.Audit.Add(Core.EventType.AirArabiaBooking, Core.Severity.High, 0, "AirArabia Booking Failed. Error :" + xErr.Attributes["ShortText"].Value.ToString() + "Response XML" + response, "");
                        bResponse = new BookingResponse(BookingResponseStatus.Failed, xErr.Attributes["ShortText"].Value.ToString(), string.Empty);

                    }
                }
                else
                {
                    XmlNode reservationNode = xBookRS.SelectSingleNode("ns1:AirReservation", nsmgr);
                    XmlNode ticketing = reservationNode.SelectSingleNode("ns1:Ticketing", nsmgr);

                    XmlNode referenceId = reservationNode.SelectSingleNode("ns1:BookingReferenceID", nsmgr);
                    if (referenceId != null)
                    {
                        bResponse.PNR = referenceId.Attributes["ID"].Value;
                        bResponse.Status = BookingResponseStatus.Successful;
                        itinerary.PNR = referenceId.Attributes["ID"].Value;
                        itinerary.FareType = "Pub";
                    }
                    else
                    {
                        Core.Audit.Add(Core.EventType.AirArabiaBooking, Core.Severity.High, 0, "AirArabia Booking Failed. Error : PNR not recieved.| Response XML" + response, "");
                        bResponse = new BookingResponse(BookingResponseStatus.Failed, "PNR not received", string.Empty);
                    }
                }
            //}
           
            //Trace.TraceError("AirArabia ReadBookingResponse exit");
            return bResponse;
        }

        public static List<FareRule> GetFareRule(string origin, string dest, string fareBasisCode)
        {
            //Trace.TraceInformation("AirArabia.GetFareRule Enter");
            List<FareRule> fareRuleList = new List<FareRule>();

            FareRule DNfareRules = new FareRule();
            DNfareRules.Origin = origin;
            DNfareRules.Destination = dest;
            DNfareRules.Airline = "G9";
            DNfareRules.FareRuleDetail = Util.GetLCCFareRules("G9");
            DNfareRules.FareBasisCode = fareBasisCode;

            fareRuleList.Add(DNfareRules);

            //Trace.TraceInformation("AirArabia.GetFareRule exit");
            return fareRuleList;
        }
        private string GetAgentCreditRequest()
        {
            //Trace.TraceInformation("AirArabia.GetRequestStringForBooking Entered");
            StringBuilder AgentAvailCredit = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(AgentAvailCredit);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");
            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-26506823");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion
            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns1", null, "http://www.opentravel.org/OTA/2003/05");
            //writer.WriteAttributeString("xmlns", "ns2", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns1", "AA_OTA_AgentAvailCreditRQ", null);
            writer.WriteAttributeString("EchoToken", "11810120749379731256589");
            // writer.WriteAttributeString("TransactionIdentifier", sessiondta.transactionID);//new for every transaction
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
            writer.WriteAttributeString("Version", "2006.01");
            #region POS
            writer.WriteStartElement("ns1", "POS", null);
            writer.WriteStartElement("ns1", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
            writer.WriteStartElement("ns1", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", userName);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion
            writer.WriteStartElement("ns1", "AgentID", null);
            writer.WriteValue(code);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
            //Trace.TraceInformation("AirArabia.GetAgentAvailCredit Exit");
            return AgentAvailCredit.ToString();
        }
        public decimal GetAgentAvailCredit()
        {
            string request = GetAgentCreditRequest();
            CookieContainer cookie = new CookieContainer();
            string response = string.Empty;
            try
            {
                response = GetResponse(request, ref cookie);
                //Core.Audit.Add(Core.EventType.AirArabiaCreditbalance, Core.Severity.High, 0, "AirArabia GetAgentAvailCredit | Request : " + request + "| Response :-" + response + "| Time" + DateTime.Now, "");
            }
            catch (WebException ex)
            {
                Core.Audit.Add(Core.EventType.AirArabiaCreditbalance, Core.Severity.High, 0, "Web Exception Returned from AirArabia | Error = " + ex.Message + "| Request XML" + request + "| Time= " + DateTime.Now, "");
                throw ex;
            }
            decimal balance = ReadAgentAvailCredit(response);
            return balance;
        }
        private decimal ReadAgentAvailCredit(string response)
        {
            decimal balance = 0;
            XmlDocument xmlDoc = new XmlDocument();
            TextReader stringRead = new StringReader(response);
            xmlDoc.Load(stringRead);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
            XmlNode xCreditRS = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AgentAvailCreditRS", nsmgr);
            XmlNode xErrors = xCreditRS.SelectSingleNode("ns1:Errors", nsmgr);
            if (xErrors != null)
            {
                XmlNodeList xError = xErrors.SelectNodes("ns1:Error", nsmgr);
                foreach (XmlNode xErr in xError)
                {
                    Core.Audit.Add(Core.EventType.AirArabiaCreditbalance, Core.Severity.High, 0, "AirArabia retreive  Agent Avail Credit Failed. Error :" + xErr.Attributes["ShortText"].Value.ToString() + "Response XML" + response, "");
                    throw new BookingEngineException("AirArabia: Get Credit balance Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString());                
                }
                
            }
            else
            {
                XmlNode xCreditAvail = xCreditRS.SelectSingleNode("ns1:AgentAvailCredit/ns1:AvailableCredit", nsmgr);
                balance=Convert.ToDecimal(xCreditAvail.Attributes["Amount"].Value);
            }
            return balance;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~AirArabia()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);

        }
        #endregion

        public DataTable GetMealDetails(SearchResult result, string sessionId)
        {
            TextReader stringRead = null;
            XmlDocument requestXmlDoc = null;
            XmlDocument responseXmlDoc = null;
            try
            {
                result.ResultKey = result.Flights[0].Select(x => x.Origin.AirportCode).FirstOrDefault() + "-" +
                    result.Flights[0].Select(x => x.Destination.AirportCode).Last();
                agentCurrency = result.Currency;
                SessionData data = (SessionData)Basket.BookingSession[sessionId][result.Airline + "-" + result.ResultKey];

                //Prepare the Meal request for the flight segment
                string request = "";
                request = GetMealRequestString(result.Flights, data, false);
                string sOrigin = result.Flights[0].Select(x => x.Origin.AirportCode).FirstOrDefault();
                string sDestination = result.Flights[0].Select(x => x.Destination.AirportCode).Last();
                try
                {
                    //StreamWriter writer = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaOnwardBaggageReq.xml");
                    //writer.Write(request);
                    //writer.Close();

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(request);
                    string filePath = xmlPath + "AirArabiaMealOnwardRequest_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    doc.Save(filePath);
                }
                catch { }
                //Get the meal response
                string response = GetResponse(request, ref data.cookie);
                try
                {
                    XmlDocument doc = new XmlDocument();
                    //doc.Load(@"C:\Users\CZDEV\Desktop\FP\BundledFare(1)\BundledFare\5_ANCI_AVAIL_RES_MEAL.xml");
                    //response = doc.InnerXml;
                    doc.LoadXml(response);
                    string filePath = xmlPath + "AirArabiaMealOnwardResponse_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    doc.Save(filePath);
                }
                catch { }

                //Validate RPH & Flight Number for meals

                requestXmlDoc = new XmlDocument();
                stringRead = new StringReader(request);
                requestXmlDoc.Load(stringRead);



                responseXmlDoc = new XmlDocument();
                stringRead = new StringReader(response);
                responseXmlDoc.Load(stringRead);

                XmlNamespaceManager nsmgr = new XmlNamespaceManager(requestXmlDoc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
                nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
                nsmgr.AddNamespace("ns2", "http://www.opentravel.org/OTA/2003/05");

                XmlNode requestRPH = requestXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns2:AA_OTA_AirMealDetailsRQ/ns2:MealDetailsRequests", nsmgr);

                XmlNode xBookRS = responseXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AirMealDetailsRS", nsmgr);

                XmlNode xErrors = xBookRS.SelectSingleNode("ns1:Errors", nsmgr);
                XmlNodeList xError = xErrors.SelectNodes("ns1:Error", nsmgr);
                if (xError != null && xError.Count > 0)
                {
                    foreach (XmlNode xErr in xError)
                    {
                        Core.Audit.Add(Core.EventType.AirArabiaPriceRetrieve, Core.Severity.High, 0, "AirArabia GetMealDetails Failed.Error " + xErr.Attributes["ShortText"].Value.ToString(), "");
                        //throw new BookingEngineException("AirArabia Onward Meal Failed :" + xErr.Attributes["ShortText"].Value.ToString());
                    }
                }
                else
                {
                    XmlNode responseRPH = responseXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AirMealDetailsRS/ns1:MealDetailsResponses", nsmgr);
                    XmlNodeList requestFlightNodes = requestRPH.SelectNodes("ns2:MealDetailsRequest/ns2:FlightSegmentInfo", nsmgr);
                    XmlNodeList responseFlightNodes = responseRPH.SelectNodes("ns1:MealDetailsResponse/ns1:FlightSegmentInfo", nsmgr);
                    if (requestFlightNodes != null && responseFlightNodes != null && ValidateResponse(requestFlightNodes, responseFlightNodes, nsmgr))
                    {
                        dtMealDetails = ReadMealResponse(response, result.Price.SupplierCurrency);
                    }
                }

                if (result.Flights.Length > 1)
                {
                    //------------------------------------------------------------------------------------------------
                    //                      Generate Return Meal Request and Response                            //
                    //------------------------------------------------------------------------------------------------
                    request = GetMealRequestString(result.Flights, data, true);
                    sOrigin = result.Flights[1].Select(x => x.Origin.AirportCode).FirstOrDefault();
                    sDestination = result.Flights[1].Select(x => x.Destination.AirportCode).Last();
                    try
                    {
                        //StreamWriter writer = new StreamWriter(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirArabiaReturnBaggageReq.xml");
                        //writer.Write(request);
                        //writer.Close();
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(request);
                        string filePath = xmlPath + "AirArabiaMealReturnRequest_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        doc.Save(filePath);
                    }
                    catch { }

                    //Get the Meal response
                    response = GetResponse(request, ref data.cookie);
                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(response);
                        string filePath = xmlPath + "AirArabiaMealReturnResponse_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        doc.Save(filePath);
                    }
                    catch { }

                    //Validate RPH & Flight Number for Return Baggage

                    requestXmlDoc = new XmlDocument();
                    stringRead = new StringReader(request);
                    requestXmlDoc.Load(stringRead);

                    responseXmlDoc = new XmlDocument();
                    stringRead = new StringReader(response);
                    responseXmlDoc.Load(stringRead);
                    xBookRS = responseXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AirMealDetailsRS", nsmgr);

                    xErrors = xBookRS.SelectSingleNode("ns1:Errors", nsmgr);
                    xError = xErrors.SelectNodes("ns1:Error", nsmgr);
                    if (xError != null && xError.Count > 0)
                    {
                        foreach (XmlNode xErr in xError)
                        {
                            Core.Audit.Add(Core.EventType.AirArabiaPriceRetrieve, Core.Severity.High, 0, "AirArabia GetMealDetails Failed.Error " + xErr.Attributes["ShortText"].Value.ToString(), "");
                            //throw new BookingEngineException("AirArabia Return Meal Failed :" + xErr.Attributes["ShortText"].Value.ToString());
                        }
                    }
                    else
                    {
                        requestRPH = requestXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns2:AA_OTA_AirMealDetailsRQ/ns2:MealDetailsRequests", nsmgr);
                        XmlNode responseRPH = responseXmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AirMealDetailsRS/ns1:MealDetailsResponses", nsmgr);
                        XmlNodeList requestFlightNodes = requestRPH.SelectNodes("ns2:MealDetailsRequest/ns2:FlightSegmentInfo", nsmgr);
                        XmlNodeList responseFlightNodes = responseRPH.SelectNodes("ns1:MealDetailsResponse/ns1:FlightSegmentInfo", nsmgr);
                        if (requestFlightNodes != null && responseFlightNodes != null && ValidateResponse(requestFlightNodes, responseFlightNodes, nsmgr))
                        {
                            dtMealDetails = ReadMealResponse(response,result.Price.SupplierCurrency);
                        }
                    }
                }
                if (dtMealDetails.Rows.Count == 0 && (result.FareType.Contains("Extra") || result.FareType.Contains("Value")))
                {
                    if (xError != null && xError.Count > 0)
                    {
                        throw new BookingEngineException("AirArabia Return Meal Failed :" + xError[0].Attributes["ShortText"].Value.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtMealDetails;
        }

        private string GetMealRequestString(FlightInfo[][] Flights, SessionData data, bool isReturn)
        {
            StringBuilder mealRequest = new StringBuilder();

            XmlWriter writer = XmlTextWriter.Create(mealRequest);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-16755466");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns2", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns2", "AA_OTA_AirMealDetailsRQ", null);
            writer.WriteAttributeString("EchoToken", "11835513529845-916011621");//check later
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("TransactionIdentifier", data.transactionID);
            writer.WriteAttributeString("Version", "2006.01");//fix
            #region POSDetailFix
            writer.WriteStartElement("ns2", "POS", null);

            writer.WriteStartElement("ns2", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");

            writer.WriteStartElement("ns2", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", userName);
            writer.WriteEndElement();

            writer.WriteStartElement("ns2", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();

            writer.WriteEndElement();//Source
            writer.WriteEndElement();//POS
            #endregion
            writer.WriteStartElement("ns2", "MealDetailsRequests", null);
            int i = isReturn ? 1 : 0;

            for (int j = 0; j < Flights[i].Length; j++)
            {
                writer.WriteStartElement("ns2", "MealDetailsRequest", null);
                writer.WriteStartElement("ns2", "FlightSegmentInfo", null);
                writer.WriteAttributeString("ArrivalDateTime", Flights[i][j].ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                writer.WriteAttributeString("DepartureDateTime", Flights[i][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                writer.WriteAttributeString("FlightNumber", Flights[i][j].Airline + Flights[i][j].FlightNumber);
                //writer.WriteAttributeString("RPH", result.Flights[i][0].SegmentId.ToString());
                writer.WriteAttributeString("RPH", Flights[i][j].UapiDepartureTime);

                writer.WriteStartElement("ns2", "DepartureAirport", null);
                writer.WriteAttributeString("LocationCode", Flights[i][j].Origin.AirportCode);
                writer.WriteAttributeString("Terminal", Flights[i][j].DepTerminal);

                writer.WriteEndElement();

                writer.WriteStartElement("ns2", "ArrivalAirport", null);
                writer.WriteAttributeString("LocationCode", Flights[i][j].Destination.AirportCode);
                writer.WriteAttributeString("Terminal", Flights[i][j].ArrTerminal);

                writer.WriteEndElement();

                writer.WriteStartElement("ns2", "OperatingAirline", null);
                writer.WriteAttributeString("Code", "G9");
                writer.WriteEndElement();

                writer.WriteEndElement();//Flight Segment Info
                writer.WriteEndElement();//MealDetailsRequest
            }

            writer.WriteEndElement();//MealDetailsRequests
            writer.WriteEndElement();//AA_OTA_AirMealDetailsRQ
            writer.WriteEndElement();


            writer.Flush();
            writer.Close();


            return mealRequest.ToString();
        }

        private DataTable ReadMealResponse(string response,string currency)
        {

            //Trace.TraceInformation("AirArabia.readBaggageResponse Enter");
            FlightItinerary itinerary = new FlightItinerary();
            XmlDocument xmlDoc = new XmlDocument();
            TextReader stringRead = new StringReader(response);
            xmlDoc.Load(stringRead);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
            nsmgr.AddNamespace("ns2", "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05");
            XmlNode xerror = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AirMealDetailsRS/ns1:Errors/ns1:Error", nsmgr);
            if (xerror != null && xerror.Attributes["ShortText"] != null)
            {
                Core.Audit.Add(Core.EventType.AirArabiaRetrieveByPNR, Core.Severity.High, 0, "Exception returned from AirArabia. Error Message:" + xerror.Attributes["ShortText"].Value.ToString() + " | " + DateTime.Now + "| request XML" + response, "");
                //Trace.TraceError("Error: " + xerror.Attributes["ShortText"].Value.ToString());
                //throw new BookingEngineException(" Error: " + xerror.Attributes["ShortText"].Value.ToString());
                //Audit.Add(EventType.AirArabiaSearch, Severity.High, 0, xerror.Attributes["ShortText"].Value, "0");
            }
            else
            {
                XmlNode xsuccess = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AirMealDetailsRS/ns1:Success", nsmgr);
                XmlNodeList xBaggages = xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:AA_OTA_AirMealDetailsRS/ns1:MealDetailsResponses/ns1:MealDetailsResponse", nsmgr);
                if (exchangeRates.ContainsKey(currency))
                {
                    roe = exchangeRates[currency];
                }
                foreach (XmlNode node in xBaggages)
                {
                    XmlNode segmentNode = null;
                    foreach (XmlNode meal in node.ChildNodes)
                    {
                        if (meal.Name == "ns1:FlightSegmentInfo")
                        {
                            segmentNode = meal;
                        }
                        if (meal.Name == "ns1:Meal")
                        {
                            DataRow dr = dtMealDetails.NewRow();

                            dr["mealCode"] = meal.SelectSingleNode("ns1:mealCode", nsmgr).InnerText;
                            dr["mealDescription"] = meal.SelectSingleNode("ns1:mealDescription", nsmgr).InnerText;
                            dr["mealCharge"] = Convert.ToDecimal(meal.SelectSingleNode("ns1:mealCharge", nsmgr).InnerText) * roe;
                            dr["currencyCode"] = agentCurrency;
                            dr["SegmentId"] = segmentNode.Attributes["RPH"].Value;
                            dtMealDetails.Rows.Add(dr);
                        }
                    }
                }
            }

            return dtMealDetails;
        }

        private string RequestMealPrice(ref FlightItinerary flightItinerary, SessionData data)
        {
            string response = string.Empty;
            StringBuilder priceReq = new StringBuilder();
            string sOrigin = flightItinerary.Segments.Select(x => x.Origin.AirportCode).FirstOrDefault();
            string sDestination = flightItinerary.Segments.Select(x => x.Destination.AirportCode).Last();

            XmlWriter writer = XmlTextWriter.Create(priceReq);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-26506823");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns2", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns2", "AA_OTA_AirMealDetailsRQ", null);
            writer.WriteAttributeString("EchoToken", "11810113621255-577396696");
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
            writer.WriteAttributeString("Version", "2006.01");
            writer.WriteAttributeString("TransactionIdentifier", data.transactionID);

            #region POSDetailFix
            writer.WriteStartElement("ns2", "POS", null);
            writer.WriteStartElement("ns2", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
            writer.WriteStartElement("ns2", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", userName);
            writer.WriteEndElement();
            writer.WriteStartElement("ns2", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("ns2", "MealDetailsRequests", null);
            writer.WriteStartElement("ns2", "MealDetailsRequest", null);
            writer.WriteStartElement("ns2", "FlightSegmentInfo", null);

            List<string> liSegmntInfo = new List<string>();

            if (flightItinerary.Segments.Where(x => x.Group == 0).Count() > 0)
            {

                writer.WriteStartElement("ns2", "MealDetailsRequest", null);
                flightItinerary.Segments.Where(x => x.Group == 0).ToList().ForEach(y =>
                {

                    writer.WriteStartElement("ns2", "FlightSegmentInfo", null);
                    writer.WriteAttributeString("ArrivalDateTime", y.ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                    writer.WriteAttributeString("DepartureDateTime", y.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                    writer.WriteAttributeString("FlightNumber", y.Airline + y.FlightNumber);
                    writer.WriteAttributeString("RPH", y.UapiDepartureTime + "|" + y.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + y.Airline + y.FlightNumber);
                    writer.WriteStartElement("ns2", "DepartureAirport", null);
                    writer.WriteAttributeString("LocationCode", y.Origin.AirportCode);
                    writer.WriteAttributeString("Terminal", y.DepTerminal);
                    writer.WriteEndElement();
                    writer.WriteStartElement("ns2", "ArrivalAirport", null);
                    writer.WriteAttributeString("LocationCode", y.Destination.AirportCode);
                    writer.WriteAttributeString("Terminal", y.ArrTerminal);
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    liSegmntInfo.Add(y.Origin.AirportCode + "-" + y.Destination.AirportCode + "|" + y.UapiDepartureTime);
                });
                writer.WriteEndElement();
            }

            if (flightItinerary.Segments.Where(x => x.Group == 1).Count() > 0)
            {
                writer.WriteStartElement("ns2", "MealDetailsRequest", null);

                flightItinerary.Segments.Where(x => x.Group == 1).ToList().ForEach(y =>
                {
                    writer.WriteStartElement("ns2", "FlightSegmentInfo", null);
                    writer.WriteAttributeString("ArrivalDateTime", y.ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                    writer.WriteAttributeString("DepartureDateTime", y.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                    writer.WriteAttributeString("FlightNumber", y.Airline + y.FlightNumber);
                    writer.WriteAttributeString("RPH", y.UapiDepartureTime + "|" + y.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + y.Airline + y.FlightNumber);
                    writer.WriteStartElement("ns2", "DepartureAirport", null);
                    writer.WriteAttributeString("LocationCode", y.Origin.AirportCode);
                    writer.WriteAttributeString("Terminal", y.DepTerminal);
                    writer.WriteEndElement();
                    writer.WriteStartElement("ns2", "ArrivalAirport", null);
                    writer.WriteAttributeString("LocationCode", y.Destination.AirportCode);
                    writer.WriteAttributeString("Terminal", y.ArrTerminal);
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    liSegmntInfo.Add(y.Origin.AirportCode + "-" + y.Destination.AirportCode + "|" + y.UapiDepartureTime);
                });
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteEndElement();


            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();

            sDestination = sOrigin == sDestination ? flightItinerary.Segments.Where(y => y.Group == 0).Select(x => x.Destination.AirportCode).Last() : sDestination;
            GenericStatic.WriteLogFileXML(Convert.ToString(priceReq.ToString()),
                    xmlPath + "AirArabiaMeakRequest_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_");

            response = GetResponse(Convert.ToString(priceReq.ToString()), ref data.cookie);

            string filePath = GenericStatic.WriteLogFileXML(response,
                xmlPath + "AirArabiaMealResponse_" + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_");

            return response;
        }
        private bool ValidateResponse(XmlNodeList requestFlightNodes, XmlNodeList responseFlightNodes, XmlNamespaceManager nsmgr)
        {
            bool isValid = false;
            try
            {
                for (int i = 0; i < requestFlightNodes.Count; i++)
                {
                    isValid = false;
                    for (int j = 0; j < responseFlightNodes.Count; j++)
                    {
                        if (requestFlightNodes[i].Attributes["FlightNumber"].Value == responseFlightNodes[j].Attributes["FlightNumber"].Value &&
                        requestFlightNodes[i].Attributes["ArrivalDateTime"].Value == responseFlightNodes[j].Attributes["ArrivalDateTime"].Value && requestFlightNodes[i].Attributes["DepartureDateTime"].Value == responseFlightNodes[j].Attributes["DepartureDateTime"].Value)
                        {
                            isValid = true;
                        }
                    }
                    if (!isValid)
                    {
                        Audit.Add(EventType.AirArabiaBooking, Severity.High, 1, "(AirArabia)Failed to validate RPH & Flight Number " + " Request Flight Number:" + requestFlightNodes[i].Attributes["FlightNumber"].Value + "," + "Response Flight No:" + responseFlightNodes[i].Attributes["FlightNumber"].Value + "," + "Request ArrivalDateTime:" + requestFlightNodes[i].Attributes["ArrivalDateTime"].Value + "," + "Reponse ArrivalDateTime:" + responseFlightNodes[i].Attributes["ArrivalDateTime"].Value + "," + "Request DepartureDateTime:" + requestFlightNodes[i].Attributes["DepartureDateTime"].Value + "," + "Reponse DepartureDateTime:" + responseFlightNodes[i].Attributes["DepartureDateTime"].Value, "");
                        isValid = false;
                        throw new Exception("Booking failed due to mismatch flight details.");
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AirArabiaBooking, Severity.High, 1, "(AirArabia)Failed to validate RPH & Flight Number. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return isValid;
        }

        private void ReadPriceFromSearchResponse(XmlDocument xmlDoc, ref SearchResult result, ref SessionData data, SearchRequest request)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

            XmlNode xPrice = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirAvailRS/ns1:AAAirAvailRSExt/ns1:PricedItineraries/ns1:PricedItinerary", nsmgr);
            fareXml = xPrice.InnerXml.ToString();
            XmlNode ItinTotalFareInfo = xPrice.SelectSingleNode("ns1:AirItineraryPricingInfo/ns1:ItinTotalFare", nsmgr);
            XmlElement ItinTotalFareElement = (XmlElement)ItinTotalFareInfo;
            result.Currency = agentCurrency;
            //result.Price = new PriceAccounts();
            result.Price.SupplierCurrency = ItinTotalFareElement["ns1:TotalFare"].Attributes["CurrencyCode"].Value;
            if (exchangeRates.ContainsKey(ItinTotalFareElement["ns1:TotalFare"].Attributes["CurrencyCode"].Value))
            {
                roe = exchangeRates[ItinTotalFareElement["ns1:TotalFare"].Attributes["CurrencyCode"].Value];
            }
            result.BaseFare = Convert.ToDouble(ItinTotalFareElement["ns1:BaseFare"].GetAttribute("Amount")) * (double)roe;
            result.TotalFare = Convert.ToDouble(ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount")) * (double)roe;
            result.Price.SupplierPrice = Convert.ToDecimal(ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount"));
            string key = BuildResultKey(result);
            if (data.totalPrice.ContainsKey(key))
            {
                data.totalPrice[key] = ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount").ToString();
            }
            else
            {
                data.totalPrice.Add(key, ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount").ToString());
            }
            result.Tax = result.TotalFare - result.BaseFare;
            XmlNodeList priceDetail = xPrice.SelectNodes("ns1:AirItineraryPricingInfo/ns1:PTC_FareBreakdowns/ns1:PTC_FareBreakdown", nsmgr);

            int paxType = 0;
            if (request.AdultCount > 0)
            {
                paxType++;
            }
            if (request.ChildCount > 0)
            {
                paxType++;
            }
            if (request.SeniorCount > 0)
            {
                paxType++;
            }
            if (request.InfantCount > 0)
            {
                paxType++;
            }

            result.FareBreakdown = new Fare[paxType];
            int f = 0;
            //Added by lokesh on 4-Apr-2018 to resolve multiple pax issue in farebreakdown.
            bool priceCalculatedForAdult = false;//Variable which holds whether the price for adult is calculated or not
            bool priceCalculatedForChild = false;//Variable which holds whether the price for child is calculated or not
            bool priceCalculatedForInfant = false;//Variable which holds whether the price for infant is calculated or not
            foreach (XmlNode paxtype in priceDetail)
            {
                XmlNode xPassengerFare = paxtype.SelectSingleNode("ns1:PassengerFare", nsmgr);
                XmlNode xPassengerType = paxtype.SelectSingleNode("ns1:PassengerTypeQuantity", nsmgr);

                if ((request.AdultCount != 0 || request.SeniorCount != 0) && xPassengerType.Attributes["Code"].Value.ToString() == "ADT")
                {

                    if (request.AdultCount != 0 && !priceCalculatedForAdult)//for adult
                    {
                        result.FareBreakdown[f] = new Fare();
                        result.FareBreakdown[f].PassengerType = PassengerType.Adult;
                        result.FareBreakdown[f].PassengerCount = request.AdultCount;

                        XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                        result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                        XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                        result.FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                        result.FareBreakdown[f].SupplierFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount;
                        priceCalculatedForAdult = true;
                        f++;
                    }
                    if (request.SeniorCount != 0)
                    {
                        result.FareBreakdown[f] = new Fare();
                        result.FareBreakdown[f].PassengerType = PassengerType.Adult;
                        result.FareBreakdown[f].PassengerCount = request.AdultCount;

                        XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                        result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                        XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                        result.FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                        result.FareBreakdown[f].SupplierFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount;
                        f++;
                    }
                }
                else if (request.ChildCount != 0 && xPassengerType.Attributes["Code"].Value.ToString() == "CHD" && !priceCalculatedForChild)
                {
                    result.FareBreakdown[f] = new Fare();
                    result.FareBreakdown[f].PassengerType = PassengerType.Child;
                    result.FareBreakdown[f].PassengerCount = request.ChildCount;

                    XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                    result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                    XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                    result.FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                    result.FareBreakdown[f].SupplierFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount;
                    priceCalculatedForChild = true;
                    f++;
                }
                else if (request.InfantCount != 0 && xPassengerType.Attributes["Code"].Value.ToString() == "INF" && !priceCalculatedForInfant)
                {
                    result.FareBreakdown[f] = new Fare();
                    result.FareBreakdown[f].PassengerType = PassengerType.Infant;
                    result.FareBreakdown[f].PassengerCount = request.InfantCount;

                    XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                    result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                    XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                    result.FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * (double)roe;
                    //result.FareBreakdown[f].Tax = Convert.ToDouble(result.FareBreakdown[f].TotalFare - result.FareBreakdown[f].BaseFare);
                    result.FareBreakdown[f].SupplierFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount;
                    priceCalculatedForInfant = true;
                    f++;
                }
            }

        }

        private void PrepareBundleServices(XmlDocument xmlDocument)
        {
            //BundleServiceExt
            lstBundlesOn.Clear();
            lstBundlesRet.Clear();
            XmlNodeList onwardBundleService = null;
            XmlNodeList returnBundleService = null;
            XmlNode origindestInfo = xmlDocument.SelectSingleNode("/Root/ns1:AirItinerary/ns1:OriginDestinationOptions", nsmgr);

            //Bind the Onward & Return Bundle Services to lstBundlesOn & lstBundlesRet
            foreach (XmlNode node in origindestInfo)
            {
                if (node.Name == "ns1:AABundledServiceExt" && node.Attributes["applicableOndSequence"].Value == "0")
                {
                    onwardBundleService = node.SelectNodes("ns1:bundledService", nsmgr);
                }
                if (node.Name == "ns1:AABundledServiceExt" && node.Attributes["applicableOndSequence"].Value == "1")
                {
                    returnBundleService = node.SelectNodes("ns1:bundledService", nsmgr);
                }
            }

            if (onwardBundleService != null && onwardBundleService.Count > 0)
            {
                foreach (XmlNode onwardBundle in onwardBundleService)
                {
                    int onwServiceId = Convert.ToInt32(onwardBundle.SelectSingleNode("ns1:bunldedServiceId", nsmgr).InnerText);
                    string onwardFareType = (onwardBundle.SelectSingleNode("ns1:bundledServiceName", nsmgr) != null) ? onwardBundle.SelectSingleNode("ns1:bundledServiceName", nsmgr).InnerText : string.Empty;
                    string onwardDescription = (onwardBundle.SelectSingleNode("ns1:description", nsmgr) != null) ? onwardBundle.SelectSingleNode("ns1:description", nsmgr).InnerText : string.Empty;
                    string onwbookingClass = (onwardBundle.SelectSingleNode("ns1:bookingClasses", nsmgr) != null) ? onwardBundle.SelectSingleNode("ns1:bookingClasses", nsmgr).InnerText : string.Empty;
                    string onwardbaggage = !string.IsNullOrEmpty(onwardDescription) ? onwardDescription.Split(',').ToList().FirstOrDefault(x => x.Contains("baggage")).Replace("checked baggage", "").Replace("Includes", "").Replace("Kg", "").Trim() : string.Empty;

                    lstBundlesOn.Add(onwServiceId + "#" + onwardFareType + "#" + onwbookingClass + "#" + onwardDescription + "#" + onwardbaggage);
                }
            }
            if (returnBundleService != null && returnBundleService.Count > 0)
            {
                foreach (XmlNode returnBundle in returnBundleService)
                {
                    int retServiceId = Convert.ToInt32(returnBundle.SelectSingleNode("ns1:bunldedServiceId", nsmgr).InnerText);
                    string retFareType = (returnBundle.SelectSingleNode("ns1:bundledServiceName", nsmgr) != null) ? returnBundle.SelectSingleNode("ns1:bundledServiceName", nsmgr).InnerText : string.Empty;
                    string retDescription = (returnBundle.SelectSingleNode("ns1:description", nsmgr) != null) ? returnBundle.SelectSingleNode("ns1:description", nsmgr).InnerText : string.Empty;
                    string retbookingClass = (returnBundle.SelectSingleNode("ns1:bookingClasses", nsmgr) != null) ? returnBundle.SelectSingleNode("ns1:bookingClasses", nsmgr).InnerText : string.Empty;
                    string retbaggage = !string.IsNullOrEmpty(retDescription) ? retDescription.Split(',').ToList().FirstOrDefault(x => x.Contains("baggage")).Replace("checked baggage", "").Replace("Includes", "").Replace("Kg", "").Trim() : string.Empty;

                    lstBundlesRet.Add(retServiceId + "#" + retFareType + "#" + retbookingClass + "#" + retDescription + "#" + retbaggage);
                }
            }
        }

    }
}
