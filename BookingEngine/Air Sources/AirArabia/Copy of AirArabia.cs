using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using CT.Core;
using CT.Configuration;
using System.Xml;
using System.IO;
using System.Net;
using System.Data;
using CT.BookingEngine;
using System.Diagnostics;
using System.Threading;
using System.Web;
namespace CT.BookingEngine.GDS
{
    public class AirArabia
    {
        class SessionData
        {
            public CookieContainer cookie;
            public string transactionID;
            public Dictionary<string, string> fare;//fare for every flight
            public string SelectedResultInfo;
            public Dictionary<string, string> totalPrice;
            public string SelectedTotalPrice;
        }
        public string[][] RPH;
        string airlineCode = "G9";
        string userName = string.Empty;
        string password = string.Empty;
        string code = string.Empty;
        string url = string.Empty;
        string nearByOrigin = string.Empty;
        string nearByDestination = string.Empty;
        string fareXml;
        string totPrice;
        public AirArabia()
        {
            userName = ConfigurationSystem.AirArabiaConfig["UserName"];
            password = ConfigurationSystem.AirArabiaConfig["Password"];
            code = ConfigurationSystem.AirArabiaConfig["Code"];
            url = ConfigurationSystem.AirArabiaConfig["Url"];

        }
        private static string BuildResultKey(SearchResult result)
        {
            int obLength = result.Flights[0].Length;
            StringBuilder key = new StringBuilder();
            key.Append(result.Flights[0][0].DepartureTime.DayOfYear.ToString());
            key.Append(result.Flights[0][0].DepartureTime.ToString("HHmm"));
            key.Append(result.Flights[0][obLength - 1].ArrivalTime.DayOfYear.ToString());
            key.Append(result.Flights[0][obLength - 1].ArrivalTime.ToString("HHmm"));
            if (result.Flights.Length > 1)
            {
                int ibLength = result.Flights[1].Length;
                key.Append(result.Flights[1][0].DepartureTime.DayOfYear.ToString());
                key.Append(result.Flights[1][0].DepartureTime.ToString("HHmm"));
                key.Append(result.Flights[1][ibLength - 1].ArrivalTime.DayOfYear.ToString());
                key.Append(result.Flights[1][ibLength - 1].ArrivalTime.ToString("HHmm"));
            }
            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    key.Append(result.Flights[i][j].FlightNumber);
                    key.Append(result.Flights[i][j].BookingClass);
                }
            }
            return key.ToString();
        }
        public void GetFareQuotes(SearchResult result, string sessionId)
        {
            Trace.TraceInformation("AirArabia.GetFareQuotes entered");
            SessionData sData = (SessionData)Basket.BookingSession[sessionId][airlineCode];//getting fares of selected flight
            if (sData.fare.ContainsKey(BuildResultKey(result)))
            {
                sData.SelectedResultInfo = sData.fare[BuildResultKey(result)];//inserting fare details of selected flight in session for Booking
                sData.SelectedTotalPrice = sData.totalPrice[BuildResultKey(result)];//getting selected total fare for inserting into xml while booking
            }
        }
        /// <summary>
        /// Method to get the search results
        /// </summary>
        /// <param name="searchRequest">Search request Object</param>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public SearchResult[] Search(SearchRequest searchRequest, string sessionId)
        {
            Trace.TraceInformation("AirArabia.Search Enter");
            bool allowSearch = false;
            SearchResult[] searchresult = new SearchResult[0];
            /*Checking if near by airport city is available using config for maping near by city*/
            if (ConfigurationSystem.SectorListConfig.ContainsKey("AirArabiaCityMapping"))
            {
                Dictionary<string, string> sectors = ConfigurationSystem.SectorListConfig["AirArabiaCityMapping"];
                if (sectors.ContainsKey(searchRequest.Segments[0].Destination))
                {
                    nearByDestination = sectors[searchRequest.Segments[0].Destination];
                }
                else
                {
                    nearByDestination = searchRequest.Segments[0].Destination;
                }
                if (sectors.ContainsKey(searchRequest.Segments[0].Origin))
                {
                    nearByOrigin = sectors[searchRequest.Segments[0].Origin];
                }
                else
                {
                    nearByOrigin = searchRequest.Segments[0].Origin;
                }
            }
            else
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaSearch, CoreLogic.Severity.High, 1, "AirArabia: Error While reading sector from AirArabiaconfig : Error" + DateTime.Now, "");
                Trace.TraceError("AirArabia: Error While reading sector from AirArabiaconfig");
            }
            if (ConfigurationSystem.SectorListConfig.ContainsKey("G9"))
            {
                Dictionary<string, string> sectors = ConfigurationSystem.SectorListConfig["G9"];
                if (nearByOrigin != string.Empty || nearByDestination != null)
                {
                    if (sectors.ContainsKey(nearByOrigin))
                    {
                        if (sectors[nearByOrigin].Contains(nearByDestination))
                        {
                            allowSearch = true;
                        }
                    }
                }
                else if (sectors.ContainsKey(searchRequest.Segments[0].Origin))
                {
                    if (sectors[searchRequest.Segments[0].Origin].Contains(searchRequest.Segments[0].Destination))
                    {
                        allowSearch = true;
                    }
                }
            }
            else
            {
                allowSearch = true;
            }
            if (allowSearch)
            {
                string requestforAvailability = GetRequestStringForAvailability(searchRequest);
                string responseOfAvailability = string.Empty;
                CookieContainer cookie = new CookieContainer();
                try
                {
                    responseOfAvailability = GetResponse(requestforAvailability, ref cookie);
                    CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaSearch, CoreLogic.Severity.Normal, 0, "AirArabia SearchResponse:" + responseOfAvailability + " Request XML - " + requestforAvailability + "| " + DateTime.Now, "");
                }
                catch (WebException excep)
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaSearch, CoreLogic.Severity.High, 0, "Web Exception returned from AirArabia. Error Message:" + excep.Message + " | " + DateTime.Now + "| request XML" + requestforAvailability, "");
                    Trace.TraceError("Error: " + excep.Message);
                    throw excep;
                }
                if (responseOfAvailability != null && responseOfAvailability.Length > 0)
                {
                    searchresult = GetSearchResult(responseOfAvailability, searchRequest, sessionId, ref cookie);
                }
                else
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaSearch, CoreLogic.Severity.High, 0, "AirArabia No Result found | Request XML - " + requestforAvailability + DateTime.Now, "");
                    Trace.TraceError("AirArabia search No result found");
                    throw new BookingEngineException("AirArabia search No result found");
                }
            }
            else
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaSearch, CoreLogic.Severity.Normal, 1, "AirArabia sector Not Available for : " + nearByOrigin + "-" + nearByDestination, "0");
            }
            Trace.TraceInformation("AirArabia.Search Exit");
            return searchresult;
        }

        private SearchResult[] GetSearchResult(string resultSt, SearchRequest request, string sessionId, ref CookieContainer cookie)
        {
            Trace.TraceInformation("AirArabia.GetSearchResult Entered");
            SearchResult[] result;
            XmlDocument xmlDoc = new XmlDocument();
            TextReader stringRead = new StringReader(resultSt);
            xmlDoc.Load(stringRead);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
            XmlNode xAvailRS = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirAvailRS", nsmgr);
            XmlNode xErrors = xAvailRS.SelectSingleNode("ns1:Errors", nsmgr);
            if (xErrors != null)
            {
                XmlNodeList xError = xErrors.SelectNodes("ns1:Error", nsmgr);
                foreach (XmlNode xErr in xError)
                {
                    Trace.TraceError("Error due to " + xErr.Attributes["ShortText"].Value.ToString());
                    CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaPriceRetrieve, CoreLogic.Severity.High, 0, "AirArabia: Get Search Results Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString() + "Response :- " + resultSt, "");
                    throw new BookingEngineException("AirArabia: Get Search Results Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString());
                }
            }
            SessionData sessionDta = new SessionData();
            sessionDta.transactionID = xAvailRS.Attributes["TransactionIdentifier"].Value.ToString();
            sessionDta.cookie = cookie;
            sessionDta.fare = new Dictionary<string, string>();
            sessionDta.totalPrice = new Dictionary<string, string>();

            int i = 0;
            XmlNodeList xnodeInfo = xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_AirAvailRS/ns1:OriginDestinationInformation", nsmgr);
            int count = xnodeInfo.Count;
            result = new SearchResult[count];
            RPH = new string[count][];

            foreach (XmlNode xnodeOridinDesiInfo in xnodeInfo)
            {
                bool isReturn = false;
                result[i] = new SearchResult();
                result[i].ResultBookingSource = BookingSource.AirArabia;
                XmlNode xFlightInfo = xnodeOridinDesiInfo.SelectSingleNode("ns1:OriginDestinationOptions", nsmgr);
                XmlNodeList flightNodes = xFlightInfo.SelectNodes("ns1:OriginDestinationOption", nsmgr);
                RPH[i] = new string[flightNodes.Count];
                result[i].FareRules = new List<FareRule>();
                int c = 0;
                List<FlightInfo> outboundFlights = new List<FlightInfo>();
                List<FlightInfo> inboundFlights = new List<FlightInfo>();
                foreach (XmlNode flightNodex in flightNodes)
                {
                    FlightInfo flight = new FlightInfo();
                    flight.ETicketEligible = true;
                    XmlNode flightNode = flightNodex.SelectSingleNode("ns1:FlightSegment", nsmgr);
                    flight.DepartureTime = Convert.ToDateTime(flightNode.Attributes["DepartureDateTime"].Value);
                    flight.ArrivalTime = Convert.ToDateTime(flightNode.Attributes["ArrivalDateTime"].Value);
                    flight.FlightNumber = flightNode.Attributes["FlightNumber"].Value.Substring(2);
                    flight.Airline = flightNode.Attributes["FlightNumber"].Value.Substring(0, 2);

                    flight.Status = "HK";
                    FareRule newFareRule = new FareRule();
                    newFareRule.Airline = Convert.ToString(flightNode.Attributes["FlightNumber"].Value.Substring(0, 2));
                    newFareRule.DepartureTime = Convert.ToDateTime(flightNode.Attributes["DepartureDateTime"].Value);
                    XmlNode FareBasisCodeNode = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirAvailRS/ns1:AAAirAvailRSExt/ns1:PricedItineraries/ns1:PricedItinerary/ns1:AirItineraryPricingInfo/ns1:PTC_FareBreakdowns/ns1:PTC_FareBreakdown/ns1:FareBasisCodes/ns1:FareBasisCode", nsmgr);
                    if (FareBasisCodeNode != null)
                    {
                        newFareRule.FareBasisCode = FareBasisCodeNode.InnerText.ToString();
                    }

                    flight.BookingClass = newFareRule.FareBasisCode.Substring(0, 1);
                    RPH[i][c] = flightNode.Attributes["RPH"].Value.ToString();
                    if (flightNode.SelectSingleNode("ns1:DepartureAirport", nsmgr) != null)
                    {
                        flight.Origin = new Airport(flightNode.SelectSingleNode("ns1:DepartureAirport", nsmgr).Attributes["LocationCode"].Value.ToString());
                        newFareRule.Origin = flightNode.SelectSingleNode("ns1:DepartureAirport", nsmgr).Attributes["LocationCode"].Value.ToString();
                        flight.DepTerminal = flightNode.SelectSingleNode("ns1:DepartureAirport", nsmgr).Attributes["Terminal"].Value.ToString();
                    }
                    if (flightNode.SelectSingleNode("ns1:ArrivalAirport", nsmgr) != null)
                    {
                        flight.Destination = new Airport(flightNode.SelectSingleNode("ns1:ArrivalAirport", nsmgr).Attributes["LocationCode"].Value.ToString());
                        flight.ArrTerminal = flightNode.SelectSingleNode("ns1:ArrivalAirport", nsmgr).Attributes["Terminal"].Value.ToString();
                        newFareRule.Destination = flightNode.SelectSingleNode("ns1:ArrivalAirport", nsmgr).Attributes["LocationCode"].Value.ToString();
                    }
                    if (flight.Origin.AirportCode == request.Segments[0].Destination)
                    {
                        isReturn = true;
                    }
                    if (isReturn)
                    {
                        inboundFlights.Add(flight);
                    }
                    else
                    {
                        outboundFlights.Add(flight);
                    }

                    result[i].FareRules.Add(newFareRule);
                    c++;
                }
                if (inboundFlights.Count > 0)
                {
                    result[i].Flights = new FlightInfo[2][];
                    result[i].Flights[0] = outboundFlights.ToArray();
                    result[i].Flights[1] = inboundFlights.ToArray();
                }
                else
                {
                    result[i].Flights = new FlightInfo[1][];
                    result[i].Flights[0] = outboundFlights.ToArray();
                }
                i++;
            }
            XmlNodeList xPriceItinerary = xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_AirAvailRS/ns1:AAAirAvailRSExt/ns1:PricedItineraries/ns1:PricedItinerary", nsmgr);
            string[] sessionRPH = new string[0];
            foreach (XmlNode xPriceItin in xPriceItinerary)
            {
                int g = 0;
                XmlNodeList xPriceFlightoption = xPriceItin.SelectNodes("ns1:AirItinerary/ns1:OriginDestinationOptions/ns1:OriginDestinationOption/ns1:FlightSegment", nsmgr);
                int seg = 0;
                sessionRPH = new string[xPriceFlightoption.Count];
                string[] flightnoInprice = new string[xPriceFlightoption.Count];
                string[] arrtime = new string[xPriceFlightoption.Count];
                string[] deptime = new string[xPriceFlightoption.Count];
                string segmentsKey = string.Empty;
                foreach (XmlNode segnode in xPriceFlightoption)
                {
                    //XmlNode segment = segnode.SelectSingleNode("ns1:FlightSegment", nsmgr);
                    //sessionRPH[seg] = segment.Attributes["RPH"].Value.ToString();
                    //flightnoInprice[seg] = segment.Attributes["FlightNumber"].Value.ToString();
                    //arrtime[seg] = segment.Attributes["ArrivalDateTime"].Value.ToString();
                    //deptime[seg] = segment.Attributes["DepartureDateTime"].Value.ToString();

                    sessionRPH[seg] = segnode.Attributes["RPH"].Value.ToString();
                    flightnoInprice[seg] = segnode.Attributes["FlightNumber"].Value.ToString();
                    arrtime[seg] = segnode.Attributes["ArrivalDateTime"].Value.ToString();
                    deptime[seg] = segnode.Attributes["DepartureDateTime"].Value.ToString();
                    seg++;
                    segmentsKey += segnode.Attributes["FlightNumber"].Value.ToString() + segnode.Attributes["DepartureDateTime"].Value.ToString() + segnode.Attributes["ArrivalDateTime"].Value.ToString();
                }
                for (int j = 0; j < result.Length; j++)//result count
                {
                    bool st = true;
                    string resultSegmentKey = string.Empty;
                    result[j].EticketEligible = true;
                    for (int f = 0; f < result[j].Flights.Length; f++)
                    {
                        for (int k = 0; k < result[j].Flights[f].Length; k++)
                        {
                            resultSegmentKey += result[j].Flights[f][k].Airline + result[j].Flights[f][k].FlightNumber + result[j].Flights[f][k].DepartureTime.ToString("yyyy-MM-ddTHH:mm:ss") + result[j].Flights[f][k].ArrivalTime.ToString("yyyy-MM-ddTHH:mm:ss");
                        }
                    }
                    if (segmentsKey == resultSegmentKey)
                    {
                        XmlNode xPrice = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirAvailRS/ns1:AAAirAvailRSExt/ns1:PricedItineraries/ns1:PricedItinerary", nsmgr);
                        fareXml = xPrice.InnerXml;
                        XmlNode ItinTotalFareInfo = xPrice.SelectSingleNode("ns1:AirItineraryPricingInfo/ns1:ItinTotalFare", nsmgr);
                        XmlElement ItinTotalFareElement = (XmlElement)ItinTotalFareInfo;
                        //StaticData staticDta = new StaticData();
                        //staticDta.Load("AED");//Loading rate of Exchange of United Arab
                        double roe = Convert.ToDouble(ConfigurationSystem.AirArabiaConfig["ROE"]);
                        string key = BuildResultKey(result[j]);
                        sessionDta.totalPrice.Add(key, ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount").ToString());
                        result[j].BaseFare = Convert.ToDouble(ItinTotalFareElement["ns1:BaseFare"].GetAttribute("Amount")) * roe;
                        result[j].TotalFare = Convert.ToDouble(ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount")) * roe;

                        result[j].Tax = result[j].TotalFare - result[j].BaseFare;
                        XmlNodeList priceDetail = xPrice.SelectNodes("ns1:AirItineraryPricingInfo/ns1:PTC_FareBreakdowns/ns1:PTC_FareBreakdown", nsmgr);
                        int paxType = 0;
                        if (request.AdultCount > 0)
                        {
                            paxType++;
                        }
                        if (request.ChildCount > 0)
                        {
                            paxType++;
                        }
                        if (request.SeniorCount > 0)
                        {
                            paxType++;
                        }
                        if (request.InfantCount > 0)
                        {
                            paxType++;
                        }
                        result[j].FareBreakdown = new Fare[paxType];
                        int f = 0;
                        foreach (XmlNode paxtype in priceDetail)
                        {
                            XmlNode xPassengerFare = paxtype.SelectSingleNode("ns1:PassengerFare", nsmgr);
                            XmlNode xPassengerType = paxtype.SelectSingleNode("ns1:PassengerTypeQuantity", nsmgr);
                            if ((request.AdultCount != 0 || request.SeniorCount != 0) && xPassengerType.Attributes["Code"].Value.ToString() == "ADT")
                            {
                                //StaticData staticDta = new StaticData();
                                //staticDta.Load("AED");//Loading rate of Exchange of United Arab
                                if (request.AdultCount != 0)
                                {
                                    result[j].FareBreakdown[f] = new Fare();
                                    result[j].FareBreakdown[f].PassengerType = PassengerType.Adult;
                                    result[j].FareBreakdown[f].PassengerCount = request.AdultCount;

                                    XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                                    result[j].FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * roe;
                                    XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                                    result[j].FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * roe;
                                    f++;
                                }
                                if (request.SeniorCount != 0)
                                {
                                    result[j].FareBreakdown[f] = new Fare();
                                    result[j].FareBreakdown[f].PassengerType = PassengerType.Senior;
                                    result[j].FareBreakdown[f].PassengerCount = request.SeniorCount;
                                    XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                                    result[j].FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * roe;
                                    XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                                    result[j].FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * roe;
                                    f++;
                                }
                            }
                            else if (request.ChildCount != 0 && xPassengerType.Attributes["Code"].Value.ToString() == "CHD")
                            {
                                result[j].FareBreakdown[f] = new Fare();
                                result[j].FareBreakdown[f].PassengerType = PassengerType.Child;
                                result[j].FareBreakdown[f].PassengerCount = request.ChildCount;
                                XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                                result[j].FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * roe;
                                XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                                result[j].FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * roe;
                                f++;
                            }
                            else if (request.InfantCount != 0 && xPassengerType.Attributes["Code"].Value.ToString() == "INF")
                            {
                                result[j].FareBreakdown[f] = new Fare();
                                result[j].FareBreakdown[f].PassengerType = PassengerType.Infant;
                                result[j].FareBreakdown[f].PassengerCount = request.InfantCount;
                                XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                                result[j].FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * roe;
                                XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                                result[j].FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * Convert.ToDouble(result[j].FareBreakdown[f].PassengerCount) * roe;
                                f++;
                            }
                        }
                    }
                    else
                    {
                        sessionRPH = GetFare(ref result[j], request, ref sessionDta, ref RPH[j], ref fareXml, sessionId, ref cookie);
                    }
                    string keyfare = BuildResultKey(result[j]);
                    sessionDta.fare.Add(keyfare, fareXml);
                }
            }
            Trace.TraceInformation("AirArabia.GetSearchResult Exit");
            Basket.BookingSession[sessionId].Add(airlineCode, (object)sessionDta);
            return result;
        }

        private string[] GetFare(ref SearchResult result, SearchRequest request, ref SessionData data, ref string[] rph, ref string fareXml, string sessionId, ref CookieContainer cookie)
        {
            string[] st = new string[0];
            int arrlen = rph.Length;
            string requestPrice = GetPriceString(result, request, data, rph);
            string response = string.Empty;
            try
            {
                response = GetResponse(requestPrice, ref cookie);
                CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaPriceRetrieve, CoreLogic.Severity.Normal, 0, "AirArabia GetPrice Response. Request: " + requestPrice + "| Response: " + response + " | Time: " + DateTime.Now, "");
            }
            catch (WebException excep)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaPriceRetrieve, CoreLogic.Severity.High, 0, "Web Exception returned from AirArabia in GetPrice. Error Message:" + excep.Message + " | " + DateTime.Now + "| request XML" + requestPrice, "");
                Trace.TraceError("Error: " + excep.Message);
                throw excep;
            }
            st = ReadPrice(response, ref result, request, ref data, ref fareXml);
            return st;
        }

        private string[] ReadPrice(string responseStr, ref SearchResult result, SearchRequest request, ref SessionData data, ref string fareXml)//reading for price if not given
        {
            XmlDocument xmlDoc = new XmlDocument();
            TextReader stringRead = new StringReader(responseStr);
            xmlDoc.Load(stringRead);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");

            XmlNode xAvailRS = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS", nsmgr);
            XmlNode xErrors = xAvailRS.SelectSingleNode("ns1:Errors", nsmgr);
            if (xErrors != null)
            {
                XmlNodeList xError = xErrors.SelectNodes("ns1:Error", nsmgr);
                foreach (XmlNode xErr in xError)
                {
                    Trace.TraceError("Error due to " + xErr.Attributes["ShortText"].Value.ToString());
                    CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaPriceRetrieve, CoreLogic.Severity.High, 0, "AirArabia: Get Tax Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString() + "Response :- " + responseStr, "");
                    throw new BookingEngineException("AirArabia: Get Tax Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString());
                }
            }
            XmlNodeList xPriceSegments = xmlDoc.SelectNodes("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS/ns1:PricedItineraries/ns1:PricedItinerary/ns1:OriginDestinationOptions/ns1:OriginDestinationOption", nsmgr);

            string[] sessionRPH = new string[xPriceSegments.Count];
            int s = 0;
            foreach (XmlNode segment in xPriceSegments)
            {

                XmlNode flightSegment = segment.SelectSingleNode("ns1:FlightSegment", nsmgr);
                sessionRPH[s] = (string)flightSegment.Attributes["RPH"].Value.ToString();
            }

            XmlNode xPrice = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirPriceRS/ns1:PricedItineraries/ns1:PricedItinerary", nsmgr);
            fareXml = xPrice.InnerXml.ToString();
            XmlNode ItinTotalFareInfo = xPrice.SelectSingleNode("ns1:AirItineraryPricingInfo/ns1:ItinTotalFare", nsmgr);
            XmlElement ItinTotalFareElement = (XmlElement)ItinTotalFareInfo;
            //StaticData staticDta = new StaticData();
            //staticDta.Load("AED");//Loading rate of Exchange of United Arab
            double roe = Convert.ToDouble(ConfigurationSystem.AirArabiaConfig["ROE"]);
            result.BaseFare = Convert.ToDouble(ItinTotalFareElement["ns1:BaseFare"].GetAttribute("Amount")) * roe;
            result.TotalFare = Convert.ToDouble(ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount")) * roe;
            string key = BuildResultKey(result);
            data.totalPrice.Add(key, ItinTotalFareElement["ns1:TotalFare"].GetAttribute("Amount").ToString());
            result.Tax = result.TotalFare - result.BaseFare;
            XmlNodeList priceDetail = xPrice.SelectNodes("ns1:AirItineraryPricingInfo/ns1:PTC_FareBreakdowns/ns1:PTC_FareBreakdown", nsmgr);
            int paxType = 0;
            if (request.AdultCount > 0)
            {
                paxType++;
            }
            if (request.ChildCount > 0)
            {
                paxType++;
            }
            if (request.SeniorCount > 0)
            {
                paxType++;
            }
            if (request.InfantCount > 0)
            {
                paxType++;
            }

            result.FareBreakdown = new Fare[paxType];
            int f = 0;
            foreach (XmlNode paxtype in priceDetail)
            {
                XmlNode xPassengerFare = paxtype.SelectSingleNode("ns1:PassengerFare", nsmgr);
                XmlNode xPassengerType = paxtype.SelectSingleNode("ns1:PassengerTypeQuantity", nsmgr);

                if ((request.AdultCount != 0 || request.SeniorCount != 0) && xPassengerType.Attributes["Code"].Value.ToString() == "ADT")
                {

                    if (request.AdultCount != 0)//for adult
                    {
                        result.FareBreakdown[f] = new Fare();
                        result.FareBreakdown[f].PassengerType = PassengerType.Adult;
                        result.FareBreakdown[f].PassengerCount = request.AdultCount;

                        XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                        result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * roe;
                        XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                        result.FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * roe;
                        f++;
                    }
                    if (request.SeniorCount != 0)
                    {
                        result.FareBreakdown[f] = new Fare();
                        result.FareBreakdown[f].PassengerType = PassengerType.Adult;
                        result.FareBreakdown[f].PassengerCount = request.AdultCount;

                        XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                        result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * roe;
                        XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                        result.FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * roe;
                        f++;
                    }
                }
                else if (request.ChildCount != 0 && xPassengerType.Attributes["Code"].Value.ToString() == "CHD")
                {
                    result.FareBreakdown[f] = new Fare();
                    result.FareBreakdown[f].PassengerType = PassengerType.Child;
                    result.FareBreakdown[f].PassengerCount = request.ChildCount;

                    XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                    result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * roe;
                    XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                    result.FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * roe;
                    f++;
                }
                else if (request.InfantCount != 0 && xPassengerType.Attributes["Code"].Value.ToString() == "INF")
                {
                    result.FareBreakdown[f] = new Fare();
                    result.FareBreakdown[f].PassengerType = PassengerType.Infant;
                    result.FareBreakdown[f].PassengerCount = request.InfantCount;

                    XmlNode xBaseFare = xPassengerFare.SelectSingleNode("ns1:BaseFare", nsmgr);
                    result.FareBreakdown[f].BaseFare = Convert.ToDouble(xBaseFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * roe;
                    XmlNode xTotalFare = xPassengerFare.SelectSingleNode("ns1:TotalFare", nsmgr);
                    result.FareBreakdown[f].TotalFare = Convert.ToDouble(xTotalFare.Attributes["Amount"].Value) * result.FareBreakdown[f].PassengerCount * roe;
                    //result.FareBreakdown[f].Tax = Convert.ToDouble(result.FareBreakdown[f].TotalFare - result.FareBreakdown[f].BaseFare);
                    f++;
                }
            }
            return sessionRPH;
        }

        private string GetPriceString(SearchResult result, SearchRequest request, SessionData data, string[] rph)//getting price string if not available
        {
            //Creating Xml For price here using result object
            StringBuilder priceReq = new StringBuilder();

            XmlWriter writer = XmlTextWriter.Create(priceReq);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");
            //getting commom header

            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-26506823");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns1", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns1", "OTA_AirPriceRQ", null);
            writer.WriteAttributeString("EchoToken", "11810113621255-577396696");
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
            writer.WriteAttributeString("Version", "20061.00");
            writer.WriteAttributeString("TransactionIdentifier", data.transactionID);
            #region POSDetailFix
            writer.WriteStartElement("ns1", "POS", null);
            writer.WriteStartElement("ns1", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
            writer.WriteStartElement("ns1", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", userName);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("ns1", "AirItinerary", null);
            if (result.Flights.Length > 1)
            {
                writer.WriteAttributeString("DirectionInd", "Return");
            }
            else
            {
                writer.WriteAttributeString("DirectionInd", "OneWay");
            }
            writer.WriteStartElement("ns1", "OriginDestinationOptions", null);
            int countRPH = 0;
            for (int f = 0; f < result.Flights.Length; f++)
            {
                writer.WriteStartElement("ns1", "OriginDestinationOption", null);
                for (int r = 0; r < result.Flights[f].Length; r++)
                {
                    writer.WriteStartElement("ns1", "FlightSegment", null);
                    writer.WriteAttributeString("ArrivalDateTime", result.Flights[f][r].ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                    writer.WriteAttributeString("DepartureDateTime", result.Flights[f][r].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                    writer.WriteAttributeString("FlightNumber", result.Flights[f][r].Airline + result.Flights[f][r].FlightNumber);
                    writer.WriteAttributeString("RPH", rph[countRPH]);
                    writer.WriteStartElement("ns1", "DepartureAirport", null);
                    writer.WriteAttributeString("LocationCode", result.Flights[f][r].Origin.AirportCode.ToString());
                    writer.WriteAttributeString("Terminal", result.Flights[f][r].DepTerminal.ToString());
                    writer.WriteEndElement();
                    writer.WriteStartElement("ns1", "ArrivalAirport", null);
                    writer.WriteAttributeString("LocationCode", result.Flights[f][r].Destination.AirportCode.ToString());
                    writer.WriteAttributeString("Terminal", result.Flights[f][r].ArrTerminal.ToString());
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    countRPH++;
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "TravelerInfoSummary", null);
            writer.WriteStartElement("ns1", "AirTravelerAvail", null);
            int count = request.AdultCount + request.SeniorCount;
            if (count != 0)
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "ADT");
                writer.WriteAttributeString("Quantity", count.ToString());
                writer.WriteEndElement();
            }
            if (request.ChildCount != 0)
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "CHD");
                writer.WriteAttributeString("Quantity", request.ChildCount.ToString());
                writer.WriteEndElement();
            }
            if (request.InfantCount != 0 && request.AdultCount + request.SeniorCount >= request.InfantCount)
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "INF");
                writer.WriteAttributeString("Quantity", request.InfantCount.ToString());
                writer.WriteEndElement();
            }
            if (request.AdultCount + request.SeniorCount < request.InfantCount)
            {
                throw new BookingEngineException("AirArabia Search Infant Count Can't greater than Adult Count");
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
            return priceReq.ToString();
        }


        private string GetResponse(string Request, ref CookieContainer cookie)
        {
            string request = Request;
            string requestUrl = url;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(requestUrl);
            req.Method = "POST";
            req.ContentLength = request.Length;
            req.ContentType = "text/xml;charset=\"utf-8\"";
            req.Accept = "text/xml";
            req.KeepAlive = false;
            req.CookieContainer = cookie;
            Stream str = req.GetRequestStream();
            StreamWriter writer = new StreamWriter(str);
            writer.Write(request);
            writer.Flush();
            Stream resp = Stream.Null;
            WebResponse response = null;
            HttpWebResponse objResponse;
            objResponse = (HttpWebResponse)req.GetResponse();
            string strResult;
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                strResult = sr.ReadToEnd();
                sr.Close();
            }
            return strResult;
        }

        private string GetRequestStringForAvailability(SearchRequest searchRequest)//done
        {
            StringBuilder availReq = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(availReq);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-26506823");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns1", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns1", "OTA_AirAvailRQ", null);
            writer.WriteAttributeString("EchoToken", "11810113621255-577396696");//check later
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("Target", "TEST");
            writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
            writer.WriteAttributeString("Version", "20061.00");//fix
            #region POSDetailFix
            writer.WriteStartElement("ns1", "POS", null);
            writer.WriteStartElement("ns1", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
            writer.WriteStartElement("ns1", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", userName);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion

            writer.WriteStartElement("ns1", "OriginDestinationInformation", null);
            writer.WriteStartElement("ns1", "DepartureDateTime", null);
            writer.WriteValue(searchRequest.Segments[0].PreferredDepartureTime);//to be inserted if return
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "OriginLocation", null);
            writer.WriteAttributeString("LocationCode", nearByOrigin);//To be inserted if return
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "DestinationLocation", null);
            writer.WriteAttributeString("LocationCode", nearByDestination);//TO be inserted if return
            writer.WriteEndElement();
            writer.WriteEndElement();

            if (searchRequest.Segments.Length > 1 && searchRequest.Segments[1] != null && searchRequest.Segments[1].PreferredDepartureTime > DateTime.MinValue)
            {
                writer.WriteStartElement("ns1", "OriginDestinationInformation", null);
                writer.WriteStartElement("ns1", "DepartureDateTime", null);
                writer.WriteValue(searchRequest.Segments[1].PreferredDepartureTime);//to be inserted if return
                writer.WriteEndElement();
                writer.WriteStartElement("ns1", "OriginLocation", null);
                writer.WriteAttributeString("LocationCode",nearByDestination);//To be inserted if return
                writer.WriteEndElement();
                writer.WriteStartElement("ns1", "DestinationLocation", null);
                writer.WriteAttributeString("LocationCode", nearByOrigin);//TO be inserted if return
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            writer.WriteStartElement("ns1", "TravelerInfoSummary", null);
            writer.WriteStartElement("ns1", "AirTravelerAvail", null);
            if (searchRequest.AdultCount != 0 || searchRequest.SeniorCount != 0)
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "ADT");
                int adult = searchRequest.AdultCount + searchRequest.SeniorCount;// both seniors and adults are considered as adults
                writer.WriteAttributeString("Quantity", adult.ToString());
                writer.WriteEndElement();
            }
            if (searchRequest.ChildCount != 0)
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "CHD");
                writer.WriteAttributeString("Quantity", searchRequest.ChildCount.ToString());//for chlidern
                writer.WriteEndElement();
            }
            if (searchRequest.InfantCount != 0 && (searchRequest.AdultCount + searchRequest.SeniorCount >= searchRequest.InfantCount))
            {
                writer.WriteStartElement("ns1", "PassengerTypeQuantity", null);
                writer.WriteAttributeString("Code", "INF");
                writer.WriteAttributeString("Quantity", searchRequest.InfantCount.ToString());// for infants
                writer.WriteEndElement();
            }
            if ((searchRequest.AdultCount + searchRequest.SeniorCount) < searchRequest.InfantCount)
            {
                throw new BookingEngineException("AirArabia Search Infant Count Can't greater than Adult Count");
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
            return availReq.ToString();
        }

        public FlightItinerary RetrieveItinerary(string pnr)
        {
            Trace.TraceInformation("AirArabia.RetrieveItinerary Enter");
            FlightItinerary itinerary = new FlightItinerary();
            CookieContainer cookie = new CookieContainer();
            string reqPNR = GetRetrieveMessage(pnr);
            string resPNR = GetResponse(reqPNR, ref cookie);
            if (resPNR != null)
            {
                itinerary = ReadRetrieveResponse(resPNR, pnr);
            }
            else
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaRetrieveByPNR, CoreLogic.Severity.High, 0, "Retrieve PNR failed| " + DateTime.Now, "");
                Trace.TraceError("AirArabia Booking No result found");
                throw new BookingEngineException("AirArabia Retreive Failed");
            }
            Trace.TraceInformation("AirArabia.RetrieveItinerary Exit");
            return itinerary;
        }

        public FlightItinerary ReadRetrieveResponse(string response, string pnr)
        {
            Trace.TraceInformation("AirArabia.readimportPnrres Enter");
            FlightItinerary itinerary = new FlightItinerary();
            XmlDocument xmlDoc = new XmlDocument();
            TextReader stringRead = new StringReader(response);
            xmlDoc.Load(stringRead);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
            nsmgr.AddNamespace("ns2", "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05");
            XmlNode xerror = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirBookRS/ns1:Errors/ns1:Error", nsmgr);
            if (xerror != null)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaRetrieveByPNR, CoreLogic.Severity.High, 0, "Exception returned from AirArabia. Error Message:" + xerror.Attributes["ShortText"].Value.ToString() + " | " + DateTime.Now + "| request XML" + response, "");
                Trace.TraceError("Error: " + xerror.Attributes["ShortText"].Value.ToString());
                throw new BookingEngineException(" Error: " + xerror.Attributes["ShortText"].Value.ToString());
            }
            else
            {
                itinerary.AirlineCode = airlineCode;
                itinerary.PNR = pnr;
                XmlNode xPnrRS = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirBookRS/ns1:AirReservation", nsmgr);
                XmlNode flightInfo = xPnrRS.SelectSingleNode("ns1:AirItinerary/ns1:OriginDestinationOptions", nsmgr);
                XmlNodeList flights = flightInfo.SelectNodes("ns1:OriginDestinationOption", nsmgr);
                XmlNodeList flightWithSegments = flightInfo.SelectNodes("ns1:OriginDestinationOption/ns1:FlightSegment", nsmgr);
                int count = flightWithSegments.Count;
                itinerary.Segments = new FlightInfo[count];
                int i = 0;
                bool setOriDes = true;
                foreach (XmlNode FlightSeg in flights)
                {
                    itinerary.FlightBookingSource = BookingSource.AirArabia;//Added later by Ritika
                    XmlNodeList segment = FlightSeg.SelectNodes("ns1:FlightSegment", nsmgr);
                    foreach (XmlNode seg in segment)
                    {
                        itinerary.Segments[i] = new FlightInfo();
                        itinerary.Segments[i].BookingClass = "P";//AS observed most of the time Booking Class from AirArabia Side is "P"
                        if (seg.HasChildNodes)
                        {
                            itinerary.Segments[i].ArrivalTime = Convert.ToDateTime(seg.Attributes["ArrivalDateTime"].Value.ToString());
                            itinerary.Segments[i].DepartureTime = Convert.ToDateTime(seg.Attributes["DepartureDateTime"].Value.ToString());
                            itinerary.Segments[i].FlightNumber = seg.Attributes["FlightNumber"].Value.Substring(2);
                            itinerary.Segments[i].Airline = seg.Attributes["FlightNumber"].Value.Substring(0, 2);
                            itinerary.AirlineCode = seg.Attributes["FlightNumber"].Value.Substring(0, 2);
                            if (seg.SelectSingleNode("ns1:DepartureAirport", nsmgr) != null)
                            {
                                itinerary.Segments[i].Origin = new Airport(seg.SelectSingleNode("ns1:DepartureAirport", nsmgr).Attributes["LocationCode"].Value.ToString());
                                itinerary.Segments[i].DepTerminal = seg.SelectSingleNode("ns1:DepartureAirport", nsmgr).Attributes["Terminal"].Value.ToString();
                            }
                            if (seg.SelectSingleNode("ns1:ArrivalAirport", nsmgr) != null)
                            {
                                itinerary.Segments[i].Destination = new Airport(seg.SelectSingleNode("ns1:ArrivalAirport", nsmgr).Attributes["LocationCode"].Value.ToString());
                                itinerary.Segments[i].ArrTerminal = seg.SelectSingleNode("ns1:ArrivalAirport", nsmgr).Attributes["Terminal"].Value.ToString();
                            }
                        }
                        i++;
                    }
                    if (setOriDes)
                    {
                        setOriDes = false;
                        itinerary.Origin = Convert.ToString(itinerary.Segments[0].Origin.CityCode);
                        itinerary.Destination = Convert.ToString(itinerary.Segments[i - 1].Destination.CityCode);
                    }
                }
                itinerary.FareRules = GetFareRule(itinerary.Origin, itinerary.Destination, "P");
                XmlNode price = xPnrRS.SelectSingleNode("ns1:PriceInfo/ns1:ItinTotalFare", nsmgr);
                XmlNode travelersinfo = xPnrRS.SelectSingleNode("ns1:TravelerInfo", nsmgr);
                XmlNodeList travelers = travelersinfo.SelectNodes("ns1:AirTraveler", nsmgr);
                int paxcount = travelers.Count;
                int pax = 0;
                itinerary.Passenger = new FlightPassenger[paxcount];
                foreach (XmlNode paxInfo in travelers)
                {
                    itinerary.Passenger[pax] = new FlightPassenger();
                    if (paxInfo.Attributes["PassengerTypeCode"].Value.ToString() == "ADT")
                    {
                        itinerary.Passenger[pax].Type = PassengerType.Adult;
                    }
                    if (paxInfo.Attributes["PassengerTypeCode"].Value.ToString() == "CHD")
                    {
                        itinerary.Passenger[pax].Type = PassengerType.Child;
                    }
                    if (paxInfo.Attributes["PassengerTypeCode"].Value.ToString() == "INF")
                    {
                        itinerary.Passenger[pax].Type = PassengerType.Infant;
                    }
                    itinerary.Passenger[pax].Price = new PriceAccounts();
                    //itinerary.Passenger[pax].Price.PublishedFare = Convert.ToDecimal(price.SelectSingleNode("ns1:BaseFare", nsmgr).Attributes["Amount"].Value);
                    //itinerary.Passenger[pax].Price.Tax = Convert.ToDecimal(price.SelectSingleNode("ns1:Taxes/ns1:Tax", nsmgr).Attributes["Amount"].Value);

                    XmlNode paxDetail = paxInfo.SelectSingleNode("ns1:PersonName", nsmgr);
                    if (paxDetail.HasChildNodes)
                    {
                        itinerary.Passenger[pax].FirstName = paxDetail.SelectSingleNode("ns1:GivenName", nsmgr).InnerText.ToString();
                        itinerary.Passenger[pax].LastName = paxDetail.SelectSingleNode("ns1:Surname", nsmgr).InnerText.ToString();
                        if (paxDetail.SelectSingleNode("ns1:NameTitle", nsmgr) != null)
                        {
                            itinerary.Passenger[pax].Title = paxDetail.SelectSingleNode("ns1:NameTitle", nsmgr).InnerText.ToString();
                        }
                        else
                        {
                            itinerary.Passenger[pax].Title = "";
                        }
                    }
                    if (paxInfo.SelectSingleNode("ns1:Telephone", nsmgr) != null)
                    {
                        itinerary.Passenger[pax].CellPhone = paxInfo.SelectSingleNode("ns1:Telephone", nsmgr).Attributes["PhoneNumber"].Value.ToString();
                    }
                    pax++;
                }
                XmlNode ticketinfo = xPnrRS.SelectSingleNode("ns1:Ticketing", nsmgr);
                //itinerary.PaymentMode = ModeOfPayment.Cash;
                if (ticketinfo.SelectSingleNode("ns1:TicketAdvisory", nsmgr) != null)
                {
                    itinerary.TicketAdvisory = ticketinfo.SelectSingleNode("ns1:TicketAdvisory", nsmgr).InnerText.ToString();
                }
            }
            Trace.TraceInformation("AirArabia.readimportPnrres Exit");
            return itinerary;
        }

        private string GetRetrieveMessage(string pnr)
        {
            Trace.TraceInformation("AirArabia.resByPNRstring Enter");
            StringBuilder ReqByPNR = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(ReqByPNR);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");
            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-26506823");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion
            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns1", null, "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05");
            writer.WriteAttributeString("xmlns", "ns2", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns2", "OTA_ReadRQ", null);
            writer.WriteAttributeString("EchoToken", "11810128220460-413253889");
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
            writer.WriteAttributeString("Version", "20061.00");//fix
            #region POSDetailFix
            writer.WriteStartElement("ns2", "POS", null);
            writer.WriteStartElement("ns2", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
            writer.WriteStartElement("ns2", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", "WSCOZMO");
            writer.WriteEndElement();
            writer.WriteStartElement("ns2", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion
            writer.WriteStartElement("ns2", "ReadRequests", null);
            writer.WriteStartElement("ns2", "ReadRequest", null);
            writer.WriteStartElement("ns2", "UniqueID", null);
            writer.WriteAttributeString("ID", pnr);
            writer.WriteAttributeString("Type", "14");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "AAReadRQExt", null);
            writer.WriteStartElement("ns1", "AALoadDataOptions", null);
            writer.WriteStartElement("ns1", "LoadTravelerInfo", null);
            writer.WriteValue("true");
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "LoadAirItinery", null);
            writer.WriteValue("true");
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "LoadPriceInfoTotals", null);
            writer.WriteValue("true");
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "LoadFullFilment", null);
            writer.WriteValue("true");
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "LoadPTC_FareBreakdowns", null);
            writer.WriteValue("true");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
            Trace.TraceInformation("AirArabia.resByPNRstring Exit");
            return ReqByPNR.ToString();
        }

        public BookingResponse Booking(FlightItinerary itinerary, int bookingAgencyId, string sessionId)
        {
            Trace.TraceInformation("AirArabia.Booking Enter");
            BookingResponse bookingResponse = new BookingResponse();
            SessionData sessiondta = (SessionData)Basket.BookingSession[sessionId][airlineCode];
            string request = GetBookingRequest(itinerary, bookingAgencyId, sessionId);
            string response = string.Empty;
            try
            {
                response = GetResponse(request, ref sessiondta.cookie);
                CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaBooking, CoreLogic.Severity.High, 0, "AirArabia Booking | Request : " + request + "| Response :-" + response + "| Time" + DateTime.Now, "");
            }
            catch (WebException ex)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaBooking, CoreLogic.Severity.High, 0, "Web Exception Returned from AirArabia | Error = " + ex.Message + "| Request XML" + request + "| Time= " + DateTime.Now, "");
                throw ex;
            }
            if (response != null && response.Length > 0)
            {
                bookingResponse = ReadBookingResponse(response, itinerary);
            }
            else
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaBooking, CoreLogic.Severity.High, 0, "AirArabia Booking Failed| Null Response from AirArabia| Time" + DateTime.Now, "");
                bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "AirArabia Booking Failed| Null Response from AirArabia", string.Empty);
            }
            Trace.TraceInformation("AirArabia.Booking Exit");
            return bookingResponse;
        }

        private string GetBookingRequest(FlightItinerary itinerary, int bookingAgencyId, string sessionId)//not complete yet
        {
            Trace.TraceInformation("AirArabia.GetRequestStringForBooking Entered");
            StringBuilder bookingReq = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(bookingReq);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");
            SessionData sessiondta = (SessionData)Basket.BookingSession[sessionId][airlineCode];
            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            //writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-26506823");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            //writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion
            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns1", null, "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05");
            writer.WriteAttributeString("xmlns", "ns2", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns2", "OTA_AirBookRQ", null);
            writer.WriteAttributeString("EchoToken", "11810120749379731256589");
            writer.WriteAttributeString("TransactionIdentifier", sessiondta.transactionID);//new for every transaction
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
            writer.WriteAttributeString("Version", "20061.00");
            #region POS
            writer.WriteStartElement("ns2", "POS", null);
            writer.WriteStartElement("ns2", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
            writer.WriteStartElement("ns2", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", userName);
            writer.WriteEndElement();
            writer.WriteStartElement("ns2", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion
            string pricedetail = sessiondta.SelectedResultInfo;
            pricedetail = pricedetail.Replace("ns1", "ns2");
            pricedetail = pricedetail.Replace("<ns2:AirItinerary xmlns:ns2=\"http://www.opentravel.org/OTA/2003/05\">", "<ns2:AirItinerary>");
            pricedetail = pricedetail.Replace("<ns2:AirItineraryPricingInfo PricingSource=\"Published\" xmlns:ns2=\"http://www.opentravel.org/OTA/2003/05\">", "<ns2:PriceInfo>");
            pricedetail = pricedetail.Replace("<ns2:AirItineraryPricingInfo PricingSource=\"Published\">", "<ns2:PriceInfo>");
            pricedetail = pricedetail.Replace("AirItineraryPricingInfo", "PriceInfo");
            writer.WriteElementString("PriceDetail", "");
            int noOfPax = itinerary.Passenger.Length;
            //Travler Info
            writer.WriteStartElement("ns2", "TravelerInfo", null);
            int noOfTravlers = itinerary.Passenger.Length;
            int adult = 0;//Counting No of Adults for assgning to infants
            //int inf = 0;
            for (int pax = 0; pax < noOfTravlers; pax++)
            {
                writer.WriteStartElement("ns2", "AirTraveler", null);
                if (itinerary.Passenger[pax].DateOfBirth > DateTime.MinValue && itinerary.Passenger[pax].Type!=PassengerType.Infant)
                {
                    writer.WriteAttributeString("BirthDate", itinerary.Passenger[pax].DateOfBirth.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                }
                if (itinerary.Passenger[pax].Type == PassengerType.Adult || itinerary.Passenger[pax].Type == PassengerType.Senior)
                {
                    writer.WriteAttributeString("PassengerTypeCode", "ADT");
                }
                if (itinerary.Passenger[pax].Type == PassengerType.Child)
                {
                    writer.WriteAttributeString("PassengerTypeCode", "CHD");
                    //writer.WriteAttributeString("BirthDate", itinerary.Passenger[pax].DateOfBirth.ToString("yyyy-MM-dd'T'HH:mm:ss"));
                }
                if (itinerary.Passenger[pax].Type == PassengerType.Infant)
                {
                    writer.WriteAttributeString("PassengerTypeCode", "INF");

                }                
                writer.WriteStartElement("ns2", "PersonName", null);
                writer.WriteStartElement("ns2", "GivenName", null);
                writer.WriteValue(itinerary.Passenger[pax].FirstName);
                writer.WriteEndElement();
                writer.WriteStartElement("ns2", "Surname", null);
                writer.WriteValue(itinerary.Passenger[pax].LastName);
                writer.WriteEndElement();

                writer.WriteStartElement("ns2", "NameTitle", null);
                if (itinerary.Passenger[pax].Title.ToUpper() == "MR" || itinerary.Passenger[pax].Title.ToUpper() == "MS" || itinerary.Passenger[pax].Title.ToUpper() == "MRS" || itinerary.Passenger[pax].Title.ToUpper() == "MSTR" || itinerary.Passenger[pax].Title.ToUpper() == "MISS")
                {
                    writer.WriteValue(itinerary.Passenger[pax].Title.ToUpper());
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
                if (itinerary.Passenger[pax].Type == PassengerType.Adult)
                {
                    writer.WriteStartElement("ns2", "Telephone", null);
                    writer.WriteAttributeString("AreaCityCode", "11");//TO be inserted
                    writer.WriteAttributeString("CountryAccessCode", "91");// needed access code here
                    writer.WriteAttributeString("PhoneNumber", itinerary.Passenger[pax].CellPhone);
                    writer.WriteEndElement();
                }
                writer.WriteStartElement("ns2", "Address", null);
                writer.WriteStartElement("ns2", "CountryName", null);
                if (itinerary.Passenger[0].Country.CountryCode != null && itinerary.Passenger[0].Country.CountryCode.Length != 0)
                {
                    writer.WriteAttributeString("Code", itinerary.Passenger[pax].Country.CountryCode);
                }
                else
                {
                    writer.WriteAttributeString("Code", "IN");
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
                if (itinerary.Passenger[pax].Type == PassengerType.Adult)
                {
                    writer.WriteStartElement("ns2", "Document", null);
                    ////writer.WriteAttributeString("DocID", itinerary.Passenger[pax].PassportNo);
                    ////writer.WriteAttributeString("DocType", "PSPT");
                    writer.WriteAttributeString("DocHolderNationality", itinerary.Passenger[pax].Nationality.CountryCode);
                    //writer.WriteStartElement("ns2", "DocHolderName", null);
                    //writer.WriteValue(itinerary.Passenger[pax].FirstName);
                    //writer.WriteEndElement();
                    writer.WriteEndElement();
                }
                if (itinerary.Passenger[pax].Type == PassengerType.Adult || itinerary.Passenger[pax].Type == PassengerType.Senior)
                {
                    adult++;
                    writer.WriteStartElement("ns2", "TravelerRefNumber", null);
                    writer.WriteAttributeString("RPH", "A" + (pax + 1).ToString());
                    writer.WriteEndElement();
                }
                if (itinerary.Passenger[pax].Type == PassengerType.Child)
                {
                    writer.WriteStartElement("ns2", "TravelerRefNumber", null);
                    writer.WriteAttributeString("RPH", "C" + (pax + 1).ToString());
                    writer.WriteEndElement();
                }

                if (itinerary.Passenger[pax].Type == PassengerType.Infant)
                {
                    writer.WriteStartElement("ns2", "TravelerRefNumber", null);
                    writer.WriteAttributeString("RPH", "I" + (pax + 1).ToString() + "/A" + adult.ToString());
                    adult--;
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            //fullfillment
            writer.WriteStartElement("ns2", "Fulfillment", null);
            writer.WriteStartElement("ns2", "PaymentDetails", null);
            writer.WriteStartElement("ns2", "PaymentDetail", null);
            writer.WriteStartElement("ns2", "DirectBill", null);
            writer.WriteStartElement("ns2", "CompanyName", null);
            writer.WriteAttributeString("Code", code);//Of Travel Boutique Online
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("ns2", "PaymentAmount", null);
            XmlDocument xmlDoc = new XmlDocument();
            writer.WriteAttributeString("Amount", sessiondta.SelectedTotalPrice);//TOtal amt be inserted
            writer.WriteAttributeString("CurrencyCode", "AED");
            writer.WriteAttributeString("DecimalPlaces", "2");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #region AAAirBookRQExt
            //  Agency agency = new Agency(bookingAgencyId);
            writer.WriteStartElement("ns1", "AAAirBookRQExt", null);
            writer.WriteStartElement("ns1", "ContactInfo", null);
            writer.WriteStartElement("ns1", "PersonName", null);
            writer.WriteStartElement("ns1", "Title", null);
            writer.WriteValue(itinerary.Passenger[0].Title.ToUpper());
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "FirstName", null);
            writer.WriteValue(itinerary.Passenger[0].FirstName);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "LastName", null);
            writer.WriteValue(itinerary.Passenger[0].LastName);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "Telephone", null);
            writer.WriteStartElement("ns1", "PhoneNumber", null);
            string phone = "41646222";
            if (!string.IsNullOrEmpty(itinerary.Passenger[0].CellPhone))
            {
                phone = itinerary.Passenger[0].CellPhone;
            }
            writer.WriteValue(phone);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "CountryCode", null);
            writer.WriteValue("91");//code of india
            writer.WriteEndElement();
            //writer.WriteStartElement("ns1", "AreaCode", null);
            //writer.WriteValue("11");//code of delhi
            writer.WriteEndElement();

            string email = "admin@travelboutiqueonline.com";
            if (!string.IsNullOrEmpty(itinerary.Passenger[0].Email))
            {
                email = itinerary.Passenger[0].Email;
            }
            writer.WriteStartElement("ns1", "Email", null);
            writer.WriteValue(email);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "Address", null);
            writer.WriteStartElement("ns1", "CountryName", null);
            writer.WriteStartElement("ns1", "CountryName", null);
            string country = "IN";
            if (!string.IsNullOrEmpty(itinerary.Passenger[0].Country.CountryCode))
            {
                country = itinerary.Passenger[0].Country.CountryCode;
            }
            writer.WriteValue(country);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "CountryCode", null);
            writer.WriteValue(country);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "CityName", null);
            string cityName = "Delhi";
            if (!string.IsNullOrEmpty(itinerary.Passenger[0].City))
            {
                cityName = itinerary.Passenger[0].City;
            }

            writer.WriteValue(cityName);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
            bookingReq = bookingReq.Replace("<PriceDetail />", pricedetail);
            
            Trace.TraceInformation("AirArabia.GetRequestStringForBooking Exit");
            return bookingReq.ToString();
        }

        private BookingResponse ReadBookingResponse(string response, FlightItinerary itinerary)
        {
            Trace.TraceError("AirArabia ReadBookingResponse entered");
            BookingResponse bResponse = new BookingResponse();
            XmlDocument xmlDoc = new XmlDocument();
            TextReader stringRead = new StringReader(response);
            xmlDoc.Load(stringRead);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
            nsmgr.AddNamespace("ns2", "http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05");
            XmlNode xBookRS = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:OTA_AirBookRS", nsmgr);
            XmlNode xErrors = xBookRS.SelectSingleNode("ns1:Errors", nsmgr);
            if (xErrors != null)
            {
                XmlNodeList xError = xErrors.SelectNodes("ns1:Error", nsmgr);
                foreach (XmlNode xErr in xError)
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaBooking, CoreLogic.Severity.High, 0, "AirArabia Booking Failed. Error :" + xErr.Attributes["ShortText"].Value.ToString() + "Response XML" + response, "");
                    bResponse = new BookingResponse(BookingResponseStatus.Failed, xErr.Attributes["ShortText"].Value.ToString(), string.Empty);

                }
            }
            else
            {
                XmlNode reservationNode = xBookRS.SelectSingleNode("ns1:AirReservation", nsmgr);
                XmlNode referenceId = reservationNode.SelectSingleNode("ns1:BookingReferenceID", nsmgr);
                if (referenceId != null)
                {
                    bResponse.PNR = referenceId.Attributes["ID"].Value;
                    bResponse.Status = BookingResponseStatus.Successful;
                    itinerary.PNR = referenceId.Attributes["ID"].Value;
                    itinerary.FareType = "Pub";
                }
                else
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaBooking, CoreLogic.Severity.High, 0, "AirArabia Booking Failed. Error : PNR not recieved.| Response XML" + response, "");
                    bResponse = new BookingResponse(BookingResponseStatus.Failed, "PNR not received", string.Empty);
                }
            }
            Trace.TraceError("AirArabia ReadBookingResponse exit");
            return bResponse;
        }

        public static List<FareRule> GetFareRule(string origin, string dest, string fareBasisCode)
        {
            Trace.TraceInformation("AirArabia.GetFareRule Enter");
            List<FareRule> fareRuleList = new List<FareRule>();

            FareRule DNfareRules = new FareRule();
            DNfareRules.Origin = origin;
            DNfareRules.Destination = dest;
            DNfareRules.Airline = "G9";
            DNfareRules.FareRuleDetail = Util.GetLCCFareRules("G9");
            DNfareRules.FareBasisCode = fareBasisCode;

            fareRuleList.Add(DNfareRules);

            Trace.TraceInformation("AirArabia.GetFareRule exit");
            return fareRuleList;
        }
        private string GetAgentCreditRequest()
        {
            Trace.TraceInformation("AirArabia.GetRequestStringForBooking Entered");
            StringBuilder AgentAvailCredit = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(AgentAvailCredit);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
            writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
            writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");
            #region Header
            writer.WriteStartElement("soap", "Header", null);
            writer.WriteStartElement("wsse", "Security", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteAttributeString("soap", "mustUnderstand", null, "1");
            writer.WriteStartElement("wsse", "UsernameToken", null);
            writer.WriteAttributeString("xmlns", "wsu", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
            writer.WriteAttributeString("wsu", "Id", null, "UsernameToken-26506823");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteStartElement("wsse", "Username", null);
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(userName);
            writer.WriteEndElement();
            writer.WriteStartElement("wsse", "Password", null);
            writer.WriteAttributeString("Type", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            writer.WriteAttributeString("xmlns", "wsse", null, "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
            writer.WriteValue(password);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion
            writer.WriteStartElement("soap", "Body", null);
            writer.WriteAttributeString("xmlns", "ns1", null, "http://www.opentravel.org/OTA/2003/05");
            //writer.WriteAttributeString("xmlns", "ns2", null, "http://www.opentravel.org/OTA/2003/05");
            writer.WriteStartElement("ns1", "AA_OTA_AgentAvailCreditRQ", null);
            writer.WriteAttributeString("EchoToken", "11810120749379731256589");
            // writer.WriteAttributeString("TransactionIdentifier", sessiondta.transactionID);//new for every transaction
            writer.WriteAttributeString("PrimaryLangID", "en-us");
            writer.WriteAttributeString("SequenceNmbr", "1");
            writer.WriteAttributeString("TimeStamp", DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss"));
            writer.WriteAttributeString("Version", "20061.00");
            #region POS
            writer.WriteStartElement("ns1", "POS", null);
            writer.WriteStartElement("ns1", "Source", null);
            writer.WriteAttributeString("TerminalID", "TestUser/Test Runner");
            writer.WriteStartElement("ns1", "RequestorID", null);
            writer.WriteAttributeString("Type", "4");
            writer.WriteAttributeString("ID", userName);
            writer.WriteEndElement();
            writer.WriteStartElement("ns1", "BookingChannel", null);
            writer.WriteAttributeString("Type", "12");
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion
            writer.WriteStartElement("ns1", "AgentID", null);
            writer.WriteValue(code);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
            Trace.TraceInformation("AirArabia.GetAgentAvailCredit Exit");
            return AgentAvailCredit.ToString();
        }
        public decimal GetAgentAvailCredit()
        {
            string request = GetAgentCreditRequest();
            CookieContainer cookie = new CookieContainer();
            string response = string.Empty;
            try
            {
                response = GetResponse(request, ref cookie);
                CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaCreditbalance, CoreLogic.Severity.High, 0, "AirArabia GetAgentAvailCredit | Request : " + request + "| Response :-" + response + "| Time" + DateTime.Now, "");
            }
            catch (WebException ex)
            {
                CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaCreditbalance, CoreLogic.Severity.High, 0, "Web Exception Returned from AirArabia | Error = " + ex.Message + "| Request XML" + request + "| Time= " + DateTime.Now, "");
                throw ex;
            }
            decimal balance = ReadAgentAvailCredit(response);
            return balance;
        }
        private decimal ReadAgentAvailCredit(string response)
        {
            decimal balance = 0;
            XmlDocument xmlDoc = new XmlDocument();
            TextReader stringRead = new StringReader(response);
            xmlDoc.Load(stringRead);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
            nsmgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nsmgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
            nsmgr.AddNamespace("ns1", "http://www.opentravel.org/OTA/2003/05");
            XmlNode xCreditRS = xmlDoc.SelectSingleNode("/soap:Envelope/soap:Body/ns1:AA_OTA_AgentAvailCreditRS", nsmgr);
            XmlNode xErrors = xCreditRS.SelectSingleNode("ns1:Errors", nsmgr);
            if (xErrors != null)
            {
                XmlNodeList xError = xErrors.SelectNodes("ns1:Error", nsmgr);
                foreach (XmlNode xErr in xError)
                {
                    CoreLogic.Audit.Add(CoreLogic.EventType.AirArabiaCreditbalance, CoreLogic.Severity.High, 0, "AirArabia retreive  Agent Avail Credit Failed. Error :" + xErr.Attributes["ShortText"].Value.ToString() + "Response XML" + response, "");
                    throw new BookingEngineException("AirArabia: Get Credit balance Failed: Error Returned" + xErr.Attributes["ShortText"].Value.ToString());                
                }
                
            }
            else
            {
                XmlNode xCreditAvail = xCreditRS.SelectSingleNode("ns1:AgentAvailCredit/ns1:AvailableCredit", nsmgr);
                balance=Convert.ToDecimal(xCreditAvail.Attributes["Amount"].Value);
            }
            return balance;
        }

    }
}
