using System;
using System.Collections.Generic;
using System.Text;
using FlyDubai.SecurityToken;
using FlyDubai.FareQuote;
using FlyDubai.ConfirmBooking;
using FlyDubai.LoginTravelAgent;
using FlyDubai.ProcessPNRPayment;
using FlyDubai.AgencyFees;
using System.Text.RegularExpressions;
using CT.Configuration;
using System.Data;
using System.IO;
using System.Xml;
using System.Diagnostics;
using CT.Core;
using FlyDubai.ServiceQuote;
using System.Net;
using System.Linq;

namespace CT.BookingEngine.GDS
{
    public class FlyDubaiApi:IDisposable
    {
        class SessionData
        {
            public string nGuid;
            public Dictionary<string, Dictionary<string, List<int>>> FareInfo;
            public Dictionary<string, List<int>> selectedFareInfo;
            public int AdultCount;
            public int SeniorCount;
            public int ChildCount;
            public int InfantCount;
        }

        DataTable dtBaggageQuotes = new DataTable("dtBaggageQuotes");
        ConnectPoint_Security guidVal = new ConnectPoint_Security();        
        ConnectPoint_TravelAgents taLogin = new ConnectPoint_TravelAgents();
        LoginTravelAgent loginTARequest = new LoginTravelAgent();
        ConnectPoint_Reservation bookEvent = new ConnectPoint_Reservation();
        ConnectPoint_Fare avail = new ConnectPoint_Fare();
        ConnectPoint_Service ssr = new ConnectPoint_Service();
        ConnectPoint_Fulfillment process = new ConnectPoint_Fulfillment();
        ConnectPoint_Fees agencyFee = new ConnectPoint_Fees();

        RetrieveSecurityToken retSecurityToken = new RetrieveSecurityToken();
        ValidateSecurityToken valSecurityToken = new ValidateSecurityToken();

        
        string gaCurrency = "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "";
        string airlineCode = "FZ";
        string loginName = string.Empty;
        string password = string.Empty;
        string loginTAUserInter = string.Empty;
        string loginTAUserLocal = string.Empty;
        string loginTApwd = string.Empty;
        string IATAInter = string.Empty;
        string IATALocal = string.Empty;
        bool testMode = true;
        string sessionId;
        int appUserId;
        string xmlPath = ConfigurationSystem.FlyDubaiConfig["XmlLogPath"];
        string agentBaseCurrency;
        Dictionary<string, decimal> exchangeRates;
        decimal rateOfExchange = 1;
        int decimalValue=3;

        ////Fare Types used for Validting in PassengerDetails.aspx.cs
        //public static string PAYTOCHANGE = "PAY TO CHANGE";
        //public static string FREETOCHANGE = "FREE TO CHANGE";
        //public static string NOCHANGE = "NO CHANGE";
        //public static string BASIC = "BASIC";

        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }

        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }

        public string LoginName
        {
            get { return loginName; }
            set { loginName = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string AgentBaseCurrency
        {
            get { return agentBaseCurrency; }
            set { agentBaseCurrency = value; }
        }

        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public int AgentDecimalValue
        {
            get { return decimalValue; }
            set { decimalValue = value; }
        }


        /// <summary>
        /// for sector list added by bangar
        /// </summary>
        public List<SectorList> Sectors { get; set; }


        public FlyDubaiApi(string origin, string sessionId)
        {
            this.sessionId = sessionId;
            //loginName = ConfigurationSystem.FlyDubaiConfig["LoginName"];
            //password = ConfigurationSystem.FlyDubaiConfig["Password"];
            loginTAUserInter = ConfigurationSystem.FlyDubaiConfig["LoginTAUserInter"];
            loginTAUserLocal = ConfigurationSystem.FlyDubaiConfig["LoginTAUserLocal"];
            loginTApwd = ConfigurationSystem.FlyDubaiConfig["LoginTAPwd"];
            IATAInter = ConfigurationSystem.FlyDubaiConfig["IATAInter"];
            IATALocal = ConfigurationSystem.FlyDubaiConfig["IATALocal"];

            testMode = Convert.ToBoolean(ConfigurationSystem.FlyDubaiConfig["TestMode"]);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            loginTARequest.IATANumber = GetIATA(origin);
            loginTARequest.Password = loginTApwd;
            loginTARequest.UserName = GetUserName(origin);
            loginTARequest.HistoricUserName = GetUserName(origin);

            guidVal.Url = ConfigurationSystem.FlyDubaiConfig["Security"];
            taLogin.Url = ConfigurationSystem.FlyDubaiConfig["TravelAgent"];
            avail.Url = ConfigurationSystem.FlyDubaiConfig["FareQuote"];
            ssr.Url = ConfigurationSystem.FlyDubaiConfig["ServiceQuote"];
            bookEvent.Url = ConfigurationSystem.FlyDubaiConfig["Reservation"];
            process.Url = ConfigurationSystem.FlyDubaiConfig["Fulfillment"];
            agencyFee.Url = ConfigurationSystem.FlyDubaiConfig["AgencyFees"];
            
            InitBaggageTable();

            try
            {
                xmlPath += DateTime.Now.ToString("ddMMMyyy") + "\\";
                if (!System.IO.Directory.Exists(xmlPath))
                {
                    System.IO.Directory.CreateDirectory(xmlPath);
                }
            }
            catch { }
        }
        public FlyDubaiApi(string sessionId)
        {
            this.sessionId = sessionId;
            InitBaggageTable();
        }

        void InitBaggageTable()
        {
            if (dtBaggageQuotes.Columns.Count <= 0)
            {
                dtBaggageQuotes.Columns.Add("CodeType", typeof(string));
                dtBaggageQuotes.Columns.Add("Description", typeof(string));
                dtBaggageQuotes.Columns.Add("Amount", typeof(decimal));
                dtBaggageQuotes.Columns.Add("CategoryId", typeof(int));
                dtBaggageQuotes.Columns.Add("LogicalFlightID", typeof(string));
                dtBaggageQuotes.Columns.Add("QtyAvailable", typeof(int));
                dtBaggageQuotes.Columns.Add("ServiceID", typeof(string));
                dtBaggageQuotes.Columns.Add("CurrencyCode", typeof(string));
                dtBaggageQuotes.Columns.Add("Refundable", typeof(bool));
                dtBaggageQuotes.Columns.Add("ApplicableTaxes", typeof(decimal));
                dtBaggageQuotes.Columns.Add("AmountType", typeof(string));
            }
        }


        public string CreateGUID()
        {
            Trace.TraceInformation("FlyDubaiApi.CreateGUID Enter");
            // Generating GUID           
            retSecurityToken.LogonID = loginName;
            retSecurityToken.Password = password;
            retSecurityToken.CarrierCodes = new FlyDubai.SecurityToken.CarrierCode[1];
            retSecurityToken.CarrierCodes[0] = new FlyDubai.SecurityToken.CarrierCode();
            retSecurityToken.CarrierCodes[0].AccessibleCarrierCode = "FZ";


            try
            {

                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(RetrieveSecurityToken));
                string filePath = @"" + xmlPath + sessionId + "_FlyDubaiRetrieveSecurityTokenRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, retSecurityToken);
                sw.Close();
                ser = null;
                //Audit.Add(EventType.FlyDubai, Severity.Normal, appUserId, filePath, "127.0.0.1");
            }
            catch { }

            ViewSecurityToken viewSecurityToken = guidVal.RetrieveSecurityToken(retSecurityToken);

            try
            {

                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(ViewSecurityToken));
                string filePath = @"" + xmlPath + sessionId + "_FlyDubaiRetrieveSecurityTokenResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, viewSecurityToken);
                sw.Close();
                ser = null;
                //Audit.Add(EventType.FlyDubai, Severity.Normal, appUserId, filePath, "127.0.0.1");
            }
            catch { }

            if (viewSecurityToken.SecurityToken != null && viewSecurityToken.SecurityToken.Length > 0)
            {
                CT.Core.Audit.Add(CT.Core.EventType.FlyDubai, CT.Core.Severity.Normal, 0, "FlyDubai Guid Generation successful | " + DateTime.Now + "| Guid=" + viewSecurityToken.SecurityToken, "");
            }
            else
            {
                CT.Core.Audit.Add(CT.Core.EventType.FlyDubai, CT.Core.Severity.High, 0, "FlyDubai Guid Generation Fail | " + DateTime.Now, "");
            }
            Trace.TraceInformation("FlyDubaiApi.CreateGUID Exit");
            return viewSecurityToken.SecurityToken;
        }

        public bool GetValidateGUID(string nguid)
        {
            Trace.TraceInformation("FlyDubaiApi.GetValidateGUID Enter");
            valSecurityToken.CarrierCodes = new FlyDubai.SecurityToken.CarrierCode[1];
            valSecurityToken.CarrierCodes[0] = new FlyDubai.SecurityToken.CarrierCode();
            valSecurityToken.CarrierCodes[0].AccessibleCarrierCode = "FZ";
            valSecurityToken.SecurityToken = nguid;

            try
            {

                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(ValidateSecurityToken));
                string filePath = @"" + xmlPath + sessionId + "_FlyDubaiValidateSecurityTokenRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, valSecurityToken);
                sw.Close();
                ser = null;
                //Audit.Add(EventType.FlyDubai, Severity.Normal, appUserId, filePath, "127.0.0.1");
            }
            catch { }

            ViewValidatedSecurityToken viewValSecurityToken = guidVal.ValidateSecurityToken(valSecurityToken);

            try
            {

                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(ViewValidatedSecurityToken));
                string filePath = @"" + xmlPath + sessionId + "_FlyDubaiValidateSecurityTokenResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, viewValSecurityToken);
                sw.Close();
                ser = null;
                //Audit.Add(EventType.FlyDubai, Severity.Normal, appUserId, filePath, "127.0.0.1");
            }
            catch { }

            if (viewValSecurityToken.ValidationResultSpecified)
            {
                CT.Core.Audit.Add(CT.Core.EventType.FlyDubai, CT.Core.Severity.Normal, 0, "FlyDubai Guid validation successful | " + DateTime.Now + "| Guid=" + nguid, "");
            }
            else
            {
                CT.Core.Audit.Add(CT.Core.EventType.FlyDubai, CT.Core.Severity.High, 0, "FlyDubai Guid invalid | " + DateTime.Now, "");
            }
            Trace.TraceInformation("FlyDubaiApi.GetValidateGUID Exit");
            return viewValSecurityToken.ValidationResultSpecified;
        }

   

        public bool LoginAgency(string nguid)
        {
            Trace.TraceInformation("FlyDubaiApi.LoginAgency Enter");
            loginTARequest.CarrierCodes = new FlyDubai.LoginTravelAgent.CarrierCode[1];
            loginTARequest.CarrierCodes[0] = new FlyDubai.LoginTravelAgent.CarrierCode();
            loginTARequest.CarrierCodes[0].AccessibleCarrierCode = "FZ";
            loginTARequest.SecurityGUID = nguid;//CreateGUID(); 


            try
            {

                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(LoginTravelAgent));
                string filePath = @"" + xmlPath + sessionId + "_FlyDubaiLoginTravelAgentRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, loginTARequest);
                sw.Close();
                ser = null;
                //Audit.Add(EventType.FlyDubai, Severity.Normal, appUserId, filePath, "127.0.0.1");
            }
            catch { }

            LoginTravelAgentStatus loginTAStatus = taLogin.LoginTravelAgent(loginTARequest); // For test

            try
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(LoginTravelAgentStatus));
                string filePath = @"" + xmlPath + sessionId + "_" + "FlyDubaiLoginTravelAgentResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, loginTAStatus);
                sw.Close();
                ser = null;
                //Audit.Add(EventType.FlyDubai, Severity.Normal, appUserId, filePath, "127.0.0.1");
            }
            catch { }

            if (!loginTAStatus.LoggedIn)
            {
                CT.Core.Audit.Add(CT.Core.EventType.FlyDubai, CT.Core.Severity.High, 0, "FlyDubai Login Failed | " + loginTAStatus.Exceptions[0].ExceptionDescription + DateTime.Now, "");
                Trace.TraceError("FlyDubai Login Failed");
                throw new BookingEngineException("FlyDubai Login Failed");
            }
            else
            {
                CT.Core.Audit.Add(CT.Core.EventType.FlyDubaiFareQuote, CT.Core.Severity.Normal, 0, "Go Air Login Successful |" + DateTime.Now, "");
            }

            Trace.TraceInformation("FlyDubaiApi.LoginAgency Exit");
            return loginTAStatus.LoggedIn;
        }

        public void GetFareTax(SearchResult result, string sessionId)
        {
            Trace.TraceInformation("FlyDubaiApi.GetFareTax Enter");
            SessionData sData = (SessionData)Basket.BookingSession[sessionId][airlineCode];
            string resultKey = BuildResultKey(result);
            Dictionary<string, List<int>> fareInfo = sData.FareInfo[resultKey];
            sData.selectedFareInfo = fareInfo;
            Basket.BookingSession[sessionId][airlineCode] = (object)sData;
        }

        public TransactionFeeDetail GetTransactionFee(string sessionID, string username)
        {
            Trace.TraceInformation("FlyDubaiApi.GetTransactionFee Enter");
            //SessionData sData = (SessionData)Basket.BookingSession[sessionID][airlineCode];
            string transFeeXML = string.Empty;
             
            ViewAgencyCommission viewAgencyCommission = null;
            try
            {
                RetrieveAgencyCommission retAgencyCommission = new RetrieveAgencyCommission();
                retAgencyCommission.CurrencyCode = FlyDubai.LoginTravelAgent.EnumerationsCurrencyCodeTypes.AED;
                retAgencyCommission.SecurityGUID = sessionID;
                retAgencyCommission.CarrierCodes = new FlyDubai.LoginTravelAgent.CarrierCode[1];
                retAgencyCommission.CarrierCodes[0] = new FlyDubai.LoginTravelAgent.CarrierCode();
                retAgencyCommission.CarrierCodes[0].AccessibleCarrierCode = "FZ";
                retAgencyCommission.HistoricUserName = username;

                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(RetrieveAgencyCommission));
                    StreamWriter sw = new StreamWriter(@"" + xmlPath + sessionId + "_FlyDubaiRetrieveAgencyCommissionRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml");
                    ser.Serialize(sw, retAgencyCommission);
                    sw.Close();
                }
                catch { }

                viewAgencyCommission = taLogin.RetrieveAgencyCommission(retAgencyCommission);
                //transFeeXML = bookEvent.GetApplicableTransactionFeesExtendedXML(sData.nGuid, gaCurrency);
                //transFeeXML = bookEvent.GetApplicableTransactionFeesXML(sData.nGuid, adCurrency);
                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(ViewAgencyCommission));
                    StreamWriter sw = new StreamWriter(@"" + xmlPath + sessionId + "_FlyDubaiRetrieveAgencyCommissionResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml");
                    ser.Serialize(sw, viewAgencyCommission);
                    sw.Close();
                }
                catch { }

            }
            catch (System.Net.WebException excep)
            {
                CT.Core.Audit.Add(CT.Core.EventType.FlyDubai, CT.Core.Severity.High, 0, "FlyDubai Transaction Fee Exception. Request = " + transFeeXML + " |error= " + excep.Message + "|" + DateTime.Now, "");
                Trace.TraceError("FlyDubai Transaction Fee Exception");
                throw new BookingEngineException("FlyDubai Transaction fee not found");
            }
            catch (System.Web.Services.Protocols.SoapException excep)
            {
                CT.Core.Audit.Add(CT.Core.EventType.FlyDubai, CT.Core.Severity.High, 0, "FlyDubai Transaction Fee Exception. Request = " + transFeeXML + " |error= " + excep.Message + "|" + DateTime.Now, "");
                Trace.TraceError("FlyDubai Transaction Exception");
                throw new BookingEngineException("FlyDubai Transaction fee not found");
            }
            catch (Exception excep)
            {
                CT.Core.Audit.Add(CT.Core.EventType.FlyDubai, CT.Core.Severity.High, 0, "FlyDubai Transaction Fee Exception. Request = " + transFeeXML + " |error= " + excep.Message + "|" + DateTime.Now, ""); ;
                Trace.TraceError("FlyDubai Transaction Exception");
                throw new BookingEngineException("FlyDubai Transaction fee not found");
            }

            //TextReader textReader = new StringReader(transFeeXML);
            //XmlDocument resp = new XmlDocument();
            //resp.Load(textReader);
            string amount = string.Empty;

            TransactionFeeDetail txnFee = new TransactionFeeDetail();
            //XmlNode node = resp.SelectSingleNode("TransactionFees/TransactionFee/Amount");
            //if (node != null)
            //{
            //    txnFee.Amount = Convert.ToDecimal(node.InnerText);
            //}
            if (viewAgencyCommission != null && viewAgencyCommission.TravelAgencyCommissions != null)
            {
                //txnFee.Amount = viewAgencyCommission.TravelAgencyCommissions[0].Amount;
                txnFee.Amount = 0;// Now transaction fee is included with fare
            }
            Trace.TraceInformation("FlyDubaiApi.GetTransactionFee Exit");
            return txnFee;
        }   

       
                

        /// <summary>
        /// Fare Rule for Go Air
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="dest"></param>
        /// <param name="fareBasisCode"></param>
        /// <returns></returns>
        public static List<FareRule> GetFareRule(string origin, string dest, string fareBasisCode)
        {
            Trace.TraceInformation("FlyDubaiApi.GetFareRule Enter");
            List<FareRule> fareRuleList = new List<FareRule>();

            FareRule DNfareRules = new FareRule();
            DNfareRules.Origin = origin;
            DNfareRules.Destination = dest;
            DNfareRules.Airline = "FZ";
            DNfareRules.FareRuleDetail = Util.GetLCCFareRules("FZ");
            DNfareRules.FareBasisCode = fareBasisCode;

            fareRuleList.Add(DNfareRules);

            Trace.TraceInformation("FlyDubaiApi.GetFareRule exit");
            return fareRuleList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private static string BuildResultKey(SearchResult result)
        {
            int obLength = result.Flights[0].Length;
            StringBuilder key = new StringBuilder();
            key.Append(result.Flights[0][0].DepartureTime.DayOfYear.ToString());
            key.Append(result.Flights[0][0].DepartureTime.ToString("HHmm"));
            if (obLength > 0)
            {
                key.Append(result.Flights[0][obLength - 1].ArrivalTime.DayOfYear.ToString());
                key.Append(result.Flights[0][obLength - 1].ArrivalTime.ToString("HHmm"));
            }
            if (result.Flights.Length > 1)
            {
                int ibLength = result.Flights[1].Length;
                if (ibLength > 0)
                {
                    key.Append(result.Flights[1][0].DepartureTime.DayOfYear.ToString());
                    key.Append(result.Flights[1][0].DepartureTime.ToString("HHmm"));

                    key.Append(result.Flights[1][ibLength - 1].ArrivalTime.DayOfYear.ToString());
                    key.Append(result.Flights[1][ibLength - 1].ArrivalTime.ToString("HHmm"));
                }
            }
            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    key.Append(result.Flights[i][j].FlightNumber);
                    key.Append(result.Flights[i][j].BookingClass);
                }
            }
            return key.ToString();
        }


        private string GetUserName(string origin)
        {
            if (origin.ToUpper() == "DXB")
            {
                return loginTAUserLocal;
            }
            else
            {
                return loginTAUserInter;
            }
        }

        private string GetIATA(string origin)
        {
            if (origin.ToUpper() == "DXB")
            {
                return IATALocal;
            }
            else
            {
                return IATAInter;
            }
        }
        /// <summary>
        /// Retrieves flight search results
        /// </summary>
        /// <param name="request"></param>
        /// <param name="guid"></param>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public SearchResult[] GetFareQuotes(SearchRequest request, string guid, string sessionId)
        {
            SearchResult[] result = new SearchResult[0];
            try
            {
                //START:PERFORM SEARCH BASED ON THE NEAR BY AIRPORT

                List<SectorList> locSectorlist = new List<SectorList>();
                locSectorlist = Sectors.FindAll(x => x.Supplier == "FlyDubaiCityMapping");

                // if (ConfigurationSystem.SectorListConfig.ContainsKey("FlyDubaiCityMapping"))
                if (locSectorlist.Count() > 0)
                {
                    //Dictionary<string, string> sectorsNearBy = ConfigurationSystem.SectorListConfig["FlyDubaiCityMapping"];
                    Dictionary<string, string> sectorsNearBy = locSectorlist.ToDictionary(r => r.Origin, r => r.Sectors);

                    if (sectorsNearBy.ContainsKey(request.Segments[0].Destination))
                    {
                        request.Segments[0].Destination = sectorsNearBy[request.Segments[0].Destination];
                    }
                    if (sectorsNearBy.ContainsKey(request.Segments[0].Origin) && request.Segments[0].NearByOriginPort)
                    {
                        request.Segments[0].Origin = sectorsNearBy[request.Segments[0].Origin];
                    }
                }
                //END:PERFORM SEARCH BASED ON THE NEAR BY AIRPORT


                TransactionFeeDetail fee = GetTransactionFee(guid, GetUserName(request.Segments[0].Origin));

                //Build Fare Quote Request
                FlyDubai.FareQuote.RetrieveFareQuote retFareQuote = new FlyDubai.FareQuote.RetrieveFareQuote();
                retFareQuote.CarrierCodes = new FlyDubai.FareQuote.CarrierCode[1];
                retFareQuote.CarrierCodes[0] = new FlyDubai.FareQuote.CarrierCode();
                retFareQuote.CarrierCodes[0].AccessibleCarrierCode = "FZ";
                retFareQuote.IataNumberOfRequestor = GetIATA(request.Segments[0].Origin);
                retFareQuote.InventoryFilterMethod = FlyDubai.FareQuote.EnumsInventoryFilterMethodType.Available;
                retFareQuote.SecurityGUID = guid;
                retFareQuote.CurrencyOfFareQuote = FlyDubai.FareQuote.EnumerationsCurrencyCodeTypes.AED;
                retFareQuote.FareFilterMethod = FlyDubai.FareQuote.EnumsFareFilterMethodType.NoCombinabilityRoundtripLowestFarePerFareType;
                retFareQuote.FareGroupMethod = FlyDubai.FareQuote.EnumsFareGroupMethodType.WebFareTypes;
                if (request.Type == SearchType.Return)
                {
                    retFareQuote.FareQuoteDetails = new FlyDubai.FareQuote.FareQuoteDetail[request.Segments.Length];
                }
                else
                {
                    retFareQuote.FareQuoteDetails = new FlyDubai.FareQuote.FareQuoteDetail[1];
                }
                retFareQuote.PromotionalCode = "";
                retFareQuote.ClientIPAddress = "";
                retFareQuote.HistoricUserName = GetUserName(request.Segments[0].Origin);
                
                
                for (int i = 0; i < request.Segments.Length; i++)
                {

                    if (request.Type == SearchType.OneWay && i > 0)
                    {
                        break;
                    }
                    else
                    {
                        FlyDubai.FareQuote.FareQuoteDetail fareQuoteDetail = new FlyDubai.FareQuote.FareQuoteDetail();
                        if (i == 0)
                        {
                            fareQuoteDetail.DateOfDeparture = request.Segments[0].PreferredDepartureTime;
                            fareQuoteDetail.Destination = request.Segments[0].Destination;
                            fareQuoteDetail.Origin = request.Segments[0].Origin;
                        }
                        else
                        {
                            fareQuoteDetail.DateOfDeparture = request.Segments[1].PreferredDepartureTime;
                            fareQuoteDetail.Destination = request.Segments[0].Origin;
                            fareQuoteDetail.Origin = request.Segments[0].Destination;
                        }
                        fareQuoteDetail.OperatingCarrierCode = "FZ";
                        fareQuoteDetail.MarketingCarrierCode = "FZ";
                        fareQuoteDetail.LanguageCode = "EN";
                        fareQuoteDetail.FareTypeCategory = 1;
                        fareQuoteDetail.TicketPackageID = "1";
                        fareQuoteDetail.LFID = -214;
                        fareQuoteDetail.UseAirportsNotMetroGroups = true;

                        fareQuoteDetail.FareBasisCode = "";
                        //if (request.Segments[0].flightCabinClass == CabinClass.Business || request.Segments[0].flightCabinClass == CabinClass.Economy)
                        //{
                        //    fareQuoteDetail.Cabin = request.Segments[0].flightCabinClass.ToString();
                        //}
                        //else
                        //{
                        //    fareQuoteDetail.Cabin = "";
                        //}
                        fareQuoteDetail.FareClass = "";


                        int paxCount = 0;

                        if (request.AdultCount > 0)
                        {
                            paxCount++;
                        }
                        if (request.ChildCount > 0)
                        {
                            paxCount++;
                        }
                        if (request.InfantCount > 0)
                        {
                            paxCount++;
                        }
                        fareQuoteDetail.FareQuoteRequestInfos = new FlyDubai.FareQuote.FareQuoteRequestInfo[paxCount];
                        for (int j = 0; j < request.AdultCount; j++)
                        {

                            FlyDubai.FareQuote.FareQuoteRequestInfo fareQuoteInfo = new FlyDubai.FareQuote.FareQuoteRequestInfo();
                            fareQuoteInfo.PassengerTypeID = 1;
                            fareQuoteInfo.TotalSeatsRequired = request.AdultCount + request.SeniorCount;
                            fareQuoteDetail.FareQuoteRequestInfos[0] = fareQuoteInfo;
                        }
                        for (int j = 0; j < request.ChildCount; j++)
                        {

                            FlyDubai.FareQuote.FareQuoteRequestInfo fareQuoteInfo1 = new FlyDubai.FareQuote.FareQuoteRequestInfo();
                            fareQuoteInfo1.PassengerTypeID = 6;
                            fareQuoteInfo1.TotalSeatsRequired = request.ChildCount;
                            fareQuoteDetail.FareQuoteRequestInfos[1] = fareQuoteInfo1;
                        }
                        for (int j = 0; j < request.InfantCount; j++)
                        {
                            FlyDubai.FareQuote.FareQuoteRequestInfo fareQuoteInfo2 = new FlyDubai.FareQuote.FareQuoteRequestInfo();
                            fareQuoteInfo2.PassengerTypeID = 5;
                            fareQuoteInfo2.TotalSeatsRequired = request.InfantCount;
                            if (request.ChildCount > 0)
                            {
                                fareQuoteDetail.FareQuoteRequestInfos[2] = fareQuoteInfo2;
                            }
                            else
                            {
                                fareQuoteDetail.FareQuoteRequestInfos[1] = fareQuoteInfo2;
                            }
                        }

                        retFareQuote.FareQuoteDetails[i] = fareQuoteDetail;
                    }
                }
                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(FlyDubai.FareQuote.RetrieveFareQuote));
                    string filePath = @"" + xmlPath + sessionId + "_" + "FlyDubaiFareQuoteRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, retFareQuote);
                    sw.Close();
                    ser = null;
                    //Audit.Add(EventType.FlyDubaiFareQuote, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
                ViewFareQuote viewFareQuote = null;
                //RadixxFlights flight = new RadixxFlights();
                //XmlDocument doc = new XmlDocument();
                //doc.Load("D:\\FlyDubai\\SearchRequest.xml");
                //string requestMessage = doc.OuterXml;

                bool allowSearch = true;
                //see if the sector is available
                //if (ConfigurationSystem.SectorListConfig.ContainsKey("FZ"))
                //{
                //    Dictionary<string, string> sectors = ConfigurationSystem.SectorListConfig["FZ"];
                //    if (sectors.ContainsKey(request.Segments[0].Origin))
                //    {
                //        if (sectors[request.Segments[0].Origin].Contains(request.Segments[0].Destination))
                //        {
                //            allowSearch = true;
                //        }
                //    }
                //}
                //else
                //{
                //    allowSearch = true;
                //}

                if (allowSearch)
                {
                    //string requestMessage = GenerateFareQuoteMessage(request);
                    //CT.Core.Audit.Add(CT.Core.EventType.FlyDubaiFareQuote, CT.Core.Severity.Normal, 1, "FluDubai search request xml = " + requestMessage, "0");
                    //string response = flight.GetFareQuote(guid, requestMessage);
                    viewFareQuote = avail.RetrieveFareQuote(retFareQuote);
                    //CT.Core.Audit.Add(CT.Core.EventType.FlyDubaiFareQuote, CT.Core.Severity.Normal, 1, "FluDubai search response xml = " + response, "0");
                    //result = CreateSearchResult(response, request, guid);


                    try
                    {

                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(ViewFareQuote));
                        string filePath = @"" + xmlPath + sessionId + "_" + "FlyDubaiFareQuoteResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, viewFareQuote);
                        sw.Close();
                        ser = null;
                        // Audit.Add(EventType.FlyDubaiFareQuote, Severity.Normal, appUserId, filePath, "127.0.0.1");
                    }
                    catch { }
                    result = CreateSearchResult(viewFareQuote, request, guid, fee.Amount, (fee.IsPercentage ? "P" : "F"));
                    SessionData sData = new SessionData();
                    sData.nGuid = guid;
                    sData.FareInfo = new  Dictionary<string,Dictionary<string,List<int>>>();
                    for (int i = 0; i < result.Length; i++)
                    {
                        if (result.Length >= i)
                        {
                            string key = BuildResultKey((SearchResult)result[i]);
                            if (sData.FareInfo.ContainsKey(key))
                            {
                                sData.FareInfo[key] = result[i].FareInformationId;
                            }
                            else
                            {
                                sData.FareInfo.Add(key, result[i].FareInformationId);
                            }
                        }
                    }
                    sData.AdultCount = request.AdultCount;
                    sData.SeniorCount = request.SeniorCount;
                    sData.ChildCount = request.ChildCount;
                    sData.InfantCount = request.InfantCount;
                    Basket.BookingSession[sessionId].Add(airlineCode, (object)sData);
                }
                else
                {
                    CT.Core.Audit.Add(CT.Core.EventType.FlyDubaiFareQuote, CT.Core.Severity.Normal, 1, "FluDubai sector Not Available for : " + request.Segments[0].Origin + "-" + request.Segments[0].Destination, "0");
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.FlyDubaiFareQuote, CT.Core.Severity.High, 1, "Exception in fly Dubai Message = "+ ex.Message + " stack trace = " + ex.StackTrace, "0");
            }
            return result;
        }

        #region old code
        /// <summary>
        /// [deprecated] old xml request to get Fare Quotes.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>        
        private string GenerateFareQuoteMessage(SearchRequest request)
        {
            int paxCount = request.AdultCount + request.ChildCount + request.SeniorCount;
            StringBuilder message = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(message);
            writer.WriteStartElement("RadixxFareQuoteRequest");
            writer.WriteElementString("CurrencyOfFareQuote", "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "");  // count of passengers
            writer.WriteElementString("IataNumberOfRequestor", IATAInter);  // count of passengers
            writer.WriteStartElement("FareQuoteDetails");
            writer.WriteStartElement("FareQuoteDetail");
            writer.WriteElementString("Origin", request.Segments[0].Origin);
            writer.WriteElementString("Destination", request.Segments[0].Destination);
            writer.WriteElementString("DateOfDeparture", request.Segments[0].PreferredDepartureTime.ToString("yyyy-MM-ddTHH:mm:ss"));
            writer.WriteElementString("FareTypeCategory", "1");
            writer.WriteElementString("FareClass", "");
            writer.WriteElementString("FareBasisCode", "");
            writer.WriteElementString("Cabin", "");
            writer.WriteElementString("NumberOfDaysBefore", "0");
            writer.WriteElementString("NumberOfDaysAfter", "0");
            writer.WriteElementString("LanguageCode", "");
            writer.WriteElementString("TicketPackageID", "");
            writer.WriteElementString("FareFilterMethod", "0");
            writer.WriteElementString("NumberOfDaysAfter", "0");
            writer.WriteStartElement("FareQuoteRequestInfos");
            if (request.AdultCount + request.SeniorCount > 0)
            {
                writer.WriteStartElement("FareQuoteRequestInfo");
                writer.WriteElementString("PassengerTypeID", "1");
                writer.WriteElementString("TotalSeatsRequired", (request.AdultCount + request.SeniorCount).ToString());
                writer.WriteEndElement();//FareQuoteRequestInfo            
            }
            if (request.ChildCount > 0)
            {
                writer.WriteStartElement("FareQuoteRequestInfo");
                writer.WriteElementString("PassengerTypeID", "6");
                writer.WriteElementString("TotalSeatsRequired", request.ChildCount.ToString());
                writer.WriteEndElement();//FareQuoteRequestInfo            
            }
            writer.WriteEndElement();//FareQuoteRequestInfos
            writer.WriteEndElement();//FareQuoteDetail
            writer.WriteEndElement();//FareQuoteDetails
            writer.WriteEndElement();//RadixxFareQuoteRequest
            writer.Close();
            return Regex.Replace(message.ToString(), "[<][?]xml.+[?][>]", string.Empty);

        }

        /// <summary>
        /// [deprecated] old method to read xml response from Fare Quotes search.
        /// </summary>
        /// <param name="response"></param>
        /// <param name="request"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
       


        /// <summary>
        /// [deprecated] old method get fare index
        /// </summary>
        /// <param name="farelist"></param>
        /// <param name="passengerType"></param>
        /// <returns></returns>
        private int[] GetLowestFareIndex(XmlNodeList farelist, string passengerType)
        {
            int index = 0;
            int fareTypeIndex = 0;
            int i = 0;
            decimal lowestfare = 0;
            foreach (XmlNode fareListNode in farelist)
            {
                //Assuming that pay to change contains cheapest fare
                if (fareListNode.Attributes["FareTypeID"].Value == "1")
                {
                    XmlNodeList nodeList = fareListNode.SelectNodes("FareInfos/FareInfo");
                    decimal fare = 0;
                    foreach (XmlNode fareInfo in nodeList)
                    {
                        //Assuming if adult is cheap than child will also be cheap
                        if (fareInfo.Attributes["PTCID"].Value.Trim() == passengerType)
                        {
                            fare = Convert.ToDecimal(fareInfo.Attributes["FareAmtInclTax"].Value);
                        }
                        else
                        {
                            i++;
                            continue;
                        }
                        if (fare < lowestfare || lowestfare == 0)
                        {
                            lowestfare = fare;
                            index = i;
                        }
                        i++;
                    }
                    break;
                }
                else
                {
                    fareTypeIndex++;
                }
            }
            int[] lowestIndex = new int[2];
            lowestIndex[0] = fareTypeIndex;
            lowestIndex[1] = index;
            return lowestIndex;
        }

        #endregion

        /// <summary>
        /// Generates search results from the Fare quote search response object.
        /// </summary>
        /// <param name="fareQuote"></param>
        /// <param name="request"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        private SearchResult[] CreateSearchResult(ViewFareQuote fareQuote, SearchRequest request, string guid, decimal transactionFee, string transFeeType)
        {
            int paxType = 0;
            int totalPassenger = request.AdultCount + request.SeniorCount + request.ChildCount;
            if (request.AdultCount > 0 || request.SeniorCount > 0)
            {
                paxType++;
            }
            //if (request.ChildCount > 0)
            //{
            //    paxType++;
            //}
            string passengerType = "1";
            if ((request.AdultCount + request.SeniorCount) >= 1 && request.InfantCount > 0)
            {
                passengerType += ",5";
            }
            if ((request.AdultCount + request.SeniorCount) >= 1 && request.ChildCount > 0)
            {
                passengerType += ",6";
            }

            string currency = "";// +CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "";

            //SearchResult[] resultList = new SearchResult[0];

            List<SearchResult> resultList = new List<SearchResult>();

            currency = fareQuote.RequestedCurrencyOfFareQuote;

            if (agentBaseCurrency != currency && exchangeRates != null)
            {
                rateOfExchange = exchangeRates[currency];
            }
            else
            {
                rateOfExchange = 1;
            }

            if (fareQuote.FlightSegments != null && fareQuote.FlightSegments.Length > 0)
            {
                List<FlyDubai.FareQuote.FlightSegment> FlightSegments = new List<FlyDubai.FareQuote.FlightSegment>();
                List<FlyDubai.FareQuote.FlightSegment> OnwardFlightSegments = new List<FlyDubai.FareQuote.FlightSegment>();
                List<FlyDubai.FareQuote.FlightSegment> ReturnFlightSegments = new List<FlyDubai.FareQuote.FlightSegment>();

                FlightSegments.AddRange(fareQuote.FlightSegments);

                List<SegmentDetail> Segments = new List<SegmentDetail>();
                List<SegmentDetail> OnwardSegments = new List<SegmentDetail>();
                List<SegmentDetail> ReturnSegments = new List<SegmentDetail>();

                Segments.AddRange(fareQuote.SegmentDetails);

                //Filter Segment Details for Onward and Return flights from the Search Request Origin & Destination
                foreach (SegmentDetail segment in Segments)
                {
                    if (segment.Origin == request.Segments[0].Origin || (request.Segments[0].Origin =="DXB" && (segment.Origin == "DXB" || segment.Origin=="DWC")))
                    {
                        OnwardSegments.Add(segment);
                    }
                    else if (segment.Origin == request.Segments[0].Destination || (request.Segments[0].Destination == "DXB" && (segment.Origin == "DXB" || segment.Origin == "DWC")))
                    {
                        ReturnSegments.Add(segment);
                    }
                }

                //Link LFID of SegmentDetail to LFID of FlightSegment
                foreach (FlyDubai.FareQuote.FlightSegment fs in FlightSegments)
                {
                    foreach (SegmentDetail segment in OnwardSegments)
                    {
                        if (segment.LFID == fs.LFID)
                        {
                            OnwardFlightSegments.Add(fs);
                        }
                    }

                    foreach (SegmentDetail segment in ReturnSegments)
                    {
                        if (segment.LFID == fs.LFID)
                        {
                            ReturnFlightSegments.Add(fs);
                        }
                    }
                }

                //Build results 
                for (int i = 0; i < OnwardFlightSegments.Count; i++)
                {
                    SegmentDetail onSegment = OnwardSegments.Find(delegate(SegmentDetail sd) { return sd.LFID == OnwardFlightSegments[i].LFID; });
                    FlyDubai.FareQuote.FlightSegment onFSegment = OnwardFlightSegments[i];

                    //Get all the FareID's of FlightSegment
                    List<FlyDubai.FareQuote.FareType> onFareInfoList = new List<FlyDubai.FareQuote.FareType>();
                    onFareInfoList.Clear();
                    foreach (FlyDubai.FareQuote.FareType fare in onFSegment.FareTypes)
                    {                        
                        onFareInfoList.Add(fare);
                    }

                    if (request.Type == SearchType.Return)
                    {
                        for (int j = 0; j < ReturnFlightSegments.Count; j++)
                        {
                            SegmentDetail retSegment = ReturnSegments.Find(delegate(SegmentDetail sd) { return sd.LFID == ReturnFlightSegments[j].LFID; });
                            FlyDubai.FareQuote.FlightSegment retFSegment = ReturnFlightSegments[j];                            

                            List<FlyDubai.FareQuote.FareType> retFareInfoList = new List<FlyDubai.FareQuote.FareType>();
                            retFareInfoList.Clear();
                            foreach (FlyDubai.FareQuote.FareType fare in retFSegment.FareTypes)
                            {
                                retFareInfoList.Add(fare);
                            }

                            #region FareInfo
                            for (int k = 0; k < onFareInfoList.Count; k++)
                            {
                                for (int c = 0; c < retFareInfoList.Count; c++)
                                {
                                    SearchResult result = new SearchResult();
                                    result.IsLCC = true;
                                    result.ValidatingAirline = onSegment.CarrierCode.Trim();
                                    result.Airline = onSegment.CarrierCode.Trim();
                                    result.ResultBookingSource = BookingSource.FlyDubai;
                                    result.GUID = guid;
                                    result.Flights = new FlightInfo[2][];
                                    result.Flights[0] = new FlightInfo[onFSegment.FlightLegDetails.Length];
                                    result.Flights[1] = new FlightInfo[retFSegment.FlightLegDetails.Length];
                                    result.Currency = fareQuote.RequestedCurrencyOfFareQuote;
                                    if (result.FareType == null || result.FareType.Length <= 0)
                                    {
                                        result.FareType = onFareInfoList[k].FareTypeName;
                                    }
                                    if (result.FareType.Length > 0)
                                    {
                                        {
                                            result.FareType += "," + retFareInfoList[c].FareTypeName;
                                        }
                                    }

                                    int fareBreakDownCounter = 0;
                                    int passengerCount = 0;
                                    
                                    //Sorting Pax Type order
                                    List<FareInfo> fareInfos = new List<FareInfo>();
                                    for (int p = 0; p < onFareInfoList[k].FareInfos.Length; p++)
                                    {
                                        FlyDubai.FareQuote.FareInfo fareInfoNode = onFareInfoList[k].FareInfos[p];
                                        fareInfos.Add(fareInfoNode);
                                    }

                                    onFareInfoList[k].FareInfos[0] = fareInfos.Find(delegate(FareInfo f) { return f.PTCID == 1; });
                                    if (onFareInfoList[k].FareInfos.Length > 1)
                                    {
                                        if (request.ChildCount > 0)
                                        {
                                            onFareInfoList[k].FareInfos[1] = fareInfos.Find(delegate(FareInfo f) { return f.PTCID == 6; });
                                        }
                                        else
                                        {
                                            onFareInfoList[k].FareInfos[1] = fareInfos.Find(delegate(FareInfo f) { return f.PTCID == 5; });
                                        }
                                    }
                                    if (onFareInfoList[k].FareInfos.Length > 2)
                                    {
                                        if (request.InfantCount > 0)
                                        {
                                            onFareInfoList[k].FareInfos[2] = fareInfos.Find(delegate(FareInfo f) { return f.PTCID == 5; });
                                        }
                                    }
                                    //End Sorting

                                    for (int p = 0; p < onFareInfoList[k].FareInfos.Length; p++)
                                    {
                                        FlyDubai.FareQuote.FareInfo fareInfoNode = onFareInfoList[k].FareInfos[p];
                                        string bookingClass = onFareInfoList[k].FareInfos[p].FCCode;
                                        string cabin = onFareInfoList[k].FareInfos[p].Cabin;

                                        if (agentBaseCurrency !=fareQuote.RequestedCurrencyOfFareQuote)
                                        {
                                            result.Currency = agentBaseCurrency;
                                        }

                                        if (result.FareBreakdown == null)
                                        {
                                            if (request.AdultCount > 0 && request.SeniorCount > 0)
                                            {
                                                result.FareBreakdown = new Fare[paxType + 1];
                                            }
                                            else
                                            {
                                                int count = 0;
                                                if (request.ChildCount > 0)
                                                {
                                                    count++;
                                                }
                                                if (request.InfantCount > 0)
                                                {
                                                    count++;
                                                }
                                                result.FareBreakdown = new Fare[paxType + count];
                                            }
                                        }
                                        if (result.FareInformationId == null)
                                        {
                                            result.FareInformationId = new Dictionary<string, List<int>>();
                                            result.FareRules = new List<FareRule>();
                                        }
                                        FareRule newFareRule = new FareRule();
                                        ApplicableTaxDetail[] taxNodeList = fareInfoNode.ApplicableTaxDetails;
                                        double tax = 0;
                                        if (result.Price == null)
                                        {
                                            result.Price = new PriceAccounts();
                                        }

                                        //for (int f = 0; f < result.FareBreakdown.Length; f++)
                                        {
                                            //fareInfoNode = onFareInfoList[k].FareInfos[p];
                                            PassengerType ptype = PassengerType.Adult;
                                            if (fareInfoNode.PTCID.ToString() == "1")
                                            {
                                                passengerCount = request.AdultCount + request.SeniorCount;
                                                ptype = PassengerType.Adult;
                                                List<int> fareIds = new List<int>();
                                                fareIds.Add(fareInfoNode.FareID);
                                                if (!result.FareInformationId.ContainsKey(PassengerType.Adult.ToString()))
                                                {
                                                    result.FareInformationId.Add(PassengerType.Adult.ToString(), fareIds);
                                                }

                                                //result.BaggageIncluded = new Dictionary<long, bool>();
                                                result.BaggageIncludedInFare = "0";

                                                foreach (ApplicableTaxDetail taxNode in taxNodeList)
                                                {
                                                    foreach (TaxDetail td in fareQuote.TaxDetails)
                                                    {
                                                        if (taxNode.TaxID == td.TaxID && (td.CodeType.Contains("BAG")))
                                                        {
                                                            result.Price.BaggageCharge = 0;
                                                            //if (onFareInfoList[k].FareTypeName != "Pay To Change")
                                                            {
                                                                //if (result.BaggageIncluded.ContainsKey(onFareInfoList[k].FareTypeID))
                                                                //{
                                                                //    result.BaggageIncluded[onFareInfoList[k].FareTypeID] = true;
                                                                //}
                                                                //else
                                                                //{
                                                                //    result.BaggageIncluded.Add(onFareInfoList[k].FareTypeID, true);
                                                                //}
                                                                result.BaggageIncludedInFare = td.TaxDesc.Substring(0, 2);
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else if (fareInfoNode.PTCID.ToString() == "6")
                                            {
                                                passengerCount = request.ChildCount;
                                                ptype = PassengerType.Child;
                                                List<int> fareIds = new List<int>();
                                                fareIds.Add(fareInfoNode.FareID);
                                                if (!result.FareInformationId.ContainsKey(PassengerType.Child.ToString()))
                                                {
                                                    result.FareInformationId.Add(PassengerType.Child.ToString(), fareIds);
                                                }
                                            }
                                            else if (fareInfoNode.PTCID == 5)
                                            {
                                                passengerCount = request.InfantCount;
                                                ptype = PassengerType.Infant;
                                                List<int> fareIds = new List<int>();
                                                fareIds.Add(fareInfoNode.FareID);
                                                if (!result.FareInformationId.ContainsKey(PassengerType.Infant.ToString()))
                                                {
                                                    result.FareInformationId.Add(PassengerType.Infant.ToString(), fareIds);
                                                }
                                            }
                                            
                                            //foreach (ApplicableTaxDetail taxNode in taxNodeList)
                                            //{
                                            //    tax += Convert.ToDouble(taxNode.Amt);
                                            //}

                                            double baseFare = (Convert.ToDouble(fareInfoNode.DisplayFareAmt));
                                            if (transFeeType == "P")
                                            {
                                                baseFare += baseFare * (double)transactionFee / 100;
                                            }
                                            else
                                            {
                                                baseFare += (double)transactionFee;
                                            }

                                            tax = Convert.ToDouble(fareInfoNode.DisplayTaxSum);
                                            result.Price.SupplierCurrency = fareQuote.RequestedCurrencyOfFareQuote;
                                            result.Price.SupplierPrice += (decimal)(tax + baseFare) * (passengerCount);
                                            result.Price.RateOfExchange = rateOfExchange;
                                            result.Price.TransactionFee = transactionFee * rateOfExchange;

                                            //tax = Math.Round(tax * (double)rateOfExchange, 3);
                                            //baseFare = Math.Round(baseFare * (double)rateOfExchange, 3);
                                            bookingClass = fareInfoNode.FCCode;
                                            cabin = fareInfoNode.Cabin;
                                            //result.Tax += (Math.Round(tax * (double)rateOfExchange, decimalValue)) * (passengerCount);
                                            //result.BaseFare += (Math.Round(baseFare * (double)rateOfExchange, decimalValue)) * (passengerCount);
                                            result.Tax += ((tax * (double)rateOfExchange)) * (passengerCount);
                                            result.BaseFare += ((baseFare * (double)rateOfExchange)) * (passengerCount);
                                            result.TotalFare = result.BaseFare + result.Tax;

                                            newFareRule.FareInfoRef = fareInfoNode.FareID.ToString();

                                            if (ptype == PassengerType.Adult)
                                            {
                                                if (request.AdultCount > 0)
                                                {
                                                    result.FareBreakdown[fareBreakDownCounter] = new Fare();
                                                    result.FareBreakdown[fareBreakDownCounter].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.AdultCount;
                                                    result.FareBreakdown[fareBreakDownCounter].SellingFare = result.FareBreakdown[fareBreakDownCounter].TotalFare * request.AdultCount;
                                                    //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                                    result.FareBreakdown[fareBreakDownCounter].PassengerType = PassengerType.Adult;
                                                    result.FareBreakdown[fareBreakDownCounter].PassengerCount = request.AdultCount;
                                                    result.FareBreakdown[fareBreakDownCounter].BaseFare = (baseFare * (double)rateOfExchange) * request.AdultCount;
                                                    result.FareBreakdown[fareBreakDownCounter].SupplierFare = (double)(baseFare + tax) * request.AdultCount;
                                                    result.FareBreakdown[fareBreakDownCounter].FareType = onFareInfoList[k].FareTypeName;
                                                    fareBreakDownCounter++;
                                                }
                                                if (request.SeniorCount > 0)
                                                {
                                                    result.FareBreakdown[fareBreakDownCounter] = new Fare();
                                                    result.FareBreakdown[fareBreakDownCounter].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.SeniorCount;
                                                    result.FareBreakdown[fareBreakDownCounter].SellingFare = result.FareBreakdown[fareBreakDownCounter].TotalFare * request.SeniorCount;
                                                    //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                                    result.FareBreakdown[fareBreakDownCounter].PassengerType = PassengerType.Senior;
                                                    result.FareBreakdown[fareBreakDownCounter].PassengerCount = request.SeniorCount;
                                                    result.FareBreakdown[fareBreakDownCounter].BaseFare = (baseFare * (double)rateOfExchange) * request.SeniorCount;
                                                    result.FareBreakdown[fareBreakDownCounter].FareType = onFareInfoList[k].FareTypeName;
                                                    fareBreakDownCounter++;
                                                }
                                                newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                            }
                                            else if (ptype == PassengerType.Child)
                                            {
                                                result.FareBreakdown[fareBreakDownCounter] = new Fare();
                                                result.FareBreakdown[fareBreakDownCounter].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.ChildCount;
                                                result.FareBreakdown[fareBreakDownCounter].SellingFare = result.FareBreakdown[fareBreakDownCounter].TotalFare * request.ChildCount;
                                                //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                                result.FareBreakdown[fareBreakDownCounter].PassengerType = PassengerType.Child;
                                                result.FareBreakdown[fareBreakDownCounter].PassengerCount = request.ChildCount;
                                                result.FareBreakdown[fareBreakDownCounter].BaseFare = (baseFare * (double)rateOfExchange) * request.ChildCount;
                                                result.FareBreakdown[fareBreakDownCounter].SupplierFare = (double)(baseFare + tax) * request.ChildCount;
                                                result.FareBreakdown[fareBreakDownCounter].FareType = onFareInfoList[k].FareTypeName;
                                                fareBreakDownCounter++;
                                                //if (newFareRule.FareBasisCode != null && string.IsNullOrEmpty(newFareRule.FareBasisCode.Trim()))
                                                {
                                                    newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                }
                                            }
                                            else if (ptype == PassengerType.Infant)
                                            {
                                                result.FareBreakdown[fareBreakDownCounter] = new Fare();
                                                result.FareBreakdown[fareBreakDownCounter].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.InfantCount;
                                                result.FareBreakdown[fareBreakDownCounter].SellingFare = result.FareBreakdown[fareBreakDownCounter].TotalFare * request.InfantCount;
                                                //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                                result.FareBreakdown[fareBreakDownCounter].PassengerType = PassengerType.Infant;
                                                result.FareBreakdown[fareBreakDownCounter].PassengerCount = request.InfantCount;
                                                result.FareBreakdown[fareBreakDownCounter].BaseFare = (baseFare * (double)rateOfExchange) * request.InfantCount;
                                                result.FareBreakdown[fareBreakDownCounter].SupplierFare = (double)(baseFare + tax) * request.InfantCount;
                                                result.FareBreakdown[fareBreakDownCounter].FareType = onFareInfoList[k].FareTypeName;
                                                fareBreakDownCounter++;
                                                //if (newFareRule.FareBasisCode != null && string.IsNullOrEmpty(newFareRule.FareBasisCode.Trim()))
                                                {
                                                    newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                }
                                            }
                                        }


                                        int legCount = 0;
                                        for (int m = 0; m < onFSegment.FlightLegDetails.Length; m++)
                                        {
                                            FlightLegDetail legDetail = onFSegment.FlightLegDetails[m];

                                            for (int l = 0; l < fareQuote.LegDetails.Length; l++)
                                            {
                                                LegDetail leg = fareQuote.LegDetails[l];
                                                if (leg.PFID == legDetail.PFID && leg.DepartureDate == legDetail.DepartureDate)
                                                {
                                                    FlightInfo fInfo = new FlightInfo();
                                                    fInfo.Airline = result.Airline;
                                                    fInfo.ArrivalTime = Convert.ToDateTime(leg.ArrivalDate.ToString().Replace("T", " "));//DateTime.ParseExact(legNode.ArrivalDate.ToString().Replace("T"," "), "yyyy-MM-dd hh:mm:ss", null);
                                                    if (onSegment.FlightNum.Contains("/"))
                                                    {
                                                        fInfo.FlightNumber = onSegment.FlightNum.Split('/')[m];
                                                    }
                                                    else
                                                    {
                                                        fInfo.FlightNumber = onSegment.FlightNum;
                                                    }
                                                    fInfo.DepartureTime = Convert.ToDateTime(leg.DepartureDate.ToString().Replace("T", " ")); //DateTime.ParseExact(legNode.DepartureDate.ToString().Replace("T", " "), "yyyy-MM-dd hh:mm:ss", null);
                                                    fInfo.Destination = new Airport(leg.Destination.Trim());
                                                    fInfo.Origin = new Airport(leg.Origin.Trim());
                                                    fInfo.Status = "HK";
                                                    fInfo.SegmentFareType = onFareInfoList[k].FareTypeName;
                                                    newFareRule.Airline = result.Airline;
                                                    newFareRule.DepartureTime = fInfo.DepartureTime;
                                                    newFareRule.Destination = fInfo.Destination.AirportName;
                                                    newFareRule.Origin = fInfo.Origin.AirportName;
                                                    newFareRule.ReturnDate = fInfo.ArrivalTime;

                                                    fInfo.BookingClass = bookingClass;
                                                    fInfo.CabinClass = cabin;
                                                    fInfo.Duration = new TimeSpan(0, (int)leg.FlightTime, 0); //fInfo.ArrivalTime.Subtract(fInfo.DepartureTime);
                                                    fInfo.OperatingCarrier = leg.OperatingCarrier;
                                                    fInfo.DepTerminal = leg.FromTerminal;
                                                    fInfo.ArrTerminal = leg.ToTerminal;
                                                    fInfo.Craft = onSegment.AircraftType;
                                                    fInfo.UapiSegmentRefKey = onSegment.LFID.ToString();
                                                    fInfo.Stops = onSegment.Stops;
                                                    fInfo.Group = 0;
                                                    if (onSegment.Origin == request.Segments[0].Origin || ((request.Segments[0].Origin == "DXB" || request.Segments[0].Origin == "DWC") && (onSegment.Origin == "DXB" || onSegment.Origin == "DWC")))
                                                    {
                                                        result.Flights[0][legCount] = fInfo;
                                                    }
                                                    legCount++;
                                                    break;
                                                }
                                            }
                                        }
                                        result.FareRules.Add(newFareRule);
                                    }

                                    //Sorting Pax Type order
                                    fareInfos = new List<FareInfo>();
                                    for (int p = 0; p < retFareInfoList[c].FareInfos.Length; p++)
                                    {
                                        FlyDubai.FareQuote.FareInfo fareInfoNode = retFareInfoList[c].FareInfos[p];
                                        fareInfos.Add(fareInfoNode);
                                    }

                                    retFareInfoList[c].FareInfos[0] = fareInfos.Find(delegate(FareInfo f) { return f.PTCID == 1; });
                                    if (retFareInfoList[c].FareInfos.Length > 1)
                                    {
                                        if (request.ChildCount > 0)
                                        {
                                            retFareInfoList[c].FareInfos[1] = fareInfos.Find(delegate(FareInfo f) { return f.PTCID == 6; });
                                        }
                                        else
                                        {
                                            retFareInfoList[c].FareInfos[1] = fareInfos.Find(delegate(FareInfo f) { return f.PTCID == 5; });
                                        }
                                    }
                                    if (retFareInfoList[c].FareInfos.Length > 2)
                                    {
                                        if (request.InfantCount > 0)
                                        {
                                            retFareInfoList[c].FareInfos[2] = fareInfos.Find(delegate(FareInfo f) { return f.PTCID == 5; });
                                        }
                                    }
                                    //End Sorting

                                    for (int q = 0; q < retFareInfoList[c].FareInfos.Length; q++)
                                    {
                                        #region Return FareInfo

                                        string bookingClass = retFareInfoList[c].FareInfos[q].FCCode;
                                        string cabin = retFareInfoList[c].FareInfos[q].Cabin;
                                       
                                        FareRule newFareRule = new FareRule();

                                        //for (int f = 0; f < result.FareBreakdown.Length; f++)
                                        {
                                            FareInfo fareInfoNode = retFareInfoList[c].FareInfos[q];
                                            PassengerType ptype = PassengerType.Adult;

                                            ApplicableTaxDetail[] taxNodeList = fareInfoNode.ApplicableTaxDetails;
                                            double tax = 0;
                                            if (result.Price == null)
                                            {
                                                result.Price = new PriceAccounts();
                                            }

                                            if (fareInfoNode.PTCID.ToString() == "1")
                                            {
                                                passengerCount = request.AdultCount + request.SeniorCount;
                                                ptype = PassengerType.Adult;
                                                List<int> fareIds = new List<int>();
                                                fareIds.Add(fareInfoNode.FareID);
                                                if (!result.FareInformationId.ContainsKey(PassengerType.Adult.ToString()))
                                                {
                                                    result.FareInformationId.Add(PassengerType.Adult.ToString(), fareIds);
                                                }
                                                else
                                                {
                                                    result.FareInformationId[PassengerType.Adult.ToString()].Add(fareInfoNode.FareID);
                                                }

                                                foreach (ApplicableTaxDetail taxNode in taxNodeList)
                                                {
                                                    foreach (TaxDetail td in fareQuote.TaxDetails)
                                                    {
                                                        if (taxNode.TaxID == td.TaxID && (td.CodeType.ToLower().Contains("bag")))
                                                        {
                                                            result.Price.BaggageCharge = 0;
                                                            //if (retFareInfoList[c].FareTypeName != "Pay To Change")
                                                            {
                                                                //if (!result.BaggageIncluded.ContainsKey(retFareInfoList[c].FareTypeID))
                                                                //{
                                                                //    result.BaggageIncluded.Add(retFareInfoList[c].FareTypeID, true);
                                                                //    result.BaggageIncludedInFare += "," + td.TaxDesc.Substring(0, 2);
                                                                //    break;
                                                                //}
                                                                //else
                                                                //{
                                                                //    result.BaggageIncluded[retFareInfoList[c].FareTypeID] = true;
                                                                    result.BaggageIncludedInFare += "," + td.TaxDesc.Substring(0, 2);
                                                                //    break;
                                                                //}
                                                            }
                                                        }
                                                    }
                                                }

                                                if (result.BaggageIncludedInFare.IndexOf(",") < 0)
                                                {
                                                    result.BaggageIncludedInFare += ",0";
                                                }
                                            }
                                            else if (fareInfoNode.PTCID.ToString() == "6")
                                            {
                                                passengerCount = request.ChildCount;
                                                ptype = PassengerType.Child;
                                                List<int> fareIds = new List<int>();
                                                fareIds.Add(fareInfoNode.FareID);
                                                if (!result.FareInformationId.ContainsKey(PassengerType.Child.ToString()))
                                                {
                                                    result.FareInformationId.Add(PassengerType.Child.ToString(), fareIds);
                                                }
                                                else
                                                {
                                                    result.FareInformationId[PassengerType.Child.ToString()].Add(fareInfoNode.FareID);
                                                }
                                            }
                                            else if (fareInfoNode.PTCID == 5)
                                            {
                                                passengerCount = request.InfantCount;
                                                ptype = PassengerType.Infant;
                                                List<int> fareIds = new List<int>();
                                                fareIds.Add(fareInfoNode.FareID);
                                                if (!result.FareInformationId.ContainsKey(PassengerType.Infant.ToString()))
                                                {
                                                    result.FareInformationId.Add(PassengerType.Infant.ToString(), fareIds);
                                                }
                                                else
                                                {
                                                    result.FareInformationId[PassengerType.Infant.ToString()].Add(fareInfoNode.FareID);
                                                }
                                            }
                                           
                                            //foreach (ApplicableTaxDetail taxNode in taxNodeList)
                                            //{
                                            //    tax += Convert.ToDouble(taxNode.Amt);
                                            //}

                                            

                                            double baseFare = (Convert.ToDouble(fareInfoNode.DisplayFareAmt));
                                            if (transFeeType == "P")
                                            {
                                                baseFare += baseFare * (double)transactionFee / 100;
                                            }
                                            else
                                            {
                                                baseFare += (double)transactionFee;
                                            }
                                            tax = Convert.ToDouble(fareInfoNode.DisplayTaxSum);
                                            result.Price.SupplierPrice += (decimal)(tax + baseFare) * (passengerCount);
                                            
                                            bookingClass = fareInfoNode.FCCode;
                                            cabin = fareInfoNode.Cabin;
                                            //result.Tax += (Math.Round(tax * (double)rateOfExchange, decimalValue)) * (passengerCount);
                                            //result.BaseFare += (Math.Round(baseFare * (double)rateOfExchange, decimalValue)) * (passengerCount);
                                            result.Tax += ((tax * (double)rateOfExchange)) * (passengerCount);
                                            result.BaseFare += ((baseFare * (double)rateOfExchange)) * (passengerCount);
                                            result.TotalFare = result.BaseFare + result.Tax;

                                            newFareRule.FareInfoRef = fareInfoNode.FareID.ToString();
                                            Fare fare = result.FareBreakdown[q];
                                            //foreach (Fare fare in result.FareBreakdown)
                                            {
                                                if (ptype == fare.PassengerType && ptype == PassengerType.Adult)
                                                {
                                                    fare.TotalFare += ((baseFare + tax) * (double)rateOfExchange) * request.AdultCount;
                                                    fare.SellingFare += fare.TotalFare * request.AdultCount;
                                                    //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                                    fare.PassengerType = PassengerType.Adult;
                                                    fare.PassengerCount = request.AdultCount;
                                                    fare.BaseFare += (baseFare * (double)rateOfExchange) * request.AdultCount;
                                                    fare.SupplierFare += (double)(baseFare + tax) * request.AdultCount;
                                                    fare.FareType = retFareInfoList[c].FareTypeName;
                                                    //if (newFareRule.FareBasisCode != null && string.IsNullOrEmpty(newFareRule.FareBasisCode.Trim()))
                                                    {
                                                        newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                    }
                                                }
                                                else if (ptype == fare.PassengerType && ptype == PassengerType.Child)
                                                {
                                                    fare.TotalFare += ((baseFare + tax) * (double)rateOfExchange) * request.ChildCount;
                                                    fare.SellingFare += fare.TotalFare * request.ChildCount;
                                                    //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                                    fare.PassengerType = ptype;
                                                    fare.PassengerCount = request.ChildCount;
                                                    fare.BaseFare += (baseFare * (double)rateOfExchange) * request.ChildCount;
                                                    fare.SupplierFare += (double)(baseFare + tax) * request.ChildCount;
                                                    //if (newFareRule.FareBasisCode != null && string.IsNullOrEmpty(newFareRule.FareBasisCode.Trim()))
                                                    {
                                                        newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                    }
                                                }
                                                else if (ptype == fare.PassengerType && ptype == PassengerType.Infant)
                                                {
                                                    fare.TotalFare += ((baseFare + tax) * (double)rateOfExchange) * request.InfantCount;
                                                    fare.SellingFare += fare.TotalFare * request.InfantCount;
                                                    //fare.Tax = Convert.ToDecimal(result.Tax);
                                                    fare.PassengerType = ptype;
                                                    fare.PassengerCount = request.InfantCount;
                                                    fare.BaseFare += (baseFare * (double)rateOfExchange) * request.InfantCount;
                                                    fare.SupplierFare += (double)(baseFare + tax) * request.InfantCount;
                                                    //if (newFareRule.FareBasisCode != null && string.IsNullOrEmpty(newFareRule.FareBasisCode.Trim()))
                                                    {
                                                        newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                    }

                                                }
                                            }

                                            //if (result.FareBreakdown[fareBreakDownCounter].PassengerType == ptype)
                                            //{
                                            //    if (request.AdultCount > 0)
                                            //    {
                                            //        result.FareBreakdown[fareBreakDownCounter].TotalFare += (baseFare + tax) * request.AdultCount;
                                            //        result.FareBreakdown[fareBreakDownCounter].SellingFare += result.FareBreakdown[fareBreakDownCounter].TotalFare * request.AdultCount;
                                            //        //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                            //        result.FareBreakdown[fareBreakDownCounter].PassengerType = PassengerType.Adult;
                                            //        result.FareBreakdown[fareBreakDownCounter].PassengerCount = request.AdultCount;
                                            //        result.FareBreakdown[fareBreakDownCounter].BaseFare += baseFare * request.AdultCount;
                                            //        fareBreakDownCounter++;
                                            //    }
                                            //    if (request.SeniorCount > 0)
                                            //    {

                                            //        result.FareBreakdown[fareBreakDownCounter].TotalFare += (baseFare + tax) * request.SeniorCount;
                                            //        result.FareBreakdown[fareBreakDownCounter].SellingFare += result.FareBreakdown[fareBreakDownCounter].TotalFare * request.SeniorCount;
                                            //        //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                            //        result.FareBreakdown[fareBreakDownCounter].PassengerType = PassengerType.Senior;
                                            //        result.FareBreakdown[fareBreakDownCounter].PassengerCount = request.SeniorCount;
                                            //        result.FareBreakdown[fareBreakDownCounter].BaseFare += baseFare * request.SeniorCount;
                                            //        fareBreakDownCounter++;
                                            //    }
                                            //    newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                            //}
                                            //else if (result.FareBreakdown[fareBreakDownCounter].PassengerType == ptype)
                                            //{

                                            //    result.FareBreakdown[fareBreakDownCounter].TotalFare += (baseFare + tax) * request.ChildCount;
                                            //    result.FareBreakdown[fareBreakDownCounter].SellingFare += result.FareBreakdown[fareBreakDownCounter].TotalFare * request.ChildCount;
                                            //    //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                            //    result.FareBreakdown[fareBreakDownCounter].PassengerType = PassengerType.Child;
                                            //    result.FareBreakdown[fareBreakDownCounter].PassengerCount = request.ChildCount;
                                            //    result.FareBreakdown[fareBreakDownCounter].BaseFare += baseFare * request.ChildCount;
                                            //    fareBreakDownCounter++;
                                            //    if (newFareRule.FareBasisCode != null && string.IsNullOrEmpty(newFareRule.FareBasisCode.Trim()))
                                            //    {
                                            //        newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                            //    }
                                            //}
                                            //else if (result.FareBreakdown[fareBreakDownCounter].PassengerType == ptype)
                                            //{

                                            //    result.FareBreakdown[fareBreakDownCounter].TotalFare += (baseFare + tax) * request.InfantCount;
                                            //    result.FareBreakdown[fareBreakDownCounter].SellingFare += result.FareBreakdown[fareBreakDownCounter].TotalFare * request.InfantCount;
                                            //    //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                            //    result.FareBreakdown[fareBreakDownCounter].PassengerType = PassengerType.Infant;
                                            //    result.FareBreakdown[fareBreakDownCounter].PassengerCount = request.InfantCount;
                                            //    result.FareBreakdown[fareBreakDownCounter].BaseFare += baseFare * request.InfantCount;
                                            //    fareBreakDownCounter++;
                                            //    if (newFareRule.FareBasisCode != null && string.IsNullOrEmpty(newFareRule.FareBasisCode.Trim()))
                                            //    {
                                            //        newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                            //    }
                                            //}
                                            //k++;
                                        }

                                        int legCount = 0;
                                        for (int m = 0; m < retFSegment.FlightLegDetails.Length; m++)
                                        {
                                            FlightLegDetail legDetail = retFSegment.FlightLegDetails[m];

                                            for (int l = 0; l < fareQuote.LegDetails.Length; l++)
                                            {
                                                LegDetail leg = fareQuote.LegDetails[l];
                                                if (leg.PFID == legDetail.PFID && leg.DepartureDate == legDetail.DepartureDate)
                                                {
                                                    FlightInfo fInfo = new FlightInfo();
                                                    fInfo.Airline = result.Airline;
                                                    fInfo.ArrivalTime = Convert.ToDateTime(leg.ArrivalDate.ToString().Replace("T", " "));//DateTime.ParseExact(legNode.ArrivalDate.ToString().Replace("T"," "), "yyyy-MM-dd hh:mm:ss", null);
                                                    if (retSegment.FlightNum.Contains("/"))
                                                    {
                                                        fInfo.FlightNumber = retSegment.FlightNum.Split('/')[m];
                                                    }
                                                    else
                                                    {
                                                        fInfo.FlightNumber = retSegment.FlightNum;
                                                    }
                                                    fInfo.DepartureTime = Convert.ToDateTime(leg.DepartureDate.ToString().Replace("T", " ")); //DateTime.ParseExact(legNode.DepartureDate.ToString().Replace("T", " "), "yyyy-MM-dd hh:mm:ss", null);
                                                    fInfo.Destination = new Airport(leg.Destination.Trim());
                                                    fInfo.Origin = new Airport(leg.Origin.Trim());
                                                    fInfo.Status = "HK";
                                                    fInfo.SegmentFareType = retFareInfoList[c].FareTypeName;
                                                    newFareRule.Airline = result.Airline;
                                                    newFareRule.DepartureTime = fInfo.DepartureTime;
                                                    newFareRule.Destination = fInfo.Destination.AirportName;
                                                    newFareRule.Origin = fInfo.Origin.AirportName;
                                                    newFareRule.ReturnDate = fInfo.ArrivalTime;

                                                    fInfo.BookingClass = bookingClass;
                                                    fInfo.CabinClass = cabin;
                                                    fInfo.Duration = new TimeSpan(0, (int)leg.FlightTime, 0);//fInfo.ArrivalTime.Subtract(fInfo.DepartureTime);
                                                    fInfo.OperatingCarrier = leg.OperatingCarrier;
                                                    fInfo.DepTerminal = leg.FromTerminal;
                                                    fInfo.ArrTerminal = leg.ToTerminal;
                                                    fInfo.Craft = retSegment.AircraftType;
                                                    fInfo.UapiSegmentRefKey = retSegment.LFID.ToString();
                                                    fInfo.Stops = retSegment.Stops;
                                                    fInfo.Group = 1;
                                                    if (retSegment.Origin == request.Segments[0].Destination || ((request.Segments[0].Destination == "DXB" || request.Segments[0].Destination == "DWC") && (retSegment.Origin == "DXB" || retSegment.Origin == "DWC")))
                                                    {
                                                        result.Flights[1][legCount] = fInfo;
                                                    }
                                                    legCount++;
                                                    break;
                                                }
                                            }
                                        }


                                        result.FareRules.Add(newFareRule);

                                        #endregion


                                        //if (fareInfoNode.PTCID == 1)
                                        //{
                                        //    k++;
                                        //}
                                    }
                                    int bagCount = 0;
                                    //foreach (KeyValuePair<long, bool> pair in result.BaggageIncluded)
                                    //{
                                    //    if (pair.Value)
                                    //    {
                                    //        bagCount+= ;
                                    //    }
                                    //    else
                                    //    {
                                    //        bagCount--;
                                    //    }
                                    //}
                                    result.Price.TransactionFee += transactionFee;
                                    bagCount = Convert.ToInt32(result.BaggageIncludedInFare.Split(',')[0]);

                                    if (result.BaggageIncludedInFare.Split(',').Length > 1)
                                    {
                                        bagCount = Convert.ToInt32(result.BaggageIncludedInFare.Split(',')[1]);
                                    }
                                    if (bagCount == 80)
                                    {
                                        result.IsBaggageIncluded = true;
                                    }
                                    else
                                    {
                                        result.IsBaggageIncluded = false;
                                    }

                                    resultList.Add(result);
                                }
                            }
                            #endregion
                        }
                    }
                    else // One Way results
                    {
                       
                        #region FareInfo
                        for (int k = 0; k < onFareInfoList.Count; k++)
                        {
                            SearchResult result = new SearchResult();
                            int fareBreakDownCounter = 0;
                            result.IsLCC = true;
                            int passengerCount = 0;
                            result.ValidatingAirline = onSegment.CarrierCode.Trim();
                            result.Airline = onSegment.CarrierCode.Trim();
                            result.ResultBookingSource = BookingSource.FlyDubai;
                            result.GUID = guid;
                            result.Currency = fareQuote.RequestedCurrencyOfFareQuote;

                            //Sorting Pax Type order
                            List<FareInfo> fareInfos = new List<FareInfo>();
                            for (int p = 0; p < onFareInfoList[k].FareInfos.Length; p++)
                            {
                                FlyDubai.FareQuote.FareInfo fareInfoNode = onFareInfoList[k].FareInfos[p];
                                fareInfos.Add(fareInfoNode);
                            }

                            onFareInfoList[k].FareInfos[0] = fareInfos.Find(delegate(FareInfo f) { return f.PTCID == 1; });
                            if (onFareInfoList[k].FareInfos.Length > 1)
                            {
                                if (request.ChildCount > 0)
                                {
                                    onFareInfoList[k].FareInfos[1] = fareInfos.Find(delegate(FareInfo f) { return f.PTCID == 6; });
                                }
                                else
                                {
                                    onFareInfoList[k].FareInfos[1] = fareInfos.Find(delegate(FareInfo f) { return f.PTCID == 5; });
                                }
                            }
                            if (onFareInfoList[k].FareInfos.Length > 2)
                            {
                                if (request.InfantCount > 0)
                                {
                                    onFareInfoList[k].FareInfos[2] = fareInfos.Find(delegate(FareInfo f) { return f.PTCID == 5; });
                                }
                            }
                            //End Sorting

                            for (int j = 0; j < onFareInfoList[k].FareInfos.Length; j++)
                            {
                                
                                if (request.Type == SearchType.OneWay)
                                {
                                    result.Flights = new FlightInfo[1][];
                                    result.Flights[0] = new FlightInfo[onFSegment.FlightLegDetails.Length];
                                }

                                //FlyDubai.FareQuote.FareInfo[] fareInfoList = onFSegment.FareTypes[0].FareInfos;
                                string bookingClass = onFareInfoList[k].FareInfos[j].FCCode;
                                string cabin = onFareInfoList[k].FareInfos[j].Cabin;
                                if (result.FareBreakdown == null)
                                {
                                    if (request.AdultCount > 0 && request.SeniorCount > 0)
                                    {
                                        result.FareBreakdown = new Fare[paxType + 1];
                                    }
                                    else
                                    {
                                        int count = 0;
                                        if (request.ChildCount > 0)
                                        {
                                            count++;
                                        }
                                        if (request.InfantCount > 0)
                                        {
                                            count++;
                                        }
                                        result.FareBreakdown = new Fare[paxType + count];
                                    }

                                    result.FareInformationId = new Dictionary<string, List<int>>();
                                    result.FareRules = new List<FareRule>();
                                }
                                FareRule newFareRule = new FareRule();

                                
                                FlyDubai.FareQuote.FareInfo fareInfoNode = onFareInfoList[k].FareInfos[j];


                                if (agentBaseCurrency != currency)
                                {
                                    result.Currency = agentBaseCurrency;
                                }
                                ApplicableTaxDetail[] taxNodeList = fareInfoNode.ApplicableTaxDetails;
                                double tax = 0;
                                if (result.Price == null)
                                {
                                    result.Price = new PriceAccounts();
                                }
                                //for (int j = 0; j < result.FareBreakdown.Length; j++)
                                {
                                    PassengerType ptype = PassengerType.Adult;
                                    if (fareInfoNode.PTCID.ToString() == "1")
                                    {
                                        passengerCount = request.AdultCount + request.SeniorCount;
                                        ptype = PassengerType.Adult;
                                        List<int> fareIds = new List<int>();
                                        fareIds.Add(fareInfoNode.FareID);
                                        result.FareInformationId.Add(PassengerType.Adult.ToString(), fareIds);
                                        result.FareType = onFareInfoList[k].FareTypeName;

                                        //result.BaggageIncluded = new Dictionary<long, bool>();
                                        result.BaggageIncludedInFare = "0";

                                        foreach (ApplicableTaxDetail taxNode in taxNodeList)
                                        {
                                            foreach (TaxDetail td in fareQuote.TaxDetails)
                                            {
                                                if (taxNode.TaxID == td.TaxID && (td.CodeType.Contains("BAG")))
                                                {
                                                    result.Price.BaggageCharge = 0;
                                                    //if (result.BaggageIncluded.ContainsKey(onFareInfoList[k].FareTypeID))
                                                    //{
                                                    //    result.BaggageIncluded[onFareInfoList[k].FareTypeID] = true;
                                                    //}
                                                    //else
                                                    //{
                                                    //    result.BaggageIncluded.Add(onFareInfoList[k].FareTypeID, true);
                                                    //}
                                                    result.BaggageIncludedInFare = td.TaxDesc.Substring(0, 2);
                                                }
                                            }
                                        }
                                    }
                                    else if (fareInfoNode.PTCID.ToString() == "6")
                                    {
                                        passengerCount = request.ChildCount;
                                        ptype = PassengerType.Child;
                                        List<int> fareIds = new List<int>();
                                        fareIds.Add(fareInfoNode.FareID);
                                        result.FareInformationId.Add(PassengerType.Child.ToString(), fareIds);
                                    }
                                    else if (fareInfoNode.PTCID == 5)
                                    {
                                        passengerCount = request.InfantCount;
                                        ptype = PassengerType.Infant;
                                        List<int> fareIds = new List<int>();
                                        fareIds.Add(fareInfoNode.FareID);
                                        result.FareInformationId.Add(PassengerType.Infant.ToString(), fareIds);
                                    }
                                    
                                    //foreach (ApplicableTaxDetail taxNode in taxNodeList)
                                    //{
                                    //    tax += Convert.ToDouble(taxNode.Amt);
                                    //}

                                    

                                    //if (!result.IsBaggageIncluded)
                                    //{
                                    //    result.BaggageIncludedInFare = "0";
                                    //}

                                    double baseFare = (Convert.ToDouble(fareInfoNode.DisplayFareAmt));
                                    
                                    if (transFeeType == "P")
                                    {
                                        baseFare += baseFare * (double)transactionFee / 100;
                                    }
                                    else
                                    {
                                        baseFare += (double)transactionFee;
                                    }

                                    tax = Convert.ToDouble(fareInfoNode.DisplayTaxSum);

                                    result.Price.RateOfExchange = rateOfExchange;
                                    result.Price.SupplierCurrency = fareQuote.RequestedCurrencyOfFareQuote;
                                    result.Price.SupplierPrice += (decimal)(baseFare + tax) * passengerCount;
                                    result.Price.TransactionFee = transactionFee;

                                   
                                    bookingClass = fareInfoNode.FCCode;
                                    cabin = fareInfoNode.Cabin;
                                    //result.Tax += (Math.Round(tax * (double)rateOfExchange, decimalValue)) * (passengerCount);
                                    //result.BaseFare += (Math.Round(baseFare * (double)rateOfExchange, decimalValue)) * (passengerCount);
                                    result.Tax += ((tax * (double)rateOfExchange)) * (passengerCount);
                                    result.BaseFare += ((baseFare * (double)rateOfExchange)) * (passengerCount);
                                    result.TotalFare = result.BaseFare + result.Tax;


                                    if (ptype == PassengerType.Adult)
                                    {
                                        if (request.AdultCount > 0)
                                        {
                                            result.FareBreakdown[fareBreakDownCounter] = new Fare();
                                            result.FareBreakdown[fareBreakDownCounter].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.AdultCount;
                                            result.FareBreakdown[fareBreakDownCounter].SellingFare = result.FareBreakdown[fareBreakDownCounter].TotalFare * request.AdultCount;
                                            //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                            result.FareBreakdown[fareBreakDownCounter].PassengerType = PassengerType.Adult;
                                            result.FareBreakdown[fareBreakDownCounter].PassengerCount = request.AdultCount;
                                            result.FareBreakdown[fareBreakDownCounter].BaseFare = (baseFare * (double)rateOfExchange) * request.AdultCount;
                                            result.FareBreakdown[fareBreakDownCounter].SupplierFare = (double)(baseFare + tax) * request.AdultCount;
                                            result.FareBreakdown[fareBreakDownCounter].FareType = onFareInfoList[k].FareTypeName;                                            
                                            fareBreakDownCounter++;
                                        }
                                        if (request.SeniorCount > 0)
                                        {
                                            result.FareBreakdown[fareBreakDownCounter] = new Fare();
                                            result.FareBreakdown[fareBreakDownCounter].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.SeniorCount;
                                            result.FareBreakdown[fareBreakDownCounter].SellingFare = result.FareBreakdown[fareBreakDownCounter].TotalFare * request.SeniorCount;
                                            //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                            result.FareBreakdown[fareBreakDownCounter].PassengerType = PassengerType.Senior;
                                            result.FareBreakdown[fareBreakDownCounter].PassengerCount = request.SeniorCount;
                                            result.FareBreakdown[fareBreakDownCounter].BaseFare = baseFare * request.SeniorCount;
                                            fareBreakDownCounter++;
                                        }
                                        newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                    }
                                    else if (ptype == PassengerType.Child)
                                    {
                                        result.FareBreakdown[fareBreakDownCounter] = new Fare();
                                        result.FareBreakdown[fareBreakDownCounter].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.ChildCount;
                                        result.FareBreakdown[fareBreakDownCounter].SellingFare = result.FareBreakdown[fareBreakDownCounter].TotalFare * request.ChildCount;
                                        //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                        result.FareBreakdown[fareBreakDownCounter].PassengerType = PassengerType.Child;
                                        result.FareBreakdown[fareBreakDownCounter].PassengerCount = request.ChildCount;
                                        result.FareBreakdown[fareBreakDownCounter].BaseFare = (baseFare * (double)rateOfExchange) * request.ChildCount;
                                        result.FareBreakdown[fareBreakDownCounter].SupplierFare = (double)(baseFare + tax) * request.ChildCount;
                                        fareBreakDownCounter++;
                                        //if (newFareRule.FareBasisCode != null && string.IsNullOrEmpty(newFareRule.FareBasisCode.Trim()))
                                        {
                                            newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                        }
                                    }
                                    else if (ptype == PassengerType.Infant)
                                    {
                                        result.FareBreakdown[fareBreakDownCounter] = new Fare();
                                        result.FareBreakdown[fareBreakDownCounter].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.InfantCount;
                                        result.FareBreakdown[fareBreakDownCounter].SellingFare = result.FareBreakdown[fareBreakDownCounter].TotalFare * request.InfantCount;
                                        //result.FareBreakdown[fareBreakDownCounter].Tax = Convert.ToDecimal(result.Tax);
                                        result.FareBreakdown[fareBreakDownCounter].PassengerType = PassengerType.Infant;
                                        result.FareBreakdown[fareBreakDownCounter].PassengerCount = request.InfantCount;
                                        result.FareBreakdown[fareBreakDownCounter].BaseFare = (baseFare * (double)rateOfExchange) * request.InfantCount;
                                        result.FareBreakdown[fareBreakDownCounter].SupplierFare = (double)(baseFare + tax) * request.InfantCount;
                                        fareBreakDownCounter++;
                                        //if (newFareRule.FareBasisCode != null && string.IsNullOrEmpty(newFareRule.FareBasisCode.Trim()))
                                        {
                                            newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                        }
                                    }
                                }



                                int legCount = 0;
                                for (int m = 0; m < onFSegment.FlightLegDetails.Length; m++)
                                {
                                    FlightLegDetail legDetail = onFSegment.FlightLegDetails[m];

                                    for (int l = 0; l < fareQuote.LegDetails.Length; l++)
                                    {
                                        LegDetail leg = fareQuote.LegDetails[l];
                                        if (leg.PFID == legDetail.PFID && leg.DepartureDate == legDetail.DepartureDate)
                                        {
                                            FlightInfo fInfo = new FlightInfo();
                                            fInfo.Airline = result.Airline;
                                            fInfo.ArrivalTime = Convert.ToDateTime(leg.ArrivalDate.ToString().Replace("T", " "));//DateTime.ParseExact(legNode.ArrivalDate.ToString().Replace("T"," "), "yyyy-MM-dd hh:mm:ss", null);
                                            if (onSegment.FlightNum.Contains("/"))
                                            {
                                                fInfo.FlightNumber = onSegment.FlightNum.Split('/')[m];
                                            }
                                            else
                                            {
                                                fInfo.FlightNumber = onSegment.FlightNum;
                                            }
                                            fInfo.DepartureTime = Convert.ToDateTime(leg.DepartureDate.ToString().Replace("T", " ")); //DateTime.ParseExact(legNode.DepartureDate.ToString().Replace("T", " "), "yyyy-MM-dd hh:mm:ss", null);
                                            fInfo.Destination = new Airport(leg.Destination.Trim());
                                            fInfo.Origin = new Airport(leg.Origin.Trim());
                                            fInfo.Status = "HK";
                                            fInfo.SegmentFareType = onFareInfoList[k].FareTypeName;
                                            newFareRule.Airline = result.Airline;
                                            newFareRule.DepartureTime = fInfo.DepartureTime;
                                            newFareRule.Destination = fInfo.Destination.AirportName;
                                            newFareRule.Origin = fInfo.Origin.AirportName;
                                            newFareRule.ReturnDate = fInfo.ArrivalTime;

                                            fInfo.BookingClass = bookingClass;
                                            fInfo.CabinClass = cabin;
                                            fInfo.Duration = new TimeSpan(0, (int)leg.FlightTime, 0);//fInfo.ArrivalTime.Subtract(fInfo.DepartureTime);
                                            fInfo.OperatingCarrier = leg.OperatingCarrier;
                                            fInfo.DepTerminal = leg.FromTerminal;
                                            fInfo.ArrTerminal = leg.ToTerminal;
                                            fInfo.Craft = onSegment.AircraftType;
                                            fInfo.Stops = onSegment.Stops;
                                            fInfo.UapiSegmentRefKey = onSegment.LFID.ToString();
                                            fInfo.Group = 0;
                                            if (onSegment.Origin == request.Segments[0].Origin || ((request.Segments[0].Origin == "DXB" || request.Segments[0].Origin == "DWC") && (onSegment.Origin == "DXB" || onSegment.Origin == "DWC")))
                                            {
                                                result.Flights[0][legCount] = fInfo;
                                            }
                                            legCount++;
                                            break;
                                        }
                                    }
                                }
                                result.FareRules.Add(newFareRule);
                            }

                            int bagCount = 0;

                            bagCount = Convert.ToInt32(result.BaggageIncludedInFare);

                            if (result.FareType == "Basic" || bagCount == 40)
                            {
                                result.IsBaggageIncluded = true;
                            }
                            else
                            {
                                result.IsBaggageIncluded = false;
                            }

                            resultList.Add(result);
                        }
                        #endregion
                    }
                }                
            }
            else
            {
                throw new BookingEngineException("FlyDubai:No Flights found");
            }
            return resultList.ToArray();
        }
        
        /// <summary>
        /// Retrieves the lowest possible fare from the Fare types for the Flight Segment.
        /// </summary>
        /// <param name="fareTypes"></param>
        /// <param name="passengerType"></param>
        /// <returns></returns>
        private int[] GetLowestFareIndex(FlyDubai.FareQuote.FareType[] fareTypes, string passengerType)
        {
            int index = 0;
            int fareTypeIndex = 0;
            int i = 0;
            decimal lowestfare = 0;
            string[] paxTypes = passengerType.Split(',');
            foreach (FlyDubai.FareQuote.FareType fareType in fareTypes)
            {
                //Assuming that pay to change contains cheapest fare                
                if (fareType.FareTypeID == 1)
                {
                    FlyDubai.FareQuote.FareInfo[] fareInfos = fareType.FareInfos;
                    decimal fare = 0;
                    foreach (FlyDubai.FareQuote.FareInfo fareInfo in fareInfos)
                    {
                        foreach (string paxType in paxTypes)
                        {
                            //Assuming if adult is cheap than child will also be cheap
                            if (fareInfo.PTCID.ToString() == paxType)
                            {
                                fare = Convert.ToDecimal(fareInfo.FareAmtInclTax);
                            }
                            else
                            {
                                i++;
                                continue;
                            }
                            if (fare < lowestfare || lowestfare == 0)
                            {
                                lowestfare = fare;
                                index = i;
                            }
                        }
                        i++;
                    }
                    break;
                }
                else
                {
                    fareTypeIndex++;
                }
            }
            int[] lowestIndex = new int[2];
            lowestIndex[0] = fareTypeIndex;
            lowestIndex[1] = index;
            return lowestIndex;
        }

        /// <summary>
        /// Retrieves available baggage details for the Flight.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public DataTable GetBaggageServicesQuotes(SearchResult result, string guid)
        {
            ViewServiceQuotes viewServiceQuotes = null;
            
            try
            {
                FlyDubai.ServiceQuote.RetrieveServiceQuotes serviceQuotes = new FlyDubai.ServiceQuote.RetrieveServiceQuotes();

                serviceQuotes.CarrierCodes = new FlyDubai.ServiceQuote.CarrierCode[1];
                serviceQuotes.CarrierCodes[0] = new FlyDubai.ServiceQuote.CarrierCode();
                serviceQuotes.CarrierCodes[0].AccessibleCarrierCode = "FZ";
                serviceQuotes.HistoricUserName = GetUserName(result.Flights[0][0].Origin.CityCode);
                List<FlyDubai.ServiceQuote.ServiceQuote> Quotes = new List<FlyDubai.ServiceQuote.ServiceQuote>();
                for (int i = 0; i < result.Flights.Length; i++ )
                {
                    for (int j = 0; j < result.Flights[i].Length; j++)
                    {
                        FlightInfo fInfo = result.Flights[i][j];
                        FlyDubai.ServiceQuote.ServiceQuote serviceQuote = new FlyDubai.ServiceQuote.ServiceQuote();
                        serviceQuote.AirportCode = fInfo.Origin.AirportCode;
                        serviceQuote.Cabin = fInfo.CabinClass;                        
                        serviceQuote.Category = "";
                        serviceQuote.Currency = FlyDubai.ServiceQuote.EnumerationsCurrencyCodeTypes.AED;
                        serviceQuote.DepartureDate = Convert.ToDateTime(fInfo.DepartureTime.ToString("yyyy-MM-dd"));
                        serviceQuote.FareBasisCode = "";
                        serviceQuote.FareClass = "";
                        serviceQuote.LogicalFlightID = Convert.ToInt32(fInfo.UapiSegmentRefKey);
                        serviceQuote.MarketingCarrierCode = "FZ";
                        serviceQuote.OperatingCarrierCode = "FZ";
                        serviceQuote.ReservationChannel = FlyDubai.ServiceQuote.EnumerationsReservationChannelTypes.TPAPI;
                        serviceQuote.DestinationAirportCode = fInfo.Destination.AirportCode;
                        serviceQuote.ServiceCode = "";                        
                        serviceQuote.UTCOffset = 0;                        

                        Quotes.Add(serviceQuote);
                    }
                }
                
                serviceQuotes.SecurityGUID = guid;
                serviceQuotes.RetrieveServiceQuotes1 = Quotes.ToArray();

                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(FlyDubai.ServiceQuote.RetrieveServiceQuotes));
                    string filePath = @"" + xmlPath + sessionId + "_" + "FlyDubaiRetrieveBaggageQuoteRequest_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, serviceQuotes);
                    sw.Close();
                    ser = null;
                    //Audit.Add(EventType.FlyDubai, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }
                
                viewServiceQuotes = ssr.RetrieveServiceQuotes(serviceQuotes);

                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(ViewServiceQuotes));
                    string filePath = @"" + xmlPath + sessionId + "_" + "FlyDubaiRetrieveBaggageQuoteResponse_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, viewServiceQuotes);
                    sw.Close();
                    ser = null;
                    //Audit.Add(EventType.FlyDubai, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

                if (viewServiceQuotes.Exceptions[0].ExceptionCode > 0)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.FlyDubaiFareQuote, CT.Core.Severity.High, 1, viewServiceQuotes.Exceptions[0].ExceptionDescription, "127.0.0.1");
                }
                else
                {
                    if (agentBaseCurrency != viewServiceQuotes.ServiceQuotes[0].CurrencyCode)
                    {
                        rateOfExchange = exchangeRates[viewServiceQuotes.ServiceQuotes[0].CurrencyCode];
                    }

                    foreach (ViewServiceQuote quote in viewServiceQuotes.ServiceQuotes)
                    {
                        DataRow row = dtBaggageQuotes.NewRow();
                        row["CodeType"] = quote.CodeType;
                        row["Description"] = quote.Description;
                        row["Amount"] = quote.Amount * rateOfExchange;
                        row["QtyAvailable"] = quote.QuantityAvailable;
                        row["CategoryID"] = quote.CategoryID;
                        if (rateOfExchange > 1)
                        {
                            row["CurrencyCode"] = agentBaseCurrency;
                        }
                        else
                        {
                            row["CurrencyCode"] = quote.CurrencyCode;
                        }
                        row["LogicalFlightID"] = quote.LogicalFlightID;
                        row["ServiceID"] = quote.ServiceID;
                        row["Refundable"] = quote.Refundable;                        
                        row["AmountType"] = quote.AmountType;

                        dtBaggageQuotes.Rows.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.FlyDubaiFareQuote, CT.Core.Severity.High, 1, ex.ToString(), "127.0.0.1");
            }
            return dtBaggageQuotes;
        }


        /// <summary>
        /// For B2C BookingAPI
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="flights"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public DataTable GetBaggageServicesQuotes(Airport origin,FlightInfo[][] flights, string guid)
        {
            ViewServiceQuotes viewServiceQuotes = null;

            try
            {
                FlyDubai.ServiceQuote.RetrieveServiceQuotes serviceQuotes = new FlyDubai.ServiceQuote.RetrieveServiceQuotes();

                serviceQuotes.CarrierCodes = new FlyDubai.ServiceQuote.CarrierCode[1];
                serviceQuotes.CarrierCodes[0] = new FlyDubai.ServiceQuote.CarrierCode();
                serviceQuotes.CarrierCodes[0].AccessibleCarrierCode = "FZ";
                serviceQuotes.HistoricUserName = GetUserName(origin.CityCode);
                List<FlyDubai.ServiceQuote.ServiceQuote> Quotes = new List<FlyDubai.ServiceQuote.ServiceQuote>();
                for (int i = 0; i < flights.Length; i++)
                {
                    for (int j = 0; j < flights[i].Length; j++)
                    {
                        FlightInfo fInfo = flights[i][j];
                        FlyDubai.ServiceQuote.ServiceQuote serviceQuote = new FlyDubai.ServiceQuote.ServiceQuote();
                        serviceQuote.AirportCode = fInfo.Origin.AirportCode;
                        serviceQuote.Cabin = fInfo.CabinClass;
                        serviceQuote.Category = "";
                        serviceQuote.Currency = FlyDubai.ServiceQuote.EnumerationsCurrencyCodeTypes.AED;
                        serviceQuote.DepartureDate = Convert.ToDateTime(fInfo.DepartureTime.ToString("yyyy-MM-dd"));
                        serviceQuote.FareBasisCode = "";
                        serviceQuote.FareClass = "";
                        serviceQuote.LogicalFlightID = Convert.ToInt32(fInfo.UapiSegmentRefKey);
                        serviceQuote.MarketingCarrierCode = "FZ";
                        serviceQuote.OperatingCarrierCode = "FZ";
                        serviceQuote.ReservationChannel = FlyDubai.ServiceQuote.EnumerationsReservationChannelTypes.TPAPI;
                        serviceQuote.DestinationAirportCode = fInfo.Destination.AirportCode;
                        serviceQuote.ServiceCode = "";
                        serviceQuote.UTCOffset = 0;

                        Quotes.Add(serviceQuote);
                    }
                }

                serviceQuotes.SecurityGUID = guid;
                serviceQuotes.RetrieveServiceQuotes1 = Quotes.ToArray();

                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(FlyDubai.ServiceQuote.RetrieveServiceQuotes));
                    string filePath = @"" + xmlPath + sessionId + "_" + "FlyDubaiRetrieveBaggageQuoteRequest_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, serviceQuotes);
                    sw.Close();
                    ser = null;
                    //Audit.Add(EventType.FlyDubai, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

                viewServiceQuotes = ssr.RetrieveServiceQuotes(serviceQuotes);

                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(ViewServiceQuotes));
                    string filePath = @"" + xmlPath + sessionId + "_" + "FlyDubaiRetrieveBaggageQuoteResponse_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, viewServiceQuotes);
                    sw.Close();
                    ser = null;
                    //Audit.Add(EventType.FlyDubai, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }

                if (viewServiceQuotes.Exceptions[0].ExceptionCode > 0)
                {
                    CT.Core.Audit.Add(CT.Core.EventType.FlyDubaiFareQuote, CT.Core.Severity.High, 1, viewServiceQuotes.Exceptions[0].ExceptionDescription, "127.0.0.1");
                }
                else
                {
                    if (agentBaseCurrency != viewServiceQuotes.ServiceQuotes[0].CurrencyCode)
                    {
                        rateOfExchange = exchangeRates[viewServiceQuotes.ServiceQuotes[0].CurrencyCode];
                    }

                    foreach (ViewServiceQuote quote in viewServiceQuotes.ServiceQuotes)
                    {
                        DataRow row = dtBaggageQuotes.NewRow();
                        row["CodeType"] = quote.CodeType;
                        row["Description"] = quote.Description;
                        row["Amount"] = quote.Amount * rateOfExchange;
                        row["QtyAvailable"] = quote.QuantityAvailable;
                        row["CategoryID"] = quote.CategoryID;
                        if (rateOfExchange > 1)
                        {
                            row["CurrencyCode"] = agentBaseCurrency;
                        }
                        else
                        {
                            row["CurrencyCode"] = quote.CurrencyCode;
                        }
                        row["LogicalFlightID"] = quote.LogicalFlightID;
                        row["ServiceID"] = quote.ServiceID;
                        row["Refundable"] = quote.Refundable;
                        row["AmountType"] = quote.AmountType;

                        dtBaggageQuotes.Rows.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.FlyDubaiFareQuote, CT.Core.Severity.High, 1, ex.ToString(), "127.0.0.1");
            }
            return dtBaggageQuotes;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="result"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        private FlyDubai.ConfirmBooking.ViewPNR GenerateSummaryPNR(FlightItinerary itinerary,  SearchType searchType)
        {
            FlyDubai.ConfirmBooking.ViewPNR pnrInfo = new FlyDubai.ConfirmBooking.ViewPNR();

            try
            {
                SummaryPNR pnrRequest = new SummaryPNR();

                pnrRequest.ActionType = SummaryPNRActionTypes.GetSummary;
                pnrRequest.SecurityGUID = itinerary.GUID;
                pnrRequest.IATANum = GetIATA(itinerary.Segments[0].Origin.AirportCode);
                pnrRequest.User = GetUserName(itinerary.Segments[0].Origin.AirportCode);
                pnrRequest.SecurityToken = itinerary.GUID;
                pnrRequest.CarrierCurrency = FlyDubai.ConfirmBooking.EnumerationsCurrencyCodeTypes.AED;
                pnrRequest.DisplayCurrency = FlyDubai.ConfirmBooking.EnumerationsCurrencyCodeTypes.AED;
                pnrRequest.ReceiptLanguageID = "1";
                pnrRequest.PromoCode = "";
                pnrRequest.ExternalBookingID = "";
                pnrRequest.HistoricUserName = GetUserName(itinerary.Segments[0].Origin.AirportCode);
                pnrRequest.ClientIPAddress = "";

                pnrRequest.CarrierCodes = new FlyDubai.ConfirmBooking.CarrierCode[1];
                pnrRequest.CarrierCodes[0] = new FlyDubai.ConfirmBooking.CarrierCode();
                pnrRequest.CarrierCodes[0].AccessibleCarrierCode = "FZ";

                pnrRequest.ReservationInfo = new FlyDubai.ConfirmBooking.ReservationInfo();
                pnrRequest.ReservationInfo.SeriesNumber = "299";
                pnrRequest.ReservationInfo.ConfirmationNumber = "";

                //----------------Assign Address of Lead Pax here------------------
                pnrRequest.Address = new FlyDubai.ConfirmBooking.Address();
                pnrRequest.Address.Address1 = "# 302, Tower 400";
                pnrRequest.Address.Address2 = "Al Soor, Street No 2";
                pnrRequest.Address.AreaCode = "";
                pnrRequest.Address.City = "Sharjah";
                pnrRequest.Address.Country = "United Arab Emirates";
                pnrRequest.Address.CountryCode = "UAE";
                pnrRequest.Address.Display = "";
                pnrRequest.Address.PhoneNumber = "97160544470";
                pnrRequest.Address.Postal = "3393";
                pnrRequest.Address.State = "";
                //--------------------------End Address----------------------------

                //-------------------Begin Passenger details-------------------------
                int contactId = 2141;
                int personId = 214;
                long profileId = 2147483648;
                List<FlyDubai.ConfirmBooking.ContactInfo> contactInfos = new List<FlyDubai.ConfirmBooking.ContactInfo>();
                List<FlyDubai.ConfirmBooking.Person> Persons = new List<FlyDubai.ConfirmBooking.Person>();
                List<Segment> Segments = new List<Segment>();

                List<int> LFIDS = new List<int>();
                foreach (FlightInfo seg in itinerary.Segments)
                {
                    if (!LFIDS.Contains(Convert.ToInt32(seg.UapiSegmentRefKey)))
                    {
                        LFIDS.Add(Convert.ToInt32(seg.UapiSegmentRefKey));
                    }
                }

                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-US");
                int paxCount = 0;

                FlightPassenger pax1 = itinerary.Passenger[0];

                FlyDubai.ConfirmBooking.ContactInfo ci = new FlyDubai.ConfirmBooking.ContactInfo();
                ci.AreaCode = "na";
                //Modified by Lokesh on 27-Nov-2017. Pass Lead Pax email for contact info.
                if (!string.IsNullOrEmpty(itinerary.Passenger[0].Email))
                {
                    ci.ContactField = itinerary.Passenger[0].Email;
                }
                else
                {
                    ci.ContactField = "tech@cozmotravel.com";//to get ticket voucher emails from FZ
                }
                ci.ContactID = -contactId;
                ci.ContactType = FlyDubai.ConfirmBooking.EnumerationsContactTypes.Email;
                ci.CountryCode = "na";
                ci.Display = "na";
                ci.Extension = "na";
                ci.PersonOrgID = -personId;
                ci.PhoneNumber = "na";
                ci.PreferredContactMethod = true;

                contactInfos.Add(ci);
                contactId++;
                FlyDubai.ConfirmBooking.ContactInfo ci1 = new FlyDubai.ConfirmBooking.ContactInfo();
                ci1.AreaCode = pax1.CellPhone.Split('-')[0];
                ci1.ContactField = pax1.CellPhone.Replace("-", "");
                ci1.ContactID = -contactId;
                ci1.ContactType = FlyDubai.ConfirmBooking.EnumerationsContactTypes.MobilePhone;
                ci1.CountryCode = "";
                ci1.Display = "na";
                ci1.Extension = "na";
                ci1.PersonOrgID = -personId;
                ci1.PhoneNumber = pax1.CellPhone.Split('-')[1];

                ci1.PreferredContactMethod = false;
                contactInfos.Add(ci1);
                contactId++;

                FlyDubai.ConfirmBooking.ContactInfo ci2 = new FlyDubai.ConfirmBooking.ContactInfo();
                ci2.AreaCode = pax1.CellPhone.Split('-')[0];
                ci2.ContactField = pax1.CellPhone.Replace("-", "");
                ci2.ContactID = -contactId;
                ci2.ContactType = FlyDubai.ConfirmBooking.EnumerationsContactTypes.HomePhone;
                ci2.CountryCode = "";
                ci2.Display = "na";
                ci2.Extension = "na";
                ci2.PersonOrgID = -personId;
                ci2.PhoneNumber = pax1.CellPhone.Split('-')[1];

                ci2.PreferredContactMethod = false;
                contactInfos.Add(ci2);

                contactId = 2141;

                foreach (FlightPassenger pax in itinerary.Passenger)
                {
                    FlyDubai.ConfirmBooking.Person passenger = new FlyDubai.ConfirmBooking.Person();
                    passenger.Age = DateTime.Now.Subtract(pax.DateOfBirth).Days / 365;
                    passenger.Comments = "na";
                    passenger.Company = "na";
                    passenger.DOB = Convert.ToDateTime(pax.DateOfBirth.ToString("yyyy-MM-dd"));

                    passenger.FirstName = pax.FirstName;
                    passenger.LastName = pax.LastName;
                    passenger.MarketingOptIn = true;
                    passenger.MiddleName = "";
                    if (pax.Country != null)
                    {
                        if (!string.IsNullOrEmpty(pax.Country.Nationality))
                        {
                            passenger.Nationality = pax.Country.Nationality;
                        }
                    }
                    passenger.NationalityLaguageID = -214;
                    if (!string.IsNullOrEmpty(pax.PassportNo))
                    {
                        passenger.Passport = pax.PassportNo;
                    }
                    else
                    {
                        passenger.Passport = string.Empty;
                    }

                    if (pax.Gender == Gender.Male)
                    {
                        passenger.Gender = FlyDubai.ConfirmBooking.EnumerationsGenderTypes.Male;
                    }
                    else if (pax.Gender == Gender.Female)
                    {
                        passenger.Gender = FlyDubai.ConfirmBooking.EnumerationsGenderTypes.Female;
                    }
                    else
                    {
                        passenger.Gender = FlyDubai.ConfirmBooking.EnumerationsGenderTypes.Unknown;
                    }
                    if (pax.Type == PassengerType.Adult || pax.Type == PassengerType.Senior)
                    {
                        passenger.WBCID = 1;
                        passenger.PTCID = 1;
                        passenger.PTC = "1";
                        passenger.Title = pax.Title;
                    }
                    else if (pax.Type == PassengerType.Child)
                    {
                        passenger.WBCID = 6;
                        passenger.PTCID = 6;
                        passenger.PTC = "6";
                        if (passenger.Gender == FlyDubai.ConfirmBooking.EnumerationsGenderTypes.Male)
                        {
                            passenger.Title = "MSTR";
                        }
                        else
                        {
                            passenger.Title = "MISS";
                        }
                    }
                    else
                    {
                        passenger.WBCID = 5;
                        passenger.PTCID = 5;
                        passenger.PTC = "5";
                        if (passenger.Gender == FlyDubai.ConfirmBooking.EnumerationsGenderTypes.Male)
                        {
                            passenger.Title = "MSTR";
                        }
                        else
                        {
                            passenger.Title = "MISS";
                        }
                    }
                    passenger.RelationType = FlyDubai.ConfirmBooking.EnumerationsRelationshipTypes.Self;
                    passenger.RedressNumber = "na";
                    passenger.TravelsWithPersonOrgID = -214;
                    passenger.ProfileId = -profileId;
                    passenger.IsPrimaryPassenger = (pax.IsLeadPax ? true : false);
                    passenger.KnownTravelerNumber = "na";
                    passenger.PersonOrgID = -personId;
                    passenger.UseInventory = true;
                    passenger.MarketingOptIn = true;
                    passenger.Comments = "na";
                    passenger.Company = "na";

                    passenger.Address = new FlyDubai.ConfirmBooking.Address();
                    passenger.Address.Address1 = "# 302, Tower 400";
                    passenger.Address.Address2 = "Al Soor, Street No 2";
                    passenger.Address.AreaCode = "";
                    passenger.Address.City = "Sharjah";
                    passenger.Address.Country = "United Arab Emirates";
                    passenger.Address.CountryCode = "UAE";
                    passenger.Address.Display = "";
                    passenger.Address.PhoneNumber = "97160544470";
                    passenger.Address.Postal = "3393";
                    passenger.Address.State = "";

                    List<FlyDubai.ConfirmBooking.ContactInfo> Contacts = new List<FlyDubai.ConfirmBooking.ContactInfo>();
                    ci = new FlyDubai.ConfirmBooking.ContactInfo();
                    ci.AreaCode = "na";
                    //Modified by Lokesh on 27-Nov-2017. Pass Lead Pax email for contact info.
                    if (!string.IsNullOrEmpty(itinerary.Passenger[0].Email))
                    {
                        ci.ContactField = itinerary.Passenger[0].Email;//to get ticket voucher emails from FZ
                    }
                    else
                    {
                        ci.ContactField = "tech@cozmotravel.com";
                    }
                    ci.ContactID = -contactId;
                    ci.ContactType = FlyDubai.ConfirmBooking.EnumerationsContactTypes.Email;
                    ci.CountryCode = "na";
                    ci.Display = "na";
                    ci.Extension = "na";
                    ci.PersonOrgID = -personId;
                    ci.PhoneNumber = "na";
                    ci.PreferredContactMethod = true;

                    Contacts.Add(ci);
                    contactId++;

                    //Start:Added by Lokesh on 4-Oct-2017 Regarding Lead Pax Destination Phone Number.
                    if (pax.IsLeadPax && !string.IsNullOrEmpty(pax.DestinationPhone))
                    {
                        FlyDubai.ConfirmBooking.ContactInfo workPhoneContact = new FlyDubai.ConfirmBooking.ContactInfo();
                        workPhoneContact.AreaCode = pax.DestinationPhone.Split('-')[0];
                        workPhoneContact.ContactField = pax.DestinationPhone.Replace("-", "");
                        workPhoneContact.ContactID = -contactId;
                        workPhoneContact.ContactType = FlyDubai.ConfirmBooking.EnumerationsContactTypes.WorkPhone;
                        workPhoneContact.CountryCode = "";
                        workPhoneContact.Display = "na";
                        workPhoneContact.Extension = "na";
                        workPhoneContact.PersonOrgID = -personId;
                        workPhoneContact.PhoneNumber = pax.DestinationPhone.Split('-')[1];
                        workPhoneContact.PreferredContactMethod = false;
                        Contacts.Add(workPhoneContact);
                        contactId++;
                    }
                    //End:Lead Pax Destination Phone Number.

                    //else //Additional Pax as well as the Lead Pax if the Lead Pax does not have any destination phone number
                   // {
                        ci1 = new FlyDubai.ConfirmBooking.ContactInfo();
                        ci1.AreaCode = pax.CellPhone.Split('-')[0];
                        ci1.ContactField = pax.CellPhone.Replace("-", "");
                        ci1.ContactID = -contactId;
                        ci1.ContactType = FlyDubai.ConfirmBooking.EnumerationsContactTypes.MobilePhone;
                        ci1.CountryCode = "";
                        ci1.Display = "na";
                        ci1.Extension = "na";
                        ci1.PersonOrgID = -personId;
                        ci1.PhoneNumber = pax.CellPhone.Split('-')[1];
                        ci1.PreferredContactMethod = false;
                        Contacts.Add(ci1);
                        contactId++;
                   // }




                    ci2 = new FlyDubai.ConfirmBooking.ContactInfo();
                    ci2.AreaCode = pax.CellPhone.Split('-')[0];
                    ci2.ContactField = pax.CellPhone.Replace("-", "");
                    ci2.ContactID = -contactId;
                    ci2.ContactType = FlyDubai.ConfirmBooking.EnumerationsContactTypes.HomePhone;
                    ci2.CountryCode = "";
                    ci2.Display = "na";
                    ci2.Extension = "na";
                    ci2.PersonOrgID = -personId;
                    ci2.PhoneNumber = pax.CellPhone.Split('-')[1];

                    ci2.PreferredContactMethod = false;
                    Contacts.Add(ci2);
                    passenger.ContactInfos = Contacts.ToArray();

                    Persons.Add(passenger);



                    if (pax.Type != PassengerType.Infant)
                    {
                        paxCount++;
                    }
                    personId++;
                    profileId++;
                    contactId++;
                }


                
                List<FlightInfo> Flights = new List<FlightInfo>();
                Flights.AddRange(itinerary.Segments);


                int paxid = 214;
                //int pCounter = 0;
                    //Add Special Services like Baggage, Seats, Meals etc here
                for (int i = 0; i < LFIDS.Count; i++)
                {
                    paxid = 214;
                    //pCounter = 0;
                    int LFID = LFIDS[i];
                    Segment segment = new Segment();
                    segment.StoreFrontID = "na";
                    segment.MarketingCode = "na";
                    segment.PersonOrgID = -214;
                    segment.Seats = new FlyDubai.ConfirmBooking.Seat[0];
                    segment.FareInformationID = itinerary.Passenger[0].Price.FareInformationID[i];//pax.Price.FareInformationID[i]; 

                    List<SpecialService> services = new List<SpecialService>();

                    for (int p = 0; p < itinerary.Passenger.Length; p++)
                    {
                        FlightPassenger pax = itinerary.Passenger[p];
                        if (pax.Type == PassengerType.Infant || (itinerary.IsBaggageIncluded))
                        {
                            if (pax.Type != PassengerType.Infant)  
                            {
                                if (itinerary.IsBaggageIncluded)
                                {
                                    segment.SpecialServices = new SpecialService[0];
                                }                                                              
                            }                            
                        }
                        else if (pax.BaggageType != null)
                        {
                            if (!itinerary.IsBaggageIncluded)                            
                            {
                                if (pax.FlyDubaiBaggageCharge[i] > 0)
                                {
                                    SpecialService outService = new SpecialService();
                                    outService.Amount = pax.FlyDubaiBaggageCharge[i];
                                    outService.ChargeComment = pax.BaggageType.Split(',')[i];
                                    outService.CodeType = pax.BaggageType.Split(',')[i];
                                    outService.Commissionable = false;
                                    outService.CommissionableSpecified = false;
                                    outService.CurrencyCode = FlyDubai.ConfirmBooking.EnumerationsCurrencyCodeTypes.AED;
                                    outService.DepartureDate = Convert.ToDateTime(Flights.Find(delegate(FlightInfo fi) { return fi.UapiSegmentRefKey == LFID.ToString(); }).DepartureTime.ToString("yyyy-MM-dd"));
                                    outService.LogicalFlightID = LFID;
                                    outService.OverrideAmount = false;
                                    outService.PersonOrgID = -paxid;
                                    outService.Refundable = true;
                                    outService.RefundableSpecified = true;
                                    outService.ServiceID = -2147483648;
                                    outService.SSRCategory = 99;

                                    services.Add(outService);
                                }
                                //segment.SpecialServices[pCounter] = outService;
                            }
                            //if (segment.SpecialServices.Length > 1)
                            //{
                            //    pCounter++;
                            //}                            
                        }
                        paxid++;
                    }
                    segment.SpecialServices = services.ToArray();
                    Segments.Add(segment);
                }

                pnrRequest.ContactInfos = contactInfos.ToArray();
                pnrRequest.Passengers = Persons.ToArray();
                pnrRequest.Segments = Segments.ToArray();
                pnrRequest.Payments = new FlyDubai.ConfirmBooking.Payment[1];
                FlyDubai.ConfirmBooking.Payment payment = new FlyDubai.ConfirmBooking.Payment();
                payment.ReservationPaymentID = -2132178889;
                payment.CompanyName = "";
                payment.FirstName = "FIRST NAME";
                payment.LastName = "LAST NAME";
                payment.CardType = "VISA";
                payment.CardHolder = "";
                payment.PaymentCurrency = FlyDubai.ConfirmBooking.EnumerationsCurrencyCodeTypes.AED;
                payment.ISOCurrency = 1;
                payment.PaymentAmount = 0;
                payment.PaymentMethod = FlyDubai.ConfirmBooking.EnumerationsPaymentMethodTypes.INVC;
                payment.CardNum = "";
                payment.CVCode = "123";
                payment.ExpirationDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
                payment.IsTACreditCard = false;
                payment.VoucherNum = -214;
                payment.GcxID = "1";
                payment.GcxOpt = "1";
                payment.OriginalAmount = 0;
                payment.OriginalCurrency = "AED";
                payment.ExchangeRate = 1;
                payment.ExchangeRateDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
                payment.PaymentComment = "paid";
                payment.BillingCountry = "AE";
                pnrRequest.Payments[0] = payment;

                //--------------------End Passenger details--------------------------

                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(SummaryPNR));
                    string filePath = @"" + xmlPath + sessionId + "_" + "FlyDubaiSummaryPNRRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, pnrRequest);
                    sw.Close();
                    ser = null;
                    //Audit.Add(EventType.FlyDubaiBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }


                pnrInfo = bookEvent.SummaryPNR(pnrRequest);

                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(FlyDubai.ConfirmBooking.ViewPNR));
                    string filePath = @"" + xmlPath + sessionId + "_" + "FlyDubaiSummaryPNRResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, pnrInfo);
                    sw.Close();
                    ser = null;
                    // Audit.Add(EventType.FlyDubaiBooking, Severity.Normal, appUserId, filePath, "127.0.0.1");
                }
                catch { }


            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Create Summary PNR" + ex.ToString(), ex);
            }

            return pnrInfo;
        }

        /// <summary>
        /// Method for sending SOAP XML request
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="searchType"></param>
        /// <returns></returns>
        //private string GetSummaryPNR(FlightItinerary itinerary, SearchType searchType)
        //{
        //    StringBuilder sb = new StringBuilder();

        //    sb.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/' xmlns:rad='http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request' xmlns:rad1='http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Request'>");
        //    sb.Append("<soapenv:Header/>");
        //    sb.Append("<soapenv:Body>");
        //    sb.Append("<tem:SummaryPNR>");
        //    sb.Append("<!--Optional:-->");
        //    sb.Append("<tem:SummaryPnrRequest>");
        //    sb.Append("<rad:SecurityGUID>" + itinerary.GUID + "</rad:SecurityGUID>");
        //    sb.Append("<rad:CarrierCodes>");
        //    sb.Append("<!--Zero or more repetitions:-->");
        //    sb.Append("<rad:CarrierCode>");
        //    sb.Append("<rad:AccessibleCarrierCode>FZ</rad:AccessibleCarrierCode>");
        //    sb.Append("</rad:CarrierCode>");
        //    sb.Append("</rad:CarrierCodes>");
        //    sb.Append("<rad:ClientIPAddress></rad:ClientIPAddress>");
        //    sb.Append("<rad:HistoricUserName>" + loginTAUserInter + "</rad:HistoricUserName>");
        //    sb.Append("<rad1:ActionType>GetSummary</rad1:ActionType>");
        //    sb.Append("<rad1:ReservationInfo>");
        //    sb.Append("<rad:SeriesNumber>C</rad:SeriesNumber>");
        //    sb.Append("<rad:ConfirmationNumber></rad:ConfirmationNumber>");
        //    sb.Append("</rad1:ReservationInfo>");
        //    sb.Append("<rad1:SecurityToken>" + itinerary.GUID + "</rad1:SecurityToken>");
        //    sb.Append("<rad1:CarrierCurrency>AED</rad1:CarrierCurrency>");
        //    sb.Append("<rad1:DisplayCurrency>AED</rad1:DisplayCurrency>");
        //    sb.Append("<rad1:IATANum>" + IATAInter + "</rad1:IATANum>");
        //    sb.Append("<rad1:User>apicozmoint</rad1:User>");
        //    sb.Append("<rad1:ReceiptLanguageID>1</rad1:ReceiptLanguageID>");
        //    sb.Append("<rad1:PromoCode></rad1:PromoCode>");
        //    sb.Append("<rad1:ExternalBookingID></rad1:ExternalBookingID>");
        //    sb.Append("<rad1:Address>");
        //    sb.Append("<rad1:Address1>" + itinerary.Passenger[0].AddressLine1 + "</rad1:Address1>");
        //    sb.Append("<rad1:Address2>na</rad1:Address2>");
        //    sb.Append("<rad1:City>Hyderabad</rad1:City>");
        //    sb.Append("<rad1:State>na</rad1:State>");
        //    sb.Append("<rad1:Postal>na</rad1:Postal>");
        //    sb.Append("<rad1:Country>" + itinerary.Passenger[0].Country.CountryCode + "</rad1:Country>");
        //    sb.Append("<rad1:CountryCode>na</rad1:CountryCode>");
        //    sb.Append("<rad1:AreaCode>na</rad1:AreaCode>");
        //    sb.Append("<rad1:PhoneNumber>na</rad1:PhoneNumber>");
        //    sb.Append("<rad1:Display>na</rad1:Display>");
        //    sb.Append("</rad1:Address>");
        //    sb.Append("<rad1:ContactInfos>");
        //    sb.Append("<!--Zero or more repetitions:-->");
        //    sb.Append("<rad1:ContactInfo>");
        //    sb.Append("<rad1:ContactID>1</rad1:ContactID>");
        //    sb.Append("<rad1:PersonOrgID>-214</rad1:PersonOrgID>");
        //    sb.Append("<rad1:ContactField>" + itinerary.Passenger[0].Email + "</rad1:ContactField>");
        //    sb.Append("<rad1:ContactType>Email</rad1:ContactType>");
        //    sb.Append("<rad1:Extension>na</rad1:Extension>");
        //    sb.Append("<rad1:CountryCode>na</rad1:CountryCode>");
        //    sb.Append("<rad1:AreaCode>na</rad1:AreaCode>");
        //    sb.Append("<rad1:PhoneNumber>na</rad1:PhoneNumber>");
        //    sb.Append("<rad1:Display>na</rad1:Display>");
        //    sb.Append("<rad1:PreferredContactMethod>true</rad1:PreferredContactMethod>");
        //    sb.Append("</rad1:ContactInfo>");
        //    sb.Append("</rad1:ContactInfos>");
        //    sb.Append("<rad1:Passengers>");

        //    int contactId = 2141;
        //    int personId = 214;
        //    long profileId = 2147483648;
        //    List<FlyDubai.ConfirmBooking.ContactInfo> contactInfos = new List<FlyDubai.ConfirmBooking.ContactInfo>();
        //    List<FlyDubai.ConfirmBooking.Person> Persons = new List<FlyDubai.ConfirmBooking.Person>();
        //    List<Segment> Segments = new List<Segment>();

        //    List<int> LFIDS = new List<int>();
        //    foreach (FlightInfo seg in itinerary.Segments)
        //    {
        //        if (!LFIDS.Contains(seg.UapiSegmentRefKey))
        //        {
        //            LFIDS.Add(seg.UapiSegmentRefKey);
        //        }
        //    }

        //    IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-US");

        //    foreach (FlightPassenger pax in itinerary.Passenger)
        //    {
        //        sb.Append("<rad1:Person>");
        //        sb.Append("<rad1:PersonOrgID>-" + personId + "</rad1:PersonOrgID>");
        //        sb.Append("<rad1:FirstName>" + pax.FirstName + "</rad1:FirstName>");
        //        sb.Append("<rad1:LastName>" + pax.LastName + "</rad1:LastName>");
        //        sb.Append("<rad1:MiddleName>na</rad1:MiddleName>");
        //        sb.Append("<rad1:Age>" + DateTime.Now.AddYears(-pax.DateOfBirth.Year).Year + "</rad1:Age>");
        //        sb.Append("<rad1:DOB>" + pax.DateOfBirth.ToString("yyyy-MM-dd") + "</rad1:DOB>");
        //        sb.Append("<rad1:Gender>" + pax.Gender.ToString() + "</rad1:Gender>");
        //        sb.Append("<rad1:Title>" + pax.Title + "</rad1:Title>");
        //        sb.Append("<rad1:NationalityLaguageID>-214</rad1:NationalityLaguageID>");
        //        sb.Append("<rad1:RelationType>Self</rad1:RelationType>");
        //        if (pax.Type == PassengerType.Adult || pax.Type == PassengerType.Senior)
        //        {
        //            sb.Append("<rad1:WBCID>1</rad1:WBCID>");
        //            sb.Append("<rad1:PTCID>1</rad1:PTCID>");
        //            sb.Append("<rad1:PTC>1</rad1:PTC>");
        //        }
        //        else if (pax.Type == PassengerType.Child)
        //        {
        //            sb.Append("<rad1:WBCID>6</rad1:WBCID>");
        //            sb.Append("<rad1:PTCID>6</rad1:PTCID>");
        //            sb.Append("<rad1:PTC>6</rad1:PTC>");
        //        }
        //        else
        //        {
        //            sb.Append("<rad1:WBCID>5</rad1:WBCID>");
        //            sb.Append("<rad1:PTCID>5</rad1:PTCID>");
        //            sb.Append("<rad1:PTC>5</rad1:PTC>");
        //        }
        //        sb.Append("<rad1:TravelsWithPersonOrgID>-214</rad1:TravelsWithPersonOrgID>");
        //        sb.Append("<rad1:RedressNumber>na</rad1:RedressNumber>");
        //        sb.Append("<rad1:KnownTravelerNumber>na</rad1:KnownTravelerNumber>");
        //        sb.Append("<rad1:MarketingOptIn>true</rad1:MarketingOptIn>");
        //        sb.Append("<rad1:UseInventory>true</rad1:UseInventory>");
        //        sb.Append("<rad1:Address>");
        //        sb.Append("<rad1:Address1>" + pax.AddressLine1 + "</rad1:Address1>");
        //        sb.Append("<rad1:Address2>" + pax.AddressLine2 + "</rad1:Address2>");
        //        sb.Append("<rad1:City>Hyderabad</rad1:City>");
        //        sb.Append("<rad1:State>na</rad1:State>");
        //        sb.Append("<rad1:Postal>na</rad1:Postal>");
        //        sb.Append("<rad1:Country>" + pax.Country.CountryCode + "</rad1:Country>");
        //        sb.Append("<rad1:CountryCode>" + pax.Country.CountryCode + "</rad1:CountryCode>");
        //        sb.Append("<rad1:AreaCode>na</rad1:AreaCode>");
        //        sb.Append("<rad1:PhoneNumber>na</rad1:PhoneNumber>");
        //        sb.Append("<rad1:Display>na</rad1:Display>");
        //        sb.Append("</rad1:Address>");
        //        sb.Append("<rad1:Company>na</rad1:Company>");
        //        sb.Append("<rad1:Comments>na</rad1:Comments>");
        //        sb.Append("<rad1:Passport>" + pax.PassportNo + "</rad1:Passport>");
        //        sb.Append("<rad1:Nationality>" + pax.Country.Nationality + "</rad1:Nationality>");
        //        sb.Append("<rad1:ProfileId>-" + profileId + "</rad1:ProfileId>");
        //        sb.Append("<rad1:IsPrimaryPassenger>true</rad1:IsPrimaryPassenger>");
        //        sb.Append("<rad1:ContactInfos>");
        //        sb.Append("<!--Zero or more repetitions:-->");
        //        sb.Append("<rad1:ContactInfo>");
        //        sb.Append("<rad1:ContactID>-" + contactId + "</rad1:ContactID>");
        //        sb.Append("<rad1:PersonOrgID>-" + personId + "</rad1:PersonOrgID>");
        //        sb.Append("<rad1:ContactField>" + pax.Email + "</rad1:ContactField>");
        //        sb.Append("<rad1:ContactType>Email</rad1:ContactType>");
        //        sb.Append("<rad1:Extension>na</rad1:Extension>");
        //        sb.Append("<rad1:CountryCode>na</rad1:CountryCode>");
        //        sb.Append("<rad1:AreaCode>na</rad1:AreaCode>");
        //        sb.Append("<rad1:PhoneNumber>na</rad1:PhoneNumber>");
        //        sb.Append("<rad1:Display>na</rad1:Display>");
        //        sb.Append("<rad1:PreferredContactMethod>true</rad1:PreferredContactMethod>");
        //        sb.Append("</rad1:ContactInfo>");
        //        sb.Append("<rad1:ContactInfo>");
        //        sb.Append("<rad1:ContactID>-2142</rad1:ContactID>");
        //        sb.Append("<rad1:PersonOrgID>-214</rad1:PersonOrgID>");
        //        sb.Append("<rad1:ContactField>-214</rad1:ContactField>");
        //        sb.Append("<rad1:ContactType>MobilePhone</rad1:ContactType>");
        //        sb.Append("<rad1:Extension>na</rad1:Extension>");
        //        sb.Append("<rad1:CountryCode>na</rad1:CountryCode>");
        //        sb.Append("<rad1:AreaCode>" + pax.CellPhone.Split('-')[0] + "</rad1:AreaCode>");
        //        sb.Append("<rad1:PhoneNumber>" + pax.CellPhone.Split('-')[1] + "</rad1:PhoneNumber>");
        //        sb.Append("<rad1:Display>na</rad1:Display>");
        //        sb.Append("<rad1:PreferredContactMethod>false</rad1:PreferredContactMethod>");
        //        sb.Append("</rad1:ContactInfo>");
        //        sb.Append("</rad1:ContactInfos>");
        //        sb.Append("</rad1:Person>");



        //        personId++;
        //        profileId++;
        //        contactId++;
        //    }


        //    sb.Append("</rad1:Passengers>");
        //    sb.Append("<rad1:Segments>");
        //    sb.Append("<rad1:Segment>");
        //    sb.Append("<rad1:PersonOrgID>-214</rad1:PersonOrgID>");
        //    sb.Append("<rad1:FareInformationID>" + itinerary.Passenger[0].Price.FareInformationID[0] + "</rad1:FareInformationID>");
        //    sb.Append("<rad1:MarketingCode>na</rad1:MarketingCode>");
        //    sb.Append("<rad1:StoreFrontID>na</rad1:StoreFrontID>");
        //    sb.Append("<rad1:SpecialServices>");
        //    sb.Append("<!--Zero or more repetitions:-->");
        //    sb.Append("</rad1:SpecialServices>");
        //    sb.Append("<rad1:Seats>");
        //    sb.Append("<!--Zero or more repetitions:-->");
        //    sb.Append("</rad1:Seats>");
        //    sb.Append("</rad1:Segment>");
        //    sb.Append("<rad1:Segment>");
        //    sb.Append("<rad1:PersonOrgID>-214</rad1:PersonOrgID>");
        //    sb.Append("<rad1:FareInformationID>" + itinerary.Passenger[0].Price.FareInformationID[1] + "</rad1:FareInformationID>");
        //    sb.Append("<rad1:MarketingCode>na</rad1:MarketingCode>");
        //    sb.Append("<rad1:StoreFrontID>na</rad1:StoreFrontID>");
        //    sb.Append("<rad1:SpecialServices>");
        //    sb.Append("<!--Zero or more repetitions:-->");
        //    sb.Append("</rad1:SpecialServices>");
        //    sb.Append("<rad1:Seats>");
        //    sb.Append("<!--Zero or more repetitions:-->");
        //    sb.Append("</rad1:Seats>");
        //    sb.Append("</rad1:Segment>");

        //    sb.Append("</rad1:Segments>");
        //    sb.Append("<rad1:Payments>");
        //    sb.Append("<!--Zero or more repetitions:-->");
        //    sb.Append("<rad1:Payment>");
        //    sb.Append("<rad1:ReservationPaymentID>-2132178889</rad1:ReservationPaymentID>");
        //    sb.Append("<rad1:CompanyName></rad1:CompanyName>");
        //    sb.Append("<rad1:FirstName>motaz</rad1:FirstName>");
        //    sb.Append("<rad1:LastName>dahab</rad1:LastName>");
        //    sb.Append("<rad1:CardType>VISA</rad1:CardType>");
        //    sb.Append("<rad1:CardHolder></rad1:CardHolder>");
        //    sb.Append("<rad1:PaymentCurrency>AED</rad1:PaymentCurrency>");
        //    sb.Append("<rad1:ISOCurrency>1</rad1:ISOCurrency>");
        //    sb.Append("<rad1:PaymentAmount>0</rad1:PaymentAmount>");
        //    sb.Append("<rad1:PaymentMethod>INVC</rad1:PaymentMethod>");
        //    sb.Append("<rad1:CardNum></rad1:CardNum>");
        //    sb.Append("<rad1:CVCode>123</rad1:CVCode>");
        //    sb.Append("<rad1:ExpirationDate>2014-10-24</rad1:ExpirationDate>");
        //    sb.Append("<rad1:IsTACreditCard>false</rad1:IsTACreditCard>");
        //    sb.Append("<rad1:VoucherNum>-214</rad1:VoucherNum>");
        //    sb.Append("<rad1:GcxID>1</rad1:GcxID>");
        //    sb.Append("<rad1:GcxOpt>1</rad1:GcxOpt>");
        //    sb.Append("<rad1:OriginalCurrency>AED</rad1:OriginalCurrency>");
        //    sb.Append("<rad1:OriginalAmount>0</rad1:OriginalAmount>");
        //    sb.Append("<rad1:ExchangeRate>1</rad1:ExchangeRate>");
        //    sb.Append("<rad1:ExchangeRateDate>2013-01-01</rad1:ExchangeRateDate>");
        //    sb.Append("<rad1:PaymentComment>paid</rad1:PaymentComment>");
        //    sb.Append("<rad1:BillingCountry>AE</rad1:BillingCountry>");
        //    sb.Append("</rad1:Payment>");
        //    sb.Append("</rad1:Payments>");
        //    sb.Append("</tem:SummaryPnrRequest>");
        //    sb.Append("</tem:SummaryPNR>");
        //    sb.Append("</soapenv:Body>");
        //    sb.Append("</soapenv:Envelope>");

        //    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
        //    {
        //        XmlDocument doc = new XmlDocument();
        //        doc.LoadXml(sb.ToString());
        //        doc.Save(@"" + xmlPath + "FlyDubai_SOAP_SummaryPNRRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml");
        //    }

        //    string url = "http://fz.connectpoint.uat.radixx.com/ConnectPoint.Reservation.svc";

        //    string response = "";
        //    WebClient myWebClient = new WebClient();
        //    myWebClient.Headers.Add("Content-Type", "text/xml; charset=utf-8");
        //    myWebClient.Headers.Add("SOAPAction", "\"http://tempuri.org/IConnectPoint_Reservation/SummaryPNR\"");
        //    byte[] byteArray = Encoding.ASCII.GetBytes(sb.ToString());
        //    //byte[] responseArray = myWebClient.UploadData(url, "POST", byteArray);
        //    response = myWebClient.UploadString(url, sb.ToString());
        //    //response = Encoding.ASCII.GetString(responseArray);
        //    FlyDubai.ConfirmBooking.ViewPNR SummaryPNR = null;
        //    try
        //    {
        //        if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
        //        {
        //            XmlDocument doc = new XmlDocument();
        //            doc.LoadXml(response);
        //            string filePath = @"" + xmlPath + "FlyDubai_SOAP_SummaryPNRResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
        //            doc.Save(filePath);

        //            //System.Runtime.Serialization.Formatters.Soap.SoapFormatter formatter = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();
        //            //FileStream fs = new FileStream(filePath, FileMode.Open);
        //            //SummaryPNR = formatter.Deserialize(fs) as FlyDubai.ConfirmBooking.ViewPNR;
        //        }
        //    }
        //    catch { }

           
        //    return response;
            
        //}


        private FlyDubai.AgencyFees.ViewPNR GetAgencyTransactionFees(FlightItinerary itinerary)
        {
            FlyDubai.AgencyFees.ViewPNR transactionFees = new FlyDubai.AgencyFees.ViewPNR();

            try
            {
                FlyDubai.AgencyFees.AssessAgencyTransactionFees feeRequest = new AssessAgencyTransactionFees();

                feeRequest.AgencyCurrency = FlyDubai.AgencyFees.EnumerationsCurrencyCodeTypes.AED;
                feeRequest.CarrierCodes = new FlyDubai.AgencyFees.CarrierCode[1];
                feeRequest.CarrierCodes[0] = new FlyDubai.AgencyFees.CarrierCode();
                feeRequest.CarrierCodes[0].AccessibleCarrierCode = "FZ";
                feeRequest.HistoricUserName = GetUserName(itinerary.Segments[0].Origin.AirportCode); 
                feeRequest.PaymentMethod = FlyDubai.AgencyFees.EnumerationsPaymentMethodTypes.INVC;
                feeRequest.SecurityGUID = itinerary.GUID;
                feeRequest.ClientIPAddress = "";

                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AssessAgencyTransactionFees));
                    StreamWriter sw = new StreamWriter(@"" + xmlPath + sessionId + "_" + "FlyDubaiAgencyTransactionFeeRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml");
                    ser.Serialize(sw, feeRequest);
                    sw.Close();
                    ser = null;
                }
                catch { }

                transactionFees = agencyFee.AssessAgencyTransactionFees(feeRequest);

                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(FlyDubai.AgencyFees.ViewPNR));
                    StreamWriter sw = new StreamWriter(@"" + xmlPath + sessionId + "_" + "FlyDubaiAgencyTransactionFeeResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml");
                    ser.Serialize(sw, transactionFees);
                    sw.Close();
                    ser = null;
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.FlyDubai, Severity.High, 1, "(FlyDubai)Failed to retrieve Agency Transaction Fees. Reason : " + ex.ToString(), "");
            }

            return transactionFees;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="reservationBalance"></param>
        /// <returns></returns>
        private FlyDubai.ProcessPNRPayment.ViewPNR ProcessPNRPayments(FlightItinerary itinerary, decimal reservationBalance)
        {
            FlyDubai.ProcessPNRPayment.ViewPNR ProcessPNR = new FlyDubai.ProcessPNRPayment.ViewPNR();
            try
            {
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-US");
                PNRPayments pnr = new PNRPayments();
                pnr.TransactionInfo = new FlyDubai.ProcessPNRPayment.TransactionInfo();
                pnr.TransactionInfo.CarrierCodes = new FlyDubai.ProcessPNRPayment.CarrierCode[1];
                pnr.TransactionInfo.CarrierCodes[0] = new FlyDubai.ProcessPNRPayment.CarrierCode();
                pnr.TransactionInfo.CarrierCodes[0].AccessibleCarrierCode = "FZ";
                pnr.TransactionInfo.ClientIPAddress = "";
                pnr.TransactionInfo.HistoricUserName = GetUserName(itinerary.Segments[0].Origin.CityCode);
                pnr.TransactionInfo.SecurityGUID = itinerary.GUID;

                pnr.ReservationInfo = new FlyDubai.ProcessPNRPayment.ReservationInfo();
                pnr.ReservationInfo.ConfirmationNumber = "";
                pnr.ReservationInfo.SeriesNumber = "299";

                List<ProcessPNRPayment> payments = new List<ProcessPNRPayment>();
                int personId = 214;
                int contactId = 2141;
                long profileId = 2147483648;
                foreach (FlightPassenger pax in itinerary.Passenger)
                {

                    ProcessPNRPayment payment = new ProcessPNRPayment();
                    payment.AuthorizationCode = "";
                    payment.BaseAmount = reservationBalance;
                    payment.BaseCurrency = FlyDubai.ProcessPNRPayment.EnumerationsCurrencyCodeTypes.AED;
                    payment.BillingCountry = "AE";
                    payment.CardCurrency = FlyDubai.ProcessPNRPayment.EnumerationsCurrencyCodeTypes.AED;
                    payment.CardHolder = "";
                    payment.CardNumber = "";
                    payment.CheckNumber = 1234;
                    payment.CurrencyPaid = FlyDubai.ProcessPNRPayment.EnumerationsCurrencyCodeTypes.AED;
                    payment.CVCode = "123";
                    payment.DatePaid = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
                    payment.DocumentReceivedBy = "FZ";
                    payment.ExchangeRate = 1;
                    payment.ExchangeRateDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
                    payment.ExpirationDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
                    payment.FFNumber = "11";
                    payment.FingerPrintingSessionID = "";
                    payment.GcxID = "1";
                    payment.GcxOptOption = "1";
                    payment.IataNumber = GetIATA(itinerary.Segments[0].Origin.AirportCode);
                    payment.IsTACreditCard = false;
                    payment.OriginalAmount = reservationBalance;
                    payment.OriginalCurrency = FlyDubai.ProcessPNRPayment.EnumerationsCurrencyCodeTypes.AED;
                    payment.PaymentAmount = reservationBalance;
                    payment.PaymentComment = "test comments";
                    payment.PaymentMethod = FlyDubai.ProcessPNRPayment.EnumerationsPaymentMethodTypes.INVC;
                    payment.PaymentReference = "test";
                    payment.Reference = "Test";
                    payment.ResponseMessage = "";
                    payment.TerminalID = 1;
                    payment.TransactionStatus = EnumerationsPaymentTransactionStatusTypes.APPROVED;
                    payment.UserData = "";
                    payment.UserID = GetUserName(itinerary.Segments[0].Origin.AirportCode);
                    payment.ValueCode = "Test";
                    payment.VoucherNumber = -profileId;

                    payment.Payor = new FlyDubai.ProcessPNRPayment.Person();
                    payment.Payor.Address = new FlyDubai.ProcessPNRPayment.Address();
                    payment.Payor.Address.Address1 = "# 302, Tower 400";
                    payment.Payor.Address.Address2 = "Al Soor, Street no 2";
                    payment.Payor.Address.AreaCode = "";
                    payment.Payor.Address.City = "Sharjah";
                    payment.Payor.Address.Country = "United Arab Emirates";
                    payment.Payor.Address.CountryCode = "AE";
                    payment.Payor.Address.Display = "";
                    payment.Payor.Address.PhoneNumber = "97160544470";
                    payment.Payor.Address.Postal = "3393";
                    payment.Payor.Address.State = "Sharjah";

                    payment.Payor.Age = DateTime.Now.AddYears(-pax.DateOfBirth.Year).Year;
                    payment.Payor.DOB = Convert.ToDateTime(pax.DateOfBirth.ToString("yyyy-MM-dd"));
                    payment.Payor.FirstName = pax.FirstName;
                    payment.Payor.IsPrimaryPassenger = (pax.IsLeadPax ? true : false);
                    payment.Payor.KnownTravelerNumber = "na";
                    payment.Payor.LastName = pax.LastName;
                    payment.Payor.MarketingOptIn = true;
                    payment.Payor.MiddleName = "";
                    if (pax.Country != null)
                    {
                        payment.Payor.Nationality = pax.Country.Nationality;
                    }
                    payment.Payor.NationalityLaguageID = 1;
                    if (!string.IsNullOrEmpty(pax.PassportNo))
                    {
                        payment.Payor.Passport = pax.PassportNo;
                    }
                    else
                    {
                        payment.Payor.Passport = string.Empty;
                    }
                    payment.Payor.PersonOrgID = -personId;
                    payment.Payor.ProfileId = -profileId;

                    if (pax.Type == PassengerType.Adult || pax.Type == PassengerType.Senior)
                    {
                        payment.Payor.PTC = "1";
                        payment.Payor.PTCID = 1;
                        payment.Payor.WBCID = 1;
                    }
                    else if (pax.Type == PassengerType.Child)
                    {
                        payment.Payor.PTC = "6";
                        payment.Payor.PTCID = 6;
                        payment.Payor.WBCID = 6;
                    }
                    else
                    {
                        payment.Payor.PTC = "5";
                        payment.Payor.PTCID = 5;
                        payment.Payor.WBCID = 5;
                    }

                    payment.Payor.RedressNumber = "na";
                    payment.Payor.RelationType = FlyDubai.ProcessPNRPayment.EnumerationsRelationshipTypes.Self;
                    payment.Payor.Title = pax.Title;
                    payment.Payor.TravelsWithPersonOrgID = -214;
                    payment.Payor.UseInventory = false;
                    payment.Payor.Comments = "";
                    payment.Payor.Company = "";


                    payment.Payor.ContactInfos = new FlyDubai.ProcessPNRPayment.ContactInfo[1];
                    FlyDubai.ProcessPNRPayment.ContactInfo ci = new FlyDubai.ProcessPNRPayment.ContactInfo();
                    ci.AreaCode = (pax.CellPhone.Contains("-") ? pax.CellPhone.Split('-')[0] : pax.CellPhone.Substring(0, 2));
                    ci.ContactField = pax.CellPhone;
                    ci.ContactID = -contactId;
                    ci.ContactType = FlyDubai.ProcessPNRPayment.EnumerationsContactTypes.MobilePhone;
                    ci.CountryCode = "na";
                    ci.Display = "na";
                    ci.Extension = "na";
                    ci.PersonOrgID = -personId;
                    ci.PhoneNumber = (pax.CellPhone.Contains("-") ? pax.CellPhone.Split('-')[1] : pax.CellPhone.Substring(2, pax.CellPhone.Length - 1));
                    ci.PreferredContactMethod = false;

                    payment.Payor.ContactInfos[0] = ci;
                  
                    payments.Add(payment);
                    personId++;
                    profileId++;
                    break;
                }
                pnr.PNRPayments1 = payments.ToArray();

                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(PNRPayments));
                    StreamWriter sw = new StreamWriter(@"" + xmlPath + sessionId + "_" + "FlyDubaiProcessPNRRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml");
                    ser.Serialize(sw, pnr);
                    sw.Close();
                    ser = null;
                }
                catch { }

                ProcessPNR = process.ProcessPNRPayment(pnr);

                try
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(FlyDubai.ProcessPNRPayment.ViewPNR));
                    StreamWriter sw = new StreamWriter(@"" + xmlPath + sessionId + "_" + "FlyDubaiProcessPNRResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml");
                    ser.Serialize(sw, ProcessPNR);
                    sw.Close();
                    ser = null;
                }
                catch { }

                #region SOAP Request Code
                /*=================SOAP XML request Start======================

                StringBuilder sb = new StringBuilder();
                sb.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/' xmlns:rad='http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Fulfillment.Request' xmlns:rad1='http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request' xmlns:rad2='http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Request'>");
                sb.Append("<soapenv:Header/>");
                sb.Append("<soapenv:Body>");
                sb.Append("<tem:ProcessPNRPayment>");
                sb.Append("<!--Optional:-->");
                sb.Append("<tem:PNRPaymentRequest>");
                sb.Append("<rad:TransactionInfo>");
                sb.Append("<rad1:SecurityGUID>" + itinerary.GUID + "</rad1:SecurityGUID>");
                sb.Append("<rad1:CarrierCodes>");
                sb.Append("<!--Zero or more repetitions:-->");
                sb.Append("<rad1:CarrierCode>");
                sb.Append("<rad1:AccessibleCarrierCode>FZ</rad1:AccessibleCarrierCode>");
                sb.Append("</rad1:CarrierCode>");
                sb.Append("</rad1:CarrierCodes><rad1:ClientIPAddress></rad1:ClientIPAddress><rad1:HistoricUserName>apicozmoint</rad1:HistoricUserName>");
                sb.Append("</rad:TransactionInfo>");
                sb.Append("<rad:ReservationInfo>");
                sb.Append("<rad1:SeriesNumber>299</rad1:SeriesNumber>");
                sb.Append("<rad1:ConfirmationNumber></rad1:ConfirmationNumber>");
                sb.Append("</rad:ReservationInfo>");
                sb.Append("<rad:PNRPayments>");
                sb.Append("<!--Zero or more repetitions:-->");
                sb.Append("<rad:ProcessPNRPayment>");
                sb.Append("<rad:BaseAmount>" + reservationBalance + "</rad:BaseAmount>");
                sb.Append("<rad:BaseCurrency>AED</rad:BaseCurrency>");
                sb.Append("<rad:CardHolder></rad:CardHolder>");
                sb.Append("<rad:CardNumber></rad:CardNumber>");
                sb.Append("<rad:CheckNumber>1234</rad:CheckNumber>");
                sb.Append("<rad:CurrencyPaid>AED</rad:CurrencyPaid>");
                sb.Append("<rad:CVCode>123</rad:CVCode>");
                sb.Append("<rad:DatePaid>" + DateTime.Now.ToString("yyyy-MM-dd") + "</rad:DatePaid>");
                sb.Append("<rad:DocumentReceivedBy>Roshan</rad:DocumentReceivedBy>");
                sb.Append("<rad:ExpirationDate>" + DateTime.Now.AddYears(2).ToString("yyyy-MM-dd") + "</rad:ExpirationDate>");
                sb.Append("<rad:ExchangeRate>1.0</rad:ExchangeRate>");
                sb.Append("<rad:ExchangeRateDate>" + DateTime.Now.ToString("yyyy-MM-dd") + "</rad:ExchangeRateDate>");
                sb.Append("<rad:FFNumber></rad:FFNumber>");
                sb.Append("<rad:PaymentComment>test comments</rad:PaymentComment>");
                sb.Append("<rad:PaymentAmount>"+reservationBalance+"</rad:PaymentAmount>");
                sb.Append("<rad:PaymentMethod>INVC</rad:PaymentMethod>");
                sb.Append("<rad:Reference>Test</rad:Reference>");
                sb.Append("<rad:TerminalID>1</rad:TerminalID>");
                sb.Append("<rad:UserData></rad:UserData>");
                sb.Append("<rad:UserID>apicozmoint</rad:UserID>");
                sb.Append("<rad:IataNumber>"+IATAcode+"</rad:IataNumber>");
                sb.Append("<rad:ValueCode>Test</rad:ValueCode>");
                sb.Append("<rad:VoucherNumber>-2147483648</rad:VoucherNumber>");
                sb.Append("<rad:IsTACreditCard>false</rad:IsTACreditCard>");
                sb.Append("<rad:GcxID>1</rad:GcxID>");
                sb.Append("<rad:GcxOptOption>1</rad:GcxOptOption>");
                sb.Append("<rad:OriginalCurrency>AED</rad:OriginalCurrency>");
                sb.Append("<rad:OriginalAmount>"+reservationBalance+"</rad:OriginalAmount>");
                sb.Append("<rad:TransactionStatus>APPROVED</rad:TransactionStatus>");
                sb.Append("<rad:AuthorizationCode></rad:AuthorizationCode>");
                sb.Append("<rad:PaymentReference>Test</rad:PaymentReference>");
                sb.Append("<rad:ResponseMessage>test</rad:ResponseMessage>");
                sb.Append("<rad:CardCurrency>AED</rad:CardCurrency>");
                sb.Append("<rad:BillingCountry>AE</rad:BillingCountry>");
                sb.Append("<rad:FingerPrintingSessionID></rad:FingerPrintingSessionID>");
                sb.Append("<rad:Payor>");
                sb.Append("<rad2:PersonOrgID>-214</rad2:PersonOrgID>");
                sb.Append("<rad2:FirstName>Mohd</rad2:FirstName>");
                sb.Append("<rad2:LastName>Ziyad</rad2:LastName>");
                sb.Append("<rad2:MiddleName></rad2:MiddleName>");
                sb.Append("<rad2:Age>28</rad2:Age>");
                sb.Append("<rad2:DOB>1985-05-05</rad2:DOB>");
                sb.Append("<rad2:Gender>Male</rad2:Gender>");
                sb.Append("<rad2:Title>MR</rad2:Title>");
                sb.Append("<rad2:NationalityLaguageID>-214</rad2:NationalityLaguageID>");
                sb.Append("<rad2:RelationType>Self</rad2:RelationType>");
                sb.Append("<rad2:WBCID>1</rad2:WBCID>");
                sb.Append("<rad2:PTCID>1</rad2:PTCID>");
                sb.Append("<rad2:PTC>1</rad2:PTC>");
                sb.Append("<rad2:TravelsWithPersonOrgID>-214</rad2:TravelsWithPersonOrgID>");
                sb.Append("<rad2:RedressNumber>na</rad2:RedressNumber>");
                sb.Append("<rad2:KnownTravelerNumber>na</rad2:KnownTravelerNumber>");
                sb.Append("<rad2:MarketingOptIn>true</rad2:MarketingOptIn>");
                sb.Append("<rad2:UseInventory>false</rad2:UseInventory>");
                sb.Append("<rad2:Address>");
                sb.Append("<rad2:Address1>" + itinerary.Passenger[0].AddressLine1 + "</rad2:Address1>");
                sb.Append("<rad2:Address2>" + itinerary.Passenger[0].AddressLine2 + "</rad2:Address2>");
                sb.Append("<rad2:City>Hyderabad</rad2:City>");
                sb.Append("<rad2:State>na</rad2:State>");
                sb.Append("<rad2:Postal>na</rad2:Postal>");
                sb.Append("<rad2:Country>" + itinerary.Passenger[0].Country.CountryName + "</rad2:Country>");
                sb.Append("<rad2:CountryCode>na</rad2:CountryCode>");
                sb.Append("<rad2:AreaCode>na</rad2:AreaCode>");
                sb.Append("<rad2:PhoneNumber>na</rad2:PhoneNumber>");
                sb.Append("<rad2:Display>na</rad2:Display>");
                sb.Append("</rad2:Address>");
                sb.Append("<rad2:Company></rad2:Company>");
                sb.Append("<rad2:Comments></rad2:Comments>");
                sb.Append("<rad2:Passport>" + itinerary.Passenger[0].PassportNo + "</rad2:Passport>");
                sb.Append("<rad2:Nationality>India</rad2:Nationality>");
                sb.Append("<rad2:ProfileId>-2147483648</rad2:ProfileId>");
                sb.Append("<rad2:IsPrimaryPassenger>true</rad2:IsPrimaryPassenger>");
                sb.Append("<rad2:ContactInfos>");
                sb.Append("<!--Zero or more repetitions:-->");
                sb.Append("<rad2:ContactInfo>");
                sb.Append("<rad2:ContactID>-2141</rad2:ContactID>");
                sb.Append("<rad2:PersonOrgID>-214</rad2:PersonOrgID>");
                sb.Append("<rad2:ContactField>MobilePhone</rad2:ContactField>");
                sb.Append("<rad2:ContactType>WorkPhone</rad2:ContactType>");
                sb.Append("<rad2:Extension>na</rad2:Extension>");
                sb.Append("<rad2:CountryCode>na</rad2:CountryCode>");
                sb.Append("<rad2:AreaCode>" + itinerary.Passenger[0].CellPhone.Split('-')[0] + "</rad2:AreaCode>");
                sb.Append("<rad2:PhoneNumber>" + itinerary.Passenger[0].CellPhone.Split('-')[1] + "</rad2:PhoneNumber>");
                sb.Append("<rad2:Display>na</rad2:Display>");
                sb.Append("<rad2:PreferredContactMethod>false</rad2:PreferredContactMethod>");
                sb.Append("</rad2:ContactInfo>");
                sb.Append("</rad2:ContactInfos>");
                sb.Append("</rad:Payor>");
                sb.Append("</rad:ProcessPNRPayment>");
                sb.Append("</rad:PNRPayments>");
                sb.Append("</tem:PNRPaymentRequest>");
                sb.Append("</tem:ProcessPNRPayment>");
                sb.Append("</soapenv:Body>");
                sb.Append("</soapenv:Envelope>");

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sb.ToString());
                    doc.Save(@""+xmlPath+"FlyDubai_SOAP_ProcessPNRRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml");
                }

                string url = "http://fz.connectpoint.uat.radixx.com/ConnectPoint.Fulfillment.svc";

                string response = "";
                WebClient myWebClient = new WebClient();
                myWebClient.Headers.Add("Content-Type", "text/xml; charset=utf-8");
                myWebClient.Headers.Add("SOAPAction", "\"http://tempuri.org/IConnectPoint_Fulfillment/ProcessPNRPayment\"");
                byte[] byteArray = Encoding.ASCII.GetBytes(sb.ToString());
                //byte[] responseArray = myWebClient.UploadData(url, "POST", byteArray);
                response = myWebClient.UploadString(url, sb.ToString());

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(response);
                        string filePath = @"" + xmlPath + "FlyDubai_SOAP_ProcessPNRResponse_" + sessionId + "_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml"; ;
                        doc.Save(filePath);

                        System.Runtime.Serialization.Formatters.Soap.SoapFormatter formatter = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();
                        formatter.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Full;
                        formatter.FilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
                        formatter.TypeFormat = System.Runtime.Serialization.Formatters.FormatterTypeStyle.TypesAlways;
                        //FileStream fs = new FileStream(filePath, FileMode.Open);
                        //ProcessPNR = formatter.Deserialize(fs) as FlyDubai.ProcessPNRPayment.ViewPNR;
                     
                    }
                }
                catch { }

                //=========================End==========================*/

                #endregion

            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Process PNR " + ex.ToString(), ex);
            }
            return ProcessPNR;
        }


        private FlyDubai.ConfirmBooking.ViewPNR CommitPNR(string guid, string username)
        {
            FlyDubai.ConfirmBooking.ViewPNR CommitPNR = new FlyDubai.ConfirmBooking.ViewPNR();
            try
            {
                CreatePNR pnr = new CreatePNR();

                pnr.ActionType = CreatePNRActionTypes.CommitSummary;
                pnr.CarrierCodes = new FlyDubai.ConfirmBooking.CarrierCode[1];
                pnr.CarrierCodes[0] = new FlyDubai.ConfirmBooking.CarrierCode();
                pnr.CarrierCodes[0].AccessibleCarrierCode = "FZ";
                pnr.ClientIPAddress = "";
                pnr.HistoricUserName = username;
                pnr.ReservationInfo = new FlyDubai.ConfirmBooking.ReservationInfo();
                pnr.ReservationInfo.ConfirmationNumber = "";
                pnr.ReservationInfo.SeriesNumber = "299";
                pnr.SecurityGUID = guid;

                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(CreatePNR));
                    string filePath = @"" + xmlPath + sessionId + "_" + "FlyDubaiCommitPNRRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, pnr);
                    sw.Close();
                    ser = null;
                    //Audit.Add(EventType.FlyDubaiBooking, Severity.Normal, appUserId, filePath, "");
                }
                catch { }

                CommitPNR = bookEvent.CreatePNR(pnr);

                try
                {

                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(FlyDubai.ConfirmBooking.ViewPNR));
                    string filePath = @"" + xmlPath + sessionId + "_" + "FlyDubaiCommitPNRResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, CommitPNR);
                    sw.Close();
                    ser = null;
                    //Audit.Add(EventType.FlyDubaiBooking, Severity.Normal, appUserId, filePath, "");
                }
                catch { }

                #region SOAP Request Code
                /*===============SOAP XML Request Start=======================
                StringBuilder sb = new StringBuilder();

                sb.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/' xmlns:rad='http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request' xmlns:rad1='http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Request'>");
                sb.Append("<soapenv:Header/>");
                sb.Append("<soapenv:Body>");
                sb.Append("<tem:CreatePNR>");
                sb.Append("<!--Optional:-->");
                sb.Append("<tem:CreatePnrRequest>");
                sb.Append("<rad:SecurityGUID>" + guid + "</rad:SecurityGUID>");
                sb.Append("<rad:CarrierCodes>");
                sb.Append("<!--Zero or more repetitions:-->");
                sb.Append("<rad:CarrierCode>");
                sb.Append("<rad:AccessibleCarrierCode>FZ</rad:AccessibleCarrierCode>");
                sb.Append("</rad:CarrierCode>");
                sb.Append("</rad:CarrierCodes>");
                sb.Append("<rad:ClientIPAddress></rad:ClientIPAddress>");
                sb.Append("<rad:HistoricUserName>apicozmoint</rad:HistoricUserName><rad1:ActionType>CommitSummary</rad1:ActionType>");
                sb.Append("<rad1:ReservationInfo>");
                sb.Append("<rad:SeriesNumber>299</rad:SeriesNumber>");
                sb.Append("<rad:ConfirmationNumber></rad:ConfirmationNumber>");
                sb.Append("</rad1:ReservationInfo>");
                sb.Append("</tem:CreatePnrRequest>");
                sb.Append("</tem:CreatePNR>");
                sb.Append("</soapenv:Body>");
                sb.Append("</soapenv:Envelope>");

                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sb.ToString());
                    doc.Save(@"" + xmlPath + "FlyDubai_SOAP_CommitPNRRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml");
                }

                string url = "http://fz.connectpoint.uat.radixx.com/ConnectPoint.Reservation.svc";

                string response = "";
                WebClient myWebClient = new WebClient();
                myWebClient.Headers.Add("Content-Type", "text/xml; charset=utf-8");
                myWebClient.Headers.Add("SOAPAction", "\"http://tempuri.org/IConnectPoint_Reservation/CreatePNR\"");
                byte[] byteArray = Encoding.ASCII.GetBytes(sb.ToString());
                //byte[] responseArray = myWebClient.UploadData(url, "POST", byteArray);
                response = myWebClient.UploadString(url, sb.ToString());

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(response);
                        string filePath = @"" + xmlPath + "FlyDubai_SOAP_CommitPNRResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                        doc.Save(filePath);


                        //System.Runtime.Serialization.Formatters.Soap.SoapFormatter formmater = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();
                        //FileStream fs = new FileStream(filePath, FileMode.Open);
                        //CommitPNR = formmater.Deserialize(fs) as FlyDubai.ConfirmBooking.ViewPNR;
                    }
                }
                catch { }*/
                
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Commit PNR" + ex.ToString(), ex);
            }

            return CommitPNR;
        }


        public FlyDubai.ConfirmBooking.ViewPNR CancelBooking( FlightItinerary itinerary)
        {
            FlyDubai.ConfirmBooking.ViewPNR cancelBookingPNR = null;

            try
            {
                string guid = CreateGUID();

                if(GetValidateGUID(guid))
                {   
                    if (LoginAgency(guid))
                    {
                        CancelPNR cancelBooking = new CancelPNR();
                        cancelBooking.ActionType = CancelPNRActionTypes.CancelReservation;
                        cancelBooking.CarrierCodes = new FlyDubai.ConfirmBooking.CarrierCode[1];
                        cancelBooking.CarrierCodes[0] = new FlyDubai.ConfirmBooking.CarrierCode();
                        cancelBooking.CarrierCodes[0].AccessibleCarrierCode = "FZ";
                        cancelBooking.HistoricUserName = loginName;
                        cancelBooking.ReservationInfo = new FlyDubai.ConfirmBooking.ReservationInfo();
                        cancelBooking.ReservationInfo.ConfirmationNumber = itinerary.PNR;
                        cancelBooking.ReservationInfo.SeriesNumber = "299";
                        cancelBooking.SecurityGUID = guid;

                        try
                        {

                            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(FlyDubai.ConfirmBooking.CancelPNR));
                            string filePath = @"" + xmlPath + sessionId + "_" + "FlyDubaiCancelPNRRequest_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, cancelBooking);
                            sw.Close();
                            ser = null;
                            // Audit.Add(EventType.FlyDubaiBooking, Severity.Normal, appUserId, filePath, "");
                        }
                        catch { }

                        cancelBookingPNR = bookEvent.CancelPNR(cancelBooking);

                        try
                        {

                            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(FlyDubai.ConfirmBooking.ViewPNR));
                            string filePath = @"" + xmlPath + sessionId + "_" + "FlyDubaiCancelPNRResponse_" + DateTime.Now.ToString("ddMMMyyyyhhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, cancelBookingPNR);
                            sw.Close();
                            ser = null;
                            //Audit.Add(EventType.FlyDubaiBooking, Severity.Normal, appUserId, filePath, "");
                        }
                        catch { }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cancelBookingPNR;
        }


        public BookingResponse Book(FlightItinerary itinerary, string sessionId)
        {
            BookingResponse bookResponse = new BookingResponse();

            try
            {
                SearchType searchType = SearchType.OneWay;

                if (itinerary.Segments[0].Origin.AirportCode == itinerary.Segments[itinerary.Segments.Length - 1].Destination.AirportCode)
                {
                    searchType = SearchType.Return;
                }

                //TransactionFeeDetail fee = GetTransactionFee(itinerary.GUID);

                FlyDubai.ConfirmBooking.ViewPNR SummaryPNR = GenerateSummaryPNR(itinerary, searchType);
                //string response = GetSummaryPNR(itinerary, searchType);

                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(response);
                //decimal reservationBalance = Convert.ToDecimal(doc.FirstChild.FirstChild.FirstChild.FirstChild.ChildNodes[28].InnerText);
                if (SummaryPNR.Exceptions[0].ExceptionCode == 0)
                {
                    //fee = GetTransactionFee(itinerary.GUID);

                    FlyDubai.AgencyFees.ViewPNR transactionFee = GetAgencyTransactionFees(itinerary);

                    /*************************************************************************************************************************
                     * transactionFee object will contain transactionFees levied per passenger per segment i.e if travelling from DEL-DOH
                     * then transactionFee of AED 5.00 will be applied for DEL-DXB or DXB-DOH (onward) and DOH-DXB or DXB-DEL (return). All pax 
                     * types will be charged with transactionFee. If either onward or return segment fails to return transactionFee then we
                     * should not allow booking and return error message with bookingResponse=Failed
                     * ***********************************************************************************************************************/
                    if (transactionFee.Exceptions[0].ExceptionCode == 0)
                    {
                        decimal transFee = 0, totalFee = 0;
                        bool transFeeApplied = false;
                        foreach (FlyDubai.AgencyFees.LogicalFlight flight in transactionFee.Airlines[0].LogicalFlight)
                        {
                            totalFee = 0;
                            foreach (FlyDubai.AgencyFees.PhysicalFlight pflight in flight.PhysicalFlights)
                            {
                                foreach (FlyDubai.AgencyFees.AirlinePerson person in pflight.Customers[0].AirlinePersons)
                                {
                                    foreach (FlyDubai.AgencyFees.Charge charge in person.Charges)
                                    {
                                        if (charge.CodeType == "TFEE")
                                        {
                                            totalFee += charge.Amount;
                                            transFee = charge.Amount;
                                        }
                                    }
                                }
                            }
                            if (totalFee / flight.PhysicalFlights[0].Customers[0].AirlinePersons.Length == transFee)
                            {
                                transFeeApplied = true;
                            }
                            else
                            {
                                transFeeApplied = false;
                            }
                        }

                        if (transFeeApplied)
                        {
                            FlyDubai.ProcessPNRPayment.ViewPNR ProcessPNR = ProcessPNRPayments(itinerary, transactionFee.ReservationBalance);

                            if (ProcessPNR.Exceptions[0].ExceptionCode == 0)
                            {
                                FlyDubai.ConfirmBooking.ViewPNR CreatePNR = CommitPNR(itinerary.GUID, GetUserName(itinerary.Segments[0].Origin.CityCode));

                                if (CreatePNR.ReservationBalance == 0 && CreatePNR.Exceptions[0].ExceptionCode == 0)
                                {
                                    bookResponse.PNR = CreatePNR.ConfirmationNumber;
                                    bookResponse.ProdType = ProductType.Flight;
                                    bookResponse.SSRDenied = false;
                                    bookResponse.SSRMessage = "";
                                    bookResponse.Status = BookingResponseStatus.Successful;
                                    itinerary.PNR = CreatePNR.ConfirmationNumber;
                                    itinerary.FareType = "Pub";
                                }
                                else
                                {
                                    bookResponse.Status = BookingResponseStatus.Failed;
                                    bookResponse.Error = CreatePNR.Exceptions[0].ExceptionDescription;
                                }
                            }
                            else
                            {
                                bookResponse.Status = BookingResponseStatus.Failed;
                                bookResponse.Error = ProcessPNR.Exceptions[0].ExceptionDescription;
                            }
                        }
                        else
                        {
                            bookResponse.Status = BookingResponseStatus.Failed;
                            bookResponse.Error = "Transaction Fee not applier per Person per Segment";
                        }
                    }
                    else
                    {
                        bookResponse.Status = BookingResponseStatus.Failed;
                        bookResponse.Error = transactionFee.Exceptions[0].ExceptionDescription;
                    }
                }
                else
                {
                    bookResponse.Status = BookingResponseStatus.Failed;
                    bookResponse.Error = SummaryPNR.Exceptions[0].ExceptionDescription;
                }
            }
            catch (Exception ex)
            {
                bookResponse.Status = BookingResponseStatus.Failed;
                bookResponse.Error = ex.Message;
                throw new Exception("Failed to Book FlyDubai Ticket: " + ex.ToString(), ex);
            }

            return bookResponse;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~FlyDubaiApi()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }    
}
