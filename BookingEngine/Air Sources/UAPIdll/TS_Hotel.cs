﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace UAPIdll.Hotel
{

    //[GeneratedCode("NSwag", "13.0.6.0 (NJsonSchema v10.0.23.0 (Newtonsoft.Json v11.0.0.0))")]
    //public partial class Client
    //{
    //    private string _baseUrl = "http://127.0.0.1/shop";
    //    private HttpClient _httpClient;
    //    private System.Lazy<JsonSerializerSettings> _settings;

    //    public Client(HttpClient httpClient)
    //    {
    //        _httpClient = httpClient;
    //        _settings = new System.Lazy<JsonSerializerSettings>(() =>
    //        {
    //            var settings = new JsonSerializerSettings();
    //            UpdateJsonSerializerSettings(settings);
    //            return settings;
    //        });
    //    }

    //    public string BaseUrl
    //    {
    //        get { return _baseUrl; }
    //        set { _baseUrl = value; }
    //    }

    //    protected JsonSerializerSettings JsonSerializerSettings { get { return _settings.Value; } }

    //    partial void UpdateJsonSerializerSettings(JsonSerializerSettings settings);
    //    partial void PrepareRequest(HttpClient client, HttpRequestMessage request, string url);
    //    partial void PrepareRequest(HttpClient client, HttpRequestMessage request, StringBuilder urlBuilder);
    //    partial void ProcessResponse(HttpClient client, HttpResponseMessage response);

    //    /// <summary>PropertyResource - Returns List of Properties</summary>
    //    /// <param name="checkinDate">Checkin date</param>
    //    /// <param name="checkoutDate">Checkout date</param>
    //    /// <param name="numberOfGuests">1 digit number, max 9</param>
    //    /// <param name="radius">Limits search to a specified radius</param>
    //    /// <param name="unitOfDistance">Miles, Kilometers, etc.</param>
    //    /// <param name="city">City (e.g., Dublin), town, or postal station (i.e., a postal service territory, often used in a military address).Use lat/lon or city/state/country params</param>
    //    /// <param name="state">State or Province code, i.e. CA. Use lat/lon or city/state/country params</param>
    //    /// <param name="country">Country Code, i.e. US, USA. Use lat/lon or city/state/country params</param>
    //    /// <param name="lat">Latitude of the location. Use lat/lon or city/state/country params</param>
    //    /// <param name="lon">Longitude of the location. Use lat/lon or city/state/country params</param>
    //    /// <param name="minRate">Minimum rate filter.</param>
    //    /// <param name="maxRate">Maximum rate filter.</param>
    //    /// <param name="getMoreKey">Get more index key.</param>
    //    /// <param name="chainCode">Hotel Chain Code</param>
    //    /// <param name="host">Host:1P or 1G</param>
    //    /// <param name="sort">Sorty by : distance or rating</param>
    //    /// <param name="hotelcontent">hotelcontent header value is use to toggle hotelcontent in the response.</param>
    //    /// <param name="accessProfile">Host access profile for Host access service call.</param>
    //    /// <param name="imageSize">Image Dimension Codes. Valid values are G, T, I, M, B, F, J, L, E, O, H, C.</param>
    //    /// <returns>Payload for the GetPropertyList operationID</returns>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public Task<Properties> GetPropertyListAsync(string checkinDate, string checkoutDate, string numberOfGuests, string radius, UnitOfDistance2? unitOfDistance, string city, string state, string country, double? lat, double? lon, object minRate, object maxRate, int? getMoreKey, string chainCode, string host, string sort, string hotelcontent, string accessProfile, string imageSize)
    //    {
    //        return GetPropertyListAsync(checkinDate, checkoutDate, numberOfGuests, radius, unitOfDistance, city, state, country, lat, lon, minRate, maxRate, getMoreKey, chainCode, host, sort, hotelcontent, accessProfile, imageSize, CancellationToken.None);
    //    }

    //    /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
    //    /// <summary>PropertyResource - Returns List of Properties</summary>
    //    /// <param name="checkinDate">Checkin date</param>
    //    /// <param name="checkoutDate">Checkout date</param>
    //    /// <param name="numberOfGuests">1 digit number, max 9</param>
    //    /// <param name="radius">Limits search to a specified radius</param>
    //    /// <param name="unitOfDistance">Miles, Kilometers, etc.</param>
    //    /// <param name="city">City (e.g., Dublin), town, or postal station (i.e., a postal service territory, often used in a military address).Use lat/lon or city/state/country params</param>
    //    /// <param name="state">State or Province code, i.e. CA. Use lat/lon or city/state/country params</param>
    //    /// <param name="country">Country Code, i.e. US, USA. Use lat/lon or city/state/country params</param>
    //    /// <param name="lat">Latitude of the location. Use lat/lon or city/state/country params</param>
    //    /// <param name="lon">Longitude of the location. Use lat/lon or city/state/country params</param>
    //    /// <param name="minRate">Minimum rate filter.</param>
    //    /// <param name="maxRate">Maximum rate filter.</param>
    //    /// <param name="getMoreKey">Get more index key.</param>
    //    /// <param name="chainCode">Hotel Chain Code</param>
    //    /// <param name="host">Host:1P or 1G</param>
    //    /// <param name="sort">Sorty by : distance or rating</param>
    //    /// <param name="hotelcontent">hotelcontent header value is use to toggle hotelcontent in the response.</param>
    //    /// <param name="accessProfile">Host access profile for Host access service call.</param>
    //    /// <param name="imageSize">Image Dimension Codes. Valid values are G, T, I, M, B, F, J, L, E, O, H, C.</param>
    //    /// <returns>Payload for the GetPropertyList operationID</returns>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public async Task<Properties> GetPropertyListAsync(string checkinDate, string checkoutDate, string numberOfGuests, string radius, UnitOfDistance2? unitOfDistance, string city, string state, string country, double? lat, double? lon, object minRate, object maxRate, int? getMoreKey, string chainCode, string host, string sort, string hotelcontent, string accessProfile, string imageSize, CancellationToken cancellationToken)
    //    {
    //        if (checkinDate == null)
    //            throw new System.ArgumentNullException("checkinDate");

    //        if (checkoutDate == null)
    //            throw new System.ArgumentNullException("checkoutDate");

    //        if (numberOfGuests == null)
    //            throw new System.ArgumentNullException("numberOfGuests");

    //        if (radius == null)
    //            throw new System.ArgumentNullException("radius");

    //        var urlBuilder_ = new StringBuilder();
    //        urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/properties?");
    //        urlBuilder_.Append(System.Uri.EscapeDataString("checkinDate") + "=").Append(System.Uri.EscapeDataString(ConvertToString(checkinDate, CultureInfo.InvariantCulture))).Append("&");
    //        urlBuilder_.Append(System.Uri.EscapeDataString("checkoutDate") + "=").Append(System.Uri.EscapeDataString(ConvertToString(checkoutDate, CultureInfo.InvariantCulture))).Append("&");
    //        urlBuilder_.Append(System.Uri.EscapeDataString("numberOfGuests") + "=").Append(System.Uri.EscapeDataString(ConvertToString(numberOfGuests, CultureInfo.InvariantCulture))).Append("&");
    //        urlBuilder_.Append(System.Uri.EscapeDataString("radius") + "=").Append(System.Uri.EscapeDataString(ConvertToString(radius, CultureInfo.InvariantCulture))).Append("&");
    //        if (unitOfDistance != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("unitOfDistance") + "=").Append(System.Uri.EscapeDataString(ConvertToString(unitOfDistance, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        if (city != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("city") + "=").Append(System.Uri.EscapeDataString(ConvertToString(city, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        if (state != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("state") + "=").Append(System.Uri.EscapeDataString(ConvertToString(state, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        if (country != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("country") + "=").Append(System.Uri.EscapeDataString(ConvertToString(country, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        if (lat != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("lat") + "=").Append(System.Uri.EscapeDataString(ConvertToString(lat, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        if (lon != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("lon") + "=").Append(System.Uri.EscapeDataString(ConvertToString(lon, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        if (minRate != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("minRate") + "=").Append(System.Uri.EscapeDataString(ConvertToString(minRate, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        if (maxRate != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("maxRate") + "=").Append(System.Uri.EscapeDataString(ConvertToString(maxRate, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        if (getMoreKey != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("getMoreKey") + "=").Append(System.Uri.EscapeDataString(ConvertToString(getMoreKey, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        if (chainCode != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("chainCode") + "=").Append(System.Uri.EscapeDataString(ConvertToString(chainCode, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        if (host != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("host") + "=").Append(System.Uri.EscapeDataString(ConvertToString(host, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        urlBuilder_.Length--;

    //        var client_ = _httpClient;
    //        try
    //        {
    //            using (var request_ = new HttpRequestMessage())
    //            {
    //                if (sort != null)
    //                    request_.Headers.TryAddWithoutValidation("sort", ConvertToString(sort, CultureInfo.InvariantCulture));
    //                if (hotelcontent != null)
    //                    request_.Headers.TryAddWithoutValidation("hotelcontent", ConvertToString(hotelcontent, CultureInfo.InvariantCulture));
    //                if (accessProfile != null)
    //                    request_.Headers.TryAddWithoutValidation("accessProfile", ConvertToString(accessProfile, CultureInfo.InvariantCulture));
    //                if (imageSize != null)
    //                    request_.Headers.TryAddWithoutValidation("imageSize", ConvertToString(imageSize, CultureInfo.InvariantCulture));
    //                request_.Method = new HttpMethod("GET");
    //                request_.Headers.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));

    //                PrepareRequest(client_, request_, urlBuilder_);
    //                var url_ = urlBuilder_.ToString();
    //                request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);
    //                PrepareRequest(client_, request_, url_);

    //                var response_ = await client_.SendAsync(request_, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
    //                try
    //                {
    //                    var headers_ = Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
    //                    if (response_.Content != null && response_.Content.Headers != null)
    //                    {
    //                        foreach (var item_ in response_.Content.Headers)
    //                            headers_[item_.Key] = item_.Value;
    //                    }

    //                    ProcessResponse(client_, response_);

    //                    var status_ = ((int)response_.StatusCode).ToString();
    //                    if (status_ == "200")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<Properties>(response_, headers_).ConfigureAwait(false);
    //                        return objectResponse_.Object;
    //                    }
    //                    else
    //                    if (status_ == "400")
    //                    {
    //                        string responseText_ = (response_.Content == null) ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
    //                        throw new ApiException("Invalid Paramaters", (int)response_.StatusCode, responseText_, headers_, null);
    //                    }
    //                    else
    //                    if (status_ == "500")
    //                    {
    //                        string responseText_ = (response_.Content == null) ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
    //                        throw new ApiException("Internal Error", (int)response_.StatusCode, responseText_, headers_, null);
    //                    }
    //                    else
    //                    if (status_ != "200" && status_ != "204")
    //                    {
    //                        var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
    //                        throw new ApiException("The HTTP status code of the response was not expected (" + (int)response_.StatusCode + ").", (int)response_.StatusCode, responseData_, headers_, null);
    //                    }

    //                    return default(Properties);
    //                }
    //                finally
    //                {
    //                    if (response_ != null)
    //                        response_.Dispose();
    //                }
    //            }
    //        }
    //        finally
    //        {
    //        }
    //    }

    //    /// <summary>propertiesDetail</summary>
    //    /// <param name="checkinDate">Checkin date</param>
    //    /// <param name="checkoutDate">Checkout date</param>
    //    /// <param name="numberOfGuests">1 digit number</param>
    //    /// <param name="host">1G(can be null/empty for 1G) or 1P</param>
    //    /// <param name="imageSize">Image Dimension Codes. Valid values are G, T, I, M, B, F, J, L, E, O, H, C.</param>
    //    /// <returns>Success</returns>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public Task<PropertyDetail> GetPropertyDetailsAsync(string chainCode, string propertyCode, string checkinDate, string checkoutDate, string numberOfGuests, string host, string imageSize, string accept)
    //    {
    //        return GetPropertyDetailsAsync(chainCode, propertyCode, checkinDate, checkoutDate, numberOfGuests, host, imageSize, accept, CancellationToken.None);
    //    }

    //    /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
    //    /// <summary>propertiesDetail</summary>
    //    /// <param name="checkinDate">Checkin date</param>
    //    /// <param name="checkoutDate">Checkout date</param>
    //    /// <param name="numberOfGuests">1 digit number</param>
    //    /// <param name="host">1G(can be null/empty for 1G) or 1P</param>
    //    /// <param name="imageSize">Image Dimension Codes. Valid values are G, T, I, M, B, F, J, L, E, O, H, C.</param>
    //    /// <returns>Success</returns>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public async Task<PropertyDetail> GetPropertyDetailsAsync(string chainCode, string propertyCode, string checkinDate, string checkoutDate, string numberOfGuests, string host, string imageSize, string accept, CancellationToken cancellationToken)
    //    {
    //        if (chainCode == null)
    //            throw new System.ArgumentNullException("chainCode");

    //        if (propertyCode == null)
    //            throw new System.ArgumentNullException("propertyCode");

    //        if (checkinDate == null)
    //            throw new System.ArgumentNullException("checkinDate");

    //        if (checkoutDate == null)
    //            throw new System.ArgumentNullException("checkoutDate");

    //        if (numberOfGuests == null)
    //            throw new System.ArgumentNullException("numberOfGuests");

    //        var urlBuilder_ = new StringBuilder();
    //        urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/properties/{chainCode}/{propertyCode}?");
    //        urlBuilder_.Replace("{chainCode}", System.Uri.EscapeDataString(ConvertToString(chainCode, CultureInfo.InvariantCulture)));
    //        urlBuilder_.Replace("{propertyCode}", System.Uri.EscapeDataString(ConvertToString(propertyCode, CultureInfo.InvariantCulture)));
    //        urlBuilder_.Append(System.Uri.EscapeDataString("checkinDate") + "=").Append(System.Uri.EscapeDataString(ConvertToString(checkinDate, CultureInfo.InvariantCulture))).Append("&");
    //        urlBuilder_.Append(System.Uri.EscapeDataString("checkoutDate") + "=").Append(System.Uri.EscapeDataString(ConvertToString(checkoutDate, CultureInfo.InvariantCulture))).Append("&");
    //        urlBuilder_.Append(System.Uri.EscapeDataString("numberOfGuests") + "=").Append(System.Uri.EscapeDataString(ConvertToString(numberOfGuests, CultureInfo.InvariantCulture))).Append("&");
    //        if (host != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("host") + "=").Append(System.Uri.EscapeDataString(ConvertToString(host, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        urlBuilder_.Length--;

    //        var client_ = _httpClient;
    //        try
    //        {
    //            using (var request_ = new HttpRequestMessage())
    //            {
    //                if (imageSize != null)
    //                    request_.Headers.TryAddWithoutValidation("imageSize", ConvertToString(imageSize, CultureInfo.InvariantCulture));
    //                if (accept != null)
    //                    request_.Headers.TryAddWithoutValidation("Accept", ConvertToString(accept, CultureInfo.InvariantCulture));
    //                request_.Method = new HttpMethod("GET");

    //                PrepareRequest(client_, request_, urlBuilder_);
    //                var url_ = urlBuilder_.ToString();
    //                request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);
    //                PrepareRequest(client_, request_, url_);

    //                var response_ = await client_.SendAsync(request_, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
    //                try
    //                {
    //                    var headers_ = Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
    //                    if (response_.Content != null && response_.Content.Headers != null)
    //                    {
    //                        foreach (var item_ in response_.Content.Headers)
    //                            headers_[item_.Key] = item_.Value;
    //                    }

    //                    ProcessResponse(client_, response_);

    //                    var status_ = ((int)response_.StatusCode).ToString();
    //                    if (status_ == "200")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<PropertyDetail>(response_, headers_).ConfigureAwait(false);
    //                        return objectResponse_.Object;
    //                    }
    //                    else
    //                    if (status_ == "400")
    //                    {
    //                        string responseText_ = (response_.Content == null) ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
    //                        throw new ApiException("Invalid Paramaters", (int)response_.StatusCode, responseText_, headers_, null);
    //                    }
    //                    else
    //                    if (status_ == "500")
    //                    {
    //                        string responseText_ = (response_.Content == null) ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
    //                        throw new ApiException("Internal Error", (int)response_.StatusCode, responseText_, headers_, null);
    //                    }
    //                    else
    //                    if (status_ != "200" && status_ != "204")
    //                    {
    //                        var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
    //                        throw new ApiException("The HTTP status code of the response was not expected (" + (int)response_.StatusCode + ").", (int)response_.StatusCode, responseData_, headers_, null);
    //                    }

    //                    return default(PropertyDetail);
    //                }
    //                finally
    //                {
    //                    if (response_ != null)
    //                        response_.Dispose();
    //                }
    //            }
    //        }
    //        finally
    //        {
    //        }
    //    }

    //    protected struct ObjectResponseResult<T>
    //    {
    //        public ObjectResponseResult(T responseObject, string responseText)
    //        {
    //            this.Object = responseObject;
    //            this.Text = responseText;
    //        }

    //        public T Object { get; }

    //        public string Text { get; }
    //    }

    //    public bool ReadResponseAsString { get; set; }

    //    protected virtual async Task<ObjectResponseResult<T>> ReadObjectResponseAsync<T>(HttpResponseMessage response, IReadOnlyDictionary<string, IEnumerable<string>> headers)
    //    {
    //        if (response == null || response.Content == null)
    //        {
    //            return new ObjectResponseResult<T>(default(T), string.Empty);
    //        }

    //        if (ReadResponseAsString)
    //        {
    //            var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
    //            try
    //            {
    //                var typedBody = JsonConvert.DeserializeObject<T>(responseText, JsonSerializerSettings);
    //                return new ObjectResponseResult<T>(typedBody, responseText);
    //            }
    //            catch (JsonException exception)
    //            {
    //                var message = "Could not deserialize the response body string as " + typeof(T).FullName + ".";
    //                throw new ApiException(message, (int)response.StatusCode, responseText, headers, exception);
    //            }
    //        }
    //        else
    //        {
    //            try
    //            {
    //                using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
    //                using (var streamReader = new StreamReader(responseStream))
    //                using (var jsonTextReader = new JsonTextReader(streamReader))
    //                {
    //                    var serializer = JsonSerializer.Create(JsonSerializerSettings);
    //                    var typedBody = serializer.Deserialize<T>(jsonTextReader);
    //                    return new ObjectResponseResult<T>(typedBody, string.Empty);
    //                }
    //            }
    //            catch (JsonException exception)
    //            {
    //                var message = "Could not deserialize the response body stream as " + typeof(T).FullName + ".";
    //                throw new ApiException(message, (int)response.StatusCode, string.Empty, headers, exception);
    //            }
    //        }
    //    }

    //    private string ConvertToString(object value, CultureInfo cultureInfo)
    //    {
    //        if (value is System.Enum)
    //        {
    //            string name = System.Enum.GetName(value.GetType(), value);
    //            if (name != null)
    //            {
    //                var field = IntrospectionExtensions.GetTypeInfo(value.GetType()).GetDeclaredField(name);
    //                if (field != null)
    //                {
    //                    var attribute = CustomAttributeExtensions.GetCustomAttribute(field, typeof(EnumMemberAttribute))
    //                        as EnumMemberAttribute;
    //                    if (attribute != null)
    //                    {
    //                        return attribute.Value != null ? attribute.Value : name;
    //                    }
    //                }
    //            }
    //        }
    //        else if (value is bool)
    //        {
    //            return System.Convert.ToString(value, cultureInfo).ToLowerInvariant();
    //        }
    //        else if (value is byte[])
    //        {
    //            return System.Convert.ToBase64String((byte[])value);
    //        }
    //        else if (value != null && value.GetType().IsArray)
    //        {
    //            var array = Enumerable.OfType<object>((System.Array)value);
    //            return string.Join(",", Enumerable.Select(array, o => ConvertToString(o, cultureInfo)));
    //        }

    //        return System.Convert.ToString(value, cultureInfo);
    //    }
    //}

    /// <summary>The standard code or abbreviation for the state, province, or region with optional name</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class StateProv
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(8, MinimumLength = 2)]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("name", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Name { get; set; }
    }

    /// <summary>Provides address information. Unformatted addresses are captured using the simple facet.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Address
    {
        /// <summary>Internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:AddressBldgRoom</summary>
        [JsonProperty("BldgRoom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public AddressBldgRoom BldgRoom { get; set; }

        /// <summary>Assigned Type: c-0600:AddressStreetNumber</summary>
        [JsonProperty("Number", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public AddressStreetNumber Number { get; set; }

        /// <summary>May contain the street number when the Street number element is missing.</summary>
        [JsonProperty("Street", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(128)]
        public string Street { get; set; }

        [JsonProperty("Address_Line", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(3)]
        public ICollection<string> Address_Line { get; set; }

        /// <summary>City (e.g., Dublin), town, or postal station (i.e., a postal service territory, often used in a military address).</summary>
        [JsonProperty("City", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(128)]
        public string City { get; set; }

        /// <summary>County or Region Name (e.g., Fairfax).</summary>
        [JsonProperty("County", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string County { get; set; }

        /// <summary>Assigned Type: c-0600:StateProv</summary>
        [JsonProperty("StateProv", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public StateProv StateProv { get; set; }

        /// <summary>Assigned Type: c-0600:Country</summary>
        [JsonProperty("Country", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Country Country { get; set; }

        /// <summary>Post Office Code number.</summary>
        [JsonProperty("PostalCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string PostalCode { get; set; }

        [JsonProperty("role", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Enum_AddressRole Role { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Enum_AddressRole
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Enum_AddressRole_Open? Value { get; set; }

        [JsonProperty("extension", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128, MinimumLength = 1)]
        public string Extension { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum Enum_AddressRole_Base
    {
        [EnumMember(Value = @"Home")]
        Home = 0,

        [EnumMember(Value = @"Business")]
        Business = 1,

        [EnumMember(Value = @"Mailing")]
        Mailing = 2,

        [EnumMember(Value = @"Delivery")]
        Delivery = 3,

        [EnumMember(Value = @"Destination")]
        Destination = 4,

        [EnumMember(Value = @"Other")]
        Other = 5,

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum Enum_AddressRole_Open
    {
        [EnumMember(Value = @"Home")]
        Home = 0,

        [EnumMember(Value = @"Business")]
        Business = 1,

        [EnumMember(Value = @"Mailing")]
        Mailing = 2,

        [EnumMember(Value = @"Delivery")]
        Delivery = 3,

        [EnumMember(Value = @"Destination")]
        Destination = 4,

        [EnumMember(Value = @"Other")]
        Other = 5,

        [EnumMember(Value = @"Other_")]
        Other_ = 6,

    }

    /// <summary>ISO 3166 code for a country with optional name</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Country
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{2}")]
        public string Value { get; set; }

        /// <summary>Use this id to internally identify this country in NextSteps</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("name", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Name { get; set; }

        /// <summary>Assigned Type: c-0600:CodeContext</summary>
        [JsonProperty("codeContext", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string CodeContext { get; set; }


    }

    /// <summary>Identifier provides the ability to create a globally unique identifier. For the identifier to be globally unique it must have a system provided identifier and the system must be identified using a global naming authority. System identification uses the domain naming system (DNS) to assure they are globally unique and should be an URL. The system provided ID will typically be a primary or surrogate key in a database.\n\nThe URL, system and company attributes can be omitted only when the system context can be implied by a parent or ancestor element.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Identifier
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("authority", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Authority { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Result
    {
        /// <summary>Indication of the processing status of the response message</summary>
        [JsonProperty("status", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultStatus_Enum? Status { get; set; }

        [JsonProperty("Error", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<Error> Error { get; set; }

        [JsonProperty("Warning", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<Warning> Warning { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Enum_TelephoneRole
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Enum_TelephoneRole_Open? Value { get; set; }

        [JsonProperty("extension", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128, MinimumLength = 1)]
        public string Extension { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum Enum_TelephoneRole_Base
    {
        [EnumMember(Value = @"Mobile")]
        Mobile = 0,

        [EnumMember(Value = @"Home")]
        Home = 1,

        [EnumMember(Value = @"Work")]
        Work = 2,

        [EnumMember(Value = @"Office")]
        Office = 3,

        [EnumMember(Value = @"Fax")]
        Fax = 4,

        [EnumMember(Value = @"Other")]
        Other = 5,

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum Enum_TelephoneRole_Open
    {
        [EnumMember(Value = @"Mobile")]
        Mobile = 0,

        [EnumMember(Value = @"Home")]
        Home = 1,

        [EnumMember(Value = @"Work")]
        Work = 2,

        [EnumMember(Value = @"Office")]
        Office = 3,

        [EnumMember(Value = @"Fax")]
        Fax = 4,

        [EnumMember(Value = @"Other")]
        Other = 5,

        [EnumMember(Value = @"Other_")]
        Other_ = 6,

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AddressStreetNumber
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("streetNmbrSuffix", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string StreetNmbrSuffix { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("streetDirection", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string StreetDirection { get; set; }

        /// <summary>Assigned Type: c-0600:RuralRouteNumber</summary>
        [JsonProperty("ruralRouteNmbr", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9]{1,5}")]
        public string RuralRouteNmbr { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("po_Box", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Po_Box { get; set; }


    }

    /// <summary>A short string  \nMinimum length of 0 and a maximum length of 128</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AddressBldgRoom
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Value { get; set; }

        /// <summary>When true, the information is a building name. When false, it is an apartment or room #</summary>
        [JsonProperty("buldingInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? BuldingInd { get; set; }


    }

    /// <summary>A URL that describes a step that can be applied to the resource containing the next step structure.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class NextStep
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }

        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("action", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(32)]
        public string Action { get; set; }

        /// <summary>possible action that can be performed on the returned results</summary>
        [JsonProperty("method", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(StringEnumConverter))]
        public NextStepMethods Method { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Description { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class NextSteps
    {
        /// <summary>The base portion of the uri in order to shorten the uri's in the individual steps</summary>
        [JsonProperty("baseURI", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        public string BaseURI { get; set; }

        /// <summary>Optional internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("NextStep", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(20)]
        public List<NextStep> NextStep { get; set; } = new List<NextStep>();

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>Describes the set of potential methods that can be taken after an operation.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum NextStepMethods
    {
        [EnumMember(Value = @"GET")]
        GET = 0,

        [EnumMember(Value = @"DELETE")]
        DELETE = 1,

        [EnumMember(Value = @"PUT")]
        PUT = 2,

        [EnumMember(Value = @"POST")]
        POST = 3,

    }

    /// <summary>Used for data stored in Name\/Value pairs</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class NameValuePair
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Value { get; set; }

        /// <summary>Optional internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("name", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(512)]
        public string Name { get; set; }


    }

    /// <summary>A monetary amount, up to 4 decimal places. Decimal place needs to be included.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class CurrencyAmount
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public double? Value { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyCode_ISO</summary>
        [JsonProperty("code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{3}")]
        public string Code { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyMinorUnit</summary>
        [JsonProperty("minorUnit", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? MinorUnit { get; set; }

        /// <summary>The origin of the requested currency code</summary>
        [JsonProperty("currencySource", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public CurrencySource_Enum? CurrencySource { get; set; }

        /// <summary>True if the currency amount has been converted from the original amount</summary>
        [JsonProperty("approximateInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ApproximateInd { get; set; }


    }

    /// <summary>The status of an error or warning</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum ResultStatus_Enum
    {
        [EnumMember(Value = @"Not processed")]
        Not_processed = 0,

        [EnumMember(Value = @"Incomplete")]
        Incomplete = 1,

        [EnumMember(Value = @"Complete")]
        Complete = 2,

        [EnumMember(Value = @"Unknown")]
        Unknown = 3,

    }

    /// <summary>Information about errors or warnings returned by atomic for the end user or to aid in troubleshooting</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("ErrorWarningDetail", typeof(ErrorWarningDetail))]
    [JsonInheritanceAttribute("ErrorDetail", typeof(ErrorDetail))]
    [JsonInheritanceAttribute("WarningDetail", typeof(WarningDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ErrorWarning
    {
        /// <summary>Http standard response code</summary>
        [JsonProperty("StatusCode", Required = Required.Always)]
        public int StatusCode { get; set; }

        /// <summary>TODO - \nTravelport standardized error\/warning text. May contain parameters, and include which specific part of the request did not work and why. Get standardized message list, with parameters if appropriate, from ?????????????????????</summary>
        [JsonProperty("Message", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string Message { get; set; }

        [JsonProperty("NameValuePair", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<NameValuePair> NameValuePair { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Information about errors or warnings returned by atomic for the end user or to aid in troubleshooting</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Error
    {
        /// <summary>Http standard response code</summary>
        [JsonProperty("StatusCode", Required = Required.Always)]
        public int StatusCode { get; set; }

        /// <summary>TODO - \nTravelport standardized error\/warning text. May contain parameters, and include which specific part of the request did not work and why. Get standardized message list, with parameters if appropriate, from ?????????????????????</summary>
        [JsonProperty("Message", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string Message { get; set; }

        [JsonProperty("NameValuePair", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<NameValuePair> NameValuePair { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Information about errors or warnings returned by atomic for the end user or to aid in troubleshooting</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Warning
    {
        /// <summary>Http standard response code</summary>
        [JsonProperty("StatusCode", Required = Required.Always)]
        public int StatusCode { get; set; }

        /// <summary>TODO - \nTravelport standardized error\/warning text. May contain parameters, and include which specific part of the request did not work and why. Get standardized message list, with parameters if appropriate, from ?????????????????????</summary>
        [JsonProperty("Message", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string Message { get; set; }

        [JsonProperty("NameValuePair", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<NameValuePair> NameValuePair { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Information about errors or warnings returned by atomic for the end user or to aid in troubleshooting</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ErrorWarningDetail : ErrorWarning
    {
        /// <summary>Travelport assigned source host or core identifier</summary>
        [JsonProperty("SourceID", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(128)]
        public string SourceID { get; set; }

        /// <summary>The error or warning code returned by the core\/host system</summary>
        [JsonProperty("SourceCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string SourceCode { get; set; }

        /// <summary>The error message as it is returned from the originating core\/host system</summary>
        [JsonProperty("SourceDescription", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string SourceDescription { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Information about errors or warnings returned by atomic for the end user or to aid in troubleshooting</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ErrorDetail : ErrorWarning
    {
        /// <summary>Travelport assigned source host or core identifier</summary>
        [JsonProperty("SourceID", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(128)]
        public string SourceID { get; set; }

        /// <summary>The error or warning code returned by the core\/host system</summary>
        [JsonProperty("SourceCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string SourceCode { get; set; }

        /// <summary>The error message as it is returned from the originating core\/host system</summary>
        [JsonProperty("SourceDescription", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string SourceDescription { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Information about errors or warnings returned by atomic for the end user or to aid in troubleshooting</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class WarningDetail : ErrorWarning
    {
        /// <summary>Travelport assigned source host or core identifier</summary>
        [JsonProperty("SourceID", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(128)]
        public string SourceID { get; set; }

        /// <summary>The error or warning code returned by the core\/host system</summary>
        [JsonProperty("SourceCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string SourceCode { get; set; }

        /// <summary>The error message as it is returned from the originating core\/host system</summary>
        [JsonProperty("SourceDescription", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string SourceDescription { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Miles, Kilometers, etc.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum UnitOfDistance
    {
        [EnumMember(Value = @"Miles")]
        Miles = 0,

        [EnumMember(Value = @"Kilometers")]
        Kilometers = 1,

    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class BaseResponse
    {
        /// <summary>Unique transaction, correlation or tracking id for a single request and reply i.e. for a single transaction. Should be a 128 bit GUID format. Also know as E2ETrackingId.</summary>
        [JsonProperty("transactionId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TransactionId { get; set; }

        /// <summary>Optional ID for internal child transactions created for processing a single request (single transaction). Should be a 128 bit GUID format. Also known as ChildTrackingId.</summary>
        [JsonProperty("traceId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TraceId { get; set; }

        /// <summary>Assigned Type: c-0600:Result</summary>
        [JsonProperty("Result", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Result Result { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>Used to specify the geographic coordinates of a location</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GeoLocation
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("latitude", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(32)]
        public string Latitude { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("longitude", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(32)]
        public string Longitude { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("altitude", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Altitude { get; set; }

        /// <summary>Provides the unit of measure for the altitude (i.e. feet, miles, kilometers)</summary>
        [JsonProperty("altitudeUnitOfDistance", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public UnitOfDistance? AltitudeUnitOfDistance { get; set; }

        /// <summary>Specifies the level of accuracy for the lat\/long\/alt</summary>
        [JsonProperty("positionAccuracy", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public PositionAccuracy_Enum? PositionAccuracy { get; set; }

        /// <summary>link for embedded map showing location</summary>
        [JsonProperty("mapURL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string MapURL { get; set; }

        /// <summary>The URL to the format for the latitude and longitude for this location.</summary>
        [JsonProperty("formatURL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string FormatURL { get; set; }


    }

    /// <summary>Specifies the level of accuracy for the position</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum PositionAccuracy_Enum
    {
        [EnumMember(Value = @"Zip9Code")]
        Zip9Code = 0,

        [EnumMember(Value = @"Zip7Code")]
        Zip7Code = 1,

        [EnumMember(Value = @"Zip5Code")]
        Zip5Code = 2,

        [EnumMember(Value = @"Street")]
        Street = 3,

        [EnumMember(Value = @"State")]
        State = 4,

        [EnumMember(Value = @"Property")]
        Property = 5,

        [EnumMember(Value = @"Intersection")]
        Intersection = 6,

        [EnumMember(Value = @"Exact")]
        Exact = 7,

        [EnumMember(Value = @"County")]
        County = 8,

        [EnumMember(Value = @"City")]
        City = 9,

        [EnumMember(Value = @"Block")]
        Block = 10,

    }

    /// <summary>The system requesting or returning the currency code specified in the attribute</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum CurrencySource_Enum
    {
        [EnumMember(Value = @"Supplier")]
        Supplier = 0,

        [EnumMember(Value = @"Charged")]
        Charged = 1,

        [EnumMember(Value = @"Requested")]
        Requested = 2,

    }

    /// <summary>Used as the Base Payload of a Response Action Facet</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ErrorResponse
    {
        /// <summary>Unique transaction, correlation or tracking id for a single request and reply i.e. for a single transaction. Should be a 128 bit GUID format. Also know as E2ETrackingId.</summary>
        [JsonProperty("transactionId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TransactionId { get; set; }

        /// <summary>Optional ID for internal child transactions created for processing a single request (single transaction). Should be a 128 bit GUID format. Also known as ChildTrackingId.</summary>
        [JsonProperty("traceId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TraceId { get; set; }

        /// <summary>Assigned Type: c-0600:Result</summary>
        [JsonProperty("Result", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Result Result { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>Identifies the availability status of an item.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum AvailabilityStatus_Enum
    {
        [EnumMember(Value = @"Open")]
        Open = 0,

        [EnumMember(Value = @"Close")]
        Close = 1,

        [EnumMember(Value = @"ClosedOnArrival")]
        ClosedOnArrival = 2,

        [EnumMember(Value = @"ClosedOnArrivalOnRequest")]
        ClosedOnArrivalOnRequest = 3,

        [EnumMember(Value = @"OnRequest")]
        OnRequest = 4,

        [EnumMember(Value = @"RemoveCloseOnly")]
        RemoveCloseOnly = 5,

    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("PropertyDetail", typeof(PropertyDetail))]
    [JsonInheritanceAttribute("Property", typeof(Property))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropertyID
    {
        /// <summary>Local reference id.</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }

        /// <summary>Assigned Type: cthp-0500:PropertyKey</summary>
        [JsonProperty("PropertyKey", Required = Required.Always)]
        public PropertyKey PropertyKey { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Property : PropertyID
    {
        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("name", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(128)]
        public string Name { get; set; }

        [JsonProperty("Rating", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(5)]
        public ICollection<Rating> Rating { get; set; }

        /// <summary>Assigned Type: c-0600:GeoLocation</summary>
        [JsonProperty("GeoLocation", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public GeoLocation GeoLocation { get; set; }

        [JsonProperty("Image", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public ICollection<Image> Image { get; set; }

        [JsonProperty("Description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(5)]
        public ICollection<string> Description { get; set; }

        [JsonProperty("BusinessService", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<Service> BusinessService { get; set; }

        [JsonProperty("AccessibilityFeature", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(25)]
        public ICollection<OTA_CodeWithDescription> AccessibilityFeature { get; set; }

        [JsonProperty("GuestRoomInfo", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<GuestRoomInfo> GuestRoomInfo { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps_Summary</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropertyDetail : Property
    {
        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("classTypeCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string ClassTypeCode { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("segmentCatagoryCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string SegmentCatagoryCode { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("locationCatagoryCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string LocationCatagoryCode { get; set; }

        /// <summary>Assigned Type: c-0600:Address_Summary</summary>
        [JsonProperty("Address", Required = Required.Always)]
        public Address Address { get; set; }

        [JsonProperty("Telephone", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(5)]
        public List<string> Telephone { get; set; } = new List<string>();

        [JsonProperty("Amenity", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(100)]
        public ICollection<OTA_CodeWithDescription> Amenity { get; set; } = new Collection<OTA_CodeWithDescription>();

        /// <summary>Assigned Type: c-0600:BaseResponse_Summary</summary>
        [JsonProperty("BaseResponse", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public BaseResponse BaseResponse { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropertyQueryMedia
    {
        [JsonProperty("PropertyMediaRequest", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(10)]
        public ICollection<PropertyMediaRequest> PropertyMediaRequest { get; set; } = new Collection<PropertyMediaRequest>();


    }

    /// <summary>The actual award or rating received by the facility.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Rating
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("provider", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Provider { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropertiesRequest
    {
        /// <summary>The order in which to return hotel properties.</summary>
        [JsonProperty("sortOrder", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(StringEnumConverter))]
        public HotelSortOrder_Enum SortOrder { get; set; }

        /// <summary>Checkin date</summary>
        [JsonProperty("checkinDate", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset CheckinDate { get; set; }

        /// <summary>Checkout date</summary>
        [JsonProperty("checkoutDate", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset CheckoutDate { get; set; }

        /// <summary>Assigned Type: c-0600:NumberDoubleDigit</summary>
        [JsonProperty("numberOfGuests", Required = Required.Always)]
        public int NumberOfGuests { get; set; }

        /// <summary>Maximum properties to return for this request</summary>
        [JsonProperty("maxProperties", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? MaxProperties { get; set; }

        /// <summary>Number of properties to return per page</summary>
        [JsonProperty("propertiesPerPage", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? PropertiesPerPage { get; set; }

        /// <summary>Assigned Type: cthp-0500:SearchRadius</summary>
        [JsonProperty("SearchRadius", Required = Required.Always)]
        public SearchRadius SearchRadius { get; set; }

        /// <summary>Assigned Type: cthp-0500:SearchAddress</summary>
        [JsonProperty("SearchAddress", Required = Required.Always)]
        public SearchAddress SearchAddress { get; set; }


    }

    /// <summary>The method to be used in sorting hotel properties</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum HotelSortOrder_Enum
    {
        [EnumMember(Value = @"StarRating")]
        StarRating = 0,

        [EnumMember(Value = @"Proximity")]
        Proximity = 1,

    }

    /// <summary>URL of the image</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Image
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:StringCharacterOne</summary>
        [JsonProperty("dimensionCategory", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1)]
        public string DimensionCategory { get; set; }

        /// <summary>Assigned Type: c-0600:NonnegativeInteger</summary>
        [JsonProperty("width", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? Width { get; set; }

        /// <summary>Assigned Type: c-0600:NonnegativeInteger</summary>
        [JsonProperty("height", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? Height { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("caption", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Caption { get; set; }

        /// <summary>Assigned Type: c-0600:NumberDoubleDigit</summary>
        [JsonProperty("pictureCategory", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? PictureCategory { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class SearchAddress
    {
        /// <summary>City (e.g., Dublin), town, or postal station (i.e., a postal service territory, often used in a military address).</summary>
        [JsonProperty("City", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(128)]
        public string City { get; set; }

        /// <summary>Assigned Type: c-0600:StateProv</summary>
        [JsonProperty("StateProv", Required = Required.Always)]
        public StateProv StateProv { get; set; }

        /// <summary>Country Code</summary>
        [JsonProperty("Country", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(128)]
        public string Country { get; set; }


    }

    /// <summary>A search radius</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class SearchRadius
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Value { get; set; }

        /// <summary>Miles, Kilometers, etc.</summary>
        [JsonProperty("unitOfDistance", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public UnitOfDistance? UnitOfDistance { get; set; }


    }

    /// <summary>code</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OTA_CodeWithDescription
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Description { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropertyMediaRequest
    {
        [JsonProperty("PropertyKey", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(10)]
        public ICollection<PropertyKey> PropertyKey { get; set; } = new Collection<PropertyKey>();


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropertyKey
    {
        /// <summary>Assigned Type: c-0600:SupplierCode</summary>
        [JsonProperty("chainCode", Required = Required.Always)]
        [Required]
        [StringLength(5, MinimumLength = 2)]
        [RegularExpression(@"([a-zA-Z0-9]{2,5})")]
        public string ChainCode { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("propertyCode", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(32)]
        public string PropertyCode { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("Properties", typeof(Properties))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropertiesID
    {
        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropsList
    {
        public Properties Properties { get; set; }
    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Properties : PropertiesID
    {
        /// <summary>Total number or properties returned for the request</summary>
        [JsonProperty("totalProperties", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? TotalProperties { get; set; }

        /// <summary>Number of properties per page</summary>
        [JsonProperty("propertiesPerPage", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? PropertiesPerPage { get; set; }

        [JsonProperty("PropertyInfo", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(1000)]
        public ICollection<PropertyInfo> PropertyInfo { get; set; } = new Collection<PropertyInfo>();

        /// <summary>Assigned Type: c-0600:BaseResponse_Summary</summary>
        [JsonProperty("BaseResponse", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public BaseResponse BaseResponse { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps_Summary</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropertiesQueryPropertyList
    {
        /// <summary>Assigned Type: cthp-0500:PropertiesRequest</summary>
        [JsonProperty("PropertiesRequest", Required = Required.Always)]
        public PropertiesRequest PropertiesRequest { get; set; }


    }

    /// <summary>Filter options that apply to a property in this list</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("PropertyInfo", typeof(PropertyInfo))]
    [JsonInheritanceAttribute("PropertyInfoDetail", typeof(PropertyInfoDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropertyInfoID
    {
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }


    }

    /// <summary>Filter options that apply to a property in this list</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropertyInfo : PropertyInfoID
    {
        /// <summary>Identifies the availability status of an item.</summary>
        [JsonProperty("availability", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public AvailabilityStatus_Enum? Availability { get; set; }

        /// <summary>Defines the distance between two points.</summary>
        [JsonProperty("distance", Required = Required.Always)]
        public double Distance { get; set; }

        /// <summary>Assigned Type: cthp-0500:Property</summary>
        [JsonProperty("Property", Required = Required.Always)]
        public Property Property { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("LowestAvailableRate", Required = Required.Always)]
        public CurrencyAmount LowestAvailableRate { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Filter options that apply to a property in this list</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropertyInfoDetail : PropertyInfo
    {
        /// <summary>Assigned Type: c-0600:BaseResponse_Summary</summary>
        [JsonProperty("BaseResponse", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public BaseResponse BaseResponse { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Service
    {
        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string Code { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Description { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("proximityCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string ProximityCode { get; set; }

        /// <summary>If present and true this service exists</summary>
        [JsonProperty("existsInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ExistsInd { get; set; }

        /// <summary>If present and true this service is included with no charge</summary>
        [JsonProperty("includedInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? IncludedInd { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuestRoomInfo
    {
        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string Code { get; set; }

        [JsonProperty("number", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Number { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Description { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ExtensionPoint
    {

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ExtensionPoint_Summary
    {

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ExtensionPoint_Detail
    {

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ExtensionPoint_Custom
    {

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ExtensionPoint_Query
    {

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ExtensionPoint_Update
    {

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ExtensionPoint_Shared
    {

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ExtensionPoint_Choice
    {

    }

    /// <summary>Miles, Kilometers, etc.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum UnitOfDistance2
    {
        [EnumMember(Value = @"Miles")]
        Miles = 0,

        [EnumMember(Value = @"Kilometers")]
        Kilometers = 1,

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    [System.AttributeUsage(System.AttributeTargets.Class, AllowMultiple = true)]
    internal class JsonInheritanceAttribute : System.Attribute
    {
        public JsonInheritanceAttribute(string key, System.Type type)
        {
            Key = key;
            Type = type;
        }

        public string Key { get; }

        public System.Type Type { get; }
    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    internal class JsonInheritanceConverter : JsonConverter
    {
        internal static readonly string DefaultDiscriminatorName = "discriminator";

        private readonly string _discriminator;

        [System.ThreadStatic]
        private static bool _isReading;

        [System.ThreadStatic]
        private static bool _isWriting;

        public JsonInheritanceConverter()
        {
            _discriminator = DefaultDiscriminatorName;
        }

        public JsonInheritanceConverter(string discriminator)
        {
            _discriminator = discriminator;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            try
            {
                _isWriting = true;

                var jObject = JObject.FromObject(value, serializer);
                jObject.AddFirst(new JProperty(_discriminator, GetSubtypeDiscriminator(value.GetType())));
                writer.WriteToken(jObject.CreateReader());
            }
            finally
            {
                _isWriting = false;
            }
        }

        public override bool CanWrite
        {
            get
            {
                if (_isWriting)
                {
                    _isWriting = false;
                    return false;
                }
                return true;
            }
        }

        public override bool CanRead
        {
            get
            {
                if (_isReading)
                {
                    _isReading = false;
                    return false;
                }
                return true;
            }
        }

        public override bool CanConvert(System.Type objectType)
        {
            return true;
        }

        public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jObject = serializer.Deserialize<JObject>(reader);
            if (jObject == null)
                return null;

            var discriminator = Extensions.Value<string>(jObject.GetValue(_discriminator));
            var subtype = GetObjectSubtype(objectType, discriminator);

            var objectContract = serializer.ContractResolver.ResolveContract(subtype) as JsonObjectContract;
            if (objectContract == null || Enumerable.All(objectContract.Properties, p => p.PropertyName != _discriminator))
            {
                jObject.Remove(_discriminator);
            }

            try
            {
                _isReading = true;
                return serializer.Deserialize(jObject.CreateReader(), subtype);
            }
            finally
            {
                _isReading = false;
            }
        }

        private System.Type GetObjectSubtype(System.Type objectType, string discriminator)
        {
            foreach (var attribute in CustomAttributeExtensions.GetCustomAttributes<JsonInheritanceAttribute>(IntrospectionExtensions.GetTypeInfo(objectType), true))
            {
                if (attribute.Key == discriminator)
                    return attribute.Type;
            }

            return objectType;
        }

        private string GetSubtypeDiscriminator(System.Type objectType)
        {
            foreach (var attribute in CustomAttributeExtensions.GetCustomAttributes<JsonInheritanceAttribute>(IntrospectionExtensions.GetTypeInfo(objectType), true))
            {
                if (attribute.Type == objectType)
                    return attribute.Key;
            }

            return objectType.Name;
        }
    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    internal class DateFormatConverter : IsoDateTimeConverter
    {
        public DateFormatConverter()
        {
            DateTimeFormat = "yyyy-MM-dd";
        }
    }

    [GeneratedCode("NSwag", "13.0.6.0 (NJsonSchema v10.0.23.0 (Newtonsoft.Json v11.0.0.0))")]
    public partial class ApiException : System.Exception
    {
        public int StatusCode { get; private set; }

        public string Response { get; private set; }

        public IReadOnlyDictionary<string, IEnumerable<string>> Headers { get; private set; }

        public ApiException(string message, int statusCode, string response, IReadOnlyDictionary<string, IEnumerable<string>> headers, System.Exception innerException)
            : base(message + "\n\nStatus: " + statusCode + "\nResponse: \n" + response.Substring(0, response.Length >= 512 ? 512 : response.Length), innerException)
        {
            StatusCode = statusCode;
            Response = response;
            Headers = headers;
        }

        public override string ToString()
        {
            return string.Format("HTTP Response: \n\n{0}\n\n{1}", Response, base.ToString());
        }
    }

    [GeneratedCode("NSwag", "13.0.6.0 (NJsonSchema v10.0.23.0 (Newtonsoft.Json v11.0.0.0))")]
    public partial class ApiException<TResult> : ApiException
    {
        public TResult Result { get; private set; }

        public ApiException(string message, int statusCode, string response, IReadOnlyDictionary<string, IEnumerable<string>> headers, TResult result, System.Exception innerException)
            : base(message, statusCode, response, headers, innerException)
        {
            Result = result;
        }
    }

    
    /// <summary>
    /// /////////////////////////////////////// Hotel Availability //////////////////////////////////////////////////////////
    /// </summary>

    /// <summary>A product or products available at the given price and terms</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("Offer", typeof(Offer))]
    [JsonInheritanceAttribute("OfferDetail", typeof(OfferDetail))]
    [JsonInheritanceAttribute("OfferConfirmed", typeof(OfferConfirmed))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OfferID
    {
        /// <summary>Internally referenced xml id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Used to reference another instance of this object</summary>
        [JsonProperty("offerRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string OfferRef { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }


    }

    /// <summary>A product or products available at the given price and terms</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Offer : OfferID
    {
        [JsonProperty("Products", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(1000)]
        public List<Products> Products { get; set; } = new List<Products>();

        /// <summary>Assigned Type: ctlg-1000:Price</summary>
        [JsonProperty("Price", Required = Required.Always)]
        public PriceHospitality Price { get; set; }

        [JsonProperty("TermsAndConditions", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(25)]
        public List<TermsAndConditionsHospitality> TermsAndConditions { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps_Summary</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>A product or products available at the given price and terms</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OfferDetail : Offer
    {
        /// <summary>Assigned Type: ctlg-1000:PointOfSale</summary>
        [JsonProperty("PointOfSale", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PointOfSale PointOfSale { get; set; }

        /// <summary>Assigned Type: c-0600:Company_Name</summary>
        [JsonProperty("Vendor", Required = Required.Always)]
        public Company_Name Vendor { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>TEMPORARY - THIS WILL EVENTUALLY COME FROM IDENTITY MANAGEMENT</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("PointOfSaleDetail", typeof(PointOfSaleDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PointOfSale
    {
        /// <summary>Assigned Type: c-0600:PseudoCityCode</summary>
        [JsonProperty("pseudoCityCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(10, MinimumLength = 2)]
        [RegularExpression(@"([a-zA-Z0-9]{2,10})")]
        public string PseudoCityCode { get; set; }

        /// <summary>Assigned Type: c-0600:SupplierCode</summary>
        [JsonProperty("providerCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(5, MinimumLength = 2)]
        [RegularExpression(@"([a-zA-Z0-9]{2,5})")]
        public string ProviderCode { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("agentSignOn", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string AgentSignOn { get; set; }

        [JsonProperty("erspUserID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? ErspUserID { get; set; }

        /// <summary>Assigned Type: c-0600:SupplierCode</summary>
        [JsonProperty("airlineVendor", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(5, MinimumLength = 2)]
        [RegularExpression(@"([a-zA-Z0-9]{2,5})")]
        public string AirlineVendor { get; set; }

        /// <summary>Assigned Type: c-0600:CountryCode_ISO3166</summary>
        [JsonProperty("country", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{2}")]
        public string Country { get; set; }

        /// <summary>Assigned Type: c-0600:AgencyCode_IATA</summary>
        [JsonProperty("agencyCode_IATA", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"([0-9]{8})")]
        public string AgencyCode_IATA { get; set; }

        /// <summary>Assigned Type: c-0600:Currency</summary>
        [JsonProperty("DefaultCurrency", Required = Required.Always)]
        public Currency DefaultCurrency { get; set; }

        /// <summary>Assigned Type: c-0600:AirportOrCity</summary>
        [JsonProperty("AgencyCity", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public AirportOrCity AgencyCity { get; set; }

        /// <summary>A positive integer with the inclusion of zero</summary>
        [JsonProperty("TierLevel", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? TierLevel { get; set; }

        /// <summary>If true, the actual PCC is replaced with the PCC specified</summary>
        [JsonProperty("overridePCCInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? OverridePCCInd { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>TEMPORARY - THIS WILL EVENTUALLY COME FROM IDENTITY MANAGEMENT</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PointOfSaleDetail : PointOfSale
    {
        /// <summary>Assigned Type: ctlg-1000:POS_BookingChannel</summary>
        [JsonProperty("POS_BookingChannel", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public POS_BookingChannelID POS_BookingChannel { get; set; }

        /// <summary>Assigned Type: ctlg-1000:POS_Requestor</summary>
        [JsonProperty("POS_Requestor", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public POS_RequestorID POS_Requestor { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>TEMPORARY</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("POS_BookingChannel", typeof(POS_BookingChannel))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class POS_BookingChannelID
    {
        [JsonProperty("ID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ID { get; set; }


    }

    /// <summary>TEMPORARY</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class POS_BookingChannel : POS_BookingChannelID
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("type", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Type { get; set; }

        /// <summary>A short string  \nMinimum length of 0 and a maximum length of 128</summary>
        [JsonProperty("CompanyName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string CompanyName { get; set; }

        /// <summary>A tiny string  \nMinimum length of 0 and a maximum length of 32</summary>
        [JsonProperty("CompanyShortName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string CompanyShortName { get; set; }

        /// <summary>Assigned Type: c-0600:Code</summary>
        [JsonProperty("Code", Required = Required.Always)]
        public Code Code { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>TEMPORARY</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("POS_Requestor", typeof(POS_Requestor))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class POS_RequestorID
    {
        [JsonProperty("ID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ID { get; set; }


    }

    /// <summary>TEMPORARY</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class POS_Requestor : POS_RequestorID
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("type", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Type { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("requestorID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string RequestorID { get; set; }

        /// <summary>A short string  \nMinimum length of 0 and a maximum length of 128</summary>
        [JsonProperty("CompanyName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string CompanyName { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>TODO</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("TermsAndConditions", typeof(TermsAndConditions))]
    [JsonInheritanceAttribute("TermsAndConditionsHospitality", typeof(TermsAndConditionsHospitality))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TermsAndConditionsID
    {
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }


    }

    /// <summary>TODO</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TermsAndConditions : TermsAndConditionsID
    {
        [JsonProperty("TextBlock", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public List<TextBlock> TextBlock { get; set; }

        /// <summary>Assigned Type: c-0600:DateTimeRange</summary>
        [JsonProperty("ValidDateTimeRange", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeRange ValidDateTimeRange { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Products
    {
        [JsonProperty("Product", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(100)]
        public List<ProductHospitality> Product { get; set; } = new List<ProductHospitality>();

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>A product is any product, service or package of products and services  that can be priced and purchased by a specific supplier.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("Product", typeof(Product))]
    [JsonInheritanceAttribute("ProductHospitality", typeof(ProductHospitality))]
    [JsonInheritanceAttribute("ProductHospitalityOffer", typeof(ProductHospitalityOffer))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ProductID
    {
        /// <summary>Local indentifier withing a given message for this product.</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Used to reference another instance of this object</summary>
        [JsonProperty("productRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ProductRef { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }

        /// <summary>Assigned Type: c-0600:SupplierLocator</summary>
        [JsonProperty("SupplierLocator", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public SupplierLocator SupplierLocator { get; set; }


    }

    /// <summary>A product is any product, service or package of products and services  that can be priced and purchased by a specific supplier.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Product : ProductID
    {
        /// <summary>Assigned Type: c-0600:NonnegativeInteger</summary>
        [JsonProperty("quantityAvailable", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? QuantityAvailable { get; set; }

        /// <summary>Assigned Type: c-0600:NonnegativeInteger</summary>
        [JsonProperty("quantityRequested", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? QuantityRequested { get; set; }

        /// <summary>Assigned Type: c-0600:Company_Name</summary>
        [JsonProperty("Vendor", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Company_Name Vendor { get; set; }

        /// <summary>Textual description of the product</summary>
        [JsonProperty("Description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1024)]
        public string Description { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps_Summary</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }

        /// <summary>Assigned Type: c-0600:Status</summary>
        [JsonProperty("Status", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Status Status { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>The price of this offer, along with rate and other information used in calculating it</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("Price", typeof(Price))]
    [JsonInheritanceAttribute("PriceHospitality", typeof(PriceHospitality))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PriceID
    {
        /// <summary>Internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }


    }

    /// <summary>The price of this offer, along with rate and other information used in calculating it</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Price : PriceID
    {
        /// <summary>The quantity of product for which this price applies - optional, implies 1</summary>
        [JsonProperty("quantity", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Quantity { get; set; }

        [JsonProperty("TotalPrice", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(3)]
        public ICollection<TotalPrice> TotalPrice { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps_Summary</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Identifies a company by name.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Company_Name
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Value { get; set; }

        /// <summary>Use this id to internally identify this company in NextSteps</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("division", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Division { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("department", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Department { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("shortName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string ShortName { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Code { get; set; }

        /// <summary>Assigned Type: c-0600:CodeContext</summary>
        [JsonProperty("codeContext", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string CodeContext { get; set; }

        /// <summary>Assigned Type: c-0600:SystemOfRecord</summary>
        [JsonProperty("systemOfRecord", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<string> SystemOfRecord { get; set; }


    }

    /// <summary>Any code used to specify an item, for example a type of traveler, service code, room amenity, etc.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Code
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:CodeContext</summary>
        [JsonProperty("codeContext", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string CodeContext { get; set; }


    }

    /// <summary>Contains the locator (PNR or external locator) or cancellation number for the reservation, order, or offer</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Locator
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(16)]
        [RegularExpression(@"([A-Z0-9]+)?")]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("type", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Type { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("source", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Source { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("sourceContext", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string SourceContext { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("otaType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string OtaType { get; set; }


    }

    /// <summary>Provides text and indicates whether it is formatted or not.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TextFormatted
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string Value { get; set; }

        /// <summary>The language in which the text is provided.</summary>
        [JsonProperty("language", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Language { get; set; }

        /// <summary>Indicates the format of text used in the description e.g. plain text or html.</summary>
        [JsonProperty("textFormat", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public TextFormat_Enum? TextFormat { get; set; }


    }

    /// <summary>Specifies the begin and end date of an event</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class DateRange
    {
        /// <summary>Specifies the start date for an event, such as a booking</summary>
        [JsonProperty("start", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset Start { get; set; }

        /// <summary>Specifies the end date an event, such as a booking</summary>
        [JsonProperty("end", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset End { get; set; }


    }

    /// <summary>Describes the format of text such as plain text or html</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum TextFormat_Enum
    {
        [EnumMember(Value = @"PlainText")]
        PlainText = 0,

        [EnumMember(Value = @"HTML")]
        HTML = 1,

    }

    /// <summary>Time stamp of the creation.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class DateCreateModify
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? Value { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("creatorID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string CreatorID { get; set; }

        /// <summary>Time stamp of last modification.</summary>
        [JsonProperty("lastModify", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? LastModify { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("lastModifierID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string LastModifierID { get; set; }

        /// <summary>Date an item will be purged from a system of record</summary>
        [JsonProperty("purge", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? Purge { get; set; }


    }

    /// <summary>Describes a range of dates between the start and end date times, inclusive of the start and end dates.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class DateTimeRange
    {
        [JsonProperty("start", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? Start { get; set; }

        [JsonProperty("end", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? End { get; set; }


    }

    /// <summary>Travelport normalized status message with the detail facet containing the mapped host provider codes</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("StatusDetail", typeof(StatusDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Status
    {
        /// <summary>Assigned Type: c-0600:Status_Travelport</summary>
        [JsonProperty("TravelportStatus", Required = Required.Always)]
        public Status_Travelport TravelportStatus { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Travelport normalized status message with the detail facet containing the mapped host provider codes</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class StatusDetail : Status
    {
        /// <summary>Assigned Type: c-0600:Status_Provider</summary>
        [JsonProperty("ProviderStatus", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Status_Provider ProviderStatus { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>The normalized enterprise status description</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Status_Travelport
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("resource", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Resource { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("action", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Action { get; set; }


    }

    /// <summary>The status description, code and function as used by the originating system</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Status_Provider
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("providerCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string ProviderCode { get; set; }

        /// <summary>Assigned Type: c-0600:CodeContext</summary>
        [JsonProperty("provider", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Provider { get; set; }


    }

    /// <summary>Descriptive text</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TextTitleAndDescription
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:Languages</summary>
        [JsonProperty("languages", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<string> Languages { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("title", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Title { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class DateOrDateWindows
    {
        /// <summary>A specific date. When used with a windows must fall between start and end.</summary>
        [JsonProperty("specific", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? Specific { get; set; }

        /// <summary>The earliest and latest dates acceptable for the start date.</summary>
        [JsonProperty("start", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? Start { get; set; }

        /// <summary>The earliest and latest dates acceptable for the end date.</summary>
        [JsonProperty("end", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? End { get; set; }

        /// <summary>Duration from  start date. \nThe duration is one of year, month, day and time values representing a single duration of time.</summary>
        [JsonProperty("duration", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Duration { get; set; }

        /// <summary>The unit of elapsed time or the day of the week applied to the value.</summary>
        [JsonProperty("durationUnit", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public DurationUnit_Enum? DurationUnit { get; set; }


    }

    /// <summary>Defines the 'Units' that can be applied to Stay restrictions.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum DurationUnit_Enum
    {
        [EnumMember(Value = @"Minutes")]
        Minutes = 0,

        [EnumMember(Value = @"Hours")]
        Hours = 1,

        [EnumMember(Value = @"Days")]
        Days = 2,

        [EnumMember(Value = @"Months")]
        Months = 3,

        [EnumMember(Value = @"MON")]
        MON = 4,

        [EnumMember(Value = @"TUES")]
        TUES = 5,

        [EnumMember(Value = @"WED")]
        WED = 6,

        [EnumMember(Value = @"THU")]
        THU = 7,

        [EnumMember(Value = @"FRI")]
        FRI = 8,

        [EnumMember(Value = @"SAT")]
        SAT = 9,

        [EnumMember(Value = @"SUN")]
        SUN = 10,

    }

    /// <summary>The supplier and the supplier's locator code for a product</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class SupplierLocator
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(16)]
        [RegularExpression(@"([A-Z0-9]+)?")]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:SupplierCode</summary>
        [JsonProperty("supplierCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(5, MinimumLength = 2)]
        [RegularExpression(@"([a-zA-Z0-9]{2,5})")]
        public string SupplierCode { get; set; }

        /// <summary>Assigned Type: c-0600:StringLittle</summary>
        [JsonProperty("supplierName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(64)]
        public string SupplierName { get; set; }


    }

    /// <summary>The provider and the provider's locator code for an offer</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ProviderLocator
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(16)]
        [RegularExpression(@"([A-Z0-9]+)?")]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:SupplierCode</summary>
        [JsonProperty("providerCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(5, MinimumLength = 2)]
        [RegularExpression(@"([a-zA-Z0-9]{2,5})")]
        public string ProviderCode { get; set; }

        /// <summary>Assigned Type: c-0600:StringLittle</summary>
        [JsonProperty("providerName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(64)]
        public string ProviderName { get; set; }


    }

    /// <summary>Airport or city code of the customer embarkation.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AirportOrCity
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(3, MinimumLength = 3)]
        [RegularExpression(@"([a-zA-Z]{3})")]
        public string Value { get; set; }

        /// <summary>Treat as specific airport or expand to multi-airport city.</summary>
        [JsonProperty("cityOrAirport", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public CityAirportTreatment_Enum? CityOrAirport { get; set; }


    }

    /// <summary>City\/Airport treatment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum CityAirportTreatment_Enum
    {
        [EnumMember(Value = @"Airport Only")]
        Airport_Only = 0,

        [EnumMember(Value = @"City or Airport")]
        City_or_Airport = 1,

        [EnumMember(Value = @"City Only")]
        City_Only = 2,

        [EnumMember(Value = @"Use Default")]
        Use_Default = 3,

    }

    /// <summary>The default currency that will apply to all prices, amounts, fares, etc. in a message.  It is placed at the top of any message using this object.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Currency
    {
        /// <summary>Assigned Type: c-0600:CurrencyCode_ISO</summary>
        [JsonProperty("code", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [RegularExpression(@"[a-zA-Z]{3}")]
        public string Code { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyMinorUnit</summary>
        [JsonProperty("minorUnit", Required = Required.Always)]
        [Range(0, int.MaxValue)]
        public int MinorUnit { get; set; }


    }

    /// <summary>The GDS or API system that is the provider</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum CoreSystem_Enum
    {
        [EnumMember(Value = @"1G")]
        _1G = 0,

        [EnumMember(Value = @"1V")]
        _1V = 1,

        [EnumMember(Value = @"1P")]
        _1P = 2,

        [EnumMember(Value = @"1J")]
        _1J = 3,

        [EnumMember(Value = @"ACH")]
        ACH = 4,

        [EnumMember(Value = @"ACS")]
        ACS = 5,

        [EnumMember(Value = @"RCH")]
        RCH = 6,

    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("ReferenceListTextBlock", typeof(ReferenceListTextBlock))]
    [JsonInheritanceAttribute("ReferenceListPropertyDates", typeof(ReferenceListPropertyDates))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ReferenceList
    {
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("ExtensionPoint_Shared", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Shared ExtensionPoint_Shared { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ReferenceListTextBlock : ReferenceList
    {
        [JsonProperty("TextBlock", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<TextBlock> TextBlock { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Amount
    {
        /// <summary>Source of the selection of this currenct</summary>
        [JsonProperty("currencySource", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public CurrencySource_Enum? CurrencySource { get; set; }

        /// <summary>Assigned Type: c-0600:Currency</summary>
        [JsonProperty("Currency", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Currency Currency { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Base", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount Base { get; set; }

        /// <summary>Assigned Type: c-0600:Taxes</summary>
        [JsonProperty("Taxes", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Taxes Taxes { get; set; }

        /// <summary>Assigned Type: c-0600:Fees</summary>
        [JsonProperty("Fees", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Fees Fees { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Total", Required = Required.Always)]
        public CurrencyAmount Total { get; set; }

        /// <summary>True indicates the price amounts are a result or a reprice request</summary>
        [JsonProperty("repriceInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? RepriceInd { get; set; }

        /// <summary>True if this amount has been converted from the original amount</summary>
        [JsonProperty("approximateInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ApproximateInd { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TotalPrice
    {
        /// <summary>Source of the selection of this currenct</summary>
        [JsonProperty("currencySource", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public CurrencySource_Enum? CurrencySource { get; set; }

        /// <summary>Assigned Type: c-0600:Currency</summary>
        [JsonProperty("Currency", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Currency Currency { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Base", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount Base { get; set; }

        /// <summary>Assigned Type: c-0600:Taxes</summary>
        [JsonProperty("Taxes", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Taxes Taxes { get; set; }

        /// <summary>Assigned Type: c-0600:Fees</summary>
        [JsonProperty("Fees", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Fees Fees { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Total", Required = Required.Always)]
        public CurrencyAmount Total { get; set; }

        /// <summary>True indicates the price amounts are a result or a reprice request</summary>
        [JsonProperty("repriceInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? RepriceInd { get; set; }

        /// <summary>True if this amount has been converted from the original amount</summary>
        [JsonProperty("approximateInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ApproximateInd { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class SegmentPrice
    {
        /// <summary>Source of the selection of this currenct</summary>
        [JsonProperty("currencySource", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public CurrencySource_Enum? CurrencySource { get; set; }

        /// <summary>Assigned Type: c-0600:Currency</summary>
        [JsonProperty("Currency", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Currency Currency { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Base", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount Base { get; set; }

        /// <summary>Assigned Type: c-0600:Taxes</summary>
        [JsonProperty("Taxes", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Taxes Taxes { get; set; }

        /// <summary>Assigned Type: c-0600:Fees</summary>
        [JsonProperty("Fees", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Fees Fees { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Total", Required = Required.Always)]
        public CurrencyAmount Total { get; set; }

        /// <summary>True indicates the price amounts are a result or a reprice request</summary>
        [JsonProperty("repriceInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? RepriceInd { get; set; }

        /// <summary>True if this amount has been converted from the original amount</summary>
        [JsonProperty("approximateInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ApproximateInd { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelerPrice
    {
        /// <summary>Source of the selection of this currenct</summary>
        [JsonProperty("currencySource", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public CurrencySource_Enum? CurrencySource { get; set; }

        /// <summary>Assigned Type: c-0600:Currency</summary>
        [JsonProperty("Currency", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Currency Currency { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Base", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount Base { get; set; }

        /// <summary>Assigned Type: c-0600:Taxes</summary>
        [JsonProperty("Taxes", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Taxes Taxes { get; set; }

        /// <summary>Assigned Type: c-0600:Fees</summary>
        [JsonProperty("Fees", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Fees Fees { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Total", Required = Required.Always)]
        public CurrencyAmount Total { get; set; }

        /// <summary>True indicates the price amounts are a result or a reprice request</summary>
        [JsonProperty("repriceInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? RepriceInd { get; set; }

        /// <summary>True if this amount has been converted from the original amount</summary>
        [JsonProperty("approximateInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ApproximateInd { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ProductPrice
    {
        /// <summary>Source of the selection of this currenct</summary>
        [JsonProperty("currencySource", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public CurrencySource_Enum? CurrencySource { get; set; }

        /// <summary>Assigned Type: c-0600:Currency</summary>
        [JsonProperty("Currency", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Currency Currency { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Base", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount Base { get; set; }

        /// <summary>Assigned Type: c-0600:Taxes</summary>
        [JsonProperty("Taxes", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Taxes Taxes { get; set; }

        /// <summary>Assigned Type: c-0600:Fees</summary>
        [JsonProperty("Fees", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Fees Fees { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Total", Required = Required.Always)]
        public CurrencyAmount Total { get; set; }

        /// <summary>True indicates the price amounts are a result or a reprice request</summary>
        [JsonProperty("repriceInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? RepriceInd { get; set; }

        /// <summary>True if this amount has been converted from the original amount</summary>
        [JsonProperty("approximateInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ApproximateInd { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>The fee amount with feecode and reporting informtion</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Fee
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public double? Value { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyCode_ISO</summary>
        [JsonProperty("code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{3}")]
        public string Code { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyMinorUnit</summary>
        [JsonProperty("minorUnit", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? MinorUnit { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("feeCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string FeeCode { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("reportingAuthority", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string ReportingAuthority { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("purpose", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Purpose { get; set; }

        /// <summary>Assigned Type: c-0600:StringLong</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string Description { get; set; }


    }

    /// <summary>List of taxes</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("FeesDetail", typeof(FeesDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Fees
    {
        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("TotalFees", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount TotalFees { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>List of taxes</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class FeesDetail : Fees
    {
        [JsonProperty("Fee", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<Fee> Fee { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>The tax amount with tax code and reporting informtion</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Tax
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public double? Value { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyCode_ISO</summary>
        [JsonProperty("code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{3}")]
        public string Code { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyMinorUnit</summary>
        [JsonProperty("minorUnit", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? MinorUnit { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("taxCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string TaxCode { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("reportingAuthority", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string ReportingAuthority { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("purpose", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Purpose { get; set; }

        /// <summary>Assigned Type: c-0600:StringLong</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string Description { get; set; }


    }

    /// <summary>List of taxes</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("TaxesDetail", typeof(TaxesDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Taxes
    {
        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("TotalTaxes", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount TotalTaxes { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>List of taxes</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TaxesDetail : Taxes
    {
        [JsonProperty("Tax", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<Tax> Tax { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>TODO: Document standard \/ A Pseudo City Code.  2 to 10 letters.  IATA? - validate - TIDS</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PseudoCityInfo
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(10, MinimumLength = 2)]
        [RegularExpression(@"([a-zA-Z0-9]{2,10})")]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:SupplierCode</summary>
        [JsonProperty("providerCode", Required = Required.Always)]
        [Required]
        [StringLength(5, MinimumLength = 2)]
        [RegularExpression(@"([a-zA-Z0-9]{2,5})")]
        public string ProviderCode { get; set; }


    }

    /// <summary>DRAFT - Pending approval</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("CommissionAmount", typeof(CommissionAmount))]
    [JsonInheritanceAttribute("CommissionPercent", typeof(CommissionPercent))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Commission
    {
        //[JsonProperty("type", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        //[JsonConverter(typeof(StringEnumConverter))]
        //public Commission_Enum? Type { get; set; }

        [JsonProperty("ExtensionPoint_Shared", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Shared ExtensionPoint_Shared { get; set; }


    }

    /// <summary>DRAFT - Pending approval</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("RefundedCommissionAmount", typeof(RefundedCommissionAmount))]
    [JsonInheritanceAttribute("RefundedCommissionPercent", typeof(RefundedCommissionPercent))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RefundedCommission
    {
        [JsonProperty("type", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Commission_Enum? Type { get; set; }

        [JsonProperty("ExtensionPoint_Shared", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Shared ExtensionPoint_Shared { get; set; }


    }

    /// <summary>DRAFT - Pending approval</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("EarnedCommissionAmount", typeof(EarnedCommissionAmount))]
    [JsonInheritanceAttribute("EarnedCommissionPercent", typeof(EarnedCommissionPercent))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class EarnedCommission
    {
        [JsonProperty("type", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Commission_Enum? Type { get; set; }

        [JsonProperty("ExtensionPoint_Shared", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Shared ExtensionPoint_Shared { get; set; }


    }

    /// <summary>DRAFT - Pending approval</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class CommissionAmount : Commission
    {
        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Amount", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount Amount { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>DRAFT - Pending approval</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RefundedCommissionAmount : RefundedCommission
    {
        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Amount", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount Amount { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>DRAFT - Pending approval</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class EarnedCommissionAmount : EarnedCommission
    {
        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Amount", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount Amount { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>DRAFT - Pending approval</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class CommissionPercent : Commission
    {
        /// <summary>A decimal number that indicated percentage.</summary>
        [JsonProperty("Percent", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0D, int.MaxValue)]
        public double? Percent { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>DRAFT - Pending approval</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RefundedCommissionPercent : RefundedCommission
    {
        /// <summary>A decimal number that indicated percentage.</summary>
        [JsonProperty("Percent", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0D, int.MaxValue)]
        public double? Percent { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>DRAFT - Pending approval</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class EarnedCommissionPercent : EarnedCommission
    {
        /// <summary>A decimal number that indicated percentage.</summary>
        [JsonProperty("Percent", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0D, int.MaxValue)]
        public double? Percent { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum Commission_Enum
    {
        [EnumMember(Value = @"Full")]
        Full = 0,

        [EnumMember(Value = @"Partial")]
        Partial = 1,

        [EnumMember(Value = @"Non-paying")]
        NonPaying = 2,

        [EnumMember(Value = @"No-show")]
        NoShow = 3,

        [EnumMember(Value = @"Adjustment")]
        Adjustment = 4,

        [EnumMember(Value = @"Commissionable")]
        Commissionable = 5,

    }

    /// <summary>Class created by Praveen</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("TextBlockDetail", typeof(TextBlockDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TextParagraph
    {
        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("title", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Title { get; set; }

        /// <summary>Internally referenced name</summary>
        [JsonProperty("name", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string name { get; set; }

        [JsonProperty("FormattedText", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(50)]
        public List<TextFormatted> FormattedText { get; set; } = new List<TextFormatted>();

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Blocks of text with title, discription, images and URLs, with language translations.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("TextBlockDetail", typeof(TextBlockDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TextBlock
    {
        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("title", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Title { get; set; }

        /// <summary>Internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("TextFormatted", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(50)]
        public List<TextFormatted> TextFormatted { get; set; } = new List<TextFormatted>();

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Blocks of text with title, discription, images and URLs, with language translations.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("BrandTextDetail", typeof(BrandTextDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class BrandText
    {
        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("title", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Title { get; set; }

        /// <summary>Internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("TextFormatted", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(50)]
        public ICollection<TextFormatted> TextFormatted { get; set; } = new Collection<TextFormatted>();

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Blocks of text with title, discription, images and URLs, with language translations.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("RateDescriptionDetail", typeof(RateDescriptionDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RateDescription
    {
        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("title", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Title { get; set; }

        /// <summary>Internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("TextFormatted", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(50)]
        public ICollection<TextFormatted> TextFormatted { get; set; } = new Collection<TextFormatted>();

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Blocks of text with title, discription, images and URLs, with language translations.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("TermsAndConditionsSubCategoryDetail", typeof(TermsAndConditionsSubCategoryDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TermsAndConditionsSubCategory
    {
        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("title", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Title { get; set; }

        /// <summary>Internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("TextFormatted", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(50)]
        public ICollection<TextFormatted> TextFormatted { get; set; } = new Collection<TextFormatted>();

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Blocks of text with title, discription, images and URLs, with language translations.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("BrandFeatureTextDetail", typeof(BrandFeatureTextDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class BrandFeatureText
    {
        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("title", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Title { get; set; }

        /// <summary>Internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("TextFormatted", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(50)]
        public ICollection<TextFormatted> TextFormatted { get; set; } = new Collection<TextFormatted>();

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Blocks of text with title, discription, images and URLs, with language translations.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TextBlockDetail : TextBlock
    {
        /// <summary>The order of the text block, if there are more than one block.</summary>
        [JsonProperty("sequence", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Sequence { get; set; }

        /// <summary>Assigned Type: c-0600:Description</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1024)]
        public string Description { get; set; }

        /// <summary>Assigned Type: c-0600:Image</summary>
        [JsonProperty("Image", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Image Image { get; set; }

        /// <summary>A URL for this block</summary>
        [JsonProperty("URL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string URL { get; set; }

        /// <summary>Assigned Type: c-0600:DateCreateModify</summary>
        [JsonProperty("DateCreateModify", Required = Required.Always)]
        public DateCreateModify DateCreateModify { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Blocks of text with title, discription, images and URLs, with language translations.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class BrandTextDetail : BrandText
    {
        /// <summary>The order of the text block, if there are more than one block.</summary>
        [JsonProperty("sequence", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Sequence { get; set; }

        /// <summary>Assigned Type: c-0600:Description</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1024)]
        public string Description { get; set; }

        /// <summary>Assigned Type: c-0600:Image</summary>
        [JsonProperty("Image", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Image Image { get; set; }

        /// <summary>A URL for this block</summary>
        [JsonProperty("URL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string URL { get; set; }

        /// <summary>Assigned Type: c-0600:DateCreateModify</summary>
        [JsonProperty("DateCreateModify", Required = Required.Always)]
        public DateCreateModify DateCreateModify { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Blocks of text with title, discription, images and URLs, with language translations.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RateDescriptionDetail : RateDescription
    {
        /// <summary>The order of the text block, if there are more than one block.</summary>
        [JsonProperty("sequence", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Sequence { get; set; }

        /// <summary>Assigned Type: c-0600:Description</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1024)]
        public string Description { get; set; }

        /// <summary>Assigned Type: c-0600:Image</summary>
        [JsonProperty("Image", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Image Image { get; set; }

        /// <summary>A URL for this block</summary>
        [JsonProperty("URL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string URL { get; set; }

        /// <summary>Assigned Type: c-0600:DateCreateModify</summary>
        [JsonProperty("DateCreateModify", Required = Required.Always)]
        public DateCreateModify DateCreateModify { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Blocks of text with title, discription, images and URLs, with language translations.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TermsAndConditionsSubCategoryDetail : TermsAndConditionsSubCategory
    {
        /// <summary>The order of the text block, if there are more than one block.</summary>
        [JsonProperty("sequence", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Sequence { get; set; }

        /// <summary>Assigned Type: c-0600:Description</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1024)]
        public string Description { get; set; }

        /// <summary>Assigned Type: c-0600:Image</summary>
        [JsonProperty("Image", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Image Image { get; set; }

        /// <summary>A URL for this block</summary>
        [JsonProperty("URL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string URL { get; set; }

        /// <summary>Assigned Type: c-0600:DateCreateModify</summary>
        [JsonProperty("DateCreateModify", Required = Required.Always)]
        public DateCreateModify DateCreateModify { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Blocks of text with title, discription, images and URLs, with language translations.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class BrandFeatureTextDetail : BrandFeatureText
    {
        /// <summary>The order of the text block, if there are more than one block.</summary>
        [JsonProperty("sequence", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Sequence { get; set; }

        /// <summary>Assigned Type: c-0600:Description</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1024)]
        public string Description { get; set; }

        /// <summary>Assigned Type: c-0600:Image</summary>
        [JsonProperty("Image", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Image Image { get; set; }

        /// <summary>A URL for this block</summary>
        [JsonProperty("URL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string URL { get; set; }

        /// <summary>Assigned Type: c-0600:DateCreateModify</summary>
        [JsonProperty("DateCreateModify", Required = Required.Always)]
        public DateCreateModify DateCreateModify { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>A product or products available at the given price and terms</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OfferConfirmed : Offer
    {
        /// <summary>Assigned Type: c-0600:StringLong</summary>
        [JsonProperty("confirmationId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string ConfirmationId { get; set; }

        /// <summary>To Be DELETED -- Date confirmation is received</summary>
        [JsonProperty("confirmedDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? ConfirmedDate { get; set; }

        /// <summary>To Be DELETED</summary>
        [JsonProperty("Description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1024)]
        public string Description { get; set; }

        /// <summary>Assigned Type: c-0600:ProviderLocator</summary>
        [JsonProperty("ProviderLocator", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ProviderLocator ProviderLocator { get; set; }

        /// <summary>Assigned Type: res-1000:Confirmation</summary>
        [JsonProperty("Confirmation", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ConfirmationID Confirmation { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Options for the effect of the settlement of a cancellation</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum CancelSettlementType_Enum
    {
        [EnumMember(Value = @"Charge")]
        Charge = 0,

        [EnumMember(Value = @"Forfeiture")]
        Forfeiture = 1,

        [EnumMember(Value = @"Refund")]
        Refund = 2,

        [EnumMember(Value = @"Retain")]
        Retain = 3,

    }

    /// <summary>Status returned in a response for a two or more phase commitment process</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum ConfirmationStatus_Enum
    {
        [EnumMember(Value = @"Pending")]
        Pending = 0,

        [EnumMember(Value = @"Confirmed")]
        Confirmed = 1,

        [EnumMember(Value = @"Cancelled")]
        Cancelled = 2,

    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class CancelRule
    {
        /// <summary>Assigned Type: res-1000:CancelSettlementType_Enum</summary>
        [JsonProperty("CancelSettlementType", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(StringEnumConverter))]
        public CancelSettlementType_Enum CancelSettlementType { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("CurrencyAmount", Required = Required.Always)]
        public CurrencyAmount CurrencyAmount { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>NEED TO DELETE ALIAS OF TICKET, AFTER ALL VENDOR LOCATOR DATA HAS BEEN HANDLED - RES TEAM USES THIS IN END TRANSACT, NEEDS TO START USING CONFIRMATION\n\nAny form of confirmation following a transaction, i.e. Refund, Forfeiture, etc.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("Confirmation", typeof(Confirmation))]
    [JsonInheritanceAttribute("ConfirmationDetail", typeof(ConfirmationDetail))]
    [JsonInheritanceAttribute("ConfirmationCancellation", typeof(ConfirmationCancellation))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ConfirmationID
    {
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }


    }

    /// <summary>NEED TO DELETE ALIAS OF TICKET, AFTER ALL VENDOR LOCATOR DATA HAS BEEN HANDLED - RES TEAM USES THIS IN END TRANSACT, NEEDS TO START USING CONFIRMATION\n\nAny form of confirmation following a transaction, i.e. Refund, Forfeiture, etc.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("Ticket", typeof(Ticket))]
    [JsonInheritanceAttribute("TicketDetail", typeof(TicketDetail))]
    [JsonInheritanceAttribute("TicketCancellation", typeof(TicketCancellation))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TicketID
    {
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }


    }

    /// <summary>NEED TO DELETE ALIAS OF TICKET, AFTER ALL VENDOR LOCATOR DATA HAS BEEN HANDLED - RES TEAM USES THIS IN END TRANSACT, NEEDS TO START USING CONFIRMATION\n\nAny form of confirmation following a transaction, i.e. Refund, Forfeiture, etc.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Confirmation : ConfirmationID
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("type", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(32)]
        public string Type { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("number", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(32)]
        public string Number { get; set; }

        /// <summary>Date confirmation is received</summary>
        [JsonProperty("confirmedDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? ConfirmedDate { get; set; }

        /// <summary>Assigned Type: c-0600:Locator</summary>
        [JsonProperty("Locator", Required = Required.Always)]
        public Locator Locator { get; set; }

        /// <summary>Assigned Type: res-1000:ConfirmationStatus_Enum</summary>
        [JsonProperty("ConfirmationStatus", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(StringEnumConverter))]
        public ConfirmationStatus_Enum ConfirmationStatus { get; set; }

        /// <summary>Assigned Type: c-0600:Status_Travelport</summary>
        [JsonProperty("Status", Required = Required.Always)]
        public Status_Travelport Status { get; set; }

        /// <summary>Assigned Type: c-0601:Comments</summary>
        [JsonProperty("Comments", Required = Required.Always)]
        public Comments Comments { get; set; }

        /// <summary>A textual description</summary>
        [JsonProperty("Description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1024)]
        public string Description { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>NEED TO DELETE ALIAS OF TICKET, AFTER ALL VENDOR LOCATOR DATA HAS BEEN HANDLED - RES TEAM USES THIS IN END TRANSACT, NEEDS TO START USING CONFIRMATION\n\nAny form of confirmation following a transaction, i.e. Refund, Forfeiture, etc.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Ticket : TicketID
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("type", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(32)]
        public string Type { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("number", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(32)]
        public string Number { get; set; }

        /// <summary>Date confirmation is received</summary>
        [JsonProperty("confirmedDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? ConfirmedDate { get; set; }

        /// <summary>Assigned Type: c-0600:Locator</summary>
        [JsonProperty("Locator", Required = Required.Always)]
        public Locator Locator { get; set; }

        /// <summary>Assigned Type: res-1000:ConfirmationStatus_Enum</summary>
        [JsonProperty("ConfirmationStatus", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(StringEnumConverter))]
        public ConfirmationStatus_Enum ConfirmationStatus { get; set; }

        /// <summary>Assigned Type: c-0600:Status_Travelport</summary>
        [JsonProperty("Status", Required = Required.Always)]
        public Status_Travelport Status { get; set; }

        /// <summary>Assigned Type: c-0601:Comments</summary>
        [JsonProperty("Comments", Required = Required.Always)]
        public Comments Comments { get; set; }

        /// <summary>A textual description</summary>
        [JsonProperty("Description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1024)]
        public string Description { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>NEED TO DELETE ALIAS OF TICKET, AFTER ALL VENDOR LOCATOR DATA HAS BEEN HANDLED - RES TEAM USES THIS IN END TRANSACT, NEEDS TO START USING CONFIRMATION\n\nAny form of confirmation following a transaction, i.e. Refund, Forfeiture, etc.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ConfirmationDetail : Confirmation
    {
        /// <summary>Assigned Type: res-1000:TicketType</summary>
        [JsonProperty("TicketType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public TicketType TicketType { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>NEED TO DELETE ALIAS OF TICKET, AFTER ALL VENDOR LOCATOR DATA HAS BEEN HANDLED - RES TEAM USES THIS IN END TRANSACT, NEEDS TO START USING CONFIRMATION\n\nAny form of confirmation following a transaction, i.e. Refund, Forfeiture, etc.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TicketDetail : TicketID
    {
        /// <summary>Assigned Type: res-1000:TicketType</summary>
        [JsonProperty("TicketType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public TicketType TicketType { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>NEED TO DELETE ALIAS OF TICKET, AFTER ALL VENDOR LOCATOR DATA HAS BEEN HANDLED - RES TEAM USES THIS IN END TRANSACT, NEEDS TO START USING CONFIRMATION\n\nAny form of confirmation following a transaction, i.e. Refund, Forfeiture, etc.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ConfirmationCancellation : Confirmation
    {
        /// <summary>Assigned Type: res-1000:CancelRule</summary>
        [JsonProperty("CancelRule", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CancelRule CancelRule { get; set; }

        /// <summary>Assigned Type: c-0600:Status_Provider</summary>
        [JsonProperty("StatusDetail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Status_Provider StatusDetail { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>NEED TO DELETE ALIAS OF TICKET, AFTER ALL VENDOR LOCATOR DATA HAS BEEN HANDLED - RES TEAM USES THIS IN END TRANSACT, NEEDS TO START USING CONFIRMATION\n\nAny form of confirmation following a transaction, i.e. Refund, Forfeiture, etc.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TicketCancellation : TicketID
    {
        /// <summary>Assigned Type: res-1000:CancelRule</summary>
        [JsonProperty("CancelRule", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CancelRule CancelRule { get; set; }

        /// <summary>Assigned Type: c-0600:Status_Provider</summary>
        [JsonProperty("StatusDetail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Status_Provider StatusDetail { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Data sent in or returned from Galileo for the ticket field</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("TicketTypeTAU", typeof(TicketTypeTAU))]
    [JsonInheritanceAttribute("TicketTypeTAW", typeof(TicketTypeTAW))]
    [JsonInheritanceAttribute("TicketTypeTL", typeof(TicketTypeTL))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TicketType
    {
        /// <summary>Assigned Type: c-0600:StringLittle</summary>
        [JsonProperty("freeText", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(64)]
        public string FreeText { get; set; }

        [JsonProperty("ExtensionPoint_Shared", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Shared ExtensionPoint_Shared { get; set; }


    }

    /// <summary>Data sent in or returned from Galileo for the ticket field</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TicketTypeTAU : TicketType
    {
        /// <summary>Assigned Type: c-0600:AccountCode</summary>
        [JsonProperty("accountCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<string> AccountCode { get; set; }

        [JsonProperty("date", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? Date { get; set; }

        /// <summary>Assigned Type: c-0600:AgencyCode_IATA</summary>
        [JsonProperty("agencyCode_IATA", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"([0-9]{8})")]
        public string AgencyCode_IATA { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>Data sent in or returned from Galileo for the ticket field</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TicketTypeTAW : TicketType
    {
        /// <summary>Assigned Type: c-0600:AccountCode</summary>
        [JsonProperty("accountCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<string> AccountCode { get; set; }

        [JsonProperty("date", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? Date { get; set; }

        /// <summary>Assigned Type: c-0600:AgencyCode_IATA</summary>
        [JsonProperty("agencyCode_IATA", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"([0-9]{8})")]
        public string AgencyCode_IATA { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("queue", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Queue { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>Data sent in or returned from Galileo for the ticket field</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TicketTypeTL : TicketType
    {
        /// <summary>Assigned Type: c-0600:AirlineCode</summary>
        [JsonProperty("airlineCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"([a-zA-Z0-9]{2,3})")]
        public string AirlineCode { get; set; }

        /// <summary>Assigned Type: c-0600:AirportCode_IATA</summary>
        [JsonProperty("airportCode_IATA", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(3, MinimumLength = 3)]
        [RegularExpression(@"([a-zA-Z]{3})")]
        public string AirportCode_IATA { get; set; }

        [JsonProperty("time", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Time { get; set; }

        [JsonProperty("date", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? Date { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>Textual information.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Comment
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string Value { get; set; }

        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("name", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Name { get; set; }

        /// <summary>Identifies the language of the comment.</summary>
        [JsonProperty("language", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Language { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Comments
    {
        [JsonProperty("Comment", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<Comment> Comment { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>A product is any product, service or package of products and services  that can be priced and purchased by a specific supplier.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ProductHospitality : Product
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("bookingCode", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(32)]
        public string BookingCode { get; set; }

        /// <summary>Total number of guests</summary>
        [JsonProperty("guests", Required = Required.Always)]
        public int Guests { get; set; }

        /// <summary>Hotel availability information</summary>
        [JsonProperty("availability", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public AvailabilityStatus_Enum? Availability { get; set; }

        [JsonProperty("adaCompliant", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoUnknown_Enum? AdaCompliant { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("moreRatesToken", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string MoreRatesToken { get; set; }

        /// <summary>Assigned Type: cthp-0600:PropertyKey</summary>
        [JsonProperty("PropertyKey", Required = Required.Always)]
        public PropertyKey PropertyKey { get; set; }

        /// <summary>Assigned Type: cthp-0600:RoomType</summary>
        [JsonProperty("RoomType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public RoomType RoomType { get; set; }

        /// <summary>Assigned Type: c-0600:DateRange</summary>
        [JsonProperty("DateRange", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public DateRange DateRange { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("CancellationID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier CancellationID { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Used to define a room (eg. its location, configuration, view).</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RoomCharacteristics
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("typeCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TypeCode { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("viewCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string ViewCode { get; set; }

        /// <summary>Assigned Type: cthp-0600:OTACodes</summary>
        [JsonProperty("bedTypeCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<string> BedTypeCode { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("category", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string Category { get; set; }

        /// <summary>Smoking allowed (Yes, No , or Unknown)</summary>
        [JsonProperty("smokingAllowed", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoUnknown_Enum? SmokingAllowed { get; set; }

        /// <summary>WiFi Included (Yes, No, Unknown)</summary>
        [JsonProperty("wifiIncluded", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoUnknown_Enum? WifiIncluded { get; set; }

        [JsonProperty("nonSmokingInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? NonSmokingInd { get; set; }


    }

    /// <summary>Hotel room amenities.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RoomAmenity
    {
        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("amenity", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string Amenity { get; set; }

        /// <summary>Assigned Type: c-0600:NonnegativeInteger</summary>
        [JsonProperty("quantity", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? Quantity { get; set; }

        /// <summary>Used to designate the quality level of the RoomAmenity e.g., premium, deluxe, standard, economy.</summary>
        [JsonProperty("qualityLevel", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public RoomAmenityQuality_Enum? QualityLevel { get; set; }


    }

    /// <summary>Provides details regarding rooms, usually guest rooms.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("RoomTypeDetail", typeof(RoomTypeDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RoomType
    {
        /// <summary>Assigned Type: cthp-0600:RoomCharacteristics</summary>
        [JsonProperty("Characteristics", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public RoomCharacteristics Characteristics { get; set; }

        /// <summary>Assigned Type: c-0600:TextTitleAndDescription</summary>
        [JsonProperty("Description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public TextTitleAndDescription Description { get; set; }

        [JsonProperty("RoomAmenity", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(1000)]
        public ICollection<RoomAmenity> RoomAmenity { get; set; }


    }

    /// <summary>Provides details regarding rooms, usually guest rooms.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RoomTypeDetail : RoomType
    {
        /// <summary>The number of rooms that have been combined to create this room type.</summary>
        [JsonProperty("numberOfUnits", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? NumberOfUnits { get; set; }

        /// <summary>TODO-(Should this be Guarantee?)Denotes the form of guarantee for this room.</summary>
        [JsonProperty("reqdGuaranteeType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ReqdGuaranteeType { get; set; }

        /// <summary>Assigned Type: cthp-0600:AdditionalDetails</summary>
        [JsonProperty("AdditionalDetails", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public AdditionalDetails AdditionalDetails { get; set; }

        [JsonProperty("RoomOccupancy", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public ICollection<RoomOccupancy> RoomOccupancy { get; set; }

        /// <summary>Indicates the room is a sleeping room when true.</summary>
        [JsonProperty("roomInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? RoomInd { get; set; }

        /// <summary>Indicates the room is converted when true.</summary>
        [JsonProperty("convertedInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ConvertedInd { get; set; }

        /// <summary>Indicates the room is an alternate room type to the requested room type when true.</summary>
        [JsonProperty("alternateInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? AlternateInd { get; set; }


    }

    /// <summary>Used to designate the quality level of the RoomAmenity</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum RoomAmenityQuality_Enum
    {
        [EnumMember(Value = @"Premium")]
        Premium = 0,

        [EnumMember(Value = @"Deluxe")]
        Deluxe = 1,

        [EnumMember(Value = @"Standard")]
        Standard = 2,

        [EnumMember(Value = @"Economy")]
        Economy = 3,

    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AdditionalDetail
    {
        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("type", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string Type { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Code { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Amount", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount Amount { get; set; }

        /// <summary>Assigned Type: c-0600:TextTitleAndDescription</summary>
        [JsonProperty("Description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public TextTitleAndDescription Description { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AdditionalDetails
    {
        [JsonProperty("AdditionalDetail", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        public ICollection<AdditionalDetail> AdditionalDetail { get; set; } = new Collection<AdditionalDetail>();

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RoomOccupancy
    {
        /// <summary>Assigned Type: c-0600:NonnegativeInteger</summary>
        [JsonProperty("minOccupancy", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? MinOccupancy { get; set; }

        /// <summary>Assigned Type: c-0600:NonnegativeInteger</summary>
        [JsonProperty("maxOccupancy", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? MaxOccupancy { get; set; }

        [JsonProperty("AgeQualifying", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(20)]
        public ICollection<AgeQualifying> AgeQualifying { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AgeQualifying
    {
        /// <summary>MinAge: The minimum age to qualify for AgeQualifyingCode.</summary>
        [JsonProperty("minAge", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? MinAge { get; set; }

        /// <summary>Max Age: The maximum age to qualify for AgeQualifyingCode.</summary>
        [JsonProperty("maxAge", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? MaxAge { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("ageBucket", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string AgeBucket { get; set; }

        /// <summary>Assigned Type: c-0600:NonnegativeInteger</summary>
        [JsonProperty("count", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? Count { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("ageQualifyingCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string AgeQualifyingCode { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum YesNoUnknown_Enum
    {
        [EnumMember(Value = @"Yes")]
        Yes = 0,

        [EnumMember(Value = @"No")]
        No = 1,

        [EnumMember(Value = @"Unknown")]
        Unknown = 2,

    }

    /// <summary>A product is any product, service or package of products and services  that can be priced and purchased by a specific supplier.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ProductHospitalityOffer : Product
    {
        /// <summary>Refers to PropertyDates object in the ReferenceList_PropertyDates</summary>
        [JsonProperty("propertyDatesRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string PropertyDatesRef { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("bookingCode", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(32)]
        public string BookingCode { get; set; }

        /// <summary>Assigned Type: cthp-0600:RoomType</summary>
        [JsonProperty("RoomType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public RoomType RoomType { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ReferenceListPropertyDates : ReferenceList
    {
        [JsonProperty("PropertyDates", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        public ICollection<PropertyDates> PropertyDates { get; set; } = new Collection<PropertyDates>();

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropertyDates
    {
        /// <summary>Hotel availability information</summary>
        [JsonProperty("availability", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public AvailabilityStatus_Enum? Availability { get; set; }

        [JsonProperty("adaCompliant", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoUnknown_Enum? AdaCompliant { get; set; }

        [JsonProperty("id", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("moreRatesToken", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string MoreRatesToken { get; set; }

        /// <summary>Assigned Type: cthp-0600:PropertyKey</summary>
        [JsonProperty("PropertyKey", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PropertyKey PropertyKey { get; set; }

        /// <summary>Assigned Type: c-0600:DateRange</summary>
        [JsonProperty("DateRange", Required = Required.Always)]
        public DateRange DateRange { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>TODO</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TermsAndConditionsHospitality : TermsAndConditions
    {
        [JsonProperty("Guarantee", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(5)]
        public List<Guarantee> Guarantee { get; set; }

        [JsonProperty("CancelPenalty", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public ICollection<CancelPenalty> CancelPenalty { get; set; }

        [JsonProperty("AcceptedCreditCard", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<string> AcceptedCreditCard { get; set; }

        [JsonProperty("Description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(5)]
        public ICollection<string> Description { get; set; }

        /// <summary>Assigned Type: cthp-0600:MealsIncluded</summary>
        [JsonProperty("MealsIncluded", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public MealsIncluded MealsIncluded { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>The price of this offer, along with rate and other information used in calculating it</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PriceHospitality : Price
    {
        ///// <summary>Specifies how the room is priced (per night, per person, etc.).</summary>
        //[JsonProperty("roomPricingType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        //[JsonConverter(typeof(StringEnumConverter))]
        //public Pricing_Enum? RoomPricingType { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("ratePlanCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string RatePlanCode { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("ratePlanName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string RatePlanName { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("ratePlanId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string RatePlanId { get; set; }

        /// <summary>A textual description</summary>
        [JsonProperty("Description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1024)]
        public string Description { get; set; }

        [JsonProperty("NightlyRate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<NightlyRate> NightlyRate { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("AverageNightlyRate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount AverageNightlyRate { get; set; }

        /// <summary>Assigned Type: c-0600:Commission</summary>
        [JsonProperty("Commission", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CommissionPercent Commission { get; set; }

        /// <summary>If present and true, the nightly price chages one or more times during the stay</summary>
        [JsonProperty("priceChangesDuringStayInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? PriceChangesDuringStayInd { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Guarantee
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Code { get; set; }

        /// <summary>An enumerated type defining the guarantee to be applied to this reservation.</summary>
        [JsonProperty("type", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public GuaranteeType_Enum? Type { get; set; }

        [JsonProperty("credentialsRequiredInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? CredentialsRequiredInd { get; set; }


    }

    /// <summary>An enumerated type defining the guarantee to be applied to this reservation.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum GuaranteeType_Enum
    {
        [EnumMember(Value = @"GuaranteeRequired")]
        GuaranteeRequired = 0,

        [EnumMember(Value = @"CC/DC/Voucher")]
        CC_DC_Voucher = 1,

        [EnumMember(Value = @"Profile")]
        Profile = 2,

        [EnumMember(Value = @"NoGuaranteesAccepted")]
        NoGuaranteesAccepted = 3,

        [EnumMember(Value = @"GuaranteesAccepted")]
        GuaranteesAccepted = 4,

        [EnumMember(Value = @"DepositRequired")]
        DepositRequired = 5,

        [EnumMember(Value = @"GuaranteesNotRequired")]
        GuaranteesNotRequired = 6,

        [EnumMember(Value = @"DepositNotRequired")]
        DepositNotRequired = 7,

        [EnumMember(Value = @"PrepayRequired")]
        PrepayRequired = 8,

        [EnumMember(Value = @"PrepayNotRequired")]
        PrepayNotRequired = 9,

        [EnumMember(Value = @"NoDepositsAccepted")]
        NoDepositsAccepted = 10,

        [EnumMember(Value = @"CREDIT CARD DEPOSIT")]
        CREDIT_CARD_DEPOSIT = 11,

    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Deadline
    {
        /// <summary>Assigned Type: c-0600:DateOrDateWindows</summary>
        [JsonProperty("SpecificDate", Required = Required.Always)]
        public DateOrDateWindows SpecificDate { get; set; }

        [JsonProperty("Time", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"(([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)((:?)[0-5]\d)?([\.,]\d+(?!:))?")]
        public string Time { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class CancelPenalty
    {
        /// <summary>Assigned Type: cthp-0600:Deadline</summary>
        [JsonProperty("Deadline", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Deadline Deadline { get; set; }

        /// <summary>Assigned Type: cthp-0600:Penalty</summary>
        [JsonProperty("Penalty", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Penalty Penalty { get; set; }

        /// <summary>Indicates that any prepayment for the reservation is non refundable, therefore a 100% penalty on the prepayment is applied, irrespective of deadline.</summary>
        [JsonProperty("nonRefundableInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? NonRefundableInd { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class MealsIncluded
    {
        [JsonProperty("breakfastInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? BreakfastInd { get; set; }

        [JsonProperty("lunchInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? LunchInd { get; set; }

        [JsonProperty("dinnerInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? DinnerInd { get; set; }


    }

    /// <summary>An enumerated type that defines how a service is priced.  Values:  Per stay,  Per person, Per night, Per person per night,  Per use.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum Pricing_Enum
    {
        [EnumMember(Value = @"Per stay")]
        Per_stay = 0,

        [EnumMember(Value = @"Per person")]
        Per_person = 1,

        [EnumMember(Value = @"Per night")]
        Per_night = 2,

        [EnumMember(Value = @"Per person per night")]
        Per_person_per_night = 3,

        [EnumMember(Value = @"Per use")]
        Per_use = 4,

    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RoomStayCandidates
    {
        [JsonProperty("RoomStayCandidate", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        public List<RoomStayCandidate> RoomStayCandidate { get; set; } = new List<RoomStayCandidate>();


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RoomStayCandidate
    {
        /// <summary>Assigned Type: cthp-0600:GuestCounts</summary>
        [JsonProperty("GuestCounts", Required = Required.Always)]
        public GuestCounts GuestCounts { get; set; }

        [JsonProperty("Amenity", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<RoomAmenity> Amenity { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuestCount
    {
        /// <summary>Assigned Type: c-0600:NonnegativeInteger</summary>
        [JsonProperty("age", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? Age { get; set; }

        /// <summary>The number of guests in one AgeQualifyingCode or Count.</summary>
        [JsonProperty("count", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Count { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("ageQualifyingCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string AgeQualifyingCode { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuestCounts
    {
        [JsonProperty("GuestCount", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(99)]
        public ICollection<GuestCount> GuestCount { get; set; } = new Collection<GuestCount>();


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class HotelSearchCriterion
    {
        [JsonProperty("PropertyRequest", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(100)]
        public List<PropertyRequest> PropertyRequest { get; set; } = new List<PropertyRequest>();

        /// <summary>Assigned Type: cthp-0600:RoomStayCandidates</summary>
        [JsonProperty("RoomStayCandidates", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public RoomStayCandidates RoomStayCandidates { get; set; }

        /// <summary>Assigned Type: cthp-0600:RatePlanCandidates</summary>
        [JsonProperty("RatePlanCandidates", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public RatePlanCandidatesDetail RatePlanCandidates { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("RatePlanCandidatesDetail", typeof(RatePlanCandidatesDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RatePlanCandidates
    {
        [JsonProperty("RatePlanCandidate", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        public List<RatePlanCandidateDetail> RatePlanCandidate { get; set; } = new List<RatePlanCandidateDetail>();

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RatePlanCandidatesDetail : RatePlanCandidates
    {
        /// <summary>Minimum number rate plans requested in response</summary>
        [JsonProperty("numberOfRatePlans", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? NumberOfRatePlans { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Element used to identify available products and rates.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("RatePlanCandidateDetail", typeof(RatePlanCandidateDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RatePlanCandidate
    {
        /// <summary>Assigned Type: c-0600:NonnegativeInteger</summary>
        [JsonProperty("priority", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? Priority { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("ratePlanCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string RatePlanCode { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("ratePlanType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string RatePlanType { get; set; }

        /// <summary>Assigned Type: c-0600:SupplierCode</summary>
        [JsonProperty("chainCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(5, MinimumLength = 2)]
        [RegularExpression(@"([a-zA-Z0-9]{2,5})")]
        public string ChainCode { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("propertyCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string PropertyCode { get; set; }


    }

    /// <summary>Element used to identify available products and rates.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class RatePlanCandidateDetail : RatePlanCandidate
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("ratePlanID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string RatePlanID { get; set; }

        /// <summary>Assigned Type: c-0600:LoyaltyIdentifier</summary>
        [JsonProperty("loyaltyIdentifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1000, MinimumLength = 1)]
        public string LoyaltyIdentifier { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PropertyRequest
    {
        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("moreRatesToken", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string MoreRatesToken { get; set; }

        /// <summary>Assigned Type: cthp-0600:PropertyKey</summary>
        [JsonProperty("PropertyKey", Required = Required.Always)]
        public PropertyKey PropertyKey { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>A monetary amount, up to 4 decimal places. Decimal place needs to be included.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class NightlyRate
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public double? Value { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyCode_ISO</summary>
        [JsonProperty("code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{3}")]
        public string Code { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyMinorUnit</summary>
        [JsonProperty("minorUnit", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? MinorUnit { get; set; }

        [JsonProperty("startDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? StartDate { get; set; }

        /// <summary>Number of nights this rate applies</summary>
        [JsonProperty("nights", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Nights { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("PenaltyAmount", typeof(PenaltyAmount))]
    [JsonInheritanceAttribute("PenaltyPercent", typeof(PenaltyPercent))]
    [JsonInheritanceAttribute("PenaltyNights", typeof(PenaltyNights))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Penalty
    {
        /// <summary>Penalty is subject to tax</summary>
        [JsonProperty("subjectToTax", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoUnknown_Enum? SubjectToTax { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PenaltyAmount : Penalty
    {
        /// <summary>This amount includes Tax</summary>
        [JsonProperty("includesTax", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoUnknown_Enum? IncludesTax { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Amount", Required = Required.Always)]
        public CurrencyAmount Amount { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PenaltyPercent : Penalty
    {
        /// <summary>What the Percent is applied to.</summary>
        [JsonProperty("appliesTo", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(StringEnumConverter))]
        public PercentAppliesTo AppliesTo { get; set; }

        /// <summary>A percentage charged as a Penalty</summary>
        [JsonProperty("Percent", Required = Required.Always)]
        [Range(0D, int.MaxValue)]
        public double Percent { get; set; }

        /// <summary>If the Penalty applies to a specific number nights, this is the the number of nights</summary>
        [JsonProperty("Nights", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Nights { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Amount", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount Amount { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PenaltyNights : Penalty
    {
        /// <summary>The number of nights that will be charged as a penalty</summary>
        [JsonProperty("Nights", Required = Required.Always)]
        public int Nights { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum PercentAppliesTo
    {
        [EnumMember(Value = @"Nights")]
        Nights = 0,

        [EnumMember(Value = @"Stay")]
        Stay = 1,

        [EnumMember(Value = @"Amount")]
        Amount = 2,

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class HotelOfferReq {

        [JsonProperty("OfferQueryHotelRequest", Required = Required.Always)]
        public OfferQueryHotelRequest OfferQueryHotelRequest { get; set; }
    }

    /// <summary>A product or products available at the given price and terms</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OfferQueryHotelRequest
    {
        /// <summary>Assigned Type: c-0600:CurrencyCode_ISO</summary>
        [JsonProperty("currency", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{3}")]
        public string Currency { get; set; }

        /// <summary>Maximum time (in milliseconds) to wait for provider responses before returning a response to the consumer of this service</summary>
        [JsonProperty("maxResponseWaitTime", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? MaxResponseWaitTime { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("moreRatesToken", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string MoreRatesToken { get; set; }

        /// <summary>Assigned Type: c-0600:AgencyCode_IATA</summary>
        [JsonProperty("agencyCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"([0-9]{8})")]
        public string AgencyCode { get; set; }

        /// <summary>Travelport core system</summary>
        [JsonProperty("sourceSystem", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public CoreSystem_Enum? SourceSystem { get; set; }

        /// <summary>Assigned Type: c-0600:DateOrDateWindows</summary>
        [JsonProperty("StayDates", Required = Required.Always)]
        public DateOrDateWindows StayDates { get; set; }

        /// <summary>Assigned Type: cthp-0600:HotelSearchCriterion_Summary</summary>
        [JsonProperty("HotelSearchCriterion", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public HotelSearchCriterion HotelSearchCriterion { get; set; }

        /// <summary>Assigned Type: c-0600:PseudoCityInfo</summary>
        [JsonProperty("PseudoCity", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PseudoCityInfo PseudoCity { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("MinimumAmount", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount MinimumAmount { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("MaximumAmount", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount MaximumAmount { get; set; }

        /// <summary>Used to specify that a verbose response is to be returned.  Verbose responses repeat the Property information in each Product and do not return the reference list.</summary>
        [JsonProperty("verboseResponseInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? VerboseResponseInd { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>A product or products available at the given price and terms</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OfferQuerySimpleHotelRequest
    {
        [JsonProperty("checkinDate", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset CheckinDate { get; set; }

        [JsonProperty("checkoutDate", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset CheckoutDate { get; set; }

        /// <summary>Assigned Type: c-0600:NumberSingleDigit</summary>
        [JsonProperty("numberOfGuests", Required = Required.Always)]
        public int NumberOfGuests { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("rateAccessCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string RateAccessCode { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("rateCategory", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string RateCategory { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("moreRatesToken", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string MoreRatesToken { get; set; }

        /// <summary>Assigned Type: cthp-0600:PropertyKey_Summary</summary>
        [JsonProperty("PropertyKey", Required = Required.Always)]
        public PropertyKey PropertyKey { get; set; }

        /// <summary>Assigned Type: c-0600:PseudoCityInfo</summary>
        [JsonProperty("PseudoCity", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PseudoCityInfo PseudoCity { get; set; }

        /// <summary>Used to specify that a verbose response is to be returned.  Verbose responses repeat the Property information in each Product and do not return the reference list.</summary>
        [JsonProperty("verboseResponseInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? VerboseResponseInd { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Offers
    {
        /// <summary>Unique transaction, correlation or tracking id for a single request and reply i.e. for a single transaction. Should be a 128 bit GUID format. Also know as E2ETrackingId.</summary>
        [JsonProperty("transactionId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TransactionId { get; set; }

        /// <summary>Optional ID for internal child transactions created for processing a single request (single transaction). Should be a 128 bit GUID format. Also known as ChildTrackingId.</summary>
        [JsonProperty("traceId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TraceId { get; set; }

        /// <summary>Assigned Type: c-0600:Result</summary>
        [JsonProperty("Result", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Result Result { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }

        [JsonProperty("Offer", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<Offer> Offer { get; set; }

        /// <summary>Assigned Type: c-0600:ReferenceList</summary>
        [JsonProperty("ReferenceList", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ReferenceList ReferenceList { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }


    /// <summary>
    /// /////////////////////////////////////////////////////////////// Hotel Rules ///////////////////////////////////////////
    /// </summary>

    //[GeneratedCode("NSwag", "13.0.6.0 (NJsonSchema v10.0.23.0 (Newtonsoft.Json v11.0.0.0))")]
    //public partial class Client
    //{
    //    private string _baseUrl = "http://127.0.0.1/";
    //    private HttpClient _httpClient;
    //    private System.Lazy<JsonSerializerSettings> _settings;

    //    public Client(HttpClient httpClient)
    //    {
    //        _httpClient = httpClient;
    //        _settings = new System.Lazy<JsonSerializerSettings>(() =>
    //        {
    //            var settings = new JsonSerializerSettings();
    //            UpdateJsonSerializerSettings(settings);
    //            return settings;
    //        });
    //    }

    //    public string BaseUrl
    //    {
    //        get { return _baseUrl; }
    //        set { _baseUrl = value; }
    //    }

    //    protected JsonSerializerSettings JsonSerializerSettings { get { return _settings.Value; } }

    //    partial void UpdateJsonSerializerSettings(JsonSerializerSettings settings);
    //    partial void PrepareRequest(HttpClient client, HttpRequestMessage request, string url);
    //    partial void PrepareRequest(HttpClient client, HttpRequestMessage request, StringBuilder urlBuilder);
    //    partial void ProcessResponse(HttpClient client, HttpResponseMessage response);

    //    /// <summary>HotelRulesResource - Get</summary>
    //    /// <param name="hotelCity">A three letter city code.  Adheres to the IATA specification (http:\/\/www.iata.org\/).  This supports metropolitan cities.  For example, DEN represents metropolitan Denver, Denver International Airport, Arapahoe County Airport, etc.  This is often used as a city code.</param>
    //    /// <param name="bookingCode">A tiny string  \nMinimum length of 0 and a maximum length of 32</param>
    //    /// <param name="storedCurrency">An ISO 4217 (3) alpha character code that specifies a monetary unit.</param>
    //    /// <param name="storedAmount">A monetary value (valid to req\/rsp Currency type)</param>
    //    /// <param name="chainCode">Hotel chain code</param>
    //    /// <param name="propertyCode">Property code</param>
    //    /// <param name="pseudoCityCode">Identifies the PCC for which the API is being called</param>
    //    /// <param name="coreAffinity">Specified the Travelport core system (e.g. '1P', '1G') with which the PCC is affiliated.</param>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public Task<HotelRulesID> GetAsync(System.DateTimeOffset checkinDate, System.DateTimeOffset checkoutDate, int numberOfGuests, string hotelCity, string bookingCode, string storedCurrency, double? storedAmount, string chainCode, string propertyCode, string pseudoCityCode, string coreAffinity)
    //    {
    //        return GetAsync(checkinDate, checkoutDate, numberOfGuests, hotelCity, bookingCode, storedCurrency, storedAmount, chainCode, propertyCode, pseudoCityCode, coreAffinity, CancellationToken.None);
    //    }

    //    /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
    //    /// <summary>HotelRulesResource - Get</summary>
    //    /// <param name="hotelCity">A three letter city code.  Adheres to the IATA specification (http:\/\/www.iata.org\/).  This supports metropolitan cities.  For example, DEN represents metropolitan Denver, Denver International Airport, Arapahoe County Airport, etc.  This is often used as a city code.</param>
    //    /// <param name="bookingCode">A tiny string  \nMinimum length of 0 and a maximum length of 32</param>
    //    /// <param name="storedCurrency">An ISO 4217 (3) alpha character code that specifies a monetary unit.</param>
    //    /// <param name="storedAmount">A monetary value (valid to req\/rsp Currency type)</param>
    //    /// <param name="chainCode">Hotel chain code</param>
    //    /// <param name="propertyCode">Property code</param>
    //    /// <param name="pseudoCityCode">Identifies the PCC for which the API is being called</param>
    //    /// <param name="coreAffinity">Specified the Travelport core system (e.g. '1P', '1G') with which the PCC is affiliated.</param>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public async Task<HotelRulesID> GetAsync(System.DateTimeOffset checkinDate, System.DateTimeOffset checkoutDate, int numberOfGuests, string hotelCity, string bookingCode, string storedCurrency, double? storedAmount, string chainCode, string propertyCode, string pseudoCityCode, string coreAffinity, CancellationToken cancellationToken)
    //    {
    //        if (checkinDate == null)
    //            throw new System.ArgumentNullException("checkinDate");

    //        if (checkoutDate == null)
    //            throw new System.ArgumentNullException("checkoutDate");

    //        if (numberOfGuests == null)
    //            throw new System.ArgumentNullException("numberOfGuests");

    //        if (chainCode == null)
    //            throw new System.ArgumentNullException("chainCode");

    //        if (propertyCode == null)
    //            throw new System.ArgumentNullException("propertyCode");

    //        var urlBuilder_ = new StringBuilder();
    //        urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/?");
    //        urlBuilder_.Append(System.Uri.EscapeDataString("checkinDate") + "=").Append(System.Uri.EscapeDataString(checkinDate.ToString("s", CultureInfo.InvariantCulture))).Append("&");
    //        urlBuilder_.Append(System.Uri.EscapeDataString("checkoutDate") + "=").Append(System.Uri.EscapeDataString(checkoutDate.ToString("s", CultureInfo.InvariantCulture))).Append("&");
    //        urlBuilder_.Append(System.Uri.EscapeDataString("numberOfGuests") + "=").Append(System.Uri.EscapeDataString(ConvertToString(numberOfGuests, CultureInfo.InvariantCulture))).Append("&");
    //        if (hotelCity != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("hotelCity") + "=").Append(System.Uri.EscapeDataString(ConvertToString(hotelCity, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        if (bookingCode != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("bookingCode") + "=").Append(System.Uri.EscapeDataString(ConvertToString(bookingCode, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        if (storedCurrency != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("storedCurrency") + "=").Append(System.Uri.EscapeDataString(ConvertToString(storedCurrency, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        if (storedAmount != null)
    //        {
    //            urlBuilder_.Append(System.Uri.EscapeDataString("storedAmount") + "=").Append(System.Uri.EscapeDataString(ConvertToString(storedAmount, CultureInfo.InvariantCulture))).Append("&");
    //        }
    //        urlBuilder_.Append(System.Uri.EscapeDataString("chainCode") + "=").Append(System.Uri.EscapeDataString(ConvertToString(chainCode, CultureInfo.InvariantCulture))).Append("&");
    //        urlBuilder_.Append(System.Uri.EscapeDataString("propertyCode") + "=").Append(System.Uri.EscapeDataString(ConvertToString(propertyCode, CultureInfo.InvariantCulture))).Append("&");
    //        urlBuilder_.Length--;

    //        var client_ = _httpClient;
    //        try
    //        {
    //            using (var request_ = new HttpRequestMessage())
    //            {
    //                if (pseudoCityCode != null)
    //                    request_.Headers.TryAddWithoutValidation("PseudoCityCode", ConvertToString(pseudoCityCode, CultureInfo.InvariantCulture));
    //                if (coreAffinity != null)
    //                    request_.Headers.TryAddWithoutValidation("CoreAffinity", ConvertToString(coreAffinity, CultureInfo.InvariantCulture));
    //                request_.Method = new HttpMethod("GET");
    //                request_.Headers.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));

    //                PrepareRequest(client_, request_, urlBuilder_);
    //                var url_ = urlBuilder_.ToString();
    //                request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);
    //                PrepareRequest(client_, request_, url_);

    //                var response_ = await client_.SendAsync(request_, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
    //                try
    //                {
    //                    var headers_ = Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
    //                    if (response_.Content != null && response_.Content.Headers != null)
    //                    {
    //                        foreach (var item_ in response_.Content.Headers)
    //                            headers_[item_.Key] = item_.Value;
    //                    }

    //                    ProcessResponse(client_, response_);

    //                    var status_ = ((int)response_.StatusCode).ToString();
    //                    if (status_ == "200")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<HotelRulesID>(response_, headers_).ConfigureAwait(false);
    //                        return objectResponse_.Object;
    //                    }
    //                    else
    //                    if (status_ == "400")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<HotelRulesID>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<HotelRulesID>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "401")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<HotelRulesID>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<HotelRulesID>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "403")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<HotelRulesID>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<HotelRulesID>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "404")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<HotelRulesID>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<HotelRulesID>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "500")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<HotelRulesID>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<HotelRulesID>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ != "200" && status_ != "204")
    //                    {
    //                        var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
    //                        throw new ApiException("The HTTP status code of the response was not expected (" + (int)response_.StatusCode + ").", (int)response_.StatusCode, responseData_, headers_, null);
    //                    }

    //                    return default(HotelRulesID);
    //                }
    //                finally
    //                {
    //                    if (response_ != null)
    //                        response_.Dispose();
    //                }
    //            }
    //        }
    //        finally
    //        {
    //        }
    //    }

    //    protected struct ObjectResponseResult<T>
    //    {
    //        public ObjectResponseResult(T responseObject, string responseText)
    //        {
    //            this.Object = responseObject;
    //            this.Text = responseText;
    //        }

    //        public T Object { get; }

    //        public string Text { get; }
    //    }

    //    public bool ReadResponseAsString { get; set; }

    //    protected virtual async Task<ObjectResponseResult<T>> ReadObjectResponseAsync<T>(HttpResponseMessage response, IReadOnlyDictionary<string, IEnumerable<string>> headers)
    //    {
    //        if (response == null || response.Content == null)
    //        {
    //            return new ObjectResponseResult<T>(default(T), string.Empty);
    //        }

    //        if (ReadResponseAsString)
    //        {
    //            var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
    //            try
    //            {
    //                var typedBody = JsonConvert.DeserializeObject<T>(responseText, JsonSerializerSettings);
    //                return new ObjectResponseResult<T>(typedBody, responseText);
    //            }
    //            catch (JsonException exception)
    //            {
    //                var message = "Could not deserialize the response body string as " + typeof(T).FullName + ".";
    //                throw new ApiException(message, (int)response.StatusCode, responseText, headers, exception);
    //            }
    //        }
    //        else
    //        {
    //            try
    //            {
    //                using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
    //                using (var streamReader = new StreamReader(responseStream))
    //                using (var jsonTextReader = new JsonTextReader(streamReader))
    //                {
    //                    var serializer = JsonSerializer.Create(JsonSerializerSettings);
    //                    var typedBody = serializer.Deserialize<T>(jsonTextReader);
    //                    return new ObjectResponseResult<T>(typedBody, string.Empty);
    //                }
    //            }
    //            catch (JsonException exception)
    //            {
    //                var message = "Could not deserialize the response body stream as " + typeof(T).FullName + ".";
    //                throw new ApiException(message, (int)response.StatusCode, string.Empty, headers, exception);
    //            }
    //        }
    //    }

    //    private string ConvertToString(object value, CultureInfo cultureInfo)
    //    {
    //        if (value is System.Enum)
    //        {
    //            string name = System.Enum.GetName(value.GetType(), value);
    //            if (name != null)
    //            {
    //                var field = IntrospectionExtensions.GetTypeInfo(value.GetType()).GetDeclaredField(name);
    //                if (field != null)
    //                {
    //                    var attribute = CustomAttributeExtensions.GetCustomAttribute(field, typeof(EnumMemberAttribute))
    //                        as EnumMemberAttribute;
    //                    if (attribute != null)
    //                    {
    //                        return attribute.Value != null ? attribute.Value : name;
    //                    }
    //                }
    //            }
    //        }
    //        else if (value is bool)
    //        {
    //            return System.Convert.ToString(value, cultureInfo).ToLowerInvariant();
    //        }
    //        else if (value is byte[])
    //        {
    //            return System.Convert.ToBase64String((byte[])value);
    //        }
    //        else if (value != null && value.GetType().IsArray)
    //        {
    //            var array = Enumerable.OfType<object>((System.Array)value);
    //            return string.Join(",", Enumerable.Select(array, o => ConvertToString(o, cultureInfo)));
    //        }

    //        return System.Convert.ToString(value, cultureInfo);
    //    }
    //}

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class CheckInOutPolicy
    {
        /// <summary>Assigned Type: ota2:LocalTime</summary>
        [JsonProperty("checkInTime", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"(([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)((:?)[0-5]\d)?([\.,]\d+(?!:))?")]
        public string CheckInTime { get; set; }

        /// <summary>Assigned Type: ota2:LocalTime</summary>
        [JsonProperty("checkOutTime", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"(([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)((:?)[0-5]\d)?([\.,]\d+(?!:))?")]
        public string CheckOutTime { get; set; }
    }

    /// <summary>A monetary amount, up to 4 decimal places. Decimal place needs to be included.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class BaseAmount
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public double? Value { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyCode_ISO</summary>
        [JsonProperty("code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{3}")]
        public string Code { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyMinorUnit</summary>
        [JsonProperty("minorUnit", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? MinorUnit { get; set; }

        [JsonProperty("startDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? StartDate { get; set; }

        /// <summary>Number of nights this rate applies</summary>
        [JsonProperty("nights", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Nights { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("HotelRules", typeof(HotelRules))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class HotelRulesID
    {
        /// <summary>Assigned Type: cthp-0600:PropertyKey</summary>
        [JsonProperty("PropertyKey", Required = Required.Always)]
        public PropertyKey PropertyKey { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class HotelRules : HotelRulesID
    {
        [JsonProperty("checkinDate", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset CheckinDate { get; set; }

        [JsonProperty("checkoutDate", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset CheckoutDate { get; set; }

        /// <summary>Assigned Type: c-0600:NumberSingleDigit</summary>
        [JsonProperty("numberOfGuests", Required = Required.Always)]
        public int NumberOfGuests { get; set; }

        /// <summary>Assigned Type: cthp-0600:Rules</summary>
        [JsonProperty("Rules", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Rules Rules { get; set; }

        [JsonProperty("NightlyRate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<BaseAmount> NightlyRate { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("TotalBeforeTaxesAndFees", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount TotalBeforeTaxesAndFees { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("TotalAfterTaxesAndFees", Required = Required.Always)]
        public CurrencyAmount TotalAfterTaxesAndFees { get; set; }

        /// <summary>Assigned Type: c-0600:Result</summary>
        [JsonProperty("Result", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Result Result { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class HotelRulesQueryRequest
    {
        [JsonProperty("checkinDate", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset CheckinDate { get; set; }

        [JsonProperty("checkoutDate", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset CheckoutDate { get; set; }

        /// <summary>Assigned Type: c-0600:NumberSingleDigit</summary>
        [JsonProperty("numberOfGuests", Required = Required.Always)]
        public int NumberOfGuests { get; set; }

        /// <summary>Assigned Type: c-0600:LocationCode_IATA</summary>
        [JsonProperty("hotelCity", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(3, MinimumLength = 3)]
        [RegularExpression(@"([a-zA-Z]{3})")]
        public string HotelCity { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("bookingCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string BookingCode { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyCode_ISO</summary>
        [JsonProperty("storedCurrency", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{3}")]
        public string StoredCurrency { get; set; }

        /// <summary>Assigned Type: c-0600:Money</summary>
        [JsonProperty("storedAmount", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public double? StoredAmount { get; set; }

        /// <summary>Assigned Type: cthp-0600:PropertyKey</summary>
        [JsonProperty("PropertyKey", Required = Required.Always)]
        public PropertyKey PropertyKey { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Rules
    {
        [JsonProperty("TextBlock", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public List<TextBlock> TextBlock { get; set; }

        [JsonProperty("TextParagraph", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public List<TextParagraph> TextParagraph { get; set; }

        [JsonProperty("AcceptedCreditCard", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public List<string> AcceptedCreditCard { get; set; }

        /// <summary>Assigned Type: cthp-0600:Guarantee</summary>
        [JsonProperty("Guarantee", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Guarantee Guarantee { get; set; }

        [JsonProperty("CancelPenalty", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public List<CancelPenalty> CancelPenalty { get; set; }

        [JsonProperty("Description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(5)]
        public List<string> Description { get; set; }

        /// <summary>Assigned Type: cthp-0600:MealsIncluded</summary>
        [JsonProperty("MealsIncluded", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public MealsIncluded MealsIncluded { get; set; }

        /// <summary>Assigned Type: cthp-0600:CheckInOutPolicy</summary>
        [JsonProperty("CheckInOutPolicy", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CheckInOutPolicy CheckInOutPolicy { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }


    /// <summary>
    /// /////////////////////////////////////////////////////// Reservation ///////////////////////////////////////////////////////////
    /// </summary>

    //[GeneratedCode("NSwag", "13.0.6.0 (NJsonSchema v10.0.23.0 (Newtonsoft.Json v11.0.0.0))")]
    //public partial class Client
    //{
    //    private string _baseUrl = "http://127.0.0.1/";
    //    private HttpClient _httpClient;
    //    private System.Lazy<JsonSerializerSettings> _settings;

    //    public Client(HttpClient httpClient)
    //    {
    //        _httpClient = httpClient;
    //        _settings = new System.Lazy<JsonSerializerSettings>(() =>
    //        {
    //            var settings = new JsonSerializerSettings();
    //            UpdateJsonSerializerSettings(settings);
    //            return settings;
    //        });
    //    }

    //    public string BaseUrl
    //    {
    //        get { return _baseUrl; }
    //        set { _baseUrl = value; }
    //    }

    //    protected JsonSerializerSettings JsonSerializerSettings { get { return _settings.Value; } }

    //    partial void UpdateJsonSerializerSettings(JsonSerializerSettings settings);
    //    partial void PrepareRequest(HttpClient client, HttpRequestMessage request, string url);
    //    partial void PrepareRequest(HttpClient client, HttpRequestMessage request, StringBuilder urlBuilder);
    //    partial void ProcessResponse(HttpClient client, HttpResponseMessage response);

    //    /// <summary>HospitalityReservationResource - Get</summary>
    //    /// <param name="reservationLocator">The provider and the provider's locator code for an offer</param>
    //    /// <param name="providerCode">A 2 to 5 Character Supplier code.  Airline, Hotel, Car, Rail codes</param>
    //    /// <param name="pseudoCityCode">Identifies the PCC for which the API is being called</param>
    //    /// <param name="coreAffinity">Specified the Travelport core system (e.g. '1P', '1G') with which the PCC is affiliated.</param>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public Task<ReservationID> GetAsync(string reservationLocator, string providerCode, string pseudoCityCode, string coreAffinity)
    //    {
    //        return GetAsync(reservationLocator, providerCode, pseudoCityCode, coreAffinity, CancellationToken.None);
    //    }

    //    /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
    //    /// <summary>HospitalityReservationResource - Get</summary>
    //    /// <param name="reservationLocator">The provider and the provider's locator code for an offer</param>
    //    /// <param name="providerCode">A 2 to 5 Character Supplier code.  Airline, Hotel, Car, Rail codes</param>
    //    /// <param name="pseudoCityCode">Identifies the PCC for which the API is being called</param>
    //    /// <param name="coreAffinity">Specified the Travelport core system (e.g. '1P', '1G') with which the PCC is affiliated.</param>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public async Task<ReservationID> GetAsync(string reservationLocator, string providerCode, string pseudoCityCode, string coreAffinity, CancellationToken cancellationToken)
    //    {
    //        if (reservationLocator == null)
    //            throw new System.ArgumentNullException("reservationLocator");

    //        if (providerCode == null)
    //            throw new System.ArgumentNullException("providerCode");

    //        var urlBuilder_ = new StringBuilder();
    //        urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/Reservations/{ReservationLocator}/{providerCode}");
    //        urlBuilder_.Replace("{ReservationLocator}", System.Uri.EscapeDataString(ConvertToString(reservationLocator, CultureInfo.InvariantCulture)));
    //        urlBuilder_.Replace("{providerCode}", System.Uri.EscapeDataString(ConvertToString(providerCode, CultureInfo.InvariantCulture)));

    //        var client_ = _httpClient;
    //        try
    //        {
    //            using (var request_ = new HttpRequestMessage())
    //            {
    //                if (pseudoCityCode != null)
    //                    request_.Headers.TryAddWithoutValidation("PseudoCityCode", ConvertToString(pseudoCityCode, CultureInfo.InvariantCulture));
    //                if (coreAffinity != null)
    //                    request_.Headers.TryAddWithoutValidation("CoreAffinity", ConvertToString(coreAffinity, CultureInfo.InvariantCulture));
    //                request_.Method = new HttpMethod("GET");
    //                request_.Headers.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));

    //                PrepareRequest(client_, request_, urlBuilder_);
    //                var url_ = urlBuilder_.ToString();
    //                request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);
    //                PrepareRequest(client_, request_, url_);

    //                var response_ = await client_.SendAsync(request_, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
    //                try
    //                {
    //                    var headers_ = Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
    //                    if (response_.Content != null && response_.Content.Headers != null)
    //                    {
    //                        foreach (var item_ in response_.Content.Headers)
    //                            headers_[item_.Key] = item_.Value;
    //                    }

    //                    ProcessResponse(client_, response_);

    //                    var status_ = ((int)response_.StatusCode).ToString();
    //                    if (status_ == "400")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "401")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "402")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "403")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "404")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "500")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "200")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ReservationID>(response_, headers_).ConfigureAwait(false);
    //                        return objectResponse_.Object;
    //                    }
    //                    else
    //                    if (status_ != "200" && status_ != "204")
    //                    {
    //                        var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
    //                        throw new ApiException("The HTTP status code of the response was not expected (" + (int)response_.StatusCode + ").", (int)response_.StatusCode, responseData_, headers_, null);
    //                    }

    //                    return default(ReservationID);
    //                }
    //                finally
    //                {
    //                    if (response_ != null)
    //                        response_.Dispose();
    //                }
    //            }
    //        }
    //        finally
    //        {
    //        }
    //    }

    //    /// <summary>HospitalityReservationResource - Update</summary>
    //    /// <param name="reservationLocator">The provider and the provider's locator code for an offer</param>
    //    /// <param name="providerCode">A 2 to 5 Character Supplier code.  Airline, Hotel, Car, Rail codes</param>
    //    /// <param name="pseudoCityCode">Identifies the PCC for which the API is being called</param>
    //    /// <param name="coreAffinity">Specified the Travelport core system (e.g. '1P', '1G') with which the PCC is affiliated.</param>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public Task<ReservationID> UpdateAsync(string reservationLocator, string providerCode, string pseudoCityCode, string coreAffinity)
    //    {
    //        return UpdateAsync(reservationLocator, providerCode, pseudoCityCode, coreAffinity, CancellationToken.None);
    //    }

    //    /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
    //    /// <summary>HospitalityReservationResource - Update</summary>
    //    /// <param name="reservationLocator">The provider and the provider's locator code for an offer</param>
    //    /// <param name="providerCode">A 2 to 5 Character Supplier code.  Airline, Hotel, Car, Rail codes</param>
    //    /// <param name="pseudoCityCode">Identifies the PCC for which the API is being called</param>
    //    /// <param name="coreAffinity">Specified the Travelport core system (e.g. '1P', '1G') with which the PCC is affiliated.</param>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public async Task<ReservationID> UpdateAsync(string reservationLocator, string providerCode, string pseudoCityCode, string coreAffinity, CancellationToken cancellationToken)
    //    {
    //        if (reservationLocator == null)
    //            throw new System.ArgumentNullException("reservationLocator");

    //        if (providerCode == null)
    //            throw new System.ArgumentNullException("providerCode");

    //        var urlBuilder_ = new StringBuilder();
    //        urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/Reservations/{ReservationLocator}/{providerCode}");
    //        urlBuilder_.Replace("{ReservationLocator}", System.Uri.EscapeDataString(ConvertToString(reservationLocator, CultureInfo.InvariantCulture)));
    //        urlBuilder_.Replace("{providerCode}", System.Uri.EscapeDataString(ConvertToString(providerCode, CultureInfo.InvariantCulture)));

    //        var client_ = _httpClient;
    //        try
    //        {
    //            using (var request_ = new HttpRequestMessage())
    //            {
    //                if (pseudoCityCode != null)
    //                    request_.Headers.TryAddWithoutValidation("PseudoCityCode", ConvertToString(pseudoCityCode, CultureInfo.InvariantCulture));
    //                if (coreAffinity != null)
    //                    request_.Headers.TryAddWithoutValidation("CoreAffinity", ConvertToString(coreAffinity, CultureInfo.InvariantCulture));
    //                request_.Content = new StringContent(string.Empty, Encoding.UTF8, "application/json");
    //                request_.Method = new HttpMethod("PUT");
    //                request_.Headers.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));

    //                PrepareRequest(client_, request_, urlBuilder_);
    //                var url_ = urlBuilder_.ToString();
    //                request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);
    //                PrepareRequest(client_, request_, url_);

    //                var response_ = await client_.SendAsync(request_, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
    //                try
    //                {
    //                    var headers_ = Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
    //                    if (response_.Content != null && response_.Content.Headers != null)
    //                    {
    //                        foreach (var item_ in response_.Content.Headers)
    //                            headers_[item_.Key] = item_.Value;
    //                    }

    //                    ProcessResponse(client_, response_);

    //                    var status_ = ((int)response_.StatusCode).ToString();
    //                    if (status_ == "400")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "401")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "402")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "403")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "404")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "500")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "200")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ReservationID>(response_, headers_).ConfigureAwait(false);
    //                        return objectResponse_.Object;
    //                    }
    //                    else
    //                    if (status_ != "200" && status_ != "204")
    //                    {
    //                        var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
    //                        throw new ApiException("The HTTP status code of the response was not expected (" + (int)response_.StatusCode + ").", (int)response_.StatusCode, responseData_, headers_, null);
    //                    }

    //                    return default(ReservationID);
    //                }
    //                finally
    //                {
    //                    if (response_ != null)
    //                        response_.Dispose();
    //                }
    //            }
    //        }
    //        finally
    //        {
    //        }
    //    }

    //    /// <summary>HospitalityReservationResource - Delete</summary>
    //    /// <param name="reservationLocator">The provider and the provider's locator code for an offer</param>
    //    /// <param name="providerCode">A 2 to 5 Character Supplier code.  Airline, Hotel, Car, Rail codes</param>
    //    /// <param name="pseudoCityCode">Identifies the PCC for which the API is being called</param>
    //    /// <param name="coreAffinity">Specified the Travelport core system (e.g. '1P', '1G') with which the PCC is affiliated.</param>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public Task DeleteAsync(string reservationLocator, string providerCode, string pseudoCityCode, string coreAffinity)
    //    {
    //        return DeleteAsync(reservationLocator, providerCode, pseudoCityCode, coreAffinity, CancellationToken.None);
    //    }

    //    /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
    //    /// <summary>HospitalityReservationResource - Delete</summary>
    //    /// <param name="reservationLocator">The provider and the provider's locator code for an offer</param>
    //    /// <param name="providerCode">A 2 to 5 Character Supplier code.  Airline, Hotel, Car, Rail codes</param>
    //    /// <param name="pseudoCityCode">Identifies the PCC for which the API is being called</param>
    //    /// <param name="coreAffinity">Specified the Travelport core system (e.g. '1P', '1G') with which the PCC is affiliated.</param>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public async Task DeleteAsync(string reservationLocator, string providerCode, string pseudoCityCode, string coreAffinity, CancellationToken cancellationToken)
    //    {
    //        if (reservationLocator == null)
    //            throw new System.ArgumentNullException("reservationLocator");

    //        if (providerCode == null)
    //            throw new System.ArgumentNullException("providerCode");

    //        var urlBuilder_ = new StringBuilder();
    //        urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/Reservations/{ReservationLocator}/{providerCode}");
    //        urlBuilder_.Replace("{ReservationLocator}", System.Uri.EscapeDataString(ConvertToString(reservationLocator, CultureInfo.InvariantCulture)));
    //        urlBuilder_.Replace("{providerCode}", System.Uri.EscapeDataString(ConvertToString(providerCode, CultureInfo.InvariantCulture)));

    //        var client_ = _httpClient;
    //        try
    //        {
    //            using (var request_ = new HttpRequestMessage())
    //            {
    //                if (pseudoCityCode != null)
    //                    request_.Headers.TryAddWithoutValidation("PseudoCityCode", ConvertToString(pseudoCityCode, CultureInfo.InvariantCulture));
    //                if (coreAffinity != null)
    //                    request_.Headers.TryAddWithoutValidation("CoreAffinity", ConvertToString(coreAffinity, CultureInfo.InvariantCulture));
    //                request_.Method = new HttpMethod("DELETE");

    //                PrepareRequest(client_, request_, urlBuilder_);
    //                var url_ = urlBuilder_.ToString();
    //                request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);
    //                PrepareRequest(client_, request_, url_);

    //                var response_ = await client_.SendAsync(request_, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
    //                try
    //                {
    //                    var headers_ = Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
    //                    if (response_.Content != null && response_.Content.Headers != null)
    //                    {
    //                        foreach (var item_ in response_.Content.Headers)
    //                            headers_[item_.Key] = item_.Value;
    //                    }

    //                    ProcessResponse(client_, response_);

    //                    var status_ = ((int)response_.StatusCode).ToString();
    //                    if (status_ == "400")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "401")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "402")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "403")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "404")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "500")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "200")
    //                    {
    //                        return;
    //                    }
    //                    else
    //                    if (status_ != "200" && status_ != "204")
    //                    {
    //                        var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
    //                        throw new ApiException("The HTTP status code of the response was not expected (" + (int)response_.StatusCode + ").", (int)response_.StatusCode, responseData_, headers_, null);
    //                    }
    //                }
    //                finally
    //                {
    //                    if (response_ != null)
    //                        response_.Dispose();
    //                }
    //            }
    //        }
    //        finally
    //        {
    //        }
    //    }

    //    /// <summary>HospitalityReservationResource - Create</summary>
    //    /// <param name="pseudoCityCode">Identifies the PCC for which the API is being called</param>
    //    /// <param name="coreAffinity">Specified the Travelport core system (e.g. '1P', '1G') with which the PCC is affiliated.</param>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public Task<ReservationID> CreateAsync(ReservationID reservationID, string pseudoCityCode, string coreAffinity)
    //    {
    //        return CreateAsync(reservationID, pseudoCityCode, coreAffinity, CancellationToken.None);
    //    }

    //    /// <param name="cancellationToken">A cancellation token that can be used by other objects or threads to receive notice of cancellation.</param>
    //    /// <summary>HospitalityReservationResource - Create</summary>
    //    /// <param name="pseudoCityCode">Identifies the PCC for which the API is being called</param>
    //    /// <param name="coreAffinity">Specified the Travelport core system (e.g. '1P', '1G') with which the PCC is affiliated.</param>
    //    /// <exception cref="ApiException">A server side error occurred.</exception>
    //    public async Task<ReservationID> CreateAsync(ReservationID reservationID, string pseudoCityCode, string coreAffinity, CancellationToken cancellationToken)
    //    {
    //        var urlBuilder_ = new StringBuilder();
    //        urlBuilder_.Append(BaseUrl != null ? BaseUrl.TrimEnd('/') : "").Append("/Reservations");

    //        var client_ = _httpClient;
    //        try
    //        {
    //            using (var request_ = new HttpRequestMessage())
    //            {
    //                if (pseudoCityCode != null)
    //                    request_.Headers.TryAddWithoutValidation("PseudoCityCode", ConvertToString(pseudoCityCode, CultureInfo.InvariantCulture));
    //                if (coreAffinity != null)
    //                    request_.Headers.TryAddWithoutValidation("CoreAffinity", ConvertToString(coreAffinity, CultureInfo.InvariantCulture));
    //                var content_ = new StringContent(JsonConvert.SerializeObject(reservationID, _settings.Value));
    //                content_.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
    //                request_.Content = content_;
    //                request_.Method = new HttpMethod("POST");
    //                request_.Headers.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));

    //                PrepareRequest(client_, request_, urlBuilder_);
    //                var url_ = urlBuilder_.ToString();
    //                request_.RequestUri = new System.Uri(url_, System.UriKind.RelativeOrAbsolute);
    //                PrepareRequest(client_, request_, url_);

    //                var response_ = await client_.SendAsync(request_, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false);
    //                try
    //                {
    //                    var headers_ = Enumerable.ToDictionary(response_.Headers, h_ => h_.Key, h_ => h_.Value);
    //                    if (response_.Content != null && response_.Content.Headers != null)
    //                    {
    //                        foreach (var item_ in response_.Content.Headers)
    //                            headers_[item_.Key] = item_.Value;
    //                    }

    //                    ProcessResponse(client_, response_);

    //                    var status_ = ((int)response_.StatusCode).ToString();
    //                    if (status_ == "400")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "401")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "402")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "403")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "404")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "500")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ErrorResponse>(response_, headers_).ConfigureAwait(false);
    //                        throw new ApiException<ErrorResponse>("A server side error occurred.", (int)response_.StatusCode, objectResponse_.Text, headers_, objectResponse_.Object, null);
    //                    }
    //                    else
    //                    if (status_ == "201")
    //                    {
    //                        var objectResponse_ = await ReadObjectResponseAsync<ReservationID>(response_, headers_).ConfigureAwait(false);
    //                        return objectResponse_.Object;
    //                    }
    //                    else
    //                    if (status_ != "200" && status_ != "204")
    //                    {
    //                        var responseData_ = response_.Content == null ? null : await response_.Content.ReadAsStringAsync().ConfigureAwait(false);
    //                        throw new ApiException("The HTTP status code of the response was not expected (" + (int)response_.StatusCode + ").", (int)response_.StatusCode, responseData_, headers_, null);
    //                    }

    //                    return default(ReservationID);
    //                }
    //                finally
    //                {
    //                    if (response_ != null)
    //                        response_.Dispose();
    //                }
    //            }
    //        }
    //        finally
    //        {
    //        }
    //    }

    //    protected struct ObjectResponseResult<T>
    //    {
    //        public ObjectResponseResult(T responseObject, string responseText)
    //        {
    //            this.Object = responseObject;
    //            this.Text = responseText;
    //        }

    //        public T Object { get; }

    //        public string Text { get; }
    //    }

    //    public bool ReadResponseAsString { get; set; }

    //    protected virtual async Task<ObjectResponseResult<T>> ReadObjectResponseAsync<T>(HttpResponseMessage response, IReadOnlyDictionary<string, IEnumerable<string>> headers)
    //    {
    //        if (response == null || response.Content == null)
    //        {
    //            return new ObjectResponseResult<T>(default(T), string.Empty);
    //        }

    //        if (ReadResponseAsString)
    //        {
    //            var responseText = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
    //            try
    //            {
    //                var typedBody = JsonConvert.DeserializeObject<T>(responseText, JsonSerializerSettings);
    //                return new ObjectResponseResult<T>(typedBody, responseText);
    //            }
    //            catch (JsonException exception)
    //            {
    //                var message = "Could not deserialize the response body string as " + typeof(T).FullName + ".";
    //                throw new ApiException(message, (int)response.StatusCode, responseText, headers, exception);
    //            }
    //        }
    //        else
    //        {
    //            try
    //            {
    //                using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
    //                using (var streamReader = new StreamReader(responseStream))
    //                using (var jsonTextReader = new JsonTextReader(streamReader))
    //                {
    //                    var serializer = JsonSerializer.Create(JsonSerializerSettings);
    //                    var typedBody = serializer.Deserialize<T>(jsonTextReader);
    //                    return new ObjectResponseResult<T>(typedBody, string.Empty);
    //                }
    //            }
    //            catch (JsonException exception)
    //            {
    //                var message = "Could not deserialize the response body stream as " + typeof(T).FullName + ".";
    //                throw new ApiException(message, (int)response.StatusCode, string.Empty, headers, exception);
    //            }
    //        }
    //    }

    //    private string ConvertToString(object value, CultureInfo cultureInfo)
    //    {
    //        if (value is System.Enum)
    //        {
    //            string name = System.Enum.GetName(value.GetType(), value);
    //            if (name != null)
    //            {
    //                var field = IntrospectionExtensions.GetTypeInfo(value.GetType()).GetDeclaredField(name);
    //                if (field != null)
    //                {
    //                    var attribute = CustomAttributeExtensions.GetCustomAttribute(field, typeof(EnumMemberAttribute))
    //                        as EnumMemberAttribute;
    //                    if (attribute != null)
    //                    {
    //                        return attribute.Value != null ? attribute.Value : name;
    //                    }
    //                }
    //            }
    //        }
    //        else if (value is bool)
    //        {
    //            return System.Convert.ToString(value, cultureInfo).ToLowerInvariant();
    //        }
    //        else if (value is byte[])
    //        {
    //            return System.Convert.ToBase64String((byte[])value);
    //        }
    //        else if (value != null && value.GetType().IsArray)
    //        {
    //            var array = Enumerable.OfType<object>((System.Array)value);
    //            return string.Join(",", Enumerable.Select(array, o => ConvertToString(o, cultureInfo)));
    //        }

    //        return System.Convert.ToString(value, cultureInfo);
    //    }
    //}

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum Gender_Enum
    {
        [EnumMember(Value = @"Male")]
        Male = 0,

        [EnumMember(Value = @"Female")]
        Female = 1,

        [EnumMember(Value = @"Unknown")]
        Unknown = 2,

    }

    /// <summary>Provides address information. Unformatted addresses are captured using the simple facet.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AddressDetail : Address
    {
        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("type", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string Type { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("use", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string Use { get; set; }

        /// <summary>Assigned Type: c-0600:Remark</summary>
        [JsonProperty("Remark", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Remark Remark { get; set; }

        /// <summary>Assigned Type: c-0600:Privacy</summary>
        [JsonProperty("Privacy", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Privacy Privacy { get; set; }

        /// <summary>The priority ranking within the group</summary>
        [JsonProperty("Priority", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, 300)]
        public int? Priority { get; set; }

        /// <summary>If true, this is a valid and complete mailing address that has been verified through an address verification service or previously mailed materials have not been returned.</summary>
        [JsonProperty("validInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ValidInd { get; set; }

        /// <summary>If true, this address came into the system through provisioning</summary>
        [JsonProperty("provisionedInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ProvisionedInd { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Electronic email addresses, in IETF specified format.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Email
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Value { get; set; }

        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("emailType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string EmailType { get; set; }

        /// <summary>Assigned Type: c-0600:StringText</summary>
        [JsonProperty("remark", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string Remark { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("preferredFormat", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string PreferredFormat { get; set; }

        /// <summary>Permission for sharing data for marketing purposes.</summary>
        [JsonProperty("shareMarketing", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoInherit_Enum? ShareMarketing { get; set; }

        /// <summary>Permission for sharing data for synchronization of information held by other travel service providers.</summary>
        [JsonProperty("shareSync", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoInherit_Enum? ShareSync { get; set; }

        /// <summary>When yes, a customer has explicitly opted out of marketing communication. This is used in combination with the ShareAllMarketInd and only one of these attributes should have a value of yes.</summary>
        [JsonProperty("optOutInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoInherit_Enum? OptOutInd { get; set; }

        /// <summary>When Yes, then the user has opted In</summary>
        [JsonProperty("optInStatus", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public OptInStatus_Enum? OptInStatus { get; set; }

        /// <summary>The datetime of receiving the opt in notice</summary>
        [JsonProperty("optInDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? OptInDate { get; set; }

        /// <summary>The datetime the opt out notice was received</summary>
        [JsonProperty("optOutDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? OptOutDate { get; set; }

        /// <summary>If true, this is a valid email address that has been system verified via a successful email transmission.</summary>
        [JsonProperty("validInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ValidInd { get; set; }

        /// <summary>If true then the email address came from the provisioning process</summary>
        [JsonProperty("provisionedInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ProvisionedInd { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Privacy
    {
        /// <summary>Optional internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Permission for sharing data for marketing purposes.</summary>
        [JsonProperty("shareMarketing", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoInherit_Enum? ShareMarketing { get; set; }

        /// <summary>Permission for sharing data for synchronization of information held by other travel service providers.</summary>
        [JsonProperty("shareSync", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoInherit_Enum? ShareSync { get; set; }

        /// <summary>When yes, a customer has explicitly opted out of marketing communication. This is used in combination with the ShareAllMarketInd and only one of these attributes should have a value of yes.</summary>
        [JsonProperty("optOutInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoInherit_Enum? OptOutInd { get; set; }

        /// <summary>When Yes, then the user has opted In</summary>
        [JsonProperty("optInStatus", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public OptInStatus_Enum? OptInStatus { get; set; }

        /// <summary>The datetime of receiving the opt in notice</summary>
        [JsonProperty("optInDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? OptInDate { get; set; }

        /// <summary>The datetime the opt out notice was received</summary>
        [JsonProperty("optOutDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? OptOutDate { get; set; }


    }

    /// <summary>Information about a telephone number, including the actual number and its usage.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("TelephoneDetail", typeof(TelephoneDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Telephone
    {
        /// <summary>Assigned Type: c-0600:TelephoneCountryAccessCode</summary>
        [JsonProperty("countryAccessCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9]{1,3}")]
        public string CountryAccessCode { get; set; }

        /// <summary>Assigned Type: c-0600:TelephoneAreaCityCode</summary>
        [JsonProperty("areaCityCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9]{1,8}")]
        public string AreaCityCode { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("phoneNumber", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(32)]
        public string PhoneNumber { get; set; }

        /// <summary>Assigned Type: c-0600:TelephoneExtension</summary>
        [JsonProperty("extension", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9]{0,5}")]
        public string Extension { get; set; }

        /// <summary>UOptional internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:StringUpperCaseAlphaNumeric_Max10</summary>
        [JsonProperty("cityCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(10)]
        [RegularExpression(@"([A-Z0-9]+)?")]
        public string CityCode { get; set; }

        /// <summary>Assigned Type: c-0600:Remark</summary>
        [JsonProperty("Remark", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Remark Remark { get; set; }

        [JsonProperty("role", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Enum_TelephoneRole Role { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Information about a telephone number, including the actual number and its usage.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TelephoneDetail : Telephone
    {
        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("phoneLocationType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string PhoneLocationType { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("phoneTechType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string PhoneTechType { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("phoneUseType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string PhoneUseType { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("pin", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Pin { get; set; }

        /// <summary>Assigned Type: c-0600:Priority</summary>
        [JsonProperty("priority", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, 300)]
        public int? Priority { get; set; }

        /// <summary>Assigned Type: c-0600:Privacy</summary>
        [JsonProperty("Privacy", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Privacy Privacy { get; set; }

        /// <summary>Assigned Type: c-0600:Enum_TelephoneRole</summary>
        [JsonProperty("Enum_TelephoneRole", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Enum_TelephoneRole Enum_TelephoneRole { get; set; }

        /// <summary>When true, indicates a default value should be used.</summary>
        [JsonProperty("defaultInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? DefaultInd { get; set; }

        /// <summary>true indicates this phone number was created through provisioned</summary>
        [JsonProperty("provisionedInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ProvisionedInd { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum YesNoInherit_Enum
    {
        [EnumMember(Value = @"Yes")]
        Yes = 0,

        [EnumMember(Value = @"No")]
        No = 1,

        [EnumMember(Value = @"Inherit")]
        Inherit = 2,

    }

    /// <summary>OTA Code list: Name Type\tNAM\t\n\t1\tFormer\n\t2\tNickname\n\t3\tAlternate\n\t4\tMaiden</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum Name
    {
        [EnumMember(Value = @"Former")]
        Former = 0,

        [EnumMember(Value = @"Nickname")]
        Nickname = 1,

        [EnumMember(Value = @"Alternate")]
        Alternate = 2,

        [EnumMember(Value = @"Maiden")]
        Maiden = 3,

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum OptInStatus_Enum
    {
        [EnumMember(Value = @"OptedIn")]
        OptedIn = 0,

        [EnumMember(Value = @"OptedOut")]
        OptedOut = 1,

        [EnumMember(Value = @"Unknown")]
        Unknown = 2,

    }

    /// <summary>The actual remark</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Remark
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        public string Value { get; set; }

        /// <summary>Optional internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Enum of the type of remark (name, general, ssr, etc)</summary>
        [JsonProperty("remarkType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public RemarkType? RemarkType { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("remarkCategory", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string RemarkCategory { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("supplier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Supplier { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("supplierType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string SupplierType { get; set; }

        /// <summary>Assigned Type: c-0600:Priority</summary>
        [JsonProperty("priority", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, 300)]
        public int? Priority { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum RemarkType
    {
        [EnumMember(Value = @"SSR")]
        SSR = 0,

        [EnumMember(Value = @"OSI")]
        OSI = 1,

        [EnumMember(Value = @"General")]
        General = 2,

        [EnumMember(Value = @"Name")]
        Name = 3,

        [EnumMember(Value = @"InternalOnly")]
        InternalOnly = 4,

        [EnumMember(Value = @"Accounting")]
        Accounting = 5,

        [EnumMember(Value = @"Itinerary")]
        Itinerary = 6,

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum GeoPoliticalAreaLevel_Enum
    {
        [EnumMember(Value = @"World")]
        World = 0,

        [EnumMember(Value = @"Global Area")]
        Global_Area = 1,

        [EnumMember(Value = @"Continent Group")]
        Continent_Group = 2,

        [EnumMember(Value = @"Continent")]
        Continent = 3,

        [EnumMember(Value = @"Country")]
        Country = 4,

        [EnumMember(Value = @"StateProvince")]
        StateProvince = 5,

        [EnumMember(Value = @"City")]
        City = 6,

        [EnumMember(Value = @"Airport")]
        Airport = 7,

    }

    /// <summary>The location code of the geographical location. Codes from Ref Pub</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GeoPoliticalArea
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Value { get; set; }

        [JsonProperty("level", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public GeoPoliticalAreaLevel_Enum? Level { get; set; }

        /// <summary>Optional internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }


    }

    /// <summary>The type of organization such as an Agency, Branch, Company, Supplier, Provider</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum OrganizationType
    {
        [EnumMember(Value = @"TravelAgency")]
        TravelAgency = 0,

        [EnumMember(Value = @"AgencyBranch")]
        AgencyBranch = 1,

        [EnumMember(Value = @"LoyaltyProgram")]
        LoyaltyProgram = 2,

        [EnumMember(Value = @"IdDocumentIssuer")]
        IdDocumentIssuer = 3,

        [EnumMember(Value = @"TravelSupplier")]
        TravelSupplier = 4,

        [EnumMember(Value = @"TravelProvider")]
        TravelProvider = 5,

        [EnumMember(Value = @"Regulatory")]
        Regulatory = 6,

    }

    /// <summary>A name of a person or organization in the localized language</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class LocalizedName
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Value { get; set; }

        /// <summary>The language of the localized name</summary>
        [JsonProperty("language", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Language { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Remarks
    {
        [JsonProperty("Remark", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(100)]
        public ICollection<Remark> Remark { get; set; } = new Collection<Remark>();

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>The type of email address from populated from OTA EAT values</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum EmailToFrom_Enum
    {
        [EnumMember(Value = @"To")]
        To = 0,

        [EnumMember(Value = @"From")]
        From = 1,

        [EnumMember(Value = @"Listserv")]
        Listserv = 2,

        [EnumMember(Value = @"CC")]
        CC = 3,

        [EnumMember(Value = @"BCC")]
        BCC = 4,

    }

    /// <summary>The email address in IETF specified format.and it's intended use as an address (to\/from\/cc\/)</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class EmailUse
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Value { get; set; }

        /// <summary>to reference the email address for NextSteps use this id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>The type of email address from populated from OTA EAT values</summary>
        [JsonProperty("emailToFrom_Enum", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public EmailToFrom_Enum? EmailToFrom_Enum { get; set; }

        /// <summary>Assigned Type: c-0600:Priority</summary>
        [JsonProperty("priority", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, 300)]
        public int? Priority { get; set; }

        /// <summary>Permission for sharing data for marketing purposes.</summary>
        [JsonProperty("shareMarketing", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoInherit_Enum? ShareMarketing { get; set; }

        /// <summary>Permission for sharing data for synchronization of information held by other travel service providers.</summary>
        [JsonProperty("shareSync", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoInherit_Enum? ShareSync { get; set; }

        /// <summary>When yes, a customer has explicitly opted out of marketing communication. This is used in combination with the ShareAllMarketInd and only one of these attributes should have a value of yes.</summary>
        [JsonProperty("optOutInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoInherit_Enum? OptOutInd { get; set; }

        /// <summary>When Yes, then the user has opted In</summary>
        [JsonProperty("optInStatus", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public OptInStatus_Enum? OptInStatus { get; set; }

        /// <summary>The datetime of receiving the opt in notice</summary>
        [JsonProperty("optInDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? OptInDate { get; set; }

        /// <summary>The datetime the opt out notice was received</summary>
        [JsonProperty("optOutDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? OptOutDate { get; set; }

        /// <summary>If true, this is a valid email address that has been system verified via a successful email transmission.</summary>
        [JsonProperty("validInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ValidInd { get; set; }

        /// <summary>If true, this is the default email address to use</summary>
        [JsonProperty("defaultInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? DefaultInd { get; set; }

        /// <summary>true indicates this email was created through the provisioning process</summary>
        [JsonProperty("provisionedInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ProvisionedInd { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class EncryptionToken
    {
        /// <summary>Note: This contains a key required to retrieve the full payment instrument details compliant with PCI DSS standards.</summary>
        [JsonProperty("encryptionKey", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKey { get; set; }

        /// <summary>Developer: This contains a reference to the key generation method being used - this is NOT the key value.</summary>
        [JsonProperty("encryptionKeyMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKeyMethod { get; set; }

        /// <summary>OpenTravel Best Practice: Encryption Method: When using the OpenTravel Encryption element, it is RECOMMENDED that all trading partners be informed of all encryption methods being used in advance of implementation to ensure message processing compatibility.</summary>
        [JsonProperty("encryptionMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionMethod { get; set; }

        /// <summary>Example: 5dfc52b51bd35553df8592078de921bc</summary>
        [JsonProperty("encryptedValue", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptedValue { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeMask</summary>
        [JsonProperty("mask", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Mask { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeToken</summary>
        [JsonProperty("token", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Token { get; set; }

        /// <summary>Developer: This contains a provider ID if multiple providers are used for secure information exchange.</summary>
        [JsonProperty("tokenProviderID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TokenProviderID { get; set; }

        /// <summary>Specifies the method that was used to authenticate the card.</summary>
        [JsonProperty("authenticationMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public EncryptionTokenTypeAuthenticationMethod? AuthenticationMethod { get; set; }

        /// <summary>Don't use this unless it is REALLY ok to not use encryption. Non-secure (plain text) value.</summary>
        [JsonProperty("PlainText", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string PlainText { get; set; }

        /// <summary>Assigned Type: c-0600:ErrorWarning</summary>
        [JsonProperty("ErrorWarning", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ErrorWarning ErrorWarning { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class BankAcctNumber
    {
        /// <summary>Note: This contains a key required to retrieve the full payment instrument details compliant with PCI DSS standards.</summary>
        [JsonProperty("encryptionKey", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKey { get; set; }

        /// <summary>Developer: This contains a reference to the key generation method being used - this is NOT the key value.</summary>
        [JsonProperty("encryptionKeyMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKeyMethod { get; set; }

        /// <summary>OpenTravel Best Practice: Encryption Method: When using the OpenTravel Encryption element, it is RECOMMENDED that all trading partners be informed of all encryption methods being used in advance of implementation to ensure message processing compatibility.</summary>
        [JsonProperty("encryptionMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionMethod { get; set; }

        /// <summary>Example: 5dfc52b51bd35553df8592078de921bc</summary>
        [JsonProperty("encryptedValue", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptedValue { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeMask</summary>
        [JsonProperty("mask", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Mask { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeToken</summary>
        [JsonProperty("token", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Token { get; set; }

        /// <summary>Developer: This contains a provider ID if multiple providers are used for secure information exchange.</summary>
        [JsonProperty("tokenProviderID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TokenProviderID { get; set; }

        /// <summary>Specifies the method that was used to authenticate the card.</summary>
        [JsonProperty("authenticationMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public EncryptionTokenTypeAuthenticationMethod? AuthenticationMethod { get; set; }

        /// <summary>Don't use this unless it is REALLY ok to not use encryption. Non-secure (plain text) value.</summary>
        [JsonProperty("PlainText", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string PlainText { get; set; }

        /// <summary>Assigned Type: c-0600:ErrorWarning</summary>
        [JsonProperty("ErrorWarning", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ErrorWarning ErrorWarning { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class BankID
    {
        /// <summary>Note: This contains a key required to retrieve the full payment instrument details compliant with PCI DSS standards.</summary>
        [JsonProperty("encryptionKey", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKey { get; set; }

        /// <summary>Developer: This contains a reference to the key generation method being used - this is NOT the key value.</summary>
        [JsonProperty("encryptionKeyMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKeyMethod { get; set; }

        /// <summary>OpenTravel Best Practice: Encryption Method: When using the OpenTravel Encryption element, it is RECOMMENDED that all trading partners be informed of all encryption methods being used in advance of implementation to ensure message processing compatibility.</summary>
        [JsonProperty("encryptionMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionMethod { get; set; }

        /// <summary>Example: 5dfc52b51bd35553df8592078de921bc</summary>
        [JsonProperty("encryptedValue", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptedValue { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeMask</summary>
        [JsonProperty("mask", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Mask { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeToken</summary>
        [JsonProperty("token", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Token { get; set; }

        /// <summary>Developer: This contains a provider ID if multiple providers are used for secure information exchange.</summary>
        [JsonProperty("tokenProviderID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TokenProviderID { get; set; }

        /// <summary>Specifies the method that was used to authenticate the card.</summary>
        [JsonProperty("authenticationMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public EncryptionTokenTypeAuthenticationMethod? AuthenticationMethod { get; set; }

        /// <summary>Don't use this unless it is REALLY ok to not use encryption. Non-secure (plain text) value.</summary>
        [JsonProperty("PlainText", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string PlainText { get; set; }

        /// <summary>Assigned Type: c-0600:ErrorWarning</summary>
        [JsonProperty("ErrorWarning", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ErrorWarning ErrorWarning { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class CardNumber
    {
        /// <summary>Note: This contains a key required to retrieve the full payment instrument details compliant with PCI DSS standards.</summary>
        [JsonProperty("encryptionKey", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKey { get; set; }

        /// <summary>Developer: This contains a reference to the key generation method being used - this is NOT the key value.</summary>
        [JsonProperty("encryptionKeyMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKeyMethod { get; set; }

        /// <summary>OpenTravel Best Practice: Encryption Method: When using the OpenTravel Encryption element, it is RECOMMENDED that all trading partners be informed of all encryption methods being used in advance of implementation to ensure message processing compatibility.</summary>
        [JsonProperty("encryptionMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionMethod { get; set; }

        /// <summary>Example: 5dfc52b51bd35553df8592078de921bc</summary>
        [JsonProperty("encryptedValue", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptedValue { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeMask</summary>
        [JsonProperty("mask", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Mask { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeToken</summary>
        [JsonProperty("token", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Token { get; set; }

        /// <summary>Developer: This contains a provider ID if multiple providers are used for secure information exchange.</summary>
        [JsonProperty("tokenProviderID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TokenProviderID { get; set; }

        /// <summary>Specifies the method that was used to authenticate the card.</summary>
        [JsonProperty("authenticationMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public EncryptionTokenTypeAuthenticationMethod? AuthenticationMethod { get; set; }

        /// <summary>Don't use this unless it is REALLY ok to not use encryption. Non-secure (plain text) value.</summary>
        [JsonProperty("PlainText", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string PlainText { get; set; }

        /// <summary>Assigned Type: c-0600:ErrorWarning</summary>
        [JsonProperty("ErrorWarning", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ErrorWarning ErrorWarning { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class SeriesCode
    {
        /// <summary>Note: This contains a key required to retrieve the full payment instrument details compliant with PCI DSS standards.</summary>
        [JsonProperty("encryptionKey", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKey { get; set; }

        /// <summary>Developer: This contains a reference to the key generation method being used - this is NOT the key value.</summary>
        [JsonProperty("encryptionKeyMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKeyMethod { get; set; }

        /// <summary>OpenTravel Best Practice: Encryption Method: When using the OpenTravel Encryption element, it is RECOMMENDED that all trading partners be informed of all encryption methods being used in advance of implementation to ensure message processing compatibility.</summary>
        [JsonProperty("encryptionMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionMethod { get; set; }

        /// <summary>Example: 5dfc52b51bd35553df8592078de921bc</summary>
        [JsonProperty("encryptedValue", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptedValue { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeMask</summary>
        [JsonProperty("mask", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Mask { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeToken</summary>
        [JsonProperty("token", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Token { get; set; }

        /// <summary>Developer: This contains a provider ID if multiple providers are used for secure information exchange.</summary>
        [JsonProperty("tokenProviderID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TokenProviderID { get; set; }

        /// <summary>Specifies the method that was used to authenticate the card.</summary>
        [JsonProperty("authenticationMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public EncryptionTokenTypeAuthenticationMethod? AuthenticationMethod { get; set; }

        /// <summary>Don't use this unless it is REALLY ok to not use encryption. Non-secure (plain text) value.</summary>
        [JsonProperty("PlainText", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string PlainText { get; set; }

        /// <summary>Assigned Type: c-0600:ErrorWarning</summary>
        [JsonProperty("ErrorWarning", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ErrorWarning ErrorWarning { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class MagneticStripe
    {
        /// <summary>Note: This contains a key required to retrieve the full payment instrument details compliant with PCI DSS standards.</summary>
        [JsonProperty("encryptionKey", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKey { get; set; }

        /// <summary>Developer: This contains a reference to the key generation method being used - this is NOT the key value.</summary>
        [JsonProperty("encryptionKeyMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKeyMethod { get; set; }

        /// <summary>OpenTravel Best Practice: Encryption Method: When using the OpenTravel Encryption element, it is RECOMMENDED that all trading partners be informed of all encryption methods being used in advance of implementation to ensure message processing compatibility.</summary>
        [JsonProperty("encryptionMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionMethod { get; set; }

        /// <summary>Example: 5dfc52b51bd35553df8592078de921bc</summary>
        [JsonProperty("encryptedValue", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptedValue { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeMask</summary>
        [JsonProperty("mask", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Mask { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeToken</summary>
        [JsonProperty("token", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Token { get; set; }

        /// <summary>Developer: This contains a provider ID if multiple providers are used for secure information exchange.</summary>
        [JsonProperty("tokenProviderID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TokenProviderID { get; set; }

        /// <summary>Specifies the method that was used to authenticate the card.</summary>
        [JsonProperty("authenticationMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public EncryptionTokenTypeAuthenticationMethod? AuthenticationMethod { get; set; }

        /// <summary>Don't use this unless it is REALLY ok to not use encryption. Non-secure (plain text) value.</summary>
        [JsonProperty("PlainText", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string PlainText { get; set; }

        /// <summary>Assigned Type: c-0600:ErrorWarning</summary>
        [JsonProperty("ErrorWarning", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ErrorWarning ErrorWarning { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AuthenticationVerification
    {
        /// <summary>Note: This contains a key required to retrieve the full payment instrument details compliant with PCI DSS standards.</summary>
        [JsonProperty("encryptionKey", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKey { get; set; }

        /// <summary>Developer: This contains a reference to the key generation method being used - this is NOT the key value.</summary>
        [JsonProperty("encryptionKeyMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKeyMethod { get; set; }

        /// <summary>OpenTravel Best Practice: Encryption Method: When using the OpenTravel Encryption element, it is RECOMMENDED that all trading partners be informed of all encryption methods being used in advance of implementation to ensure message processing compatibility.</summary>
        [JsonProperty("encryptionMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionMethod { get; set; }

        /// <summary>Example: 5dfc52b51bd35553df8592078de921bc</summary>
        [JsonProperty("encryptedValue", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptedValue { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeMask</summary>
        [JsonProperty("mask", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Mask { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeToken</summary>
        [JsonProperty("token", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Token { get; set; }

        /// <summary>Developer: This contains a provider ID if multiple providers are used for secure information exchange.</summary>
        [JsonProperty("tokenProviderID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TokenProviderID { get; set; }

        /// <summary>Specifies the method that was used to authenticate the card.</summary>
        [JsonProperty("authenticationMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public EncryptionTokenTypeAuthenticationMethod? AuthenticationMethod { get; set; }

        /// <summary>Don't use this unless it is REALLY ok to not use encryption. Non-secure (plain text) value.</summary>
        [JsonProperty("PlainText", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string PlainText { get; set; }

        /// <summary>Assigned Type: c-0600:ErrorWarning</summary>
        [JsonProperty("ErrorWarning", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ErrorWarning ErrorWarning { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Password
    {
        /// <summary>Note: This contains a key required to retrieve the full payment instrument details compliant with PCI DSS standards.</summary>
        [JsonProperty("encryptionKey", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKey { get; set; }

        /// <summary>Developer: This contains a reference to the key generation method being used - this is NOT the key value.</summary>
        [JsonProperty("encryptionKeyMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionKeyMethod { get; set; }

        /// <summary>OpenTravel Best Practice: Encryption Method: When using the OpenTravel Encryption element, it is RECOMMENDED that all trading partners be informed of all encryption methods being used in advance of implementation to ensure message processing compatibility.</summary>
        [JsonProperty("encryptionMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptionMethod { get; set; }

        /// <summary>Example: 5dfc52b51bd35553df8592078de921bc</summary>
        [JsonProperty("encryptedValue", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string EncryptedValue { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeMask</summary>
        [JsonProperty("mask", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Mask { get; set; }

        /// <summary>Assigned Type: c-0600:EncryptionTokenTypeToken</summary>
        [JsonProperty("token", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9a-zA-Z]{1,32}")]
        public string Token { get; set; }

        /// <summary>Developer: This contains a provider ID if multiple providers are used for secure information exchange.</summary>
        [JsonProperty("tokenProviderID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TokenProviderID { get; set; }

        /// <summary>Specifies the method that was used to authenticate the card.</summary>
        [JsonProperty("authenticationMethod", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public EncryptionTokenTypeAuthenticationMethod? AuthenticationMethod { get; set; }

        /// <summary>Don't use this unless it is REALLY ok to not use encryption. Non-secure (plain text) value.</summary>
        [JsonProperty("PlainText", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string PlainText { get; set; }

        /// <summary>Assigned Type: c-0600:ErrorWarning</summary>
        [JsonProperty("ErrorWarning", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ErrorWarning ErrorWarning { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum EncryptionTokenTypeAuthenticationMethod
    {
        [EnumMember(Value = @"SecurityCode")]
        SecurityCode = 0,

        [EnumMember(Value = @"MagneticStripe")]
        MagneticStripe = 1,

    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PersonName
    {
        /// <summary>Type of name of the individual, such as former, nickname, alternate or alias name. Refer to OpenTravel Code List Name Type (NAM).</summary>
        [JsonProperty("type", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Name? Type { get; set; }

        /// <summary>The language the name is represented in, e.g. an Eastern name versus a Western name.</summary>
        [JsonProperty("language", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Language { get; set; }

        /// <summary>Salutation of honorific (e.g. Mr., Mrs., Ms., Miss, Dr.)</summary>
        [JsonProperty("Prefix", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(10)]
        public string Prefix { get; set; }

        /// <summary>Given name, first name or names.</summary>
        [JsonProperty("Given", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(30)]
        public string Given { get; set; }

        /// <summary>The middle name of the person name.</summary>
        [JsonProperty("Middle", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(30)]
        public string Middle { get; set; }

        /// <summary>The surname prefix, e.g \"van der\", \"von\", \"de\".</summary>
        [JsonProperty("SurnamePrefix", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string SurnamePrefix { get; set; }

        /// <summary>Family name, last name. \nNote: Simple form of person name should be used for the full name.\nMay also be used for full name if the sending system does not have the ability to separate a full name into its parts, e.g. the surname element may be used to pass the full name.</summary>
        [JsonProperty("Surname", Required = Required.Always)]
        [Required]
        [StringLength(30, MinimumLength = 1)]
        public string Surname { get; set; }

        /// <summary>Hold various name suffixes and letters (e.g. Jr., Sr., III, Ret., Esq.)</summary>
        [JsonProperty("Suffix", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(10)]
        public string Suffix { get; set; }

        /// <summary>Degree or honors (e.g., Ph.D., M.D.)</summary>
        [JsonProperty("Title", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Title { get; set; }

        /// <summary>Assigned Type: c-0600:Privacy</summary>
        [JsonProperty("Privacy", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Privacy Privacy { get; set; }

        /// <summary>If true, this is the default or primary name within a collection of names.</summary>
        [JsonProperty("defaultInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? DefaultInd { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("Person", typeof(Person))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PersonID
    {
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Person : PersonID
    {
        /// <summary>Date of Birth</summary>
        [JsonProperty("dob", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? Dob { get; set; }

        /// <summary>Gender</summary>
        [JsonProperty("gender", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Gender_Enum? Gender { get; set; }

        /// <summary>Assigned Type: c-0600:PersonName</summary>
        [JsonProperty("PersonName", Required = Required.Always)]
        public PersonName PersonName { get; set; }

        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public ICollection<Address> Address { get; set; }

        [JsonProperty("Telephone", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public ICollection<Telephone> Telephone { get; set; }

        [JsonProperty("Email", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public ICollection<Email> Email { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps</summary>
        [JsonProperty("NextSteps", Required = Required.Always)]
        public NextSteps NextSteps { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>Specifies the ID for the membership program.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class CustomerLoyalty_C0600
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1000, MinimumLength = 1)]
        public string Value { get; set; }

        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:Priority</summary>
        [JsonProperty("priority", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, 300)]
        public int? Priority { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("programId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string ProgramId { get; set; }

        /// <summary>Assigned Type: c-0600:LoyaltyProgramName</summary>
        [JsonProperty("programName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string ProgramName { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("programExtension", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string ProgramExtension { get; set; }

        /// <summary>Assigned Type: c-0600:LoyaltySupplierType</summary>
        [JsonProperty("supplierType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string SupplierType { get; set; }

        /// <summary>Assigned Type: c-0600:LoyaltySupplier</summary>
        [JsonProperty("supplier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Supplier { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("level", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Level { get; set; }

        /// <summary>Assigned Type: c-0600:LoyaltyCardLevel_ATPCO</summary>
        [JsonProperty("loyaltyLevelATPCO", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z0-9]{1,1}")]
        public string LoyaltyLevelATPCO { get; set; }

        /// <summary>Assigned Type: c-0600:NumberSingleDigit</summary>
        [JsonProperty("tier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Tier { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("status", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Status { get; set; }

        [JsonProperty("validatedInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ValidatedInd { get; set; }


    }

    /// <summary>Agencies PCC specific information.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("AgencyPCCDetail", typeof(AgencyPCCDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AgencyPCC
    {
        /// <summary>The identifier for document level idenitification.</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:PseudoCityCode</summary>
        [JsonProperty("pseudoCityCode", Required = Required.Always)]
        [Required]
        [StringLength(10, MinimumLength = 2)]
        [RegularExpression(@"([a-zA-Z0-9]{2,10})")]
        public string PseudoCityCode { get; set; }

        /// <summary>Assigned Type: c-0600:AgencyCode_IATA</summary>
        [JsonProperty("agencyCode", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [RegularExpression(@"([0-9]{8})")]
        public string AgencyCode { get; set; }

        /// <summary>Assigned Type: c-0600:CountryCode_ISO3166</summary>
        [JsonProperty("country", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [RegularExpression(@"[a-zA-Z]{2}")]
        public string Country { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("codeAffinity", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [StringLength(32)]
        public string CodeAffinity { get; set; }

        /// <summary>Assigned Type: c-0600:NonnegativeInteger</summary>
        [JsonProperty("tierLevel", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [Range(0, int.MaxValue)]
        public int? TierLevel { get; set; }

        /// <summary>Assigned Type: c-0600:BSP</summary>
        [JsonProperty("bsp", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Bsp { get; set; }

        /// <summary>Assigned Type: c-0600:WHPS</summary>
        [JsonProperty("whps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Whps { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }

        /// <summary>Assigned Type: c-0600:AirportOrCity</summary>
        [JsonProperty("City", Required = Required.Always)]
        public AirportOrCity City { get; set; }

        /// <summary>Assigned Type: c-0600:Currency</summary>
        [JsonProperty("Currency", Required = Required.Always)]
        public Currency Currency { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Agencies PCC specific information.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AgencyPCCDetail : AgencyPCC
    {
        /// <summary>The name of the Agency</summary>
        [JsonProperty("Name", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Name { get; set; }

        /// <summary>The state or province of the Agency</summary>
        [JsonProperty("StateProv", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(8, MinimumLength = 2)]
        public string StateProv { get; set; }

        /// <summary>Postal\/Zip code of the Agency</summary>
        [JsonProperty("PostalCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string PostalCode { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("History", typeof(History))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class HistoryID
    {
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class History : HistoryID
    {
        [JsonProperty("HistoryItem", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(200)]
        public ICollection<HistoryItem> HistoryItem { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>A transaction resulting in a change of state, value is the reason for the action</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Operation
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("objectType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string ObjectType { get; set; }

        /// <summary>The event that occurred, such as ticketed or repriced</summary>
        [JsonProperty("action", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Action_Enum? Action { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("objectId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string ObjectId { get; set; }


    }

    /// <summary>Actions taken on the object</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum Action_Enum
    {
        [EnumMember(Value = @"Created")]
        Created = 0,

        [EnumMember(Value = @"Updated")]
        Updated = 1,

        [EnumMember(Value = @"Cancelled")]
        Cancelled = 2,

        [EnumMember(Value = @"Deleted")]
        Deleted = 3,

        [EnumMember(Value = @"Read")]
        Read = 4,

    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class HistoryItem
    {
        /// <summary>Uniquely identifies this item in the encomposing message</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: ota2:UTCDateTime</summary>
        [JsonProperty("datetime", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? Datetime { get; set; }

        /// <summary>Reference to the object that got changed. If missing, the parent object was changed.</summary>
        [JsonProperty("objectRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ObjectRef { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("trackingId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string TrackingId { get; set; }

        [JsonProperty("Operation", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public ICollection<Operation> Operation { get; set; }

        /// <summary>who (person or event) that made the change, such as the agent sign-on</summary>
        [JsonProperty("UserIdentity", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string UserIdentity { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    //[JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("Reservation", typeof(Reservation))]
    [JsonInheritanceAttribute("ReservationDetail", typeof(ReservationDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ReservationID
    {
        /// <summary>Internal ID</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }

        /// <summary>Assigned Type: c-0600:ProviderLocator</summary>
        [JsonProperty("ReservationLocator", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ProviderLocator ReservationLocator { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class CreateReserv {

        [JsonProperty("Reservation", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Reservation CreateReq { get; set; }
    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Reservation : ReservationID
    {
        [JsonProperty("Order", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public List<OrderDetail> Order { get; set; }

        [JsonProperty("Traveler", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(300)]
        public List<Traveler> Traveler { get; set; }

        [JsonProperty("TravelerProduct", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(3000)]
        public ICollection<TravelerProductID> TravelerProduct { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps_Summary</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }

        /// <summary>Assigned Type: c-0600:Result_Summary</summary>
        [JsonProperty("Result", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Result Result { get; set; }

        [JsonProperty("ReservationComment", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(1000)]
        public List<ReservationComment> ReservationComment { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ReservationDetail : Reservation
    {
        /// <summary>Assigned Type: c-0600:Remarks_Summary</summary>
        [JsonProperty("Remarks", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Remarks Remarks { get; set; }

        /// <summary>Assigned Type: c-0601:Comments</summary>
        [JsonProperty("Comments", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Comments Comments { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Fields that are associated with an itinerary that are stored in a remark object</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("AssociatedItineraryRemark", typeof(AssociatedItineraryRemark))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AssociatedItineraryRemarkID
    {
        [JsonProperty("ID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ID { get; set; }


    }

    /// <summary>Fields that are associated with an itinerary that are stored in a remark object</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AssociatedItineraryRemark : AssociatedItineraryRemarkID
    {
        /// <summary>Assigned Type: res-1000:ItineraryType_Enum</summary>
        [JsonProperty("ItineraryType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public ItineraryType_Enum? ItineraryType { get; set; }

        /// <summary>The textual remark</summary>
        [JsonProperty("Remark", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Remark { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>Air, Car, or Hotel</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum ItineraryType_Enum
    {
        [EnumMember(Value = @"Air")]
        Air = 0,

        [EnumMember(Value = @"Car")]
        Car = 1,

        [EnumMember(Value = @"Hotel")]
        Hotel = 2,

    }

    /// <summary>A set of offers that has been confirmed through payment or another form of exchange</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("Order", typeof(Order))]
    [JsonInheritanceAttribute("OrderDetail", typeof(OrderDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OrderID
    {
        /// <summary>Internally referenced ID</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>A set of offers that has been confirmed through payment or another form of exchange</summary>
        [JsonProperty("orderRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string OrderRef { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }

        [JsonProperty("OrderLocator", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public ICollection<OrderLocatorID> OrderLocator { get; set; }


    }

    /// <summary>A set of offers that has been confirmed through payment or another form of exchange</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Order : OrderID
    {
        /// <summary>The status of the reservation, order, or offer</summary>
        [JsonProperty("transactionStatus", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public TransactionStatus? TransactionStatus { get; set; }

        /// <summary>Assigned Type: c-0600:PseudoCityCode</summary>
        [JsonProperty("owningPCC", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(10, MinimumLength = 2)]
        [RegularExpression(@"([a-zA-Z0-9]{2,10})")]
        public string OwningPCC { get; set; }

        [JsonProperty("Offer", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public List<Offer> Offer { get; set; }

        [JsonProperty("TotalPrice", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(3)]
        public List<TotalPrice> TotalPrice { get; set; }

        [JsonProperty("Payment", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public List<Payment> Payment { get; set; }

        /// <summary>Assigned Type: res-1000:AssociatedItineraryRemark</summary>
        [JsonProperty("AssociatedItineraryRemark", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public AssociatedItineraryRemarkID AssociatedItineraryRemark { get; set; }

        [JsonProperty("Traveler", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(300)]
        public List<Traveler> Traveler { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps_Summary</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }

        /// <summary>Assigned Type: c-0600:Result_Summary</summary>
        [JsonProperty("Result", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Result Result { get; set; }

        /// <summary>Assigned Type: res-1000:OTA</summary>
        [JsonProperty("OTA", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public OTA OTA { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>A set of offers that has been confirmed through payment or another form of exchange</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OrderDetail : Order
    {
        /// <summary>Assigned Type: org-0600:TravelAgency</summary>
        [JsonProperty("TravelAgency", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public TravelAgencyID TravelAgency { get; set; }

        /// <summary>Assigned Type: res-1000:VendorRequiredData</summary>
        [JsonProperty("VendorRequiredData", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public VendorRequiredDataID VendorRequiredData { get; set; }

        /// <summary>Assigned Type: res-1000:OrderMetaData</summary>
        [JsonProperty("OrderMetaData", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public OrderMetaData OrderMetaData { get; set; }

        [JsonProperty("BackOffice", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(20)]
        public BackOffice BackOffice { get; set; }

        /// <summary>Assigned Type: c-0600:History</summary>
        [JsonProperty("History", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public HistoryID History { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>A set of offers that has been confirmed through payment or another form of exchange</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OrderQueryCompleteTransaction
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("vendorVersionNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string VendorVersionNumber { get; set; }

        /// <summary>Assigned Type: res-1000:OrderTransaction</summary>
        [JsonProperty("OrderTransaction", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public OrderTransaction OrderTransaction { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>MAY NOT NEED THIS - IF SO SHOULD BE IN ORDER\nContains specific custom information needed to book or sent back from retrieve for the different types of order(s)</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("VendorRequiredData", typeof(VendorRequiredData))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class VendorRequiredDataID
    {
        /// <summary>Internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }


    }

    /// <summary>MAY NOT NEED THIS - IF SO SHOULD BE IN ORDER\nContains specific custom information needed to book or sent back from retrieve for the different types of order(s)</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class VendorRequiredData : VendorRequiredDataID
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("vendorVersionNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string VendorVersionNumber { get; set; }

        /// <summary>Assigned Type: res-1000:OrderTransaction</summary>
        [JsonProperty("OrderTransaction", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public OrderTransaction OrderTransaction { get; set; }

        /// <summary>Assigned Type: c-0600:Result_Summary</summary>
        [JsonProperty("Result", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Result Result { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps_Summary</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>TODO</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("Payment", typeof(Payment))]
    [JsonInheritanceAttribute("PaymentAcknowledgement", typeof(PaymentAcknowledgement))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PaymentID
    {
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }


    }

    /// <summary>TODO</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Payment : PaymentID
    {
        /// <summary>What the payment was for</summary>
        [JsonProperty("OfferRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string OfferRef { get; set; }

        /// <summary>Assigned Type: c-0600:CurrencyAmount</summary>
        [JsonProperty("Amount", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CurrencyAmount Amount { get; set; }

        [JsonProperty("FormOfPayment", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        [MaxLength(5)]
        public List<FormOfPaymentPaymentCard> FormOfPayment { get; set; } = new List<FormOfPaymentPaymentCard>();

        [JsonProperty("Person", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(99)]
        public ICollection<PersonID> Person { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps_Summary</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }

        /// <summary>Assigned Type: c-0600:Result_Summary</summary>
        [JsonProperty("Result", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Result Result { get; set; }

        /// <summary>Assigned Type: c-0600:Commission</summary>
        [JsonProperty("Commission", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Commission Commission { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>TODO</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PaymentAcknowledgement : Payment
    {
        /// <summary>Assigned Type: res-1000:Receipt</summary>
        [JsonProperty("Receipt", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ReceiptID Receipt { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the payment or guarantee information for a reservation.  If zero dollars and in the amount then the payment information acts as a guarantee.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("Receipt", typeof(Receipt))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ReceiptID
    {
        /// <summary>The verification number.</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }


    }

    /// <summary>Specifies the payment or guarantee information for a reservation.  If zero dollars and in the amount then the payment information acts as a guarantee.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Receipt : ReceiptID
    {
        [JsonProperty("dateTime", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? DateTime { get; set; }

        /// <summary>Use only if the receipt is not contained by a product.</summary>
        [JsonProperty("ProductRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ProductRef { get; set; }

        [JsonProperty("Confirmation", Required = Required.Always)]
        [Required]
        [MinLength(1)]
        public ICollection<ConfirmationID> Confirmation { get; set; } = new Collection<ConfirmationID>();

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>The type of end transact indicator as specified by the 2030 documentation</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum EndTransactAction
    {
        [EnumMember(Value = @"E")]
        E = 0,

        [EnumMember(Value = @"C")]
        C = 1,

        [EnumMember(Value = @"Q")]
        Q = 2,

        [EnumMember(Value = @"P")]
        P = 3,

        [EnumMember(Value = @"R")]
        R = 4,

    }

    /// <summary>The status of the reservation, order, or offer</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum TransactionStatus
    {
        [EnumMember(Value = @"Book")]
        Book = 0,

        [EnumMember(Value = @"Quote")]
        Quote = 1,

        [EnumMember(Value = @"Hold")]
        Hold = 2,

        [EnumMember(Value = @"Initiate")]
        Initiate = 3,

        [EnumMember(Value = @"Ignore")]
        Ignore = 4,

        [EnumMember(Value = @"Modify")]
        Modify = 5,

        [EnumMember(Value = @"Commit")]
        Commit = 6,

        [EnumMember(Value = @"Cancel")]
        Cancel = 7,

        [EnumMember(Value = @"CommitOverrideEdits")]
        CommitOverrideEdits = 8,

        [EnumMember(Value = @"VerifyPrice")]
        VerifyPrice = 9,

        [EnumMember(Value = @"Ticket")]
        Ticket = 10,

    }

    /// <summary>The type of transaction - ET end transaction or I ignore</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum TransactionType
    {
        [EnumMember(Value = @"ET")]
        ET = 0,

        [EnumMember(Value = @"I")]
        I = 1,

    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class EndTransact
    {
        /// <summary>The type of transaction - ET end transaction or I ignore</summary>
        [JsonProperty("transactionType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public TransactionType? TransactionType { get; set; }

        /// <summary>Assigned Type: res-1000:ReceivedFrom</summary>
        [JsonProperty("receivedFrom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string ReceivedFrom { get; set; }

        /// <summary>The type of end transact indicator as specified by the 2030 documentation</summary>
        [JsonProperty("endTransactAction", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public EndTransactAction? EndTransactAction { get; set; }

        /// <summary>Assigned Type: res-1000:OrderLocator</summary>
        [JsonProperty("OrderLocator", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public OrderLocatorID OrderLocator { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps_Summary</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }

        /// <summary>Assigned Type: c-0600:Result_Summary</summary>
        [JsonProperty("Result", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Result Result { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OrderTransaction
    {
        /// <summary>The type of transaction - ET end transaction or I ignore</summary>
        [JsonProperty("transactionType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public TransactionType? TransactionType { get; set; }

        /// <summary>Assigned Type: res-1000:ReceivedFrom</summary>
        [JsonProperty("receivedFrom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string ReceivedFrom { get; set; }

        /// <summary>The type of end transact indicator as specified by the 2030 documentation</summary>
        [JsonProperty("endTransactAction", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public EndTransactAction? EndTransactAction { get; set; }

        /// <summary>Assigned Type: res-1000:OrderLocator</summary>
        [JsonProperty("OrderLocator", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public OrderLocatorID OrderLocator { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps_Summary</summary>
        [JsonProperty("NextSteps", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public NextSteps NextSteps { get; set; }

        /// <summary>Assigned Type: c-0600:Result_Summary</summary>
        [JsonProperty("Result", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Result Result { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PrimaryTraveler
    {
        /// <summary>The reference to the Traveler</summary>
        [JsonProperty("TravelerRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<string> TravelerRef { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>Standard identifier, but includes the PNR Record Locator</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("SystemLocator", typeof(SystemLocator))]
    [JsonInheritanceAttribute("SystemLocatorDetail", typeof(SystemLocatorDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class SystemLocatorID
    {
        /// <summary>Internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }


    }

    /// <summary>Standard identifier, but includes the PNR Record Locator</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("OrderLocator", typeof(OrderLocator))]
    [JsonInheritanceAttribute("OrderLocatorDetail", typeof(OrderLocatorDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OrderLocatorID
    {
        /// <summary>Internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }


    }

    /// <summary>Standard identifier, but includes the PNR Record Locator</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("ReservationLocator", typeof(ReservationLocator))]
    [JsonInheritanceAttribute("ReservationLocatorDetail", typeof(ReservationLocatorDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ReservationLocatorID
    {
        /// <summary>Internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }


    }

    /// <summary>Standard identifier, but includes the PNR Record Locator</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("VendorLocator", typeof(VendorLocator))]
    [JsonInheritanceAttribute("VendorLocatorDetail", typeof(VendorLocatorDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class VendorLocatorID
    {
        /// <summary>Internally referenced id</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }


    }

    /// <summary>Standard identifier, but includes the PNR Record Locator</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class SystemLocator : SystemLocatorID
    {
        /// <summary>Assigned Type: c-0600:Locator</summary>
        [JsonProperty("Locator", Required = Required.Always)]
        public Locator Locator { get; set; }

        /// <summary>Assigned Type: res-1000:PrimaryTraveler</summary>
        [JsonProperty("PrimaryTraveler", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PrimaryTraveler PrimaryTraveler { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Standard identifier, but includes the PNR Record Locator</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OrderLocator : OrderLocatorID
    {
        /// <summary>Assigned Type: c-0600:Locator</summary>
        [JsonProperty("Locator", Required = Required.Always)]
        public Locator Locator { get; set; }

        /// <summary>Assigned Type: res-1000:PrimaryTraveler</summary>
        [JsonProperty("PrimaryTraveler", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PrimaryTraveler PrimaryTraveler { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Standard identifier, but includes the PNR Record Locator</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ReservationLocator : ReservationLocatorID
    {
        /// <summary>Assigned Type: c-0600:Locator</summary>
        [JsonProperty("Locator", Required = Required.Always)]
        public Locator Locator { get; set; }

        /// <summary>Assigned Type: res-1000:PrimaryTraveler</summary>
        [JsonProperty("PrimaryTraveler", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PrimaryTraveler PrimaryTraveler { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Standard identifier, but includes the PNR Record Locator</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class VendorLocator : VendorLocatorID
    {
        /// <summary>Assigned Type: c-0600:Locator</summary>
        [JsonProperty("Locator", Required = Required.Always)]
        public Locator Locator { get; set; }

        /// <summary>Assigned Type: res-1000:PrimaryTraveler</summary>
        [JsonProperty("PrimaryTraveler", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PrimaryTraveler PrimaryTraveler { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Standard identifier, but includes the PNR Record Locator</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class SystemLocatorDetail : SystemLocator
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("travelerSurname", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TravelerSurname { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("travelerGivenName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TravelerGivenName { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("travelerEmail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TravelerEmail { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Standard identifier, but includes the PNR Record Locator</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OrderLocatorDetail : OrderLocatorID
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("travelerSurname", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TravelerSurname { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("travelerGivenName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TravelerGivenName { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("travelerEmail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TravelerEmail { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Standard identifier, but includes the PNR Record Locator</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ReservationLocatorDetail : ReservationLocatorID
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("travelerSurname", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TravelerSurname { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("travelerGivenName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TravelerGivenName { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("travelerEmail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TravelerEmail { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Standard identifier, but includes the PNR Record Locator</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class VendorLocatorDetail : VendorLocatorID
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("travelerSurname", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TravelerSurname { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("travelerGivenName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TravelerGivenName { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("travelerEmail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TravelerEmail { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>The relationship mapping a traveler to a specific product. Used for SSR, OSI and other specific offerings</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("TravelerProduct", typeof(TravelerProduct))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelerProductID
    {
        [JsonProperty("id", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }


    }

    /// <summary>The relationship mapping a traveler to a specific product. Used for SSR, OSI and other specific offerings</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelerProduct : TravelerProductID
    {
        /// <summary>A pointer to the Traveler id</summary>
        [JsonProperty("TravelerRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TravelerRef { get; set; }

        /// <summary>A pointer to the Product id</summary>
        [JsonProperty("ProductRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ProductRef { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class BackOffice
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("dataType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string DataType { get; set; }

        [JsonProperty("NameValuePair", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public List<NameValuePair> NameValuePair { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>DRAFT Meta Data about this Order</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OrderMetaData
    {
        /// <summary>Assigned Type: ota2:UTCDate</summary>
        [JsonProperty("autoDeleteDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? AutoDeleteDate { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OTA
    {
        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum CommentSourceEnum
    {
        [EnumMember(Value = @"Agency")]
        Agency = 0,

        [EnumMember(Value = @"Supplier")]
        Supplier = 1,

        [EnumMember(Value = @"Traveler")]
        Traveler = 2,

    }

    /// <summary>Textual information.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("ReservationComment", typeof(ReservationComment))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ReservationCommentID
    {
        /// <summary>Local indentifier within a given message for this object.</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }


    }

    /// <summary>Textual information.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ReservationComment : ReservationCommentID
    {
        /// <summary>The soure of the comment,  \"Agency\" or  \"Supplier\" or  \"Traveler\"</summary>
        [JsonProperty("commentSource", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public CommentSourceEnum? CommentSource { get; set; }

        /// <summary>Determines who the comment should be shared with.</summary>
        [JsonProperty("shareWith", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public ShareWithEnum? ShareWith { get; set; }

        /// <summary>Assigned Type: c-0600:TinyStrings</summary>
        [JsonProperty("shareWithSupplier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<string> ShareWithSupplier { get; set; }

        [JsonProperty("Comment", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(1000)]
        public List<Comment> Comment { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class BankAccount
    {
        [JsonProperty("listBankAccountType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public List_BankAccountType_Enum_Base? ListBankAccountType { get; set; }

        [JsonProperty("listBankAccountTypeExtension", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128, MinimumLength = 1)]
        public string ListBankAccountTypeExtension { get; set; }

        /// <summary>Implementer: Place a code value in this attribute if you have selected the \"Other_\" value in the enumerated list. Note that this value should be known to your trading partners.</summary>
        [JsonProperty("code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Code { get; set; }

        /// <summary>Example: Party Supplies</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        /// <summary>Example: Mylar Baloons</summary>
        [JsonProperty("descriptionDetail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string DescriptionDetail { get; set; }

        /// <summary>Implementer: This may be a source authority\/ owner name, ID or code.</summary>
        [JsonProperty("sourceName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string SourceName { get; set; }

        /// <summary>Implementer: This is A URL to the code source authority\/ owner.</summary>
        [JsonProperty("sourceURL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string SourceURL { get; set; }

        /// <summary>Example: OpenTravel 2012A CodeList</summary>
        [JsonProperty("resourceName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ResourceName { get; set; }

        /// <summary>Implementer: This is A URL to the code resource.</summary>
        [JsonProperty("resourceURL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ResourceURL { get; set; }

        /// <summary>Implementer: This is used for a database or key ID for the code item (if it is different from the @CodeExtension) in relationship to the obsolete code indicator.</summary>
        [JsonProperty("uniqueID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string UniqueID { get; set; }

        /// <summary>Implementer: If true, this item is obsolete and should be removed from the receiving system.</summary>
        [JsonProperty("removalInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? RemovalInd { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class BankAcct
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("checkNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string CheckNumber { get; set; }

        /// <summary>Assigned Type: c-0600:Privacy</summary>
        [JsonProperty("PrivacyGroup", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Privacy PrivacyGroup { get; set; }

        /// <summary>Example: Joseph L Smith</summary>
        [JsonProperty("BankAcctName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string BankAcctName { get; set; }

        /// <summary>Assigned Type: c-0600:BankAcctNumber</summary>
        [JsonProperty("BankAcctNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public BankAcctNumber BankAcctNumber { get; set; }

        /// <summary>Assigned Type: c-0600:BankID</summary>
        [JsonProperty("BankID", Required = Required.Always)]
        public BankID BankID { get; set; }

        /// <summary>Assigned Type: fin-0900:BankAccount</summary>
        [JsonProperty("Type", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public BankAccount Type { get; set; }

        /// <summary>Implementer: If true, checks are accepted.</summary>
        [JsonProperty("checksAcceptedInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ChecksAcceptedInd { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ConjunctionTicketNbr
    {
        /// <summary>Assigned Type: fin-0900:Coupons</summary>
        [JsonProperty("coupons", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<string> Coupons { get; set; }

        /// <summary>Conjunction ticket number in case a conjunction ticket is exchanged.</summary>
        [JsonProperty("ConjunctionTicketNbr", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string ConjunctionTicketNbr1 { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>Program rewarding frequent use by accumulating credits for services provided by vendors.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class CustomerLoyalty_FIN0900
    {
        /// <summary>A reference placeholder for this loyalty membership.</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("programID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string ProgramID { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("membershipID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string MembershipID { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("travelSector", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string TravelSector { get; set; }

        /// <summary>Assigned Type: fin-0900:VendorCodes</summary>
        [JsonProperty("vendorCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<string> VendorCode { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("allianceLoyaltyLevelName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string AllianceLoyaltyLevelName { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("customerType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string CustomerType { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("customerValue", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string CustomerValue { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("password", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Password { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("loyalLevel", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string LoyalLevel { get; set; }

        /// <summary>Provides a numeric code assigned to a particular loyalty level.</summary>
        [JsonProperty("loyalLevelCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? LoyalLevelCode { get; set; }

        /// <summary>Indicates if program is affiliated with a group of related offers accumulating credits.</summary>
        [JsonProperty("singleVendorInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public SingleVendorInd? SingleVendorInd { get; set; }

        /// <summary>Indicates when the member signed up for the loyalty program.</summary>
        [JsonProperty("SignupDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? SignupDate { get; set; }

        /// <summary>Indicates the starting date.</summary>
        [JsonProperty("EffectiveDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? EffectiveDate { get; set; }

        /// <summary>Indicates the ending date.</summary>
        [JsonProperty("ExpireDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? ExpireDate { get; set; }

        /// <summary>Permission for sharing data for marketing purposes.</summary>
        [JsonProperty("shareMarketing", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoInherit_Enum? ShareMarketing { get; set; }

        /// <summary>Permission for sharing data for synchronization of information held by other travel service providers.</summary>
        [JsonProperty("shareSync", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoInherit_Enum? ShareSync { get; set; }

        /// <summary>When yes, a customer has explicitly opted out of marketing communication. This is used in combination with the ShareAllMarketInd and only one of these attributes should have a value of yes.</summary>
        [JsonProperty("optOutInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public YesNoInherit_Enum? OptOutInd { get; set; }

        /// <summary>When Yes, then the user has opted In</summary>
        [JsonProperty("optInStatus", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public OptInStatus_Enum? OptInStatus { get; set; }

        /// <summary>The datetime of receiving the opt in notice</summary>
        [JsonProperty("optInDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? OptInDate { get; set; }

        /// <summary>The datetime the opt out notice was received</summary>
        [JsonProperty("optOutDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? OptOutDate { get; set; }

        /// <summary>When true, indicates that the ExpireDate is the first day after the applicable period (e.g. when expire date is Oct 15  the last date of the period is Oct 14).</summary>
        [JsonProperty("expireDateExclusiveInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ExpireDateExclusiveInd { get; set; }

        /// <summary>When true, indicates this is the primary customer loyalty program and when false, indicates this is not the primary customer loyalty program.</summary>
        [JsonProperty("primaryLoyaltyInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? PrimaryLoyaltyInd { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class DirectBill
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("directBill_ID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string DirectBill_ID { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("billingNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string BillingNumber { get; set; }

        /// <summary>Assigned Type: c-0600:Privacy</summary>
        [JsonProperty("PrivacyGroup", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Privacy PrivacyGroup { get; set; }

        /// <summary>Assigned Type: c-0600:Company_Name</summary>
        [JsonProperty("CompanyName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Company_Name CompanyName { get; set; }

        /// <summary>Assigned Type: c-0600:Address</summary>
        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Address Address { get; set; }

        /// <summary>Assigned Type: c-0600:Email</summary>
        [JsonProperty("Email", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Email Email { get; set; }

        /// <summary>Assigned Type: c-0600:Telephone</summary>
        [JsonProperty("Telephone", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Telephone Telephone { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("FormOfPayment", typeof(FormOfPayment))]
    [JsonInheritanceAttribute("FormOfPaymentCash", typeof(FormOfPaymentCash))]
    [JsonInheritanceAttribute("FormOfPaymentPaymentCard", typeof(FormOfPaymentPaymentCard))]
    [JsonInheritanceAttribute("FormOfPaymentBankAccount", typeof(FormOfPaymentBankAccount))]
    [JsonInheritanceAttribute("FormOfPaymentLoyaltyRedemption", typeof(FormOfPaymentLoyaltyRedemption))]
    [JsonInheritanceAttribute("FormOfPaymentDirectBill", typeof(FormOfPaymentDirectBill))]
    [JsonInheritanceAttribute("FormOfPaymentVoucher", typeof(FormOfPaymentVoucher))]
    [JsonInheritanceAttribute("FormOfPaymentMiscChargeOrder", typeof(FormOfPaymentMiscChargeOrder))]
    [JsonInheritanceAttribute("FormOfPaymentTicket", typeof(FormOfPaymentTicket))]
    [JsonInheritanceAttribute("FormOfPaymentAgencyAccount", typeof(FormOfPaymentAgencyAccount))]
    [JsonInheritanceAttribute("FormOfPaymentBSP", typeof(FormOfPaymentBSP))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class FormOfPaymentID
    {
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Specifies the form of payment.</summary>
        [JsonProperty("formOfPaymentRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string FormOfPaymentRef { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("GuaranteeCash", typeof(GuaranteeCash))]
    [JsonInheritanceAttribute("GuaranteePaymentCard", typeof(GuaranteePaymentCard))]
    [JsonInheritanceAttribute("GuaranteeBankAccount", typeof(GuaranteeBankAccount))]
    [JsonInheritanceAttribute("GuaranteeLoyaltyRedemption", typeof(GuaranteeLoyaltyRedemption))]
    [JsonInheritanceAttribute("GuaranteeDirectBill", typeof(GuaranteeDirectBill))]
    [JsonInheritanceAttribute("GuaranteeVoucher", typeof(GuaranteeVoucher))]
    [JsonInheritanceAttribute("GuaranteeMiscChargeOrder", typeof(GuaranteeMiscChargeOrder))]
    [JsonInheritanceAttribute("GuaranteeTicket", typeof(GuaranteeTicket))]
    [JsonInheritanceAttribute("GuaranteeAgencyAccount", typeof(GuaranteeAgencyAccount))]
    [JsonInheritanceAttribute("GuaranteeBSP", typeof(GuaranteeBSP))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuaranteeID
    {
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Specifies the form of payment.</summary>
        [JsonProperty("formOfPaymentRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string FormOfPaymentRef { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class FormOfPayment : FormOfPaymentID
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("costCenterID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string CostCenterID { get; set; }

        /// <summary>This is used to indicate either a charge, reserve (deposit) or refund.</summary>
        [JsonProperty("paymentTransactionTypeCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public PaymentTransactionTypeCode? PaymentTransactionTypeCode { get; set; }

        /// <summary>Assigned Type: c-0600:OTACode</summary>
        [JsonProperty("guaranteeTypeCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}")]
        public string GuaranteeTypeCode { get; set; }

        /// <summary>The type of guarantee, values from uAPI</summary>
        [JsonProperty("guaranteeType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public GuaranteeType_Enum_FIN0900? GuaranteeType { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("guaranteeID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string GuaranteeID { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("remark", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Remark { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("number", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Number { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("billingReference", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string BillingReference { get; set; }

        /// <summary>Assigned Type: c-0600:Privacy</summary>
        [JsonProperty("Privacy", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Privacy Privacy { get; set; }

        /// <summary>Assigned Type: c-0600:Amount_Summary</summary>
        [JsonProperty("Amount", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Amount Amount { get; set; }

        /// <summary>Assigned Type: c-0600:Result_Summary</summary>
        [JsonProperty("Result", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Result Result { get; set; }

        /// <summary>When true, indicates this represents a guarantee rather than a payment form.</summary>
        [JsonProperty("guaranteeIndicatorInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? GuaranteeIndicatorInd { get; set; }

        /// <summary>Indicates whether the form of payment can be reused or not. True indicates it can be reused.</summary>
        [JsonProperty("reusableInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ReusableInd { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class FormOfPaymentCash : FormOfPayment
    {
        /// <summary>Indicates that cash is being used as the form of payment.</summary>
        [JsonProperty("cashInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? CashInd { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuaranteeCash : GuaranteeID
    {
        /// <summary>Indicates that cash is being used as the form of payment.</summary>
        [JsonProperty("cashInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? CashInd { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class FormOfPaymentPaymentCard : FormOfPayment
    {
        /// <summary>Assigned Type: fin-0900:PaymentCard</summary>
        [JsonProperty("PaymentCard", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PaymentCard PaymentCard { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuaranteePaymentCard : GuaranteeID
    {
        /// <summary>Assigned Type: fin-0900:PaymentCard</summary>
        [JsonProperty("PaymentCard", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PaymentCard PaymentCard { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class FormOfPaymentBankAccount : FormOfPayment
    {
        /// <summary>Assigned Type: fin-0900:BankAcct</summary>
        [JsonProperty("BankAcct", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public BankAcct BankAcct { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuaranteeBankAccount : GuaranteeID
    {
        /// <summary>Assigned Type: fin-0900:BankAcct</summary>
        [JsonProperty("BankAcct", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public BankAcct BankAcct { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class FormOfPaymentLoyaltyRedemption : FormOfPayment
    {
        /// <summary>The quantity of loyalty units being redeemed.</summary>
        [JsonProperty("redemptionQuantity", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? RedemptionQuantity { get; set; }

        /// <summary>Assigned Type: fin-0900:LoyaltyCertificateNumberGroup</summary>
        [JsonProperty("LoyaltyCertificateNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public LoyaltyCertificateNumberGroup LoyaltyCertificateNumber { get; set; }

        /// <summary>Assigned Type: fin-0900:PromotionCodeGroup</summary>
        [JsonProperty("PromotionCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PromotionCodeGroup PromotionCode { get; set; }

        [JsonProperty("LoyaltyCertificate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(9)]
        public ICollection<LoyaltyCertificate> LoyaltyCertificate { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuaranteeLoyaltyRedemption : GuaranteeID
    {
        /// <summary>The quantity of loyalty units being redeemed.</summary>
        [JsonProperty("redemptionQuantity", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? RedemptionQuantity { get; set; }

        /// <summary>Assigned Type: fin-0900:LoyaltyCertificateNumberGroup</summary>
        [JsonProperty("LoyaltyCertificateNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public LoyaltyCertificateNumberGroup LoyaltyCertificateNumber { get; set; }

        /// <summary>Assigned Type: fin-0900:PromotionCodeGroup</summary>
        [JsonProperty("PromotionCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PromotionCodeGroup PromotionCode { get; set; }

        [JsonProperty("LoyaltyCertificate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(9)]
        public ICollection<LoyaltyCertificate> LoyaltyCertificate { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class FormOfPaymentDirectBill : FormOfPayment
    {
        /// <summary>Assigned Type: fin-0900:DirectBill</summary>
        [JsonProperty("DirectBill", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public DirectBill DirectBill { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuaranteeDirectBill : GuaranteeID
    {
        /// <summary>Assigned Type: fin-0900:DirectBill</summary>
        [JsonProperty("DirectBill", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public DirectBill DirectBill { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class FormOfPaymentVoucher : FormOfPayment
    {
        /// <summary>Specifies if the voucher is for full credit or a group day or a monetary amount or regular voucher.</summary>
        [JsonProperty("voucherType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public VoucherType? VoucherType { get; set; }

        /// <summary>Assigned Type: fin-0900:VoucherGroup</summary>
        [JsonProperty("VoucherGroup", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public VoucherGroup VoucherGroup { get; set; }

        /// <summary>Assigned Type: c-0600:Company_Name</summary>
        [JsonProperty("BillingAccountName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Company_Name BillingAccountName { get; set; }

        /// <summary>Assigned Type: c-0600:Address</summary>
        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Address Address { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuaranteeVoucher : GuaranteeID
    {
        /// <summary>Specifies if the voucher is for full credit or a group day or a monetary amount or regular voucher.</summary>
        [JsonProperty("voucherType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public VoucherType? VoucherType { get; set; }

        /// <summary>Assigned Type: fin-0900:VoucherGroup</summary>
        [JsonProperty("VoucherGroup", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public VoucherGroup VoucherGroup { get; set; }

        /// <summary>Assigned Type: c-0600:Company_Name</summary>
        [JsonProperty("BillingAccountName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Company_Name BillingAccountName { get; set; }

        /// <summary>Assigned Type: c-0600:Address</summary>
        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Address Address { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class FormOfPaymentMiscChargeOrder : FormOfPayment
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("ticketNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TicketNumber { get; set; }

        /// <summary>Assigned Type: fin-0900:OriginalIssueAttributes</summary>
        [JsonProperty("OriginalIssueAttributes", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public OriginalIssueAttributes OriginalIssueAttributes { get; set; }

        /// <summary>Indicates if a paper or electronic MCO exists.</summary>
        [JsonProperty("paperMCO_ExistInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? PaperMCO_ExistInd { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuaranteeMiscChargeOrder : GuaranteeID
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("ticketNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TicketNumber { get; set; }

        /// <summary>Assigned Type: fin-0900:OriginalIssueAttributes</summary>
        [JsonProperty("OriginalIssueAttributes", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public OriginalIssueAttributes OriginalIssueAttributes { get; set; }

        /// <summary>Indicates if a paper or electronic MCO exists.</summary>
        [JsonProperty("paperMCO_ExistInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? PaperMCO_ExistInd { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class FormOfPaymentTicket : FormOfPayment
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("ticketNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TicketNumber { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("reasonForReroute", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string ReasonForReroute { get; set; }

        /// <summary>Assigned Type: fin-0900:OriginalIssueAttributes</summary>
        [JsonProperty("OriginalIssueAttributes", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public OriginalIssueAttributes OriginalIssueAttributes { get; set; }

        /// <summary>Assigned Type: fin-0900:Rerouting</summary>
        [JsonProperty("Rerouting", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Rerouting? Rerouting { get; set; }

        [JsonProperty("ConjunctionTicketNbr", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(16)]
        public ICollection<ConjunctionTicketNbr> ConjunctionTicketNbr { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuaranteeTicket : GuaranteeID
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("ticketNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string TicketNumber { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("reasonForReroute", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string ReasonForReroute { get; set; }

        /// <summary>Assigned Type: fin-0900:OriginalIssueAttributes</summary>
        [JsonProperty("OriginalIssueAttributes", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public OriginalIssueAttributes OriginalIssueAttributes { get; set; }

        /// <summary>Assigned Type: fin-0900:Rerouting</summary>
        [JsonProperty("Rerouting", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Rerouting? Rerouting { get; set; }

        [JsonProperty("ConjunctionTicketNbr", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(16)]
        public ICollection<ConjunctionTicketNbr> ConjunctionTicketNbr { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class FormOfPaymentAgencyAccount : FormOfPayment
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("agencyId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string AgencyId { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("accountPassword", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string AccountPassword { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuaranteeAgencyAccount : GuaranteeID
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("agencyId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string AgencyId { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("accountPassword", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string AccountPassword { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class FormOfPaymentBSP : FormOfPayment
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("accountNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string AccountNumber { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Specifies the form of payment.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class GuaranteeBSP : GuaranteeID
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("accountNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string AccountNumber { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>Source: OpenTravel</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class List_BankAccountType_Enum
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public List_BankAccountType_Enum_Base? Value { get; set; }

        [JsonProperty("extension", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128, MinimumLength = 1)]
        public string Extension { get; set; }


    }

    /// <summary>Source: OpenTravel</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum List_BankAccountType_Enum_Base
    {
        [EnumMember(Value = @"Savings")]
        Savings = 0,

        [EnumMember(Value = @"Investment")]
        Investment = 1,

        [EnumMember(Value = @"Checking")]
        Checking = 2,

        [EnumMember(Value = @"Other_")]
        Other_ = 3,

    }

    /// <summary>Source: OpenTravel</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class List_PaymentCardIssuer_Enum
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public List_PaymentCardIssuer_Enum_Base? Value { get; set; }

        [JsonProperty("extension", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128, MinimumLength = 1)]
        public string Extension { get; set; }


    }

    /// <summary>Source: OpenTravel</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum List_PaymentCardIssuer_Enum_Base
    {
        [EnumMember(Value = @"VISA")]
        VISA = 0,

        [EnumMember(Value = @"USAirways")]
        USAirways = 1,

        [EnumMember(Value = @"UnitedAirlines")]
        UnitedAirlines = 2,

        [EnumMember(Value = @"StarwoodHotels")]
        StarwoodHotels = 3,

        [EnumMember(Value = @"SouthwestAirlines")]
        SouthwestAirlines = 4,

        [EnumMember(Value = @"RitzCarlton")]
        RitzCarlton = 5,

        [EnumMember(Value = @"Mastercard")]
        Mastercard = 6,

        [EnumMember(Value = @"Mariott")]
        Mariott = 7,

        [EnumMember(Value = @"Hyatt")]
        Hyatt = 8,

        [EnumMember(Value = @"Hilton")]
        Hilton = 9,

        [EnumMember(Value = @"Eurocard")]
        Eurocard = 10,

        [EnumMember(Value = @"Disney")]
        Disney = 11,

        [EnumMember(Value = @"DiscoverCard")]
        DiscoverCard = 12,

        [EnumMember(Value = @"DeltaAirlines")]
        DeltaAirlines = 13,

        [EnumMember(Value = @"ContinentalAirlines")]
        ContinentalAirlines = 14,

        [EnumMember(Value = @"Citibank")]
        Citibank = 15,

        [EnumMember(Value = @"Chase")]
        Chase = 16,

        [EnumMember(Value = @"CapitalOne")]
        CapitalOne = 17,

        [EnumMember(Value = @"BritishAirways")]
        BritishAirways = 18,

        [EnumMember(Value = @"BankOfAmerica")]
        BankOfAmerica = 19,

        [EnumMember(Value = @"AmericanExpress")]
        AmericanExpress = 20,

        [EnumMember(Value = @"Other_")]
        Other_ = 21,

    }

    /// <summary>Identifies the Loyalty Program, membership, form factor used by the certificate and its current status.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class LoyaltyCertificate
    {
        /// <summary>A unique identifying value assigned by the creating system. The ID attribute may be used to reference a primary-key value within a database or in a particular implementation.</summary>
        [JsonProperty("iD", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ID { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("iD_Context", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string ID_Context { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("certificateNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string CertificateNumber { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("memberNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string MemberNumber { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("programName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string ProgramName { get; set; }

        /// <summary>Indicates the starting date.</summary>
        [JsonProperty("EffectiveDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? EffectiveDate { get; set; }

        /// <summary>Indicates the ending date.</summary>
        [JsonProperty("ExpireDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? ExpireDate { get; set; }

        /// <summary>The number of nights of the hotel stay that are used to calculate the redemption amount.</summary>
        [JsonProperty("NmbrOfNights", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string NmbrOfNights { get; set; }

        /// <summary>Indicates what form the certificate is in e.g. Paper or Electronic.</summary>
        [JsonProperty("format", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public LoyaltyCertificateGroupFormat? Format { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("status", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string Status { get; set; }

        /// <summary>When true, indicates that the ExpireDate is the first day after the applicable period (e.g. when expire date is Oct 15  the last date of the period is Oct 14).</summary>
        [JsonProperty("expireDateExclusiveInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ExpireDateExclusiveInd { get; set; }


    }

    /// <summary>Identifies a loyalty certificate.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class LoyaltyCertificateNumberGroup
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("certificateNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string CertificateNumber { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("memberNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string MemberNumber { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("programName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string ProgramName { get; set; }


    }

    /// <summary>Provides information about the original document on which the reissue is based.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OriginalIssueAttributes
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("originalTicketNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string OriginalTicketNumber { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("originalIssuePlace", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string OriginalIssuePlace { get; set; }

        /// <summary>Date when the original ticket was issued.</summary>
        [JsonProperty("originalIssueDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public System.DateTimeOffset? OriginalIssueDate { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("originalIssueIATA", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string OriginalIssueIATA { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("originalPaymentForm", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string OriginalPaymentForm { get; set; }

        /// <summary>Indicates if the check digit of the ticket number or the interline agreement has to be checked or not.</summary>
        [JsonProperty("checkInhibitorType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public OriginalIssueAttributesCheckInhibitor? CheckInhibitorType { get; set; }

        /// <summary>Assigned Type: fin-0900:Coupons</summary>
        [JsonProperty("coupons", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<string> Coupons { get; set; }


    }

    /// <summary>Code extension definition without dimension information.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class OTACodeExtGroup
    {
        /// <summary>Implementer: Place a code value in this attribute if you have selected the \"Other_\" value in the enumerated list. Note that this value should be known to your trading partners.</summary>
        [JsonProperty("code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Code { get; set; }

        /// <summary>Example: Party Supplies</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        /// <summary>Example: Mylar Baloons</summary>
        [JsonProperty("descriptionDetail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string DescriptionDetail { get; set; }

        /// <summary>Implementer: This may be a source authority\/ owner name, ID or code.</summary>
        [JsonProperty("sourceName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string SourceName { get; set; }

        /// <summary>Implementer: This is A URL to the code source authority\/ owner.</summary>
        [JsonProperty("sourceURL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string SourceURL { get; set; }

        /// <summary>Example: OpenTravel 2012A CodeList</summary>
        [JsonProperty("resourceName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ResourceName { get; set; }

        /// <summary>Implementer: This is A URL to the code resource.</summary>
        [JsonProperty("resourceURL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ResourceURL { get; set; }

        /// <summary>Implementer: This is used for a database or key ID for the code item (if it is different from the @CodeExtension) in relationship to the obsolete code indicator.</summary>
        [JsonProperty("uniqueID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string UniqueID { get; set; }

        /// <summary>Implementer: If true, this item is obsolete and should be removed from the receiving system.</summary>
        [JsonProperty("removalInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? RemovalInd { get; set; }


    }

    /// <summary>Specifies the details of the debit or credit card.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("PaymentCardDetail", typeof(PaymentCardDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PaymentCard
    {
        /// <summary>Payment card reference ID.</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Indicated starting date.</summary>
        [JsonProperty("effectiveDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? EffectiveDate { get; set; }

        /// <summary>Assigned Type: c-0600:DateMMYY</summary>
        [JsonProperty("expireDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"(0[1-9]|1[0-2])[0-9][0-9]")]
        public string ExpireDate { get; set; }

        /// <summary>Assigned Type: c-0600:StringUpperCaseAlphaNumeric_Max16</summary>
        [JsonProperty("approvalCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(16)]
        [RegularExpression(@"([A-Z0-9]+)?")]
        public string ApprovalCode { get; set; }

        /// <summary>Assigned Type: c-0600:Privacy</summary>
        [JsonProperty("PrivacyGroup", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Privacy PrivacyGroup { get; set; }

        /// <summary>Assigned Type: fin-0900:PaymentCardType</summary>
        [JsonProperty("CardType", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(StringEnumConverter))]
        public PaymentCardType CardType { get; set; }

        /// <summary>Specifies the two character code (MC, VI, AX, etc) for the payment card (open enumeration)</summary>
        [JsonProperty("CardCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string CardCode { get; set; }

        /// <summary>Assigned Type: fin-0900:PaymentCardType_Issuer</summary>
        [JsonProperty("CardBrand", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PaymentCardType_Issuer CardBrand { get; set; }

        /// <summary>Assigned Type: fin-0900:PaymentCardType_Issuer</summary>
        [JsonProperty("CardIssuer", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PaymentCardType_Issuer CardIssuer { get; set; }

        /// <summary>Card holder name.</summary>
        [JsonProperty("CardHolderName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string CardHolderName { get; set; }

        /// <summary>Assigned Type: c-0600:CardNumber</summary>
        [JsonProperty("CardNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public CardNumber CardNumber { get; set; }

        /// <summary>Assigned Type: c-0600:SeriesCode</summary>
        [JsonProperty("SeriesCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public SeriesCode SeriesCode { get; set; }

        [JsonProperty("MagneticStripe", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(3)]
        public ICollection<MagneticStripe> MagneticStripe { get; set; }

        /// <summary>Implementer: If true, all or a portion of this data is secure, via tokenization, encryption and\/or masking.</summary>
        [JsonProperty("secureInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? SecureInd { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Specifies the details of the debit or credit card.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PaymentCardDetail : PaymentCard
    {
        /// <summary>Assigned Type: c-0600:CountryCode_ISO3166</summary>
        [JsonProperty("countryOfIssue", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{2}")]
        public string CountryOfIssue { get; set; }

        /// <summary>Assigned Type: c-0600:StringAlphaNumeric</summary>
        [JsonProperty("companyCardReference", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(4096)]
        [RegularExpression(@"([0-9a-zA-Z]+)?")]
        public string CompanyCardReference { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("remark", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Remark { get; set; }

        /// <summary>Assigned Type: c-0600:StringLittle</summary>
        [JsonProperty("bankName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(64)]
        public string BankName { get; set; }

        /// <summary>Assigned Type: c-0600:CountryCode_ISO3166</summary>
        [JsonProperty("bankCountryCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{2}")]
        public string BankCountryCode { get; set; }

        /// <summary>Assigned Type: c-0600:StateCode_ISO</summary>
        [JsonProperty("bankStateCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(2, MinimumLength = 2)]
        [RegularExpression(@"([a-zA-Z]{2})")]
        public string BankStateCode { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("CardHolderId", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier CardHolderId { get; set; }

        /// <summary>Assigned Type: c-0600:PersonName_Summary</summary>
        [JsonProperty("PersonName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PersonName PersonName { get; set; }

        /// <summary>Assigned Type: c-0600:Address</summary>
        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Address Address { get; set; }

        [JsonProperty("Telephone", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<Telephone> Telephone { get; set; }

        [JsonProperty("Email", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<Email> Email { get; set; }

        [JsonProperty("CustomerLoyalty", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<CustomerLoyalty_FIN0900> CustomerLoyalty { get; set; }

        /// <summary>Assigned Type: fin-0900:SignatureOnFile</summary>
        [JsonProperty("SignatureOnFile", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public SignatureOnFile SignatureOnFile { get; set; }

        /// <summary>Assigned Type: fin-0900:ThreeDomainSecurity</summary>
        [JsonProperty("ThreeDomainSecurity", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ThreeDomainSecurity ThreeDomainSecurity { get; set; }

        /// <summary>Implementer: If true, the credit card company is requested to delay the date on which the amount of this transaction is applied to the customer's account.</summary>
        [JsonProperty("extendedPaymentInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ExtendedPaymentInd { get; set; }

        /// <summary>True if this payment card has been issued through Enett</summary>
        [JsonProperty("enettInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? EnettInd { get; set; }

        /// <summary>If true, then the payment card holder is not one of the travelers in the reservation</summary>
        [JsonProperty("thirdPartyInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ThirdPartyInd { get; set; }

        /// <summary>If true, override airline restriction on the payment card</summary>
        [JsonProperty("acceptanceOverrideInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? AcceptanceOverrideInd { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PaymentCardType_Issuer
    {
        [JsonProperty("paymentCardIssuers", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public List_PaymentCardIssuer_Enum_Base? PaymentCardIssuers { get; set; }

        [JsonProperty("paymentCardIssuersExtension", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128, MinimumLength = 1)]
        public string PaymentCardIssuersExtension { get; set; }

        /// <summary>Implementer: Place a code value in this attribute if you have selected the \"Other_\" value in the enumerated list. Note that this value should be known to your trading partners.</summary>
        [JsonProperty("code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Code { get; set; }

        /// <summary>Example: Party Supplies</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        /// <summary>Example: Mylar Baloons</summary>
        [JsonProperty("descriptionDetail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string DescriptionDetail { get; set; }

        /// <summary>Implementer: This may be a source authority\/ owner name, ID or code.</summary>
        [JsonProperty("sourceName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string SourceName { get; set; }

        /// <summary>Implementer: This is A URL to the code source authority\/ owner.</summary>
        [JsonProperty("sourceURL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string SourceURL { get; set; }

        /// <summary>Example: OpenTravel 2012A CodeList</summary>
        [JsonProperty("resourceName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ResourceName { get; set; }

        /// <summary>Implementer: This is A URL to the code resource.</summary>
        [JsonProperty("resourceURL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ResourceURL { get; set; }

        /// <summary>Implementer: This is used for a database or key ID for the code item (if it is different from the @CodeExtension) in relationship to the obsolete code indicator.</summary>
        [JsonProperty("uniqueID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string UniqueID { get; set; }

        /// <summary>Implementer: If true, this item is obsolete and should be removed from the receiving system.</summary>
        [JsonProperty("removalInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? RemovalInd { get; set; }


    }

    /// <summary>Used to provide a promotion code of the loyalty redemption.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PromotionCodeGroup
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("promotionCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string PromotionCode { get; set; }

        /// <summary>Assigned Type: fin-0900:VendorCodes</summary>
        [JsonProperty("promotionVendorCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<string> PromotionVendorCode { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class SignatureOnFile
    {
        /// <summary>Assigned Type: fin-0900:DateEffectiveExpire</summary>
        [JsonProperty("Date_EffectiveExpire", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public DateEffectiveExpire Date_EffectiveExpire { get; set; }

        /// <summary>When true, indicates a signature has been obtained.</summary>
        [JsonProperty("signatureOnFileInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? SignatureOnFileInd { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ThreeDomainSecurity_Gateway
    {
        /// <summary>Assigned Type: c-0600:StringCharacterOne</summary>
        [JsonProperty("eCI", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1)]
        public string ECI { get; set; }

        /// <summary>Example: mycart</summary>
        [JsonProperty("merchantID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string MerchantID { get; set; }

        /// <summary>Example: 201</summary>
        [JsonProperty("processorID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ProcessorID { get; set; }

        /// <summary>Transaction URL.</summary>
        [JsonProperty("uRL", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string URL { get; set; }

        /// <summary>Assigned Type: c-0600:AuthenticationVerification</summary>
        [JsonProperty("AuthenticationVerification", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public AuthenticationVerification AuthenticationVerification { get; set; }

        /// <summary>Assigned Type: c-0600:Password</summary>
        [JsonProperty("Password", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Password Password { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ThreeDomainSecurity
    {
        /// <summary>Assigned Type: fin-0900:ThreeDomainSecurity_Gateway</summary>
        [JsonProperty("ThreeDomainSecurity_Gateway", Required = Required.Always)]
        public ThreeDomainSecurity_Gateway ThreeDomainSecurity_Gateway { get; set; }

        /// <summary>Assigned Type: fin-0900:ThreeDomainSecurity_Results</summary>
        [JsonProperty("ThreeDomainSecurity_Results", Required = Required.Always)]
        public ThreeDomainSecurity_Results ThreeDomainSecurity_Results { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ThreeDomainSecurity_Results
    {
        /// <summary>Example: AAABAFaQRwAAAAAAEZBHAAAAAAA=ECI05</summary>
        [JsonProperty("cAVV", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string CAVV { get; set; }

        /// <summary>Assigned Type: c-0600:StringCharacterOne</summary>
        [JsonProperty("pAResStatus", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(1)]
        public string PAResStatus { get; set; }

        /// <summary>Example: Y</summary>
        [JsonProperty("signatureVerfication", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string SignatureVerfication { get; set; }

        /// <summary>Example: 9D920E9-6FCF-4A74-A4E0-D6A591D1108F</summary>
        [JsonProperty("transactionID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TransactionID { get; set; }

        /// <summary>Example: 2bxUs1emK0SCevbivcApzAcAAQk=</summary>
        [JsonProperty("xID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string XID { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>A form of payment using a voucher or coupon.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class VoucherGroup
    {
        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("seriesCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string SeriesCode { get; set; }

        /// <summary>Indicates the starting date.</summary>
        [JsonProperty("EffectiveDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? EffectiveDate { get; set; }

        /// <summary>Indicates the ending date.</summary>
        [JsonProperty("ExpireDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? ExpireDate { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("billingNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string BillingNumber { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("supplierIdentifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string SupplierIdentifier { get; set; }

        /// <summary>Unique identifier of the electronic voucher.</summary>
        [JsonProperty("identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("valueType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string ValueType { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("authority", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Authority { get; set; }

        /// <summary>When true, specifies the Group Days Apply Value can be used for this Voucher Billing Account.  This indicator is used to allow the usage of a car rental days voucher when part of an inclusive tour.</summary>
        [JsonProperty("groupDaysApplyInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? GroupDaysApplyInd { get; set; }

        /// <summary>When true, indicates the voucher is electronic. An E-voucher is a value document issued by the Travel Agent for the customer. The e-voucher can be used as a proof of reservation, but more normally, as a full-payment or partial payment.</summary>
        [JsonProperty("electronicIndicatorInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ElectronicIndicatorInd { get; set; }

        /// <summary>When true, indicates that the ExpireDate is the first day after the applicable period (e.g. when expire date is Oct 15  the last date of the period is Oct 14).</summary>
        [JsonProperty("expireDateExclusiveInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ExpireDateExclusiveInd { get; set; }


    }

    /// <summary>Indicates if program is affiliated with a group of related offers accumulating credits.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum SingleVendorInd
    {
        [EnumMember(Value = @"SingleVndr")]
        SingleVndr = 0,

        [EnumMember(Value = @"Alliance")]
        Alliance = 1,

    }

    /// <summary>Indicates if the rerouting, which made the exchange necessary was voluntary or involuntary.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum Rerouting
    {
        [EnumMember(Value = @"voluntary")]
        Voluntary = 0,

        [EnumMember(Value = @"involuntary")]
        Involuntary = 1,

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum LoyaltyCertificateGroupFormat
    {
        [EnumMember(Value = @"Paper")]
        Paper = 0,

        [EnumMember(Value = @"Electronic")]
        Electronic = 1,

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum PaymentTransactionTypeCode
    {
        [EnumMember(Value = @"charge")]
        Charge = 0,

        [EnumMember(Value = @"reserve")]
        Reserve = 1,

        [EnumMember(Value = @"refund")]
        Refund = 2,

    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum OriginalIssueAttributesCheckInhibitor
    {
        [EnumMember(Value = @"CheckDigit")]
        CheckDigit = 0,

        [EnumMember(Value = @"InterlineAgreement")]
        InterlineAgreement = 1,

        [EnumMember(Value = @"Both")]
        Both = 2,

    }

    /// <summary>Used to identify the effective date and\/or expiration date.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class DateEffectiveExpire
    {
        /// <summary>Indicates the starting date.</summary>
        [JsonProperty("effective", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? Effective { get; set; }

        /// <summary>Indicates the ending date.</summary>
        [JsonProperty("expire", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? Expire { get; set; }

        /// <summary>When true, indicates that the ExpireDate is the first day after the applicable period (e.g. when expire date is Oct 15  the last date of the period is Oct 14).</summary>
        [JsonProperty("expireDateExclusiveInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ExpireDateExclusiveInd { get; set; }


    }

    /// <summary>Credit, Debit, etc.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum PaymentCardType
    {
        [EnumMember(Value = @"Credit")]
        Credit = 0,

        [EnumMember(Value = @"Debit")]
        Debit = 1,

        [EnumMember(Value = @"Gift")]
        Gift = 2,

    }

    /// <summary>Specifies if the voucher is for full credit or a group day or a monetary amount or regular voucher.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum VoucherType
    {
        [EnumMember(Value = @"FullCredit")]
        FullCredit = 0,

        [EnumMember(Value = @"GroupOrDay")]
        GroupOrDay = 1,

        [EnumMember(Value = @"SpecificValue")]
        SpecificValue = 2,

        [EnumMember(Value = @"RegularVoucher")]
        RegularVoucher = 3,

    }

    /// <summary>The type of guarantee, values from uAPI</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum GuaranteeType_Enum_FIN0900
    {
        [EnumMember(Value = @"Guarantee")]
        Guarantee = 0,

        [EnumMember(Value = @"Deposit")]
        Deposit = 1,

        [EnumMember(Value = @"Association")]
        Association = 2,

    }

    /// <summary>A person that is traveling</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("Traveler", typeof(Traveler))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelerID
    {
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>A person that is traveling</summary>
        [JsonProperty("travelerRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TravelerRef { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }


    }

    /// <summary>A person that is traveling</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Traveler : TravelerID
    {
        /// <summary>Date of Birth</summary>
        [JsonProperty("dob", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? Dob { get; set; }

        /// <summary>Gender</summary>
        [JsonProperty("gender", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Gender_Enum? Gender { get; set; }

        /// <summary>Assigned Type: c-0600:PassengerTypeCode</summary>
        [JsonProperty("passengerTypeCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(5, MinimumLength = 3)]
        [RegularExpression(@"([a-zA-Z0-9]{3,5})")]
        public string PassengerTypeCode { get; set; }

        /// <summary>Assigned Type: c-0600:CountryCode_ISO3166</summary>
        [JsonProperty("nationality", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{2}")]
        public string Nationality { get; set; }

        /// <summary>Assigned Type: c-0600:PersonName</summary>
        [JsonProperty("PersonName", Required = Required.Always)]
        public PersonName PersonName { get; set; }

        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public List<Address> Address { get; set; }

        [JsonProperty("Telephone", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public List<Telephone> Telephone { get; set; }

        [JsonProperty("Email", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public List<Email> Email { get; set; }

        /// <summary>Assigned Type: c-0600:NextSteps</summary>
        [JsonProperty("NextSteps", Required = Required.Always)]
        public NextSteps NextSteps { get; set; }

        /// <summary>Assigned Type: trav-0200:VIP</summary>
        [JsonProperty("VIP", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public VIP VIP { get; set; }

        [JsonProperty("TravelDocument", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public List<TravelDocument> TravelDocument { get; set; }

        [JsonProperty("CustomerLoyalty", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public List<CustomerLoyalty_C0600> CustomerLoyalty { get; set; }

        /// <summary>Assigned Type: trav-0200:PaxSourceAffiliation</summary>
        [JsonProperty("PaxSourceAffiliation", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public PaxSourceAffiliation PaxSourceAffiliation { get; set; }

        [JsonProperty("AlternateContact", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(20)]
        public List<AlternateContact> AlternateContact { get; set; }

        /// <summary>Assigned Type: c-0600:Remarks_Summary</summary>
        [JsonProperty("Remarks", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Remarks Remarks { get; set; }

        [JsonProperty("Profile", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public List<Profile> Profile { get; set; }

        [JsonProperty("accompaniedByInfantInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? AccompaniedByInfantInd { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AlternateContact
    {
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("contactType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string ContactType { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("relation", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Relation { get; set; }

        /// <summary>Assigned Type: c-0600:PersonName_Summary</summary>
        [JsonProperty("PersonName", Required = Required.Always)]
        public PersonName PersonName { get; set; }

        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(3)]
        public ICollection<Address> Address { get; set; }

        [JsonProperty("Telephone", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(5)]
        public ICollection<Telephone> Telephone { get; set; }

        [JsonProperty("Email", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(5)]
        public ICollection<Email> Email { get; set; }

        /// <summary>This is the contact in case of an emergency</summary>
        [JsonProperty("emergencyInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? EmergencyInd { get; set; }

        /// <summary>This is the default contact</summary>
        [JsonProperty("defaultInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? DefaultInd { get; set; }

        [JsonProperty("ExtensionPoint", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint ExtensionPoint { get; set; }


    }

    /// <summary>The parent profile id and type for this profile - from Universal Profile party_role_id</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ParentProfileID
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? Value { get; set; }

        /// <summary>The type (Agent or Agency) of the parent profile</summary>
        [JsonProperty("profileType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public ProfileType? ProfileType { get; set; }


    }

    /// <summary>The profile information that can be used for travelers, agencies, etc.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("Profile", typeof(Profile))]
    [JsonInheritanceAttribute("ProfileDetail", typeof(ProfileDetail))]
    [JsonInheritanceAttribute("ProfileUniversalProfile", typeof(ProfileUniversalProfile))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ProfileID
    {
        /// <summary>Internally used id, if one is needed. Not publically exposed</summary>
        [JsonProperty("id", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("ProfileIdentifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier ProfileIdentifier { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("ExternalIdentifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier ExternalIdentifier { get; set; }


    }

    /// <summary>The profile information that can be used for travelers, agencies, etc.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class Profile : ProfileID
    {
        /// <summary>The kind of profile, as in Traveler or Account</summary>
        [JsonProperty("profileType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public ProfileType? ProfileType { get; set; }

        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public ICollection<Address> Address { get; set; }

        [JsonProperty("Telephone", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public ICollection<Telephone> Telephone { get; set; }

        [JsonProperty("Email", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public ICollection<Email> Email { get; set; }

        /// <summary>This profile was provisioned (vs manually entered)</summary>
        [JsonProperty("provisionedInd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public bool? ProvisionedInd { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>The profile information that can be used for travelers, agencies, etc.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ProfileDetail : Profile
    {
        [JsonProperty("CustomerLoyalty", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(1000)]
        public ICollection<CustomerLoyalty_C0600> CustomerLoyalty { get; set; }

        [JsonProperty("FormOfPaymentSummary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(10)]
        public ICollection<FormOfPayment> FormOfPaymentSummary { get; set; }

        [JsonProperty("Remark", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(100)]
        public ICollection<Remark> Remark { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>The profile information that can be used for travelers, agencies, etc.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class ProfileUniversalProfile : Profile
    {
        /// <summary>The party role ID as it exists in Universal Profile</summary>
        [JsonProperty("PartyRoleID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public int? PartyRoleID { get; set; }

        /// <summary>Assigned Type: trav-0200:ParentProfileID</summary>
        [JsonProperty("ParentProfileID", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ParentProfileID ParentProfileID { get; set; }

        [JsonProperty("ExtensionPoint_Custom", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Custom ExtensionPoint_Custom { get; set; }


    }

    /// <summary>The kind of profile, as in Traveler or Account</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum ProfileType
    {
        [EnumMember(Value = @"Traveler")]
        Traveler = 0,

        [EnumMember(Value = @"Account")]
        Account = 1,

    }

    /// <summary>Attribute for an indicator for the person being a VIP. If the indicator is true, then the description of the VIPness and source (agency, loyalty, government, etc.)</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class VIP
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Value { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("source", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string Source { get; set; }

        /// <summary>Why is this person a VIP?</summary>
        [JsonProperty("description", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }


    }

    /// <summary>Speciality formats and other information as stored on the source system. Needed for cross lookup of passengers.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PaxSourceAffiliation
    {
        [JsonProperty("value", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<string> Value { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("formattedName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string FormattedName { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("formattedPhone", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string FormattedPhone { get; set; }

        /// <summary>Assigned Type: trav-0200:PassengerCoreSequence</summary>
        [JsonProperty("passengerNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(10)]
        public string PassengerNumber { get; set; }


    }

    /// <summary>Unique number assigned by authorities to document.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("PersonDocumentDetail", typeof(PersonDocumentDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PersonDocument
    {
        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("docNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string DocNumber { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("docType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string DocType { get; set; }

        /// <summary>Codes from OTA DOC - Document Type</summary>
        [JsonProperty("docTypeCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public DocTypeCode_Enum? DocTypeCode { get; set; }

        /// <summary>Date of Issue</summary>
        [JsonProperty("issueDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? IssueDate { get; set; }

        /// <summary>Date of expiration</summary>
        [JsonProperty("expireDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? ExpireDate { get; set; }

        /// <summary>Assigned Type: c-0600:StateProvCode</summary>
        [JsonProperty("stateProv_Code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(8, MinimumLength = 2)]
        public string StateProv_Code { get; set; }

        /// <summary>Assigned Type: c-0600:CountryCode_ISO</summary>
        [JsonProperty("issueCountry", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"([a-zA-Z]{2})")]
        public string IssueCountry { get; set; }

        /// <summary>The date of birth of the document holder</summary>
        [JsonProperty("birthDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? BirthDate { get; set; }

        /// <summary>Assigned Type: c-0600:CountryCode_ISO3166</summary>
        [JsonProperty("birthCountry", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{2}")]
        public string BirthCountry { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("birthPlace", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string BirthPlace { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("residence", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Residence { get; set; }

        /// <summary>Assigned Type: c-0600:Gender_Enum</summary>
        [JsonProperty("Gender", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Gender_Enum Gender { get; set; }

        /// <summary>Specifies a 2 character country code as defined in ISO3166.</summary>
        [JsonProperty("Nationality", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [RegularExpression(@"[a-zA-Z]{2}")]
        public string Nationality { get; set; }

        /// <summary>Assigned Type: c-0600:PersonName_Summary</summary>
        [JsonProperty("PersonName", Required = Required.Always)]
        public PersonName PersonName { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Unique number assigned by authorities to document.</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("TravelDocumentDetail", typeof(TravelDocumentDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelDocument
    {
        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("docNumber", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string DocNumber { get; set; }

        /// <summary>Assigned Type: c-0600:StringTiny</summary>
        [JsonProperty("docType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string DocType { get; set; }

        /// <summary>Codes from OTA DOC - Document Type</summary>
        [JsonProperty("docTypeCode", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public DocTypeCode_Enum? DocTypeCode { get; set; }

        /// <summary>Date of Issue</summary>
        [JsonProperty("issueDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? IssueDate { get; set; }

        /// <summary>Date of expiration</summary>
        [JsonProperty("expireDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? ExpireDate { get; set; }

        /// <summary>Assigned Type: c-0600:StateProvCode</summary>
        [JsonProperty("stateProv_Code", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(8, MinimumLength = 2)]
        public string StateProv_Code { get; set; }

        /// <summary>Assigned Type: c-0600:CountryCode_ISO</summary>
        [JsonProperty("issueCountry", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"([a-zA-Z]{2})")]
        public string IssueCountry { get; set; }

        /// <summary>The date of birth of the document holder</summary>
        [JsonProperty("birthDate", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(DateFormatConverter))]
        public System.DateTimeOffset? BirthDate { get; set; }

        /// <summary>Assigned Type: c-0600:CountryCode_ISO3166</summary>
        [JsonProperty("birthCountry", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [RegularExpression(@"[a-zA-Z]{2}")]
        public string BirthCountry { get; set; }

        /// <summary>Assigned Type: c-0600:StringShort</summary>
        [JsonProperty("birthPlace", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(128)]
        public string BirthPlace { get; set; }

        /// <summary>Assigned Type: c-0600:String</summary>
        [JsonProperty("residence", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(512)]
        public string Residence { get; set; }

        /// <summary>Assigned Type: c-0600:Gender_Enum</summary>
        [JsonProperty("Gender", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [JsonConverter(typeof(StringEnumConverter))]
        public Gender_Enum Gender { get; set; }

        /// <summary>Specifies a 2 character country code as defined in ISO3166.</summary>
        [JsonProperty("Nationality", Required = Required.Always)]
        [Required(AllowEmptyStrings = true)]
        [RegularExpression(@"[a-zA-Z]{2}")]
        public string Nationality { get; set; }

        /// <summary>Assigned Type: c-0600:PersonName_Summary</summary>
        [JsonProperty("PersonName", Required = Required.Always)]
        public PersonName PersonName { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>Unique number assigned by authorities to document.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class PersonDocumentDetail : PersonDocument
    {
        /// <summary>Assigned Type: c-0600:GeoPoliticalArea</summary>
        [JsonProperty("IssuedForGeoPoliticalArea", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public GeoPoliticalArea IssuedForGeoPoliticalArea { get; set; }

        /// <summary>Assigned Type: c-0600:Address</summary>
        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Address Address { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Unique number assigned by authorities to document.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelDocumentDetail : TravelDocument
    {
        /// <summary>Assigned Type: c-0600:GeoPoliticalArea</summary>
        [JsonProperty("IssuedForGeoPoliticalArea", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public GeoPoliticalArea IssuedForGeoPoliticalArea { get; set; }

        /// <summary>Assigned Type: c-0600:Address</summary>
        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Address Address { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>Codes from OTA DOC - Document Type</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum DocTypeCode_Enum
    {
        [EnumMember(Value = @"1")]
        _1 = 0,

        [EnumMember(Value = @"2")]
        _2 = 1,

        [EnumMember(Value = @"3")]
        _3 = 2,

        [EnumMember(Value = @"4")]
        _4 = 3,

        [EnumMember(Value = @"5")]
        _5 = 4,

        [EnumMember(Value = @"6")]
        _6 = 5,

        [EnumMember(Value = @"7")]
        _7 = 6,

        [EnumMember(Value = @"8")]
        _8 = 7,

        [EnumMember(Value = @"9")]
        _9 = 8,

        [EnumMember(Value = @"10")]
        _10 = 9,

        [EnumMember(Value = @"11")]
        _11 = 10,

        [EnumMember(Value = @"12")]
        _12 = 11,

        [EnumMember(Value = @"13")]
        _13 = 12,

        [EnumMember(Value = @"14")]
        _14 = 13,

        [EnumMember(Value = @"15")]
        _15 = 14,

        [EnumMember(Value = @"16")]
        _16 = 15,

        [EnumMember(Value = @"17")]
        _17 = 16,

        [EnumMember(Value = @"18")]
        _18 = 17,

        [EnumMember(Value = @"19")]
        _19 = 18,

        [EnumMember(Value = @"20")]
        _20 = 19,

        [EnumMember(Value = @"21")]
        _21 = 20,

        [EnumMember(Value = @"22")]
        _22 = 21,

    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("TravelOrganization", typeof(TravelOrganization))]
    [JsonInheritanceAttribute("TravelOrganizationDetail", typeof(TravelOrganizationDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelOrganizationID
    {
        /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
        [JsonProperty("travelOrganizationRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TravelOrganizationRef { get; set; }

        /// <summary>Simple xsd id, not for external use</summary>
        [JsonProperty("iD", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ID { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }

        [JsonProperty("AgencyPCC", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(1000)]
        public ICollection<AgencyPCC> AgencyPCC { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("AgencyBranch", typeof(AgencyBranch))]
    [JsonInheritanceAttribute("AgencyBranchDetail", typeof(AgencyBranchDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AgencyBranchID
    {
        /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
        [JsonProperty("travelOrganizationRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TravelOrganizationRef { get; set; }

        /// <summary>Simple xsd id, not for external use</summary>
        [JsonProperty("iD", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ID { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }

        [JsonProperty("AgencyPCC", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(1000)]
        public ICollection<AgencyPCC> AgencyPCC { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("TravelAgency", typeof(TravelAgency))]
    [JsonInheritanceAttribute("TravelAgencyDetail", typeof(TravelAgencyDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelAgencyID
    {
        /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
        [JsonProperty("travelOrganizationRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TravelOrganizationRef { get; set; }

        /// <summary>Simple xsd id, not for external use</summary>
        [JsonProperty("iD", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ID { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }

        [JsonProperty("AgencyPCC", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(1000)]
        public ICollection<AgencyPCC> AgencyPCC { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("TravelAccount", typeof(TravelAccount))]
    [JsonInheritanceAttribute("TravelAccountDetail", typeof(TravelAccountDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelAccountID
    {
        /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
        [JsonProperty("travelOrganizationRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TravelOrganizationRef { get; set; }

        /// <summary>Simple xsd id, not for external use</summary>
        [JsonProperty("iD", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ID { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }

        [JsonProperty("AgencyPCC", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(1000)]
        public ICollection<AgencyPCC> AgencyPCC { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [JsonConverter(typeof(JsonInheritanceConverter), "@type")]
    [JsonInheritanceAttribute("TravelSupplier", typeof(TravelSupplier))]
    [JsonInheritanceAttribute("TravelSupplierDetail", typeof(TravelSupplierDetail))]
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelSupplierID
    {
        /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
        [JsonProperty("travelOrganizationRef", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string TravelOrganizationRef { get; set; }

        /// <summary>Simple xsd id, not for external use</summary>
        [JsonProperty("iD", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string ID { get; set; }

        /// <summary>Assigned Type: c-0600:Identifier</summary>
        [JsonProperty("Identifier", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public Identifier Identifier { get; set; }

        [JsonProperty("AgencyPCC", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(1000)]
        public ICollection<AgencyPCC> AgencyPCC { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelOrganization : TravelOrganizationID
    {
        /// <summary>Agency, Branch, etc.</summary>
        [JsonProperty("organizationType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public OrganizationType? OrganizationType { get; set; }

        /// <summary>Assigned Type: c-0600:Company_Name</summary>
        [JsonProperty("OrganizationName", Required = Required.Always)]
        public Company_Name OrganizationName { get; set; }

        [JsonProperty("LocalizedName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<LocalizedName> LocalizedName { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AgencyBranch : AgencyBranchID
    {
        /// <summary>Agency, Branch, etc.</summary>
        [JsonProperty("organizationType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public OrganizationType? OrganizationType { get; set; }

        /// <summary>Assigned Type: c-0600:Company_Name</summary>
        [JsonProperty("OrganizationName", Required = Required.Always)]
        public Company_Name OrganizationName { get; set; }

        [JsonProperty("LocalizedName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<LocalizedName> LocalizedName { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelAgency : TravelAgencyID
    {
        /// <summary>Agency, Branch, etc.</summary>
        [JsonProperty("organizationType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public OrganizationType? OrganizationType { get; set; }

        /// <summary>Assigned Type: c-0600:Company_Name</summary>
        [JsonProperty("OrganizationName", Required = Required.Always)]
        public Company_Name OrganizationName { get; set; }

        [JsonProperty("LocalizedName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<LocalizedName> LocalizedName { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelAccount : TravelAccountID
    {
        /// <summary>Agency, Branch, etc.</summary>
        [JsonProperty("organizationType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public OrganizationType? OrganizationType { get; set; }

        /// <summary>Assigned Type: c-0600:Company_Name</summary>
        [JsonProperty("OrganizationName", Required = Required.Always)]
        public Company_Name OrganizationName { get; set; }

        [JsonProperty("LocalizedName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<LocalizedName> LocalizedName { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelSupplier : TravelSupplierID
    {
        /// <summary>Agency, Branch, etc.</summary>
        [JsonProperty("organizationType", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public OrganizationType? OrganizationType { get; set; }

        /// <summary>Assigned Type: c-0600:Company_Name</summary>
        [JsonProperty("OrganizationName", Required = Required.Always)]
        public Company_Name OrganizationName { get; set; }

        [JsonProperty("LocalizedName", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<LocalizedName> LocalizedName { get; set; }

        [JsonProperty("ExtensionPoint_Summary", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Summary ExtensionPoint_Summary { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelOrganizationDetail : TravelOrganization
    {
        [JsonProperty("Telephone", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<Telephone> Telephone { get; set; }

        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<Address> Address { get; set; }

        [JsonProperty("Email", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<EmailUse> Email { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class AgencyBranchDetail : AgencyBranchID
    {
        [JsonProperty("Telephone", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<Telephone> Telephone { get; set; }

        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<Address> Address { get; set; }

        [JsonProperty("Email", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<EmailUse> Email { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelAgencyDetail : TravelAgencyID
    {
        [JsonProperty("Telephone", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<Telephone> Telephone { get; set; }

        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<Address> Address { get; set; }

        [JsonProperty("Email", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<EmailUse> Email { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelAccountDetail : TravelAccountID
    {
        [JsonProperty("Telephone", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<Telephone> Telephone { get; set; }

        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<Address> Address { get; set; }

        [JsonProperty("Email", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<EmailUse> Email { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    /// <summary>An organization that has a name and a structure and members and directly works in the travel industry</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public partial class TravelSupplierDetail : TravelSupplierID
    {
        [JsonProperty("Telephone", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<Telephone> Telephone { get; set; }

        [JsonProperty("Address", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<Address> Address { get; set; }

        [JsonProperty("Email", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        [MaxLength(50)]
        public ICollection<EmailUse> Email { get; set; }

        [JsonProperty("ExtensionPoint_Detail", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public ExtensionPoint_Detail ExtensionPoint_Detail { get; set; }


    }

    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum ShareWithEnum
    {
        [EnumMember(Value = @"Supplier")]
        Supplier = 0,

        [EnumMember(Value = @"Agency")]
        Agency = 1,

    }

    /// <summary>An enumerated type defining the guarantee to be applied to this reservation.</summary>
    [GeneratedCode("NJsonSchema", "10.0.23.0 (Newtonsoft.Json v11.0.0.0)")]
    public enum GuaranteeType_Enum_CTHP0600
    {
        [EnumMember(Value = @"GuaranteeRequired")]
        GuaranteeRequired = 0,

        [EnumMember(Value = @"CC/DC/Voucher")]
        CC_DC_Voucher = 1,

        [EnumMember(Value = @"Profile")]
        Profile = 2,

        [EnumMember(Value = @"NoGuaranteesAccepted")]
        NoGuaranteesAccepted = 3,

        [EnumMember(Value = @"GuaranteesAccepted")]
        GuaranteesAccepted = 4,

        [EnumMember(Value = @"DepositRequired")]
        DepositRequired = 5,

        [EnumMember(Value = @"GuaranteesNotRequired")]
        GuaranteesNotRequired = 6,

        [EnumMember(Value = @"DepositNotRequired")]
        DepositNotRequired = 7,

        [EnumMember(Value = @"PrepayRequired")]
        PrepayRequired = 8,

        [EnumMember(Value = @"PrepayNotRequired")]
        PrepayNotRequired = 9,

        [EnumMember(Value = @"NoDepositsAccepted")]
        NoDepositsAccepted = 10,

    }

    public partial class HAmenity {

        public string Code { get; set; }
        public string Name { get; set; }

        public static List<HAmenity> GetRoom()
        {
            List<HAmenity> liHA = new List<HAmenity>();

            liHA.Add(new HAmenity { Code = "7", Name = "Balcony/Lanai/Terrace" });
            liHA.Add(new HAmenity { Code = "13", Name = "Bathtub" });
            liHA.Add(new HAmenity { Code = "59", Name = "Kitchen" });
            liHA.Add(new HAmenity { Code = "74", Name = "Non-smoking" });
            liHA.Add(new HAmenity { Code = "85", Name = "Private bathroom" });
            liHA.Add(new HAmenity { Code = "88", Name = "Refrigerator" });
            liHA.Add(new HAmenity { Code = "41", Name = "Fireplace" });
            liHA.Add(new HAmenity { Code = "68", Name = "Microwave" });
            liHA.Add(new HAmenity { Code = "69", Name = "Minibar" });
            liHA.Add(new HAmenity { Code = "107", Name = "Telephone" });
            liHA.Add(new HAmenity { Code = "965", Name = "Oriental Room Style" });
            liHA.Add(new HAmenity { Code = "966", Name = "Western Room Style" });
            liHA.Add(new HAmenity { Code = "92", Name = "Safe" });
            liHA.Add(new HAmenity { Code = "142", Name = "Shower" });
            liHA.Add(new HAmenity { Code = "251", Name = "TV" });
            liHA.Add(new HAmenity { Code = "116", Name = "VCR player" });
            liHA.Add(new HAmenity { Code = "195", Name = "Water bed" });
            liHA.Add(new HAmenity { Code = "51", Name = "High speed internet connection" });
            liHA.Add(new HAmenity { Code = "122", Name = "Wet bar" });
            liHA.Add(new HAmenity { Code = "102", Name = "Sofa bed" });

            return liHA;
        }
    }
}
