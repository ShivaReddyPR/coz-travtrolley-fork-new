﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using System.Net;
using System.Diagnostics;
using System.Threading;
using System.IO.Compression;
using System.Xml.Serialization;
using CT.BookingEngine;
using CT.Core;
using CT.Configuration;


//Access to Air India Express Service Methods
using AirIndiaExpress.Fulfillment;
using AirIndiaExpress.Pricing;
using AirIndiaExpress.Reservation;
using AirIndiaExpress.Security;
using AirIndiaExpress.TravelAgents;
using AirIndiaExpress.Fees;

namespace CT.BookingEngine.GDS
{

    public class AirIndiaExpressApi
    {

        /***************COMPLETE BOOKING FLOW OF AIR INDIA EXPRESS****************************************
         * 
         * STEP-1:Get Security Token to establish connection 
         * STEP-2: Login as Travel agent	
         * STEP-3:Retrieve FareQuote with logged in IATA passed
         * STEP-4:Get PNR summary
         * STEP-5:Commit the reservation to the DB and get confirmation
         * STEP-6:Call payment service to process  payment
         * STEP-7:Save reservation after processing payment
         * 
         * ***************************************LOGS ORDER************************************************
         * /******************STAGE -1: FLIGHT RESULTS BINDING**********************************************
         * 
         * 1.RetrieveSecurityTokenRequest
         * 2.RetrieveSecurityTokenResponse
         * 3.TravelAgencyLoginRequest
         * 4.TravelAgencyLoginResponse
         * 5.RetrieveAgencyCommissionRequest
         * 6.RetrieveAgencyCommissionResponse
         * 7.FareQuoteRequest
         * 8.FareQuoteResponse
         * 
         * /********************STAGE -2: TICKET BOOKING FLOW***********************************************
         * 9.AgencyCreditRequest
         * 10.AgencyCreditResponse
         * 11.GetSummaryPNRRequest
         * 12.GetSummaryPNRResponse
         * 13.CommitPNRRequest
         * 14.CommitPNRResponse
         * 15.ProcessPNRPaymentRequest
         * 16.ProcessPNRPaymentResponse
         * 17.SaveReservationRequest
         * 18.SaveReservationResponse           
         * **************************************************************************************************/

        /**********************************Retrieving and cancelling an existing booking ********************/
        /* STEP-1: RetrieveSecurityToken	
         * STEP-2: RetrievePNR - GetReservation	
         * STEP-3: CancelPNR -  Cancel Reservation	
         * STEP-4: CreatePNR -  Save Reservation	
          
        /****************************************************************************************************/

        //This class has all major functions related to payment for reservation.
        ConnectPoint_FulfillmentClient processFulfillment = new ConnectPoint_FulfillmentClient();

        //This class is used to obtain the fare and inventory availability for a proposed itinerary request as well as obtain the available services in a particular flight.  
        ConnectPoint_PricingClient processPricing = new ConnectPoint_PricingClient();

        //This class has all major functions related to a reservation
        ConnectPoint_ReservationClient processReservation = new ConnectPoint_ReservationClient();

        //This class allows  to execute operations and services in ConnectPoint by passing proper security credentials
        ConnectPoint_SecurityClient processSecurityToken = new ConnectPoint_SecurityClient();

        //This class allows to retrieve information for a travel agency . 
        //This class also enables travel agency to view their account balance, reservation history, and private fares.  

        ConnectPoint_TravelAgentsClient processTravelAgent = new ConnectPoint_TravelAgentsClient();

        //This class has all major functions related to fees.
        ConnectPoint_FeesClient processFees = new ConnectPoint_FeesClient();


        DataTable dtBaggageQuotes = new DataTable("dtBaggageQuotes");

        const string carrierCode = "IX"; //Air India Express IATA-Code

        string securityToken = string.Empty;
        int appUserId;
        string xmlPath = string.Empty;
        string logonId = string.Empty;
        string password = string.Empty;
        string agentBaseCurrency;
        Dictionary<string, decimal> exchangeRates;
        decimal rateOfExchange = 1;
        int decimalValue = 3;

        string travelAgentIATANumber;
        string travelAgentUserName;
        string travelAgentPassword;

       
        /// <summary>
        /// Agent Base Currency Eg:AED,INR,
        /// </summary>
        public string AgentBaseCurrency
        {
            get { return agentBaseCurrency; }
            set { agentBaseCurrency = value; }
        }

        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public int AgentDecimalValue
        {
            get { return decimalValue; }
            set { decimalValue = value; }
        }

        /// <summary>
        ///  Security Logon ID --Supplied by the Air India Express
        /// </summary>
        public string LogonId
        {
            get { return logonId; }
            set { logonId = value; }
        }
        /// <summary>
        /// Security Password -- Supplied by the Air India Express
        /// </summary>
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        /// <summary>
        /// Security GUID Used to access all the methods in the services.
        /// </summary>
        public string SecurityToken
        {
            get { return securityToken; }
            set { securityToken = value; }
        }
        /// <summary>
        /// Application User Id.
        /// </summary>
        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public AirIndiaExpressApi()
        {
            xmlPath = ConfigurationSystem.AirIndiaExpressConfig["XmlLogPath"];
            travelAgentIATANumber = ConfigurationSystem.AirIndiaExpressConfig["IATALocal"];
            travelAgentUserName = ConfigurationSystem.AirIndiaExpressConfig["LoginTAUserLocal"];
            travelAgentPassword = ConfigurationSystem.AirIndiaExpressConfig["LoginTAPwd"];

            try
            {
                if (!System.IO.Directory.Exists(xmlPath))
                {
                    System.IO.Directory.CreateDirectory(xmlPath);
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AirindiaExpress, Severity.High, 1, "(AirIndiaExpress)Failed to Intialise object  :Reason : " + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Parameterised constructor
        /// </summary>
        /// <param name="securityToken"></param>
        public AirIndiaExpressApi(string securityToken)
        {
            this.securityToken = securityToken;
            travelAgentIATANumber = ConfigurationSystem.AirIndiaExpressConfig["IATALocal"];
            travelAgentPassword = ConfigurationSystem.AirIndiaExpressConfig["LoginTAPwd"];
            travelAgentUserName = ConfigurationSystem.AirIndiaExpressConfig["LoginTAUserLocal"];
            xmlPath = ConfigurationSystem.AirIndiaExpressConfig["XmlLogPath"];
            try
            {
                if (!System.IO.Directory.Exists(xmlPath))
                {
                    System.IO.Directory.CreateDirectory(xmlPath);
                }

            }


            catch (Exception ex)
            {
                Audit.Add(EventType.AirindiaExpress, Severity.High, 1, "(AirIndiaExpress)Failed to initialise object  :Reason : " + ex.ToString(), "");
            }
        }


        /// <summary>
        /// The login process generates a GUID needed for web service requests. 
        /// This GUID is needed for authentication and access control.
        /// And Used to establish connection
        /// </summary>
        /// <returns></returns>
        public string RetrieveSecurityToken()
        {
            RetrieveSecurityToken retrieveSecurityTokenRequest = new RetrieveSecurityToken();
            ViewSecurityToken viewSecurityToken = new ViewSecurityToken();

            try
            {
                retrieveSecurityTokenRequest.CarrierCodes = new global::AirIndiaExpress.Security.CarrierCode[1];
                retrieveSecurityTokenRequest.CarrierCodes[0] = new global::AirIndiaExpress.Security.CarrierCode();
                retrieveSecurityTokenRequest.CarrierCodes[0].AccessibleCarrierCode = carrierCode;
                retrieveSecurityTokenRequest.LogonID = logonId;
                retrieveSecurityTokenRequest.Password = password;
                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(RetrieveSecurityToken));
                        string filePath = @"" + xmlPath + "RetrieveSecurityTokenRequest_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, retrieveSecurityTokenRequest);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }

                viewSecurityToken = processSecurityToken.RetrieveSecurityToken(retrieveSecurityTokenRequest);
                if (viewSecurityToken != null && !string.IsNullOrEmpty(viewSecurityToken.SecurityToken))
                {
                    securityToken = viewSecurityToken.SecurityToken;
                }

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(ViewSecurityToken));
                        string filePath = @"" + xmlPath + securityToken + "_RetrieveSecurityTokenResponse_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, viewSecurityToken);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AirindiaExpress, Severity.High, 1, "(AirIndiaExpress)Failed to Retrieve Security Token.Reason : " + ex.ToString(), "");
            }
            return securityToken;
        }


        /// <summary>
        /// Login as Travel agent in order to call the other operation in the service.
        /// </summary>
        /// <param name="nguid"></param>
        /// <returns></returns>
        public bool TravelAgencyLogin(string SecurityToken)
        {
            LoginTravelAgent loginTravelAgentRequest = new LoginTravelAgent();
            loginTravelAgentRequest.CarrierCodes = new AirIndiaExpress.TravelAgents.CarrierCode[1];
            loginTravelAgentRequest.CarrierCodes[0] = new AirIndiaExpress.TravelAgents.CarrierCode();
            loginTravelAgentRequest.CarrierCodes[0].AccessibleCarrierCode = carrierCode;
            loginTravelAgentRequest.SecurityGUID = SecurityToken;
            loginTravelAgentRequest.IATANumber = travelAgentIATANumber; //The IATA Number for the travel agency .
            loginTravelAgentRequest.UserName = travelAgentUserName;//The travel agent user name  
            loginTravelAgentRequest.Password = travelAgentPassword;//The travel agent user password  
            loginTravelAgentRequest.HistoricUserName = travelAgentUserName;
            try
            {
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(LoginTravelAgent));
                    string filePath = @"" + xmlPath + securityToken +"_TravelAgencyLoginRequest_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, loginTravelAgentRequest);
                    sw.Close();
                    ser = null;
                    Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                }
            }
            catch { }

            LoginTravelAgentStatus loginTravelAgentStatus = processTravelAgent.LoginTravelAgent(loginTravelAgentRequest);

            try
            {
                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(LoginTravelAgentStatus));
                    string filePath = @"" + xmlPath + securityToken + "_TravelAgencyLoginResponse_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, loginTravelAgentStatus);
                    sw.Close();
                    ser = null;
                    Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                }
            }
            catch { }

            if (!loginTravelAgentStatus.LoggedIn)
            {
                CT.Core.Audit.Add(CT.Core.EventType.AirindiaExpress, CT.Core.Severity.High, 0, "(AirIndiaExpress) Travel Agency Login Failed | " + loginTravelAgentStatus.Exceptions[0].ExceptionDescription + DateTime.Now, "");

                throw new BookingEngineException("(AirIndiaExpress) Travel Agency Login Failed");
            }
            else
            {
                CT.Core.Audit.Add(CT.Core.EventType.AirindiaExpress, CT.Core.Severity.Normal, 0, "(AirIndiaExpress) Travel Agency Login Successfull |" + DateTime.Now, "");
            }


            return loginTravelAgentStatus.LoggedIn;
        }

        /// <summary>
        /// Generate a fare quote request object for a proposed itinerary request.
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public RetrieveFareQuote GetFareQuoteRequest(string securityToken, SearchRequest request)
        {
            RetrieveFareQuote retrieveFareQuoteRequest = null;
            try
            {
                retrieveFareQuoteRequest = new RetrieveFareQuote();
                retrieveFareQuoteRequest.SecurityGUID = securityToken;
                retrieveFareQuoteRequest.HistoricUserName = travelAgentUserName;
                retrieveFareQuoteRequest.CarrierCodes = new AirIndiaExpress.Pricing.CarrierCode[1];
                retrieveFareQuoteRequest.CarrierCodes[0] = new AirIndiaExpress.Pricing.CarrierCode();
                retrieveFareQuoteRequest.CarrierCodes[0].AccessibleCarrierCode = carrierCode;
                retrieveFareQuoteRequest.CurrencyOfFareQuote = AirIndiaExpress.Pricing.EnumerationsCurrencyCodeTypes.AED; //The currency to be used for pricing
                retrieveFareQuoteRequest.PromotionalCode = string.Empty;
                retrieveFareQuoteRequest.IataNumberOfRequestor = travelAgentIATANumber;//IATA Number associated to fares
                retrieveFareQuoteRequest.CorporationID = -1; //Need to get confirmation on this  //Corporation ID associated to fares

                //No combinability rules applied. No fares are nested. All fares are returned.
                retrieveFareQuoteRequest.FareFilterMethod = EnumsFareFilterMethodType.NoCombinabilityAllFares; //Need to discuss this with shiva and ziyad
                retrieveFareQuoteRequest.FareGroupMethod = EnumsFareGroupMethodType.FareClassFareBasis; //Group by Fare Class and Fare Basis Code

                //Returns only available fares -Inventory Filter Method applies different rules regarding number of available seats
                retrieveFareQuoteRequest.InventoryFilterMethod = EnumsInventoryFilterMethodType.Available;


                if (request.Type == SearchType.Return)
                {
                    retrieveFareQuoteRequest.FareQuoteDetails = new FareQuoteDetail[request.Segments.Length]; //Round Trip
                }
                else
                {
                    retrieveFareQuoteRequest.FareQuoteDetails = new FareQuoteDetail[1]; //One Way
                }

                for (int i = 0; i < request.Segments.Length; i++)
                {
                    if (request.Type == SearchType.OneWay && i > 0)
                    {
                        break;
                    }
                    else
                    {
                        FareQuoteDetail fareQuoteDetail = new FareQuoteDetail(); //The legs to search 
                        if (i == 0) // One Way.
                        {
                            fareQuoteDetail.DateOfDeparture = request.Segments[0].PreferredDepartureTime; //The departure date to search for  
                            fareQuoteDetail.Destination = request.Segments[0].Destination;//Destination airport code  
                            fareQuoteDetail.Origin = request.Segments[0].Origin;//Origin airport code  
                        }
                        else //Round Trip
                        {
                            fareQuoteDetail.DateOfDeparture = request.Segments[1].PreferredDepartureTime;  //The departure date to search for 
                            fareQuoteDetail.Destination = request.Segments[0].Origin;//Origin airport code
                            fareQuoteDetail.Origin = request.Segments[0].Destination;//Destination airport code 
                        }
                        fareQuoteDetail.UseAirportsNotMetroGroups = true;//A value of True indicates that the Origin and Destination are airport codes. False indicates they are metro groups
                        fareQuoteDetail.FareTypeCategory = 1;
                        fareQuoteDetail.FareClass = string.Empty;
                        fareQuoteDetail.FareBasisCode = string.Empty;
                        fareQuoteDetail.Cabin = string.Empty;
                        fareQuoteDetail.LFID = -214; //Limit the search results to this logical flight ID. (-1 for no filter)  
                        fareQuoteDetail.OperatingCarrierCode = carrierCode;
                        fareQuoteDetail.MarketingCarrierCode = carrierCode;
                        fareQuoteDetail.NumberOfDaysAfter = 0;//Number of scheduled days after departure date to include in search
                        fareQuoteDetail.NumberOfDaysBefore = 0;//Number of scheduled days before departure date to include in search  
                        fareQuoteDetail.LanguageCode = "en"; //Language code - not currently in use. Possible future use.  
                        fareQuoteDetail.TicketPackageID = "1";//Ticket package ID - not currently in use. Possible future use.  

                        int paxCount = 0;
                        if (request.AdultCount > 0)
                        {
                            paxCount++;
                        }
                        if (request.ChildCount > 0)
                        {
                            paxCount++;
                        }
                        if (request.InfantCount > 0)
                        {
                            paxCount++;
                        }

                        //List of passenger types to include in the search
                        fareQuoteDetail.FareQuoteRequestInfos = new FareQuoteRequestInfo[paxCount];
                        // Passenger type ID Property. Standard types are: 1=Adult, 5=Infant, 6=Child. Others may exist per airline setup.
                        //TotalSeatsRequired Property :Number of seats requested for this passenger type.

                        for (int j = 0; j < request.AdultCount; j++)
                        {

                            FareQuoteRequestInfo fareQuoteRequestInfo_Adult = new FareQuoteRequestInfo();
                            fareQuoteRequestInfo_Adult.PassengerTypeID = 1;
                            fareQuoteRequestInfo_Adult.TotalSeatsRequired = request.AdultCount;
                            fareQuoteDetail.FareQuoteRequestInfos[0] = fareQuoteRequestInfo_Adult;
                        }
                        for (int j = 0; j < request.ChildCount; j++)
                        {

                            FareQuoteRequestInfo fareQuoteRequestInfo_Child = new FareQuoteRequestInfo();
                            fareQuoteRequestInfo_Child.PassengerTypeID = 6;
                            fareQuoteRequestInfo_Child.TotalSeatsRequired = request.ChildCount;
                            fareQuoteDetail.FareQuoteRequestInfos[1] = fareQuoteRequestInfo_Child;
                        }
                        for (int j = 0; j < request.InfantCount; j++)
                        {
                            FareQuoteRequestInfo fareQuoteRequestInfo_Infant = new FareQuoteRequestInfo();
                            fareQuoteRequestInfo_Infant.PassengerTypeID = 5;
                            fareQuoteRequestInfo_Infant.TotalSeatsRequired = request.InfantCount;
                            if (request.ChildCount > 0)
                            {
                                fareQuoteDetail.FareQuoteRequestInfos[2] = fareQuoteRequestInfo_Infant;
                            }
                            else
                            {
                                fareQuoteDetail.FareQuoteRequestInfos[1] = fareQuoteRequestInfo_Infant;
                            }
                        }
                        retrieveFareQuoteRequest.FareQuoteDetails[i] = fareQuoteDetail;

                    }
                }



            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.AirindiaExpress, CT.Core.Severity.High, 0, "(AirIndiaExpress) Failed to generate fare quote request. Error: " + ex.Message + DateTime.Now, "");
                throw new Exception("(AirIndiaExpress) Failed to generate fare quote request", ex);
            }
            return retrieveFareQuoteRequest;
        }

        /// <summary>
        ///Search for the Air India Express flights in the inventory for a proposed itinerary request 
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchResult[] Search(string securityToken, SearchRequest request)
        {
            SearchResult[] result = new SearchResult[0];
            try
            {
                if (AllowSearch(request))//Allow search only if the requested origin and destination is avaialble in the sectorlist provided by air india express.
                {
                    ViewAgencyCommission agencyCommission = RetrieveAgencyCommission(securityToken);
                    if (agencyCommission != null && agencyCommission.Exceptions[0].ExceptionCode == 0)
                    {
                        TransactionFeeDetail transFee = new TransactionFeeDetail();
                        transFee.Amount = agencyCommission.TravelAgencyCommissions[0].Amount;
                        RetrieveFareQuote retrieveFareQuoteRequest = GetFareQuoteRequest(securityToken, request);

                        if (retrieveFareQuoteRequest != null)
                        {
                            try
                            {
                                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                                {
                                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(RetrieveFareQuote));
                                    string filePath = @"" + xmlPath + securityToken + "_FareQuoteRequest_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                                    StreamWriter sw = new StreamWriter(filePath);
                                    ser.Serialize(sw, retrieveFareQuoteRequest);
                                    sw.Close();
                                }
                            }
                            catch { }
                            ViewFareQuote viewFareQuote = processPricing.RetrieveFareQuote(retrieveFareQuoteRequest);
                            try
                            {
                                if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                                {
                                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(ViewFareQuote));
                                    string filePath = @"" + xmlPath + securityToken + "_FareQuoteResponse_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                                    StreamWriter sw = new StreamWriter(filePath);
                                    ser.Serialize(sw, viewFareQuote);
                                    sw.Close();
                                }
                            }
                            catch { }

                            if (viewFareQuote != null && viewFareQuote.Exceptions[0].ExceptionCode == 0)
                            {
                                result = GenerateFlightResult(request, viewFareQuote, securityToken, transFee.Amount, (transFee.IsPercentage ? "P" : "F"));
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.AirindiaExpress, CT.Core.Severity.High, 1, "(AirIndiaExpress)Failed to get results. Error:" + ex.ToString(), "0");

            }
            return result;
        }

        /// <summary>
        /// Generate flight results from the retrieved fareQuoteResponse for a proposed itinerary request
        /// </summary>
        /// <param name="request"></param>
        /// <param name="fareQuoteResponse"></param>
        /// <returns></returns>
        public SearchResult[] GenerateFlightResult(SearchRequest request, ViewFareQuote fareQuoteResponse, string securityToken, decimal transactionFee, string transFeeType)
        {
            //FareQuoteResponse -- Exceptions ,FlightSegments array,LegDetails array,SegmentDetails array,Tax Details array
            List<SearchResult> resultList = new List<SearchResult>();


            try
            {
                string currency = "";
                currency = fareQuoteResponse.RequestedCurrencyOfFareQuote;

                int fareBreakDownCount = 0;

                if (request.AdultCount > 0)
                {
                    fareBreakDownCount = 1;
                }
                if (request.ChildCount > 0)
                {
                    fareBreakDownCount++;
                }
                if (request.InfantCount > 0)
                {
                    fareBreakDownCount++;
                }

                if (agentBaseCurrency != currency && exchangeRates != null)
                {
                    rateOfExchange = exchangeRates[currency];
                }
                else
                {
                    rateOfExchange = 1;
                }
                if (fareQuoteResponse.FlightSegments != null && fareQuoteResponse.FlightSegments.Length > 0)
                {
                    //Separate onward and return flight segments
                    List<AirIndiaExpress.Pricing.FlightSegment> FlightSegments = new List<AirIndiaExpress.Pricing.FlightSegment>();
                    List<AirIndiaExpress.Pricing.FlightSegment> OnwardFlightSegments = new List<AirIndiaExpress.Pricing.FlightSegment>();
                    List<AirIndiaExpress.Pricing.FlightSegment> ReturnFlightSegments = new List<AirIndiaExpress.Pricing.FlightSegment>();

                    FlightSegments.AddRange(fareQuoteResponse.FlightSegments); //Add the list of flight segments

                    List<SegmentDetail> SegmentDetails = new List<SegmentDetail>();
                    List<SegmentDetail> OnwardSegmentDetails = new List<SegmentDetail>();
                    List<SegmentDetail> ReturnSegmentDetails = new List<SegmentDetail>();

                    SegmentDetails.AddRange(fareQuoteResponse.SegmentDetails); //Add the list of segment details.

                    //Filter Segment Details for Onward and Return flights from the Search Request Origin & Destination
                    foreach (SegmentDetail segmentDetail in SegmentDetails)
                    {
                        if (segmentDetail.Origin == request.Segments[0].Origin)
                        {
                            OnwardSegmentDetails.Add(segmentDetail);
                        }
                        else if (segmentDetail.Origin == request.Segments[0].Destination)
                        {
                            ReturnSegmentDetails.Add(segmentDetail);
                        }
                    }

                    //Link LFID of SegmentDetail to LFID of FlightSegment
                    foreach (AirIndiaExpress.Pricing.FlightSegment flightSegment in FlightSegments)
                    {
                        foreach (SegmentDetail segmentDetail in OnwardSegmentDetails)
                        {
                            if (segmentDetail.LFID == flightSegment.LFID)
                            {
                                OnwardFlightSegments.Add(flightSegment);
                            }
                        }

                        foreach (SegmentDetail segmentDetail in ReturnSegmentDetails)
                        {
                            if (segmentDetail.LFID == flightSegment.LFID)
                            {
                                ReturnFlightSegments.Add(flightSegment);
                            }
                        }
                    }

                    if (OnwardFlightSegments.Count > 0)
                    {
                        for (int i = 0; i < OnwardFlightSegments.Count; i++)
                        {
                            //Get the segment detail which matches with the LFID of the  corresponding flght segment.
                            SegmentDetail onwardSegmentDetail = SegmentDetails.Find(detail => detail.LFID == OnwardFlightSegments[i].LFID);
                            AirIndiaExpress.Pricing.FlightSegment onwardFlightSegment = OnwardFlightSegments[i];

                            //Get the different fare types for the corresponding flight segment.
                            List<AirIndiaExpress.Pricing.FareType> onwardFlightSegmentFareTypes = new List<AirIndiaExpress.Pricing.FareType>();
                            onwardFlightSegmentFareTypes.Clear();
                            foreach (AirIndiaExpress.Pricing.FareType fareType in onwardFlightSegment.FareTypes)
                            {
                                onwardFlightSegmentFareTypes.Add(fareType);
                            }

                            if (request.Type == SearchType.Return)
                            {
                                for (int j = 0; j < ReturnFlightSegments.Count; j++)
                                {
                                    SegmentDetail returnSegmentDetail = SegmentDetails.Find(detail => detail.LFID == ReturnFlightSegments[j].LFID);
                                    AirIndiaExpress.Pricing.FlightSegment returnFlightSegment = ReturnFlightSegments[j];

                                    List<AirIndiaExpress.Pricing.FareType> returnFlightSegmentFareTypes = new List<AirIndiaExpress.Pricing.FareType>();
                                    returnFlightSegmentFareTypes.Clear();
                                    foreach (AirIndiaExpress.Pricing.FareType fareType in returnFlightSegment.FareTypes)
                                    {
                                        returnFlightSegmentFareTypes.Add(fareType);
                                    }

                                    #region FareInfo
                                    for (int k = 0; k < onwardFlightSegmentFareTypes.Count; k++)
                                    {
                                        if (onwardFlightSegmentFareTypes[k].FareInfos != null && onwardFlightSegmentFareTypes[k].FareInfos.Length > 0)
                                        {


                                            for (int c = 0; c < returnFlightSegmentFareTypes.Count; c++)
                                            {

                                                if (returnFlightSegmentFareTypes[c].FareInfos != null && returnFlightSegmentFareTypes[c].FareInfos.Length > 0)
                                                {


                                                    SearchResult result = new SearchResult();
                                                    int passengerCount = 0;
                                                    result.IsLCC = true;
                                                    result.ValidatingAirline = onwardSegmentDetail.CarrierCode.Trim();
                                                    result.Airline = onwardSegmentDetail.CarrierCode.Trim();
                                                    result.ResultBookingSource = BookingSource.AirIndiaExpressIntl;
                                                    result.GUID = securityToken;
                                                    result.Flights = new FlightInfo[2][];
                                                    result.Flights[0] = new FlightInfo[onwardFlightSegment.FlightLegDetails.Length];
                                                    result.Flights[1] = new FlightInfo[returnFlightSegment.FlightLegDetails.Length];
                                                    result.Currency = fareQuoteResponse.RequestedCurrencyOfFareQuote;
                                                    result.FareBreakdown = new Fare[fareBreakDownCount];

                                                    if (result.FareType == null || result.FareType.Length <= 0)
                                                    {
                                                        result.FareType = onwardFlightSegmentFareTypes[k].FareTypeName;
                                                    }
                                                    if (result.FareType.Length > 0)
                                                    {
                                                        {
                                                            result.FareType += "," + returnFlightSegmentFareTypes[c].FareTypeName;
                                                        }
                                                    }

                                                    for (int p = 0; p < onwardFlightSegmentFareTypes[k].FareInfos.Length; p++)
                                                    {
                                                        FareInfo fareInfoNode = onwardFlightSegmentFareTypes[k].FareInfos[p];
                                                        string bookingClass = onwardFlightSegmentFareTypes[k].FareInfos[p].FCCode;
                                                        string cabin = onwardFlightSegmentFareTypes[k].FareInfos[p].Cabin;

                                                        if (agentBaseCurrency != fareQuoteResponse.RequestedCurrencyOfFareQuote)
                                                        {
                                                            result.Currency = agentBaseCurrency;
                                                        }


                                                        if (request.AdultCount > 0)
                                                        {
                                                            if (result.FareBreakdown[0] == null)
                                                            {
                                                                result.FareBreakdown[0] = new Fare();
                                                            }
                                                            result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                            result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                        }

                                                        if (request.ChildCount > 0)
                                                        {

                                                            if (result.FareBreakdown[1] == null)
                                                            {
                                                                result.FareBreakdown[1] = new Fare();
                                                            }
                                                            result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                        }
                                                        //Infant Count-- if both children and infants exists in the request.
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {
                                                            if (result.FareBreakdown[2] == null)
                                                            {
                                                                result.FareBreakdown[2] = new Fare();
                                                            }
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                        }


                                                        if (result.FareInformationId == null)
                                                        {
                                                            result.FareInformationId = new Dictionary<string, List<int>>();
                                                            result.FareRules = new List<FareRule>();
                                                        }
                                                        FareRule newFareRule = new FareRule();
                                                        ApplicableTaxDetail[] taxNodeList = fareInfoNode.ApplicableTaxDetails;
                                                        double tax = 0;
                                                        if (result.Price == null)
                                                        {
                                                            result.Price = new PriceAccounts();
                                                        }

                                                        PassengerType ptype = PassengerType.Adult;
                                                        if (fareInfoNode.PTCID.ToString() == "1")
                                                        {
                                                            passengerCount = request.AdultCount;
                                                            ptype = PassengerType.Adult;
                                                            List<int> fareIds = new List<int>();
                                                            fareIds.Add(fareInfoNode.FareID);
                                                            if (!result.FareInformationId.ContainsKey(PassengerType.Adult.ToString()))
                                                            {
                                                                result.FareInformationId.Add(PassengerType.Adult.ToString(), fareIds);
                                                            }
                                                            result.BaggageIncludedInFare = "0";

                                                            foreach (ApplicableTaxDetail taxNode in taxNodeList)
                                                            {
                                                                foreach (TaxDetail td in fareQuoteResponse.TaxDetails)
                                                                {
                                                                    //FBA-Free Baggage Allowance
                                                                    if (taxNode.TaxID == td.TaxID && (td.CodeType.Contains("FBA")))
                                                                    {
                                                                        result.Price.BaggageCharge = 0;
                                                                        result.BaggageIncludedInFare = td.TaxDesc;
                                                                        //result.IsBaggageIncluded = true;
                                                                        break;

                                                                    }
                                                                }
                                                            }


                                                        }
                                                        else if (fareInfoNode.PTCID.ToString() == "6")
                                                        {
                                                            passengerCount = request.ChildCount;
                                                            ptype = PassengerType.Child;
                                                            List<int> fareIds = new List<int>();
                                                            fareIds.Add(fareInfoNode.FareID);
                                                            if (!result.FareInformationId.ContainsKey(PassengerType.Child.ToString()))
                                                            {
                                                                result.FareInformationId.Add(PassengerType.Child.ToString(), fareIds);
                                                            }
                                                        }
                                                        else if (fareInfoNode.PTCID == 5)
                                                        {
                                                            passengerCount = request.InfantCount;
                                                            ptype = PassengerType.Infant;
                                                            List<int> fareIds = new List<int>();
                                                            fareIds.Add(fareInfoNode.FareID);
                                                            if (!result.FareInformationId.ContainsKey(PassengerType.Infant.ToString()))
                                                            {
                                                                result.FareInformationId.Add(PassengerType.Infant.ToString(), fareIds);
                                                            }
                                                        }


                                                        double baseFare = (Convert.ToDouble(fareInfoNode.DisplayFareAmt));
                                                        if (transFeeType == "P")
                                                        {
                                                            baseFare += baseFare * (double)transactionFee / 100;
                                                        }
                                                        else
                                                        {
                                                            baseFare += (double)transactionFee;
                                                        }


                                                        //tax = Convert.ToDouble(fareInfoNode.DisplayTaxSum);
                                                        tax = Convert.ToDouble(fareInfoNode.BaseFareAmtInclTax - fareInfoNode.BaseFareAmt);

                                                        result.Price.SupplierCurrency = fareQuoteResponse.RequestedCurrencyOfFareQuote;
                                                        result.Price.SupplierPrice += (decimal)(tax + baseFare) * (passengerCount);
                                                        result.Price.RateOfExchange = rateOfExchange;
                                                        //result.Price.TransactionFee = transactionFee * rateOfExchange;
                                                        result.Price.TransactionFee = transactionFee;
                                                        bookingClass = fareInfoNode.FCCode;
                                                        cabin = fareInfoNode.Cabin;
                                                        result.Tax += (Math.Round(tax * (double)rateOfExchange, decimalValue)) * (passengerCount);
                                                        result.BaseFare += (Math.Round(baseFare * (double)rateOfExchange, decimalValue)) * (passengerCount);
                                                        result.TotalFare = result.BaseFare + result.Tax;
                                                        newFareRule.FareInfoRef = fareInfoNode.FareID.ToString();
                                                        if (ptype == PassengerType.Adult)
                                                        {
                                                            if (request.AdultCount > 0)
                                                            {

                                                                result.FareBreakdown[0].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.AdultCount;
                                                                result.FareBreakdown[0].SellingFare = result.FareBreakdown[0].TotalFare * request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].BaseFare = (baseFare * (double)rateOfExchange) * request.AdultCount;
                                                                result.FareBreakdown[0].SupplierFare = (double)(baseFare + tax) * request.AdultCount;
                                                                result.FareBreakdown[0].FareType = onwardFlightSegmentFareTypes[p].FareTypeName;

                                                            }

                                                            newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                        }
                                                        else if (ptype == PassengerType.Child)
                                                        {

                                                            result.FareBreakdown[1].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.ChildCount;
                                                            result.FareBreakdown[1].SellingFare = result.FareBreakdown[1].TotalFare * request.ChildCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                            result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                            result.FareBreakdown[1].BaseFare = (baseFare * (double)rateOfExchange) * request.ChildCount;
                                                            result.FareBreakdown[1].SupplierFare = (double)(baseFare + tax) * request.ChildCount;
                                                            newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                        }
                                                        else if (ptype == PassengerType.Infant)
                                                        {
                                                            if (request.InfantCount > 0)
                                                            {
                                                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                                                {
                                                                    result.FareBreakdown[2].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.InfantCount;
                                                                    result.FareBreakdown[2].SellingFare = result.FareBreakdown[2].TotalFare * request.InfantCount;
                                                                    result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                                    result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                                    result.FareBreakdown[2].BaseFare = (baseFare * (double)rateOfExchange) * request.InfantCount;
                                                                    result.FareBreakdown[2].SupplierFare = (double)(baseFare + tax) * request.InfantCount;

                                                                }
                                                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                                {
                                                                    result.FareBreakdown[1].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.InfantCount;
                                                                    result.FareBreakdown[1].SellingFare = result.FareBreakdown[1].TotalFare * request.InfantCount;
                                                                    result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                                    result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                                    result.FareBreakdown[1].BaseFare = (baseFare * (double)rateOfExchange) * request.InfantCount;
                                                                    result.FareBreakdown[1].SupplierFare = (double)(baseFare + tax) * request.InfantCount;
                                                                }
                                                            }

                                                            newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                        }

                                                        int legCount = 0;
                                                        for (int m = 0; m < onwardFlightSegment.FlightLegDetails.Length; m++)
                                                        {
                                                            FlightLegDetail legDetail = onwardFlightSegment.FlightLegDetails[m];

                                                            for (int l = 0; l < fareQuoteResponse.LegDetails.Length; l++)
                                                            {
                                                                LegDetail leg = fareQuoteResponse.LegDetails[l];
                                                                if (leg.PFID == legDetail.PFID && leg.DepartureDate == legDetail.DepartureDate)
                                                                {
                                                                    FlightInfo fInfo = new FlightInfo();
                                                                    fInfo.Airline = result.Airline;
                                                                    fInfo.ArrivalTime = Convert.ToDateTime(leg.ArrivalDate.ToString().Replace("T", " "));
                                                                    if (onwardSegmentDetail.FlightNum.Contains("/"))
                                                                    {
                                                                        fInfo.FlightNumber = onwardSegmentDetail.FlightNum.Split('/')[m];
                                                                    }
                                                                    else
                                                                    {
                                                                        fInfo.FlightNumber = onwardSegmentDetail.FlightNum;
                                                                    }
                                                                    fInfo.DepartureTime = Convert.ToDateTime(leg.DepartureDate.ToString().Replace("T", " "));
                                                                    fInfo.Destination = new Airport(leg.Destination.Trim());
                                                                    fInfo.Origin = new Airport(leg.Origin.Trim());
                                                                    fInfo.Status = "HK";
                                                                    fInfo.SegmentFareType = onwardFlightSegmentFareTypes[k].FareTypeName;
                                                                    newFareRule.Airline = result.Airline;
                                                                    newFareRule.DepartureTime = fInfo.DepartureTime;
                                                                    newFareRule.Destination = fInfo.Destination.AirportName;
                                                                    newFareRule.Origin = fInfo.Origin.AirportName;
                                                                    newFareRule.ReturnDate = fInfo.ArrivalTime;

                                                                    fInfo.BookingClass = bookingClass;
                                                                    fInfo.CabinClass = cabin;
                                                                    fInfo.Duration = new TimeSpan(0, (int)leg.FlightTime, 0);
                                                                    fInfo.OperatingCarrier = leg.OperatingCarrier;
                                                                    fInfo.DepTerminal = leg.FromTerminal;
                                                                    fInfo.ArrTerminal = leg.ToTerminal;
                                                                    fInfo.Craft = onwardSegmentDetail.AircraftType;
                                                                    fInfo.UapiSegmentRefKey = onwardSegmentDetail.LFID.ToString();
                                                                    fInfo.Stops = onwardSegmentDetail.Stops;
                                                                    fInfo.Group = 0;
                                                                    if (onwardSegmentDetail.Origin == request.Segments[0].Origin)
                                                                    {
                                                                        result.Flights[0][legCount] = fInfo;
                                                                    }
                                                                    legCount++;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        result.FareRules.Add(newFareRule);
                                                    }





                                                    for (int q = 0; q < returnFlightSegmentFareTypes[c].FareInfos.Length; q++)
                                                    {
                                                        #region Return FareInfo

                                                        string bookingClass = returnFlightSegmentFareTypes[c].FareInfos[q].FCCode;
                                                        string cabin = returnFlightSegmentFareTypes[c].FareInfos[q].Cabin;

                                                        FareRule newFareRule = new FareRule();

                                                        ApplicableTaxDetail[] taxNodeList = returnFlightSegmentFareTypes[c].FareInfos[q].ApplicableTaxDetails;
                                                        FareInfo fareInfoNode = returnFlightSegmentFareTypes[c].FareInfos[q];
                                                        PassengerType ptype = PassengerType.Adult;

                                                        double tax = 0;
                                                        if (result.Price == null)
                                                        {
                                                            result.Price = new PriceAccounts();
                                                        }

                                                        if (fareInfoNode.PTCID.ToString() == "1")
                                                        {
                                                            passengerCount = request.AdultCount + request.SeniorCount;
                                                            ptype = PassengerType.Adult;
                                                            List<int> fareIds = new List<int>();
                                                            fareIds.Add(fareInfoNode.FareID);
                                                            if (!result.FareInformationId.ContainsKey(PassengerType.Adult.ToString()))
                                                            {
                                                                result.FareInformationId.Add(PassengerType.Adult.ToString(), fareIds);
                                                            }
                                                            else
                                                            {
                                                                result.FareInformationId[PassengerType.Adult.ToString()].Add(fareInfoNode.FareID);
                                                            }

                                                            foreach (ApplicableTaxDetail taxNode in taxNodeList)
                                                            {
                                                                foreach (TaxDetail td in fareQuoteResponse.TaxDetails)
                                                                {
                                                                    if (taxNode.TaxID == td.TaxID && (td.CodeType.Contains("FBA")))
                                                                    {
                                                                        result.Price.BaggageCharge = 0;
                                                                        result.BaggageIncludedInFare += "," + td.TaxDesc;
                                                                        result.IsBaggageIncluded = true;

                                                                    }
                                                                }
                                                            }

                                                            if (result.BaggageIncludedInFare.IndexOf(",") < 0)
                                                            {
                                                                result.BaggageIncludedInFare += ",0";
                                                            }
                                                        }
                                                        else if (fareInfoNode.PTCID.ToString() == "6")
                                                        {
                                                            passengerCount = request.ChildCount;
                                                            ptype = PassengerType.Child;
                                                            List<int> fareIds = new List<int>();
                                                            fareIds.Add(fareInfoNode.FareID);
                                                            if (!result.FareInformationId.ContainsKey(PassengerType.Child.ToString()))
                                                            {
                                                                result.FareInformationId.Add(PassengerType.Child.ToString(), fareIds);
                                                            }
                                                            else
                                                            {
                                                                result.FareInformationId[PassengerType.Child.ToString()].Add(fareInfoNode.FareID);
                                                            }
                                                        }
                                                        else if (fareInfoNode.PTCID == 5)
                                                        {
                                                            passengerCount = request.InfantCount;
                                                            ptype = PassengerType.Infant;
                                                            List<int> fareIds = new List<int>();
                                                            fareIds.Add(fareInfoNode.FareID);
                                                            if (!result.FareInformationId.ContainsKey(PassengerType.Infant.ToString()))
                                                            {
                                                                result.FareInformationId.Add(PassengerType.Infant.ToString(), fareIds);
                                                            }
                                                            else
                                                            {
                                                                result.FareInformationId[PassengerType.Infant.ToString()].Add(fareInfoNode.FareID);
                                                            }
                                                        }





                                                        double baseFare = (Convert.ToDouble(fareInfoNode.DisplayFareAmt));
                                                        if (transFeeType == "P")
                                                        {
                                                            baseFare += baseFare * (double)transactionFee / 100;
                                                        }
                                                        else
                                                        {
                                                            baseFare += (double)transactionFee;
                                                        }

                                                        //tax = Convert.ToDouble(fareInfoNode.DisplayTaxSum);
                                                        tax = Convert.ToDouble(fareInfoNode.BaseFareAmtInclTax - fareInfoNode.BaseFareAmt);

                                                        result.Price.SupplierPrice += (decimal)(tax + baseFare) * (passengerCount);

                                                        bookingClass = fareInfoNode.FCCode;
                                                        cabin = fareInfoNode.Cabin;
                                                        result.Tax += (Math.Round(tax * (double)rateOfExchange, decimalValue)) * (passengerCount);
                                                        result.BaseFare += (Math.Round(baseFare * (double)rateOfExchange, decimalValue)) * (passengerCount);
                                                        result.TotalFare = result.BaseFare + result.Tax;

                                                        newFareRule.FareInfoRef = fareInfoNode.FareID.ToString();

                                                        if (ptype == PassengerType.Adult)
                                                        {
                                                            if (request.AdultCount > 0)
                                                            {

                                                                result.FareBreakdown[0].TotalFare += ((baseFare + tax) * (double)rateOfExchange) * request.AdultCount;
                                                                result.FareBreakdown[0].SellingFare += result.FareBreakdown[0].TotalFare * request.AdultCount;
                                                                result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                                result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                                result.FareBreakdown[0].BaseFare += (baseFare * (double)rateOfExchange) * request.AdultCount;
                                                                result.FareBreakdown[0].SupplierFare += (double)(baseFare + tax) * request.AdultCount;
                                                                result.FareBreakdown[0].FareType = returnFlightSegmentFareTypes[c].FareTypeName;

                                                            }

                                                            newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                        }
                                                        else if (ptype == PassengerType.Child)
                                                        {

                                                            result.FareBreakdown[1].TotalFare += ((baseFare + tax) * (double)rateOfExchange) * request.ChildCount;
                                                            result.FareBreakdown[1].SellingFare += result.FareBreakdown[1].TotalFare * request.ChildCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                            result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                            result.FareBreakdown[1].BaseFare += (baseFare * (double)rateOfExchange) * request.ChildCount;
                                                            result.FareBreakdown[1].SupplierFare += (double)(baseFare + tax) * request.ChildCount;
                                                            newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                        }
                                                        else if (ptype == PassengerType.Infant)
                                                        {
                                                            if (request.InfantCount > 0)
                                                            {
                                                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                                                {

                                                                    result.FareBreakdown[2].TotalFare += ((baseFare + tax) * (double)rateOfExchange) * request.InfantCount;
                                                                    result.FareBreakdown[2].SellingFare += result.FareBreakdown[2].TotalFare * request.InfantCount;
                                                                    result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                                    result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                                    result.FareBreakdown[2].BaseFare += (baseFare * (double)rateOfExchange) * request.InfantCount;
                                                                    result.FareBreakdown[2].SupplierFare += (double)(baseFare + tax) * request.InfantCount;

                                                                }
                                                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                                {

                                                                    result.FareBreakdown[1].TotalFare += ((baseFare + tax) * (double)rateOfExchange) * request.InfantCount;
                                                                    result.FareBreakdown[1].SellingFare += result.FareBreakdown[1].TotalFare * request.InfantCount;
                                                                    result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                                    result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                                    result.FareBreakdown[1].BaseFare += (baseFare * (double)rateOfExchange) * request.InfantCount;
                                                                    result.FareBreakdown[1].SupplierFare += (double)(baseFare + tax) * request.InfantCount;
                                                                }


                                                            }

                                                            newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                        }



                                                        int legCount = 0;
                                                        for (int m = 0; m < returnFlightSegment.FlightLegDetails.Length; m++)
                                                        {
                                                            FlightLegDetail legDetail = returnFlightSegment.FlightLegDetails[m];

                                                            for (int l = 0; l < fareQuoteResponse.LegDetails.Length; l++)
                                                            {
                                                                LegDetail leg = fareQuoteResponse.LegDetails[l];
                                                                if (leg.PFID == legDetail.PFID && leg.DepartureDate == legDetail.DepartureDate)
                                                                {
                                                                    FlightInfo fInfo = new FlightInfo();
                                                                    fInfo.Airline = result.Airline;
                                                                    fInfo.ArrivalTime = Convert.ToDateTime(leg.ArrivalDate.ToString().Replace("T", " "));
                                                                    if (returnSegmentDetail.FlightNum.Contains("/"))
                                                                    {
                                                                        fInfo.FlightNumber = returnSegmentDetail.FlightNum.Split('/')[m];
                                                                    }
                                                                    else
                                                                    {
                                                                        fInfo.FlightNumber = returnSegmentDetail.FlightNum;
                                                                    }
                                                                    fInfo.DepartureTime = Convert.ToDateTime(leg.DepartureDate.ToString().Replace("T", " "));
                                                                    fInfo.Destination = new Airport(leg.Destination.Trim());
                                                                    fInfo.Origin = new Airport(leg.Origin.Trim());
                                                                    fInfo.Status = "HK";
                                                                    fInfo.SegmentFareType = returnFlightSegmentFareTypes[c].FareTypeName;
                                                                    newFareRule.Airline = result.Airline;
                                                                    newFareRule.DepartureTime = fInfo.DepartureTime;
                                                                    newFareRule.Destination = fInfo.Destination.AirportName;
                                                                    newFareRule.Origin = fInfo.Origin.AirportName;
                                                                    newFareRule.ReturnDate = fInfo.ArrivalTime;

                                                                    fInfo.BookingClass = bookingClass;
                                                                    fInfo.CabinClass = cabin;
                                                                    fInfo.Duration = new TimeSpan(0, (int)leg.FlightTime, 0);
                                                                    fInfo.OperatingCarrier = leg.OperatingCarrier;
                                                                    fInfo.DepTerminal = leg.FromTerminal;
                                                                    fInfo.ArrTerminal = leg.ToTerminal;
                                                                    fInfo.Craft = returnSegmentDetail.AircraftType;
                                                                    fInfo.UapiSegmentRefKey = returnSegmentDetail.LFID.ToString();
                                                                    fInfo.Stops = returnSegmentDetail.Stops;
                                                                    fInfo.Group = 1;
                                                                    if (returnSegmentDetail.Origin == request.Segments[0].Destination)
                                                                    {
                                                                        result.Flights[1][legCount] = fInfo;
                                                                    }
                                                                    legCount++;
                                                                    break;
                                                                }
                                                            }
                                                        }


                                                        result.FareRules.Add(newFareRule);

                                                        #endregion


                                                    }

                                                    //  result.Price.TransactionFee += transactionFee;
                                                    // GetBaggageServicesQuotes(result, securityToken);
                                                    resultList.Add(result);

                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }

                            else
                            {

                                //Iterate through different fare types and prepare a flight result object.

                                if (request.Type == SearchType.OneWay && onwardFlightSegmentFareTypes.Count > 0)
                                {
                                    for (int p = 0; p < onwardFlightSegmentFareTypes.Count; p++)
                                    {


                                        if (onwardFlightSegmentFareTypes[p].FareInfos != null && onwardFlightSegmentFareTypes[p].FareInfos.Length > 0)
                                        {

                                            SearchResult result = new SearchResult();
                                            result.IsLCC = true;
                                            int passengerCount = 0;
                                            result.ValidatingAirline = onwardSegmentDetail.CarrierCode.Trim();
                                            result.Airline = onwardSegmentDetail.CarrierCode.Trim();
                                            result.ResultBookingSource = BookingSource.AirIndiaExpressIntl;
                                            result.GUID = securityToken;
                                            result.Currency = fareQuoteResponse.RequestedCurrencyOfFareQuote;
                                            result.FareBreakdown = new Fare[fareBreakDownCount];

                                            //Get the list of fareinfo objects for the particular fare type
                                            List<FareInfo> fareInfos = new List<FareInfo>();

                                            for (int f = 0; f < onwardFlightSegmentFareTypes[p].FareInfos.Length; f++)
                                            {
                                                FareInfo fareInfoNode = onwardFlightSegmentFareTypes[p].FareInfos[f];
                                                fareInfos.Add(fareInfoNode);
                                            }
                                            if (request.Type == SearchType.OneWay)
                                            {
                                                result.Flights = new FlightInfo[1][];
                                                result.Flights[0] = new FlightInfo[onwardFlightSegment.FlightLegDetails.Length];
                                            }

                                            if (result.Price == null)
                                            {
                                                result.Price = new PriceAccounts();
                                            }

                                            if (result.FareInformationId == null)
                                            {
                                                result.FareInformationId = new Dictionary<string, List<int>>();
                                                result.FareRules = new List<FareRule>();
                                            }

                                            if (agentBaseCurrency != currency)
                                            {
                                                result.Currency = agentBaseCurrency;
                                            }



                                            #region FareBreak down Calculation
                                            for (int j = 0; j < onwardFlightSegmentFareTypes[p].FareInfos.Length; j++)
                                            {

                                                string bookingClass = onwardFlightSegmentFareTypes[p].FareInfos[j].FCCode;
                                                string cabin = onwardFlightSegmentFareTypes[p].FareInfos[j].Cabin;

                                                if (request.AdultCount > 0)
                                                {
                                                    if (result.FareBreakdown[0] == null)
                                                    {
                                                        result.FareBreakdown[0] = new Fare();
                                                    }
                                                    result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                    result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                }

                                                if (request.ChildCount > 0)
                                                {

                                                    if (result.FareBreakdown[1] == null)
                                                    {
                                                        result.FareBreakdown[1] = new Fare();
                                                    }
                                                    result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                    result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                }
                                                //Infant Count-- if both children and infants exists in the request.
                                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                                {
                                                    if (result.FareBreakdown[2] == null)
                                                    {
                                                        result.FareBreakdown[2] = new Fare();
                                                    }
                                                    result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                    result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                }

                                                //Child Count -- If only infant count exists in the rquest.
                                                else if (request.ChildCount == 0 && request.InfantCount > 0)
                                                {
                                                    if (result.FareBreakdown[1] == null)
                                                    {
                                                        result.FareBreakdown[1] = new Fare();
                                                    }
                                                    result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                    result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                }

                                                //result.FareRules = new List<FareRule>();
                                                //result.FareInformationId = new Dictionary<string, List<int>>();

                                                FareRule newFareRule = new FareRule();
                                                FareInfo fareInfoNode = onwardFlightSegmentFareTypes[p].FareInfos[j];
                                                ApplicableTaxDetail[] taxNodeList = fareInfoNode.ApplicableTaxDetails;
                                                double tax = 0;

                                                PassengerType ptype = PassengerType.Adult;
                                                if (fareInfoNode.PTCID.ToString() == "1")
                                                {
                                                    passengerCount = request.AdultCount;
                                                    ptype = PassengerType.Adult;
                                                    result.FareType = onwardFlightSegmentFareTypes[p].FareTypeName;
                                                    result.BaggageIncludedInFare = "0";
                                                    foreach (ApplicableTaxDetail taxNode in taxNodeList)
                                                    {
                                                        foreach (TaxDetail td in fareQuoteResponse.TaxDetails)
                                                        {
                                                            //FBA -- free baggage allowance.
                                                            if (taxNode.TaxID == td.TaxID && (td.CodeType.Contains("FBA")))
                                                            {
                                                                result.Price.BaggageCharge = 0;
                                                                result.BaggageIncludedInFare = td.TaxDesc;
                                                                result.IsBaggageIncluded = true;
                                                                break;
                                                            }
                                                        }
                                                    }

                                                    // List<int> fareIds = new List<int>();
                                                    // fareIds.Add(fareInfoNode.FareID);
                                                    // result.FareInformationId.Add(PassengerType.Adult.ToString(), fareIds);

                                                    List<int> fareIds = new List<int>();
                                                    fareIds.Add(fareInfoNode.FareID);
                                                    if (!result.FareInformationId.ContainsKey(PassengerType.Adult.ToString()))
                                                    {
                                                        result.FareInformationId.Add(PassengerType.Adult.ToString(), fareIds);
                                                    }

                                                }
                                                else if (fareInfoNode.PTCID.ToString() == "6")
                                                {
                                                    passengerCount = request.ChildCount;
                                                    ptype = PassengerType.Child;
                                                    List<int> fareIds = new List<int>();
                                                    fareIds.Add(fareInfoNode.FareID);
                                                    if (!result.FareInformationId.ContainsKey(PassengerType.Child.ToString()))
                                                    {
                                                        result.FareInformationId.Add(PassengerType.Child.ToString(), fareIds);
                                                    }

                                                }
                                                else if (fareInfoNode.PTCID == 5)
                                                {
                                                    passengerCount = request.InfantCount;
                                                    ptype = PassengerType.Infant;
                                                    List<int> fareIds = new List<int>();
                                                    fareIds.Add(fareInfoNode.FareID);
                                                    if (!result.FareInformationId.ContainsKey(PassengerType.Infant.ToString()))
                                                    {
                                                        result.FareInformationId.Add(PassengerType.Infant.ToString(), fareIds);
                                                    }

                                                }
                                                double baseFare = (Convert.ToDouble(fareInfoNode.DisplayFareAmt));
                                                if (transFeeType == "P")
                                                {
                                                    baseFare += baseFare * (double)transactionFee / 100;
                                                }
                                                else
                                                {
                                                    baseFare += (double)transactionFee;
                                                }

                                                //tax = Convert.ToDouble(fareInfoNode.DisplayTaxSum);
                                                tax = Convert.ToDouble(fareInfoNode.BaseFareAmtInclTax - fareInfoNode.BaseFareAmt);

                                                result.Price.RateOfExchange = rateOfExchange;
                                                result.Price.SupplierCurrency = fareQuoteResponse.RequestedCurrencyOfFareQuote;
                                                result.Price.SupplierPrice += (decimal)(baseFare + tax) * passengerCount;
                                                result.Price.TransactionFee = transactionFee;

                                                result.Tax += (Math.Round(tax * (double)rateOfExchange, decimalValue)) * (passengerCount);
                                                result.BaseFare += (Math.Round(baseFare * (double)rateOfExchange, decimalValue)) * (passengerCount);
                                                result.TotalFare = result.BaseFare + result.Tax;


                                                if (ptype == PassengerType.Adult)
                                                {
                                                    if (request.AdultCount > 0)
                                                    {

                                                        result.FareBreakdown[0].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.AdultCount;
                                                        result.FareBreakdown[0].SellingFare = result.FareBreakdown[0].TotalFare * request.AdultCount;
                                                        result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                                        result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                                        result.FareBreakdown[0].BaseFare = (baseFare * (double)rateOfExchange) * request.AdultCount;
                                                        result.FareBreakdown[0].SupplierFare = (double)(baseFare + tax) * request.AdultCount;
                                                        result.FareBreakdown[0].FareType = onwardFlightSegmentFareTypes[p].FareTypeName;

                                                    }

                                                    newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                }
                                                else if (ptype == PassengerType.Child)
                                                {

                                                    result.FareBreakdown[1].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.ChildCount;
                                                    result.FareBreakdown[1].SellingFare = result.FareBreakdown[1].TotalFare * request.ChildCount;
                                                    result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                                    result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                                    result.FareBreakdown[1].BaseFare = (baseFare * (double)rateOfExchange) * request.ChildCount;
                                                    result.FareBreakdown[1].SupplierFare = (double)(baseFare + tax) * request.ChildCount;
                                                    newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                }
                                                else if (ptype == PassengerType.Infant)
                                                {
                                                    if (request.InfantCount > 0)
                                                    {
                                                        if (request.ChildCount > 0 && request.InfantCount > 0)
                                                        {

                                                            result.FareBreakdown[2].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.InfantCount;
                                                            result.FareBreakdown[2].SellingFare = result.FareBreakdown[2].TotalFare * request.InfantCount;
                                                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[2].BaseFare = (baseFare * (double)rateOfExchange) * request.InfantCount;
                                                            result.FareBreakdown[2].SupplierFare = (double)(baseFare + tax) * request.InfantCount;

                                                        }
                                                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                                        {

                                                            result.FareBreakdown[1].TotalFare = ((baseFare + tax) * (double)rateOfExchange) * request.InfantCount;
                                                            result.FareBreakdown[1].SellingFare = result.FareBreakdown[1].TotalFare * request.InfantCount;
                                                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                                            result.FareBreakdown[1].BaseFare = (baseFare * (double)rateOfExchange) * request.InfantCount;
                                                            result.FareBreakdown[1].SupplierFare = (double)(baseFare + tax) * request.InfantCount;
                                                        }


                                                    }

                                                    newFareRule.FareBasisCode = fareInfoNode.FBCode;
                                                }

                                                int legCount = 0;
                                                for (int m = 0; m < onwardFlightSegment.FlightLegDetails.Length; m++)
                                                {
                                                    FlightLegDetail legDetail = onwardFlightSegment.FlightLegDetails[m];

                                                    for (int l = 0; l < fareQuoteResponse.LegDetails.Length; l++)
                                                    {
                                                        LegDetail leg = fareQuoteResponse.LegDetails[l];
                                                        if (leg.PFID == legDetail.PFID && leg.DepartureDate == legDetail.DepartureDate)
                                                        {
                                                            FlightInfo fInfo = new FlightInfo();
                                                            fInfo.Airline = result.Airline;
                                                            fInfo.ArrivalTime = Convert.ToDateTime(leg.ArrivalDate.ToString().Replace("T", " "));
                                                            if (onwardSegmentDetail.FlightNum.Contains("/"))
                                                            {
                                                                fInfo.FlightNumber = onwardSegmentDetail.FlightNum.Split('/')[m];
                                                            }
                                                            else
                                                            {
                                                                fInfo.FlightNumber = onwardSegmentDetail.FlightNum;
                                                            }
                                                            fInfo.DepartureTime = Convert.ToDateTime(leg.DepartureDate.ToString().Replace("T", " "));
                                                            fInfo.Destination = new Airport(leg.Destination.Trim());
                                                            fInfo.Origin = new Airport(leg.Origin.Trim());
                                                            fInfo.Status = "HK";
                                                            fInfo.SegmentFareType = onwardFlightSegmentFareTypes[p].FareTypeName;
                                                            newFareRule.Airline = result.Airline;
                                                            newFareRule.DepartureTime = fInfo.DepartureTime;
                                                            newFareRule.Destination = fInfo.Destination.AirportName;
                                                            newFareRule.Origin = fInfo.Origin.AirportName;
                                                            newFareRule.ReturnDate = fInfo.ArrivalTime;

                                                            fInfo.BookingClass = bookingClass;
                                                            fInfo.CabinClass = cabin;
                                                            fInfo.Duration = new TimeSpan(0, (int)leg.FlightTime, 0);
                                                            fInfo.OperatingCarrier = leg.OperatingCarrier;
                                                            fInfo.DepTerminal = leg.FromTerminal;
                                                            fInfo.ArrTerminal = leg.ToTerminal;
                                                            fInfo.Craft = onwardSegmentDetail.AircraftType;
                                                            fInfo.UapiSegmentRefKey = onwardSegmentDetail.LFID.ToString();
                                                            fInfo.Group = 0;
                                                            if (onwardSegmentDetail.Origin == request.Segments[0].Origin)
                                                            {
                                                                result.Flights[0][legCount] = fInfo;
                                                            }
                                                            legCount++;
                                                            break;
                                                        }
                                                    }
                                                }
                                                result.FareRules.Add(newFareRule);
                                            }

                                            #endregion


                                            
                                            resultList.Add(result);

                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("(AirIndiaexpress)Failed to generate flight result", ex);
            }
            return resultList.ToArray();
        }

        /// <summary>
        /// Get the PNR summary for the Itinerary
        /// </summary>
        /// <param name="securityToken"></param>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public AirIndiaExpress.Reservation.SummaryPNRResponse GetSummaryPNR(string securityToken, FlightItinerary itinerary)
        {

            AirIndiaExpress.Reservation.SummaryPNRResponse viewPNR = new AirIndiaExpress.Reservation.SummaryPNRResponse();
            try
            {
                SummaryPNRRequest summaryPNRRequest = new SummaryPNRRequest();

                SummaryPNR summaryPNR = new SummaryPNR();

                summaryPNR.HistoricUserName = travelAgentUserName;

                //1.SecurityGUID
                summaryPNR.SecurityGUID = securityToken;

                //2.CarrierCodes
                summaryPNR.CarrierCodes = new AirIndiaExpress.Reservation.CarrierCode[1];
                summaryPNR.CarrierCodes[0] = new AirIndiaExpress.Reservation.CarrierCode();
                summaryPNR.CarrierCodes[0].AccessibleCarrierCode = carrierCode;

                //3.Action Type
                summaryPNR.ActionType = SummaryPNR.ActionTypes.GetSummary;

                //4.Reservation Info

                AirIndiaExpress.Reservation.ReservationInfo reservationInfo = new AirIndiaExpress.Reservation.ReservationInfo();
                reservationInfo.SeriesNumber = "299";
                reservationInfo.ConfirmationNumber = string.Empty;
                summaryPNR.ReservationInfo = reservationInfo;

                //5.SecurityToken 
                summaryPNR.SecurityToken = securityToken;

                //6.Carrier Currency
                summaryPNR.CarrierCurrency = AirIndiaExpress.Reservation.EnumerationsCurrencyCodeTypes.AED;

                //7.Display Currency

                summaryPNR.DisplayCurrency = AirIndiaExpress.Reservation.EnumerationsCurrencyCodeTypes.AED;

                //8. Travel Agency IATA Number
                summaryPNR.IATANum = travelAgentIATANumber;

                //9.User

                summaryPNR.User = travelAgentUserName;

                //10.ReceiptLanguageID
                summaryPNR.ReceiptLanguageID = "1";


                //11.PromoCode

                summaryPNR.PromoCode = string.Empty;

                //12.ExternalBookingID
                //If the PNR was originally imported from another system then this value will contain the reference number from the originating system

                summaryPNR.ExternalBookingID = "";

                //13.Address : Primary Address information used for the reservation contacts

                AirIndiaExpress.Reservation.Address primaryAddress = new AirIndiaExpress.Reservation.Address();
                primaryAddress.Address1 = "# 302, Tower 400";
                primaryAddress.Address2 = "Al Soor, Street No 2";
                primaryAddress.AreaCode = "";
                primaryAddress.City = "Sharjah";
                primaryAddress.Country = "United Arab Emirates";
                primaryAddress.CountryCode = "UAE";
                primaryAddress.Display = "";
                primaryAddress.PhoneNumber = "97160544470";
                primaryAddress.Postal = "3393";
                primaryAddress.State = "";
                summaryPNR.Address = primaryAddress;

                //14:Contact Infos -- Collection of Contact Info 


                int contactId = 2141;
                int personId = 214;
                long profileId = 2147483648;

                List<AirIndiaExpress.Reservation.ContactInfo> contactInfos = new List<AirIndiaExpress.Reservation.ContactInfo>();

                FlightPassenger pax1 = itinerary.Passenger[0];
                AirIndiaExpress.Reservation.ContactInfo contactInfo = new AirIndiaExpress.Reservation.ContactInfo();
                contactInfo.AreaCode = string.Empty;
                contactInfo.ContactField = "tech@cozmotravel.com";
                contactInfo.ContactID = -contactId;


                // The code that represents the specified mode for the contact. Use:0 = Home Phone 1 = Work Phone 2 = Mobile Phone 3 = Pager 4 = E-mail 5 = Fax 
                contactInfo.ContactType = AirIndiaExpress.Reservation.EnumerationsContactTypes.Email;
                contactInfo.CountryCode = string.Empty;
                contactInfo.Display = string.Empty;
                contactInfo.Extension = string.Empty;
                contactInfo.PersonOrgID = -personId;
                contactInfo.PhoneNumber = string.Empty;
                contactInfo.PreferredContactMethod = true;
                contactInfos.Add(contactInfo);
                contactId++;

                AirIndiaExpress.Reservation.ContactInfo mobilePhoneContact = new AirIndiaExpress.Reservation.ContactInfo();
                mobilePhoneContact.AreaCode = pax1.CellPhone.Split('-')[0];
                mobilePhoneContact.ContactField = pax1.CellPhone.Replace("-", "");
                mobilePhoneContact.ContactID = -contactId;
                mobilePhoneContact.ContactType = AirIndiaExpress.Reservation.EnumerationsContactTypes.MobilePhone;
                mobilePhoneContact.CountryCode = "";
                mobilePhoneContact.Display = "na";
                mobilePhoneContact.Extension = "na";
                mobilePhoneContact.PersonOrgID = -personId;
                mobilePhoneContact.PhoneNumber = pax1.CellPhone.Split('-')[1];
                mobilePhoneContact.PreferredContactMethod = false;
                contactInfos.Add(mobilePhoneContact);
                contactId++;

                AirIndiaExpress.Reservation.ContactInfo homePhoneContact = new AirIndiaExpress.Reservation.ContactInfo();
                homePhoneContact.AreaCode = pax1.CellPhone.Split('-')[0];
                homePhoneContact.ContactField = pax1.CellPhone.Replace("-", "");
                homePhoneContact.ContactID = -contactId;
                homePhoneContact.ContactType = AirIndiaExpress.Reservation.EnumerationsContactTypes.HomePhone;
                homePhoneContact.CountryCode = "";
                homePhoneContact.Display = "na";
                homePhoneContact.Extension = "na";
                homePhoneContact.PersonOrgID = -personId;
                homePhoneContact.PhoneNumber = pax1.CellPhone.Split('-')[1];
                homePhoneContact.PreferredContactMethod = false;
                contactInfos.Add(homePhoneContact);
                summaryPNR.ContactInfos = contactInfos.ToArray();

                contactId = 2141;

                //15.Passengers Information

                List<AirIndiaExpress.Reservation.Person> persons = new List<AirIndiaExpress.Reservation.Person>();

                foreach (FlightPassenger pax in itinerary.Passenger)
                {
                    AirIndiaExpress.Reservation.Person person = new AirIndiaExpress.Reservation.Person();
                    person.PersonOrgID = -personId;
                    person.FirstName = pax.FirstName;
                    person.LastName = pax.LastName;
                    person.MiddleName = string.Empty;
                    person.Age = DateTime.Now.Subtract(pax.DateOfBirth).Days / 365;
                    person.DOB = pax.DateOfBirth;
                    if (pax.Gender == Gender.Male)
                    {
                        person.Gender = AirIndiaExpress.Reservation.EnumerationsGenderTypes.Male;
                        person.Title = "Mr.";
                    }
                    else if (pax.Gender == Gender.Female)
                    {
                        person.Gender = AirIndiaExpress.Reservation.EnumerationsGenderTypes.Female;
                        person.Title = "Mrs.";
                    }
                    else
                    {
                        person.Gender = AirIndiaExpress.Reservation.EnumerationsGenderTypes.Unknown;
                        person.Title = string.Empty;
                    }
                    if (pax.Type == PassengerType.Adult)
                    {
                        person.WBCID = 1;
                        person.PTCID = 1;
                        person.PTC = "1";
                    }
                    else if (pax.Type == PassengerType.Child)
                    {
                        person.WBCID = 6;
                        person.PTCID = 6;
                        person.PTC = "6";

                    }
                    else
                    {
                        person.WBCID = 5;
                        person.PTCID = 5;
                        person.PTC = "5";
                    }

                    person.NationalityLaguageID = -2147483648;
                    person.RelationType = AirIndiaExpress.Reservation.EnumerationsRelationshipTypes.Self;
                    person.TravelsWithPersonOrgID = -2147483648;
                    person.RedressNumber = "na";
                    person.KnownTravelerNumber = "na";
                    person.MarketingOptIn = false;
                    person.UseInventory = false;
                    person.Company = string.Empty;
                    person.Comments = string.Empty;
                    person.Passport = pax.PassportNo;
                    //person.Nationality = pax.Country.Nationality;
                    person.Nationality = "UAE";
                    person.ProfileId = -profileId;
                    person.IsPrimaryPassenger = (pax.IsLeadPax ? true : false);

                    primaryAddress = new AirIndiaExpress.Reservation.Address();
                    primaryAddress.Address1 = "# 302, Tower 400";
                    primaryAddress.Address2 = "Al Soor, Street No 2";
                    primaryAddress.AreaCode = "";
                    primaryAddress.City = "Sharjah";
                    primaryAddress.Country = "United Arab Emirates";
                    primaryAddress.CountryCode = "UAE";
                    primaryAddress.Display = "";
                    primaryAddress.PhoneNumber = "97160544470";
                    primaryAddress.Postal = "3393";
                    primaryAddress.State = "";
                    person.Address = primaryAddress;


                    contactInfos = new List<AirIndiaExpress.Reservation.ContactInfo>();

                    AirIndiaExpress.Reservation.ContactInfo emailContactInfo = new AirIndiaExpress.Reservation.ContactInfo();
                    emailContactInfo.AreaCode = string.Empty;
                    emailContactInfo.ContactField = "tech@cozmotravel.com";
                    emailContactInfo.ContactID = -contactId;

                    // The code that represents the specified mode for the contact. Use:0 = Home Phone 1 = Work Phone 2 = Mobile Phone 3 = Pager 4 = E-mail 5 = Fax 
                    emailContactInfo.ContactType = AirIndiaExpress.Reservation.EnumerationsContactTypes.Email;
                    emailContactInfo.CountryCode = string.Empty;
                    emailContactInfo.Display = string.Empty;
                    emailContactInfo.Extension = string.Empty;
                    emailContactInfo.PersonOrgID = -personId;
                    emailContactInfo.PhoneNumber = string.Empty;
                    emailContactInfo.PreferredContactMethod = true;
                    contactInfos.Add(emailContactInfo);
                    contactId++;

                    mobilePhoneContact = new AirIndiaExpress.Reservation.ContactInfo();
                    mobilePhoneContact.AreaCode = pax.CellPhone.Split('-')[0];
                    mobilePhoneContact.ContactField = pax.CellPhone.Replace("-", "");
                    mobilePhoneContact.ContactID = -contactId;
                    mobilePhoneContact.ContactType = AirIndiaExpress.Reservation.EnumerationsContactTypes.MobilePhone;
                    mobilePhoneContact.CountryCode = "";
                    mobilePhoneContact.Display = "na";
                    mobilePhoneContact.Extension = "na";
                    mobilePhoneContact.PersonOrgID = -personId;
                    mobilePhoneContact.PhoneNumber = pax.CellPhone.Split('-')[1];
                    mobilePhoneContact.PreferredContactMethod = false;
                    contactInfos.Add(mobilePhoneContact);
                    contactId++;

                    homePhoneContact = new AirIndiaExpress.Reservation.ContactInfo();
                    homePhoneContact.AreaCode = pax.CellPhone.Split('-')[0];
                    homePhoneContact.ContactField = pax.CellPhone.Replace("-", "");
                    homePhoneContact.ContactID = -contactId;
                    homePhoneContact.ContactType = AirIndiaExpress.Reservation.EnumerationsContactTypes.HomePhone;
                    homePhoneContact.CountryCode = "";
                    homePhoneContact.Display = "na";
                    homePhoneContact.Extension = "na";
                    homePhoneContact.PersonOrgID = -personId;
                    homePhoneContact.PhoneNumber = pax.CellPhone.Split('-')[1];

                    homePhoneContact.PreferredContactMethod = false;
                    contactInfos.Add(homePhoneContact);

                    person.ContactInfos = contactInfos.ToArray();
                    persons.Add(person);

                    personId++;
                    profileId++;
                    contactId++;
                }

                summaryPNR.Passengers = persons.ToArray();


                //16.Segments Information

                List<AirIndiaExpress.Reservation.Segment> segments = new List<AirIndiaExpress.Reservation.Segment>();

                List<FlightInfo> Flights = new List<FlightInfo>();
                Flights.AddRange(itinerary.Segments);


                List<int> LFIDS = new List<int>();
                foreach (FlightInfo seg in itinerary.Segments)
                {
                    if (!LFIDS.Contains(Convert.ToInt32(seg.UapiSegmentRefKey)))
                    {
                        LFIDS.Add(Convert.ToInt32(seg.UapiSegmentRefKey));
                    }
                }
                int paxid = 214;
                for (int i = 0; i < LFIDS.Count; i++)
                {
                    paxid = 214;
                    //pCounter = 0;
                    int LFID = LFIDS[i];
                    Segment segment = new Segment();
                    segment.StoreFrontID = string.Empty;
                    segment.MarketingCode = string.Empty;
                    segment.PersonOrgID = -214;
                    segment.Seats = new AirIndiaExpress.Reservation.Seat[0];
                    segment.FareInformationID = itinerary.Passenger[0].Price.FareInformationID[i];//pax.Price.FareInformationID[i]; 
                    List<SpecialService> services = new List<SpecialService>();
                    for (int p = 0; p < itinerary.Passenger.Length; p++)
                    {
                        FlightPassenger pax = itinerary.Passenger[p];
                        if (pax.Type != PassengerType.Infant)
                        {
                            if (pax.FlyDubaiBaggageCharge[i] > 0)
                            {

                                SpecialService outService = new SpecialService();
                                outService.CodeType = pax.BaggageType.Split(',')[i];
                                outService.ServiceID = 249;
                                outService.SSRCategory = 22; //This is for baggage
                                outService.LogicalFlightID = LFID;
                                outService.DepartureDate = Flights.Find(delegate(FlightInfo fi) { return fi.UapiSegmentRefKey == LFID.ToString(); }).DepartureTime.Date;
                                outService.Amount = pax.FlyDubaiBaggageCharge[i];
                                outService.OverrideAmount = false;
                                outService.ChargeComment = "Baggage Charges";
                                outService.CurrencyCode = AirIndiaExpress.Reservation.EnumerationsCurrencyCodeTypes.AED;
                                outService.PersonOrgID = -paxid;
                                services.Add(outService);
                            }
                        }
                    }
                    paxid++;
                    segment.SpecialServices = services.ToArray();
                    segments.Add(segment);
                }

                summaryPNR.Segments = segments.ToArray();


                //17.Payments Information
                List<AirIndiaExpress.Reservation.Payment> paymentsInfo = new List<AirIndiaExpress.Reservation.Payment>();
                AirIndiaExpress.Reservation.Payment paymentDetails = new AirIndiaExpress.Reservation.Payment();
                paymentDetails.ReservationPaymentID = 1;
                paymentDetails.CompanyName = string.Empty;
                paymentDetails.FirstName = "FIRST NAME";
                paymentDetails.LastName = "LAST NAME";
                paymentDetails.CardType = "VISA";
                paymentDetails.CardHolder = string.Empty;
                paymentDetails.PaymentCurrency = AirIndiaExpress.Reservation.EnumerationsCurrencyCodeTypes.AED;
                paymentDetails.ISOCurrency = 1;
                paymentDetails.PaymentAmount = 0;

                //The original payment method that was used in the event that this payment is a refund  
                paymentDetails.PaymentMethod = AirIndiaExpress.Reservation.EnumerationsPaymentMethodTypes.INVC;
                paymentDetails.CardNum = string.Empty;
                paymentDetails.CVCode = "123";
                paymentDetails.ExpirationDate = DateTime.Now;
                paymentDetails.IsTACreditCard = false;
                paymentDetails.VoucherNum = -2147483648;
                paymentDetails.GcxID = "1";
                paymentDetails.GcxOpt = "1";
                paymentDetails.OriginalCurrency = Convert.ToString(AirIndiaExpress.Reservation.EnumerationsCurrencyCodeTypes.AED);
                paymentDetails.OriginalAmount = 0;
                paymentDetails.ExchangeRate = 1;
                paymentDetails.ExchangeRateDate = DateTime.Now;
                paymentDetails.PaymentComment = string.Empty;
                paymentDetails.BillingCountry = string.Empty;
                paymentsInfo.Add(paymentDetails);

                summaryPNR.Payments = paymentsInfo.ToArray();
                summaryPNRRequest.SummaryPnrRequest1 = summaryPNR;
                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(SummaryPNRRequest));
                        string filePath = @"" + xmlPath + securityToken + "_GetSummaryPNRRequest_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, summaryPNRRequest);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }


                viewPNR = processReservation.SummaryPNR(summaryPNRRequest);
                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AirIndiaExpress.Reservation.SummaryPNRResponse));
                        string filePath = @"" + xmlPath + securityToken + "_GetSummaryPNRResponse_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, viewPNR);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }

            }
            catch (Exception ex)
            {
                throw new Exception("(AirIndiaExpress)Failed to get response in GetSummaryPNR", ex);
            }
            return viewPNR;
        }


        /// <summary>
        /// Book an Air India Express Flight
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public BookingResponse Book(FlightItinerary itinerary, string securityToken)
        {

            BookingResponse bookResponse = new BookingResponse();
            try
            {
                decimal totalFare = 0;

                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    if (itinerary.Passenger[i].Price.RateOfExchange > 0)
                    {
                        totalFare += (itinerary.Passenger[i].Price.PublishedFare / itinerary.Passenger[i].Price.RateOfExchange) + (itinerary.Passenger[i].Price.Tax / itinerary.Passenger[i].Price.RateOfExchange);
                    }
                }
                //1.Check the available credit before booking
                ViewAvailableCredit availableCredit = RetrieveTravelAgencyCredit(securityToken);
                if (availableCredit != null && availableCredit.Exceptions[0].ExceptionCode == 0 && availableCredit.AvailableCredit > totalFare)
                {
                    //2.PNR-Get Summary
                    AirIndiaExpress.Reservation.SummaryPNRResponse summaryPNR = GetSummaryPNR(securityToken, itinerary);

                    if (summaryPNR != null && summaryPNR.SummaryPNRResult != null && summaryPNR.SummaryPNRResult.Exceptions[0].ExceptionCode == 0)
                    {
                        //3.Commit the reservation to the DB and get confirmation
                        AirIndiaExpress.Reservation.CreatePNRResponse commitPNR = CommitPNR(securityToken);
                        if (commitPNR != null && commitPNR.CreatePNRResult != null && commitPNR.CreatePNRResult.Exceptions[0].ExceptionCode == 0)
                        {
                            //4.Call payment service to process.
                            AirIndiaExpress.Fulfillment.ViewPNR processPNRPayment = ProcessPNRPayment(itinerary, securityToken, commitPNR.CreatePNRResult.ReservationBalance, commitPNR.CreatePNRResult.ConfirmationNumber);

                            if (processPNRPayment != null && processPNRPayment.Exceptions[0].ExceptionCode == 0) // For Successful Transaction
                            {
                                //5.Save reservation after proceesing payment

                                AirIndiaExpress.Reservation.CreatePNRResponse reservationResponse = SaveReservation(securityToken, processPNRPayment.ConfirmationNumber);
                                if (reservationResponse != null && reservationResponse.CreatePNRResult != null && reservationResponse.CreatePNRResult.Exceptions[0].ExceptionCode == 0)
                                {
                                    bookResponse.PNR = reservationResponse.CreatePNRResult.ConfirmationNumber;
                                    bookResponse.ProdType = ProductType.Flight;
                                    bookResponse.SSRDenied = false;
                                    bookResponse.SSRMessage = "";
                                    bookResponse.Status = BookingResponseStatus.Successful;
                                    itinerary.PNR = reservationResponse.CreatePNRResult.ConfirmationNumber;
                                    itinerary.FareType = "Pub";
                                }
                                else
                                {
                                    bookResponse.Status = BookingResponseStatus.Failed;
                                    Audit.Add(EventType.AirindiaExpress, Severity.High, 1, "(AirIndiaExpress)Error:Payment Processing failed ", "");
                                    bookResponse.Error = "Failed to book !";
                                }


                            }
                            else
                            {
                                bookResponse.Status = BookingResponseStatus.Failed;
                                Audit.Add(EventType.AirindiaExpress, Severity.High, 1, "(AirIndiaExpress)Error :Payment Processing failed ", "");
                                bookResponse.Error = "Failed to book !";
                            }

                        }
                        else
                        {
                            bookResponse.Status = BookingResponseStatus.Failed;
                            Audit.Add(EventType.AirindiaExpress, Severity.High, 1, "(AirIndiaExpress)Error : PNR commitment failed ", "");
                            bookResponse.Error = "Failed to book !";

                        }
                    }
                    else
                    {
                        bookResponse.Status = BookingResponseStatus.Failed;
                        Audit.Add(EventType.AirindiaExpress, Severity.High, 1, "(AirIndiaExpress)Error : Get PNR Summary Failed ", "");
                        bookResponse.Error = "Failed to book !";
                    }

                }
                else
                {
                    bookResponse.Status = BookingResponseStatus.Failed;
                    Audit.Add(EventType.AirindiaExpress, Severity.High, 1, "(AirIndiaExpress)Error  : No enough funds available to book ", "");
                    bookResponse.Error = "Failed to book !";
                    throw new Exception("(AirIndiaExpress)Failed to book due to insufficient funds");
                }
            }
            catch (Exception ex)
            {

                bookResponse.Status = BookingResponseStatus.Failed;
                bookResponse.Error = ex.Message;
                throw new Exception("(AirIndiaExpress)Booking failed. Error: " + ex.ToString(), ex);
            }
            return bookResponse;

        }

        /// <summary>
        /// Save the booking reservation
        /// </summary>
        /// <param name="securityToken"></param>
        /// <param name="confirmationNumber"></param>
        /// <returns></returns>
        public AirIndiaExpress.Reservation.CreatePNRResponse SaveReservation(string securityToken, string confirmationNumber)
        {
            CreatePNRResponse reservationResponse = null;

            try
            {
                CreatePNRRequest pnrRequest = new CreatePNRRequest();

                CreatePNR reservationRequest = new CreatePNR();

                reservationRequest.HistoricUserName = travelAgentUserName;
                //1.Security Token.
                reservationRequest.SecurityGUID = securityToken;
                //2.Carrier code.

                reservationRequest.CarrierCodes = new AirIndiaExpress.Reservation.CarrierCode[1];
                reservationRequest.CarrierCodes[0] = new AirIndiaExpress.Reservation.CarrierCode();
                reservationRequest.CarrierCodes[0].AccessibleCarrierCode = carrierCode;

                //3.Action type

                reservationRequest.ActionType = CreatePNR.ActionTypes.SaveReservation;

                //4.Reservation Info.

                AirIndiaExpress.Reservation.ReservationInfo reservationInfo = new AirIndiaExpress.Reservation.ReservationInfo();
                reservationInfo.SeriesNumber = "299";
                reservationInfo.ConfirmationNumber = confirmationNumber;

                reservationRequest.ReservationInfo = reservationInfo;

                pnrRequest.CreatePnrRequest1 = reservationRequest;

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(CreatePNRRequest));
                        string filePath = @"" + xmlPath + securityToken + "_SaveReservationRequest_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, pnrRequest);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }

                reservationResponse = processReservation.CreatePNR(pnrRequest);

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(CreatePNRResponse));
                        string filePath = @"" + xmlPath + securityToken + "_SaveReservationResponse_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, reservationResponse);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }
            }
            catch (Exception ex)
            {
                throw new Exception("(AirIndiaExpress)Failed to get response in save reservation", ex);
            }
            return reservationResponse;
        }

        /// <summary>
        /// Commit the reservation to the DB and gets confirmation
        /// </summary>
        /// <param name="securityToken"></param>
        /// <returns></returns>
        public AirIndiaExpress.Reservation.CreatePNRResponse CommitPNR(string securityToken)
        {
            AirIndiaExpress.Reservation.CreatePNRResponse commitPNR = new AirIndiaExpress.Reservation.CreatePNRResponse();
            try
            {
                CreatePNR createPNR = new CreatePNR();

                createPNR.HistoricUserName = travelAgentUserName;

                //1.Security GUID

                createPNR.SecurityGUID = securityToken;
                //2.Carrier Codes 


                //2.CarrierCodes
                createPNR.CarrierCodes = new AirIndiaExpress.Reservation.CarrierCode[1];
                createPNR.CarrierCodes[0] = new AirIndiaExpress.Reservation.CarrierCode();
                createPNR.CarrierCodes[0].AccessibleCarrierCode = carrierCode;

                //3.Action Type

                createPNR.ActionType = CreatePNR.ActionTypes.CommitSummary;

                //4.Reservation Info
                AirIndiaExpress.Reservation.ReservationInfo reservationInfo = new AirIndiaExpress.Reservation.ReservationInfo();
                reservationInfo.SeriesNumber = "299";
                reservationInfo.ConfirmationNumber = string.Empty;

                createPNR.ReservationInfo = reservationInfo;
                CreatePNRRequest pnrRequest = new CreatePNRRequest();
                pnrRequest.CreatePnrRequest1 = createPNR;
                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(CreatePNRRequest));
                        string filePath = @"" + xmlPath + securityToken + "_CommitPNRRequest_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, pnrRequest);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }

                commitPNR = processReservation.CreatePNR(pnrRequest);

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AirIndiaExpress.Reservation.CreatePNRResponse));
                        string filePath = @"" + xmlPath + securityToken + "_CommitPNRResponse_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, commitPNR);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }


            }
            catch (Exception ex)
            {
                throw new Exception("(AirIndiaExpress)Failed to get response in commit pnr", ex);
            }

            return commitPNR;
        }


        /// <summary>
        /// Call payment service to process payment
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="securityToken"></param>
        /// <returns></returns>
        public AirIndiaExpress.Fulfillment.ViewPNR ProcessPNRPayment(FlightItinerary itinerary, string securityToken, decimal reservationBalance, string ConfirmationNumber)
        {
            AirIndiaExpress.Fulfillment.ViewPNR processPNR = new AirIndiaExpress.Fulfillment.ViewPNR();
            try
            {
                PNRPayments PNRPaymentRequest = new PNRPayments();


                //1.TransactionInfo

                AirIndiaExpress.Fulfillment.TransactionInfo transactionInfo = new AirIndiaExpress.Fulfillment.TransactionInfo();
                transactionInfo.SecurityGUID = securityToken;
                transactionInfo.CarrierCodes = new AirIndiaExpress.Fulfillment.CarrierCode[1];
                transactionInfo.CarrierCodes[0] = new AirIndiaExpress.Fulfillment.CarrierCode();
                transactionInfo.CarrierCodes[0].AccessibleCarrierCode = carrierCode;
                PNRPaymentRequest.TransactionInfo = transactionInfo;

                //2.Reservation Info 
                AirIndiaExpress.Fulfillment.ReservationInfo reservationInfo = new AirIndiaExpress.Fulfillment.ReservationInfo();
                reservationInfo.SeriesNumber = "299";
                reservationInfo.ConfirmationNumber = ConfirmationNumber;
                PNRPaymentRequest.ReservationInfo = reservationInfo;

                //3.PNR Payments 

                List<ProcessPNRPayment> payments = new List<ProcessPNRPayment>();
                int personId = 214;
                int contactId = 2141;
                long profileId = 2147483648;
                foreach (FlightPassenger pax in itinerary.Passenger)
                {

                    ProcessPNRPayment payment = new ProcessPNRPayment();
                    payment.AuthorizationCode = "";//The authorization code received by the credit card processor authorizing the payment
                    payment.BaseAmount = reservationBalance; //The amount of the payment in the Reporting Currency  
                    payment.BaseCurrency = AirIndiaExpress.Fulfillment.EnumerationsCurrencyCodeTypes.AED;
                    payment.BillingCountry = "UAE";
                    payment.CardCurrency = AirIndiaExpress.Fulfillment.EnumerationsCurrencyCodeTypes.AED;
                    payment.CardHolder = "";//The name of the cardholder of the payment in the event that the payment is a credit card payment
                    payment.CardNumber = "";//	The credit card number for the payment. NOTE: This value will be masked
                    payment.CheckNumber = 1234; //The check number for the payment if this payment was made by check
                    payment.CurrencyPaid = AirIndiaExpress.Fulfillment.EnumerationsCurrencyCodeTypes.AED;
                    payment.CVCode = "123";//The CVV code on the back of most credit cards 
                    payment.DatePaid = DateTime.Now;//The accounting date, in GMT, for this payment  
                    payment.DocumentReceivedBy = "IX";
                    payment.ExchangeRate = 1;//The exchange rate used to convert from the payment amount in the CurrencyPaid to the ReservationCurrency 
                    payment.ExchangeRateDate = DateTime.Now;//	The effect date of the exchange rate used to convert from the payment amount in the CurrencyPaid to the ReservationCurrency
                    payment.ExpirationDate = DateTime.Now;//	The expiration date for the payment type when applicable  
                    payment.FFNumber = "";//The frequent flyer number for the passenger making this payment  
                    payment.FingerPrintingSessionID = "";//This is the value that is supplied by the front end websites. It contains encrypted data that the payment processors use for matching front end processing data with authorization requests
                    payment.GcxID = "1";//	In the event that this payment was made using GCX Dynamic Currency Conversion, this field is the transaction ID received from GCX 
                    payment.GcxOptOption = "1";//	In the event that this payment was made using GCX Dynamic Currency Conversion, this field represents the GCX options that was used in the conversion
                    payment.IataNumber = travelAgentIATANumber;//	The IATA number for the agency who's credit limit was used for this payment
                    payment.IsTACreditCard = false;
                    payment.OriginalAmount = reservationBalance;//The Original amount for this payment in the event this payment represents a refund
                    payment.OriginalCurrency = AirIndiaExpress.Fulfillment.EnumerationsCurrencyCodeTypes.AED;//	The Original Currency for this payment in the event this payment represents a refund 
                    payment.PaymentAmount = reservationBalance;//The amount for the payment in the payment currency in the CurrencyPaid field 
                    payment.PaymentComment = "Pay for ticket";//	Any comments added to the payment by the Call Center user or application
                    payment.PaymentMethod = AirIndiaExpress.Fulfillment.EnumerationsPaymentMethodTypes.INVC; //The method or type of payment  
                    payment.PaymentReference = "Refer";
                    payment.Reference = "Reference";
                    payment.ResponseMessage = "Response";
                    payment.TerminalID = 2;//Integer representing the credit card terminal that processed this transaction
                    payment.TransactionStatus = EnumerationsPaymentTransactionStatusTypes.APPROVED;//The payment effective status for this payment  
                    payment.UserData = "Data";
                    payment.UserID = travelAgentUserName;//The user ID of the person or system processing the payment 
                    payment.ValueCode = "Test";
                    payment.VoucherNumber = -profileId; //The voucher number for the Radixx voucher that was used for this payment in the event that this payment is a voucher payment

                    payment.Payor = new AirIndiaExpress.Fulfillment.Person();
                    payment.Payor.Address = new AirIndiaExpress.Fulfillment.Address();
                    payment.Payor.Address.Address1 = "# 302, Tower 400";
                    payment.Payor.Address.Address2 = "Al Soor, Street no 2";
                    payment.Payor.Address.AreaCode = "";
                    payment.Payor.Address.City = "Sharjah";
                    payment.Payor.Address.Country = "United Arab Emirates";
                    payment.Payor.Address.CountryCode = "AE";
                    payment.Payor.Address.Display = "";
                    payment.Payor.Address.PhoneNumber = "97160544470";
                    payment.Payor.Address.Postal = "3393";
                    payment.Payor.Address.State = "Sharjah";

                    payment.Payor.Age = DateTime.Now.AddYears(-pax.DateOfBirth.Year).Year;
                    payment.Payor.DOB = pax.DateOfBirth;
                    payment.Payor.FirstName = pax.FirstName;
                    payment.Payor.IsPrimaryPassenger = (pax.IsLeadPax ? true : false); //Indicator of the assumed primary passenger of the reservation. There can only be one primary traveller
                    payment.Payor.KnownTravelerNumber = "na";//Secure Flight Information  
                    payment.Payor.LastName = pax.LastName;
                    payment.Payor.MarketingOptIn = false;//	Indicator if the passenger wishes to subscribe to marketing information (true or false)
                    payment.Payor.MiddleName = "";
                    payment.Payor.Nationality = pax.Country.Nationality;
                    payment.Payor.NationalityLaguageID = 1; //The IATCN Language ID of the person's nationality  
                    payment.Payor.Passport = pax.PassportNo;
                    payment.Payor.PersonOrgID = -personId; //The database identifier for this person
                    payment.Payor.ProfileId = -profileId;//The ID of the Radixx profile to which this person is associated. This can be used to determine the number of times a passenger has travelled

                    if (pax.Gender == Gender.Male)
                    {
                        payment.Payor.Gender = AirIndiaExpress.Fulfillment.EnumerationsGenderTypes.Male;

                    }
                    else if (pax.Gender == Gender.Female)
                    {
                        payment.Payor.Gender = AirIndiaExpress.Fulfillment.EnumerationsGenderTypes.Female;

                    }
                    else
                    {
                        payment.Payor.Gender = AirIndiaExpress.Fulfillment.EnumerationsGenderTypes.Unknown;

                    }

                    //PTC:The iata standard code representing the Passenger Type Code for the passenger segment. (industry predefined Codes ADT, INF, CHD) 
                    //PTCID :The code representing the Passenger Type Code for the passenger segment. (Radixx predefined ID 1=ADULT, 5=INFANT, 6=CHILD)
                    //WBCID : Wieght and Balance Passenger Type Code ID for this passenger
                    if (pax.Type == PassengerType.Adult)
                    {
                        payment.Payor.PTC = "1";
                        payment.Payor.PTCID = 1;
                        payment.Payor.WBCID = 1;
                    }
                    else if (pax.Type == PassengerType.Child)
                    {
                        payment.Payor.PTC = "6";
                        payment.Payor.PTCID = 6;
                        payment.Payor.WBCID = 6;
                    }
                    else
                    {
                        payment.Payor.PTC = "5";
                        payment.Payor.PTCID = 5;
                        payment.Payor.WBCID = 5;
                    }

                    payment.Payor.RedressNumber = "na";//Secure Flight Information
                    payment.Payor.RelationType = AirIndiaExpress.Fulfillment.EnumerationsRelationshipTypes.Self;
                    payment.Payor.Title = pax.Title;
                    payment.Payor.TravelsWithPersonOrgID = -214;//If an infant is traveling with an adult then the infants person org id will be populated in this property
                    payment.Payor.UseInventory = true;
                    payment.Payor.Comments = "";//Any comments that are associated to this person
                    payment.Payor.Company = "";


                    payment.Payor.ContactInfos = new AirIndiaExpress.Fulfillment.ContactInfo[1];
                    AirIndiaExpress.Fulfillment.ContactInfo ci = new AirIndiaExpress.Fulfillment.ContactInfo();
                    ci.AreaCode = (pax.CellPhone.Contains("-") ? pax.CellPhone.Split('-')[0] : pax.CellPhone.Substring(0, 2));
                    ci.ContactField = pax.CellPhone;
                    ci.ContactID = -contactId;
                    ci.ContactType = AirIndiaExpress.Fulfillment.EnumerationsContactTypes.MobilePhone;
                    ci.CountryCode = "na";
                    ci.Display = "na";
                    ci.Extension = "na";
                    ci.PersonOrgID = -personId;
                    ci.PhoneNumber = (pax.CellPhone.Contains("-") ? pax.CellPhone.Split('-')[1] : pax.CellPhone.Substring(2, pax.CellPhone.Length - 1));
                    ci.PreferredContactMethod = false;

                    payment.Payor.ContactInfos[0] = ci;

                    payments.Add(payment);
                    personId++;
                    profileId++;
                    break;
                }
                PNRPaymentRequest.PNRPaymentsMember = payments.ToArray();
                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(PNRPayments));
                        string filePath = @"" + xmlPath + securityToken + "_ProcessPNRPaymentRequest_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, PNRPaymentRequest);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }

                processPNR = processFulfillment.ProcessPNRPayment(PNRPaymentRequest);

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AirIndiaExpress.Fulfillment.ViewPNR));
                        string filePath = @"" + xmlPath + securityToken + "_ProcessPNRPaymentResponse_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, processPNR);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }

            }
            catch (Exception ex)
            {
                throw new Exception("(AirIndiaExpress)Failed to get response in  ProcessPNRPayment", ex);

            }
            return processPNR;
        }


        /// <summary>
        /// Gets the travel agency credit balance.
        /// </summary>
        /// <returns></returns>
        public ViewAvailableCredit RetrieveTravelAgencyCredit(string securityToken)
        {
            ViewAvailableCredit availableCredit = new ViewAvailableCredit();

            try
            {
                RetrieveAvailableCredit agencyCreditRequest = new RetrieveAvailableCredit();
                //1.Security Token
                agencyCreditRequest.SecurityGUID = securityToken;

                //2.Carrier Code
                agencyCreditRequest.CarrierCodes = new AirIndiaExpress.TravelAgents.CarrierCode[1];
                agencyCreditRequest.CarrierCodes[0] = new AirIndiaExpress.TravelAgents.CarrierCode();
                agencyCreditRequest.CarrierCodes[0].AccessibleCarrierCode = carrierCode;

                //3.IATANumber

                agencyCreditRequest.IATANumber = travelAgentIATANumber;

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(RetrieveAvailableCredit));
                        string filePath = @"" + xmlPath + securityToken + "_AgencyCreditRequest_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, agencyCreditRequest);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }

                availableCredit = processTravelAgent.RetrieveAvailableCredit(agencyCreditRequest);

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(ViewAvailableCredit));
                        string filePath = @"" + xmlPath + securityToken + "_AgencyCreditResponse_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, availableCredit);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }



            }
            catch (Exception ex)
            {
                throw new Exception("(AirIndiaExpress)Failed to get response in RetrieveTravelAgencyCredit", ex);

            }
            return availableCredit;
        }


        /// <summary>
        /// Retrieve the successfully logged in Travel Agent commission
        /// </summary>
        /// <param name="securityToken"></param>
        /// <returns></returns>
        public ViewAgencyCommission RetrieveAgencyCommission(string securityToken)
        {
            ViewAgencyCommission agencyCommission = new ViewAgencyCommission();
            try
            {
                RetrieveAgencyCommission retrieveAgencyCommissionRequest = new RetrieveAgencyCommission();

                retrieveAgencyCommissionRequest.HistoricUserName = travelAgentUserName;

                //1.Security Token
                retrieveAgencyCommissionRequest.SecurityGUID = securityToken;

                //2.Carrier Code

                retrieveAgencyCommissionRequest.CarrierCodes = new AirIndiaExpress.TravelAgents.CarrierCode[1];
                retrieveAgencyCommissionRequest.CarrierCodes[0] = new AirIndiaExpress.TravelAgents.CarrierCode();
                retrieveAgencyCommissionRequest.CarrierCodes[0].AccessibleCarrierCode = carrierCode;

                //3.Currency Code

                retrieveAgencyCommissionRequest.CurrencyCode = AirIndiaExpress.TravelAgents.EnumerationsCurrencyCodeTypes.AED;

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(RetrieveAgencyCommission));
                        StreamWriter sw = new StreamWriter(@"" + xmlPath + securityToken + "_RetrieveAgencyCommissionRequest_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, retrieveAgencyCommissionRequest);
                        sw.Close();

                    }
                }
                catch { }

                agencyCommission = processTravelAgent.RetrieveAgencyCommission(retrieveAgencyCommissionRequest);
                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(ViewAgencyCommission));
                        StreamWriter sw = new StreamWriter(@"" + xmlPath + securityToken + "_RetrieveAgencyCommissionResponse_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml");
                        ser.Serialize(sw, agencyCommission);
                        sw.Close();

                    }
                }
                catch { }

            }
            catch (Exception ex)
            {
                throw new Exception("(AirIndiaExpress)Failed to get response in RetrieveAgencyCommission", ex);

            }
            return agencyCommission;
        }

        /// <summary>
        /// Gets the Fare Rules offered by Air India Express(Express Promo ,Express Value , Express Flexi ) .The fare rules are common for Domestic & International sectors    
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="dest"></param>
        /// <param name="fareBasisCode"></param>
        /// <returns></returns>
        public static List<FareRule> GetFareRule(string origin, string dest)
        {
            Trace.TraceInformation("(Air India Express).GetFareRules Enter");
            List<FareRule> fareRuleList = new List<FareRule>();

            FareRule DNfareRules = new FareRule();
            DNfareRules.Origin = origin;
            DNfareRules.Destination = dest;
            DNfareRules.Airline = "IX";
            DNfareRules.FareRuleDetail = Util.GetLCCFareRules("IX");
            DNfareRules.FareBasisCode = "";
            fareRuleList.Add(DNfareRules);

            Trace.TraceInformation("(Air India Express).GetFareRules exit");
            return fareRuleList;
        }

        /// <summary>
        ///This method returns a boolean which determine whether to  Send a Search Request to get the flight inventory based on the sector list that is provided by air india express.
        /// </summary>
        /// <param name="searchRequest"></param>
        /// <returns></returns>
        public bool AllowSearch(SearchRequest searchRequest)
        {
            bool allowSearch = false;
            try
            {
                if (ConfigurationSystem.SectorListConfig.ContainsKey("IX"))
                {
                    Dictionary<string, string> sectors = ConfigurationSystem.SectorListConfig["IX"];
                    if (sectors.ContainsKey(searchRequest.Segments[0].Origin))
                    {
                        if (sectors[searchRequest.Segments[0].Origin].Contains(searchRequest.Segments[0].Destination))
                        {
                            allowSearch = true;
                        }

                    }
                }
                else
                {
                    Core.Audit.Add(Core.EventType.AirindiaExpress, Core.Severity.High, 1, "(AirIndiaExpress): Error While reading sector from AirIndiaExpressConfig : Error" + DateTime.Now, "");

                }
            }
            catch (Exception ex)
            {
                throw new Exception("(AirIndiaExpress)Failed to read sector information in AllowSearch", ex);
            }
            return allowSearch;
        }

        /// <summary>
        /// Retrieving and cancelling an existing booking.
        /// </summary>
        /// <param name="itinerary"></param>
        /// 
        public Dictionary<string, string> CancelBooking(FlightItinerary itinerary)
        {
            Dictionary<string, string> cancellationData = new Dictionary<string, string>();
            AirIndiaExpress.Reservation.CancelPNRResponse cancelPNRResponse = null;
            try
            {
                //Step-1:RetrieveSecurityToken
                string securityToken = RetrieveSecurityToken();

                //Step-2:Retrieve PNR
                RetrievePNRRequest retrievePnrRequest = new RetrievePNRRequest();
                RetrievePNR retPNR = new RetrievePNR();
                retPNR.SecurityGUID = securityToken;
                retPNR.CarrierCodes = new AirIndiaExpress.Reservation.CarrierCode[1];
                retPNR.CarrierCodes[0] = new AirIndiaExpress.Reservation.CarrierCode();
                retPNR.CarrierCodes[0].AccessibleCarrierCode = carrierCode;
                retPNR.ActionType = RetrievePNR.ActionTypes.GetReservation;

                AirIndiaExpress.Reservation.ReservationInfo reservationInfo = new AirIndiaExpress.Reservation.ReservationInfo();
                reservationInfo.SeriesNumber = "299";
                reservationInfo.ConfirmationNumber = itinerary.PNR;
                retPNR.ReservationInfo = reservationInfo;
                retrievePnrRequest.RetrievePnrRequest1 = retPNR;

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(RetrievePNRRequest));
                        string filePath = @"" + xmlPath + securityToken + "_RetrievePNRRequest_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, retrievePnrRequest);
                        sw.Close();
                    }
                }
                catch { }

                AirIndiaExpress.Reservation.RetrievePNRResponse viewPNR = processReservation.RetrievePNR(retrievePnrRequest);
                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AirIndiaExpress.Reservation.RetrievePNRResponse));
                        string filePath = @"" + xmlPath + securityToken + "_RetrievePNRResponse_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, viewPNR);
                        sw.Close();
                    }
                }
                catch { }


                //Step-3:CancelPNR - Cancel Reservation	

                CancelPNRRequest cancelPnrRequest = new CancelPNRRequest();
                CancelPNR cancelPNR = new CancelPNR();
                cancelPNR.HistoricUserName = travelAgentUserName;
                cancelPNR.SecurityGUID = securityToken;
                cancelPNR.CarrierCodes = new AirIndiaExpress.Reservation.CarrierCode[1];
                cancelPNR.CarrierCodes[0] = new AirIndiaExpress.Reservation.CarrierCode();
                cancelPNR.CarrierCodes[0].AccessibleCarrierCode = carrierCode;
                cancelPNR.ActionType = CancelPNR.ActionTypes.CancelReservation;
                cancelPNR.ReservationInfo = reservationInfo;

                cancelPnrRequest.CancelPnrRequest1 = cancelPNR;

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(CancelPNRRequest));
                        string filePath = @"" + xmlPath + securityToken + "_CancelPNRRequest_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, cancelPnrRequest);
                        sw.Close();
                    }
                }
                catch { }

                cancelPNRResponse = processReservation.CancelPNR(cancelPnrRequest);


                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AirIndiaExpress.Reservation.CancelPNRResponse));
                        string filePath = @"" + xmlPath + securityToken + "_CancelPNRResponse_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, cancelPNRResponse);
                        sw.Close();
                    }
                }
                catch { }

                if (cancelPNRResponse != null && cancelPNRResponse.CancelPNRResult.Exceptions[0].ExceptionCode == 0)
                {
                    AirIndiaExpress.Reservation.CreatePNRResponse cancellationResponse = SaveReservation(securityToken, cancelPNRResponse.CancelPNRResult.ConfirmationNumber);
                    if (cancellationResponse != null && cancellationResponse.CreatePNRResult.Exceptions[0].ExceptionCode == 0)
                    {
                        string charges = string.Empty;
                        charges = cancelPNRResponse.CancelPNRResult.ChangeFee.ToString();

                        if (!string.IsNullOrEmpty(cancellationResponse.CreatePNRResult.ReservationCurrency.ToString()))
                        {
                            rateOfExchange = exchangeRates[cancellationResponse.CreatePNRResult.ReservationCurrency.ToString()];
                        }
                        else
                        {
                            rateOfExchange = exchangeRates["AED"];
                        }
                        cancellationData.Add("Cancelled", "True");
                        cancellationData.Add("Charges", (Convert.ToDecimal(charges) * rateOfExchange).ToString());
                    }
                    else
                    {
                        cancellationData.Add("Cancelled", "False");
                    }
                }
                else
                {
                    cancellationData.Add("Cancelled", "False");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.AirindiaExpress, Severity.High, 1, "(AirIndiaExpress)Failed to Cancel booking. Reason : " + ex.ToString(), "");
            }
            return cancellationData;
        }


        /// <summary>
        /// Retrieve the Special Service Requests(SSR's) baggage quotes for the AirIndiaExpress flights.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public DataTable GetBaggageServicesQuotes(SearchResult result, string securityToken)
        {
            ViewServiceQuotes viewServiceQuotes = null;
            try
            {
                RetrieveServiceQuotes serviceQuotes = new RetrieveServiceQuotes();
                serviceQuotes.HistoricUserName = travelAgentUserName;
                //1.SecurityGUID
                serviceQuotes.SecurityGUID = securityToken;

                //2.CarrierCodes
                serviceQuotes.CarrierCodes = new AirIndiaExpress.Pricing.CarrierCode[1];
                serviceQuotes.CarrierCodes[0] = new AirIndiaExpress.Pricing.CarrierCode();
                serviceQuotes.CarrierCodes[0].AccessibleCarrierCode = carrierCode;


                List<ServiceQuote> Quotes = new List<ServiceQuote>();

                for (int i = 0; i < result.Flights.Length; i++)
                {
                    for (int j = 0; j < result.Flights[i].Length; j++)
                    {
                        FlightInfo fInfo = result.Flights[i][j];

                        ServiceQuote serviceQuote = new ServiceQuote();

                        //3.LogicalFlightID
                        serviceQuote.LogicalFlightID = Convert.ToInt32(fInfo.UapiSegmentRefKey);//Retrieve services for this logical flight ID
                        //4.DepartureDate

                        serviceQuote.DepartureDate = fInfo.DepartureTime.Date; //Retrieve services for this departrue date
                        //5.AirportCode
                        serviceQuote.AirportCode = fInfo.Origin.AirportCode;//Airport code  
                        //6.ServiceCode
                        serviceQuote.ServiceCode = string.Empty;//Limit the search results to this service code.  
                        //7.Cabin
                        serviceQuote.Cabin = fInfo.CabinClass;//Limit the search results to this cabin.  
                        //8.Category
                        serviceQuote.Category = string.Empty;//Limit the results to this service category.  

                        //9.Currency

                        serviceQuote.Currency = AirIndiaExpress.Pricing.EnumerationsCurrencyCodeTypes.AED; //The currency to be used for pricing
                        //10.UTCOffset
                        serviceQuote.UTCOffset = 0; //Universal Time (UTC) offset  
                        //11.OperatingCarrierCode
                        serviceQuote.OperatingCarrierCode = carrierCode; //Limit the search results to this operating carrier code
                        //12.MarketingCarrierCode
                        serviceQuote.MarketingCarrierCode = carrierCode; //Limit the search results to this marketing carrier code  
                        //13.FareClass
                        serviceQuote.FareClass = string.Empty; //Limit the search results to this fare class.  
                        //14.FareBasisCode
                        serviceQuote.FareBasisCode = string.Empty; //Limit the search results to this fare basis code.  
                        //15.ReservationChannel
                        serviceQuote.ReservationChannel = AirIndiaExpress.Pricing.EnumerationsReservationChannelTypes.TPAPI; //Find services for this reservation channel. 
                        serviceQuote.DestinationAirportCode = fInfo.Destination.AirportCode;
                        Quotes.Add(serviceQuote);

                    }
                }
                serviceQuotes.RetrieveServiceQuotesMember = Quotes.ToArray();

                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(RetrieveServiceQuotes));
                        string filePath = @"" + xmlPath + securityToken + "_ServiceQuoteRequest_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, serviceQuotes);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }

                viewServiceQuotes = processPricing.RetrieveServiceQuotes(serviceQuotes);
                try
                {
                    if (ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(ViewServiceQuotes));
                        string filePath = @"" + xmlPath + securityToken + "_ServiceQuoteResponse_" + DateTime.Now.ToString("ddMMMyyyy_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, viewServiceQuotes);
                        sw.Close();
                        ser = null;
                        Audit.Add(EventType.AirindiaExpress, Severity.Normal, appUserId, filePath, "");
                    }
                }
                catch { }

                if (viewServiceQuotes != null && viewServiceQuotes.ServiceQuotes.Length > 0)
                {
                    InitBaggageTable();
                    if (agentBaseCurrency != viewServiceQuotes.ServiceQuotes[0].CurrencyCode)
                    {
                        rateOfExchange = exchangeRates[viewServiceQuotes.ServiceQuotes[0].CurrencyCode];
                    }
                    foreach (ViewServiceQuote quote in viewServiceQuotes.ServiceQuotes)
                    {

                        DataRow row = dtBaggageQuotes.NewRow();
                        row["CodeType"] = quote.CodeType;
                        row["Description"] = quote.Description;
                        row["Amount"] = quote.Amount * rateOfExchange;
                        row["QtyAvailable"] = quote.QuantityAvailable;
                        row["CategoryID"] = quote.CategoryID;
                        if (rateOfExchange > 1)
                        {
                            row["CurrencyCode"] = agentBaseCurrency;
                        }
                        else
                        {
                            row["CurrencyCode"] = quote.CurrencyCode;
                        }
                        row["LogicalFlightID"] = quote.LogicalFlightID;
                        row["ServiceID"] = quote.ServiceID;
                        row["Refundable"] = quote.Refundable;
                        row["AmountType"] = quote.AmountType;
                        dtBaggageQuotes.Rows.Add(row);
                    }

                }
                else
                {
                    CT.Core.Audit.Add(CT.Core.EventType.AirindiaExpress, CT.Core.Severity.High, 1, "(AirIndiaExpress)Failed to get response in GetBaggageServicesQuotes. Error : " + viewServiceQuotes.Exceptions[0].ExceptionDescription, "");
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.AirindiaExpress, CT.Core.Severity.High, 1, "(AirIndiaExpress)Failed to get response in GetBaggageServicesQuotes. Error : " + ex.ToString(), "");
            }

            return dtBaggageQuotes;
        }

        /// <summary>
        /// Initialise the baggage datatable
        /// </summary>
        void InitBaggageTable()
        {
            if (dtBaggageQuotes.Columns.Count <= 0)
            {
                dtBaggageQuotes.Columns.Add("CodeType", typeof(string));
                dtBaggageQuotes.Columns.Add("Description", typeof(string));
                dtBaggageQuotes.Columns.Add("Amount", typeof(decimal));
                dtBaggageQuotes.Columns.Add("CategoryId", typeof(int));
                dtBaggageQuotes.Columns.Add("LogicalFlightID", typeof(string));
                dtBaggageQuotes.Columns.Add("QtyAvailable", typeof(int));
                dtBaggageQuotes.Columns.Add("ServiceID", typeof(string));
                dtBaggageQuotes.Columns.Add("CurrencyCode", typeof(string));
                dtBaggageQuotes.Columns.Add("Refundable", typeof(bool));
                dtBaggageQuotes.Columns.Add("ApplicableTaxes", typeof(decimal));
                dtBaggageQuotes.Columns.Add("AmountType", typeof(string));
            }
        }

    }
}
