﻿#region NameSpace Region
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using SpiceJet.SGSession;
using SpiceJet.SGBooking;
using SpiceJet.SGContent;
using CT.Configuration;
using CT.Core;
using System.Data;
using System.Threading;
using System.Net;
#endregion

namespace CT.BookingEngine.GDS
{
    [Serializable]
    public class SpiceJetAPIV1 : IDisposable
    {
        /*************NEW Web Service 4.2.1 Booking Flow Without GST Input***************************
         
         * Ver No:1.0 ,July--2018,Modified by:Lokesh
         * S-1:LogOn
         * S-2:GetAvailabilityVer2  --- Previously GetAvailability(ver:3.4.13)
         * S-3:GetItineraryPrice
         * S-4:Sell Request
         * S-5:Sell SSR(if any)
         * S-6:UpdatePassenger
         * S-7:AddPaymentToBooking
         * S-8:BookingCommit
         * S-9:LogOut
         
         * Note1 : Booking flow will be the same, 
         * there is only change in Search. 
         * New API call namely GetAvailabilityVer2 will be used to Search the Sector 
         * instead of GetAvailability. 
         * This call introduced by Navitaire to improve the Search Response.

         * Note2:Value Changes :ContractVersion value is 420 instead of 340.
        
         * Note3:In GetAvailabilityVer2 Response, no fare node will be available in journey node. 
           Fares are given outside the journey. In Journey node, you will find the fare index which will be used to map the fare to the journey.
           - If one journey contains more than one fare then you will find more than one index in journey.
           - If any journey contains no fare index or ‘AvailableFares’ is null then it represents, 
             fare is not availble in such flight.

         * Note4:To get multiple pax result, two way to get the same:
           New : Enter the Pax type in PaxPriceType and enter the count of particular Pax Type as shown above
           Old : Enter no of pax type as same as the total pax. It is old logic. The old logic will work as it is.

         * Note5: BookingCommit : New Additional Parameters needs to be passed for booking flow.
         * Note6: EnableExceptionStackTrace : false,need to pass this in all the requests.
         * *********************************************************************************************/

        /*************NEW Web Service 4.2.1 Booking Flow With GST Input***************************
         * Ver No:1.0 ,July--2018,Modified by:Lokesh
         * S-1:LogOn
         * S-2:GetAvailabilityVer2  --- Previously GetAvailability(ver:3.4.13)
         * S-3:GetItineraryPrice
         * S-3.1.1:UpdateContacts(GSTNo) --- Retail Booking With GST Number
         * S-4:Sell Request
         * S-5:Sell SSR(if any)
         * S-5.1:GetBookingFromState[To get the updated break up after GSTN if provided in Update Contact Request]
         * S-6:UpdatePassenger
         * S-7:AddPaymentToBooking
         * S-8:BookingCommit
         * S-9:LogOut
         * *********************************************************************************************/

        /*************NEW Web Service 4.2.1 Cancellation Flow ***************************
         * Ver No:1.0 ,July--2018,Modified by:Lokesh
         * S-1:LogOn
         * S-2:GetBooking
         * S-3:CancelRequest-All
         * S-4:AddPaymentToBooking
         * S-5:BookingCommit
         * S-6:LogOut
         * ******************************************************************************/

        /*************NEW Web Service 4.2.1 FareRules Flow ***************************
        * Ver No:1.0 ,July--2018,Modified by:Lokesh
        * S-1:LogOn
        * S-2:GetAvailabilityVer2
        * S-3:GetFareRuleInfo
        * S-4:LogOut
        * *****************************************************************************/

        /*******************************Flight Types*************************************
         * Direct flight – Segment and leg count should be 1 in journey.
         * Via Flight – Segment count should be 1 and leg count should be more than 1.
         * Connecting flight – Segment count should be more than 1.
         * *****************************************************************************/

        string agentId;
        string agentDomain;
        string password;
        int contractVersion;
        Dictionary<string, decimal> exchangeRates;
        decimal rateOfExchange;
        string agentBaseCurrency;
        int agentDecimalValue;
        string xmlLogPath;
        string sSeatAvailResponseXSLT;
        Hashtable pricedItineraries = new Hashtable();
        DataTable dtBaggage = new DataTable();

        //Onward journey Flight Results List
        List<SearchResult> oneWayResultList = new List<SearchResult>();

        //For India GST implementation the currency code should be in INR format.
        string currencyCode;

        //Added for GST Flow 
        string gstNumber;
        string gstCompanyName;
        string gstOfficialEmail;

        //Added for Corporate Flow
        string promoCode;

        string sessionId;//Global sessionId
        int appUserId; //Login UserID
        string bookingSourceFlag;// To identify Source type(Corp or not)

        /// <summary>
        /// for sector list added by bangar
        /// </summary>
        public List<SectorList> Sectors { get; set; }

        //Added For one-one search
        List<SearchRequest> searchRequests = new List<SearchRequest>();//For One To One Search
        bool searchByAvailability;

        //Spicejet Baggage & Meal codes
        List<string> baggageCodes = new List<string>() { "EB05", "EB10", "EB15", "EB20", "EB30" };
        List<string> mealCodes = new List<string>() { "VGML", "NVML", "NVSW", "VGSW", "NCC1", "NCC2", "NCC6", "NCC4", "NCC5", "VCC5", "VCC2", "VCC6", "JNML", "JNSW", "DNVL", "DBML", "GFNV", "GFVG", "GFCM", "NVRT", "FPML", "LCVS", "LCNS","GOBX","VMAX","NMAX" };
        /********************Search Variables******************/

        //1.Search Request
        SearchRequest request = new SearchRequest();

        //2.Signature of the Login response
        string loginSignature = "";

        //3.Search results will be combined for return search and one way search
        List<SearchResult> CombinedSearchResults = new List<SearchResult>();

        //4.Web Service for all operations except the Login & Log Out Operations
        BookingManagerClient bookingAPI = new BookingManagerClient();

        //5.Web Service for Login and Logout
        SessionManagerClient sessionAPI = new SessionManagerClient();

        //6.Web Service for fare rules
        ContentManagerClient contentAPI = new ContentManagerClient();

        const int sessionTimeOut = 10;//SpiceJet session time out (10 minutes)

        /****************************End***********************/

        public SpiceJetAPIV1()
        {
            xmlLogPath = ConfigurationSystem.SpiceJetConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMMyyyy") + "\\";
            sSeatAvailResponseXSLT = ConfigurationSystem.XslconfigPath + "\\NavitaireSeatMap.xslt";
            if (!Directory.Exists(xmlLogPath))
            {
                Directory.CreateDirectory(xmlLogPath);
            }
            contractVersion = Convert.ToInt32(ConfigurationSystem.SpiceJetConfig["ContractVersion"]);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;//Added by Lokesh on 27/02/2018.
            if (dtBaggage.Columns.Count == 0)
            {
                dtBaggage.Columns.Add("Code", typeof(string));
                dtBaggage.Columns.Add("Price", typeof(decimal));
                dtBaggage.Columns.Add("Group", typeof(int));
                dtBaggage.Columns.Add("Currency", typeof(string));
                dtBaggage.Columns.Add("Description", typeof(string));
                dtBaggage.Columns.Add("QtyAvailable", typeof(int));
            }
            //Assign the web service url from source xml config
            bookingAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.SpiceJetConfig["BookingService"]);
            sessionAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.SpiceJetConfig["SessionService"]);
            contentAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.SpiceJetConfig["ContentService"]);
            //To retrive the fare rules used the below properties
            if (contentAPI.Endpoint.Binding is System.ServiceModel.BasicHttpBinding)
            {
                ((System.ServiceModel.BasicHttpBinding)contentAPI.Endpoint.Binding).MaxReceivedMessageSize = int.MaxValue;
                ((System.ServiceModel.BasicHttpBinding)contentAPI.Endpoint.Binding).MaxBufferSize = int.MaxValue;
            }
        }

        public string LoginName
        {
            get { return agentId; }
            set { agentId = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string AgentDomain
        {
            get { return agentDomain; }
            set { agentDomain = value; }
        }

        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public string AgentBaseCurrency
        {
            get { return agentBaseCurrency; }
            set { agentBaseCurrency = value; }
        }

        public int AgentDecimalValue
        {
            get { return agentDecimalValue; }
            set { agentDecimalValue = value; }
        }
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }

        public string GSTNumber
        {
            get { return gstNumber; }
            set { gstNumber = value; }
        }
        public string GSTCompanyName
        {
            get { return gstCompanyName; }
            set { gstCompanyName = value; }
        }
        public string GSTOfficialEmail
        {
            get { return gstOfficialEmail; }
            set { gstOfficialEmail = value; }
        }
        public string PromoCode
        {
            get { return promoCode; }
            set { promoCode = value; }
        }

        public int AppUserID
        {
            get { return appUserId; }
            set { appUserId = value; }
        }

        public string SessionID
        {
            get { return sessionId; }
            set { sessionId = value; }
        }
        public string BookingSourceFlag
        {
            get { return bookingSourceFlag; }
            set { bookingSourceFlag = value; }
        }

        public bool SearchByAvailability
        {
            get { return searchByAvailability; }
            set { searchByAvailability = value; }
        }

        /// <summary>
        /// Authenticates and creates a session for the specified user
        /// </summary>
        /// <returns></returns>
        private LogonResponse Login()
        {
            //1.The signature will be assigned from this method.
            //2.Till Search To Book One Signature Should be carried.
            //3.Both Normal Search and For Combination Search also.
            //4.This needs to be the same till search to book.
            LogonResponse response = null;
            try
            {
                LogonRequest request = new LogonRequest();
                request.logonRequestData = new LogonRequestData();
                request.logonRequestData.AgentName = agentId;
                request.logonRequestData.DomainCode = agentDomain;
                request.logonRequestData.LocationCode = "";
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.Password = password;
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.TerminalInfo = "";
                request.logonRequestData.ClientName = "";
                request.ContractVersion = contractVersion;
                request.EnableExceptionStackTrace = false;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(LogonRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGLogonRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize LogonRequest. Reason : " + ex.ToString(), "");
                }
                response = sessionAPI.Logon(request);
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(LogonResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGLogonResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, response);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize LogonResponse. Reason : " + ex.ToString(), "");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SpiceJet Login failed. Reason : " + ex.Message, ex);
            }
            return response;
        }


        /// <summary>
        /// Ends a user’s session and clears session from the server.
        /// </summary>
        /// <param name="signature"></param>
        /// <returns></returns>
        private LogoutResponse Logout(string signature)
        {
            LogoutResponse response = new LogoutResponse();

            try
            {
                LogoutRequest request = new LogoutRequest();

                request.Signature = signature;
                request.ContractVersion = contractVersion;
                request.EnableExceptionStackTrace = false;

                XmlSerializer ser = new XmlSerializer(typeof(LogoutRequest));
                string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGLogoutRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, request);
                sw.Close();

                response = sessionAPI.Logout(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Login, Severity.Normal, appUserId, "SpiceJet Logout failed.Reason: " + appUserId.ToString() + " " + DateTime.Now+ex.ToString(), "");
               
            }
            finally
            {
                Audit.Add(EventType.Login, Severity.Normal, appUserId, "SpiceJet Clearing Session For the User : " + appUserId.ToString() + " " + DateTime.Now, "");
            }
            return response;
        }

        /// <summary>
        /// Gets flight availability and fare information for a list of availability requests
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchResult[] Search(SearchRequest request)
        {
            SearchResult[] results = new SearchResult[0];
            List<SearchResult> ResultList = new List<SearchResult>();

            try
            {
                if (AllowSearch(request))//Allow search only if the requested origin and destination is avaialble in the sectorlist provided by Indigo.
                {
                    //For SearchBy Availability Assign Signature from basket.
                    LogonResponse loginResponse;
                    if (request.SearchBySegments)//Search By Availability
                    {
                        loginResponse = null;
                    }
                    else//SearchByPrice
                    {
                        loginResponse = Login();
                    }

                    if ((loginResponse != null) || request.SearchBySegments)
                    {

                        this.request = request;
                        if (loginResponse != null)
                        {
                            loginSignature = loginResponse.Signature;
                        }

                        /*Note1:In 4.2V Fare node will be given separately in GetBookingResponse*/

                        if (request.Type == SearchType.Return)
                        {
                            if (!request.SearchBySegments)//This is for regular search
                            {
                                SearchForAllFares();
                            }
                            else if (request.SearchBySegments)//For One to One Search
                            {

                                //Dictionary<string, int> readySources = new Dictionary<string, int>();
                                //Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();

                                //listOfThreads.Add("IG1", new WaitCallback(SearchRoutingReturn));
                                //listOfThreads.Add("IG2", new WaitCallback(SearchRoutingReturn));

                                SearchRequest onwardRequest = request.Copy();
                                onwardRequest.Type = SearchType.OneWay;
                                onwardRequest.Segments = new FlightSegment[1];
                                onwardRequest.Segments[0] = request.Segments[0];

                                SearchRequest returnRequest = request.Copy();
                                returnRequest.Type = SearchType.Return;
                                returnRequest.Segments = new FlightSegment[1];
                                returnRequest.Segments[0] = request.Segments[1];

                                searchRequests.Add(onwardRequest);
                                searchRequests.Add(returnRequest);

                                SearchRoutingReturn(0);
                                SearchRoutingReturn(1);


                                /*****************************Special Round Trip******************/
                                //Additional Request to call round trip fares in combination search
                                //For Special Round Trip Fares Always The Infant Count Should Be Zero
                                if (request.InfantCount <= 0 && request.Segments[0].flightCabinClass!=CabinClass.Business)
                                {
                                    SpecialRoundTripFares(2);
                                }
                                /*****************************Special Round Trip******************/


                                //    eventFlag = new AutoResetEvent[2];
                                //    readySources.Add("IG1", 1200);
                                //    readySources.Add("IG2", 1200);

                                //    int j = 0;
                                //    //Start each fare search within a Thread which will automatically terminate after the results are received.
                                //    foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                                //    {
                                //        if (readySources.ContainsKey(deThread.Key))
                                //        {
                                //            ThreadPool.QueueUserWorkItem(deThread.Value, j);
                                //            eventFlag[j] = new AutoResetEvent(false);
                                //            j++;
                                //        }
                                //    }

                                //    if (j != 0)
                                //    {
                                //        if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 2, 0), true))
                                //        {
                                //        }
                                //    }

                                //}
                            }
                        }

                        //One Way Search For Combination Mode
                        else if(request.Type == SearchType.OneWay && request.SearchBySegments)
                        {
                            searchRequests.Add(request);
                            SearchRoutingReturn(0);
                        }
                        else if (request.Type == SearchType.OneWay)
                        {
                            try
                            {

                                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                                availRequest.ContractVersion = contractVersion;
                                availRequest.Signature = loginResponse.Signature;
                                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                                {
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                                    if (!string.IsNullOrEmpty(promoCode))
                                    {
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                                    }
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "SG";
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 20;

                                    if (request.Segments[0].flightCabinClass == CabinClass.Business) // sending product class 'BC' when selects only business class in search request
                                    {
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "BC";
                                    }
                                    else
                                    {
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = ""; // sending empty product class code other than business class in search request
                                    }

                                    List<string> sgFareTypes = new List<string>();
                                    if (request.Segments[0].flightCabinClass != CabinClass.Business)//ECONOMY + ALL
                                    {
                                        sgFareTypes.Add("R");//Regular Fare.
                                         // w.e.f. 6th September 2019, criteria of Friends and Family fares to 
                                        //Minimum 2 passenger and Maximum 9 passenger, 
                                        if ((request.AdultCount + request.ChildCount >= 2) && (request.AdultCount + request.ChildCount <= 9))
                                        {
                                            sgFareTypes.Add("F");//Family Fare.
                                        }
                                        if (request.AdultCount + request.ChildCount < 4)
                                        {
                                            sgFareTypes.Add("T");//Coupon Fares
                                            sgFareTypes.Add("S");//Fare difference
                                        }
                                        sgFareTypes.Add("HB");//Hand Baggage.
                                        sgFareTypes.Add("IO");//Advance Purchase 
                                        sgFareTypes.Add("MX");//Spice Flex

                                    }
                                    //BUSINESS CLASS + ALL
                                    if (request.Segments[0].flightCabinClass == CabinClass.Business || request.Segments[0].flightCabinClass == CabinClass.All)
                                    {
                                        sgFareTypes.Add("BC");//Business class.
                                    }
                                    if (!string.IsNullOrEmpty(promoCode))
                                    {
                                        sgFareTypes.Add("C");//Corporate Fare
                                    }
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[sgFareTypes.Count()];
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = sgFareTypes.ToArray();

                                    int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];
                                    for (int a = 0; a < request.AdultCount; a++)
                                    {
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                                    }
                                    if (request.ChildCount > 0)
                                    {
                                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                                        {
                                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                                        }
                                    }
                                    if (request.InfantCount > 0)
                                    {
                                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                                        {
                                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                                        }
                                    }
                                }
                                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                                availRequest.EnableExceptionStackTrace = false;
                                try
                                {

                                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGGetAvailabilityVer2Request_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                    StreamWriter sw = new StreamWriter(filePath);
                                    ser.Serialize(sw, availRequest);
                                    sw.Close();
                                }
                                catch (Exception ex)
                                {
                                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Request. Reason : " + ex.ToString(), "");
                                }
                                GetAvailabilityVer2Response getAvailabilityVer2Response = bookingAPI.GetAvailabilityVer2(availRequest);
                                TripAvailabilityResponseVer2 tripAvailabilityResponseVer2 = getAvailabilityVer2Response.GetTripAvailabilityVer2Response;

                                try
                                {

                                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityVer2Response));
                                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGGetAvailabilityVer2Response_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                    StreamWriter sw = new StreamWriter(filePath);
                                    ser.Serialize(sw, getAvailabilityVer2Response);
                                    sw.Close();
                                }
                                catch (Exception ex)
                                {
                                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), "");
                                }

                                if (tripAvailabilityResponseVer2 != null && tripAvailabilityResponseVer2.Schedules != null && tripAvailabilityResponseVer2.Schedules.Length > 0)
                                {
                                    List<AvailableJourney> OnwardJourneys = new List<AvailableJourney>();
                                    //Schedules --2 Dimensional Array
                                    //Schedule --0 --Always onward journeys
                                    //Schedule --1 --Always return journeys
                                    for (int j = 0; j < tripAvailabilityResponseVer2.Schedules[0].Length; j++)
                                    {
                                        if (tripAvailabilityResponseVer2.Schedules[0][j].AvailableJourneys.Length > 0)
                                        {
                                            OnwardJourneys.AddRange(tripAvailabilityResponseVer2.Schedules[0][j].AvailableJourneys);
                                        }
                                    }
                                    if (OnwardJourneys != null && OnwardJourneys.Count > 0)
                                    {
                                        for (int i = 0; i < OnwardJourneys.Count; i++)
                                        {
                                            if (OnwardJourneys[i].AvailableSegment != null && OnwardJourneys[i].AvailableSegment.Length > 0)
                                            {
                                                //Condition which determines that the journey has connecting flights.
                                                if (OnwardJourneys[i].AvailableSegment.Length > 1)
                                                {
                                                    GetOnwardConnectingFlights(OnwardJourneys[i], tripAvailabilityResponseVer2, loginResponse.Signature);
                                                }
                                                else//Direct Flights
                                                {
                                                    for (int j = 0; j < OnwardJourneys[i].AvailableSegment[0].AvailableFares.Length; j++)
                                                    {
                                                        GetOnwardDirectFlights(OnwardJourneys[i], OnwardJourneys[i].AvailableSegment[0].AvailableFares[j], tripAvailabilityResponseVer2, loginResponse.Signature);
                                                    }
                                                }
                                            }

                                        }
                                        CombinedSearchResults.AddRange(oneWayResultList);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to get results for One-way search. Reason : " + ex.ToString(), "");
                            }
                        }

                        if (CombinedSearchResults.Count > 0)
                        {  
                            CombinedSearchResults = CombinedSearchResults.Where(i => !((i.FareType.Split(',')[0].Trim() == ""))).ToList(); //Removed the Product class LC which fare type comes as empty. "LC" is not required.
                            results = CombinedSearchResults.Where(i => i.TotalFare > 0).ToArray();
                        }

                    }
                }
                else
                {
                    throw new Exception("SpiceJet Search failed.Reason:Requested origin and destination not available");
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(loginSignature))
                {
                    //In Case of Exception Just Clear the Signature from the session.
                    Logout(loginSignature);
                }
                throw new Exception("SpiceJet Search failed. Reason : " + ex.ToString(), ex);
            }
            if (results.Length > 0 && request.Type == SearchType.Return && !request.SearchBySegments)
            {
                //combine hand baggage with hand baggage only.
                List<SearchResult> roundTripResultSet = results.ToList();
                results = roundTripResultSet.Where(i => ((i.FareType.Split(',')[0].ToLower().Contains("handbaggage") == false && i.FareType.Split(',')[1].ToLower().Contains("handbaggage") == false) || (i.FareType.Split(',')[0].ToLower().Contains("handbaggage") == true && i.FareType.Split(',')[1].ToLower().Contains("handbaggage") == true))).Select(p => p).ToArray();
            }
            if (request.SearchBySegments && CombinedSearchResults != null && CombinedSearchResults.Count > 0)
            {
                //For One-One Search
                //For Return Flights Binding Assign Group-1
                CombinedSearchResults.Where(x => x.Flights != null && x.Flights.Length > 0
                    && x.Flights[0] != null && x.Flights[0].Count() > 0
                    && x.Flights[0][0].Origin.AirportCode == request.Segments[0].Destination).ToList().All(p =>
                    {
                        p.Flights[0].All(u => { u.Group = 1; return true; }); return true;
                    });


                //if there are no onward flights or return flights just clear the list if the search type is return
                if (request.Type == CT.BookingEngine.SearchType.Return && ((this.CombinedSearchResults.FindAll(f => f.Flights[0].Any(s => s.Group == 0))).Count == 0 || this.CombinedSearchResults.FindAll(f => f.Flights[0].Any(s => s.Group == 1)).Count == 0))  //Fixed Issue for to not clear the results in One way combination search
                {
                    this.CombinedSearchResults.Clear();
                }

                results = CombinedSearchResults.Where(i => i.TotalFare > 0).ToArray();

            }
            if (results.Length > 0 && request.Type == SearchType.Return && !request.SearchBySegments)
            {
                //combine Spice Max with Spice max only.
                List<SearchResult> filteredResults = new List<SearchResult>();
                filteredResults = results.ToList();
                filteredResults=filteredResults.Where(i => !((i.FareType.Split(',')[0].Trim()=="" && i.FareType.Split(',')[1].Trim() == "") || (i.FareType.Split(',')[0].Trim() == "" || i.FareType.Split(',')[1].Trim() == ""))).Select(p => p).ToList();//Removed the Product class LC which fare type comes as empty. "LC" is not required.
                results = filteredResults.Where(i => ((i.FareType.Split(',')[0].ToLower().Trim().Contains("spice max") == false && i.FareType.Split(',')[1].ToLower().Trim().Contains("spice max") == false) || (i.FareType.Split(',')[0].ToLower().Trim().Contains("spice max") == true && i.FareType.Split(',')[1].ToLower().Trim().Contains("spice max") == true))).Select(p => p).ToArray();

            }
            return results;
        }

        /// <summary>
        /// This method returns the flight results for round trip journeys
        /// 
        /// </summary>
        public void SearchForAllFares()
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            List<AvailableJourney> OnwardJourneys = new List<AvailableJourney>();
            List<AvailableJourney> ReturnJourneys = new List<AvailableJourney>();

            try
            {
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();


                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = loginSignature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                    }
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "SG";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;

                    if (request.Segments[0].flightCabinClass == CabinClass.Business )
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "BC"; // sending product class 'BC' when selects only business class in search request
                    }
                    else
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = ""; //sending empty product class code other than business class
                    }
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 20;
                    int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                    List<string> sgFareTypes = new List<string>();
                    if (request.Segments[0].flightCabinClass != CabinClass.Business)//ECONOMY + ALL
                    {
                        sgFareTypes.Add("R");//Regular Fare.
                                             // w.e.f. 6th September 2019, criteria of Friends and Family fares to 
                                             //Minimum 2 passenger and Maximum 9 passenger, 
                        if ((request.AdultCount + request.ChildCount >= 2) && (request.AdultCount + request.ChildCount <= 9))
                        {
                            sgFareTypes.Add("F");//Family Fare.
                        }
                        if (request.AdultCount + request.ChildCount < 4)
                        {
                            sgFareTypes.Add("T");//Coupon Fares
                            sgFareTypes.Add("S");//Fare difference
                        }
                        sgFareTypes.Add("HB");//Hand Baggage.
                        sgFareTypes.Add("IO");//Advance Purchase 
                        sgFareTypes.Add("MX");//Spice Flex
                        
                    }
                    //BUSINESS CLASS + ALL
                    if (request.Segments[0].flightCabinClass == CabinClass.Business || request.Segments[0].flightCabinClass == CabinClass.All)  //sending 'BC fare type for Business class or for all. 
                    {
                        sgFareTypes.Add("BC");//Business class.
                    }
                    //FOR CORPORATE FARE

                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        sgFareTypes.Add("C");//Corporate Fare
                    }

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[sgFareTypes.Count()];
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = sgFareTypes.ToArray();


                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    for (int a = 0; a < request.AdultCount; a++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                    }
                    if (request.ChildCount > 0)
                    {
                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                        }
                    }
                    if (request.InfantCount > 0)
                    {
                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                        }
                    }
                }
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                availRequest.EnableExceptionStackTrace = false;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGRoundTripSearchRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, availRequest);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                }

                GetAvailabilityVer2Response getAvailabilityVer2Response = bookingAPI.GetAvailabilityVer2(availRequest);
                TripAvailabilityResponseVer2 tripAvailabilityResponseVer2 = getAvailabilityVer2Response.GetTripAvailabilityVer2Response;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityVer2Response));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGRoundTripSearchResponseVer2_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, getAvailabilityVer2Response);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Response Ver2. Reason : " + ex.ToString(), "");
                }

                //Seperate onward and return journeys
                if (tripAvailabilityResponseVer2 != null && tripAvailabilityResponseVer2.Schedules != null && tripAvailabilityResponseVer2.Schedules.Length > 0)
                {
                    //Schedules --2 Dimensional Array
                    //Schedule --0 --Always onward journeys
                    //Schedule --1 --Always return journeys
                    for (int j = 0; j < tripAvailabilityResponseVer2.Schedules[0].Length; j++)
                    {
                        if (tripAvailabilityResponseVer2.Schedules[0][j].AvailableJourneys.Length > 0)
                        {
                            OnwardJourneys.AddRange(tripAvailabilityResponseVer2.Schedules[0][j].AvailableJourneys);
                        }
                    }
                    if (tripAvailabilityResponseVer2.Schedules.Length > 1)
                    {
                        for (int j = 0; j < tripAvailabilityResponseVer2.Schedules[1].Length; j++)
                        {
                            if (tripAvailabilityResponseVer2.Schedules[1][j].AvailableJourneys.Length > 0)
                            {
                                ReturnJourneys.AddRange(tripAvailabilityResponseVer2.Schedules[1][j].AvailableJourneys);
                            }
                        }
                    }
                }


                if (request.Type == SearchType.Return && OnwardJourneys != null && OnwardJourneys.Count > 0 && ReturnJourneys != null && ReturnJourneys.Count > 0)
                {
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int j = 0; j < ReturnJourneys.Count; j++)
                        {
                            //Case-1:Both are direct flights
                            if (OnwardJourneys[i] != null && ReturnJourneys[j] != null && OnwardJourneys[i].AvailableSegment != null && ReturnJourneys[j].AvailableSegment != null && OnwardJourneys[i].AvailableSegment.Length == 1 && ReturnJourneys[j].AvailableSegment.Length == 1)
                            {
                                foreach (SpiceJet.SGBooking.AvailableFare2 onfare in OnwardJourneys[i].AvailableSegment[0].AvailableFares)
                                {
                                    foreach (SpiceJet.SGBooking.AvailableFare2 retFare in ReturnJourneys[j].AvailableSegment[0].AvailableFares)
                                    {
                                        if (onfare != null && retFare != null)
                                        {

                                            SpiceJet.SGBooking.Fare availableOnwardFare = tripAvailabilityResponseVer2.Fares[onfare.FareIndex];
                                            SpiceJet.SGBooking.Fare availableReturnFare = tripAvailabilityResponseVer2.Fares[retFare.FareIndex];
                                            if (availableOnwardFare != null && availableReturnFare != null)
                                            {
                                                SearchResult result = new SearchResult();
                                                result.IsLCC = true;
                                                // result.ResultBookingSource = BookingSource.SpiceJet;
                                                result.IsLCC = true;
                                                if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJet;
                                                }
                                                else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                                                {
                                                    result.ResultBookingSource = BookingSource.SpiceJetCorp;
                                                }
                                                result.Airline = "SG";
                                                result.Currency = agentBaseCurrency;
                                                result.EticketEligible = true;
                                                result.NonRefundable = false;
                                                result.RepriceErrorMessage = loginSignature;//Till Search To Book This Signature Should be carried.
                                                //FareRules
                                                result.FareRules = new List<FareRule>();
                                                FareRule fareRule = new FareRule();
                                                fareRule.Airline = result.Airline;
                                                fareRule.Destination = request.Segments[0].Destination;
                                                fareRule.FareBasisCode = availableOnwardFare.FareBasisCode;
                                                fareRule.FareInfoRef = availableOnwardFare.RuleNumber;
                                                fareRule.Origin = request.Segments[0].Origin;
                                                fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                                fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                                result.FareRules.Add(fareRule);

                                                result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                result.FareType = GetFareTypeForProductClass(availableOnwardFare.ProductClass);
                                                result.FareSellKey = availableOnwardFare.FareSellKey;
                                                result.Flights = new FlightInfo[2][];

                                                List<FlightInfo> onwardFlights = GetFlightInfo(OnwardJourneys[i].AvailableSegment[0], OnwardJourneys[i], availableOnwardFare.ProductClass, "ONWARD");
                                                if (onwardFlights != null && onwardFlights.Count > 0)
                                                {
                                                    result.Flights[0] = onwardFlights.ToArray();
                                                }

                                                result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                result.FareType += "," + GetFareTypeForProductClass(availableReturnFare.ProductClass);
                                                result.FareSellKey += "|" + availableReturnFare.FareSellKey;

                                                List<FlightInfo> returnFlights = GetFlightInfo(ReturnJourneys[j].AvailableSegment[0], ReturnJourneys[j], availableReturnFare.ProductClass, "RETURN");
                                                if (returnFlights != null && returnFlights.Count > 0)
                                                {
                                                    result.Flights[1] = returnFlights.ToArray();
                                                }
                                                if (result.Flights[0] != null && result.Flights[0].Length > 0 && result.Flights[1] != null && result.Flights[1].Length > 0)
                                                {
                                                    if (request.InfantCount > 0)
                                                    {
                                                        FlightDesignator Designator = OnwardJourneys[i].AvailableSegment[0].FlightDesignator;
                                                        string logFileName = Designator.FlightNumber + "_" + GetFareTypeForProductClass(availableOnwardFare.ProductClass).Replace(" ", "_") + Convert.ToString(OnwardJourneys[i].AvailableSegment[0].STD).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                        Designator = ReturnJourneys[j].AvailableSegment[0].FlightDesignator;
                                                        logFileName += Designator.FlightNumber + "_" + GetFareTypeForProductClass(availableReturnFare.ProductClass).Replace(" ", "_") + Convert.ToString(ReturnJourneys[j].AvailableSegment[0].STA).Replace('/', '_').Replace(" ", "_").Replace(':', '_');


                                                        PriceItineraryResponse itineraryResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, availableOnwardFare.FareSellKey, OnwardJourneys[i], logFileName, availableReturnFare.FareSellKey, ReturnJourneys[j].JourneySellKey, ReturnJourneys[j]);
                                                        if (itineraryResponse != null)
                                                        {
                                                            GetPaxFareBreakDown(itineraryResponse, ref result, request);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        GetPaxFareBreakDown(availableOnwardFare, ref result, request);
                                                        GetPaxFareBreakDown(availableReturnFare, ref result, request);
                                                    }
                                                }
                                                ResultList.Add(result);
                                            }
                                        }

                                    }
                                }
                            }
                            //Case 2: Both Onward and Return Connecting Flights
                            else if (OnwardJourneys[i] != null && OnwardJourneys[i].AvailableSegment != null && OnwardJourneys[i].AvailableSegment.Length > 1 && ReturnJourneys[j] != null && ReturnJourneys[j].AvailableSegment != null && ReturnJourneys[j].AvailableSegment.Length > 1)
                            {
                                List<SearchResult> conFlightsResults = GetRoundTripConnectingResults(OnwardJourneys[i], ReturnJourneys[j], tripAvailabilityResponseVer2, loginSignature);
                                if (conFlightsResults.Count > 0)
                                {
                                    ResultList.AddRange(conFlightsResults);
                                }
                            }


                            //Case-3: Onward Direct and Return Connecting
                            else if (OnwardJourneys[i] != null && OnwardJourneys[i].AvailableSegment != null && OnwardJourneys[i].AvailableSegment.Length == 1 && ReturnJourneys[j] != null && ReturnJourneys[j].AvailableSegment != null && ReturnJourneys[j].AvailableSegment.Length == 2)
                            {
                                if (OnwardJourneys[i].AvailableSegment[0].AvailableFares != null && OnwardJourneys[i].AvailableSegment[0].AvailableFares.Length > 0 && ReturnJourneys[j].AvailableSegment[0].AvailableFares != null && ReturnJourneys[j].AvailableSegment[0].AvailableFares.Length > 0 && ReturnJourneys[j].AvailableSegment[1].AvailableFares != null && ReturnJourneys[j].AvailableSegment[1].AvailableFares.Length > 0)
                                {
                                    AvailableSegment firstSegmentConReturn = null;
                                    AvailableSegment secondSegmentConReturn = null;
                                    if (ReturnJourneys[j].AvailableSegment[0] != null && ReturnJourneys[j].AvailableSegment[1] != null)
                                    {
                                        firstSegmentConReturn = ReturnJourneys[j].AvailableSegment[0];
                                        secondSegmentConReturn = ReturnJourneys[j].AvailableSegment[1];
                                    }
                                    if (firstSegmentConReturn != null && secondSegmentConReturn != null && firstSegmentConReturn.AvailableFares.Length == secondSegmentConReturn.AvailableFares.Length)
                                    {
                                        for (int f = 0; f < OnwardJourneys[i].AvailableSegment[0].AvailableFares.Length; f++)
                                        {
                                            for (int g = 0; g < firstSegmentConReturn.AvailableFares.Length; g++)
                                            {
                                                if (OnwardJourneys[i].AvailableSegment[0].AvailableFares[f] != null && firstSegmentConReturn.AvailableFares[g] != null && secondSegmentConReturn.AvailableFares[g] != null)
                                                {

                                                    SpiceJet.SGBooking.Fare availableOnwardFare = tripAvailabilityResponseVer2.Fares[OnwardJourneys[i].AvailableSegment[0].AvailableFares[f].FareIndex];

                                                    SpiceJet.SGBooking.Fare fareFirstConSegReturn = tripAvailabilityResponseVer2.Fares[ReturnJourneys[j].AvailableSegment[0].AvailableFares[g].FareIndex];
                                                    SpiceJet.SGBooking.Fare fareSecondConSegReturn = tripAvailabilityResponseVer2.Fares[ReturnJourneys[j].AvailableSegment[1].AvailableFares[g].FareIndex];

                                                    SearchResult result = new SearchResult();
                                                    result.IsLCC = true;
                                                    //result.ResultBookingSource = BookingSource.SpiceJet;
                                                    if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                                                    {
                                                        result.ResultBookingSource = BookingSource.SpiceJet;
                                                    }
                                                    else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                                                    {
                                                        result.ResultBookingSource = BookingSource.SpiceJetCorp;
                                                    }
                                                    result.Airline = "SG";
                                                    result.Currency = agentBaseCurrency;
                                                    result.EticketEligible = true;
                                                    result.NonRefundable = false;
                                                    result.RepriceErrorMessage = loginSignature;//Till Search To Book This Signature Should be carried.

                                                    result.FareRules = new List<FareRule>();
                                                    FareRule fareRule = new FareRule();
                                                    fareRule.Airline = result.Airline;
                                                    fareRule.Destination = request.Segments[0].Destination;
                                                    fareRule.FareBasisCode = availableOnwardFare.FareBasisCode;
                                                    fareRule.FareInfoRef = availableOnwardFare.RuleNumber;
                                                    fareRule.Origin = request.Segments[0].Origin;
                                                    fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                                    fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                                    result.FareRules.Add(fareRule);

                                                    result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                    result.FareSellKey = availableOnwardFare.FareSellKey;
                                                    result.FareType = GetFareTypeForProductClass(availableOnwardFare.ProductClass);

                                                    result.Flights = new FlightInfo[2][];
                                                    List<FlightInfo> onwardFlights = GetFlightInfo(OnwardJourneys[i].AvailableSegment[0], OnwardJourneys[i], availableOnwardFare.ProductClass, "ONWARD");

                                                    if (onwardFlights != null && onwardFlights.Count > 0)
                                                    {
                                                        result.Flights[0] = onwardFlights.ToArray();
                                                    }

                                                    result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                    result.FareType += "," + GetFareTypeForProductClass(fareFirstConSegReturn.ProductClass);
                                                    result.FareSellKey += "|" + fareFirstConSegReturn.FareSellKey + "^" + fareSecondConSegReturn.FareSellKey;

                                                    List<FlightInfo> firstSegmentConFlightsR = GetFlightInfo(firstSegmentConReturn, ReturnJourneys[j], fareFirstConSegReturn.ProductClass, "RETURN");
                                                    List<FlightInfo> secondSegmentConFlightsR = GetFlightInfo(secondSegmentConReturn, ReturnJourneys[j], fareSecondConSegReturn.ProductClass, "RETURN");
                                                    if (firstSegmentConFlightsR != null && secondSegmentConFlightsR != null && firstSegmentConFlightsR.Count > 0 && secondSegmentConFlightsR.Count > 0)
                                                    {
                                                        List<FlightInfo> listOfFlightsR = new List<FlightInfo>();
                                                        listOfFlightsR.AddRange(firstSegmentConFlightsR);
                                                        listOfFlightsR.AddRange(secondSegmentConFlightsR);
                                                        result.Flights[1] = listOfFlightsR.ToArray();
                                                    }
                                                    if (result.Flights[0] != null && result.Flights[0].Length > 0 && result.Flights[1] != null && result.Flights[1].Length > 0)
                                                    {
                                                        if (request.InfantCount > 0)
                                                        {
                                                            string logFileName = result.Flights[0][0].FlightNumber + "_" + result.FareType.Split(',')[0].Replace(" ", "_") + Convert.ToString(result.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                            logFileName += "_" + result.Flights[1][0].FlightNumber + "_" + result.FareType.Split(',')[1].Replace(" ", "_") + "_" + Convert.ToString(result.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                            logFileName += "_" + Convert.ToString(result.Flights[1][result.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                            PriceItineraryResponse itineraryResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, availableOnwardFare.FareSellKey, OnwardJourneys[i], logFileName, fareFirstConSegReturn.FareSellKey + "^" + fareSecondConSegReturn.FareSellKey, ReturnJourneys[j].JourneySellKey, ReturnJourneys[j]);
                                                            if (itineraryResponse != null)
                                                            {
                                                                GetPaxFareBreakDown(itineraryResponse, ref result, request);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            GetPaxFareBreakDown(availableOnwardFare, ref result, request);
                                                            GetPaxFareBreakDown(fareFirstConSegReturn, ref result, request);
                                                            GetPaxFareBreakDown(fareSecondConSegReturn, ref result, request);
                                                        }
                                                    }
                                                    ResultList.Add(result);
                                                }
                                            }
                                        }
                                    }
                                }

                            }

                            //Case-4: Onward Connecting and Return Direct
                            else if (OnwardJourneys[i] != null && OnwardJourneys[i].AvailableSegment != null && OnwardJourneys[i].AvailableSegment.Length == 2 && ReturnJourneys[j] != null && ReturnJourneys[j].AvailableSegment != null && ReturnJourneys[j].AvailableSegment.Length == 1)
                            {
                                if (ReturnJourneys[j].AvailableSegment[0].AvailableFares != null && ReturnJourneys[j].AvailableSegment[0].AvailableFares.Length > 0 && OnwardJourneys[i].AvailableSegment[0].AvailableFares != null && OnwardJourneys[i].AvailableSegment[0].AvailableFares.Length > 0 && OnwardJourneys[i].AvailableSegment[1].AvailableFares != null && OnwardJourneys[i].AvailableSegment[1].AvailableFares.Length > 0)
                                {
                                    AvailableSegment firstSegmentConOnward = null;
                                    AvailableSegment secondSegmentConOnward = null;

                                    if (OnwardJourneys[i].AvailableSegment[0] != null && OnwardJourneys[i].AvailableSegment[1] != null)
                                    {
                                        firstSegmentConOnward = OnwardJourneys[i].AvailableSegment[0];
                                        secondSegmentConOnward = OnwardJourneys[i].AvailableSegment[1];
                                    }

                                    if (firstSegmentConOnward != null && secondSegmentConOnward != null && firstSegmentConOnward.AvailableFares.Length == secondSegmentConOnward.AvailableFares.Length)
                                    {
                                        for (int f = 0; f < firstSegmentConOnward.AvailableFares.Length; f++)
                                        {
                                            for (int g = 0; g < ReturnJourneys[j].AvailableSegment[0].AvailableFares.Length; g++)
                                            {
                                                if (firstSegmentConOnward.AvailableFares[f] != null && secondSegmentConOnward.AvailableFares[f] != null && ReturnJourneys[j].AvailableSegment[0].AvailableFares[g] != null)
                                                {
                                                    SpiceJet.SGBooking.Fare fareFirstConSegOnWard = tripAvailabilityResponseVer2.Fares[OnwardJourneys[i].AvailableSegment[0].AvailableFares[f].FareIndex];
                                                    SpiceJet.SGBooking.Fare fareSecondConSegOnward = tripAvailabilityResponseVer2.Fares[OnwardJourneys[i].AvailableSegment[1].AvailableFares[f].FareIndex];

                                                    SpiceJet.SGBooking.Fare availableReturnFare = tripAvailabilityResponseVer2.Fares[ReturnJourneys[j].AvailableSegment[0].AvailableFares[g].FareIndex];

                                                    SearchResult result = new SearchResult();
                                                    result.IsLCC = true;
                                                    //result.ResultBookingSource = BookingSource.SpiceJet;
                                                    if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                                                    {
                                                        result.ResultBookingSource = BookingSource.SpiceJet;
                                                    }
                                                    else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                                                    {
                                                        result.ResultBookingSource = BookingSource.SpiceJetCorp;
                                                    }
                                                    result.Airline = "SG";
                                                    result.Currency = agentBaseCurrency;
                                                    result.EticketEligible = true;
                                                    result.RepriceErrorMessage = loginSignature;//Till Search To Book This Signature Should be carried.

                                                    result.FareRules = new List<FareRule>();
                                                    FareRule fareRule = new FareRule();
                                                    fareRule.Airline = result.Airline;
                                                    fareRule.Destination = request.Segments[0].Destination;
                                                    fareRule.FareBasisCode = fareFirstConSegOnWard.FareBasisCode;
                                                    fareRule.FareInfoRef = fareFirstConSegOnWard.RuleNumber;
                                                    fareRule.Origin = request.Segments[0].Origin;
                                                    fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                                    fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                                    result.FareRules.Add(fareRule);

                                                    result.NonRefundable = false;

                                                    result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                    result.FareSellKey = fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey;
                                                    result.FareType = GetFareTypeForProductClass(fareFirstConSegOnWard.ProductClass);
                                                    result.Flights = new FlightInfo[2][];

                                                    List<FlightInfo> firstSegmentConFlightsO = GetFlightInfo(firstSegmentConOnward, OnwardJourneys[i], fareFirstConSegOnWard.ProductClass, "ONWARD");
                                                    List<FlightInfo> secondSegmentConFlightsO = GetFlightInfo(secondSegmentConOnward, OnwardJourneys[i], fareSecondConSegOnward.ProductClass, "ONWARD");
                                                    if (firstSegmentConFlightsO != null && secondSegmentConFlightsO != null && firstSegmentConFlightsO.Count > 0 && secondSegmentConFlightsO.Count > 0)
                                                    {
                                                        List<FlightInfo> listOfFlightsO = new List<FlightInfo>();
                                                        listOfFlightsO.AddRange(firstSegmentConFlightsO);
                                                        listOfFlightsO.AddRange(secondSegmentConFlightsO);
                                                        result.Flights[0] = listOfFlightsO.ToArray();
                                                    }

                                                    result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                    result.FareType += "," + GetFareTypeForProductClass(availableReturnFare.ProductClass);
                                                    result.FareSellKey += "|" + availableReturnFare.FareSellKey;

                                                    List<FlightInfo> returnFlights = GetFlightInfo(ReturnJourneys[j].AvailableSegment[0], ReturnJourneys[j], availableReturnFare.ProductClass, "RETURN");
                                                    if (returnFlights != null && returnFlights.Count > 0)
                                                    {
                                                        result.Flights[1] = returnFlights.ToArray();
                                                    }

                                                    if (result.Flights[0] != null && result.Flights[0].Length > 0 && result.Flights[1] != null && result.Flights[1].Length > 0)
                                                    {
                                                        if (request.InfantCount > 0)
                                                        {
                                                            string logFileName = result.Flights[0][0].FlightNumber + "_" + result.FareType.Split(',')[0].Replace(" ", "_") + "_" + Convert.ToString(result.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                            logFileName += "_" + Convert.ToString(result.Flights[0][result.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                            logFileName += "_" + result.Flights[1][0].FlightNumber + "_" + result.FareType.Split(',')[1].Replace(" ", "_").Replace(" ", "_") + Convert.ToString(result.Flights[1][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');

                                                            PriceItineraryResponse itineraryResponse = GetItineraryPrice(request, OnwardJourneys[i].JourneySellKey, fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey, OnwardJourneys[i], logFileName, availableReturnFare.FareSellKey, ReturnJourneys[j].JourneySellKey, ReturnJourneys[j]);
                                                            if (itineraryResponse != null)
                                                            {
                                                                GetPaxFareBreakDown(itineraryResponse, ref result, request);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            GetPaxFareBreakDown(fareFirstConSegOnWard, ref result, request);
                                                            GetPaxFareBreakDown(fareSecondConSegOnward, ref result, request);
                                                            GetPaxFareBreakDown(availableReturnFare, ref result, request);
                                                        }
                                                    }
                                                    ResultList.Add(result);
                                                }
                                            }
                                        }
                                    }
                                }

                            }


                        }
                    }
                }

                CombinedSearchResults.AddRange(ResultList);

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to get results." + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Returns the Fare Type for a Product Class 
        /// </summary>
        /// <param name="productClass"></param>
        /// <returns></returns>
        private string GetFareTypeForProductClass(string productClass)
        {
            string fareType = "";
            switch (productClass)
            {
                case "RS":
                    fareType = "Regular Fare";
                    break;
                case "XA":
                    fareType = "Special Return Trip";
                    break;
                case "HF":
                    fareType = "SpiceFlex Fare";
                    break;
                case "XB":
                    fareType = "Family & Friends";
                    break;
                case "HO":
                    fareType = "HandBaggage Only";
                    break;
                case "SS":
                    fareType = "Spice Saver";
                    break;
                case "NF":
                    fareType = "Advance Purchase";
                    break;
                case "DD":
                    fareType = "Coupon Fare";
                    break;
                case "CN":
                    fareType = "Promo Coupon";
                    break;
                case "CP":
                    fareType = "Corporate Fare";
                    break;
                case "SC":
                    fareType = "Spice Max";
                    break;
                case "BC":
                    fareType = "BusinessClass";
                    break;
            }
            return fareType;
        }

        /// <summary>
        /// Connecting Flights Get price itenary type
        /// </summary>
        /// <param name="request"></param>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="journey"></param>
        /// <param name="onwardFare"></param>
        /// <returns></returns>

        protected PriceItineraryResponse GetItineraryPrice(SearchRequest request, string journeySellKey, string fareSellKey, AvailableJourney journey, string onwardFare, string fareSellKeyRet, string journeySellKeyRet, AvailableJourney ReturnJouney)
        {
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            IBookingManager bookingAPI = this.bookingAPI;

            int paxCount = request.AdultCount + request.ChildCount;

            PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
            priceItinRequest.Signature = loginSignature;
            priceItinRequest.ContractVersion = contractVersion;
            priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
            priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(request.AdultCount + request.ChildCount + request.InfantCount);
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[paxCount];

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[2];
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1] = new SellKeyList();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].FareSellKey = fareSellKeyRet;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].JourneySellKey = journeySellKeyRet;

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[1];
            if (request.ChildCount > 0)
            {
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[2];
            }

            //Adult Pax Price Types
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0] = new PaxPriceType();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxType = "ADT";
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxCount = (Int16)(request.AdultCount);

            //Child Pax Price Types
            if (request.ChildCount > 0)
            {
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1] = new PaxPriceType();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxType = "CHD";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxCount = (Int16)(request.ChildCount);
            }
            if (request.InfantCount > 0)
            {
                priceItinRequest.ItineraryPriceRequest.SSRRequest = new SSRRequest();
                priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = currencyCode;
                int segcount = journey.AvailableSegment.Length + ReturnJouney.AvailableSegment.Length;
                priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[segcount];
                int f = -1;
                for (int m = 0; m < journey.AvailableSegment.Length; m++)
                {
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m] = new SegmentSSRRequest();
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].ArrivalStation = journey.AvailableSegment[m].ArrivalStation;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].DepartureStation = journey.AvailableSegment[m].DepartureStation;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].FlightDesignator = journey.AvailableSegment[m].FlightDesignator;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].STD = journey.AvailableSegment[m].STD;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs = new PaxSSR[request.InfantCount];
                    for (int i = 0; i < request.InfantCount; i++)
                    {

                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i] = new PaxSSR();
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].ActionStatusCode = "NN";
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].ArrivalStation = journey.AvailableSegment[m].ArrivalStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].DepartureStation = journey.AvailableSegment[m].DepartureStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].PassengerNumber = (Int16)i;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRCode = "INFT";
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRNumber = 0;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRValue = 0;
                    }
                    f++;
                }

                for (int d = 0; d < ReturnJouney.AvailableSegment.Length; d++)
                {
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1] = new SegmentSSRRequest();
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].ArrivalStation = ReturnJouney.AvailableSegment[d].ArrivalStation;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].DepartureStation = ReturnJouney.AvailableSegment[d].DepartureStation;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].FlightDesignator = ReturnJouney.AvailableSegment[d].FlightDesignator;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].STD = ReturnJouney.AvailableSegment[d].STD;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs = new PaxSSR[request.InfantCount];
                    for (int i = 0; i < request.InfantCount; i++)
                    {

                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i] = new PaxSSR();
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].ActionStatusCode = "NN";
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].ArrivalStation = ReturnJouney.AvailableSegment[d].ArrivalStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].DepartureStation = ReturnJouney.AvailableSegment[d].DepartureStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].PassengerNumber = (Int16)i;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].SSRCode = "INFT";
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].SSRNumber = 0;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[f + 1].PaxSSRs[i].SSRValue = 0;
                    }
                    f++;
                }
            }
            //For Corporate Fares Need to get the promotion discount
            if (!string.IsNullOrEmpty(promoCode))
            {
                priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes = new string[1];
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes[0] = "C";//CorporateFare
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = promoCode;
            }
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
            priceItinRequest.EnableExceptionStackTrace = false;
            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGGetItineraryPriceRequest_" + onwardFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";

                //string filePath = xmlLogPath + "SGGetItineraryPriceRequest_" + journeykey + "_#_" + fareSellKey.Replace("~", "_") + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, priceItinRequest);
                sw.Close();
            }
            catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }


            try
            {
                piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(Booking));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGGetItineraryPriceResponse_" + onwardFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, piResponse.Booking);
                    sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
            }


            return piResponse;
        }



        /// <summary>
        /// Returns the fare breakup for the segment for One way only
        /// </summary>
        /// <param name="request"></param>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        /// 

        protected PriceItineraryResponse GetItineraryPrice(SearchRequest request, string journeySellKey, string fareSellKey, AvailableJourney journey, string logFileName)
        {
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            IBookingManager bookingAPI = this.bookingAPI;

            PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
            priceItinRequest.Signature = loginSignature;
            priceItinRequest.ContractVersion = contractVersion;
            priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
            priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(request.AdultCount + request.ChildCount);


            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[1];
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[1];
            if (request.ChildCount > 0)
            {
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[2];
            }

            //Adult Pax Price Types
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0] = new PaxPriceType();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxType = "ADT";
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxCount = (Int16)(request.AdultCount);

            //Child Pax Price Types
            if (request.ChildCount > 0)
            {
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1] = new PaxPriceType();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxType = "CHD";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxCount = (Int16)(request.ChildCount);
            }
            if (request.InfantCount > 0)
            {
                priceItinRequest.ItineraryPriceRequest.SSRRequest = new SSRRequest();
                priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = currencyCode;
                priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[journey.AvailableSegment.Length];

                for (int m = 0; m < journey.AvailableSegment.Length; m++)
                {
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m] = new SegmentSSRRequest();
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].ArrivalStation = journey.AvailableSegment[m].ArrivalStation;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].DepartureStation = journey.AvailableSegment[m].DepartureStation;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].FlightDesignator = journey.AvailableSegment[m].FlightDesignator;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].STD = journey.AvailableSegment[m].STD;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs = new PaxSSR[request.InfantCount];
                    for (int i = 0; i < request.InfantCount; i++)
                    {
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i] = new PaxSSR();
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].ActionStatusCode = "NN";
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].ArrivalStation = journey.AvailableSegment[m].ArrivalStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].DepartureStation = journey.AvailableSegment[m].DepartureStation;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].PassengerNumber = (Int16)i;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRCode = "INFT";
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRNumber = 0;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].SSRValue = 0;
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests[m].PaxSSRs[i].IsInServiceBundle = false;
                    }
                }
            }


            //For Corporate Fares Need to get the promotion discount
            if (!string.IsNullOrEmpty(promoCode))
            {
                priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes = new string[1];
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes[0] = "C";//CorporateFare
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = promoCode;
            }

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
            priceItinRequest.EnableExceptionStackTrace = false;
            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGPriceItineraryRequest_" + logFileName + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, priceItinRequest);
                sw.Close();
            }
            catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }


            try
            {
                piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(Booking));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGPriceItineraryResponse_" + logFileName + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, piResponse.Booking);
                    sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
            }
            return piResponse;
        }

        /// <summary>
        /// Returns the combined fare breakup for Round Trip segments. Some sectors may attract GST Tax and in order
        /// to retrieve those taxes we need to combine fare sell keys.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="journeySellKeyRet"></param>
        /// <param name="fareSellKeyRet"></param>
        /// <param name="onSegment"></param>
        /// <param name="retSegment"></param>
        /// <returns></returns>
        protected PriceItineraryResponse GetItineraryPrice(SearchRequest request, string journeySellKey, string fareSellKey, string journeySellKeyRet, string fareSellKeyRet, Segment onSegment, Segment retSegment, string onwardFare, string returnFare)
        {
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            try
            {

                {
                    IBookingManager bookingAPI = this.bookingAPI;


                    PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
                    priceItinRequest.Signature = loginSignature;
                    priceItinRequest.ContractVersion = contractVersion;
                    priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                    priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;

                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(request.AdultCount + request.ChildCount + request.InfantCount);
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[request.AdultCount + request.ChildCount];

                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[2];
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;

                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1] = new SellKeyList();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].FareSellKey = fareSellKeyRet;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].JourneySellKey = journeySellKeyRet;

                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[1];
                    if (request.ChildCount > 0)
                    {
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[2];
                    }

                    //Adult Pax Price Types
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxType = "ADT";
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxCount = (Int16)(request.AdultCount);

                    //Child Pax Price Types
                    if (request.ChildCount > 0)
                    {
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1] = new PaxPriceType();
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxType = "CHD";
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxCount = (Int16)(request.ChildCount);
                    }

                    if (request.InfantCount > 0)
                    {
                        priceItinRequest.ItineraryPriceRequest.SSRRequest = new SSRRequest();
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = currencyCode;
                        List<SegmentSSRRequest> infantSSrRequests = new List<SegmentSSRRequest>();

                        SegmentSSRRequest segmentSSRRequest = new SegmentSSRRequest();
                        segmentSSRRequest.ArrivalStation = onSegment.ArrivalStation;
                        segmentSSRRequest.DepartureStation = onSegment.DepartureStation;
                        segmentSSRRequest.FlightDesignator = onSegment.FlightDesignator;
                        segmentSSRRequest.STD = onSegment.STD;
                        segmentSSRRequest.PaxSSRs = new PaxSSR[request.InfantCount];
                        for (int i = 0; i < request.InfantCount; i++)
                        {
                            segmentSSRRequest.PaxSSRs[i] = new PaxSSR();
                            segmentSSRRequest.PaxSSRs[i].ActionStatusCode = "NN";
                            segmentSSRRequest.PaxSSRs[i].ArrivalStation = onSegment.ArrivalStation;
                            segmentSSRRequest.PaxSSRs[i].DepartureStation = onSegment.DepartureStation;
                            segmentSSRRequest.PaxSSRs[i].PassengerNumber = (Int16)i;
                            segmentSSRRequest.PaxSSRs[i].SSRCode = "INFT";
                            segmentSSRRequest.PaxSSRs[i].SSRNumber = 0;
                            segmentSSRRequest.PaxSSRs[i].SSRValue = 0;
                            infantSSrRequests.Add(segmentSSRRequest);
                        }
                        segmentSSRRequest = new SegmentSSRRequest();
                        segmentSSRRequest.ArrivalStation = retSegment.ArrivalStation;
                        segmentSSRRequest.DepartureStation = retSegment.DepartureStation;
                        segmentSSRRequest.FlightDesignator = retSegment.FlightDesignator;
                        segmentSSRRequest.STD = retSegment.STD;
                        segmentSSRRequest.PaxSSRs = new PaxSSR[request.InfantCount];
                        for (int i = 0; i < request.InfantCount; i++)
                        {
                            segmentSSRRequest.PaxSSRs[i] = new PaxSSR();
                            segmentSSRRequest.PaxSSRs[i].ActionStatusCode = "NN";
                            segmentSSRRequest.PaxSSRs[i].ArrivalStation = retSegment.ArrivalStation;
                            segmentSSRRequest.PaxSSRs[i].DepartureStation = retSegment.DepartureStation;
                            segmentSSRRequest.PaxSSRs[i].PassengerNumber = (Int16)i;
                            segmentSSRRequest.PaxSSRs[i].SSRCode = "INFT";
                            segmentSSRRequest.PaxSSRs[i].SSRNumber = 0;
                            segmentSSRRequest.PaxSSRs[i].SSRValue = 0;
                            infantSSrRequests.Add(segmentSSRRequest);
                        }
                        priceItinRequest.ItineraryPriceRequest.SSRRequest.SegmentSSRRequests = infantSSrRequests.ToArray();
                    }


                    //For Corporate Fares Need to get the promotion discount
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                        priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes = new string[1];
                        priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes[0] = "C";//CorporateFare
                        priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = promoCode;
                    }
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    priceItinRequest.EnableExceptionStackTrace = false;
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGGetItineraryPriceRequest_" + onwardFare + "_" + returnFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, priceItinRequest);
                        sw.Close();
                    }
                    catch { }

                    piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Booking));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGGetItineraryPriceResponse_" + onwardFare + "_" + returnFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, piResponse.Booking);
                        sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }

                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                piResponse = null;
            }
            return piResponse;
        }


        /// <summary>
        /// Depricated (Not in use Now)
        /// </summary>
        /// <param name="result"></param>
        /// <param name="request"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private BookingUpdateResponseData GetInfantFares(SearchRequest request, Journey journey, string signature)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                SellRequestData requestData = new SellRequestData();

                requestData.SellBy = SellBy.SSR;
                requestData.SellSSR = new SellSSR();
                requestData.SellSSR.SSRRequest = new SSRRequest();
                requestData.SellSSR.SSRRequest.CurrencyCode = currencyCode;
                requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[journey.Segments.Length];

                for (int i = 0; i < journey.Segments.Length; i++)
                {
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = journey.Segments[i].ArrivalStation;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = journey.Segments[i].DepartureStation;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = journey.Segments[i].FlightDesignator.CarrierCode.Trim();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = journey.Segments[i].FlightDesignator.FlightNumber.Trim();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.OpSuffix = journey.Segments[i].FlightDesignator.OpSuffix;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = journey.Segments[i].STD;

                    for (int j = 0; j < request.InfantCount; j++)
                    {
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = new PaxSSR[request.InfantCount];
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j] = new PaxSSR();
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].State = SpiceJet.SGBooking.MessageState.Modified;
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].ActionStatusCode = "NN";
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].ArrivalStation = journey.Segments[i].ArrivalStation;
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].DepartureStation = journey.Segments[i].DepartureStation;
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].PassengerNumber = (Int16)j;
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].SSRCode = "INFT";
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[j].SSRValue = 0;
                    }
                }

                if (!string.IsNullOrEmpty(promoCode))//Promo Code Implementation for corporate fare
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale = new TypeOfSale();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes = new string[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes[0] = "C";//Corporate Fare
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.PromotionCode = promoCode;
                }

                //New object for signature inclusion.
                SellRequest sellRequestObj = new SellRequest();
                sellRequestObj.ContractVersion = contractVersion;
                sellRequestObj.SellRequestData = requestData;
                sellRequestObj.Signature = signature;
                sellRequestObj.EnableExceptionStackTrace = false;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGSSRInfantRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellRequestObj);
                    sw.Close();
                }
                catch { }

                SellResponse sellResponse = bookingAPI.Sell(sellRequestObj);
                responseData = sellResponse.BookingUpdateResponseData;

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(SellResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGSSRInfantResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellResponse);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Book. Reason : " + ex.ToString(), "");
                throw ex;
            }

            return responseData;
        }

        /// <summary>
        /// Hold booking before ticket
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        /// 

        private BookingUpdateResponseData Book(FlightItinerary itinerary, string signature)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = currencyCode;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;


                string onwardFareSellKey = string.Empty;
                string onwardJourneySellKey = string.Empty;
                string returnFareSellKey = string.Empty;
                string returnJourneySellKey = string.Empty;

                List<string> sellKeysListOnward = new List<string>();
                List<string> sellKeysListReturn = new List<string>();
                for (int p = 0; p < itinerary.Segments.Length; p++)
                {
                    if (itinerary.Segments[p].Group == 0)//Onward
                    {
                        sellKeysListOnward.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                    if (itinerary.Segments[p].Group == 1)//Return
                    {
                        sellKeysListReturn.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                }
                IEnumerable<string> distinctSellKeysOnward = sellKeysListOnward.Distinct();
                IEnumerable<string> distinctSellKeysReturn = sellKeysListReturn.Distinct();

                if (distinctSellKeysOnward.Count() > 0)
                {
                    foreach (string key in distinctSellKeysOnward)
                    {
                        if (onwardJourneySellKey.Length > 0)
                        {
                            onwardJourneySellKey += "^" + key;
                        }
                        else
                        {
                            onwardJourneySellKey = key;
                        }
                    }
                }

                if (distinctSellKeysReturn.Count() > 0)
                {
                    foreach (string key in distinctSellKeysReturn)
                    {
                        if (returnJourneySellKey.Length > 0)
                        {
                            returnJourneySellKey += "^" + key;
                        }
                        else
                        {
                            returnJourneySellKey = key;
                        }
                    }
                }


                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[1];
                if (itinerary.TicketAdvisory.Split('|').Length == 2)
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[2];
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = onwardJourneySellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = itinerary.TicketAdvisory.Split('|')[0];
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";

                if (itinerary.TicketAdvisory.Split('|').Length == 2)//Round trip
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].JourneySellKey = returnJourneySellKey;
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].FareSellKey = itinerary.TicketAdvisory.Split('|')[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].StandbyPriorityCode = "";
                }
                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    if (itinerary.Passenger[i].Type != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        switch (itinerary.Passenger[i].Type)
                        {
                            case PassengerType.Adult:
                                priceType.PaxType = "ADT";
                                break;
                            case PassengerType.Child:
                                priceType.PaxType = "CHD";
                                break;

                        }
                        paxPriceTypes.Add(priceType);
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                if (!string.IsNullOrEmpty(promoCode))//Promo Code Implementation for corporate fare
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale = new TypeOfSale();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes = new string[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes[0] = "C";//Corporate Fare
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.PromotionCode = promoCode;
                }

                //New object creation for signature inclusion.
                SellRequest sellRequestObj = new SellRequest();
                sellRequestObj.ContractVersion = contractVersion;
                sellRequestObj.SellRequestData = requestData;
                sellRequestObj.Signature = signature;
                sellRequestObj.EnableExceptionStackTrace = false;


                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGBookingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellRequestObj);
                    sw.Close();
                }
                catch { }

                SellResponse sellResponse = bookingAPI.Sell(sellRequestObj);
                responseData = sellResponse.BookingUpdateResponseData;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGBookingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellResponse);
                    sw.Close();
                }
                catch { }


            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Book. Reason : " + ex.ToString(), "");
                throw ex;
            }

            return responseData;
        }

        /// <summary>
        /// Confirmation of Additional Baggage
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        /// 
        public BookingUpdateResponseData BookBaggage(FlightItinerary itinerary, string signature)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                int baggageSSR = itinerary.Passenger.Where(p => p.BaggageType != null).Count();
                int mealSSR = itinerary.Passenger.Where(p => p.MealType != null).Count();

                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.SSR;
                requestData.SellSSR = new SellSSR();
                requestData.SellSSR.SSRRequest = new SSRRequest();
                requestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                requestData.SellSSR.SSRRequest.CurrencyCode = currencyCode;
                requestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;

                //SellRequest
                //SellRequest -->SellRequestData
                //SellRequest -->SellRequestData-->SellSSR
                //SellRequest -->SellRequestData-->SellSSR -->SSRRequest
                //SellRequest -->SellRequestData-->SellSSR -->SegmentSSRRequests[]
                //SellRequest -->SellRequestData-->SellSSR -->SegmentSSRRequests[] -->SegmentSSRRequest-->PaxSSRs[]
                //SellRequest -->SellRequestData-->SellSSR -->SegmentSSRRequests[] -->SegmentSSRRequest-->PaxSSRs[] -->PaxSSR

                List<SegmentSSRRequest> segSSRRequests = new List<SegmentSSRRequest>();
                var objFlights = itinerary.Segments.Select(p => p.FlightNumber).Distinct().ToArray();
                string sOrigin = string.Empty, sDestination = string.Empty; DateTime dtSTD;
              
                //Baggage SSR'S
                for (int i = 0; i < objFlights.Length; i++)
                {
                    sOrigin = itinerary.Segments.Where(y => y.FlightNumber == objFlights[i]).Select(z => z.Origin.AirportCode).FirstOrDefault();
                    sDestination = itinerary.Segments.Where(y => y.FlightNumber == objFlights[i]).Select(z => z.Destination.AirportCode).Last();
                    dtSTD = itinerary.Segments.Where(y => y.FlightNumber == objFlights[i]).Select(f => f.DepartureTime).FirstOrDefault();

                    var objSegment = itinerary.Segments.Where(y => y.FlightNumber == objFlights[i]).FirstOrDefault();

                    SegmentSSRRequest segmentSSRRequest = new SegmentSSRRequest();
                    segmentSSRRequest.ArrivalStation = sDestination;
                    segmentSSRRequest.DepartureStation = sOrigin;
                    segmentSSRRequest.FlightDesignator = new FlightDesignator();
                    segmentSSRRequest.FlightDesignator.CarrierCode = "SG";
                    segmentSSRRequest.FlightDesignator.FlightNumber = objFlights[i];
                    segmentSSRRequest.STD = dtSTD;
                    List<PaxSSR> PaxSSRs = new List<PaxSSR>();
                    for (int j = 0; j < itinerary.Passenger.Length; j++)
                    {
                        if (itinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(itinerary.Passenger[j].BaggageType))
                            {
                                PaxSSR paxSSR = new PaxSSR();
                                paxSSR.State = SpiceJet.SGBooking.MessageState.New;
                                paxSSR.ActionStatusCode = "NN";
                                paxSSR.ArrivalStation = sDestination;
                                paxSSR.DepartureStation = sOrigin;
                                paxSSR.PassengerNumber = (Int16)j;
                                string baggageCode = itinerary.Passenger[j].BaggageType.Split(',')[0];
                                if (itinerary.Passenger[j].BaggageType.Split(',').Length > 1 && (itinerary.Segments[i].Group == 1 || (objSegment.Status == "VIA" && objSegment.Group == 1)))//Return & For VIA flights we bind the baggage SSR only one time to Request.
                                {
                                    paxSSR.SSRCode = itinerary.Passenger[j].BaggageType.Split(',')[1];
                                }
                                if (itinerary.Passenger[j].BaggageType.Split(',').Length > 1 && objSegment.Group == 1)// Onward Via & Return Connecting )
                                {
                                    paxSSR.SSRCode = itinerary.Passenger[j].BaggageType.Split(',')[1];
                                }
                                else
                                {
                                    paxSSR.SSRCode = baggageCode;
                                }
                                paxSSR.SSRNumber = (Int16)0;
                                paxSSR.SSRValue = (Int16)0;
                                if (!string.IsNullOrEmpty(paxSSR.SSRCode))
                                {
                                    PaxSSRs.Add(paxSSR);
                                }
                            }

                        if (itinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(itinerary.Passenger[j].MealType))
                        {
                            string mealCode = itinerary.Passenger[j].MealType.Split(',')[0];//For Normal One Way + Combination One Way + Combination Round Trip.
                            if (itinerary.Passenger[j].MealType.Split(',').Length > 1 && (itinerary.Segments[i].Group == 1 || (objSegment.Group == 1 && objSegment.Status == "VIA")))//For Normal Round Trip For VIA flights we bind the baggage SSR only one time to Request.
                            {
                                mealCode = itinerary.Passenger[j].MealType.Split(',')[1];
                            }
                            if (itinerary.Passenger[j].MealType.Split(',').Length > 1 && objSegment.Group == 1)//onward VIA & return connecting.
                            {
                                mealCode = itinerary.Passenger[j].MealType.Split(',')[1];
                            }
                            if (!string.IsNullOrEmpty(mealCode))
                            {
                                PaxSSR paxSSR = new PaxSSR();
                                paxSSR.State = SpiceJet.SGBooking.MessageState.New;
                                paxSSR.ActionStatusCode = "NN";

                                paxSSR.ArrivalStation = sDestination;
                                paxSSR.DepartureStation = sOrigin;
                                
                                paxSSR.PassengerNumber = (Int16)j;
                                if (itinerary.Passenger[j].MealType.Split(',').Length > 1 && itinerary.Segments[i].Group == 1)//Return
                                {
                                    paxSSR.SSRCode = itinerary.Passenger[j].MealType.Split(',')[1];
                                }
                                else
                                {
                                    paxSSR.SSRCode = mealCode;
                                }
                                paxSSR.SSRNumber = (Int16)0;
                                paxSSR.SSRValue = (Int16)0;
                                if (!string.IsNullOrEmpty(paxSSR.SSRCode))
                                {
                                    PaxSSRs.Add(paxSSR);
                                }
                            }
                        }

                        //Checking paxType is not Infant for Spice Jet Business class
                        if(itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Segments[i].SegmentFareType.Contains("Business"))
                        {
                                PaxSSR paxSSR = new PaxSSR();
                                paxSSR.State = SpiceJet.SGBooking.MessageState.New;
                                paxSSR.ActionStatusCode = "NN";
                                paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                paxSSR.PassengerNumber = (Int16)j;

                                paxSSR.SSRCode = "BIZZ";
                                PaxSSRs.Add(paxSSR);
                                mealSSR++;
                        }

                        //Sending MAXP as SSR Code for Spice Max it is Manditory
                        if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Segments[i].SegmentFareType.Contains("Spice Max"))
                        {
                            PaxSSR paxSSR = new PaxSSR();
                            paxSSR.State = SpiceJet.SGBooking.MessageState.New;
                            paxSSR.ActionStatusCode = "NN";
                            paxSSR.ArrivalStation = sDestination;
                            paxSSR.DepartureStation = sOrigin;
                            paxSSR.PassengerNumber = (Int16)j;

                            paxSSR.SSRCode = "MAXP";
                            PaxSSRs.Add(paxSSR);
                            mealSSR++;
                        }

                    }
                    

                    if (PaxSSRs != null && PaxSSRs.Count > 0)
                        {
                            segmentSSRRequest.PaxSSRs = PaxSSRs.ToArray();
                            segSSRRequests.Add(segmentSSRRequest);
                        }
                    }

                    if (!string.IsNullOrEmpty(promoCode))//Promo Code Implementation for corporate fare
                    {
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale = new TypeOfSale();
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes = new string[1];
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes[0] = "C";//Corporate Fare
                        requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.PromotionCode = promoCode;
                    }

                    //If Excess baggage or meals is selected then book it otherwise skip this operation
                    if (baggageSSR > 0 || mealSSR > 0)
                    {
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests = segSSRRequests.ToArray();

                        SellRequest sellRequestObj = new SellRequest();
                        sellRequestObj.ContractVersion = contractVersion;
                        sellRequestObj.SellRequestData = requestData;
                        sellRequestObj.Signature = signature;
                        sellRequestObj.EnableExceptionStackTrace = false;

                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGSellBaggageRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, sellRequestObj);
                            sw.Close();
                        }
                        catch { }

                        SellResponse sellResponse = bookingAPI.Sell(sellRequestObj);
                        responseData = sellResponse.BookingUpdateResponseData;

                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(SellResponse));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGSellBaggageResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, sellResponse);
                            sw.Close();
                        }
                        catch { }
                    }
                    else
                    {
                        responseData.Success = new Success();
                    }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to book Baggage. Reason : " + ex.ToString(), "");
                responseData.Success = null;
            }
            return responseData;
        }
        //public BookingUpdateResponseData BookBaggage(FlightItinerary itinerary, string signature)
        //{
        //    BookingUpdateResponseData responseData = new BookingUpdateResponseData();

        //    try
        //    {
        //        SellRequestData requestData = new SellRequestData();
        //        requestData.SellBy = SellBy.SSR;
        //        requestData.SellSSR = new SellSSR();
        //        requestData.SellSSR.SSRRequest = new SSRRequest();
        //        requestData.SellSSR.SSRRequest.CancelFirstSSR = false;
        //        requestData.SellSSR.SSRRequest.CurrencyCode = currencyCode;
        //        requestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;
        //        requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[itinerary.Segments.Length];
        //        int counter = 0, baggageCount = 0;
        //        for (int i = 0; i < itinerary.Segments.Length; i++)
        //        {
        //            requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
        //            requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
        //            requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = itinerary.Segments[i].Origin.AirportCode;
        //            requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
        //            requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = "SG";
        //            requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = itinerary.Segments[i].FlightNumber.Replace("SG", "").Trim();
        //            requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = itinerary.Segments[i].DepartureTime;
        //            List<PaxSSR> PaxSSRs = new List<PaxSSR>();
        //            for (int j = 0; j < itinerary.Passenger.Length; j++)
        //            {
        //                if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
        //                {
        //                    if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
        //                    {
        //                        PaxSSR paxSSR = new PaxSSR();
        //                        paxSSR.State = SpiceJet.SGBooking.MessageState.New;
        //                        paxSSR.ActionStatusCode = "NN";
        //                        paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
        //                        paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;
        //                        //paxSSR.FeeCode = "";
        //                        //paxSSR.Note = "";
        //                        paxSSR.PassengerNumber = (Int16)j;
        //                        if (itinerary.Passenger[j].BaggageType.Split(',').Length > 1)//Return
        //                        {
        //                            paxSSR.SSRCode = itinerary.Passenger[j].BaggageType.Split(',')[itinerary.Segments[i].Group];
        //                        }
        //                        else
        //                        {
        //                            paxSSR.SSRCode = itinerary.Passenger[j].BaggageType;//Oneway
        //                        }
        //                        //paxSSR.SSRDetail = "";
        //                        paxSSR.SSRNumber = (Int16)0;
        //                        paxSSR.SSRValue = (Int16)0;
        //                        if (paxSSR.SSRCode.Length > 0)
        //                        {
        //                            PaxSSRs.Add(paxSSR);
        //                        }
        //                        baggageCount++;
        //                    }

        //                }
        //            }
        //            requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = PaxSSRs.ToArray();
        //            counter++;
        //        }

        //        //New object creation for signature inclusion.
        //        SellRequest sellRequestObj = new SellRequest();
        //        sellRequestObj.ContractVersion = contractVersion;
        //        sellRequestObj.SellRequestData = requestData;
        //        sellRequestObj.Signature = signature;
        //        sellRequestObj.EnableExceptionStackTrace = false;

        //        //If Excess baggage is selected then book it otherwise skip this operation
        //        if (baggageCount > 0)
        //        {
        //            try
        //            {

        //                XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
        //                string filePath = xmlLogPath + sessionId + "_" + appUserId + "_SGSellBaggageRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
        //                StreamWriter sw = new StreamWriter(filePath);
        //                ser.Serialize(sw, sellRequestObj);
        //                sw.Close();
        //            }
        //            catch { }

        //            SellResponse sellResponse = bookingAPI.Sell(sellRequestObj);
        //            responseData = sellResponse.BookingUpdateResponseData;

        //            try
        //            {

        //                XmlSerializer ser = new XmlSerializer(typeof(SellResponse));
        //                string filePath = xmlLogPath + sessionId + "_" + appUserId + "_SGSellBaggageResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
        //                StreamWriter sw = new StreamWriter(filePath);
        //                ser.Serialize(sw, sellResponse);
        //                sw.Close();
        //            }
        //            catch { }
        //        }
        //        else
        //        {
        //            responseData = null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to book Baggage. Reason : " + ex.ToString(), "");
        //    }
        //    return responseData;
        //}

        /// <summary>
        /// Combining Infant fare for the Booking.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        public BookingUpdateResponseData SellSSRForInfant(FlightItinerary itinerary, string signature)
        {
            decimal bookingAmount = 0;
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.SSR;
                requestData.SellSSR = new SellSSR();
                requestData.SellSSR.SSRRequest = new SSRRequest();
                requestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                requestData.SellSSR.SSRRequest.CurrencyCode = currencyCode;
                requestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;

                var objFlights = itinerary.Segments.Select(p => p.FlightNumber).Distinct().ToArray();
                string sOrigin = string.Empty, sDestination = string.Empty; DateTime dtSTD;

                requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[objFlights.Length];

                for (int i = 0; i < objFlights.Length; i++)
                {
                    sOrigin = itinerary.Segments.Where(y => y.FlightNumber == objFlights[i]).Select(z => z.Origin.AirportCode).FirstOrDefault();
                    sDestination = itinerary.Segments.Where(y => y.FlightNumber == objFlights[i]).Select(z => z.Destination.AirportCode).Last();
                    dtSTD = itinerary.Segments.Where(y => y.FlightNumber == objFlights[i]).Select(f => f.DepartureTime).FirstOrDefault();

                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = sDestination;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = sOrigin;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = "SG";
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = objFlights[i];
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = dtSTD;
                    //requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = new PaxSSR[itinerary.Passenger.Length];
                    //if (i == 0)
                    {
                        int counter = 0;
                        List<PaxSSR> PaxSSRs = new List<PaxSSR>();
                        for (int j = 0; j < itinerary.Passenger.Length; j++)
                        {
                            if (itinerary.Passenger[j].Type == PassengerType.Infant)
                            {
                                PaxSSR paxSSR = new PaxSSR();
                                paxSSR.ActionStatusCode = "NN";
                                paxSSR.ArrivalStation = sDestination;
                                paxSSR.DepartureStation = sOrigin;
                                paxSSR.FeeCode = "";
                                paxSSR.Note = "";
                                paxSSR.PassengerNumber = (Int16)counter;
                                paxSSR.SSRCode = "INFT";
                                paxSSR.SSRDetail = "";
                                paxSSR.SSRNumber = (Int16)0;
                                paxSSR.SSRValue = (Int16)0;
                                counter++;
                                PaxSSRs.Add(paxSSR);
                            }
                        }
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = PaxSSRs.ToArray();
                    }
                }
                if (!string.IsNullOrEmpty(promoCode))//Promo Code Implementation for corporate fare
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale = new TypeOfSale();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes = new string[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes[0] = "C";//Corporate Fare
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.PromotionCode = promoCode;
                }

                //New object creation for signature inclusion.
                SellRequest sellRequestObj = new SellRequest();
                sellRequestObj.ContractVersion = contractVersion;
                sellRequestObj.SellRequestData = requestData;
                sellRequestObj.Signature = signature;
                sellRequestObj.EnableExceptionStackTrace = false;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGSellInfantRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellRequestObj);
                    sw.Close();
                }
                catch { }

                SellResponse sellResponse = bookingAPI.Sell(sellRequestObj);
                responseData = sellResponse.BookingUpdateResponseData;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGSellInfantResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellResponse);
                    sw.Close();
                }
                catch { }
                bookingAmount += responseData.Success.PNRAmount.TotalCost;
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to book Baggage. Reason : " + ex.ToString(), "");
                responseData.Success = null;
            }
            return responseData;
        }

        /// <summary>
        /// Confirmation of Total Price
        /// </summary>
        /// <param name="bookingAmount"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private AddPaymentToBookingResponse AddPaymentForBooking(decimal bookingAmount, string signature)
        {
            AddPaymentToBookingResponse response = new AddPaymentToBookingResponse();
            try
            {
                AddPaymentToBookingRequestData request = new AddPaymentToBookingRequestData();
                request.AccountNumber = agentId;
                request.AccountNumberID = 0;
                request.AuthorizationCode = "";
                request.Deposit = false;
                request.Expiration = new DateTime();
                request.Installments = 0;
                request.MessageState = SpiceJet.SGBooking.MessageState.New;
                request.ParentPaymentID = 0;
                request.PaymentAddresses = new PaymentAddress[0];
                request.PaymentFields = new PaymentField[0];
                request.PaymentMethodCode = "AG";
                request.PaymentMethodType = RequestPaymentMethodType.AgencyAccount;
                request.PaymentText = "";
                request.QuotedAmount = bookingAmount;
                request.QuotedCurrencyCode = currencyCode;
                request.ReferenceType = PaymentReferenceType.Default;
                request.Status = BookingPaymentStatus.New;
                request.ThreeDSecureRequest = new ThreeDSecureRequest();
                request.WaiveFee = false;

                //New object creation for signature inclusion.
                AddPaymentToBookingRequest addPaymentToBookingObj = new AddPaymentToBookingRequest();
                addPaymentToBookingObj.addPaymentToBookingReqData = request;
                addPaymentToBookingObj.ContractVersion = contractVersion;
                addPaymentToBookingObj.Signature = signature;
                addPaymentToBookingObj.EnableExceptionStackTrace = false;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(AddPaymentToBookingRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGAddPaymentToBookingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, addPaymentToBookingObj);
                    sw.Close();
                }
                catch { }

                response = bookingAPI.AddPaymentToBooking(addPaymentToBookingObj);

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(AddPaymentToBookingResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGAddPaymentToBookingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, response);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Add Payment for booking. Reason : " + ex.Message, "");
                throw ex;
            }
            return response;
        }

        /// <summary>This method will have the API calls to book SSR (if any), update passenger, book the itinerary and generate the PNR
        /// Based on the Ititnerary book request type it will make the necessary calls. If itinerary status is Update pax it will book SSR
        /// like baagage, meal and SSR for infant if any, once these calls are success will update the itinerary status as Assign seat.
        /// Then it will call update passenger and then assign seat, if any failure in seat assignment the itinerary status remain same 
        /// until seat assignment is successful else status will change to pax updated and will proceed for add payment and booking commit calls.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns>Booking response</returns>
        public BookingResponse BookItinerary(ref FlightItinerary itinerary)
        {
            //Note:for normal search signature will be assigned from the basket based on the user Id.
            //Note:for one-one search signature will be assigned from the itinerary object[Endorsement Property] .
            BookingResponse response = new BookingResponse();
            LogonResponse loginResponse = null;
            try
            {
                if (!string.IsNullOrEmpty(itinerary.Endorsement))
                {
                    loginResponse = new LogonResponse();
                    loginResponse.Signature = itinerary.Endorsement;
                }
                else
                {
                    loginResponse = Login();
                }
                BookingUpdateResponseData bookResponse = new BookingUpdateResponseData();

                int infantCount = itinerary.Passenger.Where(x => x.Type == PassengerType.Infant).Count();

                if (itinerary.BookRequestType == BookingFlowStatus.UpdatePax || itinerary.BookRequestType == BookingFlowStatus.AssignSeat)
                {
                    if (itinerary.BookRequestType == BookingFlowStatus.UpdatePax)
                        bookResponse = BookBaggage(itinerary, loginResponse.Signature);
                    else
                        bookResponse.Success = new Success();

                    if (bookResponse != null && bookResponse.Success != null)
                    {
                        if (infantCount > 0 && itinerary.BookRequestType == BookingFlowStatus.UpdatePax)
                        {
                            bookResponse = null;
                            bookResponse = SellSSRForInfant(itinerary, loginResponse.Signature);
                        }

                        if (bookResponse != null && bookResponse.Success != null)
                        {
                            itinerary.BookRequestType = BookingFlowStatus.AssignSeat;
                            UpdatePassengersResponse updatePaxResponse = UpdatePassengers(itinerary, loginResponse.Signature);
                            if (updatePaxResponse != null && updatePaxResponse.BookingUpdateResponseData != null && updatePaxResponse.BookingUpdateResponseData.Success != null && updatePaxResponse.BookingUpdateResponseData.Success.PNRAmount.TotalCost > 0)
                            {
                                if (itinerary.Passenger.Where(y => !string.IsNullOrEmpty(y.SeatInfo)).Count() > 0)
                                {
                                    response = AssignSeats(ref itinerary, loginResponse.Signature);
                                }
                                else
                                {
                                    itinerary.BookRequestType = BookingFlowStatus.PaxUpdated;
                                    response.Status = BookingResponseStatus.Successful; response.Error = string.Empty;
                                }

                                itinerary.ItineraryAmountDue = itinerary.ItineraryAmountDue == 0 ? updatePaxResponse.BookingUpdateResponseData.Success.PNRAmount.TotalCost :
                                    itinerary.ItineraryAmountDue;
                            }
                            else
                            {
                                response.Status = BookingResponseStatus.UpdatePaxFailed;
                                response.Error = "Update Passenger failed";
                            }
                        }
                        else
                        {
                            response.Status = BookingResponseStatus.SSRforInfantFailed;
                            response.Error = "SSR for Infant passenger failed";
                        }
                    }
                    else
                    {
                        response.Status = BookingResponseStatus.Bookbaggagefailed;
                        response.Error = "Baggage booking failed";
                    }
                }
                else if (itinerary.BookRequestType == BookingFlowStatus.PaxUpdated)
                {
                    AddPaymentToBookingResponse paymentResponse = AddPaymentForBooking(itinerary.ItineraryAmountDue, loginResponse.Signature);
                    //Checking payment Validation Errors Exist or Not.
                    if (paymentResponse != null && paymentResponse.BookingPaymentResponse != null && paymentResponse.BookingPaymentResponse.ValidationPayment != null && paymentResponse.BookingPaymentResponse.ValidationPayment.PaymentValidationErrors != null && paymentResponse.BookingPaymentResponse.ValidationPayment.PaymentValidationErrors.Length == 0)
                    {
                        try
                        {
                            BookingCommitResponse bookingCommitResponse = new BookingCommitResponse();
                            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

                            BookingCommitRequestData request = new BookingCommitRequestData();
                            request.BookingChangeCode = "";
                            request.BookingComments = new BookingComment[0];

                            request.SourcePOS = new PointOfSale();
                            request.SourcePOS.State = SpiceJet.SGBooking.MessageState.New;
                            request.SourcePOS.AgentCode = agentId;
                            request.SourcePOS.OrganizationCode = agentId;
                            request.SourcePOS.DomainCode = agentDomain;

                            request.ReceivedBy = new ReceivedByInfo();
                            request.ReceivedBy.ReceivedBy = agentId;

                            request.BookingID = 0;
                            request.BookingParentID = 0;
                            request.ChangeHoldDateTime = false;
                            request.CurrencyCode = currencyCode;
                            request.DistributeToContacts = true;
                            request.GroupName = "";
                            request.NumericRecordLocator = "";
                            request.ParentRecordLocator = "";
                            request.RecordLocator = "";
                            request.RecordLocators = new RecordLocator[0];
                            request.RestrictionOverride = false;

                            request.State = SpiceJet.SGBooking.MessageState.New;
                            request.SystemCode = "";
                            request.WaiveNameChangeFee = false;
                            request.WaivePenaltyFee = false;
                            request.WaiveSpoilageFee = false;
                            request.PaxCount = (Int16)(itinerary.Passenger.Length - infantCount);

                            for (int i = 0; i < itinerary.Passenger.Length; i++)
                            {
                                FlightPassenger pax = itinerary.Passenger[i];
                                if (pax.Type != PassengerType.Infant)
                                {

                                    if (pax.IsLeadPax)
                                    {
                                        request.BookingContacts = new BookingContact[1];
                                        request.BookingContacts[0] = new BookingContact();
                                        request.BookingContacts[0].AddressLine1 = (string.IsNullOrEmpty(pax.AddressLine1) ? "Address Line1" : (pax.AddressLine1.Length <= 52 ? pax.AddressLine1 : pax.AddressLine1.Substring(0, 52)));//Address Line1 Must Be No more than 52 Characters in length.;
                                        request.BookingContacts[0].AddressLine2 = pax.AddressLine2;
                                        request.BookingContacts[0].AddressLine3 = "";
                                        request.BookingContacts[0].City = "SHJ";
                                        request.BookingContacts[0].CompanyName = "Cozmo Travel";
                                        if (pax.Country != null && !string.IsNullOrEmpty(pax.Country.CountryCode))
                                        {
                                            request.BookingContacts[0].CountryCode = pax.Country.CountryCode;
                                        }
                                        else
                                        {
                                            request.BookingContacts[0].CountryCode = "AE";
                                        }
                                        request.BookingContacts[0].CultureCode = "en-GB";
                                        request.BookingContacts[0].CustomerNumber = "";
                                        request.BookingContacts[0].DistributionOption = DistributionOption.Email;
                                        request.BookingContacts[0].EmailAddress = pax.Email;
                                        request.BookingContacts[0].EmailAddress = "tech@cozmotravel.com";

                                        if (!string.IsNullOrEmpty(pax.Email))
                                        {
                                            request.BookingContacts[0].EmailAddress = pax.Email; //to get ticket voucher emails from SpiceJet
                                        }
                                        else
                                        {
                                            request.BookingContacts[0].EmailAddress = "tech@cozmotravel.com";
                                        }
                                        request.BookingContacts[0].Fax = "";
                                        request.BookingContacts[0].HomePhone = pax.CellPhone;
                                        request.BookingContacts[0].WorkPhone = pax.CellPhone;
                                        request.BookingContacts[0].Names = new BookingName[1];
                                        request.BookingContacts[0].Names[0] = new BookingName();
                                        request.BookingContacts[0].Names[0].FirstName = pax.FirstName;
                                        request.BookingContacts[0].Names[0].LastName = pax.LastName;
                                        request.BookingContacts[0].Names[0].MiddleName = "";
                                        request.BookingContacts[0].Names[0].Suffix = "";
                                        request.BookingContacts[0].Names[0].Title = pax.Title;
                                        request.BookingContacts[0].NotificationPreference = NotificationPreference.None;
                                        if (!string.IsNullOrEmpty(pax.DestinationPhone))
                                        {
                                            request.BookingContacts[0].OtherPhone = pax.DestinationPhone;
                                        }
                                        else
                                        {
                                            request.BookingContacts[0].OtherPhone = "";
                                        }
                                        request.BookingContacts[0].PostalCode = "3093";
                                        request.BookingContacts[0].ProvinceState = "SHJ";
                                        request.BookingContacts[0].SourceOrganization = agentId;
                                        request.BookingContacts[0].State = SpiceJet.SGBooking.MessageState.New;
                                        request.BookingContacts[0].TypeCode = "P";
                                        request.BookingContacts[0].WorkPhone = pax.CellPhone;
                                    }

                                }
                            }
                            if (infantCount > 0)
                            {
                                int j = 0;
                                int infants = 0;
                                for (int i = 0; i < itinerary.Passenger.Length; i++)
                                {
                                    FlightPassenger pax1 = itinerary.Passenger[i];
                                    if (pax1.Type == PassengerType.Adult)
                                    {
                                        if (infantCount - infants > 0)
                                        {
                                            if (i == 0)
                                            {
                                                j = itinerary.Passenger.Length - infantCount;
                                            }

                                            FlightPassenger pax = itinerary.Passenger[j];

                                            if ((j + 1) < itinerary.Passenger.Length)
                                            {
                                                j++;
                                            }
                                        }
                                        infants++;

                                    }
                                }
                            }

                            //New object for signature inclusion
                            BookingCommitRequest bookingCommitRequestObj = new BookingCommitRequest();
                            bookingCommitRequestObj.BookingCommitRequestData = request;
                            bookingCommitRequestObj.ContractVersion = contractVersion;
                            bookingCommitRequestObj.Signature = loginResponse.Signature;
                            bookingCommitRequestObj.EnableExceptionStackTrace = false;
                            try
                            {

                                XmlSerializer ser = new XmlSerializer(typeof(BookingCommitRequest));
                                string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGBookingCommitRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                StreamWriter sw = new StreamWriter(filePath);
                                ser.Serialize(sw, bookingCommitRequestObj);
                                sw.Close();
                            }
                            catch { }

                            bookingCommitResponse = bookingAPI.BookingCommit(bookingCommitRequestObj);
                            responseData = bookingCommitResponse.BookingUpdateResponseData;

                            try
                            {

                                XmlSerializer ser = new XmlSerializer(typeof(BookingCommitResponse));
                                string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGBookingCommitResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                StreamWriter sw = new StreamWriter(filePath);
                                ser.Serialize(sw, bookingCommitResponse);
                                sw.Close();
                            }
                            catch { }

                            if (responseData != null)
                            {
                                if (responseData.Error == null)
                                {
                                    response.PNR = responseData.Success.RecordLocator;
                                    response.ProdType = ProductType.Flight;
                                    response.Error = "";
                                    response.Status = BookingResponseStatus.Successful;
                                    itinerary.PNR = response.PNR;
                                    itinerary.FareType = "Pub";
                                }
                                else
                                {
                                    response.Status = BookingResponseStatus.Failed;
                                }
                            }
                        }

                        catch (Exception ex)
                        {
                            Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Commit booking. Reason : " + ex.ToString(), "");
                            response.Error = ex.Message;
                            response.Status = BookingResponseStatus.Failed;
                        }
                        finally
                        {
                            Logout(loginResponse.Signature);
                        }
                    }
                    else//AddPayment Booking failed
                    {
                        response.Status = BookingResponseStatus.Failed;
                        if (paymentResponse != null && paymentResponse.BookingPaymentResponse != null && paymentResponse.BookingPaymentResponse.ValidationPayment != null && paymentResponse.BookingPaymentResponse.ValidationPayment.PaymentValidationErrors != null && paymentResponse.BookingPaymentResponse.ValidationPayment.PaymentValidationErrors.Length > 0)
                        {
                            response.Error = paymentResponse.BookingPaymentResponse.ValidationPayment.PaymentValidationErrors[0].ErrorDescription;
                            Audit.Add(EventType.Book, Severity.High, appUserId, "Spice Jet Booking Failed. Reason : " + response.Error, "");
                        }
                    }
                }
                else
                {
                    BookingUpdateResponseData bagResponse = BookBaggage(itinerary, loginResponse.Signature);

                    if (bagResponse != null && bagResponse.Success != null && bagResponse.Success.PNRAmount != null && bagResponse.Success.PNRAmount.TotalCost > 0)
                    {
                        bookResponse = bagResponse;
                    }
                    else if (bagResponse != null && bagResponse.Warning != null)
                    {
                        Audit.Add(EventType.Book, Severity.High, appUserId, "(SpiceJet)SSR Baggage request Failed. Reason : " + bagResponse.OtherServiceInformations[0].Text, "");
                        response.Error = "Requested SSR Not available.Reason:" + bagResponse.OtherServiceInformations[0].Text;
                        return response;
                    }
                    if (infantCount > 0)
                    {

                        SellSSRForInfant(itinerary, loginResponse.Signature);
                    }
                    UpdatePassengersResponse updatePaxResponse = UpdatePassengers(itinerary, loginResponse.Signature);
                    //AssignSeatsResponse clsAssignSeatsResponse = AssignSeats(ref itinerary, loginResponse.Signature);
                    if (updatePaxResponse != null && updatePaxResponse.BookingUpdateResponseData != null && updatePaxResponse.BookingUpdateResponseData.Success != null && updatePaxResponse.BookingUpdateResponseData.Success.PNRAmount.TotalCost > 0)
                    {
                        AddPaymentToBookingResponse paymentResponse = AddPaymentForBooking(updatePaxResponse.BookingUpdateResponseData.Success.PNRAmount.TotalCost, loginResponse.Signature);
                        //Checking payment Validation Errors Exist or Not 
                        if (paymentResponse != null && paymentResponse.BookingPaymentResponse != null && paymentResponse.BookingPaymentResponse.ValidationPayment != null && paymentResponse.BookingPaymentResponse.ValidationPayment.PaymentValidationErrors != null && paymentResponse.BookingPaymentResponse.ValidationPayment.PaymentValidationErrors.Length == 0)
                        {
                            try
                            {
                                BookingCommitResponse bookingCommitResponse = new BookingCommitResponse();
                                BookingUpdateResponseData responseData = new BookingUpdateResponseData();

                                BookingCommitRequestData request = new BookingCommitRequestData();
                                request.BookingChangeCode = "";
                                request.BookingComments = new BookingComment[0];

                                request.SourcePOS = new PointOfSale();
                                request.SourcePOS.State = SpiceJet.SGBooking.MessageState.New;
                                request.SourcePOS.AgentCode = agentId;
                                request.SourcePOS.OrganizationCode = agentId;
                                request.SourcePOS.DomainCode = agentDomain;

                                request.ReceivedBy = new ReceivedByInfo();
                                request.ReceivedBy.ReceivedBy = agentId;

                                request.BookingID = 0;
                                request.BookingParentID = 0;
                                request.ChangeHoldDateTime = false;
                                request.CurrencyCode = currencyCode;
                                request.DistributeToContacts = true;
                                request.GroupName = "";
                                request.NumericRecordLocator = "";
                                request.ParentRecordLocator = "";
                                request.RecordLocator = "";
                                request.RecordLocators = new RecordLocator[0];
                                request.RestrictionOverride = false;

                                request.State = SpiceJet.SGBooking.MessageState.New;
                                request.SystemCode = "";
                                request.WaiveNameChangeFee = false;
                                request.WaivePenaltyFee = false;
                                request.WaiveSpoilageFee = false;
                                request.PaxCount = (Int16)(itinerary.Passenger.Length - infantCount);

                                for (int i = 0; i < itinerary.Passenger.Length; i++)
                                {
                                    FlightPassenger pax = itinerary.Passenger[i];
                                    if (pax.Type != PassengerType.Infant)
                                    {

                                        if (pax.IsLeadPax)
                                        {
                                            request.BookingContacts = new BookingContact[1];
                                            request.BookingContacts[0] = new BookingContact();
                                            request.BookingContacts[0].AddressLine1 = (string.IsNullOrEmpty(pax.AddressLine1) ? "Address Line1" : (pax.AddressLine1.Length <= 52 ? pax.AddressLine1 : pax.AddressLine1.Substring(0, 52)));//Address Line1 Must Be No more than 52 Characters in length.
                                            request.BookingContacts[0].AddressLine2 = pax.AddressLine2;
                                            request.BookingContacts[0].AddressLine3 = "";
                                            request.BookingContacts[0].City = "SHJ";
                                            request.BookingContacts[0].CompanyName = "Cozmo Travel";
                                            if (pax.Country != null && !string.IsNullOrEmpty(pax.Country.CountryCode))
                                            {
                                                request.BookingContacts[0].CountryCode = pax.Country.CountryCode;
                                            }
                                            else
                                            {
                                                request.BookingContacts[0].CountryCode = "AE";
                                            }
                                            request.BookingContacts[0].CultureCode = "en-GB";
                                            request.BookingContacts[0].CustomerNumber = "";
                                            request.BookingContacts[0].DistributionOption = DistributionOption.Email;
                                            request.BookingContacts[0].EmailAddress = pax.Email;
                                            request.BookingContacts[0].EmailAddress = "tech@cozmotravel.com";

                                            if (!string.IsNullOrEmpty(pax.Email))
                                            {
                                                request.BookingContacts[0].EmailAddress = pax.Email; //to get ticket voucher emails from SpiceJet
                                            }
                                            else
                                            {
                                                request.BookingContacts[0].EmailAddress = "tech@cozmotravel.com";
                                            }
                                            request.BookingContacts[0].Fax = "";
                                            request.BookingContacts[0].HomePhone = pax.CellPhone;
                                            request.BookingContacts[0].WorkPhone = pax.CellPhone;
                                            request.BookingContacts[0].Names = new BookingName[1];
                                            request.BookingContacts[0].Names[0] = new BookingName();
                                            request.BookingContacts[0].Names[0].FirstName = pax.FirstName;
                                            request.BookingContacts[0].Names[0].LastName = pax.LastName;
                                            request.BookingContacts[0].Names[0].MiddleName = "";
                                            request.BookingContacts[0].Names[0].Suffix = "";
                                            request.BookingContacts[0].Names[0].Title = pax.Title;
                                            request.BookingContacts[0].NotificationPreference = NotificationPreference.None;
                                            if (!string.IsNullOrEmpty(pax.DestinationPhone))
                                            {
                                                request.BookingContacts[0].OtherPhone = pax.DestinationPhone;
                                            }
                                            else
                                            {
                                                request.BookingContacts[0].OtherPhone = "";
                                            }
                                            request.BookingContacts[0].PostalCode = "3093";
                                            request.BookingContacts[0].ProvinceState = "SHJ";
                                            request.BookingContacts[0].SourceOrganization = agentId;
                                            request.BookingContacts[0].State = SpiceJet.SGBooking.MessageState.New;
                                            request.BookingContacts[0].TypeCode = "P";
                                            request.BookingContacts[0].WorkPhone = pax.CellPhone;
                                        }

                                    }
                                }
                                if (infantCount > 0)
                                {
                                    int j = 0;
                                    int infants = 0;
                                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                                    {
                                        FlightPassenger pax1 = itinerary.Passenger[i];
                                        if (pax1.Type == PassengerType.Adult)
                                        {
                                            if (infantCount - infants > 0)
                                            {
                                                if (i == 0)
                                                {
                                                    j = itinerary.Passenger.Length - infantCount;
                                                }

                                                FlightPassenger pax = itinerary.Passenger[j];

                                                if ((j + 1) < itinerary.Passenger.Length)
                                                {
                                                    j++;
                                                }
                                            }
                                            infants++;

                                        }
                                    }
                                }

                                //New object for signature inclusion
                                BookingCommitRequest bookingCommitRequestObj = new BookingCommitRequest();
                                bookingCommitRequestObj.BookingCommitRequestData = request;
                                bookingCommitRequestObj.ContractVersion = contractVersion;
                                bookingCommitRequestObj.Signature = loginResponse.Signature;
                                bookingCommitRequestObj.EnableExceptionStackTrace = false;
                                try
                                {

                                    XmlSerializer ser = new XmlSerializer(typeof(BookingCommitRequest));
                                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGBookingCommitRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                    StreamWriter sw = new StreamWriter(filePath);
                                    ser.Serialize(sw, bookingCommitRequestObj);
                                    sw.Close();
                                }
                                catch { }

                                bookingCommitResponse = bookingAPI.BookingCommit(bookingCommitRequestObj);
                                responseData = bookingCommitResponse.BookingUpdateResponseData;

                                try
                                {

                                    XmlSerializer ser = new XmlSerializer(typeof(BookingCommitResponse));
                                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGBookingCommitResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                    StreamWriter sw = new StreamWriter(filePath);
                                    ser.Serialize(sw, bookingCommitResponse);
                                    sw.Close();
                                }
                                catch { }

                                if (responseData != null)
                                {
                                    if (responseData.Error == null)
                                    {
                                        response.PNR = responseData.Success.RecordLocator;
                                        response.ProdType = ProductType.Flight;
                                        response.Error = "";
                                        response.Status = BookingResponseStatus.Successful;
                                        itinerary.PNR = response.PNR;
                                        itinerary.FareType = "Pub";
                                    }
                                    else
                                    {
                                        response.Status = BookingResponseStatus.Failed;
                                    }
                                }
                            }


                            catch (Exception ex)
                            {
                                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Commit booking. Reason : " + ex.ToString(), "");
                                response.Error = ex.Message;
                                response.Status = BookingResponseStatus.Failed;
                            }
                            finally
                            {
                                Logout(loginResponse.Signature);
                            }
                        }
                        else//AddPayment Booking failed
                        {
                            response.Status = BookingResponseStatus.Failed;
                            if (paymentResponse != null && paymentResponse.BookingPaymentResponse != null && paymentResponse.BookingPaymentResponse.ValidationPayment != null && paymentResponse.BookingPaymentResponse.ValidationPayment.PaymentValidationErrors != null && paymentResponse.BookingPaymentResponse.ValidationPayment.PaymentValidationErrors.Length > 0)
                            {
                                response.Error = paymentResponse.BookingPaymentResponse.ValidationPayment.PaymentValidationErrors[0].ErrorDescription;
                                Audit.Add(EventType.Book, Severity.High, appUserId, "Spice Jet Booking Failed. Reason : " + response.Error, "");
                            }
                        }
                    }
                    else//UpdatePassenger failed
                    {
                        response.Status = BookingResponseStatus.Failed;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Error = ex.Message.Contains("No such session") ? "Booking Session has been Expired. Please search Again" : ex.Message;
                Audit.Add(EventType.Book, Severity.High, appUserId, "Spice Jet Booking Failed. Reason : " + response.Error, "");
            }
            return response;
        }

        public GetBookingResponse GetBooking(string pnr, string signature)
        {
            GetBookingResponse response = new GetBookingResponse();

            try
            {
                GetBookingRequestData requestData = new GetBookingRequestData();
                requestData.GetBookingBy = GetBookingBy.RecordLocator;
                requestData.GetByRecordLocator = new GetByRecordLocator();
                requestData.GetByRecordLocator.RecordLocator = pnr;


                //New object for signature inclusion
                GetBookingRequest getBookingRequestObj = new GetBookingRequest();
                getBookingRequestObj.ContractVersion = contractVersion;
                getBookingRequestObj.Signature = signature;
                getBookingRequestObj.GetBookingReqData = requestData;
                getBookingRequestObj.EnableExceptionStackTrace = false;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGGetBookingRequest_" + pnr + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, getBookingRequestObj);
                    sw.Close();
                }
                catch { }

                response = bookingAPI.GetBooking(getBookingRequestObj);

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGGetBookingResponse_" + pnr + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, response);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to get Booking for PNR : " + pnr + ". Reason : " + ex.ToString(), "");
            }

            return response;
        }

        public Dictionary<string, string> CancelBooking(FlightItinerary itinerary)
        {
            Dictionary<string, string> cancellationData = new Dictionary<string, string>();

            LogonResponse loginResponse = Login();

            try
            {
                GetBookingResponse response = GetBooking(itinerary.PNR, loginResponse.Signature);

                CancelRequestData requestData = new CancelRequestData();
                requestData.CancelBy = CancelBy.Journey;
                requestData.CancelJourney = new CancelJourney();
                requestData.CancelJourney.CancelJourneyRequest = new CancelJourneyRequest();
                requestData.CancelJourney.CancelJourneyRequest.Journeys = new Journey[response.Booking.Journeys.Length];
                for (int i = 0; i < response.Booking.Journeys.Length; i++)
                {
                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i] = new Journey();
                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments = new Segment[response.Booking.Journeys[i].Segments.Length];
                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].State = SpiceJet.SGBooking.MessageState.New;
                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].JourneySellKey = response.Booking.Journeys[i].JourneySellKey;
                    for (int j = 0; j < response.Booking.Journeys[i].Segments.Length; j++)
                    {
                        Segment segment = response.Booking.Journeys[i].Segments[j];
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j] = new Segment();
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ActionStatusCode = "NN";
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ArrivalStation = segment.ArrivalStation;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ChannelType = SpiceJet.SGBooking.ChannelType.API;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].DepartureStation = segment.DepartureStation;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares = new SpiceJet.SGBooking.Fare[segment.Fares.Length];
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].FlightDesignator = segment.FlightDesignator;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].International = segment.International;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs = new Leg[segment.Legs.Length];
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].SegmentSellKey = segment.SegmentSellKey;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].STA = segment.STA;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].State = SpiceJet.SGBooking.MessageState.New;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].STD = segment.STD;
                        requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ChannelType = segment.ChannelType;

                        for (int f = 0; f < response.Booking.Journeys[i].Segments[j].Fares.Length; f++)
                        {
                            SpiceJet.SGBooking.Fare fare = response.Booking.Journeys[i].Segments[j].Fares[f];
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f] = new SpiceJet.SGBooking.Fare();
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].CarrierCode = fare.CarrierCode;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].ClassOfService = fare.ClassOfService;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].ClassType = fare.ClassType;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareApplicationType = fare.FareApplicationType;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareBasisCode = fare.FareBasisCode;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareClassOfService = fare.FareClassOfService;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareSellKey = fare.FareSellKey;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareSequence = fare.FareSequence;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareStatus = FareStatus.Default;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].InboundOutbound = fare.InboundOutbound;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].IsAllotmentMarketFare = false;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].RuleNumber = fare.RuleNumber;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].OriginalClassOfService = fare.OriginalClassOfService;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].TravelClassCode = fare.TravelClassCode;
                        }
                        for (int l = 0; l < response.Booking.Journeys[i].Segments[j].Legs.Length; l++)
                        {
                            Leg leg = response.Booking.Journeys[i].Segments[j].Legs[l];
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l] = new Leg();
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].ArrivalStation = leg.ArrivalStation;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].DepartureStation = leg.DepartureStation;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].FlightDesignator = leg.FlightDesignator;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].InventoryLegID = leg.InventoryLegID;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].LegInfo = leg.LegInfo;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].OperationsInfo = leg.OperationsInfo;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].STA = leg.STA;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].State = SpiceJet.SGBooking.MessageState.New;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].STD = leg.STD;
                        }
                    }
                    requestData.CancelJourney.CancelJourneyRequest.WaivePenaltyFee = false;
                    requestData.CancelJourney.CancelJourneyRequest.WaiveSpoilageFee = false;
                    requestData.CancelJourney.CancelJourneyRequest.PreventReprice = false;

                }

                //New object creation for signature inclusion.
                CancelRequest cancelRequestObj = new CancelRequest();
                cancelRequestObj.CancelRequestData = requestData;
                cancelRequestObj.ContractVersion = contractVersion;
                cancelRequestObj.Signature = loginResponse.Signature;
                cancelRequestObj.EnableExceptionStackTrace = false;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(CancelRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGCancelRequest_" + itinerary.PNR + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, cancelRequestObj);
                    sw.Close();
                }
                catch { }

                CancelResponse cancelResponse = bookingAPI.Cancel(cancelRequestObj);
                BookingUpdateResponseData responseData = cancelResponse.BookingUpdateResponseData;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(CancelResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGCancelResponse_" + itinerary.PNR + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, cancelResponse);
                    sw.Close();
                }
                catch { }

                if (responseData != null)
                {
                    if (responseData.Error == null)
                    {
                        string charges = responseData.Success.PNRAmount.TotalCost.ToString();
                        if (responseData.Success.PNRAmount.AlternateCurrencyBalanceDue > 0)
                        {
                            charges = responseData.Success.PNRAmount.AlternateCurrencyBalanceDue.ToString().Replace("-", "");
                        }
                        //rateOfExchange = (responseData.Success.PNRAmount.AlternateCurrencyCode != null && responseData.Success.PNRAmount.AlternateCurrencyCode.Trim().Length > 0 ? exchangeRates[responseData.Success.PNRAmount.AlternateCurrencyCode] : exchangeRates["AED"]);//
                        rateOfExchange = (responseData.Success.PNRAmount.AlternateCurrencyCode != null && responseData.Success.PNRAmount.AlternateCurrencyCode.Trim().Length > 0 ? exchangeRates[responseData.Success.PNRAmount.AlternateCurrencyCode] : exchangeRates[currencyCode]);// Mod By ziyad
                        cancellationData.Add("Cancelled", "True");
                        cancellationData.Add("Charges", (Convert.ToDecimal(charges) * rateOfExchange).ToString());
                    }
                    else
                    {
                        cancellationData.Add("Cancelled", "False");
                    }
                    AddPaymentToBookingResponse responseAddPayment = new AddPaymentToBookingResponse();
                    try
                    {
                        AddPaymentToBookingRequestData request = new AddPaymentToBookingRequestData();
                        request.AccountNumber = agentId;
                        request.AccountNumberID = 0;
                        request.AuthorizationCode = "";
                        request.Deposit = false;
                        request.Expiration = new DateTime();
                        request.Installments = 0;
                        request.MessageState = SpiceJet.SGBooking.MessageState.New;
                        request.ParentPaymentID = 0;
                        request.PaymentAddresses = new PaymentAddress[0];
                        request.PaymentFields = new PaymentField[0];
                        request.PaymentMethodCode = "AG";
                        request.PaymentMethodType = RequestPaymentMethodType.AgencyAccount;
                        request.PaymentText = "";
                        request.QuotedAmount = responseData.Success.PNRAmount.BalanceDue;
                        request.QuotedCurrencyCode = currencyCode;
                        request.ReferenceType = PaymentReferenceType.Default;
                        request.Status = BookingPaymentStatus.New;
                        request.ThreeDSecureRequest = new ThreeDSecureRequest();
                        request.WaiveFee = false;

                        //New object creation for signature inclusion.
                        AddPaymentToBookingRequest addPaymentToBookingObj = new AddPaymentToBookingRequest();
                        addPaymentToBookingObj.addPaymentToBookingReqData = request;
                        addPaymentToBookingObj.ContractVersion = contractVersion;
                        addPaymentToBookingObj.Signature = loginResponse.Signature;
                        addPaymentToBookingObj.EnableExceptionStackTrace = false;
                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(AddPaymentToBookingRequest));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGAddPaymentToBookingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, addPaymentToBookingObj);
                            sw.Close();
                        }
                        catch { }

                        responseAddPayment = bookingAPI.AddPaymentToBooking(addPaymentToBookingObj);

                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(AddPaymentToBookingResponse));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGAddPaymentToBookingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, responseAddPayment);
                            sw.Close();
                        }
                        catch { }

                        BookingCommitResponse bookingCommitResponse = new BookingCommitResponse();
                        BookingCommitRequestData bookingCommitRequestData = new BookingCommitRequestData();

                        bookingCommitRequestData.State = SpiceJet.SGBooking.MessageState.New;
                        bookingCommitRequestData.RecordLocator = response.Booking.RecordLocator;
                        bookingCommitRequestData.CurrencyCode = response.Booking.CurrencyCode;
                        bookingCommitRequestData.PaxCount = 0;
                        bookingCommitRequestData.BookingID = 0;
                        bookingCommitRequestData.BookingParentID = 0;
                        bookingCommitRequestData.RestrictionOverride = false;
                        bookingCommitRequestData.ChangeHoldDateTime = false;
                        bookingCommitRequestData.WaiveNameChangeFee = false;
                        bookingCommitRequestData.WaivePenaltyFee = false;
                        bookingCommitRequestData.WaiveSpoilageFee = false;
                        bookingCommitRequestData.DistributeToContacts = false;

                        //New object for signature inclusion
                        BookingCommitRequest bookingCommitRequestObj = new BookingCommitRequest();
                        bookingCommitRequestObj.BookingCommitRequestData = bookingCommitRequestData;
                        bookingCommitRequestObj.ContractVersion = contractVersion;
                        bookingCommitRequestObj.Signature = loginResponse.Signature;
                        bookingCommitRequestObj.EnableExceptionStackTrace = false;
                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(BookingCommitRequest));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGBookingCommitRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, bookingCommitRequestObj);
                            sw.Close();
                        }
                        catch { }

                        bookingCommitResponse = bookingAPI.BookingCommit(bookingCommitRequestObj);


                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(BookingCommitResponse));
                            string filePath = xmlLogPath + sessionId + "_" + appUserId + "_SGBookingCommitResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, bookingCommitResponse);
                            sw.Close();
                        }
                        catch { }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Add Payment for booking. Reason : " + ex.ToString(), "");
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.SpiceJetCancellation, Severity.High, 1, "(SpiceJet)Failed to Cancel booking. Reason : " + ex.ToString(), "");
            }
            finally
            {
                Logout(loginResponse.Signature);
            }

            return cancellationData;
        }

        /// <summary>
        /// Updates passenger details for the booking in booking state. 
        /// The UpdatePassengers method processes the list of passengers input in the UpdatePassengersRequest 
        /// using the PassengerNumber field to match the passengers to passengers in booking state. 
        /// The matching passenger details are replaced with the details from the request. 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private UpdatePassengersResponse UpdatePassengers(FlightItinerary itinerary, string signature)
        {
            UpdatePassengersResponse upResponse = new UpdatePassengersResponse();

            try
            {

                if (itinerary != null && !string.IsNullOrEmpty(signature) && signature.Length > 0)
                {

                    int paxCount = 0, infantCount = 0, infants = 0;
                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        if (itinerary.Passenger[i].Type != PassengerType.Infant)
                        {
                            paxCount++;
                        }
                        else
                        {
                            infantCount++;
                        }
                    }

                    UpdatePassengersRequest updatePassengerRequest = new UpdatePassengersRequest();
                    updatePassengerRequest.ContractVersion = contractVersion;
                    updatePassengerRequest.Signature = signature;
                    updatePassengerRequest.updatePassengersRequestData = new UpdatePassengersRequestData();
                    updatePassengerRequest.updatePassengersRequestData.Passengers = new Passenger[paxCount];

                    for (int i = 0; i < paxCount; i++)
                    {
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i] = new Passenger();
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerNumber = (Int16)i;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].State = SpiceJet.SGBooking.MessageState.New;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names = new BookingName[1];
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0] = new BookingName();
                        if (itinerary.Passenger[i].Type != PassengerType.Infant)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].Title = itinerary.Passenger[i].Title.ToUpper();
                        }
                        else
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].Title = "MR";
                        }
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].FirstName = itinerary.Passenger[i].FirstName;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].LastName = itinerary.Passenger[i].LastName;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].State = SpiceJet.SGBooking.MessageState.New;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo = new PassengerInfo();
                        if (itinerary.Passenger[i].Gender == Gender.Male)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Gender = SpiceJet.SGBooking.Gender.Male;
                        }
                        else
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Gender = SpiceJet.SGBooking.Gender.Female;
                        }
                        if (itinerary.Passenger[i].Nationality != null)
                        {
                            if (!string.IsNullOrEmpty(itinerary.Passenger[i].Nationality.CountryCode))
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Nationality = itinerary.Passenger[i].Nationality.CountryCode;
                            }
                        }
                        if (itinerary.Passenger[i].Country != null)
                        {
                            if (!string.IsNullOrEmpty(itinerary.Passenger[i].Country.CountryCode))
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.ResidentCountry = itinerary.Passenger[i].Country.CountryCode;
                            }
                        }
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos = new PassengerTypeInfo[1];
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0] = new PassengerTypeInfo();
                        if (itinerary.Passenger[i].Type == PassengerType.Adult)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].PaxType = "ADT";
                            if (itinerary.Passenger[i].DateOfBirth != DateTime.MinValue)
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = itinerary.Passenger[i].DateOfBirth;
                            }
                            else
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = DateTime.Today.AddYears(-30);
                            }
                            if (infantCount - infants > 0)
                            {
                                FlightPassenger infantPax = itinerary.Passenger[paxCount + infants];
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant = new PassengerInfant();
                                if (infantPax.DateOfBirth != DateTime.MinValue)
                                {
                                    updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.DOB = infantPax.DateOfBirth;
                                }
                                else
                                {
                                    updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.DOB = DateTime.Today.AddDays(-365);
                                }
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Gender = (infantPax.Gender == Gender.Male ? SpiceJet.SGBooking.Gender.Male : SpiceJet.SGBooking.Gender.Female);
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names = new BookingName[1];
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0] = new BookingName();
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].FirstName = infantPax.FirstName;
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].LastName = infantPax.LastName;
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].Title = "MR";
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].State = SpiceJet.SGBooking.MessageState.New;
                                if (itinerary.Passenger[i].Nationality != null)
                                {
                                    if (!string.IsNullOrEmpty(itinerary.Passenger[i].Nationality.CountryCode))
                                    {
                                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Nationality = itinerary.Passenger[i].Nationality.CountryCode;
                                    }
                                }
                                if (itinerary.Passenger[i].Country != null)
                                {
                                    if (!string.IsNullOrEmpty(itinerary.Passenger[i].Country.CountryCode))
                                    {
                                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.ResidentCountry = itinerary.Passenger[i].Country.CountryCode;
                                    }
                                }

                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.State = SpiceJet.SGBooking.MessageState.New;
                                infants++;
                            }
                        }
                        else if (itinerary.Passenger[i].Type == PassengerType.Child)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].PaxType = "CHD";
                            if (itinerary.Passenger[i].DateOfBirth != DateTime.MinValue)
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = itinerary.Passenger[i].DateOfBirth;
                            }
                            else
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = DateTime.Today.AddYears(-10);
                            }
                        }
                    }
                    updatePassengerRequest.EnableExceptionStackTrace = false;
                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(UpdatePassengersRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGUpdatePassengersRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, updatePassengerRequest);
                        sw.Close();
                    }
                    catch { }


                    upResponse = bookingAPI.UpdatePassengers(updatePassengerRequest);

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(UpdatePassengersResponse));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGUpdatePassengersResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, upResponse);
                        sw.Close();
                    }
                    catch { }
                }
                else
                {
                    throw new Exception("(SpiceJet)Failed to update passengers as itinerary or signature passed may be null");
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "(SpiceJet)Failed to update passengers. Error : " + ex.ToString(), "");
                throw ex;
            }
            return upResponse;
        }

        /// <summary>
        /// Assign seats for the selected pax. 
        /// Assign seats will send seat assign request for each passenger and each segment based on user seat selection 
        /// using the PassengerNumber field to match the passengers to passengers in booking state. 
        /// The matching passenger seat details are replaced with the seat details from the request. 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private BookingResponse AssignSeats(ref FlightItinerary clsFlightItinerary, string signature)
        {
            AssignSeatsResponse clsAssignSeatsResponse = new AssignSeatsResponse();
            DateTime dtSTD; string sOrigin = string.Empty, sDestination = string.Empty; bool bSegStatus = true;
            BookingResponse clsBookingResponse = new BookingResponse();

            var objFlights = clsFlightItinerary.Segments.Select(p => p.FlightNumber).Distinct();

            AssignSeatsRequest clsAssignSeatsRequest = new AssignSeatsRequest();

            clsAssignSeatsRequest.ContractVersion = contractVersion;
            clsAssignSeatsRequest.Signature = signature;
            clsAssignSeatsRequest.EnableExceptionStackTrace = false;
            clsAssignSeatsRequest.SellSeatRequest = new SeatSellRequest();

            clsAssignSeatsRequest.SellSeatRequest.SeatAssignmentMode = SeatAssignmentMode.PreSeatAssignment;
            clsAssignSeatsRequest.SellSeatRequest.IncludeSeatData = true;
            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests = new SegmentSeatRequest[clsFlightItinerary.Passenger.Where(m => m.Type != PassengerType.Infant).Count() * objFlights.Count()];
            int segcnt = 0;

            foreach (string flt in objFlights)
            {
                try
                {
                    sOrigin = clsFlightItinerary.Segments.Where(y => y.FlightNumber == flt).Select(z => z.Origin.AirportCode).FirstOrDefault();
                    sDestination = clsFlightItinerary.Segments.Where(y => y.FlightNumber == flt).Select(z => z.Destination.AirportCode).Last();
                    dtSTD = clsFlightItinerary.Segments.Where(y => y.FlightNumber == flt).Select(f => f.DepartureTime).FirstOrDefault();

                    clsFlightItinerary.Passenger.ToList().ForEach(p =>
                    {
                        var obj = p.liPaxSeatInfo.Where(y => y.SeatStatus == "A" && !string.IsNullOrEmpty(y.SeatNo) && y.Segment == sOrigin + "-" + sDestination).FirstOrDefault();
                        if (obj != null)
                        {
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt] = new SegmentSeatRequest();
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].ArrivalStation = sDestination;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].DepartureStation = sOrigin;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].STD = dtSTD;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].PassengerNumbers = new short[1];
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].PassengerNumbers[0] = new short();
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].PassengerNumbers[0] = (short)obj.PaxNo;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].UnitDesignator = obj.SeatNo;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].FlightDesignator = new FlightDesignator();
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].FlightDesignator.CarrierCode = "SG";
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].FlightDesignator.FlightNumber = flt.Length < 4 ? " " + flt : flt;
                            segcnt++;
                        }
                    });
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "(SpiceJet)Failed to prepare seat assign request. Error: " + ex.GetBaseException(), "");
                    clsFlightItinerary.Passenger.ToList().ForEach(y => y.liPaxSeatInfo.
                                ForEach(s => { y.SeatInfo = string.Empty; y.Price.SeatPrice = 0; s.SeatNo = "NoSeat"; s.Price = 0; s.SeatStatus = "F"; }));
                    clsBookingResponse.Error = "Failed to prepare seat assign request."; clsBookingResponse.Status = BookingResponseStatus.Assignseatfailed;
                }
            }

            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests = clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests.Where(j => j != null).ToArray();
            clsFlightItinerary.ItineraryAmountDue = 0;

            if (clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests.Length > 0)
            {
                XmlSerializer ser = new XmlSerializer(typeof(AssignSeatsRequest));
                string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + sOrigin + "_" + sDestination +
                    "_SGAssignSeatRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, clsAssignSeatsRequest);
                sw.Close();

                try
                {
                    clsAssignSeatsResponse = bookingAPI.AssignSeats(clsAssignSeatsRequest);
                }
                catch (Exception ex)
                {
                    bSegStatus = false;
                    Audit.Add(EventType.Exception, Severity.High, 1, "(SpiceJet)Failed to assign seat from supplier. Error: " + ex.GetBaseException(), "");
                }


                ser = new XmlSerializer(typeof(AssignSeatsResponse));
                filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + sOrigin + "_" + sDestination +
                    "_SGAssignSeatResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                sw = new StreamWriter(filePath);
                ser.Serialize(sw, clsAssignSeatsResponse);
                sw.Close();


                if (bSegStatus && clsAssignSeatsResponse.BookingUpdateResponseData != null && clsAssignSeatsResponse.BookingUpdateResponseData.Success != null)
                {
                    clsFlightItinerary.ItineraryAmountDue = clsAssignSeatsResponse.BookingUpdateResponseData.Success.PNRAmount.BalanceDue;
                    clsBookingResponse.Status = BookingResponseStatus.Successful;
                    clsFlightItinerary.Passenger.ToList().ForEach(y => y.liPaxSeatInfo.ForEach(s => s.SeatStatus = "S"));
                    clsFlightItinerary.BookRequestType = BookingFlowStatus.PaxUpdated;
                }
                else
                {
                    string err = string.Empty;
                    if (bSegStatus && clsAssignSeatsResponse.AssignedSeatInfo.JourneyList != null && clsAssignSeatsResponse.AssignedSeatInfo.JourneyList.Length > 0)
                    {
                        var Segments = new List<AssignSeatSegment>();
                        clsAssignSeatsResponse.AssignedSeatInfo.JourneyList.ToList().ForEach(x => { if (x.Segments != null) { Segments.AddRange(x.Segments); } });

                        var paxseats = new List<PaxSeat>();
                        Segments.ForEach(x => { if (x.PaxSeats != null) { paxseats.AddRange(x.PaxSeats); } });

                        clsFlightItinerary.Passenger.Where(b => !string.IsNullOrEmpty(b.SeatInfo)).ToList().
                            ForEach(y => y.liPaxSeatInfo.Where(a => a.SeatStatus == "A" && !string.IsNullOrEmpty(a.SeatNo)).ToList().ForEach(s =>
                        {
                            if (paxseats.Where(p => p.PassengerNumber == s.PaxNo && p.DepartureStation + "-" + p.ArrivalStation == s.Segment).Count() == 0)
                            {
                                string msg = "Failed to assign seat for passenger " + y.FirstName + " " + y.LastName + " for segment (" + s.Segment + ")";
                                err = string.IsNullOrEmpty(err) ? msg : err + "@" + msg;
                                s.SeatNo = "NoSeat"; y.Price.SeatPrice -= s.Price; s.Price = 0; s.SeatStatus = "F";
                            }
                            else
                                s.SeatStatus = "S";

                        }));
                    }
                    else
                    {
                        clsFlightItinerary.Passenger.Where(b => !string.IsNullOrEmpty(b.SeatInfo)).ToList().
                            ForEach(y => y.liPaxSeatInfo.Where(a => a.SeatStatus == "A" && !string.IsNullOrEmpty(a.SeatNo)).ToList().ForEach(s =>
                        {
                            string msg = "Failed to assign seat for passenger " + y.FirstName + " " + y.LastName + " for segment (" + s.Segment + ")";
                            err = string.IsNullOrEmpty(err) ? msg : err + "@" + msg;
                            s.SeatNo = "NoSeat"; y.Price.SeatPrice -= s.Price; s.Price = 0; s.SeatStatus = "F";
                        }));
                    }
                    clsBookingResponse.Status = BookingResponseStatus.Assignseatfailed;
                    clsBookingResponse.Error = err;
                }

            }
            else
            {
                clsFlightItinerary.ItineraryAmountDue = 0;
                clsBookingResponse.Status = BookingResponseStatus.Successful;
                clsFlightItinerary.BookRequestType = BookingFlowStatus.PaxUpdated;
            }
            return clsBookingResponse;
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~SpiceJetAPIV1()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Returns the flight details based on the segments 
        /// </summary>
        /// <param name="segment"></param>
        /// <param name="availableJourney"></param>
        /// <param name="productClass"></param>
        /// <param name="journeyType"></param>
        /// <returns></returns>
        public List<FlightInfo> GetFlightInfo(AvailableSegment segment, AvailableJourney availableJourney, string productClass, string journeyType)
        {
            List<FlightInfo> flightInfo = new List<FlightInfo>();
            try
            {
                if (availableJourney != null && segment != null && segment.Legs != null && segment.Legs.Length > 0)
                {
                    for (int k = 0; k < segment.Legs.Length; k++)
                    {
                        if (segment.Legs[k] != null)
                        {
                            FlightInfo flightDetails = new FlightInfo();
                            flightDetails.Airline = "SG";
                            flightDetails.ArrivalTime = segment.Legs[k].STA;
                            flightDetails.ArrTerminal = segment.Legs[k].LegInfo.ArrivalTerminal;
                            flightDetails.BookingClass = (segment != null && segment.CabinOfService.Trim().Length > 0 ? segment.CabinOfService : "C");
                            if (productClass == "BC")
                            {
                                flightDetails.CabinClass = "Business";
                            }
                            else if (!string.IsNullOrEmpty(segment.CabinOfService.Trim()))
                            {
                                flightDetails.CabinClass = segment.CabinOfService;
                            }
                            else
                            {
                                flightDetails.CabinClass = "Economy";
                            }
                            flightDetails.Craft = segment.Legs[k].LegInfo.EquipmentType + "-" + segment.Legs[k].LegInfo.EquipmentTypeSuffix;
                            flightDetails.DepartureTime = segment.Legs[k].STD;
                            flightDetails.DepTerminal = segment.Legs[k].LegInfo.DepartureTerminal;
                            flightDetails.Destination = new Airport(segment.Legs[k].ArrivalStation);
                            flightDetails.Duration = flightDetails.ArrivalTime.Subtract(flightDetails.DepartureTime);
                            flightDetails.ETicketEligible = segment.Legs[k].LegInfo.ETicket;
                            flightDetails.FlightNumber = segment.Legs[k].FlightDesignator.FlightNumber;
                            flightDetails.FlightStatus = FlightStatus.Confirmed;
                            if (journeyType.ToUpper() == "ONWARD")
                            {
                                flightDetails.Group = 0;
                            }
                            else if (journeyType.ToUpper() == "RETURN")
                            {
                                flightDetails.Group = 1;
                            }
                            flightDetails.OperatingCarrier = "SG";
                            flightDetails.Origin = new Airport(segment.Legs[k].DepartureStation);
                            flightDetails.Stops = availableJourney.AvailableSegment.Length - 1;

                            //Direct flight
                            if (segment.Legs.Length == 1 && availableJourney.AvailableSegment.Length == 1)
                            {
                                flightDetails.Status = string.Empty;
                            }
                            //Connecting flight
                            else if (availableJourney.AvailableSegment.Length > 1)
                            {
                                int legCount = 0;
                                for (int s = 0; s < availableJourney.AvailableSegment.Length; s++)
                                {
                                    legCount += availableJourney.AvailableSegment[s].Legs.Length;
                                }
                                if (legCount == 2)
                                {
                                    flightDetails.Status = string.Empty; //Connecting direct flight
                                }
                                else
                                {
                                    flightDetails.Status = "VIA"; //Connecting via flight
                                }
                            }
                            //VIA Flight
                            else if (segment.Legs.Length > 1 && availableJourney.AvailableSegment.Length == 1)
                            {
                                flightDetails.Status = "VIA";
                            }
                            flightDetails.UapiSegmentRefKey = segment.SegmentSellKey;
                            flightDetails.SegmentFareType = GetFareTypeForProductClass(productClass);
                            flightInfo.Add(flightDetails);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return flightInfo;
        }

        /// <summary>
        /// Calculates the pax FareBreakDown from the priceItineraryResponse for both one way and round trip journeys
        /// </summary>
        /// <param name="priceItineraryResponse"></param>
        /// <param name="searchResult"></param>
        /// <param name="searchRequest"></param>
        private void GetPaxFareBreakDown(PriceItineraryResponse priceItineraryResponse, ref SearchResult searchResult, SearchRequest searchRequest)
        {

            List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();


            if (priceItineraryResponse != null && searchResult != null)
            {
                if (priceItineraryResponse.Booking != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.CurrencyCode))
                {
                    rateOfExchange = exchangeRates[priceItineraryResponse.Booking.CurrencyCode];
                }
                int fareBreakDownCount = 0;
                List<string> FarePaxtypes = new List<string>();
                if (searchRequest.AdultCount > 0)
                {
                    fareBreakDownCount = 1;
                    FarePaxtypes.Add("ADT");
                }
                if (searchRequest.ChildCount > 0)
                {
                    fareBreakDownCount++;
                    FarePaxtypes.Add("CHD");
                }
                if (searchRequest.InfantCount > 0)
                {
                    fareBreakDownCount++;
                    FarePaxtypes.Add("INF");
                }
                searchResult.FareBreakdown = new Fare[fareBreakDownCount];

                if (searchRequest.AdultCount > 0)
                {
                    if (searchResult.FareBreakdown[0] == null)
                    {
                        searchResult.FareBreakdown[0] = new Fare();
                    }
                    searchResult.FareBreakdown[0].PassengerCount = searchRequest.AdultCount;
                    searchResult.FareBreakdown[0].PassengerType = PassengerType.Adult;
                }

                if (searchRequest.ChildCount > 0)
                {
                    if (searchResult.FareBreakdown[1] == null)
                    {
                        searchResult.FareBreakdown[1] = new Fare();
                    }
                    searchResult.FareBreakdown[1].PassengerCount = searchRequest.ChildCount;
                    searchResult.FareBreakdown[1].PassengerType = PassengerType.Child;
                }

                if (searchRequest.ChildCount > 0 && searchRequest.InfantCount > 0)
                {
                    if (searchResult.FareBreakdown[2] == null)
                    {
                        searchResult.FareBreakdown[2] = new Fare();
                    }
                    searchResult.FareBreakdown[2].PassengerCount = searchRequest.InfantCount;
                    searchResult.FareBreakdown[2].PassengerType = PassengerType.Infant;
                }

                //Child Count -- If only infant count exists in the rquest.
                else if (searchRequest.ChildCount == 0 && searchRequest.InfantCount > 0)
                {
                    if (searchResult.FareBreakdown[1] == null)
                    {
                        searchResult.FareBreakdown[1] = new Fare();
                    }
                    searchResult.FareBreakdown[1].PassengerCount = searchRequest.InfantCount;
                    searchResult.FareBreakdown[1].PassengerType = PassengerType.Infant;
                }
                searchResult.Price = new PriceAccounts();

                //Need to implement the below logic replacing the for loops
                //Suggested by praveen
                /*var vSegments = priceItineraryResponse.Booking.Journeys.Where(x => x.Segments != null && x.Segments.Length > 0).Select(y => y.Segments);
                var vFares = vSegments.Select(y => y.Where(x => x.Fares != null && x.Fares.Length > 0));
                var vPaxFares = vFares.Select(y => y.Select(x => x.Fares.Where(z => z.PaxFares != null && z.PaxFares.Length > 0)));
                var vBooking = vPaxFares.Select(y => y.Select(x => x.Select(z => z.PaxFares.
                Where((p, q) => p.PaxType == FarePaxtypes[q]))));
                var vServiceCharges = vBooking.Select(y => y.Select(x => x.Select(z => z.Where(a => a.ServiceCharges != null && a.ServiceCharges.Length > 0))));
                if(vServiceCharges != null && vServiceCharges.Count()>0)
                {
                   if(searchRequest.AdultCount >0)
                   {
                      var vServiceChargesADT = vServiceCharges.Select(p => p.Select(x => x.Select(z => z.Where(a => a.PaxType == "ADT"))));


                   }
                   if (searchRequest.ChildCount > 0)
                   {
                       var vServiceChargesCHD = vServiceCharges.Select(p => p.Select(x => x.Select(z => z.Where(a => a.PaxType == "CHD"))));
                   }
               }*/



                if (FarePaxtypes != null && FarePaxtypes.Count > 0)
                {
                    if (priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0)
                    {
                        for (int j = 0; j < priceItineraryResponse.Booking.Journeys.Length; j++)
                        {
                            if (priceItineraryResponse.Booking.Journeys[j].Segments != null && priceItineraryResponse.Booking.Journeys[j].Segments.Length > 0)
                            {
                                for (int s = 0; s < priceItineraryResponse.Booking.Journeys[j].Segments.Length; s++)
                                {
                                    if (priceItineraryResponse.Booking.Journeys[j].Segments[s] != null && priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares != null && priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares.Length > 0)
                                    {
                                        for (int f = 0; f < priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares.Length; f++)
                                        {
                                            if (priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f] != null && priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares != null && priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares.Length > 0)
                                            {
                                                for (int p = 0; p < priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares.Length; p++)
                                                {
                                                    if (priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares[p] != null)
                                                    {
                                                        foreach (string passengerFareType in FarePaxtypes)
                                                        {
                                                            if (priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares[p].PaxType.ToLower() == passengerFareType.ToLower())
                                                            {
                                                                if (priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares[p].ServiceCharges != null && priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares[p].ServiceCharges.Length > 0)
                                                                {
                                                                    foreach (BookingServiceCharge charge in priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares[p].ServiceCharges)
                                                                    {
                                                                        if (charge != null)
                                                                        {
                                                                            switch (passengerFareType)
                                                                            {
                                                                                case "ADT":
                                                                                    if (searchResult.FareBreakdown[0] != null)
                                                                                    {
                                                                                        switch (charge.ChargeType)
                                                                                        {
                                                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                                double baseFareAdult = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[0].PassengerCount);
                                                                                                searchResult.FareBreakdown[0].BaseFare += baseFareAdult;
                                                                                                searchResult.FareBreakdown[0].TotalFare += baseFareAdult;
                                                                                                searchResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[0].PassengerCount);
                                                                                                searchResult.Price.PublishedFare += (decimal)baseFareAdult;
                                                                                                searchResult.Price.SupplierPrice += charge.Amount;
                                                                                                break;

                                                                                            //Deduct discount from the base fare
                                                                                            case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                                                                double discountAdult = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[0].PassengerCount);
                                                                                                searchResult.FareBreakdown[0].BaseFare = searchResult.FareBreakdown[0].BaseFare - discountAdult;
                                                                                                searchResult.FareBreakdown[0].TotalFare = searchResult.FareBreakdown[0].TotalFare - discountAdult;
                                                                                                searchResult.FareBreakdown[0].SupplierFare = searchResult.FareBreakdown[0].SupplierFare - (double)(charge.Amount * searchResult.FareBreakdown[0].PassengerCount);
                                                                                                searchResult.Price.PublishedFare = (decimal)(searchResult.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                                                                                                searchResult.Price.SupplierPrice = searchResult.Price.SupplierPrice - charge.Amount;
                                                                                                break;
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                                                            case SpiceJet.SGBooking.ChargeType.Calculated:
                                                                                            case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                                                            //case SpiceJet.SGBooking.ChargeType.Discount:
                                                                                            case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                                                            case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                            //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                                                            //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                            case SpiceJet.SGBooking.ChargeType.Note:
                                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                                                                double tax = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[0].PassengerCount);
                                                                                                searchResult.FareBreakdown[0].TotalFare += tax;
                                                                                                searchResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[0].PassengerCount);
                                                                                                searchResult.Price.Tax += (decimal)tax;
                                                                                                if (charge.ChargeCode.ToString().Contains("GST"))
                                                                                                {
                                                                                                    searchResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[0].PassengerCount);
                                                                                                }
                                                                                                searchResult.Price.SupplierPrice += charge.Amount;
                                                                                                adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                                                                break;
                                                                                        }
                                                                                    }

                                                                                    break;

                                                                                case "CHD":
                                                                                    if (searchResult.FareBreakdown[1] != null)
                                                                                    {
                                                                                        switch (charge.ChargeType)
                                                                                        {
                                                                                            case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                                                                double baseFareChd = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                                                                                searchResult.FareBreakdown[1].BaseFare += baseFareChd;
                                                                                                searchResult.FareBreakdown[1].TotalFare += baseFareChd;
                                                                                                searchResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[1].PassengerCount);
                                                                                                searchResult.Price.PublishedFare += (decimal)baseFareChd;
                                                                                                searchResult.Price.SupplierPrice += charge.Amount;
                                                                                                break;
                                                                                            case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                                                                //Deduct discount from the base fare
                                                                                                double discountChild = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                                                                                searchResult.FareBreakdown[1].BaseFare = searchResult.FareBreakdown[1].BaseFare - discountChild;
                                                                                                searchResult.FareBreakdown[1].TotalFare = searchResult.FareBreakdown[1].TotalFare - discountChild;
                                                                                                searchResult.FareBreakdown[1].SupplierFare = searchResult.FareBreakdown[1].SupplierFare - (double)(charge.Amount * searchResult.FareBreakdown[1].PassengerCount);
                                                                                                searchResult.Price.PublishedFare = (decimal)(searchResult.Price.PublishedFare - Convert.ToDecimal(discountChild));
                                                                                                searchResult.Price.SupplierPrice = searchResult.Price.SupplierPrice - charge.Amount;
                                                                                                break;
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                                                            case SpiceJet.SGBooking.ChargeType.Calculated:
                                                                                            case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                                                            //case SpiceJet.SGBooking.ChargeType.Discount:
                                                                                            case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                                                            case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                            //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                                                            //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                            case SpiceJet.SGBooking.ChargeType.Note:
                                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                                                                double tax = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                                                                                searchResult.FareBreakdown[1].TotalFare += tax;
                                                                                                searchResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[1].PassengerCount);
                                                                                                searchResult.Price.Tax += (decimal)tax;
                                                                                                if (charge.ChargeCode.ToString().Contains("GST"))
                                                                                                {
                                                                                                    searchResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                                                                                }
                                                                                                searchResult.Price.SupplierPrice += charge.Amount;
                                                                                                chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                                                                break;
                                                                                        }
                                                                                    }
                                                                                    break;

                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            #region Infant Pax Break Down Calculation
                            if (searchRequest.InfantCount > 0 && priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Passengers != null && priceItineraryResponse.Booking.Passengers.Length > 0 && priceItineraryResponse.Booking.Passengers[0] != null && priceItineraryResponse.Booking.Passengers[0].PassengerFees != null && priceItineraryResponse.Booking.Passengers[0].PassengerFees.Length > 0 && priceItineraryResponse.Booking.Passengers[0].PassengerFees[j] != null && priceItineraryResponse.Booking.Passengers[0].PassengerFees[j].ServiceCharges != null && priceItineraryResponse.Booking.Passengers[0].PassengerFees[j] != null && priceItineraryResponse.Booking.Passengers[0].PassengerFees[j].ServiceCharges.Length > 0)
                            {
                                foreach (BookingServiceCharge charge in priceItineraryResponse.Booking.Passengers[0].PassengerFees[j].ServiceCharges)
                                {
                                    if (searchRequest.ChildCount > 0 && searchRequest.InfantCount > 0 && charge != null)
                                    {
                                        if (searchResult.FareBreakdown[2] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[2].PassengerCount);
                                                    searchResult.FareBreakdown[2].TotalFare += baseFare;
                                                    searchResult.FareBreakdown[2].BaseFare += baseFare;
                                                    searchResult.FareBreakdown[2].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[2].PassengerCount);
                                                    searchResult.Price.PublishedFare += (decimal)baseFare;
                                                    searchResult.Price.SupplierPrice += charge.Amount;
                                                    break;
                                                //Deduct discount from the base fare
                                                case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                    double discountInfant = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[2].PassengerCount);
                                                    searchResult.FareBreakdown[2].BaseFare = searchResult.FareBreakdown[2].BaseFare - discountInfant;
                                                    searchResult.FareBreakdown[2].TotalFare = searchResult.FareBreakdown[2].TotalFare - discountInfant;
                                                    searchResult.FareBreakdown[2].SupplierFare = searchResult.FareBreakdown[2].SupplierFare - (double)(charge.Amount * searchResult.FareBreakdown[2].PassengerCount);
                                                    searchResult.Price.PublishedFare = (decimal)(searchResult.Price.PublishedFare - Convert.ToDecimal(discountInfant));
                                                    searchResult.Price.SupplierPrice = searchResult.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                case SpiceJet.SGBooking.ChargeType.Calculated:
                                                case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                //case SpiceJet.SGBooking.ChargeType.Discount:
                                                case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                case SpiceJet.SGBooking.ChargeType.Note:
                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Unmapped:

                                                    double tax = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[2].PassengerCount);
                                                    searchResult.FareBreakdown[2].TotalFare += tax;
                                                    searchResult.FareBreakdown[2].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[2].PassengerCount);
                                                    searchResult.Price.Tax += (decimal)tax;
                                                    searchResult.Price.SupplierPrice += charge.Amount;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                        searchResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[2].PassengerCount);
                                                    }
                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                    else if (searchRequest.ChildCount <= 0 && searchRequest.InfantCount > 0)
                                    {
                                        if (searchResult.FareBreakdown[1] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                    double baseFare = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                                    searchResult.FareBreakdown[1].BaseFare += baseFare;
                                                    searchResult.FareBreakdown[1].TotalFare += baseFare;
                                                    searchResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[1].PassengerCount);
                                                    searchResult.Price.PublishedFare += (decimal)baseFare;
                                                    searchResult.Price.SupplierPrice += charge.Amount;
                                                    break;

                                                case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                    //Deduct discount from the base fare
                                                    double discountInfant = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                                    searchResult.FareBreakdown[1].BaseFare = searchResult.FareBreakdown[1].BaseFare - discountInfant;
                                                    searchResult.FareBreakdown[1].TotalFare = searchResult.FareBreakdown[1].TotalFare - discountInfant;
                                                    searchResult.FareBreakdown[1].SupplierFare = searchResult.FareBreakdown[1].SupplierFare - (double)(charge.Amount * searchResult.FareBreakdown[1].PassengerCount);
                                                    searchResult.Price.PublishedFare = (decimal)(searchResult.Price.PublishedFare - Convert.ToDecimal(discountInfant));
                                                    searchResult.Price.SupplierPrice = searchResult.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                case SpiceJet.SGBooking.ChargeType.Calculated:
                                                case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                //case SpiceJet.SGBooking.ChargeType.Discount:
                                                case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                case SpiceJet.SGBooking.ChargeType.Note:

                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                                    searchResult.FareBreakdown[1].TotalFare += tax;
                                                    searchResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[1].PassengerCount);
                                                    searchResult.Price.Tax += (decimal)tax;
                                                    searchResult.Price.SupplierPrice += charge.Amount;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                        searchResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                                    }
                                                    inftPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }

                                }
                            }
                            #endregion

                        }
                    }
                }
            }

            #region Baggage Calculation
            if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0)
            {
                if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[0] != null && priceItineraryResponse.Booking.Journeys[0].Segments != null && priceItineraryResponse.Booking.Journeys[0].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[0].Segments[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                {
                    searchResult.BaggageIncludedInFare = priceItineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                }
                for (int k = 1; k < searchResult.Flights[0].Length; k++)
                {
                    if (searchResult.Flights[0][k].Status.ToUpper() != "VIA")
                    {
                        if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[0] != null && priceItineraryResponse.Booking.Journeys[0].Segments != null && priceItineraryResponse.Booking.Journeys[0].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[0].Segments[k] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                        {
                            searchResult.BaggageIncludedInFare += "," + priceItineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                        }
                    }
                    else
                    {
                        if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[0] != null && priceItineraryResponse.Booking.Journeys[0].Segments != null && priceItineraryResponse.Booking.Journeys[0].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[0].Segments[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                        {
                            searchResult.BaggageIncludedInFare += "," + priceItineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                        }
                    }
                }
                if (searchRequest.Type == SearchType.Return && !searchRequest.SearchBySegments)
                {
                    if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[1] != null && priceItineraryResponse.Booking.Journeys[1].Segments != null && priceItineraryResponse.Booking.Journeys[1].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[1].Segments[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                    {
                        searchResult.BaggageIncludedInFare += "|" + priceItineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                    }
                    for (int k = 1; k < searchResult.Flights[1].Length; k++)
                    {
                        if (searchResult.Flights[1][k].Status.ToUpper() != "VIA")
                        {
                            if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[1] != null && priceItineraryResponse.Booking.Journeys[1].Segments != null && priceItineraryResponse.Booking.Journeys[1].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[1].Segments[k] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                            {
                                searchResult.BaggageIncludedInFare += "," + priceItineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                            }
                        }
                        else
                        {
                            if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[1] != null && priceItineraryResponse.Booking.Journeys[1].Segments != null && priceItineraryResponse.Booking.Journeys[1].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[1].Segments[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                            {
                                searchResult.BaggageIncludedInFare += "," + priceItineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                            }
                        }
                    }
                }
            }
            #endregion

            if (adtPaxTaxBreakUp.Count > 0)
            {
                paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
            }
            if (chdPaxTaxBreakUp.Count > 0)
            {
                paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
            }
            if (inftPaxTaxBreakUp.Count > 0)
            {
                paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
            }
            if (paxTypeTaxBreakUp.Count > 0)
            {
                searchResult.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
            }

            searchResult.Price.SupplierCurrency = currencyCode;
            searchResult.BaseFare = (double)searchResult.Price.PublishedFare;
            searchResult.Tax = (double)searchResult.Price.Tax;
            searchResult.TotalFare = (double)(Math.Round((searchResult.BaseFare + searchResult.Tax), agentDecimalValue));
        }

        /// <summary>
        /// Gets the Connecting Flight Results in case of Round Trip Journey.
        /// </summary>
        /// <param name="OnwardJourneys"></param>
        /// <param name="ReturnJourneys"></param>
        /// <param name="tripAvailabilityResponseVer2"></param>
        /// <returns></returns>
        public List<SearchResult> GetRoundTripConnectingResults(AvailableJourney OnwardJourneys, AvailableJourney ReturnJourneys, TripAvailabilityResponseVer2 tripAvailabilityResponseVer2, string signature)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {

                if (OnwardJourneys != null && ReturnJourneys != null && OnwardJourneys.AvailableSegment != null && OnwardJourneys.AvailableSegment.Length > 0 && ReturnJourneys.AvailableSegment != null && ReturnJourneys.AvailableSegment.Length > 0 && OnwardJourneys.AvailableSegment.Length == 2 && ReturnJourneys.AvailableSegment.Length == 2)
                {
                    AvailableSegment firstSegmentConOnward = null;//Onward Journey First Connecting Segment
                    AvailableSegment secondSegmentConOnward = null;//Onward Journey Second Connecting Segment

                    AvailableSegment firstSegmentConReturn = null;//Return Journey First Connecting Segment
                    AvailableSegment secondSegmentConReturn = null;//Return Journey First Connecting Segment

                    if (OnwardJourneys.AvailableSegment[0] != null && OnwardJourneys.AvailableSegment[1] != null)
                    {
                        firstSegmentConOnward = OnwardJourneys.AvailableSegment[0];
                        secondSegmentConOnward = OnwardJourneys.AvailableSegment[1];
                    }
                    if (ReturnJourneys.AvailableSegment[0] != null && ReturnJourneys.AvailableSegment[1] != null)
                    {
                        firstSegmentConReturn = ReturnJourneys.AvailableSegment[0];
                        secondSegmentConReturn = ReturnJourneys.AvailableSegment[1];
                    }

                    if (firstSegmentConOnward != null && secondSegmentConOnward != null && firstSegmentConReturn != null && secondSegmentConReturn != null)
                    {
                        if (firstSegmentConOnward.AvailableFares != null && firstSegmentConOnward.AvailableFares.Length > 0 &&
                            secondSegmentConOnward.AvailableFares != null && secondSegmentConOnward.AvailableFares.Length > 0 &&

                            firstSegmentConReturn.AvailableFares != null && firstSegmentConReturn.AvailableFares.Length > 0 &&
                            secondSegmentConReturn.AvailableFares != null && secondSegmentConReturn.AvailableFares.Length > 0 &&

                            firstSegmentConOnward.AvailableFares.Length == secondSegmentConOnward.AvailableFares.Length &&
                            firstSegmentConReturn.AvailableFares.Length == secondSegmentConReturn.AvailableFares.Length)
                        {
                            for (int f = 0; f < firstSegmentConOnward.AvailableFares.Length; f++)
                            {
                                for (int g = 0; g < firstSegmentConReturn.AvailableFares.Length; g++)
                                {
                                    if (firstSegmentConOnward.AvailableFares[f] != null && secondSegmentConOnward.AvailableFares[f] != null && firstSegmentConReturn.AvailableFares[g] != null && secondSegmentConReturn.AvailableFares[g] != null)
                                    {
                                        SpiceJet.SGBooking.Fare fareFirstConSegOnWard = tripAvailabilityResponseVer2.Fares[OnwardJourneys.AvailableSegment[0].AvailableFares[f].FareIndex];
                                        SpiceJet.SGBooking.Fare fareSecondConSegOnward = tripAvailabilityResponseVer2.Fares[OnwardJourneys.AvailableSegment[1].AvailableFares[f].FareIndex];

                                        SpiceJet.SGBooking.Fare fareFirstConSegReturn = tripAvailabilityResponseVer2.Fares[ReturnJourneys.AvailableSegment[0].AvailableFares[g].FareIndex];
                                        SpiceJet.SGBooking.Fare fareSecondConSegReturn = tripAvailabilityResponseVer2.Fares[ReturnJourneys.AvailableSegment[1].AvailableFares[g].FareIndex];

                                        SearchResult result = new SearchResult();
                                        result.IsLCC = true;
                                        //result.ResultBookingSource = BookingSource.SpiceJet;
                                        if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                                        {
                                            result.ResultBookingSource = BookingSource.SpiceJet;
                                        }
                                        else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                                        {
                                            result.ResultBookingSource = BookingSource.SpiceJetCorp;
                                        }
                                        result.Airline = "SG";
                                        result.Currency = agentBaseCurrency;
                                        result.EticketEligible = true;
                                        result.NonRefundable = false;
                                        result.RepriceErrorMessage = signature;//Till Search To Book only signature should be carried.

                                        result.FareRules = new List<FareRule>();
                                        FareRule fareRule = new FareRule();
                                        fareRule.Airline = result.Airline;
                                        fareRule.Destination = request.Segments[0].Destination;
                                        fareRule.FareBasisCode = fareFirstConSegOnWard.FareBasisCode;
                                        fareRule.FareInfoRef = fareFirstConSegOnWard.RuleNumber;
                                        fareRule.Origin = request.Segments[0].Origin;
                                        fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                        fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                        result.FareRules.Add(fareRule);

                                        result.JourneySellKey = OnwardJourneys.JourneySellKey;
                                        result.FareSellKey = fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey;
                                        result.FareType = GetFareTypeForProductClass(fareFirstConSegOnWard.ProductClass);

                                        result.Flights = new FlightInfo[2][];

                                        List<FlightInfo> firstSegmentConFlightsO = GetFlightInfo(firstSegmentConOnward, OnwardJourneys, fareFirstConSegOnWard.ProductClass, "ONWARD");
                                        List<FlightInfo> secondSegmentConFlightsO = GetFlightInfo(secondSegmentConOnward, OnwardJourneys, fareSecondConSegOnward.ProductClass, "ONWARD");
                                        if (firstSegmentConFlightsO != null && secondSegmentConFlightsO != null && firstSegmentConFlightsO.Count > 0 && secondSegmentConFlightsO.Count > 0)
                                        {
                                            List<FlightInfo> listOfFlightsO = new List<FlightInfo>();
                                            listOfFlightsO.AddRange(firstSegmentConFlightsO);
                                            listOfFlightsO.AddRange(secondSegmentConFlightsO);
                                            result.Flights[0] = listOfFlightsO.ToArray();
                                        }

                                        result.JourneySellKey += "|" + ReturnJourneys.JourneySellKey;
                                        result.FareType += "," + GetFareTypeForProductClass(fareFirstConSegReturn.ProductClass);
                                        result.FareSellKey += "|" + fareFirstConSegReturn.FareSellKey + "^" + fareSecondConSegReturn.FareSellKey;

                                        List<FlightInfo> firstSegmentConFlightsR = GetFlightInfo(firstSegmentConReturn, ReturnJourneys, fareFirstConSegReturn.ProductClass, "RETURN");
                                        List<FlightInfo> secondSegmentConFlightsR = GetFlightInfo(secondSegmentConReturn, ReturnJourneys, fareSecondConSegReturn.ProductClass, "RETURN");
                                        if (firstSegmentConFlightsR != null && secondSegmentConFlightsR != null && firstSegmentConFlightsR.Count > 0 && secondSegmentConFlightsR.Count > 0)
                                        {
                                            List<FlightInfo> listOfFlightsR = new List<FlightInfo>();
                                            listOfFlightsR.AddRange(firstSegmentConFlightsR);
                                            listOfFlightsR.AddRange(secondSegmentConFlightsR);
                                            result.Flights[1] = listOfFlightsR.ToArray();
                                        }
                                        if (result.Flights[0] != null && result.Flights[1] != null && result.Flights[0].Length > 0 && result.Flights[1].Length > 0)
                                        {
                                            if (request.InfantCount > 0)
                                            {
                                                string logFileName = result.Flights[0][0].FlightNumber + "_" + result.FareType.Split(',')[0].Replace(" ", "_") + "_" + Convert.ToString(result.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                logFileName += "_" + Convert.ToString(result.Flights[0][result.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                logFileName += "_" + result.Flights[1][0].FlightNumber + "_" + result.FareType.Split(',')[1].Replace(" ", "_") + "_" + Convert.ToString(result.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                logFileName += "_" + Convert.ToString(result.Flights[1][result.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                                PriceItineraryResponse itineraryResponse = GetItineraryPrice(request, OnwardJourneys.JourneySellKey, fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey, OnwardJourneys, logFileName, fareFirstConSegReturn.FareSellKey + "^" + fareSecondConSegReturn.FareSellKey, ReturnJourneys.JourneySellKey, ReturnJourneys);
                                                if (itineraryResponse != null)
                                                {
                                                    GetPaxFareBreakDown(itineraryResponse, ref result, request);
                                                }
                                            }
                                            else
                                            {
                                                GetPaxFareBreakDown(fareFirstConSegOnWard, ref result, request);
                                                GetPaxFareBreakDown(fareSecondConSegOnward, ref result, request);
                                                GetPaxFareBreakDown(fareFirstConSegReturn, ref result, request);
                                                GetPaxFareBreakDown(fareSecondConSegReturn, ref result, request);
                                            }
                                        }
                                        ResultList.Add(result);
                                    }
                                }
                            }
                        }

                    }

                }

            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Search, Severity.High, 0, "(SpiceJet)Failed to get return Combination results." + ex.ToString(), "");
            }
            return ResultList;
        }

        /// <summary>
        /// Gets the Onward Direct flights in case of One Way trip
        /// </summary>
        /// <param name="OnwardJourney"></param>
        /// <param name="OnwardFare"></param>
        /// <param name="tripAvailabilityResponseVer2"></param>
        private void GetOnwardDirectFlights(AvailableJourney OnwardJourney, SpiceJet.SGBooking.AvailableFare2 OnwardFare, TripAvailabilityResponseVer2 tripAvailabilityResponseVer2)
        {
            try
            {
                if (OnwardJourney != null && OnwardFare != null)
                {
                    SpiceJet.SGBooking.Fare availableOnwardFare = tripAvailabilityResponseVer2.Fares[OnwardFare.FareIndex];

                    SearchResult result = new SearchResult();
                    result.IsLCC = true;
                    //result.ResultBookingSource = BookingSource.SpiceJet;
                    if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                    {
                        result.ResultBookingSource = BookingSource.SpiceJet;
                    }
                    else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                    {
                        result.ResultBookingSource = BookingSource.SpiceJetCorp;
                    }
                    result.Airline = "SG";
                    result.Currency = agentBaseCurrency;
                    result.EticketEligible = true;

                    result.FareRules = new List<FareRule>();
                    FareRule fareRule = new FareRule();
                    fareRule.Airline = result.Airline;
                    fareRule.Destination = request.Segments[0].Destination;
                    fareRule.FareBasisCode = availableOnwardFare.FareBasisCode;
                    fareRule.FareInfoRef = availableOnwardFare.RuleNumber;
                    fareRule.Origin = request.Segments[0].Origin;
                    fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                    fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                    result.FareRules.Add(fareRule);

                    result.JourneySellKey = OnwardJourney.JourneySellKey;
                    result.FareType = GetFareTypeForProductClass(availableOnwardFare.ProductClass);
                    result.FareSellKey = availableOnwardFare.FareSellKey;
                    result.NonRefundable = false;
                    result.Flights = new FlightInfo[1][];
                    List<FlightInfo> directFlights = GetFlightInfo(OnwardJourney.AvailableSegment[0], OnwardJourney, availableOnwardFare.ProductClass, "ONWARD");
                    if (directFlights != null && directFlights.Count > 0)
                    {
                        result.Flights[0] = directFlights.ToArray();
                    }
                    if (result.Flights[0] != null && result.Flights[0].Length > 0 && !string.IsNullOrEmpty(result.JourneySellKey) && !string.IsNullOrEmpty(result.FareSellKey))
                    {
                        if (request.InfantCount > 0)
                        {
                            string logFileName = result.Flights[0][0].FlightNumber + "_" + result.FareType.Replace(" ", "_") + "_" + Convert.ToString(result.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            PriceItineraryResponse itineraryResponse = GetItineraryPrice(request, OnwardJourney.JourneySellKey, availableOnwardFare.FareSellKey, OnwardJourney, logFileName);
                            GetPaxFareBreakDown(itineraryResponse, ref result, request);
                        }
                        else
                        {
                            GetPaxFareBreakDown(availableOnwardFare, ref result, request);
                        }
                    }
                    oneWayResultList.Add(result);
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Search, Severity.High, 0, "(Spicejet)Failed to get onward direct flight results." + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Gets the Connecting Flights in case of OneWay Trip
        /// </summary>
        /// <param name="journeyOnWard"></param>
        /// <param name="tripAvailabilityResponseVer2"></param>
        private void GetOnwardConnectingFlights(AvailableJourney journeyOnWard, TripAvailabilityResponseVer2 tripAvailabilityResponseVer2)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {
                if (journeyOnWard != null && journeyOnWard.AvailableSegment != null && journeyOnWard.AvailableSegment.Length > 0 && journeyOnWard.AvailableSegment.Length == 2)
                {
                    /*Fare mapping on segments based on One to One relationship. 
                     *For ex. combine 1st Fare of the 1st Segment with the 1st Fare of the 2nd Segment, 
                     *The 2nd Fare of the 1st Segment with the 2nd Fare of the 2nd Segment and so on (if there are more fares under each Segment).             
                     */
                    AvailableSegment firstSegmentCon = null;
                    AvailableSegment secondSegmentCon = null;
                    if (journeyOnWard.AvailableSegment[0] != null && journeyOnWard.AvailableSegment[1] != null)
                    {
                        firstSegmentCon = journeyOnWard.AvailableSegment[0];
                        secondSegmentCon = journeyOnWard.AvailableSegment[1];
                    }
                    if (firstSegmentCon != null && secondSegmentCon != null && firstSegmentCon.AvailableFares != null && secondSegmentCon.AvailableFares != null &&
                        firstSegmentCon.AvailableFares.Length > 0 && secondSegmentCon.AvailableFares.Length > 0 && (firstSegmentCon.AvailableFares.Length == secondSegmentCon.AvailableFares.Length))
                    {
                        for (int f = 0; f < firstSegmentCon.AvailableFares.Length; f++)
                        {
                            if (firstSegmentCon.AvailableFares[f] != null && secondSegmentCon.AvailableFares[f] != null)
                            {
                                SpiceJet.SGBooking.Fare fareFirstConSegOnWard = tripAvailabilityResponseVer2.Fares[firstSegmentCon.AvailableFares[f].FareIndex];
                                SpiceJet.SGBooking.Fare fareSecondConSegOnward = tripAvailabilityResponseVer2.Fares[secondSegmentCon.AvailableFares[f].FareIndex];

                                SearchResult result = new SearchResult();
                                result.IsLCC = true;
                                //result.ResultBookingSource = BookingSource.SpiceJet;
                                if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                                {
                                    result.ResultBookingSource = BookingSource.SpiceJet;
                                }
                                else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                                {
                                    result.ResultBookingSource = BookingSource.SpiceJetCorp;
                                }
                                result.Airline = "SG";
                                result.Currency = agentBaseCurrency;
                                result.EticketEligible = true;

                                result.FareRules = new List<FareRule>();
                                FareRule fareRule = new FareRule();
                                fareRule.Airline = result.Airline;
                                fareRule.Destination = request.Segments[0].Destination;
                                fareRule.FareBasisCode = fareFirstConSegOnWard.FareBasisCode;
                                fareRule.FareInfoRef = fareFirstConSegOnWard.RuleNumber;
                                fareRule.Origin = request.Segments[0].Origin;
                                fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                result.FareRules.Add(fareRule);

                                result.JourneySellKey = journeyOnWard.JourneySellKey;
                                result.FareType = GetFareTypeForProductClass(fareFirstConSegOnWard.ProductClass);
                                result.FareSellKey = fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey;
                                result.NonRefundable = false;
                                result.Flights = new FlightInfo[1][];

                                List<FlightInfo> firstSegmentConFlights = GetFlightInfo(firstSegmentCon, journeyOnWard, fareFirstConSegOnWard.ProductClass, "ONWARD");
                                List<FlightInfo> secondSegmentConFlights = GetFlightInfo(secondSegmentCon, journeyOnWard, fareSecondConSegOnward.ProductClass, "ONWARD");
                                if (firstSegmentConFlights != null && secondSegmentConFlights != null && firstSegmentConFlights.Count > 0 && secondSegmentConFlights.Count > 0)
                                {
                                    List<FlightInfo> listOfFlights = new List<FlightInfo>();
                                    listOfFlights.AddRange(firstSegmentConFlights);
                                    listOfFlights.AddRange(secondSegmentConFlights);
                                    result.Flights[0] = listOfFlights.ToArray();
                                }

                                if (result.Flights[0] != null && result.Flights[0].Length > 0) //Onward Journey Connecting or Via Flights
                                {
                                    string logFileName = string.Empty;
                                    for (int m = 0; m < result.Flights[0].Length; m++)
                                    {
                                        if (result.Flights[0][m].Status.ToUpper() != "VIA")
                                        {
                                            if (!string.IsNullOrEmpty(logFileName))
                                            {
                                                logFileName += "_" + result.Flights[0][1].FlightNumber + "_" + result.FareType + "_" + Convert.ToString(result.Flights[0][1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            }
                                            else
                                            {
                                                logFileName = result.Flights[0][m].FlightNumber + "_" + result.FareType.Replace(" ", "_") + "_" + Convert.ToString(result.Flights[0][m].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            }
                                        }
                                        else
                                        {
                                            logFileName = result.Flights[0][0].FlightNumber + "_" + result.FareType.Replace(" ", "_") + "_" + Convert.ToString(result.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                        }
                                    }
                                    if (request.InfantCount > 0)
                                    {
                                        PriceItineraryResponse itineraryResponse = GetItineraryPrice(request, journeyOnWard.JourneySellKey, fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey, journeyOnWard, logFileName);
                                        if (itineraryResponse != null)
                                        {
                                            GetPaxFareBreakDown(itineraryResponse, ref result, request);
                                        }
                                    }
                                    else
                                    {
                                        GetPaxFareBreakDown(fareFirstConSegOnWard, ref result, request);
                                        GetPaxFareBreakDown(fareSecondConSegOnward, ref result, request);
                                    }
                                }
                                ResultList.Add(result);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception("SpiceJet failed to get Connecting Flights Results.Reason : " + ex.Message, ex);
            }
            if (ResultList.Count > 0)
            {
                oneWayResultList.AddRange(ResultList);
            }
        }

        public List<FareRule> GetFareRules(SearchRequest request, SearchResult result)
        {
            List<FareRule> fareRules = new List<FareRule>();

            //S-1:Login
            //S-2:Search Request
            //S-3:Fare Rule Request
            //S-4: LogOut
            try
            {
                //S-1:Login
                LogonResponse loginResponse = Login();
                if (loginResponse != null && loginResponse.Signature.Length > 0)
                {
                    //S-2:Search Request
                    if (request.Type == SearchType.Return)
                    {
                        GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                        availRequest.ContractVersion = contractVersion;
                        availRequest.Signature = loginResponse.Signature;
                        availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                        for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "SG";
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                            
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 20;

                            int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;

                            if (request.Segments[0].flightCabinClass == CabinClass.Business)
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "BC";
                            }
                            else
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "";
                            }

                            List<string> sgFareTypes = new List<string>();
                            if (request.Segments[0].flightCabinClass != CabinClass.Business)//ECONOMY + ALL
                            {
                                sgFareTypes.Add("R");//Regular Fare.
                                                     // w.e.f. 6th September 2019, criteria of Friends and Family fares to 
                                                     //Minimum 2 passenger and Maximum 9 passenger, 
                                if ((request.AdultCount + request.ChildCount >= 2) && (request.AdultCount + request.ChildCount <= 9))
                                {
                                    sgFareTypes.Add("F");//Family Fare.
                                }
                                if (request.AdultCount + request.ChildCount < 4)
                                {
                                    sgFareTypes.Add("T");//Coupon Fares
                                    sgFareTypes.Add("S");//Fare difference
                                }
                                sgFareTypes.Add("HB");//Hand Baggage.
                                sgFareTypes.Add("IO");//Advance Purchase 
                                sgFareTypes.Add("MX");//Spice Flex

                            }
                            //BUSINESS CLASS + ALL
                            if (request.Segments[0].flightCabinClass == CabinClass.Business || request.Segments[0].flightCabinClass == CabinClass.All)
                            {
                                sgFareTypes.Add("BC");//Business class.
                            }
                            if (!string.IsNullOrEmpty(promoCode))
                            {
                                sgFareTypes.Add("C");//Corporate Fare
                            }
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[sgFareTypes.Count()];
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = sgFareTypes.ToArray();

                            //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[4];
                            //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                            //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "MX";
                            //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "IO";
                            //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[3] = "HB";

                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                            for (int a = 0; a < request.AdultCount; a++)
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                            }
                            if (request.ChildCount > 0)
                            {
                                for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                                {
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                                }
                            }
                            if (request.InfantCount > 0)
                            {
                                for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                                {
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                                }
                            }
                        }
                        availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                        availRequest.EnableExceptionStackTrace = false;
                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGRoundTripSearchRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, availRequest);
                            sw.Close();
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                        }

                        GetAvailabilityVer2Response getAvailabilityVer2Response = bookingAPI.GetAvailabilityVer2(availRequest);
                        TripAvailabilityResponseVer2 tripAvailabilityResponseVer2 = getAvailabilityVer2Response.GetTripAvailabilityVer2Response;

                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityVer2Response));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGRoundTripSearchResponseVer2_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, getAvailabilityVer2Response);
                            sw.Close();
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Response Ver2. Reason : " + ex.ToString(), "");
                        }
                    }

                    else if (request.Type == SearchType.OneWay)
                    {
                        GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                        GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                        availRequest.ContractVersion = contractVersion;
                        availRequest.Signature = loginResponse.Signature;
                        availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                        for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "SG";
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 20;

                            if (request.Segments[0].flightCabinClass == CabinClass.Business)
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "BC";
                            }
                            else
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "";
                            }

                            List<string> sgFareTypes = new List<string>();
                            if (request.Segments[0].flightCabinClass != CabinClass.Business)//ECONOMY + ALL
                            {
                                sgFareTypes.Add("R");//Regular Fare.
                                                     // w.e.f. 6th September 2019, criteria of Friends and Family fares to 
                                                     //Minimum 2 passenger and Maximum 9 passenger, 
                                if ((request.AdultCount + request.ChildCount >= 2) && (request.AdultCount + request.ChildCount <= 9))
                                {
                                    sgFareTypes.Add("F");//Family Fare.
                                }
                                if (request.AdultCount + request.ChildCount < 4)
                                {
                                    sgFareTypes.Add("T");//Coupon Fares
                                    sgFareTypes.Add("S");//Fare difference
                                }
                                sgFareTypes.Add("HB");//Hand Baggage.
                                sgFareTypes.Add("IO");//Advance Purchase 
                                sgFareTypes.Add("MX");//Spice Flex

                            }
                            //BUSINESS CLASS + ALL
                            if (request.Segments[0].flightCabinClass == CabinClass.Business || request.Segments[0].flightCabinClass == CabinClass.All)
                            {
                                sgFareTypes.Add("BC");//Business class.
                            }
                            if (!string.IsNullOrEmpty(promoCode))
                            {
                                sgFareTypes.Add("C");//Corporate Fare
                            }
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[sgFareTypes.Count()];
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = sgFareTypes.ToArray();


                            //Different fare types:
                            //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[4];
                            //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";
                            //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "MX";
                            //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "IO";
                            //availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[3] = "HB";
                            int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];
                            for (int a = 0; a < request.AdultCount; a++)
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                            }
                            if (request.ChildCount > 0)
                            {
                                for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                                {
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                                }
                            }
                            if (request.InfantCount > 0)
                            {
                                for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                                {
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                                }
                            }
                        }
                        availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                        availRequest.EnableExceptionStackTrace = false;
                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGGetAvailabilityVer2Request_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, availRequest);
                            sw.Close();
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Request. Reason : " + ex.ToString(), "");
                        }

                        GetAvailabilityVer2Response getAvailabilityVer2Response = bookingAPI.GetAvailabilityVer2(availRequest);
                        TripAvailabilityResponseVer2 tripAvailabilityResponseVer2 = getAvailabilityVer2Response.GetTripAvailabilityVer2Response;

                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityVer2Response));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGGetAvailabilityVer2Response_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, getAvailabilityVer2Response);
                            sw.Close();
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), "");
                        }
                    }

                    //S-3:Fare Rule Request
                    GetFareRuleInfoRequest fareRuleInfoRequest = new GetFareRuleInfoRequest();
                    fareRuleInfoRequest.ContractVersion = contractVersion;
                    fareRuleInfoRequest.Signature = loginResponse.Signature;
                    fareRuleInfoRequest.EnableExceptionStackTrace = false;

                    FareRuleRequestData fareRuleRequestData = new FareRuleRequestData();
                    fareRuleRequestData.CultureCode = "en-GB";
                    fareRuleRequestData.CarrierCode = "SG";
                    fareRuleRequestData.ClassOfService = result.Flights[0][0].BookingClass;
                    fareRuleRequestData.FareBasisCode = result.FareRules[0].FareBasisCode;
                    fareRuleRequestData.RuleNumber = result.FareRules[0].FareInfoRef;
                    fareRuleInfoRequest.fareRuleReqData = fareRuleRequestData;
                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(GetFareRuleInfoRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGFareRuleInfoRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, fareRuleInfoRequest);
                        sw.Close();
                    }
                    catch { }
                    GetFareRuleInfoResponse fareRuleInfoResponse = contentAPI.GetFareRuleInfo(fareRuleInfoRequest);
                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(GetFareRuleInfoResponse));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGFareRuleInfoResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, fareRuleInfoResponse);
                        sw.Close();
                    }
                    catch { }

                    if (fareRuleInfoResponse != null)
                    {
                        byte[] encodedBytes = fareRuleInfoResponse.FareRuleInfo.Data;
                        string decodedText = System.Text.Encoding.UTF8.GetString(encodedBytes);
                        //To Get the text in plain format.
                        System.Windows.Forms.RichTextBox rtBox = new System.Windows.Forms.RichTextBox();
                        rtBox.Rtf = decodedText;
                        string plainText = rtBox.Text;
                        FareRule DNfareRules = new FareRule();
                        DNfareRules.Origin = result.Flights[0][0].Origin.CityCode;
                        DNfareRules.Destination = result.Flights[0][0].Destination.CityCode;
                        DNfareRules.Airline = "SG";
                        DNfareRules.FareRuleDetail = plainText;
                        DNfareRules.FareBasisCode = result.FareRules[0].FareBasisCode;
                        fareRules.Add(DNfareRules);
                    }
                    //S-4: LogOut
                    //Logout(loginResponse.Signature);
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.SpiceJet, Severity.High, 1, "(SpiceJet)Failed to get Fare Rules.Reason" + ex.ToString(), "");
            }

            return fareRules;
        }

        /****Below are the important points with respect to GST implementation****/
        //1.GST Implementation.
        //2.All carriers travelling to/ from /within india be able to apply the correct GST charges.
        //3.GST Registration Number ,Name of GST holder, Email of GST holder.
        //4.If PNR has multiple passengers only 1 GST Registration Number is allowed per PNR
        //5.In case of B2B transactions,2 new method calls will be used in the booking flow.
        //6.Methods: UpdateContactRequest and GetBookingFromState.

        /// <summary>
        /// Updates the GST details for the lead passenger.
        /// </summary>
        /// <returns></returns>
        public UpdateContactsResponse UpdateContactGSTDetails(string signature)
        {
            UpdateContactsResponse updateContactsResponse = new UpdateContactsResponse();

            try
            {
                UpdateContactsRequest updateContactsRequest = new UpdateContactsRequest();

                updateContactsRequest.ContractVersion = contractVersion;
                updateContactsRequest.Signature = signature;

                UpdateContactsRequestData updateContactsRequestData = new UpdateContactsRequestData();
                updateContactsRequestData.BookingContactList = new BookingContact[1];
                updateContactsRequestData.BookingContactList[0] = new BookingContact();
                updateContactsRequestData.BookingContactList[0].State = SpiceJet.SGBooking.MessageState.New;
                updateContactsRequestData.BookingContactList[0].TypeCode = "G";
                updateContactsRequestData.BookingContactList[0].EmailAddress = gstOfficialEmail;
                updateContactsRequestData.BookingContactList[0].CustomerNumber = gstNumber;
                updateContactsRequestData.BookingContactList[0].CompanyName = gstCompanyName;
                updateContactsRequestData.BookingContactList[0].DistributionOption = DistributionOption.None;
                updateContactsRequestData.BookingContactList[0].NotificationPreference = NotificationPreference.None;
                updateContactsRequest.updateContactsRequestData = updateContactsRequestData;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(UpdateContactsRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGLeadPaxGSTRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, updateContactsRequest);
                    sw.Close();
                }
                catch { }
                updateContactsResponse = bookingAPI.UpdateContacts(updateContactsRequest);
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(UpdateContactsResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGLeadPaxGSTResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, updateContactsResponse);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "(SpiceJet)Failed to update GST Details for the lead passenger. Error : " + ex.ToString(), "");
                throw ex;
            }
            return updateContactsResponse;
        }

        /// <summary>
        /// To Get the bifurcation of Taxes after GSTN update.
        /// </summary>
        /// <returns></returns>
        public GetBookingFromStateResponse GetBookingFromState(string signature)
        {
            GetBookingFromStateResponse getBookingFromStateResponse = new GetBookingFromStateResponse();
            try
            {
                GetBookingFromStateRequest getBookingFromStateRequest = new GetBookingFromStateRequest();
                getBookingFromStateRequest.ContractVersion = contractVersion;
                getBookingFromStateRequest.Signature = signature;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingFromStateRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SG_GST_Tax_Breakup_Request_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, getBookingFromStateRequest);
                    sw.Close();
                }
                catch { }
                getBookingFromStateResponse = bookingAPI.GetBookingFromState(getBookingFromStateRequest);
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingFromStateResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SG_GST_Tax_Breakup_Response_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, getBookingFromStateResponse);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "(SpiceJet)Failed to get updated GST break up for the lead passenger. Error : " + ex.ToString(), "");
                throw ex;
            }
            return getBookingFromStateResponse;
        }


        ///<summary>
        /// Holds the selected itinerary
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        //public void GetUpdatedGSTFareBreakUp(ref FlightItinerary itinerary)
        //{
        //    try
        //    {
        //        //If the lead pax supplies the GST input.
        //        //Then Get the updated GST farebreak up.
        //        LogonResponse loginResponse = Login();
        //        if (itinerary != null && itinerary.Passenger != null && itinerary.Passenger.Length > 0 && !string.IsNullOrEmpty(loginResponse.Signature))
        //        {
        //            if (itinerary.Passenger[0] != null && itinerary.Passenger[0].IsLeadPax && !string.IsNullOrEmpty(itinerary.Passenger[0].GSTTaxRegNo))
        //            {
        //                GetBookingFromStateResponse updatedGSTBreakup = GetBookingFromState(loginResponse.Signature);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Audit.Add(EventType.Exception, Severity.High, 1, "(SpiceJet)Failed to GetUpdatedGSTFareBreakUp . Error : " + ex.ToString(), "");
        //        throw ex;
        //    }
        //}
        public void GetUpdatedGSTFareBreakUp(ref SearchResult result, SearchRequest request) // Added by Ziyad 22-nov-2018
        {
            //Note1:For Normal Search Signature will be assigned from the basket from the user id.
            //Note2:For One-One Search Signature Will be assigned from the itinerary object [endorsement property]
            try
            {
                //If the lead pax supplies the GST input.
                //Then Get the updated GST farebreak up.
                LogonResponse logonResponse = null;
                if (!string.IsNullOrEmpty(result.RepriceErrorMessage))
                {
                    logonResponse = new LogonResponse();
                    logonResponse.Signature = result.RepriceErrorMessage;
                }
                else
                {
                    logonResponse = Login();
                }

                //Get the bifurcation of taxes after GST input.
                if (logonResponse != null && logonResponse.Signature.Length > 0)
                {
                    GetBookingFromStateResponse updatedGSTBreakup = GetBookingFromState(logonResponse.Signature);
                    if (updatedGSTBreakup != null && updatedGSTBreakup.BookingData != null && updatedGSTBreakup.BookingData.Journeys != null && updatedGSTBreakup.BookingData.Journeys.Length > 0)
                    {
                        rateOfExchange = exchangeRates[updatedGSTBreakup.BookingData.CurrencyCode];

                        List<PaxFare> adultPaxfare = new List<PaxFare>();
                        List<PaxFare> childPaxfare = new List<PaxFare>();


                        List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
                        List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

                        paxTypeTaxBreakUp.Clear();
                        adtPaxTaxBreakUp.Clear();
                        chdPaxTaxBreakUp.Clear();
                        inftPaxTaxBreakUp.Clear();

                        for (int journey = 0; journey < updatedGSTBreakup.BookingData.Journeys.Length; journey++)
                        {
                            if (updatedGSTBreakup.BookingData.Journeys[journey] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length > 0)
                            {
                                for (int segment = 0; segment < updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length; segment++)
                                {
                                    if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length > 0)
                                    {
                                        for (int fare = 0; fare < updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length; fare++)
                                        {
                                            if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).Count() > 0)
                                            {
                                                adultPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).FirstOrDefault());
                                            }
                                            if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).Count() > 0)
                                            {
                                                childPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).FirstOrDefault());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // For infant updating from intial result fare
                        Fare infantFare = result.FareBreakdown.FirstOrDefault(P => (P.PassengerType == PassengerType.Infant));
                        result.Price = new PriceAccounts();
                        result.BaseFare = 0;
                        result.Tax = 0;
                        result.TotalFare = 0;
                        result.FareBreakdown = null;
                        int fareBreakDownCount = 0;
                        if (request.AdultCount > 0)
                        {
                            fareBreakDownCount = 1;
                        }
                        if (request.ChildCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        if (request.InfantCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        result.FareBreakdown = new Fare[fareBreakDownCount];
                        if (request.AdultCount > 0)
                        {
                            result.FareBreakdown[0] = new Fare();
                            result.FareBreakdown[0].PassengerCount = request.AdultCount;
                            result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                        }
                        if (request.ChildCount > 0)
                        {
                            result.FareBreakdown[1] = new Fare();
                            result.FareBreakdown[1].PassengerCount = request.ChildCount;
                            result.FareBreakdown[1].PassengerType = PassengerType.Child;
                        }
                        //if (request.ChildCount > 0 && request.InfantCount > 0)
                        //{
                        //    result.FareBreakdown[2] = new Fare();
                        //    result.FareBreakdown[2].PassengerCount = request.InfantCount;
                        //    result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                        //}
                        //else if (request.ChildCount == 0 && request.InfantCount > 0)
                        //{
                        //    result.FareBreakdown[1] = new Fare();
                        //    result.FareBreakdown[1].PassengerCount = request.InfantCount;
                        //    result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                        //}
                        if (adultPaxfare != null && adultPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareAdult in adultPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareAdult.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (result.FareBreakdown[0] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                    double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                    result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                    result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                    result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                    result.Price.PublishedFare += (decimal)baseFareAdult;
                                                    result.Price.SupplierPrice += charge.Amount;
                                                    break;

                                                case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                    double discountAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                    result.FareBreakdown[0].BaseFare = result.FareBreakdown[0].BaseFare - discountAdult;
                                                    result.FareBreakdown[0].TotalFare = result.FareBreakdown[0].TotalFare - discountAdult;
                                                    result.FareBreakdown[0].SupplierFare = result.FareBreakdown[0].SupplierFare - (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                    result.Price.PublishedFare = (decimal)(result.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                                                    result.Price.SupplierPrice = result.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                case SpiceJet.SGBooking.ChargeType.Calculated:
                                                case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                //case SpiceJet.SGBooking.ChargeType.Discount:
                                                case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                case SpiceJet.SGBooking.ChargeType.Note:
                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                    result.FareBreakdown[0].TotalFare += tax;
                                                    result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                    result.Price.Tax += (decimal)tax;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                         result.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                    }
                                                    result.Price.SupplierPrice += charge.Amount;
                                                    adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (childPaxfare != null && childPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareChild in childPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareChild.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (result.FareBreakdown[1] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                    double baseFareChild = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                    result.FareBreakdown[1].BaseFare += baseFareChild;
                                                    result.FareBreakdown[1].TotalFare += baseFareChild;
                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                    result.Price.PublishedFare += (decimal)baseFareChild;
                                                    result.Price.SupplierPrice += charge.Amount;
                                                    break;

                                                case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                    double discountChild = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                    result.FareBreakdown[1].BaseFare = result.FareBreakdown[1].BaseFare - discountChild;
                                                    result.FareBreakdown[1].TotalFare = result.FareBreakdown[1].TotalFare - discountChild;
                                                    result.FareBreakdown[1].SupplierFare = result.FareBreakdown[0].SupplierFare - (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                    result.Price.PublishedFare = (decimal)(result.Price.PublishedFare - Convert.ToDecimal(discountChild));
                                                    result.Price.SupplierPrice = result.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                case SpiceJet.SGBooking.ChargeType.Calculated:
                                                case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                //case SpiceJet.SGBooking.ChargeType.Discount:
                                                case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                case SpiceJet.SGBooking.ChargeType.Note:
                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                    result.FareBreakdown[1].TotalFare += tax;
                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                    result.Price.Tax += (decimal)tax;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                        result.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                    }
                                                    result.Price.SupplierPrice += charge.Amount;
                                                    chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (request.InfantCount > 0)
                        {

                            if (infantFare != null)
                            {

                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                {
                                    result.FareBreakdown[2] = infantFare;
                                }
                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                {

                                    result.FareBreakdown[1] = infantFare;

                                }

                                result.Price.PublishedFare += (decimal)infantFare.BaseFare;
                                result.Price.Tax += (decimal)infantFare.Tax;
                                result.Price.SupplierPrice += (decimal)infantFare.SupplierFare;

                            }
                            //for (int journey = 0; journey < updatedGSTBreakup.BookingData.Journeys.Length; journey++)
                            //{

                            //    decimal infantPrice = 0;
                            //   // infantPrice = GetInfantAvailibilityFares(request);
                            //    if (request.ChildCount > 0 && request.InfantCount > 0)
                            //    {
                            //        double baseFare = (double)((infantPrice * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                            //        result.FareBreakdown[2].TotalFare += baseFare;
                            //        result.FareBreakdown[2].BaseFare += baseFare;
                            //        result.FareBreakdown[2].SupplierFare += (double)(infantPrice * result.FareBreakdown[2].PassengerCount);
                            //        result.Price.PublishedFare += (decimal)baseFare;
                            //        result.Price.SupplierPrice += infantPrice;
                            //    }
                            //    else if (request.ChildCount <= 0 && request.InfantCount > 0)
                            //    {
                            //        double baseFare = (double)((infantPrice * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                            //        result.FareBreakdown[1].BaseFare += baseFare;
                            //        result.FareBreakdown[1].TotalFare += baseFare;
                            //        result.FareBreakdown[1].SupplierFare += (double)(infantPrice * result.FareBreakdown[1].PassengerCount);
                            //        result.Price.PublishedFare += (decimal)baseFare;
                            //        result.Price.SupplierPrice += infantPrice;
                            //    }
                            //}
                        }

                        if (adtPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                        }
                        if (chdPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                        }
                        if (inftPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                        }
                        if (paxTypeTaxBreakUp.Count > 0)
                        {
                            result.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                        }
                        // if(infantFare!=null )
                        //{
                        //    result.Price.PublishedFare += (decimal)infantFare.BaseFare;
                        //    result.Price.Tax+= (decimal)infantFare.Tax;
                        //    result.Price.Tax += (decimal)infantFare.Tax;

                        //}
                        result.Price.SupplierCurrency = currencyCode;
                        result.BaseFare = (double)result.Price.PublishedFare;
                        result.Tax = (double)result.Price.Tax;
                        result.TotalFare = (double)Math.Round((result.BaseFare + result.Tax), agentDecimalValue);
                    }
                }

            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "(SG)Failed to GetUpdatedGSTFareBreakUp. Error : " + ex.ToString(), "");
                throw ex;
            }

        }

        /// <summary>
        /// This method returns the Seat Availability response based on the segments.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public SeatAvailabilityResp GetSeatAvailableSSR(SearchResult result, string sOrigin, string sDestination,
            SeatAvailabilityResp clsSeatAvailabilityResp, string session)
        {
            //Note1:for normal search signature will be assigned from the basket.
            //Note2:for one -one search signature will be assigned from the itinerary object.
            SegmentSeats clsSegmentSeats = new SegmentSeats();
            LogonResponse loginResponse = new LogonResponse();
            try
            {
                //Step1: Assign API credentails and Login
                if (result != null && !string.IsNullOrEmpty(result.RepriceErrorMessage) && result.RepriceErrorMessage.Length > 0)//For One-One Search
                {
                    //Here the signature is different for 2 different results
                    //So from the result object we need to assign here
                    //till seaech to book unique signature should be carried.
                    loginResponse.Signature = result.RepriceErrorMessage;
                }
                else//For Normal Search 
                {
                    loginResponse = Login();
                }
                if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature) && loginResponse.Signature.Length > 0)
                {
                    //FlightInfo seg = result.Flights[0].ToList().Where(x => x.Origin.AirportCode == sOrigin && x.Destination.AirportCode == sDestination).
                    //    FirstOrDefault();

                    SegmentSeats seg = clsSeatAvailabilityResp.SegmentSeat.Where(x => x.Origin == sOrigin && x.Destination == sDestination).FirstOrDefault();

                    GetSeatAvailabilityRequest clsGetSeatAvailabilityRequest = new GetSeatAvailabilityRequest();

                    clsGetSeatAvailabilityRequest.ContractVersion = contractVersion;
                    clsGetSeatAvailabilityRequest.Signature = loginResponse.Signature;

                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest = new SeatAvailabilityRequest();
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.STD = seg.STD;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.DepartureStation = seg.Origin;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.ArrivalStation = seg.Destination;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.IncludeSeatFees = true;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.SeatAssignmentMode = SeatAssignmentMode.PreSeatAssignment;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.FlightNumber = seg.FlightNo;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.CarrierCode = "SG";

                    GenericStatic.WriteLogFile(typeof(GetSeatAvailabilityRequest), clsGetSeatAvailabilityRequest,
                        xmlLogPath + sessionId + "_" + appUserId + "_" + seg.Origin + "_" + seg.Destination + "_" + "6E_GetSeatAvailabilityRequest_");

                    GetSeatAvailabilityResponse clsGetSeatAvailabilityResponse = bookingAPI.GetSeatAvailability(clsGetSeatAvailabilityRequest);

                    string filePath = GenericStatic.WriteLogFile(typeof(GetSeatAvailabilityResponse), clsGetSeatAvailabilityResponse,
                        xmlLogPath + sessionId + "_" + appUserId + "_" + seg.Origin + "_" + seg.Destination + "_" + "6E_GetSeatAvailabilityResponse_");

                    clsSegmentSeats = GenericStatic.TransformXMLAndDeserialize(clsSegmentSeats, File.ReadAllText(filePath), sSeatAvailResponseXSLT);
                    clsSegmentSeats.SeatInfoDetails = clsSegmentSeats.SeatInfoDetails.GroupBy(x => x.SeatNo).Select(y => y.First()).ToList();

                    var itemIndex = clsSeatAvailabilityResp.SegmentSeat.FindIndex(x => x.Origin == sOrigin && x.Destination == sDestination);
                    clsSeatAvailabilityResp.SegmentSeat.RemoveAt(itemIndex);
                    clsSeatAvailabilityResp.SegmentSeat.Insert(itemIndex, clsSegmentSeats);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to get seat availability response. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return clsSeatAvailabilityResp;
        }

        /// <summary>
        /// This method returns the Available SSR's based on the segments.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public DataTable GetAvailableSSR(SearchResult result)
        {
            //Note1:for normal search signature will be assigned from the basket.
            //Note2:for one -one search signature will be assigned from the itinerary object.
            DataTable dtSourceBaggage = null;
            LogonResponse loginResponse = new LogonResponse();
            try
            {
                //SSR : AdditionalBaggage and Meals [For Both Onward and Return Journeys]
                //Note:Infant is also an SSR.
                //But we will get the infant price from the price itinerary response.

                //Steps to get the available SSR Without GST input.
                //1.Login
                //2.Sell [Holds the complete itinerary]
                //3.GetSSRAvailabilityForBooking.

                //Steps to get the available SSR With GST input.
                //1.Login
                //2.UpdateContactRequest 
                //3.Sell[Holds the complete itinerary]
                //4.GetSSRAvailabilityForBooking
                //5.GetBookingFromState

                //Step1: Login
                if (result != null && !string.IsNullOrEmpty(result.RepriceErrorMessage))//For One-One Search
                {
                    //Here the signature is different for 2 different results
                    //So from the result object we need to assign here
                    //till seaech to book unique signature should be carried.
                    loginResponse.Signature = result.RepriceErrorMessage;
                }
                else//For Normal Search 
                {
                    loginResponse = Login();
                }
                if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature) && loginResponse.Signature.Length > 0)
                {

                    //This method will releases the itinerary.
                    //Clears the previous objects from the session if any objects are there.
                    //This method will avoid duplicate bookings. ADded by ziyad--nov-22-2018
                    ClearRequest clearRequest = new ClearRequest();
                    clearRequest.ContractVersion = contractVersion;
                    clearRequest.EnableExceptionStackTrace = false;
                    clearRequest.Signature = loginResponse.Signature;
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "SG_ClearRequest_" + appUserId.ToString() + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearRequest);
                        sw.Close();
                    }
                    catch { }
                    ClearResponse clearResponse = bookingAPI.Clear(clearRequest);
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearResponse));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "SG_ClearResponse_" + appUserId.ToString() + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearResponse);
                        sw.Close();
                    }
                    catch { }


                    //Note:Verify any GST input available then execute UpdateContactsRequest first.
                    //Step-1.1: If any GST input then UpdateContactGSTDetails method
                    UpdateContactsResponse updateGSTResponse = null;
                    if (!string.IsNullOrEmpty(gstCompanyName) && (gstCompanyName.Length > 0) &&
                       !string.IsNullOrEmpty(gstNumber) && (gstNumber.Length > 0) &&
                        !string.IsNullOrEmpty(gstOfficialEmail) && (gstOfficialEmail.Length > 0)
                       && !string.IsNullOrEmpty(loginResponse.Signature))
                    {

                        updateGSTResponse = UpdateContactGSTDetails(loginResponse.Signature);

                    }
                    //Step2:Sell
                    BookingUpdateResponseData responseData = null;
                    if (searchByAvailability)
                    {
                        responseData = ExecuteSellForSearchByAvailability(loginResponse.Signature, result);
                    }
                    else
                    {
                        responseData = ExecuteSell(loginResponse.Signature, result);
                    }
                    if (responseData != null)
                    {
                        //Step-3: GetAvailableSSR

                        GetSSRAvailabilityForBookingResponse ssrAvaialbleResponse = new GetSSRAvailabilityForBookingResponse();
                        GetSSRAvailabilityForBookingRequest ssrRequest = new GetSSRAvailabilityForBookingRequest();
                        ssrRequest.ContractVersion = contractVersion;
                        ssrRequest.Signature = loginResponse.Signature;

                        SSRAvailabilityForBookingRequest availRequest = new SSRAvailabilityForBookingRequest();
                        availRequest.InventoryControlled = true;
                        availRequest.NonInventoryControlled = true;
                        availRequest.SeatDependent = true;
                        availRequest.NonSeatDependent = true;
                        availRequest.CurrencyCode = currencyCode;

                        List<LegKey> legKeysList = new List<LegKey>();
                        for (int i = 0; i < result.Flights.Length; i++)
                        {
                            for (int j = 0; j < result.Flights[i].Length; j++)
                            {
                                LegKey legKey = new LegKey();
                                legKey.CarrierCode = "SG";
                                legKey.FlightNumber = result.Flights[i][j].FlightNumber;
                                legKey.DepartureDate = result.Flights[i][j].DepartureTime;
                                legKey.DepartureStation = result.Flights[i][j].Origin.AirportCode;
                                legKey.ArrivalStation = result.Flights[i][j].Destination.AirportCode;
                                legKeysList.Add(legKey);
                            }
                        }
                        if (legKeysList != null && legKeysList.Count > 0)
                        {
                            availRequest.SegmentKeyList = legKeysList.ToArray();
                        }
                        ssrRequest.SSRAvailabilityForBookingRequest = availRequest;
                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(GetSSRAvailabilityForBookingRequest));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SG_AvailableSSR_Request_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, ssrRequest);
                            sw.Close();
                        }
                        catch { }
                        ssrAvaialbleResponse = bookingAPI.GetSSRAvailabilityForBooking(ssrRequest);
                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(GetSSRAvailabilityForBookingResponse));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SG_AvailableSSR_Response_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, ssrAvaialbleResponse);
                            sw.Close();
                        }
                        catch { }
                        SSRAvailabilityForBookingResponse ssrResponse = null;
                        if (ssrAvaialbleResponse != null && ssrAvaialbleResponse.SSRAvailabilityForBookingResponse != null)
                        {
                            ssrResponse = ssrAvaialbleResponse.SSRAvailabilityForBookingResponse;
                        }
                        if (ssrResponse != null && ssrResponse.SSRSegmentList != null && ssrResponse.SSRSegmentList.Length > 0)
                        {
                            dtSourceBaggage = new DataTable();
                            List<AvailablePaxSSR> availablePaxSSRs_OnwardBag = new List<AvailablePaxSSR>();//OnwardBaggage
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnBag = new List<AvailablePaxSSR>();//ReturnBaggage

                            List<AvailablePaxSSR> availablePaxSSRs_OnwardMeal = new List<AvailablePaxSSR>();//OnwardMeal
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnMeal = new List<AvailablePaxSSR>();//ReturnMeal


                            for (int i = 0; i < result.Flights.Length; i++)
                            {
                                for (int j = 0; j < result.Flights[i].Select(x => x.FlightNumber).Distinct().ToArray().Count(); j++)
                                {
                                    string DepartureStation = result.Flights[i][j].Origin.AirportCode;
                                    string ArrivalStation = result.Flights[i][j].Destination.AirportCode;
                                    for (int p = 0; p < ssrResponse.SSRSegmentList.Length; p++)
                                    {
                                        SSRSegment sSRSegment = ssrResponse.SSRSegmentList[p];
                                        if (sSRSegment.AvailablePaxSSRList != null && sSRSegment.AvailablePaxSSRList.Length > 0 && sSRSegment.LegKey.DepartureStation == DepartureStation && sSRSegment.LegKey.ArrivalStation == ArrivalStation)
                                        {
                                            if (i == 0)
                                            {
                                                availablePaxSSRs_OnwardBag.AddRange(sSRSegment.AvailablePaxSSRList.ToList().Where(x => x.SSRCode.StartsWith("EB")));
                                                availablePaxSSRs_OnwardMeal.AddRange(sSRSegment.AvailablePaxSSRList.ToList());
                                            }
                                            else
                                            {
                                                availablePaxSSRs_ReturnBag.AddRange(sSRSegment.AvailablePaxSSRList.ToList().Where(x => x.SSRCode.StartsWith("EB")));
                                                availablePaxSSRs_ReturnMeal.AddRange(sSRSegment.AvailablePaxSSRList.ToList());
                                            }
                                        }
                                    }
                                }
                            }

                            if (availablePaxSSRs_OnwardBag.Count > 0)
                            {
                                GetSpicejetSSRs(ref dtSourceBaggage, "0", availablePaxSSRs_OnwardBag,"Baggage");
                            } //OnwardBaggage

                            if (availablePaxSSRs_ReturnBag.Count > 0)
                            {
                                GetSpicejetSSRs(ref dtSourceBaggage, "1", availablePaxSSRs_ReturnBag, "Baggage");
                            } //ReturnBaggage

                            if (availablePaxSSRs_OnwardMeal.Count > 0)
                            {
                                GetSpicejetSSRs(ref dtSourceBaggage, "0", availablePaxSSRs_OnwardMeal,"Meal");
                            } //OnwardMeal

                            if (availablePaxSSRs_ReturnMeal.Count > 0)
                            {
                                GetSpicejetSSRs(ref dtSourceBaggage, "1", availablePaxSSRs_ReturnMeal,"Meal");
                            } //ReturnMeal

                        }
                    }
                }
            }
            catch (Exception ex)
            {


                //In case of exception in any of the above requests call the clear method.
                //This method will release the itinerary.
                //Clears the session.
                if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature))
                {
                    ClearRequest clearRequest = new ClearRequest();
                    clearRequest.ContractVersion = contractVersion;
                    clearRequest.EnableExceptionStackTrace = false;
                    clearRequest.Signature = loginResponse.Signature;
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SG_ClearRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearRequest);
                        sw.Close();
                    }
                    catch { }
                    ClearResponse clearResponse = bookingAPI.Clear(clearRequest);
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearResponse));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SG_ClearResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearResponse);
                        sw.Close();
                    }
                    catch { }
                }
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Execute Sell Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return dtSourceBaggage;
        }

        /// <summary>
        /// This method holds the itinerary.This method is a part of the booking process.
        /// </summary>
        /// <param name="signature"></param>
        /// <param name="searchResult"></param>
        /// <returns></returns>
        public BookingUpdateResponseData ExecuteSell(string signature, SearchResult searchResult)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();
            try
            {

                FlightItinerary itinerary = new FlightItinerary();
                itinerary.Segments = SearchResult.GetSegments(searchResult);
                itinerary.TicketAdvisory = searchResult.FareSellKey;

                string onwardJourneySellKey = string.Empty;
                string returnJourneySellKey = string.Empty;

                List<string> sellKeysListOnward = new List<string>();
                List<string> sellKeysListReturn = new List<string>();
                for (int p = 0; p < itinerary.Segments.Length; p++)
                {
                    if (itinerary.Segments[p].Group == 0)//Onward
                    {
                        sellKeysListOnward.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                    if (itinerary.Segments[p].Group == 1)//Return
                    {
                        sellKeysListReturn.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                }
                IEnumerable<string> distinctSellKeysOnward = sellKeysListOnward.Distinct();
                IEnumerable<string> distinctSellKeysReturn = sellKeysListReturn.Distinct();

                if (distinctSellKeysOnward.Count() > 0)
                {
                    foreach (string key in distinctSellKeysOnward)
                    {
                        if (onwardJourneySellKey.Length > 0)
                        {
                            onwardJourneySellKey += "^" + key;
                        }
                        else
                        {
                            onwardJourneySellKey = key;
                        }
                    }
                }
                if (distinctSellKeysReturn.Count() > 0)
                {
                    foreach (string key in distinctSellKeysReturn)
                    {
                        if (returnJourneySellKey.Length > 0)
                        {
                            returnJourneySellKey += "^" + key;
                        }
                        else
                        {
                            returnJourneySellKey = key;
                        }
                    }
                }

                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = currencyCode;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[1];
                if (itinerary.TicketAdvisory.Split('|').Length > 1)
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[2];
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = onwardJourneySellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = itinerary.TicketAdvisory.Split('|')[0];
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";

                if (itinerary.TicketAdvisory.Split('|').Length > 1)//Round trip
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].JourneySellKey = returnJourneySellKey;
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].FareSellKey = itinerary.TicketAdvisory.Split('|')[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].StandbyPriorityCode = "";
                }
                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < searchResult.FareBreakdown.Length; i++)
                {
                    if (searchResult.FareBreakdown[i] != null && searchResult.FareBreakdown[i].PassengerType != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        for (int h = 0; h < searchResult.FareBreakdown[i].PassengerCount; h++)
                        {
                            switch (searchResult.FareBreakdown[i].PassengerType)
                            {
                                case PassengerType.Adult:
                                    priceType.PaxType = "ADT";
                                    break;
                                case PassengerType.Child:
                                    priceType.PaxType = "CHD";
                                    break;

                            }
                            paxPriceTypes.Add(priceType);
                        }
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                if (!string.IsNullOrEmpty(promoCode))//Promo Code Implementation for corporate fare
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale = new TypeOfSale();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes = new string[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes[0] = "C";//Corporate Fare
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.PromotionCode = promoCode;
                }

                //New object creation for signature inclusion.
                SellRequest sellRequestObj = new SellRequest();
                sellRequestObj.ContractVersion = contractVersion;
                sellRequestObj.SellRequestData = requestData;
                sellRequestObj.Signature = signature;
                sellRequestObj.EnableExceptionStackTrace = false;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGSellRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellRequestObj);
                    sw.Close();
                }
                catch { }
                SellResponse sellResponse = bookingAPI.Sell(sellRequestObj);
                if (sellResponse != null && sellResponse.BookingUpdateResponseData != null)
                {
                    responseData = sellResponse.BookingUpdateResponseData;
                }
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGSellResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, responseData);
                    sw.Close();
                }
                catch { }

                //Added by lokesh on 20/12/2018
                //To Avoid Duplicate Bookings
                //Checking the segment count with the SSR response segment count
                //If that doesn't match we will call the LogOut method.
                //This Validation Does not allow the additional bookings that are already there in the signature.
                if (responseData != null && responseData.Success != null && responseData.Success.PNRAmount != null)
                {
                    int segCount = distinctSellKeysOnward.Count();
                    if (distinctSellKeysReturn.Count() > 0)
                    {
                        segCount = segCount + distinctSellKeysReturn.Count();
                    }
                    if (segCount != Convert.ToInt32(responseData.Success.PNRAmount.SegmentCount))
                    {
                        Logout(signature);
                        throw new Exception("(Indigo)Segment Count Mismatch ! Please Search Again..");
                    }
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Execute Sell Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return responseData;
        }

        /// <summary>
        /// Calculates the pax FareBreakDown from the priceItineraryResponse for both one way and round trip journeys
        /// </summary>
        /// <param name="priceItineraryResponse"></param>
        /// <param name="searchResult"></param>
        /// <param name="searchRequest"></param>
        private void GetPaxFareBreakDown(SpiceJet.SGBooking.Fare priceItineraryResponse, ref SearchResult searchResult, SearchRequest searchRequest)
        {
            if (priceItineraryResponse != null && priceItineraryResponse.PaxFares != null && priceItineraryResponse.PaxFares.Length > 0)
            {
                if (priceItineraryResponse.PaxFares[0] != null && priceItineraryResponse.PaxFares[0].ServiceCharges[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.PaxFares[0].ServiceCharges[0].CurrencyCode))
                {
                    rateOfExchange = exchangeRates[priceItineraryResponse.PaxFares[0].ServiceCharges[0].CurrencyCode];
                }
                PaxFare adultPaxfare = priceItineraryResponse.PaxFares.Where(i => i.PaxType.Equals("ADT")).FirstOrDefault();
                PaxFare childPaxfare = priceItineraryResponse.PaxFares.Where(i => i.PaxType.Equals("CHD")).FirstOrDefault();
                if (searchResult.FareBreakdown == null)
                {
                    searchResult.FareBreakdown = new Fare[1];
                    if (searchRequest.ChildCount > 0)
                    {
                        searchResult.FareBreakdown = new Fare[2];
                    }
                    if (searchRequest.AdultCount > 0)
                    {
                        if (searchResult.FareBreakdown[0] == null)
                        {
                            searchResult.FareBreakdown[0] = new Fare();
                        }
                        searchResult.FareBreakdown[0].PassengerCount = request.AdultCount;
                        searchResult.FareBreakdown[0].PassengerType = PassengerType.Adult;

                    }
                    if (searchRequest.ChildCount > 0)
                    {
                        if (searchResult.FareBreakdown[1] == null)
                        {
                            searchResult.FareBreakdown[1] = new Fare();
                        }
                        searchResult.FareBreakdown[1].PassengerCount = request.ChildCount;
                        searchResult.FareBreakdown[1].PassengerType = PassengerType.Child;

                    }
                }
                if (searchResult.Price == null)
                {
                    searchResult.Price = new PriceAccounts();
                }

                //Calculates the Adult Price
                if (adultPaxfare != null && adultPaxfare.ServiceCharges != null && adultPaxfare.ServiceCharges.Length > 0)
                {
                    foreach (BookingServiceCharge charge in adultPaxfare.ServiceCharges)
                    {
                        if (charge != null)
                        {
                            if (searchResult.FareBreakdown[0] != null)
                            {
                                switch (charge.ChargeType)
                                {
                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                        double baseFareAdult = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[0].PassengerCount);
                                        searchResult.FareBreakdown[0].BaseFare += baseFareAdult;
                                        searchResult.FareBreakdown[0].TotalFare += baseFareAdult;
                                        searchResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[0].PassengerCount);
                                        searchResult.Price.PublishedFare += (decimal)baseFareAdult;
                                        searchResult.Price.SupplierPrice += charge.Amount;
                                        break;
                                    //Deduct discount from the base fare
                                    case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                        double discountAdult = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[0].PassengerCount);
                                        searchResult.FareBreakdown[0].BaseFare = searchResult.FareBreakdown[0].BaseFare - discountAdult;
                                        searchResult.FareBreakdown[0].TotalFare = searchResult.FareBreakdown[0].TotalFare - discountAdult;
                                        searchResult.FareBreakdown[0].SupplierFare = searchResult.FareBreakdown[0].SupplierFare - (double)(charge.Amount * searchResult.FareBreakdown[0].PassengerCount);
                                        searchResult.Price.PublishedFare = (decimal)(searchResult.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                                        searchResult.Price.SupplierPrice = searchResult.Price.SupplierPrice - charge.Amount;
                                        break;
                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                        double tax = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[0].PassengerCount);
                                        searchResult.FareBreakdown[0].TotalFare += tax;
                                        searchResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[0].PassengerCount);
                                        searchResult.Price.Tax += (decimal)tax;
                                        searchResult.Price.SupplierPrice += charge.Amount;
                                        break;
                                }
                            }
                        }
                    }
                }

                //Calculates the Child Price
                if (childPaxfare != null && childPaxfare.ServiceCharges != null && childPaxfare.ServiceCharges.Length > 0)
                {
                    foreach (BookingServiceCharge charge in childPaxfare.ServiceCharges)
                    {
                        if (charge != null)
                        {
                            if (searchResult.FareBreakdown[1] != null)
                            {
                                switch (charge.ChargeType)
                                {
                                    case SpiceJet.SGBooking.ChargeType.FarePrice:
                                        double baseFareChd = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                        searchResult.FareBreakdown[1].BaseFare += baseFareChd;
                                        searchResult.FareBreakdown[1].TotalFare += baseFareChd;
                                        searchResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[1].PassengerCount);
                                        searchResult.Price.PublishedFare += (decimal)baseFareChd;
                                        searchResult.Price.SupplierPrice += charge.Amount;
                                        break;
                                    case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                        //Deduct discount from the base fare
                                        double discountChild = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                        searchResult.FareBreakdown[1].BaseFare = searchResult.FareBreakdown[1].BaseFare - discountChild;
                                        searchResult.FareBreakdown[1].TotalFare = searchResult.FareBreakdown[1].TotalFare - discountChild;
                                        searchResult.FareBreakdown[1].SupplierFare = searchResult.FareBreakdown[1].SupplierFare - (double)(charge.Amount * searchResult.FareBreakdown[1].PassengerCount);
                                        searchResult.Price.PublishedFare = (decimal)(searchResult.Price.PublishedFare - Convert.ToDecimal(discountChild));
                                        searchResult.Price.SupplierPrice = searchResult.Price.SupplierPrice - charge.Amount;
                                        break;
                                    case SpiceJet.SGBooking.ChargeType.Tax:
                                        double tax = (double)((charge.Amount * rateOfExchange) * searchResult.FareBreakdown[1].PassengerCount);
                                        searchResult.FareBreakdown[1].TotalFare += tax;
                                        searchResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * searchResult.FareBreakdown[1].PassengerCount);
                                        searchResult.Price.Tax += (decimal)tax;
                                        searchResult.Price.SupplierPrice += charge.Amount;
                                        break;
                                }
                            }
                        }
                    }
                }

                searchResult.Price.SupplierCurrency = currencyCode;
                searchResult.BaseFare = (double)searchResult.Price.PublishedFare;
                searchResult.Tax = (double)searchResult.Price.Tax;
                searchResult.TotalFare = (double)(Math.Round((searchResult.BaseFare + searchResult.Tax), agentDecimalValue));
            }
        }

        private void AssignPaxTaxComponents(PriceItineraryResponse priceItineraryResponse, ref SearchResult searchResult, SearchRequest searchRequest)
        {

            List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
            List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
            List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();


            if (priceItineraryResponse != null && searchResult != null)
            {
                if (priceItineraryResponse.Booking != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.CurrencyCode))
                {
                    rateOfExchange = exchangeRates[priceItineraryResponse.Booking.CurrencyCode];
                }
                List<string> FarePaxtypes = new List<string>();
                if (request.AdultCount > 0)
                {
                    FarePaxtypes.Add("ADT");
                }
                if (request.ChildCount > 0)
                {
                    FarePaxtypes.Add("CHD");
                }

                if (FarePaxtypes != null && FarePaxtypes.Count > 0)
                {
                    if (priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0)
                    {
                        for (int j = 0; j < priceItineraryResponse.Booking.Journeys.Length; j++)
                        {
                            if (priceItineraryResponse.Booking.Journeys[j].Segments != null && priceItineraryResponse.Booking.Journeys[j].Segments.Length > 0)
                            {
                                for (int s = 0; s < priceItineraryResponse.Booking.Journeys[j].Segments.Length; s++)
                                {
                                    if (priceItineraryResponse.Booking.Journeys[j].Segments[s] != null && priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares != null && priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares.Length > 0)
                                    {
                                        for (int f = 0; f < priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares.Length; f++)
                                        {
                                            if (priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f] != null && priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares != null && priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares.Length > 0)
                                            {
                                                for (int p = 0; p < priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares.Length; p++)
                                                {
                                                    if (priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares[p] != null)
                                                    {
                                                        foreach (string passengerFareType in FarePaxtypes)
                                                        {
                                                            if (priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares[p].PaxType.ToLower() == passengerFareType.ToLower())
                                                            {
                                                                if (priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares[p].ServiceCharges != null && priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares[p].ServiceCharges.Length > 0)
                                                                {
                                                                    foreach (BookingServiceCharge charge in priceItineraryResponse.Booking.Journeys[j].Segments[s].Fares[f].PaxFares[p].ServiceCharges)
                                                                    {
                                                                        if (charge != null)
                                                                        {
                                                                            switch (passengerFareType)
                                                                            {
                                                                                case "ADT":
                                                                                    if (searchResult.FareBreakdown[0] != null)
                                                                                    {
                                                                                        switch (charge.ChargeType)
                                                                                        {
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                                                            case SpiceJet.SGBooking.ChargeType.Calculated:
                                                                                            case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                                                            //case SpiceJet.SGBooking.ChargeType.Discount:
                                                                                            case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                                                            case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                            //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                                                            //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                            case SpiceJet.SGBooking.ChargeType.Note:
                                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                                                                adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                                                                break;
                                                                                        }
                                                                                    }

                                                                                    break;

                                                                                case "CHD":
                                                                                    if (searchResult.FareBreakdown[1] != null)
                                                                                    {
                                                                                        switch (charge.ChargeType)
                                                                                        {
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                                                            case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                                                            case SpiceJet.SGBooking.ChargeType.Calculated:
                                                                                            case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                                                            //case SpiceJet.SGBooking.ChargeType.Discount:
                                                                                            case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                                                            case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                                                            case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                                                            // case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                                                            //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                                                            case SpiceJet.SGBooking.ChargeType.Note:
                                                                                            case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                                                            case SpiceJet.SGBooking.ChargeType.Tax:
                                                                                            case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                                                            case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                                                                chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                                                                break;
                                                                                        }
                                                                                    }
                                                                                    break;

                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            #region Baggage Calculation
            if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0)
            {
                if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[0] != null && priceItineraryResponse.Booking.Journeys[0].Segments != null && priceItineraryResponse.Booking.Journeys[0].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[0].Segments[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                {
                    searchResult.BaggageIncludedInFare = priceItineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                }
                for (int k = 1; k < searchResult.Flights[0].Length; k++)
                {
                    if (searchResult.Flights[0][k].Status.ToUpper() != "VIA")
                    {
                        if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[0] != null && priceItineraryResponse.Booking.Journeys[0].Segments != null && priceItineraryResponse.Booking.Journeys[0].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[0].Segments[k] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                        {
                            searchResult.BaggageIncludedInFare += "," + priceItineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                        }
                    }
                    else
                    {
                        if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[0] != null && priceItineraryResponse.Booking.Journeys[0].Segments != null && priceItineraryResponse.Booking.Journeys[0].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[0].Segments[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                        {
                            searchResult.BaggageIncludedInFare += "," + priceItineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                        }
                    }
                }
                if (request.Type == SearchType.Return)
                {
                    if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[1] != null && priceItineraryResponse.Booking.Journeys[1].Segments != null && priceItineraryResponse.Booking.Journeys[1].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[1].Segments[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                    {
                        searchResult.BaggageIncludedInFare += "|" + priceItineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                    }
                    for (int k = 1; k < searchResult.Flights[1].Length; k++)
                    {
                        if (searchResult.Flights[1][k].Status.ToUpper() != "VIA")
                        {
                            if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[1] != null && priceItineraryResponse.Booking.Journeys[1].Segments != null && priceItineraryResponse.Booking.Journeys[1].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[1].Segments[k] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                            {
                                searchResult.BaggageIncludedInFare += "," + priceItineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                            }
                        }
                        else
                        {
                            if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0 && priceItineraryResponse.Booking.Journeys[1] != null && priceItineraryResponse.Booking.Journeys[1].Segments != null && priceItineraryResponse.Booking.Journeys[1].Segments.Length > 0 && priceItineraryResponse.Booking.Journeys[1].Segments[0] != null && !string.IsNullOrEmpty(priceItineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                            {
                                searchResult.BaggageIncludedInFare += "," + priceItineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                            }
                        }
                    }
                }
            }
            #endregion

            if (adtPaxTaxBreakUp.Count > 0)
            {
                paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
            }
            if (chdPaxTaxBreakUp.Count > 0)
            {
                paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
            }
            if (paxTypeTaxBreakUp.Count > 0)
            {
                searchResult.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
            }
        }

        public void GetTaxBreakUp(ref SearchResult resultObj, SearchRequest request)
        {
            try
            {
                //Note1:gets the priceItineraryResponse
                //Note2:If normal search assign pax tax components
                //Noet3:if one-one search calculate pax fare break down.
                PriceItineraryResponse priceItineraryResponse = null;
                if (request.Type == SearchType.OneWay || request.SearchBySegments)
                {
                    priceItineraryResponse = priceItineraryResponse = GetPriceResponseForOneWay(request, resultObj.JourneySellKey, resultObj.FareSellKey, resultObj.RepriceErrorMessage);
                }
                else if (request.Type == SearchType.Return)
                {
                    priceItineraryResponse = GetPriceResponseForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj.RepriceErrorMessage);
                }
                if (priceItineraryResponse != null)
                {
                    //During Reprice also deduct the promotion discount from the base fare.
                    GetPaxFareBreakDown(priceItineraryResponse, ref resultObj, request);
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Search, Severity.High, appUserId, "(Spice Jet)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                throw new BookingEngineException("(Spice Jet) No fare available");
            }
        }

        protected PriceItineraryResponse GetPriceResponseForOneWay(SearchRequest request, string journeySellKey, string fareSellKey, string signature)
        {
            //Note1:for normal search signature will be assigned from the basket
            //Note2:for one-one search assign signature from the itinerary object.

            if (string.IsNullOrEmpty(signature)) //For Normal Search
            {
                LogonResponse loginResponse = Login();
                if (loginResponse != null && loginResponse.Signature.Length > 0)
                {

                    this.request = request;
                    loginSignature = loginResponse.Signature;
                }
            }
            else
            {
                loginSignature = signature;
            }
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            IBookingManager bookingAPI = this.bookingAPI;

            PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
            priceItinRequest.Signature = loginSignature;
            priceItinRequest.ContractVersion = contractVersion;
            priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
            priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(request.AdultCount + request.ChildCount);


            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[1];
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[1];
            if (request.ChildCount > 0)
            {
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[2];
            }

            //Adult Pax Price Types
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0] = new PaxPriceType();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxType = "ADT";
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxCount = (Int16)(request.AdultCount);

            //Child Pax Price Types
            if (request.ChildCount > 0)
            {
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1] = new PaxPriceType();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxType = "CHD";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxCount = (Int16)(request.ChildCount);
            }


            //For Corporate Fares Need to get the promotion discount
            if (!string.IsNullOrEmpty(promoCode))
            {
                priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes = new string[1];
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes[0] = "C";//CorporateFare
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = promoCode;
            }

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
            priceItinRequest.EnableExceptionStackTrace = false;
            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGPriceItineraryRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, priceItinRequest);
                sw.Close();
            }
            catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
            try
            {
                piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(Booking));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGPriceItineraryResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, piResponse.Booking);
                    sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
            }
            return piResponse;
        }

        protected PriceItineraryResponse GetPriceResponseForRoundTrip(SearchRequest request, string journeySellKey, string fareSellKey, string fareSellKeyRet, string journeySellKeyRet, string signature)
        {
            //Note1:for normal search assign signature from the basket
            //Note2:for one-one search assign signature from the itinerary object.
            if (string.IsNullOrEmpty(signature))
            {
                LogonResponse loginResponse = Login();
                if (loginResponse != null && loginResponse.Signature.Length > 0)
                {

                    this.request = request;
                    loginSignature = loginResponse.Signature;
                }
            }
            else
            {
                loginSignature = signature;
            }

            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            IBookingManager bookingAPI = this.bookingAPI;

            int paxCount = request.AdultCount + request.ChildCount;

            PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
            priceItinRequest.Signature = loginSignature;
            priceItinRequest.ContractVersion = contractVersion;
            priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
            priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(request.AdultCount + request.ChildCount + request.InfantCount);
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[paxCount];

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[2];
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1] = new SellKeyList();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].FareSellKey = fareSellKeyRet;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].JourneySellKey = journeySellKeyRet;

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[1];
            if (request.ChildCount > 0)
            {
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[2];
            }

            //Adult Pax Price Types
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0] = new PaxPriceType();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxType = "ADT";
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxCount = (Int16)(request.AdultCount);

            //Child Pax Price Types
            if (request.ChildCount > 0)
            {
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1] = new PaxPriceType();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxType = "CHD";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxCount = (Int16)(request.ChildCount);
            }


            //For Corporate Fares Need to get the promotion discount
            if (!string.IsNullOrEmpty(promoCode))
            {
                priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes = new string[1];
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes[0] = "C";//CorporateFare
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = promoCode;
            }

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
            priceItinRequest.EnableExceptionStackTrace = false;
            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGGetItineraryPriceRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, priceItinRequest);
                sw.Close();
            }
            catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
            try
            {
                piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(Booking));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGGetItineraryPriceResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, piResponse.Booking);
                    sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
            }
            return piResponse;
        }

        //Note1:the below methods are written for one-one search
        //Note2:SearchRoutingReturn will be called twice to get onward and return results
        //Note3:Create Session is used to get signture particulary used for one-one search
        //Note4:OnwardDirect Flights and Onward connecting flights method will assign signature to all the results of one-one search
        //Note5:ExecuteSellForSearchByAvailability will be called to hold the itinerary.

        /// <summary>
        /// This method will do Routing search for Return. This method will be called two times one for Onward segment and two for Return segment.
        /// </summary>
        /// <param name="eventNumber"></param>
        protected void SearchRoutingReturn(int eventNumber)
        {
            try
            {
                LogonResponse loginResponse = CreateSession(eventNumber);
                if (loginResponse != null && loginResponse.Signature.Length > 0)
                {

                    SearchRequest request = searchRequests[Convert.ToInt32(eventNumber)];
                    GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                    GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                    availRequest.ContractVersion = contractVersion;
                    availRequest.Signature = loginResponse.Signature;
                    availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                    for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                        if (!string.IsNullOrEmpty(promoCode))
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                        }
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "SG";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 20;

                        if (request.Segments[0].flightCabinClass == CabinClass.Business)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "BC";
                        }
                        else
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "";
                        }

                        List<string> sgFareTypes = new List<string>();
                        if (request.Segments[0].flightCabinClass != CabinClass.Business)//ECONOMY + ALL
                        {
                            sgFareTypes.Add("R");//Regular Fare.
                                                 // w.e.f. 6th September 2019, criteria of Friends and Family fares to 
                                                 //Minimum 2 passenger and Maximum 9 passenger, 
                            if ((request.AdultCount + request.ChildCount >= 2) && (request.AdultCount + request.ChildCount <= 9))
                            {
                                sgFareTypes.Add("F");//Family Fare.
                            }
                            if (request.AdultCount + request.ChildCount < 4)
                            {
                                sgFareTypes.Add("T");//Coupon Fares
                                sgFareTypes.Add("S");//Fare difference
                            }
                            sgFareTypes.Add("HB");//Hand Baggage.
                            sgFareTypes.Add("IO");//Advance Purchase 
                            sgFareTypes.Add("MX");//Spice Flex

                        }
                        //BUSINESS CLASS + ALL
                        if (request.Segments[0].flightCabinClass == CabinClass.Business || request.Segments[0].flightCabinClass == CabinClass.All)
                        {
                            sgFareTypes.Add("BC");//Business class.
                        }
                        if (!string.IsNullOrEmpty(promoCode))
                        {
                            sgFareTypes.Add("C");//Corporate Fare
                        }
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[sgFareTypes.Count()];
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = sgFareTypes.ToArray();


                        ////Different fare types:
                        //if ((request.AdultCount + request.ChildCount) >= 4)
                        //{
                        //    if (request.Segments[0].flightCabinClass != CabinClass.Business) 
                        //    {
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[8];
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R"; //Regular Fare
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "F"; //Family Fares
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "HB";//Hand Baggage
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[3] = "IO";//Advance Purchase (Non Refundable but changeable at Rs.2000/2350 domestic/ineternational)
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[4] = "MX";//SpiceFlex
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[5] = "BC"; //Business class
                        //    }
                        //    else if (request.Segments[0].flightCabinClass == CabinClass.Business) //Only for Business class results
                        //    {
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[6] = "BC";//Business Class

                        //    }
                        //    if (!string.IsNullOrEmpty(promoCode))
                        //    {
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[7] = "C";//CorporateFare
                        //    }
                        //}
                        //else
                        //{
                        //    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[9];
                        //    if (request.Segments[0].flightCabinClass != CabinClass.Business)
                        //    {
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";//Regular
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "HB";//Hand Baggage
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "IO";//Advance Purchase
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[3] = "T";//Added by brahma 22.07.2016 For Coupon Fares
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[4] = "S";//Added by shiva 26.Sep.2016 For Fare difference
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[5] = "MX";//SpiceFlex
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[6] = "BC"; //Business Class
                        //    }
                        //    else if (request.Segments[0].flightCabinClass == CabinClass.Business)
                        //    {
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[7] = "BC"; //Business Class
                        //    }
                        //    if (!string.IsNullOrEmpty(promoCode))
                        //    {
                        //        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[8] = "C";//Corporate Fare
                        //    }
                        //}


                        int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];
                        for (int a = 0; a < request.AdultCount; a++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                        }
                        if (request.ChildCount > 0)
                        {
                            for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                            }
                        }
                        if (request.InfantCount > 0)
                        {
                            for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                            }
                        }
                    }
                    availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.EnableExceptionStackTrace = false;
                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                        string filePath = xmlLogPath + Convert.ToString(eventNumber) + "_" + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_GetAvailabilityReq_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, availRequest);
                        sw.Close();
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Request. Reason : " + ex.ToString(), "");
                    }
                    GetAvailabilityVer2Response getAvailabilityVer2Response = bookingAPI.GetAvailabilityVer2(availRequest);
                    TripAvailabilityResponseVer2 tripAvailabilityResponseVer2 = getAvailabilityVer2Response.GetTripAvailabilityVer2Response;

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityVer2Response));
                        string filePath = xmlLogPath + Convert.ToString(eventNumber) + "_" + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_GetAvailabilityRes_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, getAvailabilityVer2Response);
                        sw.Close();
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize Search Response. Reason : " + ex.ToString(), "");
                    }
                    if (tripAvailabilityResponseVer2 != null && tripAvailabilityResponseVer2.Schedules != null && tripAvailabilityResponseVer2.Schedules.Length > 0)
                    {
                        List<AvailableJourney> OnwardJourneys = new List<AvailableJourney>();
                        //Schedules --2 Dimensional Array
                        //Schedule --0 --Always onward journeys
                        //Schedule --1 --Always return journeys
                        for (int j = 0; j < tripAvailabilityResponseVer2.Schedules[0].Length; j++)
                        {
                            if (tripAvailabilityResponseVer2.Schedules[0][j].AvailableJourneys.Length > 0)
                            {
                                OnwardJourneys.AddRange(tripAvailabilityResponseVer2.Schedules[0][j].AvailableJourneys);
                            }
                        }
                        if (OnwardJourneys != null && OnwardJourneys.Count > 0)
                        {
                            for (int i = 0; i < OnwardJourneys.Count; i++)
                            {
                                if (OnwardJourneys[i].AvailableSegment != null && OnwardJourneys[i].AvailableSegment.Length > 0)
                                {
                                    //Condition which determines that the journey has connecting flights.
                                    if (OnwardJourneys[i].AvailableSegment.Length > 1)
                                    {
                                        GetOnwardConnectingFlights(OnwardJourneys[i], tripAvailabilityResponseVer2, loginResponse.Signature);
                                    }
                                    else//Direct Flights
                                    {
                                        for (int j = 0; j < OnwardJourneys[i].AvailableSegment[0].AvailableFares.Length; j++)
                                        {
                                            GetOnwardDirectFlights(OnwardJourneys[i], OnwardJourneys[i].AvailableSegment[0].AvailableFares[j], tripAvailabilityResponseVer2, loginResponse.Signature);
                                        }
                                    }
                                }

                            }
                            CombinedSearchResults.AddRange(oneWayResultList);
                            oneWayResultList.Clear();
                        }
                    }
                }

                //eventFlag[(int)eventNumber].Set();
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to get results for One-One  search. Reason : " + ex.ToString(), "");
                //eventFlag[(int)eventNumber].Set();
            }
        }



        /// <summary>
        /// Authenticates and creates a session for the specified user
        /// </summary>
        /// <returns></returns>
        private LogonResponse CreateSession(object eventNumber)
        {
            LogonResponse response = null;
            try
            {
                response = new LogonResponse();
                LogonRequest request = new LogonRequest();
                request.logonRequestData = new LogonRequestData();
                request.logonRequestData.AgentName = agentId;
                request.logonRequestData.DomainCode = agentDomain;
                request.logonRequestData.LocationCode = "";
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.Password = password;
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.TerminalInfo = "";
                request.logonRequestData.ClientName = "";
                request.ContractVersion = contractVersion;
                request.EnableExceptionStackTrace = false;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(LogonRequest));
                    string filePath = xmlLogPath + Convert.ToInt32(eventNumber) + "_" + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGLogonRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize LogonRequest. Reason : " + ex.ToString(), "");
                }
                response = sessionAPI.Logon(request);
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(LogonResponse));
                    string filePath = xmlLogPath + Convert.ToInt32(eventNumber) + "_" + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGLogonResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, response);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Serialize LogonResponse. Reason : " + ex.ToString(), "");
                }
            }
            catch (Exception ex)
            {

                throw new Exception("SpiceJet Login failed.Reason : " + ex.Message, ex);
            }
            return response;
        }

        /// <summary>
        /// Gets the Onward Direct flights in case of One Way trip
        /// </summary>
        /// <param name="OnwardJourney"></param>
        /// <param name="OnwardFare"></param>
        /// <param name="tripAvailabilityResponseVer2"></param>
        private void GetOnwardDirectFlights(AvailableJourney OnwardJourney, SpiceJet.SGBooking.AvailableFare2 OnwardFare, TripAvailabilityResponseVer2 tripAvailabilityResponseVer2, string signature)
        {
            try
            {
                if (OnwardJourney != null && OnwardFare != null)
                {
                    SpiceJet.SGBooking.Fare availableOnwardFare = tripAvailabilityResponseVer2.Fares[OnwardFare.FareIndex];

                    SearchResult result = new SearchResult();
                    result.RepriceErrorMessage = signature;
                    result.IsLCC = true;
                    //result.ResultBookingSource = BookingSource.SpiceJet;
                    if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                    {
                        result.ResultBookingSource = BookingSource.SpiceJet;
                    }
                    else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                    {
                        result.ResultBookingSource = BookingSource.SpiceJetCorp;
                    }
                    result.Airline = "SG";
                    result.Currency = agentBaseCurrency;
                    result.EticketEligible = true;

                    result.FareRules = new List<FareRule>();
                    FareRule fareRule = new FareRule();
                    fareRule.Airline = result.Airline;
                    fareRule.Destination = request.Segments[0].Destination;
                    fareRule.FareBasisCode = availableOnwardFare.FareBasisCode;
                    fareRule.FareInfoRef = availableOnwardFare.RuleNumber;
                    fareRule.Origin = request.Segments[0].Origin;
                    fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                    fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                    result.FareRules.Add(fareRule);

                    result.JourneySellKey = OnwardJourney.JourneySellKey;
                    result.FareType = GetFareTypeForProductClass(availableOnwardFare.ProductClass);
                    result.FareSellKey = availableOnwardFare.FareSellKey;
                    result.NonRefundable = false;
                    result.Flights = new FlightInfo[1][];
                    List<FlightInfo> directFlights = GetFlightInfo(OnwardJourney.AvailableSegment[0], OnwardJourney, availableOnwardFare.ProductClass, "ONWARD");
                    if (directFlights != null && directFlights.Count > 0)
                    {
                        result.Flights[0] = directFlights.ToArray();
                    }
                    if (result.Flights[0] != null && result.Flights[0].Length > 0 && !string.IsNullOrEmpty(result.JourneySellKey) && !string.IsNullOrEmpty(result.FareSellKey))
                    {
                        if (request.InfantCount > 0)
                        {
                            this.loginSignature = signature;
                            string logFileName = result.Flights[0][0].FlightNumber + "_" + result.FareType.Replace(" ", "_") + "_" + Convert.ToString(result.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            PriceItineraryResponse itineraryResponse = GetItineraryPrice(request, OnwardJourney.JourneySellKey, availableOnwardFare.FareSellKey, OnwardJourney, logFileName);
                            GetPaxFareBreakDown(itineraryResponse, ref result, request);
                        }
                        else
                        {
                            GetPaxFareBreakDown(availableOnwardFare, ref result, request);
                        }
                    }
                    if (result.FareType.Contains("Special Return Trip"))
                    {
                        result.IsSpecialRoundTrip = true;//For identifying the special return trips in combination search.
                    }
                    oneWayResultList.Add(result);
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Search, Severity.High, 0, "(Spicejet)Failed to get onward direct flight results." + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Gets the Connecting Flights in case of OneWay Trip
        /// </summary>
        /// <param name="journeyOnWard"></param>
        /// <param name="tripAvailabilityResponseVer2"></param>
        private void GetOnwardConnectingFlights(AvailableJourney journeyOnWard, TripAvailabilityResponseVer2 tripAvailabilityResponseVer2, string signature)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {
                if (journeyOnWard != null && journeyOnWard.AvailableSegment != null && journeyOnWard.AvailableSegment.Length > 0 && journeyOnWard.AvailableSegment.Length == 2)
                {
                    /*Fare mapping on segments based on One to One relationship. 
                     *For ex. combine 1st Fare of the 1st Segment with the 1st Fare of the 2nd Segment, 
                     *The 2nd Fare of the 1st Segment with the 2nd Fare of the 2nd Segment and so on (if there are more fares under each Segment).             
                     */
                    AvailableSegment firstSegmentCon = null;
                    AvailableSegment secondSegmentCon = null;
                    if (journeyOnWard.AvailableSegment[0] != null && journeyOnWard.AvailableSegment[1] != null)
                    {
                        firstSegmentCon = journeyOnWard.AvailableSegment[0];
                        secondSegmentCon = journeyOnWard.AvailableSegment[1];
                    }
                    if (firstSegmentCon != null && secondSegmentCon != null && firstSegmentCon.AvailableFares != null && secondSegmentCon.AvailableFares != null &&
                        firstSegmentCon.AvailableFares.Length > 0 && secondSegmentCon.AvailableFares.Length > 0 && (firstSegmentCon.AvailableFares.Length == secondSegmentCon.AvailableFares.Length))
                    {
                        for (int f = 0; f < firstSegmentCon.AvailableFares.Length; f++)
                        {
                            if (firstSegmentCon.AvailableFares[f] != null && secondSegmentCon.AvailableFares[f] != null)
                            {
                                SpiceJet.SGBooking.Fare fareFirstConSegOnWard = tripAvailabilityResponseVer2.Fares[firstSegmentCon.AvailableFares[f].FareIndex];
                                SpiceJet.SGBooking.Fare fareSecondConSegOnward = tripAvailabilityResponseVer2.Fares[secondSegmentCon.AvailableFares[f].FareIndex];

                                SearchResult result = new SearchResult();
                                result.RepriceErrorMessage = signature;
                                result.IsLCC = true;
                                //result.ResultBookingSource = BookingSource.SpiceJet;
                                if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SG")
                                {
                                    result.ResultBookingSource = BookingSource.SpiceJet;
                                }
                                else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "SGCORP")
                                {
                                    result.ResultBookingSource = BookingSource.SpiceJetCorp;
                                }
                                result.Airline = "SG";
                                result.Currency = agentBaseCurrency;
                                result.EticketEligible = true;

                                result.FareRules = new List<FareRule>();
                                FareRule fareRule = new FareRule();
                                fareRule.Airline = result.Airline;
                                fareRule.Destination = request.Segments[0].Destination;
                                fareRule.FareBasisCode = fareFirstConSegOnWard.FareBasisCode;
                                fareRule.FareInfoRef = fareFirstConSegOnWard.RuleNumber;
                                fareRule.Origin = request.Segments[0].Origin;
                                fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                result.FareRules.Add(fareRule);

                                result.JourneySellKey = journeyOnWard.JourneySellKey;
                                result.FareType = GetFareTypeForProductClass(fareFirstConSegOnWard.ProductClass);
                                result.FareSellKey = fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey;
                                result.NonRefundable = false;
                                result.Flights = new FlightInfo[1][];

                                List<FlightInfo> firstSegmentConFlights = GetFlightInfo(firstSegmentCon, journeyOnWard, fareFirstConSegOnWard.ProductClass, "ONWARD");
                                List<FlightInfo> secondSegmentConFlights = GetFlightInfo(secondSegmentCon, journeyOnWard, fareSecondConSegOnward.ProductClass, "ONWARD");
                                if (firstSegmentConFlights != null && secondSegmentConFlights != null && firstSegmentConFlights.Count > 0 && secondSegmentConFlights.Count > 0)
                                {
                                    List<FlightInfo> listOfFlights = new List<FlightInfo>();
                                    listOfFlights.AddRange(firstSegmentConFlights);
                                    listOfFlights.AddRange(secondSegmentConFlights);
                                    result.Flights[0] = listOfFlights.ToArray();
                                }

                                if (result.Flights[0] != null && result.Flights[0].Length > 0) //Onward Journey Connecting or Via Flights
                                {
                                    string logFileName = string.Empty;
                                    for (int m = 0; m < result.Flights[0].Length; m++)
                                    {
                                        if (result.Flights[0][m].Status.ToUpper() != "VIA")
                                        {
                                            if (!string.IsNullOrEmpty(logFileName))
                                            {
                                                logFileName += "_" + result.Flights[0][1].FlightNumber + "_" + result.FareType + "_" + Convert.ToString(result.Flights[0][1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            }
                                            else
                                            {
                                                logFileName = result.Flights[0][m].FlightNumber + "_" + result.FareType.Replace(" ", "_") + "_" + Convert.ToString(result.Flights[0][m].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                            }
                                        }
                                        else
                                        {
                                            logFileName = result.Flights[0][0].FlightNumber + "_" + result.FareType.Replace(" ", "_") + "_" + Convert.ToString(result.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                        }
                                    }
                                    if (request.InfantCount > 0)
                                    {
                                        this.loginSignature = signature;
                                        PriceItineraryResponse itineraryResponse = GetItineraryPrice(request, journeyOnWard.JourneySellKey, fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey, journeyOnWard, logFileName);
                                        if (itineraryResponse != null)
                                        {
                                            GetPaxFareBreakDown(itineraryResponse, ref result, request);
                                        }
                                    }
                                    else
                                    {
                                        GetPaxFareBreakDown(fareFirstConSegOnWard, ref result, request);
                                        GetPaxFareBreakDown(fareSecondConSegOnward, ref result, request);
                                    }
                                }
                                if (result.FareType.Contains("Special Return Trip"))
                                {
                                    result.IsSpecialRoundTrip = true;//For identifying the special return trips in combination search.
                                }
                                ResultList.Add(result);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception("SpiceJet failed to get Connecting Flights Results.Reason : " + ex.Message, ex);
            }
            if (ResultList.Count > 0)
            {
                oneWayResultList.AddRange(ResultList);
            }
        }

        public BookingUpdateResponseData ExecuteSellForSearchByAvailability(string signature, SearchResult searchResult)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();
            try
            {

                FlightItinerary itinerary = new FlightItinerary();
                itinerary.Segments = SearchResult.GetSegments(searchResult);
                itinerary.TicketAdvisory = searchResult.FareSellKey;

                string onwardJourneySellKey = string.Empty;
                List<string> sellKeysListOnward = new List<string>();

                for (int p = 0; p < itinerary.Segments.Length; p++)
                {
                    sellKeysListOnward.Add(itinerary.Segments[p].UapiSegmentRefKey);
                }
                IEnumerable<string> distinctSellKeysOnward = sellKeysListOnward.Distinct();


                if (distinctSellKeysOnward.Count() > 0)
                {
                    foreach (string key in distinctSellKeysOnward)
                    {
                        if (onwardJourneySellKey.Length > 0)
                        {
                            onwardJourneySellKey += "^" + key;
                        }
                        else
                        {
                            onwardJourneySellKey = key;
                        }
                    }
                }


                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = currencyCode;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[1];

                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = onwardJourneySellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = itinerary.TicketAdvisory.Split('|')[0];
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";

                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < searchResult.FareBreakdown.Length; i++)
                {
                    if (searchResult.FareBreakdown[i] != null && searchResult.FareBreakdown[i].PassengerType != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        for (int h = 0; h < searchResult.FareBreakdown[i].PassengerCount; h++)
                        {
                            switch (searchResult.FareBreakdown[i].PassengerType)
                            {
                                case PassengerType.Adult:
                                    priceType.PaxType = "ADT";
                                    break;
                                case PassengerType.Child:
                                    priceType.PaxType = "CHD";
                                    break;

                            }
                            paxPriceTypes.Add(priceType);
                        }
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                if (!string.IsNullOrEmpty(promoCode))//Promo Code Implementation for corporate fare
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale = new TypeOfSale();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes = new string[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes[0] = "C";//Corporate Fare
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.PromotionCode = promoCode;
                }

                //New object creation for signature inclusion.
                SellRequest sellRequestObj = new SellRequest();
                sellRequestObj.ContractVersion = contractVersion;
                sellRequestObj.SellRequestData = requestData;
                sellRequestObj.Signature = signature;
                sellRequestObj.EnableExceptionStackTrace = false;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGSellRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellRequestObj);
                    sw.Close();
                }
                catch { }
                SellResponse sellResponse = bookingAPI.Sell(sellRequestObj);
                if (sellResponse != null && sellResponse.BookingUpdateResponseData != null)
                {
                    responseData = sellResponse.BookingUpdateResponseData;
                }
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGSellResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, responseData);
                    sw.Close();
                }
                catch { }

                //Added by lokesh on 20/12/2018
                //To Avoid Duplicate Bookings
                //Checking the segment count with the SSR response segment count
                //If that doesn't match we will call the LogOut method.
                //This Validation Does not allow the additional bookings that are already there in the signature.

                if (responseData != null && responseData.Success != null && responseData.Success.PNRAmount != null)
                {
                    int segCount = distinctSellKeysOnward.Count();
                    if (segCount != Convert.ToInt32(responseData.Success.PNRAmount.SegmentCount))
                    {
                        Logout(signature);
                        throw new Exception("(SpiceJet)Segment Count Mismatch ! Please Search Again..");
                    }
                }

            }
            catch (Exception ex)
            {

                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Execute Sell Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return responseData;
        }

        /// <summary>
        ///This method returns a boolean which determine whether to send a  Search Request to get the flight inventory based on the sector list that is provided by Spice Jet.
        /// </summary>
        /// <param name="searchRequest"></param>
        /// <returns></returns>
        public bool AllowSearch(SearchRequest searchRequest)
        {
            bool allowSearch = false;
            try
            {
                //START:PERFORM SEARCH BASED ON THE NEAR BY AIRPORT

                List<SectorList> locSectorlist = new List<SectorList>();
                locSectorlist = Sectors.FindAll(x => x.Supplier == "SpiceJetCityMapping");


                // if (ConfigurationSystem.SectorListConfig.ContainsKey("SpiceJetCityMapping"))
                if (locSectorlist.Count() > 0)
                {
                    // Dictionary<string, string> sectorsNearBy = ConfigurationSystem.SectorListConfig["SpiceJetCityMapping"];
                    Dictionary<string, string> sectorsNearBy = locSectorlist.ToDictionary(r => r.Origin, r => r.Sectors);
                    if (sectorsNearBy.ContainsKey(searchRequest.Segments[0].Destination))
                    {
                        searchRequest.Segments[0].Destination = sectorsNearBy[searchRequest.Segments[0].Destination];
                    }
                    if (sectorsNearBy.ContainsKey(searchRequest.Segments[0].Origin) && searchRequest.Segments[0].NearByOriginPort)
                    {
                        searchRequest.Segments[0].Origin = sectorsNearBy[searchRequest.Segments[0].Origin];
                    }
                }
                //END:PERFORM SEARCH BASED ON THE NEAR BY AIRPORT

                locSectorlist = Sectors.FindAll(x => x.Supplier == "SpiceJet").ToList();

                //if (ConfigurationSystem.SectorListConfig.ContainsKey("SpiceJet"))
                if (locSectorlist.Count() > 0)
                {
                    // Dictionary<string, string> sectors = ConfigurationSystem.SectorListConfig["SpiceJet"];
                    Dictionary<string, string> sectors = locSectorlist.ToDictionary(r => r.Origin, r => r.Sectors);
                    if (sectors.ContainsKey(searchRequest.Segments[0].Origin))
                    {
                        if (sectors[searchRequest.Segments[0].Origin].Contains(searchRequest.Segments[0].Destination))
                        {
                            allowSearch = true;
                        }
                    }
                }
                else
                {
                    Core.Audit.Add(Core.EventType.Book, Core.Severity.High, appUserId, "SpiceJet: Error While reading sector from SectorListConfig : Error" + DateTime.Now, "");
                }
            }
            catch (Exception ex)
            {
                Core.Audit.Add(Core.EventType.Book, Core.Severity.High, appUserId, "SpiceJet: Error While reading sector from SectorListConfig : Error" + ex.ToString() + DateTime.Now, "");
            }
            return allowSearch;
        }

        /// <summary>
        /// Gets the Special Round Trip Fares For Combination One-One Search
        /// </summary>
        public void SpecialRoundTripFares(int eventNumber)
        {
            List<AvailableJourney> OnwardJourneys = new List<AvailableJourney>();
            List<AvailableJourney> ReturnJourneys = new List<AvailableJourney>();
            LogonResponse loginResponse = CreateSession(eventNumber);
            loginSignature = loginResponse.Signature;
            try
            {
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                availRequest.ContractVersion = contractVersion;
                availRequest.Signature = loginSignature;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();
                availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = SpiceJet.SGBooking.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = FlightType.All;
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                    }
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "SG";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DiscountCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClassCode = "";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 20;
                    int paxCount = request.AdultCount + request.ChildCount + request.InfantCount;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[1];
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R"; //Regular Fare
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses = new string[1];
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[0] = "XA";//Special round Trip Fares
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(paxCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];
                    for (int a = 0; a < request.AdultCount; a++)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxType = "ADT";
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[a].PaxDiscountCode = "";
                    }
                    if (request.ChildCount > 0)
                    {
                        for (int c = request.AdultCount; c < (paxCount - request.InfantCount); c++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[c].PaxType = "CHD";
                        }
                    }
                    if (request.InfantCount > 0)
                    {
                        for (int k = (request.AdultCount + request.ChildCount); k < paxCount; k++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[k].PaxType = "INFT";
                        }
                    }
                }
                availRequest.TripAvailabilityRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                availRequest.EnableExceptionStackTrace = false;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SpecialRTRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, availRequest);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                }
                GetAvailabilityVer2Response getAvailabilityVer2Response = bookingAPI.GetAvailabilityVer2(availRequest);
                TripAvailabilityResponseVer2 tripAvailabilityResponseVer2 = getAvailabilityVer2Response.GetTripAvailabilityVer2Response;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityVer2Response));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SpecialRTResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, getAvailabilityVer2Response);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to Save Search Response Ver2. Reason : " + ex.ToString(), "");
                }

                //Seperate onward and return journeys
                if (tripAvailabilityResponseVer2 != null && tripAvailabilityResponseVer2.Schedules != null && tripAvailabilityResponseVer2.Schedules.Length > 0)
                {
                    //Schedules --2 Dimensional Array
                    //Schedule --0 --Always onward journeys
                    //Schedule --1 --Always return journeys
                    for (int j = 0; j < tripAvailabilityResponseVer2.Schedules[0].Length; j++)
                    {
                        if (tripAvailabilityResponseVer2.Schedules[0][j].AvailableJourneys.Length > 0)
                        {
                            OnwardJourneys.AddRange(tripAvailabilityResponseVer2.Schedules[0][j].AvailableJourneys);
                        }
                    }
                    if (tripAvailabilityResponseVer2.Schedules.Length > 1)
                    {
                        for (int j = 0; j < tripAvailabilityResponseVer2.Schedules[1].Length; j++)
                        {
                            if (tripAvailabilityResponseVer2.Schedules[1][j].AvailableJourneys.Length > 0)
                            {
                                ReturnJourneys.AddRange(tripAvailabilityResponseVer2.Schedules[1][j].AvailableJourneys);
                            }
                        }
                    }

                    if (OnwardJourneys != null && OnwardJourneys.Count > 0)
                    {
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            if (OnwardJourneys[i].AvailableSegment != null && OnwardJourneys[i].AvailableSegment.Length > 0)
                            {
                                //Condition which determines that the journey has connecting flights.
                                if (OnwardJourneys[i].AvailableSegment.Length > 1)
                                {
                                    GetOnwardConnectingFlights(OnwardJourneys[i], tripAvailabilityResponseVer2, loginResponse.Signature);
                                }
                                else//Direct Flights
                                {
                                    for (int j = 0; j < OnwardJourneys[i].AvailableSegment[0].AvailableFares.Length; j++)
                                    {
                                        GetOnwardDirectFlights(OnwardJourneys[i], OnwardJourneys[i].AvailableSegment[0].AvailableFares[j], tripAvailabilityResponseVer2, loginResponse.Signature);
                                    }
                                }
                            }

                        }
                        CombinedSearchResults.AddRange(oneWayResultList);
                        oneWayResultList.Clear();
                    }

                    if (ReturnJourneys != null && ReturnJourneys.Count > 0)
                    {
                        for (int i = 0; i < ReturnJourneys.Count; i++)
                        {
                            if (ReturnJourneys[i].AvailableSegment != null && ReturnJourneys[i].AvailableSegment.Length > 0)
                            {
                                //Condition which determines that the journey has connecting flights.
                                if (ReturnJourneys[i].AvailableSegment.Length > 1)
                                {
                                    GetOnwardConnectingFlights(ReturnJourneys[i], tripAvailabilityResponseVer2, loginResponse.Signature);
                                }
                                else//Direct Flights
                                {
                                    for (int j = 0; j < ReturnJourneys[i].AvailableSegment[0].AvailableFares.Length; j++)
                                    {
                                        GetOnwardDirectFlights(ReturnJourneys[i], ReturnJourneys[i].AvailableSegment[0].AvailableFares[j], tripAvailabilityResponseVer2, loginResponse.Signature);
                                    }
                                }
                            }

                        }
                        CombinedSearchResults.AddRange(oneWayResultList);
                        oneWayResultList.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetSearch, Severity.High, 1, "(SpiceJet)Failed to get special round trip results." + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Special Round Trip Repricing
        /// </summary>
        /// <param name="resONW"></param>
        /// <param name="resRET"></param>
        /// <param name="request"></param>
        public void SpecialRoundTripReprice(ref SearchResult onwResult, ref SearchResult retResult, SearchRequest request)
        {
            try
            {
                PriceItineraryResponse priceItineraryResponse = GetPriceResponseForRoundTrip(request, onwResult.JourneySellKey.Split('|')[0], onwResult.FareSellKey.Split('|')[0], retResult.FareSellKey.Split('|')[0], retResult.JourneySellKey.Split('|')[0], retResult.RepriceErrorMessage);
                if (priceItineraryResponse != null)
                {
                    //From the PriceItineraryResponse separate the onward price and return price components.


                    //Onward Result
                    if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0)
                    {
                        rateOfExchange = exchangeRates[priceItineraryResponse.Booking.CurrencyCode];

                        List<PaxFare> adultPaxfare = new List<PaxFare>();
                        List<PaxFare> childPaxfare = new List<PaxFare>();


                        List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
                        List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

                        paxTypeTaxBreakUp.Clear();
                        adtPaxTaxBreakUp.Clear();
                        chdPaxTaxBreakUp.Clear();
                        inftPaxTaxBreakUp.Clear();

                        int journey = 0;
                        if (priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0)
                        {
                            if (priceItineraryResponse.Booking.Journeys[journey] != null && priceItineraryResponse.Booking.Journeys[journey].Segments != null && priceItineraryResponse.Booking.Journeys[journey].Segments.Length > 0)
                            {
                                for (int segment = 0; segment < priceItineraryResponse.Booking.Journeys[journey].Segments.Length; segment++)
                                {
                                    if (priceItineraryResponse.Booking.Journeys[journey].Segments[segment] != null && priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares != null && priceItineraryResponse.Booking.Journeys[journey].Segments[segment] != null && priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares.Length > 0)
                                    {
                                        for (int fare = 0; fare < priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares.Length; fare++)
                                        {
                                            if (priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).Count() > 0)
                                            {
                                                adultPaxfare.Add(priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).FirstOrDefault());
                                            }
                                            if (priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).Count() > 0)
                                            {
                                                childPaxfare.Add(priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).FirstOrDefault());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //Fare infantFare = null;
                        //// For infant updating from intial result fare
                        //if (onwResult.FareBreakdown != null)
                        //{
                        //    infantFare = onwResult.FareBreakdown.FirstOrDefault(P => (P.PassengerType == PassengerType.Infant));
                        //}
                        onwResult.Price = new PriceAccounts();
                        onwResult.BaseFare = 0;
                        onwResult.Tax = 0;
                        onwResult.TotalFare = 0;
                        onwResult.FareBreakdown = null;
                        int fareBreakDownCount = 0;
                        if (request.AdultCount > 0)
                        {
                            fareBreakDownCount = 1;
                        }
                        if (request.ChildCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        if (request.InfantCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        onwResult.FareBreakdown = new Fare[fareBreakDownCount];
                        if (request.AdultCount > 0)
                        {
                            onwResult.FareBreakdown[0] = new Fare();
                            onwResult.FareBreakdown[0].PassengerCount = request.AdultCount;
                            onwResult.FareBreakdown[0].PassengerType = PassengerType.Adult;
                        }
                        if (request.ChildCount > 0)
                        {
                            onwResult.FareBreakdown[1] = new Fare();
                            onwResult.FareBreakdown[1].PassengerCount = request.ChildCount;
                            onwResult.FareBreakdown[1].PassengerType = PassengerType.Child;
                        }

                        if (adultPaxfare != null && adultPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareAdult in adultPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareAdult.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (onwResult.FareBreakdown[0] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                    double baseFareAdult = (double)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.FareBreakdown[0].BaseFare += baseFareAdult;
                                                    onwResult.FareBreakdown[0].TotalFare += baseFareAdult;
                                                    onwResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.Price.PublishedFare += (decimal)baseFareAdult;
                                                    onwResult.Price.SupplierPrice += charge.Amount;
                                                    break;

                                                case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                    double discountAdult = (double)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.FareBreakdown[0].BaseFare = onwResult.FareBreakdown[0].BaseFare - discountAdult;
                                                    onwResult.FareBreakdown[0].TotalFare = onwResult.FareBreakdown[0].TotalFare - discountAdult;
                                                    onwResult.FareBreakdown[0].SupplierFare = onwResult.FareBreakdown[0].SupplierFare - (double)(charge.Amount * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.Price.PublishedFare = (decimal)(onwResult.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                                                    onwResult.Price.SupplierPrice = onwResult.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                case SpiceJet.SGBooking.ChargeType.Calculated:
                                                case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                //case SpiceJet.SGBooking.ChargeType.Discount:
                                                case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                case SpiceJet.SGBooking.ChargeType.Note:
                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.FareBreakdown[0].TotalFare += tax;
                                                    onwResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.Price.Tax += (decimal)tax;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                        onwResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[0].PassengerCount);
                                                    }
                                                    onwResult.Price.SupplierPrice += charge.Amount;
                                                    adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (childPaxfare != null && childPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareChild in childPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareChild.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (onwResult.FareBreakdown[1] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                    double baseFareChild = (double)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[1].PassengerCount);
                                                    onwResult.FareBreakdown[1].BaseFare += baseFareChild;
                                                    onwResult.FareBreakdown[1].TotalFare += baseFareChild;
                                                    onwResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * onwResult.FareBreakdown[1].PassengerCount);
                                                    onwResult.Price.PublishedFare += (decimal)baseFareChild;
                                                    onwResult.Price.SupplierPrice += charge.Amount;
                                                    break;

                                                case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                    double discountChild = (double)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[1].PassengerCount);
                                                    onwResult.FareBreakdown[1].BaseFare = onwResult.FareBreakdown[1].BaseFare - discountChild;
                                                    onwResult.FareBreakdown[1].TotalFare = onwResult.FareBreakdown[1].TotalFare - discountChild;
                                                    onwResult.FareBreakdown[1].SupplierFare = onwResult.FareBreakdown[0].SupplierFare - (double)(charge.Amount * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.Price.PublishedFare = (decimal)(onwResult.Price.PublishedFare - Convert.ToDecimal(discountChild));
                                                    onwResult.Price.SupplierPrice = onwResult.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                case SpiceJet.SGBooking.ChargeType.Calculated:
                                                case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                //case SpiceJet.SGBooking.ChargeType.Discount:
                                                case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                case SpiceJet.SGBooking.ChargeType.Note:
                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[1].PassengerCount);
                                                    onwResult.FareBreakdown[1].TotalFare += tax;
                                                    onwResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * onwResult.FareBreakdown[1].PassengerCount);
                                                    onwResult.Price.Tax += (decimal)tax;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                        onwResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[1].PassengerCount);
                                                    }
                                                    onwResult.Price.SupplierPrice += charge.Amount;
                                                    chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //if (request.InfantCount > 0)
                        //{

                        //    if (infantFare != null)
                        //    {

                        //        if (request.ChildCount > 0 && request.InfantCount > 0)
                        //        {
                        //            onwResult.FareBreakdown[2] = infantFare;
                        //        }
                        //        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                        //        {

                        //            onwResult.FareBreakdown[1] = infantFare;

                        //        }

                        //        onwResult.Price.PublishedFare += (decimal)infantFare.BaseFare;
                        //        onwResult.Price.Tax += (decimal)infantFare.Tax;
                        //        onwResult.Price.SupplierPrice += (decimal)infantFare.SupplierFare;

                        //    }

                        //}

                        if (adtPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                        }
                        if (chdPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                        }
                        if (inftPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                        }
                        if (paxTypeTaxBreakUp.Count > 0)
                        {
                            onwResult.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                        }

                        onwResult.Price.SupplierCurrency = currencyCode;
                        onwResult.BaseFare = (double)onwResult.Price.PublishedFare;
                        onwResult.Tax = (double)onwResult.Price.Tax;
                        onwResult.TotalFare = (double)Math.Round((onwResult.BaseFare + onwResult.Tax), agentDecimalValue);
                    }

                    //Return Result
                    if (priceItineraryResponse != null && priceItineraryResponse.Booking != null && priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0)
                    {
                        rateOfExchange = exchangeRates[priceItineraryResponse.Booking.CurrencyCode];

                        List<PaxFare> adultPaxfare = new List<PaxFare>();
                        List<PaxFare> childPaxfare = new List<PaxFare>();


                        List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
                        List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

                        paxTypeTaxBreakUp.Clear();
                        adtPaxTaxBreakUp.Clear();
                        chdPaxTaxBreakUp.Clear();
                        inftPaxTaxBreakUp.Clear();

                        int journey = 1;//Return Journey
                        if (priceItineraryResponse.Booking.Journeys != null && priceItineraryResponse.Booking.Journeys.Length > 0)
                        {
                            if (priceItineraryResponse.Booking.Journeys[journey] != null && priceItineraryResponse.Booking.Journeys[journey].Segments != null && priceItineraryResponse.Booking.Journeys[journey].Segments.Length > 0)
                            {
                                for (int segment = 0; segment < priceItineraryResponse.Booking.Journeys[journey].Segments.Length; segment++)
                                {
                                    if (priceItineraryResponse.Booking.Journeys[journey].Segments[segment] != null && priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares != null && priceItineraryResponse.Booking.Journeys[journey].Segments[segment] != null && priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares.Length > 0)
                                    {
                                        for (int fare = 0; fare < priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares.Length; fare++)
                                        {
                                            if (priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).Count() > 0)
                                            {
                                                adultPaxfare.Add(priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).FirstOrDefault());
                                            }
                                            if (priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).Count() > 0)
                                            {
                                                childPaxfare.Add(priceItineraryResponse.Booking.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).FirstOrDefault());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // For infant updating from intial result fare
                        // Fare infantFare = null;
                        //if (retResult.FareBreakdown != null)
                        //{
                        //     infantFare = retResult.FareBreakdown.FirstOrDefault(P => (P.PassengerType == PassengerType.Infant));
                        //}
                        retResult.Price = new PriceAccounts();
                        retResult.BaseFare = 0;
                        retResult.Tax = 0;
                        retResult.TotalFare = 0;
                        retResult.FareBreakdown = null;
                        int fareBreakDownCount = 0;
                        if (request.AdultCount > 0)
                        {
                            fareBreakDownCount = 1;
                        }
                        if (request.ChildCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        if (request.InfantCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        retResult.FareBreakdown = new Fare[fareBreakDownCount];
                        if (request.AdultCount > 0)
                        {
                            retResult.FareBreakdown[0] = new Fare();
                            retResult.FareBreakdown[0].PassengerCount = request.AdultCount;
                            retResult.FareBreakdown[0].PassengerType = PassengerType.Adult;
                        }
                        if (request.ChildCount > 0)
                        {
                            retResult.FareBreakdown[1] = new Fare();
                            retResult.FareBreakdown[1].PassengerCount = request.ChildCount;
                            retResult.FareBreakdown[1].PassengerType = PassengerType.Child;
                        }

                        if (adultPaxfare != null && adultPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareAdult in adultPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareAdult.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (retResult.FareBreakdown[0] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                    double baseFareAdult = (double)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.FareBreakdown[0].BaseFare += baseFareAdult;
                                                    retResult.FareBreakdown[0].TotalFare += baseFareAdult;
                                                    retResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.Price.PublishedFare += (decimal)baseFareAdult;
                                                    retResult.Price.SupplierPrice += charge.Amount;
                                                    break;

                                                case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                    double discountAdult = (double)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.FareBreakdown[0].BaseFare = retResult.FareBreakdown[0].BaseFare - discountAdult;
                                                    retResult.FareBreakdown[0].TotalFare = retResult.FareBreakdown[0].TotalFare - discountAdult;
                                                    retResult.FareBreakdown[0].SupplierFare = retResult.FareBreakdown[0].SupplierFare - (double)(charge.Amount * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.Price.PublishedFare = (decimal)(retResult.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                                                    retResult.Price.SupplierPrice = retResult.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                case SpiceJet.SGBooking.ChargeType.Calculated:
                                                case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                //case SpiceJet.SGBooking.ChargeType.Discount:
                                                case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                case SpiceJet.SGBooking.ChargeType.Note:
                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.FareBreakdown[0].TotalFare += tax;
                                                    retResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.Price.Tax += (decimal)tax;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                        retResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[0].PassengerCount);
                                                    }
                                                    retResult.Price.SupplierPrice += charge.Amount;
                                                    adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (childPaxfare != null && childPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareChild in childPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareChild.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (retResult.FareBreakdown[1] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                    double baseFareChild = (double)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[1].PassengerCount);
                                                    retResult.FareBreakdown[1].BaseFare += baseFareChild;
                                                    retResult.FareBreakdown[1].TotalFare += baseFareChild;
                                                    retResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * retResult.FareBreakdown[1].PassengerCount);
                                                    retResult.Price.PublishedFare += (decimal)baseFareChild;
                                                    retResult.Price.SupplierPrice += charge.Amount;
                                                    break;

                                                case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                    double discountChild = (double)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[1].PassengerCount);
                                                    retResult.FareBreakdown[1].BaseFare = retResult.FareBreakdown[1].BaseFare - discountChild;
                                                    retResult.FareBreakdown[1].TotalFare = retResult.FareBreakdown[1].TotalFare - discountChild;
                                                    retResult.FareBreakdown[1].SupplierFare = retResult.FareBreakdown[0].SupplierFare - (double)(charge.Amount * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.Price.PublishedFare = (decimal)(retResult.Price.PublishedFare - Convert.ToDecimal(discountChild));
                                                    retResult.Price.SupplierPrice = retResult.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                case SpiceJet.SGBooking.ChargeType.Calculated:
                                                case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                //case SpiceJet.SGBooking.ChargeType.Discount:
                                                case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                case SpiceJet.SGBooking.ChargeType.Note:
                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[1].PassengerCount);
                                                    retResult.FareBreakdown[1].TotalFare += tax;
                                                    retResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * retResult.FareBreakdown[1].PassengerCount);
                                                    retResult.Price.Tax += (decimal)tax;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                        retResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[1].PassengerCount);
                                                    }
                                                    retResult.Price.SupplierPrice += charge.Amount;
                                                    chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //if (request.InfantCount > 0)
                        //{

                        //    if (infantFare != null)
                        //    {

                        //        if (request.ChildCount > 0 && request.InfantCount > 0)
                        //        {
                        //            retResult.FareBreakdown[2] = infantFare;
                        //        }
                        //        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                        //        {

                        //            retResult.FareBreakdown[1] = infantFare;

                        //        }

                        //        retResult.Price.PublishedFare += (decimal)infantFare.BaseFare;
                        //        retResult.Price.Tax += (decimal)infantFare.Tax;
                        //        retResult.Price.SupplierPrice += (decimal)infantFare.SupplierFare;

                        //    }

                        //}

                        if (adtPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                        }
                        if (chdPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                        }
                        if (inftPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                        }
                        if (paxTypeTaxBreakUp.Count > 0)
                        {
                            retResult.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                        }

                        retResult.Price.SupplierCurrency = currencyCode;
                        retResult.BaseFare = (double)retResult.Price.PublishedFare;
                        retResult.Tax = (double)retResult.Price.Tax;
                        retResult.TotalFare = (double)Math.Round((retResult.BaseFare + retResult.Tax), agentDecimalValue);
                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the Special Round Trip Fares SSR [Baggage and Meals]
        /// </summary>
        /// <param name="onwResult"></param>
        /// <param name="retResult"></param>
        /// <returns></returns>
        public DataTable GetSpecialRoundTripSSR(SearchResult onwResult, SearchResult retResult)
        {
            //Note1:for normal search signature will be assigned from the basket.
            //Note2:for one -one search signature will be assigned from the itinerary object.
            DataTable dtSourceBaggage = null;
            LogonResponse loginResponse = new LogonResponse();
            try
            {
                //SSR : AdditionalBaggage and Meals [For Both Onward and Return Journeys]
                //Note:Infant is also an SSR.
                //But we will get the infant price from the price itinerary response.

                //Steps to get the available SSR Without GST input.
                //1.Login
                //2.Sell [Holds the complete itinerary]
                //3.GetSSRAvailabilityForBooking.

                //Steps to get the available SSR With GST input.
                //1.Login
                //2.UpdateContactRequest 
                //3.Sell[Holds the complete itinerary]
                //4.GetSSRAvailabilityForBooking
                //5.GetBookingFromState

                //Step1: Login
                if (onwResult != null && !string.IsNullOrEmpty(onwResult.RepriceErrorMessage))//For One-One Search
                {
                    //Here the signature is different for 2 different results
                    //So from the result object we need to assign here
                    //till seaech to book unique signature should be carried.
                    loginResponse.Signature = onwResult.RepriceErrorMessage;
                }

                if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature))
                {
                    ClearRequest clearRequest = new ClearRequest();
                    clearRequest.ContractVersion = contractVersion;
                    clearRequest.EnableExceptionStackTrace = false;
                    clearRequest.Signature = loginResponse.Signature;
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "SG_ClearRequest_" + appUserId.ToString() + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearRequest);
                        sw.Close();
                    }
                    catch { }
                    ClearResponse clearResponse = bookingAPI.Clear(clearRequest);
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearResponse));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "SG_ClearResponse_" + appUserId.ToString() + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearResponse);
                        sw.Close();
                    }
                    catch { }


                    //Note:Verify any GST input available then execute UpdateContactsRequest first.
                    //Step-1.1: If any GST input then UpdateContactGSTDetails method
                    UpdateContactsResponse updateGSTResponse = null;
                    if (!string.IsNullOrEmpty(gstCompanyName) && (gstCompanyName.Length > 0) &&
                       !string.IsNullOrEmpty(gstNumber) && (gstNumber.Length > 0) &&
                        !string.IsNullOrEmpty(gstOfficialEmail) && (gstOfficialEmail.Length > 0)
                       && !string.IsNullOrEmpty(loginResponse.Signature))
                    {

                        updateGSTResponse = UpdateContactGSTDetails(loginResponse.Signature);

                    }
                    //Step2:Sell
                    BookingUpdateResponseData responseData = GetSellResponse(loginResponse.Signature, onwResult, retResult);
                    if (responseData != null)
                    {
                        //Step-3: GetAvailableSSR

                        GetSSRAvailabilityForBookingResponse ssrAvaialbleResponse = new GetSSRAvailabilityForBookingResponse();
                        GetSSRAvailabilityForBookingRequest ssrRequest = new GetSSRAvailabilityForBookingRequest();
                        ssrRequest.ContractVersion = contractVersion;
                        ssrRequest.Signature = loginResponse.Signature;

                        SSRAvailabilityForBookingRequest availRequest = new SSRAvailabilityForBookingRequest();
                        availRequest.InventoryControlled = true;
                        availRequest.NonInventoryControlled = true;
                        availRequest.SeatDependent = true;
                        availRequest.NonSeatDependent = true;
                        availRequest.CurrencyCode = currencyCode;

                        List<LegKey> legKeysList = new List<LegKey>();
                        for (int i = 0; i < onwResult.Flights.Length; i++)
                        {
                            for (int j = 0; j < onwResult.Flights[i].Length; j++)
                            {
                                LegKey legKey = new LegKey();
                                legKey.CarrierCode = "SG";
                                legKey.FlightNumber = onwResult.Flights[i][j].FlightNumber;
                                legKey.DepartureDate = onwResult.Flights[i][j].DepartureTime;
                                legKey.DepartureStation = onwResult.Flights[i][j].Origin.AirportCode;
                                legKey.ArrivalStation = onwResult.Flights[i][j].Destination.AirportCode;
                                legKeysList.Add(legKey);
                            }
                        }
                        for (int i = 0; i < retResult.Flights.Length; i++)
                        {
                            for (int j = 0; j < retResult.Flights[i].Length; j++)
                            {
                                LegKey legKey = new LegKey();
                                legKey.CarrierCode = "SG";
                                legKey.FlightNumber = retResult.Flights[i][j].FlightNumber;
                                legKey.DepartureDate = retResult.Flights[i][j].DepartureTime;
                                legKey.DepartureStation = retResult.Flights[i][j].Origin.AirportCode;
                                legKey.ArrivalStation = retResult.Flights[i][j].Destination.AirportCode;
                                legKeysList.Add(legKey);
                            }
                        }
                        if (legKeysList != null && legKeysList.Count > 0)
                        {
                            availRequest.SegmentKeyList = legKeysList.ToArray();
                        }
                        ssrRequest.SSRAvailabilityForBookingRequest = availRequest;
                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(GetSSRAvailabilityForBookingRequest));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SG_AvailableSSR_Request_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, ssrRequest);
                            sw.Close();
                        }
                        catch { }
                        ssrAvaialbleResponse = bookingAPI.GetSSRAvailabilityForBooking(ssrRequest);
                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(GetSSRAvailabilityForBookingResponse));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SG_AvailableSSR_Response_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, ssrAvaialbleResponse);
                            sw.Close();
                        }
                        catch { }
                        SSRAvailabilityForBookingResponse ssrResponse = null;
                        if (ssrAvaialbleResponse != null && ssrAvaialbleResponse.SSRAvailabilityForBookingResponse != null)
                        {
                            ssrResponse = ssrAvaialbleResponse.SSRAvailabilityForBookingResponse;
                        }
                        if (ssrResponse != null && ssrResponse.SSRSegmentList != null && ssrResponse.SSRSegmentList.Length > 0)
                        {
                            dtSourceBaggage = new DataTable();

                            List<AvailablePaxSSR> availablePaxSSRs_OnwardBag = new List<AvailablePaxSSR>();//OnwardBaggage
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnBag = new List<AvailablePaxSSR>();//ReturnBaggage

                            List<AvailablePaxSSR> availablePaxSSRs_OnwardMeal = new List<AvailablePaxSSR>();//OnwardMeal
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnMeal = new List<AvailablePaxSSR>();//ReturnMeal

                            //Onward Result SSR
                            for (int i = 0; i < onwResult.Flights.Length; i++)
                            {
                                for (int j = 0; j < onwResult.Flights[i].Length; j++)
                                {
                                    string DepartureStation = onwResult.Flights[i][j].Origin.AirportCode;
                                    string ArrivalStation = onwResult.Flights[i][j].Destination.AirportCode;

                                    for (int p = 0; p < ssrResponse.SSRSegmentList.Length; p++)
                                    {
                                        SSRSegment sSRSegment = ssrResponse.SSRSegmentList[p];
                                        if (sSRSegment.AvailablePaxSSRList != null && sSRSegment.AvailablePaxSSRList.Length > 0 && sSRSegment.LegKey.DepartureStation == DepartureStation && sSRSegment.LegKey.ArrivalStation == ArrivalStation)
                                        {
                                            availablePaxSSRs_OnwardBag.AddRange(sSRSegment.AvailablePaxSSRList.ToList());
                                            availablePaxSSRs_OnwardMeal.AddRange(sSRSegment.AvailablePaxSSRList.ToList());
                                        }
                                    }
                                }
                            }

                            //Return Result SSR
                            for (int i = 0; i < retResult.Flights.Length; i++)
                            {
                                for (int j = 0; j < retResult.Flights[i].Length; j++)
                                {
                                    string DepartureStation = retResult.Flights[i][j].Origin.AirportCode;
                                    string ArrivalStation = retResult.Flights[i][j].Destination.AirportCode;

                                    for (int p = 0; p < ssrResponse.SSRSegmentList.Length; p++)
                                    {
                                        SSRSegment sSRSegment = ssrResponse.SSRSegmentList[p];
                                        if (sSRSegment.AvailablePaxSSRList != null && sSRSegment.AvailablePaxSSRList.Length > 0 && sSRSegment.LegKey.DepartureStation == DepartureStation && sSRSegment.LegKey.ArrivalStation == ArrivalStation)
                                        {
                                            availablePaxSSRs_ReturnBag.AddRange(sSRSegment.AvailablePaxSSRList.ToList());
                                            availablePaxSSRs_ReturnMeal.AddRange(sSRSegment.AvailablePaxSSRList.ToList());
                                        }
                                    }
                                }
                            }

                            if (availablePaxSSRs_OnwardBag.Count > 0)
                            {
                                GetSpicejetSSRs(ref dtSourceBaggage, "0", availablePaxSSRs_OnwardBag, "Baggage");
                            } //OnwardBaggage

                            if (availablePaxSSRs_ReturnBag.Count > 0)
                            {
                                GetSpicejetSSRs(ref dtSourceBaggage, "1", availablePaxSSRs_ReturnBag, "Baggage");
                            } //ReturnBaggage

                            if (availablePaxSSRs_OnwardMeal.Count > 0)
                            {
                                GetSpicejetSSRs(ref dtSourceBaggage, "0", availablePaxSSRs_OnwardMeal, "Meal");
                            } //OnwardMeal

                            if (availablePaxSSRs_ReturnMeal.Count > 0)
                            {
                                GetSpicejetSSRs(ref dtSourceBaggage, "1", availablePaxSSRs_ReturnMeal, "Meal");
                            } //ReturnMeal

                        }
                    }
                }
            }
            catch (Exception ex)
            {


                //In case of exception in any of the above requests call the clear method.
                //This method will release the itinerary.
                //Clears the session.
                if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature))
                {
                    ClearRequest clearRequest = new ClearRequest();
                    clearRequest.ContractVersion = contractVersion;
                    clearRequest.EnableExceptionStackTrace = false;
                    clearRequest.Signature = loginResponse.Signature;
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SG_ClearRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearRequest);
                        sw.Close();
                    }
                    catch { }
                    ClearResponse clearResponse = bookingAPI.Clear(clearRequest);
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearResponse));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SG_ClearResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearResponse);
                        sw.Close();
                    }
                    catch { }
                }
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Execute Sell Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return dtSourceBaggage;
        }

        /// <summary>
        /// Executes the Sell For Special Round Trip SSR's
        /// </summary>
        /// <param name="signature"></param>
        /// <param name="onwResult"></param>
        /// <param name="retResult"></param>
        /// <returns></returns>
        public BookingUpdateResponseData GetSellResponse(string signature, SearchResult onwResult, SearchResult retResult)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();
            try
            {

                FlightItinerary itineraryONW = new FlightItinerary();
                itineraryONW.Segments = SearchResult.GetSegments(onwResult);
                itineraryONW.TicketAdvisory = onwResult.FareSellKey;

                FlightItinerary itineraryRET = new FlightItinerary();
                itineraryRET.Segments = SearchResult.GetSegments(retResult);
                itineraryRET.TicketAdvisory = retResult.FareSellKey;

                string onwardJourneySellKey = string.Empty;
                string returnJourneySellKey = string.Empty;

                List<string> sellKeysListOnward = new List<string>();
                List<string> sellKeysListReturn = new List<string>();
                for (int p = 0; p < itineraryONW.Segments.Length; p++)
                {
                    sellKeysListOnward.Add(itineraryONW.Segments[p].UapiSegmentRefKey);
                }
                for (int p = 0; p < itineraryRET.Segments.Length; p++)
                {
                    sellKeysListReturn.Add(itineraryRET.Segments[p].UapiSegmentRefKey);
                }

                IEnumerable<string> distinctSellKeysOnward = sellKeysListOnward.Distinct();
                IEnumerable<string> distinctSellKeysReturn = sellKeysListReturn.Distinct();

                if (distinctSellKeysOnward.Count() > 0)
                {
                    foreach (string key in distinctSellKeysOnward)
                    {
                        if (onwardJourneySellKey.Length > 0)
                        {
                            onwardJourneySellKey += "^" + key;
                        }
                        else
                        {
                            onwardJourneySellKey = key;
                        }
                    }
                }
                if (distinctSellKeysReturn.Count() > 0)
                {
                    foreach (string key in distinctSellKeysReturn)
                    {
                        if (returnJourneySellKey.Length > 0)
                        {
                            returnJourneySellKey += "^" + key;
                        }
                        else
                        {
                            returnJourneySellKey = key;
                        }
                    }
                }

                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = currencyCode;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[2];
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = onwardJourneySellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = onwResult.FareSellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1] = new SellKeyList();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].JourneySellKey = returnJourneySellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].FareSellKey = retResult.FareSellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].StandbyPriorityCode = "";

                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < onwResult.FareBreakdown.Length; i++)
                {
                    if (onwResult.FareBreakdown[i] != null && onwResult.FareBreakdown[i].PassengerType != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        for (int h = 0; h < onwResult.FareBreakdown[i].PassengerCount; h++)
                        {
                            switch (onwResult.FareBreakdown[i].PassengerType)
                            {
                                case PassengerType.Adult:
                                    priceType.PaxType = "ADT";
                                    break;
                                case PassengerType.Child:
                                    priceType.PaxType = "CHD";
                                    break;

                            }
                            paxPriceTypes.Add(priceType);
                        }
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                if (!string.IsNullOrEmpty(promoCode))//Promo Code Implementation for corporate fare
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale = new TypeOfSale();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes = new string[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes[0] = "C";//Corporate Fare
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.PromotionCode = promoCode;
                }

                //New object creation for signature inclusion.
                SellRequest sellRequestObj = new SellRequest();
                sellRequestObj.ContractVersion = contractVersion;
                sellRequestObj.SellRequestData = requestData;
                sellRequestObj.Signature = signature;
                sellRequestObj.EnableExceptionStackTrace = false;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGSellRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellRequestObj);
                    sw.Close();
                }
                catch { }
                SellResponse sellResponse = bookingAPI.Sell(sellRequestObj);
                if (sellResponse != null && sellResponse.BookingUpdateResponseData != null)
                {
                    responseData = sellResponse.BookingUpdateResponseData;
                }
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + appUserId + "_SGSellResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, responseData);
                    sw.Close();
                }
                catch { }

                //Added by lokesh on 20/12/2018
                //To Avoid Duplicate Bookings
                //Checking the segment count with the SSR response segment count
                //If that doesn't match we will call the LogOut method.
                //This Validation Does not allow the additional bookings that are already there in the signature.
                if (responseData != null && responseData.Success != null && responseData.Success.PNRAmount != null)
                {
                    int segCount = distinctSellKeysOnward.Count();
                    if (distinctSellKeysReturn.Count() > 0)
                    {
                        segCount = segCount + distinctSellKeysReturn.Count();
                    }
                    if (segCount != Convert.ToInt32(responseData.Success.PNRAmount.SegmentCount))
                    {
                        Logout(signature);
                        throw new Exception("(Spicejet)Segment Count Mismatch ! Please Search Again..");
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SpiceJet)Failed to Execute Sell Method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return responseData;
        }

        /// <summary>
        /// Gets the updated GST Fare Break Up For Combination Search
        /// </summary>
        /// <param name="onwResult"></param>
        /// <param name="retResult"></param>
        /// <param name="request"></param>
        public void GetUpdatedGSTFareBreakUp(ref SearchResult onwResult, ref SearchResult retResult, SearchRequest request)
        {
            try
            {
                //If the lead pax supplies the GST input.
                //Then Get the updated GST farebreak up.
                LogonResponse logonResponse = null;
                if (!string.IsNullOrEmpty(onwResult.RepriceErrorMessage))
                {
                    logonResponse = new LogonResponse();
                    logonResponse.Signature = onwResult.RepriceErrorMessage;
                }

                //Get the bifurcation of taxes after GST input.
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature))
                {
                    GetBookingFromStateResponse updatedGSTBreakup = GetBookingFromState(logonResponse.Signature);

                    //Onward Result
                    if (updatedGSTBreakup != null && updatedGSTBreakup.BookingData != null && updatedGSTBreakup.BookingData.Journeys != null && updatedGSTBreakup.BookingData.Journeys.Length > 0)
                    {
                        rateOfExchange = exchangeRates[updatedGSTBreakup.BookingData.CurrencyCode];

                        List<PaxFare> adultPaxfare = new List<PaxFare>();
                        List<PaxFare> childPaxfare = new List<PaxFare>();


                        List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
                        List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

                        paxTypeTaxBreakUp.Clear();
                        adtPaxTaxBreakUp.Clear();
                        chdPaxTaxBreakUp.Clear();
                        inftPaxTaxBreakUp.Clear();

                        int journey = 0;
                        if (updatedGSTBreakup.BookingData.Journeys != null && updatedGSTBreakup.BookingData.Journeys.Length > 0)
                        {
                            if (updatedGSTBreakup.BookingData.Journeys[journey] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length > 0)
                            {
                                for (int segment = 0; segment < updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length; segment++)
                                {
                                    if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length > 0)
                                    {
                                        for (int fare = 0; fare < updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length; fare++)
                                        {
                                            if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).Count() > 0)
                                            {
                                                adultPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).FirstOrDefault());
                                            }
                                            if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).Count() > 0)
                                            {
                                                childPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).FirstOrDefault());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // For infant updating from intial result fare
                        Fare infantFare = onwResult.FareBreakdown.FirstOrDefault(P => (P.PassengerType == PassengerType.Infant));
                        onwResult.Price = new PriceAccounts();
                        onwResult.BaseFare = 0;
                        onwResult.Tax = 0;
                        onwResult.TotalFare = 0;
                        onwResult.FareBreakdown = null;
                        int fareBreakDownCount = 0;
                        if (request.AdultCount > 0)
                        {
                            fareBreakDownCount = 1;
                        }
                        if (request.ChildCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        if (request.InfantCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        onwResult.FareBreakdown = new Fare[fareBreakDownCount];
                        if (request.AdultCount > 0)
                        {
                            onwResult.FareBreakdown[0] = new Fare();
                            onwResult.FareBreakdown[0].PassengerCount = request.AdultCount;
                            onwResult.FareBreakdown[0].PassengerType = PassengerType.Adult;
                        }
                        if (request.ChildCount > 0)
                        {
                            onwResult.FareBreakdown[1] = new Fare();
                            onwResult.FareBreakdown[1].PassengerCount = request.ChildCount;
                            onwResult.FareBreakdown[1].PassengerType = PassengerType.Child;
                        }

                        if (adultPaxfare != null && adultPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareAdult in adultPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareAdult.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (onwResult.FareBreakdown[0] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                    double baseFareAdult = (double)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.FareBreakdown[0].BaseFare += baseFareAdult;
                                                    onwResult.FareBreakdown[0].TotalFare += baseFareAdult;
                                                    onwResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.Price.PublishedFare += (decimal)baseFareAdult;
                                                    onwResult.Price.SupplierPrice += charge.Amount;
                                                    break;

                                                case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                    double discountAdult = (double)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.FareBreakdown[0].BaseFare = onwResult.FareBreakdown[0].BaseFare - discountAdult;
                                                    onwResult.FareBreakdown[0].TotalFare = onwResult.FareBreakdown[0].TotalFare - discountAdult;
                                                    onwResult.FareBreakdown[0].SupplierFare = onwResult.FareBreakdown[0].SupplierFare - (double)(charge.Amount * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.Price.PublishedFare = (decimal)(onwResult.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                                                    onwResult.Price.SupplierPrice = onwResult.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                case SpiceJet.SGBooking.ChargeType.Calculated:
                                                case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                //case SpiceJet.SGBooking.ChargeType.Discount:
                                                case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                case SpiceJet.SGBooking.ChargeType.Note:
                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.FareBreakdown[0].TotalFare += tax;
                                                    onwResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.Price.Tax += (decimal)tax;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                        onwResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[0].PassengerCount);
                                                    }
                                                    onwResult.Price.SupplierPrice += charge.Amount;
                                                    adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (childPaxfare != null && childPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareChild in childPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareChild.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (onwResult.FareBreakdown[1] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                    double baseFareChild = (double)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[1].PassengerCount);
                                                    onwResult.FareBreakdown[1].BaseFare += baseFareChild;
                                                    onwResult.FareBreakdown[1].TotalFare += baseFareChild;
                                                    onwResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * onwResult.FareBreakdown[1].PassengerCount);
                                                    onwResult.Price.PublishedFare += (decimal)baseFareChild;
                                                    onwResult.Price.SupplierPrice += charge.Amount;
                                                    break;

                                                case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                    double discountChild = (double)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[1].PassengerCount);
                                                    onwResult.FareBreakdown[1].BaseFare = onwResult.FareBreakdown[1].BaseFare - discountChild;
                                                    onwResult.FareBreakdown[1].TotalFare = onwResult.FareBreakdown[1].TotalFare - discountChild;
                                                    onwResult.FareBreakdown[1].SupplierFare = onwResult.FareBreakdown[0].SupplierFare - (double)(charge.Amount * onwResult.FareBreakdown[0].PassengerCount);
                                                    onwResult.Price.PublishedFare = (decimal)(onwResult.Price.PublishedFare - Convert.ToDecimal(discountChild));
                                                    onwResult.Price.SupplierPrice = onwResult.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                case SpiceJet.SGBooking.ChargeType.Calculated:
                                                case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                //case SpiceJet.SGBooking.ChargeType.Discount:
                                                case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                case SpiceJet.SGBooking.ChargeType.Note:
                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[1].PassengerCount);
                                                    onwResult.FareBreakdown[1].TotalFare += tax;
                                                    onwResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * onwResult.FareBreakdown[1].PassengerCount);
                                                    onwResult.Price.Tax += (decimal)tax;
                                                    onwResult.Price.SupplierPrice += charge.Amount;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                        onwResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * onwResult.FareBreakdown[1].PassengerCount);
                                                    }
                                                    chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (request.InfantCount > 0)
                        {

                            if (infantFare != null)
                            {

                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                {
                                    onwResult.FareBreakdown[2] = infantFare;
                                }
                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                {

                                    onwResult.FareBreakdown[1] = infantFare;

                                }

                                onwResult.Price.PublishedFare += (decimal)infantFare.BaseFare;
                                onwResult.Price.Tax += (decimal)infantFare.Tax;
                                onwResult.Price.SupplierPrice += (decimal)infantFare.SupplierFare;

                            }

                        }

                        if (adtPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                        }
                        if (chdPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                        }
                        if (inftPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                        }
                        if (paxTypeTaxBreakUp.Count > 0)
                        {
                            onwResult.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                        }

                        onwResult.Price.SupplierCurrency = currencyCode;
                        onwResult.BaseFare = (double)onwResult.Price.PublishedFare;
                        onwResult.Tax = (double)onwResult.Price.Tax;
                        onwResult.TotalFare = (double)Math.Round((onwResult.BaseFare + onwResult.Tax), agentDecimalValue);
                    }

                    //Return Result
                    if (updatedGSTBreakup != null && updatedGSTBreakup.BookingData != null && updatedGSTBreakup.BookingData.Journeys != null && updatedGSTBreakup.BookingData.Journeys.Length > 0)
                    {
                        rateOfExchange = exchangeRates[updatedGSTBreakup.BookingData.CurrencyCode];

                        List<PaxFare> adultPaxfare = new List<PaxFare>();
                        List<PaxFare> childPaxfare = new List<PaxFare>();


                        List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
                        List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

                        paxTypeTaxBreakUp.Clear();
                        adtPaxTaxBreakUp.Clear();
                        chdPaxTaxBreakUp.Clear();
                        inftPaxTaxBreakUp.Clear();

                        int journey = 1;//Return Journey
                        if (updatedGSTBreakup.BookingData.Journeys != null && updatedGSTBreakup.BookingData.Journeys.Length > 0)
                        {
                            if (updatedGSTBreakup.BookingData.Journeys[journey] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length > 0)
                            {
                                for (int segment = 0; segment < updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length; segment++)
                                {
                                    if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length > 0)
                                    {
                                        for (int fare = 0; fare < updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length; fare++)
                                        {
                                            if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).Count() > 0)
                                            {
                                                adultPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).FirstOrDefault());
                                            }
                                            if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).Count() > 0)
                                            {
                                                childPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).FirstOrDefault());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // For infant updating from intial result fare
                        Fare infantFare = retResult.FareBreakdown.FirstOrDefault(P => (P.PassengerType == PassengerType.Infant));
                        retResult.Price = new PriceAccounts();
                        retResult.BaseFare = 0;
                        retResult.Tax = 0;
                        retResult.TotalFare = 0;
                        retResult.FareBreakdown = null;
                        int fareBreakDownCount = 0;
                        if (request.AdultCount > 0)
                        {
                            fareBreakDownCount = 1;
                        }
                        if (request.ChildCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        if (request.InfantCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        retResult.FareBreakdown = new Fare[fareBreakDownCount];
                        if (request.AdultCount > 0)
                        {
                            retResult.FareBreakdown[0] = new Fare();
                            retResult.FareBreakdown[0].PassengerCount = request.AdultCount;
                            retResult.FareBreakdown[0].PassengerType = PassengerType.Adult;
                        }
                        if (request.ChildCount > 0)
                        {
                            retResult.FareBreakdown[1] = new Fare();
                            retResult.FareBreakdown[1].PassengerCount = request.ChildCount;
                            retResult.FareBreakdown[1].PassengerType = PassengerType.Child;
                        }

                        if (adultPaxfare != null && adultPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareAdult in adultPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareAdult.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (retResult.FareBreakdown[0] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                    double baseFareAdult = (double)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.FareBreakdown[0].BaseFare += baseFareAdult;
                                                    retResult.FareBreakdown[0].TotalFare += baseFareAdult;
                                                    retResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.Price.PublishedFare += (decimal)baseFareAdult;
                                                    retResult.Price.SupplierPrice += charge.Amount;
                                                    break;

                                                case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                    double discountAdult = (double)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.FareBreakdown[0].BaseFare = retResult.FareBreakdown[0].BaseFare - discountAdult;
                                                    retResult.FareBreakdown[0].TotalFare = retResult.FareBreakdown[0].TotalFare - discountAdult;
                                                    retResult.FareBreakdown[0].SupplierFare = retResult.FareBreakdown[0].SupplierFare - (double)(charge.Amount * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.Price.PublishedFare = (decimal)(retResult.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                                                    retResult.Price.SupplierPrice = retResult.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                case SpiceJet.SGBooking.ChargeType.Calculated:
                                                case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                //case SpiceJet.SGBooking.ChargeType.Discount:
                                                case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                case SpiceJet.SGBooking.ChargeType.Note:
                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.FareBreakdown[0].TotalFare += tax;
                                                    retResult.FareBreakdown[0].SupplierFare += (double)(charge.Amount * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.Price.Tax += (decimal)tax;
                                                    retResult.Price.SupplierPrice += charge.Amount;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                        retResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[0].PassengerCount);
                                                    }
                                                    adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (childPaxfare != null && childPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareChild in childPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareChild.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (retResult.FareBreakdown[1] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case SpiceJet.SGBooking.ChargeType.FarePrice:
                                                    double baseFareChild = (double)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[1].PassengerCount);
                                                    retResult.FareBreakdown[1].BaseFare += baseFareChild;
                                                    retResult.FareBreakdown[1].TotalFare += baseFareChild;
                                                    retResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * retResult.FareBreakdown[1].PassengerCount);
                                                    retResult.Price.PublishedFare += (decimal)baseFareChild;
                                                    retResult.Price.SupplierPrice += charge.Amount;
                                                    break;

                                                case SpiceJet.SGBooking.ChargeType.PromotionDiscount:
                                                    double discountChild = (double)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[1].PassengerCount);
                                                    retResult.FareBreakdown[1].BaseFare = retResult.FareBreakdown[1].BaseFare - discountChild;
                                                    retResult.FareBreakdown[1].TotalFare = retResult.FareBreakdown[1].TotalFare - discountChild;
                                                    retResult.FareBreakdown[1].SupplierFare = retResult.FareBreakdown[0].SupplierFare - (double)(charge.Amount * retResult.FareBreakdown[0].PassengerCount);
                                                    retResult.Price.PublishedFare = (decimal)(retResult.Price.PublishedFare - Convert.ToDecimal(discountChild));
                                                    retResult.Price.SupplierPrice = retResult.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceCancelFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceFee:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServiceMarkup:
                                                case SpiceJet.SGBooking.ChargeType.AddOnServicePrice:
                                                case SpiceJet.SGBooking.ChargeType.Calculated:
                                                case SpiceJet.SGBooking.ChargeType.ConnectionAdjustmentAmount:
                                                //case SpiceJet.SGBooking.ChargeType.Discount:
                                                case SpiceJet.SGBooking.ChargeType.DiscountPoints:
                                                case SpiceJet.SGBooking.ChargeType.FarePoints:
                                                case SpiceJet.SGBooking.ChargeType.FareSurcharge:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedAddOnServiceFee:
                                                //case SpiceJet.SGBooking.ChargeType.IncludedTravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Loyalty:
                                                case SpiceJet.SGBooking.ChargeType.Note:
                                                case SpiceJet.SGBooking.ChargeType.ServiceCharge:
                                                case SpiceJet.SGBooking.ChargeType.Tax:
                                                case SpiceJet.SGBooking.ChargeType.TravelFee:
                                                case SpiceJet.SGBooking.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[1].PassengerCount);
                                                    retResult.FareBreakdown[1].TotalFare += tax;
                                                    retResult.FareBreakdown[1].SupplierFare += (double)(charge.Amount * retResult.FareBreakdown[1].PassengerCount);
                                                    retResult.Price.Tax += (decimal)tax;
                                                    retResult.Price.SupplierPrice += charge.Amount;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                        retResult.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * retResult.FareBreakdown[1].PassengerCount);
                                                    }
                                                    chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (request.InfantCount > 0)
                        {

                            if (infantFare != null)
                            {

                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                {
                                    retResult.FareBreakdown[2] = infantFare;
                                }
                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                {

                                    retResult.FareBreakdown[1] = infantFare;

                                }

                                retResult.Price.PublishedFare += (decimal)infantFare.BaseFare;
                                retResult.Price.Tax += (decimal)infantFare.Tax;
                                retResult.Price.SupplierPrice += (decimal)infantFare.SupplierFare;

                            }

                        }

                        if (adtPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                        }
                        if (chdPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                        }
                        if (inftPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                        }
                        if (paxTypeTaxBreakUp.Count > 0)
                        {
                            retResult.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                        }

                        retResult.Price.SupplierCurrency = currencyCode;
                        retResult.BaseFare = (double)retResult.Price.PublishedFare;
                        retResult.Tax = (double)retResult.Price.Tax;
                        retResult.TotalFare = (double)Math.Round((retResult.BaseFare + retResult.Tax), agentDecimalValue);
                    }

                }

            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, 1, "(SG)Failed to GetUpdatedGSTFareBreakUp. Error : " + ex.ToString(), "");
                throw ex;
            }

        }

        //Added  Baggage & new meals codes 
        public void GetSpicejetSSRs(ref DataTable dtSourceBaggage, string type, List<AvailablePaxSSR> paxSSRs, string ssrType)
        {
            try
            {
                if (dtSourceBaggage.Columns.Count == 0)
                {
                    dtSourceBaggage.Columns.Add("Code", typeof(string));
                    dtSourceBaggage.Columns.Add("Price", typeof(decimal));
                    dtSourceBaggage.Columns.Add("Group", typeof(int));
                    dtSourceBaggage.Columns.Add("Currency", typeof(string));
                    dtSourceBaggage.Columns.Add("Description", typeof(string));
                    dtSourceBaggage.Columns.Add("QtyAvailable", typeof(int));
                }
                List<string> ssrCodes = ssrType == "Baggage" ? baggageCodes : mealCodes;
                foreach (string ssrCode in ssrCodes)
                {
                    AvailablePaxSSR availableSSR = null;
                    List<AvailablePaxSSR> availablePaxSSRs = null;
                    if (ssrType == "Baggage")
                        availableSSR = paxSSRs.Where(m => m.SSRCode.Contains(ssrCode)).FirstOrDefault();
                    else
                        availablePaxSSRs = paxSSRs.Where(m => m.SSRCode.Contains(ssrCode)).ToList();
                    if (availableSSR != null || (availablePaxSSRs != null && availablePaxSSRs.Count>0))
                    {
                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                        if (availableSSR != null && availableSSR.PaxSSRPriceList != null)
                        {
                            paxSSRPrices.AddRange(availableSSR.PaxSSRPriceList);
                        }
                        else if (availablePaxSSRs != null && availablePaxSSRs.Count > 0)
                        {
                            for (int i = 0; i < availablePaxSSRs.Count; i++)
                            {
                                paxSSRPrices.AddRange(availablePaxSSRs[i].PaxSSRPriceList);
                            }
                        }
                        for (int k = 0; k < paxSSRPrices.Count; k++)
                        {
                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                        }

                        decimal baggagePrice = charges.Where(o => o.ChargeType == SpiceJet.SGBooking.ChargeType.ServiceCharge || o.ChargeType == SpiceJet.SGBooking.ChargeType.Tax).Sum(item => item.Amount);
                        if (charges.Count > 0 && !string.IsNullOrEmpty(charges[0].CurrencyCode))
                            rateOfExchange = exchangeRates[charges[0].CurrencyCode];

                        DataRow dr = dtSourceBaggage.NewRow();
                        dr["Code"] = ssrCode;
                        dr["Price"] = Math.Round(Convert.ToDecimal(baggagePrice) * rateOfExchange, agentDecimalValue);
                        dr["Group"] = type;
                        dr["Currency"] = agentBaseCurrency;
                        switch (ssrCode)
                        {
                            case "EB05":
                                dr["Description"] = "5Kg Excess Baggage";
                                break;
                            case "EB10":
                                dr["Description"] = "10Kg Excess Baggage";
                                break;
                            case "EB15":
                                dr["Description"] = "15Kg Excess Baggage";
                                break;
                            case "EB20":
                                dr["Description"] = "20Kg Excess Baggage";
                                break;
                            case "EB30":
                                dr["Description"] = "30Kg Excess Baggage";
                                break;

                            //meal codes
                            case "VGML":
                                dr["Description"] = "Veg Meal";
                                break;
                            case "NVML":
                                dr["Description"] = "Non Veg Meal";
                                break;
                            case "NVSW":
                                dr["Description"] = "Non Veg sandwich";
                                break;
                            case "VGSW":
                                dr["Description"] = "Veg Sandwich";
                                break;

                            case "NCC1":
                                dr["Description"] = "Grilled Chicken Breast with Mushroom Sauce, Yellow Rice, Sautéed Carrots & Beans Baton";
                                break;
                            case "NCC2":
                                dr["Description"] = "Chicken in Red Thai Curry with Steamed Rice";
                                break;
                            case "NCC6":
                                dr["Description"] = "Chicken schezwan on bed of fried rice";
                                break;
                            case "NCC4":
                                dr["Description"] = "Tandoori Chicken tangri with chicken haryali tikka & vegetable shami kebab.";
                                break;
                            case "NCC5":
                                dr["Description"] = "Tawa Fish masala on bed of  Steamed rice with tadka masoor dal";
                                break;
                            case "VCC5":
                                dr["Description"] = "Vegetable Pasta in Neapolitan sauce";
                                break;
                            case "VCC2":
                                dr["Description"] = "Vegtable in Red Thai Curry with Steamed Rice";
                                break;
                            case "VCC6":
                                dr["Description"] = "Vegetable Daliya";
                                break;
                            case "JNML":
                                dr["Description"] = "Jain Hot Meal";
                                break;
                            case "JNSW":
                                dr["Description"] = "Jain Cold Sandwich (current Cucumber and Tomato sandwich)";
                                break;
                            case "DNVL":
                                dr["Description"] = "Non - Vegetarian Diabetic Hot Meal";
                                break;
                            case "DBML":
                                dr["Description"] = "Vegetarian Diabetic Hot Meal";
                                break;
                            case "GFNV":
                                dr["Description"] = "Non - Vegetarian Gluten-free Hot Meal";
                                break;
                            case "GFVG":
                                dr["Description"] = "Vegetarian Gluten-free Hot Meal";
                                break;
                            case "GFCM":
                                dr["Description"] = "Vegetarian Gluten-free Cold Meal (Dhokla) ";
                                break;
                            case "FPML":
                                dr["Description"] = "Fruit Platter ";
                                break;
                            case "LCVS":
                                dr["Description"] = "Low cal salad Vegetarian";
                                break;
                            case "LCNS":
                                dr["Description"] = "Low cal salad Non - Vegetarian";
                                break;
                            case "GOBX":
                                dr["Description"] = "Goodie Box";
                                break;
                            case "VMAX":
                                dr["Description"] = "Veg Meal (Free Meal)";
                                break;
                            case "NMAX":
                                dr["Description"] = "Non Veg Meal (Free Meal)";
                                break;
                        }

                        dr["QtyAvailable"] = 5000;
                        dtSourceBaggage.Rows.Add(dr);
                    }
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo) Error in GetIndigoSSRs() . Reason : " + ex.ToString(), "");
                throw ex;
            }
        }

    }

    #endregion
}

