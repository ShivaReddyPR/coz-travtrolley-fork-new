﻿using PassiveSegment.BO;
using System;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;

namespace PassiveSegment
{
    public class PassiveSegmentServiceManual
    {
        /// <summary>
        /// To create and send soap request to UAPI for passive segment reservation
        /// </summary>
        /// <param name="clsPassiveFlightItinerary"></param>
        /// <param name="clsUAPICredentials"></param>
        /// <param name="xmllogPath"></param>
        /// <returns></returns>
        public string CreatePassiveReservation(PassiveFlightItinerary clsPassiveFlightItinerary, UAPICredentials clsUAPICredentials, string xmllogPath)
        {
            string sURLCode = string.Empty; string sOrigin = string.Empty; string sDestination = string.Empty;

            try
            {
                /* clsPassiveFlightItinerary.Passenger = clsPassiveFlightItinerary.Passenger.Where(x => !string.IsNullOrEmpty(x.FirstName)).ToArray();
                 clsPassiveFlightItinerary.Segments = clsPassiveFlightItinerary.Segments.Where(x => !string.IsNullOrEmpty(x.Origin)).ToArray();

                 sOrigin = clsPassiveFlightItinerary.Segments.Select(x => x.Origin).FirstOrDefault();
                 sDestination = clsPassiveFlightItinerary.Segments.Select(x => x.Destination).LastOrDefault();
                 sDestination = sOrigin == sDestination ? clsPassiveFlightItinerary.Segments.Where(y => y.Group == 0).Select(x => x.Destination).LastOrDefault()
                     : sDestination;

                 StringBuilder createreq = new StringBuilder();
                 XmlWriter writer = XmlTextWriter.Create(createreq);
                 writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                 writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                 writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                 writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                 writer.WriteStartElement("soap", "Body", null);
                 writer.WriteStartElement("univ", "PassiveCreateReservationReq", "http://www.travelport.com/schema/universal_v42_0");
                 writer.WriteAttributeString("xmlns", "com", null, "http://www.travelport.com/schema/common_v42_0");
                 writer.WriteAttributeString("xmlns", "pas", null, "http://www.travelport.com/schema/passive_v42_0");
                 writer.WriteAttributeString("AuthorizedBy", "test");
                 writer.WriteAttributeString("ProviderCode", "1G");
                 writer.WriteAttributeString("TargetBranch", clsUAPICredentials.TargetBranch);

                 writer.WriteStartElement("com", "BillingPointOfSaleInfo", null);
                 writer.WriteAttributeString("OriginApplication", "UAPI");
                 writer.WriteEndElement();

                 foreach (FlightPassenger passenger in clsPassiveFlightItinerary.Passenger)
                 {
                     string tempvar = passenger.Type == PassengerType.Adult ? "ADT" : passenger.Type == PassengerType.Child ? "CNN" : "INF";

                     writer.WriteStartElement("com", "BookingTraveler", null);
                     writer.WriteAttributeString("TravelerType", tempvar);

                     if (passenger.DateOfBirth != DateTime.MinValue)
                         writer.WriteAttributeString("DOB", passenger.DateOfBirth.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture));
                     else if (passenger.Type == PassengerType.Infant && passenger.DateOfBirth == DateTime.MinValue)
                         writer.WriteAttributeString("DOB", DateTime.Today.AddDays(-365).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture));

                     writer.WriteStartElement("com", "BookingTravelerName", null);
                     writer.WriteAttributeString("Prefix", passenger.Title);
                     writer.WriteAttributeString("First", passenger.FirstName);
                     writer.WriteAttributeString("Last", passenger.LastName);
                     writer.WriteEndElement();

                     writer.WriteStartElement("com", "PhoneNumber", null);
                     writer.WriteAttributeString("Type", "Mobile");
                     writer.WriteAttributeString("Number", clsPassiveFlightItinerary.Passenger[0].CellPhone);
                     writer.WriteEndElement();

                     writer.WriteEndElement();
                 }*/

                /* To pass DI info to supplier based on flex information added by user */
                /*clsPassiveFlightItinerary.Passenger.Where(p => p.FlexDetailsList != null && p.FlexDetailsList.Count > 0).ToList().
                    ForEach(pa =>
                    {
                        pa.FlexDetailsList.Where(y => !string.IsNullOrEmpty(y.FlexData)).ToList().ForEach(x =>
                        {
                            x.FlexData = (!string.IsNullOrEmpty(x.FlexData)) ? x.FlexData.Replace("@", "//").Replace("_", "--") : x.FlexData;

                            writer.WriteStartElement("com", "AccountingRemark", null);
                            writer.WriteAttributeString("Category", "FT");
                            writer.WriteAttributeString("TypeInGds", "Other");
                            writer.WriteStartElement("com", "RemarkData", null);
                            writer.WriteValue((x.FlexGDSprefix + x.FlexData).Length < 84 ? x.FlexGDSprefix + x.FlexData :
                                    (x.FlexGDSprefix + x.FlexData).Substring(0, 84));
                            writer.WriteEndElement();
                            writer.WriteStartElement("com", "BookingTravelerRef", null);
                            writer.WriteValue(x.PaxId.ToString());
                            writer.WriteEndElement();
                            writer.WriteEndElement();
                        });
                    });

                for (int s = 0; s < clsPassiveFlightItinerary.Segments.Length; s++)
                {
                    writer.WriteStartElement("pas", "PassiveSegment", null);
                    writer.WriteAttributeString("Key", s.ToString());
                    writer.WriteAttributeString("SupplierCode", clsPassiveFlightItinerary.Segments[s].Airline.ToUpper());
                    writer.WriteAttributeString("Status", "AK");
                    writer.WriteAttributeString("StartDate", clsPassiveFlightItinerary.Segments[s].DepartureTime.ToString("yyyy-MM-dd") + "T" +
                        clsPassiveFlightItinerary.Segments[s].DepartureTime.ToString("HH:mm:00.000") + "+04:00");
                    writer.WriteAttributeString("NumberOfItems", clsPassiveFlightItinerary.Passenger.Where(x => x.Type != PassengerType.Infant).ToArray().Length.ToString());
                    if (clsPassiveFlightItinerary.Segments[s].ArrivalTime != null && clsPassiveFlightItinerary.Segments[s].ArrivalTime != DateTime.MinValue)
                        writer.WriteAttributeString("EndDate", clsPassiveFlightItinerary.Segments[s].ArrivalTime.ToString("yyyy-MM-dd") + "T" +
                            clsPassiveFlightItinerary.Segments[s].ArrivalTime.ToString("HH:mm:00.000") + "+04:00");
                    writer.WriteAttributeString("Origin", clsPassiveFlightItinerary.Segments[s].Origin.ToUpper());
                    writer.WriteAttributeString("Destination", clsPassiveFlightItinerary.Segments[s].Destination.ToUpper());
                    writer.WriteAttributeString("FlightNumber", clsPassiveFlightItinerary.Segments[s].FlightNumber.Trim().ToUpper());
                    writer.WriteAttributeString("ClassOfService", clsPassiveFlightItinerary.Segments[s].BookingClass.ToUpper());
                    writer.WriteAttributeString("SegmentType", "Air");

                    if (s == clsPassiveFlightItinerary.Segments.Length - 1)
                    {
                        writer.WriteStartElement("pas", "Amount", null);
                        writer.WriteAttributeString("Type", "Due");
                        writer.WriteAttributeString("AmountDuePaid", clsPassiveFlightItinerary.sSupplierCurrency + clsPassiveFlightItinerary.ItineraryAmountDue.ToString());
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }

                for (int s = 0; s < clsPassiveFlightItinerary.Segments.Length; s++)
                {
                    writer.WriteStartElement("pas", "PassiveRemark", null);
                    writer.WriteAttributeString("PassiveSegmentRef", s.ToString());
                    writer.WriteStartElement("pas", "Text", null);
                    writer.WriteValue(clsPassiveFlightItinerary.PNR.ToUpper());
                    writer.WriteEndElement();
                    writer.WriteStartElement("pas", "Type", null);
                    writer.WriteValue("CF");
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }

                for (int s = 0; s < clsPassiveFlightItinerary.Segments.Length; s++)
                {
                    writer.WriteStartElement("pas", "AssociatedRemark", null);
                    writer.WriteAttributeString("SegmentRef", s.ToString());
                    writer.WriteStartElement("com", "RemarkData", null);
                    writer.WriteValue("0" + clsPassiveFlightItinerary.Segments[s].Airline.ToUpper() + clsPassiveFlightItinerary.Segments[s].FlightNumber + "M" +
                        clsPassiveFlightItinerary.Segments[s].DepartureTime.ToString("ddMMM").ToUpper() + clsPassiveFlightItinerary.Segments[s].Origin.ToUpper() +
                        clsPassiveFlightItinerary.Segments[s].Destination.ToUpper() + "NN1");
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();

                writer.Flush();
                writer.Close();

                WriteLogFileXML(createreq.ToString(),
                    xmllogPath + "_" + sOrigin + "_" + sDestination + "_UAPI_PassiveSegmentCreateRequest_"); */



                // startt
                string createreq = string.Empty;
                UAPICredentials clsUAPICredentialsTEMP = new UAPICredentials();
                //test Credentials
                clsUAPICredentialsTEMP.EndPointURL = "https://emea.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/";
                clsUAPICredentialsTEMP.UserName = "Universal API/uAPI7997403705-43d54fd1";
                clsUAPICredentialsTEMP.Password = "x-5Y6P}c!4";

                //Live Credentials
                //clsUAPICredentialsTEMP.EndPointURL = "https://emea.universal-api.travelport.com/B2BGateway/connect/uAPI/";
                //clsUAPICredentialsTEMP.UserName = "Universal API/uAPI1358885613-c2ec105f";
                //clsUAPICredentialsTEMP.Password = "M}y2&6Tt7D";
                XmlDocument docRes = new XmlDocument();
                docRes.Load(@"D:\Temp\b2b\misc\WrongREq.xml");
                createreq = docRes.InnerXml.ToString();
                // end
                createreq = createreq.Replace("–", "-");
                const string contentType = "text/xml";
                Version _protocolVersion = HttpVersion.Version11;
                
                ServicePoint sp = ServicePointManager.FindServicePoint(new Uri(clsUAPICredentialsTEMP.EndPointURL + "/PassiveService"));
                sp.Expect100Continue = false;
                sp.UseNagleAlgorithm = false;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(clsUAPICredentialsTEMP.EndPointURL + "/PassiveService");
                request.Method = "POST";

                request.ContentType = contentType;
                request.KeepAlive = true;
                request.ProtocolVersion = HttpVersion.Version11;

                //Basic Authentication required to process the request.               
                string _auth = string.Format("{0}:{1}", clsUAPICredentialsTEMP.UserName, clsUAPICredentialsTEMP.Password);
                string _enc = Convert.ToBase64String(Encoding.ASCII.GetBytes(_auth));
                string _cred = string.Format("{0} {1}", "Basic", _enc);
                request.Headers.Add(HttpRequestHeader.Authorization, _cred);
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                byte[] lbPostBuffer = Encoding.Default.GetBytes(createreq.ToString());
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                request.ContentLength = lbPostBuffer.Length;
                Stream PostStream = request.GetRequestStream();
                PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                PostStream.Close();

                string reservationRsp = string.Empty;
                using (WebResponse response = request.GetResponse())
                {
                    HttpWebResponse httpResponse = response as HttpWebResponse;
                    // handle if response is a compressed stream
                    using (Stream dataStream = GetStreamForResponse(httpResponse))
                    {
                        Encoding enc = System.Text.Encoding.GetEncoding(1252);
                        using (StreamReader reader = new StreamReader(dataStream, enc))
                        {
                            reservationRsp = reader.ReadToEnd();
                        }
                    }
                }

                WriteLogFileXML(reservationRsp,
                    xmllogPath + "_" + sOrigin + "_" + sDestination + "_UAPI_PassiveSegmentCreateResponse_");

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(new StringReader(reservationRsp));
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                nsmgr.AddNamespace("universal", "http://www.travelport.com/schema/universal_v42_0");

                sURLCode = Convert.ToString(xmlDoc.
                    SelectSingleNode("/soap:Envelope/soap:Body/universal:PassiveCreateReservationRsp/universal:UniversalRecord", nsmgr).Attributes["LocatorCode"].Value);
            }
            catch (SoapException se)
            {
                WriteLogFileText(xmllogPath + "_" + sOrigin + "_" + sDestination + "_UAPI_PassiveSegmentSoapError_", Convert.ToString(se.GetBaseException()));
                throw se;
            }
            catch (Exception ex)
            {
                WriteLogFileText(xmllogPath + "_" + sOrigin + "_" + sDestination + "_UAPI_PassiveSegmentError_", Convert.ToString(ex.GetBaseException()));
                throw ex;
            }

            return sURLCode;
        }

        /// <summary>
        /// To generate text file for the given string of data
        /// </summary>
        /// <param name="data"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static void WriteLogFileText(string fileName, string data)
        {
            string filePath = string.Empty;
            try
            {
                File.WriteAllText(fileName + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".txt", data);
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        /// <summary>
        /// To generate xml file based on given string of xml data
        /// </summary>
        /// <param name="data"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static string WriteLogFileXML(string data, string fileName)
        {
            string filePath = string.Empty;
            try
            {
                XmlDocument doc = new XmlDocument();
                filePath = fileName + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                doc.LoadXml(data);
                doc.Save(filePath);
            }
            catch (Exception ex)
            {
                WriteLogFileText(fileName + "_Error_", Convert.ToString(ex.GetBaseException()));
            }
            return filePath;
        }

        /// <summary>
        /// To get the exact stream of data based on encoding format
        /// </summary>
        /// <param name="webResponse"></param>
        /// <returns></returns>
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }
    }
}
