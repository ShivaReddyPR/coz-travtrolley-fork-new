using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace PassiveSegment.BO
{
    public class UAPICredentials
    {
        public string UserName = string.Empty;
        public string Password = string.Empty;
        public string TargetBranch = string.Empty;
        public string EndPointURL = string.Empty;
    }

    public enum ItineraryStatus
    {
        OnHold = 1,
        Ticketed = 2
    }

    public enum BookingSource
    {
        WorldSpan = 1,
        Abacus = 2,
        SpiceJet = 3,
        Amadeus = 4,
        Galileo = 5,
        Indigo = 6,
        Paramount = 7,
        AirDeccan = 8,
        Mdlr = 9,
        GoAir = 10,
        ThirdParty = 11,
        Sama = 12,
        AirArabia = 13,
        AirIndiaExpressIntl = 14,
        //AirIndiaExpressDom = 15,
        HermesAirLine = 16,
        FlyDubai = 17,
        AlJazeera = 18,
        UAPI = 20,
        TBOAir = 21,
        FlightInventory = 22,
        PKFares = 23,
        Baggage = 24,
        OffLine = 25,
        SpiceJetCorp = 26,
        IndigoCorp = 27,
        Jazeera = 28,
        GoAirCorp = 29
    }

    public enum BookingMode
    {
        Auto = 1,
        Manual = 2,
        Import = 3,
        WhiteLabel = 4,
        BookingAPI = 5,
        Itimes = 6,
        ManualImport = 7
    }

    public enum ModeOfPayment
    {
        /// <summary>
        /// Value not assigned.
        /// </summary>?
        Null = 0,
        /// <summary>
        /// Payment made from cash account.
        /// </summary>
        Cash = 1,
        /// <summary>
        /// Payment made from credit account.
        /// </summary>
        Credit = 2,
        /// <summary>
        /// Payment made by credit card.
        /// </summary>
        CreditCard = 3
    }

    public enum BookingFlowStatus
    {
        NoStatus = 0,
        UpdatePax = 1,
        AssignSeat = 2,
        PaxUpdated = 3,
        AddPayment = 4,
        AssignSeatFailed = 5,
        BookingSuccess = 6
    }

    public enum PassengerType
    {
        Adult = 1,
        Child = 2,
        Infant = 3,
        Senior = 4
    }

    public enum Gender
    {
        Null = 0,
        Male = 1,
        Female = 2
    }

    public enum FlightStatus
    {
        Confirmed = 1,
        Waitlisted = 2
    }

    public enum SSRStatus
    {
        Accepted = 1,
        Denied = 2,
        Unknown = 3,
        Deleted = 4
    }

    [Serializable]
    public class SSR
    {
        /// <summary>
        /// 
        /// </summary>
        private int ssrId;
        public int SsrId
        {
            get
            {
                return ssrId;
            }
            set
            {
                ssrId = value;
            }
        }

        //FlightId of the itinerary to which the SSR belongs
        private int flightId;
        public int FlightId
        {
            get
            {
                return flightId;
            }
            set
            {
                flightId = value;
            }
        }

        //Id of the Pax corresponding to whom this particular SSR is being Stored
        private int paxId;
        public int PaxId
        {
            get
            {
                return paxId;
            }
            set
            {
                paxId = value;
            }
        }

        //SSR Code E.g. - FQTV for frequent flyer no., Meal for meal request
        private string ssrCode;
        public string SsrCode
        {
            get
            {
                return ssrCode;
            }
            set
            {
                ssrCode = value;
            }
        }

        private string detail;
        public string Detail
        {
            get
            {
                return detail;
            }
            set
            {
                detail = value;
            }
        }

        private SSRStatus status;
        public SSRStatus Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        private string ssrStatus;
        public string SsrStatus
        {
            get
            {
                return ssrStatus;
            }
            set
            {
                ssrStatus = value;
            }
        }

        public static SSR Copy(SSR ssr)
        {
            SSR copy = new SSR();
            copy.ssrId = ssr.ssrId;
            copy.paxId = ssr.paxId;
            copy.flightId = ssr.flightId;
            copy.ssrCode = ssr.ssrCode;
            copy.ssrStatus = ssr.ssrStatus;
            copy.status = ssr.status;
            copy.detail = ssr.detail;
            return copy;
        }
    }

    [Serializable]
    public struct Meal
    {
        private string code;
        private string description;

        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }

    [Serializable]
    public class FlightPassenger
    {
        #region Private Variables
        /// <summary>
        /// Unique Id for a passenger
        /// </summary>
        int paxId;
        /// <summary>
        /// flight id of the flight to which the pax is linked
        /// </summary>
        int flightId;
        /// <summary>
        /// First name of passenger
        /// </summary>
        string firstName;
        /// <summary>
        /// Last name of passenger
        /// </summary>
        string lastName;
        /// <summary>
        /// TODO
        /// </summary>
        string title;
        /// <summary>
        /// full Name of passenger. (Title + FName + LName).
        /// </summary>
        string fullName;
        /// <summary>
        /// Mobile phone number of passenger
        /// </summary>
        string cellPhone;
        /// <summary>
        /// Indicates if the passenger is primary (leading) passenger
        /// </summary>
        bool isLeadPax;
        /// <summary>
        /// Date of Birth of passenger
        /// </summary> 
        DateTime dateOfBirth;
        /// <summary>
        /// Type of passenger. Adult, Child, Infant or Senior
        /// </summary>
        PassengerType type;
        /// <summary>
        /// Passport no of passenger
        /// </summary>
        string passportNo;
        /// <summary>
        /// Nationality of the passenger
        /// </summary>
        string nationality;
        /// <summary>
        /// Country issueing passport.
        /// </summary>
        string country;
        /// <summary>
        /// City Name
        /// </summary>
        string city;
        /// <summary>
        /// Address Line1
        /// </summary>
        string addressLine1;
        /// <summary>
        /// Address Line2
        /// </summary>
        string addressLine2;
        /// <summary>
        /// gender of the passenger
        /// </summary>
        Gender gender;
        /// <summary>
        /// EmailId of passenger
        /// </summary>
        string email;
        /// <summary>
        /// Meal Preference of passenger
        /// </summary>
        Meal meal;
        /// <summary>
        /// Frequent flier airline
        /// </summary>
        string ffAirline;
        /// <summary>
        /// Frequent flier number
        /// </summary>
        string ffNumber;
        /// <summary>
        /// LastName.FirstName.Title spaces replaced with '.'.
        /// Used to identify a passenger in a booking.
        /// </summary>
        string paxKey;
        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// Expirt Date of passport
        /// </summary> 
        DateTime passportExpiry;
        /// <summary>
        /// BaggageCode for G9
        /// </summary>
        string baggageCode;

        /// <summary>
        /// BaggageCode Type for FlyDubai (BAGB,BAGL etc)
        /// </summary>
        string baggageType;
        /// <summary>
        /// Category/SSRCategory Id for FlyDubai (99 for Baggage)
        /// </summary>
        string categoryId;
        /// <summary>
        /// Baggage Charge for Flydubai.
        /// </summary>
        List<decimal> baggageCharge;

        /// <summary>
        /// Flex Fields
        /// </summary>
        List<FlightFlexDetails> flexDetailsList;


        string destinationPhone;//Added by Lokesh on 25-sept-2017 regarding Pax Destination phone for booking tickets

        string gstStateCode; //Added by Lokesh on 27-Mar-2018 For G9 Source if the customer is travelling from India and selects the state Id.
        string gstTaxRegNo;//Added by Lokesh on 27-Mar-2018 For G9 Source if the customer is travelling from India and provides the TaxRegNo.
        /// <summary>
        /// Mandatory for FraudLabs. Saved for Audit in B2C
        /// </summary>
        string stateCode;

        /// <summary>
        /// string key stands for Tax Type.
        /// double value is the Tax Value.
        /// </summary>
        private List<KeyValuePair<string, decimal>> taxBreakup;

        string mealType; //For Spicejet And indigo Meal Selection
        string mealDesc; //For Spicejet And indigo Meal Selection in Eticket display purpose;

        /// <summary>
        /// To add selected seat per pax per segment
        /// class object Added by Praveen to save selected seat info
        /// </summary>
        private List<PaxSeatInfo> _liPaxSeatInfo;

        /// <summary>
        /// To display all segments seat info
        /// string Added by Praveen to show selected seat info of all segments in payment confirmation page
        /// </summary>
        private string sSeatInfo;

        /// <summary>
        /// To store corporate profile Id
        /// string Added by Praveen to store corporate profile Id and save the same in itinerary passenger table
        /// </summary>
        private string _CorpProfileId;

        #endregion

        #region Public Members
        /// <summary>
        /// Gets or Sets paxId
        /// </summary>
        public int PaxId
        {
            get
            {
                return paxId;
            }
            set
            {
                paxId = value;
            }
        }

        /// <summary>
        /// Gets of sets the Flight Id.
        /// </summary>
        public int FlightId
        {
            get
            {
                return flightId;
            }
            set
            {
                flightId = value;
            }
        }

        /// <summary>
        /// Gets or sets First name
        /// </summary>
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }
        }

        /// <summary>
        /// Gets or sets Last name
        /// </summary>
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        /// <summary>
        /// Gets or sets the title of passenger
        /// </summary>
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        /// <summary>
        /// Gets full name of a passenger.
        /// </summary>
        public string FullName
        {
            get
            {
                if (fullName == null || fullName.Length == 0)
                {
                    StringBuilder fName = new StringBuilder();
                    fName.Append(title);
                    fName.Append(" ");
                    fName.Append(firstName);
                    fName.Append(" ");
                    fName.Append(lastName);
                    fullName = fName.ToString().Trim();
                }
                return fullName;
            }
        }

        /// <summary>
        /// Gets or sets the Cellphone of passenger
        /// </summary>
        public string CellPhone
        {
            get
            {
                return cellPhone;
            }
            set
            {
                cellPhone = value;
            }
        }

        /// <summary>
        /// Gets or sets the isLeadPax property.
        /// </summary>
        public bool IsLeadPax
        {
            get
            {
                return isLeadPax;
            }
            set
            {
                isLeadPax = value;
            }
        }

        /// <summary>
        /// Gets or sets Date of birth
        /// </summary>
        public DateTime DateOfBirth
        {
            get
            {
                return dateOfBirth;
            }
            set
            {
                dateOfBirth = value;
            }
        }

        /// <summary>
        /// Gets or sets the passenger type
        /// </summary>
        public PassengerType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        /// <summary>
        /// Gets or sets the passport number
        /// </summary>
        public string PassportNo
        {
            get
            {
                return passportNo;
            }
            set
            {
                passportNo = value;
            }
        }
        /// <summary>
        /// get or set the Nationality
        /// </summary>
        public string Nationality
        {
            get
            {
                return nationality;
            }
            set
            {
                nationality = value;
            }
        }
        public string Country
        {
            get
            {
                return country;
            }
            set
            {
                country = value;
            }
        }

        /// <summary>
        ///  gets or sets city value
        /// </summary>
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        /// <summary>
        /// Gets or sets the address line 1
        /// </summary>
        public string AddressLine1
        {
            get
            {
                return addressLine1;
            }
            set
            {
                addressLine1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the address line 2
        /// </summary>
        public string AddressLine2
        {
            get
            {
                return addressLine2;
            }
            set
            {
                addressLine2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the Gender
        /// </summary>
        public Gender Gender
        {
            get
            {
                return gender;
            }
            set
            {
                gender = value;
            }
        }

        /// <summary>
        /// Gets or sets the email id of passenger
        /// </summary>
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }

        /// <summary>
        /// Gets or sets the Meal preference of passenger
        /// </summary>
        public Meal Meal
        {
            get
            {
                return meal;
            }
            set
            {
                meal = value;
            }
        }

        /// <summary>
        /// Gets or sets the Frequent Flier airline of passenger
        /// </summary>
        public string FFAirline
        {
            get
            {
                return ffAirline;
            }
            set
            {
                ffAirline = value;
            }
        }

        /// <summary>
        /// Gets or sets the Frequent Flier Number of the passenger
        /// </summary>
        public string FFNumber
        {
            get
            {
                return ffNumber;
            }
            set
            {
                ffNumber = value;
            }
        }

        /// <summary>
        /// Gets PaxKey. LastName.FirstName.Title with spaces replaced with '.'.
        /// </summary>
        public string PaxKey
        {
            get
            {
                if (paxKey == null || paxKey.Length == 0)
                {
                    StringBuilder key = new StringBuilder();
                    key.Append(lastName);
                    key.Append(".");
                    key.Append(firstName);
                    if (title != null && title.Length > 0)
                    {
                        //key.Append(".");
                        key.Append(title);
                    }
                    paxKey = key.ToString().Replace(' ', '.').Trim().ToUpper();
                }
                paxKey = paxKey.Replace(".", "");
                return paxKey;
            }
        }

        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets or sets passport expiry date
        /// </summary>
        public DateTime PassportExpiry
        {
            get
            {
                return passportExpiry;
            }
            set
            {
                passportExpiry = value;
            }
        }
        /// <summary>
        /// BaggageCode for G9 (Air Arabia)
        /// </summary>
        public string BaggageCode
        {
            get { return baggageCode; }
            set { baggageCode = value; }
        }

        /// <summary>
        /// BaggageCode Type for FlyDubai (BAGB,BAGL etc)
        /// </summary>
        public string BaggageType
        {
            get { return baggageType; }
            set { baggageType = value; }
        }
        /// <summary>
        /// Category/SSRCategory Id for FlyDubai (99 for Baggage)
        /// </summary>
        public string CategoryId
        {
            get { return categoryId; }
            set { categoryId = value; }
        }

        public List<decimal> FlyDubaiBaggageCharge
        {
            get { return baggageCharge; }
            set { baggageCharge = value; }
        }
        /// <summary>
        /// Flex Details
        /// </summary>
        public List<FlightFlexDetails> FlexDetailsList
        {
            get { return flexDetailsList; }
            set { flexDetailsList = value; }
        }


        //Added by Lokesh on 25-sept-2017 regarding pax destination phone for booking tickets     
        public string DestinationPhone
        {
            get { return destinationPhone; }
            set { destinationPhone = value; }
        }

        //Added by Lokesh on 27/03/2018 For G9 Source only for lead Pax 
        //if the customer is travelling from India and provides any GST info (StateCode and Tax RegNo)

        public string GSTStateCode
        {
            get { return gstStateCode; }
            set { gstStateCode = value; }
        }

        public string GSTTaxRegNo
        {
            get { return gstTaxRegNo; }
            set { gstTaxRegNo = value; }
        }

        /// <summary>
        /// Mandatory for FraudLabs. Saved for audit checking for FraudLabs (B2C)
        /// </summary>
        public string StateCode
        {
            get { return stateCode; }
            set { stateCode = value; }
        }

        /// <summary>
        /// Saving TaxBreakup for LCC Airlines after booking. TaxBreakup will be read from Ticket response.
        /// </summary>
        public List<KeyValuePair<string, decimal>> TaxBreakup
        {
            get { return taxBreakup; }
            set { taxBreakup = value; }
        }

        public string MealType
        {
            get { return mealType; }
            set { mealType = value; }
        }
        public string MealDesc
        {
            get { return mealDesc; }
            set { mealDesc = value; }
        }
        public string SeatInfo
        {
            get { return sSeatInfo; }
            set { sSeatInfo = value; }
        }
        public string CorpProfileId
        {
            get { return _CorpProfileId; }
            set { _CorpProfileId = value; }
        }

        public List<PaxSeatInfo> liPaxSeatInfo { get { return _liPaxSeatInfo; } set { _liPaxSeatInfo = value; } }
        #endregion

        /// <summary>
        /// Gets the passenger type code for a given PassengerType
        /// </summary>
        /// <param name="type">PassengerType for which PTC is needed</param>
        /// <returns>Passenger type code as string</returns>
        public static string GetPTC(PassengerType type)
        {
            switch (type)
            {
                case PassengerType.Adult:
                    return "ADT";
                case PassengerType.Child:
                    return "CNN";
                case PassengerType.Infant:
                    return "INF";
                case PassengerType.Senior:
                    return "SNN";
                default:
                    return string.Empty;
            }
        }

        public static FlightPassenger[] GetDummyPaxList(int adult, int child, int infant, int senior)
        {
            //Trace.TraceInformation("Passenger.GettDummyPaxList entered.");
            int[] paxCount = { adult, child, infant, senior };
            FlightPassenger[] paxList = new FlightPassenger[adult + child + infant + senior];
            for (int i = 0, k = 0; i < 4; i++)
            {
                for (int j = 0; j < paxCount[i]; j++, k++)
                {
                    paxList[k] = new FlightPassenger();
                    PassengerType type = (PassengerType)(i + 1);
                    paxList[k].type = type;
                    paxList[k].title = string.Empty;
                    paxList[k].firstName = GetPTC(type) + Convert.ToChar(j + 65);
                    paxList[k].lastName = "TEST";
                    if (type == PassengerType.Infant)
                    {
                        paxList[k].dateOfBirth = DateTime.Now.AddYears(-1);
                    }
                }
            }
            return paxList;
        }

        public override string ToString()
        {
            StringBuilder paxString = new StringBuilder(100);
            paxString.AppendFormat("{0} ", paxId);
            paxString.Append(title);
            paxString.AppendFormat(" {0} {1}", firstName, lastName);
            paxString.AppendFormat(" {0}", type.ToString());
            if (gender != Gender.Null)
            {
                paxString.AppendFormat(" {0}", gender.ToString());
            }
            if (!string.IsNullOrEmpty(email))
            {
                paxString.AppendFormat(" {0}", email);
            }
            if (!string.IsNullOrEmpty(addressLine1))
            {
                paxString.AppendFormat(" {0}", addressLine1);
            }
            if (!string.IsNullOrEmpty(addressLine2))
            {
                paxString.AppendFormat(" {0}", addressLine2);
            }
            return paxString.ToString();
        }
    }

    [Serializable]
    public class FlightFlexDetails
    {
        #region variables
        int _detailId;
        int _flexId;
        int _paxId;
        string _flexLabel;
        string _flexData;
        int _createdBy;
        int productId;
        string _flexGDSprefix;
        #endregion
        #region properities
        public int DetailsId
        {
            get { return _detailId; }
            set { _detailId = value; }
        }
        public int FlexId
        {
            get { return _flexId; }
            set { _flexId = value; }
        }
        public int PaxId
        {
            get { return _paxId; }
            set { _paxId = value; }
        }
        public string FlexLabel
        {
            get { return _flexLabel; }
            set { _flexLabel = value; }
        }
        public string FlexData
        {
            get { return _flexData; }
            set { _flexData = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public int ProductID
        {
            get { return productId; }
            set { productId = value; }
        }
        public string FlexGDSprefix
        {
            get { return _flexGDSprefix; }
            set { _flexGDSprefix = value; }
        }
        #endregion        
    }

    [Serializable]
    public class PaxSeatInfo
    {
        #region variables
        int _sPaxNo { get; set; }
        string _sSegment { get; set; }
        string _sSeatNo { get; set; }
        decimal _dcPrice { get; set; }
        string _sSeatStatus { get; set; }
        #endregion

        #region properities
        public int PaxNo { get { return _sPaxNo; } set { _sPaxNo = value; } }
        public string Segment { get { return _sSegment; } set { _sSegment = value; } }
        public string SeatNo { get { return _sSeatNo; } set { _sSeatNo = value; } }
        public decimal Price { get { return _dcPrice; } set { _dcPrice = value; } }
        public string SeatStatus { get { return _sSeatStatus; } set { _sSeatStatus = value; } }
        #endregion
    }

    [Serializable]
    public class FlightInfo
    {
        #region Members
        /// <summary>
        /// Unique identity number for UAPIS Segment Key. Used also for FlyDubai LFID(Logical FlightID). Later need rename it to Generic.
        /// </summary>
        string uapiSegmentRefKey;
        /// <summary>
        /// Unique identity number for a leg of a booking
        /// </summary>
        int segmentId;
        /// <summary>
        /// Flight id to which the leg belongs
        /// </summary>
        int flightId;
        /// <summary>
        /// Name of the airline
        /// </summary>
        string airline;
        /// <summary>
        /// Origin Airport
        /// </summary>
        string origin;
        /// <summary>
        /// Destination Airport
        /// </summary>
        string destination;
        /// <summary>
        /// Flight number
        /// </summary>
        string flightNumber;
        /// <summary>
        /// Departure time at orgin airport
        /// </summary>
        DateTime departureTime;
        /// <summary>
        /// Arrival time at the destination
        /// </summary>
        DateTime arrivalTime;
        /// <summary>
        /// Booking Class
        /// </summary>
        string bookingClass;
        /// <summary>
        /// Booking Cabin Class
        /// </summary>
        string cabinClass;
        /// <summary>
        /// Availability in different classes.
        /// </summary>
        Dictionary<string, byte> availabiLity;
        /// <summary>
        /// Arrival terminal of destination airport
        /// </summary>
        string arrTerminal;
        /// <summary>
        /// Departure terminal at origin airport
        /// </summary>
        string depTerminal;
        /// <summary>
        /// Status of flight;
        /// </summary>
        FlightStatus flightStatus;
        /// <summary>
        /// status of flight booking
        /// </summary>
        string status;
        /// <summary>
        /// Meal Type to be served in flight
        /// </summary>
        string mealType;
        /// <summary>
        /// Indicates if e-ticket can be issued
        /// </summary>
        bool eTicketEligible;
        /// <summary>
        /// Duration of the flight
        /// </summary>
        TimeSpan duration;
        /// <summary>
        /// Ground time at origin 
        /// </summary>
        TimeSpan groundTime;
        /// <summary>
        /// Total accumulated duration. Including ground time.
        /// </summary>
        TimeSpan accumulatedDuration;
        /// <summary>
        /// Indicates the origin is stop over, if true.
        /// </summary>
        bool stopOver;
        /// <summary>
        /// Stops in the flight
        /// </summary>
        int stops;
        /// <summary>
        /// Aircraft type code
        /// </summary>
        string craft;
        /// <summary>
        /// Distance in miles
        /// </summary>
        int mile;
        /// <summary>
        /// Airline Direct Resource Locator.
        /// </summary>
        string airlinePNR;
        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// conjunction number in case of cunjunction ticket.
        /// </summary>
        string conjunctionNo;


        /// <summary>
        /// Fare Info Key for a segment (UAPI)
        /// </summary>
        string fareInfoKey;

        /// <summary>
        /// to store UAPI Departure Date Format
        /// </summary>
        string uapiDepartureTime;

        /// <summary>
        /// to store UAPI Arrival Date Format
        /// </summary>
        string uapiArrivalTime;


        /// <summary>
        /// Group - 0 outbount,1 inbound(UAPI)
        /// </summary>
        int group;

        string operatingCarrier;
        // To store UAPI private fare values
        Hashtable uapiReservationValues;
        /// <summary>
        /// Stores fare types for FlyDubai like Pay To Change, Free To Change or Basic
        /// </summary>
        string segmentFareType;

        /// <summary>
        /// TourCode returned for Negotiated Fare from UAPI
        /// </summary>
        string tourCode;
        #endregion

        #region Properties
        public int Group
        {
            get { return group; }
            set { group = value; }
        }


        public string UapiDepartureTime
        {
            get { return uapiDepartureTime; }
            set { uapiDepartureTime = value; }
        }

        public string UapiArrivalTime
        {
            get { return uapiArrivalTime; }
            set { uapiArrivalTime = value; }
        }

        public string ConjunctionNo
        {
            get { return conjunctionNo; }
            set { conjunctionNo = value; }
        }

        public string OperatingCarrier
        {
            get
            {
                return operatingCarrier;
            }
            set
            {
                operatingCarrier = value;
            }
        }

        public int SegmentId
        {
            get
            {
                return segmentId;
            }
            set
            {
                segmentId = value;
            }
        }



        public int FlightId
        {
            get
            {
                return flightId;
            }
            set
            {
                flightId = value;
            }
        }

        public string Airline
        {
            get
            {
                return airline;
            }
            set
            {
                airline = value;
            }
        }

        public string Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
            }
        }

        public string Destination
        {
            get
            {
                return destination;
            }
            set
            {
                destination = value;
            }
        }

        public string FlightNumber
        {
            get
            {
                return flightNumber;
            }
            set
            {
                flightNumber = value;
            }
        }

        public DateTime DepartureTime
        {
            get
            {
                return departureTime;
            }
            set
            {
                departureTime = value;
            }
        }

        public DateTime ArrivalTime
        {
            get
            {
                return arrivalTime;
            }
            set
            {
                arrivalTime = value;
            }
        }

        public string BookingClass
        {
            get
            {
                return bookingClass;
            }
            set
            {
                bookingClass = value;
            }
        }
        public string CabinClass
        {
            get
            {
                return cabinClass;
            }
            set
            {
                cabinClass = value;
            }
        }

        public string FareInfoKey
        {
            get
            {
                return fareInfoKey;
            }
            set
            {
                fareInfoKey = value;
            }
        }

        [System.Xml.Serialization.XmlIgnore]
        public Dictionary<string, byte> AvailabiLity
        {
            get
            {
                return availabiLity;
            }
            set
            {
                availabiLity = value;
            }
        }

        public string ArrTerminal
        {
            get
            {
                return arrTerminal;
            }
            set
            {
                arrTerminal = value;
            }
        }

        public string DepTerminal
        {
            get
            {
                return depTerminal;
            }
            set
            {
                depTerminal = value;
            }
        }

        public FlightStatus FlightStatus
        {
            get
            {
                return flightStatus;
            }
            set
            {
                flightStatus = value;
            }
        }

        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public string MealType
        {
            get
            {
                return mealType;
            }
            set
            {
                mealType = value;
            }
        }

        public bool ETicketEligible
        {
            get
            {
                return eTicketEligible;
            }
            set
            {
                eTicketEligible = value;
            }
        }

        public string AirlinePNR
        {
            get
            {
                return airlinePNR;
            }
            set
            {
                airlinePNR = value;
            }
        }

        public string Craft
        {
            get
            {
                return craft;
            }
            set
            {
                craft = value;
            }
        }

        public bool StopOver
        {
            get
            {
                return stopOver;
            }
            set
            {
                stopOver = value;
            }
        }

        public int Stops
        {
            get
            {
                return stops;
            }
            set
            {
                stops = value;
            }
        }

        public int Mile
        {
            get
            {
                return mile;
            }
            set
            {
                mile = value;
            }
        }

        [System.Xml.Serialization.XmlElement(typeof(XmlTimeSpan))]
        public TimeSpan Duration
        {
            get
            {
                return duration;
            }
            set
            {
                duration = value;
            }
        }
        [System.Xml.Serialization.XmlElement(typeof(XmlTimeSpan))]
        public TimeSpan GroundTime
        {
            get
            {
                return groundTime;
            }
            set
            {
                groundTime = value;
            }
        }
        [System.Xml.Serialization.XmlElement(typeof(XmlTimeSpan))]
        public TimeSpan AccumulatedDuration
        {
            get
            {
                return accumulatedDuration;
            }
            set
            {
                accumulatedDuration = value;
            }
        }

        public string UapiSegmentRefKey
        {
            get
            {
                return uapiSegmentRefKey;
            }
            set
            {
                uapiSegmentRefKey = value;
            }
        }
        /// <summary>
        /// Gets Identity of flight in format 
        /// Airline code + flight number + departure date time in format "ddMMMyyyyHHmm". 
        /// eg. BA030227MAR20070910 for (BA 0302 27MAR2007 1410);
        /// </summary>
        public string FlightKey
        {
            get
            {
                StringBuilder key = new StringBuilder();
                key.Append(airline);
                //if (flightNumber.Length == 3)
                //{
                //    key.Append("0");
                //}
                key.Append(flightNumber.PadLeft(4, '0'));
                key.Append(departureTime.ToString("dd"));
                key.Append(departureTime.ToString("MMM").ToUpper());
                key.Append(departureTime.ToString("yyyyHHmm"));
                return key.ToString();
            }
        }

        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }

        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }

        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }
        [System.Xml.Serialization.XmlIgnore]
        public Hashtable UAPIReservationValues
        {
            get { return uapiReservationValues; }
            set { uapiReservationValues = value; }
        }

        /// <summary>
        /// Stores fare types for Fly Dubai like Pay To Change or Free To Change or Basic.
        /// </summary>
        public string SegmentFareType
        {
            get { return segmentFareType; }
            set { segmentFareType = value; }
        }

        /// <summary>
        /// Tour Code returned for Negotiated Fare
        /// </summary>
        public string TourCode
        {
            get { return tourCode; }
            set { tourCode = value; }
        }
        #endregion


        public static FlightInfo Copy(FlightInfo flight)
        {
            FlightInfo newFlight = new FlightInfo();
            newFlight.accumulatedDuration = flight.accumulatedDuration;
            newFlight.airline = flight.airline;
            newFlight.airlinePNR = flight.airlinePNR;
            newFlight.arrivalTime = flight.arrivalTime;
            newFlight.arrTerminal = flight.arrTerminal;
            newFlight.availabiLity = flight.availabiLity;
            newFlight.bookingClass = flight.bookingClass;
            newFlight.cabinClass = flight.cabinClass;
            newFlight.craft = flight.craft;
            newFlight.createdBy = flight.createdBy;
            newFlight.createdOn = flight.createdOn;
            newFlight.departureTime = flight.departureTime;
            newFlight.depTerminal = flight.depTerminal;
            newFlight.destination = flight.destination;
            newFlight.duration = flight.duration;
            newFlight.eTicketEligible = flight.eTicketEligible;
            newFlight.flightId = flight.flightId;
            newFlight.flightNumber = flight.flightNumber;
            newFlight.flightStatus = flight.flightStatus;
            newFlight.groundTime = flight.groundTime;
            newFlight.lastModifiedBy = flight.lastModifiedBy;
            newFlight.lastModifiedOn = flight.lastModifiedOn;
            newFlight.mealType = flight.mealType;
            newFlight.mile = flight.mile;
            newFlight.operatingCarrier = flight.operatingCarrier;
            newFlight.origin = flight.origin;
            newFlight.segmentId = flight.segmentId;
            newFlight.status = flight.status;
            newFlight.stopOver = flight.stopOver;
            newFlight.stops = flight.stops;
            if (flight.fareInfoKey != null) newFlight.fareInfoKey = flight.fareInfoKey;
            /*if (flight.group !=null) */
            newFlight.Group = flight.Group;
            if (!string.IsNullOrEmpty(flight.UapiDepartureTime)) newFlight.UapiDepartureTime = flight.UapiDepartureTime;
            if (!string.IsNullOrEmpty(flight.UapiArrivalTime)) newFlight.uapiArrivalTime = flight.uapiArrivalTime;
            if (flight.UapiSegmentRefKey != null) newFlight.UapiSegmentRefKey = flight.UapiSegmentRefKey;
            newFlight.UAPIReservationValues = flight.UAPIReservationValues;
            return newFlight;
        }

        public override string ToString()
        {
            StringBuilder flightString = new StringBuilder(100);
            flightString.Append(segmentId);
            flightString.Append(" ");
            flightString.Append(airline);
            flightString.Append(" ");
            flightString.Append(flightNumber);
            flightString.Append(" ");
            flightString.Append(bookingClass);
            flightString.Append(departureTime.ToString(" ddMMM "));
            flightString.Append(origin);
            flightString.Append(destination);
            flightString.Append(departureTime.ToString(" HHmm "));
            flightString.Append(arrivalTime.ToString("HHmm "));
            flightString.Append(eTicketEligible ? "E " : "  ");
            flightString.Append(airlinePNR);
            return flightString.ToString();
        }
    }

    public class XmlTimeSpan
    {
        private TimeSpan m_internal = TimeSpan.Zero;

        public XmlTimeSpan()
            : this(TimeSpan.Zero)
        {
        }

        public XmlTimeSpan(TimeSpan input)
        {
            m_internal = input;
        }

        public static implicit operator TimeSpan(XmlTimeSpan input)
        {
            return (input != null) ? input.m_internal : TimeSpan.Zero;
        }

        // Alternative to the implicit operator TimeSpan(XmlTimeSpan input)
        public TimeSpan ToTimeSpan()
        {
            return m_internal;
        }

        public static implicit operator XmlTimeSpan(TimeSpan input)
        {
            return new XmlTimeSpan(input);
        }

        // Alternative to the implicit operator XmlTimeSpan(TimeSpan input)
        public void FromTimeSpan(TimeSpan input)
        {
            this.m_internal = input;
        }

        [XmlText]
        public string Value
        {
            get
            {
                return XmlConvert.ToString(m_internal);
            }
            set
            {
                m_internal = XmlConvert.ToTimeSpan(value);
            }
        }
    }

    [Serializable]
    public class PassiveFlightItinerary
    {
        #region PrivateMembers
        /// <summary>
        /// Id of flight booked
        /// </summary>
        int flightId;
        /// <summary>
        /// Flight segments
        /// </summary>
        FlightInfo[] segments;
        /// <summary>
        /// Details of other passengers
        /// </summary>
        FlightPassenger[] passenger;
        /// <summary>
        /// PNR of the booking.
        /// </summary>
        string pnr;
        /// <summary>
        /// Booking source/GDS
        /// </summary>
        BookingSource flightBookingSource;
        /// <summary>
        /// Origin airport of the OND
        /// </summary>
        private string origin;
        /// <summary>
        /// Destination airport of the OND
        /// </summary>
        private string destination;
        /// <summary>
        /// Last ticketing date
        /// </summary>
        private DateTime lastTicketDate;
        /// <summary>
        /// Ticket advisory showing additional information or instruction about ticketing.
        /// </summary>
        private string ticketAdvisory;
        /// <summary>
        /// Fare type ( Published, Securate, or ....)
        /// </summary>
        private string fareType;
        /// <summary>
        /// Indicates the number of hits made for generating e-Ticket for this itinerary.
        /// </summary>
        private int eTicketHit;
        /// <summary>
        /// validating airline code
        /// </summary>
        private string airlineCode;
        /// <summary>
        /// first segment departure date is travel date
        /// </summary>
        private DateTime travelDate;
        /// <summary>
        /// Mode how booking is made(Import or Automatic or Manual)
        /// </summary>
        private BookingMode bookingMode;
        /// <summary>
        /// List of SSRs associated with this flight.
        /// </summary>
        private List<SSR> ssr;
        /// <summary>
        /// MemberId of the member who created this entry
        /// </summary>
        int createdBy;
        /// <summary>
        /// Date and time when the entry was created
        /// </summary>
        DateTime createdOn;
        /// <summary>
        /// MemberId of the member who modified the entry last.
        /// </summary>
        int lastModifiedBy;
        /// <summary>
        /// Date and time when the entry was last modified
        /// </summary>
        DateTime lastModifiedOn;
        /// <summary>
        /// pricing type infomation ie "AUTO PRICED"
        /// </summary>
        private string pricingType;
        /// <summary>
        /// 
        /// </summary>
        private string validatingAirline;
        /// <summary>
        /// Two character airline code of the validating airline.
        /// </summary>
        private string validatingAirlineCode;
        private string tourCode;

        private string endorsement;
        /// <summary>
        /// Ticket Information as given by worldspan.
        /// </summary>
        private bool ticketed;
        /// <summary>
        /// Flag indicating if the flight is domestic.
        /// Public proper
        /// </summary>
        private bool isDomestic;
        /// <summary>
        /// Mode of payment.
        /// </summary>
        private ModeOfPayment paymentMode;
        /// <summary>
        /// True if the fare is non refundable.
        /// </summary>
        private bool nonRefundable;
        /// <summary>
        /// agencyid of the agency to which the booking belongs
        /// </summary>
        private int agencyId;
        /// <summary>
        /// booking id of this itinerary
        /// </summary>
        private int bookingId;
        /// <summary>
        /// stores the alias airlinecode of the flight name like ITR in case of ITRed. 
        /// </summary>
        string aliasAirlineCode;
        /// <summary>
        /// Tell wheter booking is done by other  
        /// </summary>
        private bool isOurBooking = true;

        string supplierCode;
        /// <summary>
        /// Assignig UAPI Pricing Solution Key
        /// </summary>
        int uapiPricingSolutionKey;

        /// <summary>
        /// Assignig UAPI UR
        /// </summary>
        string universalRecord;// For 

        /// <summary>
        /// Assignig UAPI Provider PNR (Gelileo)
        /// </summary>
        string airLocatorCode;// For storing Air Reservation Locator code to issue the Ticket 
        string supplierLocatorCode;// For storing Airline Pnr for future info
        int locationId;
        string locationName;
        string specialRequest;

        string guid;
        /// <summary>
        /// Set 'True' if included in Fare for FlyDubai.
        /// </summary>
        private bool isBaggageIncluded;

        /// <summary>
        /// Whether Insurance is added or not 
        /// </summary>
        private bool isInsured;
        /// <summary>
        /// Whether B2B or B2C transaction. Default B2B.
        /// </summary>
        private string transactionType = "B2B";

        /// <summary>
        /// For TBO Air
        /// </summary>
        private bool isLcc;
        /// <summary>
        /// TripId is used for Corporate profile bookings 
        /// which will be grouped based on TripId. Three
        /// bookings will be saved with this TripId in order
        /// to identify whether bookings to corporate or not.
        /// </summary>
        private string tripId;
        /// <summary>
        /// Check whether GST fields need to pass for the supplier while booking for TBOAir
        /// </summary>
        private bool isGSTMandatory;

        private string gstCompanyAddress;

        private string gstCompanyContactNumber;

        private string gstCompanyName;

        private string gstNumber;

        private string gstCompanyEmail;
        //end GST fields for TBOAir

        private string routingTripId;

        /// <summary>
        /// To identify book request type, if the request is update pax or booking commit request
        /// Added by Praveen to change the booking flow for seat assignment requirement 
        /// </summary>
        private BookingFlowStatus _sBookRequestType;

        /// <summary>
        /// To hold pending amount for itinerary booking
        /// Added by Praveen to change the booking flow for seat assignment requirement 
        /// </summary>
        private decimal _dcItineraryAmountDue;

        private bool _isSpecialRoundTrip;//For Combination Search If both onward and return results are SpecialRoundTripFares;

        private string _suppliercurrency;

        #endregion

        #region Public Properties

        /// <summary>
        /// Assign supplier currency
        /// </summary>
        public string sSupplierCurrency
        {
            get
            {
                return _suppliercurrency;
            }
            set
            {
                _suppliercurrency = value;
            }
        }

        /// <summary>
        /// Assing UAPI Pricing Solution Key
        /// </summary>
        public string UniversalRecord
        {
            get
            {
                return universalRecord;
            }
            set
            {
                universalRecord = value;
            }
        }

        public string AirLocatorCode
        {
            get
            {
                return airLocatorCode;
            }
            set
            {
                airLocatorCode = value;
            }
        }
        public string SupplierLocatorCode
        {
            get
            {
                return supplierLocatorCode;
            }
            set
            {
                supplierLocatorCode = value;
            }
        }

        /// <summary>
        /// Assing UAPI Pricing Solution Key
        /// </summary>
        /// <summary>
        /// Assing UAPI Pricing Solution Key
        /// </summary>
        public int UapiPricingSolutionKey
        {
            get
            {
                return uapiPricingSolutionKey;
            }
            set
            {
                uapiPricingSolutionKey = value;
            }
        }
        /// <summary>
        /// Gets or sets the flightId
        /// </summary>
        public int FlightId
        {
            get
            {
                return flightId;
            }
            set
            {
                flightId = value;
            }
        }
        /// <summary>
        /// Gets or sets the flight segments
        /// </summary>
        public FlightInfo[] Segments
        {
            get
            {
                return segments;
            }
            set
            {
                segments = value;
            }
        }
        /// <summary>
        /// Gets or sets other passengers' detail
        /// </summary>
        public FlightPassenger[] Passenger
        {
            get
            {
                return passenger;
            }
            set
            {
                passenger = value;
            }
        }
        /// <summary>
        /// Gets or sets the PNR of booking
        /// </summary>
        public string PNR
        {
            get
            {
                return pnr;
            }
            set
            {
                pnr = value;
            }
        }
        /// <summary>
        /// Gets or sets the flight booking source
        /// </summary>
        public BookingSource FlightBookingSource
        {
            get
            {
                return flightBookingSource;
            }
            set
            {
                flightBookingSource = value;
            }
        }
        /// <summary>
        /// Gets or sets the destination airport of the OND
        /// </summary>
        public string Destination
        {
            get
            {
                return destination;
            }
            set
            {
                destination = value;
            }
        }
        /// <summary>
        /// Gets or sets the fare type
        /// </summary>
        public string FareType
        {
            get
            {
                return fareType;
            }
            set
            {
                fareType = value;
            }
        }
        /// <summary>
        /// Gets or sets the last ticketing date
        /// </summary>
        public DateTime LastTicketDate
        {
            get
            {
                return lastTicketDate;
            }
            set
            {
                lastTicketDate = value;
            }
        }
        /// <summary>
        /// Gets or sets ticketAdvisory.
        /// </summary>
        public string TicketAdvisory
        {
            get
            {
                return ticketAdvisory;
            }
            set
            {
                ticketAdvisory = value;
            }
        }
        /// <summary>
        /// Gets or sets the origin airport of the OND
        /// </summary>
        public string Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
            }
        }

        /// <summary>
        /// Gets the number of eticket hits made for this itinerary.
        /// </summary>
        public int ETicketHit
        {
            get
            {
                return eTicketHit;
            }
            set
            {
                eTicketHit = value;
            }
        }
        /// <summary>
        /// Gets or sets createdBy
        /// </summary>
        public int CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }
        }
        /// <summary>
        /// Gets or sets createdOn Date
        /// </summary>
        public DateTime CreatedOn
        {
            get
            {
                return createdOn;
            }
            set
            {
                createdOn = value;
            }
        }
        /// <summary>
        /// Gets or sets lastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                lastModifiedBy = value;
            }
        }
        /// <summary>
        /// Gets or sets lastModifiedOn Date
        /// </summary>
        public DateTime LastModifiedOn
        {
            get
            {
                return lastModifiedOn;
            }
            set
            {
                lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets or Sets PricingType string
        /// </summary>
        public string PricingType
        {
            get
            {
                return pricingType;
            }
            set
            {
                pricingType = value;
            }
        }

        /// <summary>
        /// Gets or Sets LocationId integer
        /// </summary>
        public int LocationId
        {
            get
            {
                return locationId;
            }
            set
            {
                locationId = value;
            }
        }

        public string LocationName
        {
            get
            {
                return locationName;
            }
            set
            {
                locationName = value;
            }
        }

        public string SupplierCode
        {
            get { return supplierCode; }
            set { supplierCode = value; }
        }

        public bool Ticketed
        {
            get
            {
                return ticketed;
            }
            set
            {
                ticketed = value;
            }
        }
        /// <summary>
        /// Gets or sets the isDomestic field
        /// </summary>
        public bool IsDomestic
        {
            get
            {
                return this.isDomestic;
            }
            set
            {
                this.isDomestic = value;
            }
        }
        public bool IsOurBooking
        {
            get
            {
                return isOurBooking;
            }
            set
            {
                isOurBooking = value;
            }
        }
        /// <summary>
        /// Gets the airline code of itinerary. "YY" for multi airline.
        /// </summary>
        public string AirlineCode
        {
            get
            {
                if (string.IsNullOrEmpty(airlineCode))
                {
                    if (segments != null && segments.Length > 0)
                    {
                        airlineCode = segments[0].Airline;
                        for (int i = 1; i < segments.Length; i++)
                        {
                            if (airlineCode != segments[i].Airline)
                            {
                                airlineCode = "YY";
                                break;
                            }
                        }
                    }
                }
                return airlineCode;
            }
            set
            {
                airlineCode = value;
            }
        }

        /// <summary>
        /// Gets or Sets travelDate field
        /// </summary>
        public DateTime TravelDate
        {
            get
            {
                return this.travelDate;
            }
            set
            {
                this.travelDate = value;
            }
        }
        /// <summary>
        /// Gets or Sets BookingMode field
        /// </summary>
        public BookingMode BookingMode
        {
            get
            {
                if ((int)this.bookingMode == 0)
                {
                    this.bookingMode = BookingMode.Auto;
                }
                return this.bookingMode;
            }
            set
            {
                this.bookingMode = value;
            }
        }

        /// <summary>
        /// Mode of Payment.
        /// </summary>
        public ModeOfPayment PaymentMode
        {
            get
            {
                return this.paymentMode;
            }
            set
            {
                this.paymentMode = value;
            }
        }
        /// <summary>
        /// True if the fare associated is non refundable.
        /// </summary>
        public bool NonRefundable
        {
            get
            {
                return this.nonRefundable;
            }
            set
            {
                this.nonRefundable = value;
            }
        }
        /// <summary>
        /// agency Id to which this booking belongs
        /// </summary>
        public int AgencyId
        {
            get
            {
                return agencyId;
            }
            set
            {
                agencyId = value;
            }
        }
        /// <summary>
        /// booking id of this itinerary
        /// </summary>
        public int BookingId
        {
            get
            {
                return bookingId;
            }
            set
            {
                bookingId = value;
            }
        }

        /// <summary>
        /// gets and sets the alias airlinecode of the flight name like ITR in case of ITRed. 
        /// </summary>
        public string AliasAirlineCode
        {
            get
            {
                return aliasAirlineCode;
            }
            set
            {
                aliasAirlineCode = value;
            }
        }
        /// <summary>
        /// Storing Remarks at the time of requesting cancellation
        /// </summary>
        public string SpecialRequest
        {
            get { return specialRequest; }
            set { specialRequest = value; }
        }



        public string GUID
        {
            get { return guid; }
            set { guid = value; }
        }

        /// <summary>
        /// If Baggage Fare is included in Tax details then set 'True' otherwise set 'False'. Added for FlyDubai.
        /// </summary>
        public bool IsBaggageIncluded
        {
            get { return isBaggageIncluded; }
            set { isBaggageIncluded = value; }
        }

        public bool IsInsured
        {
            get
            {
                return isInsured;
            }
            set
            {
                isInsured = value;
            }
        }

        /// <summary>
        /// Whether B2B or B2C transaction. Default B2B.
        /// </summary>
        public string TransactionType
        {
            get { return transactionType; }
            set { transactionType = value; }
        }

        /// <summary>
        /// True or False for TBOAir
        /// </summary>
        public bool IsLCC
        {
            get { return isLcc; }
            set { isLcc = value; }
        }

        public string TripId
        {
            get { return tripId; }
            set { tripId = value; }
        }

        /// <summary>
        /// To identify book request type, if the request is update pax or booking commit request
        /// Added by Praveen to change the booking flow for seat assignment requirement 
        /// </summary>
        public BookingFlowStatus BookRequestType
        {
            get
            {
                return _sBookRequestType;
            }
            set
            {
                _sBookRequestType = value;
            }
        }

        /// <summary>
        /// To identify book request type, if the request is update pax or booking commit request
        /// Added by Praveen to change the booking flow for seat assignment requirement 
        /// </summary>
        public decimal ItineraryAmountDue
        {
            get
            {
                return _dcItineraryAmountDue;
            }
            set
            {
                _dcItineraryAmountDue = value;
            }
        }

        #endregion

        #region Some additional Properties

        public bool IsGSTMandatory
        {
            get
            {
                return isGSTMandatory;
            }

            set
            {
                isGSTMandatory = value;
            }
        }

        public string GstCompanyAddress
        {
            get
            {
                return gstCompanyAddress;
            }

            set
            {
                gstCompanyAddress = value;
            }
        }

        public string GstCompanyContactNumber
        {
            get
            {
                return gstCompanyContactNumber;
            }

            set
            {
                gstCompanyContactNumber = value;
            }
        }

        public string GstCompanyName
        {
            get
            {
                return gstCompanyName;
            }

            set
            {
                gstCompanyName = value;
            }
        }

        public string GstNumber
        {
            get
            {
                return gstNumber;
            }

            set
            {
                gstNumber = value;
            }
        }

        public string GstCompanyEmail
        {
            get
            {
                return gstCompanyEmail;
            }

            set
            {
                gstCompanyEmail = value;
            }
        }

        /// <summary>
        /// Routing Identification Id for Onward and Return Itineraries
        /// </summary>
        public string RoutingTripId { get => routingTripId; set => routingTripId = value; }

        /// <summary>
        /// //For Combination Search If both onward and return results are SpecialRoundTripFares;
        /// </summary>
        public bool IsSpecialRoundTrip
        {
            get
            {
                return _isSpecialRoundTrip;
            }

            set
            {
                _isSpecialRoundTrip = value;
            }
        }

        #endregion

        #region Class Constructors
        public PassiveFlightItinerary()
        {

        }
        #endregion

        #region Methods

        public bool CheckDomestic(string country)
        {
            bool result = true;
            if (country != segments[0].Origin)
            {
                result = false;
            }
            else
            {
                for (int i = 0; i < segments.Length; i++)
                {
                    if (country != segments[i].Destination)
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        public List<SSR> GetSsrList()
        {
            List<SSR> ssrList = new List<SSR>();
            for (int i = 0; i < Passenger.Length; i++)
            {
                if (Passenger[i].Meal.Code != null || Passenger[i].Meal.Code == string.Empty)
                {
                    for (int j = 0; j < Segments.Length; j++)
                    {
                        SSR ssr = new SSR();
                        ssr.PaxId = Passenger[i].PaxId;
                        ssr.SsrCode = "MEAL";
                        ssr.Detail = Passenger[i].Meal.Code + " " + Segments[j].Origin + "-" + Segments[j].Destination;
                        ssrList.Add(ssr);
                    }
                }
                if ((Passenger[i].FFAirline != null && Passenger[i].FFAirline.Length > 0) && (Passenger[i].FFNumber != null && Passenger[i].FFNumber.Length > 0))
                {
                    SSR ssr = new SSR();
                    ssr.PaxId = Passenger[i].PaxId;
                    ssr.SsrCode = "FQTV";
                    ssr.Detail = Passenger[i].FFAirline + Passenger[i].FFNumber;
                    ssrList.Add(ssr);
                }
                if ((Passenger[i].PassportNo != null && Passenger[i].PassportNo.Length > 0) && (Passenger[i].Country != null && Passenger[i].Country != null && Passenger[i].Country.Length > 0))
                {
                    SSR ssr = new SSR();
                    ssr.PaxId = Passenger[i].PaxId;
                    ssr.SsrCode = "PSPT";
                    ssr.Detail = Passenger[i].PassportNo + "-" + Passenger[i].Country;
                    ssrList.Add(ssr);
                }
                if ((Passenger[i].Type == PassengerType.Child || Passenger[i].Type == PassengerType.Infant) && (Passenger[i].DateOfBirth != null && Passenger[i].DateOfBirth > new DateTime()))
                {
                    SSR ssr = new SSR();
                    ssr.PaxId = Passenger[i].PaxId;
                    if (Passenger[i].Type == PassengerType.Child)
                    {
                        ssr.SsrCode = "CHLD";
                    }
                    else
                    {
                        ssr.SsrCode = "INFT";
                    }
                    ssr.Detail = Passenger[i].DateOfBirth.ToString("dd/MM/yyyy");
                    ssrList.Add(ssr);
                }
            }
            return ssrList;
        }

        public override string ToString()
        {
            StringBuilder itineraryString = new StringBuilder(2048);
            itineraryString.Append("Details of Itinerary :\r\n\r\n");
            //itineraryString.AppendFormat("Product Id : {0}\r\n", ProductId);
            //itineraryString.AppendFormat("Product Type : {0}\r\n", ProductType.ToString());
            itineraryString.AppendFormat("Booking Mode : {0}\r\n", bookingMode.ToString());
            itineraryString.AppendFormat("Booking Id : {0}\r\n", bookingId);
            itineraryString.AppendFormat("Flight Id : {0}\r\n", flightId);
            itineraryString.AppendFormat("Agency Id : {0}\r\n", agencyId);
            itineraryString.AppendFormat("PNR : {0}\r\n", pnr);
            itineraryString.AppendFormat("Routing Trip Id : {0}\r\n", routingTripId);
            itineraryString.AppendFormat("Is Domestic : {0}\r\n", IsDomestic);
            itineraryString.AppendFormat("Booking Source : {0}\r\n", flightBookingSource);
            itineraryString.AppendFormat("Travel Date : {0}\r\n", travelDate);
            itineraryString.AppendFormat("Origin : {0}\r\nDestination : {1}\r\n", origin, destination);
            for (int i = 0; i < segments.Length; i++)
            {
                itineraryString.AppendFormat("{0}\r\n", segments[i].ToString());
            }
            for (int i = 0; i < passenger.Length; i++)
            {
                itineraryString.AppendFormat("{0}\r\n", passenger[i].ToString());
            }
            itineraryString.AppendFormat("Payment Mode : {0}\r\n", paymentMode.ToString());
            itineraryString.AppendFormat("Fare Type : {0}\r\n", fareType);
            itineraryString.AppendFormat("Pricing Type : {0}\r\n", pricingType);
            itineraryString.AppendFormat("Non Refundable : {0}\r\n", nonRefundable);
            itineraryString.AppendFormat("aliasAirlineCode : {0}\r\n", aliasAirlineCode);
            itineraryString.AppendFormat("isOurBooking :  {0}\r\n", IsOurBooking);
            return itineraryString.ToString();
        }

        #endregion
    }
}
