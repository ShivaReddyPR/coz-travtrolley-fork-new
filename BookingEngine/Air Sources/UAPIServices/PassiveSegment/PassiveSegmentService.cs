﻿using PassiveSegment.BO;
using System;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;

namespace PassiveSegment
{
    public class PassiveSegmentService
    {
        /// <summary>
        /// To create and send soap request to UAPI for passive segment reservation
        /// </summary>
        /// <param name="clsPassiveFlightItinerary"></param>
        /// <param name="clsUAPICredentials"></param>
        /// <param name="xmllogPath"></param>
        /// <returns></returns>
        public string CreatePassiveReservation(PassiveFlightItinerary clsPassiveFlightItinerary, UAPICredentials clsUAPICredentials, string xmllogPath)
        {
            string sURLCode = string.Empty; string sOrigin = string.Empty; string sDestination = string.Empty;

            try
            {
                clsPassiveFlightItinerary.Passenger = clsPassiveFlightItinerary.Passenger.Where(x => !string.IsNullOrEmpty(x.FirstName)).ToArray();
                clsPassiveFlightItinerary.Segments = clsPassiveFlightItinerary.Segments.Where(x => !string.IsNullOrEmpty(x.Origin)).ToArray();

                /* Set origin and destination for offline booking from flex data */
                if (clsPassiveFlightItinerary.Passenger[0].FlexDetailsList != null
                    && clsPassiveFlightItinerary.Passenger[0].FlexDetailsList.Where(x => !string.IsNullOrEmpty(x.FlexGDSprefix) && x.FlexGDSprefix == "S1ORG").Count() > 0)
                {
                    var liSeg = clsPassiveFlightItinerary.Passenger[0].FlexDetailsList.
                        Where(x => !string.IsNullOrEmpty(x.FlexGDSprefix) && !string.IsNullOrEmpty(x.FlexData)).Select(y => y).ToList();

                    sOrigin = liSeg.Where(x => x.FlexGDSprefix.EndsWith("ORG")).Select(s => s.FlexData).FirstOrDefault();
                    sDestination = liSeg.Where(x => x.FlexGDSprefix.EndsWith("DST")).Select(s => s.FlexData).LastOrDefault();

                    if (sOrigin == sDestination && liSeg.Where(x => x.FlexGDSprefix.EndsWith("ORG")).Count() == 2 && 
                        liSeg.Where(x => x.FlexGDSprefix.EndsWith("DST")).Count() == 2)
                        sDestination = liSeg.Where(x => x.FlexGDSprefix == "S1DST").Select(s => s.FlexData).FirstOrDefault();
                }
                else
                {
                    sOrigin = clsPassiveFlightItinerary.Segments.Select(x => x.Origin).FirstOrDefault();
                    sDestination = clsPassiveFlightItinerary.Segments.Select(x => x.Destination).LastOrDefault();
                    sDestination = sOrigin == sDestination ? clsPassiveFlightItinerary.Segments.Where(y => y.Group == 0).Select(x => x.Destination).LastOrDefault()
                        : sDestination;
                }                

                StringBuilder createreq = new StringBuilder();
                XmlWriter writer = XmlTextWriter.Create(createreq);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"utf-8\"");
                writer.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                writer.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");

                writer.WriteStartElement("soap", "Body", null);
                writer.WriteStartElement("univ", "PassiveCreateReservationReq", "http://www.travelport.com/schema/universal_v42_0");
                writer.WriteAttributeString("xmlns", "com", null, "http://www.travelport.com/schema/common_v42_0");
                writer.WriteAttributeString("xmlns", "pas", null, "http://www.travelport.com/schema/passive_v42_0");
                writer.WriteAttributeString("AuthorizedBy", "test");
                writer.WriteAttributeString("ProviderCode", "1G");
                writer.WriteAttributeString("TargetBranch", clsUAPICredentials.TargetBranch);

                writer.WriteStartElement("com", "BillingPointOfSaleInfo", null);
                writer.WriteAttributeString("OriginApplication", "UAPI");
                writer.WriteEndElement();

                foreach (FlightPassenger passenger in clsPassiveFlightItinerary.Passenger)
                {
                    string tempvar = passenger.Type == PassengerType.Adult ? "ADT" : passenger.Type == PassengerType.Child ? "CNN" : "INF";

                    writer.WriteStartElement("com", "BookingTraveler", null);
                    writer.WriteAttributeString("TravelerType", tempvar);

                    if (passenger.DateOfBirth != DateTime.MinValue)
                        writer.WriteAttributeString("DOB", passenger.DateOfBirth.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture));
                    else if (passenger.Type == PassengerType.Infant && passenger.DateOfBirth == DateTime.MinValue)
                        writer.WriteAttributeString("DOB", DateTime.Today.AddDays(-365).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture));

                    writer.WriteStartElement("com", "BookingTravelerName", null);
                    writer.WriteAttributeString("Prefix", passenger.Title);
                    writer.WriteAttributeString("First", passenger.FirstName);
                    writer.WriteAttributeString("Last", passenger.LastName);
                    writer.WriteEndElement();

                    writer.WriteStartElement("com", "PhoneNumber", null);
                    writer.WriteAttributeString("Type", "Mobile");
                    writer.WriteAttributeString("Number", clsPassiveFlightItinerary.Passenger[0].CellPhone);
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }

                /* To pass DI info to supplier based on flex information added by user */
                clsPassiveFlightItinerary.Passenger.Where(p => p.FlexDetailsList != null && p.FlexDetailsList.Count > 0).ToList().
                    ForEach(pa =>
                    {
                        pa.FlexDetailsList.Where(y => !string.IsNullOrEmpty(y.FlexGDSprefix) || !string.IsNullOrEmpty(y.FlexData)).ToList().ForEach(x =>
                        {
                            x.FlexData = string.IsNullOrEmpty(x.FlexData) ? string.Empty : ReplaceNonASCII(x.FlexData.Replace("@", "//").Replace("_", "--").Replace("–", "-"));

                            writer.WriteStartElement("com", "AccountingRemark", null);
                            writer.WriteAttributeString("ProviderCode", "1G");
                            writer.WriteAttributeString("Category", "FT");
                            writer.WriteAttributeString("TypeInGds", "Other");
                            writer.WriteStartElement("com", "RemarkData", null);
                            writer.WriteValue((x.FlexGDSprefix + x.FlexData).Length < 84 ? x.FlexGDSprefix + x.FlexData :
                                    (x.FlexGDSprefix + x.FlexData).Substring(0, 84));
                            writer.WriteEndElement();
                            //writer.WriteStartElement("com", "BookingTravelerRef", null);
                            //writer.WriteValue(x.PaxId.ToString());
                            //writer.WriteEndElement();
                            writer.WriteEndElement();
                        });
                    });

                DateTime dtPrevSegArr = new DateTime();
                for (int s = 0; s < clsPassiveFlightItinerary.Segments.Length; s++)
                {
                    writer.WriteStartElement("pas", "PassiveSegment", null);
                    writer.WriteAttributeString("Key", s.ToString());
                    writer.WriteAttributeString("SupplierCode", clsPassiveFlightItinerary.Segments[s].Airline.ToUpper());
                    writer.WriteAttributeString("Status", "AK");

                    DateTime dtDepDate = clsPassiveFlightItinerary.Segments[s].DepartureTime.ToString("yyyy-MM-dd") == DateTime.Now.ToString("yyyy-MM-dd") || 
                        (s > 0 && dtPrevSegArr > clsPassiveFlightItinerary.Segments[s].DepartureTime) ?
                        clsPassiveFlightItinerary.Segments[s].DepartureTime.AddDays(1) : clsPassiveFlightItinerary.Segments[s].DepartureTime;

                    writer.WriteAttributeString("StartDate", dtDepDate.ToString("yyyy-MM-dd") + "T" + dtDepDate.ToString("HH:mm:00.000") + "+04:00");

                    writer.WriteAttributeString("NumberOfItems", clsPassiveFlightItinerary.Passenger.Where(x => x.Type != PassengerType.Infant).ToArray().Length.ToString());

                    if (clsPassiveFlightItinerary.Segments[s].ArrivalTime != null && clsPassiveFlightItinerary.Segments[s].ArrivalTime != DateTime.MinValue)
                    {
                        DateTime dtArrDate = clsPassiveFlightItinerary.Segments[s].ArrivalTime.ToString("yyyy-MM-dd") == DateTime.Now.ToString("yyyy-MM-dd") || 
                            dtDepDate > clsPassiveFlightItinerary.Segments[s].ArrivalTime ? 
                            clsPassiveFlightItinerary.Segments[s].ArrivalTime.AddDays(1) : clsPassiveFlightItinerary.Segments[s].ArrivalTime;

                        writer.WriteAttributeString("EndDate", dtArrDate.ToString("yyyy-MM-dd") + "T" + dtArrDate.ToString("HH:mm:00.000") + "+04:00");
                        dtPrevSegArr = dtArrDate;
                    }
                    writer.WriteAttributeString("Origin", clsPassiveFlightItinerary.Segments[s].Origin.ToUpper());
                    writer.WriteAttributeString("Destination", clsPassiveFlightItinerary.Segments[s].Destination.ToUpper());
                    writer.WriteAttributeString("FlightNumber", clsPassiveFlightItinerary.Segments[s].FlightNumber.Trim().ToUpper());
                    writer.WriteAttributeString("ClassOfService", clsPassiveFlightItinerary.Segments[s].BookingClass.ToUpper());
                    writer.WriteAttributeString("SegmentType", "Air");

                    if (s == clsPassiveFlightItinerary.Segments.Length - 1)
                    {
                        writer.WriteStartElement("pas", "Amount", null);
                        writer.WriteAttributeString("Type", "Due");
                        writer.WriteAttributeString("AmountDuePaid", clsPassiveFlightItinerary.sSupplierCurrency + clsPassiveFlightItinerary.ItineraryAmountDue.ToString());
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }

                for (int s = 0; s < clsPassiveFlightItinerary.Segments.Length; s++)
                {
                    writer.WriteStartElement("pas", "PassiveRemark", null);
                    writer.WriteAttributeString("PassiveSegmentRef", s.ToString());
                    writer.WriteStartElement("pas", "Text", null);
                    writer.WriteValue(clsPassiveFlightItinerary.PNR.ToUpper());
                    writer.WriteEndElement();
                    writer.WriteStartElement("pas", "Type", null);
                    writer.WriteValue("CF");
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }

                for (int s = 0; s < clsPassiveFlightItinerary.Segments.Length; s++)
                {
                    writer.WriteStartElement("pas", "AssociatedRemark", null);
                    writer.WriteAttributeString("SegmentRef", s.ToString());
                    writer.WriteStartElement("com", "RemarkData", null);
                    writer.WriteValue("0" + clsPassiveFlightItinerary.Segments[s].Airline.ToUpper() + clsPassiveFlightItinerary.Segments[s].FlightNumber + "M" +
                        clsPassiveFlightItinerary.Segments[s].DepartureTime.ToString("ddMMM").ToUpper() + clsPassiveFlightItinerary.Segments[s].Origin.ToUpper() +
                        clsPassiveFlightItinerary.Segments[s].Destination.ToUpper() + "NN1");
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();

                writer.Flush();
                writer.Close();

                WriteLogFileXML(createreq.ToString(),
                    xmllogPath + "_" + clsPassiveFlightItinerary.PNR + "_" + sOrigin + "_" + sDestination + "_UAPI_PassiveSegCreateReq_");

                const string contentType = "text/xml";
                Version _protocolVersion = HttpVersion.Version11;

                ServicePoint sp = ServicePointManager.FindServicePoint(new Uri(clsUAPICredentials.EndPointURL + "/PassiveService"));
                sp.Expect100Continue = false;
                sp.UseNagleAlgorithm = false;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(clsUAPICredentials.EndPointURL + "/PassiveService");
                request.Method = "POST";

                request.ContentType = contentType;
                request.KeepAlive = true;
                request.ProtocolVersion = HttpVersion.Version11;

                //Basic Authentication required to process the request.               
                string _auth = string.Format("{0}:{1}", clsUAPICredentials.UserName, clsUAPICredentials.Password);
                string _enc = Convert.ToBase64String(Encoding.ASCII.GetBytes(_auth));
                string _cred = string.Format("{0} {1}", "Basic", _enc);
                request.Headers.Add(HttpRequestHeader.Authorization, _cred);
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");
                byte[] lbPostBuffer = Encoding.Default.GetBytes(createreq.ToString());
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                request.ContentLength = lbPostBuffer.Length;
                Stream PostStream = request.GetRequestStream();
                PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                PostStream.Close();

                string reservationRsp = string.Empty;
                using (WebResponse response = request.GetResponse())
                {
                    HttpWebResponse httpResponse = response as HttpWebResponse;
                    // handle if response is a compressed stream
                    using (Stream dataStream = GetStreamForResponse(httpResponse))
                    {
                        Encoding enc = Encoding.GetEncoding(1252);
                        using (StreamReader reader = new StreamReader(dataStream, enc))
                        {
                            reservationRsp = reader.ReadToEnd();
                        }
                    }
                }

                try
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(new StringReader(reservationRsp));
                    XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
                    nsmgr.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
                    nsmgr.AddNamespace("universal", "http://www.travelport.com/schema/universal_v42_0");
                    nsmgr.AddNamespace("passive", "http://www.travelport.com/schema/passive_v42_0");

                    sURLCode = Convert.ToString(xmlDoc.
                        SelectSingleNode("/soap:Envelope/soap:Body/universal:PassiveCreateReservationRsp/universal:UniversalRecord", nsmgr).Attributes["LocatorCode"].Value);

                    sURLCode += "|" + Convert.ToString(xmlDoc.
                        SelectSingleNode("/soap:Envelope/soap:Body/universal:PassiveCreateReservationRsp/universal:UniversalRecord/passive:PassiveReservation", nsmgr).Attributes["LocatorCode"].Value);

                    WriteLogFileXML(reservationRsp,
                        xmllogPath + "_" + sURLCode.Split('|')[1] + "_" + sOrigin + "_" + sDestination + "_UAPI_PassiveSegCreateRsp_");
                }
                catch(Exception exe)
                {
                    WriteLogFileXML(reservationRsp,
                        xmllogPath + "_" + clsPassiveFlightItinerary.PNR + "_" + sOrigin + "_" + sDestination + "_UAPI_PassiveSegCreateRsp_");

                    throw exe;
                }
            }
            catch (SoapException se)
            {
                WriteLogFileText(xmllogPath + "_" + sOrigin + "_" + sDestination + "_UAPI_PassiveSegSoapError_", Convert.ToString(se.GetBaseException()));
                throw se;
            }
            catch (Exception ex)
            {
                WriteLogFileText(xmllogPath + "_" + sOrigin + "_" + sDestination + "_UAPI_PassiveSegError_", Convert.ToString(ex.GetBaseException()));
                throw ex;
            }

            return sURLCode;
        }

        /// <summary>
        /// To generate text file for the given string of data
        /// </summary>
        /// <param name="data"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static void WriteLogFileText(string fileName, string data)
        {
            string filePath = string.Empty;
            try
            {
                File.WriteAllText(fileName + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".txt", data);
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        /// <summary>
        /// To generate xml file based on given string of xml data
        /// </summary>
        /// <param name="data"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static string WriteLogFileXML(string data, string fileName)
        {
            string filePath = string.Empty;
            try
            {
                XmlDocument doc = new XmlDocument();
                filePath = fileName + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                doc.LoadXml(data);
                doc.Save(filePath);
            }
            catch (Exception ex)
            {
                WriteLogFileText(fileName + "_Error_", Convert.ToString(ex.GetBaseException()));
            }
            return filePath;
        }

        /// <summary>
        /// To get the exact stream of data based on encoding format
        /// </summary>
        /// <param name="webResponse"></param>
        /// <returns></returns>
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = webResponse.GetResponseStream();
                    break;
            }
            return stream;
        }

        /// <summary>
        /// To replace NonASCII characters from the input string
        /// </summary>
        /// <param name="sInput"></param>
        /// <returns></returns>
        public static string ReplaceNonASCII(string sInput)
        {
            try
            {
                return Encoding.ASCII.GetString(
                    Encoding.Convert(
                        Encoding.UTF8,
                        Encoding.GetEncoding(
                            Encoding.ASCII.EncodingName,
                            new EncoderReplacementFallback(string.Empty),
                            new DecoderExceptionFallback()
                            ),
                        Encoding.UTF8.GetBytes(sInput)
                    )
                );
            }
            catch (Exception ex)
            {
                return sInput;
            }
        }
    }
}






