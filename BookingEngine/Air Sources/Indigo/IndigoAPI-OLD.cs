﻿#region NameSpace

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;

using CT.Configuration;
using CT.Core;


using Indigo.BookingMgr; //Reference to Indigo BookingManager WebService
using Indigo.SessionMgr; //Reference to Indigo SessionManager WebService
using Indigo.ContentMgr; //Reference to Indigo ContentManager WebService
using System.Net;

#endregion

namespace CT.BookingEngine.GDS
{
    [Serializable]
    public class IndigoAPI:IDisposable
    {
        #region IndigoAPI member variables
       // AutoResetEvent[] rteventFlag = new AutoResetEvent[0];
        string xmlLogPath;
        string agentId;
        string agentDomain;
        string password;
        string organisationCode;
        int contractVersion;
        string agentBaseCurrency;
        int agentDecimalValue;
        decimal rateOfExchange;
        int appUserId;
        const int sessionTimeOut = 10;//Indigo session time out (10 minutes)

        DataTable dtBaggage = new DataTable(); //baggage datatable which holds the complete baggage information


        Dictionary<string, decimal> exchangeRates;

        string loginSignature = "";//Login response signature.

        //Indigio Web Service Calls.
        BookingManagerClient bookingAPI = new BookingManagerClient();
        SessionManagerClient sessionAPI = new SessionManagerClient();
        ContentManagerClient contentAPI = new ContentManagerClient();

        //Search variables
        SearchRequest request = new SearchRequest();
        //AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        //AutoResetEvent[] priceEventFlag = new AutoResetEvent[0];

        List<SearchResult> CombinedSearchResults = new List<SearchResult>();//List which holds the flight results for both one way and return type.

        Hashtable pricedItineraries = new Hashtable();


        int oneWayFareBreakDowncount = 0; //One way fare break down.
        List<SearchResult> oneWayResultList = new List<SearchResult>();//Onward journey Flight Results List
        List<Indigo.BookingMgr.Fare> faresList = new List<Indigo.BookingMgr.Fare>();

        //Round Trip Journeys Price Itenarary variables.

        List<Journey> OnwardJourneys = new List<Journey>();
        List<Journey> ReturnJourneys = new List<Journey>();

        List<Indigo.BookingMgr.Fare> roundTripOnwardFaresList = new List<Indigo.BookingMgr.Fare>();
        List<Indigo.BookingMgr.Fare> roundTripReturnFaresList = new List<Indigo.BookingMgr.Fare>();

        LogonResponse logonResponse = new LogonResponse();
        List<SearchResult> roundTripResultList = new List<SearchResult>();
        string currencyCode;//For India GST implementation the currency code should be in INR format.

        //Added for GST flow
        //Capture the GST inputs from the agentmaster and location master.
        string gstNumber;
        string gstCompanyName;
        string gstOfficialEmail;

        //Added for corporate booking flow
        string promoCode;

        #endregion

        #region Properties

        public string LoginName
        {
            get { return agentId; }
            set { agentId = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string AgentDomain
        {
            get { return agentDomain; }
            set { agentDomain = value; }
        }

        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public string AgentBaseCurrency
        {
            get { return agentBaseCurrency; }
            set { agentBaseCurrency = value; }
        }

        public int AgentDecimalValue
        {
            get { return agentDecimalValue; }
            set { agentDecimalValue = value; }
        }

        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }

        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }

        public string GSTNumber
        {
            get { return gstNumber; }
            set { gstNumber = value; }
        }
        public string GSTCompanyName
        {
            get { return gstCompanyName; }
            set { gstCompanyName = value; }
        }
        public string GSTOfficialEmail
        {
            get { return gstOfficialEmail; }
            set { gstOfficialEmail = value; }
        }
        public string PromoCode
        {
            get { return promoCode; }
            set { promoCode = value; }
        }

        #endregion

        #region Default Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public IndigoAPI()
        {
            xmlLogPath = ConfigurationSystem.IndigoConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMMyyyy") + "\\";
            if (!Directory.Exists(xmlLogPath))
            {
                Directory.CreateDirectory(xmlLogPath);
            }

            //agentId = ConfigurationSystem.IndigoConfig["AgentId"];
            //agentDomain = ConfigurationSystem.IndigoConfig["AgentDomain"];
            //password = ConfigurationSystem.IndigoConfig["Password"];
            //organisationCode = ConfigurationSystem.IndigoConfig["OrganisationCode"];
            contractVersion = Convert.ToInt32(ConfigurationSystem.IndigoConfig["ContractVersion"]);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;//Added by Lokesh on 14/03/2018.

            if (dtBaggage.Columns.Count == 0)
            {
                dtBaggage.Columns.Add("Code", typeof(string));
                dtBaggage.Columns.Add("Price", typeof(decimal));
                dtBaggage.Columns.Add("Group", typeof(int));
                dtBaggage.Columns.Add("Currency", typeof(string));
                dtBaggage.Columns.Add("Description", typeof(string));
                dtBaggage.Columns.Add("QtyAvailable", typeof(int));
            }
            //Assign the web service url from source xml config

            bookingAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.IndigoConfig["BookingService"]);
            sessionAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.IndigoConfig["SessionService"]);
            contentAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.IndigoConfig["ContentService"]);
        }
        #endregion

        #region Session Methods
        /// <summary>
        /// Authenticates and creates a session for the specified user.
        /// </summary>
        /// <returns></returns>
        private LogonResponse Login()
        {

            FlightSessionData sessionData = new FlightSessionData();

            try
            {

                if (Basket.FlightBookingSession != null && !Basket.FlightBookingSession.ContainsKey(appUserId.ToString()))
                {
                    LogonRequest request = new LogonRequest();
                    request.logonRequestData = new LogonRequestData();
                    request.logonRequestData.AgentName = agentId;
                    request.logonRequestData.DomainCode = agentDomain;
                    request.logonRequestData.LocationCode = "";
                    request.logonRequestData.RoleCode = "";
                    request.logonRequestData.Password = password;
                    request.logonRequestData.RoleCode = "";
                    request.logonRequestData.TerminalInfo = "";
                    request.ContractVersion = contractVersion;
                    //Added by Lokesh on 11-June-2018
                    //New skies Version 4.5
                    //Need to send EnableExceptionStackTrace = false(by default) in the request.
                    request.EnableExceptionStackTrace = false;

                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(LogonRequest));
                        string filePath = xmlLogPath + "IndigoLogonRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, request);
                        sw.Close();
                    }
                    catch { }

                    logonResponse = sessionAPI.Logon(request);
                    sessionData.SearchTime = DateTime.Now;
                    //Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo Login Time : " + DateTime.Now, "");
                    sessionData.Log = new List<string>();
                    sessionData.Log.Add(logonResponse.Signature);
                    if (Basket.FlightBookingSession.ContainsKey(appUserId.ToString()))
                    {
                        Basket.FlightBookingSession[appUserId.ToString()] = sessionData;
                    }
                    else
                    {
                        Basket.FlightBookingSession.Add(appUserId.ToString(), sessionData);
                    }
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(LogonResponse));
                        string filePath = xmlLogPath + "IndigoLogonResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, logonResponse);
                        sw.Close();
                    }
                    catch { }
                }
                else
                {
                    sessionData = Basket.FlightBookingSession[appUserId.ToString()];
                    //If the user search time is greater tan the default session time out then we will call login  method to get a new signature.
                    if (DateTime.Now.Subtract(sessionData.SearchTime).Minutes > sessionTimeOut)
                    {
                        Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo Session Expired.Creating a new session for user: " + appUserId.ToString() + DateTime.Now, "");

                        Basket.FlightBookingSession.Remove(appUserId.ToString());//from the basket we will remove that particular user id as session expires.
                        logonResponse = Login();
                    }
                    else
                    {
                        logonResponse.Signature = sessionData.Log[0];
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Indigo Login failed. Reason : " + ex.Message, ex);
            }

            return logonResponse;
        }
        /// <summary>
        /// Ends a user’s session and clears session from the server.
        /// </summary>
        /// <param name="signature"></param>
        /// <returns></returns>
        private LogoutResponse Logout(string signature)
        {
            LogoutResponse response = new LogoutResponse();

            try
            {
                LogoutRequest request = new LogoutRequest();
                request.Signature = signature;
                request.ContractVersion = contractVersion;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                request.EnableExceptionStackTrace = false;

                XmlSerializer ser = new XmlSerializer(typeof(LogoutRequest));
                string filePath = xmlLogPath + "IndigoLogoutRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, request);
                sw.Close();
                //Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo Logout Request Sent Time : " + DateTime.Now, "");
                response =  sessionAPI.Logout(request);
            }
            catch (Exception ex)
            {
                throw new Exception("Indigo Logout failed.Reason : " + ex.ToString(), ex);
            }
            finally
            {
                Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo Clearing Session For the User : " + appUserId.ToString() + " " + DateTime.Now, "");
                Basket.FlightBookingSession.Remove(appUserId.ToString());
            }

            return response;
        }

        #endregion

        #region Search Methods

        /// <summary>
        ///This method returns a boolean which determine whether to send a  Search Request to get the flight inventory based on the sector list that is provided by Indigo.
        /// </summary>
        /// <param name="searchRequest"></param>
        /// <returns></returns>
        public bool AllowSearch(SearchRequest searchRequest)
        {
            bool allowSearch = false;
            try
            {
                if (ConfigurationSystem.SectorListConfig.ContainsKey("Indigo"))
                {
                    Dictionary<string, string> sectors = ConfigurationSystem.SectorListConfig["Indigo"];
                    if (sectors.ContainsKey(searchRequest.Segments[0].Origin))
                    {
                        if (sectors[searchRequest.Segments[0].Origin].Contains(searchRequest.Segments[0].Destination))
                        {
                            allowSearch = true;
                        }
                    }
                }
                else
                {
                    Core.Audit.Add(Core.EventType.Book, Core.Severity.High, appUserId, "Indigo: Error While reading sector from SectorListConfig : Error" + DateTime.Now, "");
                }
            }
            catch (Exception ex)
            {
                Core.Audit.Add(Core.EventType.Book, Core.Severity.High, appUserId, "Indigo: Error While reading sector from SectorListConfig : Error" + ex.ToString() + DateTime.Now, "");
            }
            return allowSearch;
        }


        /// <summary>
        /// Gets flight availability and fare information for both oneway and return trips
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchResult[] Search(SearchRequest request)
        {
            /*  The following steps describes the complete flow for Indigo(6E) Source.
             * 01.Logon Request -- Logon Response (For authentication and for signature)
             * 02. GetAvailability Request --GetAvailability Response (Flight Availability Details)
             * 03. PriceItinerary Request -- PriceItinerary Response(Returns the fare break down for each itenary).
             * 04. Sell
             * 05. UpdatePassengers
             * 06. AddPaymenttoBooking
             * 07. BookingCommit
             * 08: LogOut Request
             */

            SearchResult[] results = new SearchResult[0];
            List<SearchResult> ResultList = new List<SearchResult>();
            LogonResponse loginResponse = null;
            try
            {
                if (AllowSearch(request))//Allow search only if the requested origin and destination is avaialble in the sectorlist provided by Indigo.
                {
                    //Authenticates and creates a session for the specified user
                    loginResponse = Login();
                    if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature) && loginResponse.Signature.Length > 0)
                    {
                        #region After successful authentication call the necessary Indigo methods to get the results.

                        if (loginResponse != null && loginResponse.Signature.Length > 0)
                        {
                            TripAvailabilityResponse tripAvailResponse = null;
                            this.request = request;
                            loginSignature = loginResponse.Signature;

                            if (request.Type == SearchType.Return)
                            {
                                SearchForRetailFare();
                            }
                            //As the search is of one way type we will call all fares
                            else if (request.Type == SearchType.OneWay)
                            {
                                try
                                {
                                    #region One Way Search Results

                                    #region GetAvailabilityRequest Object
                                    //S-1:Create object for GetAvailabilityRequest
                                    GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                                    GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                                    //Assign Signature generated from Session
                                    availRequest.Signature = loginResponse.Signature;
                                    availRequest.ContractVersion = contractVersion;
                                    //Added by Lokesh on 11-June-2018
                                    //New skies Version 4.5
                                    //Need to send EnableExceptionStackTrace = false(by default) in the request.
                                    availRequest.EnableExceptionStackTrace = false;
                                    availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();

                                    if (request.Type == SearchType.OneWay)
                                    {
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                                    }

                                    //Assign all Availability details to get flight details
                                    for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                                    {
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = Indigo.BookingMgr.FlightType.All;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "6E";
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = false;

                                        //For connecting flights 
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 10;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = Indigo.BookingMgr.AvailabilityType.Default;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;

                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;

                                        //Kindly enter value as ‘R’,  ‘Z’ and ‘M’ in tag namely FareTypes in GetAvailability API call and set the value as ‘CompressByProductClass for the tag namely FareClassControl  and set ProductClasses tag to Null or enter all the available ProductClasses as mentioned below to get the lowest fare among the all ProductClasses.
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R"; //Retail Fare
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "Z"; //Lite Fare
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "F"; //SME FARE

                                        int paxTypeCount = 1;
                                        if (request.ChildCount > 0)
                                        {
                                            paxTypeCount++;
                                        }
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(request.AdultCount + request.ChildCount);
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxTypeCount];


                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[0] = new PaxPriceType();
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[0].PaxType = "ADT";//Adult Passenger

                                        if (request.ChildCount > 0)
                                        {
                                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[1] = new PaxPriceType();
                                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[1].PaxType = "CHD"; //Child Passenger
                                        }

                                    }
                                    #endregion

                                    #region Indigo One Way SearchRequest and Search Response Logs
                                    try
                                    {


                                        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                                        string filePath = xmlLogPath + "IndigoSearchRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                        StreamWriter sw = new StreamWriter(filePath);
                                        ser.Serialize(sw, availRequest);
                                        sw.Close();

                                    }
                                    catch (Exception ex)
                                    {
                                        Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                                    }

                                    //S-2:GetAvailability: -Gets flight availability and fare information.
                                    //Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo One Way search request sent: " + DateTime.Now, "");
                                   GetAvailabilityResponse availResponse_v_4_5  = bookingAPI.GetAvailability(availRequest);
                                    tripAvailResponse = availResponse_v_4_5.GetTripAvailabilityResponse;
                                    //Again we will reset the search time for maintaining the idle time out
                                    if (Basket.FlightBookingSession.ContainsKey(appUserId.ToString()))
                                    {
                                        FlightSessionData sessionData = Basket.FlightBookingSession[appUserId.ToString()];
                                        sessionData.SearchTime = DateTime.Now;
                                    }

                                    //Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo One Way search response received: " + DateTime.Now, "");


                                    try
                                    {
                                        string filePath = xmlLogPath + "IndigoSearchResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                        XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                                        doc.Save(filePath);
                                    }
                                    catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                                    
                                    #endregion

                                    if (tripAvailResponse != null && request.Type == SearchType.OneWay)
                                    {
                                        List<Journey> OnwardJourneys = new List<Journey>();

                                        /*(tripAvailResponse.Schedules) is a Jagged Array(Array of Arrays) */

                                        if (tripAvailResponse.Schedules.Length > 0)
                                        {
                                            /*S-1: Read the total number of schedules elements*/
                                            for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                                            {
                                                /*Root element Schedules*/
                                                for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                                                {
                                                    /*S-2: Read the total number of journey fields for each schedule elemnent*/
                                                    if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                                                    {
                                                        OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                                                    }
                                                }
                                            }
                                        }


                                        if (OnwardJourneys.Count > 0)
                                        {
                                            int fareBreakDownCount = 0;

                                            if (request.AdultCount > 0)
                                            {
                                                fareBreakDownCount = 1;
                                            }
                                            if (request.ChildCount > 0)
                                            {
                                                fareBreakDownCount++;
                                            }
                                            if (request.InfantCount > 0)
                                            {
                                                fareBreakDownCount++;
                                            }
                                            oneWayFareBreakDowncount = fareBreakDownCount;
                                            for (int i = 0; i < OnwardJourneys.Count; i++)
                                            {
                                                if (OnwardJourneys[i].Segments.Length > 1)
                                                {
                                                    GetConnectingFlightsResultsForOneWay(OnwardJourneys[i], fareBreakDownCount);
                                                }
                                                else
                                                {

                                                    for (int j = 0; j < OnwardJourneys[i].Segments[0].Fares.Length; j++)
                                                    {
                                                        GenerateFlightResultForOneWayDirectFlights(OnwardJourneys[i], OnwardJourneys[i].Segments[0].Fares[j]);
                                                    }

                                                }
                                            }
                                            CombinedSearchResults.AddRange(oneWayResultList);
                                        }
                                    }
                                    #endregion
                                }
                                catch (Exception ex)
                                {
                                    if (ex.Message.ToLower().Contains("session"))//Session Exception
                                    {
                                        Basket.FlightBookingSession.Remove(appUserId.ToString());//Clear basket
                                    }
                                    Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to get results for One-way search. Reason : " + ex.ToString(), "");
                                }
                            }

                            results = CombinedSearchResults.ToArray();//Combine all the results and add them to array.
                        }
                        #endregion
                    }
                    else
                    {
                        throw new Exception("(Indigo)Failed to get the Login Response");
                    }

                }
                else
                {
                    throw new Exception("Indigo Search failed.Reason:Requested origin and destination not available");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Indigo Search failed. Reason : " + ex.ToString(), ex);
            }
            
            Array.Sort(results, delegate(SearchResult h1, SearchResult h2) { return h1.TotalFare.CompareTo(h2.TotalFare); });

            if (results.Length > 0)
            {
                //Get the Tax Details information with respect to airline code,origin and destination.
                CT.BookingEngine.AirlineJourneyPriceDetails priceDetails = new CT.BookingEngine.AirlineJourneyPriceDetails();
                decimal savedTax = priceDetails.LoadTaxDetails("6E", request.Segments[0].Origin, request.Segments[0].Destination, request.Type,currencyCode);

                for (int i = 0; i < results.Length; i++)
                {
                    if (savedTax <= 0) //If there is no tax avialble in the table then we will save the tax in the table from itenarary response.
                    {
                        CalculateTax(ref results[i], savedTax);
                        savedTax = priceDetails.LoadTaxDetails("6E", request.Segments[0].Origin, request.Segments[0].Destination, request.Type,currencyCode);
                    }
                    else //Tax avialble so we will directly calculate tax from the available.
                    {
                        CalculateTax(ref results[i], savedTax);
                    }
                }
            }
            if(results.Length >0 && request.Type == SearchType.Return)
            {
                //New Logic Combinations Suggested by Indigo:24July,2018
                //In case of RoundTrip Journey the following combinations only allowed
                //1. Flexi fare can be combined with Flexi fare only.
                //2.Corporate fare can be combined with Corporate Fares.
                //3.SME fare can be combined with SME Fares.
                //4.Lite fare can be combined with Lite Fares.
                //5.Sale,RoundTrip,Retail,SpecialRoundTripCan Perform combinations.

                List<SearchResult> filteredResults = new List<SearchResult>();
                if(results.Length > 0)
                {
                   for(int i=0;i< results.Length;i++)
                    {
                        if(results[i].FareType.Split(',')[0].ToLower().Contains("flexi") == false && results[i].FareType.Split(',')[1].ToLower().Contains("flexi") == false)
                        {
                            if (results[i].FareType.Split(',')[0].ToLower().Contains("lite") == false && results[i].FareType.Split(',')[1].ToLower().Contains("lite") == false)
                            {
                                if (results[i].FareType.Split(',')[0].ToLower().Contains("corporate") == false && results[i].FareType.Split(',')[1].ToLower().Contains("corporate") == false)
                                {
                                    if (results[i].FareType.Split(',')[0].ToLower().Contains("sme") == false && results[i].FareType.Split(',')[1].ToLower().Contains("sme") == false)
                                    {
                                        filteredResults.Add(results[i]);
                                    }
                                    else if (results[i].FareType.Split(',')[0].ToLower().Contains("sme") == true && results[i].FareType.Split(',')[1].ToLower().Contains("sme") == true)
                                    {
                                        filteredResults.Add(results[i]);
                                    }
                                }
                                else if (results[i].FareType.Split(',')[0].ToLower().Contains("corporate") == true && results[i].FareType.Split(',')[1].ToLower().Contains("corporate") == true)
                                {
                                    filteredResults.Add(results[i]);
                                }
                            }
                            else if(results[i].FareType.Split(',')[0].ToLower().Contains("lite") == true && results[i].FareType.Split(',')[1].ToLower().Contains("lite") == true)
                            {
                                filteredResults.Add(results[i]);
                            }
                        }
                        else if (results[i].FareType.Split(',')[0].ToLower().Contains("flexi") == true && results[i].FareType.Split(',')[1].ToLower().Contains("flexi") == true)
                        {
                            filteredResults.Add(results[i]);

                        }
                    }
                }
                if(filteredResults.Count >0)
                {
                    results = filteredResults.ToArray();
                }
            }
            return results;
        }

        #endregion

        #region OneWay Flight Result Creation

        /// <summary>
        /// Generate Flight Result For OneWay if the flights are direct flights.
        /// </summary>
        /// <param name="OnwardJourney"></param>
        /// <param name="OnwardFare"></param>
        private void GenerateFlightResultForOneWayDirectFlights(Journey OnwardJourney, Indigo.BookingMgr.Fare OnwardFare)
        {
            try
            {
                //Here it is a direct flight--So there will be only one segment.
                if (OnwardJourney != null && OnwardFare != null && OnwardJourney.Segments != null && OnwardJourney.Segments.Length >0 && OnwardJourney.Segments[0] != null)
                {
                    Journey journey = OnwardJourney;
                    Indigo.BookingMgr.Fare fare = OnwardFare;
                    SearchResult result = new SearchResult();
                    result.IsLCC = true;
                    result.ResultBookingSource = BookingSource.Indigo;
                    result.Airline = "6E";
                    result.Currency = agentBaseCurrency;
                    result.EticketEligible = true;
                    result.FareBreakdown = new Fare[oneWayFareBreakDowncount];
                    result.FareRules = new List<FareRule>();
                    FareRule fareRule = new FareRule();
                    fareRule.Airline = result.Airline;
                    fareRule.Destination = request.Segments[0].Destination;
                    fareRule.FareBasisCode = fare.FareBasisCode;
                    fareRule.FareInfoRef = fare.RuleNumber;
                    fareRule.Origin = request.Segments[0].Origin;
                    fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                    fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                    result.FareRules.Add(fareRule);
                    result.JourneySellKey = journey.JourneySellKey;
                    result.FareType = GetFareTypeForProductClass(fare.ProductClass);
                    result.FareSellKey = fare.FareSellKey;
                    result.NonRefundable = false;

                    if (request.Type == SearchType.OneWay)
                    {
                        result.Flights = new FlightInfo[1][];
                    }
                    List<FlightInfo> directFlights = GetFlightInfo(OnwardJourney.Segments[0], OnwardJourney, fare.ProductClass, "ONWARD");

                    if (directFlights != null && directFlights.Count > 0)
                    {
                        List<FlightInfo> listOfFlights = new List<FlightInfo>();
                        listOfFlights.AddRange(directFlights);
                        result.Flights[0] = listOfFlights.ToArray();
                    }
                    //List which holds the Pax types  : "ADT","CHD" ,"INFT";
                    List<string> FarePaxtypes = new List<string>();
                    if (request.AdultCount > 0)
                    {
                        FarePaxtypes.Add("ADT");
                    }
                    if (request.ChildCount > 0)
                    {
                        FarePaxtypes.Add("CHD");
                    }
                    if (request.InfantCount > 0)
                    {
                        FarePaxtypes.Add("INFT");
                    }
                    //Intially we will calculate the base fare for the result from the search response.
                    CalculateBaseFareForOneWaydirectFlights(ref result, FarePaxtypes, OnwardJourney, OnwardFare);
                    oneWayResultList.Add(result);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to get Retail Fare results." + ex.ToString(), "");

            }
        }


        /// <summary>
        /// Gets connecting flight results for onward journeys only.
        /// </summary>
        /// <param name="journeyOnWard"></param>
        /// <param name="fareBreakDownCount"></param>
        private void GetConnectingFlightsResultsForOneWay(Journey journeyOnWard, int fareBreakDownCount)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {
                if (journeyOnWard != null && journeyOnWard.Segments != null && journeyOnWard.Segments.Length > 0 && journeyOnWard.Segments.Length == 2)
                {
                    /*Fare mapping on segments based on One to One relationship. 
                     *For ex. combine 1st Fare of the 1st Segment with the 1st Fare of the 2nd Segment, 
                     *The 2nd Fare of the 1st Segment with the 2nd Fare of the 2nd Segment and so on (if there are more fares under each Segment).             
                     */
                    Segment firstSegmentCon = null;
                    Segment secondSegmentCon = null;
                    if (journeyOnWard.Segments[0] != null && journeyOnWard.Segments[1] != null)
                    {
                        firstSegmentCon = journeyOnWard.Segments[0];
                        secondSegmentCon = journeyOnWard.Segments[1];
                    }
                    if (firstSegmentCon != null && secondSegmentCon != null && firstSegmentCon.Fares != null && secondSegmentCon.Fares != null &&
                        firstSegmentCon.Fares.Length > 0 && secondSegmentCon.Fares.Length > 0 && (firstSegmentCon.Fares.Length == secondSegmentCon.Fares.Length))
                    {
                        for (int f = 0; f < firstSegmentCon.Fares.Length; f++)
                        {
                            if (firstSegmentCon.Fares[f] != null && secondSegmentCon.Fares[f] != null)
                            {
                                Indigo.BookingMgr.Fare fareFirstConSeg = firstSegmentCon.Fares[f];
                                Indigo.BookingMgr.Fare fareSecondConSeg = secondSegmentCon.Fares[f];

                                SearchResult result = new SearchResult();
                                result.IsLCC = true;
                                result.ResultBookingSource = BookingSource.Indigo;
                                result.Airline = "6E";
                                result.Currency = agentBaseCurrency;
                                result.EticketEligible = true;
                                result.FareBreakdown = new Fare[fareBreakDownCount];
                                result.FareRules = new List<FareRule>();

                                FareRule fareRule = new FareRule();
                                fareRule.Airline = result.Airline;
                                fareRule.Destination = request.Segments[0].Destination;
                                fareRule.FareBasisCode = fareFirstConSeg.FareBasisCode;
                                fareRule.FareInfoRef = fareFirstConSeg.RuleNumber;
                                fareRule.Origin = request.Segments[0].Origin;
                                fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                result.FareRules.Add(fareRule);

                                result.JourneySellKey = journeyOnWard.JourneySellKey;
                                result.FareType = GetFareTypeForProductClass(fareFirstConSeg.ProductClass);
                                result.FareSellKey = fareFirstConSeg.FareSellKey + "^" + fareSecondConSeg.FareSellKey;
                                result.NonRefundable = false;
                                if (request.Type == SearchType.OneWay)
                                {
                                    result.Flights = new FlightInfo[1][];
                                }
                                List<FlightInfo> firstSegmentConFlights = GetFlightInfo(firstSegmentCon, journeyOnWard, fareFirstConSeg.ProductClass, "ONWARD");
                                List<FlightInfo> secondSegmentConFlights = GetFlightInfo(secondSegmentCon, journeyOnWard, fareSecondConSeg.ProductClass, "ONWARD");
                                if (firstSegmentConFlights != null && secondSegmentConFlights != null && firstSegmentConFlights.Count > 0 && secondSegmentConFlights.Count > 0)
                                {
                                    List<FlightInfo> listOfFlights = new List<FlightInfo>();
                                    listOfFlights.AddRange(firstSegmentConFlights);
                                    listOfFlights.AddRange(secondSegmentConFlights);
                                    result.Flights[0] = listOfFlights.ToArray();
                                }
                                //List which holds the Pax types  : "ADT","CHD" ,"INFT";
                                List<string> FarePaxtypes = new List<string>();
                                if (request.AdultCount > 0)
                                {
                                    FarePaxtypes.Add("ADT");
                                }
                                if (request.ChildCount > 0)
                                {
                                    FarePaxtypes.Add("CHD");
                                }
                                if (request.InfantCount > 0)
                                {
                                    FarePaxtypes.Add("INFT");
                                }
                                CalculateBaseFareForOneWayConnectingFlights(ref result, FarePaxtypes, journeyOnWard, fareFirstConSeg, fareSecondConSeg);
                               
                                ResultList.Add(result);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Indigo failed to get Connecting Flights Results.Reason : " + ex.Message, ex);
            }

            if (ResultList.Count > 0)
            {
                oneWayResultList.AddRange(ResultList);
            }
        }

        #endregion

        #region Return trip Flight Result Creation
        /// <summary>
        /// Gets the flight availability details for Retail Fare Product Class
        /// </summary>
        /// <param name="eventNumber"></param>
        /// 
        public void SearchForRetailFare()
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {
                #region Flight Results For Different Fare Types

                #region GetAvailabilityRequest Object
                //S-1:Create object for GetAvailabilityRequest
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                //Assign Signature generated from Session
                availRequest.Signature = loginSignature;
                availRequest.ContractVersion = contractVersion;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                availRequest.EnableExceptionStackTrace = false;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();

                if (request.Type == SearchType.OneWay)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }
                else
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                }

                //Assign all Availability details to get flight details
                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = Indigo.BookingMgr.FlightType.All;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "6E";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = false;

                    //Kindly enter value as ‘R’,  ‘Z’ and ‘M’ in tag namely FareTypes in GetAvailability API call and set the value as ‘CompressByProductClass for the tag namely FareClassControl  and set ProductClasses tag to Null or enter all the available ProductClasses as mentioned below to get the lowest fare among the all ProductClasses.
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";//Retail Fare
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "Z"; //Lite Fare
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "F"; //Corporate Fare

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 10;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = Indigo.BookingMgr.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;

                    int paxCount = 1;
                    if (request.ChildCount > 0)
                    {
                        paxCount++;
                    }

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(request.AdultCount + request.ChildCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[0] = new PaxPriceType();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[0].PaxType = "ADT";//Adult Passenger

                    if (request.ChildCount > 0)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[1] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[1].PaxType = "CHD"; //Child Passenger
                    }
                }

                #region Indigo Round trip SearchRequest and Search Response Logs
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                    string filePath = xmlLogPath + "IndigoRoundtrip_" + appUserId.ToString() + "_" + GetFareTypeForProductClass("R").Replace(" ", "_") + "_SearchRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, availRequest);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                }

                //S-2:GetAvailability: -Gets flight availability and fare information.

                //Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo round trip search request sent: " + DateTime.Now, "");
                GetAvailabilityResponse availResponse_v_4_5 = bookingAPI.GetAvailability(availRequest);
                TripAvailabilityResponse tripAvailResponse = availResponse_v_4_5.GetTripAvailabilityResponse;
                //Again we will reset the search time for maintaining the idle time out
                if (Basket.FlightBookingSession.ContainsKey(appUserId.ToString()))
                {
                    FlightSessionData sessionData = Basket.FlightBookingSession[appUserId.ToString()];
                    sessionData.SearchTime = DateTime.Now;
                }
                //Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo round trip search response received: " + DateTime.Now, "");

                try
                {
                    string filePath = xmlLogPath + "IndigoRoundtrip_" + appUserId.ToString() + "_" + GetFareTypeForProductClass("R").Replace(" ", "_") + "_SearchResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    XmlDocument doc = CustomXmlSerializer.Serialize(tripAvailResponse, 1, "GetAvailResponse");
                    doc.Save(filePath);
                }
                catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                
                #endregion

                #endregion

                #region Flight Result object creation
                if (tripAvailResponse != null)
                {
                    List<Journey> OnwardJourneys = new List<Journey>();
                    List<Journey> ReturnJourneys = new List<Journey>();

                    List<Indigo.BookingMgr.Fare> onwardFares = new List<Indigo.BookingMgr.Fare>();
                    List<Indigo.BookingMgr.Fare> returnFares = new List<Indigo.BookingMgr.Fare>();

                    for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                    {
                        for (int j = 0; j < tripAvailResponse.Schedules[i].Length; j++)
                        {
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Origin)
                            {
                                OnwardJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                            if (tripAvailResponse.Schedules[i][j].DepartureStation == request.Segments[0].Destination)
                            {
                                ReturnJourneys.AddRange(tripAvailResponse.Schedules[i][j].Journeys);
                            }
                        }
                    }
                    for (int i = 0; i < OnwardJourneys.Count; i++)
                    {
                        for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                        {
                            onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                        }
                    }

                    for (int j = 0; j < ReturnJourneys.Count; j++)
                    {
                        for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                        {
                            returnFares.AddRange(ReturnJourneys[j].Segments[k].Fares);
                        }
                    }
                    int fareBreakDownCount = 0;

                    if (request.AdultCount > 0)
                    {
                        fareBreakDownCount = 1;
                    }
                    if (request.ChildCount > 0)
                    {
                        fareBreakDownCount++;
                    }
                    if (request.InfantCount > 0)
                    {
                        fareBreakDownCount++;
                    }
                    if (request.Type == SearchType.Return)
                    {

                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            for (int j = 0; j < ReturnJourneys.Count; j++)
                            {
                                //Case-1:Both Onward and Return Direct Flights
                                if (OnwardJourneys[i] != null && ReturnJourneys[j] != null && OnwardJourneys[i].Segments != null && OnwardJourneys[i].Segments.Length == 1 && ReturnJourneys[j].Segments != null && ReturnJourneys[j].Segments.Length == 1)
                                {
                                    if (OnwardJourneys[i].Segments[0].Fares != null && ReturnJourneys[j].Segments[0].Fares != null && OnwardJourneys[i].Segments[0].Fares.Length > 0 && ReturnJourneys[j].Segments[0].Fares.Length > 0)
                                    {
                                        foreach (Indigo.BookingMgr.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                        {
                                            foreach (Indigo.BookingMgr.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                            {
                                                if (onfare != null && retFare != null)
                                                {
                                                    SearchResult result = new SearchResult();
                                                    result.IsLCC = true;
                                                    result.ResultBookingSource = BookingSource.Indigo;
                                                    result.Airline = "6E";
                                                    result.Currency = agentBaseCurrency;
                                                    result.EticketEligible = true;
                                                    result.FareBreakdown = new Fare[fareBreakDownCount];
                                                    result.NonRefundable = false;

                                                    result.FareRules = new List<FareRule>();
                                                    FareRule fareRule = new FareRule();
                                                    fareRule.Airline = result.Airline;
                                                    fareRule.Destination = request.Segments[0].Destination;
                                                    fareRule.FareBasisCode = onfare.FareBasisCode;
                                                    fareRule.FareInfoRef = onfare.RuleNumber;
                                                    fareRule.Origin = request.Segments[0].Origin;
                                                    fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                                    fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                                    result.FareRules.Add(fareRule);

                                                    result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                    result.FareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                    result.FareSellKey = onfare.FareSellKey;

                                                    if (request.Type == SearchType.OneWay)
                                                    {
                                                        result.Flights = new FlightInfo[1][];
                                                    }
                                                    else
                                                    {
                                                        result.Flights = new FlightInfo[2][];
                                                    }

                                                    List<FlightInfo> onwardFlights = GetFlightInfo(OnwardJourneys[i].Segments[0], OnwardJourneys[i], onfare.ProductClass, "ONWARD");

                                                    if (onwardFlights != null && onwardFlights.Count > 0)
                                                    {
                                                        List<FlightInfo> listOfOnwardFlights = new List<FlightInfo>();
                                                        listOfOnwardFlights.AddRange(onwardFlights);
                                                        result.Flights[0] = listOfOnwardFlights.ToArray();
                                                    }
                                                    CalculateBaseFareForReturnDirectFlights(ref result, OnwardJourneys[i], onfare);

                                                    result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                    result.FareType += "," + GetFareTypeForProductClass(retFare.ProductClass);
                                                    result.FareSellKey += "|" + retFare.FareSellKey;

                                                    List<FlightInfo> returnFlights = GetFlightInfo(ReturnJourneys[j].Segments[0], ReturnJourneys[j], retFare.ProductClass, "RETURN");

                                                    if (returnFlights != null && returnFlights.Count > 0)
                                                    {
                                                        List<FlightInfo> listOfReturnFlights = new List<FlightInfo>();
                                                        listOfReturnFlights.AddRange(returnFlights);
                                                        result.Flights[1] = listOfReturnFlights.ToArray();
                                                    }

                                                    CalculateBaseFareForReturnDirectFlights(ref result, ReturnJourneys[j], retFare);
                                                    ResultList.Add(result);
                                                }
                                            }
                                        }
                                    }

                                }

                                //Case-2:Both Onward and Return Connecting Flights
                                else if ((OnwardJourneys[i] != null && OnwardJourneys[i].Segments != null && OnwardJourneys[i].Segments.Length > 1) && (ReturnJourneys[j] != null && ReturnJourneys[j].Segments != null && ReturnJourneys[j].Segments.Length > 1))
                                {
                                    List<SearchResult> conFlightsResults = GetReturnConnectingFlightsCombinationsResults(OnwardJourneys[i], ReturnJourneys[j], fareBreakDownCount);
                                    if (conFlightsResults.Count > 0)
                                    {
                                        ResultList.AddRange(conFlightsResults);
                                    }
                                }

                                //Case-3: Onward Direct and Return Connecting
                                else if (OnwardJourneys[i] != null && OnwardJourneys[i].Segments != null && OnwardJourneys[i].Segments.Length == 1 && ReturnJourneys[j] != null && ReturnJourneys[j].Segments != null && ReturnJourneys[j].Segments.Length ==2)
                                {
                                    if (OnwardJourneys[i].Segments[0].Fares != null && OnwardJourneys[i].Segments[0].Fares.Length > 0 && ReturnJourneys[j].Segments[0].Fares != null && ReturnJourneys[j].Segments[0].Fares.Length > 0 && ReturnJourneys[j].Segments[1].Fares != null && ReturnJourneys[j].Segments[1].Fares.Length > 0)
                                    {
                                        Segment firstSegmentConReturn = null;
                                        Segment secondSegmentConReturn = null;
                                        if (ReturnJourneys[j].Segments[0] != null && ReturnJourneys[j].Segments[1] != null)
                                        {
                                            firstSegmentConReturn = ReturnJourneys[j].Segments[0];
                                            secondSegmentConReturn = ReturnJourneys[j].Segments[1];
                                        }
                                        if (firstSegmentConReturn != null && secondSegmentConReturn != null && firstSegmentConReturn.Fares.Length == secondSegmentConReturn.Fares.Length)
                                        {
                                            for (int f = 0; f < OnwardJourneys[i].Segments[0].Fares.Length; f++)
                                            {
                                                for (int g = 0; g < firstSegmentConReturn.Fares.Length; g++)
                                                {
                                                    if (OnwardJourneys[i].Segments[0].Fares[f] != null && firstSegmentConReturn.Fares[g] != null && secondSegmentConReturn.Fares[g] != null)
                                                    {
                                                        Indigo.BookingMgr.Fare fareFirstConSegReturn = firstSegmentConReturn.Fares[g];
                                                        Indigo.BookingMgr.Fare fareSecondConSegReturn = secondSegmentConReturn.Fares[g];

                                                        SearchResult result = new SearchResult();
                                                        result.IsLCC = true;
                                                        result.ResultBookingSource = BookingSource.Indigo;
                                                        result.Airline = "6E";
                                                        result.Currency = agentBaseCurrency;
                                                        result.EticketEligible = true;
                                                        result.FareBreakdown = new Fare[fareBreakDownCount];
                                                        result.NonRefundable = false;

                                                        result.FareRules = new List<FareRule>();
                                                        FareRule fareRule = new FareRule();
                                                        fareRule.Airline = result.Airline;
                                                        fareRule.Destination = request.Segments[0].Destination;
                                                        fareRule.FareBasisCode = OnwardJourneys[i].Segments[0].Fares[f].FareBasisCode;
                                                        fareRule.FareInfoRef = OnwardJourneys[i].Segments[0].Fares[f].RuleNumber;
                                                        fareRule.Origin = request.Segments[0].Origin;
                                                        fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                                        fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                                        result.FareRules.Add(fareRule);

                                                        result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                        result.FareSellKey = OnwardJourneys[i].Segments[0].Fares[f].FareSellKey;
                                                        result.FareType = GetFareTypeForProductClass(OnwardJourneys[i].Segments[0].Fares[f].ProductClass);
                                                        if (request.Type == SearchType.OneWay)
                                                        {
                                                            result.Flights = new FlightInfo[1][];
                                                        }
                                                        else
                                                        {
                                                            result.Flights = new FlightInfo[2][];
                                                        }
                                                        List<FlightInfo> onwardFlights = GetFlightInfo(OnwardJourneys[i].Segments[0], OnwardJourneys[i], OnwardJourneys[i].Segments[0].Fares[f].ProductClass, "ONWARD");

                                                        if (onwardFlights != null && onwardFlights.Count > 0)
                                                        {
                                                            List<FlightInfo> listOfOnwardFlights = new List<FlightInfo>();
                                                            listOfOnwardFlights.AddRange(onwardFlights);
                                                            result.Flights[0] = listOfOnwardFlights.ToArray();
                                                        }
                                                        CalculateBaseFareForReturnDirectFlights(ref result, OnwardJourneys[i], OnwardJourneys[i].Segments[0].Fares[f]);


                                                        result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                        result.FareType += "," + GetFareTypeForProductClass(fareFirstConSegReturn.ProductClass);
                                                        result.FareSellKey += "|" + fareFirstConSegReturn.FareSellKey + "^" + fareSecondConSegReturn.FareSellKey;


                                                        List<FlightInfo> firstSegmentConFlightsR = GetFlightInfo(firstSegmentConReturn, ReturnJourneys[j], fareFirstConSegReturn.ProductClass, "RETURN");
                                                        List<FlightInfo> secondSegmentConFlightsR = GetFlightInfo(secondSegmentConReturn, ReturnJourneys[j], fareSecondConSegReturn.ProductClass, "RETURN");
                                                        if (firstSegmentConFlightsR != null && secondSegmentConFlightsR != null && firstSegmentConFlightsR.Count > 0 && secondSegmentConFlightsR.Count > 0)
                                                        {
                                                            List<FlightInfo> listOfFlightsR = new List<FlightInfo>();
                                                            listOfFlightsR.AddRange(firstSegmentConFlightsR);
                                                            listOfFlightsR.AddRange(secondSegmentConFlightsR);
                                                            result.Flights[1] = listOfFlightsR.ToArray();
                                                        }
                                                        CalculateBaseFareForReturnConnectingFlights(ref result, ReturnJourneys[j]);

                                                        ResultList.Add(result);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }

                                //Case-4: Onward Connecting and Return Direct
                                else if (OnwardJourneys[i] != null && OnwardJourneys[i].Segments != null && OnwardJourneys[i].Segments.Length == 2 && ReturnJourneys[j] != null && ReturnJourneys[j].Segments != null && ReturnJourneys[j].Segments.Length == 1)
                                {
                                    if (ReturnJourneys[j].Segments[0].Fares != null && ReturnJourneys[j].Segments[0].Fares.Length > 0 && OnwardJourneys[i].Segments[0].Fares != null && OnwardJourneys[i].Segments[0].Fares.Length > 0 && OnwardJourneys[i].Segments[1].Fares != null && OnwardJourneys[i].Segments[1].Fares.Length > 0)
                                    {
                                        Segment firstSegmentConOnward = null;
                                        Segment secondSegmentConOnward = null;

                                        if (OnwardJourneys[i].Segments[0] != null && OnwardJourneys[i].Segments[1] != null)
                                        {
                                            firstSegmentConOnward = OnwardJourneys[i].Segments[0];
                                            secondSegmentConOnward = OnwardJourneys[i].Segments[1];
                                        }

                                        if (firstSegmentConOnward != null && secondSegmentConOnward != null && firstSegmentConOnward.Fares.Length == secondSegmentConOnward.Fares.Length)
                                        {
                                            for (int f = 0; f < firstSegmentConOnward.Fares.Length; f++)
                                            {
                                                for (int g = 0; g< ReturnJourneys[j].Segments[0].Fares.Length; g++)
                                                {
                                                    if (firstSegmentConOnward.Fares[f] != null && secondSegmentConOnward.Fares[f] != null && ReturnJourneys[j].Segments[0].Fares[g] != null)
                                                    {
                                                        Indigo.BookingMgr.Fare fareFirstConSegOnWard = firstSegmentConOnward.Fares[f];
                                                        Indigo.BookingMgr.Fare fareSecondConSegOnward = secondSegmentConOnward.Fares[f];

                                                        SearchResult result = new SearchResult();
                                                        result.IsLCC = true;
                                                        result.ResultBookingSource = BookingSource.Indigo;
                                                        result.Airline = "6E";
                                                        result.Currency = agentBaseCurrency;
                                                        result.EticketEligible = true;
                                                        result.FareBreakdown = new Fare[fareBreakDownCount];
                                                        result.FareRules = new List<FareRule>();
                                                        FareRule fareRule = new FareRule();
                                                        fareRule.Airline = result.Airline;
                                                        fareRule.Destination = request.Segments[0].Destination;
                                                        fareRule.FareBasisCode = OnwardJourneys[i].Segments[0].Fares[f].FareBasisCode;
                                                        fareRule.FareInfoRef = OnwardJourneys[i].Segments[0].Fares[f].RuleNumber;
                                                        fareRule.Origin = request.Segments[0].Origin;
                                                        fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                                        fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                                        result.FareRules.Add(fareRule);

                                                        result.NonRefundable = false;

                                                        result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                        result.FareSellKey = fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey;
                                                        result.FareType = GetFareTypeForProductClass(fareFirstConSegOnWard.ProductClass);
                                                        if (request.Type == SearchType.OneWay)
                                                        {
                                                            result.Flights = new FlightInfo[1][];

                                                        }
                                                        else
                                                        {
                                                            result.Flights = new FlightInfo[2][];

                                                        }
                                                        List<FlightInfo> firstSegmentConFlightsO = GetFlightInfo(firstSegmentConOnward, OnwardJourneys[i], fareFirstConSegOnWard.ProductClass, "ONWARD");
                                                        List<FlightInfo> secondSegmentConFlightsO = GetFlightInfo(secondSegmentConOnward, OnwardJourneys[i], fareSecondConSegOnward.ProductClass, "ONWARD");
                                                        if (firstSegmentConFlightsO != null && secondSegmentConFlightsO != null && firstSegmentConFlightsO.Count > 0 && secondSegmentConFlightsO.Count > 0)
                                                        {
                                                            List<FlightInfo> listOfFlightsO = new List<FlightInfo>();
                                                            listOfFlightsO.AddRange(firstSegmentConFlightsO);
                                                            listOfFlightsO.AddRange(secondSegmentConFlightsO);
                                                            result.Flights[0] = listOfFlightsO.ToArray();
                                                        }
                                                        CalculateBaseFareForReturnConnectingFlights(ref result, OnwardJourneys[i]);


                                                        result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                        result.FareType += "," + GetFareTypeForProductClass(ReturnJourneys[j].Segments[0].Fares[g].ProductClass);
                                                        result.FareSellKey += "|" + ReturnJourneys[j].Segments[0].Fares[g].FareSellKey;

                                                        List<FlightInfo> returnFlights = GetFlightInfo(ReturnJourneys[j].Segments[0], ReturnJourneys[j], ReturnJourneys[j].Segments[0].Fares[g].ProductClass, "RETURN");

                                                        if (returnFlights != null && returnFlights.Count > 0)
                                                        {
                                                            List<FlightInfo> listOfReturnFlights = new List<FlightInfo>();
                                                            listOfReturnFlights.AddRange(returnFlights);
                                                            result.Flights[1] = listOfReturnFlights.ToArray();
                                                        }
                                                        CalculateBaseFareForReturnDirectFlights(ref result, ReturnJourneys[j], ReturnJourneys[j].Segments[0].Fares[g]);
                                                        ResultList.Add(result);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }

                            }
                        }

                    }
                }
                #endregion

                #endregion

                CombinedSearchResults.AddRange(ResultList);

            }

            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to get Retail fare type results " + ex.ToString(), "");

            }

        }

        /// <summary>
        /// Gets the Connecting flight results for Return  trip.
        /// </summary>
        /// <param name="OnwardJourneys"></param>
        /// <param name="ReturnJourneys"></param>
        /// <param name="fareBreakDownCount"></param>
        /// <returns></returns>
        public List<SearchResult> GetReturnConnectingFlightsCombinationsResults(Journey OnwardJourneys, Journey ReturnJourneys, int fareBreakDownCount)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            { 
            
             if (OnwardJourneys != null && ReturnJourneys != null && OnwardJourneys.Segments != null && OnwardJourneys.Segments.Length >0 && ReturnJourneys.Segments != null && ReturnJourneys.Segments.Length >0 && OnwardJourneys.Segments.Length ==2 && ReturnJourneys.Segments.Length == 2)
            {
                Segment firstSegmentConOnward = null;//Onward Journey First Connecting Segment
                Segment secondSegmentConOnward = null;//Onward Journey Second Connecting Segment

                Segment firstSegmentConReturn = null;//Return Journey First Connecting Segment
                Segment secondSegmentConReturn = null;//Return Journey First Connecting Segment

                if (OnwardJourneys.Segments[0] != null && OnwardJourneys.Segments[1] != null)
                {
                    firstSegmentConOnward = OnwardJourneys.Segments[0];
                    secondSegmentConOnward = OnwardJourneys.Segments[1];
                }
                if (ReturnJourneys.Segments[0] != null && ReturnJourneys.Segments[1] != null)
                {
                    firstSegmentConReturn = ReturnJourneys.Segments[0];
                    secondSegmentConReturn = ReturnJourneys.Segments[1];
                }

                if(firstSegmentConOnward != null && secondSegmentConOnward != null && firstSegmentConReturn != null && secondSegmentConReturn != null)
                {
                    if (firstSegmentConOnward.Fares != null && firstSegmentConOnward.Fares.Length > 0 &&
                        secondSegmentConOnward.Fares != null && secondSegmentConOnward.Fares.Length >0 &&

                        firstSegmentConReturn.Fares != null && firstSegmentConReturn.Fares.Length >0 && 
                        secondSegmentConReturn.Fares != null && secondSegmentConReturn.Fares.Length > 0 && 

                        firstSegmentConOnward.Fares.Length == secondSegmentConOnward.Fares.Length && 
                        firstSegmentConReturn.Fares.Length == secondSegmentConReturn.Fares.Length)
                    {
                        for (int f = 0; f < firstSegmentConOnward.Fares.Length; f++)
                        {
                            for (int g = 0; g < firstSegmentConReturn.Fares.Length; g++)
                            {
                                if (firstSegmentConOnward.Fares[f] != null && secondSegmentConOnward.Fares[f] != null && firstSegmentConReturn.Fares[g] != null && secondSegmentConReturn.Fares[g] != null)              
                                {
                                    Indigo.BookingMgr.Fare fareFirstConSegOnWard = firstSegmentConOnward.Fares[f];
                                    Indigo.BookingMgr.Fare fareSecondConSegOnward = secondSegmentConOnward.Fares[f];

                                    Indigo.BookingMgr.Fare fareFirstConSegReturn = firstSegmentConReturn.Fares[g];
                                    Indigo.BookingMgr.Fare fareSecondConSegReturn = secondSegmentConReturn.Fares[g];

                                    SearchResult result = new SearchResult();
                                    result.IsLCC = true;
                                    result.ResultBookingSource = BookingSource.Indigo;
                                    result.Airline = "6E";
                                    result.Currency = agentBaseCurrency;
                                    result.EticketEligible = true;
                                    result.FareBreakdown = new Fare[fareBreakDownCount];
                                    result.FareRules = new List<FareRule>();
                                    result.NonRefundable = false;

                                    FareRule fareRule = new FareRule();
                                    fareRule.Airline = result.Airline;
                                    fareRule.Destination = request.Segments[0].Destination;
                                    fareRule.FareBasisCode = OnwardJourneys.Segments[0].Fares[f].FareBasisCode;
                                    fareRule.FareInfoRef = OnwardJourneys.Segments[0].Fares[f].RuleNumber;
                                    fareRule.Origin = request.Segments[0].Origin;
                                    fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                    fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                    result.FareRules.Add(fareRule);

                                    result.JourneySellKey = OnwardJourneys.JourneySellKey;
                                    result.FareSellKey = fareFirstConSegOnWard.FareSellKey+"^"+ fareSecondConSegOnward.FareSellKey;
                                    result.FareType = GetFareTypeForProductClass(fareFirstConSegOnWard.ProductClass);
                                    if (request.Type == SearchType.OneWay)
                                    {
                                        result.Flights = new FlightInfo[1][];
                                        
                                    }
                                    else
                                    {
                                        result.Flights = new FlightInfo[2][];
                                       
                                    }
                                    List<FlightInfo> firstSegmentConFlightsO = GetFlightInfo(firstSegmentConOnward, OnwardJourneys, fareFirstConSegOnWard.ProductClass, "ONWARD");
                                    List<FlightInfo> secondSegmentConFlightsO = GetFlightInfo(secondSegmentConOnward, OnwardJourneys, fareSecondConSegOnward.ProductClass, "ONWARD");
                                    if (firstSegmentConFlightsO != null && secondSegmentConFlightsO != null && firstSegmentConFlightsO.Count > 0 && secondSegmentConFlightsO.Count > 0)
                                    {
                                        List<FlightInfo> listOfFlightsO = new List<FlightInfo>();
                                        listOfFlightsO.AddRange(firstSegmentConFlightsO);
                                        listOfFlightsO.AddRange(secondSegmentConFlightsO);
                                        result.Flights[0] = listOfFlightsO.ToArray();
                                    }
                                    CalculateBaseFareForReturnConnectingFlights(ref result, OnwardJourneys);


                                    result.JourneySellKey += "|" + ReturnJourneys.JourneySellKey;
                                    result.FareType += "," + GetFareTypeForProductClass(fareFirstConSegReturn.ProductClass);
                                    result.FareSellKey += "|" + fareFirstConSegReturn.FareSellKey + "^" + fareSecondConSegReturn.FareSellKey;


                                    List<FlightInfo> firstSegmentConFlightsR = GetFlightInfo(firstSegmentConReturn, ReturnJourneys, fareFirstConSegReturn.ProductClass, "RETURN");
                                    List<FlightInfo> secondSegmentConFlightsR = GetFlightInfo(secondSegmentConReturn, ReturnJourneys, fareSecondConSegReturn.ProductClass, "RETURN");
                                    if (firstSegmentConFlightsR != null && secondSegmentConFlightsR != null && firstSegmentConFlightsR.Count > 0 && secondSegmentConFlightsR.Count > 0)
                                    {
                                        List<FlightInfo> listOfFlightsR = new List<FlightInfo>();
                                        listOfFlightsR.AddRange(firstSegmentConFlightsR);
                                        listOfFlightsR.AddRange(secondSegmentConFlightsR);
                                        result.Flights[1] = listOfFlightsR.ToArray();
                                    }
                                    CalculateBaseFareForReturnConnectingFlights(ref result, ReturnJourneys);
                                    ResultList.Add(result);

                                }
                            }
                        }
                    }

                }

            }
                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to get return Combination results." + ex.ToString(), "");
                //throw new Exception("(Indigo) failed to get Connecting Flights Combinations Results.Reason : " + ex.Message, ex);
            }
            return ResultList;
        }

        #endregion

        #region Price Calculation(Base Price and Tax) for one way and return trips

        #region Base Price Calculation

        /// <summary>
        /// For one way(Direct Flights or Direct Flights With Via Combination), this method Calculates the BaseFare for that itinerary i.e avaialble from the Search Response .
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="FarePaxtypes"></param>
        /// <param name="Onwardjourney"></param>
        /// <param name="OnwardFare"></param>
        public void CalculateBaseFareForOneWaydirectFlights(ref SearchResult resultObj, List<String> FarePaxtypes, Journey Onwardjourney, Indigo.BookingMgr.Fare OnwardFare)
        {
            try
            {
                if (Onwardjourney.Segments.Length > 0 && !string.IsNullOrEmpty(Onwardjourney.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode))
                {
                    rateOfExchange = exchangeRates[Onwardjourney.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];
                }
                resultObj.Price = new PriceAccounts();
                int tripCount = 1;
                if (request.Type == SearchType.Return)
                {
                    tripCount++;
                }

                for (int journeyOnWardReturn = 0; journeyOnWardReturn < tripCount; journeyOnWardReturn++)
                {
                    #region FareBreakdown of adult and child
                    foreach (String passengerType in FarePaxtypes)
                    {
                        switch (passengerType)
                        {
                            case "ADT": //Adult
                                if (resultObj.FareBreakdown[0] == null)
                                {
                                    resultObj.FareBreakdown[0] = new Fare();
                                }
                                resultObj.FareBreakdown[0].PassengerCount = request.AdultCount;
                                resultObj.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                break;
                            case "CHD": //Child
                                if (resultObj.FareBreakdown[1] == null)
                                {
                                    resultObj.FareBreakdown[1] = new Fare();
                                }
                                resultObj.FareBreakdown[1].PassengerCount = request.ChildCount;
                                resultObj.FareBreakdown[1].PassengerType = PassengerType.Child;
                                break;

                        }

                        //Infant Count-- if both children and infants exists in the request.
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            if (resultObj.FareBreakdown[2] == null)
                            {
                                resultObj.FareBreakdown[2] = new Fare();
                            }
                            resultObj.FareBreakdown[2].PassengerCount = request.InfantCount;
                            resultObj.FareBreakdown[2].PassengerType = PassengerType.Infant;
                        }

                        //Child Count -- If only infant count exists in the rquest.
                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                        {
                            if (resultObj.FareBreakdown[1] == null)
                            {
                                resultObj.FareBreakdown[1] = new Fare();
                            }
                            resultObj.FareBreakdown[1].PassengerCount = request.InfantCount;
                            resultObj.FareBreakdown[1].PassengerType = PassengerType.Infant;
                        }

                        int journeySegCount = 0;
                        int onwardSegCount = 0;
                        int returnSegCount = 0;

                        for (int p = 0; p < tripCount; p++)
                        {
                            if (p == 0)
                            {
                                for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                {
                                    if (resultObj.Flights[p][l].Status.ToUpper() != "VIA")
                                    {
                                        onwardSegCount++;
                                    }
                                }
                                if (onwardSegCount == 0)
                                {
                                    onwardSegCount = 1;
                                }
                            }
                            else
                            {
                                for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                {
                                    if (resultObj.Flights[p][l].Status.ToUpper() != "VIA")
                                    {
                                        returnSegCount++;
                                    }
                                }
                                if (returnSegCount == 0)
                                {
                                    returnSegCount = 1;
                                }

                            }
                        }
                        if (journeyOnWardReturn == 0)
                        {
                            journeySegCount = onwardSegCount;
                        }
                        else
                        {
                            journeySegCount = returnSegCount;
                        }
                        for (int k = 0; k < journeySegCount; k++)
                        {
                            //We need to combine fares of all the segments from the price itenary response

                            foreach (PaxFare pfare in OnwardFare.PaxFares)
                            {
                                if (passengerType == pfare.PaxType)
                                {
                                    foreach (BookingServiceCharge charge in OnwardFare.PaxFares[0].ServiceCharges)
                                    {
                                        switch (passengerType)
                                        {
                                            case "ADT":
                                                double baseFareAdult = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[0].PassengerCount);
                                                resultObj.FareBreakdown[0].BaseFare += baseFareAdult;
                                                resultObj.FareBreakdown[0].TotalFare += baseFareAdult;
                                                resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                resultObj.Price.PublishedFare += (decimal)baseFareAdult;
                                                resultObj.Price.SupplierPrice += charge.Amount;
                                                break;
                                            case "CHD":
                                                double baseFareChd = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                                                resultObj.FareBreakdown[1].BaseFare += baseFareChd;
                                                resultObj.FareBreakdown[1].TotalFare += baseFareChd;
                                                resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                resultObj.Price.PublishedFare += (decimal)baseFareChd;
                                                resultObj.Price.SupplierPrice += charge.Amount;
                                                break;
                                        }
                                    }
                                }
                            }
                        }

                    }
                    #endregion
                    //Infant fare break down
                    #region FareBreakdown of Infant
                    if (request.InfantCount > 0)
                    {
                        decimal infantPrice = 0;
                        infantPrice = GetInfantAvailibilityFares(request);
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[2].PassengerCount);
                            resultObj.FareBreakdown[2].TotalFare += baseFare;
                            resultObj.FareBreakdown[2].BaseFare += baseFare;
                            resultObj.FareBreakdown[2].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[2].PassengerCount);
                            resultObj.Price.PublishedFare += (decimal)baseFare;
                            resultObj.Price.SupplierPrice += infantPrice;
                        }
                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                        {
                            double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                            resultObj.FareBreakdown[1].BaseFare += baseFare;
                            resultObj.FareBreakdown[1].TotalFare += baseFare;
                            resultObj.FareBreakdown[1].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[1].PassengerCount);
                            resultObj.Price.PublishedFare += (decimal)baseFare;
                            resultObj.Price.SupplierPrice += infantPrice;
                        }
                    }
                    #endregion
                }


                resultObj.BaseFare = (double)Math.Round(resultObj.Price.PublishedFare, agentDecimalValue);
                resultObj.TotalFare = resultObj.BaseFare;


            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to  Calculate BaseFare For OneWay directFlights. Reason : " + ex.ToString(), "");
            }

        }

        /// <summary>
        /// For one way(Connecting Flights), this method Calculates the BaseFare for that itinerary i.e avaialble from the Search Response .
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="FarePaxtypes"></param>
        /// <param name="Onwardjourney"></param>
        /// <param name="OnwardFareConnecting1"></param>
        /// <param name="OnwardFareConnecting2"></param>
        public void CalculateBaseFareForOneWayConnectingFlights(ref SearchResult resultObj, List<String> FarePaxtypes, Journey Onwardjourney, Indigo.BookingMgr.Fare OnwardFareConnecting1, Indigo.BookingMgr.Fare OnwardFareConnecting2)
        {
            try
            {
                if (Onwardjourney.Segments.Length > 0 && !string.IsNullOrEmpty(Onwardjourney.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode))
                {
                    rateOfExchange = exchangeRates[Onwardjourney.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];
                }
                resultObj.Price = new PriceAccounts();
                int tripCount = 1;
                if (request.Type == SearchType.Return)
                {
                    tripCount++;
                }

                for (int journeyOnWardReturn = 0; journeyOnWardReturn < tripCount; journeyOnWardReturn++)
                {
                    #region FareBreakdown of adult and child
                    foreach (String passengerType in FarePaxtypes)
                    {
                        switch (passengerType)
                        {
                            case "ADT": //Adult
                                if (resultObj.FareBreakdown[0] == null)
                                {
                                    resultObj.FareBreakdown[0] = new Fare();
                                }
                                resultObj.FareBreakdown[0].PassengerCount = request.AdultCount;
                                resultObj.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                break;
                            case "CHD": //Child
                                if (resultObj.FareBreakdown[1] == null)
                                {
                                    resultObj.FareBreakdown[1] = new Fare();
                                }
                                resultObj.FareBreakdown[1].PassengerCount = request.ChildCount;
                                resultObj.FareBreakdown[1].PassengerType = PassengerType.Child;
                                break;

                        }

                        //Infant Count-- if both children and infants exists in the request.
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            if (resultObj.FareBreakdown[2] == null)
                            {
                                resultObj.FareBreakdown[2] = new Fare();
                            }
                            resultObj.FareBreakdown[2].PassengerCount = request.InfantCount;
                            resultObj.FareBreakdown[2].PassengerType = PassengerType.Infant;
                        }

                        //Child Count -- If only infant count exists in the rquest.
                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                        {
                            if (resultObj.FareBreakdown[1] == null)
                            {
                                resultObj.FareBreakdown[1] = new Fare();
                            }
                            resultObj.FareBreakdown[1].PassengerCount = request.InfantCount;
                            resultObj.FareBreakdown[1].PassengerType = PassengerType.Infant;
                        }

                        int journeySegCount = 0;
                        int onwardSegCount = 0;
                        int returnSegCount = 0;

                        for (int p = 0; p < tripCount; p++)
                        {
                            if (p == 0)
                            {
                                for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                {
                                    if (resultObj.Flights[p][l].Status.ToUpper() != "VIA")
                                    {
                                        onwardSegCount++;
                                    }
                                }
                                if (onwardSegCount == 0)
                                {
                                    onwardSegCount = 1;
                                }
                            }
                            else
                            {
                                for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                {
                                    if (resultObj.Flights[p][l].Status.ToUpper() != "VIA")
                                    {
                                        returnSegCount++;
                                    }
                                }
                                if (returnSegCount == 0)
                                {
                                    returnSegCount = 1;
                                }

                            }
                        }
                        if (journeyOnWardReturn == 0)
                        {
                            journeySegCount = onwardSegCount;
                        }
                        else
                        {
                            journeySegCount = returnSegCount;
                        }
                        for (int k = 0; k < journeySegCount; k++)
                        {
                            Indigo.BookingMgr.Fare OnwardFare = null;
                            if (k == 0)
                            {
                                OnwardFare = OnwardFareConnecting1;
                            }
                            else
                            {
                                OnwardFare = OnwardFareConnecting2;
                            }

                            //We need to combine fares of all the segments from the price itenary response

                            foreach (PaxFare pfare in OnwardFare.PaxFares)
                            {
                                if (passengerType == pfare.PaxType)
                                {
                                    foreach (BookingServiceCharge charge in OnwardFare.PaxFares[0].ServiceCharges)
                                    {
                                        switch (passengerType)
                                        {
                                            case "ADT":
                                                double baseFareAdult = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[0].PassengerCount);
                                                resultObj.FareBreakdown[0].BaseFare += baseFareAdult;
                                                resultObj.FareBreakdown[0].TotalFare += baseFareAdult;
                                                resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                resultObj.Price.PublishedFare += (decimal)baseFareAdult;
                                                resultObj.Price.SupplierPrice += charge.Amount;
                                                break;
                                            case "CHD":
                                                double baseFareChd = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                                                resultObj.FareBreakdown[1].BaseFare += baseFareChd;
                                                resultObj.FareBreakdown[1].TotalFare += baseFareChd;
                                                resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                resultObj.Price.PublishedFare += (decimal)baseFareChd;
                                                resultObj.Price.SupplierPrice += charge.Amount;
                                                break;
                                        }
                                    }
                                }
                            }
                        }

                    }
                    #endregion
                    //Infant fare break down
                    #region FareBreakdown of Infant
                    if (request.InfantCount > 0)
                    {
                        decimal infantPrice = 0;
                        infantPrice = GetInfantAvailibilityFares(request);
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[2].PassengerCount);
                            resultObj.FareBreakdown[2].TotalFare += baseFare;
                            resultObj.FareBreakdown[2].BaseFare += baseFare;
                            resultObj.FareBreakdown[2].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[2].PassengerCount);
                            resultObj.Price.PublishedFare += (decimal)baseFare;
                            resultObj.Price.SupplierPrice += infantPrice;
                        }
                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                        {
                            double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                            resultObj.FareBreakdown[1].BaseFare += baseFare;
                            resultObj.FareBreakdown[1].TotalFare += baseFare;
                            resultObj.FareBreakdown[1].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[1].PassengerCount);
                            resultObj.Price.PublishedFare += (decimal)baseFare;
                            resultObj.Price.SupplierPrice += infantPrice;
                        }
                    }
                    #endregion
                }


                resultObj.BaseFare = (double)Math.Round(resultObj.Price.PublishedFare, agentDecimalValue);
                resultObj.TotalFare = resultObj.BaseFare;


            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to  Calculate BaseFare For Connecting Flights. Reason : " + ex.ToString(), "");

            }
            //return resultObj;
        }


        /// <summary>
        /// For Round Trip(Direct Flights or Direct Flights With Via Combination), this method Calculates the BaseFare for that itinerary i.e avaialble from the Search Response .
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="journey"></param>
        /// <param name="fare"></param>
        private void CalculateBaseFareForReturnDirectFlights(ref SearchResult resultObj, Journey journey, Indigo.BookingMgr.Fare fare)
        {
            try
            {
                List<string> FarePaxtypes = new List<string>();
                if (request.AdultCount > 0)
                {
                    FarePaxtypes.Add("ADT");
                }
                if (request.ChildCount > 0)
                {
                    FarePaxtypes.Add("CHD");
                }
                if (request.InfantCount > 0)
                {
                    FarePaxtypes.Add("INFT");
                }
                if (journey.Segments.Length > 0 && !string.IsNullOrEmpty(journey.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode))
                {
                    rateOfExchange = exchangeRates[journey.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];
                }
                if (resultObj.Price == null)
                {
                    resultObj.Price = new PriceAccounts();
                }
                #region FareBreakdown of adult and child
                foreach (String passengerType in FarePaxtypes)
                {
                    switch (passengerType)
                    {
                        case "ADT": //Adult
                            if (resultObj.FareBreakdown[0] == null)
                            {
                                resultObj.FareBreakdown[0] = new Fare();
                            }
                            resultObj.FareBreakdown[0].PassengerCount = request.AdultCount;
                            resultObj.FareBreakdown[0].PassengerType = PassengerType.Adult;
                            break;
                        case "CHD": //Child
                            if (resultObj.FareBreakdown[1] == null)
                            {
                                resultObj.FareBreakdown[1] = new Fare();
                            }
                            resultObj.FareBreakdown[1].PassengerCount = request.ChildCount;
                            resultObj.FareBreakdown[1].PassengerType = PassengerType.Child;
                            break;

                    }

                    //Infant Count-- if both children and infants exists in the request.
                    if (request.ChildCount > 0 && request.InfantCount > 0)
                    {
                        if (resultObj.FareBreakdown[2] == null)
                        {
                            resultObj.FareBreakdown[2] = new Fare();
                        }
                        resultObj.FareBreakdown[2].PassengerCount = request.InfantCount;
                        resultObj.FareBreakdown[2].PassengerType = PassengerType.Infant;
                    }

                    //Child Count -- If only infant count exists in the rquest.
                    else if (request.ChildCount == 0 && request.InfantCount > 0)
                    {
                        if (resultObj.FareBreakdown[1] == null)
                        {
                            resultObj.FareBreakdown[1] = new Fare();
                        }
                        resultObj.FareBreakdown[1].PassengerCount = request.InfantCount;
                        resultObj.FareBreakdown[1].PassengerType = PassengerType.Infant;
                    }


                    //We need to combine fares of all the segments from the price itenary response

                    foreach (PaxFare pfare in fare.PaxFares)
                    {
                        if (passengerType == pfare.PaxType)
                        {
                            foreach (BookingServiceCharge charge in fare.PaxFares[0].ServiceCharges)
                            {
                                switch (passengerType)
                                {
                                    case "ADT":
                                        double baseFareAdult = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[0].PassengerCount);
                                        resultObj.FareBreakdown[0].BaseFare += baseFareAdult;
                                        resultObj.FareBreakdown[0].TotalFare += baseFareAdult;
                                        resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                        resultObj.Price.PublishedFare += (decimal)baseFareAdult;
                                        resultObj.Price.SupplierPrice += charge.Amount;
                                        break;
                                    case "CHD":
                                        double baseFareChd = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                                        resultObj.FareBreakdown[1].BaseFare += baseFareChd;
                                        resultObj.FareBreakdown[1].TotalFare += baseFareChd;
                                        resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                        resultObj.Price.PublishedFare += (decimal)baseFareChd;
                                        resultObj.Price.SupplierPrice += charge.Amount;
                                        break;
                                }
                            }
                        }
                    }


                }
                #endregion

                //Infant fare break down
                #region FareBreakdown of Infant
                if (request.InfantCount > 0)
                {
                    decimal infantPrice = 0;
                    infantPrice = GetInfantAvailibilityFares(request);
                    if (request.ChildCount > 0 && request.InfantCount > 0)
                    {
                        double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[2].PassengerCount);
                        resultObj.FareBreakdown[2].TotalFare += baseFare;
                        resultObj.FareBreakdown[2].BaseFare += baseFare;
                        resultObj.FareBreakdown[2].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[2].PassengerCount);
                        resultObj.Price.PublishedFare += (decimal)baseFare;
                        resultObj.Price.SupplierPrice += infantPrice;
                    }
                    else if (request.ChildCount <= 0 && request.InfantCount > 0)
                    {
                        double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.FareBreakdown[1].BaseFare += baseFare;
                        resultObj.FareBreakdown[1].TotalFare += baseFare;
                        resultObj.FareBreakdown[1].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.Price.PublishedFare += (decimal)baseFare;
                        resultObj.Price.SupplierPrice += infantPrice;
                    }
                }
                #endregion

                resultObj.BaseFare = (double)Math.Round(resultObj.Price.PublishedFare, agentDecimalValue);
                resultObj.TotalFare = resultObj.BaseFare;

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to  Calculate Base Fare For Return Direct Flights. Reason : " + ex.ToString(), "");

            }

        }


        /// <summary>
        /// For Return(Connecting Flights), this method Calculates the BaseFare for that itinerary i.e avaialble from the Search Response .
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="journey"></param>
        private void CalculateBaseFareForReturnConnectingFlights(ref SearchResult resultObj, Journey journey)
        {
            try
            {
                List<string> FarePaxtypes = new List<string>();
                if (request.AdultCount > 0)
                {
                    FarePaxtypes.Add("ADT");
                }
                if (request.ChildCount > 0)
                {
                    FarePaxtypes.Add("CHD");
                }
                if (request.InfantCount > 0)
                {
                    FarePaxtypes.Add("INFT");
                }

                if (journey.Segments.Length > 0 && !string.IsNullOrEmpty(journey.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode))
                {
                    rateOfExchange = exchangeRates[journey.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];

                }
                if (resultObj.Price == null)
                {
                    resultObj.Price = new PriceAccounts();
                }



                #region FareBreakdown of adult and child
                foreach (String passengerType in FarePaxtypes)
                {
                    switch (passengerType)
                    {
                        case "ADT": //Adult
                            if (resultObj.FareBreakdown[0] == null)
                            {
                                resultObj.FareBreakdown[0] = new Fare();
                            }
                            resultObj.FareBreakdown[0].PassengerCount = request.AdultCount;
                            resultObj.FareBreakdown[0].PassengerType = PassengerType.Adult;
                            break;
                        case "CHD": //Child
                            if (resultObj.FareBreakdown[1] == null)
                            {
                                resultObj.FareBreakdown[1] = new Fare();
                            }
                            resultObj.FareBreakdown[1].PassengerCount = request.ChildCount;
                            resultObj.FareBreakdown[1].PassengerType = PassengerType.Child;
                            break;

                    }

                    //Infant Count-- if both children and infants exists in the request.
                    if (request.ChildCount > 0 && request.InfantCount > 0)
                    {
                        if (resultObj.FareBreakdown[2] == null)
                        {
                            resultObj.FareBreakdown[2] = new Fare();
                        }
                        resultObj.FareBreakdown[2].PassengerCount = request.InfantCount;
                        resultObj.FareBreakdown[2].PassengerType = PassengerType.Infant;
                    }

                    //Child Count -- If only infant count exists in the rquest.
                    else if (request.ChildCount == 0 && request.InfantCount > 0)
                    {
                        if (resultObj.FareBreakdown[1] == null)
                        {
                            resultObj.FareBreakdown[1] = new Fare();
                        }
                        resultObj.FareBreakdown[1].PassengerCount = request.InfantCount;
                        resultObj.FareBreakdown[1].PassengerType = PassengerType.Infant;
                    }

                    for (int k = 0; k < journey.Segments.Length; k++)
                    {


                        //We need to combine fares of all the segments from the price itenary response

                        foreach (PaxFare pfare in journey.Segments[k].Fares[0].PaxFares)
                        {
                            if (passengerType == pfare.PaxType)
                            {
                                foreach (BookingServiceCharge charge in journey.Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                {
                                    switch (passengerType)
                                    {
                                        case "ADT":
                                            double baseFareAdult = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[0].PassengerCount);
                                            resultObj.FareBreakdown[0].BaseFare += baseFareAdult;
                                            resultObj.FareBreakdown[0].TotalFare += baseFareAdult;
                                            resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                            resultObj.Price.PublishedFare += (decimal)baseFareAdult;
                                            resultObj.Price.SupplierPrice += charge.Amount;
                                            break;
                                        case "CHD":
                                            double baseFareChd = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                                            resultObj.FareBreakdown[1].BaseFare += baseFareChd;
                                            resultObj.FareBreakdown[1].TotalFare += baseFareChd;
                                            resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                            resultObj.Price.PublishedFare += (decimal)baseFareChd;
                                            resultObj.Price.SupplierPrice += charge.Amount;
                                            break;
                                    }
                                }
                            }
                        }
                    }

                }
                #endregion
                //Infant fare break down
                #region FareBreakdown of Infant
                if (request.InfantCount > 0)
                {
                    decimal infantPrice = 0;
                    infantPrice = GetInfantAvailibilityFares(request);
                    if (request.ChildCount > 0 && request.InfantCount > 0)
                    {
                        double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[2].PassengerCount);
                        resultObj.FareBreakdown[2].TotalFare += baseFare;
                        resultObj.FareBreakdown[2].BaseFare += baseFare;
                        resultObj.FareBreakdown[2].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[2].PassengerCount);
                        resultObj.Price.PublishedFare += (decimal)baseFare;
                        resultObj.Price.SupplierPrice += infantPrice;
                    }
                    else if (request.ChildCount <= 0 && request.InfantCount > 0)
                    {
                        double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.FareBreakdown[1].BaseFare += baseFare;
                        resultObj.FareBreakdown[1].TotalFare += baseFare;
                        resultObj.FareBreakdown[1].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.Price.PublishedFare += (decimal)baseFare;
                        resultObj.Price.SupplierPrice += infantPrice;
                    }
                }
                #endregion

                resultObj.BaseFare = (double)Math.Round(resultObj.Price.PublishedFare, agentDecimalValue);
                resultObj.TotalFare = resultObj.BaseFare;




            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Calculate BaseFare For Return Connecting Flights. Reason : " + ex.ToString(), "");

            }
            //return resultObj;
        }


        /// <summary>
        /// This method is used to get Infant Fares from the Source Baggage Info Table
        /// </summary>
        /// <param name="onwardFare"></param>
        /// <param name="returnFare"></param>
        /// <returns></returns>
        protected decimal GetInfantAvailibilityFares(SearchRequest request)
        {
            decimal InfantPrice = 0;
            try
            {
                DataTable dtSourceBaggage = SourceBaggage.GetSourceBaggages(BookingSource.Indigo,currencyCode);

                if (request.Type == SearchType.OneWay)
                {
                    Airport origin = new Airport(request.Segments[0].Origin);
                    Airport destination = new Airport(request.Segments[0].Destination);

                    //If Booking is Domestic
                    if (origin.CountryCode == "IN" && destination.CountryCode == "IN")
                    {
                        DataRow[] rows = dtSourceBaggage.Select("BaggageCode='INFT' AND IsDomestic=1");

                        if (rows != null && rows.Length > 0)
                        {
                            rateOfExchange = exchangeRates[rows[0]["BaggageCurrency"].ToString()];
                            InfantPrice = Convert.ToDecimal(rows[0]["BaggagePrice"]);
                        }
                    }
                    else//If International Booking
                    {
                        DataRow[] rows = dtSourceBaggage.Select("BaggageCode='INFT' AND IsDomestic=0");

                        if (rows != null && rows.Length > 0)
                        {
                            rateOfExchange = exchangeRates[rows[0]["BaggageCurrency"].ToString()];
                            InfantPrice = Convert.ToDecimal(rows[0]["BaggagePrice"]);
                        }
                    }
                }
                else//Return or Multi-Way
                {
                    Airport origin = new Airport(request.Segments[0].Origin);
                    Airport destination = new Airport(request.Segments[0].Destination);

                    //If Domestic Booking
                    if (origin.CountryCode == "IN" && destination.CountryCode == "IN")
                    {
                        DataRow[] rows = dtSourceBaggage.Select("BaggageCode='INFT' AND IsDomestic=1");

                        if (rows != null && rows.Length > 0)
                        {
                            rateOfExchange = exchangeRates[rows[0]["BaggageCurrency"].ToString()];
                            InfantPrice = (Convert.ToDecimal(rows[0]["BaggagePrice"]));
                        }
                    }
                    else//If International Booking
                    {
                        DataRow[] rows = dtSourceBaggage.Select("BaggageCode='INFT' AND IsDomestic=0");

                        if (rows != null && rows.Length > 0)
                        {
                            rateOfExchange = exchangeRates[rows[0]["BaggageCurrency"].ToString()];
                            InfantPrice = (Convert.ToDecimal(rows[0]["BaggagePrice"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Indigo)Failed to get Infant Fares. " + ex.ToString(), "");
            }

            return InfantPrice;
        }
        #endregion

        #region Tax Price Calculation

        /// <summary>
        /// This method calculates the tax price for Oneway and for Return flights either from the tax available in the table or from the Itenary response.
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="itineraryResponse"></param>
        /// <param name="taxComponent"></param>
        private void CalculateTax(ref SearchResult resultObj, decimal savedTax)
        {
            try
            {
                #region Tax Calculation For OneWay Flights
                if (request.Type == SearchType.OneWay)
                {
                    //Onward Journey Direct Flights
                    if (resultObj.Flights[0].Length == 1)
                    {
                        if (savedTax > 0)
                        {
                            //Tax calculation based on the result available in the table
                            AssignTax(ref resultObj, savedTax);
                        }
                        else  //Tax Calculation from PriceItineraryResponse
                        {

                            string onwardFare = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            PriceItineraryResponse itineraryResponse = GetItineraryPriceForOneWayTrip(request, resultObj.JourneySellKey, resultObj.FareSellKey, resultObj, onwardFare);
                            if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                            {
                                rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                                RetriveTaxFromPriceItinerary(ref resultObj, itineraryResponse);
                            }
                        }
                    }
                    //Onward Journey Connecting or Via Flights
                    if (resultObj.Flights[0].Length > 1)
                    {

                        if (savedTax > 0)
                        {
                            //Tax calculation based on the result available in the table
                            AssignTax(ref resultObj, savedTax);
                        }

                        else //tax calculation from price itinerary
                        {
                            string onwardFare_connecting = string.Empty;
                            for (int m = 0; m < resultObj.Flights[0].Length; m++)
                            {
                                if (resultObj.Flights[0][m].Status.ToUpper() != "VIA")
                                {
                                    if (!string.IsNullOrEmpty(onwardFare_connecting))
                                    {
                                        onwardFare_connecting += "_" + resultObj.Flights[0][1].FlightNumber + "_" + resultObj.FareType + "_" + Convert.ToString(resultObj.Flights[0][1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                    }
                                    else
                                    {
                                        onwardFare_connecting = resultObj.Flights[0][m].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][m].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                    }
                                }
                                else
                                {
                                    onwardFare_connecting = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                }
                            }

                            PriceItineraryResponse itineraryResponse = GetItineraryPriceForOneWayTrip(request, resultObj.JourneySellKey, resultObj.FareSellKey, resultObj, onwardFare_connecting);
                            if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                            {
                                rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                                RetriveTaxFromPriceItinerary(ref resultObj, itineraryResponse);
                            }

                        }
                    }
                }
                #endregion


                #region Tax Calculation For Return flights

                if (request.Type == SearchType.Return)
                {


                    if (resultObj.Flights[0].Length == 1 && resultObj.Flights[1].Length == 1)//Both onward and return journeys are direct flights.
                    {
                        if (savedTax > 0)
                        {
                            //Tax calculation based on the resultObj available in the table
                            AssignTax(ref resultObj, savedTax);
                        }
                        else  //Tax Calculation from PriceItineraryResponse
                        {

                            string onwardFare = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            string returnFare = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_").Replace(" ", "_") + Convert.ToString(resultObj.Flights[1][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, onwardFare + returnFare);
                            if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                            {
                                rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                                RetriveTaxFromPriceItinerary(ref resultObj, itineraryResponse);
                            }
                        }

                    }
                    if (resultObj.Flights[0].Length > 1 && resultObj.Flights[1].Length > 1)//Both onward and return journeys may be connecting or via flights.
                    {

                        if (savedTax > 0)
                        {
                            //Tax calculation based on the resultObj available in the table
                            AssignTax(ref resultObj, savedTax);
                        }
                        else  //Tax Calculation from PriceItineraryResponse
                        {

                            string connectingFareType = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            connectingFareType += "_" + Convert.ToString(resultObj.Flights[0][resultObj.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            string connectingFareType1 = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            connectingFareType1 += "_" + Convert.ToString(resultObj.Flights[1][resultObj.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, connectingFareType + connectingFareType1);
                            if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                            {
                                rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                                RetriveTaxFromPriceItinerary(ref resultObj, itineraryResponse);
                            }
                        }

                    }
                    //Onward may be connecting or via and return direct flights
                    if (resultObj.Flights[0].Length > 1 && resultObj.Flights[1].Length == 1)
                    {

                        if (savedTax > 0)
                        {
                            //Tax calculation based on the resultObj available in the table
                            AssignTax(ref resultObj, savedTax);
                        }
                        else //Tax calculation from itinerary response
                        {

                            string connectingFareType = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            connectingFareType += "_" + Convert.ToString(resultObj.Flights[0][resultObj.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            string returnFare = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_").Replace(" ", "_") + Convert.ToString(resultObj.Flights[1][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, connectingFareType + returnFare);
                            if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                            {
                                rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                                RetriveTaxFromPriceItinerary(ref resultObj, itineraryResponse);
                            }
                        }

                    }
                    //Onward direct flight and return may be via or connecting flights
                    if (resultObj.Flights[0].Length == 1 && resultObj.Flights[1].Length > 1)
                    {

                        if (savedTax > 0)
                        {
                            //Tax calculation based on the resultObj available in the table
                            AssignTax(ref resultObj, savedTax);
                        }
                        else //Tax calculation from itinerary response
                        {
                            string onwardFare = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            string connectingFareType1 = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            connectingFareType1 += "_" + Convert.ToString(resultObj.Flights[1][resultObj.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, onwardFare + connectingFareType1);
                            if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                            {
                                rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                                RetriveTaxFromPriceItinerary(ref resultObj, itineraryResponse);
                            }
                        }

                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Calculate TaxComponent For Flights. Reason : " + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Gets the ItineraryPrice for Indigo flights.
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="request"></param>
        public void GetItineraryPriceForIndigoFlights(ref SearchResult resultObj, SearchRequest request)
        {
            try
            {
                //List which holds the Pax types  : "ADT","CHD" ,"INFT";
                List<string> FarePaxtypes = new List<string>();
                if (request.AdultCount > 0)
                {
                    FarePaxtypes.Add("ADT");
                }
                if (request.ChildCount > 0)
                {
                    FarePaxtypes.Add("CHD");
                }
                if (request.InfantCount > 0)
                {
                    FarePaxtypes.Add("INFT");
                }
                if (request.Type == SearchType.OneWay)
                {
                    if (resultObj.Flights[0].Length == 1)//Onward Journey Direct Flights
                    {
                        string onwardFare = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        PriceItineraryResponse itineraryResponse = GetItineraryPriceForOneWayTrip(request, resultObj.JourneySellKey, resultObj.FareSellKey, resultObj, onwardFare);
                        if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                        {
                            rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                            resultObj = CalculateFareBreakdown(resultObj, FarePaxtypes, rateOfExchange, itineraryResponse, request);
                        }
                    }
                    if (resultObj.Flights[0].Length > 1) //Onward Journey Connecting or Via Flights
                    {
                        string onwardFare_connecting = string.Empty;
                        for (int m = 0; m < resultObj.Flights[0].Length; m++)
                        {
                            if (resultObj.Flights[0][m].Status.ToUpper() != "VIA")
                            {
                                if (!string.IsNullOrEmpty(onwardFare_connecting))
                                {
                                    onwardFare_connecting += "_" + resultObj.Flights[0][1].FlightNumber + "_" + resultObj.FareType + "_" + Convert.ToString(resultObj.Flights[0][1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                }
                                else
                                {
                                    onwardFare_connecting = resultObj.Flights[0][m].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][m].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                }
                            }
                            else
                            {
                                onwardFare_connecting = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            }
                        }

                        PriceItineraryResponse itineraryResponse = GetItineraryPriceForOneWayTrip(request, resultObj.JourneySellKey, resultObj.FareSellKey, resultObj, onwardFare_connecting);
                        if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                        {
                            rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                            resultObj = CalculateFareBreakdown(resultObj, FarePaxtypes, rateOfExchange, itineraryResponse, request);
                        }
                    }
                }
                if (request.Type == SearchType.Return)
                {
                    if (resultObj.Flights[0].Length == 1 && resultObj.Flights[1].Length == 1)//Both onward and return journeys are direct flights.
                    {
                        string onwardFare = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        string returnFare = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_").Replace(" ", "_") + Convert.ToString(resultObj.Flights[1][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, onwardFare + returnFare);
                        if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                        {
                            rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                            resultObj = CalculateFareBreakdown(resultObj, FarePaxtypes, rateOfExchange, itineraryResponse, request);
                        }
                    }
                    if (resultObj.Flights[0].Length > 1 && resultObj.Flights[1].Length > 1)//Both onward and return journeys may be connecting or via flights.
                    {

                        string connectingFareType = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        connectingFareType += "_" + Convert.ToString(resultObj.Flights[0][resultObj.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        string connectingFareType1 = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        connectingFareType1 += "_" + Convert.ToString(resultObj.Flights[1][resultObj.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, connectingFareType + connectingFareType1);
                        if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                        {
                            rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                            resultObj = CalculateFareBreakdown(resultObj, FarePaxtypes, rateOfExchange, itineraryResponse, request);
                        }

                    }
                    //Onward may be connecting or via and return direct flights
                    if (resultObj.Flights[0].Length > 1 && resultObj.Flights[1].Length == 1)
                    {
                        string connectingFareType = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        connectingFareType += "_" + Convert.ToString(resultObj.Flights[0][resultObj.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        string returnFare = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_").Replace(" ", "_") + Convert.ToString(resultObj.Flights[1][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');

                        PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, connectingFareType + returnFare);
                        if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                        {
                            rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                            resultObj = CalculateFareBreakdown(resultObj, FarePaxtypes, rateOfExchange, itineraryResponse, request);
                        }

                    }
                    //Onward direct flight and return may be via or connecting flights
                    if (resultObj.Flights[0].Length == 1 && resultObj.Flights[1].Length > 1)
                    {
                        string onwardFare = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        string connectingFareType1 = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        connectingFareType1 += "_" + Convert.ToString(resultObj.Flights[1][resultObj.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, onwardFare + connectingFareType1);
                        if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                        {
                            rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                            resultObj = CalculateFareBreakdown(resultObj, FarePaxtypes, rateOfExchange, itineraryResponse, request);
                        }

                    }



                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                throw new BookingEngineException("(Indigo) No fare available");

            }
        }

        /// <summary>
        /// Gets the ItineraryPrice for one way journeys
        /// </summary>
        /// <param name="request"></param>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="journey"></param>
        /// <param name="onwardFare"></param>
        /// <returns></returns>
        protected PriceItineraryResponse GetItineraryPriceForOneWayTrip(SearchRequest request, string journeySellKey, string fareSellKey, SearchResult resultObj, string onwardFare)
        {


            LogonResponse loginResponse = Login();
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature) && loginResponse.Signature.Length > 0)
            {

                
                IBookingManager bookingAPI = this.bookingAPI;

                int paxCount = 1;
                if (request.ChildCount > 0)
                {
                    paxCount++;
                }


                PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
                priceItinRequest.Signature = loginResponse.Signature;
                priceItinRequest.ContractVersion = contractVersion;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                priceItinRequest.EnableExceptionStackTrace = false;
                priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS = new PointOfSale();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = "AG";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.DomainCode = agentDomain;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.OrganizationCode = agentId;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(paxCount);
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[paxCount];

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[1];
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;


                if (request.AdultCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxType = "ADT";
                }
                //for (int i = 0; i < request.AdultCount; i++)
                //{
                //    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                //    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "ADT";
                //}


                if (request.ChildCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxType = "CHD";
                }

                //for (int i = 0; i < request.ChildCount; i++)
                //{
                //    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[request.AdultCount + i] = new PaxPriceType();
                //    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[request.AdultCount + i].PaxType = "CHD";
                //}


                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                    string filePath = xmlLogPath + "IndigoItineraryPriceRequest_" + appUserId.ToString() + "_" + onwardFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, priceItinRequest);
                    sw.Close();
                }
                catch { }


                try
                {
                    piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);

                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Booking));
                        string filePath = xmlLogPath + "IndigoItineraryPriceResponse_" + appUserId.ToString() + "_" + onwardFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, piResponse.Booking);
                        sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
                }
                catch (Exception ex)
                {
                    if (ex.Message.ToLower().Contains("session"))//Session Exception
                    {
                        Basket.FlightBookingSession.Remove(appUserId.ToString());//Clear basket
                    }
                    Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                    throw new BookingEngineException("(Indigo) No fare available");
                }
            }
            else
            {
                throw new Exception("(Indigo)Failed to get the LogonResponse");
            }
            return piResponse;
        }

        /// <summary>
        /// Gets the ItineraryPrice for round trip journeys.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="fareSellKeyRet"></param>
        /// <param name="journeySellKeyRet"></param>
        /// <param name="resultObj"></param>
        /// <param name="onwardFare"></param>
        /// <returns></returns>
        protected PriceItineraryResponse GetItineraryPriceForRoundTrip(SearchRequest request, string journeySellKey, string fareSellKey, string fareSellKeyRet, string journeySellKeyRet, SearchResult resultObj, string onwardFare)
        {
            LogonResponse loginResponse = Login();
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature) && loginResponse.Signature.Length > 0)
            {


                IBookingManager bookingAPI = this.bookingAPI;

                int paxCount = 1;
                if (request.ChildCount > 0)
                {
                    paxCount++;
                }


                PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
                priceItinRequest.Signature = loginResponse.Signature;
                priceItinRequest.ContractVersion = contractVersion;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                priceItinRequest.EnableExceptionStackTrace = false;
                priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;




                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(paxCount);
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[paxCount];

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS = new PointOfSale();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = "AG";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.DomainCode = agentDomain;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.OrganizationCode = agentId;


                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[2];
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].FareSellKey = fareSellKeyRet;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].JourneySellKey = journeySellKeyRet;
                if (request.AdultCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxType = "ADT";
                }
                if (request.ChildCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxType = "CHD";
                }

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                    string filePath = xmlLogPath + "IndigoPriceRequestRoundTrip_" + appUserId.ToString() + "_" + onwardFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";

                    //string filePath = xmlLogPath + "SGGetItineraryPriceRequest_" + journeykey + "_#_" + fareSellKey.Replace("~", "_") + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, priceItinRequest);
                    sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }


                try
                {
                    piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);

                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Booking));
                        string filePath = xmlLogPath + "IndigoPriceResponseRoundTrip_" + appUserId.ToString() + "_" + onwardFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        //string filePath = xmlLogPath + "SGGetItineraryPriceResponse_" + journeykey + "_#_" + fareSellKey.Replace("~", "_") + "__" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, piResponse.Booking);
                        sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
                }
                catch (Exception ex)
                {
                    if (ex.Message.ToLower().Contains("session"))//Session Exception
                    {
                        Basket.FlightBookingSession.Remove(appUserId.ToString());//Clear basket
                    }
                    Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                    throw new BookingEngineException("(Indigo) No fare available");
                }
            }
            else
            {
                throw new Exception("(Indigo)Failed to get the LogonResponse");
            }


            return piResponse;
        }

        /// <summary>
        /// Retrieve the Tax from PriceItinerary both for one way and return flights and calculate the tax for the itinerary.
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="itineraryResponse"></param>
        /// <param name="taxComponent"></param>
        private void RetriveTaxFromPriceItinerary(ref SearchResult resultObj, PriceItineraryResponse itineraryResponse)
        {
            try
            {
                //For infant there will be no tax so we will calculate tax for only adult and child pax types.
                List<string> FarePaxtypes = new List<string>();
                if (request.AdultCount > 0)
                {
                    FarePaxtypes.Add("ADT");
                }
                if (request.ChildCount > 0)
                {
                    FarePaxtypes.Add("CHD");
                }

                int tripCount = 1;
                if (request.Type == SearchType.Return)
                {
                    tripCount++;
                }
                decimal newTaxPrice = 0; //variable which holds the new tax price.



                for (int journeyOnWardReturn = 0; journeyOnWardReturn < tripCount; journeyOnWardReturn++)
                {
                    #region FareBreakdown of adult and child
                    foreach (String passengerType in FarePaxtypes)
                    {
                        int journeySegCount = 0;
                        int onwardSegCount = 0;
                        int returnSegCount = 0;

                        for (int p = 0; p < tripCount; p++)
                        {
                            if (p == 0)  // For Onward Trips
                            {
                                for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                {
                                    if (resultObj.Flights[p][l].Status.ToUpper() != "VIA") // If the onward flights are not via 
                                    {
                                        onwardSegCount++;
                                    }
                                }
                                if (onwardSegCount == 0) // If the onward flights are direct or direct with via combination
                                {
                                    onwardSegCount = 1;
                                }
                            }
                            else
                            {
                                for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                {
                                    if (resultObj.Flights[p][l].Status.ToUpper() != "VIA")// If the return flights are not via 
                                    {
                                        returnSegCount++;
                                    }
                                }
                                if (returnSegCount == 0)// If the return flights are direct or direct with via combination
                                {
                                    returnSegCount = 1;
                                }

                            }
                        }

                        if (journeyOnWardReturn == 0)//Flag which determines whether to iterate over the onward segments or return segments.
                        {
                            journeySegCount = onwardSegCount; //Iterate over onward segments
                        }
                        else
                        {
                            journeySegCount = returnSegCount;//Iterate over return segments
                        }


                        //As the tax for both adult and child are same so we will sum up all the charges for Adult Pax type.
                        //Then calculate the tax component from the saved tax.
                        for (int k = 0; k < journeySegCount; k++)
                        {
                            if (itineraryResponse != null)
                            {
                                #region CalculateBaseFare and tax component of adult and child from itinerary response
                                foreach (PaxFare pfare in itineraryResponse.Booking.Journeys[journeyOnWardReturn].Segments[k].Fares[0].PaxFares)
                                {
                                    if (passengerType == pfare.PaxType)
                                    {
                                        foreach (BookingServiceCharge charge in itineraryResponse.Booking.Journeys[journeyOnWardReturn].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                        {
                                            #region Tax Component Calculation For Adult and Child
                                            switch (passengerType)
                                            {
                                                case "ADT":
                                                    switch (charge.ChargeType)
                                                    {
                                                        case Indigo.BookingMgr.ChargeType.Tax:
                                                        case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                        case Indigo.BookingMgr.ChargeType.TravelFee:
                                                        case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                        case Indigo.BookingMgr.ChargeType.Loyalty:

                                                            newTaxPrice += charge.Amount; //Summing up all the new tax prices.

                                                            break;
                                                    }
                                                    break;

                                            }
                                            #endregion
                                        }
                                    }
                                }
                                #endregion

                            }

                        }
                    }
                    #endregion
                }


                //save the tax price.
                try
                {
                    if (newTaxPrice > 0)
                    {
                        AirlineJourneyPriceDetails priceDetails = new AirlineJourneyPriceDetails();
                        priceDetails.AirlineCode = resultObj.Airline;
                        priceDetails.Currency = itineraryResponse.Booking.CurrencyCode;
                        priceDetails.DecimalPoint = agentDecimalValue;
                        priceDetails.Destination = request.Segments[0].Destination;
                        priceDetails.Origin = request.Segments[0].Origin;
                        priceDetails.Tax = newTaxPrice;
                        if (request.Type == SearchType.OneWay)
                        {
                            priceDetails.Type = SearchType.OneWay;
                        }
                        else
                        {
                            priceDetails.Type = SearchType.Return;
                        }
                        priceDetails.Save();
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to save Tax FromPriceItinerary. Reason : " + ex.ToString(), "");
                }

                //Calculation of tax from the tax component.
                if (newTaxPrice > 0)
                {
                    AssignTax(ref resultObj, newTaxPrice);
                }

                //Baggage Calculation 
                if (itineraryResponse != null)
                {
                    resultObj.BaggageIncludedInFare = itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();

                    for (int k = 1; k < resultObj.Flights[0].Length; k++)
                    {
                        if (resultObj.Flights[0][k].Status.ToUpper() != "VIA")//If the onward flights are not via
                        {
                            resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                        }
                    }
                    if (request.Type == SearchType.Return)
                    {

                        resultObj.BaggageIncludedInFare += "|" + itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                        for (int k = 1; k < resultObj.Flights[1].Length; k++)
                        {
                            if (resultObj.Flights[1][k].Status.ToUpper() != "VIA")//If the return flights are not via
                            {
                                resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                            }
                        }
                    }
                }



                resultObj.Price.SupplierCurrency = currencyCode;
                resultObj.BaseFare = (double)Math.Round(resultObj.Price.PublishedFare, agentDecimalValue);
                resultObj.Tax = (double)Math.Round(resultObj.Price.Tax, agentDecimalValue);
                resultObj.TotalFare = resultObj.BaseFare + resultObj.Tax;



            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Calculate Tax FromPriceItinerary. Reason : " + ex.ToString(), "");

            }
        }

        /// <summary>
        /// Retrieve the Tax from the table BKE_Airline_Journey_Price_Details and calculate the tax for the itinerary.
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="taxComponent"></param>
        private void AssignTax(ref SearchResult resultObj, decimal taxComponent)
        {
            try
            {
                if (taxComponent > 0)
                {
                    //For infant there will be no tax so we will calculate tax for only adult and child pax types.
                    List<string> FarePaxtypes = new List<string>();
                    if (request.AdultCount > 0)
                    {
                        FarePaxtypes.Add("ADT");
                    }
                    if (request.ChildCount > 0)
                    {
                        FarePaxtypes.Add("CHD");
                    }


                    int tripCount = 1;
                    if (request.Type == SearchType.Return)
                    {
                        tripCount++;
                    }
                    if (request.Type == SearchType.Return)
                    {
                        //the tax we save in the table is of both (onward + return).
                        // so will break up the sum into two equal halves.
                        taxComponent = taxComponent / 2;
                    }


                    for (int m = 0; m < tripCount; m++)
                    {
                        foreach (String passengerType in FarePaxtypes)
                        {
                            switch (passengerType)
                            {
                                case "ADT":
                                    double taxAdult = (double)((taxComponent * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                                    resultObj.FareBreakdown[0].TotalFare += taxAdult;
                                    resultObj.FareBreakdown[0].SupplierFare += (double)(taxComponent * resultObj.FareBreakdown[0].PassengerCount);
                                    resultObj.Price.Tax += (decimal)taxAdult;
                                    resultObj.Price.SupplierPrice += taxComponent;
                                    break;
                                case "CHD":
                                    double taxChild = (double)((taxComponent * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                                    resultObj.FareBreakdown[1].TotalFare += taxChild;
                                    resultObj.FareBreakdown[1].SupplierFare += (double)(taxComponent * resultObj.FareBreakdown[1].PassengerCount);
                                    resultObj.Price.Tax += (decimal)taxChild;
                                    resultObj.Price.SupplierPrice += taxComponent;
                                    break;
                            }
                        }

                    }
                    resultObj.Price.SupplierCurrency = currencyCode;
                    resultObj.BaseFare = (double)resultObj.Price.PublishedFare;
                    resultObj.Tax = (double)resultObj.Price.Tax;
                    resultObj.TotalFare = resultObj.BaseFare + resultObj.Tax;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Calculate Tax. Reason : " + ex.ToString(), "");

            }

        }

        /// <summary>
        /// Calculates the fare break down 
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="FarePaxtypes"></param>
        /// <param name="rateOfExchange"></param>
        /// <param name="itineraryResponse"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchResult CalculateFareBreakdown(SearchResult resultObj, List<String> FarePaxtypes, decimal rateOfExchange, PriceItineraryResponse itineraryResponse, SearchRequest request)
        {
            try
            {
                List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
                List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

                paxTypeTaxBreakUp.Clear();
                adtPaxTaxBreakUp.Clear();
                chdPaxTaxBreakUp.Clear();
                inftPaxTaxBreakUp.Clear();

                resultObj.Price = new PriceAccounts();
                int tripOneWayReturn = 1;
                if (request.Type == SearchType.Return)
                {
                    tripOneWayReturn++;
                }
                decimal newTaxPrice = 0;
                for (int jourOnWardReturn = 0; jourOnWardReturn < tripOneWayReturn; jourOnWardReturn++)
                {
                    #region FareBreakdown of adult and child
                    foreach (String passengerType in FarePaxtypes)
                    {
                        switch (passengerType)
                        {
                            case "ADT": //Adult
                                if (resultObj.FareBreakdown[0] == null)
                                {
                                    resultObj.FareBreakdown[0] = new Fare();
                                }
                                resultObj.FareBreakdown[0].PassengerCount = request.AdultCount;
                                resultObj.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                break;
                            case "CHD": //Child
                                if (resultObj.FareBreakdown[1] == null)
                                {
                                    resultObj.FareBreakdown[1] = new Fare();
                                }
                                resultObj.FareBreakdown[1].PassengerCount = request.ChildCount;
                                resultObj.FareBreakdown[1].PassengerType = PassengerType.Child;
                                break;

                        }

                        //Infant Count-- if both children and infants exists in the request.
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            if (resultObj.FareBreakdown[2] == null)
                            {
                                resultObj.FareBreakdown[2] = new Fare();
                            }
                            resultObj.FareBreakdown[2].PassengerCount = request.InfantCount;
                            resultObj.FareBreakdown[2].PassengerType = PassengerType.Infant;
                        }

                        //Child Count -- If only infant count exists in the rquest.
                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                        {
                            if (resultObj.FareBreakdown[1] == null)
                            {
                                resultObj.FareBreakdown[1] = new Fare();
                            }
                            resultObj.FareBreakdown[1].PassengerCount = request.InfantCount;
                            resultObj.FareBreakdown[1].PassengerType = PassengerType.Infant;
                        }
                        if (itineraryResponse != null)
                        {
                            int journeySegCount = 0;
                            int onwardSegCount = 0;
                            int returnSegCount = 0;

                            for (int p = 0; p < tripOneWayReturn; p++)
                            {
                                if (p == 0) //For onward
                                {
                                    for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                    {
                                        if (resultObj.Flights[p][l].Status.ToUpper() != "VIA") //If the onward flights are not via
                                        {
                                            onwardSegCount++;
                                        }
                                    }
                                    if (onwardSegCount == 0)//If the onward flights are direct or direct with via combination
                                    {
                                        onwardSegCount = 1;
                                    }
                                }
                                else
                                {
                                    for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                    {
                                        if (resultObj.Flights[p][l].Status.ToUpper() != "VIA")//If the return flights are not via
                                        {
                                            returnSegCount++;
                                        }
                                    }
                                    if (returnSegCount == 0)//If the return flights are direct or direct with via combination
                                    {
                                        returnSegCount = 1;
                                    }

                                }
                            }
                            if (jourOnWardReturn == 0) //Flag which determines wheether to iterate over the onward segments or return segments
                            {
                                journeySegCount = onwardSegCount; //Onward segments iteration
                            }
                            else
                            {
                                journeySegCount = returnSegCount;//return segments iteration
                            }


                            for (int k = 0; k < itineraryResponse.Booking.Journeys[jourOnWardReturn].Segments.Length; k++)
                            {



                                //We need to combine fares of all the segments from the price itenary response

                                foreach (PaxFare pfare in itineraryResponse.Booking.Journeys[jourOnWardReturn].Segments[k].Fares[0].PaxFares)
                                {
                                    if (passengerType == pfare.PaxType)
                                    {
                                        foreach (BookingServiceCharge charge in itineraryResponse.Booking.Journeys[jourOnWardReturn].Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                        {
                                            switch (passengerType)
                                            {
                                                case "ADT":

                                                    switch (charge.ChargeType)
                                                    {
                                                        case Indigo.BookingMgr.ChargeType.FarePrice:
                                                            double baseFareAdult = (double)((charge.Amount * rateOfExchange)* resultObj.FareBreakdown[0].PassengerCount);
                                                            resultObj.FareBreakdown[0].BaseFare += baseFareAdult;
                                                            resultObj.FareBreakdown[0].TotalFare += baseFareAdult;
                                                            resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                            resultObj.Price.PublishedFare += (decimal)baseFareAdult;
                                                            resultObj.Price.SupplierPrice += charge.Amount;
                                                            break;

                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                        case Indigo.BookingMgr.ChargeType.Calculated:
                                                        case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                        case Indigo.BookingMgr.ChargeType.Discount:
                                                        case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                        case Indigo.BookingMgr.ChargeType.FarePoints:
                                                        case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                        case Indigo.BookingMgr.ChargeType.IncludedAddOnServiceFee:
                                                        case Indigo.BookingMgr.ChargeType.IncludedTravelFee:
                                                        case Indigo.BookingMgr.ChargeType.Loyalty:
                                                        case Indigo.BookingMgr.ChargeType.Note:
                                                        case Indigo.BookingMgr.ChargeType.PromotionDiscount:
                                                        case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                        case Indigo.BookingMgr.ChargeType.Tax:
                                                        case Indigo.BookingMgr.ChargeType.TravelFee:
                                                        case Indigo.BookingMgr.ChargeType.Unmapped:

                                                            newTaxPrice += charge.Amount;
                                                            adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                            break;
                                                    }

                                                    break;

                                                case "CHD":

                                                    switch (charge.ChargeType)
                                                    {
                                                        case Indigo.BookingMgr.ChargeType.FarePrice:
                                                            double baseFareChd = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                                                            resultObj.FareBreakdown[1].BaseFare += baseFareChd;
                                                            resultObj.FareBreakdown[1].TotalFare += baseFareChd;
                                                            resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                            resultObj.Price.PublishedFare += (decimal)baseFareChd;
                                                            resultObj.Price.SupplierPrice += charge.Amount;
                                                            break;

                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                        case Indigo.BookingMgr.ChargeType.Calculated:
                                                        case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                        case Indigo.BookingMgr.ChargeType.Discount:
                                                        case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                        case Indigo.BookingMgr.ChargeType.FarePoints:
                                                        case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                        case Indigo.BookingMgr.ChargeType.IncludedAddOnServiceFee:
                                                        case Indigo.BookingMgr.ChargeType.IncludedTravelFee:
                                                        case Indigo.BookingMgr.ChargeType.Loyalty:
                                                        case Indigo.BookingMgr.ChargeType.Note:
                                                        case Indigo.BookingMgr.ChargeType.PromotionDiscount:
                                                        case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                        case Indigo.BookingMgr.ChargeType.Tax:
                                                        case Indigo.BookingMgr.ChargeType.TravelFee:
                                                        case Indigo.BookingMgr.ChargeType.Unmapped:
                                                            chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                            break;
                                                    }


                                                    break;

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    //Infant fare break down
                    if (request.InfantCount > 0)
                    {
                        decimal infantPrice = 0;
                        infantPrice = GetInfantAvailibilityFares(request);
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            double baseFare = (double)((infantPrice * rateOfExchange) * resultObj.FareBreakdown[2].PassengerCount);
                            resultObj.FareBreakdown[2].TotalFare += baseFare;
                            resultObj.FareBreakdown[2].BaseFare += baseFare;
                            resultObj.FareBreakdown[2].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[2].PassengerCount);
                            resultObj.Price.PublishedFare += (decimal)baseFare;
                            resultObj.Price.SupplierPrice += infantPrice;
                        }
                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                        {
                            double baseFare = (double)((infantPrice * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                            resultObj.FareBreakdown[1].BaseFare += baseFare;
                            resultObj.FareBreakdown[1].TotalFare += baseFare;
                            resultObj.FareBreakdown[1].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[1].PassengerCount);
                            resultObj.Price.PublishedFare += (decimal)baseFare;
                            resultObj.Price.SupplierPrice += infantPrice;
                        }
                    }

                }

                //Here we will update the latest tax price.
                try
                {
                    if (newTaxPrice > 0)
                    {
                        AirlineJourneyPriceDetails priceDetails = new AirlineJourneyPriceDetails();
                        priceDetails.AirlineCode = resultObj.Airline;
                        priceDetails.Currency = itineraryResponse.Booking.CurrencyCode;
                        priceDetails.DecimalPoint = agentDecimalValue;
                        priceDetails.Destination = request.Segments[0].Destination;
                        priceDetails.Origin = request.Segments[0].Origin;
                        priceDetails.Tax = newTaxPrice;

                        if (request.Type == SearchType.OneWay)
                        {
                            priceDetails.Type = SearchType.OneWay;
                        }
                        else
                        {
                            priceDetails.Type = SearchType.Return;
                        }
                        priceDetails.Save();
                    }
                }



                catch (Exception ex)
                {
                    Audit.Add(Core.EventType.Search, Core.Severity.High, appUserId, "(Indigo)Failed to save tax details : Error" + ex.ToString() + DateTime.Now, "");
                }



                //Calculation of tax from the tax component.

                if (newTaxPrice > 0)
                {
                    this.request = request; //SearchRequest assignment for tax calculation
                    AssignTax(ref resultObj, newTaxPrice);
                }






                //Baggage Calculation
                resultObj.BaggageIncludedInFare = itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();

                for (int k = 1; k < resultObj.Flights[0].Length; k++)
                {
                    if (resultObj.Flights[0][k].Status.ToUpper() != "VIA")
                    {
                        resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                    }
                }
                if (request.Type == SearchType.Return)
                {

                    resultObj.BaggageIncludedInFare += "|" + itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                    for (int k = 1; k < resultObj.Flights[1].Length; k++)
                    {
                        if (resultObj.Flights[1][k].Status.ToUpper() != "VIA")
                        {
                            resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                        }
                    }
                }

                if (adtPaxTaxBreakUp.Count > 0)
                {
                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                }
                if (chdPaxTaxBreakUp.Count > 0)
                {
                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                }
                if (inftPaxTaxBreakUp.Count > 0)
                {
                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                }
                if (paxTypeTaxBreakUp.Count > 0)
                {
                    resultObj.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                }
                resultObj.Price.SupplierCurrency = currencyCode;
                resultObj.BaseFare = (double)resultObj.Price.PublishedFare;
                resultObj.Tax = (double)resultObj.Price.Tax;
                resultObj.TotalFare = (double)Math.Round((resultObj.BaseFare + resultObj.Tax), agentDecimalValue);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Calculate fare break down. Reason : " + ex.ToString(), "");
            }
            return resultObj;
        }

        #endregion

        #endregion

        #region Special service requests like baggage and infant calculation

        /// <summary>
        /// Returns baggage options available for the result.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public DataTable GetBaggage(SearchResult result)
        {
            DataTable dtSourceBaggage = SourceBaggage.GetSourceBaggages(BookingSource.Indigo,currencyCode);

            rateOfExchange = exchangeRates[dtSourceBaggage.Rows[0]["BaggageCurrency"].ToString()];

            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    DataRow[] baggages = new DataRow[0];
                    if (result.Flights[0][0].Origin.CountryCode == "IN" && result.Flights[0][result.Flights[0].Length - 1].Destination.CountryCode == "IN")
                    {
                        baggages = dtSourceBaggage.Select("IsDomestic=1 AND BaggageCode <> 'INFT'");
                    }
                    else
                    {
                        baggages = dtSourceBaggage.Select("IsDomestic=0 AND BaggageCode <> 'INFT'");
                    }

                    if (!result.Flights[i][j].SegmentFareType.Contains("HandBaggage"))
                    {
                        foreach (DataRow row in baggages)
                        {
                            DataRow dr = dtBaggage.NewRow();
                            dr["Code"] = row["BaggageCode"];
                            dr["Price"] = Math.Round(Convert.ToDecimal(row["BaggagePrice"]) * rateOfExchange, agentDecimalValue);
                            dr["Group"] = result.Flights[i][j].Group;
                            dr["Currency"] = agentBaseCurrency;
                            switch (row["BaggageCode"].ToString())
                            {
                                case "XBPA":
                                    dr["Description"] = "Prepaid Excess Baggage – 5 Kg";
                                    break;
                                case "XBPB":
                                    dr["Description"] = "Prepaid Excess Baggage – 10 Kg";
                                    break;
                                case "XBPC":
                                    dr["Description"] = "Prepaid Excess Baggage – 15 Kg";
                                    break;
                                case "XBPD":
                                    dr["Description"] = "Prepaid Excess Baggage – 30 Kg";
                                    break;

                            }
                            dr["QtyAvailable"] = 5000;
                            dtBaggage.Rows.Add(dr);
                        }
                    }
                }
            }

            dtBaggage.DefaultView.Sort = "Code ASC,Group ASC";

            return dtBaggage;

        }


        /// <summary>
        /// Used to book the Itinerary if any Infant passengers are included in the Itinerary.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>        
        public decimal SellSSRForInfant(FlightItinerary itinerary, string signature)
        {
            decimal bookingAmount = 0;
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                SellRequest request = new SellRequest();
                request.ContractVersion = contractVersion;
                request.Signature = signature;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                request.EnableExceptionStackTrace = false;

                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.SSR;
                requestData.SellSSR = new SellSSR();
                requestData.SellSSR.SSRRequest = new SSRRequest();
                requestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                requestData.SellSSR.SSRRequest.CurrencyCode = currencyCode;
                requestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;
                requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[itinerary.Segments.Length];

                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = "6E";
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = itinerary.Segments[i].FlightNumber.Replace("6E", "").Trim();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = itinerary.Segments[i].DepartureTime;
                    //requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = new PaxSSR[itinerary.Passenger.Length];
                    //if (i == 0)
                    {
                        int counter = 0;
                        List<PaxSSR> PaxSSRs = new List<PaxSSR>();
                        for (int j = 0; j < itinerary.Passenger.Length; j++)
                        {
                            if (itinerary.Passenger[j].Type == PassengerType.Infant)
                            {
                                PaxSSR paxSSR = new PaxSSR();
                                paxSSR.ActionStatusCode = "NN";
                                paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                paxSSR.FeeCode = "";
                                paxSSR.Note = "";
                                paxSSR.PassengerNumber = (Int16)counter;
                                paxSSR.SSRCode = "INFT";
                                paxSSR.SSRDetail = "";
                                paxSSR.SSRNumber = (Int16)0;
                                paxSSR.SSRValue = (Int16)0;
                                counter++;
                                PaxSSRs.Add(paxSSR);
                            }
                        }
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = PaxSSRs.ToArray();
                    }
                }

                request.SellRequestData = requestData;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + "IndigoBookInfantRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch { }

                SellResponse sellResponse_v_4_5 = bookingAPI.Sell(request);
                responseData = sellResponse_v_4_5.BookingUpdateResponseData;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                    string filePath = xmlLogPath + "IndigoBookInfantResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, responseData);
                    sw.Close();
                }
                catch { }
                bookingAmount += responseData.Success.PNRAmount.TotalCost;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to book Baggage. Reason : " + ex.ToString(), "");
            }



            return bookingAmount;
        }

        /// <summary>
        /// This method returns the Count of Infant as well as Baggage Count for Various Flight Segments Which determines whether to send a SSR request or not
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private string GetSSRCount(FlightItinerary itinerary)
        {
            string baggage_infant_count = string.Empty;
            try
            {

                int onwardSegCount = 0, returnSegCount = 0;
                bool IsOnwardVIABooking = false, IsReturnVIABooking = false;
                int oncount = 0;

                foreach (FlightInfo seg in itinerary.Segments)
                {
                    if (seg.Group == 0)
                    {
                        oncount++;

                        if (seg.Status == string.Empty)
                        {
                            onwardSegCount++;
                        }
                        else //OneWay VIA Booking
                        {

                            IsOnwardVIABooking = true;
                            onwardSegCount = 1;
                        }
                    }

                    if (seg.Group == 1)
                    {

                        if (seg.Status == string.Empty)
                        {
                            returnSegCount++;
                        }
                        else //Return VIA Booking
                        {

                            IsReturnVIABooking = true;
                            returnSegCount = 1;
                        }
                    }
                }

                int infantCount = 0, baggageCount = 0;
                if (onwardSegCount == 1 || (onwardSegCount == 1 && returnSegCount == 1))
                {
                    #region Onward or Return(Via or Direct Flights)

                    //The below if block is for the following conditions :
                    //1.If both onward and return are VIA bookings.
                    //2.Onward Via Return Direct Flight
                    //3.Onward Direct Return Via Flight.

                    if ((IsOnwardVIABooking && IsReturnVIABooking) || (IsOnwardVIABooking && returnSegCount == 1) || (onwardSegCount == 1 && IsReturnVIABooking))
                    {
                        for (int i = 0; i < 2; i++)
                        {
                            infantCount = 0;
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                {
                                    if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                    {
                                        baggageCount++;
                                    }
                                }
                                if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                {

                                    infantCount++;
                                }
                            }
                            //Meal SSR
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0)
                                {
                                   baggageCount++;
                                }
                            }
                        }
                    }

                    //If only Onward Via Booking
                    else if (IsOnwardVIABooking)
                    {
                        for (int i = 0; i < 1; i++)
                        {
                            infantCount = 0;
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                {
                                    if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                    {
                                        baggageCount++;
                                    }
                                }
                                if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                {

                                    infantCount++;
                                }
                            }
                            //Meal SSR
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0)
                                {
                                    baggageCount++;
                                }
                            }


                        }
                    }

                   //If both onward and return are direct flights.
                    else
                    {
                        for (int i = 0; i < itinerary.Segments.Length; i++)
                        {
                            infantCount = 0;

                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                {
                                    if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                    {

                                        baggageCount++;
                                    }
                                }
                                if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                {

                                    infantCount++;
                                }
                            }

                            //Meal SSR
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0)
                                {
                                    baggageCount++;
                                }
                            }


                        }
                    }
                    #endregion
                }
                else
                {

                    if (returnSegCount > 0)
                    {
                        #region Return More Than One Segment(Connecting Flights)
                        for (int i = 0; i < itinerary.Segments.Length; i++)
                        {
                            infantCount = 0;
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (i == 0)
                                {
                                    if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                    {
                                        if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                        {

                                            baggageCount++;
                                        }
                                    }
                                }
                                if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                {
                                    infantCount++;
                                }
                            }
                            //Meal SSR
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0)
                                {
                                    baggageCount++;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Only OnwardSegments (Connecting Flights)
                        for (int i = 0; i < itinerary.Segments.Length; i++)
                        {
                            infantCount = 0;
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (i == 0)
                                {
                                    if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                    {
                                        if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                        {

                                            baggageCount++;
                                        }
                                    }
                                }

                                if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                {

                                    infantCount++;
                                }
                            }

                            //Meal SSR
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0)
                                {
                                    baggageCount++;
                                }
                            }
                        }
                        #endregion
                    }
                }
                if (baggageCount > 0 || infantCount > 0)
                {
                    baggage_infant_count = Convert.ToString(baggageCount) + "~" + Convert.ToString(infantCount);
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to get Baggage Count. Reason : " + ex.ToString(), "");
            }
            return baggage_infant_count;
        }


        #endregion

        #region Fare rules

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fareBasisCode"></param>
        /// <param name="classOfService"></param>
        /// <param name="ruleNumber"></param>
        /// <param name="origin"></param>
        /// <param name="dest"></param>
        /// <returns></returns>
        public List<FareRule> GetFareRuleInfo(string fareBasisCode, string classOfService, string ruleNumber, string origin, string dest)
        {
            List<FareRule> fareRules = new List<FareRule>();
            try
            {
                //The below are the steps for retrieving FareRule Information
                //S-1:LogOn
                //S-2:GetAvailability
                //S-3:GetFareRuleInfo
                //S-4:LogOut.

                logonResponse = Login();
                FareRuleInfo fareRuleInfo = null;

                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {

                    //Added by Lokesh on 11-June-2018
                    //New skies Version 4.5
                    //Need to send EnableExceptionStackTrace = false(by default) in the request.
                   
                    GetFareRuleInfoRequest request = new GetFareRuleInfoRequest();
                    request.ContractVersion = contractVersion;
                    request.Signature = logonResponse.Signature;
                    request.EnableExceptionStackTrace = false;

                    FareRuleRequestData fareRuleReqData = new FareRuleRequestData();
                    fareRuleReqData.FareBasisCode = fareBasisCode;
                    fareRuleReqData.ClassOfService = classOfService;
                    fareRuleReqData.CarrierCode = "6E";
                    fareRuleReqData.RuleNumber = ruleNumber;
                    fareRuleReqData.CultureCode = "en-GB"; //use fixed value en-GB
                    request.fareRuleReqData = fareRuleReqData;

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(GetFareRuleInfoRequest));
                        string filePath = xmlLogPath + "IndigoFareRuleInfoSearchRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, request);
                        sw.Close();
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Save Fare Rule Info Search Request. Reason : " + ex.ToString(), "");
                    }
                    GetFareRuleInfoResponse response_v_4_5 = contentAPI.GetFareRuleInfo(request);
                    fareRuleInfo = response_v_4_5.FareRuleInfo;


                    try
                    {
                        string filePath = xmlLogPath + "IndigoFareRuleInfoSearchResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        XmlDocument doc = CustomXmlSerializer.Serialize(fareRuleInfo, 1, "GetFareRuleInfoResponse");
                        doc.Save(filePath);
                    }
                    catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }
                    

                    if (fareRuleInfo != null)
                    {
                        byte[] encodedBytes = fareRuleInfo.Data;
                        string decodedText = Encoding.UTF8.GetString(encodedBytes);

                        FareRule DNfareRules = new FareRule();
                        DNfareRules.Origin = origin;
                        DNfareRules.Destination = dest;
                        DNfareRules.Airline = "6E";
                        DNfareRules.FareRuleDetail = decodedText;
                        DNfareRules.FareBasisCode = fareBasisCode;
                        fareRules.Add(DNfareRules);
                    }
                    //Logout(logonResponse.Signature);
                }
                else
                {
                    throw new Exception("(Indigo)Failed to get the LogonResponse");
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to retrieve fare rule information. Reason : " + ex.ToString(), "");
            }
            return fareRules;
        }


        #endregion

        #region Booking Methods

        /// <summary>
        /// Books an itinerary
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private BookingUpdateResponseData Book(FlightItinerary itinerary, string signature)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {  
                SellRequestData requestData = new SellRequestData();
                SellRequest request = new SellRequest();

                request.ContractVersion = contractVersion;
                request.Signature = signature;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                request.EnableExceptionStackTrace = false;
                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = currencyCode;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;

                //Define SourcePOS data
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS = new PointOfSale();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.AgentCode = "AG";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.DomainCode = agentDomain;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.OrganizationCode = agentId;

                string onwardFareSellKey = string.Empty;
                string onwardJourneySellKey = string.Empty;
                string returnFareSellKey = string.Empty;
                string returnJourneySellKey = string.Empty;

                List<string> sellKeysListOnward = new List<string>();
                List<string> sellKeysListReturn = new List<string>();
                for (int p = 0; p < itinerary.Segments.Length; p++)
                {
                    if (itinerary.Segments[p].Group == 0)//Onward
                    {
                        sellKeysListOnward.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                    if (itinerary.Segments[p].Group == 1)//Return
                    {
                        sellKeysListReturn.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                }
                IEnumerable<string> distinctSellKeysOnward = sellKeysListOnward.Distinct();
                IEnumerable<string> distinctSellKeysReturn = sellKeysListReturn.Distinct();

                if(distinctSellKeysOnward.Count() >0)
                {
                    foreach (string key in distinctSellKeysOnward)
                    {
                        if (onwardJourneySellKey.Length > 0)
                        {
                            onwardJourneySellKey += "^" + key;
                        }
                        else
                        {
                            onwardJourneySellKey = key;
                        }
                    }
                }

                if (distinctSellKeysReturn.Count() > 0)
                {
                    foreach (string key in distinctSellKeysReturn)
                    {
                        if (returnJourneySellKey.Length > 0)
                        {
                            returnJourneySellKey += "^" + key;
                        }
                        else
                        {
                            returnJourneySellKey = key;
                        }
                    }
                }


                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[1];
                if (itinerary.TicketAdvisory.Split('|').Length == 2)
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[2];
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = onwardJourneySellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = itinerary.TicketAdvisory.Split('|')[0];
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";

                if (itinerary.TicketAdvisory.Split('|').Length == 2)//Round trip
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].JourneySellKey = returnJourneySellKey;
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].FareSellKey = itinerary.TicketAdvisory.Split('|')[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].StandbyPriorityCode = "";
                }
                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    if (itinerary.Passenger[i].Type != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        switch (itinerary.Passenger[i].Type)
                        {
                            case PassengerType.Adult:
                                priceType.PaxType = "ADT";
                                break;
                            case PassengerType.Child:
                                priceType.PaxType = "CHD";
                                break;
                           
                        }
                        paxPriceTypes.Add(priceType);
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;

                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                request.SellRequestData = requestData;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + "IndigoBookingRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch { }
                
                SellResponse sellResponse_v_4_5 = bookingAPI.Sell(request);
                responseData = sellResponse_v_4_5.BookingUpdateResponseData;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                    string filePath = xmlLogPath + "IndigoBookingResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, responseData);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to Book. Reason : " + ex.ToString(), "");
                throw ex;
            }

            return responseData;
        }

        /// <summary>
        /// Used to book the Baggage, chosen by the user for the Itinerary.
        /// Also Infants need to be booked from this method itself.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private BookingUpdateResponseData BookBaggage(FlightItinerary itinerary, string signature)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                logonResponse = Login();
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {

                    int onwardSegCount = 0, returnSegCount = 0;
                    string onFlightNumber = string.Empty, retFlightNumber = string.Empty, onwardSellKey = string.Empty;
                    DateTime onDepartureTime = new DateTime();
                    DateTime retDepartureTime = new DateTime();
                    bool IsOnwardVIABooking = false, IsReturnVIABooking = false;
                    int oncount = 0;
                    foreach (FlightInfo seg in itinerary.Segments)
                    {
                        if (seg.Group == 0)
                        {
                            oncount++;
                            if (onwardSegCount == 0)
                            {
                                onwardSellKey = seg.UapiSegmentRefKey;
                                onFlightNumber = seg.Origin.AirportCode + "-" + seg.Destination.AirportCode + "-" + seg.FlightNumber;
                                onDepartureTime = seg.DepartureTime;
                            }
                            if (seg.Status == string.Empty)
                            {
                                onwardSegCount++;
                            }
                            else //OneWay VIA Booking
                            {
                                onFlightNumber = itinerary.Segments[0].Origin.AirportCode + "-" + seg.Destination.AirportCode + "-" + seg.FlightNumber;
                                IsOnwardVIABooking = true;
                                onwardSegCount = 1;
                            }
                        }

                        if (seg.Group == 1)
                        {
                            if (returnSegCount == 0)
                            {
                                retFlightNumber = seg.Origin.AirportCode + "-" + seg.Destination.AirportCode + "-" + seg.FlightNumber;
                                retDepartureTime = seg.DepartureTime;
                            }
                            if (seg.Status == string.Empty)
                            {
                                returnSegCount++;
                            }
                            else //Return VIA Booking
                            {
                                retFlightNumber = itinerary.Segments[oncount].Origin.AirportCode + "-" + seg.Destination.AirportCode + "-" + seg.FlightNumber;
                                IsReturnVIABooking = true;
                                returnSegCount = 1;
                            }
                        }
                    }

                    if (IsOnwardVIABooking && onwardSegCount == 1 && returnSegCount == 0)
                    {
                        //This condition determines that the onward flights are connecting with via combination.
                        //Different segments with different legs.
                        //For ex : COK - CCJ journey {COK-BOM,BOM-AMD,AMD-MAA,MAA-CCJ}
                        //Segment 1 - 2 Legs : {COK-BOM,BOM-AMD}
                        //Segment 2 - 2 Legs : {AMD-MAA,MAA-CCJ}
                        //Here in the journey 2 segments with each segment having 2 legs. So total 4.

                        if (itinerary.Segments.Length >= 4)
                        {
                            onFlightNumber = itinerary.Segments[0].Origin.AirportCode + "-" + itinerary.Segments[1].Destination.AirportCode + "-" + itinerary.Segments[0].FlightNumber;
                        }
                        //This condition determines that the onward flights are direct with via combination.
                        //Same Segment but legs may be different.
                        //For ex : IXA - HYD journey
                        //Single segment - 3 Legs :{Agartala-Guwahati,Guwahati-Bangalore,Bangalore-Hyderabad}
                        else
                        {
                            onFlightNumber = itinerary.Segments[0].Origin.AirportCode + "-" + itinerary.Segments[itinerary.Segments.Length - 1].Destination.AirportCode + "-" + itinerary.Segments[0].FlightNumber;
                        }
                    }


                    SellRequest request = new SellRequest();
                    request.ContractVersion = contractVersion;
                    request.Signature = logonResponse.Signature;
                    //Added by Lokesh on 11-June-2018
                    //New skies Version 4.5
                    //Need to send EnableExceptionStackTrace = false(by default) in the request.
                    request.EnableExceptionStackTrace = false;
                    SellRequestData requestData = new SellRequestData();
                    requestData.SellBy = SellBy.SSR;
                    requestData.SellSSR = new SellSSR();
                    requestData.SellSSR.SSRRequest = new SSRRequest();
                    requestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                    requestData.SellSSR.SSRRequest.CurrencyCode = currencyCode;
                    requestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;

                    //Direct flight OneWay or Return
                    int infantCount = 0, baggageCount = 0;
                    if (onwardSegCount == 1 || (onwardSegCount == 1 && returnSegCount == 1))
                    {

                        //Onward Direct & Return Direct
                        //Onward Via & Return Direct
                        //Onward Direct & Return VIA
                        //Onward VIA & Return VIA

                        #region Onward or Return with Single Segment having Single Leg or  Multiple Legs.
                        if ((IsOnwardVIABooking && IsReturnVIABooking) || (IsOnwardVIABooking && returnSegCount == 1) || (onwardSegCount == 1 && IsReturnVIABooking))
                        {
                            string[] onward = onFlightNumber.Split('-');
                            string[] depart = retFlightNumber.Split('-');

                            requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[2];
                            for (int i = 0; i < 2; i++)
                            {
                                infantCount = 0;
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = (i == 0 ? onward[1] : depart[1]);
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = (i == 0 ? onward[0] : depart[0]);
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = "6E";
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = (i == 0 ? onward[2] : depart[2]);
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = (i == 0 ? onDepartureTime : retDepartureTime);
                                List<PaxSSR> PaxSSRs = new List<PaxSSR>();
                                for (int j = 0; j < itinerary.Passenger.Length; j++)
                                {
                                    if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                    {
                                        if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                        {
                                            PaxSSR paxSSR = new PaxSSR();
                                            paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                            paxSSR.ActionStatusCode = "NN";
                                            paxSSR.ArrivalStation = (i == 0 ? onward[1] : depart[1]);
                                            paxSSR.DepartureStation = (i == 0 ? onward[0] : depart[0]);
                                            paxSSR.PassengerNumber = (Int16)j;
                                            if (itinerary.Passenger[j].BaggageType.Split(',').Length > 1)//Return
                                            {
                                                paxSSR.SSRCode = itinerary.Passenger[j].BaggageType.Split(',')[i];
                                            }
                                            else
                                            {
                                                paxSSR.SSRCode = itinerary.Passenger[j].BaggageType;//Oneway
                                            }

                                            paxSSR.SSRNumber = (Int16)0;
                                            paxSSR.SSRValue = (Int16)0;
                                            if (paxSSR.SSRCode.Length > 0)
                                            {
                                                PaxSSRs.Add(paxSSR);
                                            }
                                            baggageCount++;
                                        }
                                    }
                                    if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                    {
                                        PaxSSR paxSSR = new PaxSSR();
                                        paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                        paxSSR.ActionStatusCode = "NN";

                                        paxSSR.ArrivalStation = (i == 0 ? onward[1] : depart[1]);
                                        paxSSR.DepartureStation = (i == 0 ? onward[0] : depart[0]);

                                        paxSSR.PassengerNumber = (Int16)infantCount;

                                        paxSSR.SSRCode = "INFT";//Oneway

                                        paxSSR.SSRNumber = (Int16)0;
                                        paxSSR.SSRValue = (Int16)0;
                                        if (!string.IsNullOrEmpty(paxSSR.SSRCode) && paxSSR.SSRCode.Length > 0)
                                        {
                                            PaxSSRs.Add(paxSSR);
                                        }
                                        infantCount++;
                                    }
                                }

                                //Meal Selection SSR
                                for (int j = 0; j < itinerary.Passenger.Length; j++)
                                {
                                    if (itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0 && itinerary.Passenger[j].Type != PassengerType.Infant)
                                    {
                                        //if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType.Length > 0)
                                        {
                                            PaxSSR paxSSR = new PaxSSR();
                                            paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                            paxSSR.ActionStatusCode = "NN";
                                            paxSSR.ArrivalStation = (i == 0 ? onward[1] : depart[1]);
                                            paxSSR.DepartureStation = (i == 0 ? onward[0] : depart[0]);
                                            paxSSR.PassengerNumber = (Int16)j;
                                            if (itinerary.Passenger[j].MealType.Split(',').Length > 1 && itinerary.Segments[i].Group ==1)//Return
                                            {
                                                paxSSR.SSRCode = itinerary.Passenger[j].MealType.Split(',')[1];
                                            }
                                            else if(itinerary.Segments[i].Group == 0)//Oneway
                                            {
                                                paxSSR.SSRCode = itinerary.Passenger[j].MealType.Split(',')[0];//Oneway
                                            }

                                            paxSSR.SSRNumber = (Int16)0;
                                            paxSSR.SSRValue = (Int16)0;
                                            if (!string.IsNullOrEmpty(paxSSR.SSRCode) && paxSSR.SSRCode.Length > 0)
                                            {
                                                PaxSSRs.Add(paxSSR);
                                            }
                                            baggageCount++;
                                        }
                                    }
                                   
                                }

                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = PaxSSRs.ToArray();
                            }
                        }
                        #endregion

                        //Onward Via With Single Segment having single leg or multiple legs
                        #region OnwardViaBooking
                        else if (IsOnwardVIABooking)
                        {
                            string[] onward = onFlightNumber.Split('-');
                            string[] depart = retFlightNumber.Split('-');

                            requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[1];
                            for (int i = 0; i < 1; i++)
                            {
                                infantCount = 0;
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = (onward[1]);
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = (onward[0]);
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = "6E";
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = (i == 0 ? onward[2] : depart[2]);
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = (i == 0 ? onDepartureTime : retDepartureTime);
                                List<PaxSSR> PaxSSRs = new List<PaxSSR>();
                                for (int j = 0; j < itinerary.Passenger.Length; j++)
                                {
                                    if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                    {
                                        if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                        {
                                            PaxSSR paxSSR = new PaxSSR();
                                            paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                            paxSSR.ActionStatusCode = "NN";
                                            paxSSR.ArrivalStation = onward[1];
                                            paxSSR.DepartureStation = onward[0];
                                            paxSSR.PassengerNumber = (Int16)j;
                                            if (itinerary.Passenger[j].BaggageType.Split(',').Length > 1)//Return
                                            {
                                                paxSSR.SSRCode = itinerary.Passenger[j].BaggageType.Split(',')[i];
                                            }
                                            else
                                            {
                                                paxSSR.SSRCode = itinerary.Passenger[j].BaggageType;//Oneway
                                            }

                                            paxSSR.SSRNumber = (Int16)0;
                                            paxSSR.SSRValue = (Int16)0;
                                            if (paxSSR.SSRCode.Length > 0)
                                            {
                                                PaxSSRs.Add(paxSSR);
                                            }
                                            baggageCount++;
                                        }
                                    }
                                    if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                    {
                                        PaxSSR paxSSR = new PaxSSR();
                                        paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                        paxSSR.ActionStatusCode = "NN";

                                        paxSSR.ArrivalStation = onward[1];
                                        paxSSR.DepartureStation = onward[0];

                                        paxSSR.PassengerNumber = (Int16)infantCount;

                                        paxSSR.SSRCode = "INFT";//Oneway

                                        paxSSR.SSRNumber = (Int16)0;
                                        paxSSR.SSRValue = (Int16)0;
                                        if (paxSSR.SSRCode.Length > 0)
                                        {
                                            PaxSSRs.Add(paxSSR);
                                        }
                                        infantCount++;
                                    }
                                }

                                //Meal Selection SSR
                                for (int j = 0; j < itinerary.Passenger.Length; j++)
                                {
                                    if (itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0 && itinerary.Passenger[j].Type != PassengerType.Infant)
                                    {
                                        //if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType.Length > 0)
                                        {
                                            PaxSSR paxSSR = new PaxSSR();
                                            paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                            paxSSR.ActionStatusCode = "NN";
                                            paxSSR.ArrivalStation = onward[1];
                                            paxSSR.DepartureStation = onward[0];
                                            paxSSR.PassengerNumber = (Int16)j;
                                            if (itinerary.Passenger[j].MealType.Split(',').Length > 1 && itinerary.Segments[i].Group == 1)//Return
                                            {
                                                paxSSR.SSRCode = itinerary.Passenger[j].MealType.Split(',')[1];
                                            }
                                            else if(itinerary.Segments[i].Group == 0)
                                            {
                                                paxSSR.SSRCode = itinerary.Passenger[j].MealType.Split(',')[0];//Oneway
                                            }

                                            paxSSR.SSRNumber = (Int16)0;
                                            paxSSR.SSRValue = (Int16)0;
                                            if (!string.IsNullOrEmpty(paxSSR.SSRCode) && paxSSR.SSRCode.Length > 0)
                                            {
                                                PaxSSRs.Add(paxSSR);
                                            }
                                            baggageCount++;
                                        }
                                    }
                                   
                                }

                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = PaxSSRs.ToArray();
                            }
                        }
                        #endregion

                        //Onward Connecting Booking (or) Onward Connecting With Via Combination
                        //More than one segment ,segment having single leg or multiple legs

                        #region Onward Connecting Booking (or) Onward Connecting With Via Combination
                        else
                        {
                            requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[itinerary.Segments.Length];
                            for (int i = 0; i < itinerary.Segments.Length; i++)
                            {
                                infantCount = 0;
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = "6E";
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = itinerary.Segments[i].FlightNumber.Replace("6E", "").Trim();
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = itinerary.Segments[i].DepartureTime;
                                List<PaxSSR> PaxSSRs = new List<PaxSSR>();
                                for (int j = 0; j < itinerary.Passenger.Length; j++)
                                {
                                    if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                    {
                                        if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                        {
                                            PaxSSR paxSSR = new PaxSSR();
                                            paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                            paxSSR.ActionStatusCode = "NN";
                                            paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                            paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                            //paxSSR.FeeCode = "";
                                            //paxSSR.Note = "";
                                            paxSSR.PassengerNumber = (Int16)j;
                                            if (itinerary.Passenger[j].BaggageType.Split(',').Length > 1)//Return
                                            {
                                                paxSSR.SSRCode = itinerary.Passenger[j].BaggageType.Split(',')[itinerary.Segments[i].Group];
                                            }
                                            else
                                            {
                                                paxSSR.SSRCode = itinerary.Passenger[j].BaggageType;//Oneway
                                            }
                                            //paxSSR.SSRDetail = "";
                                            paxSSR.SSRNumber = (Int16)0;
                                            paxSSR.SSRValue = (Int16)0;
                                            if (paxSSR.SSRCode.Length > 0)
                                            {
                                                PaxSSRs.Add(paxSSR);
                                            }
                                            baggageCount++;
                                        }
                                    }
                                    if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                    {
                                        PaxSSR paxSSR = new PaxSSR();
                                        paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                        paxSSR.ActionStatusCode = "NN";
                                        paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                        paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                        paxSSR.PassengerNumber = (Int16)infantCount;

                                        paxSSR.SSRCode = "INFT";//Oneway

                                        paxSSR.SSRNumber = (Int16)0;
                                        paxSSR.SSRValue = (Int16)0;
                                        if (!string.IsNullOrEmpty(paxSSR.SSRCode) && paxSSR.SSRCode.Length > 0)
                                        {
                                            PaxSSRs.Add(paxSSR);
                                        }
                                        infantCount++;
                                    }
                                }

                                for (int j = 0; j < itinerary.Passenger.Length; j++)
                                {
                                    if (itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0 && itinerary.Passenger[j].Type != PassengerType.Infant)
                                    {
                                        //if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType.Length > 0)
                                        {
                                            PaxSSR paxSSR = new PaxSSR();
                                            paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                            paxSSR.ActionStatusCode = "NN";
                                            paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                            paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                            //paxSSR.FeeCode = "";
                                            //paxSSR.Note = "";
                                            paxSSR.PassengerNumber = (Int16)j;
                                            if (itinerary.Passenger[j].MealType.Split(',').Length > 1 && itinerary.Segments[i].Group == 1)//Return
                                            {
                                                paxSSR.SSRCode = itinerary.Passenger[j].MealType.Split(',')[itinerary.Segments[i].Group];
                                            }
                                            else if(itinerary.Segments[i].Group == 0)
                                            {
                                                paxSSR.SSRCode = itinerary.Passenger[j].MealType.Split(',')[0];//Oneway
                                            }
                                            //paxSSR.SSRDetail = "";
                                            paxSSR.SSRNumber = (Int16)0;
                                            paxSSR.SSRValue = (Int16)0;
                                            if (!string.IsNullOrEmpty(paxSSR.SSRCode) && paxSSR.SSRCode.Length > 0)
                                            {
                                                PaxSSRs.Add(paxSSR);
                                            }
                                            baggageCount++;
                                        }
                                    }
                                   
                                }

                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = PaxSSRs.ToArray();
                            }
                        }
                        #endregion

                    }
                    //No Direct Flights OneWay or Return.
                    else
                    {
                        string[] onward = onFlightNumber.Split('-');
                        string[] depart = retFlightNumber.Split('-');

                        if (returnSegCount > 0)
                        {
                            requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[2];
                        }
                        else
                        {
                            requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[itinerary.Segments.Length];
                        }

                        if (returnSegCount > 0)
                        {

                            requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[itinerary.Segments.Length];

                            bool onwardBaggageSent = true;
                            bool returnBaggageSent = true;

                            //Return Connecting Flights With more than one segment and each segment having single leg or multiple legs
                            #region Return More Than One Segment
                            for (int i = 0; i < itinerary.Segments.Length; i++)
                            {

                                infantCount = 0;
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = "6E";
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = itinerary.Segments[i].FlightNumber;
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = itinerary.Segments[i].DepartureTime;
                                List<PaxSSR> PaxSSRs = new List<PaxSSR>();
                                for (int j = 0; j < itinerary.Passenger.Length; j++)
                                {
                                    if ((itinerary.Segments[i].Group == 0 && onwardBaggageSent) || (itinerary.Segments[i].Group == 1 && returnBaggageSent)) // For Connecting Flights need to pass the baggage only for first segment
                                    {
                                        if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                        {
                                            if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                            {
                                                PaxSSR paxSSR = new PaxSSR();
                                                paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                                paxSSR.ActionStatusCode = "NN";
                                                paxSSR.ArrivalStation = (i == 0 ? onward[1] : depart[1]);
                                                paxSSR.DepartureStation = (i == 0 ? onward[0] : depart[0]);
                                                paxSSR.PassengerNumber = (Int16)j;
                                                if (itinerary.Passenger[j].BaggageType.Split(',').Length > 1)//Return
                                                {
                                                    if (itinerary.Segments[i].Group == 0)
                                                    {
                                                        paxSSR.SSRCode = itinerary.Passenger[j].BaggageType.Split(',')[0]; // Oneway baggage
                                                    }
                                                    else
                                                    {
                                                        paxSSR.SSRCode = itinerary.Passenger[j].BaggageType.Split(',')[1]; // Return baggage.
                                                    }
                                                }
                                                else
                                                {
                                                    paxSSR.SSRCode = itinerary.Passenger[j].BaggageType;//Oneway
                                                }

                                                paxSSR.SSRNumber = (Int16)0;
                                                paxSSR.SSRValue = (Int16)0;
                                                if (!string.IsNullOrEmpty(paxSSR.SSRCode) && paxSSR.SSRCode.Length > 0)
                                                {
                                                    PaxSSRs.Add(paxSSR);
                                                }
                                                baggageCount++;
                                            }
                                        }
                                    }


                                    if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                    {
                                        PaxSSR paxSSR = new PaxSSR();
                                        paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                        paxSSR.ActionStatusCode = "NN";

                                        paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                        paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;

                                        paxSSR.PassengerNumber = (Int16)infantCount;

                                        paxSSR.SSRCode = "INFT";//Oneway

                                        paxSSR.SSRNumber = (Int16)0;
                                        paxSSR.SSRValue = (Int16)0;
                                        if (!string.IsNullOrEmpty(paxSSR.SSRCode) && paxSSR.SSRCode.Length > 0)
                                        {
                                            PaxSSRs.Add(paxSSR);
                                        }
                                        infantCount++;
                                    }


                                }

                                for (int j = 0; j < itinerary.Passenger.Length; j++)
                                {
                                    if ((itinerary.Segments[i].Group == 0 && onwardBaggageSent) || (itinerary.Segments[i].Group == 1 && returnBaggageSent)) // For Connecting Flights need to pass the baggage only for first segment
                                    {
                                        if (itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0 && itinerary.Passenger[j].Type != PassengerType.Infant)
                                        {
                                            //if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType.Length > 0)
                                            {
                                                PaxSSR paxSSR = new PaxSSR();
                                                paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                                paxSSR.ActionStatusCode = "NN";
                                                paxSSR.ArrivalStation = (i == 0 ? onward[1] : depart[1]);
                                                paxSSR.DepartureStation = (i == 0 ? onward[0] : depart[0]);
                                                paxSSR.PassengerNumber = (Int16)j;
                                                if (itinerary.Passenger[j].MealType.Split(',').Length > 1 && itinerary.Segments[i].Group ==1)//Return
                                                {
                                                    
                                                 paxSSR.SSRCode = itinerary.Passenger[j].MealType.Split(',')[1];
                                                    
                                                }
                                                else if(itinerary.Segments[i].Group == 0)
                                                {
                                                    paxSSR.SSRCode = itinerary.Passenger[j].MealType.Split(',')[0];//Oneway
                                                }

                                                paxSSR.SSRNumber = (Int16)0;
                                                paxSSR.SSRValue = (Int16)0;
                                                if (!string.IsNullOrEmpty(paxSSR.SSRCode) && paxSSR.SSRCode.Length > 0)
                                                {
                                                    PaxSSRs.Add(paxSSR);
                                                }
                                                baggageCount++;
                                            }
                                        }
                                    }


                                   


                                }

                                if (PaxSSRs.Count > 0)
                                {

                                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = PaxSSRs.ToArray();
                                }

                                if (onwardBaggageSent)
                                {
                                    onwardBaggageSent = false;
                                }
                                if (itinerary.Segments[i].Group == 1 && returnBaggageSent)
                                {
                                    returnBaggageSent = false;
                                }

                            }
                            #endregion

                        }
                        else
                        {
                            //Onward Connecting Flights With more than one segment and each segment having single leg or multiple legs
                            #region Only OnwardSegments
                            for (int i = 0; i < itinerary.Segments.Length; i++)
                            {
                                infantCount = 0;
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                                //requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = onward[1];
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = "6E";
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = itinerary.Segments[i].FlightNumber;
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = itinerary.Segments[i].DepartureTime;
                                List<PaxSSR> PaxSSRs = new List<PaxSSR>();
                                for (int j = 0; j < itinerary.Passenger.Length; j++)
                                {
                                    if (i == 0)
                                    {
                                        if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                        {
                                            if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                            {
                                                PaxSSR paxSSR = new PaxSSR();
                                                paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                                paxSSR.ActionStatusCode = "NN";
                                                paxSSR.ArrivalStation = onward[1];
                                                paxSSR.DepartureStation = onward[0];
                                                //paxSSR.FeeCode = "";
                                                //paxSSR.Note = "";
                                                paxSSR.PassengerNumber = (Int16)j;
                                                if (itinerary.Passenger[j].BaggageType.Split(',').Length > 1)//Return
                                                {
                                                    paxSSR.SSRCode = itinerary.Passenger[j].BaggageType.Split(',')[i];
                                                }
                                                else
                                                {
                                                    paxSSR.SSRCode = itinerary.Passenger[j].BaggageType;//Oneway
                                                }
                                                //paxSSR.SSRDetail = "";
                                                paxSSR.SSRNumber = (Int16)0;
                                                paxSSR.SSRValue = (Int16)0;
                                                if (!string.IsNullOrEmpty(paxSSR.SSRCode) && paxSSR.SSRCode.Length > 0)
                                                {
                                                    PaxSSRs.Add(paxSSR);
                                                }
                                                baggageCount++;
                                            }
                                        }
                                    }

                                    if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                    {
                                        PaxSSR paxSSR = new PaxSSR();
                                        paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                        paxSSR.ActionStatusCode = "NN";
                                        paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                        paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                        paxSSR.PassengerNumber = (Int16)infantCount;

                                        paxSSR.SSRCode = "INFT";//Oneway

                                        paxSSR.SSRNumber = (Int16)0;
                                        paxSSR.SSRValue = (Int16)0;
                                        if (!string.IsNullOrEmpty(paxSSR.SSRCode) && paxSSR.SSRCode.Length > 0)
                                        {
                                            PaxSSRs.Add(paxSSR);
                                        }
                                        infantCount++;
                                    }


                                }

                                for (int j = 0; j < itinerary.Passenger.Length; j++)
                                {
                                    if (i == 0)
                                    {
                                        if (itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0 && itinerary.Passenger[j].Type != PassengerType.Infant)
                                        {
                                            //if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType.Length > 0)
                                            {
                                                PaxSSR paxSSR = new PaxSSR();
                                                paxSSR.State = Indigo.BookingMgr.MessageState.New;
                                                paxSSR.ActionStatusCode = "NN";
                                                paxSSR.ArrivalStation = onward[1];
                                                paxSSR.DepartureStation = onward[0];
                                                paxSSR.PassengerNumber = (Int16)j;
                                                if (itinerary.Passenger[j].MealType.Split(',').Length > 1 && itinerary.Segments[i].Group == 1)//Return
                                                {
                                                    paxSSR.SSRCode = itinerary.Passenger[j].MealType.Split(',')[i];
                                                }
                                                else if(itinerary.Segments[i].Group == 0)
                                                {
                                                    paxSSR.SSRCode = itinerary.Passenger[j].MealType;//Oneway
                                                }
                                                //paxSSR.SSRDetail = "";
                                                paxSSR.SSRNumber = (Int16)0;
                                                paxSSR.SSRValue = (Int16)0;
                                                if (!string.IsNullOrEmpty(paxSSR.SSRCode) && paxSSR.SSRCode.Length > 0)
                                                {
                                                    PaxSSRs.Add(paxSSR);
                                                }
                                                baggageCount++;
                                            }
                                        }
                                    }
                                }
                                requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = PaxSSRs.ToArray();
                            }
                            #endregion
                        }
                    }


                    request.SellRequestData = requestData;

                    //If Excess baggage is selected then book it otherwise skip this operation
                    if (baggageCount > 0 || infantCount > 0)
                    {
                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                            string filePath = xmlLogPath + "IndigoBookBaggageRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, request);
                            sw.Close();
                        }
                        catch { }

                        SellResponse sellResponse_v_4_5 = bookingAPI.Sell(request);
                        responseData = sellResponse_v_4_5.BookingUpdateResponseData;

                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                            string filePath = xmlLogPath + "IndigoBookBaggageResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, responseData);
                            sw.Close();
                        }
                        catch { }
                    }
                    else
                    {
                        responseData = null;
                    }
                }
                else
                {
                    throw new Exception("(Indigo)Failed to get the LogonResponse");
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to book Baggage. Reason : " + ex.ToString(), "");
                throw ex;
            }

            //Logout(loginResponse.Signature);

            return responseData;
        }

        /// <summary>
        ///  Update passenger data for the booking.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private BookingUpdateResponseData UpdatePassengers(FlightItinerary itinerary, string signature)
        {
            BookingUpdateResponseData upResponse = new BookingUpdateResponseData();

            try
            {
                logonResponse = Login();
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {

                    int paxCount = 0, infantCount = 0, infants = 0;
                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        if (itinerary.Passenger[i].Type != PassengerType.Infant)
                        {
                            paxCount++;
                        }
                        else
                        {
                            infantCount++;
                        }
                    }

                    UpdatePassengersRequest updatePassengerRequest = new UpdatePassengersRequest();
                    updatePassengerRequest.ContractVersion = contractVersion;
                    updatePassengerRequest.Signature = logonResponse.Signature;
                    //Added by Lokesh on 11-June-2018
                    //New skies Version 4.5
                    //Need to send EnableExceptionStackTrace = false(by default) in the request.
                    updatePassengerRequest.EnableExceptionStackTrace = false;
                    updatePassengerRequest.updatePassengersRequestData = new UpdatePassengersRequestData();
                    updatePassengerRequest.updatePassengersRequestData.Passengers = new Passenger[paxCount];

                    for (int i = 0; i < paxCount; i++)
                    {
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i] = new Passenger();
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerNumber = (Int16)i;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].State = Indigo.BookingMgr.MessageState.New;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names = new BookingName[1];
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0] = new BookingName();
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].Title = itinerary.Passenger[i].Title.ToUpper();
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].FirstName = itinerary.Passenger[i].FirstName;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].LastName = itinerary.Passenger[i].LastName;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].State = Indigo.BookingMgr.MessageState.New;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo = new PassengerInfo();
                        if (itinerary.Passenger[i].Gender == Gender.Male)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Gender = Indigo.BookingMgr.Gender.Male;
                        }
                        else
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Gender = Indigo.BookingMgr.Gender.Female;
                        }
                        if (itinerary.Passenger[i].Nationality != null)
                        {
                            if (!string.IsNullOrEmpty(itinerary.Passenger[i].Nationality.CountryCode))
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Nationality = itinerary.Passenger[i].Nationality.CountryCode;
                            }
                        }
                        if (itinerary.Passenger[i].Country != null)
                        {
                            if (!string.IsNullOrEmpty(itinerary.Passenger[i].Country.CountryCode))
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.ResidentCountry = itinerary.Passenger[i].Country.CountryCode;
                            }
                        }                   
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos = new PassengerTypeInfo[1];
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0] = new PassengerTypeInfo();
                        if (itinerary.Passenger[i].Type == PassengerType.Adult)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].PaxType = "ADT";
                            if (itinerary.Passenger[i].DateOfBirth != DateTime.MinValue)
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = itinerary.Passenger[i].DateOfBirth;
                            }
                            else
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = DateTime.Today.AddYears(-30);
                            }
                            if (infantCount - infants > 0)
                            {
                                FlightPassenger infantPax = itinerary.Passenger[paxCount + infants];
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant = new PassengerInfant();
                                if (infantPax.DateOfBirth != DateTime.MinValue)
                                {
                                    updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.DOB = infantPax.DateOfBirth;
                                }
                                else
                                {
                                    updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.DOB = DateTime.Today.AddDays(-365);
                                }
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Gender = (infantPax.Gender == Gender.Male ? Indigo.BookingMgr.Gender.Male : Indigo.BookingMgr.Gender.Female);
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names = new BookingName[1];
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0] = new BookingName();
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].FirstName = infantPax.FirstName;
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].LastName = infantPax.LastName;
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].Title = string.Empty;//As per latest 4.2 infant will have no title
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].State = Indigo.BookingMgr.MessageState.New;
                                if (itinerary.Passenger[i].Nationality != null)
                                {
                                    if (!string.IsNullOrEmpty(itinerary.Passenger[i].Nationality.CountryCode))
                                    {
                                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Nationality = itinerary.Passenger[i].Nationality.CountryCode;
                                    }
                                }
                                if (itinerary.Passenger[i].Country != null)
                                {
                                    if (!string.IsNullOrEmpty(itinerary.Passenger[i].Country.CountryCode))
                                    {
                                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.ResidentCountry = itinerary.Passenger[i].Country.CountryCode;
                                    }
                                }
                                
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.State = Indigo.BookingMgr.MessageState.New;
                                infants++;
                            }
                        }
                        else if (itinerary.Passenger[i].Type == PassengerType.Child)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].PaxType = "CHD";
                            if (itinerary.Passenger[i].DateOfBirth != DateTime.MinValue)
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = itinerary.Passenger[i].DateOfBirth;
                            }
                            else
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = DateTime.Today.AddYears(-10);
                            }
                        }
                    }

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(UpdatePassengersRequest));
                        string filePath = xmlLogPath + "IndigoUpdatePassengersRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, updatePassengerRequest);
                        sw.Close();
                    }
                    catch { }

                    UpdatePassengersResponse upResponse_v_4_5 = bookingAPI.UpdatePassengers(updatePassengerRequest);
                    upResponse = upResponse_v_4_5.BookingUpdateResponseData;

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                        string filePath = xmlLogPath + "IndigoUpdatePassengersResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, upResponse);
                        sw.Close();
                    }
                    catch { }
                }
                else
                {
                    throw new Exception("(Indigo)Failed to get the LogonResponse");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, appUserId, "Failed to update passengers. Error : " + ex.ToString(), "");
                throw ex;
            }
            return upResponse;
        }

        /// <summary>
        /// Used to confirm the booking amount against the available balance.
        /// </summary>
        /// <param name="bookingAmount"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private AddPaymentToBookingResponseData AddPaymentForBooking(decimal bookingAmount, string signature)
        {
            AddPaymentToBookingResponseData response = new AddPaymentToBookingResponseData();


            try
            {
                logonResponse = Login();
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {
                    AddPaymentToBookingRequest payRequest = new AddPaymentToBookingRequest();
                    payRequest.ContractVersion = contractVersion;
                    payRequest.Signature = logonResponse.Signature;
                    //Added by Lokesh on 11-June-2018
                    //New skies Version 4.5
                    //Need to send EnableExceptionStackTrace = false(by default) in the request.
                    payRequest.EnableExceptionStackTrace = false;
                    AddPaymentToBookingRequestData request = new AddPaymentToBookingRequestData();
                    request.AccountNumber = agentId;
                    request.AccountNumberID = 0;
                    request.AuthorizationCode = "";
                    request.Deposit = false;
                    request.Expiration = new DateTime();
                    request.Installments = 1;
                    request.MessageState = Indigo.BookingMgr.MessageState.New;
                    request.ParentPaymentID = 0;
                    request.PaymentAddresses = new PaymentAddress[0];
                    request.PaymentFields = new PaymentField[0];
                    request.PaymentMethodCode = "AG";
                    request.PaymentMethodType = RequestPaymentMethodType.AgencyAccount;
                    request.PaymentText = "";
                    request.QuotedAmount = bookingAmount;
                    request.QuotedCurrencyCode = currencyCode;
                    request.ReferenceType = PaymentReferenceType.Default;
                    request.Status = BookingPaymentStatus.New;
                    request.ThreeDSecureRequest = new ThreeDSecureRequest();
                    request.WaiveFee = false;

                    payRequest.addPaymentToBookingReqData = request;
                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(AddPaymentToBookingRequest));
                        string filePath = xmlLogPath + "IndigoAddPaymentToBookingRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, payRequest);
                        sw.Close();
                    }
                    catch { }
                    AddPaymentToBookingResponse response_v_4_5 = bookingAPI.AddPaymentToBooking(payRequest);
                    response = response_v_4_5.BookingPaymentResponse;

                    // AddPaymentToBookingResponse.addPaymentToBookingResp = bookingAPI.AddPaymentToBooking(contractVersion, signature, payRequest.addPaymentToBookingReqData);

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(AddPaymentToBookingResponseData));
                        string filePath = xmlLogPath + "IndigoAddPaymentToBookingResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, response);
                        sw.Close();
                    }
                    catch { }
                }
                else
                {
                    throw new Exception("(Indigo)Failed to get the LogonResponse");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to Add Payment for booking. Reason : " + ex.ToString(), "");
                throw ex;
            }

            //Logout(loginResponse.Signature);

            //Payment validation errors check.

            // bool PaymentSuccess = false;
            //  if (response.ValidationPayment.PaymentValidationErrors != null && response.ValidationPayment.PaymentValidationErrors.Length == 0)
            //  {
            //      PaymentSuccess = true;
            //  }
            ///  else
            //  {
            //      PaymentSuccess = false;
            //  }
        
            return response;
        }

        /// <summary>
        /// Book the Itinerary
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public BookingResponse BookItinerary(ref FlightItinerary itinerary)
        {
            BookingResponse response = new BookingResponse();


            try
            {
                
                    logonResponse = Login();

                    if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                    {
                    BookingUpdateResponseData bookResponse = new BookingUpdateResponseData(); //= Book(itinerary, logonResponse.Signature);
                        if(true)
                        //if (bookResponse != null && bookResponse.Success != null && bookResponse.Success.PNRAmount.TotalCost > 0)
                        {
                            int infantCount = 0;
                            foreach (FlightPassenger pax in itinerary.Passenger)
                            {
                                if (pax.Type == PassengerType.Infant)
                                {
                                    infantCount += 1;
                                }
                            }

                            string count = GetSSRCount(itinerary);
                            if (!string.IsNullOrEmpty(count))
                            {
                                int baggageSSRCount = Convert.ToInt32(count.Split('~')[0]);
                                int infantSSRCount = Convert.ToInt32(count.Split('~')[1]);
                                if (baggageSSRCount > 0 || infantSSRCount > 0)
                                {
                                    BookingUpdateResponseData bagResponse = BookBaggage(itinerary, logonResponse.Signature);
                                    if (bagResponse != null && bagResponse.Success != null && bagResponse.Success.PNRAmount.TotalCost > 0)
                                    {
                                        bookResponse = bagResponse;
                                    }
                                    else
                                    {
                                        Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)SSR Baggage request Failed. Reason : " + bagResponse.OtherServiceInformations[0].Text, "");
                                        throw new Exception("Requested SSR Not available.Reason:" + bagResponse.OtherServiceInformations[0].Text);
                                    }
                                }
                            }



                            decimal bookingAmount = 0;
                            //No Seperate request for Infant, BookBaggage(...) method will handle infant requests
                            //if (infantCount > 0)
                            //{
                            //    //SellSSR by Infant
                            //    bookingAmount = SellSSRForInfant(itinerary, loginResponse.Signature);
                            //}
                            //else
                            {
                                //bookingAmount = bookResponse.Success.PNRAmount.TotalCost;
                            }

                            BookingUpdateResponseData upResponse = UpdatePassengers(itinerary, logonResponse.Signature);

                            if (upResponse != null && upResponse.Success != null && upResponse.Success.PNRAmount.TotalCost > 0)
                            {
                                bookingAmount = upResponse.Success.PNRAmount.TotalCost;
                                AddPaymentToBookingResponseData paymentResponse = AddPaymentForBooking(bookingAmount, logonResponse.Signature);

                                //If there are any Payment validations failed do not allow to commit booking
                                if (paymentResponse != null && paymentResponse.ValidationPayment != null && paymentResponse.ValidationPayment.PaymentValidationErrors != null && paymentResponse.ValidationPayment.PaymentValidationErrors.Length == 0)
                                {
                                    logonResponse = Login();
                                    if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                                    {
                                        BookingUpdateResponseData responseData = new BookingUpdateResponseData();
                                        BookingCommitRequest bookRequest = new BookingCommitRequest();
                                        bookRequest.ContractVersion = contractVersion;
                                        bookRequest.Signature = logonResponse.Signature;
                                    //Added by Lokesh on 11-June-2018
                                    //New skies Version 4.5
                                    //Need to send EnableExceptionStackTrace = false(by default) in the request.
                                    bookRequest.EnableExceptionStackTrace = false;

                                        BookingCommitRequestData request = new BookingCommitRequestData();
                                        request.BookingChangeCode = "";
                                        request.BookingComments = new BookingComment[0];

                                        request.SourcePOS = new PointOfSale();
                                        request.SourcePOS.AgentCode = "AG";
                                        request.SourcePOS.OrganizationCode = agentId;
                                        request.SourcePOS.DomainCode = agentDomain;

                                        request.BookingID = 0;
                                        request.BookingParentID = 0;
                                        request.ChangeHoldDateTime = false;
                                        request.CurrencyCode = currencyCode;
                                        request.DistributeToContacts = true;//Send booking confirmation email to the pax
                                        request.GroupName = "";
                                        request.NumericRecordLocator = "";
                                        request.ParentRecordLocator = "";
                                        request.RecordLocator = "";
                                        request.RecordLocators = new RecordLocator[0];
                                        request.RestrictionOverride = false;

                                        request.State = Indigo.BookingMgr.MessageState.New;
                                        request.SystemCode = "";
                                        request.WaiveNameChangeFee = false;
                                        request.WaivePenaltyFee = false;
                                        request.WaiveSpoilageFee = false;
                                        request.Passengers = new Passenger[0];
                                        request.PaxCount = (Int16)(itinerary.Passenger.Length - infantCount);

                                        for (int i = 0; i < itinerary.Passenger.Length; i++)
                                        {
                                            FlightPassenger pax = itinerary.Passenger[i];
                                            if (pax.Type != PassengerType.Infant)
                                            {
                                                if (pax.IsLeadPax)
                                                {
                                                    request.BookingContacts = new BookingContact[1];
                                                    request.BookingContacts[0] = new BookingContact();
                                                    request.BookingContacts[0].AddressLine1 = (string.IsNullOrEmpty(pax.AddressLine1) ? "Address Line1" : pax.AddressLine1);
                                                    request.BookingContacts[0].AddressLine2 = pax.AddressLine2;
                                                    request.BookingContacts[0].AddressLine3 = "";
                                                    request.BookingContacts[0].City = "SHJ";
                                                    request.BookingContacts[0].CompanyName = "Cozmo Travel";
                                                    if (pax.Country != null && !string.IsNullOrEmpty(pax.Country.CountryCode))
                                                    {
                                                        
                                                     request.BookingContacts[0].CountryCode = pax.Country.CountryCode;
                                                        
                                                    }
                                                    else //Since we are hard coding city as "SHJ" so we are passing country code as "AE";
                                                    {
                                                        request.BookingContacts[0].CountryCode = "AE"; 
                                                    }
                                                    request.BookingContacts[0].CultureCode = "en-GB";
                                                    request.BookingContacts[0].SourceOrganization = agentId;
                                                    request.BookingContacts[0].CustomerNumber = "";
                                                    request.BookingContacts[0].DistributionOption = DistributionOption.Email;
                                                    //Modified by Lokesh on 27-Nov-2017. Pass Lead Pax email for contact info.
                                                    if (!string.IsNullOrEmpty(pax.Email))
                                                    {
                                                        request.BookingContacts[0].EmailAddress = pax.Email; //The Original Ticket details will be sent to this email ,not to the passenger email address.
                                                    }
                                                    else
                                                    {
                                                        request.BookingContacts[0].EmailAddress = "tech@cozmotravel.com"; //The Original Ticket details will be sent to this email ,not to the passenger email address.
                                                    }
                                                    request.BookingContacts[0].Fax = "";
                                                    request.BookingContacts[0].HomePhone = pax.CellPhone;
                                                    request.BookingContacts[0].Names = new BookingName[1];
                                                    request.BookingContacts[0].Names[0] = new BookingName();
                                                    request.BookingContacts[0].Names[0].FirstName = pax.FirstName;
                                                    request.BookingContacts[0].Names[0].LastName = pax.LastName;
                                                    request.BookingContacts[0].Names[0].MiddleName = "";
                                                    request.BookingContacts[0].Names[0].State = Indigo.BookingMgr.MessageState.New;
                                                    request.BookingContacts[0].Names[0].Suffix = "";
                                                    request.BookingContacts[0].Names[0].Title = pax.Title.ToUpper();//Send always UPPER CASE
                                                    request.BookingContacts[0].NotificationPreference = NotificationPreference.None;
                                                    //Added by Lokesh on 4-Oct-2017 Regarding Lead Pax Destination Phone Number.
                                                    if (!string.IsNullOrEmpty(pax.DestinationPhone))
                                                    {
                                                        request.BookingContacts[0].OtherPhone = pax.DestinationPhone;
                                                    }
                                                    else
                                                    {
                                                        request.BookingContacts[0].OtherPhone = "";
                                                    }
                                                    request.BookingContacts[0].PostalCode = "3093";
                                                    request.BookingContacts[0].ProvinceState = "SHJ";
                                                    request.BookingContacts[0].State = Indigo.BookingMgr.MessageState.New;
                                                    request.BookingContacts[0].TypeCode = "P";
                                                    request.BookingContacts[0].WorkPhone = pax.CellPhone;
                                                }
                                            }
                                        }

                            bookRequest.BookingCommitRequestData = request;

                                    try
                                    {

                                        XmlSerializer ser = new XmlSerializer(typeof(BookingCommitRequest));
                                        string filePath = xmlLogPath + "IndigoBookingCommitRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                        StreamWriter sw = new StreamWriter(filePath);
                                        ser.Serialize(sw, bookRequest);
                                        sw.Close();
                                    }
                                    catch { }
                                    BookingCommitResponse bookingCommitResponse_v_4_5 = bookingAPI.BookingCommit(bookRequest);
                                    responseData = bookingCommitResponse_v_4_5.BookingUpdateResponseData;

                                    try
                                    {

                                        XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                                        string filePath = xmlLogPath + "IndigoBookingCommitResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                        StreamWriter sw = new StreamWriter(filePath);
                                        ser.Serialize(sw, responseData);
                                        sw.Close();
                                    }
                                    catch { }

                                        if (responseData != null)
                                        {
                                            if (responseData.Error == null)
                                            {
                                                response.PNR = responseData.Success.RecordLocator;
                                                response.ProdType = ProductType.Flight;
                                                response.Error = "";
                                                response.Status = BookingResponseStatus.Successful;
                                                itinerary.PNR = response.PNR;
                                                itinerary.FareType = "Pub";
                                            }
                                            else
                                            {
                                                response.Status = BookingResponseStatus.Failed;
                                            }
                                        }
                                        else
                                        {
                                            response.Status = BookingResponseStatus.Failed;
                                        }
                                    }
                                    else //Failed to get the LogOn Response
                                    {
                                        throw new Exception("(Indigo)Failed to get the LogonResponse");
                                    }
                                }
                                else//AddPayment failed
                                {
                                    response.Status = BookingResponseStatus.Failed;
                                    if (paymentResponse.ValidationPayment.PaymentValidationErrors != null && paymentResponse.ValidationPayment.PaymentValidationErrors.Length > 0)
                                    {
                                        response.Error = paymentResponse.ValidationPayment.PaymentValidationErrors[0].ErrorDescription;
                                        Audit.Add(EventType.Book, Severity.High, appUserId, "Indigo Booking Failed. Reason : " + response.Error, "");
                                    }
                                }
                            }
                            else//UpdatePassenger failed
                            {
                                response.Status = BookingResponseStatus.Failed;
                            }
                        }
                        else//Book failed
                        {
                            response.Status = BookingResponseStatus.Failed;
                        }
                    }
                    else
                    {
                        throw new Exception("(Indigo)Failed to get the LogonResponse");
                    }
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("session"))//Session Exception
                {
                    Basket.FlightBookingSession.Remove(appUserId.ToString());//Clear basket
                }
                Audit.Add(EventType.Book, Severity.High, 1, "(Indigo)Failed to Commit booking. Reason : " + ex.ToString(), "");
                response.Error = ex.Message;
                response.Status = BookingResponseStatus.Failed;
            }
            finally
            {
                Logout(logonResponse.Signature);
            }

            return response;
        }

        #endregion

        #region Booking Cancellation

        /// <summary>
        /// Retrieves a booking by its PNR.
        /// </summary>
        /// <param name="pnr"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        public GetBookingResponse GetBooking(string pnr, string signature)
        {
            GetBookingResponse response = new GetBookingResponse();

            try
            {
                GetBookingRequest request = new GetBookingRequest();
                request.ContractVersion = contractVersion;
                request.Signature = signature;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                request.EnableExceptionStackTrace = false;

                GetBookingRequestData requestData = new GetBookingRequestData();
                requestData.GetBookingBy = GetBookingBy.RecordLocator;
                requestData.GetByRecordLocator = new GetByRecordLocator();
                requestData.GetByRecordLocator.RecordLocator = pnr;
                request.GetBookingReqData = requestData;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingRequest));
                    string filePath = xmlLogPath + "IndigoGetBookingRequest_" + appUserId.ToString() + "_" + pnr + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch { }

                response = bookingAPI.GetBooking(request);

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingResponse));
                    string filePath = xmlLogPath + "IndigoGetBookingResponse_" + appUserId.ToString() + "_" + pnr + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, response);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to get Booking for PNR : " + pnr + ". Reason : " + ex.ToString(), "");
            }

            return response;
        }

        /// <summary>
        /// Cancels the booking for the Itinerary.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public Dictionary<string, string> CancelBooking(FlightItinerary itinerary)
        {
            Dictionary<string, string> cancellationData = new Dictionary<string, string>();

            try
            {
                #region Indigo Cancel a Booking Flow

                /* The following steps describes the complete Cancel Booking Flow in Indigo.
                 * 1.LogOn Request.
                 * 2.Get Booking Request.
                 * 3.Cancel Request.
                 * 4.LogOut Request.
                 */

                LogonResponse loginResponse = Login();

                if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature) && loginResponse.Signature.Length > 0)
                {

                    try
                    {
                        GetBookingResponse response = GetBooking(itinerary.PNR, loginResponse.Signature);

                    CancelRequest request = new CancelRequest();
                    request.ContractVersion = contractVersion;
                    request.Signature = loginResponse.Signature;
                        //Added by Lokesh on 11-June-2018
                        //New skies Version 4.5
                        //Need to send EnableExceptionStackTrace = false(by default) in the request.
                        request.EnableExceptionStackTrace = false;

                        CancelRequestData requestData = new CancelRequestData();
                        requestData.CancelBy = CancelBy.Journey;
                        requestData.CancelJourney = new CancelJourney();
                        requestData.CancelJourney.CancelJourneyRequest = new CancelJourneyRequest();
                        requestData.CancelJourney.CancelJourneyRequest.Journeys = new Journey[response.Booking.Journeys.Length];
                        for (int i = 0; i < response.Booking.Journeys.Length; i++)
                        {
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i] = new Journey();
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments = new Segment[response.Booking.Journeys[i].Segments.Length];
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].State = Indigo.BookingMgr.MessageState.New;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].JourneySellKey = response.Booking.Journeys[i].JourneySellKey;
                            for (int j = 0; j < response.Booking.Journeys[i].Segments.Length; j++)
                            {
                                Segment segment = response.Booking.Journeys[i].Segments[j];
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j] = new Segment();
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ActionStatusCode = "NN";
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ArrivalStation = segment.ArrivalStation;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ChannelType = Indigo.BookingMgr.ChannelType.API;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].DepartureStation = segment.DepartureStation;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares = new Indigo.BookingMgr.Fare[segment.Fares.Length];
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].FlightDesignator = segment.FlightDesignator;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].International = segment.International;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs = new Leg[segment.Legs.Length];
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].SegmentSellKey = segment.SegmentSellKey;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].STA = segment.STA;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].State = Indigo.BookingMgr.MessageState.New;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].STD = segment.STD;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ChannelType = segment.ChannelType;

                                for (int f = 0; f < response.Booking.Journeys[i].Segments[j].Fares.Length; f++)
                                {
                                    Indigo.BookingMgr.Fare fare = response.Booking.Journeys[i].Segments[j].Fares[f];
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f] = new Indigo.BookingMgr.Fare();
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].CarrierCode = fare.CarrierCode;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].ClassOfService = fare.ClassOfService;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].ClassType = fare.ClassType;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareApplicationType = fare.FareApplicationType;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareBasisCode = fare.FareBasisCode;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareClassOfService = fare.FareClassOfService;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareSellKey = fare.FareSellKey;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareSequence = fare.FareSequence;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareStatus = FareStatus.Default;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].InboundOutbound = fare.InboundOutbound;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].IsAllotmentMarketFare = false;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].RuleNumber = fare.RuleNumber;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].OriginalClassOfService = fare.OriginalClassOfService;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].TravelClassCode = fare.TravelClassCode;
                                }
                                for (int l = 0; l < response.Booking.Journeys[i].Segments[j].Legs.Length; l++)
                                {
                                    Leg leg = response.Booking.Journeys[i].Segments[j].Legs[l];
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l] = new Leg();
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].ArrivalStation = leg.ArrivalStation;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].DepartureStation = leg.DepartureStation;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].FlightDesignator = leg.FlightDesignator;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].InventoryLegID = leg.InventoryLegID;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].LegInfo = leg.LegInfo;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].OperationsInfo = leg.OperationsInfo;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].STA = leg.STA;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].State = Indigo.BookingMgr.MessageState.New;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].STD = leg.STD;
                                }
                            }
                            requestData.CancelJourney.CancelJourneyRequest.WaivePenaltyFee = false;
                            requestData.CancelJourney.CancelJourneyRequest.WaiveSpoilageFee = false;
                            requestData.CancelJourney.CancelJourneyRequest.PreventReprice = false;
                        }
                        request.CancelRequestData = requestData;
                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(CancelRequest));
                            string filePath = xmlLogPath + "IndigoCancelRequest_" + appUserId.ToString() + "_" + itinerary.PNR + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, request);
                            sw.Close();
                        }
                        catch { }

                        //Using the existing bookingAPI
                        CancelResponse cancelRes_v_4_5 = bookingAPI.Cancel(request);
                        BookingUpdateResponseData responseData = cancelRes_v_4_5.BookingUpdateResponseData;

                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                            string filePath = xmlLogPath + "IndigoCancelResponse_" + appUserId.ToString() + "_" + itinerary.PNR + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, responseData);
                            sw.Close();
                        }
                        catch { }

                        if (responseData != null)
                        {
                            if (responseData.Error == null)
                            {
                                string charges = responseData.Success.PNRAmount.TotalCost.ToString();
                                if (responseData.Success.PNRAmount.AlternateCurrencyBalanceDue > 0)
                                {
                                    charges = responseData.Success.PNRAmount.AlternateCurrencyBalanceDue.ToString().Replace("-", "");
                                }
                                rateOfExchange = (responseData.Success.PNRAmount.AlternateCurrencyCode != null && responseData.Success.PNRAmount.AlternateCurrencyCode.Trim().Length > 0 ? exchangeRates[responseData.Success.PNRAmount.AlternateCurrencyCode] : exchangeRates[currencyCode]);
                                cancellationData.Add("Cancelled", "True");
                                cancellationData.Add("Charges", (Convert.ToDecimal(charges) * rateOfExchange).ToString());
                            }
                            else
                            {
                                cancellationData.Add("Cancelled", "False");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.IndigoCancellation, Severity.High, appUserId, "(Indigo)Failed to Cancel booking. Reason : " + ex.ToString(), "");
                        throw ex;
                    }
                    Logout(loginResponse.Signature);
                }
                else
                {
                    throw new Exception("(Indigo)Failed to get the LogonResponse");
                }

                


                #endregion
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed Cancel Booking Failed : Reason : " + ex.ToString(), "");
            }

            return cancellationData;
        }

        #endregion

        #region Helper methods
        /// <summary>
        /// Returns the Fare Type for a Product Class 
        /// </summary>
        /// <param name="productClass"></param>
        /// <returns></returns>
        private string GetFareTypeForProductClass(string productClass)
        {
            string fareType = "";
            try
            {

                switch (productClass)
                {
                    case "R":
                        fareType = "Retail Fare";
                        break;
                    case "S":
                        fareType = "Sale Fare";
                        break;
                    case "A":
                        fareType = "Family Fare"; //(applicable for 4 - 9 passengers)
                        break;
                    case "N":
                        fareType = "Round Trip Fare";
                        break;
                    case "J":
                        fareType = "Flexi Fare";
                        break;
                    case "B":
                        fareType = "Lite Fare";
                        break;
                    case "F":
                        fareType = "Corporate Fare";
                        break;
                    case "M":
                        fareType = "SME Fare";
                        break;
                    case "T":
                        fareType = "Tactical Fare";
                        break;

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Indigo failed to get Get Fare Type For ProductClass . Reason : " + ex.Message, ex);
            }
            return fareType;
        }

        #endregion
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~IndigoAPI()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion


        //public List<SearchResult>GetJourneyResult(Journey[] journeys)
        //{
        //    List<SearchResult> ResultList = new List<SearchResult>();
        //    //Loop Variables -- j-- journey
        //    //s--Segment
        //    //f-- Fare
        //    //Direct flight – Segment and leg count should be 1 in journey.
        //    //Via Flight – Segment count should be 1 and leg count should be more than 1.
        //    //Connecting flight – Segment count should be more than 1.

        //    try
        //    {
        //        if(journeys != null && journeys.Length >0)
        //        {
        //           for(int j=0;j<journeys.Length;j++)
        //            {
        //                if (journeys[j].Segments != null && journeys[j].Segments.Length > 0)
        //                {
        //                    if(journeys[j].Segments.Length>1)//Connecting
        //                    {
        //                        List<Indigo.BookingMgr.Fare> connectingFares = new List<Indigo.BookingMgr.Fare>();
        //                        connectingFares.Clear();

        //                        for (int s=0;s<journeys[j].Segments.Length;s++)
        //                        {
        //                           if(journeys[j].Segments[s].Fares != null && journeys[j].Segments[s].Fares.Length >0)
        //                            {
        //                                 //List<Indigo.BookingMgr.Fare> conFares = new List<Indigo.BookingMgr.Fare>(journeys[j].Segments[s].Fares);
        //                                 //conFares.FindAll(delegate (Indigo.BookingMgr.Fare f) { return f.ProductClass == connectingFares[0].ProductClass; });
        //                                for(int f=0;f< journeys[j].Segments[s].Fares.Length;f++)
        //                                {
        //                                    if (connectingFares.Count > 0 && connectingFares[0].ProductClass == journeys[j].Segments[s].Fares[f].ProductClass)
        //                                    {
        //                                        connectingFares.Add(journeys[j].Segments[s].Fares[f]);
        //                                    }
        //                                    else
        //                                    {
        //                                        connectingFares.Add(journeys[j].Segments[s].Fares[f]);
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if(journeys[j].Segments.Length ==1)//Direct
        //                    {
        //                        for (int s = 0; s < journeys[j].Segments.Length; s++)
        //                        {
        //                            if (journeys[j].Segments[s].Fares != null && journeys[j].Segments[s].Fares.Length > 0)
        //                            {
        //                                for (int f = 0; f < journeys[j].Segments[s].Fares.Length; f++)
        //                                {

        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return ResultList;
        //}

            /// <summary>
            /// This methods returns the flight objects info based on segments legs info
            /// </summary>
            /// <param name="segment"></param>
            /// <param name="availableJourney"></param>
            /// <param name="productClass"></param>
            /// <param name="journeyType"></param>
            /// <returns></returns>
        public List<FlightInfo> GetFlightInfo(Segment segment, Journey availableJourney, string productClass, string journeyType)
        {
            List<FlightInfo> flightInfo = new List<FlightInfo>();
            try
            {
                if (availableJourney != null && segment != null && segment.Legs != null && segment.Legs.Length > 0)
                {
                    for (int k = 0; k < segment.Legs.Length; k++)
                    {
                        if (segment.Legs[k] != null)
                        {
                            FlightInfo flightDetails = new FlightInfo();
                            flightDetails.Airline = "6E";
                            flightDetails.ArrivalTime = segment.Legs[k].STA;
                            flightDetails.ArrTerminal = segment.Legs[k].LegInfo.ArrivalTerminal;
                            flightDetails.BookingClass = (segment != null && segment.CabinOfService.Length > 0 ? segment.CabinOfService : "C");
                            if (!string.IsNullOrEmpty(segment.CabinOfService.Trim()))
                            {
                                flightDetails.CabinClass = segment.CabinOfService;
                            }
                            else
                            {
                                flightDetails.CabinClass = "Economy";
                            }
                            flightDetails.Craft = segment.Legs[k].LegInfo.EquipmentType + "-" + segment.Legs[k].LegInfo.EquipmentTypeSuffix;
                            flightDetails.DepartureTime = segment.Legs[k].STD;
                            flightDetails.DepTerminal = segment.Legs[k].LegInfo.DepartureTerminal;
                            flightDetails.Destination = new Airport(segment.Legs[k].ArrivalStation);
                            flightDetails.Duration = flightDetails.ArrivalTime.Subtract(flightDetails.DepartureTime);
                            flightDetails.ETicketEligible = segment.Legs[k].LegInfo.ETicket;
                            flightDetails.FlightNumber = segment.Legs[k].FlightDesignator.FlightNumber;
                            flightDetails.FlightStatus = FlightStatus.Confirmed;
                            if (journeyType.ToUpper() == "ONWARD")
                            {
                                flightDetails.Group = 0;
                            }
                            else if (journeyType.ToUpper() == "RETURN")
                            {
                                flightDetails.Group = 1;
                            }
                            flightDetails.OperatingCarrier = "6E";
                            flightDetails.Origin = new Airport(segment.Legs[k].DepartureStation);
                            flightDetails.Stops = availableJourney.Segments.Length - 1;

                            //Direct flight
                            if(segment.Legs.Length == 1 && availableJourney.Segments.Length ==1)
                            {
                                flightDetails.Status = string.Empty;
                            }
                            //Connecting flight
                            else if (availableJourney.Segments.Length>1)
                            {
                                int legCount = 0;
                                for (int s = 0; s < availableJourney.Segments.Length; s++)
                                {
                                    legCount += availableJourney.Segments[s].Legs.Length;
                                }
                                if (legCount == 2)
                                {
                                    flightDetails.Status = string.Empty; //Connecting direct flight
                                }
                                else
                                {
                                    flightDetails.Status = "VIA"; //Connecting via flight
                                }
                            }
                            //VIA Flight
                            else if (segment.Legs.Length > 1 && availableJourney.Segments.Length == 1)
                            {
                                flightDetails.Status = "VIA";
                            }
                            flightDetails.UapiSegmentRefKey = segment.SegmentSellKey;
                            flightDetails.SegmentFareType = GetFareTypeForProductClass(productClass);
                            flightInfo.Add(flightDetails);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return flightInfo;
        }

        /************GST Flow For India*****************/
        /*********Start:Steps for GST Flow*******************/
        /*1.Logon Request
         *2.GetAvailabilityRequest
         *3.GetItineraryPriceRequest
         *4.UpdateContactsRequest(GST Parameters Passed in this method)
         *5.Sell Request
         *6.GetBookingFromStateRequest[Gets the bifurfacion of taxes in this method]
         *7.UpdatePassengersRequest.
         *8.AddPaymentToBookingRequest.
         *9.BookingCommitRequest.
         *10.LogOutRequest
         /*********End:Steps for GST Flow*******************/


        /****Below are the important points with respect to GST implementation****/
        //1.GST Implementation.
        //2.All carriers travelling to/ from /within india be able to apply the correct GST charges.
        //3.GST Registration Number ,Name of GST holder, Email of GST holder.
        //4.If PNR has multiple passengers only 1 GST Registration Number is allowed per PNR
        //5.In case of B2B transactions,2 new method calls will be used in the booking flow.
        //6.Methods: UpdateContactRequest and GetBookingFromState.

        ///<summary>
        /// Gets the Updated GSTFareBreakUp 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public void GetUpdatedGSTFareBreakUp(ref FlightItinerary itinerary)
        {
            try
            {
                //If the lead pax supplies the GST input.
                //Then Get the updated GST farebreak up.
                logonResponse = Login();
                //Get the bifurcation of taxes after GST input.
                if (itinerary.Passenger[0] != null && itinerary.Passenger[0].IsLeadPax && !string.IsNullOrEmpty(itinerary.Passenger[0].GSTTaxRegNo))
                  {
                     GetBookingFromStateResponse updatedGSTBreakup = GetBookingFromState(logonResponse.Signature);
                  }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo)Failed to GetUpdatedGSTFareBreakUp. Error : " + ex.ToString(), "");
                throw ex;
            }

        }

        /// <summary>
        /// Updates the GST details for the lead passenger.
        /// </summary>
        /// <returns></returns>
        public UpdateContactsResponse UpdateContactGSTDetails(string signature)
        {
            UpdateContactsResponse updateContactsResponse = new UpdateContactsResponse();
            try
            {
                UpdateContactsRequest updateContactsRequest = new UpdateContactsRequest();
                //Header
                updateContactsRequest.ContractVersion = contractVersion;
                updateContactsRequest.Signature = signature;
                updateContactsRequest.EnableExceptionStackTrace = false;
                //Body
                UpdateContactsRequestData updateContactsRequestData = new UpdateContactsRequestData();
                updateContactsRequestData.BookingContactList = new BookingContact[1];
                updateContactsRequestData.BookingContactList[0] = new BookingContact();
                updateContactsRequestData.BookingContactList[0].State = Indigo.BookingMgr.MessageState.New;
                updateContactsRequestData.BookingContactList[0].TypeCode = "I";
                updateContactsRequestData.BookingContactList[0].EmailAddress = gstOfficialEmail;
                updateContactsRequestData.BookingContactList[0].CustomerNumber = gstNumber;
                updateContactsRequestData.BookingContactList[0].CompanyName = gstCompanyName;
                updateContactsRequestData.BookingContactList[0].DistributionOption = DistributionOption.None;
                updateContactsRequestData.BookingContactList[0].NotificationPreference = NotificationPreference.None;
                updateContactsRequest.updateContactsRequestData = updateContactsRequestData;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(UpdateContactsRequest));
                    string filePath = xmlLogPath + "IndigoLeadPaxGSTRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, updateContactsRequest);
                    sw.Close();
                }
                catch { }
                updateContactsResponse = bookingAPI.UpdateContacts(updateContactsRequest);
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(UpdateContactsResponse));
                    string filePath = xmlLogPath + "IndigoLeadPaxGSTResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, updateContactsResponse);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo)Failed to update GST Details for the lead passenger. Error : " + ex.ToString(), "");
                throw ex;
            }
            return updateContactsResponse;
        }

        /// <summary>
        /// To Get the bifurcation of Taxes after GSTN update.
        /// </summary>
        /// <returns></returns>
        public GetBookingFromStateResponse GetBookingFromState(string signature)
        {
            GetBookingFromStateResponse getBookingFromStateResponse = new GetBookingFromStateResponse();
            Booking booking = new Booking();
            try
            {
                GetBookingFromStateRequest getBookingFromStateRequest = new GetBookingFromStateRequest();
                getBookingFromStateRequest.ContractVersion = contractVersion;
                getBookingFromStateRequest.Signature = signature;
                getBookingFromStateRequest.EnableExceptionStackTrace = false;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingFromStateRequest));
                    string filePath = xmlLogPath + "Indigo_GST_Tax_Breakup_Request_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, getBookingFromStateRequest);
                    sw.Close();
                }
                catch { }
                getBookingFromStateResponse = bookingAPI.GetBookingFromState(getBookingFromStateRequest);
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingFromStateResponse));
                    string filePath = xmlLogPath + "Indigo_GST_Tax_Breakup_Response_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, getBookingFromStateResponse);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo)Failed to get updated GST break up for the lead passenger. Error : " + ex.ToString(), "");
                throw ex;
            }
            return getBookingFromStateResponse;
        }

        /// <summary>
        /// This method returns the Available SSR's based on the segments.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public DataTable GetAvailableSSR(SearchResult result)
        {
            DataTable dtSourceBaggage = null;
            logonResponse = Login();

            //This method will releases the itinerary.
            //Clears the previous objects from the session if any objects are there.
            //This method will avoid duplicate bookings.
            if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
            {
                ClearRequest clearRequest = new ClearRequest();
                clearRequest.ContractVersion = contractVersion;
                clearRequest.EnableExceptionStackTrace = false;
                clearRequest.Signature = logonResponse.Signature;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(ClearRequest));
                    string filePath = xmlLogPath + "Indigo_ClearRequest_" + appUserId.ToString() + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, clearRequest);
                    sw.Close();
                }
                catch { }
                ClearResponse clearResponse = bookingAPI.Clear(clearRequest);
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(ClearResponse));
                    string filePath = xmlLogPath + "Indigo_ClearResponse_" + appUserId.ToString() + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, clearResponse);
                    sw.Close();
                }
                catch { }
            }


            try
            {

                //SSR : AdditionalBaggage and Meals [For Both Onward and Return Journeys]
                //Note:Infant is also an SSR.
                //But we will get the infant price from the price itinerary response.

                //Steps to get the available SSR Without GST input.
                //1.Login
                //2.Sell [Holds the complete itinerary]
                //3.GetSSRAvailabilityForBooking.

                //Steps to get the available SSR With GST input.
                //1.Login
                //2.UpdateContactRequest 
                //3.Sell[Holds the complete itinerary]
                //4.GetSSRAvailabilityForBooking
                //5.GetBookingFromState
               
               
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {
                   
                    //Note:Verify any GST input available then execute UpdateContactsRequest first.
                    //Step-1.1: If any GST input then UpdateContactGSTDetails method
                    UpdateContactsResponse updateGSTResponse = null;
                    if (!string.IsNullOrEmpty(gstCompanyName) && (gstCompanyName.Length > 0) &&
                       !string.IsNullOrEmpty(gstNumber) && (gstNumber.Length > 0) &&
                        !string.IsNullOrEmpty(gstOfficialEmail) && (gstOfficialEmail.Length > 0)
                       && !string.IsNullOrEmpty(logonResponse.Signature))
                    {

                        updateGSTResponse = UpdateContactGSTDetails(logonResponse.Signature);

                    }

                    //Step2:Sell
                    BookingUpdateResponseData responseData = ExecuteSell(logonResponse.Signature, result);
                    if (responseData != null)
                    {
                        //Step-3: GetAvailableSSR

                        GetSSRAvailabilityForBookingResponse ssrResponse = new GetSSRAvailabilityForBookingResponse();
                        GetSSRAvailabilityForBookingRequest ssrRequest = new GetSSRAvailabilityForBookingRequest();
                        ssrRequest.ContractVersion = contractVersion;
                        ssrRequest.EnableExceptionStackTrace = false;
                        ssrRequest.Signature = logonResponse.Signature;

                        SSRAvailabilityForBookingRequest availRequest = new SSRAvailabilityForBookingRequest();
                        availRequest.InventoryControlled = true;
                        availRequest.NonInventoryControlled = true;
                        availRequest.SeatDependent = true;
                        availRequest.NonSeatDependent = true;
                        availRequest.CurrencyCode = currencyCode;

                        List<LegKey> legKeysList = new List<LegKey>();
                        for (int i = 0; i < result.Flights.Length; i++)
                        {
                            for (int j = 0; j < result.Flights[i].Length; j++)
                            {
                                LegKey legKey = new LegKey();
                                legKey.CarrierCode = "6E";
                                legKey.FlightNumber = result.Flights[i][j].FlightNumber;
                                legKey.DepartureDate = result.Flights[i][j].DepartureTime;
                                legKey.DepartureStation = result.Flights[i][j].Origin.AirportCode;
                                legKey.ArrivalStation = result.Flights[i][j].Destination.AirportCode;
                                legKeysList.Add(legKey);
                            }
                        }
                        if (legKeysList != null && legKeysList.Count > 0)
                        {
                            availRequest.SegmentKeyList = legKeysList.ToArray();
                        }
                        ssrRequest.SSRAvailabilityForBookingRequest = availRequest;
                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(GetSSRAvailabilityForBookingRequest));
                            string filePath = xmlLogPath + "Indigo_PaxSSR_Request_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, ssrRequest);
                            sw.Close();
                        }
                        catch { }
                        ssrResponse = bookingAPI.GetSSRAvailabilityForBooking(ssrRequest);
                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(GetSSRAvailabilityForBookingResponse));
                            string filePath = xmlLogPath + "Indigo_PaxSSR_Response_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, ssrResponse);
                            sw.Close();
                        }
                        catch { }
                        if (ssrResponse != null && ssrResponse.SSRAvailabilityForBookingResponse != null && ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList != null && ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList.Length > 0)
                        {
                            dtSourceBaggage = new DataTable();
                            if (dtSourceBaggage.Columns.Count == 0)
                            {
                                dtSourceBaggage.Columns.Add("Code", typeof(string));
                                dtSourceBaggage.Columns.Add("Price", typeof(decimal));
                                dtSourceBaggage.Columns.Add("Group", typeof(int));
                                dtSourceBaggage.Columns.Add("Currency", typeof(string));
                                dtSourceBaggage.Columns.Add("Description", typeof(string));
                                dtSourceBaggage.Columns.Add("QtyAvailable", typeof(int));
                            }
                            List<AvailablePaxSSR> availablePaxSSRs_OnwardBag = new List<AvailablePaxSSR>();//OnwardBaggage
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnBag = new List<AvailablePaxSSR>();//ReturnBaggage

                            List<AvailablePaxSSR> availablePaxSSRs_OnwardMeal = new List<AvailablePaxSSR>();//OnwardMeal
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnMeal = new List<AvailablePaxSSR>();//ReturnMeal

                            for (int i = 0; i < result.Flights.Length; i++)
                            {
                                for (int j = 0; j < result.Flights[i].Length; j++)
                                {
                                    string DepartureStation = result.Flights[i][j].Origin.AirportCode;
                                    string ArrivalStation = result.Flights[i][j].Destination.AirportCode;

                                    for (int p = 0; p < ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList.Length; p++)
                                    {
                                        SSRSegment sSRSegment = ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList[p];
                                        if (sSRSegment.AvailablePaxSSRList != null && sSRSegment.AvailablePaxSSRList.Length > 0 && sSRSegment.LegKey.DepartureStation == DepartureStation && sSRSegment.LegKey.ArrivalStation == ArrivalStation)
                                        {
                                            if (i == 0)
                                            {
                                                availablePaxSSRs_OnwardBag.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.Contains("XBP"))).ToList());
                                                availablePaxSSRs_OnwardMeal.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.Contains("VGML"))).ToList());
                                                availablePaxSSRs_OnwardMeal.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.Contains("NVML"))).ToList());

                                            }
                                            else
                                            {
                                                availablePaxSSRs_ReturnBag.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.Contains("XBP"))).ToList());
                                                availablePaxSSRs_ReturnMeal.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.Contains("VGML"))).ToList());
                                                availablePaxSSRs_ReturnMeal.AddRange(sSRSegment.AvailablePaxSSRList.Where(m => (m.SSRCode.Contains("NVML"))).ToList());

                                            }
                                        }
                                    }

                                }
                            }




                            List<string> SSR_BaggageCodes = new List<string>();
                            SSR_BaggageCodes.Add("XBPA");
                            SSR_BaggageCodes.Add("XBPB");
                            SSR_BaggageCodes.Add("XBPC");
                            SSR_BaggageCodes.Add("XBPD");

                            List<string> SSR_MealCodes = new List<string>();
                            SSR_MealCodes.Add("VGML");
                            SSR_MealCodes.Add("NVML");


                            if (availablePaxSSRs_OnwardBag.Count > 0)
                            {
                                foreach (string baggageCode in SSR_BaggageCodes)
                                {
                                    List<AvailablePaxSSR> bagggeSSR = availablePaxSSRs_OnwardBag.Where(m => m.SSRCode.Contains(baggageCode)).ToList();
                                    if (bagggeSSR != null && bagggeSSR.Count > 0)
                                    {
                                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                                        for (int r = 0; r < bagggeSSR.Count; r++)
                                        {
                                            paxSSRPrices.AddRange(bagggeSSR[r].PaxSSRPriceList);
                                        }
                                        for (int k = 0; k < paxSSRPrices.Count; k++)
                                        {
                                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                                        }

                                        decimal baggagePrice = charges.Where(o => o.ChargeType == Indigo.BookingMgr.ChargeType.ServiceCharge).Sum(item => item.Amount);
                                        rateOfExchange = exchangeRates[charges[0].CurrencyCode];

                                        DataRow dr = dtSourceBaggage.NewRow();
                                        dr["Code"] = baggageCode;
                                        dr["Price"] = Math.Round(Convert.ToDecimal(baggagePrice) * rateOfExchange, agentDecimalValue);
                                        dr["Group"] = "0";
                                        dr["Currency"] = agentBaseCurrency;
                                        switch (baggageCode)
                                        {
                                            case "XBPA":
                                                dr["Description"] = "Prepaid Excess Baggage – 5 Kg";
                                                break;
                                            case "XBPB":
                                                dr["Description"] = "Prepaid Excess Baggage – 10 Kg";
                                                break;
                                            case "XBPC":
                                                dr["Description"] = "Prepaid Excess Baggage – 15 Kg";
                                                break;
                                            case "XBPD":
                                                dr["Description"] = "Prepaid Excess Baggage – 30 Kg";
                                                break;

                                        }
                                        dr["QtyAvailable"] = 5000;
                                        dtSourceBaggage.Rows.Add(dr);
                                    }

                                }
                            }

                            if (availablePaxSSRs_ReturnBag.Count > 0)
                            {
                                foreach (string baggageCode in SSR_BaggageCodes)
                                {
                                    List<AvailablePaxSSR> bagggeSSR = availablePaxSSRs_ReturnBag.Where(m => m.SSRCode.Contains(baggageCode)).ToList();
                                    if (bagggeSSR != null && bagggeSSR.Count > 0)
                                    {
                                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                                        for (int r = 0; r < bagggeSSR.Count; r++)
                                        {
                                            paxSSRPrices.AddRange(bagggeSSR[r].PaxSSRPriceList);
                                        }
                                        for (int k = 0; k < paxSSRPrices.Count; k++)
                                        {
                                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                                        }

                                        decimal baggagePrice = charges.Where(o => o.ChargeType == Indigo.BookingMgr.ChargeType.ServiceCharge).Sum(item => item.Amount);
                                        rateOfExchange = exchangeRates[charges[0].CurrencyCode];

                                        DataRow dr = dtSourceBaggage.NewRow();
                                        dr["Code"] = baggageCode;
                                        dr["Price"] = Math.Round(Convert.ToDecimal(baggagePrice) * rateOfExchange, agentDecimalValue);
                                        dr["Group"] = "1";
                                        dr["Currency"] = agentBaseCurrency;
                                        switch (baggageCode)
                                        {
                                            case "XBPA":
                                                dr["Description"] = "Prepaid Excess Baggage – 5 Kg";
                                                break;
                                            case "XBPB":
                                                dr["Description"] = "Prepaid Excess Baggage – 10 Kg";
                                                break;
                                            case "XBPC":
                                                dr["Description"] = "Prepaid Excess Baggage – 15 Kg";
                                                break;
                                            case "XBPD":
                                                dr["Description"] = "Prepaid Excess Baggage – 30 Kg";
                                                break;

                                        }
                                        dr["QtyAvailable"] = 5000;
                                        dtSourceBaggage.Rows.Add(dr);
                                    }

                                }
                            }

                            if (availablePaxSSRs_OnwardMeal.Count > 0)
                            {
                                foreach (string mealCode in SSR_MealCodes)
                                {
                                    List<AvailablePaxSSR> mealSSR = availablePaxSSRs_OnwardMeal.Where(m => m.SSRCode.Contains(mealCode)).ToList();
                                    if (mealSSR != null && mealSSR.Count > 0)
                                    {
                                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                                        for (int r = 0; r < mealSSR.Count; r++)
                                        {
                                            paxSSRPrices.AddRange(mealSSR[r].PaxSSRPriceList);
                                        }
                                        for (int k = 0; k < paxSSRPrices.Count; k++)
                                        {
                                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                                        }

                                        decimal mealPrice = charges.Where(o => o.ChargeType == Indigo.BookingMgr.ChargeType.ServiceCharge).Sum(item => item.Amount);
                                        rateOfExchange = exchangeRates[charges[0].CurrencyCode];

                                        DataRow dr = dtSourceBaggage.NewRow();
                                        dr["Code"] = mealCode;
                                        dr["Price"] = Math.Round(Convert.ToDecimal(mealPrice) * rateOfExchange, agentDecimalValue);
                                        dr["Group"] = "0";
                                        dr["Currency"] = agentBaseCurrency;
                                        switch (mealCode)
                                        {
                                            case "VGML":
                                                dr["Description"] = "Veg Meal";
                                                break;
                                            case "NVML":
                                                dr["Description"] = "Non Veg Meal";
                                                break;
                                        }
                                        dr["QtyAvailable"] = 5000;
                                        dtSourceBaggage.Rows.Add(dr);
                                    }

                                }
                            } //OnwardMeal

                            if (availablePaxSSRs_ReturnMeal.Count > 0)
                            {
                                foreach (string mealCode in SSR_MealCodes)
                                {
                                    List<AvailablePaxSSR> mealSSR = availablePaxSSRs_ReturnMeal.Where(m => m.SSRCode.Contains(mealCode)).ToList();
                                    if (mealSSR != null && mealSSR.Count > 0)
                                    {
                                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                                        for (int r = 0; r < mealSSR.Count; r++)
                                        {
                                            paxSSRPrices.AddRange(mealSSR[r].PaxSSRPriceList);
                                        }
                                        for (int k = 0; k < paxSSRPrices.Count; k++)
                                        {
                                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                                        }

                                        decimal mealPrice = charges.Where(o => o.ChargeType == Indigo.BookingMgr.ChargeType.ServiceCharge).Sum(item => item.Amount);
                                        rateOfExchange = exchangeRates[charges[0].CurrencyCode];

                                        DataRow dr = dtSourceBaggage.NewRow();
                                        dr["Code"] = mealCode;
                                        dr["Price"] = Math.Round(Convert.ToDecimal(mealPrice) * rateOfExchange, agentDecimalValue);
                                        dr["Group"] = "1";
                                        dr["Currency"] = agentBaseCurrency;
                                        switch (mealCode)
                                        {
                                            case "VGML":
                                                dr["Description"] = "Veg Meal";
                                                break;
                                            case "NVML":
                                                dr["Description"] = "Non Veg Meal";
                                                break;
                                        }
                                        dr["QtyAvailable"] = 5000;
                                        dtSourceBaggage.Rows.Add(dr);
                                    }

                                }
                            } //ReturnMeal


                        }
                    }
                }
            }
            catch(Exception ex)
            {
                //In case of exception in any of the above requests call the clear method.
                //This method will release the itinerary.
                //Clears the session.
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {
                    ClearRequest clearRequest = new ClearRequest();
                    clearRequest.ContractVersion = contractVersion;
                    clearRequest.EnableExceptionStackTrace = false;
                    clearRequest.Signature = logonResponse.Signature;
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearRequest));
                        string filePath = xmlLogPath + "Indigo_ClearRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearRequest);
                        sw.Close();
                    }
                    catch { }
                    ClearResponse clearResponse = bookingAPI.Clear(clearRequest);
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearResponse));
                        string filePath = xmlLogPath + "Indigo_ClearResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearResponse);
                        sw.Close();
                    }
                    catch { }
                }
                throw ex;
            }
            return dtSourceBaggage;
        }

        /// <summary>
        /// This method is used to perform SellRequest to get additional baggage.
        /// </summary>
        /// <param name="signature"></param>
        /// <param name="searchResult"></param>
        /// <returns></returns>
        private BookingUpdateResponseData ExecuteSell(string signature, SearchResult searchResult)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                FlightItinerary itinerary = new FlightItinerary();
                itinerary.Segments = SearchResult.GetSegments(searchResult);
                itinerary.TicketAdvisory = searchResult.FareSellKey;

                SellRequestData requestData = new SellRequestData();
                SellRequest request = new SellRequest();

                request.ContractVersion = contractVersion;
                request.Signature = signature;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                request.EnableExceptionStackTrace = false;
                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = currencyCode;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;

                //Define SourcePOS data
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS = new PointOfSale();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.AgentCode = "AG";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.DomainCode = agentDomain;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.OrganizationCode = agentId;

                string onwardFareSellKey = string.Empty;
                string onwardJourneySellKey = string.Empty;
                string returnFareSellKey = string.Empty;
                string returnJourneySellKey = string.Empty;

                List<string> sellKeysListOnward = new List<string>();
                List<string> sellKeysListReturn = new List<string>();
                for (int p = 0; p < itinerary.Segments.Length; p++)
                {
                    if (itinerary.Segments[p].Group == 0)//Onward
                    {
                        sellKeysListOnward.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                    if (itinerary.Segments[p].Group == 1)//Return
                    {
                        sellKeysListReturn.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                }
                IEnumerable<string> distinctSellKeysOnward = sellKeysListOnward.Distinct();
                IEnumerable<string> distinctSellKeysReturn = sellKeysListReturn.Distinct();

                if (distinctSellKeysOnward.Count() > 0)
                {
                    foreach (string key in distinctSellKeysOnward)
                    {
                        if (onwardJourneySellKey.Length > 0)
                        {
                            onwardJourneySellKey += "^" + key;
                        }
                        else
                        {
                            onwardJourneySellKey = key;
                        }
                    }
                }

                if (distinctSellKeysReturn.Count() > 0)
                {
                    foreach (string key in distinctSellKeysReturn)
                    {
                        if (returnJourneySellKey.Length > 0)
                        {
                            returnJourneySellKey += "^" + key;
                        }
                        else
                        {
                            returnJourneySellKey = key;
                        }
                    }
                }


                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[1];
                if (itinerary.TicketAdvisory.Split('|').Length == 2)
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[2];
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = onwardJourneySellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = itinerary.TicketAdvisory.Split('|')[0];
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";

                if (itinerary.TicketAdvisory.Split('|').Length == 2)//Round trip
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].JourneySellKey = returnJourneySellKey;
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].FareSellKey = itinerary.TicketAdvisory.Split('|')[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].StandbyPriorityCode = "";
                }
                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < searchResult.FareBreakdown.Length; i++)
                {
                    if (searchResult.FareBreakdown[i] != null && searchResult.FareBreakdown[i].PassengerType != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        for (int h = 0; h < searchResult.FareBreakdown[i].PassengerCount; h++)
                        {
                            switch (searchResult.FareBreakdown[i].PassengerType)
                            {
                                case PassengerType.Adult:
                                    priceType.PaxType = "ADT";
                                    break;
                                case PassengerType.Child:
                                    priceType.PaxType = "CHD";
                                    break;

                            }
                            paxPriceTypes.Add(priceType);
                        }
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                request.SellRequestData = requestData;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + "IndigoSellRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch { }

                SellResponse sellResponse_v_4_5 = bookingAPI.Sell(request);
                responseData = sellResponse_v_4_5.BookingUpdateResponseData;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellResponse));
                    string filePath = xmlLogPath + "IndigoSellResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellResponse_v_4_5);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to Book. Reason : " + ex.ToString(), "");
                throw ex;
            }

            return responseData;
        }


    }
}
