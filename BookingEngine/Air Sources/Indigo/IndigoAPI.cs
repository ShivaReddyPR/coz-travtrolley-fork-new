﻿#region NameSpace

using CT.Configuration;
using CT.Core;
using Indigo.BookingMgr; //Reference to Indigo BookingManager WebService
using Indigo.ContentMgr; //Reference to Indigo ContentManager WebService
using Indigo.SessionMgr; //Reference to Indigo SessionManager WebService
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading; //For One-one search 
using System.Xml;
using System.Xml.Serialization;

#endregion

namespace CT.BookingEngine.GDS
{
    [Serializable]
    public class IndigoAPI : IDisposable
    {
        #region IndigoAPI member variables
        // AutoResetEvent[] rteventFlag = new AutoResetEvent[0];
        string xmlLogPath;
        string sSeatAvailResponseXSLT;
        string agentId;
        string agentDomain;
        string password;
        string organisationCode;
        int contractVersion;
        string agentBaseCurrency;
        int agentDecimalValue;
        decimal rateOfExchange;
        int appUserId;
        const int sessionTimeOut = 10;//Indigo session time out (10 minutes)

        DataTable dtBaggage = new DataTable(); //baggage datatable which holds the complete baggage information

        //Added for one-one search request
        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        List<SearchRequest> searchRequests = new List<SearchRequest>();

        Dictionary<string, decimal> exchangeRates;

        string loginSignature = "";//Login response signature.

        //Indigio Web Service Calls.
        BookingManagerClient bookingAPI = new BookingManagerClient();
        SessionManagerClient sessionAPI = new SessionManagerClient();
        ContentManagerClient contentAPI = new ContentManagerClient();

        //Search variables
        SearchRequest request = new SearchRequest();
        //AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        //AutoResetEvent[] priceEventFlag = new AutoResetEvent[0];

        List<SearchResult> CombinedSearchResults = new List<SearchResult>();//List which holds the flight results for both one way and return type.

        Hashtable pricedItineraries = new Hashtable();


        int oneWayFareBreakDowncount = 0; //One way fare break down.
        List<SearchResult> oneWayResultList = new List<SearchResult>();//Onward journey Flight Results List
        List<Indigo.BookingMgr.Fare> faresList = new List<Indigo.BookingMgr.Fare>();

        //Round Trip Journeys Price Itenarary variables.

        List<Journey> OnwardJourneys = new List<Journey>();
        List<Journey> ReturnJourneys = new List<Journey>();

        List<Indigo.BookingMgr.Fare> roundTripOnwardFaresList = new List<Indigo.BookingMgr.Fare>();
        List<Indigo.BookingMgr.Fare> roundTripReturnFaresList = new List<Indigo.BookingMgr.Fare>();

        LogonResponse logonResponse = new LogonResponse();
        List<SearchResult> roundTripResultList = new List<SearchResult>();
        string currencyCode;//For India GST implementation the currency code should be in INR format.

        //Added for GST flow
        //Capture the GST inputs from the agentmaster and location master.
        string gstNumber;
        string gstCompanyName;
        string gstOfficialEmail;

        //Added for corporate booking flow
        string promoCode;

        string bookingSourceFlag;

        string sessionId;

        bool searchByAvailability; // this property used to identify the one - one search to get the Available SSR's // Added By suresh 

        //Taxbreakup variables declaration For Special Round Trip
        List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
        List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
        List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
        List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
        bool oneWayCombinationSearch = false;//If one way then no need to reset the event.

        #endregion
        //Baggage & Meal Codes 
        List<string> baggageCodes = new List<string>() { "XBPA", "XBPB", "XBPC", "XBPD", "XBPE", "IXBA", "IXBB", "IXBC" };
        List<string> mealCodes = new List<string>() { "VGML", "NVML", "TCSW", "PTSW", "CJSW", "CTSW", "POHA", "UPMA", "SACH", "VBIR", "DACH", "CHFR", "CHCH", "MASP", "SMAL" };


        #region Properties

        public string LoginName
        {
            get { return agentId; }
            set { agentId = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string AgentDomain
        {
            get { return agentDomain; }
            set { agentDomain = value; }
        }

        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public string AgentBaseCurrency
        {
            get { return agentBaseCurrency; }
            set { agentBaseCurrency = value; }
        }

        public int AgentDecimalValue
        {
            get { return agentDecimalValue; }
            set { agentDecimalValue = value; }
        }

        public int AppUserId
        {
            get { return appUserId; }
            set { appUserId = value; }
        }

        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }

        public string GSTNumber
        {
            get { return gstNumber; }
            set { gstNumber = value; }
        }
        public string GSTCompanyName
        {
            get { return gstCompanyName; }
            set { gstCompanyName = value; }
        }
        public string GSTOfficialEmail
        {
            get { return gstOfficialEmail; }
            set { gstOfficialEmail = value; }
        }
        public string PromoCode
        {
            get { return promoCode; }
            set { promoCode = value; }
        }
        public string BookingSourceFlag
        {
            get { return bookingSourceFlag; }
            set { bookingSourceFlag = value; }
        }
        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }

        public bool SearchByAvailability
        {
            get { return searchByAvailability; }
            set { searchByAvailability = value; }
        }

        /// <summary>
        /// for sector list added by bangar
        /// </summary>
        public List<SectorList> Sectors { get; set; }

        #endregion

        /******====================Start:Promotion Code Implementation Steps*********************/
        /* Kindly find below API calls and methods where PromotionCode is applied:

 ·         Getavailability ->  PromotionCode :- Kindly enter Promocode in tag namely “PromotionCode”.
 ·         GetItineraryPrice -> ItineraryPriceRequest -> TypeOfSale -> PromotionCode : Kindly enter Promocode in tag namely “PromotionCode”.
 ·         GetItineraryPrice -> ItineraryPriceRequest -> TypeOfSale -> FareTypes : Kindly enter FareType as “F” for corporate.
 ·         Sell -> SellJourneyByKeyRequest -> TypeOfSale -> PromotionCode : Kindly enter Promocode in tag namely “PromotionCode”.
 ·         Sell -> SellJourneyByKeyRequest -> TypeOfSale -> FareTypes : Kindly enter FareType as “F” for corporate.
           Same could you identify in Getavailability Response -> Fares -> PaxFares -> ServiceCharges -> BookingServiceCharge -> TicketCode */

        /******====================End:Promotion Code********************************************/

        #region Default Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public IndigoAPI()
        {
            xmlLogPath = ConfigurationSystem.IndigoConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMMyyyy") + "\\";
            sSeatAvailResponseXSLT = ConfigurationSystem.XslconfigPath + "\\NavitaireSeatMap.xslt";
            if (!Directory.Exists(xmlLogPath))
            {
                Directory.CreateDirectory(xmlLogPath);
            }

            //agentId = ConfigurationSystem.IndigoConfig["AgentId"];
            //agentDomain = ConfigurationSystem.IndigoConfig["AgentDomain"];
            //password = ConfigurationSystem.IndigoConfig["Password"];
            //organisationCode = ConfigurationSystem.IndigoConfig["OrganisationCode"];
            contractVersion = Convert.ToInt32(ConfigurationSystem.IndigoConfig["ContractVersion"]);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;//Added by Lokesh on 14/03/2018.

            if (dtBaggage.Columns.Count == 0)
            {
                dtBaggage.Columns.Add("Code", typeof(string));
                dtBaggage.Columns.Add("Price", typeof(decimal));
                dtBaggage.Columns.Add("Group", typeof(int));
                dtBaggage.Columns.Add("Currency", typeof(string));
                dtBaggage.Columns.Add("Description", typeof(string));
                dtBaggage.Columns.Add("QtyAvailable", typeof(int));
            }
            //Assign the web service url from source xml config

            bookingAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.IndigoConfig["BookingService"]);
            sessionAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.IndigoConfig["SessionService"]);
            contentAPI.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationSystem.IndigoConfig["ContentService"]);
            //To retrive the fare rules used the below properties
            if (contentAPI.Endpoint.Binding is System.ServiceModel.BasicHttpBinding)
            {
                ((System.ServiceModel.BasicHttpBinding)contentAPI.Endpoint.Binding).MaxReceivedMessageSize = int.MaxValue;
                ((System.ServiceModel.BasicHttpBinding)contentAPI.Endpoint.Binding).MaxBufferSize = int.MaxValue;
            }
        }
        #endregion

        #region Session Methods
        /// <summary>
        /// Authenticates and creates a session for the specified user.
        /// </summary>
        /// <returns></returns>
        private LogonResponse Login()
        {
            try
            {
                LogonRequest request = new LogonRequest();
                request.logonRequestData = new LogonRequestData();
                request.logonRequestData.AgentName = agentId;
                request.logonRequestData.DomainCode = agentDomain;
                request.logonRequestData.LocationCode = "";
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.Password = password;
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.TerminalInfo = "";
                request.ContractVersion = contractVersion;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                request.EnableExceptionStackTrace = false;

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(LogonRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoLogonRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch { }
                logonResponse = sessionAPI.Logon(request);
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(LogonResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoLogonResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, logonResponse);
                    sw.Close();
                }
                catch { }


            }
            catch (Exception ex)
            {
                throw new Exception("Indigo Login failed. Reason : " + ex.Message, ex);
            }

            return logonResponse;
        }
        /// <summary>
        /// Ends a user’s session and clears session from the server.
        /// </summary>
        /// <param name="signature"></param>
        /// <returns></returns>
        private LogoutResponse Logout(string signature)
        {
            LogoutResponse response = new LogoutResponse();

            try
            {
                LogoutRequest request = new LogoutRequest();
                request.Signature = signature;
                request.ContractVersion = contractVersion;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                request.EnableExceptionStackTrace = false;

                XmlSerializer ser = new XmlSerializer(typeof(LogoutRequest));
                string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoLogoutRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                StreamWriter sw = new StreamWriter(filePath);
                ser.Serialize(sw, request);
                sw.Close();
                //Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo Logout Request Sent Time : " + DateTime.Now, "");
                response = sessionAPI.Logout(request);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo Failed to log out  : " + appUserId.ToString() + " " + DateTime.Now + "-" + ex.ToString(), "");
            }
            finally
            {
                Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo Clearing Session For the User : " + appUserId.ToString() + " " + DateTime.Now, "");

            }

            return response;
        }

        #endregion

        #region Search Methods

        /// <summary>
        ///This method returns a boolean which determine whether to send a  Search Request to get the flight inventory based on the sector list that is provided by Indigo.
        /// </summary>
        /// <param name="searchRequest"></param>
        /// <returns></returns>
        public bool AllowSearch(SearchRequest searchRequest)
        {
            bool allowSearch = false;
            try
            {
                //START:PERFORM SEARCH BASED ON THE NEAR BY AIRPORT
                List<SectorList> locSectorlist = new List<SectorList>();
                locSectorlist = Sectors.FindAll(x => x.Supplier == "IndigoCityMapping");

                // if (ConfigurationSystem.SectorListConfig.ContainsKey("IndigoCityMapping"))
                if (locSectorlist.Count() > 0)
                {
                    // Dictionary<string, string> sectorsNearBy = ConfigurationSystem.SectorListConfig["IndigoCityMapping"];
                    Dictionary<string, string> sectorsNearBy = locSectorlist.ToDictionary(r => r.Origin, r => r.Sectors);
                    if (sectorsNearBy.ContainsKey(searchRequest.Segments[0].Destination))
                    {
                        searchRequest.Segments[0].Destination = sectorsNearBy[searchRequest.Segments[0].Destination];
                    }
                    if (sectorsNearBy.ContainsKey(searchRequest.Segments[0].Origin) && searchRequest.Segments[0].NearByOriginPort)
                    {
                        searchRequest.Segments[0].Origin = sectorsNearBy[searchRequest.Segments[0].Origin];
                    }
                }
                //END:PERFORM SEARCH BASED ON THE NEAR BY AIRPORT

                locSectorlist = Sectors.FindAll(x => x.Supplier == "Indigo").ToList();

                //if (ConfigurationSystem.SectorListConfig.ContainsKey("Indigo"))
                if (locSectorlist.Count() > 0)
                {
                    // Dictionary<string, string> sectors = ConfigurationSystem.SectorListConfig["Indigo"];
                    Dictionary<string, string> sectors = locSectorlist.ToDictionary(r => r.Origin, r => r.Sectors);
                    if (sectors.ContainsKey(searchRequest.Segments[0].Origin))
                    {
                        if (sectors[searchRequest.Segments[0].Origin].Contains(searchRequest.Segments[0].Destination))
                        {
                            allowSearch = true;
                        }
                    }
                }
                else
                {
                    Core.Audit.Add(Core.EventType.Book, Core.Severity.High, appUserId, "Indigo: Error While reading sector from SectorListConfig : Error" + DateTime.Now, "");
                }
            }
            catch (Exception ex)
            {
                Core.Audit.Add(Core.EventType.Book, Core.Severity.High, appUserId, "Indigo: Error While reading sector from SectorListConfig : Error" + ex.ToString() + DateTime.Now, "");
            }
            return allowSearch;
        }


        /// <summary>
        /// Gets flight availability and fare information for both oneway and return trips
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchResult[] Search(SearchRequest request)
        {
            /*  The following steps describes the complete flow for Indigo(6E) Source.
             * 01.Logon Request -- Logon Response (For authentication and for signature)
             * 02. GetAvailability Request --GetAvailability Response (Flight Availability Details)
             * 03. PriceItinerary Request -- PriceItinerary Response(Returns the fare break down for each itenary).
             * 04. Sell
             * 05. UpdatePassengers
             * 06. AddPaymenttoBooking
             * 07. BookingCommit
             * 08: LogOut Request
             */

            SearchResult[] results = new SearchResult[0];
            List<SearchResult> ResultList = new List<SearchResult>();
            LogonResponse loginResponse = null;
            try
            {
                if (AllowSearch(request))//Allow search only if the requested origin and destination is avaialble in the sectorlist provided by Indigo.
                {
                    //Authenticates and creates a session for the specified user
                    if (!request.SearchBySegments)
                    {
                        loginResponse = Login();//First time gets the Signature from the API.
                    }
                    if ((loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature) && loginResponse.Signature.Length > 0) || request.SearchBySegments)
                    {
                        #region After successful authentication call the necessary Indigo methods to get the results.

                        if ((loginResponse != null && loginResponse.Signature.Length > 0) || request.SearchBySegments)
                        {
                            TripAvailabilityResponse tripAvailResponse = null;
                            this.request = request;
                            if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature))
                            {
                                loginSignature = loginResponse.Signature;
                            }

                            if (!request.SearchBySegments && request.Type == SearchType.Return)
                            {
                                SearchForRetailFare();
                            }
                            //START ==========Combination Search ========== RETURN
                            else if (request.SearchBySegments && request.Type == SearchType.Return) // One to one search logic
                            {

                                //Step 1: spilt the request into two onward requests
                                //Step 2: Fomr Two thread Requests and call the api methods .
                                Dictionary<string, int> readySources = new Dictionary<string, int>();
                                Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();

                                listOfThreads.Add("IG1", new WaitCallback(SearchRoutingReturn));
                                listOfThreads.Add("IG2", new WaitCallback(SearchRoutingReturn));

                                SearchRequest onwardRequest = request.Copy();
                                onwardRequest.Type = SearchType.OneWay;
                                onwardRequest.Segments = new FlightSegment[1];
                                onwardRequest.Segments[0] = request.Segments[0];

                                //SearchRoutingReturn(onwardRequest);


                                SearchRequest returnRequest = request.Copy();
                                returnRequest.Type = SearchType.Return;
                                returnRequest.Segments = new FlightSegment[1];
                                returnRequest.Segments[0] = request.Segments[1];

                                //SearchRoutingReturn(returnRequest);

                                searchRequests.Add(onwardRequest);
                                searchRequests.Add(returnRequest);

                                eventFlag = new AutoResetEvent[2];
                                readySources.Add("IG1", 1200);
                                readySources.Add("IG2", 1200);


                                int j = 0;
                                //Start each fare search within a Thread which will automatically terminate after the results are received.
                                foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                                {
                                    if (readySources.ContainsKey(deThread.Key))
                                    {
                                        ThreadPool.QueueUserWorkItem(deThread.Value, j);
                                        eventFlag[j] = new AutoResetEvent(false);
                                        j++;
                                    }
                                }

                                if (j != 0)
                                {
                                    if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 2, 0), true))
                                    {
                                        //TODO: audit which thread is timed out                
                                    }
                                }

                                //Additional Request to call round trip fares in combination search
                                //For Special Round Trip Fares Always The Infant Count Should Be Zero.
                                if (request.InfantCount <= 0 && bookingSourceFlag == "6E")
                                {//ONLY FOR 6E Only ...//FOR CORP THERE WILL BE NO SPECIAL ROUND TRIP FARES                               
                                    SearchForRetailFare();
                                }
                            }
                            //END ==========Combination Search ========== RETURN

                            //==========START:Combination Search ==========ONE WAY============
                            else if (request.SearchBySegments && request.Type == SearchType.OneWay)
                            {
                                SearchRequest onwardRequest = request.Copy();
                                onwardRequest.Type = SearchType.OneWay;
                                onwardRequest.Segments = new FlightSegment[1];
                                onwardRequest.Segments[0] = request.Segments[0];
                                searchRequests.Add(onwardRequest);
                                oneWayCombinationSearch = true;
                                SearchRoutingReturn(0);

                            }
                            //==========END:Combination Search ==========ONE WAY============

                            //As the search is of one way type we will call all fares
                            else if (request.Type == SearchType.OneWay)
                            {
                                try
                                {
                                    #region One Way Search Results

                                    #region GetAvailabilityRequest Object
                                    //S-1:Create object for GetAvailabilityRequest
                                    GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                                    GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                                    //Assign Signature generated from Session
                                    availRequest.Signature = loginResponse.Signature;
                                    availRequest.ContractVersion = contractVersion;
                                    //Added by Lokesh on 11-June-2018
                                    //New skies Version 4.5
                                    //Need to send EnableExceptionStackTrace = false(by default) in the request.
                                    availRequest.EnableExceptionStackTrace = false;
                                    availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();

                                    if (request.Type == SearchType.OneWay)
                                    {
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                                    }

                                    //Assign all Availability details to get flight details
                                    for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                                    {
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = Indigo.BookingMgr.FlightType.All;
                                        if (!string.IsNullOrEmpty(promoCode))
                                        {
                                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                                        }
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "6E";
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;

                                        //For connecting flights 
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 10;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = Indigo.BookingMgr.AvailabilityType.Default;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;

                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;

                                        //Kindly enter value as ‘R’,  ‘Z’ and ‘M’ in tag namely FareTypes in GetAvailability API call and set the value as ‘CompressByProductClass for the tag namely FareClassControl  and set ProductClasses tag to Null or enter all the available ProductClasses as mentioned below to get the lowest fare among the all ProductClasses.
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R"; //Retail Fare
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "Z"; //Lite Fare
                                        
                                        //6E FARE TYPES : Retail / Return Trip,Sale Fare,Family Fare,Special Round Trip,Flexi Fare,Lite Fare,SME Fare
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "F"; //SME FARE

                                        //FOR CORPORATE NEED TO SEND ONLY THE CORPORATE FARE TYPE
                                        if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag == "6ECORP")
                                        {
                                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[1];
                                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "F"; //CORPORATE FARE
                                        }

                                        int paxTypeCount = 1;
                                        if (request.ChildCount > 0)
                                        {
                                            paxTypeCount++;
                                        }
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(request.AdultCount + request.ChildCount);
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxTypeCount];


                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[0] = new PaxPriceType();
                                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[0].PaxType = "ADT";//Adult Passenger

                                        if (request.ChildCount > 0)
                                        {
                                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[1] = new PaxPriceType();
                                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[1].PaxType = "CHD"; //Child Passenger
                                        }

                                    }
                                    #endregion

                                    #region Indigo One Way SearchRequest and Search Response Logs
                                    try
                                    {


                                        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoSearchRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                                        StreamWriter sw = new StreamWriter(filePath);
                                        ser.Serialize(sw, availRequest);
                                        sw.Close();

                                    }
                                    catch (Exception ex)
                                    {
                                        Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                                    }

                                    //S-2:GetAvailability: -Gets flight availability and fare information.
                                    //Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo One Way search request sent: " + DateTime.Now, "");
                                    GetAvailabilityResponse availResponse_v_4_5 = bookingAPI.GetAvailability(availRequest);
                                    tripAvailResponse = availResponse_v_4_5.GetTripAvailabilityResponse;


                                    try
                                    {
                                        //The default nullable types are not serialized so during xml serialization the default nullable types should be passed for serializing the object.
                                        Type[] nullableTypes = new Type[] { typeof(Indigo.BookingMgr.AvailableFare) };
                                        XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityResponse), nullableTypes);
                                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoSearchResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                                        StreamWriter sw = new StreamWriter(filePath);
                                        ser.Serialize(sw, availResponse_v_4_5);
                                        sw.Close();
                                    }
                                    catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }

                                    #endregion

                                    if (tripAvailResponse != null && request.Type == SearchType.OneWay)
                                    {
                                        List<Journey> OnwardJourneys = new List<Journey>();

                                        /*(tripAvailResponse.Schedules) is a Jagged Array(Array of Arrays) */
                                        //all flights to nearby airports as well such as Sharjah (SHJ), Dubai World Center airport (DWC) along with DXB(Dubai).
                                        if (tripAvailResponse != null && tripAvailResponse.Schedules.Length > 0)
                                        {
                                            //Schedules --2 Dimensional Array
                                            //Schedule --0 --Always onward journeys
                                            //Schedule --1 --Always return journeys
                                            for (int j = 0; j < tripAvailResponse.Schedules[0].Length; j++)
                                            {
                                                if (tripAvailResponse.Schedules[0][j].Journeys.Length > 0)
                                                {
                                                    OnwardJourneys.AddRange(tripAvailResponse.Schedules[0][j].Journeys);
                                                }
                                            }
                                        }

                                        if (OnwardJourneys.Count > 0)
                                        {
                                            int fareBreakDownCount = 0;

                                            if (request.AdultCount > 0)
                                            {
                                                fareBreakDownCount = 1;
                                            }
                                            if (request.ChildCount > 0)
                                            {
                                                fareBreakDownCount++;
                                            }
                                            if (request.InfantCount > 0)
                                            {
                                                fareBreakDownCount++;
                                            }
                                            oneWayFareBreakDowncount = fareBreakDownCount;
                                            for (int i = 0; i < OnwardJourneys.Count; i++)
                                            {
                                                if (OnwardJourneys[i].Segments.Length > 1)
                                                {
                                                    GetConnectingFlightsResultsForOneWay(OnwardJourneys[i], fareBreakDownCount, loginResponse.Signature);
                                                }
                                                else
                                                {

                                                    for (int j = 0; j < OnwardJourneys[i].Segments[0].Fares.Length; j++)
                                                    {
                                                        GenerateFlightResultForOneWayDirectFlights(OnwardJourneys[i], OnwardJourneys[i].Segments[0].Fares[j], loginResponse.Signature);
                                                    }

                                                }
                                            }
                                            CombinedSearchResults.AddRange(oneWayResultList);
                                        }
                                    }
                                    #endregion
                                }
                                catch (Exception ex)
                                {

                                    Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to get results for One-way search. Reason : " + ex.ToString(), "");
                                }
                            }
                            results = CombinedSearchResults.ToArray();
                        }
                        #endregion
                    }
                    else
                    {
                        throw new Exception("(Indigo)Failed to get the Login Response");
                    }

                }
                else
                {
                    throw new Exception("Indigo Search failed.Reason:Requested origin and destination not available");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Indigo Search failed. Reason : " + ex.ToString(), ex);
            }

            Array.Sort(results, delegate (SearchResult h1, SearchResult h2) { return h1.TotalFare.CompareTo(h2.TotalFare); });

            
            if (!request.SearchBySegments && results.Length > 0 && request.Type == SearchType.Return) //for Normal search
            {
                //New Logic Combinations Suggested by Indigo:24July,2018
                //In case of RoundTrip Journey the following combinations only allowed
                //1. Flexi fare can be combined with Flexi fare only.
                //2.Corporate fare can be combined with Corporate Fares.
                //3.SME fare can be combined with SME Fares.
                //4.Lite fare can be combined with Lite Fares.
                //5.Sale,RoundTrip,Retail,SpecialRoundTripCan Perform combinations.

                List<SearchResult> filteredResults = new List<SearchResult>();
                if (results.Length > 0)
                {
                    for (int i = 0; i < results.Length; i++)
                    {
                        if (results[i].FareType.Split(',')[0].ToLower().Contains("flexi") == false && results[i].FareType.Split(',')[1].ToLower().Contains("flexi") == false)
                        {
                            if (results[i].FareType.Split(',')[0].ToLower().Contains("lite") == false && results[i].FareType.Split(',')[1].ToLower().Contains("lite") == false)
                            {
                                if (results[i].FareType.Split(',')[0].ToLower().Contains("corporate") == false && results[i].FareType.Split(',')[1].ToLower().Contains("corporate") == false)
                                {
                                    if (results[i].FareType.Split(',')[0].ToLower().Contains("sme") == false && results[i].FareType.Split(',')[1].ToLower().Contains("sme") == false)
                                    {
                                        filteredResults.Add(results[i]);
                                    }
                                    else if (results[i].FareType.Split(',')[0].ToLower().Contains("sme") == true && results[i].FareType.Split(',')[1].ToLower().Contains("sme") == true)
                                    {
                                        filteredResults.Add(results[i]);
                                    }
                                }
                                else if (results[i].FareType.Split(',')[0].ToLower().Contains("corporate") == true && results[i].FareType.Split(',')[1].ToLower().Contains("corporate") == true)
                                {
                                    filteredResults.Add(results[i]);
                                }
                            }
                            else if (results[i].FareType.Split(',')[0].ToLower().Contains("lite") == true && results[i].FareType.Split(',')[1].ToLower().Contains("lite") == true)
                            {
                                filteredResults.Add(results[i]);
                            }
                        }
                        else if (results[i].FareType.Split(',')[0].ToLower().Contains("flexi") == true && results[i].FareType.Split(',')[1].ToLower().Contains("flexi") == true)
                        {
                            filteredResults.Add(results[i]);

                        }
                    }
                }
                if (filteredResults.Count > 0)
                {
                    results = filteredResults.ToArray();
                }
            }
            return results;
        }

        #endregion

        #region OneWay Flight Result Creation

        /// <summary>
        /// Generate Flight Result For OneWay if the flights are direct flights.
        /// </summary>
        /// <param name="OnwardJourney"></param>
        /// <param name="OnwardFare"></param>
        private void GenerateFlightResultForOneWayDirectFlights(Journey OnwardJourney, Indigo.BookingMgr.Fare OnwardFare, string signature)
        {
            try
            {
                //Here it is a direct flight--So there will be only one segment.
                if (OnwardJourney != null && OnwardFare != null && OnwardJourney.Segments != null && OnwardJourney.Segments.Length > 0 && OnwardJourney.Segments[0] != null)
                {

                    SearchResult result = new SearchResult();
                    result.IsLCC = true;
                    if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "6E")
                    {
                        result.ResultBookingSource = BookingSource.Indigo;
                    }
                    else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "6ECORP")
                    {
                        result.ResultBookingSource = BookingSource.IndigoCorp;
                    }
                    result.Airline = "6E";
                    result.Currency = agentBaseCurrency;
                    result.EticketEligible = true;
                    result.FareBreakdown = new Fare[oneWayFareBreakDowncount];
                    result.FareRules = new List<FareRule>();
                    FareRule fareRule = new FareRule();
                    fareRule.Airline = result.Airline;
                    fareRule.Destination = request.Segments[0].Destination;
                    fareRule.FareBasisCode = OnwardFare.FareBasisCode;
                    fareRule.FareInfoRef = OnwardFare.RuleNumber;
                    fareRule.Origin = request.Segments[0].Origin;
                    fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                    fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                    result.FareRules.Add(fareRule);
                    result.JourneySellKey = OnwardJourney.JourneySellKey;
                    result.FareType = GetFareTypeForProductClass(OnwardFare.ProductClass);
                    result.FareSellKey = OnwardFare.FareSellKey;
                    result.NonRefundable = false;
                    result.RepriceErrorMessage = signature;


                    result.Flights = new FlightInfo[1][];

                    List<FlightInfo> directFlights = GetFlightInfo(OnwardJourney.Segments[0], OnwardJourney, OnwardFare.ProductClass, "ONWARD");

                    if (directFlights != null && directFlights.Count > 0)
                    {
                        List<FlightInfo> listOfFlights = new List<FlightInfo>();
                        listOfFlights.AddRange(directFlights);
                        result.Flights[0] = listOfFlights.ToArray();
                    }
                    //List which holds the Pax types  : "ADT","CHD" ,"INFT";
                    List<string> FarePaxtypes = new List<string>();
                    if (request.AdultCount > 0)
                    {
                        FarePaxtypes.Add("ADT");
                    }
                    if (request.ChildCount > 0)
                    {
                        FarePaxtypes.Add("CHD");
                    }
                    if (request.InfantCount > 0)
                    {
                        FarePaxtypes.Add("INFT");
                    }
                    //Intially we will calculate the base fare for the result from the search response.
                    CalculateBaseFareForOneWaydirectFlights(ref result, FarePaxtypes, OnwardJourney, OnwardFare);
                    if (result.FareType == "SPECIAL ROUND TRIP FARE")
                    {
                        result.IsSpecialRoundTrip = true;
                    }
                    oneWayResultList.Add(result);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to get Retail Fare results." + ex.ToString(), "");

            }
        }


        /// <summary>
        /// Gets connecting flight results for onward journeys only.
        /// </summary>
        /// <param name="journeyOnWard"></param>
        /// <param name="fareBreakDownCount"></param>
        private void GetConnectingFlightsResultsForOneWay(Journey journeyOnWard, int fareBreakDownCount, string signature)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {
                if (journeyOnWard != null && journeyOnWard.Segments != null && journeyOnWard.Segments.Length > 0 && journeyOnWard.Segments.Length == 2)
                {
                    /*Fare mapping on segments based on One to One relationship. 
                     *For ex. combine 1st Fare of the 1st Segment with the 1st Fare of the 2nd Segment, 
                     *The 2nd Fare of the 1st Segment with the 2nd Fare of the 2nd Segment and so on (if there are more fares under each Segment).             
                     */
                    Segment firstSegmentCon = null;
                    Segment secondSegmentCon = null;
                    if (journeyOnWard.Segments[0] != null && journeyOnWard.Segments[1] != null)
                    {
                        firstSegmentCon = journeyOnWard.Segments[0];
                        secondSegmentCon = journeyOnWard.Segments[1];
                    }
                    if (firstSegmentCon != null && secondSegmentCon != null && firstSegmentCon.Fares != null && secondSegmentCon.Fares != null &&
                        firstSegmentCon.Fares.Length > 0 && secondSegmentCon.Fares.Length > 0 && (firstSegmentCon.Fares.Length == secondSegmentCon.Fares.Length))
                    {
                        for (int f = 0; f < firstSegmentCon.Fares.Length; f++)
                        {
                            if (firstSegmentCon.Fares[f] != null && secondSegmentCon.Fares[f] != null)
                            {
                                Indigo.BookingMgr.Fare fareFirstConSeg = firstSegmentCon.Fares[f];
                                Indigo.BookingMgr.Fare fareSecondConSeg = secondSegmentCon.Fares[f];

                                SearchResult result = new SearchResult();
                                result.IsLCC = true;
                                if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "6E")
                                {
                                    result.ResultBookingSource = BookingSource.Indigo;
                                }
                                else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "6ECORP")
                                {
                                    result.ResultBookingSource = BookingSource.IndigoCorp;
                                }
                                result.Airline = "6E";
                                result.Currency = agentBaseCurrency;
                                result.EticketEligible = true;
                                result.FareBreakdown = new Fare[fareBreakDownCount];
                                result.FareRules = new List<FareRule>();

                                FareRule fareRule = new FareRule();
                                fareRule.Airline = result.Airline;
                                fareRule.Destination = request.Segments[0].Destination;
                                fareRule.FareBasisCode = fareFirstConSeg.FareBasisCode;
                                fareRule.FareInfoRef = fareFirstConSeg.RuleNumber;
                                fareRule.Origin = request.Segments[0].Origin;
                                fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                result.FareRules.Add(fareRule);

                                result.JourneySellKey = journeyOnWard.JourneySellKey;
                                result.FareType = GetFareTypeForProductClass(fareFirstConSeg.ProductClass);
                                result.FareSellKey = fareFirstConSeg.FareSellKey + "^" + fareSecondConSeg.FareSellKey;
                                result.NonRefundable = false;
                                if (!string.IsNullOrEmpty(signature))
                                {
                                    result.RepriceErrorMessage = signature;
                                }

                                result.Flights = new FlightInfo[1][];

                                List<FlightInfo> firstSegmentConFlights = GetFlightInfo(firstSegmentCon, journeyOnWard, fareFirstConSeg.ProductClass, "ONWARD");
                                List<FlightInfo> secondSegmentConFlights = GetFlightInfo(secondSegmentCon, journeyOnWard, fareSecondConSeg.ProductClass, "ONWARD");
                                if (firstSegmentConFlights != null && secondSegmentConFlights != null && firstSegmentConFlights.Count > 0 && secondSegmentConFlights.Count > 0)
                                {
                                    List<FlightInfo> listOfFlights = new List<FlightInfo>();
                                    listOfFlights.AddRange(firstSegmentConFlights);
                                    listOfFlights.AddRange(secondSegmentConFlights);
                                    result.Flights[0] = listOfFlights.ToArray();
                                }
                                //List which holds the Pax types  : "ADT","CHD" ,"INFT";
                                List<string> FarePaxtypes = new List<string>();
                                if (request.AdultCount > 0)
                                {
                                    FarePaxtypes.Add("ADT");
                                }
                                if (request.ChildCount > 0)
                                {
                                    FarePaxtypes.Add("CHD");
                                }
                                if (request.InfantCount > 0)
                                {
                                    FarePaxtypes.Add("INFT");
                                }
				if (result.FareType == "SPECIAL ROUND TRIP FARE")
                                {
                                    result.IsSpecialRoundTrip = true;
				}
                                //defining the fare break down for Indigo caching 
                                if (result.FareBreakdown[0] == null)//Adult
                                {
                                    result.FareBreakdown[0] = new Fare();
                                    result.FareBreakdown[0].PassengerCount = request.AdultCount;
                                    result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                }

                                if (request.ChildCount > 0)//Child
                                {

                                    if (result.FareBreakdown[1] == null)
                                    {
                                        result.FareBreakdown[1] = new Fare();
                                        result.FareBreakdown[1].PassengerCount = request.ChildCount;
                                        result.FareBreakdown[1].PassengerType = PassengerType.Child;
                                    }
                                }

                                if (request.ChildCount > 0 && request.InfantCount > 0)//If both child and infant exists
                                {
                                    if (result.FareBreakdown[2] == null)
                                    {
                                        result.FareBreakdown[2] = new Fare();
                                    }
                                    result.FareBreakdown[2].PassengerCount = request.InfantCount;
                                    result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                                }

                                else if (request.ChildCount == 0 && request.InfantCount > 0)//If only infant exists
                                {
                                    if (result.FareBreakdown[1] == null)
                                    {
                                        result.FareBreakdown[1] = new Fare();
                                    }
                                    result.FareBreakdown[1].PassengerCount = request.InfantCount;
                                    result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                                }
                                result.Price = new PriceAccounts();
                                
                                    if (journeyOnWard.Segments.Length > 0 && !string.IsNullOrEmpty(journeyOnWard.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode))
                                    {
                                        rateOfExchange = exchangeRates[journeyOnWard.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];
                                    }

                                CalculateBaseFareForOneWayConnectingFlights(ref result, FarePaxtypes, journeyOnWard, fareFirstConSeg, fareSecondConSeg);
                                ResultList.Add(result);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Indigo failed to get Connecting Flights Results.Reason : " + ex.Message, ex);
            }

            if (ResultList.Count > 0)
            {
                oneWayResultList.AddRange(ResultList);
            }
        }

        #endregion

        #region Return trip Flight Result Creation
        /// <summary>
        /// Gets the flight availability details for Retail Fare Product Class
        /// </summary>
        /// <param name="eventNumber"></param>
        /// 
        public void SearchForRetailFare()
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {
                if (request.SearchBySegments)//For Combination Search
                {
                    logonResponse = CreateSession();
                    if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature))
                    {
                        loginSignature = logonResponse.Signature;
                    }
                }

                #region Flight Results For Different Fare Types

                #region GetAvailabilityRequest Object
                //S-1:Create object for GetAvailabilityRequest
                GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                GetAvailabilityResponse availResponse = new GetAvailabilityResponse();

                //Assign Signature generated from Session
                availRequest.Signature = loginSignature;
                availRequest.ContractVersion = contractVersion;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                availRequest.EnableExceptionStackTrace = false;
                availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();

                if (request.Type == SearchType.OneWay)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }
                else
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                }

                //Assign all Availability details to get flight details
                for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                {
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = Indigo.BookingMgr.FlightType.All;
                    if (!string.IsNullOrEmpty(promoCode))
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                    }
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "6E";
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;

                    //Kindly enter value as ‘R’,  ‘Z’ and ‘M’ in tag namely FareTypes in GetAvailability API call and set the value as ‘CompressByProductClass for the tag namely FareClassControl  and set ProductClasses tag to Null or enter all the available ProductClasses as mentioned below to get the lowest fare among the all ProductClasses.
                    if (!request.SearchBySegments)//For regular B2B flow
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";//Retail Fare
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "Z"; //Lite Fare
                        
                        //6E FARE TYPES : Retail / Return Trip,Sale Fare,Family Fare,Special Round Trip,Flexi Fare,Lite Fare,SME Fare
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "F"; //SME Fare

                    }
                    else if (request.SearchBySegments && bookingSourceFlag == "6E")//For combination search Only For 6E
                    {
                        //FOR 6E Corp there will be no special round trip fares.
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[1];
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R";//Retail Fare
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses = new string[1];
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ProductClasses[0] = "N";//Special round Trip Fare
                    }

                    //FOR CORPORATE NEED TO SEND ONLY THE CORPORATE FARE TYPE
                    if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag == "6ECORP")
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[1];
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "F"; //CORPORATE FARE
                    }

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 10;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = Indigo.BookingMgr.AvailabilityType.Default;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;

                    int paxCount = 1;
                    if (request.ChildCount > 0)
                    {
                        paxCount++;
                    }

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(request.AdultCount + request.ChildCount);
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxCount];

                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[0] = new PaxPriceType();
                    availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[0].PaxType = "ADT";//Adult Passenger

                    if (request.ChildCount > 0)
                    {
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[1] = new PaxPriceType();
                        availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[1].PaxType = "CHD"; //Child Passenger
                    }
                }

                #region Indigo Round trip SearchRequest and Search Response Logs
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoRoundtrip_" + appUserId.ToString() + "_" + GetFareTypeForProductClass("R").Replace(" ", "_") + "_SearchRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, availRequest);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                }

                //S-2:GetAvailability: -Gets flight availability and fare information.

                //Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo round trip search request sent: " + DateTime.Now, "");
                GetAvailabilityResponse availResponse_v_4_5 = bookingAPI.GetAvailability(availRequest);
                TripAvailabilityResponse tripAvailResponse = availResponse_v_4_5.GetTripAvailabilityResponse;

                try
                {
                    //The default nullable types are not serialized so during xml serialization the default nullable types should be passed for serializing the object.
                    Type[] nullableTypes = new Type[] { typeof(Indigo.BookingMgr.AvailableFare) };

                    XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityResponse),nullableTypes);
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoRoundtrip_" + appUserId.ToString() + "_" + GetFareTypeForProductClass("R").Replace(" ", "_") + "_SearchResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, availResponse_v_4_5);
                    sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }

                #endregion

                #endregion
                if (!request.SearchBySegments)//For Regular Flow
                {
                    #region Flight Result object creation
                    if (tripAvailResponse != null)
                    {
                        List<Journey> OnwardJourneys = new List<Journey>();
                        List<Journey> ReturnJourneys = new List<Journey>();

                        List<Indigo.BookingMgr.Fare> onwardFares = new List<Indigo.BookingMgr.Fare>();
                        List<Indigo.BookingMgr.Fare> returnFares = new List<Indigo.BookingMgr.Fare>();

                        //Seperate onward and return journeys
                        if (tripAvailResponse != null && tripAvailResponse.Schedules != null && tripAvailResponse.Schedules.Length > 0)
                        {
                            //our system will automatically return all flights to nearby airports as well such as Sharjah (SHJ), Dubai World Center airport(DWC) along with DXB(Dubai).
                            //Schedules --2 Dimensional Array
                            //Schedule --0 --Always onward journeys
                            //Schedule --1 --Always return journeys
                            for (int j = 0; j < tripAvailResponse.Schedules[0].Length; j++)
                            {
                                if (tripAvailResponse.Schedules[0][j].Journeys.Length > 0)
                                {
                                    OnwardJourneys.AddRange(tripAvailResponse.Schedules[0][j].Journeys);
                                }
                            }
                            if (tripAvailResponse.Schedules.Length > 1)
                            {
                                for (int j = 0; j < tripAvailResponse.Schedules[1].Length; j++)
                                {
                                    if (tripAvailResponse.Schedules[1][j].Journeys.Length > 0)
                                    {
                                        ReturnJourneys.AddRange(tripAvailResponse.Schedules[1][j].Journeys);
                                    }
                                }
                            }
                            //If no schedules avialble either onward or return do not display the results
                            if (tripAvailResponse.Schedules != null && tripAvailResponse.Schedules.Length == 1)
                            {
                                OnwardJourneys.Clear();
                                ReturnJourneys.Clear();
                            }
                        }
                        for (int i = 0; i < OnwardJourneys.Count; i++)
                        {
                            for (int k = 0; k < OnwardJourneys[i].Segments.Length; k++)
                            {
                                onwardFares.AddRange(OnwardJourneys[i].Segments[k].Fares);
                            }
                        }

                        for (int j = 0; j < ReturnJourneys.Count; j++)
                        {
                            for (int k = 0; k < ReturnJourneys[j].Segments.Length; k++)
                            {
                                returnFares.AddRange(ReturnJourneys[j].Segments[k].Fares);
                            }
                        }
                        int fareBreakDownCount = 0;

                        if (request.AdultCount > 0)
                        {
                            fareBreakDownCount = 1;
                        }
                        if (request.ChildCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        if (request.InfantCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        if (request.Type == SearchType.Return)
                        {

                            for (int i = 0; i < OnwardJourneys.Count; i++)
                            {
                                for (int j = 0; j < ReturnJourneys.Count; j++)
                                {
                                    //Case-1:Both Onward and Return Direct Flights
                                    if (OnwardJourneys[i] != null && ReturnJourneys[j] != null && OnwardJourneys[i].Segments != null && OnwardJourneys[i].Segments.Length == 1 && ReturnJourneys[j].Segments != null && ReturnJourneys[j].Segments.Length == 1)
                                    {
                                        if (OnwardJourneys[i].Segments[0].Fares != null && ReturnJourneys[j].Segments[0].Fares != null && OnwardJourneys[i].Segments[0].Fares.Length > 0 && ReturnJourneys[j].Segments[0].Fares.Length > 0)
                                        {
                                            foreach (Indigo.BookingMgr.Fare onfare in OnwardJourneys[i].Segments[0].Fares)
                                            {
                                                foreach (Indigo.BookingMgr.Fare retFare in ReturnJourneys[j].Segments[0].Fares)
                                                {
                                                    if (onfare != null && retFare != null)
                                                    {
                                                        SearchResult result = new SearchResult();
                                                        result.IsLCC = true;
                                                        if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "6E")
                                                        {
                                                            result.ResultBookingSource = BookingSource.Indigo;
                                                        }
                                                        else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "6ECORP")
                                                        {
                                                            result.ResultBookingSource = BookingSource.IndigoCorp;
                                                        }
                                                        result.Airline = "6E";
                                                        result.Currency = agentBaseCurrency;
                                                        result.EticketEligible = true;
                                                        result.FareBreakdown = new Fare[fareBreakDownCount];
                                                        result.NonRefundable = false;
                                                        result.RepriceErrorMessage = loginSignature;//Till Search to book this signature is carried;

                                                        result.FareRules = new List<FareRule>();
                                                        FareRule fareRule = new FareRule();
                                                        fareRule.Airline = result.Airline;
                                                        fareRule.Destination = request.Segments[0].Destination;
                                                        fareRule.FareBasisCode = onfare.FareBasisCode;
                                                        fareRule.FareInfoRef = onfare.RuleNumber;
                                                        fareRule.Origin = request.Segments[0].Origin;
                                                        fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                                        fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                                        result.FareRules.Add(fareRule);

                                                        result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                        result.FareType = GetFareTypeForProductClass(onfare.ProductClass);
                                                        result.FareSellKey = onfare.FareSellKey;

                                                        if (request.Type == SearchType.OneWay)
                                                        {
                                                            result.Flights = new FlightInfo[1][];
                                                        }
                                                        else
                                                        {
                                                            result.Flights = new FlightInfo[2][];
                                                        }

                                                        List<FlightInfo> onwardFlights = GetFlightInfo(OnwardJourneys[i].Segments[0], OnwardJourneys[i], onfare.ProductClass, "ONWARD");

                                                        if (onwardFlights != null && onwardFlights.Count > 0)
                                                        {
                                                            List<FlightInfo> listOfOnwardFlights = new List<FlightInfo>();
                                                            listOfOnwardFlights.AddRange(onwardFlights);
                                                            result.Flights[0] = listOfOnwardFlights.ToArray();
                                                        }
                                                        CalculateBaseFareForReturnDirectFlights(ref result, OnwardJourneys[i], onfare);

                                                        result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                        result.FareType += "," + GetFareTypeForProductClass(retFare.ProductClass);
                                                        result.FareSellKey += "|" + retFare.FareSellKey;

                                                        List<FlightInfo> returnFlights = GetFlightInfo(ReturnJourneys[j].Segments[0], ReturnJourneys[j], retFare.ProductClass, "RETURN");

                                                        if (returnFlights != null && returnFlights.Count > 0)
                                                        {
                                                            List<FlightInfo> listOfReturnFlights = new List<FlightInfo>();
                                                            listOfReturnFlights.AddRange(returnFlights);
                                                            result.Flights[1] = listOfReturnFlights.ToArray();
                                                        }

                                                        CalculateBaseFareForReturnDirectFlights(ref result, ReturnJourneys[j], retFare);
                                                        ResultList.Add(result);
                                                    }
                                                }
                                            }
                                        }

                                    }

                                    //Case-2:Both Onward and Return Connecting Flights
                                    else if ((OnwardJourneys[i] != null && OnwardJourneys[i].Segments != null && OnwardJourneys[i].Segments.Length > 1) && (ReturnJourneys[j] != null && ReturnJourneys[j].Segments != null && ReturnJourneys[j].Segments.Length > 1))
                                    {
                                        List<SearchResult> conFlightsResults = GetReturnConnectingFlightsCombinationsResults(OnwardJourneys[i], ReturnJourneys[j], fareBreakDownCount, loginSignature);
                                        if (conFlightsResults.Count > 0)
                                        {
                                            ResultList.AddRange(conFlightsResults);
                                        }
                                    }

                                    //Case-3: Onward Direct and Return Connecting
                                    else if (OnwardJourneys[i] != null && OnwardJourneys[i].Segments != null && OnwardJourneys[i].Segments.Length == 1 && ReturnJourneys[j] != null && ReturnJourneys[j].Segments != null && ReturnJourneys[j].Segments.Length == 2)
                                    {
                                        if (OnwardJourneys[i].Segments[0].Fares != null && OnwardJourneys[i].Segments[0].Fares.Length > 0 && ReturnJourneys[j].Segments[0].Fares != null && ReturnJourneys[j].Segments[0].Fares.Length > 0 && ReturnJourneys[j].Segments[1].Fares != null && ReturnJourneys[j].Segments[1].Fares.Length > 0)
                                        {
                                            Segment firstSegmentConReturn = null;
                                            Segment secondSegmentConReturn = null;
                                            if (ReturnJourneys[j].Segments[0] != null && ReturnJourneys[j].Segments[1] != null)
                                            {
                                                firstSegmentConReturn = ReturnJourneys[j].Segments[0];
                                                secondSegmentConReturn = ReturnJourneys[j].Segments[1];
                                            }
                                            if (firstSegmentConReturn != null && secondSegmentConReturn != null && firstSegmentConReturn.Fares.Length == secondSegmentConReturn.Fares.Length)
                                            {
                                                for (int f = 0; f < OnwardJourneys[i].Segments[0].Fares.Length; f++)
                                                {
                                                    for (int g = 0; g < firstSegmentConReturn.Fares.Length; g++)
                                                    {
                                                        if (OnwardJourneys[i].Segments[0].Fares[f] != null && firstSegmentConReturn.Fares[g] != null && secondSegmentConReturn.Fares[g] != null)
                                                        {
                                                            Indigo.BookingMgr.Fare fareFirstConSegReturn = firstSegmentConReturn.Fares[g];
                                                            Indigo.BookingMgr.Fare fareSecondConSegReturn = secondSegmentConReturn.Fares[g];

                                                            SearchResult result = new SearchResult();
                                                            result.IsLCC = true;
                                                            if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "6E")
                                                            {
                                                                result.ResultBookingSource = BookingSource.Indigo;
                                                            }
                                                            else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "6ECORP")
                                                            {
                                                                result.ResultBookingSource = BookingSource.IndigoCorp;
                                                            }
                                                            result.Airline = "6E";
                                                            result.Currency = agentBaseCurrency;
                                                            result.EticketEligible = true;
                                                            result.FareBreakdown = new Fare[fareBreakDownCount];
                                                            result.NonRefundable = false;
                                                            result.RepriceErrorMessage = loginSignature;//Till Search To Book Same Signature Should be carried

                                                            result.FareRules = new List<FareRule>();
                                                            FareRule fareRule = new FareRule();
                                                            fareRule.Airline = result.Airline;
                                                            fareRule.Destination = request.Segments[0].Destination;
                                                            fareRule.FareBasisCode = OnwardJourneys[i].Segments[0].Fares[f].FareBasisCode;
                                                            fareRule.FareInfoRef = OnwardJourneys[i].Segments[0].Fares[f].RuleNumber;
                                                            fareRule.Origin = request.Segments[0].Origin;
                                                            fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                                            fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                                            result.FareRules.Add(fareRule);

                                                            result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                            result.FareSellKey = OnwardJourneys[i].Segments[0].Fares[f].FareSellKey;
                                                            result.FareType = GetFareTypeForProductClass(OnwardJourneys[i].Segments[0].Fares[f].ProductClass);
                                                            if (request.Type == SearchType.OneWay)
                                                            {
                                                                result.Flights = new FlightInfo[1][];
                                                            }
                                                            else
                                                            {
                                                                result.Flights = new FlightInfo[2][];
                                                            }
                                                            List<FlightInfo> onwardFlights = GetFlightInfo(OnwardJourneys[i].Segments[0], OnwardJourneys[i], OnwardJourneys[i].Segments[0].Fares[f].ProductClass, "ONWARD");

                                                            if (onwardFlights != null && onwardFlights.Count > 0)
                                                            {
                                                                List<FlightInfo> listOfOnwardFlights = new List<FlightInfo>();
                                                                listOfOnwardFlights.AddRange(onwardFlights);
                                                                result.Flights[0] = listOfOnwardFlights.ToArray();
                                                            }
                                                            CalculateBaseFareForReturnDirectFlights(ref result, OnwardJourneys[i], OnwardJourneys[i].Segments[0].Fares[f]);


                                                            result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                            result.FareType += "," + GetFareTypeForProductClass(fareFirstConSegReturn.ProductClass);
                                                            result.FareSellKey += "|" + fareFirstConSegReturn.FareSellKey + "^" + fareSecondConSegReturn.FareSellKey;


                                                            List<FlightInfo> firstSegmentConFlightsR = GetFlightInfo(firstSegmentConReturn, ReturnJourneys[j], fareFirstConSegReturn.ProductClass, "RETURN");
                                                            List<FlightInfo> secondSegmentConFlightsR = GetFlightInfo(secondSegmentConReturn, ReturnJourneys[j], fareSecondConSegReturn.ProductClass, "RETURN");
                                                            if (firstSegmentConFlightsR != null && secondSegmentConFlightsR != null && firstSegmentConFlightsR.Count > 0 && secondSegmentConFlightsR.Count > 0)
                                                            {
                                                                List<FlightInfo> listOfFlightsR = new List<FlightInfo>();
                                                                listOfFlightsR.AddRange(firstSegmentConFlightsR);
                                                                listOfFlightsR.AddRange(secondSegmentConFlightsR);
                                                                result.Flights[1] = listOfFlightsR.ToArray();
                                                            }
                                                            CalculateBaseFareForReturnConnectingFlights(ref result, ReturnJourneys[j]);

                                                            ResultList.Add(result);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }

                                    //Case-4: Onward Connecting and Return Direct
                                    else if (OnwardJourneys[i] != null && OnwardJourneys[i].Segments != null && OnwardJourneys[i].Segments.Length == 2 && ReturnJourneys[j] != null && ReturnJourneys[j].Segments != null && ReturnJourneys[j].Segments.Length == 1)
                                    {
                                        if (ReturnJourneys[j].Segments[0].Fares != null && ReturnJourneys[j].Segments[0].Fares.Length > 0 && OnwardJourneys[i].Segments[0].Fares != null && OnwardJourneys[i].Segments[0].Fares.Length > 0 && OnwardJourneys[i].Segments[1].Fares != null && OnwardJourneys[i].Segments[1].Fares.Length > 0)
                                        {
                                            Segment firstSegmentConOnward = null;
                                            Segment secondSegmentConOnward = null;

                                            if (OnwardJourneys[i].Segments[0] != null && OnwardJourneys[i].Segments[1] != null)
                                            {
                                                firstSegmentConOnward = OnwardJourneys[i].Segments[0];
                                                secondSegmentConOnward = OnwardJourneys[i].Segments[1];
                                            }

                                            if (firstSegmentConOnward != null && secondSegmentConOnward != null && firstSegmentConOnward.Fares.Length == secondSegmentConOnward.Fares.Length)
                                            {
                                                for (int f = 0; f < firstSegmentConOnward.Fares.Length; f++)
                                                {
                                                    for (int g = 0; g < ReturnJourneys[j].Segments[0].Fares.Length; g++)
                                                    {
                                                        if (firstSegmentConOnward.Fares[f] != null && secondSegmentConOnward.Fares[f] != null && ReturnJourneys[j].Segments[0].Fares[g] != null)
                                                        {
                                                            Indigo.BookingMgr.Fare fareFirstConSegOnWard = firstSegmentConOnward.Fares[f];
                                                            Indigo.BookingMgr.Fare fareSecondConSegOnward = secondSegmentConOnward.Fares[f];

                                                            SearchResult result = new SearchResult();
                                                            result.IsLCC = true;
                                                            if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "6E")
                                                            {
                                                                result.ResultBookingSource = BookingSource.Indigo;
                                                            }
                                                            else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "6ECORP")
                                                            {
                                                                result.ResultBookingSource = BookingSource.IndigoCorp;
                                                            }
                                                            result.Airline = "6E";
                                                            result.Currency = agentBaseCurrency;
                                                            result.EticketEligible = true;
                                                            result.FareBreakdown = new Fare[fareBreakDownCount];
                                                            result.FareRules = new List<FareRule>();
                                                            FareRule fareRule = new FareRule();
                                                            fareRule.Airline = result.Airline;
                                                            fareRule.Destination = request.Segments[0].Destination;
                                                            fareRule.FareBasisCode = OnwardJourneys[i].Segments[0].Fares[f].FareBasisCode;
                                                            fareRule.FareInfoRef = OnwardJourneys[i].Segments[0].Fares[f].RuleNumber;
                                                            fareRule.Origin = request.Segments[0].Origin;
                                                            fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                                            fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                                            result.FareRules.Add(fareRule);

                                                            result.NonRefundable = false;
                                                            result.RepriceErrorMessage = loginSignature;//Till Search To Book Single Signature should be carried.

                                                            result.JourneySellKey = OnwardJourneys[i].JourneySellKey;
                                                            result.FareSellKey = fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey;
                                                            result.FareType = GetFareTypeForProductClass(fareFirstConSegOnWard.ProductClass);
                                                            if (request.Type == SearchType.OneWay)
                                                            {
                                                                result.Flights = new FlightInfo[1][];

                                                            }
                                                            else
                                                            {
                                                                result.Flights = new FlightInfo[2][];

                                                            }
                                                            List<FlightInfo> firstSegmentConFlightsO = GetFlightInfo(firstSegmentConOnward, OnwardJourneys[i], fareFirstConSegOnWard.ProductClass, "ONWARD");
                                                            List<FlightInfo> secondSegmentConFlightsO = GetFlightInfo(secondSegmentConOnward, OnwardJourneys[i], fareSecondConSegOnward.ProductClass, "ONWARD");
                                                            if (firstSegmentConFlightsO != null && secondSegmentConFlightsO != null && firstSegmentConFlightsO.Count > 0 && secondSegmentConFlightsO.Count > 0)
                                                            {
                                                                List<FlightInfo> listOfFlightsO = new List<FlightInfo>();
                                                                listOfFlightsO.AddRange(firstSegmentConFlightsO);
                                                                listOfFlightsO.AddRange(secondSegmentConFlightsO);
                                                                result.Flights[0] = listOfFlightsO.ToArray();
                                                            }
                                                            CalculateBaseFareForReturnConnectingFlights(ref result, OnwardJourneys[i]);


                                                            result.JourneySellKey += "|" + ReturnJourneys[j].JourneySellKey;
                                                            result.FareType += "," + GetFareTypeForProductClass(ReturnJourneys[j].Segments[0].Fares[g].ProductClass);
                                                            result.FareSellKey += "|" + ReturnJourneys[j].Segments[0].Fares[g].FareSellKey;

                                                            List<FlightInfo> returnFlights = GetFlightInfo(ReturnJourneys[j].Segments[0], ReturnJourneys[j], ReturnJourneys[j].Segments[0].Fares[g].ProductClass, "RETURN");

                                                            if (returnFlights != null && returnFlights.Count > 0)
                                                            {
                                                                List<FlightInfo> listOfReturnFlights = new List<FlightInfo>();
                                                                listOfReturnFlights.AddRange(returnFlights);
                                                                result.Flights[1] = listOfReturnFlights.ToArray();
                                                            }
                                                            CalculateBaseFareForReturnDirectFlights(ref result, ReturnJourneys[j], ReturnJourneys[j].Segments[0].Fares[g]);
                                                            ResultList.Add(result);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }

                                }
                            }

                        }
                    }
                    #endregion

                    #endregion

                    CombinedSearchResults.AddRange(ResultList);
                }
                else if (request.SearchBySegments)//For Combination Flow
                {
                    if (tripAvailResponse != null && tripAvailResponse.Schedules != null && tripAvailResponse.Schedules.Length > 0)
                    {
                        //Schedules --2 Dimensional Array
                        //Schedule --0 --Always onward journeys
                        //Schedule --1 --Always return journeys
                        for (int j = 0; j < tripAvailResponse.Schedules[0].Length; j++)
                        {
                            if (tripAvailResponse.Schedules[0][j].Journeys.Length > 0)
                            {
                                OnwardJourneys.AddRange(tripAvailResponse.Schedules[0][j].Journeys);
                            }
                        }
                        if (tripAvailResponse.Schedules.Length > 1)
                        {
                            for (int j = 0; j < tripAvailResponse.Schedules[1].Length; j++)
                            {
                                if (tripAvailResponse.Schedules[1][j].Journeys.Length > 0)
                                {
                                    OnwardJourneys.AddRange(tripAvailResponse.Schedules[1][j].Journeys);
                                }
                            }
                        }
                        if (tripAvailResponse.Schedules.Length == 1)//If either or return no schedules available then do not form the results
                        {
                            OnwardJourneys.Clear();
                        }
                        if (OnwardJourneys != null && OnwardJourneys.Count > 0)
                        {
                            GetRTCombinationResults();
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to get Retail fare type results " + ex.ToString(), "");

            }

        }

        /// <summary>
        /// Gets the Connecting flight results for Return  trip.
        /// </summary>
        /// <param name="OnwardJourneys"></param>
        /// <param name="ReturnJourneys"></param>
        /// <param name="fareBreakDownCount"></param>
        /// <returns></returns>
        public List<SearchResult> GetReturnConnectingFlightsCombinationsResults(Journey OnwardJourneys, Journey ReturnJourneys, int fareBreakDownCount, string signature)
        {
            List<SearchResult> ResultList = new List<SearchResult>();
            try
            {

                if (OnwardJourneys != null && ReturnJourneys != null && OnwardJourneys.Segments != null && OnwardJourneys.Segments.Length > 0 && ReturnJourneys.Segments != null && ReturnJourneys.Segments.Length > 0 && OnwardJourneys.Segments.Length == 2 && ReturnJourneys.Segments.Length == 2)
                {
                    Segment firstSegmentConOnward = null;//Onward Journey First Connecting Segment
                    Segment secondSegmentConOnward = null;//Onward Journey Second Connecting Segment

                    Segment firstSegmentConReturn = null;//Return Journey First Connecting Segment
                    Segment secondSegmentConReturn = null;//Return Journey First Connecting Segment

                    if (OnwardJourneys.Segments[0] != null && OnwardJourneys.Segments[1] != null)
                    {
                        firstSegmentConOnward = OnwardJourneys.Segments[0];
                        secondSegmentConOnward = OnwardJourneys.Segments[1];
                    }
                    if (ReturnJourneys.Segments[0] != null && ReturnJourneys.Segments[1] != null)
                    {
                        firstSegmentConReturn = ReturnJourneys.Segments[0];
                        secondSegmentConReturn = ReturnJourneys.Segments[1];
                    }

                    if (firstSegmentConOnward != null && secondSegmentConOnward != null && firstSegmentConReturn != null && secondSegmentConReturn != null)
                    {
                        if (firstSegmentConOnward.Fares != null && firstSegmentConOnward.Fares.Length > 0 &&
                            secondSegmentConOnward.Fares != null && secondSegmentConOnward.Fares.Length > 0 &&

                            firstSegmentConReturn.Fares != null && firstSegmentConReturn.Fares.Length > 0 &&
                            secondSegmentConReturn.Fares != null && secondSegmentConReturn.Fares.Length > 0 &&

                            firstSegmentConOnward.Fares.Length == secondSegmentConOnward.Fares.Length &&
                            firstSegmentConReturn.Fares.Length == secondSegmentConReturn.Fares.Length)
                        {
                            for (int f = 0; f < firstSegmentConOnward.Fares.Length; f++)
                            {
                                for (int g = 0; g < firstSegmentConReturn.Fares.Length; g++)
                                {
                                    if (firstSegmentConOnward.Fares[f] != null && secondSegmentConOnward.Fares[f] != null && firstSegmentConReturn.Fares[g] != null && secondSegmentConReturn.Fares[g] != null)
                                    {
                                        Indigo.BookingMgr.Fare fareFirstConSegOnWard = firstSegmentConOnward.Fares[f];
                                        Indigo.BookingMgr.Fare fareSecondConSegOnward = secondSegmentConOnward.Fares[f];

                                        Indigo.BookingMgr.Fare fareFirstConSegReturn = firstSegmentConReturn.Fares[g];
                                        Indigo.BookingMgr.Fare fareSecondConSegReturn = secondSegmentConReturn.Fares[g];

                                        SearchResult result = new SearchResult();
                                        result.IsLCC = true;
                                        if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "6E")
                                        {
                                            result.ResultBookingSource = BookingSource.Indigo;
                                        }
                                        else if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag.Length > 0 && bookingSourceFlag.ToUpper() == "6ECORP")
                                        {
                                            result.ResultBookingSource = BookingSource.IndigoCorp;
                                        }
                                        result.Airline = "6E";
                                        result.Currency = agentBaseCurrency;
                                        result.EticketEligible = true;
                                        result.FareBreakdown = new Fare[fareBreakDownCount];
                                        result.FareRules = new List<FareRule>();
                                        result.NonRefundable = false;
                                        result.RepriceErrorMessage = signature;//Till Search To Book Single signature should be carried

                                        FareRule fareRule = new FareRule();
                                        fareRule.Airline = result.Airline;
                                        fareRule.Destination = request.Segments[0].Destination;
                                        fareRule.FareBasisCode = OnwardJourneys.Segments[0].Fares[f].FareBasisCode;
                                        fareRule.FareInfoRef = OnwardJourneys.Segments[0].Fares[f].RuleNumber;
                                        fareRule.Origin = request.Segments[0].Origin;
                                        fareRule.DepartureTime = request.Segments[0].PreferredDepartureTime;
                                        fareRule.ReturnDate = request.Segments[0].PreferredArrivalTime;
                                        result.FareRules.Add(fareRule);

                                        result.JourneySellKey = OnwardJourneys.JourneySellKey;
                                        result.FareSellKey = fareFirstConSegOnWard.FareSellKey + "^" + fareSecondConSegOnward.FareSellKey;
                                        result.FareType = GetFareTypeForProductClass(fareFirstConSegOnWard.ProductClass);
                                        if (request.Type == SearchType.OneWay)
                                        {
                                            result.Flights = new FlightInfo[1][];

                                        }
                                        else
                                        {
                                            result.Flights = new FlightInfo[2][];

                                        }
                                        List<FlightInfo> firstSegmentConFlightsO = GetFlightInfo(firstSegmentConOnward, OnwardJourneys, fareFirstConSegOnWard.ProductClass, "ONWARD");
                                        List<FlightInfo> secondSegmentConFlightsO = GetFlightInfo(secondSegmentConOnward, OnwardJourneys, fareSecondConSegOnward.ProductClass, "ONWARD");
                                        if (firstSegmentConFlightsO != null && secondSegmentConFlightsO != null && firstSegmentConFlightsO.Count > 0 && secondSegmentConFlightsO.Count > 0)
                                        {
                                            List<FlightInfo> listOfFlightsO = new List<FlightInfo>();
                                            listOfFlightsO.AddRange(firstSegmentConFlightsO);
                                            listOfFlightsO.AddRange(secondSegmentConFlightsO);
                                            result.Flights[0] = listOfFlightsO.ToArray();
                                        }
                                        CalculateBaseFareForReturnConnectingFlights(ref result, OnwardJourneys);


                                        result.JourneySellKey += "|" + ReturnJourneys.JourneySellKey;
                                        result.FareType += "," + GetFareTypeForProductClass(fareFirstConSegReturn.ProductClass);
                                        result.FareSellKey += "|" + fareFirstConSegReturn.FareSellKey + "^" + fareSecondConSegReturn.FareSellKey;


                                        List<FlightInfo> firstSegmentConFlightsR = GetFlightInfo(firstSegmentConReturn, ReturnJourneys, fareFirstConSegReturn.ProductClass, "RETURN");
                                        List<FlightInfo> secondSegmentConFlightsR = GetFlightInfo(secondSegmentConReturn, ReturnJourneys, fareSecondConSegReturn.ProductClass, "RETURN");
                                        if (firstSegmentConFlightsR != null && secondSegmentConFlightsR != null && firstSegmentConFlightsR.Count > 0 && secondSegmentConFlightsR.Count > 0)
                                        {
                                            List<FlightInfo> listOfFlightsR = new List<FlightInfo>();
                                            listOfFlightsR.AddRange(firstSegmentConFlightsR);
                                            listOfFlightsR.AddRange(secondSegmentConFlightsR);
                                            result.Flights[1] = listOfFlightsR.ToArray();
                                        }
                                        CalculateBaseFareForReturnConnectingFlights(ref result, ReturnJourneys);
                                        ResultList.Add(result);

                                    }
                                }
                            }
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to get return Combination results." + ex.ToString(), "");
                //throw new Exception("(Indigo) failed to get Connecting Flights Combinations Results.Reason : " + ex.Message, ex);
            }
            return ResultList;
        }

        #endregion

        #region Price Calculation(Base Price and Tax) for one way and return trips

        #region Base Price Calculation

        /// <summary>
        /// For one way(Direct Flights or Direct Flights With Via Combination), this method Calculates the BaseFare for that itinerary i.e avaialble from the Search Response .
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="FarePaxtypes"></param>
        /// <param name="Onwardjourney"></param>
        /// <param name="OnwardFare"></param>
        public void CalculateBaseFareForOneWaydirectFlights(ref SearchResult resultObj, List<String> FarePaxtypes, Journey Onwardjourney, Indigo.BookingMgr.Fare OnwardFare)
        {
            try
            {
                if (Onwardjourney.Segments.Length > 0 && !string.IsNullOrEmpty(Onwardjourney.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode))
                {
                    rateOfExchange = exchangeRates[Onwardjourney.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];
                }
                resultObj.Price = new PriceAccounts();
                int tripCount = 1;
                if (!request.SearchBySegments && request.Type == SearchType.Return) //For normal search
                {
                    tripCount++;
                }

                ComputePriceFromAvalibilityResponse(ref resultObj, OnwardFare);

                for (int journeyOnWardReturn = 0; journeyOnWardReturn < tripCount; journeyOnWardReturn++)
                {
                    
                    //Infant fare break down
                    #region FareBreakdown of Infant
                    if (request.InfantCount > 0)
                    {
                        decimal infantPrice = 0;
                        infantPrice = GetInfantAvailibilityFares(request);
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[2].PassengerCount);
                            resultObj.FareBreakdown[2].TotalFare += baseFare;
                            resultObj.FareBreakdown[2].BaseFare += baseFare;
                            resultObj.FareBreakdown[2].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[2].PassengerCount);
                            resultObj.Price.PublishedFare += (decimal)baseFare;
                            resultObj.Price.SupplierPrice += infantPrice;
                        }
                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                        {
                            double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                            resultObj.FareBreakdown[1].BaseFare += baseFare;
                            resultObj.FareBreakdown[1].TotalFare += baseFare;
                            resultObj.FareBreakdown[1].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[1].PassengerCount);
                            resultObj.Price.PublishedFare += (decimal)baseFare;
                            resultObj.Price.SupplierPrice += infantPrice;
                        }
                    }
                    #endregion
                }


                resultObj.BaseFare = (double)Math.Round(resultObj.Price.PublishedFare, agentDecimalValue);
                resultObj.Tax = (double)resultObj.Price.Tax;
                resultObj.TotalFare = (double)(Math.Round((resultObj.BaseFare + resultObj.Tax), agentDecimalValue));               

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to  Calculate BaseFare For OneWay directFlights. Reason : " + ex.ToString(), "");
            }

        }

        /// <summary>
        /// For one way(Connecting Flights), this method Calculates the BaseFare for that itinerary i.e avaialble from the Search Response .
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="FarePaxtypes"></param>
        /// <param name="Onwardjourney"></param>
        /// <param name="OnwardFareConnecting1"></param>
        /// <param name="OnwardFareConnecting2"></param>
        public void CalculateBaseFareForOneWayConnectingFlights(ref SearchResult resultObj, List<String> FarePaxtypes, Journey Onwardjourney, Indigo.BookingMgr.Fare OnwardFareConnecting1, Indigo.BookingMgr.Fare OnwardFareConnecting2)
        {
            try
            {
                if (Onwardjourney.Segments.Length > 0 && !string.IsNullOrEmpty(Onwardjourney.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode))
                {
                    rateOfExchange = exchangeRates[Onwardjourney.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];
                }
                resultObj.Price = new PriceAccounts();
                int tripCount = 1;
                if (!request.SearchBySegments && request.Type == SearchType.Return)//Normal search
                {
                    tripCount++;
                }

                for (int journeyOnWardReturn = 0; journeyOnWardReturn < tripCount; journeyOnWardReturn++)
                {
                    #region FareBreakdown of adult and child
                    foreach (String passengerType in FarePaxtypes)
                    {
                        switch (passengerType)
                        {
                            case "ADT": //Adult
                                if (resultObj.FareBreakdown[0] == null)
                                {
                                    resultObj.FareBreakdown[0] = new Fare();
                                }
                                resultObj.FareBreakdown[0].PassengerCount = request.AdultCount;
                                resultObj.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                break;
                            case "CHD": //Child
                                if (resultObj.FareBreakdown[1] == null)
                                {
                                    resultObj.FareBreakdown[1] = new Fare();
                                }
                                resultObj.FareBreakdown[1].PassengerCount = request.ChildCount;
                                resultObj.FareBreakdown[1].PassengerType = PassengerType.Child;
                                break;

                        }

                        //Infant Count-- if both children and infants exists in the request.
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            if (resultObj.FareBreakdown[2] == null)
                            {
                                resultObj.FareBreakdown[2] = new Fare();
                            }
                            resultObj.FareBreakdown[2].PassengerCount = request.InfantCount;
                            resultObj.FareBreakdown[2].PassengerType = PassengerType.Infant;
                        }

                        //Child Count -- If only infant count exists in the rquest.
                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                        {
                            if (resultObj.FareBreakdown[1] == null)
                            {
                                resultObj.FareBreakdown[1] = new Fare();
                            }
                            resultObj.FareBreakdown[1].PassengerCount = request.InfantCount;
                            resultObj.FareBreakdown[1].PassengerType = PassengerType.Infant;
                        }

                        int journeySegCount = 0;
                        int onwardSegCount = 0;
                        int returnSegCount = 0;

                        for (int p = 0; p < tripCount; p++)
                        {
                            if (p == 0)
                            {
                                for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                {
                                    if (resultObj.Flights[p][l].Status.ToUpper() != "VIA")
                                    {
                                        onwardSegCount++;
                                    }
                                }
                                if (onwardSegCount == 0)
                                {
                                    onwardSegCount = 1;
                                }
                            }
                            else
                            {
                                for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                {
                                    if (resultObj.Flights[p][l].Status.ToUpper() != "VIA")
                                    {
                                        returnSegCount++;
                                    }
                                }
                                if (returnSegCount == 0)
                                {
                                    returnSegCount = 1;
                                }

                            }
                        }
                        if (journeyOnWardReturn == 0)
                        {
                            journeySegCount = onwardSegCount;
                        }
                        else
                        {
                            journeySegCount = returnSegCount;
                        }
                        for (int k = 0; k < journeySegCount; k++)
                        {
                            Indigo.BookingMgr.Fare OnwardFare = null;
                            if (k == 0)
                            {
                                OnwardFare = OnwardFareConnecting1;
                            }
                            else
                            {
                                OnwardFare = OnwardFareConnecting2;
                            }

                            //We need to combine fares of all the segments from the price itenary response

                            foreach (PaxFare pfare in OnwardFare.PaxFares)
                            {
                                if (passengerType == pfare.PaxType)
                                {
                                    foreach (BookingServiceCharge charge in OnwardFare.PaxFares[0].ServiceCharges)
                                    {
                                        switch (passengerType)
                                        {
                                            case "ADT":
                                                if (resultObj.FareBreakdown[0] != null)
                                                {
                                                    switch (charge.ChargeType)
                                                    {
                                                        case Indigo.BookingMgr.ChargeType.FarePrice:
                                                            double baseFareAdult = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[0].PassengerCount);
                                                            resultObj.FareBreakdown[0].BaseFare += baseFareAdult;
                                                            resultObj.FareBreakdown[0].TotalFare += baseFareAdult;
                                                            resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                            resultObj.Price.PublishedFare += (decimal)baseFareAdult;
                                                            resultObj.Price.SupplierPrice += charge.Amount;
                                                            break;
                                                        //Deduct discount from the base fare
                                                        case Indigo.BookingMgr.ChargeType.PromotionDiscount:
                                                            double discountAdult = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                                                            resultObj.FareBreakdown[0].BaseFare = resultObj.FareBreakdown[0].BaseFare - discountAdult;
                                                            resultObj.FareBreakdown[0].TotalFare = resultObj.FareBreakdown[0].TotalFare - discountAdult;
                                                            resultObj.FareBreakdown[0].SupplierFare = resultObj.FareBreakdown[0].SupplierFare - (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                            resultObj.Price.PublishedFare = (decimal)(resultObj.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                                                            resultObj.Price.SupplierPrice = resultObj.Price.SupplierPrice - charge.Amount;
                                                            break;
                                                        case Indigo.BookingMgr.ChargeType.TravelFee:
                                                        case Indigo.BookingMgr.ChargeType.Tax:
                                                        case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                        case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                        case Indigo.BookingMgr.ChargeType.Calculated:
                                                        case Indigo.BookingMgr.ChargeType.Note:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                        case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                        case Indigo.BookingMgr.ChargeType.Loyalty:
                                                        case Indigo.BookingMgr.ChargeType.FarePoints:
                                                        case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                        case Indigo.BookingMgr.ChargeType.Unmapped:

                                                            double tax = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                                                            resultObj.FareBreakdown[0].TotalFare += tax;
                                                            resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                            resultObj.Price.Tax += (decimal)tax;                                                           
                                                            resultObj.Price.SupplierPrice += charge.Amount;
                                                            break;
                                                    }
                                                }
                                                break;
                                                    
                                            case "CHD":
                                                if (resultObj.FareBreakdown[1] != null)
                                                {
                                                    switch (charge.ChargeType)
                                                    {
                                                        case Indigo.BookingMgr.ChargeType.FarePrice:
                                                            double baseFareChd = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                                                            resultObj.FareBreakdown[1].BaseFare += baseFareChd;
                                                            resultObj.FareBreakdown[1].TotalFare += baseFareChd;
                                                            resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                            resultObj.Price.PublishedFare += (decimal)baseFareChd;
                                                            resultObj.Price.SupplierPrice += charge.Amount;
                                                            break;
                                                        //Deduct discount from the base fare
                                                        case Indigo.BookingMgr.ChargeType.PromotionDiscount:
                                                            double discountChild = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                                                            resultObj.FareBreakdown[1].BaseFare = resultObj.FareBreakdown[1].BaseFare - discountChild;
                                                            resultObj.FareBreakdown[1].TotalFare = resultObj.FareBreakdown[1].TotalFare - discountChild;
                                                            resultObj.FareBreakdown[1].SupplierFare = resultObj.FareBreakdown[1].SupplierFare - (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                            resultObj.Price.PublishedFare = (decimal)(resultObj.Price.PublishedFare - Convert.ToDecimal(discountChild));
                                                            resultObj.Price.SupplierPrice = resultObj.Price.SupplierPrice - charge.Amount;
                                                            break;

                                                        case Indigo.BookingMgr.ChargeType.TravelFee:
                                                        case Indigo.BookingMgr.ChargeType.Tax:
                                                        case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                        case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                        case Indigo.BookingMgr.ChargeType.Calculated:
                                                        case Indigo.BookingMgr.ChargeType.Note:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                        case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                        case Indigo.BookingMgr.ChargeType.Loyalty:
                                                        case Indigo.BookingMgr.ChargeType.FarePoints:
                                                        case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                        case Indigo.BookingMgr.ChargeType.Unmapped:
                                                            double tax = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                                                            resultObj.FareBreakdown[1].TotalFare += tax;
                                                            resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                            resultObj.Price.Tax += (decimal)tax;
                                                            resultObj.Price.SupplierPrice += charge.Amount;
                                                            break;
                                                    }

                                                }
                                                break;      
                                        }
                                    }
                                }
                            }
                        }

                    }
                    #endregion
                    //Infant fare break down
                    #region FareBreakdown of Infant
                    if (request.InfantCount > 0)
                    {
                        decimal infantPrice = 0;
                        infantPrice = GetInfantAvailibilityFares(request);
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[2].PassengerCount);
                            resultObj.FareBreakdown[2].TotalFare += baseFare;
                            resultObj.FareBreakdown[2].BaseFare += baseFare;
                            resultObj.FareBreakdown[2].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[2].PassengerCount);
                            resultObj.Price.PublishedFare += (decimal)baseFare;
                            resultObj.Price.SupplierPrice += infantPrice;
                        }
                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                        {
                            double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                            resultObj.FareBreakdown[1].BaseFare += baseFare;
                            resultObj.FareBreakdown[1].TotalFare += baseFare;
                            resultObj.FareBreakdown[1].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[1].PassengerCount);
                            resultObj.Price.PublishedFare += (decimal)baseFare;
                            resultObj.Price.SupplierPrice += infantPrice;
                        }
                    }
                    #endregion
                }


                resultObj.BaseFare = (double)Math.Round(resultObj.Price.PublishedFare, agentDecimalValue);
                resultObj.Tax = (double)resultObj.Price.Tax;
                resultObj.TotalFare = (double)(Math.Round((resultObj.BaseFare + resultObj.Tax), agentDecimalValue));
                


            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to  Calculate BaseFare For Connecting Flights. Reason : " + ex.ToString(), "");

            }
            //return resultObj;
        }


        /// <summary>
        /// For Round Trip(Direct Flights or Direct Flights With Via Combination), this method Calculates the BaseFare for that itinerary i.e avaialble from the Search Response .
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="journey"></param>
        /// <param name="fare"></param>
        private void CalculateBaseFareForReturnDirectFlights(ref SearchResult resultObj, Journey journey, Indigo.BookingMgr.Fare fare)
        {
            try
            {
                List<string> FarePaxtypes = new List<string>();
                if (request.AdultCount > 0)
                {
                    FarePaxtypes.Add("ADT");
                }
                if (request.ChildCount > 0)
                {
                    FarePaxtypes.Add("CHD");
                }
                if (request.InfantCount > 0)
                {
                    FarePaxtypes.Add("INFT");
                }
                if (journey.Segments.Length > 0 && !string.IsNullOrEmpty(journey.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode))
                {
                    rateOfExchange = exchangeRates[journey.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];
                }
                if (resultObj.Price == null)
                {
                    resultObj.Price = new PriceAccounts();
                }
                #region FareBreakdown of adult and child
                foreach (String passengerType in FarePaxtypes)
                {
                    switch (passengerType)
                    {
                        case "ADT": //Adult
                            if (resultObj.FareBreakdown[0] == null)
                            {
                                resultObj.FareBreakdown[0] = new Fare();
                            }
                            resultObj.FareBreakdown[0].PassengerCount = request.AdultCount;
                            resultObj.FareBreakdown[0].PassengerType = PassengerType.Adult;
                            break;
                        case "CHD": //Child
                            if (resultObj.FareBreakdown[1] == null)
                            {
                                resultObj.FareBreakdown[1] = new Fare();
                            }
                            resultObj.FareBreakdown[1].PassengerCount = request.ChildCount;
                            resultObj.FareBreakdown[1].PassengerType = PassengerType.Child;
                            break;

                    }

                    //Infant Count-- if both children and infants exists in the request.
                    if (request.ChildCount > 0 && request.InfantCount > 0)
                    {
                        if (resultObj.FareBreakdown[2] == null)
                        {
                            resultObj.FareBreakdown[2] = new Fare();
                        }
                        resultObj.FareBreakdown[2].PassengerCount = request.InfantCount;
                        resultObj.FareBreakdown[2].PassengerType = PassengerType.Infant;
                    }

                    //Child Count -- If only infant count exists in the rquest.
                    else if (request.ChildCount == 0 && request.InfantCount > 0)
                    {
                        if (resultObj.FareBreakdown[1] == null)
                        {
                            resultObj.FareBreakdown[1] = new Fare();
                        }
                        resultObj.FareBreakdown[1].PassengerCount = request.InfantCount;
                        resultObj.FareBreakdown[1].PassengerType = PassengerType.Infant;
                    }


                    //We need to combine fares of all the segments from the price itenary response

                    foreach (PaxFare pfare in fare.PaxFares)
                    {
                        if (passengerType == pfare.PaxType)
                        {
                            foreach (BookingServiceCharge charge in fare.PaxFares[0].ServiceCharges)
                            {
                                switch (passengerType)
                                {
                                    case "ADT":
                                        if(resultObj.FareBreakdown[0]!=null)
                                        {
                                            switch(charge.ChargeType)
                                            {
                                                case Indigo.BookingMgr.ChargeType.FarePrice:
                                                    double baseFareAdult = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[0].PassengerCount);
                                                    resultObj.FareBreakdown[0].BaseFare += baseFareAdult;
                                                    resultObj.FareBreakdown[0].TotalFare += baseFareAdult;
                                                    resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                    resultObj.Price.PublishedFare += (decimal)baseFareAdult;
                                                    resultObj.Price.SupplierPrice += charge.Amount;
                                                    break;
                                                //Deduct discount from the base fare
                                                case Indigo.BookingMgr.ChargeType.PromotionDiscount:
                                                    double discountAdult = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                                                    resultObj.FareBreakdown[0].BaseFare = resultObj.FareBreakdown[0].BaseFare - discountAdult;
                                                    resultObj.FareBreakdown[0].TotalFare = resultObj.FareBreakdown[0].TotalFare - discountAdult;
                                                    resultObj.FareBreakdown[0].SupplierFare = resultObj.FareBreakdown[0].SupplierFare - (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                    resultObj.Price.PublishedFare = (decimal)(resultObj.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                                                    resultObj.Price.SupplierPrice = resultObj.Price.SupplierPrice - charge.Amount;
                                                    break;

                                                case Indigo.BookingMgr.ChargeType.TravelFee:
                                                case Indigo.BookingMgr.ChargeType.Tax:
                                                case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                case Indigo.BookingMgr.ChargeType.Calculated:
                                                case Indigo.BookingMgr.ChargeType.Note:
                                                case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                case Indigo.BookingMgr.ChargeType.Loyalty:
                                                case Indigo.BookingMgr.ChargeType.FarePoints:
                                                case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                case Indigo.BookingMgr.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                                                    resultObj.FareBreakdown[0].TotalFare += tax;
                                                    resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                    resultObj.Price.Tax += (decimal)tax;
                                                    resultObj.Price.SupplierPrice += charge.Amount;
                                                    break;
                                            }

                                        }
                                        break;

                                    case "CHD":
                                        if (resultObj.FareBreakdown[1] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case Indigo.BookingMgr.ChargeType.FarePrice:
                                                    double baseFareChd = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                                                    resultObj.FareBreakdown[1].BaseFare += baseFareChd;
                                                    resultObj.FareBreakdown[1].TotalFare += baseFareChd;
                                                    resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                    resultObj.Price.PublishedFare += (decimal)baseFareChd;
                                                    resultObj.Price.SupplierPrice += charge.Amount;
                                                    break;
                                                //Deduct discount from the base fare
                                                case Indigo.BookingMgr.ChargeType.PromotionDiscount:
                                                    double discountChild = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                                                    resultObj.FareBreakdown[1].BaseFare = resultObj.FareBreakdown[1].BaseFare - discountChild;
                                                    resultObj.FareBreakdown[1].TotalFare = resultObj.FareBreakdown[1].TotalFare - discountChild;
                                                    resultObj.FareBreakdown[1].SupplierFare = resultObj.FareBreakdown[1].SupplierFare - (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                    resultObj.Price.PublishedFare = (decimal)(resultObj.Price.PublishedFare - Convert.ToDecimal(discountChild));
                                                    resultObj.Price.SupplierPrice = resultObj.Price.SupplierPrice - charge.Amount;
                                                    break;

                                                case Indigo.BookingMgr.ChargeType.TravelFee:
                                                case Indigo.BookingMgr.ChargeType.Tax:
                                                case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                case Indigo.BookingMgr.ChargeType.Calculated:
                                                case Indigo.BookingMgr.ChargeType.Note:
                                                case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                case Indigo.BookingMgr.ChargeType.Loyalty:
                                                case Indigo.BookingMgr.ChargeType.FarePoints:
                                                case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                case Indigo.BookingMgr.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                                                    resultObj.FareBreakdown[1].TotalFare += tax;
                                                    resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                    resultObj.Price.Tax += (decimal)tax;
                                                    resultObj.Price.SupplierPrice += charge.Amount;
                                                    break;
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                    }


                }
                #endregion

                //Infant fare break down
                #region FareBreakdown of Infant
                if (request.InfantCount > 0)
                {
                    decimal infantPrice = 0;
                    infantPrice = GetInfantAvailibilityFares(request);
                    if (request.ChildCount > 0 && request.InfantCount > 0)
                    {
                        double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[2].PassengerCount);
                        resultObj.FareBreakdown[2].TotalFare += baseFare;
                        resultObj.FareBreakdown[2].BaseFare += baseFare;
                        resultObj.FareBreakdown[2].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[2].PassengerCount);
                        resultObj.Price.PublishedFare += (decimal)baseFare;
                        resultObj.Price.SupplierPrice += infantPrice;
                    }
                    else if (request.ChildCount <= 0 && request.InfantCount > 0)
                    {
                        double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.FareBreakdown[1].BaseFare += baseFare;
                        resultObj.FareBreakdown[1].TotalFare += baseFare;
                        resultObj.FareBreakdown[1].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.Price.PublishedFare += (decimal)baseFare;
                        resultObj.Price.SupplierPrice += infantPrice;
                    }
                }
                #endregion

                resultObj.BaseFare = (double)Math.Round(resultObj.Price.PublishedFare, agentDecimalValue);
                resultObj.Tax = (double)resultObj.Price.Tax;
                resultObj.TotalFare = (double)(Math.Round((resultObj.BaseFare + resultObj.Tax), agentDecimalValue));
                

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to  Calculate Base Fare For Return Direct Flights. Reason : " + ex.ToString(), "");

            }

        }


        /// <summary>
        /// For Return(Connecting Flights), this method Calculates the BaseFare for that itinerary i.e avaialble from the Search Response .
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="journey"></param>
        private void CalculateBaseFareForReturnConnectingFlights(ref SearchResult resultObj, Journey journey)
        {
            try
            {
                List<string> FarePaxtypes = new List<string>();
                if (request.AdultCount > 0)
                {
                    FarePaxtypes.Add("ADT");
                }
                if (request.ChildCount > 0)
                {
                    FarePaxtypes.Add("CHD");
                }
                if (request.InfantCount > 0)
                {
                    FarePaxtypes.Add("INFT");
                }

                if (journey.Segments.Length > 0 && !string.IsNullOrEmpty(journey.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode))
                {
                    rateOfExchange = exchangeRates[journey.Segments[0].Fares[0].PaxFares[0].ServiceCharges[0].CurrencyCode];

                }
                if (resultObj.Price == null)
                {
                    resultObj.Price = new PriceAccounts();
                }



                #region FareBreakdown of adult and child
                foreach (String passengerType in FarePaxtypes)
                {
                    switch (passengerType)
                    {
                        case "ADT": //Adult
                            if (resultObj.FareBreakdown[0] == null)
                            {
                                resultObj.FareBreakdown[0] = new Fare();
                            }
                            resultObj.FareBreakdown[0].PassengerCount = request.AdultCount;
                            resultObj.FareBreakdown[0].PassengerType = PassengerType.Adult;
                            break;
                        case "CHD": //Child
                            if (resultObj.FareBreakdown[1] == null)
                            {
                                resultObj.FareBreakdown[1] = new Fare();
                            }
                            resultObj.FareBreakdown[1].PassengerCount = request.ChildCount;
                            resultObj.FareBreakdown[1].PassengerType = PassengerType.Child;
                            break;

                    }

                    //Infant Count-- if both children and infants exists in the request.
                    if (request.ChildCount > 0 && request.InfantCount > 0)
                    {
                        if (resultObj.FareBreakdown[2] == null)
                        {
                            resultObj.FareBreakdown[2] = new Fare();
                        }
                        resultObj.FareBreakdown[2].PassengerCount = request.InfantCount;
                        resultObj.FareBreakdown[2].PassengerType = PassengerType.Infant;
                    }

                    //Child Count -- If only infant count exists in the rquest.
                    else if (request.ChildCount == 0 && request.InfantCount > 0)
                    {
                        if (resultObj.FareBreakdown[1] == null)
                        {
                            resultObj.FareBreakdown[1] = new Fare();
                        }
                        resultObj.FareBreakdown[1].PassengerCount = request.InfantCount;
                        resultObj.FareBreakdown[1].PassengerType = PassengerType.Infant;
                    }

                    for (int k = 0; k < journey.Segments.Length; k++)
                    {


                        //We need to combine fares of all the segments from the price itenary response

                        foreach (PaxFare pfare in journey.Segments[k].Fares[0].PaxFares)
                        {
                            if (passengerType == pfare.PaxType)
                            {
                                foreach (BookingServiceCharge charge in journey.Segments[k].Fares[0].PaxFares[0].ServiceCharges)
                                {
                                    switch (passengerType)
                                    {
                                        case "ADT":
                                            if (resultObj.FareBreakdown[0] != null)
                                            {
                                                switch(charge.ChargeType)
                                                {
                                                    case Indigo.BookingMgr.ChargeType.FarePrice:
                                                        double baseFareAdult = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[0].PassengerCount);
                                                        resultObj.FareBreakdown[0].BaseFare += baseFareAdult;
                                                        resultObj.FareBreakdown[0].TotalFare += baseFareAdult;
                                                        resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                        resultObj.Price.PublishedFare += (decimal)baseFareAdult;
                                                        resultObj.Price.SupplierPrice += charge.Amount;
                                                        break;
                                                    //Deduct discount from the base fare
                                                    case Indigo.BookingMgr.ChargeType.PromotionDiscount:
                                                        double discountAdult = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                                                        resultObj.FareBreakdown[0].BaseFare = resultObj.FareBreakdown[0].BaseFare - discountAdult;
                                                        resultObj.FareBreakdown[0].TotalFare = resultObj.FareBreakdown[0].TotalFare - discountAdult;
                                                        resultObj.FareBreakdown[0].SupplierFare = resultObj.FareBreakdown[0].SupplierFare - (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                        resultObj.Price.PublishedFare = (decimal)(resultObj.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                                                        resultObj.Price.SupplierPrice = resultObj.Price.SupplierPrice - charge.Amount;
                                                        break;

                                                    case Indigo.BookingMgr.ChargeType.TravelFee:
                                                    case Indigo.BookingMgr.ChargeType.Tax:
                                                    case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                    case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                    case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                    case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                    case Indigo.BookingMgr.ChargeType.Calculated:
                                                    case Indigo.BookingMgr.ChargeType.Note:
                                                    case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                    case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                    case Indigo.BookingMgr.ChargeType.Loyalty:
                                                    case Indigo.BookingMgr.ChargeType.FarePoints:
                                                    case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                    case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                    case Indigo.BookingMgr.ChargeType.Unmapped:
                                                        double tax = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                                                        resultObj.FareBreakdown[0].TotalFare += tax;
                                                        resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                        resultObj.Price.Tax += (decimal)tax;
                                                        resultObj.Price.SupplierPrice += charge.Amount;
                                                        break;
                                                }
                                            }
                                            break;
                                                
                                        case "CHD":
                                            if (resultObj.FareBreakdown[1] != null)
                                            {
                                                switch (charge.ChargeType)
                                                {
                                                    case Indigo.BookingMgr.ChargeType.FarePrice:
                                                        double baseFareChd = (double)(Math.Round((charge.Amount * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                                                        resultObj.FareBreakdown[1].BaseFare += baseFareChd;
                                                        resultObj.FareBreakdown[1].TotalFare += baseFareChd;
                                                        resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                        resultObj.Price.PublishedFare += (decimal)baseFareChd;
                                                        resultObj.Price.SupplierPrice += charge.Amount;
                                                        break;

                                                    //Deduct discount from the base fare
                                                    case Indigo.BookingMgr.ChargeType.PromotionDiscount:
                                                        double discountChild = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                                                        resultObj.FareBreakdown[1].BaseFare = resultObj.FareBreakdown[1].BaseFare - discountChild;
                                                        resultObj.FareBreakdown[1].TotalFare = resultObj.FareBreakdown[1].TotalFare - discountChild;
                                                        resultObj.FareBreakdown[1].SupplierFare = resultObj.FareBreakdown[1].SupplierFare - (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                        resultObj.Price.PublishedFare = (decimal)(resultObj.Price.PublishedFare - Convert.ToDecimal(discountChild));
                                                        resultObj.Price.SupplierPrice = resultObj.Price.SupplierPrice - charge.Amount;
                                                        break;


                                                    case Indigo.BookingMgr.ChargeType.TravelFee:
                                                    case Indigo.BookingMgr.ChargeType.Tax:
                                                    case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                    case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                    case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                    case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                    case Indigo.BookingMgr.ChargeType.Calculated:
                                                    case Indigo.BookingMgr.ChargeType.Note:
                                                    case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                    case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                    case Indigo.BookingMgr.ChargeType.Loyalty:
                                                    case Indigo.BookingMgr.ChargeType.FarePoints:
                                                    case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                    case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                    case Indigo.BookingMgr.ChargeType.Unmapped:
                                                        double tax = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                                                        resultObj.FareBreakdown[1].TotalFare += tax;
                                                        resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                        resultObj.Price.Tax += (decimal)tax;                            
                                                        resultObj.Price.SupplierPrice += charge.Amount;
                                                        break;
                                                }
                                            }
                                            break;

                                                
                                    }
                                }
                            }
                        }
                    }

                }
                #endregion
                //Infant fare break down
                #region FareBreakdown of Infant
                if (request.InfantCount > 0)
                {
                    decimal infantPrice = 0;
                    infantPrice = GetInfantAvailibilityFares(request);
                    if (request.ChildCount > 0 && request.InfantCount > 0)
                    {
                        double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[2].PassengerCount);
                        resultObj.FareBreakdown[2].TotalFare += baseFare;
                        resultObj.FareBreakdown[2].BaseFare += baseFare;
                        resultObj.FareBreakdown[2].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[2].PassengerCount);
                        resultObj.Price.PublishedFare += (decimal)baseFare;
                        resultObj.Price.SupplierPrice += infantPrice;
                    }
                    else if (request.ChildCount <= 0 && request.InfantCount > 0)
                    {
                        double baseFare = (double)(Math.Round((infantPrice * rateOfExchange), agentDecimalValue) * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.FareBreakdown[1].BaseFare += baseFare;
                        resultObj.FareBreakdown[1].TotalFare += baseFare;
                        resultObj.FareBreakdown[1].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.Price.PublishedFare += (decimal)baseFare;
                        resultObj.Price.SupplierPrice += infantPrice;
                    }
                }
                #endregion

                resultObj.BaseFare = (double)Math.Round(resultObj.Price.PublishedFare, agentDecimalValue);
                resultObj.Tax = (double)resultObj.Price.Tax;
                resultObj.TotalFare = (double)(Math.Round((resultObj.BaseFare + resultObj.Tax), agentDecimalValue));
                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Calculate BaseFare For Return Connecting Flights. Reason : " + ex.ToString(), "");

            }
            //return resultObj;
        }


        /// <summary>
        /// This method is used to get Infant Fares from the Source Baggage Info Table
        /// </summary>
        /// <param name="onwardFare"></param>
        /// <param name="returnFare"></param>
        /// <returns></returns>
        protected decimal GetInfantAvailibilityFares(SearchRequest request)
        {
            decimal InfantPrice = 0;
            try
            {
                DataTable dtSourceBaggage = SourceBaggage.GetSourceBaggages(BookingSource.Indigo, currencyCode);

                if (request.Type == SearchType.OneWay)
                {
                    Airport origin = new Airport(request.Segments[0].Origin);
                    Airport destination = new Airport(request.Segments[0].Destination);

                    //If Booking is Domestic
                    if (origin.CountryCode == "IN" && destination.CountryCode == "IN")
                    {
                        DataRow[] rows = dtSourceBaggage.Select("BaggageCode='INFT' AND IsDomestic=1");

                        if (rows != null && rows.Length > 0)
                        {
                            rateOfExchange = exchangeRates[rows[0]["BaggageCurrency"].ToString()];
                            InfantPrice = Convert.ToDecimal(rows[0]["BaggagePrice"]);
                        }
                    }
                    else//If International Booking
                    {
                        DataRow[] rows = dtSourceBaggage.Select("BaggageCode='INFT' AND IsDomestic=0");

                        if (rows != null && rows.Length > 0)
                        {
                            rateOfExchange = exchangeRates[rows[0]["BaggageCurrency"].ToString()];
                            InfantPrice = Convert.ToDecimal(rows[0]["BaggagePrice"]);
                        }
                    }
                }
                else//Return or Multi-Way
                {
                    Airport origin = new Airport(request.Segments[0].Origin);
                    Airport destination = new Airport(request.Segments[0].Destination);

                    //If Domestic Booking
                    if (origin.CountryCode == "IN" && destination.CountryCode == "IN")
                    {
                        DataRow[] rows = dtSourceBaggage.Select("BaggageCode='INFT' AND IsDomestic=1");

                        if (rows != null && rows.Length > 0)
                        {
                            rateOfExchange = exchangeRates[rows[0]["BaggageCurrency"].ToString()];
                            InfantPrice = (Convert.ToDecimal(rows[0]["BaggagePrice"]));
                        }
                    }
                    else//If International Booking
                    {
                        DataRow[] rows = dtSourceBaggage.Select("BaggageCode='INFT' AND IsDomestic=0");

                        if (rows != null && rows.Length > 0)
                        {
                            rateOfExchange = exchangeRates[rows[0]["BaggageCurrency"].ToString()];
                            InfantPrice = (Convert.ToDecimal(rows[0]["BaggagePrice"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Indigo)Failed to get Infant Fares. " + ex.ToString(), "");
            }

            return InfantPrice;
        }
        #endregion

        #region Tax Price Calculation

        /// <summary>
        /// This method calculates the tax price for Oneway and for Return flights either from the tax available in the table or from the Itenary response.
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="itineraryResponse"></param>
        /// <param name="taxComponent"></param>
        private void CalculateTax(ref SearchResult resultObj, List<decimal> savedTaxes)
        {
            try
            {
                #region Tax Calculation For OneWay Flights
                if (request.Type == SearchType.OneWay || request.SearchBySegments) //For one-one or normal oneway requests
                {
                    //Onward Journey Direct Flights
                    if (resultObj.Flights[0].Length == 1)
                    {
                        if (savedTaxes.Count > 0)
                        {
                            //Tax calculation based on the result available in the table
                            AssignTax(ref resultObj, savedTaxes);                           

                        }
                        else  //Tax Calculation from PriceItineraryResponse
                        {

                            string onwardFare = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            PriceItineraryResponse itineraryResponse = GetItineraryPriceForOneWayTrip(request, resultObj.JourneySellKey, resultObj.FareSellKey, resultObj, onwardFare);
                            if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                            {
                                rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                                RetriveTaxFromPriceItinerary(ref resultObj, itineraryResponse);
                            }
                        }
                    }
                    //Onward Journey Connecting or Via Flights
                    if (resultObj.Flights[0].Length > 1)
                    {

                        if (savedTaxes.Count > 0)
                        {
                            //Tax calculation based on the result available in the table
                            AssignTax(ref resultObj, savedTaxes);
                        }

                        else //tax calculation from price itinerary
                        {
                            string onwardFare_connecting = string.Empty;
                            for (int m = 0; m < resultObj.Flights[0].Length; m++)
                            {
                                if (resultObj.Flights[0][m].Status.ToUpper() != "VIA")
                                {
                                    if (!string.IsNullOrEmpty(onwardFare_connecting))
                                    {
                                        onwardFare_connecting += "_" + resultObj.Flights[0][1].FlightNumber + "_" + resultObj.FareType + "_" + Convert.ToString(resultObj.Flights[0][1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                    }
                                    else
                                    {
                                        onwardFare_connecting = resultObj.Flights[0][m].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][m].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                    }
                                }
                                else
                                {
                                    onwardFare_connecting = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                }
                            }

                            PriceItineraryResponse itineraryResponse = GetItineraryPriceForOneWayTrip(request, resultObj.JourneySellKey, resultObj.FareSellKey, resultObj, onwardFare_connecting);
                            if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                            {
                                rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                                RetriveTaxFromPriceItinerary(ref resultObj, itineraryResponse);
                            }

                        }
                    }
                }
                #endregion


                #region Tax Calculation For Return flights
                if (!request.SearchBySegments && request.Type == SearchType.Return) //For normal search
                {


                    if (resultObj.Flights[0].Length == 1 && resultObj.Flights[1].Length == 1)//Both onward and return journeys are direct flights.
                    {
                        if (savedTaxes.Count > 0)
                        {
                            //Tax calculation based on the resultObj available in the table
                            AssignTax(ref resultObj, savedTaxes);
                        }
                        else  //Tax Calculation from PriceItineraryResponse
                        {

                            string onwardFare = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            string returnFare = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_").Replace(" ", "_") + Convert.ToString(resultObj.Flights[1][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, onwardFare + returnFare);
                            if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                            {
                                rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                                RetriveTaxFromPriceItinerary(ref resultObj, itineraryResponse);
                            }
                        }

                    }
                    if (resultObj.Flights[0].Length > 1 && resultObj.Flights[1].Length > 1)//Both onward and return journeys may be connecting or via flights.
                    {

                        if (savedTaxes.Count > 0)
                        {
                            //Tax calculation based on the resultObj available in the table
                            AssignTax(ref resultObj, savedTaxes);
                        }
                        else  //Tax Calculation from PriceItineraryResponse
                        {

                            string connectingFareType = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            connectingFareType += "_" + Convert.ToString(resultObj.Flights[0][resultObj.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            string connectingFareType1 = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            connectingFareType1 += "_" + Convert.ToString(resultObj.Flights[1][resultObj.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, connectingFareType + connectingFareType1);
                            if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                            {
                                rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                                RetriveTaxFromPriceItinerary(ref resultObj, itineraryResponse);
                            }
                        }

                    }
                    //Onward may be connecting or via and return direct flights
                    if (resultObj.Flights[0].Length > 1 && resultObj.Flights[1].Length == 1)
                    {

                        if (savedTaxes.Count > 0)
                        {
                            //Tax calculation based on the resultObj available in the table
                            AssignTax(ref resultObj, savedTaxes);
                        }
                        else //Tax calculation from itinerary response
                        {

                            string connectingFareType = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            connectingFareType += "_" + Convert.ToString(resultObj.Flights[0][resultObj.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            string returnFare = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_").Replace(" ", "_") + Convert.ToString(resultObj.Flights[1][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, connectingFareType + returnFare);
                            if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                            {
                                rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                                RetriveTaxFromPriceItinerary(ref resultObj, itineraryResponse);
                            }
                        }

                    }
                    //Onward direct flight and return may be via or connecting flights
                    if (resultObj.Flights[0].Length == 1 && resultObj.Flights[1].Length > 1)
                    {

                        if (savedTaxes.Count > 0)
                        {
                            //Tax calculation based on the resultObj available in the table
                            AssignTax(ref resultObj, savedTaxes);
                        }
                        else //Tax calculation from itinerary response
                        {
                            string onwardFare = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            string connectingFareType1 = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            connectingFareType1 += "_" + Convert.ToString(resultObj.Flights[1][resultObj.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, onwardFare + connectingFareType1);
                            if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                            {
                                rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                                RetriveTaxFromPriceItinerary(ref resultObj, itineraryResponse);
                            }
                        }

                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Calculate TaxComponent For Flights. Reason : " + ex.ToString(), "");
            }
        }

        /// <summary>
        /// Gets the ItineraryPrice for Indigo flights.
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="request"></param>
        public void GetItineraryPriceForIndigoFlights(ref SearchResult resultObj, SearchRequest request)
        {
            try
            {
                //List which holds the Pax types  : "ADT","CHD" ,"INFT";
                List<string> FarePaxtypes = new List<string>();
                if (request.AdultCount > 0)
                {
                    FarePaxtypes.Add("ADT");
                }
                if (request.ChildCount > 0)
                {
                    FarePaxtypes.Add("CHD");
                }
                if (request.InfantCount > 0)
                {
                    FarePaxtypes.Add("INFT");
                }
                if (request.SearchBySegments || request.Type == SearchType.OneWay) //For one-one or normal oneway requests
                {
                    if (resultObj.Flights[0].Length == 1)//Onward Journey Direct Flights
                    {
                        string onwardFare = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        PriceItineraryResponse itineraryResponse = GetItineraryPriceForOneWayTrip(request, resultObj.JourneySellKey, resultObj.FareSellKey, resultObj, onwardFare);
                        if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                        {
                            rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                            resultObj = CalculateFareBreakdown(resultObj, FarePaxtypes, rateOfExchange, itineraryResponse, request);
                        }
                    }
                    if (resultObj.Flights[0].Length > 1) //Onward Journey Connecting or Via Flights
                    {
                        string onwardFare_connecting = string.Empty;
                        for (int m = 0; m < resultObj.Flights[0].Length; m++)
                        {
                            if (resultObj.Flights[0][m].Status.ToUpper() != "VIA")
                            {
                                if (!string.IsNullOrEmpty(onwardFare_connecting))
                                {
                                    onwardFare_connecting += "_" + resultObj.Flights[0][1].FlightNumber + "_" + resultObj.FareType + "_" + Convert.ToString(resultObj.Flights[0][1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                }
                                else
                                {
                                    onwardFare_connecting = resultObj.Flights[0][m].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][m].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                                }
                            }
                            else
                            {
                                onwardFare_connecting = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                            }
                        }

                        PriceItineraryResponse itineraryResponse = GetItineraryPriceForOneWayTrip(request, resultObj.JourneySellKey, resultObj.FareSellKey, resultObj, onwardFare_connecting);
                        if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                        {
                            rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                            resultObj = CalculateFareBreakdown(resultObj, FarePaxtypes, rateOfExchange, itineraryResponse, request);
                        }
                    }
                }
                if (!request.SearchBySegments && request.Type == SearchType.Return) // search type return for normal request
                {
                    if (resultObj.Flights[0].Length == 1 && resultObj.Flights[1].Length == 1)//Both onward and return journeys are direct flights.
                    {
                        string onwardFare = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        string returnFare = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_").Replace(" ", "_") + Convert.ToString(resultObj.Flights[1][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, onwardFare + returnFare);
                        if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                        {
                            rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                            resultObj = CalculateFareBreakdown(resultObj, FarePaxtypes, rateOfExchange, itineraryResponse, request);
                        }
                    }
                    if (resultObj.Flights[0].Length > 1 && resultObj.Flights[1].Length > 1)//Both onward and return journeys may be connecting or via flights.
                    {

                        string connectingFareType = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        connectingFareType += "_" + Convert.ToString(resultObj.Flights[0][resultObj.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        string connectingFareType1 = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        connectingFareType1 += "_" + Convert.ToString(resultObj.Flights[1][resultObj.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, connectingFareType + connectingFareType1);
                        if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                        {
                            rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                            resultObj = CalculateFareBreakdown(resultObj, FarePaxtypes, rateOfExchange, itineraryResponse, request);
                        }

                    }
                    //Onward may be connecting or via and return direct flights
                    if (resultObj.Flights[0].Length > 1 && resultObj.Flights[1].Length == 1)
                    {
                        string connectingFareType = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        connectingFareType += "_" + Convert.ToString(resultObj.Flights[0][resultObj.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        string returnFare = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_").Replace(" ", "_") + Convert.ToString(resultObj.Flights[1][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');

                        PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, connectingFareType + returnFare);
                        if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                        {
                            rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                            resultObj = CalculateFareBreakdown(resultObj, FarePaxtypes, rateOfExchange, itineraryResponse, request);
                        }

                    }
                    //Onward direct flight and return may be via or connecting flights
                    if (resultObj.Flights[0].Length == 1 && resultObj.Flights[1].Length > 1)
                    {
                        string onwardFare = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        string connectingFareType1 = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        connectingFareType1 += "_" + Convert.ToString(resultObj.Flights[1][resultObj.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                        PriceItineraryResponse itineraryResponse = GetItineraryPriceForRoundTrip(request, resultObj.JourneySellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[0], resultObj.FareSellKey.Split('|')[1], resultObj.JourneySellKey.Split('|')[1], resultObj, onwardFare + connectingFareType1);
                        if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                        {
                            rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                            resultObj = CalculateFareBreakdown(resultObj, FarePaxtypes, rateOfExchange, itineraryResponse, request);
                        }

                    }



                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                throw new BookingEngineException("(Indigo) No fare available");

            }
        }

        /// <summary>
        /// Gets the ItineraryPrice for one way journeys
        /// </summary>
        /// <param name="request"></param>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="journey"></param>
        /// <param name="onwardFare"></param>
        /// <returns></returns>
        protected PriceItineraryResponse GetItineraryPriceForOneWayTrip(SearchRequest request, string journeySellKey, string fareSellKey, SearchResult resultObj, string onwardFare)
        {


            LogonResponse loginResponse = new LogonResponse();
            loginResponse.Signature = resultObj.RepriceErrorMessage;//Till Search To Book This Signature Should be carried.
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature) && loginResponse.Signature.Length > 0)
            {
                IBookingManager bookingAPI = this.bookingAPI;

                int paxCount = 1;
                if (request.ChildCount > 0)
                {
                    paxCount++;
                }

                PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
                priceItinRequest.Signature = loginResponse.Signature;
                priceItinRequest.ContractVersion = contractVersion;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                priceItinRequest.EnableExceptionStackTrace = false;
                priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS = new PointOfSale();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = "AG";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.DomainCode = agentDomain;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.OrganizationCode = agentId;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(paxCount);
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[paxCount];

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[1];
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;


                if (request.AdultCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxType = "ADT";
                }
                //for (int i = 0; i < request.AdultCount; i++)
                //{
                //    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                //    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "ADT";
                //}


                if (request.ChildCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxType = "CHD";
                }

                //for (int i = 0; i < request.ChildCount; i++)
                //{
                //    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[request.AdultCount + i] = new PaxPriceType();
                //    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[request.AdultCount + i].PaxType = "CHD";
                //}


                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                if (!string.IsNullOrEmpty(promoCode))
                {
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes = new string[1];
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes[0] = "F";
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = promoCode;
                }
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoItineraryPriceRequest_" + appUserId.ToString() + "_" + onwardFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, priceItinRequest);
                    sw.Close();
                }
                catch { }


                try
                {
                    piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);

                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Booking));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoItineraryPriceResponse_" + appUserId.ToString() + "_" + onwardFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, piResponse.Booking);
                        sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
                }
                catch (Exception ex)
                {

                    Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                    throw new BookingEngineException("(Indigo) No fare available");
                }
            }
            else
            {
                throw new Exception("(Indigo)Failed to get the LogonResponse");
            }
            return piResponse;
        }

        /// <summary>
        /// Gets the ItineraryPrice for round trip journeys.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="fareSellKeyRet"></param>
        /// <param name="journeySellKeyRet"></param>
        /// <param name="resultObj"></param>
        /// <param name="onwardFare"></param>
        /// <returns></returns>
        protected PriceItineraryResponse GetItineraryPriceForRoundTrip(SearchRequest request, string journeySellKey, string fareSellKey, string fareSellKeyRet, string journeySellKeyRet, SearchResult resultObj, string onwardFare)
        {
            LogonResponse loginResponse = new LogonResponse();
            loginResponse.Signature = resultObj.RepriceErrorMessage;////Till Search To Book This Signature Should be carried.
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature) && loginResponse.Signature.Length > 0)
            {


                IBookingManager bookingAPI = this.bookingAPI;

                int paxCount = 1;
                if (request.ChildCount > 0)
                {
                    paxCount++;
                }


                PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
                priceItinRequest.Signature = loginResponse.Signature;
                priceItinRequest.ContractVersion = contractVersion;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                priceItinRequest.EnableExceptionStackTrace = false;
                priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;




                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(paxCount);
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[paxCount];

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS = new PointOfSale();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = "AG";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.DomainCode = agentDomain;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.OrganizationCode = agentId;


                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[2];
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = fareSellKey;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = journeySellKey;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].FareSellKey = fareSellKeyRet;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].JourneySellKey = journeySellKeyRet;
                if (request.AdultCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxType = "ADT";
                }
                if (request.ChildCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxType = "CHD";
                }

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                if (!string.IsNullOrEmpty(promoCode))
                {
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes = new string[1];
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes[0] = "F";
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = promoCode;
                }

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoPriceRequestRoundTrip_" + appUserId.ToString() + "_" + onwardFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";

                    //string filePath = xmlLogPath + "SGGetItineraryPriceRequest_" + journeykey + "_#_" + fareSellKey.Replace("~", "_") + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, priceItinRequest);
                    sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }


                try
                {
                    piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);

                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Booking));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoPriceResponseRoundTrip_" + appUserId.ToString() + "_" + onwardFare + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        //string filePath = xmlLogPath + "SGGetItineraryPriceResponse_" + journeykey + "_#_" + fareSellKey.Replace("~", "_") + "__" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, piResponse.Booking);
                        sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
                }
                catch (Exception ex)
                {

                    Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                    throw new BookingEngineException("(Indigo) No fare available");
                }
            }
            else
            {
                throw new Exception("(Indigo)Failed to get the LogonResponse");
            }


            return piResponse;
        }

        /// <summary>
        /// Retrieve the Tax from PriceItinerary both for one way and return flights and calculate the tax for the itinerary.
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="itineraryResponse"></param>
        /// <param name="taxComponent"></param>
        private void RetriveTaxFromPriceItinerary(ref SearchResult resultObj, PriceItineraryResponse itineraryResponse)
        {
            try
            {
                //For infant there will be no tax so we will calculate tax for only adult and child pax types.
                List<string> FarePaxtypes = new List<string>();
                if (request.AdultCount > 0)
                {
                    FarePaxtypes.Add("ADT");
                }
                if (request.ChildCount > 0)
                {
                    FarePaxtypes.Add("CHD");
                }

                int tripCount = 1;
                if (!request.SearchBySegments && request.Type == SearchType.Return) //search type is return and request is not one-one searh
                {
                    tripCount++;
                }
                //decimal newTaxPrice = 0; //variable which holds the new tax price.
                decimal adultTax = 0, childTax = 0;//Read the total adult, child taxes for adding in lstTaxDetails collection
                List<decimal> lstTaxDetails = new List<decimal>();//For assigning taxes pax type wise
                List<AirlineJourneyPriceDetails> lstPriceDetails = new List<AirlineJourneyPriceDetails>();//For saving segment wise and pax type wise taxes

                for (int journeyOnWardReturn = 0; journeyOnWardReturn < tripCount; journeyOnWardReturn++)
                {
                    #region FareBreakdown of adult and child
                    foreach (String passengerType in FarePaxtypes)
                    {
                        int journeySegCount = 0;
                        int onwardSegCount = 0;
                        int returnSegCount = 0;

                        for (int p = 0; p < tripCount; p++)
                        {
                            if (p == 0)  // For Onward Trips
                            {
                                for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                {
                                    if (resultObj.Flights[p][l].Status.ToUpper() != "VIA") // If the onward flights are not via 
                                    {
                                        onwardSegCount++;
                                    }
                                }
                                if (onwardSegCount == 0) // If the onward flights are direct or direct with via combination
                                {
                                    onwardSegCount = 1;
                                }
                            }
                            else
                            {
                                for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                {
                                    if (resultObj.Flights[p][l].Status.ToUpper() != "VIA")// If the return flights are not via 
                                    {
                                        returnSegCount++;
                                    }
                                }
                                if (returnSegCount == 0)// If the return flights are direct or direct with via combination
                                {
                                    returnSegCount = 1;
                                }

                            }
                        }

                        if (journeyOnWardReturn == 0)//Flag which determines whether to iterate over the onward segments or return segments.
                        {
                            journeySegCount = onwardSegCount; //Iterate over onward segments
                        }
                        else
                        {
                            journeySegCount = returnSegCount;//Iterate over return segments
                        }


                        //As the tax for both adult and child are same so we will sum up all the charges for Adult Pax type.
                        //Then calculate the tax component from the saved tax.
                        for (int k = 0; k < journeySegCount; k++)
                        {
                            if (itineraryResponse != null)
                            {
                                #region CalculateBaseFare and tax component of adult and child from itinerary response
                                foreach (PaxFare pfare in itineraryResponse.Booking.Journeys[journeyOnWardReturn].Segments[k].Fares[0].PaxFares)
                                {
                                    if (passengerType == pfare.PaxType)
                                    {
                                        Segment segment = itineraryResponse.Booking.Journeys[journeyOnWardReturn].Segments[k];

                                        AirlineJourneyPriceDetails priceDetails = new AirlineJourneyPriceDetails();
                                        priceDetails.AirlineCode = resultObj.Airline;
                                        priceDetails.Currency = itineraryResponse.Booking.CurrencyCode;
                                        priceDetails.DecimalPoint = agentDecimalValue;
                                        priceDetails.Destination = segment.ArrivalStation;
                                        priceDetails.Origin = segment.DepartureStation;
                                       // priceDetails.PaxType = passengerType;

                                        if (request.Type == SearchType.OneWay)
                                        {
                                            priceDetails.Type = SearchType.OneWay;
                                        }
                                        else
                                        {
                                            priceDetails.Type = SearchType.Return;
                                        }

                                        foreach (BookingServiceCharge charge in segment.Fares[0].PaxFares[0].ServiceCharges)
                                        {
                                            #region Tax Component Calculation For Adult and Child
                                            switch (passengerType)
                                            {
                                                case "ADT":                                                
                                                    switch (charge.ChargeType)
                                                    {
                                                        case Indigo.BookingMgr.ChargeType.TravelFee:
                                                        case Indigo.BookingMgr.ChargeType.Tax:
                                                        case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                        case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                        case Indigo.BookingMgr.ChargeType.Calculated:
                                                        case Indigo.BookingMgr.ChargeType.Note:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                        case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                        case Indigo.BookingMgr.ChargeType.Loyalty:
                                                        case Indigo.BookingMgr.ChargeType.FarePoints:
                                                        case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                        case Indigo.BookingMgr.ChargeType.Unmapped:

                                                            adultTax += charge.Amount; //Summing up all the new tax prices.
                                                            priceDetails.Tax += charge.Amount;
                                                            break;
                                                    }
                                                    break;
                                                case "CHD":
                                                    switch (charge.ChargeType)
                                                    {
                                                        case Indigo.BookingMgr.ChargeType.TravelFee:
                                                        case Indigo.BookingMgr.ChargeType.Tax:
                                                        case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                        case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                        case Indigo.BookingMgr.ChargeType.Calculated:
                                                        case Indigo.BookingMgr.ChargeType.Note:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                        case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                        case Indigo.BookingMgr.ChargeType.Loyalty:
                                                        case Indigo.BookingMgr.ChargeType.FarePoints:
                                                        case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                        case Indigo.BookingMgr.ChargeType.Unmapped:

                                                            childTax += charge.Amount; //Summing up all the new tax prices.
                                                            priceDetails.Tax += charge.Amount;
                                                            break;
                                                    }
                                                    break;

                                            }
                                            #endregion
                                        }

                                        lstPriceDetails.Add(priceDetails);
                                    }
                                }
                                #endregion

                            }

                        }
                    }
                    #endregion
                }


                //save the tax price.
                try
                {
                    //if (newTaxPrice > 0)
                    //{
                    //    AirlineJourneyPriceDetails priceDetails = new AirlineJourneyPriceDetails();
                    //    priceDetails.AirlineCode = resultObj.Airline;
                    //    priceDetails.Currency = itineraryResponse.Booking.CurrencyCode;
                    //    priceDetails.DecimalPoint = agentDecimalValue;
                    //    priceDetails.Destination = request.Segments[0].Destination;
                    //    priceDetails.Origin = request.Segments[0].Origin;
                    //    priceDetails.Tax = newTaxPrice;
                    //    if (request.Type == SearchType.OneWay)
                    //    {
                    //        priceDetails.Type = SearchType.OneWay;
                    //    }
                    //    else
                    //    {
                    //        priceDetails.Type = SearchType.Return;
                    //    }
                    //    priceDetails.Save();
                    //}

                    //Add total adult & child taxes in the collection for assigning the result
                    lstTaxDetails.Add(adultTax);
                    lstTaxDetails.Add(childTax);

                    //Save the tax values for segment & pax type wise details
                    //lstPriceDetails.ForEach(p => p.Save()); // Not saving tax in bke_airline_journey_pricedetails table
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to save Tax FromPriceItinerary. Reason : " + ex.ToString(), "");
                }

                //Calculation of tax from the tax component.
                if (lstPriceDetails.Count > 0)
                {
                    AssignTax(ref resultObj, lstTaxDetails);
                }

                //Baggage Calculation Added default baggage for VIA flights to BaggageIncludedInFare 
                if (itineraryResponse != null && itineraryResponse.Booking != null && itineraryResponse.Booking.Journeys != null && itineraryResponse.Booking.Journeys.Length > 0)
                {

                    if (itineraryResponse.Booking.Journeys[0] != null && itineraryResponse.Booking.Journeys[0].Segments != null && itineraryResponse.Booking.Journeys[0].Segments.Length > 0 && itineraryResponse.Booking.Journeys[0].Segments[0] != null && itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                    {
                        resultObj.BaggageIncludedInFare = itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                    }
                    for (int k = 1; k < resultObj.Flights[0].Length; k++)
                    {
                        if (resultObj.Flights[0][k].Status.ToUpper() != "VIA")//If the onward flights are not via
                        {
                            if (itineraryResponse.Booking.Journeys[0] != null && itineraryResponse.Booking.Journeys[0].Segments != null && itineraryResponse.Booking.Journeys[0].Segments.Length > 0 && itineraryResponse.Booking.Journeys[0].Segments[k] != null && itineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                            {
                                resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                            }
                        }
                        else //For VIA Flights adding default baggage segment wise
                        {
                            if (itineraryResponse.Booking.Journeys[0] != null && itineraryResponse.Booking.Journeys[0].Segments != null && itineraryResponse.Booking.Journeys[0].Segments.Length > 0 && itineraryResponse.Booking.Journeys[0].Segments[0] != null && itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                            {
                                resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                            }
                        }
                    }
                    if (!request.SearchBySegments && request.Type == SearchType.Return) //search type is return and request is not one-one searh
                    {
                        if (itineraryResponse.Booking.Journeys[1] != null && itineraryResponse.Booking.Journeys[1].Segments != null && itineraryResponse.Booking.Journeys[1].Segments.Length > 0 && itineraryResponse.Booking.Journeys[1].Segments[0] != null && itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                        {
                            resultObj.BaggageIncludedInFare += "|" + itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                        }

                        for (int k = 1; k < resultObj.Flights[1].Length; k++)
                        {
                            if (resultObj.Flights[1][k].Status.ToUpper() != "VIA")//If the return flights are not via
                            {
                                if (itineraryResponse.Booking.Journeys[1] != null && itineraryResponse.Booking.Journeys[1].Segments != null && itineraryResponse.Booking.Journeys[1].Segments.Length > 0 && itineraryResponse.Booking.Journeys[1].Segments[k] != null && itineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments != null  && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                                {
                                    resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                }
                            }
                            else //For VIA Flights adding default baggage segment wise
                            {
                                if (itineraryResponse.Booking.Journeys[1] != null && itineraryResponse.Booking.Journeys[1].Segments != null && itineraryResponse.Booking.Journeys[1].Segments.Length > 0 && itineraryResponse.Booking.Journeys[1].Segments[0] != null && itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                                {
                                    resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                }
                            }
                        }
                    }

                }

                resultObj.Price.SupplierCurrency = currencyCode;
                resultObj.BaseFare = (double)Math.Round(resultObj.Price.PublishedFare, agentDecimalValue);
                resultObj.Tax = (double)Math.Round(resultObj.Price.Tax, agentDecimalValue);
                resultObj.TotalFare = resultObj.BaseFare + resultObj.Tax;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Calculate Tax FromPriceItinerary. Reason : " + ex.ToString(), "");

            }
        }

        /// <summary>
        /// Retrieve the Tax from the table BKE_Airline_Journey_Price_Details and calculate the tax for the itinerary.
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="taxComponent"></param>
        private void AssignTax(ref SearchResult resultObj, List<decimal> savedTaxes)
        {
            try
            {
                #region Old Code
                //if (taxComponent > 0)
                //{
                //    //For infant there will be no tax so we will calculate tax for only adult and child pax types.
                //    List<string> FarePaxtypes = new List<string>();
                //    if (request.AdultCount > 0)
                //    {
                //        FarePaxtypes.Add("ADT");
                //    }
                //    if (request.ChildCount > 0)
                //    {
                //        FarePaxtypes.Add("CHD");
                //    }


                //    int tripCount = 1;
                //    if (!request.SearchBySegments && request.Type == SearchType.Return) //search type is return and request is not one-one searh
                //    {
                //        tripCount++;
                //    }
                //    if (!request.SearchBySegments && request.Type == SearchType.Return) //search type is return and request is not one-one searh
                //    {
                //        //the tax we save in the table is of both (onward + return).
                //        // so will break up the sum into two equal halves.
                //        taxComponent = taxComponent / 2;
                //    }


                //    for (int m = 0; m < tripCount; m++)
                //    {
                //        foreach (String passengerType in FarePaxtypes)
                //        {
                //            switch (passengerType)
                //            {
                //                case "ADT":
                //                    double taxAdult = (double)((taxComponent * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                //                    resultObj.FareBreakdown[0].TotalFare += taxAdult;
                //                    resultObj.FareBreakdown[0].SupplierFare += (double)(taxComponent * resultObj.FareBreakdown[0].PassengerCount);
                //                    resultObj.Price.Tax += (decimal)taxAdult;
                //                    resultObj.Price.SupplierPrice += taxComponent;
                //                    break;
                //                case "CHD":
                //                    double taxChild = (double)((taxComponent * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                //                    resultObj.FareBreakdown[1].TotalFare += taxChild;
                //                    resultObj.FareBreakdown[1].SupplierFare += (double)(taxComponent * resultObj.FareBreakdown[1].PassengerCount);
                //                    resultObj.Price.Tax += (decimal)taxChild;
                //                    resultObj.Price.SupplierPrice += taxComponent;
                //                    break;
                //            }
                //        }

                //    }
                //    resultObj.Price.SupplierCurrency = currencyCode;
                //    resultObj.BaseFare = (double)resultObj.Price.PublishedFare;
                //    resultObj.Tax = (double)resultObj.Price.Tax;
                //    resultObj.TotalFare = resultObj.BaseFare + resultObj.Tax;
                //} 
                #endregion

                if (savedTaxes.Count > 0)
                {
                    SearchResult result = resultObj;
                    resultObj.FareBreakdown.ToList().ForEach(f =>
                    {
                        if (f.PassengerType == PassengerType.Adult)
                        {
                            double taxAdult = (double)((savedTaxes[0] * rateOfExchange) * f.PassengerCount);
                            f.TotalFare += taxAdult;
                            f.SupplierFare += (double)(savedTaxes[0] * f.PassengerCount);
                            result.Price.Tax += (decimal)taxAdult;
                            result.Price.SupplierPrice += savedTaxes[0];
                        };
                        if (f.PassengerType == PassengerType.Child && savedTaxes.Count == 2)
                        {
                            double taxAdult = (double)((savedTaxes[1] * rateOfExchange) * f.PassengerCount);
                            f.TotalFare += taxAdult;
                            f.SupplierFare += (double)(savedTaxes[1] * f.PassengerCount);
                            result.Price.Tax += (decimal)taxAdult;
                            result.Price.SupplierPrice += savedTaxes[1];
                        };
                    });

                    resultObj.Price.SupplierCurrency = currencyCode;
                    resultObj.BaseFare = (double)resultObj.Price.PublishedFare;
                    resultObj.Tax = (double)resultObj.Price.Tax;
                    resultObj.TotalFare = resultObj.BaseFare + resultObj.Tax;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Calculate Tax. Reason : " + ex.ToString(), "");

            }

        }

        /// <summary>
        /// Calculates the fare break down 
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="FarePaxtypes"></param>
        /// <param name="rateOfExchange"></param>
        /// <param name="itineraryResponse"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchResult CalculateFareBreakdown(SearchResult resultObj, List<String> FarePaxtypes, decimal rateOfExchange, PriceItineraryResponse itineraryResponse, SearchRequest request)
        {
            try
            {
                List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
                List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

                paxTypeTaxBreakUp.Clear();
                adtPaxTaxBreakUp.Clear();
                chdPaxTaxBreakUp.Clear();
                inftPaxTaxBreakUp.Clear();

                //decimal newTaxPrice = 0; //variable which holds the new tax price.
                decimal adultTax = 0, childTax = 0;//Read the total adult, child taxes for adding in lstTaxDetails collection
                List<decimal> lstTaxDetails = new List<decimal>();//For assigning taxes pax type wise
                List<AirlineJourneyPriceDetails> lstPriceDetails = new List<AirlineJourneyPriceDetails>();//For saving segment wise and pax type wise taxes


                resultObj.Price = new PriceAccounts();
                int tripOneWayReturn = 1;
                if (!request.SearchBySegments && request.Type == SearchType.Return) //search type is return and request is not one-one searh
                {
                    tripOneWayReturn++;
                }
                //decimal newTaxPrice = 0;
                for (int jourOnWardReturn = 0; jourOnWardReturn < tripOneWayReturn; jourOnWardReturn++)
                {
                    #region FareBreakdown of adult and child
                    foreach (String passengerType in FarePaxtypes)
                    {
                        switch (passengerType)
                        {
                            case "ADT": //Adult
                                if (resultObj.FareBreakdown[0] == null)
                                {
                                    resultObj.FareBreakdown[0] = new Fare();
                                }
                                resultObj.FareBreakdown[0].PassengerCount = request.AdultCount;
                                resultObj.FareBreakdown[0].PassengerType = PassengerType.Adult;
                                break;
                            case "CHD": //Child
                                if (resultObj.FareBreakdown[1] == null)
                                {
                                    resultObj.FareBreakdown[1] = new Fare();
                                }
                                resultObj.FareBreakdown[1].PassengerCount = request.ChildCount;
                                resultObj.FareBreakdown[1].PassengerType = PassengerType.Child;
                                break;

                        }

                        //Infant Count-- if both children and infants exists in the request.
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            if (resultObj.FareBreakdown[2] == null)
                            {
                                resultObj.FareBreakdown[2] = new Fare();
                            }
                            resultObj.FareBreakdown[2].PassengerCount = request.InfantCount;
                            resultObj.FareBreakdown[2].PassengerType = PassengerType.Infant;
                        }

                        //Child Count -- If only infant count exists in the rquest.
                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                        {
                            if (resultObj.FareBreakdown[1] == null)
                            {
                                resultObj.FareBreakdown[1] = new Fare();
                            }
                            resultObj.FareBreakdown[1].PassengerCount = request.InfantCount;
                            resultObj.FareBreakdown[1].PassengerType = PassengerType.Infant;
                        }
                        if (itineraryResponse != null)
                        {
                            int journeySegCount = 0;
                            int onwardSegCount = 0;
                            int returnSegCount = 0;

                            for (int p = 0; p < tripOneWayReturn; p++)
                            {
                                if (p == 0) //For onward
                                {
                                    for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                    {
                                        if (resultObj.Flights[p][l].Status.ToUpper() != "VIA") //If the onward flights are not via
                                        {
                                            onwardSegCount++;
                                        }
                                    }
                                    if (onwardSegCount == 0)//If the onward flights are direct or direct with via combination
                                    {
                                        onwardSegCount = 1;
                                    }
                                }
                                else
                                {
                                    for (int l = 0; l < resultObj.Flights[p].Length; l++)
                                    {
                                        if (resultObj.Flights[p][l].Status.ToUpper() != "VIA")//If the return flights are not via
                                        {
                                            returnSegCount++;
                                        }
                                    }
                                    if (returnSegCount == 0)//If the return flights are direct or direct with via combination
                                    {
                                        returnSegCount = 1;
                                    }

                                }
                            }
                            if (jourOnWardReturn == 0) //Flag which determines wheether to iterate over the onward segments or return segments
                            {
                                journeySegCount = onwardSegCount; //Onward segments iteration
                            }
                            else
                            {
                                journeySegCount = returnSegCount;//return segments iteration
                            }


                            for (int k = 0; k < itineraryResponse.Booking.Journeys[jourOnWardReturn].Segments.Length; k++)
                            {

                                Segment segment = itineraryResponse.Booking.Journeys[jourOnWardReturn].Segments[k];

                                //We need to combine fares of all the segments from the price itenary response

                                foreach (PaxFare pfare in segment.Fares[0].PaxFares)
                                {
                                    if (passengerType == pfare.PaxType)
                                    {                                        
                                        AirlineJourneyPriceDetails priceDetails = new AirlineJourneyPriceDetails();
                                        priceDetails.AirlineCode = resultObj.Airline;
                                        priceDetails.Currency = itineraryResponse.Booking.CurrencyCode;
                                        priceDetails.DecimalPoint = agentDecimalValue;
                                        priceDetails.Destination = segment.ArrivalStation;
                                        priceDetails.Origin = segment.DepartureStation;
                                        //priceDetails.PaxType = passengerType;

                                        if (request.Type == SearchType.OneWay)
                                        {
                                            priceDetails.Type = SearchType.OneWay;
                                        }
                                        else
                                        {
                                            priceDetails.Type = SearchType.Return;
                                        }

                                        foreach (BookingServiceCharge charge in segment.Fares[0].PaxFares[0].ServiceCharges)
                                        {
                                            switch (passengerType)
                                            {
                                                case "ADT":

                                                    switch (charge.ChargeType)
                                                    {
                                                        case Indigo.BookingMgr.ChargeType.FarePrice:
                                                            double baseFareAdult = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                                                            resultObj.FareBreakdown[0].BaseFare += baseFareAdult;
                                                            resultObj.FareBreakdown[0].TotalFare += baseFareAdult;
                                                            resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                            resultObj.Price.PublishedFare += (decimal)baseFareAdult;
                                                            resultObj.Price.SupplierPrice += charge.Amount;

                                                            break;

                                                        case Indigo.BookingMgr.ChargeType.PromotionDiscount:
                                                            double discountAdult = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                                                            resultObj.FareBreakdown[0].BaseFare = resultObj.FareBreakdown[0].BaseFare - discountAdult;
                                                            resultObj.FareBreakdown[0].TotalFare = resultObj.FareBreakdown[0].TotalFare - discountAdult;
                                                            resultObj.FareBreakdown[0].SupplierFare = resultObj.FareBreakdown[0].SupplierFare - (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                                                            resultObj.Price.PublishedFare = resultObj.Price.PublishedFare - Convert.ToDecimal(discountAdult);
                                                            resultObj.Price.SupplierPrice = resultObj.Price.SupplierPrice - charge.Amount;
                                                            break;
                                                        case Indigo.BookingMgr.ChargeType.TravelFee:
                                                        case Indigo.BookingMgr.ChargeType.Tax:
                                                        case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                        case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                        case Indigo.BookingMgr.ChargeType.Calculated:
                                                        case Indigo.BookingMgr.ChargeType.Note:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                        case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                        case Indigo.BookingMgr.ChargeType.Loyalty:
                                                        case Indigo.BookingMgr.ChargeType.FarePoints:
                                                        case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                        case Indigo.BookingMgr.ChargeType.Unmapped:
                                                            if (charge.ChargeCode.ToString().Contains("GST"))
                                                            {
                                                               resultObj.Price.K3Tax += ((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                                                            }
                                                            //newTaxPrice += charge.Amount;
                                                            adultTax += charge.Amount;
                                                            priceDetails.Tax += charge.Amount;
                                                            adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                            break;
                                                    }

                                                    break;

                                                case "CHD":

                                                    switch (charge.ChargeType)
                                                    {
                                                        case Indigo.BookingMgr.ChargeType.FarePrice:
                                                            double baseFareChd = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                                                            resultObj.FareBreakdown[1].BaseFare += baseFareChd;
                                                            resultObj.FareBreakdown[1].TotalFare += baseFareChd;
                                                            resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                            resultObj.Price.PublishedFare += (decimal)baseFareChd;
                                                            resultObj.Price.SupplierPrice += charge.Amount;
                                                            
                                                            break;

                                                        case Indigo.BookingMgr.ChargeType.PromotionDiscount:
                                                            double discountChild = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                                                            resultObj.FareBreakdown[1].BaseFare = resultObj.FareBreakdown[1].BaseFare - discountChild;
                                                            resultObj.FareBreakdown[1].TotalFare = resultObj.FareBreakdown[1].TotalFare - discountChild;
                                                            resultObj.FareBreakdown[1].SupplierFare = resultObj.FareBreakdown[1].SupplierFare - (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                                                            resultObj.Price.PublishedFare = resultObj.Price.PublishedFare - Convert.ToDecimal(discountChild);
                                                            resultObj.Price.SupplierPrice = resultObj.Price.SupplierPrice - charge.Amount;
                                                            break;
                                                        case Indigo.BookingMgr.ChargeType.TravelFee:
                                                        case Indigo.BookingMgr.ChargeType.Tax:
                                                        case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                        case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                        case Indigo.BookingMgr.ChargeType.Calculated:
                                                        case Indigo.BookingMgr.ChargeType.Note:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                        case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                        case Indigo.BookingMgr.ChargeType.Loyalty:
                                                        case Indigo.BookingMgr.ChargeType.FarePoints:
                                                        case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                        case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                        case Indigo.BookingMgr.ChargeType.Unmapped:
                                                            if (charge.ChargeCode.ToString().Contains("GST"))
                                                            {
                                                               resultObj.Price.K3Tax += ((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                                                            }
                                                            childTax += charge.Amount;
                                                            priceDetails.Tax += charge.Amount;
                                                            chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                            break;
                                                    }


                                                    break;

                                            }
                                        }

                                        lstPriceDetails.Add(priceDetails);
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    //Infant fare break down
                    if (request.InfantCount > 0)
                    {
                        decimal infantPrice = 0;
                        infantPrice = GetInfantAvailibilityFares(request);
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            double baseFare = (double)((infantPrice * rateOfExchange) * resultObj.FareBreakdown[2].PassengerCount);
                            resultObj.FareBreakdown[2].TotalFare += baseFare;
                            resultObj.FareBreakdown[2].BaseFare += baseFare;
                            resultObj.FareBreakdown[2].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[2].PassengerCount);
                            resultObj.Price.PublishedFare += (decimal)baseFare;
                            resultObj.Price.SupplierPrice += infantPrice;
                        }
                        else if (request.ChildCount <= 0 && request.InfantCount > 0)
                        {
                            double baseFare = (double)((infantPrice * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                            resultObj.FareBreakdown[1].BaseFare += baseFare;
                            resultObj.FareBreakdown[1].TotalFare += baseFare;
                            resultObj.FareBreakdown[1].SupplierFare += (double)(infantPrice * resultObj.FareBreakdown[1].PassengerCount);
                            resultObj.Price.PublishedFare += (decimal)baseFare;
                            resultObj.Price.SupplierPrice += infantPrice;
                        }
                    }

                }

                //Here we will update the latest tax price.
                try
                {
                    //if (newTaxPrice > 0)
                    //{
                    //    AirlineJourneyPriceDetails priceDetails = new AirlineJourneyPriceDetails();
                    //    priceDetails.AirlineCode = resultObj.Airline;
                    //    priceDetails.Currency = itineraryResponse.Booking.CurrencyCode;
                    //    priceDetails.DecimalPoint = agentDecimalValue;
                    //    priceDetails.Destination = request.Segments[0].Destination;
                    //    priceDetails.Origin = request.Segments[0].Origin;
                    //    priceDetails.Tax = newTaxPrice;

                    //    if (request.Type == SearchType.OneWay)
                    //    {
                    //        priceDetails.Type = SearchType.OneWay;
                    //    }
                    //    else
                    //    {
                    //        priceDetails.Type = SearchType.Return;
                    //    }
                    //    priceDetails.Save();
                    //}

                    //Add total adult & child taxes in the collection for assigning the result
                    lstTaxDetails.Add(adultTax);
                    lstTaxDetails.Add(childTax);

                    //Save the tax values for segment & pax type wise details
                    //lstPriceDetails.ForEach(p => p.Save());
                }
                catch (Exception ex)
                {
                    Audit.Add(Core.EventType.Search, Core.Severity.High, appUserId, "(Indigo)Failed to save tax details : Error" + ex.ToString() + DateTime.Now, "");
                }              

                //Calculation of tax from the tax component.
                if (lstPriceDetails.Count > 0)
                {
                    this.request = request; //SearchRequest assignment for tax calculation
                    AssignTax(ref resultObj, lstTaxDetails);
                }

                //Baggage Calculation Added default baggage for VIA flights to BaggageIncludedInFare .
                if (itineraryResponse != null && itineraryResponse.Booking != null && itineraryResponse.Booking.Journeys != null && itineraryResponse.Booking.Journeys.Length > 0)
                {
                    if (itineraryResponse.Booking.Journeys[0] != null && itineraryResponse.Booking.Journeys[0].Segments != null && itineraryResponse.Booking.Journeys[0].Segments.Length > 0 && itineraryResponse.Booking.Journeys[0].Segments[0] != null && itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                    {
                        resultObj.BaggageIncludedInFare = itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                    }

                    for (int k = 1; k < resultObj.Flights[0].Length; k++)
                    {
                        if (resultObj.Flights[0][k].Status.ToUpper() != "VIA")
                        {
                            if (itineraryResponse.Booking.Journeys[0] != null && itineraryResponse.Booking.Journeys[0].Segments != null && itineraryResponse.Booking.Journeys[0].Segments.Length > 0 && itineraryResponse.Booking.Journeys[0].Segments[k] != null && itineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                            {
                                resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[0].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                            }
                        }
                        else  //For VIA Flights adding default baggage segment wise
                        {
                            if (itineraryResponse.Booking.Journeys[0] != null && itineraryResponse.Booking.Journeys[0].Segments != null && itineraryResponse.Booking.Journeys[0].Segments.Length > 0 && itineraryResponse.Booking.Journeys[0].Segments[0] != null && itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                            {
                                resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[0].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                            }
                        }
                    }
                    if (!request.SearchBySegments && request.Type == SearchType.Return) //search type is return and request is not one-one searh
                    {
                        if (itineraryResponse.Booking.Journeys[1] != null && itineraryResponse.Booking.Journeys[1].Segments != null && itineraryResponse.Booking.Journeys[1].Segments.Length > 0 && itineraryResponse.Booking.Journeys[1].Segments[0] != null && itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                        {
                            resultObj.BaggageIncludedInFare += "|" + itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                        }
                        for (int k = 1; k < resultObj.Flights[1].Length; k++)
                        {
                            if (resultObj.Flights[1][k].Status.ToUpper() != "VIA")
                            {
                                if (itineraryResponse.Booking.Journeys[1] != null && itineraryResponse.Booking.Journeys[1].Segments != null && itineraryResponse.Booking.Journeys[1].Segments.Length > 0 && itineraryResponse.Booking.Journeys[1].Segments[k] != null && itineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                                {
                                    resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[1].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                }
                            }
                            else //For VIA Flights adding default baggage segment wise
                            {
                                if (itineraryResponse.Booking.Journeys[1] != null && itineraryResponse.Booking.Journeys[1].Segments != null && itineraryResponse.Booking.Journeys[1].Segments.Length > 0 && itineraryResponse.Booking.Journeys[1].Segments[0] != null && itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                                {
                                    resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[1].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                }
                            }
                        }
                    }
                }
                if (adtPaxTaxBreakUp.Count > 0)
                {
                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                }
                if (chdPaxTaxBreakUp.Count > 0)
                {
                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                }
                if (inftPaxTaxBreakUp.Count > 0)
                {
                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                }
                if (paxTypeTaxBreakUp.Count > 0)
                {
                    resultObj.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                }
                resultObj.Price.SupplierCurrency = currencyCode;
                resultObj.BaseFare = (double)resultObj.Price.PublishedFare;
                resultObj.Tax = (double)resultObj.Price.Tax;
                resultObj.TotalFare = (double)Math.Round((resultObj.BaseFare + resultObj.Tax), agentDecimalValue);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Calculate fare break down. Reason : " + ex.ToString(), "");
            }
            return resultObj;
        }

        #endregion

        #endregion

        #region Special service requests like baggage and infant calculation

        /// <summary>
        /// Returns baggage options available for the result.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public DataTable GetBaggage(SearchResult result)
        {
            DataTable dtSourceBaggage = SourceBaggage.GetSourceBaggages(BookingSource.Indigo, currencyCode);

            rateOfExchange = exchangeRates[dtSourceBaggage.Rows[0]["BaggageCurrency"].ToString()];

            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    DataRow[] baggages = new DataRow[0];
                    if (result.Flights[0][0].Origin.CountryCode == "IN" && result.Flights[0][result.Flights[0].Length - 1].Destination.CountryCode == "IN")
                    {
                        baggages = dtSourceBaggage.Select("IsDomestic=1 AND BaggageCode <> 'INFT'");
                    }
                    else
                    {
                        baggages = dtSourceBaggage.Select("IsDomestic=0 AND BaggageCode <> 'INFT'");
                    }

                    if (!result.Flights[i][j].SegmentFareType.Contains("HandBaggage"))
                    {
                        foreach (DataRow row in baggages)
                        {
                            DataRow dr = dtBaggage.NewRow();
                            dr["Code"] = row["BaggageCode"];
                            dr["Price"] = Math.Round(Convert.ToDecimal(row["BaggagePrice"]) * rateOfExchange, agentDecimalValue);
                            dr["Group"] = result.Flights[i][j].Group;
                            dr["Currency"] = agentBaseCurrency;
                            switch (row["BaggageCode"].ToString())
                            {
                                case "XBPA":
                                    dr["Description"] = "Prepaid Excess Baggage – 5 Kg";
                                    break;
                                case "XBPB":
                                    dr["Description"] = "Prepaid Excess Baggage – 10 Kg";
                                    break;
                                case "XBPC":
                                    dr["Description"] = "Prepaid Excess Baggage – 15 Kg";
                                    break;
                                case "XBPD":
                                    dr["Description"] = "Prepaid Excess Baggage – 30 Kg";
                                    break;

                            }
                            dr["QtyAvailable"] = 5000;
                            dtBaggage.Rows.Add(dr);
                        }
                    }
                }
            }

            dtBaggage.DefaultView.Sort = "Code ASC,Group ASC";

            return dtBaggage;

        }

        /// <summary>
        /// This method returns the Seat Availability response based on the segments.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public SeatAvailabilityResp GetSeatAvailableSSR(SearchResult result, string sOrigin, string sDestination,
            SeatAvailabilityResp clsSeatAvailabilityResp, string session)
        {
            SegmentSeats clsSegmentSeats = new SegmentSeats();
            LogonResponse loginResponse = new LogonResponse();
            try
            {
                //Step1: Assign API credentails and Login
                if (result != null && !string.IsNullOrEmpty(result.RepriceErrorMessage) && result.RepriceErrorMessage.Length > 0)//For One-One Search
                {
                    //Here the signature is different for 2 different results
                    //So from the result object we need to assign here
                    //till seaech to book unique signature should be carried.
                    loginResponse.Signature = result.RepriceErrorMessage;
                }
                else//For Normal Search 
                {
                    loginResponse = Login();
                }
                if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature) && loginResponse.Signature.Length > 0)
                {
                    SegmentSeats seg = clsSeatAvailabilityResp.SegmentSeat.Where(x => x.Origin == sOrigin && x.Destination == sDestination).FirstOrDefault();

                    GetSeatAvailabilityRequest clsGetSeatAvailabilityRequest = new GetSeatAvailabilityRequest();

                    clsGetSeatAvailabilityRequest.ContractVersion = contractVersion;
                    clsGetSeatAvailabilityRequest.Signature = loginResponse.Signature;

                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest = new SeatAvailabilityRequest();
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.STD = seg.STD;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.DepartureStation = seg.Origin;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.ArrivalStation = seg.Destination;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.IncludeSeatFees = true;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.SeatAssignmentMode = SeatAssignmentMode.PreSeatAssignment;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.FlightNumber = seg.FlightNo;
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.CarrierCode = "6E";
                    clsGetSeatAvailabilityRequest.SeatAvailabilityRequest.CollectedCurrencyCode = AgentBaseCurrency;

                    GenericStatic.WriteLogFile(typeof(GetSeatAvailabilityRequest), clsGetSeatAvailabilityRequest,
                        xmlLogPath + sessionId + "_" + appUserId + "_" + seg.Origin + "_" + seg.Destination + "_" + "6E_GetSeatAvailabilityRequest_");

                    GetSeatAvailabilityResponse clsGetSeatAvailabilityResponse = bookingAPI.GetSeatAvailability(clsGetSeatAvailabilityRequest);

                    string filePath = GenericStatic.WriteLogFile(typeof(GetSeatAvailabilityResponse), clsGetSeatAvailabilityResponse,
                        xmlLogPath + sessionId + "_" + appUserId + "_" + seg.Origin + "_" + seg.Destination + "_" + "6E_GetSeatAvailabilityResponse_");

                    clsSegmentSeats = GenericStatic.TransformXMLAndDeserialize(clsSegmentSeats, File.ReadAllText(filePath), sSeatAvailResponseXSLT);
                    clsSegmentSeats.SeatInfoDetails = clsSegmentSeats.SeatInfoDetails.GroupBy(x => x.SeatNo).Select(y => y.First()).ToList();

                    /* To restrict seat no 1A for domestic journey */
                    Airport Origin = new Airport(sOrigin);
                    Airport Destination = new Airport(sDestination);
                    if (Origin.CountryCode == Destination.CountryCode)
                        clsSegmentSeats.SeatInfoDetails.Where(x => x.SeatNo == "1A").ToList().ForEach(y => y.AvailablityType = "Restricted");

                    var itemIndex = clsSeatAvailabilityResp.SegmentSeat.FindIndex(x => x.Origin == sOrigin && x.Destination == sDestination);
                    clsSeatAvailabilityResp.SegmentSeat.RemoveAt(itemIndex);
                    clsSeatAvailabilityResp.SegmentSeat.Insert(itemIndex, clsSegmentSeats);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(Indigo)Failed to get seat availability response. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return clsSeatAvailabilityResp;
        }


        /// <summary>
        /// Used to book the Itinerary if any Infant passengers are included in the Itinerary.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>        
        public decimal SellSSRForInfant(FlightItinerary itinerary, string signature)
        {
            decimal bookingAmount = 0;
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                SellRequest request = new SellRequest();
                request.ContractVersion = contractVersion;
                request.Signature = signature;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                request.EnableExceptionStackTrace = false;

                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.SSR;
                requestData.SellSSR = new SellSSR();
                requestData.SellSSR.SSRRequest = new SSRRequest();
                requestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                requestData.SellSSR.SSRRequest.CurrencyCode = currencyCode;
                requestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;
                requestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[itinerary.Segments.Length];

                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = "6E";
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = itinerary.Segments[i].FlightNumber.Replace("6E", "").Trim();
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = itinerary.Segments[i].DepartureTime;
                    //requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = new PaxSSR[itinerary.Passenger.Length];
                    //if (i == 0)
                    {
                        int counter = 0;
                        List<PaxSSR> PaxSSRs = new List<PaxSSR>();
                        for (int j = 0; j < itinerary.Passenger.Length; j++)
                        {
                            if (itinerary.Passenger[j].Type == PassengerType.Infant)
                            {
                                PaxSSR paxSSR = new PaxSSR();
                                paxSSR.ActionStatusCode = "NN";
                                paxSSR.ArrivalStation = itinerary.Segments[i].Destination.AirportCode;
                                paxSSR.DepartureStation = itinerary.Segments[i].Origin.AirportCode;
                                paxSSR.FeeCode = "";
                                paxSSR.Note = "";
                                paxSSR.PassengerNumber = (Int16)counter;
                                paxSSR.SSRCode = "INFT";
                                paxSSR.SSRDetail = "";
                                paxSSR.SSRNumber = (Int16)0;
                                paxSSR.SSRValue = (Int16)0;
                                counter++;
                                PaxSSRs.Add(paxSSR);
                            }
                        }
                        requestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = PaxSSRs.ToArray();
                    }
                }

                request.SellRequestData = requestData;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoBookInfantRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch { }

                SellResponse sellResponse_v_4_5 = bookingAPI.Sell(request);
                responseData = sellResponse_v_4_5.BookingUpdateResponseData;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoBookInfantResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, responseData);
                    sw.Close();
                }
                catch { }
                bookingAmount += responseData.Success.PNRAmount.TotalCost;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to book Baggage. Reason : " + ex.ToString(), "");
            }



            return bookingAmount;
        }

        /// <summary>
        /// This method returns the Count of Infant as well as Baggage Count for Various Flight Segments Which determines whether to send a SSR request or not
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        private string GetSSRCount(FlightItinerary itinerary)
        {
            string baggage_infant_count = string.Empty;
            try
            {

                int onwardSegCount = 0, returnSegCount = 0;
                bool IsOnwardVIABooking = false, IsReturnVIABooking = false;
                int oncount = 0;

                foreach (FlightInfo seg in itinerary.Segments)
                {
                    if (seg.Group == 0)
                    {
                        oncount++;

                        if (seg.Status == string.Empty)
                        {
                            onwardSegCount++;
                        }
                        else //OneWay VIA Booking
                        {

                            IsOnwardVIABooking = true;
                            onwardSegCount = 1;
                        }
                    }

                    if (seg.Group == 1)
                    {

                        if (seg.Status == string.Empty)
                        {
                            returnSegCount++;
                        }
                        else //Return VIA Booking
                        {

                            IsReturnVIABooking = true;
                            returnSegCount = 1;
                        }
                    }
                }

                int infantCount = 0, baggageCount = 0;
                if (onwardSegCount == 1 || (onwardSegCount == 1 && returnSegCount == 1))
                {
                    #region Onward or Return(Via or Direct Flights)

                    //The below if block is for the following conditions :
                    //1.If both onward and return are VIA bookings.
                    //2.Onward Via Return Direct Flight
                    //3.Onward Direct Return Via Flight.

                    if ((IsOnwardVIABooking && IsReturnVIABooking) || (IsOnwardVIABooking && returnSegCount == 1) || (onwardSegCount == 1 && IsReturnVIABooking))
                    {
                        for (int i = 0; i < 2; i++)
                        {
                            infantCount = 0;
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                {
                                    if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                    {
                                        baggageCount++;
                                    }
                                }
                                if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                {

                                    infantCount++;
                                }
                            }
                            //Meal SSR
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0)
                                {
                                    baggageCount++;
                                }
                            }
                        }
                    }

                    //If only Onward Via Booking
                    else if (IsOnwardVIABooking)
                    {
                        for (int i = 0; i < 1; i++)
                        {
                            infantCount = 0;
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                {
                                    if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                    {
                                        baggageCount++;
                                    }
                                }
                                if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                {

                                    infantCount++;
                                }
                            }
                            //Meal SSR
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0)
                                {
                                    baggageCount++;
                                }
                            }


                        }
                    }

                    //If both onward and return are direct flights.
                    else
                    {
                        for (int i = 0; i < itinerary.Segments.Length; i++)
                        {
                            infantCount = 0;

                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                {
                                    if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                    {

                                        baggageCount++;
                                    }
                                }
                                if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                {

                                    infantCount++;
                                }
                            }

                            //Meal SSR
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0)
                                {
                                    baggageCount++;
                                }
                            }


                        }
                    }
                    #endregion
                }
                else
                {

                    if (returnSegCount > 0)
                    {
                        #region Return More Than One Segment(Connecting Flights)
                        for (int i = 0; i < itinerary.Segments.Length; i++)
                        {
                            infantCount = 0;
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (i == 0)
                                {
                                    if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                    {
                                        if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                        {

                                            baggageCount++;
                                        }
                                    }
                                }
                                if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                {
                                    infantCount++;
                                }
                            }
                            //Meal SSR
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0)
                                {
                                    baggageCount++;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Only OnwardSegments (Connecting Flights)
                        for (int i = 0; i < itinerary.Segments.Length; i++)
                        {
                            infantCount = 0;
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (i == 0)
                                {
                                    if (itinerary.Passenger[j].BaggageType != null && itinerary.Passenger[j].BaggageType.Trim().Length > 0)
                                    {
                                        if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].BaggageType.Length > 0)
                                        {

                                            baggageCount++;
                                        }
                                    }
                                }

                                if (itinerary.Passenger[j].Type == PassengerType.Infant)
                                {

                                    infantCount++;
                                }
                            }

                            //Meal SSR
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                if (itinerary.Passenger[j].Type != PassengerType.Infant && itinerary.Passenger[j].MealType != null && itinerary.Passenger[j].MealType.Trim().Length > 0)
                                {
                                    baggageCount++;
                                }
                            }
                        }
                        #endregion
                    }
                }
                if (baggageCount > 0 || infantCount > 0)
                {
                    baggage_infant_count = Convert.ToString(baggageCount) + "~" + Convert.ToString(infantCount);
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to get Baggage Count. Reason : " + ex.ToString(), "");
            }
            return baggage_infant_count;
        }


        #endregion

        #region Fare rules

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fareBasisCode"></param>
        /// <param name="classOfService"></param>
        /// <param name="ruleNumber"></param>
        /// <param name="origin"></param>
        /// <param name="dest"></param>
        /// <returns></returns>
        public List<FareRule> GetFareRuleInfo(string fareBasisCode, string classOfService, string ruleNumber, string origin, string dest)
        {
            List<FareRule> fareRules = new List<FareRule>();
            try
            {
                //The below are the steps for retrieving FareRule Information
                //S-1:LogOn
                //S-2:GetAvailability
                //S-3:GetFareRuleInfo
                //S-4:LogOut.

                logonResponse = Login();
                FareRuleInfo fareRuleInfo = null;

                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {

                    //Added by Lokesh on 11-June-2018
                    //New skies Version 4.5
                    //Need to send EnableExceptionStackTrace = false(by default) in the request.

                    GetFareRuleInfoRequest request = new GetFareRuleInfoRequest();
                    request.ContractVersion = contractVersion;
                    request.Signature = logonResponse.Signature;
                    request.EnableExceptionStackTrace = false;

                    FareRuleRequestData fareRuleReqData = new FareRuleRequestData();
                    fareRuleReqData.FareBasisCode = fareBasisCode;
                    fareRuleReqData.ClassOfService = classOfService;
                    fareRuleReqData.CarrierCode = "6E";
                    fareRuleReqData.RuleNumber = ruleNumber;
                    fareRuleReqData.CultureCode = "en-GB"; //use fixed value en-GB
                    request.fareRuleReqData = fareRuleReqData;

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(GetFareRuleInfoRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoFareRuleInfoSearchRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, request);
                        sw.Close();
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Save Fare Rule Info Search Request. Reason : " + ex.ToString(), "");
                    }
                    GetFareRuleInfoResponse response_v_4_5 = contentAPI.GetFareRuleInfo(request);
                    fareRuleInfo = response_v_4_5.FareRuleInfo;


                    try
                    {
                        //Changed from custom xml serializer to xml serializer
                        XmlSerializer ser = new XmlSerializer(typeof(GetFareRuleInfoResponse));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoFareRuleInfoSearchResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, response_v_4_5);
                        sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }


                    if (fareRuleInfo != null)
                    {
                        byte[] encodedBytes = fareRuleInfo.Data;
                        string decodedText = Encoding.UTF8.GetString(encodedBytes);
                        //To Get the text in plain format.
                        System.Windows.Forms.RichTextBox rtBox = new System.Windows.Forms.RichTextBox();
                        rtBox.Rtf = decodedText;
                        string plainText = rtBox.Text;

                        FareRule DNfareRules = new FareRule();
                        DNfareRules.Origin = origin;
                        DNfareRules.Destination = dest;
                        DNfareRules.Airline = "6E";
                        DNfareRules.FareRuleDetail = plainText;
                        DNfareRules.FareBasisCode = fareBasisCode;
                        fareRules.Add(DNfareRules);
                    }
                    //Logout(logonResponse.Signature);
                }
                else
                {
                    throw new Exception("(Indigo)Failed to get the LogonResponse");
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to retrieve fare rule information. Reason : " + ex.ToString(), "");
            }
            return fareRules;
        }


        #endregion

        #region Booking Methods

        /// <summary>
        /// Books an itinerary
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private BookingUpdateResponseData Book(FlightItinerary itinerary, string signature)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                SellRequestData requestData = new SellRequestData();
                SellRequest request = new SellRequest();

                request.ContractVersion = contractVersion;
                request.Signature = signature;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                request.EnableExceptionStackTrace = false;
                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = currencyCode;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;

                //Define SourcePOS data
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS = new PointOfSale();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.AgentCode = "AG";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.DomainCode = agentDomain;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.OrganizationCode = agentId;

                string onwardFareSellKey = string.Empty;
                string onwardJourneySellKey = string.Empty;
                string returnFareSellKey = string.Empty;
                string returnJourneySellKey = string.Empty;

                List<string> sellKeysListOnward = new List<string>();
                List<string> sellKeysListReturn = new List<string>();
                for (int p = 0; p < itinerary.Segments.Length; p++)
                {
                    if (itinerary.Segments[p].Group == 0)//Onward
                    {
                        sellKeysListOnward.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                    if (itinerary.Segments[p].Group == 1)//Return
                    {
                        sellKeysListReturn.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                }
                IEnumerable<string> distinctSellKeysOnward = sellKeysListOnward.Distinct();
                IEnumerable<string> distinctSellKeysReturn = sellKeysListReturn.Distinct();

                if (distinctSellKeysOnward.Count() > 0)
                {
                    foreach (string key in distinctSellKeysOnward)
                    {
                        if (onwardJourneySellKey.Length > 0)
                        {
                            onwardJourneySellKey += "^" + key;
                        }
                        else
                        {
                            onwardJourneySellKey = key;
                        }
                    }
                }

                if (distinctSellKeysReturn.Count() > 0)
                {
                    foreach (string key in distinctSellKeysReturn)
                    {
                        if (returnJourneySellKey.Length > 0)
                        {
                            returnJourneySellKey += "^" + key;
                        }
                        else
                        {
                            returnJourneySellKey = key;
                        }
                    }
                }


                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[1];
                if (itinerary.TicketAdvisory.Split('|').Length == 2)
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[2];
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = onwardJourneySellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = itinerary.TicketAdvisory.Split('|')[0];
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";

                if (itinerary.TicketAdvisory.Split('|').Length == 2)//Round trip
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].JourneySellKey = returnJourneySellKey;
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].FareSellKey = itinerary.TicketAdvisory.Split('|')[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].StandbyPriorityCode = "";
                }
                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    if (itinerary.Passenger[i].Type != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        switch (itinerary.Passenger[i].Type)
                        {
                            case PassengerType.Adult:
                                priceType.PaxType = "ADT";
                                break;
                            case PassengerType.Child:
                                priceType.PaxType = "CHD";
                                break;

                        }
                        paxPriceTypes.Add(priceType);
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;

                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                request.SellRequestData = requestData;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoBookingRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch { }

                SellResponse sellResponse_v_4_5 = bookingAPI.Sell(request);
                responseData = sellResponse_v_4_5.BookingUpdateResponseData;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoBookingResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, responseData);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to Book. Reason : " + ex.ToString(), "");
                throw ex;
            }

            return responseData;
        }

        /// <summary>
        /// Used to book the Baggage, chosen by the user for the Itinerary.
        /// Also Infants need to be booked from this method itself.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private BookingUpdateResponseData BookSSR(FlightItinerary itinerary, string signature)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();
            try
            {
                //START:VARIABLES WHICH IDENTIFIES WHETHER TO SEND THE SELL SSR REQUEST TO THE API

                int baggageSSR  = itinerary.Passenger.Where(p => p.BaggageType != null).Count();
                int mealSSR = itinerary.Passenger.Where(p => p.MealType != null).Count();
                int infantSSR = itinerary.Passenger.Where(p => p.Type == PassengerType.Infant).Count();
                int infantCount = 0;//Increment this variable to add a unique number in the request.

                //END:VARIABLES WHICH IDENTIFIES WHETHER TO SEND THE SELL SSR REQUEST TO THE API

                SellRequest request = new SellRequest();
                request.ContractVersion = contractVersion;
                request.Signature = signature;
                request.EnableExceptionStackTrace = false;

                SellRequestData requestData = new SellRequestData();
                requestData.SellBy = SellBy.SSR;
                requestData.SellSSR = new SellSSR();
                requestData.SellSSR.SSRRequest = new SSRRequest();
                requestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                requestData.SellSSR.SSRRequest.CurrencyCode = currencyCode;
                requestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;

                //================START:STRUCTURE OF SELL REQUEST OBJECT=================
                //SellRequest
                //SellRequest -->SellRequestData
                //SellRequest -->SellRequestData-->SellSSR
                //SellRequest -->SellRequestData-->SellSSR -->SSRRequest
                //SellRequest -->SellRequestData-->SellSSR -->SegmentSSRRequests[]
                //SellRequest -->SellRequestData-->SellSSR -->SegmentSSRRequests[] -->SegmentSSRRequest-->PaxSSRs[]
                //SellRequest -->SellRequestData-->SellSSR -->SegmentSSRRequests[] -->SegmentSSRRequest-->PaxSSRs[] -->PaxSSR
                //================END:STRUCTURE OF SELL REQUEST OBJECT=================

                //For BAGGAGE THE SSR REQUEST SHOULD BE JOURNEY WISE
                //For MEAL THE SSR REQUEST SHOULD BE SEGMENT WISE
                //For INFANT THE SSR REQUEST SHOULD BE SEGMENT WISE

                List<SegmentSSRRequest> segSSRRequests = new List<SegmentSSRRequest>();

                var objflight = itinerary.Segments.Select(p => p.FlightNumber).Distinct().ToArray();
                List<PaxSSR> segBaggagePaxSSR = null;

                //==================START:BAGGAGE,MEAL SSSR AND INFANT SSR===========
                for (int i = 0; i < objflight.Length; i++)//Adding Baggage Journey wise 
                {
                    infantCount = 0;
                    var sOrigin = itinerary.Segments.Where(p => p.FlightNumber == objflight[i]).Select(z => z.Origin.AirportCode).FirstOrDefault();
                    var sDestination = itinerary.Segments.Where(p => p.FlightNumber == objflight[i]).Select(z => z.Destination.AirportCode).Last();
                    DateTime dtSTD = itinerary.Segments.Where(y => y.FlightNumber == objflight[i]).Select(f => f.DepartureTime).FirstOrDefault();

                    var objSegment = itinerary.Segments.Where(y => y.FlightNumber == objflight[i]).FirstOrDefault();

                    SegmentSSRRequest segmentSSRRequest = new SegmentSSRRequest();
                    segmentSSRRequest.ArrivalStation = sDestination;
                    segmentSSRRequest.DepartureStation = sOrigin;
                    segmentSSRRequest.FlightDesignator = new FlightDesignator();
                    segmentSSRRequest.FlightDesignator.CarrierCode = "6E";
                    segmentSSRRequest.FlightDesignator.FlightNumber = objflight[i];
                    segmentSSRRequest.STD = dtSTD;
                    segBaggagePaxSSR = new List<PaxSSR>();
                    //BAGGAGE SSR
                    for (int j = 0; j < itinerary.Passenger.Length; j++)
                    {
                        if (!string.IsNullOrEmpty(itinerary.Passenger[j].BaggageType))
                        {
                            PaxSSR paxSSR = new PaxSSR();
                            paxSSR.State = Indigo.BookingMgr.MessageState.New;
                            paxSSR.ActionStatusCode = "NN";
                            paxSSR.ArrivalStation = sDestination;
                            paxSSR.DepartureStation = sOrigin;
                            paxSSR.PassengerNumber = (Int16)j;
                            string baggageCode = itinerary.Passenger[j].BaggageType.Split(',')[0];
                            if (itinerary.Passenger[j].BaggageType.Split(',').Length > 1 && (itinerary.Segments[i].Group == 1 || (objSegment.Status == "VIA" && objSegment.Group == 1)))//Return  & For VIA flights we bind the baggage SSR only one time to Request.
                            {
                                paxSSR.SSRCode = itinerary.Passenger[j].BaggageType.Split(',')[1];
                            }
                            if (itinerary.Passenger[j].BaggageType.Split(',').Length > 1 && objSegment.Group == 1)// Onward Via & Return Connecting 
                            {
                                paxSSR.SSRCode = itinerary.Passenger[j].BaggageType.Split(',')[1];
                            }
                            else
                            {
                                paxSSR.SSRCode = baggageCode;
                            }
                            paxSSR.SSRNumber = (Int16)0;
                            paxSSR.SSRValue = (Int16)0;
                            if (!string.IsNullOrEmpty(paxSSR.SSRCode))
                            {
                                segBaggagePaxSSR.Add(paxSSR);
                            }
                        }
                        if (itinerary.Passenger[j].Type == PassengerType.Infant)
                        {
                            PaxSSR paxSSR = new PaxSSR();
                            paxSSR.State = Indigo.BookingMgr.MessageState.New;
                            paxSSR.ActionStatusCode = "NN";
                            paxSSR.ArrivalStation = sDestination;
                            paxSSR.DepartureStation = sOrigin;
                            paxSSR.PassengerNumber = (Int16)infantCount;
                            paxSSR.SSRCode = "INFT";//Oneway
                            paxSSR.SSRNumber = (Int16)0;
                            paxSSR.SSRValue = (Int16)0;
                            if (!string.IsNullOrEmpty(paxSSR.SSRCode))
                            {
                                segBaggagePaxSSR.Add(paxSSR);
                            }
                            infantCount++;
                        }
                    }
                    //MEAL SSR
                    for (int j = 0; j < itinerary.Passenger.Length; j++)
                    {
                        if (!string.IsNullOrEmpty(itinerary.Passenger[j].MealType))
                        {
                            PaxSSR paxSSR = new PaxSSR();
                            paxSSR.State = Indigo.BookingMgr.MessageState.New;
                            paxSSR.ActionStatusCode = "NN";
                            if (objSegment.Status == "VIA")
                            {
                                paxSSR.ArrivalStation = objSegment.Destination.AirportCode;
                                paxSSR.DepartureStation = objSegment.Origin.AirportCode;
                            }
                            else
                            {
                                paxSSR.ArrivalStation = sDestination;
                                paxSSR.DepartureStation = sOrigin;
                            }
                            paxSSR.PassengerNumber = (Int16)j;
                            string mealCode = string.Empty;

                            //Normal--One Way
                            //Combination--One Way
                            //Combination -- Round Trip
                            //Example:  NVML  --- Only onward meal selected
                            //Example : NVML, -- --- Only onward meal selected and no return meal selected
                            //Example : NVML,NVML -- Both onward  and return meal selected
                            //Example :,NVML  -- no onward meal only return meal selected.
                            
                            if (itinerary.Segments[i].Group == 0 && itinerary.Passenger[j].MealType.Split(',').Length==1)//Normal--One Way + Combination--One Way
                            {
                                mealCode = itinerary.Passenger[j].MealType.Split(',')[0];
                            }
                            if (itinerary.Segments[i].Group == 1 && itinerary.Passenger[j].MealType.Split(',').Length == 1)//Combination: Round Trip.
                            {
                                mealCode = itinerary.Passenger[j].MealType.Split(',')[0];
                            }
                            //Example : NVML,NVML -- Both onward  and return meal selected
                            if (itinerary.Segments[i].Group == 0 && itinerary.Passenger[j].MealType.Split(',').Length > 1)//Normal :Return Trip.
                            {
                                mealCode = itinerary.Passenger[j].MealType.Split(',')[0];
                            }
                            //Example : NVML,NVML -- Both onward  and return meal selected
                            if (itinerary.Segments[i].Group == 1 && itinerary.Passenger[j].MealType.Split(',').Length > 1)//Normal :Return Trip.
                            {
                                mealCode = itinerary.Passenger[j].MealType.Split(',')[1];
                            }
                            if (itinerary.Passenger[j].MealType.Split(',').Length > 1 && objSegment.Group == 1 && objSegment.Status == "VIA")//For VIA flights we bind the baggage SSR only one time to Request.
                            {
                                mealCode = itinerary.Passenger[j].MealType.Split(',')[1];
                            }
                            if (itinerary.Passenger[j].MealType.Split(',').Length > 1 && objSegment.Group == 1)//onward VIA & return connecting.
                            {
                                mealCode = itinerary.Passenger[j].MealType.Split(',')[1];
                            }

                            if (!string.IsNullOrEmpty(mealCode))
                            {
                                paxSSR.SSRNumber = (Int16)0;
                                paxSSR.SSRValue = (Int16)0;
                                paxSSR.SSRCode = mealCode;
                                segBaggagePaxSSR.Add(paxSSR);
                            }
                        }
                    }

                    if (segBaggagePaxSSR != null && segBaggagePaxSSR.Count > 0)
                    {
                        segmentSSRRequest.PaxSSRs = segBaggagePaxSSR.ToArray();
                        segSSRRequests.Add(segmentSSRRequest);
                    }
                }
                //==================END:BAGGAGE,MEAL SSSR FOR ALL THE PASSENGERS===========

                //If Excess baggage or meal or infant is selected then book it otherwise skip this operation
                if (baggageSSR >0 || infantSSR >0 || mealSSR >0)
                {
                    requestData.SellSSR.SSRRequest.SegmentSSRRequests = segSSRRequests.ToArray();
                    request.SellRequestData = requestData;
                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoBookBaggageRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, request);
                        sw.Close();
                    }
                    catch { }

                    SellResponse sellResponse_v_4_5 = bookingAPI.Sell(request);
                    responseData = sellResponse_v_4_5.BookingUpdateResponseData;

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoBookBaggageResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, responseData);
                        sw.Close();
                    }
                    catch { }
                }
                else
                {
                    responseData = null;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to book Baggage. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return responseData;
        }

        /// <summary>
        ///  Update passenger data for the booking.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private BookingUpdateResponseData UpdatePassengers(FlightItinerary itinerary, string signature)
        {
            BookingUpdateResponseData upResponse = new BookingUpdateResponseData();

            try
            {
                logonResponse = new LogonResponse();
                logonResponse.Signature = itinerary.Endorsement;////Till Search To Book This Signature Should be carried.
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {

                    int paxCount = 0, infantCount = 0, infants = 0;
                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        if (itinerary.Passenger[i].Type != PassengerType.Infant)
                        {
                            paxCount++;
                        }
                        else
                        {
                            infantCount++;
                        }
                    }

                    UpdatePassengersRequest updatePassengerRequest = new UpdatePassengersRequest();
                    updatePassengerRequest.ContractVersion = contractVersion;
                    updatePassengerRequest.Signature = logonResponse.Signature;
                    //Added by Lokesh on 11-June-2018
                    //New skies Version 4.5
                    //Need to send EnableExceptionStackTrace = false(by default) in the request.
                    updatePassengerRequest.EnableExceptionStackTrace = false;
                    updatePassengerRequest.updatePassengersRequestData = new UpdatePassengersRequestData();
                    updatePassengerRequest.updatePassengersRequestData.Passengers = new Passenger[paxCount];

                    for (int i = 0; i < paxCount; i++)
                    {
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i] = new Passenger();
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerNumber = (Int16)i;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].State = Indigo.BookingMgr.MessageState.New;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names = new BookingName[1];
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0] = new BookingName();
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].Title = itinerary.Passenger[i].Title.ToUpper();
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].FirstName = itinerary.Passenger[i].FirstName;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].LastName = itinerary.Passenger[i].LastName;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Names[0].State = Indigo.BookingMgr.MessageState.New;
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo = new PassengerInfo();
                        if (itinerary.Passenger[i].Gender == Gender.Male)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Gender = Indigo.BookingMgr.Gender.Male;
                        }
                        else
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Gender = Indigo.BookingMgr.Gender.Female;
                        }
                        if (itinerary.Passenger[i].Nationality != null)
                        {
                            if (!string.IsNullOrEmpty(itinerary.Passenger[i].Nationality.CountryCode))
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.Nationality = itinerary.Passenger[i].Nationality.CountryCode;
                            }
                        }
                        if (itinerary.Passenger[i].Country != null)
                        {
                            if (!string.IsNullOrEmpty(itinerary.Passenger[i].Country.CountryCode))
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerInfo.ResidentCountry = itinerary.Passenger[i].Country.CountryCode;
                            }
                        }
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos = new PassengerTypeInfo[1];
                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0] = new PassengerTypeInfo();
                        if (itinerary.Passenger[i].Type == PassengerType.Adult)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].PaxType = "ADT";
                            if (itinerary.Passenger[i].DateOfBirth != DateTime.MinValue)
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = itinerary.Passenger[i].DateOfBirth;
                            }
                            else
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = DateTime.Today.AddYears(-30);
                            }
                            if (infantCount - infants > 0)
                            {
                                FlightPassenger infantPax = itinerary.Passenger[paxCount + infants];
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant = new PassengerInfant();
                                if (infantPax.DateOfBirth != DateTime.MinValue)
                                {
                                    updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.DOB = infantPax.DateOfBirth;
                                }
                                else
                                {
                                    updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.DOB = DateTime.Today.AddDays(-365);
                                }
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Gender = (infantPax.Gender == Gender.Male ? Indigo.BookingMgr.Gender.Male : Indigo.BookingMgr.Gender.Female);
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names = new BookingName[1];
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0] = new BookingName();
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].FirstName = infantPax.FirstName;
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].LastName = infantPax.LastName;
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].Title = string.Empty;//As per latest 4.2 infant will have no title
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Names[0].State = Indigo.BookingMgr.MessageState.New;
                                if (itinerary.Passenger[i].Nationality != null)
                                {
                                    if (!string.IsNullOrEmpty(itinerary.Passenger[i].Nationality.CountryCode))
                                    {
                                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.Nationality = itinerary.Passenger[i].Nationality.CountryCode;
                                    }
                                }
                                if (itinerary.Passenger[i].Country != null)
                                {
                                    if (!string.IsNullOrEmpty(itinerary.Passenger[i].Country.CountryCode))
                                    {
                                        updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.ResidentCountry = itinerary.Passenger[i].Country.CountryCode;
                                    }
                                }

                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].Infant.State = Indigo.BookingMgr.MessageState.New;
                                infants++;
                            }
                        }
                        else if (itinerary.Passenger[i].Type == PassengerType.Child)
                        {
                            updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].PaxType = "CHD";
                            if (itinerary.Passenger[i].DateOfBirth != DateTime.MinValue)
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = itinerary.Passenger[i].DateOfBirth;
                            }
                            else
                            {
                                updatePassengerRequest.updatePassengersRequestData.Passengers[i].PassengerTypeInfos[0].DOB = DateTime.Today.AddYears(-10);
                            }
                        }
                    }

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(UpdatePassengersRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoUpdatePassengersRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, updatePassengerRequest);
                        sw.Close();
                    }
                    catch { }

                    UpdatePassengersResponse upResponse_v_4_5 = bookingAPI.UpdatePassengers(updatePassengerRequest);
                    upResponse = upResponse_v_4_5.BookingUpdateResponseData;

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoUpdatePassengersResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, upResponse);
                        sw.Close();
                    }
                    catch { }
                }
                else
                {
                    throw new Exception("(Indigo)Failed to get the LogonResponse");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, appUserId, "Failed to update passengers. Error : " + ex.ToString(), "");
                throw ex;
            }
            return upResponse;
        }

        /// <summary>
        /// Assign seats for the selected pax. 
        /// Assign seats will send seat assign request for each passenger and each segment based on user seat selection 
        /// using the PassengerNumber field to match the passengers to passengers in booking state. 
        /// The matching passenger seat details are replaced with the seat details from the request. 
        /// </summary>
        /// <param name="clsFlightItinerary"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private BookingResponse AssignSeats(ref FlightItinerary clsFlightItinerary, string signature)
        {
            AssignSeatsResponse clsAssignSeatsResponse = new AssignSeatsResponse();
            DateTime dtSTD; string sOrigin = string.Empty, sDestination = string.Empty; bool bSegStatus = true;
            BookingResponse clsBookingResponse = new BookingResponse();

            var objFlights = clsFlightItinerary.Segments.Select(p => p.FlightNumber).Distinct();

            AssignSeatsRequest clsAssignSeatsRequest = new AssignSeatsRequest();

            clsAssignSeatsRequest.ContractVersion = contractVersion;
            clsAssignSeatsRequest.Signature = signature;
            clsAssignSeatsRequest.EnableExceptionStackTrace = false;
            clsAssignSeatsRequest.SellSeatRequest = new SeatSellRequest();

            clsAssignSeatsRequest.SellSeatRequest.CollectedCurrencyCode = AgentBaseCurrency;
            clsAssignSeatsRequest.SellSeatRequest.BlockType = UnitHoldType.Session;
            clsAssignSeatsRequest.SellSeatRequest.SeatAssignmentMode = SeatAssignmentMode.PreSeatAssignment;
            clsAssignSeatsRequest.SellSeatRequest.IncludeSeatData = true;
            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests = new SegmentSeatRequest[clsFlightItinerary.Passenger.Where(m => m.Type != PassengerType.Infant).Count() * objFlights.Count()];
            int segcnt = 0;

            foreach (string flt in objFlights)
            {
                try
                {
                    sOrigin = clsFlightItinerary.Segments.Where(y => y.FlightNumber == flt).Select(z => z.Origin.AirportCode).FirstOrDefault();
                    sDestination = clsFlightItinerary.Segments.Where(y => y.FlightNumber == flt).Select(z => z.Destination.AirportCode).Last();
                    dtSTD = clsFlightItinerary.Segments.Where(y => y.FlightNumber == flt).Select(f => f.DepartureTime).FirstOrDefault();

                    clsFlightItinerary.Passenger.ToList().ForEach(p =>
                    {
                        var obj = p.liPaxSeatInfo.Where(y => y.SeatStatus == "A" && !string.IsNullOrEmpty(y.SeatNo) && y.Segment == sOrigin + "-" + sDestination).FirstOrDefault();
                        if (obj != null)
                        {
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt] = new SegmentSeatRequest();
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].ArrivalStation = sDestination;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].DepartureStation = sOrigin;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].STD = dtSTD;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].PassengerNumbers = new short[1];
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].PassengerNumbers[0] = new short();
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].PassengerNumbers[0] = (short)obj.PaxNo;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].UnitDesignator = obj.SeatNo;
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].FlightDesignator = new FlightDesignator();
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].FlightDesignator.CarrierCode = "6E";
                            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests[segcnt].FlightDesignator.FlightNumber = flt.Length < 4 ? " " + flt : flt;
                            segcnt++;
                        }
                    });
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo)Failed to prepare seat assign request. Error: " + ex.GetBaseException(), "");
                    clsFlightItinerary.Passenger.ToList().ForEach(y => y.liPaxSeatInfo.
                                ForEach(s => { y.SeatInfo = string.Empty; y.Price.SeatPrice = 0; s.SeatNo = "NoSeat"; s.Price = 0; s.SeatStatus = "F"; }));
                    clsBookingResponse.Error = "Failed to prepare seat assign request."; clsBookingResponse.Status = BookingResponseStatus.Assignseatfailed;
                    throw ex;
                }
            }

            clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests = clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests.Where(j => j != null).ToArray();
            clsFlightItinerary.ItineraryAmountDue = 0;

            if (clsAssignSeatsRequest.SellSeatRequest.SegmentSeatRequests.Length > 0)
            {
                GenericStatic.WriteLogFile(typeof(AssignSeatsRequest), clsAssignSeatsRequest,
                    xmlLogPath + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + "_6E_AssignSeatRequest_");

                try
                {
                    clsAssignSeatsResponse = bookingAPI.AssignSeats(clsAssignSeatsRequest);
                }
                catch (Exception ex)
                {
                    bSegStatus = false;
                    Audit.Add(EventType.Exception, Severity.High, 1, "(SpiceJet)Failed to assign seat from supplier. Error: " + ex.GetBaseException(), "");
                }

                GenericStatic.WriteLogFile(typeof(AssignSeatsResponse), clsAssignSeatsResponse,
                    xmlLogPath + sessionId + "_" + appUserId + "_" + sOrigin + "_" + sDestination + "_" + "_6E_AssignSeatResponse_");

                if (bSegStatus && clsAssignSeatsResponse.BookingUpdateResponseData != null && clsAssignSeatsResponse.BookingUpdateResponseData.Success != null)
                {
                    clsFlightItinerary.ItineraryAmountDue = clsAssignSeatsResponse.BookingUpdateResponseData.Success.PNRAmount.BalanceDue;
                    clsBookingResponse.Status = BookingResponseStatus.Successful;
                    clsFlightItinerary.Passenger.ToList().ForEach(y => y.liPaxSeatInfo.ForEach(s => s.SeatStatus = "S"));
                    clsFlightItinerary.BookRequestType = BookingFlowStatus.PaxUpdated;
                }
                else
                {
                    string err = string.Empty;
                    if (bSegStatus && clsAssignSeatsResponse.AssignedSeatInfo.JourneyList != null && clsAssignSeatsResponse.AssignedSeatInfo.JourneyList.Length > 0)
                    {
                        var Segments = new List<AssignSeatSegment>();
                        clsAssignSeatsResponse.AssignedSeatInfo.JourneyList.ToList().ForEach(x => { if (x.Segments != null) { Segments.AddRange(x.Segments); } });

                        var paxseats = new List<PaxSeat>();
                        Segments.ForEach(x => { if (x.PaxSeats != null) { paxseats.AddRange(x.PaxSeats); } });

                        clsFlightItinerary.Passenger.Where(b => !string.IsNullOrEmpty(b.SeatInfo)).ToList().
                            ForEach(y => y.liPaxSeatInfo.Where(a => a.SeatStatus == "A" && !string.IsNullOrEmpty(a.SeatNo)).ToList().ForEach(s =>
                            {
                                if (paxseats.Where(p => p.PassengerNumber == s.PaxNo && p.DepartureStation + "-" + p.ArrivalStation == s.Segment).Count() == 0)
                                {
                                    string msg = "Failed to assign seat for passenger " + y.FirstName + " " + y.LastName + " for segment (" + s.Segment + ")";
                                    err = string.IsNullOrEmpty(err) ? msg : err + "@" + msg;
                                    s.SeatNo = "NoSeat"; y.Price.SeatPrice -= s.Price; s.Price = 0; s.SeatStatus = "F";
                                }
                                else
                                {
                                    s.SeatStatus = "S";
                                }
                            }));
                    }
                    else
                    {
                        clsFlightItinerary.Passenger.Where(b => !string.IsNullOrEmpty(b.SeatInfo)).ToList().
                            ForEach(y => y.liPaxSeatInfo.Where(a => a.SeatStatus == "A" && !string.IsNullOrEmpty(a.SeatNo)).ToList().ForEach(s =>
                            {
                                string msg = "Failed to assign seat for passenger " + y.FirstName + " " + y.LastName + " for segment (" + s.Segment + ")";
                                err = string.IsNullOrEmpty(err) ? msg : err + "@" + msg;
                                s.SeatNo = "NoSeat"; y.Price.SeatPrice -= s.Price; s.Price = 0; s.SeatStatus = "F";
                            }));
                    }
                    clsBookingResponse.Status = BookingResponseStatus.Assignseatfailed;
                    clsBookingResponse.Error = err;
                }

            }
            else
            {
                clsFlightItinerary.ItineraryAmountDue = 0;
                clsBookingResponse.Status = BookingResponseStatus.Successful;
                clsFlightItinerary.BookRequestType = BookingFlowStatus.PaxUpdated;
            }
            return clsBookingResponse;
        }

        /// <summary>
        /// Used to confirm the booking amount against the available balance.
        /// </summary>
        /// <param name="bookingAmount"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        private AddPaymentToBookingResponseData AddPaymentForBooking(decimal bookingAmount, string signature)
        {
            AddPaymentToBookingResponseData response = new AddPaymentToBookingResponseData();


            try
            {
                logonResponse = new LogonResponse();
                logonResponse.Signature = signature;////Till Search To Book This Signature Should be carried.
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {
                    AddPaymentToBookingRequest payRequest = new AddPaymentToBookingRequest();
                    payRequest.ContractVersion = contractVersion;
                    payRequest.Signature = logonResponse.Signature;
                    //Added by Lokesh on 11-June-2018
                    //New skies Version 4.5
                    //Need to send EnableExceptionStackTrace = false(by default) in the request.
                    payRequest.EnableExceptionStackTrace = false;
                    AddPaymentToBookingRequestData request = new AddPaymentToBookingRequestData();
                    request.AccountNumber = agentId;
                    request.AccountNumberID = 0;
                    request.AuthorizationCode = "";
                    request.Deposit = false;
                    request.Expiration = new DateTime();
                    request.Installments = 1;
                    request.MessageState = Indigo.BookingMgr.MessageState.New;
                    request.ParentPaymentID = 0;
                    request.PaymentAddresses = new PaymentAddress[0];
                    request.PaymentFields = new PaymentField[0];
                    request.PaymentMethodCode = "AG";
                    request.PaymentMethodType = RequestPaymentMethodType.AgencyAccount;
                    request.PaymentText = "";
                    request.QuotedAmount = bookingAmount;
                    request.QuotedCurrencyCode = currencyCode;
                    request.ReferenceType = PaymentReferenceType.Default;
                    request.Status = BookingPaymentStatus.New;
                    request.ThreeDSecureRequest = new ThreeDSecureRequest();
                    request.WaiveFee = false;

                    payRequest.addPaymentToBookingReqData = request;
                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(AddPaymentToBookingRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoAddPaymentToBookingRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, payRequest);
                        sw.Close();
                    }
                    catch { }
                    AddPaymentToBookingResponse response_v_4_5 = bookingAPI.AddPaymentToBooking(payRequest);
                    response = response_v_4_5.BookingPaymentResponse;

                    // AddPaymentToBookingResponse.addPaymentToBookingResp = bookingAPI.AddPaymentToBooking(contractVersion, signature, payRequest.addPaymentToBookingReqData);

                    try
                    {

                        XmlSerializer ser = new XmlSerializer(typeof(AddPaymentToBookingResponseData));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoAddPaymentToBookingResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, response);
                        sw.Close();
                    }
                    catch { }
                }
                else
                {
                    throw new Exception("(Indigo)Failed to get the LogonResponse");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to Add Payment for booking. Reason : " + ex.ToString(), "");
                throw ex;
            }

            //Logout(loginResponse.Signature);

            //Payment validation errors check.

            // bool PaymentSuccess = false;
            //  if (response.ValidationPayment.PaymentValidationErrors != null && response.ValidationPayment.PaymentValidationErrors.Length == 0)
            //  {
            //      PaymentSuccess = true;
            //  }
            ///  else
            //  {
            //      PaymentSuccess = false;
            //  }

            return response;
        }

        /// <summary>
        /// Book the Itinerary
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public BookingResponse BookItinerary(ref FlightItinerary itinerary)
        {
            BookingResponse response = new BookingResponse();

            try
            {

                logonResponse = new LogonResponse();////Till Search To Book This Signature Should be carried.
                logonResponse.Signature = itinerary.Endorsement;//Till Search To Book This Signature Should Carry

                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {
                    BookingUpdateResponseData bookResponse = new BookingUpdateResponseData(); //= Book(itinerary, logonResponse.Signature);

                    if (itinerary.BookRequestType == BookingFlowStatus.UpdatePax || itinerary.BookRequestType == BookingFlowStatus.AssignSeat)
                    {
                        int sBagMealcnt = itinerary.Passenger.Where(y => y.Type != PassengerType.Infant && (!string.IsNullOrEmpty(y.BaggageType)
                        || !string.IsNullOrEmpty(y.MealType))).Count();

                        int sInfntcnt = itinerary.Passenger.Where(y => y.Type == PassengerType.Infant).Count();

                        if (itinerary.BookRequestType == BookingFlowStatus.UpdatePax && (sBagMealcnt > 0 || sInfntcnt > 0))
                            bookResponse = BookSSR(itinerary, logonResponse.Signature);
                        else
                        {
                            response.Status = BookingResponseStatus.Successful;
                            bookResponse.Success = new Success();
                        }

                        if (bookResponse != null && bookResponse.Success != null)
                        {
                            bookResponse = UpdatePassengers(itinerary, logonResponse.Signature);
                            if (bookResponse != null && bookResponse.Success != null && bookResponse.Success.PNRAmount!=null && bookResponse.Success.PNRAmount.TotalCost > 0)
                            {
                                itinerary.BookRequestType = BookingFlowStatus.AssignSeat;
                                if (itinerary.Passenger.Where(y => !string.IsNullOrEmpty(y.SeatInfo)).Count() > 0)
                                {
                                    response = AssignSeats(ref itinerary, logonResponse.Signature);
                                }
                                else
                                {
                                    itinerary.BookRequestType = BookingFlowStatus.PaxUpdated;
                                    response.Status = BookingResponseStatus.Successful; response.Error = string.Empty;
                                }

                                itinerary.ItineraryAmountDue = itinerary.ItineraryAmountDue == 0 ? bookResponse.Success.PNRAmount.TotalCost :
                                    itinerary.ItineraryAmountDue;
                            }
                            else
                            {
                                response.Status = BookingResponseStatus.UpdatePaxFailed;
                                response.Error = "Update Passenger failed, please check the logs";
                            }
                        }
                        else
                        {
                            response.Status = BookingResponseStatus.Bookbaggagefailed;
                            response.Error = "SSR booking failed, please check the logs";
                        }
                    }
                    else if (itinerary.BookRequestType == BookingFlowStatus.PaxUpdated)
                    {
                        //Step-3:AddPaymentToBooking
                        itinerary.BookRequestType = BookingFlowStatus.AddPayment;
                        AddPaymentToBookingResponseData paymentResponse = AddPaymentForBooking(itinerary.ItineraryAmountDue, logonResponse.Signature);

                        int sInfntcnt = itinerary.Passenger.Where(y => y.Type == PassengerType.Infant).Count();

                        //If there are any Payment validations failed do not allow to commit booking
                        if (paymentResponse != null && paymentResponse.ValidationPayment != null && paymentResponse.ValidationPayment.PaymentValidationErrors != null && paymentResponse.ValidationPayment.PaymentValidationErrors.Length == 0)
                        {
                            if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                            {
                                BookingUpdateResponseData responseData = new BookingUpdateResponseData();
                                BookingCommitRequest bookRequest = new BookingCommitRequest();
                                bookRequest.ContractVersion = contractVersion;
                                bookRequest.Signature = logonResponse.Signature;
                                //Added by Lokesh on 11-June-2018
                                //New skies Version 4.5
                                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                                bookRequest.EnableExceptionStackTrace = false;

                                BookingCommitRequestData request = new BookingCommitRequestData();
                                request.BookingChangeCode = "";
                                request.BookingComments = new BookingComment[0];

                                request.SourcePOS = new PointOfSale();
                                request.SourcePOS.AgentCode = "AG";
                                request.SourcePOS.OrganizationCode = agentId;
                                request.SourcePOS.DomainCode = agentDomain;

                                request.BookingID = 0;
                                request.BookingParentID = 0;
                                request.ChangeHoldDateTime = false;
                                request.CurrencyCode = currencyCode;
                                request.DistributeToContacts = true;//Send booking confirmation email to the pax
                                request.GroupName = "";
                                request.NumericRecordLocator = "";
                                request.ParentRecordLocator = "";
                                request.RecordLocator = "";
                                request.RecordLocators = new RecordLocator[0];
                                request.RestrictionOverride = false;

                                request.State = Indigo.BookingMgr.MessageState.New;
                                request.SystemCode = "";
                                request.WaiveNameChangeFee = false;
                                request.WaivePenaltyFee = false;
                                request.WaiveSpoilageFee = false;
                                request.Passengers = new Passenger[0];
                                request.PaxCount = (Int16)itinerary.Passenger.Where(s => s.Type != PassengerType.Infant).Count();

                                for (int i = 0; i < itinerary.Passenger.Length; i++)
                                {
                                    FlightPassenger pax = itinerary.Passenger[i];
                                    if (pax.Type != PassengerType.Infant)
                                    {
                                        if (pax.IsLeadPax)
                                        {
                                            request.BookingContacts = new BookingContact[1];
                                            request.BookingContacts[0] = new BookingContact();
                                            request.BookingContacts[0].AddressLine1 = (string.IsNullOrEmpty(pax.AddressLine1) ? "Address Line1" : (pax.AddressLine1.Length <=52 ? pax.AddressLine1: pax.AddressLine1.Substring(0, 52)));//Address Line Must Be No more than 52 Characters in length.
                                            request.BookingContacts[0].AddressLine2 = pax.AddressLine2;
                                            request.BookingContacts[0].AddressLine3 = "";
                                            request.BookingContacts[0].City = "SHJ";
                                            request.BookingContacts[0].CompanyName = "Cozmo Travel";
                                            if (pax.Country != null && !string.IsNullOrEmpty(pax.Country.CountryCode))
                                            {

                                                request.BookingContacts[0].CountryCode = pax.Country.CountryCode;

                                            }
                                            else //Since we are hard coding city as "SHJ" so we are passing country code as "AE";
                                            {
                                                request.BookingContacts[0].CountryCode = "AE";
                                            }
                                            request.BookingContacts[0].CultureCode = "en-GB";
                                            request.BookingContacts[0].SourceOrganization = agentId;
                                            request.BookingContacts[0].CustomerNumber = "";
                                            request.BookingContacts[0].DistributionOption = DistributionOption.Email;
                                            //Modified by Lokesh on 27-Nov-2017. Pass Lead Pax email for contact info.
                                            if (!string.IsNullOrEmpty(pax.Email))
                                            {
                                                request.BookingContacts[0].EmailAddress = pax.Email; //The Original Ticket details will be sent to this email ,not to the passenger email address.
                                            }
                                            else
                                            {
                                                request.BookingContacts[0].EmailAddress = "tech@cozmotravel.com"; //The Original Ticket details will be sent to this email ,not to the passenger email address.
                                            }
                                            request.BookingContacts[0].Fax = "";
                                            request.BookingContacts[0].HomePhone = pax.CellPhone;
                                            request.BookingContacts[0].Names = new BookingName[1];
                                            request.BookingContacts[0].Names[0] = new BookingName();
                                            request.BookingContacts[0].Names[0].FirstName = pax.FirstName;
                                            request.BookingContacts[0].Names[0].LastName = pax.LastName;
                                            request.BookingContacts[0].Names[0].MiddleName = "";
                                            request.BookingContacts[0].Names[0].State = Indigo.BookingMgr.MessageState.New;
                                            request.BookingContacts[0].Names[0].Suffix = "";
                                            request.BookingContacts[0].Names[0].Title = pax.Title.ToUpper();//Send always UPPER CASE
                                            request.BookingContacts[0].NotificationPreference = NotificationPreference.None;
                                            //Added by Lokesh on 4-Oct-2017 Regarding Lead Pax Destination Phone Number.
                                            if (!string.IsNullOrEmpty(pax.DestinationPhone))
                                            {
                                                request.BookingContacts[0].OtherPhone = pax.DestinationPhone;
                                            }
                                            else
                                            {
                                                request.BookingContacts[0].OtherPhone = "";
                                            }
                                            request.BookingContacts[0].PostalCode = "3093";
                                            request.BookingContacts[0].ProvinceState = "SHJ";
                                            request.BookingContacts[0].State = Indigo.BookingMgr.MessageState.New;
                                            request.BookingContacts[0].TypeCode = "P";
                                            request.BookingContacts[0].WorkPhone = pax.CellPhone;
                                        }
                                    }
                                }

                                bookRequest.BookingCommitRequestData = request;

                                try
                                {

                                    XmlSerializer ser = new XmlSerializer(typeof(BookingCommitRequest));
                                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoBookingCommitRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                    StreamWriter sw = new StreamWriter(filePath);
                                    ser.Serialize(sw, bookRequest);
                                    sw.Close();
                                }
                                catch { }
                                BookingCommitResponse bookingCommitResponse_v_4_5 = bookingAPI.BookingCommit(bookRequest);
                                responseData = bookingCommitResponse_v_4_5.BookingUpdateResponseData;

                                try
                                {

                                    XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoBookingCommitResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                    StreamWriter sw = new StreamWriter(filePath);
                                    ser.Serialize(sw, responseData);
                                    sw.Close();
                                }
                                catch { }

                                if (responseData != null && responseData.Error == null)
                                {
                                    if (!string.IsNullOrEmpty(responseData.Success.RecordLocator))
                                    {
                                        response.PNR = responseData.Success.RecordLocator;
                                        response.ProdType = ProductType.Flight;
                                        response.Error = "";
                                        response.Status = BookingResponseStatus.Successful;
                                        itinerary.PNR = response.PNR;
                                        itinerary.FareType = "Pub";
                                        GetBooking(responseData.Success.RecordLocator, logonResponse.Signature);
                                        itinerary.BookRequestType = BookingFlowStatus.BookingSuccess;
                                    }
                                    else
                                    {
                                        response.Status = BookingResponseStatus.Failed;
                                    }
                                }
                                else
                                {
                                    response.Status = BookingResponseStatus.Failed;
                                }
                            }
                            else //Failed to get the LogOn Response
                            {
                                throw new Exception("(Indigo)Failed to get the LogonResponse");
                            }
                        }
                        else//AddPayment failed
                        {
                            response.Status = BookingResponseStatus.Failed;
                            if (paymentResponse != null && paymentResponse.ValidationPayment != null && paymentResponse.ValidationPayment.PaymentValidationErrors != null && paymentResponse.ValidationPayment.PaymentValidationErrors.Length > 0)
                            {
                                response.Error = paymentResponse.ValidationPayment.PaymentValidationErrors[0].ErrorDescription;
                                Audit.Add(EventType.Book, Severity.High, appUserId, "Indigo Booking Failed. Reason : " + response.Error, "");
                            }
                        }
                    }
                    else
                    {


                        if (true)
                        //if (bookResponse != null && bookResponse.Success != null && bookResponse.Success.PNRAmount.TotalCost > 0)
                        {
                            int infantCount = 0;
                            foreach (FlightPassenger pax in itinerary.Passenger)
                            {
                                if (pax.Type == PassengerType.Infant)
                                {
                                    infantCount += 1;
                                }
                            }

                            string count = GetSSRCount(itinerary);
                            if (!string.IsNullOrEmpty(count))
                            {
                                int baggageSSRCount = Convert.ToInt32(count.Split('~')[0]);
                                int infantSSRCount = Convert.ToInt32(count.Split('~')[1]);
                                if (baggageSSRCount > 0 || infantSSRCount > 0)
                                {
                                    BookingUpdateResponseData bagResponse = BookSSR(itinerary, logonResponse.Signature);
                                    if (bagResponse != null && bagResponse.Success != null && bagResponse.Success.PNRAmount.TotalCost > 0)
                                    {
                                        bookResponse = bagResponse;
                                    }
                                    else
                                    {
                                        Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)SSR Baggage request Failed. Reason : " + bagResponse.OtherServiceInformations[0].Text, "");
                                        throw new Exception("Requested SSR Not available.Reason:" + bagResponse.OtherServiceInformations[0].Text);
                                    }
                                }
                            }



                            decimal bookingAmount = 0;
                            //No Seperate request for Infant, BookBaggage(...) method will handle infant requests
                            //if (infantCount > 0)
                            //{
                            //    //SellSSR by Infant
                            //    bookingAmount = SellSSRForInfant(itinerary, loginResponse.Signature);
                            //}
                            //else
                            {
                                //bookingAmount = bookResponse.Success.PNRAmount.TotalCost;
                            }

                            BookingUpdateResponseData upResponse = UpdatePassengers(itinerary, logonResponse.Signature);

                            if (upResponse != null && upResponse.Success != null && upResponse.Success.PNRAmount.TotalCost > 0)
                            {
                                bookingAmount = upResponse.Success.PNRAmount.TotalCost;
                                AddPaymentToBookingResponseData paymentResponse = AddPaymentForBooking(bookingAmount, logonResponse.Signature);

                                //If there are any Payment validations failed do not allow to commit booking
                                if (paymentResponse != null && paymentResponse.ValidationPayment != null && paymentResponse.ValidationPayment.PaymentValidationErrors != null && paymentResponse.ValidationPayment.PaymentValidationErrors.Length == 0)
                                {
                                    if (string.IsNullOrEmpty(itinerary.Endorsement)) // For normal search
                                    {
                                        logonResponse = Login();
                                    }


                                    if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                                    {
                                        BookingUpdateResponseData responseData = new BookingUpdateResponseData();
                                        BookingCommitRequest bookRequest = new BookingCommitRequest();
                                        bookRequest.ContractVersion = contractVersion;
                                        bookRequest.Signature = logonResponse.Signature;
                                        //Added by Lokesh on 11-June-2018
                                        //New skies Version 4.5
                                        //Need to send EnableExceptionStackTrace = false(by default) in the request.
                                        bookRequest.EnableExceptionStackTrace = false;

                                        BookingCommitRequestData request = new BookingCommitRequestData();
                                        request.BookingChangeCode = "";
                                        request.BookingComments = new BookingComment[0];

                                        request.SourcePOS = new PointOfSale();
                                        request.SourcePOS.AgentCode = "AG";
                                        request.SourcePOS.OrganizationCode = agentId;
                                        request.SourcePOS.DomainCode = agentDomain;

                                        request.BookingID = 0;
                                        request.BookingParentID = 0;
                                        request.ChangeHoldDateTime = false;
                                        request.CurrencyCode = currencyCode;
                                        request.DistributeToContacts = true;//Send booking confirmation email to the pax
                                        request.GroupName = "";
                                        request.NumericRecordLocator = "";
                                        request.ParentRecordLocator = "";
                                        request.RecordLocator = "";
                                        request.RecordLocators = new RecordLocator[0];
                                        request.RestrictionOverride = false;

                                        request.State = Indigo.BookingMgr.MessageState.New;
                                        request.SystemCode = "";
                                        request.WaiveNameChangeFee = false;
                                        request.WaivePenaltyFee = false;
                                        request.WaiveSpoilageFee = false;
                                        request.Passengers = new Passenger[0];
                                        request.PaxCount = (Int16)(itinerary.Passenger.Length - infantCount);

                                        for (int i = 0; i < itinerary.Passenger.Length; i++)
                                        {
                                            FlightPassenger pax = itinerary.Passenger[i];
                                            if (pax.Type != PassengerType.Infant)
                                            {
                                                if (pax.IsLeadPax)
                                                {
                                                    request.BookingContacts = new BookingContact[1];
                                                    request.BookingContacts[0] = new BookingContact();
                                                    request.BookingContacts[0].AddressLine1 = (string.IsNullOrEmpty(pax.AddressLine1) ? "Address Line1" : (pax.AddressLine1.Length <= 52 ? pax.AddressLine1 : pax.AddressLine1.Substring(0, 52)));//Address Line1 Must Be No more than 52 Characters in length.;
                                                    request.BookingContacts[0].AddressLine2 = pax.AddressLine2;
                                                    request.BookingContacts[0].AddressLine3 = "";
                                                    request.BookingContacts[0].City = "SHJ";
                                                    request.BookingContacts[0].CompanyName = "Cozmo Travel";
                                                    if (pax.Country != null && !string.IsNullOrEmpty(pax.Country.CountryCode))
                                                    {

                                                        request.BookingContacts[0].CountryCode = pax.Country.CountryCode;

                                                    }
                                                    else //Since we are hard coding city as "SHJ" so we are passing country code as "AE";
                                                    {
                                                        request.BookingContacts[0].CountryCode = "AE";
                                                    }
                                                    request.BookingContacts[0].CultureCode = "en-GB";
                                                    request.BookingContacts[0].SourceOrganization = agentId;
                                                    request.BookingContacts[0].CustomerNumber = "";
                                                    request.BookingContacts[0].DistributionOption = DistributionOption.Email;
                                                    //Modified by Lokesh on 27-Nov-2017. Pass Lead Pax email for contact info.
                                                    if (!string.IsNullOrEmpty(pax.Email))
                                                    {
                                                        request.BookingContacts[0].EmailAddress = pax.Email; //The Original Ticket details will be sent to this email ,not to the passenger email address.
                                                    }
                                                    else
                                                    {
                                                        request.BookingContacts[0].EmailAddress = "tech@cozmotravel.com"; //The Original Ticket details will be sent to this email ,not to the passenger email address.
                                                    }
                                                    request.BookingContacts[0].Fax = "";
                                                    request.BookingContacts[0].HomePhone = pax.CellPhone;
                                                    request.BookingContacts[0].Names = new BookingName[1];
                                                    request.BookingContacts[0].Names[0] = new BookingName();
                                                    request.BookingContacts[0].Names[0].FirstName = pax.FirstName;
                                                    request.BookingContacts[0].Names[0].LastName = pax.LastName;
                                                    request.BookingContacts[0].Names[0].MiddleName = "";
                                                    request.BookingContacts[0].Names[0].State = Indigo.BookingMgr.MessageState.New;
                                                    request.BookingContacts[0].Names[0].Suffix = "";
                                                    request.BookingContacts[0].Names[0].Title = pax.Title.ToUpper();//Send always UPPER CASE
                                                    request.BookingContacts[0].NotificationPreference = NotificationPreference.None;
                                                    //Added by Lokesh on 4-Oct-2017 Regarding Lead Pax Destination Phone Number.
                                                    if (!string.IsNullOrEmpty(pax.DestinationPhone))
                                                    {
                                                        request.BookingContacts[0].OtherPhone = pax.DestinationPhone;
                                                    }
                                                    else
                                                    {
                                                        request.BookingContacts[0].OtherPhone = "";
                                                    }
                                                    request.BookingContacts[0].PostalCode = "3093";
                                                    request.BookingContacts[0].ProvinceState = "SHJ";
                                                    request.BookingContacts[0].State = Indigo.BookingMgr.MessageState.New;
                                                    request.BookingContacts[0].TypeCode = "P";
                                                    request.BookingContacts[0].WorkPhone = pax.CellPhone;
                                                }
                                            }
                                        }

                                        bookRequest.BookingCommitRequestData = request;

                                        try
                                        {

                                            XmlSerializer ser = new XmlSerializer(typeof(BookingCommitRequest));
                                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoBookingCommitRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                            StreamWriter sw = new StreamWriter(filePath);
                                            ser.Serialize(sw, bookRequest);
                                            sw.Close();
                                        }
                                        catch { }
                                        BookingCommitResponse bookingCommitResponse_v_4_5 = bookingAPI.BookingCommit(bookRequest);
                                        responseData = bookingCommitResponse_v_4_5.BookingUpdateResponseData;

                                        try
                                        {

                                            XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoBookingCommitResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                                            StreamWriter sw = new StreamWriter(filePath);
                                            ser.Serialize(sw, responseData);
                                            sw.Close();
                                        }
                                        catch { }

                                        if (responseData != null)
                                        {
                                            if (responseData.Error == null)
                                            {
                                                response.PNR = responseData.Success.RecordLocator;
                                                response.ProdType = ProductType.Flight;
                                                response.Error = "";
                                                response.Status = BookingResponseStatus.Successful;
                                                itinerary.PNR = response.PNR;
                                                itinerary.FareType = "Pub";

                                                //Added by lokesh
                                                //For Code Share Flights need to call GetBooking Method
                                                if (!string.IsNullOrEmpty(responseData.Success.RecordLocator))
                                                {
                                                    GetBooking(responseData.Success.RecordLocator, logonResponse.Signature);
                                                }
                                            }
                                            else
                                            {
                                                response.Status = BookingResponseStatus.Failed;
                                            }
                                        }
                                        else
                                        {
                                            response.Status = BookingResponseStatus.Failed;
                                        }
                                    }
                                    else //Failed to get the LogOn Response
                                    {
                                        throw new Exception("(Indigo)Failed to get the LogonResponse");
                                    }
                                }
                                else//AddPayment failed
                                {
                                    response.Status = BookingResponseStatus.Failed;
                                    if (paymentResponse != null && paymentResponse.ValidationPayment != null && paymentResponse.ValidationPayment.PaymentValidationErrors != null && paymentResponse.ValidationPayment.PaymentValidationErrors.Length > 0)
                                    {
                                        response.Error = paymentResponse.ValidationPayment.PaymentValidationErrors[0].ErrorDescription;
                                        Audit.Add(EventType.Book, Severity.High, appUserId, "Indigo Booking Failed. Reason : " + response.Error, "");
                                    }
                                }
                            }
                            else//UpdatePassenger failed
                            {
                                response.Status = BookingResponseStatus.Failed;
                            }
                        }
                        else//Book failed
                        {
                            response.Status = BookingResponseStatus.Failed;
                        }
                    }
                }
                else
                {
                    throw new Exception("(Indigo)Failed to get the LogonResponse");
                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Book, Severity.High, 1, "(Indigo)Failed to Commit booking. Reason : " + ex.ToString(), "");
                response.Error = ex.Message.Contains("No such session") ? "Booking Session has been Expired. Please search Again" : ex.Message;
                response.Status = BookingResponseStatus.Failed;
                Logout(logonResponse.Signature);
            }
            finally
            {
                if (itinerary.BookRequestType != BookingFlowStatus.AssignSeat && itinerary.BookRequestType != BookingFlowStatus.PaxUpdated)
                  Logout(logonResponse.Signature);
            }

            return response;
        }

        #endregion

        #region Booking Cancellation

        /// <summary>
        /// Retrieves a booking by its PNR.
        /// </summary>
        /// <param name="pnr"></param>
        /// <param name="signature"></param>
        /// <returns></returns>
        public GetBookingResponse GetBooking(string pnr, string signature)
        {
            GetBookingResponse response = new GetBookingResponse();

            try
            {
                GetBookingRequest request = new GetBookingRequest();
                request.ContractVersion = contractVersion;
                request.Signature = signature;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                request.EnableExceptionStackTrace = false;

                GetBookingRequestData requestData = new GetBookingRequestData();
                requestData.GetBookingBy = GetBookingBy.RecordLocator;
                requestData.GetByRecordLocator = new GetByRecordLocator();
                requestData.GetByRecordLocator.RecordLocator = pnr;
                request.GetBookingReqData = requestData;

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoGetBookingRequest_" + appUserId.ToString() + "_" + pnr + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch { }

                response = bookingAPI.GetBooking(request);

                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoGetBookingResponse_" + appUserId.ToString() + "_" + pnr + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, response);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to get Booking for PNR : " + pnr + ". Reason : " + ex.ToString(), "");
            }

            return response;
        }

        /// <summary>
        /// Cancels the booking for the Itinerary.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public Dictionary<string, string> CancelBooking(FlightItinerary itinerary)
        {
            Dictionary<string, string> cancellationData = new Dictionary<string, string>();

            try
            {
                #region Indigo Cancel a Booking Flow

                /* The following steps describes the complete Cancel Booking Flow in Indigo.
                 * 1.LogOn Request.
                 * 2.Get Booking Request.
                 * 3.Cancel Request.
                 * 4.LogOut Request.
                 */

                LogonResponse loginResponse = Login();

                if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature) && loginResponse.Signature.Length > 0)
                {

                    try
                    {
                        GetBookingResponse response = GetBooking(itinerary.PNR, loginResponse.Signature);

                        CancelRequest request = new CancelRequest();
                        request.ContractVersion = contractVersion;
                        request.Signature = loginResponse.Signature;
                        //Added by Lokesh on 11-June-2018
                        //New skies Version 4.5
                        //Need to send EnableExceptionStackTrace = false(by default) in the request.
                        request.EnableExceptionStackTrace = false;

                        CancelRequestData requestData = new CancelRequestData();
                        requestData.CancelBy = CancelBy.Journey;
                        requestData.CancelJourney = new CancelJourney();
                        requestData.CancelJourney.CancelJourneyRequest = new CancelJourneyRequest();
                        requestData.CancelJourney.CancelJourneyRequest.Journeys = new Journey[response.Booking.Journeys.Length];
                        for (int i = 0; i < response.Booking.Journeys.Length; i++)
                        {
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i] = new Journey();
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments = new Segment[response.Booking.Journeys[i].Segments.Length];
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].State = Indigo.BookingMgr.MessageState.New;
                            requestData.CancelJourney.CancelJourneyRequest.Journeys[i].JourneySellKey = response.Booking.Journeys[i].JourneySellKey;
                            for (int j = 0; j < response.Booking.Journeys[i].Segments.Length; j++)
                            {
                                Segment segment = response.Booking.Journeys[i].Segments[j];
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j] = new Segment();
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ActionStatusCode = "NN";
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ArrivalStation = segment.ArrivalStation;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ChannelType = Indigo.BookingMgr.ChannelType.API;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].DepartureStation = segment.DepartureStation;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares = new Indigo.BookingMgr.Fare[segment.Fares.Length];
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].FlightDesignator = segment.FlightDesignator;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].International = segment.International;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs = new Leg[segment.Legs.Length];
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].SegmentSellKey = segment.SegmentSellKey;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].STA = segment.STA;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].State = Indigo.BookingMgr.MessageState.New;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].STD = segment.STD;
                                requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].ChannelType = segment.ChannelType;

                                for (int f = 0; f < response.Booking.Journeys[i].Segments[j].Fares.Length; f++)
                                {
                                    Indigo.BookingMgr.Fare fare = response.Booking.Journeys[i].Segments[j].Fares[f];
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f] = new Indigo.BookingMgr.Fare();
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].CarrierCode = fare.CarrierCode;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].ClassOfService = fare.ClassOfService;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].ClassType = fare.ClassType;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareApplicationType = fare.FareApplicationType;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareBasisCode = fare.FareBasisCode;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareClassOfService = fare.FareClassOfService;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareSellKey = fare.FareSellKey;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareSequence = fare.FareSequence;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].FareStatus = FareStatus.Default;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].InboundOutbound = fare.InboundOutbound;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].IsAllotmentMarketFare = false;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].RuleNumber = fare.RuleNumber;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].OriginalClassOfService = fare.OriginalClassOfService;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Fares[f].TravelClassCode = fare.TravelClassCode;
                                }
                                for (int l = 0; l < response.Booking.Journeys[i].Segments[j].Legs.Length; l++)
                                {
                                    Leg leg = response.Booking.Journeys[i].Segments[j].Legs[l];
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l] = new Leg();
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].ArrivalStation = leg.ArrivalStation;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].DepartureStation = leg.DepartureStation;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].FlightDesignator = leg.FlightDesignator;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].InventoryLegID = leg.InventoryLegID;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].LegInfo = leg.LegInfo;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].OperationsInfo = leg.OperationsInfo;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].STA = leg.STA;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].State = Indigo.BookingMgr.MessageState.New;
                                    requestData.CancelJourney.CancelJourneyRequest.Journeys[i].Segments[j].Legs[l].STD = leg.STD;
                                }
                            }
                            requestData.CancelJourney.CancelJourneyRequest.WaivePenaltyFee = false;
                            requestData.CancelJourney.CancelJourneyRequest.WaiveSpoilageFee = false;
                            requestData.CancelJourney.CancelJourneyRequest.PreventReprice = false;
                        }
                        request.CancelRequestData = requestData;
                        try
                        {

                            XmlSerializer ser = new XmlSerializer(typeof(CancelRequest));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoCancelRequest_" + appUserId.ToString() + "_" + itinerary.PNR + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, request);
                            sw.Close();
                        }
                        catch { }

                        //Using the existing bookingAPI
                        CancelResponse cancelRes_v_4_5 = bookingAPI.Cancel(request);
                        BookingUpdateResponseData responseData = cancelRes_v_4_5.BookingUpdateResponseData;

                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(BookingUpdateResponseData));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoCancelResponse_" + appUserId.ToString() + "_" + itinerary.PNR + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, responseData);
                            sw.Close();
                        }
                        catch { }

                        if (responseData != null)
                        {
                            if (responseData.Error == null)
                            {
                                string charges = responseData.Success.PNRAmount.TotalCost.ToString();
                                if (responseData.Success.PNRAmount.AlternateCurrencyBalanceDue > 0)
                                {
                                    charges = responseData.Success.PNRAmount.AlternateCurrencyBalanceDue.ToString().Replace("-", "");
                                }
                                rateOfExchange = (responseData.Success.PNRAmount.AlternateCurrencyCode != null && responseData.Success.PNRAmount.AlternateCurrencyCode.Trim().Length > 0 ? exchangeRates[responseData.Success.PNRAmount.AlternateCurrencyCode] : exchangeRates[currencyCode]);
                                cancellationData.Add("Cancelled", "True");
                                cancellationData.Add("Charges", (Convert.ToDecimal(charges) * rateOfExchange).ToString());
                            }
                            else
                            {
                                cancellationData.Add("Cancelled", "False");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.IndigoCancellation, Severity.High, appUserId, "(Indigo)Failed to Cancel booking. Reason : " + ex.ToString(), "");
                        throw ex;
                    }
                    Logout(loginResponse.Signature);
                }
                else
                {
                    throw new Exception("(Indigo)Failed to get the LogonResponse");
                }




                #endregion
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed Cancel Booking Failed : Reason : " + ex.ToString(), "");
            }

            return cancellationData;
        }

        #endregion

        #region Helper methods
        /// <summary>
        /// Returns the Fare Type for a Product Class 
        /// </summary>
        /// <param name="productClass"></param>
        /// <returns></returns>
        private string GetFareTypeForProductClass(string productClass)
        {
            string fareType = "";
            try
            {

                switch (productClass)
                {
                    case "R":
                        fareType = "Saver Fare";
                        break;
                    case "S":
                        fareType = "Sale Fare";
                        break;
                    case "A":
                        fareType = "Family Fare"; //(applicable for 4 - 9 passengers)
                        break;
                    case "N":
                        fareType = "SPECIAL ROUND TRIP FARE";
                        break;
                    case "J":
                        fareType = "Flexi Plus Fare";
                        break;
                    case "B":
                        fareType = "Lite Fare";
                        break;
                    case "F":
                        fareType = "Corporate Fare";
                        break;
                    case "M":
                        fareType = "SME Fare";
                        break;
                    case "T":
                        fareType = "Promo Fare";
                        break;

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Indigo failed to get Get Fare Type For ProductClass . Reason : " + ex.Message, ex);
            }
            return fareType;
        }

        #endregion
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~IndigoAPI()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion


        //public List<SearchResult>GetJourneyResult(Journey[] journeys)
        //{
        //    List<SearchResult> ResultList = new List<SearchResult>();
        //    //Loop Variables -- j-- journey
        //    //s--Segment
        //    //f-- Fare
        //    //Direct flight – Segment and leg count should be 1 in journey.
        //    //Via Flight – Segment count should be 1 and leg count should be more than 1.
        //    //Connecting flight – Segment count should be more than 1.

        //    try
        //    {
        //        if(journeys != null && journeys.Length >0)
        //        {
        //           for(int j=0;j<journeys.Length;j++)
        //            {
        //                if (journeys[j].Segments != null && journeys[j].Segments.Length > 0)
        //                {
        //                    if(journeys[j].Segments.Length>1)//Connecting
        //                    {
        //                        List<Indigo.BookingMgr.Fare> connectingFares = new List<Indigo.BookingMgr.Fare>();
        //                        connectingFares.Clear();

        //                        for (int s=0;s<journeys[j].Segments.Length;s++)
        //                        {
        //                           if(journeys[j].Segments[s].Fares != null && journeys[j].Segments[s].Fares.Length >0)
        //                            {
        //                                 //List<Indigo.BookingMgr.Fare> conFares = new List<Indigo.BookingMgr.Fare>(journeys[j].Segments[s].Fares);
        //                                 //conFares.FindAll(delegate (Indigo.BookingMgr.Fare f) { return f.ProductClass == connectingFares[0].ProductClass; });
        //                                for(int f=0;f< journeys[j].Segments[s].Fares.Length;f++)
        //                                {
        //                                    if (connectingFares.Count > 0 && connectingFares[0].ProductClass == journeys[j].Segments[s].Fares[f].ProductClass)
        //                                    {
        //                                        connectingFares.Add(journeys[j].Segments[s].Fares[f]);
        //                                    }
        //                                    else
        //                                    {
        //                                        connectingFares.Add(journeys[j].Segments[s].Fares[f]);
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if(journeys[j].Segments.Length ==1)//Direct
        //                    {
        //                        for (int s = 0; s < journeys[j].Segments.Length; s++)
        //                        {
        //                            if (journeys[j].Segments[s].Fares != null && journeys[j].Segments[s].Fares.Length > 0)
        //                            {
        //                                for (int f = 0; f < journeys[j].Segments[s].Fares.Length; f++)
        //                                {

        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return ResultList;
        //}

        /// <summary>
        /// This methods returns the flight objects info based on segments legs info
        /// </summary>
        /// <param name="segment"></param>
        /// <param name="availableJourney"></param>
        /// <param name="productClass"></param>
        /// <param name="journeyType"></param>
        /// <returns></returns>
        public List<FlightInfo> GetFlightInfo(Segment segment, Journey availableJourney, string productClass, string journeyType)
        {
            List<FlightInfo> flightInfo = new List<FlightInfo>();
            try
            {
                if (availableJourney != null && segment != null && segment.Legs != null && segment.Legs.Length > 0)
                {
                    for (int k = 0; k < segment.Legs.Length; k++)
                    {
                        if (segment.Legs[k] != null)
                        {
                            FlightInfo flightDetails = new FlightInfo();
                            flightDetails.Airline = "6E";
                            flightDetails.ArrivalTime = segment.Legs[k].STA;
                            flightDetails.ArrTerminal = segment.Legs[k].LegInfo.ArrivalTerminal;
                            flightDetails.BookingClass = (segment != null && segment.CabinOfService.Trim().Length > 0 ? segment.CabinOfService : "C");
                            if (!string.IsNullOrEmpty(segment.CabinOfService.Trim()))
                            {
                                flightDetails.CabinClass = segment.CabinOfService;
                            }
                            else
                            {
                                flightDetails.CabinClass = "Economy";
                            }
                            flightDetails.Craft = segment.Legs[k].LegInfo.EquipmentType + "-" + segment.Legs[k].LegInfo.EquipmentTypeSuffix;
                            flightDetails.DepartureTime = segment.Legs[k].STD;
                            flightDetails.DepTerminal = segment.Legs[k].LegInfo.DepartureTerminal;
                            flightDetails.Destination = new Airport(segment.Legs[k].ArrivalStation);
                            flightDetails.Duration = flightDetails.ArrivalTime.Subtract(flightDetails.DepartureTime);
                            flightDetails.ETicketEligible = segment.Legs[k].LegInfo.ETicket;
                            flightDetails.FlightNumber = segment.Legs[k].FlightDesignator.FlightNumber;
                            flightDetails.FlightStatus = FlightStatus.Confirmed;
                            if (journeyType.ToUpper() == "ONWARD")
                            {
                                flightDetails.Group = 0;
                            }
                            else if (journeyType.ToUpper() == "RETURN")
                            {
                                flightDetails.Group = 1;
                            }
                            flightDetails.OperatingCarrier = "6E";
                            flightDetails.Origin = new Airport(segment.Legs[k].DepartureStation);
                            flightDetails.Stops = availableJourney.Segments.Length - 1;

                            //Direct flight
                            if (segment.Legs.Length == 1 && availableJourney.Segments.Length == 1)
                            {
                                flightDetails.Status = string.Empty;
                            }
                            //Connecting flight
                            else if (availableJourney.Segments.Length > 1)
                            {
                                int legCount = 0;
                                for (int s = 0; s < availableJourney.Segments.Length; s++)
                                {
                                    legCount += availableJourney.Segments[s].Legs.Length;
                                }
                                if (legCount == 2)
                                {
                                    flightDetails.Status = string.Empty; //Connecting direct flight
                                }
                                else
                                {
                                    flightDetails.Status = "VIA"; //Connecting via flight
                                }
                            }
                            //VIA Flight
                            else if (segment.Legs.Length > 1 && availableJourney.Segments.Length == 1)
                            {
                                flightDetails.Status = "VIA";
                            }
                            flightDetails.UapiSegmentRefKey = segment.SegmentSellKey;
                            flightDetails.SegmentFareType = GetFareTypeForProductClass(productClass);

                            //Code Share means:The Operating Carrier is the airline that flies the plane.
                            //The marketing carrier the partner airline that sells seats on the operating carriers flights.
                            //Code Share Flights Identification
                            //1.SegmentType--> 'L' for Code Share Flights.
                            //2.Code Share Flight Number : Journeys-->Segments-->XrefFlightDesignator-->FlightNumber.
                            //3.Code Share Carrier Code: Journeys-->Segments-->XrefFlightDesignator-->Carrier Code.
                            if (!string.IsNullOrEmpty(segment.SegmentType) && segment.SegmentType.ToUpper() == "L") //Code Share
                            {
                                flightDetails.OperatingCarrier = segment.Legs[k].LegInfo.OperatingCarrier;
                            }
                            flightInfo.Add(flightDetails);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return flightInfo;
        }

        /************GST Flow For India*****************/
        /*********Start:Steps for GST Flow*******************/
        /*1.Logon Request
         *2.GetAvailabilityRequest
         *3.GetItineraryPriceRequest
         *4.UpdateContactsRequest(GST Parameters Passed in this method)
         *5.Sell Request
         *6.GetBookingFromStateRequest[Gets the bifurfacion of taxes in this method]
         *7.UpdatePassengersRequest.
         *8.AddPaymentToBookingRequest.
         *9.BookingCommitRequest.
         *10.LogOutRequest
         /*********End:Steps for GST Flow*******************/


        /****Below are the important points with respect to GST implementation****/
        //1.GST Implementation.
        //2.All carriers travelling to/ from /within india be able to apply the correct GST charges.
        //3.GST Registration Number ,Name of GST holder, Email of GST holder.
        //4.If PNR has multiple passengers only 1 GST Registration Number is allowed per PNR
        //5.In case of B2B transactions,2 new method calls will be used in the booking flow.
        //6.Methods: UpdateContactRequest and GetBookingFromState.

        ///<summary>
        /// Gets the Updated GSTFareBreakUp 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public void GetUpdatedGSTFareBreakUp(ref SearchResult result, SearchRequest request)
        {
            try
            {
                //If the lead pax supplies the GST input.
                //Then Get the updated GST farebreak up.
                logonResponse = new LogonResponse();
                logonResponse.Signature = result.RepriceErrorMessage;////Till Search To Book This Signature Should be carried.
                //Get the bifurcation of taxes after GST input.
                if (logonResponse != null && logonResponse.Signature.Length > 0)
                {
                    GetBookingFromStateResponse updatedGSTBreakup = GetBookingFromState(logonResponse.Signature);
                    if (updatedGSTBreakup != null && updatedGSTBreakup.BookingData != null && updatedGSTBreakup.BookingData.Journeys != null && updatedGSTBreakup.BookingData.Journeys.Length > 0)
                    {
                        rateOfExchange = exchangeRates[updatedGSTBreakup.BookingData.CurrencyCode];

                        List<PaxFare> adultPaxfare = new List<PaxFare>();
                        List<PaxFare> childPaxfare = new List<PaxFare>();


                        List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUp = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
                        List<KeyValuePair<String, decimal>> adtPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> chdPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();
                        List<KeyValuePair<String, decimal>> inftPaxTaxBreakUp = new List<KeyValuePair<String, decimal>>();

                        paxTypeTaxBreakUp.Clear();
                        adtPaxTaxBreakUp.Clear();
                        chdPaxTaxBreakUp.Clear();
                        inftPaxTaxBreakUp.Clear();

                        for (int journey = 0; journey < updatedGSTBreakup.BookingData.Journeys.Length; journey++)
                        {
                            if (updatedGSTBreakup.BookingData.Journeys[journey] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length > 0)
                            {
                                for (int segment = 0; segment < updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length; segment++)
                                {
                                    if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length > 0)
                                    {
                                        for (int fare = 0; fare < updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length; fare++)
                                        {
                                            if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).Count() > 0)
                                            {
                                                adultPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).FirstOrDefault());
                                            }
                                            if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).Count() > 0)
                                            {
                                                childPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).FirstOrDefault());
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        result.Price = new PriceAccounts();
                        result.BaseFare = 0;
                        result.Tax = 0;
                        result.TotalFare = 0;
                        result.FareBreakdown = null;
                        int fareBreakDownCount = 0;
                        if (request.AdultCount > 0)
                        {
                            fareBreakDownCount = 1;
                        }
                        if (request.ChildCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        if (request.InfantCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        result.FareBreakdown = new Fare[fareBreakDownCount];
                        if (request.AdultCount > 0)
                        {
                            result.FareBreakdown[0] = new Fare();
                            result.FareBreakdown[0].PassengerCount = request.AdultCount;
                            result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                        }
                        if (request.ChildCount > 0)
                        {
                            result.FareBreakdown[1] = new Fare();
                            result.FareBreakdown[1].PassengerCount = request.ChildCount;
                            result.FareBreakdown[1].PassengerType = PassengerType.Child;
                        }
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            result.FareBreakdown[2] = new Fare();
                            result.FareBreakdown[2].PassengerCount = request.InfantCount;
                            result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                        }
                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                        {
                            result.FareBreakdown[1] = new Fare();
                            result.FareBreakdown[1].PassengerCount = request.InfantCount;
                            result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                        }
                        if (adultPaxfare != null && adultPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareAdult in adultPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareAdult.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (result.FareBreakdown[0] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case Indigo.BookingMgr.ChargeType.FarePrice:
                                                    double baseFareAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                    result.FareBreakdown[0].BaseFare += baseFareAdult;
                                                    result.FareBreakdown[0].TotalFare += baseFareAdult;
                                                    result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                    result.Price.PublishedFare += (decimal)baseFareAdult;
                                                    result.Price.SupplierPrice += charge.Amount;
                                                    
                                                    break;

                                                case Indigo.BookingMgr.ChargeType.PromotionDiscount:
                                                    double discountAdult = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                    result.FareBreakdown[0].BaseFare = result.FareBreakdown[0].BaseFare - discountAdult;
                                                    result.FareBreakdown[0].TotalFare = result.FareBreakdown[0].TotalFare - discountAdult;
                                                    result.FareBreakdown[0].SupplierFare = result.FareBreakdown[0].SupplierFare - (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                    result.Price.PublishedFare = (decimal)(result.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                                                    result.Price.SupplierPrice = result.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case Indigo.BookingMgr.ChargeType.TravelFee:
                                                case Indigo.BookingMgr.ChargeType.Tax:
                                                case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                case Indigo.BookingMgr.ChargeType.Calculated:
                                                case Indigo.BookingMgr.ChargeType.Note:
                                                case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                case Indigo.BookingMgr.ChargeType.Loyalty:
                                                case Indigo.BookingMgr.ChargeType.FarePoints:
                                                case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                case Indigo.BookingMgr.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                    result.FareBreakdown[0].TotalFare += tax;
                                                    result.FareBreakdown[0].SupplierFare += (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                    result.Price.Tax += (decimal)tax;
                                                    result.Price.SupplierPrice += charge.Amount;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                         result.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * result.FareBreakdown[0].PassengerCount);
                                                    }
                                                    adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (childPaxfare != null && childPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareChild in childPaxfare)
                            {
                                foreach (BookingServiceCharge charge in paxFareChild.ServiceCharges)
                                {
                                    if (charge != null)
                                    {
                                        if (result.FareBreakdown[1] != null)
                                        {
                                            switch (charge.ChargeType)
                                            {
                                                case Indigo.BookingMgr.ChargeType.FarePrice:
                                                    double baseFareChild = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                    result.FareBreakdown[1].BaseFare += baseFareChild;
                                                    result.FareBreakdown[1].TotalFare += baseFareChild;
                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                    result.Price.PublishedFare += (decimal)baseFareChild;
                                                    result.Price.SupplierPrice += charge.Amount;
     
                                                    break;

                                                case Indigo.BookingMgr.ChargeType.PromotionDiscount:
                                                    double discountChild = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                    result.FareBreakdown[1].BaseFare = result.FareBreakdown[1].BaseFare - discountChild;
                                                    result.FareBreakdown[1].TotalFare = result.FareBreakdown[1].TotalFare - discountChild;
                                                    result.FareBreakdown[1].SupplierFare = result.FareBreakdown[0].SupplierFare - (double)(charge.Amount * result.FareBreakdown[0].PassengerCount);
                                                    result.Price.PublishedFare = (decimal)(result.Price.PublishedFare - Convert.ToDecimal(discountChild));
                                                    result.Price.SupplierPrice = result.Price.SupplierPrice - charge.Amount;
                                                    break;
                                                case Indigo.BookingMgr.ChargeType.TravelFee:
                                                case Indigo.BookingMgr.ChargeType.Tax:
                                                case Indigo.BookingMgr.ChargeType.ServiceCharge:
                                                case Indigo.BookingMgr.ChargeType.ConnectionAdjustmentAmount:
                                                case Indigo.BookingMgr.ChargeType.AddOnServicePrice:
                                                case Indigo.BookingMgr.ChargeType.AddOnServiceFee:
                                                case Indigo.BookingMgr.ChargeType.Calculated:
                                                case Indigo.BookingMgr.ChargeType.Note:
                                                case Indigo.BookingMgr.ChargeType.AddOnServiceMarkup:
                                                case Indigo.BookingMgr.ChargeType.FareSurcharge:
                                                case Indigo.BookingMgr.ChargeType.Loyalty:
                                                case Indigo.BookingMgr.ChargeType.FarePoints:
                                                case Indigo.BookingMgr.ChargeType.DiscountPoints:
                                                case Indigo.BookingMgr.ChargeType.AddOnServiceCancelFee:
                                                case Indigo.BookingMgr.ChargeType.Unmapped:
                                                    double tax = (double)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                    result.FareBreakdown[1].TotalFare += tax;
                                                    result.FareBreakdown[1].SupplierFare += (double)(charge.Amount * result.FareBreakdown[1].PassengerCount);
                                                    result.Price.Tax += (decimal)tax;
                                                    result.Price.SupplierPrice += charge.Amount;
                                                    if (charge.ChargeCode.ToString().Contains("GST"))
                                                    {
                                                         result.Price.K3Tax += (decimal)((charge.Amount * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                                    }
                                                    chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (request.InfantCount > 0)
                        {
                            for (int journey = 0; journey < updatedGSTBreakup.BookingData.Journeys.Length; journey++)
                            {

                                decimal infantPrice = 0;
                                infantPrice = GetInfantAvailibilityFares(request);
                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                {
                                    double baseFare = (double)((infantPrice * rateOfExchange) * result.FareBreakdown[2].PassengerCount);
                                    result.FareBreakdown[2].TotalFare += baseFare;
                                    result.FareBreakdown[2].BaseFare += baseFare;
                                    result.FareBreakdown[2].SupplierFare += (double)(infantPrice * result.FareBreakdown[2].PassengerCount);
                                    result.Price.PublishedFare += (decimal)baseFare;
                                    result.Price.SupplierPrice += infantPrice;
                                }
                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                {
                                    double baseFare = (double)((infantPrice * rateOfExchange) * result.FareBreakdown[1].PassengerCount);
                                    result.FareBreakdown[1].BaseFare += baseFare;
                                    result.FareBreakdown[1].TotalFare += baseFare;
                                    result.FareBreakdown[1].SupplierFare += (double)(infantPrice * result.FareBreakdown[1].PassengerCount);
                                    result.Price.PublishedFare += (decimal)baseFare;
                                    result.Price.SupplierPrice += infantPrice;
                                }
                            }
                        }

                        if (adtPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                        }
                        if (chdPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                        }
                        if (inftPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                        }
                        if (paxTypeTaxBreakUp.Count > 0)
                        {
                            result.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                        }
                        result.Price.SupplierCurrency = currencyCode;
                        result.BaseFare = (double)result.Price.PublishedFare;
                        result.Tax = (double)result.Price.Tax;
                        result.TotalFare = (double)Math.Round((result.BaseFare + result.Tax), agentDecimalValue);
                    }
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo)Failed to GetUpdatedGSTFareBreakUp. Error : " + ex.ToString(), "");
                throw ex;
            }

        }

        /// <summary>
        /// Updates the GST details for the lead passenger.
        /// </summary>
        /// <returns></returns>
        public UpdateContactsResponse UpdateContactGSTDetails(string signature)
        {
            UpdateContactsResponse updateContactsResponse = new UpdateContactsResponse();
            try
            {
                UpdateContactsRequest updateContactsRequest = new UpdateContactsRequest();
                //Header
                updateContactsRequest.ContractVersion = contractVersion;
                updateContactsRequest.Signature = signature;
                updateContactsRequest.EnableExceptionStackTrace = false;
                //Body
                UpdateContactsRequestData updateContactsRequestData = new UpdateContactsRequestData();
                updateContactsRequestData.BookingContactList = new BookingContact[1];
                updateContactsRequestData.BookingContactList[0] = new BookingContact();
                updateContactsRequestData.BookingContactList[0].State = Indigo.BookingMgr.MessageState.New;
                updateContactsRequestData.BookingContactList[0].TypeCode = "I";
                updateContactsRequestData.BookingContactList[0].EmailAddress = gstOfficialEmail;
                updateContactsRequestData.BookingContactList[0].CustomerNumber = gstNumber;
                updateContactsRequestData.BookingContactList[0].CompanyName = gstCompanyName;
                updateContactsRequestData.BookingContactList[0].DistributionOption = DistributionOption.None;
                updateContactsRequestData.BookingContactList[0].NotificationPreference = NotificationPreference.None;
                updateContactsRequest.updateContactsRequestData = updateContactsRequestData;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(UpdateContactsRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoLeadPaxGSTRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, updateContactsRequest);
                    sw.Close();
                }
                catch { }
                updateContactsResponse = bookingAPI.UpdateContacts(updateContactsRequest);
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(UpdateContactsResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoLeadPaxGSTResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, updateContactsResponse);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo)Failed to update GST Details for the lead passenger. Error : " + ex.ToString(), "");
                throw ex;
            }
            return updateContactsResponse;
        }

        /// <summary>
        /// To Get the bifurcation of Taxes after GSTN update.
        /// </summary>
        /// <returns></returns>
        public GetBookingFromStateResponse GetBookingFromState(string signature)
        {
            GetBookingFromStateResponse getBookingFromStateResponse = new GetBookingFromStateResponse();
            Booking booking = new Booking();
            try
            {
                GetBookingFromStateRequest getBookingFromStateRequest = new GetBookingFromStateRequest();
                getBookingFromStateRequest.ContractVersion = contractVersion;
                getBookingFromStateRequest.Signature = signature;
                getBookingFromStateRequest.EnableExceptionStackTrace = false;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingFromStateRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_GST_Tax_Breakup_Request_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, getBookingFromStateRequest);
                    sw.Close();
                }
                catch { }
                getBookingFromStateResponse = bookingAPI.GetBookingFromState(getBookingFromStateRequest);
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(GetBookingFromStateResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_GST_Tax_Breakup_Response_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, getBookingFromStateResponse);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo)Failed to get updated GST break up for the lead passenger. Error : " + ex.ToString(), "");
                throw ex;
            }
            return getBookingFromStateResponse;
        }

        /// <summary>
        /// This method returns the Available SSR's based on the segments.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public DataTable GetAvailableSSR(SearchResult result)
        {
            DataTable dtSourceBaggage = null;
            logonResponse = new LogonResponse();
            logonResponse.Signature = result.RepriceErrorMessage;//Till Search To Book Same Signature Should be carried
            //This method will releases the itinerary.
            //Clears the previous objects from the session if any objects are there.
            //This method will avoid duplicate bookings.
            if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
            {
                ClearRequest clearRequest = new ClearRequest();
                clearRequest.ContractVersion = contractVersion;
                clearRequest.EnableExceptionStackTrace = false;
                clearRequest.Signature = logonResponse.Signature;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(ClearRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_ClearRequest_" + appUserId.ToString() + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, clearRequest);
                    sw.Close();
                }
                catch { }
                ClearResponse clearResponse = bookingAPI.Clear(clearRequest);
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(ClearResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_ClearResponse_" + appUserId.ToString() + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, clearResponse);
                    sw.Close();
                }
                catch { }
            }

            //Marketing CodeShare Schedule Flights Count
            //Kindly be informed that as per latest guidelines we are not allowed to sell excess baggage on MCS flights
            List<FlightInfo> flights = new List<FlightInfo>(result.Flights[0]);
            if (result.Flights.Length > 1)
            {
                flights.AddRange(result.Flights[1]);
            }
            int codeShareFlightCount = flights.FindAll(a => !a.OperatingCarrier.Trim().Equals("6E")).Count();

            try
            {

                //SSR : AdditionalBaggage and Meals [For Both Onward and Return Journeys]
                //Note:Infant is also an SSR.
                //But we will get the infant price from the price itinerary response.

                //Steps to get the available SSR Without GST input.
                //1.Login
                //2.Sell [Holds the complete itinerary]
                //3.GetSSRAvailabilityForBooking.

                //Steps to get the available SSR With GST input.
                //1.Login
                //2.UpdateContactRequest 
                //3.Sell[Holds the complete itinerary]
                //4.GetSSRAvailabilityForBooking
                //5.GetBookingFromState


                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {

                    //Note:Verify any GST input available then execute UpdateContactsRequest first.
                    //Step-1.1: If any GST input then UpdateContactGSTDetails method
                    UpdateContactsResponse updateGSTResponse = null;
                    if (!string.IsNullOrEmpty(gstCompanyName) && (gstCompanyName.Length > 0) &&
                       !string.IsNullOrEmpty(gstNumber) && (gstNumber.Length > 0) &&
                        !string.IsNullOrEmpty(gstOfficialEmail) && (gstOfficialEmail.Length > 0)
                       && !string.IsNullOrEmpty(logonResponse.Signature))
                    {

                        updateGSTResponse = UpdateContactGSTDetails(logonResponse.Signature);

                    }

                    //Step2:Sell
                    BookingUpdateResponseData responseData = ExecuteSell(logonResponse.Signature, result);
                    if (responseData != null && codeShareFlightCount == 0)
                    //Kindly be informed that as per latest guidelines we are not allowed to sell excess baggage on MCS(Code Share) flights.
                    //1.If there are any code share flights we will not call excess baggage request.
                    {
                        //Step-3: GetAvailableSSR

                        GetSSRAvailabilityForBookingResponse ssrResponse = new GetSSRAvailabilityForBookingResponse();
                        GetSSRAvailabilityForBookingRequest ssrRequest = new GetSSRAvailabilityForBookingRequest();
                        ssrRequest.ContractVersion = contractVersion;
                        ssrRequest.EnableExceptionStackTrace = false;
                        ssrRequest.Signature = logonResponse.Signature;

                        SSRAvailabilityForBookingRequest availRequest = new SSRAvailabilityForBookingRequest();
                        availRequest.InventoryControlled = true;
                        availRequest.NonInventoryControlled = true;
                        availRequest.SeatDependent = true;
                        availRequest.NonSeatDependent = true;
                        availRequest.CurrencyCode = currencyCode;

                        List<LegKey> legKeysList = new List<LegKey>();
                        for (int i = 0; i < result.Flights.Length; i++)
                        {
                            for (int j = 0; j < result.Flights[i].Length; j++)
                            {
                                LegKey legKey = new LegKey();
                                legKey.CarrierCode = "6E";
                                legKey.FlightNumber = result.Flights[i][j].FlightNumber;
                                legKey.DepartureDate = result.Flights[i][j].DepartureTime;
                                legKey.DepartureStation = result.Flights[i][j].Origin.AirportCode;
                                legKey.ArrivalStation = result.Flights[i][j].Destination.AirportCode;
                                legKeysList.Add(legKey);
                            }
                        }
                        if (legKeysList != null && legKeysList.Count > 0)
                        {
                            availRequest.SegmentKeyList = legKeysList.ToArray();
                        }
                        ssrRequest.SSRAvailabilityForBookingRequest = availRequest;
                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(GetSSRAvailabilityForBookingRequest));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_PaxSSR_Request_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, ssrRequest);
                            sw.Close();
                        }
                        catch { }
                        ssrResponse = bookingAPI.GetSSRAvailabilityForBooking(ssrRequest);
                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(GetSSRAvailabilityForBookingResponse));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_PaxSSR_Response_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, ssrResponse);
                            sw.Close();
                        }
                        catch { }
                        if (ssrResponse != null && ssrResponse.SSRAvailabilityForBookingResponse != null && ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList != null && ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList.Length > 0)
                        {
                            dtSourceBaggage = new DataTable();

                            List<AvailablePaxSSR> availablePaxSSRs_OnwardBag = new List<AvailablePaxSSR>();//OnwardBaggage
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnBag = new List<AvailablePaxSSR>();//ReturnBaggage

                            List<AvailablePaxSSR> availablePaxSSRs_OnwardMeal = new List<AvailablePaxSSR>();//OnwardMeal
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnMeal = new List<AvailablePaxSSR>();//ReturnMeal

                            for (int i = 0; i < result.Flights.Length; i++)
                            {
                                for (int j = 0; j < result.Flights[i].Select(x => x.FlightNumber).Distinct().ToArray().Count(); j++)
                                {
                                    string DepartureStation = result.Flights[i][j].Origin.AirportCode;
                                    string ArrivalStation = result.Flights[i][j].Destination.AirportCode;

                                    for (int p = 0; p < ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList.Length; p++)
                                    {
                                        SSRSegment sSRSegment = ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList[p];
                                        if (sSRSegment.AvailablePaxSSRList != null && sSRSegment.AvailablePaxSSRList.Length > 0 && sSRSegment.LegKey.DepartureStation == DepartureStation && sSRSegment.LegKey.ArrivalStation == ArrivalStation)
                                        {
                                            
                                                if (i == 0)
                                                {
                                                    availablePaxSSRs_OnwardBag.AddRange(sSRSegment.AvailablePaxSSRList.ToList().Where(x => x.SSRCode.StartsWith("XB")));
                                                    availablePaxSSRs_OnwardBag.AddRange(sSRSegment.AvailablePaxSSRList.ToList().Where(x => x.SSRCode.StartsWith("IX")));
                                                    availablePaxSSRs_OnwardMeal.AddRange(sSRSegment.AvailablePaxSSRList.ToList());
                                                }
                                                else
                                                {
                                                    availablePaxSSRs_ReturnBag.AddRange(sSRSegment.AvailablePaxSSRList.ToList().Where(x => x.SSRCode.StartsWith("XB")));
                                                    availablePaxSSRs_ReturnBag.AddRange(sSRSegment.AvailablePaxSSRList.ToList().Where(x => x.SSRCode.StartsWith("IX")));
                                                    availablePaxSSRs_ReturnMeal.AddRange(sSRSegment.AvailablePaxSSRList.ToList());

                                            }
                                        }
                                    }

                                }
                            }

                            if (availablePaxSSRs_OnwardBag.Count > 0)
                            {
                                GetIndigoSSRs(ref dtSourceBaggage, "0", availablePaxSSRs_OnwardBag, "Baggage");
                            }

                            if (availablePaxSSRs_ReturnBag.Count > 0)
                            {
                                GetIndigoSSRs(ref dtSourceBaggage, "1", availablePaxSSRs_ReturnBag, "Baggage");
                            }

                            if (availablePaxSSRs_OnwardMeal.Count > 0)
                            {
                                GetIndigoSSRs(ref dtSourceBaggage, "0", availablePaxSSRs_OnwardMeal,"Meal");
                            } //OnwardMeal

                            if (availablePaxSSRs_ReturnMeal.Count > 0)
                            {
                                GetIndigoSSRs(ref dtSourceBaggage, "1", availablePaxSSRs_ReturnMeal,"Meal");
                            } //ReturnMeal


                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //In case of exception in any of the above requests call the clear method.
                //This method will release the itinerary.
                //Clears the session.
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {
                    ClearRequest clearRequest = new ClearRequest();
                    clearRequest.ContractVersion = contractVersion;
                    clearRequest.EnableExceptionStackTrace = false;
                    clearRequest.Signature = logonResponse.Signature;
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_ClearRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearRequest);
                        sw.Close();
                    }
                    catch { }
                    ClearResponse clearResponse = bookingAPI.Clear(clearRequest);
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearResponse));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_ClearResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearResponse);
                        sw.Close();
                    }
                    catch { }
                }
                throw ex;
            }
            return dtSourceBaggage;
        }

        /// <summary>
        /// This method is used to perform SellRequest to get additional baggage.
        /// </summary>
        /// <param name="signature"></param>
        /// <param name="searchResult"></param>
        /// <returns></returns>
        private BookingUpdateResponseData ExecuteSell(string signature, SearchResult searchResult)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                FlightItinerary itinerary = new FlightItinerary();
                itinerary.Segments = SearchResult.GetSegments(searchResult);
                itinerary.TicketAdvisory = searchResult.FareSellKey;

                SellRequestData requestData = new SellRequestData();
                SellRequest request = new SellRequest();

                request.ContractVersion = contractVersion;
                request.Signature = signature;
                //Added by Lokesh on 11-June-2018
                //New skies Version 4.5
                //Need to send EnableExceptionStackTrace = false(by default) in the request.
                request.EnableExceptionStackTrace = false;
                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = currencyCode;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;

                //Define SourcePOS data
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS = new PointOfSale();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.AgentCode = "AG";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.DomainCode = agentDomain;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.OrganizationCode = agentId;

                string onwardFareSellKey = string.Empty;
                string onwardJourneySellKey = string.Empty;
                string returnFareSellKey = string.Empty;
                string returnJourneySellKey = string.Empty;

                List<string> sellKeysListOnward = new List<string>();
                List<string> sellKeysListReturn = new List<string>();
                for (int p = 0; p < itinerary.Segments.Length; p++)
                {
                    if (itinerary.Segments[p].Group == 0)//Onward
                    {
                        sellKeysListOnward.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                    if (itinerary.Segments[p].Group == 1)//Return
                    {
                        sellKeysListReturn.Add(itinerary.Segments[p].UapiSegmentRefKey);
                    }
                }
                IEnumerable<string> distinctSellKeysOnward = sellKeysListOnward.Distinct();
                IEnumerable<string> distinctSellKeysReturn = sellKeysListReturn.Distinct();

                if (distinctSellKeysOnward.Count() > 0)
                {
                    foreach (string key in distinctSellKeysOnward)
                    {
                        if (onwardJourneySellKey.Length > 0)
                        {
                            onwardJourneySellKey += "^" + key;
                        }
                        else
                        {
                            onwardJourneySellKey = key;
                        }
                    }
                }

                if (distinctSellKeysReturn.Count() > 0)
                {
                    foreach (string key in distinctSellKeysReturn)
                    {
                        if (returnJourneySellKey.Length > 0)
                        {
                            returnJourneySellKey += "^" + key;
                        }
                        else
                        {
                            returnJourneySellKey = key;
                        }
                    }
                }


                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[1];
                if (itinerary.TicketAdvisory.Split('|').Length == 2)
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[2];
                }

                if (!string.IsNullOrEmpty(onwardJourneySellKey))//For Normal One Way Trip
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = onwardJourneySellKey;
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = itinerary.TicketAdvisory.Split('|')[0];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";
                }

                if (itinerary.TicketAdvisory.Split('|').Length == 1 && !string.IsNullOrEmpty(returnJourneySellKey)) //for one-one return
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = returnJourneySellKey;
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = itinerary.TicketAdvisory.Split('|')[0];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";
                }
                if (itinerary.TicketAdvisory.Split('|').Length == 2)//Round trip
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1] = new SellKeyList();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].JourneySellKey = returnJourneySellKey;
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].FareSellKey = itinerary.TicketAdvisory.Split('|')[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].StandbyPriorityCode = "";
                }
                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < searchResult.FareBreakdown.Length; i++)
                {
                    if (searchResult.FareBreakdown[i] != null && searchResult.FareBreakdown[i].PassengerType != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        for (int h = 0; h < searchResult.FareBreakdown[i].PassengerCount; h++)
                        {
                            switch (searchResult.FareBreakdown[i].PassengerType)
                            {
                                case PassengerType.Adult:
                                    priceType.PaxType = "ADT";
                                    break;
                                case PassengerType.Child:
                                    priceType.PaxType = "CHD";
                                    break;

                            }
                            paxPriceTypes.Add(priceType);
                        }
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                if (!string.IsNullOrEmpty(promoCode))//Promo Code Implementation for corporate fare
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale = new TypeOfSale();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes = new string[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes[0] = "F";//Corporate Fare
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.PromotionCode = promoCode;
                }
                request.SellRequestData = requestData;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoSellRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch { }

                SellResponse sellResponse_v_4_5 = bookingAPI.Sell(request);
                responseData = sellResponse_v_4_5.BookingUpdateResponseData;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoSellResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellResponse_v_4_5);
                    sw.Close();
                }
                catch { }

                //Added by lokesh on 20/12/2018
                //To Avoid Duplicate Bookings
                //Checking the segment count with the SSR response segment count
                //If that doesn't match we will call the LogOut method.
                //This Validation Does not allow the additional bookings that are already there in the signature.
                if (responseData != null && responseData.Success != null && responseData.Success.PNRAmount != null)
                {
                    int segCount = distinctSellKeysOnward.Count();
                    if (distinctSellKeysReturn.Count() > 0)
                    {
                        segCount = segCount + distinctSellKeysReturn.Count();
                    }
                    if (segCount != Convert.ToInt32(responseData.Success.PNRAmount.SegmentCount))
                    {
                        Logout(signature);
                        throw new Exception("(Indigo)Segment Count Mismatch ! Please Search Again..");
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to Book. Reason : " + ex.ToString(), "");
                throw ex;
            }

            return responseData;
        }
        /// <summary>
        /// Authenticates and creates a session for the specified user
        /// </summary>
        /// <param name="eventNumber"></param>
        /// <returns></returns>        
        public LogonResponse CreateSession(object eventNumber)
        {
            try
            {
                //SearchRequest request = searchRequests[Convert.ToInt32(eventNumber)];
                LogonRequest request = new LogonRequest();
                request.logonRequestData = new LogonRequestData();
                request.logonRequestData.AgentName = agentId;
                request.logonRequestData.DomainCode = agentDomain;
                request.logonRequestData.LocationCode = "";
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.Password = password;
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.TerminalInfo = "";
                request.ContractVersion = contractVersion;
                request.EnableExceptionStackTrace = false;

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(LogonRequest));
                    string filePath = xmlLogPath + Convert.ToString(eventNumber) + "_" + bookingSourceFlag + "_" + sessionId + "_" + "IndigoLogonRequest" + "_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo)Failed to Serialize LogonRequest. Reason : " + ex.ToString(), "");
                }

                logonResponse = sessionAPI.Logon(request);
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(LogonResponse));
                    string filePath = xmlLogPath + Convert.ToString(eventNumber) + "_" + bookingSourceFlag + "_" + sessionId + "_" + "IndigoLogonResponse_" + "_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, logonResponse);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo)Failed to Serialize LogonResponse. Reason : " + ex.ToString(), "");
                }

            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo) Failed to Login . Reason : " + ex.ToString(), "");
                logonResponse = null;
                 
            }
            return logonResponse;

        }
        //int eventNumber = 0;
        /// <summary>
        /// Indigo One-One Search 
        /// </summary>
        /// <param name="eventNumber"></param>
        public void SearchRoutingReturn(object eventNumber)
        {
            try
            {
                SearchRequest request = searchRequests[Convert.ToInt32(eventNumber)];
                LogonResponse logonResponse = !oneWayCombinationSearch ? CreateSession(eventNumber) : CreateSession();
                //eventNumber++;
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {
                    try
                    {
                        #region One Way Search Results
                        TripAvailabilityResponse tripAvailResponse = null;
                        #region GetAvailabilityRequest Object
                        //S-1:Create object for GetAvailabilityRequest
                        GetAvailabilityRequest availRequest = new GetAvailabilityRequest();
                        GetAvailabilityResponse availResponse = new GetAvailabilityResponse();
                        request.SearchBySegments = true;
                        //Assign Signature generated from Session
                        availRequest.Signature = logonResponse.Signature;
                        availRequest.ContractVersion = contractVersion;
                        //Added by Lokesh on 11-June-2018
                        //New skies Version 4.5
                        //Need to send EnableExceptionStackTrace = false(by default) in the request.
                        availRequest.EnableExceptionStackTrace = false;
                        availRequest.TripAvailabilityRequest = new TripAvailabilityRequest();

                        availRequest.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                        //Assign all Availability details to get flight details
                        for (int i = 0; i < availRequest.TripAvailabilityRequest.AvailabilityRequests.Length; i++)
                        {
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i] = new AvailabilityRequest();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].ArrivalStation = request.Segments[i].Destination;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].BeginDate = request.Segments[i].PreferredDepartureTime;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FlightType = Indigo.BookingMgr.FlightType.All;
                            if (!string.IsNullOrEmpty(promoCode))
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PromotionCode = promoCode;
                            }
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CarrierCode = "6E";
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].CurrencyCode = currencyCode;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DepartureStation = request.Segments[i].Origin;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].DisplayCurrencyCode = currencyCode;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].Dow = DOW.Daily;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].EndDate = request.Segments[i].PreferredArrivalTime;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeTaxesAndFees = true;

                            //For connecting flights 
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].MaximumConnectingFlights = 10;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareClassControl = FareClassControl.CompressByProductClass;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].AvailabilityType = Indigo.BookingMgr.AvailabilityType.Default;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].SSRCollectionsMode = SSRCollectionsMode.None;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].InboundOutbound = InboundOutbound.None;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].IncludeAllotments = false;

                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].LoyaltyFilter = LoyaltyFilter.MonetaryOnly;
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareRuleFilter = FareRuleFilter.Default;

                            //Kindly enter value as ‘R’,  ‘Z’ and ‘M’ in tag namely FareTypes in GetAvailability API call and set the value as ‘CompressByProductClass for the tag namely FareClassControl  and set ProductClasses tag to Null or enter all the available ProductClasses as mentioned below to get the lowest fare among the all ProductClasses.
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[3];
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "R"; //Retail Fare
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[1] = "Z"; //Lite Fare
                            
                           //6E FARE TYPES : Retail / Return Trip,Sale Fare,Family Fare,Special Round Trip,Flexi Fare,Lite Fare
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[2] = "F"; //SME FARE



                            //FOR CORPORATE NEED TO SEND ONLY THE CORPORATE FARE TYPE
                            if (!string.IsNullOrEmpty(bookingSourceFlag) && bookingSourceFlag == "6ECORP")
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes = new string[1];
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].FareTypes[0] = "F"; //CORPORATE FARE
                            }

                            int paxTypeCount = 1;
                            if (request.ChildCount > 0)
                            {
                                paxTypeCount++;
                            }
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxCount = Convert.ToInt16(request.AdultCount + request.ChildCount);
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes = new PaxPriceType[paxTypeCount];


                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[0] = new PaxPriceType();
                            availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[0].PaxType = "ADT";//Adult Passenger

                            if (request.ChildCount > 0)
                            {
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[1] = new PaxPriceType();
                                availRequest.TripAvailabilityRequest.AvailabilityRequests[i].PaxPriceTypes[1].PaxType = "CHD"; //Child Passenger
                            }

                        }
                        #endregion

                        #region Indigo One Way SearchRequest and Search Response Logs
                        try
                        {


                            XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityRequest));
                            string filePath = xmlLogPath + Convert.ToString(eventNumber) + "_" + bookingSourceFlag + "_" + sessionId + "_" + "IndigoSearchRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, availRequest);
                            sw.Close();

                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Save Search Request. Reason : " + ex.ToString(), "");
                        }

                        //S-2:GetAvailability: -Gets flight availability and fare information.
                        //Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo One Way search request sent: " + DateTime.Now, "");
                        GetAvailabilityResponse availResponse_v_4_5 = bookingAPI.GetAvailability(availRequest);
                        tripAvailResponse = availResponse_v_4_5.GetTripAvailabilityResponse;



                        //Audit.Add(EventType.Login, Severity.Normal, appUserId, "Indigo One Way search response received: " + DateTime.Now, "");


                        try
                        {
                            //The default nullable types are not serialized so during xml serialization the default nullable types should be passed for serializing the object.
                            Type[] nullableTypes = new Type[] { typeof(Indigo.BookingMgr.AvailableFare) };
                            XmlSerializer ser = new XmlSerializer(typeof(GetAvailabilityResponse), nullableTypes);
                            string filePath = xmlLogPath + Convert.ToString(eventNumber) + "_" + bookingSourceFlag + "_" + sessionId + "_" + "IndigoSearchResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmssfff") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, availResponse_v_4_5);
                            sw.Close();
                        }
                        catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize Search Response. Reason : " + ex.ToString(), ""); }

                        #endregion

                        if (tripAvailResponse != null)
                        {
                            List<Journey> OnwardJourneys = new List<Journey>();

                            /*(tripAvailResponse.Schedules) is a Jagged Array(Array of Arrays) */

                            if (tripAvailResponse.Schedules.Length > 0)
                            {
                                /*S-1: Read the total number of schedules elements*/
                                for (int i = 0; i < tripAvailResponse.Schedules.Length; i++)
                                {
                                    /*Root element Schedules*/
                                    for (int j = 0; j < tripAvailResponse.Schedules[0].Length; j++)
                                    {
                                        if (tripAvailResponse.Schedules[0][j].Journeys.Length > 0)
                                        {
                                            OnwardJourneys.AddRange(tripAvailResponse.Schedules[0][j].Journeys);
                                        }
                                    }
                                }
                            }


                            if (OnwardJourneys.Count > 0)
                            {
                                int fareBreakDownCount = 0;

                                if (request.AdultCount > 0)
                                {
                                    fareBreakDownCount = 1;
                                }
                                if (request.ChildCount > 0)
                                {
                                    fareBreakDownCount++;
                                }
                                if (request.InfantCount > 0)
                                {
                                    fareBreakDownCount++;
                                }
                                oneWayFareBreakDowncount = fareBreakDownCount;
                                for (int i = 0; i < OnwardJourneys.Count; i++)
                                {
                                    if (OnwardJourneys[i].Segments.Length > 1)
                                    {
                                        GetConnectingFlightsResultsForOneWay(OnwardJourneys[i], fareBreakDownCount, logonResponse.Signature);
                                    }
                                    else
                                    {

                                        for (int j = 0; j < OnwardJourneys[i].Segments[0].Fares.Length; j++)
                                        {
                                            GenerateFlightResultForOneWayDirectFlights(OnwardJourneys[i], OnwardJourneys[i].Segments[0].Fares[j], logonResponse.Signature);
                                        }

                                    }
                                }
                                if (request.SearchBySegments || oneWayResultList != null && oneWayResultList.Count > 0)
                                {
                                    var returnFlightResults = oneWayResultList.FindAll(i => i.Flights[0].Any(f => f.Origin.AirportCode == request.Segments[0].Origin && request.Type == SearchType.Return));//.Where(j => j.Origin.AirportCode == request.Segments[0].Destination).ToList());
                                    if (returnFlightResults != null)
                                    {
                                        returnFlightResults.ToList().ForEach(f => f.Flights[0].ToList().ForEach(s => s.Group = 1));

                                    }
                                    if (request.Type == SearchType.Return && CombinedSearchResults.Count > 0 && returnFlightResults.Count() == 0)
                                    {
                                        oneWayResultList.Clear();
                                        CombinedSearchResults.Clear();
                                    }

                                }
                                if (request.Type == SearchType.OneWay && oneWayResultList.Count == 0)
                                {
                                    oneWayResultList.Clear();
                                    CombinedSearchResults.Clear();
                                }
                                if (oneWayResultList.Count > 0)
                                {
                                    oneWayResultList.RemoveAll(x => x.TotalFare == 0);
                                    CombinedSearchResults.AddRange(oneWayResultList);
                                    oneWayResultList.Clear();
                                }
                                if (!oneWayCombinationSearch)//If one way then no need to reset the event
                                {
                                    eventFlag[(int)eventNumber].Set();
                                }
                            }
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {

                        Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to get results for One-way search. Reason : " + ex.ToString(), "");

                    }

                }
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to get results for One-way search. Reason : " + ex.ToString(), "");
            }
            finally
            {
                if (!oneWayCombinationSearch && eventFlag.Length > 0)//If one way then no need to reset the event
                {
                    if (eventFlag[(int)eventNumber] != null)
                    {
                        eventFlag[(int)eventNumber].Set();
                    }
                }
            }
        }

        /// <summary>
        /// Gets the special round trip fare combination search results
        /// </summary>
        public void GetRTCombinationResults()
        {
            if (OnwardJourneys.Count > 0)
            {
                int fareBreakDownCount = 0;

                if (request.AdultCount > 0)
                {
                    fareBreakDownCount = 1;
                }
                if (request.ChildCount > 0)
                {
                    fareBreakDownCount++;
                }
                if (request.InfantCount > 0)
                {
                    fareBreakDownCount++;
                }
                oneWayFareBreakDowncount = fareBreakDownCount;
                for (int i = 0; i < OnwardJourneys.Count; i++)
                {
                    if (OnwardJourneys[i].Segments.Length > 1)
                    {
                        GetConnectingFlightsResultsForOneWay(OnwardJourneys[i], fareBreakDownCount, logonResponse.Signature);
                    }
                    else
                    {

                        for (int j = 0; j < OnwardJourneys[i].Segments[0].Fares.Length; j++)
                        {
                            GenerateFlightResultForOneWayDirectFlights(OnwardJourneys[i], OnwardJourneys[i].Segments[0].Fares[j], logonResponse.Signature);
                        }

                    }
                }
                if (request.SearchBySegments || oneWayResultList != null && oneWayResultList.Count > 0)
                {
                    var returnFlightResults = oneWayResultList.Select(i => i.Flights[0].ToList());
                    if (returnFlightResults != null)
                    {
                        foreach (var item in returnFlightResults)
                        {
                            if (item[0].Origin.AirportCode == request.Segments[0].Destination && request.Type == SearchType.Return)
                            {
                                item[0].Group = 1;
                            }
                        }
                    }
                    //If there are no onward or return results in One-One Search
                    //Then do not bind the result set
                    //Clear the lists which holds the result objects
                    if (returnFlightResults.Count() == 0 || oneWayResultList.Count == 0)
                    {
                        oneWayResultList.Clear();
                        CombinedSearchResults.Clear();
                    }
                }
                if (oneWayResultList.Count > 0)
                {
                    oneWayResultList.RemoveAll(x => x.TotalFare == 0);
                    CombinedSearchResults.AddRange(oneWayResultList);
                    oneWayResultList.Clear();
                }
            }
        }


        /// <summary>
        /// Gets the ItineraryPrice for Indigo Special Round Trip flights.
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="request"></param>
        public void Get6ERoundTripPrice(ref SearchResult resONW, ref SearchResult resRET, SearchRequest request)
        {
            try
            {
                //Onward Result Fare BreakDown
                if (resONW.FareBreakdown[0] == null)//Adult
                {
                    resONW.FareBreakdown[0] = new Fare();
                    resONW.FareBreakdown[0].PassengerCount = request.AdultCount;
                    resONW.FareBreakdown[0].PassengerType = PassengerType.Adult;
                }
                if (request.ChildCount > 0)//Child
                {
                    if (resONW.FareBreakdown[1] == null)
                    {
                        resONW.FareBreakdown[1] = new Fare();
                        resONW.FareBreakdown[1].PassengerCount = request.ChildCount;
                        resONW.FareBreakdown[1].PassengerType = PassengerType.Child;
                    }
                }

                //Return Result Fare BreakDown
                if (resRET.FareBreakdown[0] == null)//Adult
                {
                    resRET.FareBreakdown[0] = new Fare();
                    resRET.FareBreakdown[0].PassengerCount = request.AdultCount;
                    resRET.FareBreakdown[0].PassengerType = PassengerType.Adult;
                }
                if (request.ChildCount > 0)//Child
                {
                    if (resRET.FareBreakdown[1] == null)
                    {
                        resRET.FareBreakdown[1] = new Fare();
                        resRET.FareBreakdown[1].PassengerCount = request.ChildCount;
                        resRET.FareBreakdown[1].PassengerType = PassengerType.Child;
                    }
                }
                //Get the log file names for different flights
                string logFileName = GetLogFileName(resONW);
                //logFileName += GetLogFileName(resRET);
                PriceItineraryResponse itineraryResponse = GetRoundTripFare(request, resONW, resRET, logFileName);
                if (itineraryResponse != null && !string.IsNullOrEmpty(itineraryResponse.Booking.CurrencyCode))
                {
                    rateOfExchange = exchangeRates[itineraryResponse.Booking.CurrencyCode];
                    Compute6ERoundTripFare(ref resONW, itineraryResponse, request, 0);
                    Compute6ERoundTripFare(ref resRET, itineraryResponse, request, 1);

                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                throw new BookingEngineException("(Indigo) No fare available");
            }
        }

        /// <summary>
        /// Calculates the fare break down 
        /// </summary>
        /// <param name="resultObj"></param>
        /// <param name="FarePaxtypes"></param>
        /// <param name="rateOfExchange"></param>
        /// <param name="itineraryResponse"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public void Compute6ERoundTripFare(ref SearchResult resultObj, PriceItineraryResponse itineraryResponse, SearchRequest request, int type)
        {
            try
            {
                //S-1:Booking
                //S-2:Booking -->Journeys[]
                //S-3:Booking -->Journey -->Segments[]
                //S-4:Booking -->Journey -->Segment -->Fares[]
                //S-5:Booking -->Journey -->Segment -->Fare -->PaxFares[]
                //S-6:Booking -->Journey-->Segment-->Fare-->PaxFare --><PaxType>ADT</PaxType> -->BookingServiceCharge

                List<Indigo.BookingMgr.Journey> vJourneys = new List<Indigo.BookingMgr.Journey>();
                List<Indigo.BookingMgr.Segment> vSegments = new List<Indigo.BookingMgr.Segment>();
                List<Indigo.BookingMgr.Fare> vFares = new List<Indigo.BookingMgr.Fare>();
                List<Indigo.BookingMgr.PaxFare> vTotalPaxFares = new List<Indigo.BookingMgr.PaxFare>();
                List<Indigo.BookingMgr.PaxFare> vADTPaxFares = new List<Indigo.BookingMgr.PaxFare>();
                List<Indigo.BookingMgr.PaxFare> vCHDPaxFares = new List<Indigo.BookingMgr.PaxFare>();



                paxTypeTaxBreakUp.Clear();
                adtPaxTaxBreakUp.Clear();
                chdPaxTaxBreakUp.Clear();
                inftPaxTaxBreakUp.Clear();
                vJourneys.Clear();
                vSegments.Clear();
                vFares.Clear();
                vTotalPaxFares.Clear();
                vADTPaxFares.Clear();
                vCHDPaxFares.Clear();


                //S-1:Journeys
                if (itineraryResponse != null && itineraryResponse.Booking != null && itineraryResponse.Booking.Journeys != null && itineraryResponse.Booking.Journeys.Length > 0)
                {
                    vJourneys.AddRange(itineraryResponse.Booking.Journeys);
                }
                //S-2:Segments
                if (vJourneys != null && vJourneys.Count > 0)
                {
                    if (type == 0)//Onward
                    {
                        vSegments.AddRange(vJourneys[0].Segments.ToList());
                    }
                    else if (type == 1 && vJourneys.Count > 1)//Return
                    {
                        vSegments.AddRange(vJourneys[1].Segments.ToList());
                    }
                }
                //S-3:Fares
                if (vSegments != null && vSegments.Count > 0)
                {
                    for (int a = 0; a < vSegments.Count; a++)
                    {
                        vFares.AddRange(vSegments[a].Fares.ToList());
                    }
                }
                //S-4:Total Pax Fares
                if (vFares != null && vFares.Count > 0)
                {
                    for (int a = 0; a < vFares.Count; a++)
                    {
                        vTotalPaxFares.AddRange(vFares[a].PaxFares.ToList());
                    }
                }
                //S-5: ADT PaxFare and CHD PaxFare
                if (vTotalPaxFares != null && vTotalPaxFares.Count > 0)
                {
                    vADTPaxFares = vTotalPaxFares.Where(a => a.PaxType == "ADT").Select(b => b).ToList();
                    if (vADTPaxFares != null && vADTPaxFares.Count > 0)
                    {
                        foreach (PaxFare adultPaxFare in vADTPaxFares)
                        {
                            ComputeAdultPaxFare(ref resultObj, adultPaxFare);
                        }
                    }
                    if (request.ChildCount > 0)
                    {
                        vCHDPaxFares = vTotalPaxFares.Where(a => a.PaxType == "CHD").Select(b => b).ToList();
                        if (vCHDPaxFares != null && vCHDPaxFares.Count > 0)
                        {
                            foreach (PaxFare childPaxFare in vCHDPaxFares)
                            {
                                ComputeChildPaxFare(ref resultObj, childPaxFare);
                            }
                        }
                    }
                }

                //Baggage Calculation Added default baggage for VIA flights to BaggageIncludedInFare .
                try
                {
                    if(itineraryResponse != null && itineraryResponse.Booking != null && itineraryResponse.Booking.Journeys != null && itineraryResponse.Booking.Journeys.Length > 0)
                    {
                        if (itineraryResponse.Booking.Journeys[type] != null && itineraryResponse.Booking.Journeys[type].Segments != null && itineraryResponse.Booking.Journeys[type].Segments.Length > 0 && itineraryResponse.Booking.Journeys[type].Segments[0] != null && itineraryResponse.Booking.Journeys[type].Segments[0].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[type].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                        {
                            resultObj.BaggageIncludedInFare = itineraryResponse.Booking.Journeys[type].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                        }
                        for (int k = 1; k < resultObj.Flights[0].Length; k++)
                        {
                            if (resultObj.Flights[0][k].Status.ToUpper() != "VIA")
                            {
                                if (itineraryResponse.Booking.Journeys[type] != null && itineraryResponse.Booking.Journeys[type].Segments != null && itineraryResponse.Booking.Journeys[type].Segments.Length > 0 && itineraryResponse.Booking.Journeys[type].Segments[k] != null && itineraryResponse.Booking.Journeys[type].Segments[k].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[type].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                                {
                                    resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[type].Segments[k].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                }
                            }
                            else  //For VIA Flights adding default baggage segment wise
                            {
                                if (itineraryResponse.Booking.Journeys[type] != null && itineraryResponse.Booking.Journeys[type].Segments != null && itineraryResponse.Booking.Journeys[type].Segments.Length > 0 && itineraryResponse.Booking.Journeys[type].Segments[0] != null && itineraryResponse.Booking.Journeys[type].Segments[0].PaxSegments != null && !string.IsNullOrEmpty(itineraryResponse.Booking.Journeys[type].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString()))
                                {
                                    resultObj.BaggageIncludedInFare += "," + itineraryResponse.Booking.Journeys[type].Segments[0].PaxSegments[0].BaggageAllowanceWeight.ToString();
                                }

                            }
                        }
                    }
                }
                catch
                {
                    resultObj.BaggageIncludedInFare = string.Empty;
                }


                if (adtPaxTaxBreakUp.Count > 0)
                {
                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                }
                if (chdPaxTaxBreakUp.Count > 0)
                {
                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                }
                if (inftPaxTaxBreakUp.Count > 0)
                {
                    paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                }
                if (paxTypeTaxBreakUp.Count > 0)
                {
                    resultObj.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                }
                resultObj.Price.SupplierCurrency = currencyCode;
                resultObj.BaseFare = (double)resultObj.Price.PublishedFare;
                resultObj.Tax = (double)resultObj.Price.Tax;
                resultObj.TotalFare = (double)Math.Round((resultObj.BaseFare + resultObj.Tax), agentDecimalValue);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Calculate fare break down. Reason : " + ex.ToString(), "");
            }

        }

        //compute Adult Fare from Availibility Response
        private void ComputeAdultPaxFare(ref SearchResult resultObj, PaxFare adultPaxfare)
        {
            //Calculates the Adult Price
            if (resultObj.FareBreakdown[0] != null)
            {
                foreach (BookingServiceCharge charge in adultPaxfare.ServiceCharges)
                {
                    if (charge != null && charge.ChargeType == Indigo.BookingMgr.ChargeType.FarePrice)//Base Fare
                    {
                        double baseFareAdult = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                        resultObj.FareBreakdown[0].BaseFare += baseFareAdult;
                        resultObj.FareBreakdown[0].TotalFare += baseFareAdult;
                        resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                        resultObj.Price.PublishedFare += (decimal)baseFareAdult;
                        resultObj.Price.SupplierPrice += charge.Amount;
                        
                    }
                    else if (charge != null && charge.ChargeType != Indigo.BookingMgr.ChargeType.FarePrice && charge.ChargeType == Indigo.BookingMgr.ChargeType.PromotionDiscount)//Discount 
                    {
                        //Deduct discount from the base fare
                        double discountAdult = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                        resultObj.FareBreakdown[0].BaseFare = resultObj.FareBreakdown[0].BaseFare - discountAdult;
                        resultObj.FareBreakdown[0].TotalFare = resultObj.FareBreakdown[0].TotalFare - discountAdult;
                        resultObj.FareBreakdown[0].SupplierFare = resultObj.FareBreakdown[0].SupplierFare - (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                        resultObj.Price.PublishedFare = (decimal)(resultObj.Price.PublishedFare - Convert.ToDecimal(discountAdult));
                        resultObj.Price.SupplierPrice = resultObj.Price.SupplierPrice - charge.Amount;
                    }
                    else if (charge != null && charge.ChargeType != Indigo.BookingMgr.ChargeType.FarePrice && charge.ChargeType != Indigo.BookingMgr.ChargeType.PromotionDiscount && charge.ChargeType != Indigo.BookingMgr.ChargeType.IncludedTax && charge.ChargeType != Indigo.BookingMgr.ChargeType.Discount)//All the tax componsnets calculation
                    {
                        double tax = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                        resultObj.FareBreakdown[0].TotalFare += tax;
                        resultObj.FareBreakdown[0].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[0].PassengerCount);
                        resultObj.Price.Tax += (decimal)tax;
                        if (charge.ChargeCode.ToString().Contains("GST"))
                        {
                            resultObj.Price.K3Tax += ((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[0].PassengerCount);
                        }
                        resultObj.Price.SupplierPrice += charge.Amount;
                        adtPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                    }
                }
            }

        }

        //compute Child Fare from Availibility Response
        private void ComputeChildPaxFare(ref SearchResult resultObj, PaxFare childPaxfare)
        {
            //Calculates the Child Price
            if (resultObj.FareBreakdown[1] != null)
            {
                foreach (BookingServiceCharge charge in childPaxfare.ServiceCharges)
                {
                    if (charge != null && charge.ChargeType == Indigo.BookingMgr.ChargeType.FarePrice)//Base Fare
                    {
                        double baseFareChd = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.FareBreakdown[1].BaseFare += baseFareChd;
                        resultObj.FareBreakdown[1].TotalFare += baseFareChd;
                        resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.Price.PublishedFare += (decimal)baseFareChd;
                        resultObj.Price.SupplierPrice += charge.Amount;

                    }
                    else if (charge != null && charge.ChargeType != Indigo.BookingMgr.ChargeType.FarePrice && charge.ChargeType == Indigo.BookingMgr.ChargeType.PromotionDiscount)//Discount 
                    {
                        //Deduct discount from the base fare
                        double discountChild = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.FareBreakdown[1].BaseFare = resultObj.FareBreakdown[1].BaseFare - discountChild;
                        resultObj.FareBreakdown[1].TotalFare = resultObj.FareBreakdown[1].TotalFare - discountChild;
                        resultObj.FareBreakdown[1].SupplierFare = resultObj.FareBreakdown[1].SupplierFare - (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.Price.PublishedFare = (decimal)(resultObj.Price.PublishedFare - Convert.ToDecimal(discountChild));
                        resultObj.Price.SupplierPrice = resultObj.Price.SupplierPrice - charge.Amount;
                    }
                    else if (charge != null && charge.ChargeType != Indigo.BookingMgr.ChargeType.FarePrice && charge.ChargeType != Indigo.BookingMgr.ChargeType.PromotionDiscount && charge.ChargeType != Indigo.BookingMgr.ChargeType.IncludedTax && charge.ChargeType != Indigo.BookingMgr.ChargeType.Discount)//All the tax componsnets calculation
                    {
                        double tax = (double)((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.FareBreakdown[1].TotalFare += tax;
                        resultObj.FareBreakdown[1].SupplierFare += (double)(charge.Amount * resultObj.FareBreakdown[1].PassengerCount);
                        resultObj.Price.Tax += (decimal)tax;
                        if (charge.ChargeCode.ToString().Contains("GST"))
                        {
                            resultObj.Price.K3Tax += ((charge.Amount * rateOfExchange) * resultObj.FareBreakdown[1].PassengerCount);
                        }
                        resultObj.Price.SupplierPrice += charge.Amount;
                        chdPaxTaxBreakUp.Add(new KeyValuePair<string, Decimal>(charge.ChargeCode, Convert.ToDecimal(charge.Amount * rateOfExchange)));

                    }
                }
            }
        }

        //Get the LogFile Name for All flights.
        public string GetLogFileName(SearchResult resultObj)
        {
            string logFileName = string.Empty;
            if (resultObj.Flights != null && resultObj.Flights.Length > 1)//Round Trip
            {
                //Get the log file names for different flights
                //Both are direct flighst
                if (resultObj.Flights[0].Length == 1 && resultObj.Flights[1].Length == 1)
                {
                    logFileName = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_").Replace(" ", "_") + Convert.ToString(resultObj.Flights[0][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    logFileName += "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                }
                //Both may be via or connecting flights
                else if (resultObj.Flights[0].Length > 1 && resultObj.Flights[1].Length > 1)
                {
                    logFileName = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    logFileName += "_" + Convert.ToString(resultObj.Flights[0][resultObj.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    logFileName = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    logFileName += "_" + Convert.ToString(resultObj.Flights[1][resultObj.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                }
                //Onward may be via or connecting flights and return direct
                else if (resultObj.Flights[0].Length > 1 && resultObj.Flights[1].Length == 1)
                {
                    logFileName = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    logFileName += "_" + Convert.ToString(resultObj.Flights[0][resultObj.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    logFileName = "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_").Replace(" ", "_") + Convert.ToString(resultObj.Flights[1][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                }
                //return may be via or connecting flights and onward direct
                else if (resultObj.Flights[0].Length == 1 && resultObj.Flights[1].Length > 1)
                {
                    logFileName = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    logFileName += "_" + resultObj.Flights[1][0].FlightNumber + "_" + resultObj.FareType.Split(',')[1].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[1][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    logFileName += "_" + Convert.ToString(resultObj.Flights[1][resultObj.Flights[1].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                }
            }
            else//Only One Way
            {
                //direct flighst
                if (resultObj.Flights[0].Length == 1)
                {
                    logFileName = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_").Replace(" ", "_") + Convert.ToString(resultObj.Flights[0][0].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                }
                //Both may be via or connecting flights
                else if (resultObj.Flights[0].Length > 1)
                {
                    logFileName = resultObj.Flights[0][0].FlightNumber + "_" + resultObj.FareType.Split(',')[0].Replace(" ", "_") + "_" + Convert.ToString(resultObj.Flights[0][0].DepartureTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                    logFileName += "_" + Convert.ToString(resultObj.Flights[0][resultObj.Flights[0].Length - 1].ArrivalTime).Replace('/', '_').Replace(" ", "_").Replace(':', '_');
                }
            }
            return logFileName;
        }


        /// <summary>
        /// Gets the ItineraryPrice for special round trip fare.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="journeySellKey"></param>
        /// <param name="fareSellKey"></param>
        /// <param name="fareSellKeyRet"></param>
        /// <param name="journeySellKeyRet"></param>
        /// <param name="resultObj"></param>
        /// <param name="onwardFare"></param>
        /// <returns></returns>
        protected PriceItineraryResponse GetRoundTripFare(SearchRequest request, SearchResult resONW, SearchResult resRET, string logFileName)
        {
            LogonResponse loginResponse = null;
            if (request.SearchBySegments) //For Combination Search
            {
                loginResponse = new LogonResponse();
                loginResponse.Signature = resONW.RepriceErrorMessage;
            }
            PriceItineraryResponse piResponse = new PriceItineraryResponse();
            if (loginResponse != null && !string.IsNullOrEmpty(loginResponse.Signature))
            {
                IBookingManager bookingAPI = this.bookingAPI;

                int paxCount = 1;
                if (request.ChildCount > 0)
                {
                    paxCount++;
                }

                PriceItineraryRequest priceItinRequest = new PriceItineraryRequest();
                priceItinRequest.Signature = loginResponse.Signature;
                priceItinRequest.ContractVersion = contractVersion;
                priceItinRequest.EnableExceptionStackTrace = false;
                priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = currencyCode;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (Int16)(request.AdultCount + request.ChildCount + request.InfantCount);
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[paxCount];

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS = new PointOfSale();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = "AG";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.DomainCode = agentDomain;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.OrganizationCode = agentId;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[2];

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].FareSellKey = resONW.FareSellKey.Split('|')[0];
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0].JourneySellKey = resONW.JourneySellKey.Split('|')[0];


                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1] = new SellKeyList();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].FareSellKey = resRET.FareSellKey.Split('|')[0];
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1].JourneySellKey = resRET.JourneySellKey.Split('|')[0];

                if (request.AdultCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxType = "ADT";
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[0].PaxCount = (Int16)(request.AdultCount);
                }
                if (request.ChildCount > 0)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxType = "CHD";
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[1].PaxCount = (Int16)(request.ChildCount);
                }
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                if (!string.IsNullOrEmpty(promoCode))
                {
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes = new string[1];
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes[0] = "F";
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = promoCode;
                }

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(PriceItineraryRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoPriceRequestRoundTrip_" + appUserId.ToString() + "_" + logFileName + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, priceItinRequest);
                    sw.Close();
                }
                catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }

                try
                {
                    piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);

                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(Booking));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoPriceResponseRoundTrip_" + appUserId.ToString() + "_" + logFileName + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, piResponse.Booking);
                        sw.Close();
                    }
                    catch (Exception ex) { Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to Serialize GetItineraryPrice Response. Reason : " + ex.ToString(), ""); }
                }
                catch (Exception ex)
                {
                   
                    Audit.Add(EventType.Search, Severity.High, appUserId, "(Indigo)Failed to GetItineraryPrice. Reason : " + ex.ToString(), "");
                    throw new BookingEngineException("(Indigo) No fare available");
                }
            }
            else
            {
                throw new Exception("(Indigo)Failed to get the LogonResponse");
            }

            return piResponse;
        }


        /// <summary>
        /// Gets the Available SSR for Special Round trip In combination Search 
        /// </summary>
        /// <param name="onwResult"></param>
        /// <param name="retResult"></param>
        /// <returns></returns>
        public DataTable GetSpecialRoundTripSSR(SearchResult onwResult, SearchResult retResult)
        {
            DataTable dtSourceBaggage = null;
            LogonResponse logonResponse = null;
            if (!string.IsNullOrEmpty(onwResult.RepriceErrorMessage))
            {
                logonResponse = new LogonResponse();
                logonResponse.Signature = onwResult.RepriceErrorMessage;
            }
            
            //This method will releases the itinerary.
            //Clears the previous objects from the session if any objects are there.
            //This method will avoid duplicate bookings.
            if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
            {
                ClearRequest clearRequest = new ClearRequest();
                clearRequest.ContractVersion = contractVersion;
                clearRequest.EnableExceptionStackTrace = false;
                clearRequest.Signature = logonResponse.Signature;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(ClearRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_ClearRequest_" + appUserId.ToString() + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, clearRequest);
                    sw.Close();
                }
                catch { }
                ClearResponse clearResponse = bookingAPI.Clear(clearRequest);
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(ClearResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_ClearResponse_" + appUserId.ToString() + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, clearResponse);
                    sw.Close();
                }
                catch { }
            }

            //Marketing CodeShare Schedule Flights Count
            //Kindly be informed that as per latest guidelines we are not allowed to sell excess baggage on MCS flights
            List<FlightInfo> flights = new List<FlightInfo>(onwResult.Flights[0]);
            if (retResult.Flights[0] != null)
            {
                flights.AddRange(retResult.Flights[0]);
            }
            int codeShareFlightCount = flights.FindAll(a => !a.OperatingCarrier.Trim().Equals("6E")).Count();

            try
            {

                //SSR : AdditionalBaggage and Meals [For Both Onward and Return Journeys]
                //Note:Infant is also an SSR.
                //But we will get the infant price from the price itinerary response.

                //Steps to get the available SSR Without GST input.
                //1.Login
                //2.Sell [Holds the complete itinerary]
                //3.GetSSRAvailabilityForBooking.

                //Steps to get the available SSR With GST input.
                //1.Login
                //2.UpdateContactRequest 
                //3.Sell[Holds the complete itinerary]
                //4.GetSSRAvailabilityForBooking
                //5.GetBookingFromState


                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature))
                {

                    //Note:Verify any GST input available then execute UpdateContactsRequest first.
                    //Step-1.1: If any GST input then UpdateContactGSTDetails method
                    UpdateContactsResponse updateGSTResponse = null;
                    if (!string.IsNullOrEmpty(gstCompanyName) && (gstCompanyName.Length > 0) &&
                       !string.IsNullOrEmpty(gstNumber) && (gstNumber.Length > 0) &&
                        !string.IsNullOrEmpty(gstOfficialEmail) && (gstOfficialEmail.Length > 0)
                       && !string.IsNullOrEmpty(logonResponse.Signature))
                    {

                        updateGSTResponse = UpdateContactGSTDetails(logonResponse.Signature);

                    }

                    //Step2:Sell
                    BookingUpdateResponseData responseData = GetSellResponse(logonResponse.Signature, onwResult, retResult);


                    if (responseData != null)
                    {
                        //Step-3: GetAvailableSSR

                        GetSSRAvailabilityForBookingResponse ssrResponse = new GetSSRAvailabilityForBookingResponse();
                        GetSSRAvailabilityForBookingRequest ssrRequest = new GetSSRAvailabilityForBookingRequest();
                        ssrRequest.ContractVersion = contractVersion;
                        ssrRequest.EnableExceptionStackTrace = false;
                        ssrRequest.Signature = logonResponse.Signature;

                        SSRAvailabilityForBookingRequest availRequest = new SSRAvailabilityForBookingRequest();
                        availRequest.InventoryControlled = true;
                        availRequest.NonInventoryControlled = true;
                        availRequest.SeatDependent = true;
                        availRequest.NonSeatDependent = true;
                        availRequest.CurrencyCode = currencyCode;

                        List<LegKey> legKeysList = new List<LegKey>();
                        for (int i = 0; i < onwResult.Flights.Length; i++)
                        {
                            for (int j = 0; j < onwResult.Flights[i].Length; j++)
                            {
                                LegKey legKey = new LegKey();
                                legKey.CarrierCode = "6E";
                                legKey.FlightNumber = onwResult.Flights[i][j].FlightNumber;
                                legKey.DepartureDate = onwResult.Flights[i][j].DepartureTime;
                                legKey.DepartureStation = onwResult.Flights[i][j].Origin.AirportCode;
                                legKey.ArrivalStation = onwResult.Flights[i][j].Destination.AirportCode;
                                legKeysList.Add(legKey);
                            }
                        }

                        for (int i = 0; i < retResult.Flights.Length; i++)
                        {
                            for (int j = 0; j < retResult.Flights[i].Length; j++)
                            {
                                LegKey legKey = new LegKey();
                                legKey.CarrierCode = "6E";
                                legKey.FlightNumber = retResult.Flights[i][j].FlightNumber;
                                legKey.DepartureDate = retResult.Flights[i][j].DepartureTime;
                                legKey.DepartureStation = retResult.Flights[i][j].Origin.AirportCode;
                                legKey.ArrivalStation = retResult.Flights[i][j].Destination.AirportCode;
                                legKeysList.Add(legKey);
                            }
                        }
                        if (legKeysList != null && legKeysList.Count > 0)
                        {
                            availRequest.SegmentKeyList = legKeysList.ToArray();
                        }
                        ssrRequest.SSRAvailabilityForBookingRequest = availRequest;
                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(GetSSRAvailabilityForBookingRequest));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_PaxSSR_Request_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, ssrRequest);
                            sw.Close();
                        }
                        catch { }
                        ssrResponse = bookingAPI.GetSSRAvailabilityForBooking(ssrRequest);
                        try
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(GetSSRAvailabilityForBookingResponse));
                            string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_PaxSSR_Response_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                            StreamWriter sw = new StreamWriter(filePath);
                            ser.Serialize(sw, ssrResponse);
                            sw.Close();
                        }
                        catch { }
                        if (ssrResponse != null && ssrResponse.SSRAvailabilityForBookingResponse != null && ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList != null && ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList.Length > 0)
                        {
                            dtSourceBaggage = new DataTable();
                            if (dtSourceBaggage.Columns.Count == 0)
                            {
                                dtSourceBaggage.Columns.Add("Code", typeof(string));
                                dtSourceBaggage.Columns.Add("Price", typeof(decimal));
                                dtSourceBaggage.Columns.Add("Group", typeof(int));
                                dtSourceBaggage.Columns.Add("Currency", typeof(string));
                                dtSourceBaggage.Columns.Add("Description", typeof(string));
                                dtSourceBaggage.Columns.Add("QtyAvailable", typeof(int));
                            }
                            List<AvailablePaxSSR> availablePaxSSRs_OnwardBag = new List<AvailablePaxSSR>();//OnwardBaggage
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnBag = new List<AvailablePaxSSR>();//ReturnBaggage

                            List<AvailablePaxSSR> availablePaxSSRs_OnwardMeal = new List<AvailablePaxSSR>();//OnwardMeal
                            List<AvailablePaxSSR> availablePaxSSRs_ReturnMeal = new List<AvailablePaxSSR>();//ReturnMeal

                            //Onward Result SSR
                            for (int i = 0; i < onwResult.Flights.Length; i++)
                            {
                                for (int j = 0; j < onwResult.Flights[i].Length; j++)
                                {
                                    string DepartureStation = onwResult.Flights[i][j].Origin.AirportCode;
                                    string ArrivalStation = onwResult.Flights[i][j].Destination.AirportCode;

                                    for (int p = 0; p < ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList.Length; p++)
                                    {
                                        SSRSegment sSRSegment = ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList[p];
                                        if (sSRSegment.AvailablePaxSSRList != null && sSRSegment.AvailablePaxSSRList.Length > 0 && sSRSegment.LegKey.DepartureStation == DepartureStation && sSRSegment.LegKey.ArrivalStation == ArrivalStation)
                                        {
                                            availablePaxSSRs_OnwardBag.AddRange(sSRSegment.AvailablePaxSSRList.ToList());
                                            availablePaxSSRs_OnwardMeal.AddRange(sSRSegment.AvailablePaxSSRList.ToList());

                                        }
                                    }

                                }
                            }

                            //Return Result SSR
                            for (int i = 0; i < retResult.Flights.Length; i++)
                            {
                                for (int j = 0; j < retResult.Flights[i].Length; j++)
                                {
                                    string DepartureStation = retResult.Flights[i][j].Origin.AirportCode;
                                    string ArrivalStation = retResult.Flights[i][j].Destination.AirportCode;

                                    for (int p = 0; p < ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList.Length; p++)
                                    {
                                        SSRSegment sSRSegment = ssrResponse.SSRAvailabilityForBookingResponse.SSRSegmentList[p];
                                        if (sSRSegment.AvailablePaxSSRList != null && sSRSegment.AvailablePaxSSRList.Length > 0 && sSRSegment.LegKey.DepartureStation == DepartureStation && sSRSegment.LegKey.ArrivalStation == ArrivalStation)
                                        {
                                            availablePaxSSRs_ReturnBag.AddRange(sSRSegment.AvailablePaxSSRList.ToList());
                                            availablePaxSSRs_ReturnMeal.AddRange(sSRSegment.AvailablePaxSSRList.ToList());
                                        }
                                    }

                                }
                            }

                            if (availablePaxSSRs_OnwardBag.Count > 0)
                            {
                                GetIndigoSSRs(ref dtSourceBaggage, "0", availablePaxSSRs_OnwardBag,"Baggage");
                            }

                            if (availablePaxSSRs_ReturnBag.Count > 0)
                            {
                                GetIndigoSSRs(ref dtSourceBaggage, "1", availablePaxSSRs_ReturnBag, "Baggage");
                            }

                            if (availablePaxSSRs_OnwardMeal.Count > 0)
                            {
                                GetIndigoSSRs(ref dtSourceBaggage, "0", availablePaxSSRs_OnwardMeal,"Meal");
                            } //OnwardMeal

                            if (availablePaxSSRs_ReturnMeal.Count > 0)
                            {
                                GetIndigoSSRs(ref dtSourceBaggage, "1", availablePaxSSRs_OnwardMeal, "Meal");
                            } //ReturnMeal


                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //In case of exception in any of the above requests call the clear method.
                //This method will release the itinerary.
                //Clears the session.
                if (logonResponse != null && !string.IsNullOrEmpty(logonResponse.Signature) && logonResponse.Signature.Length > 0)
                {
                    ClearRequest clearRequest = new ClearRequest();
                    clearRequest.ContractVersion = contractVersion;
                    clearRequest.EnableExceptionStackTrace = false;
                    clearRequest.Signature = logonResponse.Signature;
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearRequest));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_ClearRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearRequest);
                        sw.Close();
                    }
                    catch { }
                    ClearResponse clearResponse = bookingAPI.Clear(clearRequest);
                    try
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ClearResponse));
                        string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "Indigo_ClearResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                        StreamWriter sw = new StreamWriter(filePath);
                        ser.Serialize(sw, clearResponse);
                        sw.Close();
                    }
                    catch { }
                }
                throw ex;
            }
            return dtSourceBaggage;
        }

        /// <summary>
        /// Gets the Sell Response for Special Round trip In combination Search 
        /// </summary>
        /// <param name="signature"></param>
        /// <param name="onwResult"></param>
        /// <param name="retResult"></param>
        /// <returns></returns>
        private BookingUpdateResponseData GetSellResponse(string signature, SearchResult onwResult, SearchResult retResult)
        {
            BookingUpdateResponseData responseData = new BookingUpdateResponseData();

            try
            {
                SellRequestData requestData = new SellRequestData();
                SellRequest request = new SellRequest();

                request.ContractVersion = contractVersion;
                request.Signature = signature;
                request.EnableExceptionStackTrace = false;
                requestData.SellBy = SellBy.JourneyBySellKey;
                requestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = new SellJourneyByKeyRequestData();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.ActionStatusCode = "NN";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.CurrencyCode = currencyCode;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.IsAllotmentMarketFare = false;

                //Define SourcePOS data
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS = new PointOfSale();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.AgentCode = "AG";
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.DomainCode = agentDomain;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.SourcePOS.OrganizationCode = agentId;

                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys = new SellKeyList[2];

                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0] = new SellKeyList();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].JourneySellKey = onwResult.JourneySellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].FareSellKey = onwResult.FareSellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[0].StandbyPriorityCode = "";

                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1] = new SellKeyList();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].JourneySellKey = retResult.JourneySellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].FareSellKey = retResult.FareSellKey;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.JourneySellKeys[1].StandbyPriorityCode = "";

                List<PaxPriceType> paxPriceTypes = new List<PaxPriceType>();
                for (int i = 0; i < onwResult.FareBreakdown.Length; i++)
                {
                    if (onwResult.FareBreakdown[i] != null && onwResult.FareBreakdown[i].PassengerType != PassengerType.Infant)
                    {
                        PaxPriceType priceType = new PaxPriceType();
                        priceType.PaxDiscountCode = "";
                        for (int h = 0; h < onwResult.FareBreakdown[i].PassengerCount; h++)
                        {
                            switch (onwResult.FareBreakdown[i].PassengerType)
                            {
                                case PassengerType.Adult:
                                    priceType.PaxType = "ADT";
                                    break;
                                case PassengerType.Child:
                                    priceType.PaxType = "CHD";
                                    break;

                            }
                            paxPriceTypes.Add(priceType);
                        }
                    }
                }
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxPriceType = paxPriceTypes.ToArray();
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.PaxCount = (Int16)paxPriceTypes.Count;
                requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.LoyaltyFilter = LoyaltyFilter.MonetaryOnly;

                if (!string.IsNullOrEmpty(promoCode))//Promo Code Implementation for corporate fare
                {
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale = new TypeOfSale();
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes = new string[1];
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.FareTypes[0] = "F";//Corporate Fare
                    requestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData.TypeOfSale.PromotionCode = promoCode;
                }
                request.SellRequestData = requestData;
                try
                {

                    XmlSerializer ser = new XmlSerializer(typeof(SellRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoSellRequest_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch { }

                SellResponse sellResponse_v_4_5 = bookingAPI.Sell(request);
                responseData = sellResponse_v_4_5.BookingUpdateResponseData;
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(SellResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoSellResponse_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, sellResponse_v_4_5);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Book, Severity.High, appUserId, "(Indigo)Failed to Book. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return responseData;
        }

        /// <summary>
        /// Gets the updated GST break up for Indigo supplier for special round trip fares in combination search.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="request"></param>
        public void GetUpdatedGSTFareBreakUp(ref SearchResult onwResult, ref SearchResult retResult, SearchRequest request)
        {
            try
            {
                //If the lead pax supplies the GST input.
                //Then Get the updated GST farebreak up.
                if (request.SearchBySegments) //For One-one search
                {
                    logonResponse.Signature = onwResult.RepriceErrorMessage; //assigning signature if request is one - one search
                }

                //Taxbreakup variables declaration For Special Round Trip Onward Result
                List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>> paxTypeTaxBreakUpOnward = new List<KeyValuePair<string, List<KeyValuePair<String, decimal>>>>();
                List<KeyValuePair<String, decimal>> adtPaxTaxBreakUpOnward = new List<KeyValuePair<String, decimal>>();
                List<KeyValuePair<String, decimal>> chdPaxTaxBreakUpOnward = new List<KeyValuePair<String, decimal>>();
                List<KeyValuePair<String, decimal>> inftPaxTaxBreakUpOnward = new List<KeyValuePair<String, decimal>>();

                //Get the bifurcation of taxes after GST input.
                if (logonResponse != null && logonResponse.Signature.Length > 0)
                {
                    GetBookingFromStateResponse updatedGSTBreakup = GetBookingFromState(logonResponse.Signature);

                    //Onward Result
                    if (updatedGSTBreakup != null && updatedGSTBreakup.BookingData != null && updatedGSTBreakup.BookingData.Journeys != null && updatedGSTBreakup.BookingData.Journeys.Length > 0)
                    {
                        rateOfExchange = exchangeRates[updatedGSTBreakup.BookingData.CurrencyCode];

                        List<PaxFare> adultPaxfare = new List<PaxFare>();
                        List<PaxFare> childPaxfare = new List<PaxFare>();

                        paxTypeTaxBreakUp.Clear();
                        adtPaxTaxBreakUp.Clear();
                        chdPaxTaxBreakUp.Clear();
                        inftPaxTaxBreakUp.Clear();
                        int journey = 0;


                        if (updatedGSTBreakup.BookingData.Journeys[journey] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length > 0)
                        {
                            for (int segment = 0; segment < updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length; segment++)
                            {
                                if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length > 0)
                                {
                                    for (int fare = 0; fare < updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length; fare++)
                                    {
                                        if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).Count() > 0)
                                        {
                                            adultPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).FirstOrDefault());
                                        }
                                        if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).Count() > 0)
                                        {
                                            childPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).FirstOrDefault());
                                        }
                                    }
                                }
                            }
                        }

                        Fare infantFare = onwResult.FareBreakdown.FirstOrDefault(P => (P.PassengerType == PassengerType.Infant));
                        onwResult.Price = new PriceAccounts();
                        onwResult.BaseFare = 0;
                        onwResult.Tax = 0;
                        onwResult.TotalFare = 0;
                        onwResult.FareBreakdown = null;
                        int fareBreakDownCount = 0;
                        if (request.AdultCount > 0)
                        {
                            fareBreakDownCount = 1;
                        }
                        if (request.ChildCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        if (request.InfantCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        onwResult.FareBreakdown = new Fare[fareBreakDownCount];
                        if (request.AdultCount > 0)
                        {
                            onwResult.FareBreakdown[0] = new Fare();
                            onwResult.FareBreakdown[0].PassengerCount = request.AdultCount;
                            onwResult.FareBreakdown[0].PassengerType = PassengerType.Adult;
                        }
                        if (request.ChildCount > 0)
                        {
                            onwResult.FareBreakdown[1] = new Fare();
                            onwResult.FareBreakdown[1].PassengerCount = request.ChildCount;
                            onwResult.FareBreakdown[1].PassengerType = PassengerType.Child;
                        }
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            onwResult.FareBreakdown[2] = new Fare();
                            onwResult.FareBreakdown[2].PassengerCount = request.InfantCount;
                            onwResult.FareBreakdown[2].PassengerType = PassengerType.Infant;
                        }
                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                        {
                            onwResult.FareBreakdown[1] = new Fare();
                            onwResult.FareBreakdown[1].PassengerCount = request.InfantCount;
                            onwResult.FareBreakdown[1].PassengerType = PassengerType.Infant;
                        }


                        if (adultPaxfare != null && adultPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareAdult in adultPaxfare)
                            {
                                ComputeAdultPaxFare(ref onwResult, paxFareAdult);
                            }
                        }

                        if (childPaxfare != null && childPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareChild in childPaxfare)
                            {
                                ComputeChildPaxFare(ref onwResult, paxFareChild);
                            }
                        }


                        if (request.InfantCount > 0)
                        {
                            if (infantFare != null)
                            {
                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                {
                                    onwResult.FareBreakdown[2] = infantFare;
                                }
                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                {

                                    onwResult.FareBreakdown[1] = infantFare;

                                }
                                onwResult.Price.PublishedFare += (decimal)infantFare.BaseFare;
                                onwResult.Price.Tax += (decimal)infantFare.Tax;
                                onwResult.Price.SupplierPrice += (decimal)infantFare.SupplierFare;
                            }
                        }

                        if (adtPaxTaxBreakUp.Count > 0)
                        {
                            adtPaxTaxBreakUpOnward.AddRange(adtPaxTaxBreakUp);
                            paxTypeTaxBreakUpOnward.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUpOnward));
                        }
                        if (chdPaxTaxBreakUp.Count > 0)
                        {
                            chdPaxTaxBreakUpOnward.AddRange(chdPaxTaxBreakUp);
                            paxTypeTaxBreakUpOnward.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUpOnward));
                        }
                        if (inftPaxTaxBreakUp.Count > 0)
                        {
                            inftPaxTaxBreakUpOnward.AddRange(inftPaxTaxBreakUp);
                            paxTypeTaxBreakUpOnward.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUpOnward));
                        }
                        if (paxTypeTaxBreakUpOnward.Count > 0)
                        {
                            onwResult.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUpOnward;
                        }
                        onwResult.Price.SupplierCurrency = currencyCode;
                        onwResult.BaseFare = (double)onwResult.Price.PublishedFare;
                        onwResult.Tax = (double)onwResult.Price.Tax;
                        onwResult.TotalFare = (double)Math.Round((onwResult.BaseFare + onwResult.Tax), agentDecimalValue);
                    }




                    //Return Result
                    if (updatedGSTBreakup != null && updatedGSTBreakup.BookingData != null && updatedGSTBreakup.BookingData.Journeys != null && updatedGSTBreakup.BookingData.Journeys.Length > 0)
                    {
                        rateOfExchange = exchangeRates[updatedGSTBreakup.BookingData.CurrencyCode];

                        List<PaxFare> adultPaxfare = new List<PaxFare>();
                        List<PaxFare> childPaxfare = new List<PaxFare>();

                        paxTypeTaxBreakUp.Clear();
                        adtPaxTaxBreakUp.Clear();
                        chdPaxTaxBreakUp.Clear();
                        inftPaxTaxBreakUp.Clear();

                        int journey = 1;//Return result

                        if (updatedGSTBreakup.BookingData.Journeys[journey] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length > 0)
                        {
                            for (int segment = 0; segment < updatedGSTBreakup.BookingData.Journeys[journey].Segments.Length; segment++)
                            {
                                if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment] != null && updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length > 0)
                                {
                                    for (int fare = 0; fare < updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares.Length; fare++)
                                    {
                                        if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).Count() > 0)
                                        {
                                            adultPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("ADT")).FirstOrDefault());
                                        }
                                        if (updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).Count() > 0)
                                        {
                                            childPaxfare.Add(updatedGSTBreakup.BookingData.Journeys[journey].Segments[segment].Fares[fare].PaxFares.Where(i => i.PaxType.Equals("CHD")).FirstOrDefault());
                                        }
                                    }
                                }
                            }
                        }
                        Fare infantFare = retResult.FareBreakdown.FirstOrDefault(P => (P.PassengerType == PassengerType.Infant));
                        retResult.Price = new PriceAccounts();
                        retResult.BaseFare = 0;
                        retResult.Tax = 0;
                        retResult.TotalFare = 0;
                        retResult.FareBreakdown = null;
                        int fareBreakDownCount = 0;
                        if (request.AdultCount > 0)
                        {
                            fareBreakDownCount = 1;
                        }
                        if (request.ChildCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        if (request.InfantCount > 0)
                        {
                            fareBreakDownCount++;
                        }
                        retResult.FareBreakdown = new Fare[fareBreakDownCount];
                        if (request.AdultCount > 0)
                        {
                            retResult.FareBreakdown[0] = new Fare();
                            retResult.FareBreakdown[0].PassengerCount = request.AdultCount;
                            retResult.FareBreakdown[0].PassengerType = PassengerType.Adult;
                        }
                        if (request.ChildCount > 0)
                        {
                            retResult.FareBreakdown[1] = new Fare();
                            retResult.FareBreakdown[1].PassengerCount = request.ChildCount;
                            retResult.FareBreakdown[1].PassengerType = PassengerType.Child;
                        }
                        if (request.ChildCount > 0 && request.InfantCount > 0)
                        {
                            retResult.FareBreakdown[2] = new Fare();
                            retResult.FareBreakdown[2].PassengerCount = request.InfantCount;
                            retResult.FareBreakdown[2].PassengerType = PassengerType.Infant;
                        }
                        else if (request.ChildCount == 0 && request.InfantCount > 0)
                        {
                            retResult.FareBreakdown[1] = new Fare();
                            retResult.FareBreakdown[1].PassengerCount = request.InfantCount;
                            retResult.FareBreakdown[1].PassengerType = PassengerType.Infant;
                        }
                        if (adultPaxfare != null && adultPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareAdult in adultPaxfare)
                            {
                                ComputeAdultPaxFare(ref retResult, paxFareAdult);
                            }
                        }

                        if (childPaxfare != null && childPaxfare.Count > 0)
                        {
                            foreach (PaxFare paxFareChild in childPaxfare)
                            {
                                ComputeChildPaxFare(ref retResult, paxFareChild);
                            }
                        }


                        if (request.InfantCount > 0)
                        {
                            if (infantFare != null)
                            {
                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                {
                                    retResult.FareBreakdown[2] = infantFare;
                                }
                                else if (request.ChildCount <= 0 && request.InfantCount > 0)
                                {

                                    retResult.FareBreakdown[1] = infantFare;

                                }
                                retResult.Price.PublishedFare += (decimal)infantFare.BaseFare;
                                retResult.Price.Tax += (decimal)infantFare.Tax;
                                retResult.Price.SupplierPrice += (decimal)infantFare.SupplierFare;
                            }
                        }

                        if (adtPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("ADT", adtPaxTaxBreakUp));
                        }
                        if (chdPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("CHD", chdPaxTaxBreakUp));
                        }
                        if (inftPaxTaxBreakUp.Count > 0)
                        {
                            paxTypeTaxBreakUp.Add(new KeyValuePair<string, List<KeyValuePair<string, decimal>>>("INF", inftPaxTaxBreakUp));
                        }
                        if (paxTypeTaxBreakUp.Count > 0)
                        {
                            retResult.Price.PaxTypeTaxBreakUp = paxTypeTaxBreakUp;
                        }
                        retResult.Price.SupplierCurrency = currencyCode;
                        retResult.BaseFare = (double)retResult.Price.PublishedFare;
                        retResult.Tax = (double)retResult.Price.Tax;
                        retResult.TotalFare = (double)Math.Round((retResult.BaseFare + retResult.Tax), agentDecimalValue);
                    }
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo)Failed to GetUpdatedGSTFareBreakUp. Error : " + ex.ToString(), "");
                throw ex;
            }

        }

        /// <summary>
        /// This method used Only for oneWay CombinationSearch
        /// </summary>
        /// <returns></returns>
        public LogonResponse CreateSession()
        {
            try
            {
                //SearchRequest request = searchRequests[Convert.ToInt32(eventNumber)];
                LogonRequest request = new LogonRequest();
                request.logonRequestData = new LogonRequestData();
                request.logonRequestData.AgentName = agentId;
                request.logonRequestData.DomainCode = agentDomain;
                request.logonRequestData.LocationCode = "";
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.Password = password;
                request.logonRequestData.RoleCode = "";
                request.logonRequestData.TerminalInfo = "";
                request.ContractVersion = contractVersion;
                request.EnableExceptionStackTrace = false;

                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(LogonRequest));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoLogonRequest" + "_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, request);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo)Failed to Serialize LogonRequest. Reason : " + ex.ToString(), "");
                }

                logonResponse = sessionAPI.Logon(request);
                try
                {
                    XmlSerializer ser = new XmlSerializer(typeof(LogonResponse));
                    string filePath = xmlLogPath + bookingSourceFlag + "_" + sessionId + "_" + "IndigoLogonResponse_" + "_" + appUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml";
                    StreamWriter sw = new StreamWriter(filePath);
                    ser.Serialize(sw, logonResponse);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo)Failed to Serialize LogonResponse. Reason : " + ex.ToString(), "");
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo) Failed to Login . Reason : " + ex.ToString(), "");
                logonResponse = null;

            }

            return logonResponse;

        }

        public void ComputePriceFromAvalibilityResponse(ref SearchResult resultObj, Indigo.BookingMgr.Fare OnwardFare)
        {
            int fareBreakDownCount = 0;
            if (request.AdultCount > 0)
            {
                fareBreakDownCount = 1;
            }
            if (request.ChildCount > 0)
            {
                fareBreakDownCount++;
            }
            if (request.InfantCount > 0)
            {
                fareBreakDownCount++;
            }
            resultObj.FareBreakdown = new Fare[fareBreakDownCount];
            if (request.AdultCount > 0)
            {
                resultObj.FareBreakdown[0] = new Fare();
                resultObj.FareBreakdown[0].PassengerCount = request.AdultCount;
                resultObj.FareBreakdown[0].PassengerType = PassengerType.Adult;
            }
            if (request.ChildCount > 0)
            {
                resultObj.FareBreakdown[1] = new Fare();
                resultObj.FareBreakdown[1].PassengerCount = request.ChildCount;
                resultObj.FareBreakdown[1].PassengerType = PassengerType.Child;
            }
            if (request.ChildCount > 0 && request.InfantCount > 0)
            {
                resultObj.FareBreakdown[2] = new Fare();
                resultObj.FareBreakdown[2].PassengerCount = request.InfantCount;
                resultObj.FareBreakdown[2].PassengerType = PassengerType.Infant;
            }
            else if (request.ChildCount == 0 && request.InfantCount > 0)
            {
                resultObj.FareBreakdown[1] = new Fare();
                resultObj.FareBreakdown[1].PassengerCount = request.InfantCount;
                resultObj.FareBreakdown[1].PassengerType = PassengerType.Infant;
            }
            if (resultObj.Price == null)
                resultObj.Price = new PriceAccounts();
            paxTypeTaxBreakUp.Clear();
            adtPaxTaxBreakUp.Clear();
            chdPaxTaxBreakUp.Clear();
            inftPaxTaxBreakUp.Clear();
            PaxFare adultPaxfare = OnwardFare.PaxFares.Where(i => i.PaxType.Equals("ADT")).FirstOrDefault();
            PaxFare childPaxfare = OnwardFare.PaxFares.Where(i => i.PaxType.Equals("CHD")).FirstOrDefault();
            if (adultPaxfare != null && adultPaxfare.ServiceCharges != null && adultPaxfare.ServiceCharges.Length > 0)
            {
                ComputeAdultPaxFare(ref resultObj, adultPaxfare);
            }
            if (childPaxfare != null && childPaxfare.ServiceCharges != null && childPaxfare.ServiceCharges.Length > 0)
            {
                ComputeChildPaxFare(ref resultObj, childPaxfare);
            }
            resultObj.Price.SupplierCurrency = currencyCode;
            resultObj.BaseFare = (double)Math.Round(resultObj.Price.PublishedFare, agentDecimalValue);
            resultObj.Tax = (double)resultObj.Price.Tax;
            resultObj.TotalFare = (double)(Math.Round((resultObj.BaseFare + resultObj.Tax), agentDecimalValue));
        }

        public void GetIndigoSSRs(ref DataTable dtSourceBaggage, string type, List<AvailablePaxSSR> paxSSRs, string ssrType)
        {
            try
            {
                if (dtSourceBaggage.Columns.Count == 0)
                {
                    dtSourceBaggage.Columns.Add("Code", typeof(string));
                    dtSourceBaggage.Columns.Add("Price", typeof(decimal));
                    dtSourceBaggage.Columns.Add("Group", typeof(int));
                    dtSourceBaggage.Columns.Add("Currency", typeof(string));
                    dtSourceBaggage.Columns.Add("Description", typeof(string));
                    dtSourceBaggage.Columns.Add("QtyAvailable", typeof(int));
                }
                List<string> ssrCodes = ssrType == "Baggage" ? baggageCodes : mealCodes;
                foreach (string ssrCode in ssrCodes)
                {
                    AvailablePaxSSR availableSSR = null;
                    List<AvailablePaxSSR> availablePaxSSRs = null;
                    if(ssrType=="Baggage")
                        availableSSR = paxSSRs.Where(m => m.SSRCode.Contains(ssrCode)).FirstOrDefault();//baggage
                    else
                        availablePaxSSRs = paxSSRs.Where(m => m.SSRCode.Contains(ssrCode)).ToList(); //Meals
                    if (availableSSR != null || (availablePaxSSRs!=null && availablePaxSSRs.Count>0))
                    {
                        List<PaxSSRPrice> paxSSRPrices = new List<PaxSSRPrice>();
                        List<BookingServiceCharge> charges = new List<BookingServiceCharge>();
                        if (availableSSR != null && availableSSR.PaxSSRPriceList != null) //Baggage
                        {
                            paxSSRPrices.AddRange(availableSSR.PaxSSRPriceList);
                        }
                        else if (availablePaxSSRs != null) //Meal
                        {
                            for (int i = 0; i < availablePaxSSRs.Count; i++)
                            {
                                paxSSRPrices.AddRange(availablePaxSSRs[i].PaxSSRPriceList);
                            }
                        }
                        for (int k = 0; k < paxSSRPrices.Count; k++)
                        {
                            charges.AddRange(paxSSRPrices[k].PaxFee.ServiceCharges.ToList());
                        }

                        decimal baggagePrice = charges.Where(o => o.ChargeType == Indigo.BookingMgr.ChargeType.ServiceCharge || o.ChargeType == Indigo.BookingMgr.ChargeType.Tax).Sum(item => item.Amount);
                        if(charges.Count>0 && !string.IsNullOrEmpty(charges[0].CurrencyCode))
                            rateOfExchange = exchangeRates[charges[0].CurrencyCode];

                        DataRow dr = dtSourceBaggage.NewRow();
                        dr["Code"] = ssrCode;
                        dr["Price"] = Math.Round(Convert.ToDecimal(baggagePrice) * rateOfExchange, agentDecimalValue);
                        dr["Group"] = type;
                        dr["Currency"] = agentBaseCurrency;
                        switch (ssrCode)
                        {
                            //meal codes
                            case "VGML":
                                dr["Description"] = "Veg Meal";
                                break;
                            case "NVML":
                                dr["Description"] = "Non Veg Meal";
                                break;

                            case "TCSW":
                                dr["Description"] = "Tomato Cucumber Cheese Lettuce Sandwich Combo     ";
                                break;
                            case "PTSW":
                                dr["Description"] = "Paneer Tikka Sandwich  Combo";
                                break;
                            case "CJSW":
                                dr["Description"] = "Chicken Junglee Sandwich  Combo";
                                break;
                            case "CTSW":
                                dr["Description"] = "Chicken Tikka Sandwich Combo";
                                break;
                            case "POHA":
                                dr["Description"] = "POHA Combo";
                                break;
                            case "UPMA":
                                dr["Description"] = "RAVA UPMA  Combo";
                                break;
                            case "SACH":
                                dr["Description"] = "SAMBAR CHAWAL  Combo ";
                                break;
                            case "VBIR":
                                dr["Description"] = "VEG BIRYANI  Combo  ";
                                break;
                            case "DACH":
                                dr["Description"] = "DAL CHAWAL  Combo";
                                break;
                            case "CHFR":
                                dr["Description"] = "Chinese Fried Rice";
                                break;
                            case "CHCH":
                                dr["Description"] = "Chole Chawal";
                                break;
                            case "MASP":
                                dr["Description"] = "Makhana salt and pepper";
                                break;
                            case "SMAL":
                                dr["Description"] = "Smoked Almonds";
                                break;

                            //Baggage Codes
                            case "XBPA":
                                dr["Description"] = "Prepaid Excess Baggage – 5 Kg";
                                break;
                            case "XBPB":
                                dr["Description"] = "Prepaid Excess Baggage – 10 Kg";
                                break;
                            case "XBPC":
                                dr["Description"] = "Prepaid Excess Baggage – 15 Kg";
                                break;
                            case "XBPD":
                                dr["Description"] = "Prepaid Excess Baggage – 30 Kg";
                                break;
                            case "XBPE":
                                dr["Description"] = "Prepaid Excess Baggage – 3 Kg";
                                break;

                            //FOR INTERNATIONAL SECTORS
                            case "IXBA":
                                dr["Description"] = "International Connections Baggage – 8 KG";
                                break;
                            case "IXBB":
                                dr["Description"] = "International Connections Baggage – 15 KG";
                                break;
                            case "IXBC":
                                dr["Description"] = "International Connections Baggage – 30 KG";
                                break;

                        }

                        dr["QtyAvailable"] = 5000;
                        dtSourceBaggage.Rows.Add(dr);
                    }
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(Indigo) Error in GetIndigoSSRs() . Reason : " + ex.ToString(), "");
                throw ex;
            }
        }
    }
}
