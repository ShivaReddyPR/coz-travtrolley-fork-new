﻿using System;
using System.Collections.Generic;
using System.Text;
using CT.Configuration;
using CT.Core;
using CT.BookingEngine;
using System.IO;
using System.Security.Cryptography;
using System.IO.Compression;
using System.Net;
using Newtonsoft.Json;
using System.Xml;
using System.Xml.Serialization;
using System.Web.Script.Serialization;

namespace PKFARES
{
    [Serializable]
    public class API : IDisposable
    {
        string xmlLogPath;
        string partnerID;
        string partnerKey;
        string signID;
        int appUserID;
        string sessionID;
        int decimalValue;
        string agentBaseCurrency;
        Dictionary<string, decimal> exchangeRates;
        decimal rateOfExchange = 1;

        public int AppUserID
        {
            get
            {
                return appUserID;
            }

            set
            {
                appUserID = value;
            }
        }

        public string SessionID
        {
            get
            {
                return sessionID;
            }

            set
            {
                sessionID = value;
            }
        }

        public int DecimalValue
        {
            get
            {
                return decimalValue;
            }

            set
            {
                decimalValue = value;
            }
        }

        public string AgentBaseCurrency
        {
            get
            {
                return agentBaseCurrency;
            }

            set
            {
                agentBaseCurrency = value;
            }
        }

        public Dictionary<string, decimal> ExchangeRates
        {
            get
            {
                return exchangeRates;
            }

            set
            {
                exchangeRates = value;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public API()
        {
            xmlLogPath = ConfigurationSystem.PKFaresConfig["XmlLogPath"];
            partnerID = ConfigurationSystem.PKFaresConfig["PartnerID"];
            partnerKey = ConfigurationSystem.PKFaresConfig["PartnerKey"];
            //Create SignID with MD5 hashing
            using (MD5 hash = MD5.Create())
            {
                signID = GetMd5Hash(hash, partnerID + partnerKey);
            }

            xmlLogPath = Path.Combine(xmlLogPath, DateTime.Now.ToString("ddMMMyyyy") + "\\");
            if(!Directory.Exists(xmlLogPath))
            {
                Directory.CreateDirectory(xmlLogPath);
            }
        }


        #region Helper Methods
        /// <summary>
        /// Static method for Computing MD5 hash for SignID
        /// </summary>
        /// <param name="md5Hash"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        /// <summary>
        /// Method to encode JSON request into base64 string
        /// </summary>
        /// <param name="toEncode"></param>
        /// <returns></returns>
        static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);

            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;
        }

        /// <summary>
        /// Decrypt response from GZIP
        /// </summary>
        /// <param name="webResponse"></param>
        /// <returns></returns>
        private Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            Stream stream;
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
                case "DEFLATE":
                    stream = new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;

                default:
                    stream = new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                    break;
            }
            return stream;
        }

        /// <summary>
        /// Method to send request and get response
        /// </summary>
        /// <param name="requestString"></param>
        /// <returns></returns>
        string GetResponse(string requestString, string requestType)
        {
            string responseFromServer = "";
            try
            {                
                //string contentType = "application/octet-stream";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationSystem.PKFaresConfig[requestType] + "?param=" + requestString);
                request.Method = "POST"; //Using POST method       
                string postData = "param=" + requestString;
                //request.AutomaticDecompression = DecompressionMethods.GZip;
                
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate");                

                HttpWebResponse httpResponse = request.GetResponse() as HttpWebResponse;

                // handle if response is a compressed stream
                Stream dataStream = GetStreamForResponse(httpResponse);
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("(PKFares)Failed to get response from Search request : " + ex.Message, ex);
            }
            return responseFromServer;
        }

        /// <summary>
        /// Method for converting JSON into Xml.
        /// </summary>
        /// <param name="json"></param>
        /// <param name="filePath"></param>
        /// <param name="requestType"></param>
        void SerializeJSONToXML(string json, string filePath, string requestType)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ObjectCreationHandling = ObjectCreationHandling.Reuse;
            settings.StringEscapeHandling = StringEscapeHandling.Default;
            settings.Formatting = Newtonsoft.Json.Formatting.Indented;
            settings.PreserveReferencesHandling = PreserveReferencesHandling.All;
            settings.TypeNameHandling = TypeNameHandling.All;            

            if (string.IsNullOrEmpty(requestType))
            {
                XmlDocument doc = JsonConvert.DeserializeXmlNode(json) as XmlDocument;
                doc.Save(filePath);
            }
            else
            {
                XmlDocument doc = JsonConvert.DeserializeXmlNode(json, requestType);
                doc.Save(filePath);                
            }
            
        }

        

        string GetPassengerType(PassengerType paxType)
        {
            string passengerType = string.Empty;

            switch (paxType)
            {
                case PassengerType.Adult:
                    passengerType = "ADT";
                    break;
                case PassengerType.Child:
                    passengerType = "CHD";
                    break;
                case PassengerType.Infant:
                    passengerType = "INF";
                    break;
            }

            return passengerType;
        }

        string GetGender(Gender gender)
        {
            string passengerGender = string.Empty;

            switch(gender)
            {
                case Gender.Male:
                    passengerGender = "M";
                    break;
                case Gender.Female:
                    passengerGender = "F";
                    break;
            }

            return passengerGender;
        }

        #endregion
        
        #region JSON Request Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        string GenerateSearchRequest(SearchRequest request)
        {
            StringBuilder searchJSON = new StringBuilder();

            try
            {
                searchJSON.AppendLine("{");
                searchJSON.AppendLine("\"authentication\": {");
                searchJSON.AppendLine("\"partnerId\": \"" + partnerID + "\",");
                searchJSON.AppendLine("\"sign\": \"" + signID + "\"");
                searchJSON.AppendLine("},");
                searchJSON.AppendLine("\"search\": {");
                searchJSON.AppendLine("\"adults\":" + request.AdultCount + ",");
                searchJSON.AppendLine("\"airline\": \"" + (request.Segments[0].PreferredAirlines != null && request.Segments[0].PreferredAirlines.Length > 0 ? request.Segments[0].PreferredAirlines[0] : "") + "\",");
                searchJSON.AppendLine("\"children\": " + request.ChildCount + ",");
                searchJSON.AppendLine("\"nonstop\": " + (request.MaxStops == "-1" || request.MaxStops == "0" ? 1 : 0) + ",");
                searchJSON.AppendLine("\"searchAirLegs\": [");
                searchJSON.AppendLine("{");
                searchJSON.AppendLine("\"cabinClass\":\"" + (request.Segments[0].flightCabinClass == CabinClass.All ? CabinClass.Economy.ToString() : request.Segments[0].flightCabinClass.ToString()) + "\",");
                searchJSON.AppendLine("\"departureDate\": \"" + request.Segments[0].PreferredDepartureTime.ToString("yyyy-MM-dd") + "\",");
                searchJSON.AppendLine("\"destination\": \"" + request.Segments[0].Destination + "\",");
                searchJSON.AppendLine("\"origin\": \"" + request.Segments[0].Origin + "\"");

                if (request.Type != SearchType.OneWay)
                {
                    searchJSON.AppendLine("},");
                    searchJSON.AppendLine("{");
                    searchJSON.AppendLine("\"cabinClass\": \"" + (request.Segments[0].flightCabinClass == CabinClass.All ? CabinClass.Economy.ToString() : request.Segments[0].flightCabinClass.ToString()) + "\",");
                    searchJSON.AppendLine("\"departureDate\": \"" + request.Segments[1].PreferredDepartureTime.ToString("yyyy-MM-dd") + "\",");
                    searchJSON.AppendLine("\"destination\": \"" + request.Segments[1].Destination + "\",");
                    searchJSON.AppendLine("\"origin\": \"" + request.Segments[1].Origin + "\"");
                    searchJSON.AppendLine("}");
                }
                else
                {
                    searchJSON.AppendLine("}");
                }
                searchJSON.AppendLine("],");
                searchJSON.AppendLine("\"solutions\":0");
                searchJSON.AppendLine("}");
                searchJSON.AppendLine("}");
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Convert JSON for Search. Reason : " + ex.Message, ex);
            }

            return searchJSON.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        string GeneratePrecisePricingRequest(SearchRequest request, SearchResult result)
        {
            StringBuilder searchJSON = new StringBuilder();

            try
            {
                searchJSON.AppendLine("{");
                searchJSON.AppendLine("\"authentication\": {");
                searchJSON.AppendLine("\"partnerId\": \"" + partnerID + "\",");
                searchJSON.AppendLine("\"sign\": \"" + signID + "\"");
                searchJSON.AppendLine("},");
                searchJSON.AppendLine("\"pricing\": {");
                searchJSON.AppendLine("\"adults\":" + request.AdultCount + ",");
                if (request.ChildCount > 0)
                {
                    searchJSON.AppendLine("\"children\": " + request.ChildCount + ",");
                }
                searchJSON.AppendLine("\"journeys\": {");
                searchJSON.AppendLine("\"journey_0\": [");
                List<FlightInfo> onwardSeg = new List<FlightInfo>();
                List<FlightInfo> inwardSeg = new List<FlightInfo>();

                for (int i = 0; i < result.Flights.Length; i++)
                {
                    for (int j = 0; j < result.Flights[i].Length; j++)
                    {
                        if (result.Flights[i][j].Group == 0)
                        {
                            onwardSeg.Add(result.Flights[i][j]);
                        }
                        else
                        {
                            inwardSeg.Add(result.Flights[i][j]);
                        }
                    }
                }
                for (int i = 0; i < onwardSeg.Count; i++)
                {
                    FlightInfo seg = onwardSeg[i];
                    
                    searchJSON.AppendLine("{");                    
                    searchJSON.AppendLine("\"airline\": \"" + seg.Airline + "\",");
                    searchJSON.AppendLine("\"flightNum\": \"" + seg.FlightNumber + "\",");
                    searchJSON.AppendLine("\"arrival\": \"" + seg.Destination.AirportCode + "\",");
                    searchJSON.AppendLine("\"arrivalDate\": \"" + seg.ArrivalTime.ToString("yyyy-MM-dd") + "\",");
                    searchJSON.AppendLine("\"arrivalTime\": \"" + seg.ArrivalTime.ToString("HH:mm") + "\",");                    
                    searchJSON.AppendLine("\"departure\": \"" + seg.Origin.AirportCode + "\",");
                    searchJSON.AppendLine("\"departureDate\": \"" + seg.DepartureTime.ToString("yyyy-MM-dd") + "\",");
                    searchJSON.AppendLine("\"departureTime\": \"" + seg.DepartureTime.ToString("HH:mm") + "\",");
                    searchJSON.AppendLine("\"bookingCode\": \"" + seg.BookingClass + "\",");
                    searchJSON.AppendLine("\"cabinClass\": \"" + seg.CabinClass + "\"");

                    if (onwardSeg.Count > 1 && i < (onwardSeg.Count - 1))
                    {
                        searchJSON.AppendLine("},");
                    }
                    else
                    {
                        searchJSON.AppendLine("}");
                    }
                }
                searchJSON.AppendLine("]" + (request.Type != SearchType.OneWay ? "," : ""));
                if (inwardSeg.Count > 0)
                {
                    searchJSON.AppendLine("\"journey_1\": [");

                    for (int i = 0; i < inwardSeg.Count; i++)
                    {
                        FlightInfo seg = inwardSeg[i];
                        
                        searchJSON.AppendLine("{");
                        searchJSON.AppendLine("\"airline\": \"" + seg.Airline + "\",");
                        searchJSON.AppendLine("\"flightNum\": \"" + seg.FlightNumber + "\",");
                        searchJSON.AppendLine("\"arrival\": \"" + seg.Destination.AirportCode + "\",");
                        searchJSON.AppendLine("\"arrivalDate\": \"" + seg.ArrivalTime.ToString("yyyy-MM-dd") + "\",");
                        searchJSON.AppendLine("\"arrivalTime\": \"" + seg.ArrivalTime.ToString("HH:mm") + "\",");
                        searchJSON.AppendLine("\"departure\": \"" + seg.Origin.AirportCode + "\",");
                        searchJSON.AppendLine("\"departureDate\": \"" + seg.DepartureTime.ToString("yyyy-MM-dd") + "\",");
                        searchJSON.AppendLine("\"departureTime\": \"" + seg.DepartureTime.ToString("HH:mm") + "\",");
                        searchJSON.AppendLine("\"bookingCode\": \"" + seg.BookingClass + "\",");
                        searchJSON.AppendLine("\"cabinClass\": \"" + seg.CabinClass + "\"");

                        if (inwardSeg.Count > 1 && i < (inwardSeg.Count - 1))
                        {
                            searchJSON.AppendLine("},");
                        }
                        else
                        {
                            searchJSON.AppendLine("}");
                        }
                    }

                    searchJSON.AppendLine("]");
                }
                searchJSON.AppendLine("}");//Journeys                
                searchJSON.AppendLine("}");//Pricing
                searchJSON.AppendLine("}");//Root
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Convert JSON for PrecisePricing. Reason : " + ex.Message, ex);
            }

            return searchJSON.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        string GeneratePreciseBookingRequest(FlightItinerary itinerary)
        {
            StringBuilder searchJSON = new StringBuilder();

            try
            {
                searchJSON.AppendLine("{");
                searchJSON.AppendLine("\"authentication\": {");
                searchJSON.AppendLine("\"partnerId\": \"" + partnerID + "\",");
                searchJSON.AppendLine("\"sign\": \"" + signID + "\"");
                searchJSON.AppendLine("},");
                searchJSON.AppendLine("\"booking\": {");
                searchJSON.AppendLine("\"passengers\": [");
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    FlightPassenger pax = itinerary.Passenger[i];
                    searchJSON.AppendLine("{");
                    searchJSON.AppendLine("\"birthday\":\"" + pax.DateOfBirth.ToString("yyyy-MM-dd") + "\",");
                    searchJSON.AppendLine("\"cardExpiredDate\":\"" + pax.PassportExpiry.ToString("yyyy-MM-dd") + "\",");
                    searchJSON.AppendLine("\"cardNum\":\"" + pax.PassportNo + "\",");
                    searchJSON.AppendLine("\"cardType\":\"P\",");
                    searchJSON.AppendLine("\"firstName\":\"" + pax.FirstName + "\",");
                    searchJSON.AppendLine("\"lastName\":\"" + pax.LastName + "\",");
                    searchJSON.AppendLine("\"nationality\":\"" + pax.Nationality.CountryCode + "\",");
                    searchJSON.AppendLine("\"psgType\":\"" + GetPassengerType(pax.Type) + "\",");
                    searchJSON.AppendLine("\"sex\":\"" + GetGender(pax.Gender) + "\"");
                    if (itinerary.Passenger.Length > 1 && i < (itinerary.Passenger.Length - 1))
                    {
                        searchJSON.AppendLine("},");
                    }
                    else
                    {
                        searchJSON.AppendLine("}");
                    }
                }
                searchJSON.AppendLine("],");

                List<FlightInfo> onwardSeg = new List<FlightInfo>();
                List<FlightInfo> inwardSeg = new List<FlightInfo>();


                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    if (itinerary.Segments[i].Group == 0)
                    {
                        onwardSeg.Add(itinerary.Segments[i]);
                    }
                    else
                    {
                        inwardSeg.Add(itinerary.Segments[i]);
                    }
                }
                searchJSON.AppendLine("\"solution\": {");
                searchJSON.AppendLine("\"adtFare\": \"" + (itinerary.TBOFareBreakdown[0].SupplierFare - itinerary.TBOFareBreakdown[0].SupplierTax) + "\",");
                searchJSON.AppendLine("\"adtTax\": \"" + itinerary.TBOFareBreakdown[0].SupplierTax + "\",");
                if (itinerary.TBOFareBreakdown.Length > 1 && itinerary.TBOFareBreakdown[1].PassengerType == PassengerType.Child)
                {
                    searchJSON.AppendLine("\"chdFare\": \"" + (itinerary.TBOFareBreakdown[1].SupplierFare - itinerary.TBOFareBreakdown[1].SupplierTax) + "\",");
                    searchJSON.AppendLine("\"chdTax\": \"" + itinerary.TBOFareBreakdown[1].SupplierTax + "\",");
                }                
                searchJSON.AppendLine("\"journeys\": {");
                searchJSON.AppendLine("\"journey_0\": [");
                for (int i = 0; i < onwardSeg.Count; i++)
                {
                    FlightInfo seg = onwardSeg[i];

                    searchJSON.AppendLine("{");
                    searchJSON.AppendLine("\"airline\": \"" + seg.Airline + "\",");
                    searchJSON.AppendLine("\"flightNum\": \"" + seg.FlightNumber + "\",");
                    searchJSON.AppendLine("\"arrival\": \"" + seg.Destination.AirportCode + "\",");
                    searchJSON.AppendLine("\"arrivalDate\": \"" + seg.ArrivalTime.ToString("yyyy-MM-dd") + "\",");
                    searchJSON.AppendLine("\"arrivalTime\": \"" + seg.ArrivalTime.ToString("HH:mm") + "\",");
                    searchJSON.AppendLine("\"departure\": \"" + seg.Origin.AirportCode + "\",");
                    searchJSON.AppendLine("\"departureDate\": \"" + seg.DepartureTime.ToString("yyyy-MM-dd") + "\",");
                    searchJSON.AppendLine("\"departureTime\": \"" + seg.DepartureTime.ToString("HH:mm") + "\",");
                    searchJSON.AppendLine("\"bookingCode\": \"" + seg.BookingClass + "\"");
                    

                    if (onwardSeg.Count > 1 && i < (onwardSeg.Count - 1))
                    {
                        searchJSON.AppendLine("},");
                    }
                    else
                    {
                        searchJSON.AppendLine("}");
                    }
                }
                searchJSON.AppendLine("]" + (inwardSeg.Count > 0 ? "," : ""));
                if (inwardSeg.Count > 0)
                {
                    searchJSON.AppendLine("\"journey_1\": [");

                    for (int i = 0; i < inwardSeg.Count; i++)
                    {
                        FlightInfo seg = inwardSeg[i];

                        searchJSON.AppendLine("{");
                        searchJSON.AppendLine("\"airline\": \"" + seg.Airline + "\",");
                        searchJSON.AppendLine("\"flightNum\": \"" + seg.FlightNumber + "\",");
                        searchJSON.AppendLine("\"arrival\": \"" + seg.Destination.AirportCode + "\",");
                        searchJSON.AppendLine("\"arrivalDate\": \"" + seg.ArrivalTime.ToString("yyyy-MM-dd") + "\",");
                        searchJSON.AppendLine("\"arrivalTime\": \"" + seg.ArrivalTime.ToString("HH:mm") + "\",");
                        searchJSON.AppendLine("\"departure\": \"" + seg.Origin.AirportCode + "\",");
                        searchJSON.AppendLine("\"departureDate\": \"" + seg.DepartureTime.ToString("yyyy-MM-dd") + "\",");
                        searchJSON.AppendLine("\"departureTime\": \"" + seg.DepartureTime.ToString("HH:mm") + "\",");
                        searchJSON.AppendLine("\"bookingCode\": \"" + seg.BookingClass + "\"");
                        

                        if (inwardSeg.Count > 1 && i < (inwardSeg.Count - 1))
                        {
                            searchJSON.AppendLine("},");
                        }
                        else
                        {
                            searchJSON.AppendLine("}");
                        }
                    }

                    searchJSON.AppendLine("]");
                }
                searchJSON.AppendLine("}");//Journeys 
                searchJSON.AppendLine("}");//Solution
                searchJSON.AppendLine("}");//Booking
                searchJSON.AppendLine("}");//Root
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Convert JSON for PreciseBooking. Reason : " + ex.Message, ex);
            }

            return searchJSON.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        string GenerateOrderPricingRequest(string orderNumber)
        {
            StringBuilder searchJSON = new StringBuilder();

            searchJSON.AppendLine("{");
            searchJSON.AppendLine("\"authentication\": {");
            searchJSON.AppendLine("\"partnerId\": \"" + partnerID + "\",");
            searchJSON.AppendLine("\"sign\": \"" + signID + "\"");
            searchJSON.AppendLine("},");
            searchJSON.AppendLine("\"orderPricing\": {");
            searchJSON.AppendLine("\"orderNum\": \"" + orderNumber + "\"");
            searchJSON.AppendLine("}");//OrderPricing
            searchJSON.AppendLine("}");

            return searchJSON.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        string GeneratePenaltyRequest(SearchRequest request, SearchResult result)
        {
            StringBuilder searchJSON = new StringBuilder();

            try
            {
                searchJSON.AppendLine("{");
                searchJSON.AppendLine("\"authentication\": {");
                searchJSON.AppendLine("\"partnerId\": \"" + partnerID + "\",");
                searchJSON.AppendLine("\"sign\": \"" + signID + "\"");
                searchJSON.AppendLine("},");
                searchJSON.AppendLine("\"penalty\": {");
                searchJSON.AppendLine("\"journeys\": {");
                searchJSON.AppendLine("\"journey_0\": [");
                List<FlightInfo> onwardSeg = new List<FlightInfo>();
                List<FlightInfo> inwardSeg = new List<FlightInfo>();

                for (int i = 0; i < result.Flights.Length; i++)
                {
                    for (int j = 0; j < result.Flights[i].Length; j++)
                    {
                        if (result.Flights[i][j].Group == 0)
                        {
                            onwardSeg.Add(result.Flights[i][j]);
                        }
                        else
                        {
                            inwardSeg.Add(result.Flights[i][j]);
                        }
                    }
                }
                for (int i = 0; i < onwardSeg.Count; i++)
                {
                    FlightInfo seg = onwardSeg[i];

                    searchJSON.AppendLine("{");
                    searchJSON.AppendLine("\"airline\": \"" + seg.Airline + "\",");
                    searchJSON.AppendLine("\"arrival\": \"" + seg.Destination.AirportCode + "\",");
                    searchJSON.AppendLine("\"arrivalDate\": \"" + seg.ArrivalTime.ToString("yyyy-MM-dd") + "\",");
                    searchJSON.AppendLine("\"arrivalTime\": \"" + seg.ArrivalTime.ToString("HH:mm") + "\",");
                    searchJSON.AppendLine("\"bookingCode\": \"" + seg.BookingClass + "\",");
                    searchJSON.AppendLine("\"cabinClass\": \"" + seg.CabinClass + "\",");
                    searchJSON.AppendLine("\"departure\": \"" + seg.Origin.AirportCode + "\",");
                    searchJSON.AppendLine("\"departureDate\": \"" + seg.DepartureTime.ToString("yyyy-MM-dd") + "\",");
                    searchJSON.AppendLine("\"departureTime\": \"" + seg.DepartureTime.ToString("HH:mm") + "\",");
                    searchJSON.AppendLine("\"flightNum\": \"" + seg.FlightNumber + "\"");
                    if (onwardSeg.Count > 1 && i < (onwardSeg.Count - 1))
                    {
                        searchJSON.AppendLine("},");
                    }
                    else
                    {
                        searchJSON.AppendLine("}");
                    }
                }
                searchJSON.AppendLine("]" + (request.Type != SearchType.OneWay ? "," : ""));
                if (inwardSeg.Count > 0)
                {
                    searchJSON.AppendLine("\"journey_1\": [");

                    for (int i = 0; i < inwardSeg.Count; i++)
                    {
                        FlightInfo seg = inwardSeg[i];

                        searchJSON.AppendLine("{");
                        searchJSON.AppendLine("\"airline\": \"" + seg.Airline + "\",");
                        searchJSON.AppendLine("\"arrival\": \"" + seg.Destination.AirportCode + "\",");
                        searchJSON.AppendLine("\"arrivalDate\": \"" + seg.ArrivalTime.ToString("yyyy-MM-dd") + "\",");
                        searchJSON.AppendLine("\"arrivalTime\": \"" + seg.ArrivalTime.ToString("HH:mm") + "\",");
                        searchJSON.AppendLine("\"bookingCode\": \"" + seg.BookingClass + "\",");
                        searchJSON.AppendLine("\"cabinClass\": \"" + seg.CabinClass + "\",");
                        searchJSON.AppendLine("\"departure\": \"" + seg.Origin.AirportCode + "\",");
                        searchJSON.AppendLine("\"departureDate\": \"" + seg.DepartureTime.ToString("yyyy-MM-dd") + "\",");
                        searchJSON.AppendLine("\"departureTime\": \"" + seg.DepartureTime.ToString("HH:mm") + "\",");
                        searchJSON.AppendLine("\"flightNum\": \"" + seg.FlightNumber + "\"");
                        if (inwardSeg.Count > 1 && i < (inwardSeg.Count - 1))
                        {
                            searchJSON.AppendLine("},");
                        }
                        else
                        {
                            searchJSON.AppendLine("}");
                        }
                    }

                    searchJSON.AppendLine("]");
                }
                searchJSON.AppendLine("}");//Journeys                
                searchJSON.AppendLine("}");//Penalty
                searchJSON.AppendLine("}");//Root
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Convert JSON for Penalty. Reason : " + ex.Message, ex);
            }

            return searchJSON.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <param name="pnr"></param>
        /// <returns></returns>
        string GenerateCancelOrderRequest(string orderNumber, string pnr)
        {
            StringBuilder searchJSON = new StringBuilder();

            searchJSON.AppendLine("{");
            searchJSON.AppendLine("\"authentication\": {");
            searchJSON.AppendLine("\"partnerId\": \"" + partnerID + "\",");
            searchJSON.AppendLine("\"sign\": \"" + signID + "\"");
            searchJSON.AppendLine("},");
            searchJSON.AppendLine("\"cancel\": {");
            searchJSON.AppendLine("\"orderNum\": \"" + orderNumber + "\",");
            searchJSON.AppendLine("\"virtualPnr\": \"" + pnr + "\"");
            searchJSON.AppendLine("}");//OrderPricing
            searchJSON.AppendLine("}");

            return searchJSON.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="name"></param>
        /// <param name="orderNumber"></param>
        /// <param name="telephone"></param>
        /// <param name="pnr"></param>
        /// <returns></returns>
        string GenerateTicketingRequest(string email, string name, string orderNumber, string telephone, string pnr)
        {
            StringBuilder searchJSON = new StringBuilder();

            searchJSON.AppendLine("{");
            searchJSON.AppendLine("\"authentication\": {");
            searchJSON.AppendLine("\"partnerId\": \"" + partnerID + "\",");
            searchJSON.AppendLine("\"sign\": \"" + signID + "\"");
            searchJSON.AppendLine("},");
            searchJSON.AppendLine("\"ticketing\": {");
            searchJSON.AppendLine("\"email\": \"" + email + "\",");
            searchJSON.AppendLine("\"name\": \"" + name + "\",");
            searchJSON.AppendLine("\"orderNum\": \"" + orderNumber + "\",");
            searchJSON.AppendLine("\"telNum\": \"" + telephone + "\",");
            searchJSON.AppendLine("\"PNR\": \"" + pnr + "\"");
            searchJSON.AppendLine("}");//OrderPricing
            searchJSON.AppendLine("}");

            return searchJSON.ToString();
        }

        #endregion


        /// <summary>
        /// Method to get Flight results 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<SearchResult> Search(SearchRequest request)
        {
            List<SearchResult> Results = new List<SearchResult>();
            
            try
            {
                string jsonRequest = GenerateSearchRequest(request);

                string filePath = xmlLogPath + sessionID + "_" + appUserID + "_ShoppingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                try
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    dynamic searchResponse = js.Deserialize<dynamic>(jsonRequest);
                    string rsp = JsonConvert.SerializeObject(searchResponse, Newtonsoft.Json.Formatting.Indented);
                    StreamWriter sw = new StreamWriter(filePath + ".json");
                    sw.Write(rsp);
                    sw.Close();

                    SerializeJSONToXML(rsp, filePath + ".xml", "SearchRequest");
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Search, Severity.High, appUserID, "(PKFARES)Failed to convert Search Request JSON into xml. Reason : " + ex.Message, "");
                }
                filePath = xmlLogPath + sessionID + "_" + appUserID + "_ShoppingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                string base64String = EncodeTo64(jsonRequest);
                //base64String += "&environment=Santestdbox&funcName=shopping";
                string response = GetResponse(base64String, "Shopping");
                                
                try
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    dynamic searchResponse = js.Deserialize<dynamic>(response);
                    string rsp = JsonConvert.SerializeObject(searchResponse, Newtonsoft.Json.Formatting.Indented);
                    StreamWriter sw = new StreamWriter(filePath + ".json");
                    sw.Write(rsp);
                    sw.Close();

                    SerializeJSONToXML(rsp, filePath + ".xml", "SearchResponse");                 

                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Search, Severity.High, appUserID, "(PKFARES)Failed to convert Search Response JSON into xml. Reason : " + ex.Message, "");
                }

                if (!string.IsNullOrEmpty(response) && response.Length > 0)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(filePath + ".xml");

                    XmlNodeList Solutions = doc.SelectNodes("SearchResponse/data/solutions");
                    XmlNodeList Flights = doc.SelectNodes("SearchResponse/data/flights");
                    XmlNodeList Segments = doc.SelectNodes("SearchResponse/data/segments");

                    foreach (XmlNode solution in Solutions)
                    {
                        SearchResult result = new SearchResult();
                        string currency = solution.SelectSingleNode("currency").InnerText;
                        result.ResultBookingSource = BookingSource.PKFares;
                        if (exchangeRates.ContainsKey(currency))
                        {
                            rateOfExchange = exchangeRates[currency];
                        }

                        result.Currency = agentBaseCurrency;
                        result.Price = new PriceAccounts();
                        result.Price.AccPriceType = PriceType.PublishedFare;
                        result.Price.RateOfExchange = rateOfExchange;
                        result.FareType = "PUB";

                        double baseFare = Convert.ToDouble(solution.SelectSingleNode("adtFare").InnerText) * request.AdultCount;
                        baseFare += Convert.ToDouble(solution.SelectSingleNode("chdFare").InnerText) * request.ChildCount;
                        baseFare += Convert.ToDouble(solution.SelectSingleNode("infFare").InnerText) * request.InfantCount;
                        

                        double tax = (Convert.ToDouble(solution.SelectSingleNode("adtTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * request.AdultCount;
                        tax += (Convert.ToDouble(solution.SelectSingleNode("chdTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * request.ChildCount;
                        tax += Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) * request.InfantCount;
                        

                        result.Price.BaseFare = (decimal)(baseFare * (double)rateOfExchange);
                        result.Price.Tax = (decimal)(tax * (double)rateOfExchange);
                        result.Price.PublishedFare = result.Price.BaseFare;
                        result.Price.Currency = agentBaseCurrency;
                        result.Price.SupplierCurrency = currency;
                        result.Price.SupplierPrice = (decimal)(baseFare + tax);
                        result.TotalFare = Math.Round((baseFare + tax) * (double)rateOfExchange, decimalValue);
                        result.Tax = tax * (double)rateOfExchange;
                        result.BaseFare = baseFare * (double)rateOfExchange;

                        if (request.ChildCount == 0)
                        {
                            result.FareBreakdown = new Fare[1];
                        }
                        else
                        {
                            result.FareBreakdown = new Fare[2];
                        }
                        result.FareBreakdown[0] = new Fare();
                        result.FareBreakdown[0].BaseFare = Convert.ToDouble(solution.SelectSingleNode("adtFare").InnerText) * (double)rateOfExchange * request.AdultCount;
                        result.FareBreakdown[0].TotalFare = (Convert.ToDouble(solution.SelectSingleNode("adtFare").InnerText) + Convert.ToDouble(solution.SelectSingleNode("adtTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * (double)rateOfExchange * request.AdultCount;
                        result.FareBreakdown[0].SupplierFare = (Convert.ToDouble(solution.SelectSingleNode("adtFare").InnerText) + Convert.ToDouble(solution.SelectSingleNode("adtTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * request.AdultCount;
                        result.FareBreakdown[0].SupplierTax = Convert.ToDouble(solution.SelectSingleNode("adtTax").InnerText) * request.AdultCount;
                        result.FareBreakdown[0].PassengerType = PassengerType.Adult;
                        result.FareBreakdown[0].PassengerCount = request.AdultCount;


                        if (request.ChildCount > 0)
                        {
                            result.FareBreakdown[1] = new Fare();
                            result.FareBreakdown[1].BaseFare = Convert.ToDouble(solution.SelectSingleNode("chdFare").InnerText) * (double)rateOfExchange * request.ChildCount;
                            result.FareBreakdown[1].TotalFare = (Convert.ToDouble(solution.SelectSingleNode("chdFare").InnerText) + Convert.ToDouble(solution.SelectSingleNode("chdTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * (double)rateOfExchange * request.ChildCount;
                            result.FareBreakdown[1].SupplierFare = (Convert.ToDouble(solution.SelectSingleNode("chdFare").InnerText) + Convert.ToDouble(solution.SelectSingleNode("chdTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * request.ChildCount;
                            result.FareBreakdown[1].SupplierTax = Convert.ToDouble(solution.SelectSingleNode("chdTax").InnerText) * request.ChildCount;
                            result.FareBreakdown[1].PassengerType = PassengerType.Child;
                            result.FareBreakdown[1].PassengerCount = request.ChildCount;
                        }

                        //if (request.ChildCount > 0 && request.InfantCount > 0)
                        //{
                        //    result.FareBreakdown[2] = new Fare();
                        //    result.FareBreakdown[2].BaseFare = Convert.ToDouble(solution.SelectSingleNode("infFare").InnerText) * (double)rateOfExchange * request.InfantCount;
                        //    result.FareBreakdown[2].TotalFare = result.FareBreakdown[2].BaseFare + (Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) * (double)rateOfExchange * request.InfantCount) + (Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText) * (double)rateOfExchange * request.InfantCount);
                        //    result.FareBreakdown[2].SupplierFare = (Convert.ToDouble(solution.SelectSingleNode("infFare").InnerText) + Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * request.InfantCount;
                        //    result.FareBreakdown[2].SupplierTax = Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) * request.InfantCount;
                        //    result.FareBreakdown[2].PassengerType = PassengerType.Infant;
                        //    result.FareBreakdown[2].PassengerCount = request.InfantCount;
                        //}
                        //else if (request.ChildCount == 0 && request.InfantCount > 0)
                        //{
                        //    result.FareBreakdown[1] = new Fare();
                        //    result.FareBreakdown[1].BaseFare = Convert.ToDouble(solution.SelectSingleNode("infFare").InnerText) * (double)rateOfExchange * request.InfantCount;
                        //    result.FareBreakdown[1].TotalFare = result.FareBreakdown[1].BaseFare + (Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) * (double)rateOfExchange * request.InfantCount) + (Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText) * (double)rateOfExchange * request.InfantCount);
                        //    result.FareBreakdown[1].SupplierFare = (Convert.ToDouble(solution.SelectSingleNode("infFare").InnerText) + Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * request.InfantCount;
                        //    result.FareBreakdown[1].SupplierTax = Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) * request.InfantCount;
                        //    result.FareBreakdown[1].PassengerType = PassengerType.Infant;
                        //    result.FareBreakdown[1].PassengerCount = request.InfantCount;
                        //}

                        if (request.Type == SearchType.OneWay )
                        {
                            result.Flights = new FlightInfo[1][];
                        }
                        else if (request.Type == SearchType.Return || request.Type == SearchType.MultiWay)
                        {
                            result.Flights = new FlightInfo[2][];
                        }

                        foreach (XmlNode flight in Flights)
                        {
                            if (flight.SelectSingleNode("flightId").InnerText == solution.SelectSingleNode("journeys/journey_0").InnerText)
                            {
                                int segmentCount = (flight.SelectNodes("segmengtIds").Count);
                                result.Flights[0] = new FlightInfo[segmentCount];

                                XmlNodeList flightSegments = flight.SelectNodes("segmengtIds");
                                List<FlightInfo> outboundSegments = new List<FlightInfo>();

                                foreach (XmlNode fs in flightSegments)
                                {
                                    foreach (XmlNode segment in Segments)
                                    {
                                        if (segment.SelectSingleNode("segmentId").InnerText == fs.InnerText)
                                        {
                                            FlightInfo fi = new FlightInfo();
                                            fi.Airline = segment.SelectSingleNode("airline").InnerText;
                                            fi.AccumulatedDuration = new TimeSpan(0, Convert.ToInt16(segment.SelectSingleNode("flightTime").InnerText), 0);
                                            fi.ArrivalTime = Convert.ToDateTime(segment.SelectSingleNode("strArrivalDate").InnerText + " " + segment.SelectSingleNode("strArrivalTime").InnerText);
                                            fi.ArrTerminal = segment.SelectSingleNode("arrivalTerminal") != null ? segment.SelectSingleNode("arrivalTerminal").InnerText : "";
                                            fi.BookingClass = segment.SelectSingleNode("bookingCode").InnerText;
                                            fi.CabinClass = segment.SelectSingleNode("cabinClass").InnerText;
                                            fi.Craft = segment.SelectSingleNode("equipment") != null ? segment.SelectSingleNode("equipment").InnerText : "";
                                            fi.CreatedBy = appUserID;
                                            fi.CreatedOn = DateTime.Now;
                                            fi.DepartureTime = Convert.ToDateTime(segment.SelectSingleNode("strDepartureDate").InnerText + " " + segment.SelectSingleNode("strDepartureTime").InnerText);
                                            fi.DepTerminal = segment.SelectSingleNode("departureTerminal") != null ? segment.SelectSingleNode("departureTerminal").InnerText : "";
                                            fi.Destination = new Airport(segment.SelectSingleNode("arrival").InnerText);
                                            fi.Duration = fi.AccumulatedDuration;
                                            fi.ETicketEligible = true;
                                            fi.FlightNumber = segment.SelectSingleNode("flightNum").InnerText;
                                            fi.FlightStatus = FlightStatus.Confirmed;
                                            fi.Group = 0;
                                            fi.OperatingCarrier = segment.SelectSingleNode("opFltAirline") != null ? segment.SelectSingleNode("opFltAirline").InnerText : "";
                                            fi.Origin = new Airport(segment.SelectSingleNode("departure").InnerText);
                                            fi.Stops = segmentCount - 1;
                                            outboundSegments.Add(fi);
                                            break;
                                        }
                                    }
                                }

                                result.Flights[0] = outboundSegments.ToArray();

                            }
                            else if (request.Type != SearchType.OneWay && solution.SelectSingleNode("journeys/journey_1") != null && flight.SelectSingleNode("flightId").InnerText == solution.SelectSingleNode("journeys/journey_1").InnerText)
                            {
                                int segmentCount = (flight.SelectNodes("segmengtIds").Count);
                                result.Flights[1] = new FlightInfo[segmentCount];

                                XmlNodeList flightSegments = flight.SelectNodes("segmengtIds");
                                List<FlightInfo> inboundSegments = new List<FlightInfo>();

                                foreach (XmlNode fs in flightSegments)
                                {
                                    foreach (XmlNode segment in Segments)
                                    {
                                        if (segment.SelectSingleNode("segmentId").InnerText == fs.InnerText)
                                        {
                                            FlightInfo fi = new FlightInfo();
                                            fi.Airline = segment.SelectSingleNode("airline").InnerText;
                                            fi.AccumulatedDuration = new TimeSpan(0, Convert.ToInt16(segment.SelectSingleNode("flightTime").InnerText), 0);
                                            fi.ArrivalTime = Convert.ToDateTime(segment.SelectSingleNode("strArrivalDate").InnerText + " " + segment.SelectSingleNode("strArrivalTime").InnerText);
                                            fi.ArrTerminal = segment.SelectSingleNode("arrivalTerminal") != null ? segment.SelectSingleNode("arrivalTerminal").InnerText : "";
                                            fi.BookingClass = segment.SelectSingleNode("bookingCode").InnerText;
                                            fi.CabinClass = segment.SelectSingleNode("cabinClass").InnerText;
                                            fi.Craft = segment.SelectSingleNode("equipment") != null ? segment.SelectSingleNode("equipment").InnerText : "";
                                            fi.CreatedBy = appUserID;
                                            fi.CreatedOn = DateTime.Now;
                                            fi.DepartureTime = Convert.ToDateTime(segment.SelectSingleNode("strDepartureDate").InnerText + " " + segment.SelectSingleNode("strDepartureTime").InnerText);
                                            fi.DepTerminal = segment.SelectSingleNode("departureTerminal") != null ? segment.SelectSingleNode("departureTerminal").InnerText : "";
                                            fi.Destination = new Airport(segment.SelectSingleNode("arrival").InnerText);
                                            fi.Duration = fi.AccumulatedDuration;
                                            fi.ETicketEligible = true;
                                            fi.FlightNumber = segment.SelectSingleNode("flightNum").InnerText;
                                            fi.FlightStatus = FlightStatus.Confirmed;
                                            fi.Group = 1;
                                            fi.OperatingCarrier = segment.SelectSingleNode("opFltAirline") != null ? segment.SelectSingleNode("opFltAirline").InnerText : "";
                                            fi.Origin = new Airport(segment.SelectSingleNode("departure").InnerText);
                                            fi.Stops = segmentCount - 1;
                                            inboundSegments.Add(fi);
                                            break;
                                        }
                                    }
                                }
                                result.Flights[1] = inboundSegments.ToArray();

                            }
                        }

                        result.Airline = solution.SelectSingleNode("platingCarrier").InnerText;

                        /****************************************************************************
                         *      Filtering airlines based on stops selected in search
                         * **************************************************************************/

                        int stops = 0;
                        if (request.MaxStops == "-1" || request.MaxStops == "0")
                        {
                            stops = 0;
                        }
                        else if (request.MaxStops == "1")
                        {
                            stops = 1;
                        }
                        else if (request.MaxStops == "2")
                        {
                            stops = 2;
                        }
                        int onward = result.Flights[0].Length, returns = 0, onseg = 0, retseg = 0;

                        if(request.Type != SearchType.OneWay)
                        {
                            returns = result.Flights[1].Length;
                        }
                        if (stops > 0)
                        {
                            for (int i = 0; i < result.Flights.Length; i++)
                            {
                                for (int j = 0; j < result.Flights[i].Length; j++)
                                {
                                    if (result.Flights[i][j].Group == 0 && result.Flights[i][j].Stops == stops)
                                    {
                                        onseg++;
                                    }
                                    else if (result.Flights[i][j].Group == 1 && result.Flights[i][j].Stops == stops)
                                    {
                                        retseg++;
                                    }
                                }
                            }
                        }

                        //Check if both onward and return are having same connecting aiports, otherwise omit the result
                        bool OnwardConnection = false, returnConnection = false,SameConnection=false;
                        for (int i = 0; i < result.Flights.Length; i++)
                        {
                            for (int j = 0; j < result.Flights[i].Length; j++)
                            {
                                if (j > 0 && result.Flights[i][j - 1].Destination.AirportCode == result.Flights[i][j].Origin.AirportCode)
                                {
                                    if (i == 0)
                                    {
                                        OnwardConnection = true;
                                    }
                                    else
                                    {
                                        returnConnection = true;
                                    }
                                }
                                else if (result.Flights[i].Length == 1)//If there is no connection for Onward or return segment then they are same connection by default
                                {
                                    if (i == 0)
                                    {
                                        OnwardConnection = true;
                                    }
                                    else
                                    {
                                        returnConnection = true;
                                    }
                                }
                            }
                        }

                        if(request.Type != SearchType.OneWay && OnwardConnection && returnConnection)//If search type is not one way and onward & return are same connection
                        {
                            SameConnection = true;
                        }
                        else if(request.Type == SearchType.OneWay && OnwardConnection) //if search type is one way the by default same connection
                        {
                            SameConnection = true;
                        }

                        //If Single Stop or Two stops are selected and same segments are available then add result otherwise omit result
                        if (stops > 0 && SameConnection && onward == onseg && returns == retseg)
                        {
                            Results.Add(result);
                        }
                        else if(stops == 0 && SameConnection)
                        {
                            Results.Add(result);
                        }         
                    }//Solution Loop
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserID, "(PKFares)Failed to get Search Results. Reason : " + ex.ToString(), "", "PKFares");
            }

            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(SearchResult[]));
                StreamWriter sw = new StreamWriter(xmlLogPath + sessionID + "_" + appUserID + "_PKFareSearchResults_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml");
                ser.Serialize(sw, Results.ToArray());
                sw.Close();
                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.Normal, appUserID, "(PKFares)Failed to write Search Results. Reason : " + ex.ToString(), "", "PKFares");
            }

            return Results;
        }

        /// <summary>
        /// Method to confirm Pricing and availability for the result. 
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public SearchResult PrecisePricing(SearchRequest request, SearchResult result)
        {
            SearchResult pricedResult = result;   
            
            try
            {
                string jsonRequest = GeneratePrecisePricingRequest(request, result);
                
                try
                {
                    string filePath = xmlLogPath + sessionID + "_" + appUserID + "_PrecisePricingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    dynamic pricingRequest = js.Deserialize<dynamic>(jsonRequest);
                    string rsp = JsonConvert.SerializeObject(pricingRequest, Newtonsoft.Json.Formatting.Indented);
                    StreamWriter sw = new StreamWriter(filePath + ".json");
                    sw.Write(rsp);
                    sw.Close();

                    SerializeJSONToXML(rsp, filePath + ".xml", "PrecisePricingRequest");
                }
                catch { }
                string base64String = EncodeTo64(jsonRequest);                

                string response = GetResponse(base64String, "PrecisePricing");

                response = response.Replace("\"1\":", "\"journey_0\":");
                response = response.Replace("\"2\":", "\"journey_1\":");
                
                string resfilePath = xmlLogPath + sessionID + "_" + appUserID + "_PrecisePricingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                try
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    dynamic pricingResponse = js.Deserialize<dynamic>(response);
                    string rsp = JsonConvert.SerializeObject(pricingResponse, Newtonsoft.Json.Formatting.Indented);
                    StreamWriter sw = new StreamWriter(resfilePath + ".json");
                    sw.Write(rsp);
                    sw.Close();

                    SerializeJSONToXML(rsp, resfilePath + ".xml", "PrecisePricingResponse");
                    
                }
                catch(Exception ex) { Audit.Add(EventType.Exception, Severity.High, appUserID, "(PKFARES)Failed to serialize PrecisePricing Response. Reason :" + ex.ToString(), "", "PKFares"); }

                if (!string.IsNullOrEmpty(response) && response.Length > 0)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(resfilePath + ".xml");

                    XmlNode errorNode = doc.SelectSingleNode("PrecisePricingResponse/errorMsg");

                    if (errorNode != null && errorNode.InnerText.ToLower() == "ok")
                    {
                        XmlNode solution = doc.SelectSingleNode("PrecisePricingResponse/data/solution");
                        XmlNodeList Flights = doc.SelectNodes("PrecisePricingResponse/data/flights");
                        XmlNodeList Segments = doc.SelectNodes("PrecisePricingResponse/data/segments");

                        if (solution != null && Flights != null && Segments != null)
                        {
                            string currency = solution.SelectSingleNode("currency").InnerText;
                            pricedResult.ResultBookingSource = BookingSource.PKFares;
                            if (exchangeRates.ContainsKey(currency))
                            {
                                rateOfExchange = exchangeRates[currency];
                            }
                            
                            pricedResult.Price.AccPriceType = PriceType.PublishedFare;
                            pricedResult.Price.RateOfExchange = rateOfExchange;
                            pricedResult.FareType = "PUB";

                            double baseFare = Convert.ToDouble(solution.SelectSingleNode("adtFare").InnerText) * request.AdultCount;
                            baseFare += Convert.ToDouble(solution.SelectSingleNode("chdFare").InnerText) * request.ChildCount;
                            baseFare += Convert.ToDouble(solution.SelectSingleNode("infFare").InnerText) * request.InfantCount;
                            //baseFare = baseFare * (double)rateOfExchange;

                            double tax = (Convert.ToDouble(solution.SelectSingleNode("adtTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * request.AdultCount;
                            tax += (Convert.ToDouble(solution.SelectSingleNode("chdTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * request.ChildCount;
                            tax += Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) * request.InfantCount;
                            //tax = tax * (double)rateOfExchange;

                            pricedResult.Currency = agentBaseCurrency;
                            pricedResult.Price.BaseFare = (decimal)(baseFare * (double)rateOfExchange);
                            pricedResult.Price.Tax = (decimal)(tax * (double)rateOfExchange);
                            pricedResult.Price.PublishedFare = pricedResult.Price.BaseFare;
                            pricedResult.Price.Currency = agentBaseCurrency;
                            pricedResult.Price.SupplierCurrency = currency;
                            pricedResult.Price.SupplierPrice = (decimal)(baseFare + tax);
                            pricedResult.TotalFare = (baseFare + tax) * (double)rateOfExchange;
                            pricedResult.Tax = tax * (double)rateOfExchange;
                            pricedResult.BaseFare = baseFare * (double)rateOfExchange;

                            if (request.ChildCount == 0)
                            {
                                pricedResult.FareBreakdown = new Fare[1];
                            }
                            else
                            {
                                pricedResult.FareBreakdown = new Fare[2];
                            }

                            pricedResult.FareBreakdown[0] = new Fare();
                            pricedResult.FareBreakdown[0].BaseFare = Convert.ToDouble(solution.SelectSingleNode("adtFare").InnerText) * (double)rateOfExchange * request.AdultCount;
                            pricedResult.FareBreakdown[0].TotalFare = (Convert.ToDouble(solution.SelectSingleNode("adtFare").InnerText) + Convert.ToDouble(solution.SelectSingleNode("adtTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * (double)rateOfExchange * request.AdultCount;
                            pricedResult.FareBreakdown[0].SupplierFare = (Convert.ToDouble(solution.SelectSingleNode("adtFare").InnerText) + Convert.ToDouble(solution.SelectSingleNode("adtTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * request.AdultCount;
                            pricedResult.FareBreakdown[0].SupplierTax = Convert.ToDouble(solution.SelectSingleNode("adtTax").InnerText) * request.AdultCount;
                            pricedResult.FareBreakdown[0].PassengerType = PassengerType.Adult;
                            pricedResult.FareBreakdown[0].PassengerCount = request.AdultCount;


                            if (request.ChildCount > 0)
                            {
                                pricedResult.FareBreakdown[1] = new Fare();
                                pricedResult.FareBreakdown[1].BaseFare = Convert.ToDouble(solution.SelectSingleNode("chdFare").InnerText) * (double)rateOfExchange * request.ChildCount;
                                pricedResult.FareBreakdown[1].TotalFare = (Convert.ToDouble(solution.SelectSingleNode("chdFare").InnerText) + Convert.ToDouble(solution.SelectSingleNode("chdTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * (double)rateOfExchange * request.ChildCount;
                                pricedResult.FareBreakdown[1].SupplierFare = (Convert.ToDouble(solution.SelectSingleNode("chdFare").InnerText) + Convert.ToDouble(solution.SelectSingleNode("chdTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * request.ChildCount;
                                pricedResult.FareBreakdown[1].SupplierTax = Convert.ToDouble(solution.SelectSingleNode("chdTax").InnerText) * request.ChildCount;
                                pricedResult.FareBreakdown[1].PassengerType = PassengerType.Child;
                                pricedResult.FareBreakdown[1].PassengerCount = request.ChildCount;
                            }

                            //if (request.ChildCount > 0 && request.InfantCount > 0)
                            //{
                            //    pricedResult.FareBreakdown[2] = new Fare();
                            //    pricedResult.FareBreakdown[2].BaseFare = Convert.ToDouble(solution.SelectSingleNode("infFare").InnerText) * (double)rateOfExchange * request.InfantCount;
                            //    pricedResult.FareBreakdown[2].TotalFare = pricedResult.FareBreakdown[2].BaseFare + (Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) * (double)rateOfExchange * request.InfantCount) + (Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText) * (double)rateOfExchange * request.InfantCount);
                            //    pricedResult.FareBreakdown[2].SupplierFare = (Convert.ToDouble(solution.SelectSingleNode("infFare").InnerText) + Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * request.InfantCount;
                            //    pricedResult.FareBreakdown[2].SupplierTax = Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) * request.InfantCount;
                            //    pricedResult.FareBreakdown[2].PassengerType = PassengerType.Infant;
                            //    pricedResult.FareBreakdown[2].PassengerCount = request.InfantCount;
                            //}
                            //else if (request.ChildCount == 0 && request.InfantCount > 0)
                            //{
                            //    pricedResult.FareBreakdown[1] = new Fare();
                            //    pricedResult.FareBreakdown[1].BaseFare = Convert.ToDouble(solution.SelectSingleNode("infFare").InnerText) * (double)rateOfExchange * request.InfantCount;
                            //    pricedResult.FareBreakdown[1].TotalFare = pricedResult.FareBreakdown[1].BaseFare + (Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) * (double)rateOfExchange * request.InfantCount) + (Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText) * (double)rateOfExchange * request.InfantCount);
                            //    pricedResult.FareBreakdown[1].SupplierFare = (Convert.ToDouble(solution.SelectSingleNode("infFare").InnerText) + Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) + Convert.ToDouble(solution.SelectSingleNode("tktFee").InnerText)) * request.InfantCount;
                            //    pricedResult.FareBreakdown[1].SupplierTax = Convert.ToDouble(solution.SelectSingleNode("infTax").InnerText) * request.InfantCount;
                            //    pricedResult.FareBreakdown[1].PassengerType = PassengerType.Infant;
                            //    pricedResult.FareBreakdown[1].PassengerCount = request.InfantCount;
                            //}

                            pricedResult.BaggageIncludedInFare = solution.SelectSingleNode("baggages/journey_0/baggage").InnerText;
                            if (request.Type == SearchType.Return && solution.SelectSingleNode("baggages/journey_1") != null)
                            {
                                pricedResult.BaggageIncludedInFare += "," + solution.SelectSingleNode("baggages/journey_1/baggage").InnerText;
                            }

                            foreach (XmlNode flight in Flights)
                            {
                                if (flight.SelectSingleNode("flightId").InnerText == solution.SelectSingleNode("journeys/journey_0").InnerText)
                                {
                                    int segmentCount = (flight.SelectNodes("segmengtIds").Count);
                                    pricedResult.Flights[0] = new FlightInfo[segmentCount];

                                    XmlNodeList flightSegments = flight.SelectNodes("segmengtIds");
                                    List<FlightInfo> outboundSegments = new List<FlightInfo>();

                                    foreach (XmlNode fs in flightSegments)
                                    {
                                        foreach (XmlNode segment in Segments)
                                        {
                                            if (segment.SelectSingleNode("segmentId").InnerText == fs.InnerText)
                                            {
                                                FlightInfo fi = new FlightInfo();
                                                fi.Airline = segment.SelectSingleNode("airline").InnerText;
                                                fi.AccumulatedDuration = new TimeSpan(0, Convert.ToInt16(segment.SelectSingleNode("flightTime").InnerText), 0);
                                                fi.ArrivalTime = Convert.ToDateTime(segment.SelectSingleNode("strArrivalDate").InnerText + " " + segment.SelectSingleNode("strArrivalTime").InnerText);
                                                fi.ArrTerminal = segment.SelectSingleNode("arrivalTerminal") != null ? segment.SelectSingleNode("arrivalTerminal").InnerText : "";
                                                fi.BookingClass = segment.SelectSingleNode("bookingCode").InnerText;
                                                fi.CabinClass = segment.SelectSingleNode("cabinClass").InnerText;
                                                fi.Craft = segment.SelectSingleNode("equipment") != null ? segment.SelectSingleNode("equipment").InnerText : "";
                                                fi.CreatedBy = appUserID;
                                                fi.CreatedOn = DateTime.Now;
                                                fi.DepartureTime = Convert.ToDateTime(segment.SelectSingleNode("strDepartureDate").InnerText + " " + segment.SelectSingleNode("strDepartureTime").InnerText);
                                                fi.DepTerminal = segment.SelectSingleNode("departureTerminal") != null ? segment.SelectSingleNode("departureTerminal").InnerText : "";
                                                fi.Destination = new Airport(segment.SelectSingleNode("arrival").InnerText);
                                                fi.Duration = fi.AccumulatedDuration;
                                                fi.ETicketEligible = true;
                                                fi.FlightNumber = segment.SelectSingleNode("flightNum").InnerText;
                                                fi.FlightStatus = FlightStatus.Confirmed;
                                                fi.Group = 0;
                                                fi.OperatingCarrier = segment.SelectSingleNode("opFltAirline") != null ? segment.SelectSingleNode("opFltAirline").InnerText : "";
                                                fi.Origin = new Airport(segment.SelectSingleNode("departure").InnerText);
                                                fi.Stops = segmentCount - 1;
                                                outboundSegments.Add(fi);
                                                break;
                                            }
                                        }
                                    }

                                    pricedResult.Flights[0] = outboundSegments.ToArray();

                                }
                                else if (solution.SelectSingleNode("journeys/journey_1") != null && flight.SelectSingleNode("flightId").InnerText == solution.SelectSingleNode("journeys/journey_1").InnerText)
                                {
                                    int segmentCount = (flight.SelectNodes("segmengtIds").Count);
                                    pricedResult.Flights[1] = new FlightInfo[segmentCount];

                                    XmlNodeList flightSegments = flight.SelectNodes("segmengtIds");
                                    List<FlightInfo> inboundSegments = new List<FlightInfo>();

                                    foreach (XmlNode fs in flightSegments)
                                    {
                                        foreach (XmlNode segment in Segments)
                                        {
                                            if (segment.SelectSingleNode("segmentId").InnerText == fs.InnerText)
                                            {
                                                FlightInfo fi = new FlightInfo();
                                                fi.Airline = segment.SelectSingleNode("airline").InnerText;
                                                fi.AccumulatedDuration = new TimeSpan(0, Convert.ToInt16(segment.SelectSingleNode("flightTime").InnerText), 0);
                                                fi.ArrivalTime = Convert.ToDateTime(segment.SelectSingleNode("strArrivalDate").InnerText + " " + segment.SelectSingleNode("strArrivalTime").InnerText);
                                                fi.ArrTerminal = segment.SelectSingleNode("arrivalTerminal") != null ? segment.SelectSingleNode("arrivalTerminal").InnerText : "";
                                                fi.BookingClass = segment.SelectSingleNode("bookingCode").InnerText;
                                                fi.CabinClass = segment.SelectSingleNode("cabinClass").InnerText;
                                                fi.Craft = segment.SelectSingleNode("equipment") != null ? segment.SelectSingleNode("equipment").InnerText : "";
                                                fi.CreatedBy = appUserID;
                                                fi.CreatedOn = DateTime.Now;
                                                fi.DepartureTime = Convert.ToDateTime(segment.SelectSingleNode("strDepartureDate").InnerText + " " + segment.SelectSingleNode("strDepartureTime").InnerText);
                                                fi.DepTerminal = segment.SelectSingleNode("departureTerminal") != null ? segment.SelectSingleNode("departureTerminal").InnerText : "";
                                                fi.Destination = new Airport(segment.SelectSingleNode("arrival").InnerText);
                                                fi.Duration = fi.AccumulatedDuration;
                                                fi.ETicketEligible = true;
                                                fi.FlightNumber = segment.SelectSingleNode("flightNum").InnerText;
                                                fi.FlightStatus = FlightStatus.Confirmed;
                                                fi.Group = 1;
                                                fi.OperatingCarrier = segment.SelectSingleNode("opFltAirline") != null ? segment.SelectSingleNode("opFltAirline").InnerText : "";
                                                fi.Origin = new Airport(segment.SelectSingleNode("departure").InnerText);
                                                fi.Stops = segmentCount - 1;
                                                inboundSegments.Add(fi);
                                                break;
                                            }
                                        }
                                    }
                                    pricedResult.Flights[1] = inboundSegments.ToArray();
                                }
                            }
                        }
                        else //If there is no proper response set baggage default.
                        {
                            if (request.Type == SearchType.OneWay)
                            {
                                pricedResult.BaggageIncludedInFare = "Airline Norms";
                            }
                            else
                            {
                                pricedResult.BaggageIncludedInFare = "Airline Norms, Airline Norms";
                            }
                        }
                    }
                    else
                    {
                        if (request.Type == SearchType.OneWay)
                        {
                            pricedResult.BaggageIncludedInFare = "Airline Norms";
                        }
                        else
                        {
                            pricedResult.BaggageIncludedInFare = "Airline Norms, Airline Norms";
                        }
                        Audit.Add(EventType.RePrice, Severity.Normal, appUserID, "(PKFARES)Failed to Reprice. Reason : " + response + "", "PKFares");
                        throw new Exception(errorNode.InnerText);
                    }
                }
                else //If there is no proper response set baggage default.
                {
                    if (request.Type == SearchType.OneWay)
                    {
                        pricedResult.BaggageIncludedInFare = "Airline Norms";
                    }
                    else
                    {
                        pricedResult.BaggageIncludedInFare = "Airline Norms, Airline Norms";
                    }
                    throw new Exception("Fare not found");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.RePrice, Severity.High, appUserID, "(PKFARES)Failed to Reprice. Reason :" + ex.ToString(), "");
                //Throwing exception to redirect to Errorpage in Passenger page
                throw new Exception("Failed from PKFares", ex);
            }

            return pricedResult;
        }

        /// <summary>
        /// Method to get Fare rule details
        /// </summary>
        /// <param name="request"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public List<FareRule> Penalty(SearchRequest request, SearchResult result)
        {
            List<FareRule> FareRules = new List<FareRule>();

            try
            {
                if (result.FareRules != null && result.FareRules.Count > 0)
                {
                    FareRules = result.FareRules;
                }
                else
                {
                    string jsonRequest = GeneratePenaltyRequest(request, result);
                    string filePath = xmlLogPath + sessionID + "_" + appUserID + "_PenaltyRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                    try
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        dynamic penaltyRequest = js.Deserialize<dynamic>(jsonRequest);
                        string rsp = JsonConvert.SerializeObject(penaltyRequest, Newtonsoft.Json.Formatting.Indented);
                        StreamWriter sw = new StreamWriter(filePath + ".json");
                        sw.Write(rsp);
                        sw.Close();

                        SerializeJSONToXML(rsp, filePath + ".xml", "PenaltyRequest");
                    }
                    catch { }
                    string base64String = EncodeTo64(jsonRequest);

                    string response = GetResponse(base64String,"Penalty");
                    
                    filePath = xmlLogPath + sessionID + "_" + appUserID + "_PenaltyResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                    try
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        dynamic penaltyResponse = js.Deserialize<dynamic>(response);
                        string rsp = JsonConvert.SerializeObject(penaltyResponse, Newtonsoft.Json.Formatting.Indented);
                        StreamWriter sw = new StreamWriter(filePath + ".json");
                        sw.Write(rsp);
                        sw.Close();

                        SerializeJSONToXML(rsp, filePath + ".xml", "PenaltyResponse");                        
                    }
                    catch(Exception ex) { Audit.Add(EventType.Exception, Severity.High, appUserID, "(PKFares)Failed to write Penalty Response xml. Reason : " + ex.ToString(), "", "PKFares"); }

                    if (response != null && response.Length > 0)
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(filePath + ".xml");

                        XmlNode errorNode = doc.SelectSingleNode("PenaltyResponse/errorMsg");

                        if (errorNode != null && errorNode.InnerText.ToLower() == "ok")
                        {
                            if (doc.SelectSingleNode("PenaltyResponse/data") != null && doc.SelectSingleNode("PenaltyResponse/data/penalties") != null)
                            {
                                FareRule rule = new FareRule();
                                rule.Airline = result.Flights[0][0].Airline;
                                rule.Destination = request.Segments[0].Destination;
                                rule.FareBasisCode = "";
                                rule.FareInfoRef = "";
                                rule.FareRestriction = "";
                                rule.FareRuleKeyValue = "";
                                rule.Origin = request.Segments[0].Origin;
                                rule.FareRuleDetail = doc.SelectSingleNode("PenaltyResponse/data/penalties").InnerText;
                                FareRules.Add(rule);
                                result.FareRules = FareRules;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GetFareRule, Severity.High, appUserID, "(PKFares)Failed to Retrieve Fare Rules. Reason : " + ex.ToString(), "");
            }

            return FareRules;
        }
            
        /// <summary>
        /// Method to raise booking request. Two request will be raised in this method.
        /// 1. PreciseBooking request 
        /// 2. OrderPricing request
        /// If both are successful then only we need to consider Booking is successful otherwise failure
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public BookingResponse Book(FlightItinerary itinerary)
        {
            BookingResponse bookingResponse = new BookingResponse();

            try
            {
                string jsonRequest = GeneratePreciseBookingRequest(itinerary);
                string filePath = xmlLogPath + sessionID + "_" + appUserID + "_PreciseBookingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                try
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    dynamic bookingRequest = js.Deserialize<dynamic>(jsonRequest);
                    string rsp = JsonConvert.SerializeObject(bookingRequest, Newtonsoft.Json.Formatting.Indented);
                    StreamWriter sw = new StreamWriter(filePath + ".json");
                    sw.Write(rsp);
                    sw.Close();

                    SerializeJSONToXML(rsp, filePath + ".xml", "PreciseBookingRequest");
                }
                catch(Exception ex) { Audit.Add(EventType.Book, Severity.High, appUserID, "(PKFARES)Failed to serialize Precise Booking Request. Reason : " + ex.ToString(), ""); }
                string base64String = EncodeTo64(jsonRequest);

                string response = GetResponse(base64String, "PreciseBooking");

                filePath = xmlLogPath + sessionID + "_" + appUserID + "_PreciseBookingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                try
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    dynamic bookResponse = js.Deserialize<dynamic>(response);
                    string rsp = JsonConvert.SerializeObject(bookResponse, Newtonsoft.Json.Formatting.Indented);
                    StreamWriter sw = new StreamWriter(filePath + ".json");
                    sw.Write(rsp);
                    sw.Close();

                    SerializeJSONToXML(rsp, filePath + ".xml", "PreciseBookingResponse");
                }
                catch(Exception ex) { Audit.Add(EventType.Book, Severity.High, appUserID, "(PKFARES)Failed to serialize Precise Booking Response. Reason : " + ex.ToString(), ""); }

                if (!string.IsNullOrEmpty(response) && response.Length > 0)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(filePath + ".xml");

                    XmlNode errorNode = doc.SelectSingleNode("PreciseBookingResponse/errorMsg");

                    if (errorNode != null && errorNode.InnerText.ToLower() == "ok")
                    {
                        if (doc.SelectSingleNode("PreciseBookingResponse/data") != null && doc.SelectSingleNode("PreciseBookingResponse/data/orderNum") != null && doc.SelectSingleNode("PreciseBookingResponse/data/pnr") != null)
                        {
                            itinerary.TicketAdvisory = doc.SelectSingleNode("PreciseBookingResponse/data/orderNum").InnerText;
                            itinerary.PNR = doc.SelectSingleNode("PreciseBookingResponse/data/pnr").InnerText;

                            XmlNode solution = doc.SelectSingleNode("PreciseBookingResponse/data/solution");
                            XmlNodeList Flights = doc.SelectNodes("PreciseBookingResponse/data/flights");
                            XmlNodeList Segments = doc.SelectNodes("PreciseBookingResponse/data/segments");

                            List<FlightInfo> FlightSegments = new List<FlightInfo>(itinerary.Segments);

                            #region Update Segment info
                            foreach (XmlNode flight in Flights)
                            {
                                if (flight.SelectSingleNode("flightId").InnerText == solution.SelectSingleNode("journeys/journey_0").InnerText)
                                {
                                    int segmentCount = (flight.SelectNodes("segmengtIds").Count);

                                    XmlNodeList flightSegments = flight.SelectNodes("segmengtIds");
                                    List<FlightInfo> outboundSegments = new List<FlightInfo>();

                                    foreach (XmlNode fs in flightSegments)
                                    {
                                        foreach (XmlNode segment in Segments)
                                        {
                                            if (segment.SelectSingleNode("segmentId").InnerText == fs.InnerText)
                                            {
                                                FlightInfo fi = FlightSegments.Find(delegate (FlightInfo fis) { return fis.FlightNumber == segment.SelectSingleNode("flightNum").InnerText && fis.Airline == segment.SelectSingleNode("airline").InnerText; });
                                                fi.Airline = segment.SelectSingleNode("airline").InnerText;
                                                fi.AccumulatedDuration = new TimeSpan(0, Convert.ToInt16(segment.SelectSingleNode("flightTime").InnerText), 0);
                                                fi.ArrivalTime = Convert.ToDateTime(segment.SelectSingleNode("strArrivalDate").InnerText + " " + segment.SelectSingleNode("strArrivalTime").InnerText);
                                                fi.ArrTerminal = segment.SelectSingleNode("arrivalTerminal") != null ? segment.SelectSingleNode("arrivalTerminal").InnerText : "";
                                                fi.BookingClass = segment.SelectSingleNode("bookingCode").InnerText;
                                                fi.CabinClass = segment.SelectSingleNode("cabinClass").InnerText;
                                                fi.Craft = segment.SelectSingleNode("equipment") != null ? segment.SelectSingleNode("equipment").InnerText : "";
                                                fi.CreatedBy = appUserID;
                                                fi.CreatedOn = DateTime.Now;
                                                fi.DepartureTime = Convert.ToDateTime(segment.SelectSingleNode("strDepartureDate").InnerText + " " + segment.SelectSingleNode("strDepartureTime").InnerText);
                                                fi.DepTerminal = segment.SelectSingleNode("departureTerminal") != null ? segment.SelectSingleNode("departureTerminal").InnerText : "";
                                                fi.Destination = new Airport(segment.SelectSingleNode("arrival").InnerText);
                                                fi.Duration = fi.AccumulatedDuration;
                                                fi.ETicketEligible = true;
                                                fi.FlightNumber = segment.SelectSingleNode("flightNum").InnerText;
                                                fi.FlightStatus = FlightStatus.Confirmed;
                                                fi.Group = 0;
                                                fi.OperatingCarrier = segment.SelectSingleNode("opFltAirline") != null ? segment.SelectSingleNode("opFltAirline").InnerText : "";
                                                fi.Origin = new Airport(segment.SelectSingleNode("departure").InnerText);
                                                fi.Stops = segmentCount - 1;

                                                break;
                                            }
                                        }
                                    }
                                }
                                else if (solution.SelectSingleNode("journeys/journey_1") != null && flight.SelectSingleNode("flightId").InnerText == solution.SelectSingleNode("journeys/journey_1").InnerText)
                                {
                                    int segmentCount = (flight.SelectNodes("segmengtIds").Count);

                                    XmlNodeList flightSegments = flight.SelectNodes("segmengtIds");
                                    List<FlightInfo> inboundSegments = new List<FlightInfo>();

                                    foreach (XmlNode fs in flightSegments)
                                    {
                                        foreach (XmlNode segment in Segments)
                                        {
                                            if (segment.SelectSingleNode("segmentId").InnerText == fs.InnerText)
                                            {
                                                FlightInfo fi = FlightSegments.Find(delegate (FlightInfo fis) { return fis.FlightNumber == segment.SelectSingleNode("flightNum").InnerText && fis.Airline == segment.SelectSingleNode("airline").InnerText; });
                                                fi.Airline = segment.SelectSingleNode("airline").InnerText;
                                                fi.AccumulatedDuration = new TimeSpan(0, Convert.ToInt16(segment.SelectSingleNode("flightTime").InnerText), 0);
                                                fi.ArrivalTime = Convert.ToDateTime(segment.SelectSingleNode("strArrivalDate").InnerText + " " + segment.SelectSingleNode("strArrivalTime").InnerText);
                                                fi.ArrTerminal = segment.SelectSingleNode("arrivalTerminal") != null ? segment.SelectSingleNode("arrivalTerminal").InnerText : "";
                                                fi.BookingClass = segment.SelectSingleNode("bookingCode").InnerText;
                                                fi.CabinClass = segment.SelectSingleNode("cabinClass").InnerText;
                                                fi.Craft = segment.SelectSingleNode("equipment") != null ? segment.SelectSingleNode("equipment").InnerText : "";
                                                fi.CreatedBy = appUserID;
                                                fi.CreatedOn = DateTime.Now;
                                                fi.DepartureTime = Convert.ToDateTime(segment.SelectSingleNode("strDepartureDate").InnerText + " " + segment.SelectSingleNode("strDepartureTime").InnerText);
                                                fi.DepTerminal = segment.SelectSingleNode("departureTerminal") != null ? segment.SelectSingleNode("departureTerminal").InnerText : "";
                                                fi.Destination = new Airport(segment.SelectSingleNode("arrival").InnerText);
                                                fi.Duration = fi.AccumulatedDuration;
                                                fi.ETicketEligible = true;
                                                fi.FlightNumber = segment.SelectSingleNode("flightNum").InnerText;
                                                fi.FlightStatus = FlightStatus.Confirmed;
                                                fi.Group = 1;
                                                fi.OperatingCarrier = segment.SelectSingleNode("opFltAirline") != null ? segment.SelectSingleNode("opFltAirline").InnerText : "";
                                                fi.Origin = new Airport(segment.SelectSingleNode("departure").InnerText);
                                                fi.Stops = segmentCount - 1;
                                                break;
                                            }
                                        }
                                    }
                                }
                            } 
                            #endregion

                            if (itinerary.FareRules == null)
                            {
                                itinerary.FareRules = new List<FareRule>();
                            }

                            /****************************************************************************************************
                             *                  Check the price returned from PreciseBooking response                           * 
                             * **************************************************************************************************/

                            //Calculate the booking amount from Itinerary
                            decimal bookingAmount = 0m,apiBookingAmount=0m;
                            int adults = 0, childs = 0;
                           
                            //Consider Original Booking amount (adtFare+adtTax+tktFee) + (chdFare+chdTax+tktFee) for comparison
                            //SupplierFare contains Fare + Tax + tktFee

                            foreach (Fare fare in itinerary.TBOFareBreakdown)
                            {
                                bookingAmount += (decimal)(fare.SupplierFare);
                                switch (fare.PassengerType)
                                {
                                    case PassengerType.Adult:
                                        adults = fare.PassengerCount;
                                        break;
                                    case PassengerType.Child:
                                        childs = fare.PassengerCount;
                                        break;
                                }
                            }

                            //Multiply returned ((adtFare + adtTax + tktFee) * adults) + ((chdFare + chdTax + tktFee) * childs) with passenger count
                            apiBookingAmount = (Convert.ToDecimal(solution.SelectSingleNode("adtFare").InnerText) + Convert.ToDecimal(solution.SelectSingleNode("adtTax").InnerText) + Convert.ToDecimal(solution.SelectSingleNode("tktFee").InnerText)) * adults;
                            apiBookingAmount += (Convert.ToDecimal(solution.SelectSingleNode("chdFare").InnerText) + Convert.ToDecimal(solution.SelectSingleNode("chdTax").InnerText) + Convert.ToDecimal(solution.SelectSingleNode("tktFee").InnerText)) * childs;                           

                            //If both price are same then continue booking, otherwise stop and returned failed booking response
                            if (Math.Ceiling(bookingAmount) == Math.Ceiling(apiBookingAmount))
                            {
                                //Confirm Booking with Order Pricing 
                                jsonRequest = GenerateOrderPricingRequest(itinerary.TicketAdvisory);

                                filePath = xmlLogPath + sessionID + "_" + appUserID + "_OrderPricingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                                try
                                {
                                    JavaScriptSerializer js = new JavaScriptSerializer();
                                    dynamic orderPricingRequest = js.Deserialize<dynamic>(jsonRequest);
                                    string rsp = JsonConvert.SerializeObject(orderPricingRequest, Newtonsoft.Json.Formatting.Indented);
                                    StreamWriter sw = new StreamWriter(filePath + ".json");
                                    sw.Write(rsp);
                                    sw.Close();

                                    SerializeJSONToXML(rsp, filePath + ".xml", "OrderPricingResponse");
                                }
                                catch (Exception ex) { Audit.Add(EventType.Book, Severity.High, appUserID, "(PKFARES)Failed to serialize Order Pricing Request. Reason : " + ex.ToString(), ""); }
                                base64String = EncodeTo64(jsonRequest);

                                response = GetResponse(base64String, "OrderPricing");

                                filePath = xmlLogPath + sessionID + "_" + appUserID + "_OrderPricingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                                try
                                {
                                    JavaScriptSerializer js = new JavaScriptSerializer();
                                    dynamic orderPricingResponse = js.Deserialize<dynamic>(response);
                                    string rsp = JsonConvert.SerializeObject(orderPricingResponse, Newtonsoft.Json.Formatting.Indented);
                                    StreamWriter sw = new StreamWriter(filePath + ".json");
                                    sw.Write(rsp);
                                    sw.Close();

                                    SerializeJSONToXML(rsp, filePath + ".xml", "OrderPricingResponse");
                                }
                                catch (Exception ex) { Audit.Add(EventType.Book, Severity.High, appUserID, "(PKFARES)Failed to serialize Order Pricing Response. Reason : " + ex.ToString(), ""); }

                                if (!string.IsNullOrEmpty(response) && response.Length > 0)
                                {
                                    doc = new XmlDocument();
                                    doc.Load(filePath + ".xml");

                                    XmlNode errorMsg = doc.SelectSingleNode("OrderPricingResponse/errorMsg");

                                    if (errorMsg != null && errorMsg.InnerText.ToLower() == "ok")
                                    {
                                        if (doc.SelectSingleNode("OrderPricingResponse/data") != null && doc.SelectSingleNode("OrderPricingResponse/data/orderResult") != null && doc.SelectSingleNode("OrderPricingResponse/data/orderResult/orderNum") != null && doc.SelectSingleNode("OrderPricingResponse/data/orderResult/pnr") != null)
                                        {
                                            bookingResponse.PNR = itinerary.PNR;
                                            bookingResponse.Status = BookingResponseStatus.Successful;
                                            Audit.Add(EventType.Book, Severity.Normal, appUserID, "(PKFares)Booking Sucessful. PNR :" + itinerary.PNR + " Order Number:" + itinerary.TicketAdvisory, "");

                                            solution = doc.SelectSingleNode("OrderPricingResponse/data/solution");
                                            Flights = doc.SelectNodes("OrderPricingResponse/data/flights");
                                            Segments = doc.SelectNodes("OrderPricingResponse/data/segments");


                                            /*********************************************************************************************************************
                                             *                                  Update Segment information final time
                                             * *******************************************************************************************************************/
                                            #region Update Segment info
                                            foreach (XmlNode flight in Flights)                                            
                                            {
                                                if (flight.SelectSingleNode("flightId").InnerText == solution.SelectSingleNode("journeys/journey_0").InnerText)
                                                {
                                                    int segmentCount = (flight.SelectNodes("segmengtIds").Count);

                                                    XmlNodeList flightSegments = flight.SelectNodes("segmengtIds");
                                                    List<FlightInfo> outboundSegments = new List<FlightInfo>();

                                                    foreach (XmlNode fs in flightSegments)
                                                    {
                                                        foreach (XmlNode segment in Segments)
                                                        {
                                                            if (segment.SelectSingleNode("segmentId").InnerText == fs.InnerText)
                                                            {
                                                                FlightInfo fi = FlightSegments.Find(delegate (FlightInfo fis) { return fis.FlightNumber == segment.SelectSingleNode("flightNum").InnerText && fis.Airline == segment.SelectSingleNode("airline").InnerText; });
                                                                fi.Airline = segment.SelectSingleNode("airline").InnerText;
                                                                fi.AccumulatedDuration = new TimeSpan(0, Convert.ToInt16(segment.SelectSingleNode("flightTime").InnerText), 0);
                                                                fi.ArrivalTime = Convert.ToDateTime(segment.SelectSingleNode("strArrivalDate").InnerText + " " + segment.SelectSingleNode("strArrivalTime").InnerText);
                                                                fi.ArrTerminal = segment.SelectSingleNode("arrivalTerminal") != null ? segment.SelectSingleNode("arrivalTerminal").InnerText : "";
                                                                fi.BookingClass = segment.SelectSingleNode("bookingCode").InnerText;
                                                                fi.CabinClass = segment.SelectSingleNode("cabinClass").InnerText;
                                                                fi.Craft = segment.SelectSingleNode("equipment") != null ? segment.SelectSingleNode("equipment").InnerText : "";
                                                                fi.CreatedBy = appUserID;
                                                                fi.CreatedOn = DateTime.Now;
                                                                fi.DepartureTime = Convert.ToDateTime(segment.SelectSingleNode("strDepartureDate").InnerText + " " + segment.SelectSingleNode("strDepartureTime").InnerText);
                                                                fi.DepTerminal = segment.SelectSingleNode("departureTerminal") != null ? segment.SelectSingleNode("departureTerminal").InnerText : "";
                                                                fi.Destination = new Airport(segment.SelectSingleNode("arrival").InnerText);
                                                                fi.Duration = fi.AccumulatedDuration;
                                                                fi.ETicketEligible = true;
                                                                fi.FlightNumber = segment.SelectSingleNode("flightNum").InnerText;
                                                                fi.FlightStatus = FlightStatus.Confirmed;
                                                                fi.Group = 0;
                                                                fi.OperatingCarrier = segment.SelectSingleNode("opFltAirline") != null ? segment.SelectSingleNode("opFltAirline").InnerText : "";
                                                                fi.Origin = new Airport(segment.SelectSingleNode("departure").InnerText);
                                                                fi.Stops = segmentCount - 1;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                                else if (solution.SelectSingleNode("journeys/journey_1") != null && flight.SelectSingleNode("flightId").InnerText == solution.SelectSingleNode("journeys/journey_1").InnerText)
                                                {
                                                    int segmentCount = (flight.SelectNodes("segmengtIds").Count);

                                                    XmlNodeList flightSegments = flight.SelectNodes("segmengtIds");
                                                    List<FlightInfo> inboundSegments = new List<FlightInfo>();

                                                    foreach (XmlNode fs in flightSegments)
                                                    {
                                                        foreach (XmlNode segment in Segments)
                                                        {
                                                            if (segment.SelectSingleNode("segmentId").InnerText == fs.InnerText)
                                                            {
                                                                FlightInfo fi = FlightSegments.Find(delegate (FlightInfo fis) { return fis.FlightNumber == segment.SelectSingleNode("flightNum").InnerText && fis.Airline == segment.SelectSingleNode("airline").InnerText; });
                                                                fi.Airline = segment.SelectSingleNode("airline").InnerText;
                                                                fi.AccumulatedDuration = new TimeSpan(0, Convert.ToInt16(segment.SelectSingleNode("flightTime").InnerText), 0);
                                                                fi.ArrivalTime = Convert.ToDateTime(segment.SelectSingleNode("strArrivalDate").InnerText + " " + segment.SelectSingleNode("strArrivalTime").InnerText);
                                                                fi.ArrTerminal = segment.SelectSingleNode("arrivalTerminal") != null ? segment.SelectSingleNode("arrivalTerminal").InnerText: "";
                                                                fi.BookingClass = segment.SelectSingleNode("bookingCode").InnerText;
                                                                fi.CabinClass = segment.SelectSingleNode("cabinClass").InnerText;
                                                                fi.Craft = segment.SelectSingleNode("equipment") != null ? segment.SelectSingleNode("equipment").InnerText : "";
                                                                fi.CreatedBy = appUserID;
                                                                fi.CreatedOn = DateTime.Now;
                                                                fi.DepartureTime = Convert.ToDateTime(segment.SelectSingleNode("strDepartureDate").InnerText + " " + segment.SelectSingleNode("strDepartureTime").InnerText);
                                                                fi.DepTerminal = segment.SelectSingleNode("departureTerminal") != null ? segment.SelectSingleNode("departureTerminal").InnerText : "";
                                                                fi.Destination = new Airport(segment.SelectSingleNode("arrival").InnerText);
                                                                fi.Duration = fi.AccumulatedDuration;
                                                                fi.ETicketEligible = true;
                                                                fi.FlightNumber = segment.SelectSingleNode("flightNum").InnerText;
                                                                fi.FlightStatus = FlightStatus.Confirmed;
                                                                fi.Group = 1;
                                                                fi.OperatingCarrier = segment.SelectSingleNode("opFltAirline") != null ? segment.SelectSingleNode("opFltAirline").InnerText : "";
                                                                fi.Origin = new Airport(segment.SelectSingleNode("departure").InnerText);
                                                                fi.Stops = segmentCount - 1;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            } 
                                            #endregion

                                            /********************************************End updating Segment information*********************************************/

                                        }
                                        else
                                        {
                                            CancelBooking(itinerary.TicketAdvisory, itinerary.PNR);
                                            //TODO: Need to Cancel Order at this stage (later)
                                            bookingResponse.Status = BookingResponseStatus.Failed;
                                            bookingResponse.Error = "Failed to confirm the Booking";
                                            Audit.Add(EventType.Book, Severity.Normal, appUserID, "(PKFares)Cancelled the Booking. Failed to Confirm Booking. PNR :" + itinerary.PNR, "");
                                        }
                                    }
                                    else
                                    {
                                        //TODO: Need to Cancel Order at this stage (later)
                                        CancelBooking(itinerary.TicketAdvisory, itinerary.PNR);
                                        bookingResponse.Status = BookingResponseStatus.Failed;
                                        bookingResponse.Error = doc.SelectSingleNode("OrderPricingResponse/errorCode").InnerText + " " + doc.SelectSingleNode("OrderPricingResponse/errorMsg").InnerText;
                                        Audit.Add(EventType.Book, Severity.Normal, appUserID, "(PKFares)Cancelled the Booking. Error response returned from OrderPricing. PNR :" + itinerary.PNR + " Error:" + bookingResponse.Error, "");
                                    }
                                }
                                else
                                {
                                    CancelBooking(itinerary.TicketAdvisory, itinerary.PNR);
                                    bookingResponse.Status = BookingResponseStatus.Failed;
                                    bookingResponse.Error = doc.SelectSingleNode("OrderPricingResponse/errorCode").InnerText + " " + doc.SelectSingleNode("OrderPricingResponse/errorMsg").InnerText;
                                    Audit.Add(EventType.Book, Severity.Normal, appUserID, "(PKFares)Cancelled the Booking. Failed to get response from OrderPricing. PNR :" + itinerary.PNR, "");
                                }
                            }
                            else
                            {
                                CancelBooking(itinerary.TicketAdvisory, itinerary.PNR);
                                bookingResponse.Status = BookingResponseStatus.Failed;
                                bookingResponse.Error = "Price has been changed for this Itinerary!";
                                Audit.Add(EventType.Book, Severity.Normal, appUserID, "(PKFares)Cancelled the Booking. Price has been changed for this Itinerary! PNR :" + itinerary.PNR + " BookingAmt: " + bookingAmount + " ApiAmount: " + apiBookingAmount, "");
                            }
                        }
                        else
                        {
                            bookingResponse.Status = BookingResponseStatus.Failed;
                            bookingResponse.Error = doc.SelectSingleNode("PreciseBookingResponse/errorCode").InnerText + " " + doc.SelectSingleNode("PreciseBookingResponse/errorMsg").InnerText;
                        }
                    }
                    else
                    {
                        bookingResponse.Status = BookingResponseStatus.Failed;
                        bookingResponse.Error = doc.SelectSingleNode("PreciseBookingResponse/errorCode").InnerText + " " + doc.SelectSingleNode("PreciseBookingResponse/errorMsg").InnerText;
                    }
                }
                else
                {
                    bookingResponse.Status = BookingResponseStatus.Failed;
                    bookingResponse.Error = "Failed to get Booking Response";
                }
            }
            catch (Exception ex)
            {
                bookingResponse.Status = BookingResponseStatus.Failed;
                bookingResponse.Error = ex.Message;

                Audit.Add(EventType.Book, Severity.High, appUserID, "(PKFARES)Failed to Book. Reason : " + ex.ToString(), "");
            }

            return bookingResponse;
        }

        /// <summary>
        /// Raises request for Ticketing. Ticket numbers will not be returned. 
        /// We need to raise TicketNumPush request for getting Ticket numbers.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public TicketingResponse Ticket(FlightItinerary itinerary, ref Ticket[] tickets)
        {
            TicketingResponse ticketResponse = new TicketingResponse();

            try
            {
                FlightPassenger pax = itinerary.Passenger[0];
                string jsonRequest = GenerateTicketingRequest("b2bsupport@cozmotravel.com", pax.LastName + "/" + pax.FirstName, itinerary.TicketAdvisory, itinerary.Passenger[0].CellPhone, itinerary.PNR);

                string filePath = xmlLogPath + sessionID + "_" + appUserID + "_TicketingRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                try
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    dynamic ticketingRequest = js.Deserialize<dynamic>(jsonRequest);
                    string rsp = JsonConvert.SerializeObject(ticketingRequest, Newtonsoft.Json.Formatting.Indented);
                    StreamWriter sw = new StreamWriter(filePath + ".json");
                    sw.Write(rsp);
                    sw.Close();

                    SerializeJSONToXML(rsp, filePath + ".xml", "TicketingRequest");
                }
                catch (Exception ex) { Audit.Add(EventType.Book, Severity.High, appUserID, "(PKFARES)Failed to serialize Ticketing Request. Reason : " + ex.ToString(), ""); }

                string base64 = EncodeTo64(jsonRequest);

                string response = GetResponse(base64, "Ticketing");

                filePath = xmlLogPath + sessionID + "_" + appUserID + "_TicketingResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                try
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    dynamic ticketingResponse = js.Deserialize<dynamic>(response);
                    string rsp = JsonConvert.SerializeObject(ticketingResponse, Newtonsoft.Json.Formatting.Indented);
                    StreamWriter sw = new StreamWriter(filePath + ".json");
                    sw.Write(rsp);
                    sw.Close();

                    SerializeJSONToXML(rsp, filePath + ".xml", "TicketingResponse");
                }
                catch (Exception ex) { Audit.Add(EventType.Book, Severity.High, appUserID, "(PKFARES)Failed to serialize Ticketing Response. Reason : " + ex.ToString(), ""); }

                if (!string.IsNullOrEmpty(response) && response.Length > 0)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(filePath + ".xml");

                    XmlNode errorNode = doc.SelectSingleNode("TicketingResponse/errorMsg");

                    if (errorNode != null && errorNode.InnerText.ToLower() == "ok")
                    {
                        XmlNode ticketingRsp = doc.SelectSingleNode("TicketingResponse/data");

                        if (ticketingRsp != null && ticketingRsp.SelectSingleNode("orderNum") != null && !string.IsNullOrEmpty(ticketingRsp.SelectSingleNode("orderNum").InnerText))
                        {
                            ticketResponse.Status = TicketingResponseStatus.Successful;
                            ticketResponse.PNR = itinerary.PNR;
                            ticketResponse.Message = "Ticketing Successful";

                            Audit.Add(EventType.Book, Severity.Normal, appUserID, "(PKFares)Ticketing Sucessful. PNR :" + itinerary.PNR + " Order Number:" + itinerary.TicketAdvisory, "");

                            List<Ticket> lstTicket = new List<Ticket>();
                            for (int i = 0; i < itinerary.Passenger.Length; i++)
                            {
                                Ticket ticket = new Ticket();

                                ticket.ConjunctionNumber = string.Empty;
                                ticket.CorporateCode = string.Empty;
                                ticket.CreatedBy = itinerary.CreatedBy;
                                ticket.CreatedOn = DateTime.Now;
                                ticket.Endorsement = string.Empty;
                                ticket.ETicket = true;
                                ticket.FareCalculation = string.Empty;
                                ticket.FareRule = string.Empty;
                                ticket.FlightId = itinerary.FlightId;
                                ticket.IssueDate = DateTime.Now;
                                ticket.LastModifiedBy = itinerary.CreatedBy;
                                ticket.PaxFirstName = itinerary.Passenger[i].FirstName;
                                ticket.PaxId = itinerary.Passenger[i].PaxId;
                                ticket.PaxLastName = itinerary.Passenger[i].LastName;
                                ticket.PaxType = itinerary.Passenger[i].Type;
                                ticket.Price = itinerary.Passenger[i].Price;
                                ticket.Status = "Ticketed";
                                ticket.TicketAdvisory = string.Empty;
                                ticket.TicketDesignator = string.Empty;
                                ticket.TicketNumber = itinerary.PNR;
                                ticket.TicketType = string.Empty;
                                ticket.Title = itinerary.Passenger[i].Title;
                                ticket.ValidatingAriline = itinerary.ValidatingAirlineCode;
                                ticket.PtcDetail = new List<SegmentPTCDetail>();

                                string[] baggages = itinerary.Passenger[i].BaggageCode.Split(',');
                                if (string.IsNullOrEmpty(itinerary.Passenger[i].BaggageCode))
                                {
                                    baggages = new string[itinerary.Segments.Length];
                                }
                                try
                                {
                                    for (int j = 0; j < itinerary.Segments.Length; j++)
                                    {
                                        string baggage = baggages[j];
                                        SegmentPTCDetail ptcDetail = new SegmentPTCDetail();
                                        ptcDetail.Baggage = (string.IsNullOrEmpty(baggage) ? "Airline Norms" : baggage);
                                        switch (itinerary.Passenger[i].Type)
                                        {
                                            case PassengerType.Adult:
                                                ptcDetail.PaxType = "ADT";
                                                break;
                                            case PassengerType.Child:
                                                ptcDetail.PaxType = "CHD";
                                                break;
                                            case PassengerType.Infant:
                                                ptcDetail.PaxType = "INF";
                                                break;
                                        }
                                        ptcDetail.FlightKey = itinerary.Segments[j].FlightKey;
                                        ptcDetail.SegmentId = itinerary.Segments[j].SegmentId;
                                        ptcDetail.FareBasis = "";
                                        ptcDetail.NVA = "";
                                        ptcDetail.NVB = "";
                                        ticket.PtcDetail.Add(ptcDetail);
                                    }
                                }
                                catch { }
                                lstTicket.Add(ticket);
                            }
                            tickets = lstTicket.ToArray();

                        }
                        else
                        {
                            ticketResponse.Message = "Failed to get Order Number!";
                            ticketResponse.Status = TicketingResponseStatus.OtherError;
                            Audit.Add(EventType.Book, Severity.Normal, appUserID, "(PKFares)Ticketing Failed due to OrderNum not returned. PNR : " + itinerary.PNR, "");
                        }
                    }
                    else
                    {
                        ticketResponse.Message = "Failed to get Ticketing Response. " + errorNode.InnerText;
                        ticketResponse.Status = TicketingResponseStatus.OtherError;
                        Audit.Add(EventType.Book, Severity.Normal, appUserID, "(PKFares)Ticketing Failed. PNR : " + itinerary.PNR + " Error: " + errorNode.InnerText, "");
                    }
                }
            }
            catch(Exception ex)
            {
                ticketResponse.Status = TicketingResponseStatus.OtherError;
                ticketResponse.Message = ex.Message;
                Audit.Add(EventType.Ticketing, Severity.High, appUserID, "(PKFARES)Failed to Ticket. Reason : " + ex.ToString(), "");
            }

            return ticketResponse;
        }


        /// <summary>
        /// Cancels the booking before it is Ticketed. OrderNumber will be stored in TicketAdvisory in FlightItinerary.
        /// </summary>
        /// <param name="orderNum"></param>
        /// <param name="pnr"></param>
        /// <returns></returns>
        public bool CancelBooking(string orderNum, string pnr)
        {
            bool cancelled = false;

            try
            {
                string jsonRequest = GenerateCancelOrderRequest(orderNum, pnr);

                string filePath = xmlLogPath + sessionID + "_" + appUserID + "_CancelOrderRequest_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                try
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    dynamic cancelOrderRequest = js.Deserialize<dynamic>(jsonRequest);
                    string rsp = JsonConvert.SerializeObject(cancelOrderRequest, Newtonsoft.Json.Formatting.Indented);
                    StreamWriter sw = new StreamWriter(filePath + ".json");
                    sw.Write(rsp);
                    sw.Close();

                    SerializeJSONToXML(rsp, filePath + ".xml", "CancelOrderRequest");
                }
                catch (Exception ex) { Audit.Add(EventType.Book, Severity.High, appUserID, "(PKFARES)Failed to serialize Cancel Order Request. Reason : " + ex.ToString(), ""); }

                string base64 = EncodeTo64(jsonRequest);

                string response = GetResponse(base64, "CancelOrder");

                filePath = xmlLogPath + sessionID + "_" + appUserID + "_CancelOrderResponse_" + DateTime.Now.ToString("yyyyMMdd_hhmmss");
                try
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    dynamic cancelOrderResponse = js.Deserialize<dynamic>(response);
                    string rsp = JsonConvert.SerializeObject(cancelOrderResponse, Newtonsoft.Json.Formatting.Indented);
                    StreamWriter sw = new StreamWriter(filePath + ".json");
                    sw.Write(rsp);
                    sw.Close();

                    SerializeJSONToXML(rsp, filePath + ".xml", "CancelOrderResponse");
                }
                catch (Exception ex) { Audit.Add(EventType.Book, Severity.High, appUserID, "(PKFARES)Failed to serialize Cancel Order Response. Reason : " + ex.ToString(), ""); }

                if (!string.IsNullOrEmpty(response) && response.Length > 0)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(filePath + ".xml");

                    XmlNode errorMsg = doc.SelectSingleNode("CancelOrderResponse/errorMsg");

                    if (errorMsg != null && errorMsg.InnerText.ToLower() == "ok")
                    {
                        cancelled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.CancellBooking, Severity.High, appUserID, "(PKFARES)Failed to Cancel Booking. Reason : " + ex.ToString(), "");
            }

            return cancelled;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~API()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
