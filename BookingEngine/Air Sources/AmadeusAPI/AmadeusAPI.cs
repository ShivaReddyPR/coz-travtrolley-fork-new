﻿#region NamespaceRegion
using System;
using System.Collections.Generic;
using System.IO;
using CT.Configuration;
using CT.Core;
using System.Net;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using CT.BookingEngine.GDS.IBE;
using System.Threading;
using System.Linq;
#endregion

namespace CT.BookingEngine.GDS
{
    /// <summary>
    /// Used to create the FlightResult object
    /// </summary>
    struct TempResult
    {
        public TimeSpan Duration;
        public FlightInfo[] Flight;
    }
    public class AmadeusAPI
    {

        #region DataMembers
        string userName;//Assigned From mse
        string password;// Assigned From mse
        string xmlPath;// Assigned From config
        string wsap;//Web Service Access Point (WSAP) //Assigned From config
        string address;//Assigned From config
        string officeId;//Assigned From mse
        decimal rateOfExchange = 1;
        int appUserId; //Assigned from mse
        string sessionId;//Assigned from mse

        string posType;//Assigned From config
        string requestType;//Assigned From config
        string agentyDutyCode;//Assigned From config
        string agentBaseCurrency;//Assigned from mse
        Dictionary<string, decimal> exchangeRates;//Assigned from mse
        int decimalValue;//Assigned from mse

        //Session Handling Variables
        AmadeusWebServicesPTClient client;
        SessionHandler hSession;
        TransactionFlowLinkHandler hLink;
        SecurityHandler hSecurity;
        AddressingHandler hAddressing;
        string marketIataCode; //used for Ticket_CancelDocument

        #endregion


        #region Added for CombinationSearch  by bangar
        List<SearchRequest> searchRequests = new List<SearchRequest>();
        private bool searchBySegments;
        public bool SearchBySegments { get => searchBySegments; set => searchBySegments = value; }
        public string sSeatAvailResponseXSLT { get; private set; }
        AutoResetEvent[] eventFlag = new AutoResetEvent[0];
        List<SearchResult> finalResult = new List<SearchResult>();
        bool isOneWayCombinationSearch = false;
        public string AppUserId = "1";//to avoid exception
        #endregion


        #region Properties
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public int AppUserID
        {
            get { return appUserId; }
            set { appUserId = value; }
        }

        public string SessionID
        {
            get { return sessionId; }
            set { sessionId = value; }
        }

        /// <summary>
        /// To be used to continue booking
        /// </summary>

        public SessionHandler SessionHandle
        {
            get { return hSession; }
            set { hSession = value; }
        }

        /// <summary>
        /// To be used to continue booking
        /// </summary>

        public TransactionFlowLinkHandler TransactionHandle
        {
            get { return hLink; }
            set { hLink = value; }
        }

        /// <summary>
        /// To be used to continue booking
        /// </summary>        
        public SecurityHandler SecurityHandle
        {
            get { return hSecurity; }
            set { hSecurity = value; }
        }

        public string PosType
        {
            get { return posType; }
            set { posType = value; }
        }

        public string RequestType
        {
            get { return requestType; }
            set { requestType = value; }
        }

        public string AgentyDutyCode
        {
            get { return agentyDutyCode; }
            set { agentyDutyCode = value; }
        }
        public string OfficeId
        {
            get { return officeId; }
            set { officeId = value; }
        }

        /// <summary>
        /// Agent Base Currency Eg:AED,INR,
        /// </summary>
        public string AgentBaseCurrency
        {
            get { return agentBaseCurrency; }
            set { agentBaseCurrency = value; }
        }

        public Dictionary<string, decimal> ExchangeRates
        {
            get { return exchangeRates; }
            set { exchangeRates = value; }
        }

        public int AgentDecimalValue
        {
            get { return decimalValue; }
            set { decimalValue = value; }
        }

        public string MarketIataCode
        {
            get { return marketIataCode; }
            set { marketIataCode = value; }
        }

        #endregion


        /************************Start:METHODS FLOW***************************
       01 Search:
      1_Fare_MasterPricerTravelBoardSearchRequest_
      2_Fare_MasterPricerTravelBoardSearchResponse_

      02 Price Check:
      3_Fare_InformativePricingWithoutPNRRequest_
      4_Fare_InformativePricingWithoutPNRResponse_

      03 MiniRules:
      5_Fare_InformativePricingWithoutPNRRequest_
      6_Fare_InformativePricingWithoutPNRResponse_
      7_MiniRule_GetFromPricingRequest_
      8_MiniRule_GetFromPricingResponse_
      9_Security_SignOutRequest_
     10_Security_SignOutResponse_

       04 Booking :
     11_Air_SellFromRecommendationRequest_
     12_Air_SellFromRecommendationResponse_
     13_PNR_AddMultiElementsRequest_
     14_PNR_AddMultiElementsResponse_
     15_Fare_PricePNRWithBookingClassRequest_
     16_Fare_PricePNRWithBookingClassResponse_
     17_Ticket_CreateTSTFromPricingRequest_
     18_Ticket_CreateTSTFromPricingResponse_
     19_PNR_AddMultiElementsAfterTSTRequest_
     20 _PNR_AddMultiElementsAfterTSTResponse_
     21 _Security_SignOutRequest_
     22 _Security_SignOutResponse_

       05 Ticketing:
      23_PNR_RetrieveRequest_
      24_PNR_RetrieveResponse_
      25_DocIssuance_IssueTicketRequest_
      26_DocIssuance_IssueTicketResponse_
      27_PNR_RetrieveRequest_
      28_PNR_RetrieveResponse_

       06 Cancellation:
      29_PNR_RetrieveRequest_
      30_PNR_RetrieveResponse_
      31_PNR_CancelBookingRequest_
      32_PNR_CancelBookingResponse_

       07 Void :
      33_PNR_RetrieveRequest_
      34_PNR_RetrieveResponse_
      35_Ticket_CancelDocumentRequest_
      36_Ticket_CancelDocumentResponse_   
      37_PNR_RetrieveRequest_
      38_PNR_RetrieveResponse_
      39_PNR_CancelBookingRequest_
      40_PNR_CancelBookingResponse_
     /************************End:METHODS FLOW***************************/


        /// <summary>
        /// Default Constructor
        /// </summary>
        public AmadeusAPI()
        {
            this.wsap = ConfigurationSystem.AmadeusConfig["WSAP"];
            this.address = ConfigurationSystem.AmadeusConfig["ServiceURL"];
            this.xmlPath = ConfigurationSystem.AmadeusConfig["XmlLogPath"] + DateTime.Now.ToString("ddMMMyyyy") + "\\";
            this.posType = ConfigurationSystem.AmadeusConfig["PosType"];
            this.requestType = ConfigurationSystem.AmadeusConfig["RequestType"];
            this.agentyDutyCode = ConfigurationSystem.AmadeusConfig["AgentyDutyCode"];
            if (!Directory.Exists(xmlPath))
            {
                Directory.CreateDirectory(xmlPath);
            }

        }

        #region Helper Methods
        /// <summary>
        /// Assigns SOAP Header Credentials
        /// </summary>

        public void GenerateHeader()
        {
            ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            client = new AmadeusWebServicesPTClient("AmadeusWebServicesPort", address);
            hSecurity = new SecurityHandler(client, userName, password);
            hSession = new SessionHandler(client, hSecurity);
            hLink = new TransactionFlowLinkHandler(client);
            hAddressing = new AddressingHandler(client, wsap);
            hSecurity.pcc = officeId;
            hSecurity.posType = posType;
            hSecurity.requestType = requestType;
            hSecurity.agentyDutyCode = agentyDutyCode;
        }

        /// <summary>
        /// Gets the PAX Gender for different PAX types
        /// </summary>
        /// <param name="gender"></param>
        /// <param name="paxType"></param>
        /// <returns></returns>
        public string GetPaxGenderForSSRDOCS(Gender gender, PassengerType paxType)
        {
            string paxGender = string.Empty;
            switch (paxType)
            {
                case PassengerType.Adult:
                case PassengerType.Child:
                    paxGender = (gender == Gender.Male ? "M" : "F");
                    break;
                case PassengerType.Infant:
                    paxGender = (gender == Gender.Male ? "MI" : "FI");
                    break;
            }

            return paxGender;
        }

        #endregion

        #region 01:Search
        /// <summary>
        /// A basic low fare search is composed of the following mandatory elements:
        ///Itinerary geographical information(origins/destinations)
        ///Itinerary date information
        ///Passenger information(number of passenger seats required and associated passenger type codes for travelling passengers)
        ///Number of recommendations to be returned
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public List<SearchResult> Search(SearchRequest request)
        {
            Dictionary<string, int> readySources = new Dictionary<string, int>();
            List<SearchResult> lstresults = new List<SearchResult>();
            Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>(); // by bangar
            Airline.GetAllAirlinesList();

            GenerateHeader();
            
                searchBySegments = request.SearchBySegments;
            if (searchBySegments)
            {
                if (request.Type == SearchType.Return)//Round Trip Combination Search by bangar
                {
                    
                    listOfThreads.Add("1AO1", new WaitCallback(SearchRoutingReturn));
                    listOfThreads.Add("1AO2", new WaitCallback(SearchRoutingReturn));

                    SearchRequest onwardRequest = request.Copy();
                    onwardRequest.Type = SearchType.OneWay;
                    onwardRequest.Segments = new FlightSegment[1];
                    onwardRequest.Segments[0] = request.Segments[0];

                    SearchRequest returnRequest = request.Copy();
                    returnRequest.Type = SearchType.Return;
                    returnRequest.Segments = new FlightSegment[1];
                    returnRequest.Segments[0] = request.Segments[1];

                    searchRequests.Add(onwardRequest);
                    searchRequests.Add(returnRequest);

                    eventFlag = new AutoResetEvent[2];
                    readySources.Add("1AO1", 1200);
                    readySources.Add("1AO2", 1200);

                    int t = 0;
                    //Start each fare search within a Thread which will automatically terminate after the results are received.
                    foreach (KeyValuePair<string, WaitCallback> deThread in listOfThreads)
                    {
                        if (readySources.ContainsKey(deThread.Key))
                        {
                            ThreadPool.QueueUserWorkItem(deThread.Value, t);
                            eventFlag[t] = new AutoResetEvent(false);
                            t++;
                        }
                    }

                    if (t != 0)
                    {
                        if (!WaitHandle.WaitAll(eventFlag, new TimeSpan(0, 2, 0), true))
                        {
                            //TODO: audit which thread is timed out                
                        }
                    }
                }
                //===============START:ONE WAY COMBINATION SEARCH==============/
                else if (request.Type == SearchType.OneWay)//One Way Combination Search
                {
                    
                    SearchRequest onwardRequest = request.Copy();
                    onwardRequest.Type = SearchType.OneWay;
                    onwardRequest.Segments = new FlightSegment[1];
                    onwardRequest.Segments[0] = request.Segments[0];
                    searchRequests.Add(onwardRequest);
                    isOneWayCombinationSearch = true;
                    SearchRoutingReturn(0);
                }
                //===============END:ONE WAY COMBINATION SEARCH==============/
            }
            else 
            {

                SearchResult[] result = new SearchResult[0];
                
                result = RegularSearch(request);
                finalResult.AddRange(result);

            }

                Basket.FlightBookingSession[sessionId].Log.Add("Amadeus Search complete. Result count = " + finalResult.Count.ToString());
            ///Check for onward and return flights in case routing search and search type is return
            if (searchBySegments && request.Type == SearchType.Return)
            {
                bool onwardFound = finalResult.Any(r => r.Flights.ToList().All(s => s.ToList().All(f => f.Group == 0)));
                bool returnFound = finalResult.Any(r => r.Flights.ToList().All(s => s.ToList().All(f => f.Group == 1)));
                if (!onwardFound && !returnFound)
                {
                    finalResult = new List<SearchResult>();
                }

            }

            // for Preffered Airlines searching added by bangar
            if (request.RestrictAirline == true && (request.Segments[0].PreferredAirlines != null && request.Segments[0].PreferredAirlines.Length > 0))
                    finalResult = finalResult.FindAll(d => d.Flights.Any(s => s.Any(p => p.Airline.Any(q => string.Join(",", request.Segments[0].PreferredAirlines).Contains(p.Airline)))));

            return finalResult;
           
        }

        public void SearchRoutingReturn(object eventNumber)
        {
            try
            {
                SearchRequest request = searchRequests[Convert.ToInt32(eventNumber)];
                SearchResult[] result = new SearchResult[0];
                result = RegularSearch(request);

                finalResult.AddRange(result);


            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(AppUserId), "Failed to get Amadeus SOAP Search response. Error: " + ex.ToString(), "");
            }
            finally
            {
                eventFlag[(int)eventNumber].Set();
            }
        }

        public  SearchResult[] RegularSearch(SearchRequest request)
        {
            List<SearchResult> Results = new List<SearchResult>();

            try
            {
                //Fare_MasterPricerTravelBoard(MPTB)
                //A MPTB search is composed of the following mandatory elements:

                Fare_MasterPricerTravelBoardSearch search = new Fare_MasterPricerTravelBoardSearch();

                //1.numberOfUnit
                search.numberOfUnit = new NumberOfUnitDetailsType_260583C1[2];

                //This number may not always be equal to the total number of passengers traveling. 
                //e.g., 2 Adults, 1 Child and 1 Infant - as an infant does not occupy a seat, this request requires only 3 seats for 4 traveling passengers
                search.numberOfUnit[0] = new NumberOfUnitDetailsType_260583C1();
                search.numberOfUnit[0].numberOfUnits = Convert.ToString(request.AdultCount + request.ChildCount); //Number of Passenger Seats
                search.numberOfUnit[0].typeOfUnit = "PX";//Number of seats occupied by passengers on board

                //RC = number of recommendations.
                //The maximum number of recommendations in MPTB is 250
                search.numberOfUnit[1] = new NumberOfUnitDetailsType_260583C1();
                search.numberOfUnit[1].numberOfUnits = "250";
                search.numberOfUnit[1].typeOfUnit = "RC";

                //2.paxReference
                //Associated Passenger Type Codes for Traveling Passengers
                //ADT ==Adult
                //CH == Child
                //INF == Infant
                int PaxReference = 1;
                if (request.ChildCount > 0)
                {
                    PaxReference++;
                }
                if (request.InfantCount > 0)
                {
                    PaxReference++;
                }
                search.paxReference = new TravellerReferenceInformationType2[PaxReference];

                //Adult
                search.paxReference[0] = new TravellerReferenceInformationType2();
                search.paxReference[0].ptc = new string[1];
                search.paxReference[0].ptc[0] = "ADT";
                search.paxReference[0].traveller = new TravellerDetailsType4[request.AdultCount];
                for (int adult = 0; adult < request.AdultCount; adult++)
                {
                    search.paxReference[0].traveller[adult] = new TravellerDetailsType4();
                    search.paxReference[0].traveller[adult].@ref = (adult + 1).ToString();
                }

                //If only child exists
                if (request.ChildCount > 0)
                {
                    search.paxReference[1] = new TravellerReferenceInformationType2();
                    search.paxReference[1].ptc = new string[1];
                    search.paxReference[1].ptc[0] = "CH";//Child
                    search.paxReference[1].traveller = new TravellerDetailsType4[request.ChildCount];
                    int paxRef = request.AdultCount;
                    for (int child = 0; child < request.ChildCount; child++)
                    {
                        search.paxReference[1].traveller[child] = new TravellerDetailsType4();
                        search.paxReference[1].traveller[child].@ref = (paxRef + 1).ToString();
                        paxRef++;
                    }
                }
                //If only infant exists
                if (request.ChildCount == 0 && request.InfantCount > 0)
                {
                    search.paxReference[1] = new TravellerReferenceInformationType2();
                    search.paxReference[1].ptc = new string[1];
                    search.paxReference[1].ptc[0] = "INF";//Infant
                    search.paxReference[1].traveller = new TravellerDetailsType4[request.InfantCount];
                    for (int infant = 0; infant < request.InfantCount; infant++)
                    {
                        search.paxReference[1].traveller[infant] = new TravellerDetailsType4();
                        search.paxReference[1].traveller[infant].@ref = (infant + 1).ToString();
                        search.paxReference[1].traveller[infant].infantIndicator = (1).ToString();
                    }

                }
                //If both(child and infant) of them exists
                else if (request.ChildCount > 0 && request.InfantCount > 0)
                {
                    search.paxReference[2] = new TravellerReferenceInformationType2();
                    search.paxReference[2].ptc = new string[1];
                    search.paxReference[2].ptc[0] = "INF";//Infant
                    search.paxReference[2].traveller = new TravellerDetailsType4[request.InfantCount];
                    for (int infant = 0; infant < request.InfantCount; infant++)
                    {
                        search.paxReference[2].traveller[infant] = new TravellerDetailsType4();
                        search.paxReference[2].traveller[infant].@ref = (infant + 1).ToString();
                        search.paxReference[2].traveller[infant].infantIndicator = (1).ToString();
                    }
                }
                //3.Fare options  -- RP,RU,TAC,ET
                //RP -- Published fares
                //TAC -- Ticket ability check
                //RU -- Unifares.
                //ET -- ELECTRONIC TICKET ONLY
                //RF -- Refundable fares

                search.fareOptions = new Fare_MasterPricerTravelBoardSearchFareOptions();
                search.fareOptions.pricingTickInfo = new PricingTicketingDetailsType1();

                if (request.RefundableFares)//Refundable fares only filter
                {
                    search.fareOptions.pricingTickInfo.pricingTicketing = new string[5];
                }
                else
                {
                    search.fareOptions.pricingTickInfo.pricingTicketing = new string[4];
                }
                search.fareOptions.pricingTickInfo.pricingTicketing[0] = "RP";
                search.fareOptions.pricingTickInfo.pricingTicketing[1] = "RU";
                search.fareOptions.pricingTickInfo.pricingTicketing[2] = "TAC";
                search.fareOptions.pricingTickInfo.pricingTicketing[3] = "ET";
                if (request.RefundableFares)//Refundable fares only filter
                {
                    search.fareOptions.pricingTickInfo.pricingTicketing[4] = "RF";
                }

                //4.itinerary Options.
                int searchType = 1;//OneWay
                if (request.Type == SearchType.Return)
                {
                    ++searchType;
                }
                search.itinerary = new Fare_MasterPricerTravelBoardSearchItinerary[searchType];
                for (int r = 0; r < searchType; r++)
                {
                    //4.1 requestedSegmentRef
                    search.itinerary[r] = new Fare_MasterPricerTravelBoardSearchItinerary();
                    search.itinerary[r].requestedSegmentRef = new OriginAndDestinationRequestType2();
                    search.itinerary[r].requestedSegmentRef.segRef = (r + 1).ToString();

                    //4.2 departureLocalization  //Origin/Destination
                    search.itinerary[r].departureLocalization = new DepartureLocationType1();
                    search.itinerary[r].departureLocalization.departurePoint = new ArrivalLocationDetailsType_120834C1();

                    if (r == 0)//OneWay  //Future multicity -- r--serachType
                    {
                        search.itinerary[r].departureLocalization.departurePoint.locationId = request.Segments[0].Origin;
                    }
                    else//RoundTrip
                    {
                        search.itinerary[r].departureLocalization.departurePoint.locationId = request.Segments[0].Destination;
                    }

                    //4.2 arrivalLocalization  //Origin/Destination
                    search.itinerary[r].arrivalLocalization = new ArrivalLocalizationType1();
                    search.itinerary[r].arrivalLocalization.arrivalPointDetails = new ArrivalLocationDetailsType1();
                    if (r == 0)//OneWay
                    {
                        search.itinerary[r].arrivalLocalization.arrivalPointDetails.locationId = request.Segments[0].Destination;
                    }
                    else//RoundTrip
                    {
                        search.itinerary[r].arrivalLocalization.arrivalPointDetails.locationId = request.Segments[0].Origin;
                    }
                    //4.3 firstDateTimeDetail
                    search.itinerary[r].timeDetails = new DateAndTimeInformationType_181295S1();
                    search.itinerary[r].timeDetails.firstDateTimeDetail = new DateAndTimeDetailsTypeI1();
                    if (r == 0)//OneWay
                    {
                        search.itinerary[r].timeDetails.firstDateTimeDetail.date = request.Segments[0].PreferredDepartureTime.ToString("ddMMyy");
                    }
                    else
                    {
                        search.itinerary[r].timeDetails.firstDateTimeDetail.date = request.Segments[0].PreferredDepartureTime.ToString("ddMMyy");
                    }

                    //if (searchBySegments)
                    //    flightInfo.Group = request.Type == SearchType.OneWay ? 0 : 1;
                    //else
                    //    flight.Group = segGroup;

                }

                //Filters:
                //1.Cabin Class
                //2.Prefered Airline
                //3.Refundable && Non-Refundable fares only
                //4.Non Stop Flights Only.


                //Fare_MasterPricerTravelBoardSearch/travelFlightInfo/cabinId/cabin
                //C--Business
                //F -- First, supersonic
                //M--Economic Standard
                //W --Economic Premium
                //Y -- Economic
                string cabinClass = string.Empty;
                switch (request.Segments[0].flightCabinClass)
                {
                    case CabinClass.Business:
                        cabinClass = "C";
                        break;
                    case CabinClass.Economy:
                        cabinClass = "Y";
                        break;
                    case CabinClass.First:
                        cabinClass = "F";
                        break;
                    case CabinClass.PremiumEconomy:
                        cabinClass = "W";
                        break;
                }
                if (!string.IsNullOrEmpty(cabinClass) && cabinClass.Length > 0 || (request.Segments[0].PreferredAirlines.Length > 0) || (!string.IsNullOrEmpty(request.MaxStops) && Convert.ToInt32(request.MaxStops) >= 0))
                {
                    TravelFlightInformationType_185853S1 travelFlightInfo = new TravelFlightInformationType_185853S1();
                    //1.Cabin Class Filter
                    if (!string.IsNullOrEmpty(cabinClass) && cabinClass.Length > 0)
                    {
                        travelFlightInfo.cabinId = new CabinIdentificationType_233500C1();
                        travelFlightInfo.cabinId.cabin = new string[1];
                        travelFlightInfo.cabinId.cabin[0] = cabinClass;
                    }
                    //2.Prefered Airline Filter
                    if (request.Segments[0].PreferredAirlines.Length > 0)
                    {
                        travelFlightInfo.companyIdentity = new CompanyIdentificationType_233548C1[1];
                        //F-- Preferred
                        //M -- Mandatory
                        //N -- Night class
                        //O -- Force Full airline recommendation
                        //P -- Polled (deprecated)
                        //R -- Fare Family Repartition (deprecated)
                        //T	 --List of carriers for which the BSP checks should be bypassed
                        //V -- Mandatory validating carrier list
                        //W -- Excluded validating carrier list
                        //X -- Excluded

                        //To determine airline inclusion/exclusion
                        travelFlightInfo.companyIdentity[0] = new CompanyIdentificationType_233548C1();
                        travelFlightInfo.companyIdentity[0].carrierQualifier = "F";//F-- Preferred
                        travelFlightInfo.companyIdentity[0].carrierId = new string[request.Segments[0].PreferredAirlines.Length];
                        for (int i = 0; i < request.Segments[0].PreferredAirlines.Length; i++)
                        {
                            travelFlightInfo.companyIdentity[0].carrierId[i] = request.Segments[0].PreferredAirlines[i];
                        }
                    }
                    //3.Flights Stops(Non-Stop,1Stop,2Stop,2+Stops)

                    if (!string.IsNullOrEmpty(request.MaxStops) && Convert.ToInt32(request.MaxStops) >= 0)
                    {
                        //C-- Connecting service
                        //D -- Direct service
                        //DN -- Disable Negociated Space
                        //FLF -- Flight facts
                        //GOV -- Government approval requested
                        //IFS -- In-flight services
                        //N-- Non-stop service
                        //OL -- To return the cheapest online solution
                        //OV -- Overnight not allowed
                        if (Convert.ToInt32(request.MaxStops) == 0) //Non-Stop or Direct Flights
                        {
                            travelFlightInfo.flightDetail = new string[2];
                            travelFlightInfo.flightDetail[0] = "N";
                            travelFlightInfo.flightDetail[1] = "D";
                            
                        }
                    }
                    
                    search.travelFlightInfo = travelFlightInfo;
                }


                Session session = hSession.Session;
                TransactionFlowLinkType link = hLink.Link;

                hSession.handleSession(SessionHandler.TransactionStatusCode.Start);
                hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.New);
                //hAddressing.update();

                Fare_MasterPricerTravelBoardSearchRequest searchRequest = new Fare_MasterPricerTravelBoardSearchRequest(session, link, hSecurity.getHostedUser(), search);
                //Serialize searchRequest
                try
                {
                    //if (searchBySegments)
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_Fare_MasterPricerTravelBoardSearchRequest_" + request.Segments[0].Origin +"_"+ request.Segments[0].Destination +"_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(searchRequest.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, searchRequest);
                        sw.Close();
                    }
                    //else
                    //{
                    //    string filepath = xmlPath + sessionId + "_" + appUserId + "_Fare_MasterPricerTravelBoardSearchRequest_" + request.Segments[0].Origin + "_" + request.Segments[0].Destination + "_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    //    XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(searchRequest.GetType());
                    //    StreamWriter sw = new StreamWriter(filepath);
                    //    xsr.Serialize(sw, searchRequest);
                    //    sw.Close();
                    //}
                }
                catch { }




                Fare_MasterPricerTravelBoardSearchReply response = client.Fare_MasterPricerTravelBoardSearch(ref session, ref link, hSecurity.getHostedUser(), search);
                if (response != null)
                {

                    //Security_SignOutRequest signoutRequest = new Security_SignOutRequest();
                    //signoutRequest.Security_SignOut = new Security_SignOut();
                    //signoutRequest.AMA_SecurityHostedUser = hSecurity.getHostedUser();
                    //signoutRequest.Session = hSession.Session;
                    //signoutRequest.TransactionFlowLink = hLink.Link;

                    //try
                    //{
                    //    string filepath = xmlPath + sessionId + "_" + appUserId + "_Security_SignOutRequest_" + "_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    //    XmlSerializer xsr = new XmlSerializer(signoutRequest.GetType());
                    //    StreamWriter sw = new StreamWriter(filepath);
                    //    xsr.Serialize(sw, signoutRequest);
                    //    sw.Close();
                    //}
                    //catch { }

                    //  Security_SignOutReply signOutReply = client.Security_SignOut(ref session, ref link, hSecurity.getHostedUser(), signoutRequest.Security_SignOut);
                    //  Security_SignOutResponse signoutResponse = new Security_SignOutResponse(session, link, signOutReply);
                    hSession.Session = session;
                    hLink.Link = link;
                    hSession.handleSession(SessionHandler.TransactionStatusCode.End);
                    hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.None);
                    //hAddressing.update();

                    //try
                    //{
                    //    string filepath = xmlPath + sessionId + "_" + appUserId + "_Security_SignOutResponse_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    //    XmlSerializer xsr = new XmlSerializer(signoutResponse.GetType());
                    //    StreamWriter sw = new StreamWriter(filepath);
                    //    xsr.Serialize(sw, signoutResponse);
                    //    sw.Close();
                    //}
                    //catch { }

                    Fare_MasterPricerTravelBoardSearchResponse searchResponse = new Fare_MasterPricerTravelBoardSearchResponse(session, link, response);
                    //Serialize searchResponse
                    try
                    {

                        //if (searchBySegments)
                        {
                            string filepath = xmlPath + sessionId + "_" + appUserId + "_Fare_MasterPricerTravelBoardSearchResponse_" + request.Segments[0].Origin +"_" + request.Segments[0].Destination + "_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                            XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(searchResponse.GetType());
                            StreamWriter sw = new StreamWriter(filepath);
                            xsr.Serialize(sw, searchResponse);
                            sw.Close();
                        }
                        //else
                        //{
                        //    string filepath = xmlPath + sessionId + "_" + appUserId + "_Fare_MasterPricerTravelBoardSearchResponse_" + request.Segments[0].Origin + "_" + request.Segments[0].Destination + "_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        //    XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(searchResponse.GetType());
                        //    StreamWriter sw = new StreamWriter(filepath);
                        //    xsr.Serialize(sw, searchResponse);
                        //    sw.Close();
                        //}
                    }
                    catch { }


                    //Fare_MasterPricerTravelBoardSearchReply elements

                    //*replyStatus - C 1
                    //*errorMessage - C 1
                    //*conversionRate - C 1
                    //solutionFamily - C 20
                    //familyInformation - C 200
                    //amountInfoForAllPax - C 1
                    //amountInfoPerPax - C 20
                    //feeDetails - C 2099
                    //companyIdText - C 5000
                    //officeIdDetails - C 20
                    //*flightIndex - C 6
                    //*recommendation - C 100000
                    //otherSolutions - C 100009
                    //warningInfo - C 9
                    //globalInformation - C 9
                    //*serviceFeesGrp - C 3
                    //value - C 100009
                    //mnrGrp - C 1

                    /*****************Observed Elements In the Response ******************/
                    /* //*replyStatus - C 1
                     * //*errorMessage - C 1
                     *  //*conversionRate - C 1
                     * //*flightIndex - C 6
                     *  //*recommendation - C 100000
                     * ********************************************************************/


                    //Form the result set
                    bool validResponse = true;
                    string errorMessage = string.Empty;
                    if (searchResponse.Fare_MasterPricerTravelBoardSearchReply.errorMessage != null)
                    {
                        Fare_MasterPricerTravelBoardSearchReplyErrorMessage err = searchResponse.Fare_MasterPricerTravelBoardSearchReply.errorMessage;
                        if (err.applicationError != null && err.applicationError.applicationErrorDetail != null && !string.IsNullOrEmpty(err.applicationError.applicationErrorDetail.error))
                        {
                            errorMessage = "Error Code:" + err.applicationError.applicationErrorDetail.error;
                        }
                        if (err.errorMessageText != null && err.errorMessageText.description.Length > 0)
                        {
                            errorMessage += ";Error Description:";
                            for (int t = 0; t < err.errorMessageText.description.Length; t++)
                            {


                                if (!string.IsNullOrEmpty(err.errorMessageText.description[t]))
                                {
                                    errorMessage += err.errorMessageText.description[t];
                                }
                            }
                        }
                        validResponse = false;
                        throw new Exception(errorMessage);
                    }
                    if (validResponse) //There is no error in the response
                    {
                        Fare_MasterPricerTravelBoardSearchReply res = searchResponse.Fare_MasterPricerTravelBoardSearchReply;
                        //1.conversionRate / conversionRateDetail / currency
                        string currency = string.Empty;
                        if (res.conversionRate != null && res.conversionRate.Length > 0)
                        {
                            if (!string.IsNullOrEmpty(res.conversionRate[0].currency))
                            {
                                currency = res.conversionRate[0].currency;
                            }
                        }
                        //2.FlightIndex
                        if (res.flightIndex != null && res.flightIndex.Length > 0)
                        {
                            #region TempResult Creation
                            TempResult[][] resultList = new TempResult[res.flightIndex.Length][];
                            for (int i = 0; i < res.flightIndex.Length; i++)
                            {
                                Fare_MasterPricerTravelBoardSearchReplyFlightIndex flightIndex = res.flightIndex[i];
                                int? groupIndex = null;
                                if (flightIndex.requestedSegmentRef != null && !string.IsNullOrEmpty(flightIndex.requestedSegmentRef.segRef))
                                {
                                    groupIndex = Convert.ToInt16(flightIndex.requestedSegmentRef.segRef) - 1;
                                }

                                //Group of Flights
                                if (flightIndex.groupOfFlights != null && flightIndex.groupOfFlights.Length > 0)
                                {
                                    Fare_MasterPricerTravelBoardSearchReplyFlightIndexGroupOfFlights[] groupOfFlights = flightIndex.groupOfFlights;
                                    resultList[i] = new TempResult[groupOfFlights.Length];
                                    for (int j = 0; j < groupOfFlights.Length; j++)
                                    {
                                        Fare_MasterPricerTravelBoardSearchReplyFlightIndexGroupOfFlights groupOfFlight = groupOfFlights[j];
                                        if (groupOfFlight != null)
                                        {
                                            int? index = null;
                                            TempResult result = new TempResult();

                                            #region Flight Proposals
                                            if (groupOfFlight.propFlightGrDetail != null && groupOfFlight.propFlightGrDetail.flightProposal != null && groupOfFlight.propFlightGrDetail.flightProposal.Length > 0)
                                            {
                                                //Flight Proposals
                                                ProposedSegmentDetailsType3[] flightProposals = groupOfFlight.propFlightGrDetail.flightProposal;
                                                for (int k = 0; k < flightProposals.Length; k++)
                                                {
                                                    ProposedSegmentDetailsType3 flightProposal = flightProposals[k];
                                                    if (string.IsNullOrEmpty(flightProposal.unitQualifier))
                                                    {
                                                        if (!string.IsNullOrEmpty(flightProposal.@ref))
                                                        {
                                                            index = Convert.ToInt32(flightProposal.@ref) - 1;
                                                        }
                                                    }
                                                    else if (!string.IsNullOrEmpty(flightProposal.unitQualifier) && flightProposal.unitQualifier == "EFT")
                                                    {
                                                        /****Start: unitQualifier *****/
                                                        //DUR --Duration for Rail journey
                                                        // EFT--Elapse Flying Time
                                                        // MCX--Majority carrier
                                                        // RJS--Rank in Journey Server 
                                                        /****End: unitQualifier *****/
                                                        if (!string.IsNullOrEmpty(flightProposal.@ref))
                                                        {
                                                            int n = flightProposal.@ref.Length - 2;
                                                            int hr = Convert.ToInt16(flightProposal.@ref.Substring(0, n));
                                                            int min = Convert.ToInt16(flightProposal.@ref.Substring(n));
                                                            result.Duration = new TimeSpan(hr, min, 0);
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion

                                            #region Flight Details
                                            if (groupOfFlight.flightDetails != null && groupOfFlight.flightDetails.Length > 0)
                                            {
                                                //Flight Details -- Flight Information
                                                Fare_MasterPricerTravelBoardSearchReplyFlightIndexGroupOfFlightsFlightDetails[] flightDetails = groupOfFlight.flightDetails;
                                                result.Flight = new FlightInfo[flightDetails.Length];
                                                for (int k = 0; k < flightDetails.Length; k++)
                                                {
                                                    Fare_MasterPricerTravelBoardSearchReplyFlightIndexGroupOfFlightsFlightDetails flightDetail = flightDetails[k];
                                                    FlightInfo flight = new FlightInfo();
                                                    string depDate = string.Empty;
                                                    string depTime = string.Empty;
                                                    string arrDate = string.Empty;
                                                    string arrTime = string.Empty;

                                                    TravelProductType3 flightInformation = flightDetail.flightInformation;
                                                    if (flightInformation != null)
                                                    {
                                                        //productDateTime
                                                        if (flightInformation.productDateTime != null)
                                                        {
                                                            depDate = flightInformation.productDateTime.dateOfDeparture;
                                                            depTime = flightInformation.productDateTime.timeOfDeparture;
                                                            arrDate = flightInformation.productDateTime.dateOfArrival;
                                                            arrTime = flightInformation.productDateTime.timeOfArrival;
                                                            flight.DepartureTime = DateTime.ParseExact(depDate + depTime, "ddMMyyHHmm", null);
                                                            flight.ArrivalTime = DateTime.ParseExact(arrDate + arrTime, "ddMMyyHHmm", null);
                                                            flight.Duration = flight.ArrivalTime.Subtract(flight.DepartureTime);
                                                        }
                                                        //Location List
                                                        if (flightInformation.location != null && flightInformation.location.Length > 0)
                                                        {
                                                            flight.Origin = new Airport(flightInformation.location[0].locationId);
                                                            flight.DepTerminal = (!string.IsNullOrEmpty(flightInformation.location[0].terminal) ? flightInformation.location[0].terminal : string.Empty);

                                                            if (flightInformation.location.Length > 1)
                                                            {
                                                                flight.Destination = new Airport(flightInformation.location[1].locationId);
                                                                flight.ArrTerminal = (!string.IsNullOrEmpty(flightInformation.location[1].terminal) ? flightInformation.location[1].terminal : string.Empty);
                                                            }
                                                        }
                                                        //companyId
                                                        if (flightInformation.companyId != null)
                                                        {
                                                            flight.Airline = flightInformation.companyId.marketingCarrier;
                                                            flight.OperatingCarrier = flightInformation.companyId.operatingCarrier;
                                                        }
                                                        // FlightNumber 
                                                        flight.FlightNumber = flightInformation.flightOrtrainNumber;

                                                        //Product Detail
                                                        if (flightInformation.productDetail != null)
                                                        {
                                                            flight.Craft = flightInformation.productDetail.equipmentType;
                                                        }
                                                        //addProductDetail
                                                        if (flightInformation.addProductDetail != null && !string.IsNullOrEmpty(flightInformation.addProductDetail.electronicTicketing))
                                                        {
                                                            if (flightInformation.addProductDetail.electronicTicketing == "Y")
                                                            {
                                                                flight.ETicketEligible = true;
                                                            }
                                                            else
                                                            {
                                                                flight.ETicketEligible = false;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            flight.ETicketEligible = false;
                                                        }
                                                        // added by bangar for assigning group 
                                                        flight.Group = request.Type == SearchType.OneWay ? 0 : 1;

                                                        result.Flight[k] = flight;
                                                    }//FlightInformation is not null
                                                }//flightDetails loop
                                            }//flightDetails is not null
                                            #endregion

                                            resultList[(int)groupIndex][(int)index] = result;
                                        }//groupOfFlight is not null
                                    }//groupOfFlight loop
                                }//groupOfFlight is not null
                            }//flightIndex loop
                            #endregion

                            List<SearchResult> finalResult = new List<SearchResult>();
                            if (res.recommendation != null && res.recommendation.Length > 0)
                            {
                                int resCount = 0;
                                int counter = 0;
                                for (int s = 0; s < res.recommendation.Length; s++)
                                {
                                    if (res.recommendation[s].segmentFlightRef != null)
                                    {
                                        resCount += res.recommendation[s].segmentFlightRef.Length;
                                    }
                                }
                                SearchResult[] result1DArray = new SearchResult[resCount];
                                for (int i = 0; i < res.recommendation.Length; i++)
                                {
                                    Fare_MasterPricerTravelBoardSearchReplyRecommendation rec = res.recommendation[i];
                                    if (rec.segmentFlightRef != null)
                                    {
                                        for (int s = 0; s < rec.segmentFlightRef.Length; s++)
                                        {
                                            ReferenceInfoType8 segFlightRef = rec.segmentFlightRef[s];
                                            result1DArray[counter] = new SearchResult();
                                            result1DArray[counter].ResultBookingSource = BookingSource.Amadeus;

                                            #region Reading FlightInfo (segments)
                                            if (segFlightRef.referencingDetail != null)
                                            {
                                                List<ReferencingDetailsType_191583C1> refDetails = new List<ReferencingDetailsType_191583C1>(segFlightRef.referencingDetail);
                                                List<ReferencingDetailsType_191583C1> segRefDetails = refDetails.FindAll(delegate (ReferencingDetailsType_191583C1 r) { return r.refQualifier == "S"; });
                                                result1DArray[counter].Flights = new FlightInfo[segRefDetails.Count][];
                                                for (int j = 0; j < segFlightRef.referencingDetail.Length; j++)
                                                {
                                                    ReferencingDetailsType_191583C1 refDetail = segFlightRef.referencingDetail[j];
                                                    if (refDetail.refQualifier == "S")//Segments
                                                    {
                                                        int segIndex = Convert.ToInt16(refDetail.refNumber) - 1;
                                                        result1DArray[counter].Flights[j] = new FlightInfo[resultList[j][segIndex].Flight.Length];
                                                        int tempCounter = 0;
                                                        foreach (FlightInfo fi in resultList[j][segIndex].Flight)
                                                        {
                                                            result1DArray[counter].Flights[j][tempCounter] = new FlightInfo();
                                                            result1DArray[counter].Flights[j][tempCounter].Airline = fi.Airline;
                                                            result1DArray[counter].Flights[j][tempCounter].AirlinePNR = fi.AirlinePNR;
                                                            result1DArray[counter].Flights[j][tempCounter].ArrivalTime = fi.ArrivalTime;
                                                            result1DArray[counter].Flights[j][tempCounter].ArrTerminal = fi.ArrTerminal;
                                                            result1DArray[counter].Flights[j][tempCounter].AvailabiLity = fi.AvailabiLity;
                                                            result1DArray[counter].Flights[j][tempCounter].Craft = fi.Craft;
                                                            result1DArray[counter].Flights[j][tempCounter].DepartureTime = fi.DepartureTime;
                                                            result1DArray[counter].Flights[j][tempCounter].DepTerminal = fi.DepTerminal;
                                                            result1DArray[counter].Flights[j][tempCounter].Destination = fi.Destination;
                                                            result1DArray[counter].Flights[j][tempCounter].FlightNumber = fi.FlightNumber;
                                                            result1DArray[counter].Flights[j][tempCounter].Origin = fi.Origin;
                                                            result1DArray[counter].Flights[j][tempCounter].ETicketEligible = fi.ETicketEligible;
                                                            result1DArray[counter].Flights[j][tempCounter].Origin = fi.Origin;
                                                            result1DArray[counter].Flights[j][tempCounter].BookingClass = fi.BookingClass;
                                                            result1DArray[counter].Flights[j][tempCounter].Stops = resultList[j][segIndex].Flight.Length - 1;
                                                            result1DArray[counter].Flights[j][tempCounter].OperatingCarrier = fi.OperatingCarrier;
                                                            result1DArray[counter].Flights[j][tempCounter].Duration = fi.Duration;
                                                            result1DArray[counter].Flights[j][tempCounter].Group = fi.Group; // added by bangar
                                                            if (string.IsNullOrEmpty(result1DArray[counter].Flights[j][tempCounter].ConjunctionNo))
                                                            {
                                                                result1DArray[counter].Flights[j][tempCounter].ConjunctionNo = refDetail.refNumber;
                                                            }
                                                            else
                                                            {
                                                                result1DArray[counter].Flights[j][tempCounter].ConjunctionNo += "," + refDetail.refNumber;
                                                            }
                                                            tempCounter++;
                                                        }
                                                        result1DArray[counter].Flights[j][result1DArray[counter].Flights[j].Length - 1].AccumulatedDuration = resultList[j][segIndex].Duration;
                                                    }
                                                    if (refDetail.refQualifier == "B") //Baggage
                                                    {
                                                        if (res.serviceFeesGrp != null)
                                                        {
                                                            Fare_MasterPricerTravelBoardSearchReplyServiceFeesGrp[] serviceFees = res.serviceFeesGrp;

                                                            int segCount = 1;

                                                            if (request.Type == SearchType.Return)
                                                            {
                                                                segCount = 2;
                                                            }
                                                            for (int m = 0; m < segCount; m++)
                                                            {
                                                                foreach (Fare_MasterPricerTravelBoardSearchReplyServiceFeesGrpFreeBagAllowanceGrp bag in serviceFees[0].freeBagAllowanceGrp)
                                                                {
                                                                    if (refDetail.refNumber == bag.itemNumberInfo[0].number)
                                                                    {
                                                                        if (string.IsNullOrEmpty(result1DArray[counter].BaggageIncludedInFare))
                                                                        {
                                                                            result1DArray[counter].BaggageIncludedInFare = bag.freeBagAllownceInfo.baggageDetails.freeAllowance + (bag.freeBagAllownceInfo.baggageDetails.quantityCode.ToLower() == "w" ? " KG" : " Piece");
                                                                        }
                                                                        else
                                                                        {
                                                                            result1DArray[counter].BaggageIncludedInFare += "," + bag.freeBagAllownceInfo.baggageDetails.freeAllowance + (bag.freeBagAllownceInfo.baggageDetails.quantityCode.ToLower() == "w" ? " KG" : " Piece");
                                                                        }
                                                                        break;
                                                                    }
                                                                }
                                                            }

                                                            //added by bangar for displaying baggage at passenger details summary level.
                                                            for (int x = 0; x < result1DArray[counter].Flights.Length; x++)
                                                            {
                                                                for (int y = 0; y < result1DArray[counter].Flights[x].Length; y++)
                                                                {
                                                                    result1DArray[counter].Flights[x][y].DefaultBaggage = string.IsNullOrEmpty(result1DArray[counter].Flights[x][y].DefaultBaggage) ? result1DArray[counter].BaggageIncludedInFare : "," + result1DArray[counter].BaggageIncludedInFare;
                                                                   
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                            #endregion

                                            //ValidatingAirline
                                            if (rec.paxFareProduct != null)
                                            {
                                                if (rec.paxFareProduct.Length > 0 && rec.paxFareProduct[0].paxFareDetail != null && rec.paxFareProduct[0].paxFareDetail.codeShareDetails != null && rec.paxFareProduct[0].paxFareDetail.codeShareDetails.Length > 0 && !string.IsNullOrEmpty(rec.paxFareProduct[0].paxFareDetail.codeShareDetails[0].transportStageQualifier))
                                                {
                                                    if (rec.paxFareProduct[0].paxFareDetail.codeShareDetails[0].transportStageQualifier == "V")
                                                    {
                                                        if (!string.IsNullOrEmpty(rec.paxFareProduct[0].paxFareDetail.codeShareDetails[0].company) && rec.paxFareProduct[0].paxFareDetail.codeShareDetails[0].company.Length == 2)
                                                        {
                                                            result1DArray[counter].ValidatingAirline = rec.paxFareProduct[0].paxFareDetail.codeShareDetails[0].company;
                                                        }
                                                    }
                                                }

                                                if (result1DArray[counter].Price == null)
                                                {
                                                    result1DArray[counter].Price = new PriceAccounts();
                                                }
                                                result1DArray[counter].FareBreakdown = new Fare[rec.paxFareProduct.Length];
                                                result1DArray[counter].BaseFare = 0;
                                                result1DArray[counter].TotalFare = 0;
                                                result1DArray[counter].Tax = 0;
                                                for (int j = 0; j < rec.paxFareProduct.Length; j++)
                                                {
                                                    Fare_MasterPricerTravelBoardSearchReplyRecommendationPaxFareProduct paxFareProduct = rec.paxFareProduct[j];
                                                    result1DArray[counter].FareBreakdown[j] = new Fare();
                                                    if (paxFareProduct.paxFareDetail != null)
                                                    {
                                                        int numPax = Convert.ToInt32(paxFareProduct.paxFareDetail.paxFareNum);
                                                    }

                                                    TravellerDetailsType5[] travellers = paxFareProduct.paxReference[0].traveller;


                                                    if (agentBaseCurrency != currency && exchangeRates != null)
                                                    {
                                                        rateOfExchange = exchangeRates[currency];
                                                    }
                                                    else
                                                    {
                                                        rateOfExchange = 1;
                                                    }
                                                    result1DArray[counter].Currency = currency;
                                                    result1DArray[counter].Price.SupplierCurrency = currency;
                                                    result1DArray[counter].Price.SupplierPrice += (decimal)(paxFareProduct.paxFareDetail.totalFareAmount) * (travellers.Length);
                                                    result1DArray[counter].Price.RateOfExchange = rateOfExchange;

                                                    double tax = Convert.ToDouble(paxFareProduct.paxFareDetail.totalTaxAmount);
                                                    double baseFare = Convert.ToDouble(paxFareProduct.paxFareDetail.totalFareAmount) - Convert.ToDouble(paxFareProduct.paxFareDetail.totalTaxAmount);
                                                    result1DArray[counter].FareBreakdown[j].TotalFare = Math.Round((((double)paxFareProduct.paxFareDetail.totalFareAmount) * (double)rateOfExchange), decimalValue) * travellers.Length;
                                                    result1DArray[counter].FareBreakdown[j].SellingFare = result1DArray[counter].FareBreakdown[j].TotalFare;

                                                    result1DArray[counter].FareBreakdown[j].BaseFare = Math.Round(((baseFare) * (double)rateOfExchange), decimalValue) * travellers.Length;
                                                    result1DArray[counter].FareBreakdown[j].SupplierFare = (double)(paxFareProduct.paxFareDetail.totalFareAmount) * travellers.Length;


                                                    result1DArray[counter].BaseFare += result1DArray[counter].FareBreakdown[j].BaseFare;
                                                    result1DArray[counter].Tax += Math.Round(((tax) * (double)rateOfExchange), decimalValue) * travellers.Length;
                                                    result1DArray[counter].TotalFare += result1DArray[counter].FareBreakdown[j].TotalFare;




                                                    //result1DArray[counter].FareBreakdown[j].TotalFare = (double)paxFareProduct.paxFareDetail.totalFareAmount * travellers.Length;
                                                    //result1DArray[counter].TotalFare += result1DArray[counter].FareBreakdown[j].TotalFare;
                                                    //decimal tax = paxFareProduct.paxFareDetail.totalTaxAmount;
                                                    //result1DArray[counter].FareBreakdown[j].BaseFare = result1DArray[counter].FareBreakdown[j].TotalFare - (double)(tax * travellers.Length);
                                                    //result1DArray[counter].BaseFare += result1DArray[counter].FareBreakdown[j].BaseFare;
                                                    result1DArray[counter].FareBreakdown[j].PassengerCount = travellers.Length;
                                                    result1DArray[counter].FareBreakdown[j].FareType = string.Empty;
                                                    string paxType = paxFareProduct.paxReference[0].ptc[0];
                                                    switch (paxType)
                                                    {
                                                        case "ADT":
                                                            result1DArray[counter].FareBreakdown[j].PassengerType = PassengerType.Adult;
                                                            break;
                                                        case "CH":
                                                        case "CNN":
                                                            result1DArray[counter].FareBreakdown[j].PassengerType = PassengerType.Child;
                                                            break;
                                                        case "INF":
                                                            result1DArray[counter].FareBreakdown[j].PassengerType = PassengerType.Infant;
                                                            break;
                                                    }

                                                    // Reading information common for all pax type.
                                                    if (j == 0)
                                                    {
                                                        if (paxFareProduct.fare != null)
                                                        {
                                                            Fare_MasterPricerTravelBoardSearchReplyRecommendationPaxFareProductFare[] fares = paxFareProduct.fare;
                                                            result1DArray[counter].TicketAdvisory = string.Empty;
                                                            for (int k = 0; k < fares.Length; k++)
                                                            {
                                                                Fare_MasterPricerTravelBoardSearchReplyRecommendationPaxFareProductFare fare = fares[k];
                                                                if (fare.pricingMessage != null && fare.pricingMessage.description != null)
                                                                {
                                                                    string[] desc = fare.pricingMessage.description;
                                                                    if (desc.Length > 0)
                                                                    {
                                                                        string advice = string.Empty;
                                                                        for (int l = 0; l < desc.Length; l++)
                                                                        {
                                                                            if (l > 0)
                                                                            {
                                                                                advice += " ";
                                                                            }
                                                                            advice += desc[l];
                                                                        }
                                                                        if (k > 0)
                                                                        {
                                                                            result1DArray[counter].TicketAdvisory += "\n";
                                                                        }
                                                                        result1DArray[counter].TicketAdvisory += advice;
                                                                        Match match = Regex.Match(advice, "LAST TKT DTE");
                                                                        if (match.Success)
                                                                        {
                                                                            Match dateMatch = Regex.Match(advice, @"[\d]{2}[a-zA-Z]{3}[\d]{2}");
                                                                            if (dateMatch.Success)
                                                                            {
                                                                                result1DArray[counter].LastTicketDate = dateMatch.Value;
                                                                            }
                                                                        }
                                                                        // Reading Non-Refundable
                                                                        match = Regex.Match(advice, "NON-REFUNDABLE");
                                                                        if (match.Success)
                                                                        {
                                                                            result1DArray[counter].NonRefundable = true;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        result1DArray[counter].FareRules = new List<FareRule>();
                                                        if (paxFareProduct.fareDetails != null)
                                                        {
                                                            Fare_MasterPricerTravelBoardSearchReplyRecommendationPaxFareProductFareDetails[] fareDetails = paxFareProduct.fareDetails;
                                                            for (int k = 0; k < fareDetails.Length; k++)
                                                            {
                                                                Fare_MasterPricerTravelBoardSearchReplyRecommendationPaxFareProductFareDetails fareDetail = fareDetails[k];
                                                                if (fareDetail.segmentRef != null && !string.IsNullOrEmpty(fareDetail.segmentRef.segRef))
                                                                {
                                                                    string segRef = fareDetail.segmentRef.segRef;
                                                                    int index = Convert.ToInt16(segRef) - 1;
                                                                    if (fareDetail.groupOfFares != null)
                                                                    {
                                                                        Fare_MasterPricerTravelBoardSearchReplyRecommendationPaxFareProductFareDetailsGroupOfFares[] groupOfFares = fareDetail.groupOfFares;
                                                                        for (int p = 0; p < groupOfFares.Length; p++)
                                                                        {
                                                                            Fare_MasterPricerTravelBoardSearchReplyRecommendationPaxFareProductFareDetailsGroupOfFares groupOfFare = groupOfFares[p];
                                                                            if (groupOfFare.productInformation != null)
                                                                            {
                                                                                if (groupOfFare.productInformation.cabinProduct != null && !string.IsNullOrEmpty(groupOfFare.productInformation.cabinProduct.rbd))
                                                                                {
                                                                                    result1DArray[counter].Flights[index][p].BookingClass = groupOfFare.productInformation.cabinProduct.rbd;
                                                                                }
                                                                                FareRule fareRule = new FareRule();

                                                                                if (groupOfFare.productInformation.fareProductDetail != null && !string.IsNullOrEmpty(groupOfFare.productInformation.fareProductDetail.fareBasis))
                                                                                {
                                                                                    fareRule.FareBasisCode = groupOfFare.productInformation.fareProductDetail.fareBasis;
                                                                                }
                                                                                fareRule.Airline = result1DArray[counter].Flights[index][p].Airline;
                                                                                fareRule.Destination = result1DArray[counter].Flights[index][p].Destination.CityCode;
                                                                                fareRule.Origin = result1DArray[counter].Flights[index][p].Origin.CityCode;

                                                                                if (!string.IsNullOrEmpty(groupOfFare.productInformation.breakPoint))
                                                                                {
                                                                                    fareRule.FareRestriction = groupOfFare.productInformation.breakPoint.Trim();
                                                                                }
                                                                                result1DArray[counter].FareRules.Add(fareRule);
                                                                                if (result1DArray[counter].FareType == null || result1DArray[counter].FareType.Length == 0)
                                                                                {
                                                                                    if (groupOfFare.productInformation.fareProductDetail != null && groupOfFare.productInformation.fareProductDetail.fareType != null && groupOfFares[p].productInformation.fareProductDetail.fareType.Length > 0)
                                                                                    {
                                                                                        List<string> fareTypes = new List<string>();
                                                                                        for (int z = 0; z < groupOfFare.productInformation.fareProductDetail.fareType.Length; z++)
                                                                                        {
                                                                                            fareTypes.Add(groupOfFare.productInformation.fareProductDetail.fareType[z]);
                                                                                        }
                                                                                        if (fareTypes != null && fareTypes.Count > 0)
                                                                                        {
                                                                                            if (fareTypes.Contains("RP"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "RP";// Published fares
                                                                                            }
                                                                                            else if (fareTypes.Contains("ET"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "ET";//ELECTRONIC TICKET ONLY
                                                                                            }
                                                                                            else if (fareTypes.Contains("MSP"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "MSP";//MSP Fares
                                                                                            }
                                                                                            else if (fareTypes.Contains("PT"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "PT";//Paper Ticket only
                                                                                            }
                                                                                            else if (fareTypes.Contains("RA"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "RA";//ATPCO nego fares - CAT35
                                                                                            }
                                                                                            else if (fareTypes.Contains("RB"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "RB";//ATPCO nego corporate fares
                                                                                            }
                                                                                            else if (fareTypes.Contains("RC"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "RC";//Amadeus Nego Corporate Fares
                                                                                            }
                                                                                            else if (fareTypes.Contains("RD"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "RD";//Dynamic Discounted Fares
                                                                                            }
                                                                                            else if (fareTypes.Contains("RN"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "RN";//Amadeus Nego Fares
                                                                                            }
                                                                                            else if (fareTypes.Contains("RV"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "RV";//ATPCO privat fares - CAT15
                                                                                            }
                                                                                            else if (fareTypes.Contains("RX"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "RX";//Corporate ATPCO private fares - CAT15
                                                                                            }
                                                                                            else if (fareTypes.Contains("RZ"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "RZ";//Corporate Dynamic Discounted Fares
                                                                                            }
                                                                                            else if (fareTypes.Contains("SF"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "SF";//SF Fares
                                                                                            }
                                                                                            else if (fareTypes.Contains("W"))
                                                                                            {
                                                                                                result1DArray[counter].FareType = "W";//WebFares
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }

                                                                                if (groupOfFare.productInformation.cabinProduct != null && !string.IsNullOrEmpty(groupOfFare.productInformation.cabinProduct.avlStatus))
                                                                                {
                                                                                    int avb = Convert.ToInt32(groupOfFare.productInformation.cabinProduct.avlStatus);
                                                                                    if (avb < request.AdultCount + request.ChildCount + request.InfantCount)
                                                                                    {
                                                                                        result1DArray[counter].Flights[index][p].FlightStatus = FlightStatus.Waitlisted;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        result1DArray[counter].Flights[index][p].FlightStatus = FlightStatus.Confirmed;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            // result1DArray[counter].Price.SupplierCurrency = currency;
                                            //result1DArray[counter].Price.SupplierPrice += (decimal)(tax + baseFare) * (passengerCount);
                                            //  result1DArray[counter].Price.RateOfExchange = rateOfExchange;
                                            //  result1DArray[counter].Currency = currency;
                                            //  result1DArray[counter].Tax = result1DArray[counter].TotalFare - result1DArray[counter].BaseFare;
                                            counter++;
                                        }//SegmentFlightRef loop
                                    }//SegmentFlightRef is not null
                                }//Recommendation loop
                                Results.AddRange(result1DArray);
                                //TODO: Filter refundable or non refundable results based on filter using delegate
                            }//Recommendation is not null
                        }//flightIndex is not null                        
                    }//ValidResponse

                    //TODO: Filters to be implemented
                    //1. Stops (Non stop, One stop, two plus stops
                    //2. Refundable or non-refundable
                    //3. Cabin class
                    //4. Preferred Airline
                    try
                    {
                        //if (searchBySegments)
                        {
                            XmlSerializer ser = new XmlSerializer(typeof(SearchResult[]));
                            StreamWriter sw = new StreamWriter(xmlPath + sessionId + "_" + appUserId + "_AmadeusSearchResults_" + request.Segments[0].Origin +"_"+ request.Segments[0].Destination + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml");
                            ser.Serialize(sw, Results.ToArray());
                            sw.Close();
                        }
                        //else
                        //{
                        //    XmlSerializer ser = new XmlSerializer(typeof(SearchResult[]));
                        //    StreamWriter sw = new StreamWriter(xmlPath + sessionId + "_" + appUserId + "_AmadeusSearchResults_" +request.Segments[0].Origin + "_" + request.Segments[0].Destination + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xml");
                        //    ser.Serialize(sw, Results.ToArray());
                        //    sw.Close();
                        //}
                        
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Search, Severity.Normal, appUserId, "(Amadeus)Failed to write Search Results. Reason : " + ex.ToString(), "", "Amadeus");
                    }

                }//Response != null
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Amadeus)Failed to get Search Response. Reason: " + ex.ToString(), "");
            }
            
            if (Results != null && Results.Count > 0 && request.Type == SearchType.Return)
            {
                for (int i = 0; i < Results.Count; i++)
                {
                    if (Results[i] != null && Results[i].Flights != null && Results[i].Flights.Length > 0 && Results[i].Flights[1] != null && Results[i].Flights[1].Length > 0)
                    {
                        for (int j = 0; j < Results[i].Flights[1].Length; j++)
                        {
                            if (Results[i].Flights[1][j] != null)
                            {
                                Results[i].Flights[1][j].Group = 1;
                            }
                        }
                    }
                }
            }
            return Results.ToArray();
        }

        #endregion

        #region 02:Price Check(Re-Price)
        /// <summary>
        /// The InformativePricingWithoutPNR function provided in the Fare interface is used to price informatively an itinerary without any PNR.
        /// Since there is no PNR, all the information about the passengers and the itinerary is provided in the query.
        /// Note that, if a PNR exists, it is neither taken into account nor updated. No pricing record (TST) can be created to store the results.
        /// </summary>
        /// <param name="searchResult"></param>
        /// <param name="request"></param>
        public void Fare_InformativePricingWithoutPNR(ref SearchResult searchResult, SearchRequest request)
        {
            try
            {
                /* The Fare_PriceUpsellWithoutPNR query is made of 3 groups:

                 passengersGroup: used to give details regarding passengers to price.
                 segmentGroup: used to give details regarding itinerary.
                 pricingOptionGroup: used to give detaild about pricing options to apply.Can be repeated up to 999 times.*/

                GenerateHeader();

                //Fare_InformativePricingWithoutPNR structure
                //0.originatorGroup -- C
                //1.passengersGroup --M -- Passengers description
                //2.segmentGroup -- M  -- Itinerary description and pricing information related at flight level.
                //3.pricingOptionGroup --C

                Session session = hSession.Session;
                TransactionFlowLinkType link = hLink.Link;

                hSession.handleSession(SessionHandler.TransactionStatusCode.Start);
                hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.New);
                //hAddressing.update();

                int t = 1; //SegmentRepition Control Increment Varaiable.
                int p = 1; // travellersID[m].measurementValue Increment Varaiable  .
                Fare_InformativePricingWithoutPNR resWithoutPNR = new Fare_InformativePricingWithoutPNR();
                Fare_InformativePricingWithoutPNRPassengersGroup[] paxGroups = new Fare_InformativePricingWithoutPNRPassengersGroup[searchResult.FareBreakdown.Length];

                #region passengersGroup
                if (searchResult.FareBreakdown.Length > 0)
                {
                    //1.passengersGroup --M -- Passengers description
                    for (int k = 0; k < searchResult.FareBreakdown.Length; k++)
                    {
                        if (searchResult.FareBreakdown[k] != null)
                        {
                            //Fare_InformativePricingWithoutPNR/passengersGroup structure
                            // 1.1segmentRepetitionControl -- M -- Contains: * Number of passengers in the group * Group tattoo
                            // 1.2travellersID -- C -- Passengers' tattoos provided by the carrier in case of LCC pricing with U2. NOT USED FOR FSC.
                            // 1.3discountPtc -- C --PTC
                            paxGroups[k] = new Fare_InformativePricingWithoutPNRPassengersGroup();
                            string paxRef = string.Empty;
                            if (searchResult.FareBreakdown[k].PassengerType == PassengerType.Adult)
                            {
                                paxRef = "ADT";
                            }
                            if (searchResult.FareBreakdown[k].PassengerType == PassengerType.Child)
                            {
                                paxRef = "CH";
                            }
                            if (searchResult.FareBreakdown[k].PassengerType == PassengerType.Infant)
                            {
                                paxRef = "INF";
                            }

                            ////Fare_InformativePricingWithoutPNR/passengersGroup/segmentRepetitionControl structure
                            //1.1.1segmentControlDetails structure.
                            //quantity -- C -- Numeric value of a quantity.
                            //numberOfUnits --C  -- Number of units of a certain type.

                            paxGroups[k].segmentRepetitionControl = new SegmentRepetitionControlDetailsTypeI[1];
                            paxGroups[k].segmentRepetitionControl[0] = new SegmentRepetitionControlDetailsTypeI();
                            paxGroups[k].segmentRepetitionControl[0].quantity = t.ToString();
                            paxGroups[k].segmentRepetitionControl[0].numberOfUnits = searchResult.FareBreakdown[k].PassengerCount.ToString();

                            //Fare_InformativePricingWithoutPNR/passengersGroup/travellersID structure
                            //1.2.1measurementValue --C -- Value of the measured unit.
                            paxGroups[k].travellersID = new SpecificTravellerDetailsTypeI[searchResult.FareBreakdown[k].PassengerCount];
                            for (int m = 0; m < searchResult.FareBreakdown[k].PassengerCount; m++)
                            {
                                paxGroups[k].travellersID[m] = new SpecificTravellerDetailsTypeI();
                                paxGroups[k].travellersID[m].measurementValue = (p).ToString();
                                p++;
                            }

                            //Fare_InformativePricingWithoutPNR / passengersGroup / discountPtc Structure
                            //1.3.1valueQualifier -- C -- Indication of the objective of number of units information.
                            //1.3.2fareDetails -- C 
                            //1.3.4identityNumber --C --Unique number affixed by the manufacturer to individual pieces of products for identification purposes.
                            //Fare_InformativePricingWithoutPNR / passengersGroup / discountPtc/fareDetails
                            //1.3.2.1qualifier -- C --Indication of the objective of number of units information.
                            paxGroups[k].discountPtc = new FareInformationTypeI();
                            paxGroups[k].discountPtc.valueQualifier = paxRef;
                            if (paxRef == "INF")
                            {
                                paxGroups[k].discountPtc.fareDetails = new FareDetailsTypeI();
                                paxGroups[k].discountPtc.fareDetails.qualifier = "766"; //766  -- Infant without seat
                            }
                            t++;
                        }
                    }
                    resWithoutPNR.passengersGroup = paxGroups;
                }
                #endregion

                #region segmentGroup
                //2.segmentGroup -- M  -- Itinerary description and pricing information related at flight level.
                if (searchResult.Flights.Length > 0)
                {
                    //Fare_InformativePricingWithoutPNR / segmentGroup/ 
                    //2.1 --   segmentInformation -- M -- Convey the information related to a segment (company, flight number, origin, destination...).
                    //2.2 -- additionnalSegmentDetails --C -- Used for technical stops, even if it is currently deprecated.
                    //2.3 --    inventory -- C--	To store the flight inventory (opened classes and number of remaining seats) if known.

                    int segmentsLength = 0;
                    for (int i = 0; i < searchResult.Flights.Length; i++)
                    {
                        segmentsLength += searchResult.Flights[i].Length;
                    }
                    int v = 0;//Segments loop iteration variable
                    Fare_InformativePricingWithoutPNRSegmentGroup[] segmentGroup = new Fare_InformativePricingWithoutPNRSegmentGroup[segmentsLength];
                    for (int i = 0; i < searchResult.Flights.Length; i++)
                    {
                        for (int j = 0; j < searchResult.Flights[i].Length; j++)
                        {
                            //2.1Fare_InformativePricingWithoutPNR / segmentGroup / segmentInformation Structure
                            //2.1.1 -- flightDate --C
                            //2.1.2 -- boardPointDetails --C
                            //2.1.3 -- offpointDetails --C
                            //2.1.4 -- companyDetails --C
                            //2.1.5 -- flightIdentification --C
                            //2.1.6 -- flightTypeDetails --C
                            //2.1.7 -- itemNumber -- Serial number designating each separate item within a series of articles.
                            //2.1.8 -- specialSegment -- Identifies the value to be attributed to indicators required by the processing system.

                            //As per the logs Need to pass -- flightDate,boardPointDetails,offpointDetails,companyDetails,flightIdentification
                            segmentGroup[v] = new Fare_InformativePricingWithoutPNRSegmentGroup();

                            //2.1Fare_InformativePricingWithoutPNR / segmentGroup / segmentInformation
                            segmentGroup[v].segmentInformation = new TravelProductInformationTypeI2();

                            //2.1.1 -- flightDate --C
                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/flightDate structure
                            //departureDate -- C -- A date that applies to a means of transport or a traveller.
                            //departureTime --C--A time that applies to a means of transport or a traveller.
                            //arrivalDate --C --An additional date that applies to a means of transport or a traveller.
                            //arrivalTime --C --An additional time that applies to a means of transport or a traveller.
                            segmentGroup[v].segmentInformation.flightDate = new ProductDateTimeTypeI4();
                            segmentGroup[v].segmentInformation.flightDate.departureDate = searchResult.Flights[i][j].DepartureTime.ToString("ddMMyy");

                            //2.1.2 -- boardPointDetails --C
                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/boardPointDetails structure
                            //trueLocationId -- C --Identification of the name of place/location, other than 3164 City name.
                            // ARNK --ARNK (for RTG use only)
                            //ZZZ -- ZZZ (used to designate all cities)
                            segmentGroup[v].segmentInformation.boardPointDetails = new LocationTypeI_217754C();
                            segmentGroup[v].segmentInformation.boardPointDetails.trueLocationId = searchResult.Flights[i][j].Origin.AirportCode;

                            //2.1.2 -- offpointDetails --C
                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/offpointDetails structure
                            //trueLocationId -- C --Identification of the name of place/location, other than 3164 City name.
                            // ARNK --ARNK (for RTG use only)
                            //ZZZ -- ZZZ (used to designate all cities)
                            segmentGroup[v].segmentInformation.offpointDetails = new LocationTypeI_217754C();
                            segmentGroup[v].segmentInformation.offpointDetails.trueLocationId = searchResult.Flights[i][j].Destination.AirportCode;

                            //2.1.4 -- companyDetails --C
                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/companyDetails structure
                            //marketingCompany -- C -- The coded description of supplier of a service or product, i.e. airline designator code.
                            //operatingCompany --C -- The coded description of supplier of a service or product, i.e. airline designator code.

                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/companyDetails/marketingCompany
                            //Company identification codesets
                            //7CC ==Industry Car Rental Companies
                            //7HH ==Industry Hotel Chains

                            segmentGroup[v].segmentInformation.companyDetails = new CompanyIdentificationTypeI_217756C();
                            segmentGroup[v].segmentInformation.companyDetails.marketingCompany = searchResult.Flights[i][j].Airline;

                            //2.1.5 -- flightIdentification --C
                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/flightIdentification  structure
                            //flightNumber -- M --Unique reference given by the supplier to identify a product.
                            //bookingClass --C --A code from an industry code list which provides specific data about a product characteristic.
                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/flightIdentification/bookingClass
                            //Characteristic identification codesets
                            //1 ==Request all non-displayable RBD's
                            //2 ==Request all RBD's including non-displayable RBD's.
                            //3 ==Request all Frequent Flyer Program Award Classes
                            //4 ==Total number of seats in the allotment
                            //5 ==Number of seats sold in the allotment
                            //6 == Number of seats unsold in the allotment
                            //700 == Request is expanded to include nonmatching connections

                            segmentGroup[v].segmentInformation.flightIdentification = new ProductIdentificationDetailsTypeI2();
                            segmentGroup[v].segmentInformation.flightIdentification.flightNumber = searchResult.Flights[i][j].FlightNumber;
                            segmentGroup[v].segmentInformation.flightIdentification.bookingClass = searchResult.Flights[i][j].BookingClass;
                            v++;
                        }
                    }
                    resWithoutPNR.segmentGroup = segmentGroup;
                }
                #endregion

                #region pricingOptionGroup
                //3.pricingOptionGroup -- C
                //Fare_InformativePricingWithoutPNR/pricingOptionGroup structure
                //3.1 pricingOptionKey -- M
                //3.2 optionDetail --C
                //3.3 pricingOptionKey-- M   
                //3.4 optionDetail-- C
                //3.5 carrierInformation-- C   
                //3.6 currency-- C   
                //3.7 penDisInformation-- C   
                //3.8 monetaryInformation-- C   
                //3.9 taxInformation-- C   
                //3.10 dateInformation-- C  
                //3.11 frequentFlyerInformation-- C
                //3.12 formOfPaymentInformation-- C  
                //3.13 locationInformation-- C
                //3.14 paxSegTstReference-- C

                //Fare_InformativePricingWithoutPNR/pricingOptionGroup/pricingOptionKey
                //pricingOptionKey --M -- Give Key name of a pricing option 

                /* Fare_InformativePricingWithoutPNR / pricingOptionGroup / pricingOptionKey / pricingOptionKey
                  Attribute Key codesets
                  Value   Description
                  AC  Add Country taxes
                  AT  Add Tax
                  AWD AWarD
                  BND Bound Input
                  CC  Controlling Carrier Override
                  CON Connection
                  DAT past DATe pricing
                  DIA Diagnostic Tool
                  DO booking Date Override
                  ET Exempt Taxes
                  FBA Fare BAsis simple override
                  FBL Fare Basis force override
                  FBP Force Break Point
                  FCO Fare Currency Override
                  FCS Fare Currency Selection
                  FOP Form Of Payment
                  GRI Global Route Indicator
                  IP Instant Pricing
                  MA  Mileage Accrual
                  MBT Fare amount override with M/BT
                  MC  Miles and Cash(Pricing by Points)
                  MIT Fare amount override with M/IT
                  NBP No BreakPoint
                  NF No ticketing Fee
                  NOP No Option
                  OBF OB Fees(include and/or exclude)
                  PAX Passenger discount/PTC
                  PFF Pricing by Fare Family
                  PL Pricing Logic
                  POS Point Of Sale
                  POT Point Of Ticketing override
                  PRM expanded PaRaMeters
                  PSR PSR
                  PTA Point of Turnaround
                  PTC PTC only
                  RC Corporate negociated fares
                  RLI Return LIst of fare
                  RLO Return LOwest possible fare
                  RN Negociated fare
                  RP  Published Fares
                  RU Unifares
                  RW Corporate Unifares
                  STO Stopover
                  TKT TicKet Type
                  VC Validating Carrier
                  WC  Withold Country taxes
                  WQ  Withold Q surcharges
                  WT  Withold Tax
                  ZAP ZAP-off*/

                resWithoutPNR.pricingOptionGroup = new Fare_InformativePricingWithoutPNRPricingOptionGroup[1];
                resWithoutPNR.pricingOptionGroup[0] = new Fare_InformativePricingWithoutPNRPricingOptionGroup();
                resWithoutPNR.pricingOptionGroup[0].pricingOptionKey = new PricingOptionKey();
                resWithoutPNR.pricingOptionGroup[0].pricingOptionKey.pricingOptionKey = searchResult.FareType;

                #endregion

                Fare_InformativePricingWithoutPNRRequest pnrRequest = new Fare_InformativePricingWithoutPNRRequest(session, link, hSecurity.getHostedUser(), resWithoutPNR);
                //Serialize pnrRequest
                try
                {
                    string filepath = xmlPath + sessionId + "_" + appUserId + "_Fare_InformativePricingWithoutPNRRequest_" + request.Segments[0].Origin + "_" + request.Segments[0].Destination + "_" +DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pnrRequest.GetType());
                    StreamWriter sw = new StreamWriter(filepath);
                    xsr.Serialize(sw, pnrRequest);
                    sw.Close();
                }
                catch { }
                Fare_InformativePricingWithoutPNRReply response = client.Fare_InformativePricingWithoutPNR(ref session, ref link, hSecurity.getHostedUser(), resWithoutPNR);
                hSession.Session = session;
                hLink.Link = link;
                hSession.handleSession(SessionHandler.TransactionStatusCode.End);
                hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.None);
                //hAddressing.update();

                if (response != null)
                {
                    // hSession.Session = session;
                    // hLink.Link = link;
                    // hSession.handleSession(SessionHandler.TransactionStatusCode.End);
                    // hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.None);
                    Fare_InformativePricingWithoutPNRResponse pnrResponse = new Fare_InformativePricingWithoutPNRResponse(session, link, response);
                    //Serialize pnrResponse
                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_Fare_InformativePricingWithoutPNRResponse_" + request.Segments[0].Origin + "_" + request.Segments[0].Destination + "_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pnrResponse.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, pnrResponse);
                        sw.Close();
                    }
                    catch { }

                    //Form the result set
                    bool validResponse = true;
                    string errorMessage = string.Empty;
                    Fare_InformativePricingWithoutPNRReply pnrReply = pnrResponse.Fare_InformativePricingWithoutPNRReply;
                    if (pnrReply.errorGroup != null)
                    {
                        ErrorGroupType3 err = pnrReply.errorGroup;
                        if (err.errorOrWarningCodeDetails != null && err.errorOrWarningCodeDetails.errorDetails != null && !string.IsNullOrEmpty(err.errorOrWarningCodeDetails.errorDetails.errorCode))
                        {
                            errorMessage = "Error Code:" + err.errorOrWarningCodeDetails.errorDetails.errorCode;
                        }
                        if (err.errorOrWarningCodeDetails != null && err.errorOrWarningCodeDetails.errorDetails != null && !string.IsNullOrEmpty(err.errorOrWarningCodeDetails.errorDetails.errorCategory))
                        {
                            errorMessage += ";Error Category:" + err.errorOrWarningCodeDetails.errorDetails.errorCategory;
                        }
                        if (err.errorWarningDescription != null && err.errorWarningDescription.freeText.Length > 0)
                        {
                            errorMessage += ";Error Description:";
                            for (int m = 0; m < err.errorWarningDescription.freeText.Length; m++)
                            {
                                if (!string.IsNullOrEmpty(err.errorWarningDescription.freeText[m]))
                                {
                                    errorMessage += err.errorWarningDescription.freeText[m];
                                }
                            }
                        }
                        validResponse = false;
                        throw new Exception(errorMessage);
                    }
                    if (validResponse)
                    {
                        //Fare_InformativePricingWithoutPNRReply structure
                        //1.messageDetails -- C //Contains general information about the message, especially the use case. Tells if the request was correctly performed of not
                        //2. errorGroup --C -- If there is any error
                        //3.mainGroup --C
                        if (pnrReply.messageDetails != null
                            && !string.IsNullOrEmpty(pnrReply.messageDetails.responseType)
                            && pnrReply.messageDetails.responseType.ToUpper() == "A")
                        {
                            //ResponseType -- A -- OK
                            //3.mainGroup --C
                            //3.0 --  convertionRate -- C -- Convertion rates and currency information.
                            //3.1 -- dummySegment - M 
                            //3.2 --  generalIndicatorsGroup -- C
                            //3.3 --  pricingGroupLevelGroup -- C

                            if (pnrReply.mainGroup != null && pnrReply.mainGroup.pricingGroupLevelGroup.Length > 0)
                            {

                                searchResult.BaseFare = 0;
                                searchResult.TotalFare = 0;
                                searchResult.Tax = 0;

                                Fare_InformativePricingWithoutPNRReplyMainGroupPricingGroupLevelGroup[] pricingGroupLevel = pnrReply.mainGroup.pricingGroupLevelGroup;

                                Fare_InformativePricingWithoutPNRReplyMainGroupPricingGroupLevelGroup adultPricing = null;
                                Fare_InformativePricingWithoutPNRReplyMainGroupPricingGroupLevelGroup childPricing = null;
                                Fare_InformativePricingWithoutPNRReplyMainGroupPricingGroupLevelGroup infantPricing = null;

                                if (request.AdultCount > 0)
                                {
                                    adultPricing = pricingGroupLevel[0];
                                }
                                if (request.ChildCount > 0)
                                {
                                    childPricing = pricingGroupLevel[1];
                                }
                                //Infant-- if both children and infants exists in the request.
                                if (request.ChildCount > 0 && request.InfantCount > 0)
                                {
                                    infantPricing = pricingGroupLevel[2];
                                }
                                //If only infant exists in the rquest.
                                else if (request.ChildCount == 0 && request.InfantCount > 0)
                                {
                                    infantPricing = pricingGroupLevel[1];
                                }
                                if (searchResult.FareBreakdown != null && searchResult.FareBreakdown.Length > 0)
                                {
                                    searchResult.BaseFare = 0;
                                    searchResult.Tax = 0;
                                    searchResult.TotalFare = 0;
                                    searchResult.Price.SupplierPrice = 0;

                                    for (int k = 0; k < searchResult.FareBreakdown.Length; k++)
                                    {
                                        if (searchResult.FareBreakdown[k] != null)
                                        {
                                            if (searchResult.FareBreakdown[k].PassengerType == PassengerType.Adult && adultPricing != null)
                                            {
                                                double adultBaseFare = 0;
                                                double totalAdultFare = 0;
                                                string currency = string.Empty;

                                                Fare_InformativePricingWithoutPNRReplyMainGroupPricingGroupLevelGroupFareInfoGroup fareInfoGroup = adultPricing.fareInfoGroup;
                                                //monetaryDetails
                                                if (fareInfoGroup.fareAmount != null && fareInfoGroup.fareAmount.monetaryDetails != null)
                                                {
                                                    //B--Base Fare
                                                    //E --Equivalent Fare
                                                    if (fareInfoGroup.fareAmount.monetaryDetails.typeQualifier == "B" || fareInfoGroup.fareAmount.monetaryDetails.typeQualifier == "E")
                                                    {
                                                        adultBaseFare = Convert.ToDouble(fareInfoGroup.fareAmount.monetaryDetails.amount);
                                                    }
                                                }
                                                //otherMonetaryDetails
                                                if (fareInfoGroup.fareAmount != null && fareInfoGroup.fareAmount.otherMonetaryDetails != null && fareInfoGroup.fareAmount.otherMonetaryDetails.Length > 0)
                                                {
                                                    MonetaryInformationDetailsType_223867C[] otherMonetaryDetails = fareInfoGroup.fareAmount.otherMonetaryDetails;
                                                    foreach (MonetaryInformationDetailsType_223867C detail in otherMonetaryDetails)
                                                    {
                                                        //B == Base Fare
                                                        //E == Equivalent Fare
                                                        //712 == Total fare amount

                                                        if (detail.typeQualifier == "B" || detail.typeQualifier == "E")
                                                        {
                                                            adultBaseFare = Convert.ToDouble(detail.amount);
                                                        }
                                                        else if (detail.typeQualifier == "712")
                                                        {
                                                            totalAdultFare = Convert.ToDouble(detail.amount);
                                                            currency = detail.currency;
                                                        }
                                                    }
                                                }


                                                if (agentBaseCurrency != currency && exchangeRates != null)
                                                {
                                                    rateOfExchange = exchangeRates[currency];
                                                }
                                                else
                                                {
                                                    rateOfExchange = 1;
                                                }
                                                searchResult.Currency = currency;
                                                searchResult.Price.SupplierCurrency = currency;
                                                searchResult.Price.SupplierPrice += (decimal)(totalAdultFare) * (request.AdultCount);
                                                searchResult.Price.RateOfExchange = rateOfExchange;


                                                double tax = (totalAdultFare - adultBaseFare);
                                                searchResult.FareBreakdown[k].TotalFare = Math.Round(((totalAdultFare) * (double)rateOfExchange), decimalValue) * request.AdultCount;
                                                searchResult.FareBreakdown[k].SellingFare = searchResult.FareBreakdown[k].TotalFare;
                                                searchResult.FareBreakdown[k].PassengerType = PassengerType.Adult;
                                                searchResult.FareBreakdown[k].PassengerCount = request.AdultCount;
                                                searchResult.FareBreakdown[k].BaseFare = Math.Round(((adultBaseFare) * (double)rateOfExchange), decimalValue) * request.AdultCount;
                                                searchResult.FareBreakdown[k].SupplierFare = (double)(totalAdultFare) * searchResult.FareBreakdown[k].PassengerCount;


                                                searchResult.BaseFare += searchResult.FareBreakdown[k].BaseFare;
                                                searchResult.Tax += Math.Round(((tax) * (double)rateOfExchange), decimalValue) * request.AdultCount;
                                                searchResult.TotalFare += searchResult.FareBreakdown[k].TotalFare;

                                            }
                                            else if (searchResult.FareBreakdown[k].PassengerType == PassengerType.Child && childPricing != null)
                                            {
                                                double childBaseFare = 0;
                                                double totalChildFare = 0;
                                                string currency = string.Empty;
                                                Fare_InformativePricingWithoutPNRReplyMainGroupPricingGroupLevelGroupFareInfoGroup fareInfoGroup = childPricing.fareInfoGroup;
                                                //monetaryDetails
                                                if (fareInfoGroup.fareAmount != null && fareInfoGroup.fareAmount.monetaryDetails != null)
                                                {
                                                    //B--Base Fare
                                                    //E --Equivalent Fare
                                                    if (fareInfoGroup.fareAmount.monetaryDetails.typeQualifier == "B" || fareInfoGroup.fareAmount.monetaryDetails.typeQualifier == "E")
                                                    {
                                                        childBaseFare = Convert.ToDouble(fareInfoGroup.fareAmount.monetaryDetails.amount);
                                                    }
                                                }
                                                //otherMonetaryDetails
                                                if (fareInfoGroup.fareAmount != null && fareInfoGroup.fareAmount.otherMonetaryDetails != null && fareInfoGroup.fareAmount.otherMonetaryDetails.Length > 0)
                                                {
                                                    MonetaryInformationDetailsType_223867C[] otherMonetaryDetails = fareInfoGroup.fareAmount.otherMonetaryDetails;
                                                    foreach (MonetaryInformationDetailsType_223867C detail in otherMonetaryDetails)
                                                    {
                                                        //B == Base Fare
                                                        //E == Equivalent Fare
                                                        //712 == Total fare amount

                                                        if (detail.typeQualifier == "B" || detail.typeQualifier == "E")
                                                        {
                                                            childBaseFare = Convert.ToDouble(detail.amount);
                                                        }
                                                        else if (detail.typeQualifier == "712")
                                                        {
                                                            totalChildFare = Convert.ToDouble(detail.amount);
                                                            currency = detail.currency;
                                                        }
                                                    }
                                                }
                                                if (agentBaseCurrency != currency && exchangeRates != null)
                                                {
                                                    rateOfExchange = exchangeRates[currency];
                                                }
                                                else
                                                {
                                                    rateOfExchange = 1;
                                                }
                                                searchResult.Currency = currency;
                                                searchResult.Price.SupplierCurrency = currency;
                                                searchResult.Price.SupplierPrice += (decimal)(totalChildFare) * (request.ChildCount);
                                                searchResult.Price.RateOfExchange = rateOfExchange;


                                                double tax = (totalChildFare - childBaseFare);
                                                searchResult.FareBreakdown[k].TotalFare = Math.Round(((totalChildFare) * (double)rateOfExchange), decimalValue) * request.ChildCount;
                                                searchResult.FareBreakdown[k].SellingFare = searchResult.FareBreakdown[k].TotalFare;
                                                searchResult.FareBreakdown[k].PassengerType = PassengerType.Child;
                                                searchResult.FareBreakdown[k].PassengerCount = request.ChildCount;
                                                searchResult.FareBreakdown[k].BaseFare = Math.Round(((childBaseFare) * (double)rateOfExchange), decimalValue) * request.ChildCount;
                                                searchResult.FareBreakdown[k].SupplierFare = (double)(totalChildFare) * searchResult.FareBreakdown[k].PassengerCount;


                                                searchResult.BaseFare += searchResult.FareBreakdown[k].BaseFare;
                                                searchResult.Tax += Math.Round(((tax) * (double)rateOfExchange), decimalValue) * request.ChildCount;
                                                searchResult.TotalFare += searchResult.FareBreakdown[k].TotalFare;
                                            }
                                            else if (searchResult.FareBreakdown[k].PassengerType == PassengerType.Infant && infantPricing != null)
                                            {
                                                double infantBaseFare = 0;
                                                double totalInfantFare = 0;
                                                string currency = string.Empty;
                                                Fare_InformativePricingWithoutPNRReplyMainGroupPricingGroupLevelGroupFareInfoGroup fareInfoGroup = infantPricing.fareInfoGroup;
                                                //monetaryDetails
                                                if (fareInfoGroup.fareAmount != null && fareInfoGroup.fareAmount.monetaryDetails != null)
                                                {
                                                    //B--Base Fare
                                                    //E --Equivalent Fare
                                                    if (fareInfoGroup.fareAmount.monetaryDetails.typeQualifier == "B" || fareInfoGroup.fareAmount.monetaryDetails.typeQualifier == "E")
                                                    {
                                                        infantBaseFare = Convert.ToDouble(fareInfoGroup.fareAmount.monetaryDetails.amount);
                                                    }
                                                }
                                                //otherMonetaryDetails
                                                if (fareInfoGroup.fareAmount != null && fareInfoGroup.fareAmount.otherMonetaryDetails != null && fareInfoGroup.fareAmount.otherMonetaryDetails.Length > 0)
                                                {
                                                    MonetaryInformationDetailsType_223867C[] otherMonetaryDetails = fareInfoGroup.fareAmount.otherMonetaryDetails;
                                                    foreach (MonetaryInformationDetailsType_223867C detail in otherMonetaryDetails)
                                                    {
                                                        //B == Base Fare
                                                        //E == Equivalent Fare
                                                        //712 == Total fare amount
                                                        if (detail.typeQualifier == "B" || detail.typeQualifier == "E")
                                                        {
                                                            infantBaseFare = Convert.ToDouble(detail.amount);
                                                        }
                                                        else if (detail.typeQualifier == "712")
                                                        {
                                                            totalInfantFare = Convert.ToDouble(detail.amount);
                                                            currency = detail.currency;
                                                        }
                                                    }
                                                }

                                                if (agentBaseCurrency != currency && exchangeRates != null)
                                                {
                                                    rateOfExchange = exchangeRates[currency];
                                                }
                                                else
                                                {
                                                    rateOfExchange = 1;
                                                }
                                                searchResult.Currency = currency;
                                                searchResult.Price.SupplierCurrency = currency;
                                                searchResult.Price.SupplierPrice += (decimal)(totalInfantFare) * (request.ChildCount);
                                                searchResult.Price.RateOfExchange = rateOfExchange;

                                                double tax = (totalInfantFare - infantBaseFare);
                                                searchResult.FareBreakdown[k].TotalFare = Math.Round(((totalInfantFare) * (double)rateOfExchange), decimalValue) * request.InfantCount;
                                                searchResult.FareBreakdown[k].SellingFare = searchResult.FareBreakdown[k].TotalFare;
                                                searchResult.FareBreakdown[k].PassengerType = PassengerType.Infant;
                                                searchResult.FareBreakdown[k].PassengerCount = request.InfantCount;
                                                searchResult.FareBreakdown[k].BaseFare = Math.Round(((infantBaseFare) * (double)rateOfExchange), decimalValue) * request.InfantCount;
                                                searchResult.FareBreakdown[k].SupplierFare = (double)(totalInfantFare) * searchResult.FareBreakdown[k].PassengerCount;


                                                searchResult.BaseFare += searchResult.FareBreakdown[k].BaseFare;
                                                searchResult.Tax += Math.Round(((tax) * (double)rateOfExchange), decimalValue) * request.InfantCount;
                                                searchResult.TotalFare += searchResult.FareBreakdown[k].TotalFare;

                                            }
                                        }
                                    }
                                }

                                SearchResult sr = searchResult;

                                string baggage = string.Empty;
                                searchResult.BaggageIncludedInFare = string.Empty;
                                //700 referes Kilo of weight
                               int n =  pnrReply.mainGroup.pricingGroupLevelGroup.Count();

                                //pnrReply.mainGroup.pricingGroupLevelGroup.ToList().ForEach(b =>
                                //{
                                //        b.fareInfoGroup.segmentLevelGroup.ToList().ForEach(d =>
                                //        {
                                //            baggage += string.IsNullOrEmpty( baggage) ? d.baggageAllowance.baggageDetails.freeAllowance + ( d.baggageAllowance.baggageDetails.quantityCode.Contains("700") ? " KG" : " Piece") :
                                //            "," + d.baggageAllowance.baggageDetails.freeAllowance + (d.baggageAllowance.baggageDetails.quantityCode.Contains("700") ? " KG" : " Piece");                                          

                                //        });


                                //});


                                pnrReply.mainGroup.pricingGroupLevelGroup.ToList().ForEach(b =>
                                {
                                    b.fareInfoGroup.segmentLevelGroup.ToList().ForEach(d =>
                                    {
                                        baggage += string.IsNullOrEmpty(baggage) ? d.baggageAllowance.baggageDetails.freeAllowance + (d.baggageAllowance.baggageDetails.quantityCode.Contains("700") ? " KG" : " Piece") :
                                        "," + d.baggageAllowance.baggageDetails.freeAllowance + (d.baggageAllowance.baggageDetails.quantityCode.Contains("700") ? " KG" : " Piece");

                                    });

                                    sr.BaggageIncludedInFare += string.IsNullOrEmpty(sr.BaggageIncludedInFare) ? baggage : "|" + baggage;
                                });

                               
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Amadeus)Failed to get informative pricing without PNR. Reason: " + ex.ToString(), "");
            }
        }
        #endregion

        #region 03:Mini Rules

        public bool Fare_InformativePricingWithoutPNR(SearchResult searchResult)
        {
            bool validResponse = true;
            try
            {
                /* The Fare_PriceUpsellWithoutPNR query is made of 3 groups:

                 passengersGroup: used to give details regarding passengers to price.
                 segmentGroup: used to give details regarding itinerary.
                 pricingOptionGroup: used to give detaild about pricing options to apply.Can be repeated up to 999 times.*/



                //Fare_InformativePricingWithoutPNR structure
                //0.originatorGroup -- C
                //1.passengersGroup --M -- Passengers description
                //2.segmentGroup -- M  -- Itinerary description and pricing information related at flight level.
                //3.pricingOptionGroup --C

                hSession.handleSession(SessionHandler.TransactionStatusCode.Start);
                hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.New);
                Session session = hSession.Session;
                TransactionFlowLinkType link = hLink.Link;
                //hAddressing.update();

                int t = 1; //SegmentRepition Control Increment Varaiable.
                int p = 1; // travellersID[m].measurementValue Increment Varaiable  .
                Fare_InformativePricingWithoutPNR resWithoutPNR = new Fare_InformativePricingWithoutPNR();
                Fare_InformativePricingWithoutPNRPassengersGroup[] paxGroups = new Fare_InformativePricingWithoutPNRPassengersGroup[searchResult.FareBreakdown.Length];

                #region passengersGroup
                if (searchResult.FareBreakdown.Length > 0)
                {
                    //1.passengersGroup --M -- Passengers description
                    for (int k = 0; k < searchResult.FareBreakdown.Length; k++)
                    {
                        if (searchResult.FareBreakdown[k] != null)
                        {
                            //Fare_InformativePricingWithoutPNR/passengersGroup structure
                            // 1.1segmentRepetitionControl -- M -- Contains: * Number of passengers in the group * Group tattoo
                            // 1.2travellersID -- C -- Passengers' tattoos provided by the carrier in case of LCC pricing with U2. NOT USED FOR FSC.
                            // 1.3discountPtc -- C --PTC
                            paxGroups[k] = new Fare_InformativePricingWithoutPNRPassengersGroup();
                            string paxRef = string.Empty;
                            if (searchResult.FareBreakdown[k].PassengerType == PassengerType.Adult)
                            {
                                paxRef = "ADT";
                            }
                            if (searchResult.FareBreakdown[k].PassengerType == PassengerType.Child)
                            {
                                paxRef = "CH";
                            }
                            if (searchResult.FareBreakdown[k].PassengerType == PassengerType.Infant)
                            {
                                paxRef = "INF";
                            }

                            ////Fare_InformativePricingWithoutPNR/passengersGroup/segmentRepetitionControl structure
                            //1.1.1segmentControlDetails structure.
                            //quantity -- C -- Numeric value of a quantity.
                            //numberOfUnits --C  -- Number of units of a certain type.

                            paxGroups[k].segmentRepetitionControl = new SegmentRepetitionControlDetailsTypeI[1];
                            paxGroups[k].segmentRepetitionControl[0] = new SegmentRepetitionControlDetailsTypeI();
                            paxGroups[k].segmentRepetitionControl[0].quantity = t.ToString();
                            paxGroups[k].segmentRepetitionControl[0].numberOfUnits = searchResult.FareBreakdown[k].PassengerCount.ToString();

                            //Fare_InformativePricingWithoutPNR/passengersGroup/travellersID structure
                            //1.2.1measurementValue --C -- Value of the measured unit.
                            paxGroups[k].travellersID = new SpecificTravellerDetailsTypeI[searchResult.FareBreakdown[k].PassengerCount];
                            for (int m = 0; m < searchResult.FareBreakdown[k].PassengerCount; m++)
                            {
                                paxGroups[k].travellersID[m] = new SpecificTravellerDetailsTypeI();
                                paxGroups[k].travellersID[m].measurementValue = (p).ToString();
                                p++;
                            }

                            //Fare_InformativePricingWithoutPNR / passengersGroup / discountPtc Structure
                            //1.3.1valueQualifier -- C -- Indication of the objective of number of units information.
                            //1.3.2fareDetails -- C 
                            //1.3.4identityNumber --C --Unique number affixed by the manufacturer to individual pieces of products for identification purposes.
                            //Fare_InformativePricingWithoutPNR / passengersGroup / discountPtc/fareDetails
                            //1.3.2.1qualifier -- C --Indication of the objective of number of units information.
                            paxGroups[k].discountPtc = new FareInformationTypeI();
                            paxGroups[k].discountPtc.valueQualifier = paxRef;
                            if (paxRef == "INF")
                            {
                                paxGroups[k].discountPtc.fareDetails = new FareDetailsTypeI();
                                paxGroups[k].discountPtc.fareDetails.qualifier = "766"; //766  -- Infant without seat
                            }
                            t++;
                        }
                    }
                    resWithoutPNR.passengersGroup = paxGroups;
                }
                #endregion

                #region segmentGroup
                //2.segmentGroup -- M  -- Itinerary description and pricing information related at flight level.
                if (searchResult.Flights.Length > 0)
                {
                    //Fare_InformativePricingWithoutPNR / segmentGroup/ 
                    //2.1 --   segmentInformation -- M -- Convey the information related to a segment (company, flight number, origin, destination...).
                    //2.2 -- additionnalSegmentDetails --C -- Used for technical stops, even if it is currently deprecated.
                    //2.3 --    inventory -- C--	To store the flight inventory (opened classes and number of remaining seats) if known.

                    int segmentsLength = 0;
                    for (int i = 0; i < searchResult.Flights.Length; i++)
                    {
                        segmentsLength += searchResult.Flights[i].Length;
                    }
                    int v = 0;//Segments loop iteration variable
                    Fare_InformativePricingWithoutPNRSegmentGroup[] segmentGroup = new Fare_InformativePricingWithoutPNRSegmentGroup[segmentsLength];
                    for (int i = 0; i < searchResult.Flights.Length; i++)
                    {
                        for (int j = 0; j < searchResult.Flights[i].Length; j++)
                        {
                            //2.1Fare_InformativePricingWithoutPNR / segmentGroup / segmentInformation Structure
                            //2.1.1 -- flightDate --C
                            //2.1.2 -- boardPointDetails --C
                            //2.1.3 -- offpointDetails --C
                            //2.1.4 -- companyDetails --C
                            //2.1.5 -- flightIdentification --C
                            //2.1.6 -- flightTypeDetails --C
                            //2.1.7 -- itemNumber -- Serial number designating each separate item within a series of articles.
                            //2.1.8 -- specialSegment -- Identifies the value to be attributed to indicators required by the processing system.

                            //As per the logs Need to pass -- flightDate,boardPointDetails,offpointDetails,companyDetails,flightIdentification
                            segmentGroup[v] = new Fare_InformativePricingWithoutPNRSegmentGroup();

                            //2.1Fare_InformativePricingWithoutPNR / segmentGroup / segmentInformation
                            segmentGroup[v].segmentInformation = new TravelProductInformationTypeI2();

                            //2.1.1 -- flightDate --C
                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/flightDate structure
                            //departureDate -- C -- A date that applies to a means of transport or a traveller.
                            //departureTime --C--A time that applies to a means of transport or a traveller.
                            //arrivalDate --C --An additional date that applies to a means of transport or a traveller.
                            //arrivalTime --C --An additional time that applies to a means of transport or a traveller.
                            segmentGroup[v].segmentInformation.flightDate = new ProductDateTimeTypeI4();
                            segmentGroup[v].segmentInformation.flightDate.departureDate = searchResult.Flights[i][j].DepartureTime.ToString("ddMMyy");

                            //2.1.2 -- boardPointDetails --C
                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/boardPointDetails structure
                            //trueLocationId -- C --Identification of the name of place/location, other than 3164 City name.
                            // ARNK --ARNK (for RTG use only)
                            //ZZZ -- ZZZ (used to designate all cities)
                            segmentGroup[v].segmentInformation.boardPointDetails = new LocationTypeI_217754C();
                            segmentGroup[v].segmentInformation.boardPointDetails.trueLocationId = searchResult.Flights[i][j].Origin.AirportCode;

                            //2.1.2 -- offpointDetails --C
                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/offpointDetails structure
                            //trueLocationId -- C --Identification of the name of place/location, other than 3164 City name.
                            // ARNK --ARNK (for RTG use only)
                            //ZZZ -- ZZZ (used to designate all cities)
                            segmentGroup[v].segmentInformation.offpointDetails = new LocationTypeI_217754C();
                            segmentGroup[v].segmentInformation.offpointDetails.trueLocationId = searchResult.Flights[i][j].Destination.AirportCode;

                            //2.1.4 -- companyDetails --C
                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/companyDetails structure
                            //marketingCompany -- C -- The coded description of supplier of a service or product, i.e. airline designator code.
                            //operatingCompany --C -- The coded description of supplier of a service or product, i.e. airline designator code.

                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/companyDetails/marketingCompany
                            //Company identification codesets
                            //7CC ==Industry Car Rental Companies
                            //7HH ==Industry Hotel Chains

                            segmentGroup[v].segmentInformation.companyDetails = new CompanyIdentificationTypeI_217756C();
                            segmentGroup[v].segmentInformation.companyDetails.marketingCompany = searchResult.Flights[i][j].Airline;

                            //2.1.5 -- flightIdentification --C
                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/flightIdentification  structure
                            //flightNumber -- M --Unique reference given by the supplier to identify a product.
                            //bookingClass --C --A code from an industry code list which provides specific data about a product characteristic.
                            //Fare_InformativePricingWithoutPNR/segmentGroup/segmentInformation/flightIdentification/bookingClass
                            //Characteristic identification codesets
                            //1 ==Request all non-displayable RBD's
                            //2 ==Request all RBD's including non-displayable RBD's.
                            //3 ==Request all Frequent Flyer Program Award Classes
                            //4 ==Total number of seats in the allotment
                            //5 ==Number of seats sold in the allotment
                            //6 == Number of seats unsold in the allotment
                            //700 == Request is expanded to include nonmatching connections

                            segmentGroup[v].segmentInformation.flightIdentification = new ProductIdentificationDetailsTypeI2();
                            segmentGroup[v].segmentInformation.flightIdentification.flightNumber = searchResult.Flights[i][j].FlightNumber;
                            segmentGroup[v].segmentInformation.flightIdentification.bookingClass = searchResult.Flights[i][j].BookingClass;
                            v++;
                        }
                    }
                    resWithoutPNR.segmentGroup = segmentGroup;
                }
                #endregion

                #region pricingOptionGroup
                //3.pricingOptionGroup -- C
                //Fare_InformativePricingWithoutPNR/pricingOptionGroup structure
                //3.1 pricingOptionKey -- M
                //3.2 optionDetail --C
                //3.3 pricingOptionKey-- M   
                //3.4 optionDetail-- C
                //3.5 carrierInformation-- C   
                //3.6 currency-- C   
                //3.7 penDisInformation-- C   
                //3.8 monetaryInformation-- C   
                //3.9 taxInformation-- C   
                //3.10 dateInformation-- C  
                //3.11 frequentFlyerInformation-- C
                //3.12 formOfPaymentInformation-- C  
                //3.13 locationInformation-- C
                //3.14 paxSegTstReference-- C

                //Fare_InformativePricingWithoutPNR/pricingOptionGroup/pricingOptionKey
                //pricingOptionKey --M -- Give Key name of a pricing option 

                /* Fare_InformativePricingWithoutPNR / pricingOptionGroup / pricingOptionKey / pricingOptionKey
                  Attribute Key codesets
                  Value   Description
                  AC  Add Country taxes
                  AT  Add Tax
                  AWD AWarD
                  BND Bound Input
                  CC  Controlling Carrier Override
                  CON Connection
                  DAT past DATe pricing
                  DIA Diagnostic Tool
                  DO booking Date Override
                  ET Exempt Taxes
                  FBA Fare BAsis simple override
                  FBL Fare Basis force override
                  FBP Force Break Point
                  FCO Fare Currency Override
                  FCS Fare Currency Selection
                  FOP Form Of Payment
                  GRI Global Route Indicator
                  IP Instant Pricing
                  MA  Mileage Accrual
                  MBT Fare amount override with M/BT
                  MC  Miles and Cash(Pricing by Points)
                  MIT Fare amount override with M/IT
                  NBP No BreakPoint
                  NF No ticketing Fee
                  NOP No Option
                  OBF OB Fees(include and/or exclude)
                  PAX Passenger discount/PTC
                  PFF Pricing by Fare Family
                  PL Pricing Logic
                  POS Point Of Sale
                  POT Point Of Ticketing override
                  PRM expanded PaRaMeters
                  PSR PSR
                  PTA Point of Turnaround
                  PTC PTC only
                  RC Corporate negociated fares
                  RLI Return LIst of fare
                  RLO Return LOwest possible fare
                  RN Negociated fare
                  RP  Published Fares
                  RU Unifares
                  RW Corporate Unifares
                  STO Stopover
                  TKT TicKet Type
                  VC Validating Carrier
                  WC  Withold Country taxes
                  WQ  Withold Q surcharges
                  WT  Withold Tax
                  ZAP ZAP-off*/

                resWithoutPNR.pricingOptionGroup = new Fare_InformativePricingWithoutPNRPricingOptionGroup[1];
                resWithoutPNR.pricingOptionGroup[0] = new Fare_InformativePricingWithoutPNRPricingOptionGroup();
                resWithoutPNR.pricingOptionGroup[0].pricingOptionKey = new PricingOptionKey();
                resWithoutPNR.pricingOptionGroup[0].pricingOptionKey.pricingOptionKey = searchResult.FareType;

                #endregion

                Fare_InformativePricingWithoutPNRRequest pnrRequest = new Fare_InformativePricingWithoutPNRRequest(session, link, hSecurity.getHostedUser(), resWithoutPNR);
                //Serialize pnrRequest
                try
                {
                    string filepath = xmlPath + sessionId + "_" + appUserId + "_Fare_InformativePricingWithoutPNRRequest_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pnrRequest.GetType());
                    StreamWriter sw = new StreamWriter(filepath);
                    xsr.Serialize(sw, pnrRequest);
                    sw.Close();
                }
                catch { }
                Fare_InformativePricingWithoutPNRReply response = client.Fare_InformativePricingWithoutPNR(ref session, ref link, hSecurity.getHostedUser(), resWithoutPNR);
                hSession.Session = session;
                hLink.Link = link;

                hSession.AfterReply(SessionHandler.TransactionStatusCode.Continue);
                hLink.AfterReply(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                //hAddressing.update();

                if (response != null)
                {
                    // hSession.Session = session;
                    // hLink.Link = link;
                    // hSession.handleSession(SessionHandler.TransactionStatusCode.End);
                    // hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.None);
                    Fare_InformativePricingWithoutPNRResponse pnrResponse = new Fare_InformativePricingWithoutPNRResponse(session, link, response);
                    //Serialize pnrResponse
                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_Fare_InformativePricingWithoutPNRResponse_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pnrResponse.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, pnrResponse);
                        sw.Close();
                    }
                    catch { }

                    //Form the result set

                    string errorMessage = string.Empty;
                    Fare_InformativePricingWithoutPNRReply pnrReply = pnrResponse.Fare_InformativePricingWithoutPNRReply;
                    if (pnrReply.errorGroup != null)
                    {
                        ErrorGroupType3 err = pnrReply.errorGroup;
                        if (err.errorOrWarningCodeDetails != null && err.errorOrWarningCodeDetails.errorDetails != null && !string.IsNullOrEmpty(err.errorOrWarningCodeDetails.errorDetails.errorCode))
                        {
                            errorMessage = "Error Code:" + err.errorOrWarningCodeDetails.errorDetails.errorCode;
                        }
                        if (err.errorOrWarningCodeDetails != null && err.errorOrWarningCodeDetails.errorDetails != null && !string.IsNullOrEmpty(err.errorOrWarningCodeDetails.errorDetails.errorCategory))
                        {
                            errorMessage += ";Error Category:" + err.errorOrWarningCodeDetails.errorDetails.errorCategory;
                        }
                        if (err.errorWarningDescription != null && err.errorWarningDescription.freeText.Length > 0)
                        {
                            errorMessage += ";Error Description:";
                            for (int m = 0; m < err.errorWarningDescription.freeText.Length; m++)
                            {
                                if (!string.IsNullOrEmpty(err.errorWarningDescription.freeText[m]))
                                {
                                    errorMessage += err.errorWarningDescription.freeText[m];
                                }
                            }
                        }
                        validResponse = false;
                    }

                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Search, Severity.High, appUserId, "(Amadeus)Failed to get informative pricing without PNR. Reason: " + ex.ToString(), "");
                throw ex;
            }
            return validResponse;
        }

        /// <summary>
        /// Retrieve Mini Rules for a specific fare recommendation returned by pricing/re-pricing
        /// </summary>
        /// <returns></returns>
        public  List<FareRule> GetFareRules(SearchResult result)
        {
            GenerateHeader();
            List<FareRule> FareRules = new List<FareRule>();
            try
            {
                /**********Start: Steps to retrive Fare Rules******************
                 * Step-1: Call Fare_InformativePricingWithoutPNR -- Start.
                 * Step-2: Call MiniRule_GetFromPricing -- In Series.
                 * Step-3: Secuirity_SignOut -- End.
                 *********Start: Steps to retrive Fare Rules*******************/

                if (Fare_InformativePricingWithoutPNR(result))
                {
                    hSession.handleSession(SessionHandler.TransactionStatusCode.Continue);
                    hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                    Session session = hSession.Session;
                    TransactionFlowLinkType link = hLink.Link;
                    //hAddressing.update();

                    MiniRule_GetFromPricingReply reply = new MiniRule_GetFromPricingReply();

                    ItemReferencesAndVersionsType7[] fareRuleRequest = new ItemReferencesAndVersionsType7[1];
                    fareRuleRequest[0] = new ItemReferencesAndVersionsType7();
                    //Code giving specific meaning to a reference segment or a reference number
                    fareRuleRequest[0].referenceType = "FRN";//Fare Recommendation Number
                    fareRuleRequest[0].uniqueReference = "ALL";

                    MiniRule_GetFromPricingRequest request = new MiniRule_GetFromPricingRequest(session, link, hSecurity.getHostedUser(), fareRuleRequest);
                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_MiniRule_GetFromPricingRequest_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new XmlSerializer(request.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, request);
                        sw.Close();

                    }
                    catch { }

                    reply = client.MiniRule_GetFromPricing(ref session, ref link, hSecurity.getHostedUser(), fareRuleRequest);

                    hSession.Session = session;
                    hLink.Link = link;
                    hSession.AfterReply(SessionHandler.TransactionStatusCode.Continue);
                    hLink.AfterReply(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                    //hAddressing.update();
                    MiniRule_GetFromPricingResponse response = new MiniRule_GetFromPricingResponse(session, link, reply);

                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_MiniRule_GetFromPricingResponse_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new XmlSerializer(response.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, response);
                        sw.Close();
                    }
                    catch { }
                    SignOut();
                    //MiniRule_GetFromPricingReply Structure
                    //1 --  responseDetails--M -- Provides the output status of the retrieve Mini-Rules
                    //2 --errorWarningGroup--C -- Error group
                    //3 --mnrByFareRecommendation	--C --Mini-Rules for a Fare Recommendation

                    //MiniRule_GetFromPricingReply/responseDetails/statusCode
                    //N --	Recoverable error
                    //OK processed. Further data in further segments follow
                    bool validMiniRuleReponse = ValidateMiniRuleReponse(reply);
                    if (validMiniRuleReponse && reply.responseDetails != null && reply.responseDetails.statusCode == "O")
                    {


                        foreach (MiniRule_GetFromPricingReplyMnrByFareRecommendationFareComponentInfo fare in reply.mnrByFareRecommendation[0].fareComponentInfo)
                        {
                            FareRule rule = new FareRule();
                            rule.Airline = result.Airline;
                            rule.Destination = fare.originAndDestination.destination;
                            rule.FareBasisCode = fare.fareQualifierDetails.additionalFareDetails.rateClass;
                            rule.Origin = fare.originAndDestination.origin;
                            rule.FareRuleKeyValue = fare.fareComponentRef[1].value;
                            FareRules.Add(rule);
                        }

                        List<MiniRulesRegulPropertiesType> RulesInfoGrp = new List<MiniRulesRegulPropertiesType>();
                        RulesInfoGrp.AddRange(reply.mnrByFareRecommendation[0].mnrRulesInfoGrp);

                        List<MiniRulesRegulPropertiesType> OnwardRules = RulesInfoGrp.FindAll(delegate (MiniRulesRegulPropertiesType r) { return r.mnrFCInfoGrp[0].refInfo[0].value == "1"; });
                        List<MiniRulesRegulPropertiesType> ReturnRules = RulesInfoGrp.FindAll(delegate (MiniRulesRegulPropertiesType r) { return r.mnrFCInfoGrp[0].refInfo[0].value == "2"; });

                        foreach (FareRule rule in FareRules)
                        {
                            rule.FareRestriction = string.Empty;
                            rule.FareRuleDetail = string.Empty;
                            rule.FareInfoRef = string.Empty;

                            #region Onward Rules
                            if (rule.FareRuleKeyValue == "1")
                            {
                                foreach (MiniRulesRegulPropertiesType infoGrp in OnwardRules)
                                {
                                    foreach (MiniRulesRegulPropertiesTypeMnrFCInfoGrp mnrFCInfoGrp in infoGrp.mnrFCInfoGrp)
                                    {
                                        if (mnrFCInfoGrp.locationInfo != null && mnrFCInfoGrp.locationInfo.Length > 0)
                                        {
                                            if (rule.FareRestriction.Length > 0)
                                            {
                                                rule.FareRestriction += "," + mnrFCInfoGrp.locationInfo[0].locationDescription.code;
                                            }
                                            else
                                            {
                                                rule.FareRestriction = mnrFCInfoGrp.locationInfo[0].locationDescription.code;
                                            }
                                        }
                                        if (infoGrp.mnrDateInfoGrp != null && infoGrp.mnrDateInfoGrp.Length > 0)
                                        {
                                            if (rule.FareRestriction.Length > 0)
                                            {
                                                rule.FareRestriction += "," + GetDateQualifierDetail(infoGrp.mnrDateInfoGrp[0].dateInfo[0].qualifier) + " " + infoGrp.mnrDateInfoGrp[0].dateInfo[0].date;
                                            }
                                            else
                                            {
                                                rule.FareRestriction = GetDateQualifierDetail(infoGrp.mnrDateInfoGrp[0].dateInfo[0].qualifier) + " " + infoGrp.mnrDateInfoGrp[0].dateInfo[0].date;
                                            }
                                        }
                                        if (infoGrp.mnrMonInfoGrp != null && infoGrp.mnrMonInfoGrp.Length > 0)
                                        {
                                            foreach (MiniRulesRegulPropertiesTypeMnrMonInfoGrp mnrMonInfoGrp in infoGrp.mnrMonInfoGrp)
                                            {
                                                if (rule.FareRuleDetail.Length > 0)
                                                {
                                                    rule.FareRuleDetail += "," + GetMonetoryDetailQualifierDetail(mnrMonInfoGrp.monetaryInfo[0].typeQualifier) + " " + mnrMonInfoGrp.monetaryInfo[0].currency + " " + mnrMonInfoGrp.monetaryInfo[0].amount;
                                                }
                                                else
                                                {
                                                    rule.FareRuleDetail = GetMonetoryDetailQualifierDetail(mnrMonInfoGrp.monetaryInfo[0].typeQualifier) + " " + mnrMonInfoGrp.monetaryInfo[0].currency + " " + mnrMonInfoGrp.monetaryInfo[0].amount;
                                                }
                                            }
                                        }

                                        if (infoGrp.mnrRestriAppInfoGrp != null && infoGrp.mnrRestriAppInfoGrp.Length > 0)
                                        {
                                            if (rule.FareInfoRef.Length > 0)
                                            {
                                                rule.FareInfoRef += "," + GetRestrictionQualifierDetail(infoGrp.mnrRestriAppInfoGrp[0].mnrRestriAppInfo[0].indicator) + " " + GetRestrictionActonDetail(infoGrp.mnrRestriAppInfoGrp[0].mnrRestriAppInfo[0].action);
                                            }
                                            else
                                            {
                                                rule.FareInfoRef = GetRestrictionQualifierDetail(infoGrp.mnrRestriAppInfoGrp[0].mnrRestriAppInfo[0].indicator) + " " + GetRestrictionActonDetail(infoGrp.mnrRestriAppInfoGrp[0].mnrRestriAppInfo[0].action);
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region Return Rules
                            if (rule.FareRuleKeyValue == "2")
                            {
                                foreach (MiniRulesRegulPropertiesType infoGrp in ReturnRules)
                                {
                                    foreach (MiniRulesRegulPropertiesTypeMnrFCInfoGrp mnrFCInfoGrp in infoGrp.mnrFCInfoGrp)
                                    {
                                        if (mnrFCInfoGrp.locationInfo != null && mnrFCInfoGrp.locationInfo.Length > 0)
                                        {
                                            if (rule.FareRestriction.Length > 0)
                                            {
                                                rule.FareRestriction += "," + mnrFCInfoGrp.locationInfo[0].locationDescription.code;
                                            }
                                            else
                                            {
                                                rule.FareRestriction = mnrFCInfoGrp.locationInfo[0].locationDescription.code;
                                            }
                                        }
                                        if (infoGrp.mnrDateInfoGrp != null && infoGrp.mnrDateInfoGrp.Length > 0)
                                        {
                                            if (rule.FareRestriction.Length > 0)
                                            {
                                                rule.FareRestriction += "," + GetDateQualifierDetail(infoGrp.mnrDateInfoGrp[0].dateInfo[0].qualifier) + " " + infoGrp.mnrDateInfoGrp[0].dateInfo[0].date;
                                            }
                                            else
                                            {
                                                rule.FareRestriction = GetDateQualifierDetail(infoGrp.mnrDateInfoGrp[0].dateInfo[0].qualifier) + " " + infoGrp.mnrDateInfoGrp[0].dateInfo[0].date;
                                            }
                                        }
                                        if (infoGrp.mnrMonInfoGrp != null && infoGrp.mnrMonInfoGrp.Length > 0)
                                        {
                                            foreach (MiniRulesRegulPropertiesTypeMnrMonInfoGrp mnrMonInfoGrp in infoGrp.mnrMonInfoGrp)
                                            {
                                                if (rule.FareRuleDetail.Length > 0)
                                                {
                                                    rule.FareRuleDetail += "," + GetMonetoryDetailQualifierDetail(mnrMonInfoGrp.monetaryInfo[0].typeQualifier) + " " + mnrMonInfoGrp.monetaryInfo[0].currency + " " + mnrMonInfoGrp.monetaryInfo[0].amount;
                                                }
                                                else
                                                {
                                                    rule.FareRuleDetail = GetMonetoryDetailQualifierDetail(mnrMonInfoGrp.monetaryInfo[0].typeQualifier) + " " + mnrMonInfoGrp.monetaryInfo[0].currency + " " + mnrMonInfoGrp.monetaryInfo[0].amount;
                                                }
                                            }
                                        }

                                        if (infoGrp.mnrRestriAppInfoGrp != null && infoGrp.mnrRestriAppInfoGrp.Length > 0)
                                        {
                                            if (rule.FareInfoRef.Length > 0)
                                            {
                                                rule.FareInfoRef += "," + GetRestrictionQualifierDetail(infoGrp.mnrRestriAppInfoGrp[0].mnrRestriAppInfo[0].indicator) + " " + GetRestrictionActonDetail(infoGrp.mnrRestriAppInfoGrp[0].mnrRestriAppInfo[0].action);
                                            }
                                            else
                                            {
                                                rule.FareInfoRef = GetRestrictionQualifierDetail(infoGrp.mnrRestriAppInfoGrp[0].mnrRestriAppInfo[0].indicator) + " " + GetRestrictionActonDetail(infoGrp.mnrRestriAppInfoGrp[0].mnrRestriAppInfo[0].action);
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                SignOut();
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to get Fare Rules. Reason : " + ex.ToString(), "");
            }

            return FareRules;
        }

        #endregion

        #region 04:Booking Creation

        /*   Service description
            The LowestFare_SellFromRecommendation is designed to sell a recommendations proposed by the LowestFare_Search 
            function.This function has an optional selling algorithm that can be specified which can overcome situation where 
            a normal Sell would be rejected by the airlines.A basic low fare sell is composed of the following minimum mandatory
            elements:The Lowest Fare Search message function with Id: 183 An itinerary composed of:
            Origin and Destination: departure and arrival cities
            Message Id: 183 if the optimization algorithm is to be applied, else empty
            A list of segments, composed of:
            flight number, board and off airports, departure date
            number of passengers requested */


        public Air_SellFromRecommendationReply SellFromRecommendation(FlightItinerary itinerary)
        { 
            Air_SellFromRecommendationReply sellReply = null;
            try
            {
                hSession.handleSession(SessionHandler.TransactionStatusCode.Start);
                hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.New);
                //hAddressing.update();

                Session session = hSession.Session;
                TransactionFlowLinkType link = hLink.Link;

                int turnPoint = 0;
                if ((itinerary.Destination == null || itinerary.Destination.Length == 0) || (itinerary.Origin == null || itinerary.Origin.Length == 0))
                {
                    throw new BookingEngineException("Booking Failed: Unknown Destination");
                }
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    if (itinerary.Destination == itinerary.Segments[i].Destination.CityCode || itinerary.Destination == itinerary.Segments[i].Destination.AirportCode)
                    {
                        turnPoint = i + 1;
                        break;
                    }
                }
                if (turnPoint == 0)
                {
                    throw new BookingEngineException("Booking Failed: Return Point Not found");
                }
                int trips = itinerary.Segments.Length > turnPoint ? 2 : 1;
                int seatCount = 0;
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    if (itinerary.Passenger[i].Type != PassengerType.Infant)
                    {
                        seatCount++;
                    }
                }
                //Air_SellFromRecommendation structure
                //messageActionDetails -- C --This will specify the action of the message.
                //originatorDetails -- C --This group will contain the ORG segment.
                //recordLocator --C --PNR recloc.
                //itineraryDetails --C --This group describes passenger's origin and destination of the journey

                Air_SellFromRecommendation sell = new Air_SellFromRecommendation();

                //Air_SellFromRecommendation / messageActionDetails / messageFunctionDetails

                //messageFunction --C
                //183 --	Lowest fare across airline, flight, class criteria.
                //M1 --	Trigger Sell Optimization Algorithm, option cancel all if unsuccessful.
                //M2 Trigger Sell Optimization Algorithm, option keep all confirmed if unsuccessful.

                //additionalMessageFunction --C
                //183 --	Lowest fare across airline, flight, class criteria.
                //M1 --	Trigger Sell Optimization Algorithm, option cancel all if unsuccessful.
                //M2 Trigger Sell Optimization Algorithm, option keep all confirmed if unsuccessful.

                MessageActionDetailsTypeI msgDetails = new MessageActionDetailsTypeI();
                msgDetails.messageFunctionDetails = new MessageFunctionBusinessDetailsTypeI();
                msgDetails.messageFunctionDetails.messageFunction = "183";
                msgDetails.messageFunctionDetails.additionalMessageFunction = new string[1];
                msgDetails.messageFunctionDetails.additionalMessageFunction[0] = "M1";
                sell.messageActionDetails = msgDetails;

                sell.itineraryDetails = new Air_SellFromRecommendationItineraryDetails[itinerary.Segments.Length];
                int segmentCounter = 0;
                int segmentLimiter = 0;
                for (int i = 0; i < trips; i++)
                {
                    if (i == 0)
                    {
                        segmentLimiter = turnPoint;
                    }
                    else
                    {
                        segmentLimiter = itinerary.Segments.Length;
                    }
                    //itineraryDetails
                    //originDestinationDetails
                    sell.itineraryDetails[i] = new Air_SellFromRecommendationItineraryDetails();
                    sell.itineraryDetails[i].originDestinationDetails = new OriginAndDestinationDetailsTypeI();
                    if (i == 0)
                    {
                        sell.itineraryDetails[i].originDestinationDetails.origin = itinerary.Segments[0].Origin.AirportCode;
                        sell.itineraryDetails[i].originDestinationDetails.destination = itinerary.Segments[turnPoint - 1].Destination.AirportCode;
                    }
                    else
                    {
                        sell.itineraryDetails[i].originDestinationDetails.origin = itinerary.Segments[turnPoint - 1].Destination.AirportCode;
                        sell.itineraryDetails[i].originDestinationDetails.destination = itinerary.Segments[0].Origin.AirportCode;
                    }
                    //message
                    sell.itineraryDetails[i].message = new MessageActionDetailsTypeI();
                    sell.itineraryDetails[i].message.messageFunctionDetails = new MessageFunctionBusinessDetailsTypeI();
                    sell.itineraryDetails[i].message.messageFunctionDetails.messageFunction = "183";

                    //segmentInformation
                    sell.itineraryDetails[i].segmentInformation = new Air_SellFromRecommendationItineraryDetailsSegmentInformation[segmentLimiter];
                    for (int j = segmentCounter; j < segmentLimiter; j++, segmentCounter++)
                    {
                        sell.itineraryDetails[i].segmentInformation[j] = new Air_SellFromRecommendationItineraryDetailsSegmentInformation();

                        sell.itineraryDetails[i].segmentInformation[j].travelProductInformation = new TravelProductInformationTypeI();

                        sell.itineraryDetails[i].segmentInformation[j].travelProductInformation.flightDate = new ProductDateTimeTypeI();
                        sell.itineraryDetails[i].segmentInformation[j].travelProductInformation.flightDate.departureDate = itinerary.Segments[segmentCounter].DepartureTime.ToString("ddMMyy");
                        sell.itineraryDetails[i].segmentInformation[j].travelProductInformation.flightDate.departureTime = itinerary.Segments[segmentCounter].DepartureTime.ToString("HHmm");

                        sell.itineraryDetails[i].segmentInformation[j].travelProductInformation.boardPointDetails = new LocationTypeI();
                        sell.itineraryDetails[i].segmentInformation[j].travelProductInformation.boardPointDetails.trueLocationId = itinerary.Segments[segmentCounter].Origin.AirportCode;

                        sell.itineraryDetails[i].segmentInformation[j].travelProductInformation.offpointDetails = new LocationTypeI();
                        sell.itineraryDetails[i].segmentInformation[j].travelProductInformation.offpointDetails.trueLocationId = itinerary.Segments[segmentCounter].Destination.AirportCode;

                        sell.itineraryDetails[i].segmentInformation[j].travelProductInformation.companyDetails = new CompanyIdentificationTypeI();
                        sell.itineraryDetails[i].segmentInformation[j].travelProductInformation.companyDetails.marketingCompany = itinerary.Segments[segmentCounter].Airline;

                        sell.itineraryDetails[i].segmentInformation[j].travelProductInformation.flightIdentification = new ProductIdentificationDetailsTypeI();
                        sell.itineraryDetails[i].segmentInformation[j].travelProductInformation.flightIdentification.flightNumber = itinerary.Segments[segmentCounter].FlightNumber;
                        sell.itineraryDetails[i].segmentInformation[j].travelProductInformation.flightIdentification.bookingClass = itinerary.Segments[segmentCounter].BookingClass;

                        sell.itineraryDetails[i].segmentInformation[j].relatedproductInformation = new RelatedProductInformationTypeI();
                        sell.itineraryDetails[i].segmentInformation[j].relatedproductInformation.quantity = seatCount.ToString();
                        sell.itineraryDetails[i].segmentInformation[j].relatedproductInformation.statusCode = new string[1];
                        sell.itineraryDetails[i].segmentInformation[j].relatedproductInformation.statusCode[0] = "NN";//Sell Segment

                    }
                }

                //Serialize the request
                Air_SellFromRecommendationRequest sellRequest = new Air_SellFromRecommendationRequest(session, link, hSecurity.getHostedUser(), sell);
                //Serialize searchRequest
                try
                {
                    string filepath = xmlPath + sessionId + "_" + appUserId + "_Air_SellFromRecommendationRequest_" + /*itinerary.Segments[0].Origin + "_" + itinerary.Segments[0].Destination +*/ DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(sellRequest.GetType());
                    StreamWriter sw = new StreamWriter(filepath);
                    xsr.Serialize(sw, sellRequest);
                    sw.Close();
                }
                catch { }
                //Serialize the response
                sellReply = client.Air_SellFromRecommendation(ref session, ref link, hSecurity.getHostedUser(), sell);
                hSession.Session = session;
                hLink.Link = link;
                hSession.AfterReply(SessionHandler.TransactionStatusCode.Continue);
                hLink.AfterReply(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                //hAddressing.update();
                if (sellReply != null)
                {
                    //hSession.Session = session;
                    //hLink.Link = link;
                    //hSession.handleSession(SessionHandler.TransactionStatusCode.Continue);
                    //hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                    Air_SellFromRecommendationResponse sellResponse = new Air_SellFromRecommendationResponse(session, link, sellReply);
                    //Serialize pnrResponse
                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_Air_SellFromRecommendationResponse_" + /*itinerary.Segments[0].Origin + "_" + itinerary.Segments[0].Destination + */ DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(sellResponse.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, sellResponse);
                        sw.Close();
                    }
                    catch { }
                }

            }
            catch (Exception ex)
            {
                SignOut();
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to execute SellFromRecommendation method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return sellReply;
        }

        /// <summary>
        ///The AddMultiElements function allows a user to make an entire reservation in the Amadeus system with one transaction that the full itinerary details must be known at the time of the function usage.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public PNR_Reply PNRAddMultiElements(FlightItinerary itinerary)
        {
            PNR_Reply pnrReply = null;
            try
            {
                hSession.handleSession(SessionHandler.TransactionStatusCode.Continue);
                hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                //hAddressing.update();
                Session session = hSession.Session;
                TransactionFlowLinkType link = hLink.Link;

                int infantCount = 0;
                List<FlightPassenger> infantPassenger = new List<FlightPassenger>();
                List<FlightPassenger> passenger = new List<FlightPassenger>();
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    if (itinerary.Passenger[i].Type == PassengerType.Infant)
                    {
                        infantPassenger.Add(itinerary.Passenger[i]);
                        infantCount++;
                    }
                    else
                    {
                        passenger.Add(itinerary.Passenger[i]);
                    }
                }
                List<string> distinctAirline = new List<string>();
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    if (!distinctAirline.Contains(itinerary.Segments[i].Airline))
                    {
                        distinctAirline.Add(itinerary.Segments[i].Airline);
                    }
                }
                PNR_AddMultiElements pnrElements = new PNR_AddMultiElements();
                //PNR_AddMultiElements structure
                //reservationInfo -- C--To specify a reference to a reservation
                //pnrActions -- M --To specify specific Actions to be processed on PNR
                //travellerInfo --C --This group is used to convey passenger information
                //originDestinationDetails -- C --for connected/not connected air segments
                //dataElementsMaster --C --Data elements master containg the DUM delimiter and group 8

                //pnrActions
                pnrElements.pnrActions = new string[1];
                pnrElements.pnrActions[0] = "0";//No special processing


                //travellerInfo
                //travellerInfo structure
                //elementManagementPassenger
                //passengerData
                //enhancedPassengerData
                pnrElements.travellerInfo = new PNR_AddMultiElementsTravellerInfo[passenger.Count];
                for (int i = 0; i < passenger.Count; i++)
                {
                    pnrElements.travellerInfo[i] = new PNR_AddMultiElementsTravellerInfo();

                    //elementManagementPassenger -- M--	To specify the PNR segments/elements references and action to apply
                    pnrElements.travellerInfo[i].elementManagementPassenger = new ElementManagementSegmentType3();

                    //elementManagementPassenger --reference
                    pnrElements.travellerInfo[i].elementManagementPassenger.reference = new ReferencingDetailsType12();
                    pnrElements.travellerInfo[i].elementManagementPassenger.reference.qualifier = "PR"; //Passenger Client-request-message-defined ref. nbr
                    pnrElements.travellerInfo[i].elementManagementPassenger.reference.number = Convert.ToString(i + 1);

                    pnrElements.travellerInfo[i].elementManagementPassenger.segmentName = "NM";//Name Element

                    //Passenger Data
                    pnrElements.travellerInfo[i].passengerData = new PNR_AddMultiElementsTravellerInfoPassengerData[1];
                    pnrElements.travellerInfo[i].passengerData[0] = new PNR_AddMultiElementsTravellerInfoPassengerData();
                    pnrElements.travellerInfo[i].passengerData[0].travellerInformation = new TravellerInformationTypeI();
                    pnrElements.travellerInfo[i].passengerData[0].travellerInformation.traveller = new TravellerSurnameInformationTypeI();
                    pnrElements.travellerInfo[i].passengerData[0].travellerInformation.traveller.surname = passenger[i].LastName.Replace(".", string.Empty);
                    if (infantCount > 0)
                    {
                        pnrElements.travellerInfo[i].passengerData[0].travellerInformation.traveller.quantity = "2";
                    }
                    else
                    {
                        pnrElements.travellerInfo[i].passengerData[0].travellerInformation.traveller.quantity = "1";
                    }

                    //Passengers
                    if (infantCount > 0)
                    {
                        pnrElements.travellerInfo[i].passengerData[0].travellerInformation.passenger = new TravellerDetailsTypeI2[2];
                    }
                    else
                    {
                        pnrElements.travellerInfo[i].passengerData[0].travellerInformation.passenger = new TravellerDetailsTypeI2[1];
                    }
                    pnrElements.travellerInfo[i].passengerData[0].travellerInformation.passenger[0] = new TravellerDetailsTypeI2();
                    pnrElements.travellerInfo[i].passengerData[0].travellerInformation.passenger[0].firstName = passenger[i].FirstName.Replace(".", string.Empty);
                    string paxType = string.Empty;
                    if (passenger[i].Type == PassengerType.Adult)
                    {
                        paxType = "ADT";
                    }
                    else if (passenger[i].Type == PassengerType.Child)
                    {
                        paxType = "CHD";
                    }
                    else if (passenger[i].Type == PassengerType.Senior)
                    {
                        paxType = "YCD";
                    }
                    pnrElements.travellerInfo[i].passengerData[0].travellerInformation.passenger[0].type = paxType;

                    pnrElements.travellerInfo[i].passengerData[0].dateOfBirth = new DateAndTimeInformationType5();
                    pnrElements.travellerInfo[i].passengerData[0].dateOfBirth.dateAndTimeDetails = new DateAndTimeDetailsTypeI_56946C();
                    pnrElements.travellerInfo[i].passengerData[0].dateOfBirth.dateAndTimeDetails.date = passenger[i].DateOfBirth.ToString("ddMMMyy");
                    pnrElements.travellerInfo[i].passengerData[0].dateOfBirth.dateAndTimeDetails.qualifier = "706";

                    if (infantCount > 0 && (passenger[i].Type == PassengerType.Adult || passenger[i].Type == PassengerType.Senior))
                    {
                        pnrElements.travellerInfo[i].passengerData[0].travellerInformation.passenger[0].infantIndicator = "1";
                        pnrElements.travellerInfo[i].passengerData[0].travellerInformation.passenger[1] = new TravellerDetailsTypeI2();
                        pnrElements.travellerInfo[i].passengerData[0].travellerInformation.passenger[1].firstName = infantPassenger[0].FirstName;
                        pnrElements.travellerInfo[i].passengerData[0].travellerInformation.passenger[1].type = "INF";

                        //Infant DOB Details
                        pnrElements.travellerInfo[i].passengerData[0].dateOfBirth = new DateAndTimeInformationType5();
                        pnrElements.travellerInfo[i].passengerData[0].dateOfBirth.dateAndTimeDetails = new DateAndTimeDetailsTypeI_56946C();
                        pnrElements.travellerInfo[i].passengerData[0].dateOfBirth.dateAndTimeDetails.date = infantPassenger[0].DateOfBirth.ToString("ddMMMyy");
                        pnrElements.travellerInfo[i].passengerData[0].dateOfBirth.dateAndTimeDetails.qualifier = "706";

                        infantPassenger.RemoveAt(0);
                        infantCount--;
                    }

                }
                //dataElementsMaster
                pnrElements.dataElementsMaster = new PNR_AddMultiElementsDataElementsMaster();
                pnrElements.dataElementsMaster.marker1 = new DummySegmentTypeI6();

                List<PNR_AddMultiElementsDataElementsMasterDataElementsIndiv> dataElementsIndiv = new List<PNR_AddMultiElementsDataElementsMasterDataElementsIndiv>();

                //Received From
                PNR_AddMultiElementsDataElementsMasterDataElementsIndiv dataElement0 = new PNR_AddMultiElementsDataElementsMasterDataElementsIndiv();
                dataElement0.elementManagementData = new ElementManagementSegmentType3();
                dataElement0.elementManagementData.reference = new ReferencingDetailsType12();
                dataElement0.elementManagementData.reference.qualifier = "OT";//Other element tatoo reference number
                dataElement0.elementManagementData.reference.number = DateTime.Now.Millisecond.ToString();
                dataElement0.elementManagementData.segmentName = "RF";//Receive From element
                dataElement0.freetextData = new LongFreeTextType();
                dataElement0.freetextData.freetextDetail = new FreeTextQualificationType2();
                dataElement0.freetextData.freetextDetail.subjectQualifier = "3";//Literal Text
                dataElement0.freetextData.freetextDetail.type = "P22";//Receive From
                dataElement0.freetextData.longFreetext = "Received From CozmoTravel";
                dataElementsIndiv.Add(dataElement0);

                //Ticketing Type
                dataElement0 = new PNR_AddMultiElementsDataElementsMasterDataElementsIndiv();
                dataElement0.elementManagementData = new ElementManagementSegmentType3();
                dataElement0.elementManagementData.reference = new ReferencingDetailsType12();
                dataElement0.elementManagementData.reference.qualifier = "OT";//Other element tatoo reference number
                dataElement0.elementManagementData.reference.number = "1";
                dataElement0.elementManagementData.segmentName = "TK"; //Ticket element
                dataElement0.ticketElement = new TicketElementType();

                dataElement0.ticketElement.ticket = new TicketInformationType();
                dataElement0.ticketElement.ticket.indicator = "XL";//automatically cancellation happens
                dataElement0.ticketElement.ticket.date = DateTime.Now.AddDays(1).ToString("ddMMyy");
                dataElementsIndiv.Add(dataElement0);

                //Mobile Phone number
                dataElement0 = new PNR_AddMultiElementsDataElementsMasterDataElementsIndiv();
                dataElement0.elementManagementData = new ElementManagementSegmentType3();
                dataElement0.elementManagementData.reference = new ReferencingDetailsType12();
                dataElement0.elementManagementData.reference.qualifier = "OT";//Other element tatoo reference number
                dataElement0.elementManagementData.reference.number = "1";
                dataElement0.elementManagementData.segmentName = "AP";
                dataElement0.freetextData = new LongFreeTextType();
                dataElement0.freetextData.freetextDetail = new FreeTextQualificationType2();
                dataElement0.freetextData.freetextDetail.subjectQualifier = "3";//Literal Text
                dataElement0.freetextData.freetextDetail.type = "5";//Phone Number
                dataElement0.freetextData.longFreetext = itinerary.Passenger[0].CellPhone;
                dataElementsIndiv.Add(dataElement0);

                //Email
                dataElement0 = new PNR_AddMultiElementsDataElementsMasterDataElementsIndiv();
                dataElement0.elementManagementData = new ElementManagementSegmentType3();
                dataElement0.elementManagementData.segmentName = "AP"; //Contact element
                dataElement0.freetextData = new LongFreeTextType();
                dataElement0.freetextData.freetextDetail = new FreeTextQualificationType2();
                dataElement0.freetextData.freetextDetail.subjectQualifier = "3";//Literal text
                dataElement0.freetextData.freetextDetail.type = "P02"; //E-mail address
                dataElement0.freetextData.longFreetext = "tech@cozmotravel.com";
                dataElementsIndiv.Add(dataElement0);

                //form of payment

                dataElement0 = new PNR_AddMultiElementsDataElementsMasterDataElementsIndiv();
                dataElement0.elementManagementData = new ElementManagementSegmentType3();
                dataElement0.elementManagementData.reference = new ReferencingDetailsType12();
                dataElement0.elementManagementData.reference.qualifier = "OT";//Other element tatoo reference number
                dataElement0.elementManagementData.reference.number = "1";
                dataElement0.elementManagementData.segmentName = "FP"; //Form of payment
                dataElement0.formOfPayment = new FormOfPaymentDetailsTypeI4[1];
                dataElement0.formOfPayment[0] = new FormOfPaymentDetailsTypeI4();

                //CA --CASH
                //CC --Credit card
                //CK -- Check
                //MS -- Miscellaneous
                dataElement0.formOfPayment[0].identification = "MS";//Miscellaneous
                dataElementsIndiv.Add(dataElement0);

                //Passengers Passport Number Details elements
                int travellerRef = 1;
                for (int i = 0; i < itinerary.Passenger.Length; i++)
                {
                    if (!string.IsNullOrEmpty(itinerary.Passenger[i].PassportNo) && itinerary.Passenger[i].PassportNo.Length > 0)
                    {

                        PNR_AddMultiElementsDataElementsMasterDataElementsIndiv dataElement4 = new PNR_AddMultiElementsDataElementsMasterDataElementsIndiv();
                        dataElement4.elementManagementData = new ElementManagementSegmentType3();
                        dataElement4.elementManagementData.segmentName = "SSR"; //SSR element;
                        dataElement4.serviceRequest = new SpecialRequirementsDetailsTypeI();
                        dataElement4.serviceRequest.ssr = new SpecialRequirementsTypeDetailsTypeI();
                        dataElement4.serviceRequest.ssr.type = "DOCS";
                        dataElement4.serviceRequest.ssr.status = "HK";//Holds confirmed.
                        dataElement4.serviceRequest.ssr.quantity = "1";
                        dataElement4.serviceRequest.ssr.companyId = itinerary.Segments[0].Airline;
                        dataElement4.serviceRequest.ssr.freetext = new string[1];
                        FlightPassenger pax = itinerary.Passenger[i];
                        dataElement4.serviceRequest.ssr.freetext[0] = "P-" + pax.Country.CountryCode + "-" + pax.PassportNo + "-" + pax.Nationality.CountryCode + "-" + pax.DateOfBirth.ToString("ddMMMyy") + "-" + GetPaxGenderForSSRDOCS(pax.Gender, pax.Type) + "-" + pax.PassportExpiry.ToString("ddMMMyy") + "-" + pax.FirstName + "-" + pax.LastName;
                        dataElement4.referenceForDataElement = new ReferencingDetailsType12[1];
                        dataElement4.referenceForDataElement[0] = new ReferencingDetailsType12();
                        dataElement4.referenceForDataElement[0].qualifier = "PR";//passenger Client-request-message-defined ref. nbr
                        if (itinerary.Passenger[i].Type == PassengerType.Infant)
                        {
                            dataElement4.referenceForDataElement[0].number = Convert.ToString(travellerRef);
                            travellerRef++;
                        }
                        else
                        {
                            dataElement4.referenceForDataElement[0].number = Convert.ToString(i + 1);
                            //travellerRef++;
                        }
                        dataElementsIndiv.Add(dataElement4);
                    }
                }
                pnrElements.dataElementsMaster.dataElementsIndiv = dataElementsIndiv.ToArray();
                PNR_AddMultiElementsRequest pnrAddMultiElementsRequest = new PNR_AddMultiElementsRequest(session, link, hSecurity.getHostedUser(), pnrElements);
                //Serialize pnrAddMultiElementsRequest
                try
                {
                    string filepath = xmlPath + sessionId + "_" + appUserId + "_PNR_AddMultiElementsRequest_" + /*itinerary.Segments[0].Origin + "_" + itinerary.Segments[0].Destination + "_" +*/ DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pnrAddMultiElementsRequest.GetType());
                    StreamWriter sw = new StreamWriter(filepath);
                    xsr.Serialize(sw, pnrAddMultiElementsRequest);
                    sw.Close();
                }
                catch { }

                //Serialize the response
                pnrReply = client.PNR_AddMultiElements(ref session, ref link, hSecurity.getHostedUser(), pnrElements);
                hSession.Session = session;
                hLink.Link = link;
                hSession.AfterReply(SessionHandler.TransactionStatusCode.Continue);
                hLink.AfterReply(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                //hAddressing.update();
                if (pnrReply != null)
                {

                    PNR_AddMultiElementsResponse pnrResponse = new PNR_AddMultiElementsResponse(session, link, pnrReply);
                    //Serialize pnrResponse
                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_PNR_AddMultiElementsResponse_" + /*itinerary.Segments[0].Origin + "_" + itinerary.Segments[0].Destination + "_" +*/ DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pnrResponse.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, pnrResponse);
                        sw.Close();
                    }
                    catch { }
                }

            }
            catch (Exception ex)
            {
                SignOut();
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to execute PNR_AddMultiElements method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return pnrReply;
        }

        /// <summary>
        /// The PricePNRWithBookingClass function requests fare information corresponding to the current booking class of the Passenger Name Record (PNR) itinerary
        /// </summary>
        /// <param name="fareType"></param>
        /// <returns></returns>
        public Fare_PricePNRWithBookingClassReply PNRWithBookingClass(string fareType,FlightItinerary itinerary)
        {
            hSession.handleSession(SessionHandler.TransactionStatusCode.Continue);
            hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
            //hAddressing.update();
            Session session = hSession.Session;
            TransactionFlowLinkType link = hLink.Link;

            Fare_PricePNRWithBookingClassReply pnrWithBookingClass = null;
            try
            {
                Fare_PricePNRWithBookingClassPricingOptionGroup[] pricingOptionGroup = new Fare_PricePNRWithBookingClassPricingOptionGroup[1];
                pricingOptionGroup[0] = new Fare_PricePNRWithBookingClassPricingOptionGroup();
                pricingOptionGroup[0].pricingOptionKey = new PricingOptionKey1();
                pricingOptionGroup[0].pricingOptionKey.pricingOptionKey = fareType;


                Fare_PricePNRWithBookingClassRequest priceRequest = new Fare_PricePNRWithBookingClassRequest(session, link, hSecurity.getHostedUser(), pricingOptionGroup);
                //Serialize the request
                try
                {
                    
                    string filepath = xmlPath + sessionId + "_" + appUserId + "_Fare_PricePNRWithBookingClassRequest_" + /*itinerary.Segments[0].Origin + "_" + itinerary.Segments[0].Destination + "_" +*/ DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(priceRequest.GetType());
                    StreamWriter sw = new StreamWriter(filepath);
                    xsr.Serialize(sw, priceRequest);
                    sw.Close();
                }
                catch { }

                //Serialize the response
                pnrWithBookingClass = client.Fare_PricePNRWithBookingClass(ref session, ref link, hSecurity.getHostedUser(), pricingOptionGroup);
                hSession.Session = session;
                hLink.Link = link;
                hSession.AfterReply(SessionHandler.TransactionStatusCode.Continue);
                hLink.AfterReply(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                //hAddressing.update();
                if (pnrWithBookingClass != null)
                {
                    //hSession.Session = session;
                    //hLink.Link = link;
                    //hSession.handleSession(SessionHandler.TransactionStatusCode.Continue);
                    //hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                    Fare_PricePNRWithBookingClassResponse response = new Fare_PricePNRWithBookingClassResponse(session, link, pnrWithBookingClass);
                    //Serialize pnrResponse
                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_Fare_PricePNRWithBookingClassResponse_" + /*itinerary.Segments[0].Origin + "_" + itinerary.Segments[0].Destination + "_" +*/ DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(response.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, response);
                        sw.Close();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                SignOut();
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to execute PNRWithBookingClass method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return pnrWithBookingClass;
        }
        /// <summary>
        /// The CreateTST function allows the Amadeus system to process fares from an itineary for the flight segments in a PNR (Passenger Name Record). 
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public Ticket_CreateTSTFromPricingReply CreateTST(FlightItinerary itinerary)
        {

            /*The system prices the itinerary in the currency of the country of origin, and includes the equivalent amount to be paid when the journey does not originate in the country where your terminal is located.
              Fares can be requested for selected passengers, or for all passengers in a PNR
              In the case where more than one fare is applicable for an itinerary, the System displays a list of applicable fares.
              The system will display the price for three minutes.*/
            /*Transitional Stored Ticket (TST)*/

            hSession.handleSession(SessionHandler.TransactionStatusCode.Continue);
            hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
            //hAddressing.update();
            Session session = hSession.Session;
            TransactionFlowLinkType link = hLink.Link;

            Ticket_CreateTSTFromPricingReply tstReply = null;
            try
            {
                int adultRefCount = 0;
                int childRefCount = 0;
                int infantRefCount = 0;


                for (int r = 0; r < itinerary.Passenger.Length; r++)
                {
                    if (itinerary.Passenger[r].Type == PassengerType.Adult)
                    {
                        adultRefCount = 1;
                    }
                    else if (itinerary.Passenger[r].Type == PassengerType.Child)
                    {
                        childRefCount = 1;
                    }
                    else if (itinerary.Passenger[r].Type == PassengerType.Infant)
                    {
                        infantRefCount = 1;
                    }
                }
                int totalPaxRefCount = adultRefCount + childRefCount + infantRefCount;


                Ticket_CreateTSTFromPricing priceRequest = new Ticket_CreateTSTFromPricing();
                priceRequest.psaList = new Ticket_CreateTSTFromPricingPsaList[totalPaxRefCount];
                for (int i = 0; i < totalPaxRefCount; i++)
                {

                    priceRequest.psaList[i] = new Ticket_CreateTSTFromPricingPsaList();
                    priceRequest.psaList[i].itemReference = new ItemReferencesAndVersionsType10();
                    priceRequest.psaList[i].itemReference.referenceType = "TST";
                    priceRequest.psaList[i].itemReference.uniqueReference = Convert.ToString(i + 1);
                }
                Ticket_CreateTSTFromPricingRequest pricingRequest = new Ticket_CreateTSTFromPricingRequest(session, link, hSecurity.getHostedUser(), priceRequest);
                //Serialize searchRequest
                try
                {
                    string filepath = xmlPath + sessionId + "_" + appUserId + "_Ticket_CreateTSTFromPricingRequest_" +/* itinerary.Segments[0].Origin +"_"+ itinerary.Segments[0].Destination + "_" +*/ DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pricingRequest.GetType());
                    StreamWriter sw = new StreamWriter(filepath);
                    xsr.Serialize(sw, pricingRequest);
                    sw.Close();
                }
                catch { }

                tstReply = client.Ticket_CreateTSTFromPricing(ref session, ref link, hSecurity.getHostedUser(), priceRequest);
                hSession.Session = session;
                hLink.Link = link;
                hSession.AfterReply(SessionHandler.TransactionStatusCode.Continue);
                hLink.AfterReply(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                //hAddressing.update();
                if (tstReply != null)
                {
                    Ticket_CreateTSTFromPricingResponse pricingResponse = new Ticket_CreateTSTFromPricingResponse(session, link, tstReply);
                    //Serialize pnrResponse
                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_Ticket_CreateTSTFromPricingResponse_" + /*itinerary.Segments[0].Origin + "_" + itinerary.Segments[0].Destination + "_" +*/ DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pricingResponse.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, pricingResponse);
                        sw.Close();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                SignOut();
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to execute CreateTST method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return tstReply;
        }

        /// <summary>
        /// The AddMultiElements function allows a user to make an entire reservation in the Amadeus system with one transaction, bearing in mind, that the full itinerary details must be known at the time of the function usage.This function allows many different elements to be combined into one transaction, and so, it is more efficient by reducing the number of transactions required from the client application.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public PNR_Reply AddMultiElementsAfterTST(FlightItinerary itinerary)
        {
            PNR_Reply pnrReply = null;
            try
            {
                hSession.handleSession(SessionHandler.TransactionStatusCode.Continue);
                hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                //hAddressing.update();
                Session session = hSession.Session;
                TransactionFlowLinkType link = hLink.Link;

                PNR_AddMultiElements pnrElements = new PNR_AddMultiElements();
                //PNR_AddMultiElements structure
                //reservationInfo -- C--To specify a reference to a reservation
                //pnrActions -- M --To specify specific Actions to be processed on PNR
                //travellerInfo --C --This group is used to convey passenger information
                //originDestinationDetails -- C --for connected/not connected air segments
                //dataElementsMaster --C --Data elements master containg the DUM delimiter and group 8

                //pnrActions
                pnrElements.pnrActions = new string[2];
                pnrElements.pnrActions[0] = "11";
                pnrElements.pnrActions[1] = "30";


                PNR_AddMultiElementsRequest pnrAddMultiElementsRequest = new PNR_AddMultiElementsRequest(session, link, hSecurity.getHostedUser(), pnrElements);
                //Serialize pnrAddMultiElementsRequest
                try
                {
                    string filepath = xmlPath + sessionId + "_" + appUserId + "_PNR_AddMultiElementsAfterTSTRequest_" + /*itinerary.Segments[0].Origin +"_" + itinerary.Segments[0].Destination + "_" +*/ DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pnrAddMultiElementsRequest.GetType());
                    StreamWriter sw = new StreamWriter(filepath);
                    xsr.Serialize(sw, pnrAddMultiElementsRequest);
                    sw.Close();
                }
                catch { }

                //Serialize the response
                pnrReply = client.PNR_AddMultiElements(ref session, ref link, hSecurity.getHostedUser(), pnrElements);
                hSession.Session = session;
                hLink.Link = link;
                hSession.AfterReply(SessionHandler.TransactionStatusCode.Continue);
                hLink.AfterReply(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                //hAddressing.update();
                if (pnrReply != null)
                {

                    PNR_AddMultiElementsResponse pnrResponse = new PNR_AddMultiElementsResponse(session, link, pnrReply);
                    //Serialize pnrResponse
                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_PNR_AddMultiElementsAfterTSTResponse_" + /*itinerary.Segments[0].Origin + "_" + itinerary.Segments[0].Destination + "_" +*/ DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pnrResponse.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, pnrResponse);
                        sw.Close();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                SignOut();
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to execute AddMultiElementsAfterTST method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return pnrReply;
        }

        /// <summary>
        /// Allows a user to make an entire reservation in the Amadeus system
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public BookingResponse Book(ref FlightItinerary itinerary)
        {
            /*-------Amadeus  Booking Creation Steps ------------------
            01 Air_SellFromRecommendation
            01 Air_SellFromRecommendationReply
            02 PNR_AddMultiElements
            02 PNR_Reply
            03 Fare_PricePNRWithBookingClass
            03 Fare_PricePNRWithBookingClassReply
            04 Ticket_CreateTSTFromPricing
            04 Ticket_CreateTSTFromPricingReply
            05 PNR_AddMultiElements
            05 PNR_Reply
            06 PNR_AddMultiElements(Only if warning received)
            06 PNR_Reply(Only if warning received)
            07 Security_SignOut
            07 Security_SignOutReply 
            ------------------------------------------------------------*/
            GenerateHeader();
            BookingResponse bookResponse = new BookingResponse();
            try
            {
                int seatCount = 0;
                if (itinerary != null && itinerary.Passenger.Length > 0)
                {
                    for (int i = 0; i < itinerary.Passenger.Length; i++)
                    {
                        if (itinerary.Passenger[i].Type != PassengerType.Infant)
                        {
                            seatCount++;
                        }
                    }
                    //Step-1: Air_SellFromRecommendationReply.
                    Air_SellFromRecommendationReply sellReply = SellFromRecommendation(itinerary);
                    bool validSeatCount = ValidateSeatCount(sellReply, seatCount);
                    bool validSellResponse = ValidateSellResponse(sellReply);
                    //Both seat count  matches and no error in the response.
                    if (validSeatCount && validSellResponse)
                    {
                        //Step-2: PNR_Reply.
                        PNR_Reply pnrReply = PNRAddMultiElements(itinerary);
                        bool validPNRResponse = ValidatePNRResponse(pnrReply);
                        if (validPNRResponse && pnrReply != null && !string.IsNullOrEmpty(pnrReply.securityInformation.responsibilityInformation.typeOfPnrElement))
                        {

                            //Step-3: Fare_PricePNRWithBookingClassReply
                            Fare_PricePNRWithBookingClassReply bookingclassReply = PNRWithBookingClass(pnrReply.securityInformation.responsibilityInformation.typeOfPnrElement, itinerary);
                            bool validBookingClassResponse = ValidatePNRWithBookingClassResponse(bookingclassReply);
                            if (validBookingClassResponse && bookingclassReply != null)
                            {
                                //Step-4: Ticket_CreateTSTFromPricingReply
                                Ticket_CreateTSTFromPricingReply tstReply = CreateTST(itinerary);
                                bool validTSTResponse = ValidateTSTFromPricingResponse(tstReply);
                                if (validTSTResponse && tstReply != null)
                                {
                                    //Step-5: PNR_Reply.
                                    PNR_Reply pnrReply1 = AddMultiElementsAfterTST(itinerary);
                                    bool validPNRResponse1 = ValidatePNRResponse(pnrReply1);
                                    if (validPNRResponse1 && pnrReply1 != null && !string.IsNullOrEmpty(pnrReply1.pnrHeader[0].reservationInfo[0].controlNumber))//PNR
                                    {

                                        string depDate = string.Empty;
                                        string depTime = string.Empty;
                                        string arrDate = string.Empty;
                                        string arrTime = string.Empty;

                                        bookResponse.PNR = pnrReply1.pnrHeader[0].reservationInfo[0].controlNumber;
                                        bookResponse.ProdType = ProductType.Flight;
                                        bookResponse.SSRDenied = false;
                                        bookResponse.SSRMessage = "";
                                        bookResponse.Status = BookingResponseStatus.Successful;
                                        itinerary.PNR = pnrReply1.pnrHeader[0].reservationInfo[0].controlNumber;

                                        for (int i = 0; i < itinerary.Segments.Length; i++)
                                        {
                                            int n = pnrReply1.originDestinationDetails[0].itineraryInfo[i].flightDetail.productDetails.duration.Length - 2;
                                            int hr = Convert.ToInt16(pnrReply1.originDestinationDetails[0].itineraryInfo[i].flightDetail.productDetails.duration.Substring(0, n));
                                            int min = Convert.ToInt16(pnrReply1.originDestinationDetails[0].itineraryInfo[i].flightDetail.productDetails.duration.Substring(n));
                                                                                        
                                            depDate = pnrReply1.originDestinationDetails[0].itineraryInfo[i].travelProduct.product.depDate;
                                            depTime = pnrReply1.originDestinationDetails[0].itineraryInfo[i].travelProduct.product.depTime;
                                            arrDate = pnrReply1.originDestinationDetails[0].itineraryInfo[i].travelProduct.product.arrDate;
                                            arrTime = pnrReply1.originDestinationDetails[0].itineraryInfo[i].travelProduct.product.arrTime;
                                            itinerary.Segments[i].DepartureTime = DateTime.ParseExact(depDate + depTime, "ddMMyyHHmm", null);
                                            itinerary.Segments[i].ArrivalTime = DateTime.ParseExact(arrDate + arrTime, "ddMMyyHHmm", null);
                                            itinerary.Segments[i].Duration = new TimeSpan(hr, min, 0);

                                            itinerary.Segments[i].Airline = pnrReply1.originDestinationDetails[0].itineraryInfo[i].travelProduct.companyDetail.identification;
                                            itinerary.Segments[i].FlightNumber = pnrReply1.originDestinationDetails[0].itineraryInfo[i].travelProduct.productDetails.identification;
                                            itinerary.Segments[i].BookingClass = pnrReply1.originDestinationDetails[0].itineraryInfo[i].travelProduct.productDetails.classOfService;
                                            itinerary.Segments[i].Craft = pnrReply1.originDestinationDetails[0].itineraryInfo[i].flightDetail.productDetails.equipment;

                                        }
                                        //Step-6 :Sign Out
                                        SignOut();
                                    }
                                    else
                                    {
                                        SignOut();
                                        bookResponse.Status = BookingResponseStatus.Failed;
                                        Audit.Add(EventType.Exception, Severity.High, 1, "(Amadeus)Failed to execute Book method.Reason:Not a Valid PNR_Reply", "");
                                        bookResponse.Error = "Failed to book !";
                                        throw new Exception("Not a Valid PNR_Reply");
                                    } //Step-5: PNR_Reply.
                                }
                                else
                                {
                                    SignOut();
                                    bookResponse.Status = BookingResponseStatus.Failed;
                                    Audit.Add(EventType.Exception, Severity.High, 1, "(Amadeus)Failed to execute Book method.Reason:Not a Valid Ticket_CreateTSTFromPricingReply", "");
                                    bookResponse.Error = "Failed to book !";
                                    throw new Exception("Not a Valid Ticket_CreateTSTFromPricingReply");
                                }//Step-4: Ticket_CreateTSTFromPricingReply

                            }
                            else
                            {
                                SignOut();
                                bookResponse.Status = BookingResponseStatus.Failed;
                                Audit.Add(EventType.Exception, Severity.High, 1, "(Amadeus)Failed to execute Book method.Reason:Not a Valid Fare_PricePNRWithBookingClassReply", "");
                                bookResponse.Error = "Failed to book !";
                                throw new Exception("Not a Valid Fare_PricePNRWithBookingClassReply");
                            }//Step-3: Fare_PricePNRWithBookingClassReply
                        }
                        else
                        {
                            SignOut();
                            bookResponse.Status = BookingResponseStatus.Failed;
                            Audit.Add(EventType.Exception, Severity.High, 1, "(Amadeus)Failed to execute Book method.Reason:Not a Valid PNR_Reply", "");
                            bookResponse.Error = "Failed to book !";
                            throw new Exception("Not a Valid PNR_Reply");
                        } //Step-2: PNR_Reply.
                    }
                    else
                    {
                        SignOut();
                        bookResponse.Status = BookingResponseStatus.Failed;
                        Audit.Add(EventType.Exception, Severity.High, 1, "(Amadeus)Failed to execute Book method.Reason:Not a Valid Air_SellFromRecommendationReply Response", "");
                        bookResponse.Error = "Failed to book !";
                        throw new Exception("Not a Valid Air_SellFromRecommendationReply");
                    }//Step-1: Air_SellFromRecommendationReply.
                }
                else
                {
                    bookResponse.Status = BookingResponseStatus.Failed;
                    Audit.Add(EventType.Exception, Severity.High, 1, "(Amadeus)Failed to execute Book method. Reason: Not a Valid Itinerary", "");
                    bookResponse.Error = "Failed to book !";
                    throw new Exception("Not a Valid Itinerary");
                }//itinerary Block
            }
            catch (Exception ex)
            {
                bookResponse.Status = BookingResponseStatus.Failed;
                bookResponse.Error = ex.Message;
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to execute Book method. Reason : " + ex.ToString(), "");
                throw new Exception("(Amadeus)Failed to execute Book method. Reason: " + ex.ToString(), ex);
            }
            return bookResponse;
        }

        #endregion



        #region 05:Issuance

        /// <summary>
        /// The Retrieve service is used to retrieve and display an active Passenger Name Record (PNR) or to redisplay a PNR during the current user session.
        /// </summary>
        /// <param name="PNR"></param>
        /// <returns></returns>
        public PNR_Reply RetrievePNR(string PNR)
        {
            //hSession.handleSession(SessionHandler.TransactionStatusCode.None);
            //hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.None);

            //Session session = hSession.Session;
            //TransactionFlowLinkType link = hLink.Link;

            hSession.handleSession(SessionHandler.TransactionStatusCode.Start);
            hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.New);
            //hAddressing.update();
            Session session = hSession.Session;
            TransactionFlowLinkType link = hLink.Link;

            PNR_Reply pnrReply = null;
            try
            {
                if (!string.IsNullOrEmpty(PNR))
                {
                    PNR_Retrieve retrivePNR = new PNR_Retrieve();
                    retrivePNR.retrievalFacts = new PNR_RetrieveRetrievalFacts();
                    retrivePNR.retrievalFacts.retrieve = new RetrievePNRType();
                    retrivePNR.retrievalFacts.retrieve.type = "2";//Retrieve by record locator

                    retrivePNR.retrievalFacts.reservationOrProfileIdentifier = new ReservationControlInformationDetailsType3[1];
                    retrivePNR.retrievalFacts.reservationOrProfileIdentifier[0] = new ReservationControlInformationDetailsType3();
                    retrivePNR.retrievalFacts.reservationOrProfileIdentifier[0].controlNumber = PNR;


                    PNR_RetrieveRequest pnr_RetrieveRequest = new PNR_RetrieveRequest(session, link, hSecurity.getHostedUser(), retrivePNR);
                    //Serialize PNR_RetrieveRequest
                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_PNR_RetrieveRequest_" + PNR + "_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pnr_RetrieveRequest.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, pnr_RetrieveRequest);
                        sw.Close();
                    }
                    catch { }
                    pnrReply = client.PNR_Retrieve(ref session, ref link, hSecurity.getHostedUser(), retrivePNR);

                    //hSession.Session = session;
                    //hLink.Link = link;
                    //hSession.AfterReply(SessionHandler.TransactionStatusCode.None);
                    //hLink.AfterReply(TransactionFlowLinkHandler.TransactionFlowLinkAction.None);

                    hSession.Session = session;
                    hLink.Link = link;
                    hSession.AfterReply(SessionHandler.TransactionStatusCode.Continue);
                    hLink.AfterReply(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                    //hAddressing.update();
                    if (pnrReply != null)
                    {
                        //hSession.Session = session;
                        //hLink.Link = link;
                        //hSession.handleSession(SessionHandler.TransactionStatusCode.Continue);
                        //hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowU_PNR_RetrieveRequest_p);

                        PNR_AddMultiElementsResponse pnr_Retrieve2Response = new PNR_AddMultiElementsResponse(session, link, pnrReply);
                        //Serialize DocIssuance_IssueTicketResponse
                        try
                        {
                            string filepath = xmlPath + sessionId + "_" + appUserId + "_PNR_RetrieveResponse_" + PNR + "_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                            XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pnr_Retrieve2Response.GetType());
                            StreamWriter sw = new StreamWriter(filepath);
                            xsr.Serialize(sw, pnr_Retrieve2Response);
                            sw.Close();
                        }
                        catch { }
                    }

                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to execute RetrievePNR method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return pnrReply;
        }


        public PNR_Reply RetrieveStatelessPNR(string PNR)
        {
            hSession.handleSession(SessionHandler.TransactionStatusCode.None);
            hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.None);
            //hAddressing.update();
            Session session = hSession.Session;
            TransactionFlowLinkType link = hLink.Link;



            PNR_Reply pnrReply = null;
            try
            {
                if (!string.IsNullOrEmpty(PNR))
                {
                    PNR_Retrieve retrivePNR = new PNR_Retrieve();
                    retrivePNR.retrievalFacts = new PNR_RetrieveRetrievalFacts();
                    retrivePNR.retrievalFacts.retrieve = new RetrievePNRType();
                    retrivePNR.retrievalFacts.retrieve.type = "2";//Retrieve by record locator

                    retrivePNR.retrievalFacts.reservationOrProfileIdentifier = new ReservationControlInformationDetailsType3[1];
                    retrivePNR.retrievalFacts.reservationOrProfileIdentifier[0] = new ReservationControlInformationDetailsType3();
                    retrivePNR.retrievalFacts.reservationOrProfileIdentifier[0].controlNumber = PNR;


                    PNR_RetrieveRequest pnr_RetrieveRequest = new PNR_RetrieveRequest(session, link, hSecurity.getHostedUser(), retrivePNR);
                    //Serialize PNR_RetrieveRequest
                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_PNR_RetrieveRequest_" + PNR + "_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pnr_RetrieveRequest.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, pnr_RetrieveRequest);
                        sw.Close();
                    }
                    catch { }
                    pnrReply = client.PNR_Retrieve(ref session, ref link, hSecurity.getHostedUser(), retrivePNR);

                    hSession.Session = session;
                    hLink.Link = link;
                    hSession.AfterReply(SessionHandler.TransactionStatusCode.None);
                    hLink.AfterReply(TransactionFlowLinkHandler.TransactionFlowLinkAction.None);
                    //hAddressing.update();

                    if (pnrReply != null)
                    {

                        PNR_AddMultiElementsResponse pnr_Retrieve2Response = new PNR_AddMultiElementsResponse(session, link, pnrReply);
                        //Serialize PNR_AddMultiElementsResponse
                        try
                        {
                            string filepath = xmlPath + sessionId + "_" + appUserId + "_PNR_RetrieveResponse_" + PNR + "_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                            XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pnr_Retrieve2Response.GetType());
                            StreamWriter sw = new StreamWriter(filepath);
                            xsr.Serialize(sw, pnr_Retrieve2Response);
                            sw.Close();
                        }
                        catch { }
                    }

                }
            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to execute RetrievePNR method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return pnrReply;
        }

        /// <summary>
        /// The service is used to ISSUE A TICKET
        /// </summary>
        /// <returns></returns>
        public DocIssuance_IssueTicketReply RetrieveDocIssuanceTicket()
        {
            DocIssuance_IssueTicketReply docReply = null;
            try
            {
                hSession.handleSession(SessionHandler.TransactionStatusCode.Continue);
                hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                Session session = hSession.Session;
                TransactionFlowLinkType link = hLink.Link;
                //hAddressing.update();
                DocIssuance_IssueTicket docRequest = new DocIssuance_IssueTicket();
                docRequest.optionGroup = new DocIssuance_IssueTicketOptionGroup[1];
                docRequest.optionGroup[0] = new DocIssuance_IssueTicketOptionGroup();
                docRequest.optionGroup[0].switches = new StatusTypeI();
                docRequest.optionGroup[0].switches.statusDetails = new StatusDetailsTypeI();
                docRequest.optionGroup[0].switches.statusDetails.indicator = "ET"; //Electronic Override

                DocIssuance_IssueTicketRequest docIssuanceIssueTicketRequest = new DocIssuance_IssueTicketRequest(session, link, hSecurity.getHostedUser(), docRequest);
                //Serialize DocIssuance_IssueTicketRequest
                try
                {
                    string filepath = xmlPath + sessionId + "_" + appUserId + "_DocIssuance_IssueTicketRequest_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(docIssuanceIssueTicketRequest.GetType());
                    StreamWriter sw = new StreamWriter(filepath);
                    xsr.Serialize(sw, docIssuanceIssueTicketRequest);
                    sw.Close();
                }
                catch { }

                docReply = client.DocIssuance_IssueTicket(ref session, ref link, hSecurity.getHostedUser(), docRequest);

                hSession.Session = session;
                hLink.Link = link;
                hSession.AfterReply(SessionHandler.TransactionStatusCode.Continue);
                hLink.AfterReply(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                //hAddressing.update();

                if (docReply != null)
                {
                    // hSession.Session = session;
                    // hLink.Link = link;
                    // hSession.handleSession(SessionHandler.TransactionStatusCode.Continue);
                    // hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);
                    DocIssuance_IssueTicketResponse docIssuanceResponse = new DocIssuance_IssueTicketResponse(session, link, docReply);
                    //Serialize DocIssuance_IssueTicketResponse
                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_DocIssuance_IssueTicketResponse_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(docIssuanceResponse.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, docIssuanceResponse);
                        sw.Close();
                    }
                    catch { }



                }
            }
            catch (Exception ex)
            {
                SignOut();
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to execute RetrieveDocIssuanceTicket method. Reason : " + ex.ToString(), "");
                throw ex;
            }
            return docReply;

        }

        #endregion

        #region 06:Cancellation
        /// <summary>
        /// The CancelPNR method is used to specific actions to be processed on a PNR such as identifying the PNR elements and sub elements that need to be cancelled.  
        /// </summary>
        /// <param name="PNR"></param>
        /// <returns></returns>
        public PNR_Reply CancelPNR(string PNR)
        {
            PNR_Reply pnrReply = null;
            try
            {
                hSession.handleSession(SessionHandler.TransactionStatusCode.None);
                hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.None);
                //hAddressing.update();
                Session session = hSession.Session;
                TransactionFlowLinkType link = hLink.Link;

                PNR_Cancel pnrCancel = new PNR_Cancel();
                pnrCancel.reservationInfo = new ReservationControlInformationType2();
                pnrCancel.reservationInfo.reservation = new ReservationControlInformationDetailsTypeI3();
                pnrCancel.reservationInfo.reservation.controlNumber = PNR;

                pnrCancel.pnrActions = new string[1];
                pnrCancel.pnrActions[0] = "10"; //End transact(ET)

                pnrCancel.cancelElements = new CancelPNRElementType[1];
                pnrCancel.cancelElements[0] = new CancelPNRElementType();
                pnrCancel.cancelElements[0].entryType = "I";//Cancel itinerary type

                PNR_CancelRequest cancelRequest = new PNR_CancelRequest(session, link, hSecurity.getHostedUser(), pnrCancel);
                //Serialize PNR_CancelRequest
                try
                {
                    string filepath = xmlPath + sessionId + "_" + appUserId + "_PNR_CancelBookingRequest_" + PNR + "_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(cancelRequest.GetType());
                    StreamWriter sw = new StreamWriter(filepath);
                    xsr.Serialize(sw, cancelRequest);
                    sw.Close();
                }
                catch { }

                pnrReply = client.PNR_Cancel(ref session, ref link, hSecurity.getHostedUser(), pnrCancel);
                hSession.Session = session;
                hLink.Link = link;
                hSession.AfterReply(SessionHandler.TransactionStatusCode.None);
                hLink.AfterReply(TransactionFlowLinkHandler.TransactionFlowLinkAction.None);
                //hAddressing.update();
                if (pnrReply != null)
                {


                    PNR_AddMultiElementsResponse pnr_CancelResponse = new PNR_AddMultiElementsResponse(session, link, pnrReply);
                    //Serialize PNR_AddMultiElementsResponse
                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_PNR_CancelBookingResponse_" + PNR  + "_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(pnr_CancelResponse.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, pnr_CancelResponse);
                        sw.Close();
                    }
                    catch { }

                    //SignOut();
                }

            }
            catch (Exception ex)
            {

                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to execute CancelPNR method. Reason : " + ex.ToString(), "");
                throw ex;

            }
            return pnrReply;
        }

        public string CancelItinerary(string PNR)
        {
            /* ==========START:STEPS TO CANCEL THE BOOKING WHICH IS NOT TICKETED====
             * 
             * STEP-1:PASS  THE PNR AND GET THE PNR_Reply
             * STEP-2:VALIDATE THE PNR_Reply.
             * STEP-3:IN THE PNR_Reply MAKE sure the booking does not contain any FA lines.
             * STEP-4:THEN call the CancelPNR method for successul cancellation of the booking.
             * 
             *=========START:STEPS TO CANCEL THE BOOKING WHICH IS NOT TICKETED=====*/

            string response = string.Empty;
            try
            {
                GenerateHeader();//Generates The SOAP Header Request
                if (!string.IsNullOrEmpty(PNR) && PNR.Length > 0)
                {
                    PNR_Reply pnrResponse = RetrieveStatelessPNR(PNR);
                    if (pnrResponse != null && ValidatePNRResponse(pnrResponse))
                    {
                        PNR_Reply pnrCancelResponse = CancelPNR(PNR);
                        if (pnrCancelResponse != null && ValidatePNRResponse(pnrCancelResponse))
                        {
                            response = PNR; //If there is no error node then the booking got cancelled successfully.
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.CancellBooking, Severity.High, appUserId, "(Amadeus)Failed to Cancel Booking.Reason : " + ex.ToString(), "");
            }
            return response;
        }

        #endregion

        #region 07:Ticketing Methods

        /// <summary>
        ///  The service is used to ISSUE A TICKET in the Amadeus System
        /// </summary>
        /// <param name="itinerary"></param>
        /// <param name="tickets"></param>
        /// <returns></returns>
        public TicketingResponse CreateTicket(ref FlightItinerary itinerary, out Ticket[] tickets)
        {
            TicketingResponse ticketingResponse = new TicketingResponse();
            tickets = new Ticket[0];
            GenerateHeader();
            try
            {
                if (itinerary != null && !string.IsNullOrEmpty(itinerary.PNR) && itinerary.PNR.Length > 0)
                {
                    PNR_Reply pnrReply = RetrievePNR(itinerary.PNR);
                    if (ValidatePNRResponse(pnrReply))
                    {
                        DocIssuance_IssueTicketReply docReply = RetrieveDocIssuanceTicket();
                        SignOut();
                        if (ValidateDocIssuanceIssueTicketReply(docReply))
                        {
                            PNR_Reply pnrResponse = RetrieveStatelessPNR(itinerary.PNR);
                            List<Ticket> tempTicket = new List<Ticket>();
                            ticketingResponse = TempCreateTicket(ref itinerary, ref tempTicket, pnrResponse);
                            tickets = tempTicket.ToArray();
                        }
                        else
                        {

                            ticketingResponse.Message = "Not a valid DocIssuance_IssueTicketReply";
                            ticketingResponse.Status = TicketingResponseStatus.OtherError;
                            throw new Exception("Not a valid DOC Reply");
                        }
                    }
                    else
                    {
                        SignOut();
                        ticketingResponse.Message = "Not a valid PNR_Reply";
                        ticketingResponse.Status = TicketingResponseStatus.OtherError;
                        throw new Exception("Not a valid PNR");
                    }
                }
            }
            catch (Exception ex)
            {
                ticketingResponse.Message = ex.Message;
                ticketingResponse.Status = TicketingResponseStatus.OtherError;
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to generate Ticket. Error : " + ex.ToString(), "");
            }
            return ticketingResponse;
        }

        private TicketingResponse TempCreateTicket(ref FlightItinerary itinerary, ref List<Ticket> tickets, PNR_Reply pnrReply)
        {
            TicketingResponse ticketingResponse = new TicketingResponse();
            try
            {
                List<PNR_ReplyDataElementsMasterDataElementsIndiv> ElementsDiv = new List<PNR_ReplyDataElementsMasterDataElementsIndiv>();
                ElementsDiv.AddRange(pnrReply.dataElementsMaster.dataElementsIndiv);
                List<PNR_ReplyDataElementsMasterDataElementsIndiv> TicketElements = ElementsDiv.FindAll(delegate (PNR_ReplyDataElementsMasterDataElementsIndiv ed) { return ed.elementManagementData.segmentName == "FA" && ed.otherDataFreetext[0].freetextDetail.type == "P06"; });

                if (TicketElements == null)
                {
                    TicketElements = ElementsDiv;
                }
                int segmentCount = itinerary.Segments.Length;

                Dictionary<int, string> PaxTickets = new Dictionary<int, string>();
                string ticketNumber = string.Empty;
                int ticketId = 0;
                foreach (PNR_ReplyDataElementsMasterDataElementsIndiv dataElementsIndiv in TicketElements)
                {
                    //Ticket Number Automated Tickets element
                    if (dataElementsIndiv.elementManagementData.segmentName == "FA")
                    {
                        ReferencingDetailsType_111975C rde = dataElementsIndiv.referenceForDataElement[segmentCount];
                        if (rde.qualifier == "PT")
                        {
                            string[] data = dataElementsIndiv.otherDataFreetext[0].longFreetext.Split('/');
                            ticketNumber = data[0].Replace("PAX", "").Replace("INF", "").Replace("-", "").Trim();//Read Ticket Number
                            PaxTickets.Add(ticketId, ticketNumber);
                            ticketId++;
                        }
                    }
                }
                itinerary.PNR = pnrReply.pnrHeader[0].reservationInfo[0].controlNumber;
                ticketingResponse.Status = TicketingResponseStatus.Successful;
                ticketId = 0;
                foreach (FlightPassenger pax in itinerary.Passenger)
                {
                    Ticket ticket = new Ticket();
                    ticket.CreatedBy = pax.CreatedBy;
                    ticket.CreatedOn = pax.CreatedOn;
                    ticket.ETicket = true;
                    ticket.FareRule = string.Empty;
                    ticket.ConjunctionNumber = string.Empty;
                    ticket.CorporateCode = string.Empty;
                    ticket.Endorsement = string.Empty;
                    ticket.FareCalculation = string.Empty;
                    ticket.FareRule = string.Empty;
                    ticket.FlightId = pax.FlightId;
                    ticket.FOP = string.Empty;
                    ticket.IssueDate = DateTime.Now;
                    ticket.IssueInExchange = string.Empty;
                    ticket.LastModifiedBy = pax.LastModifiedBy;
                    ticket.LastModifiedOn = pax.LastModifiedOn;
                    ticket.OriginalIssue = string.Empty;
                    ticket.PaxFirstName = pax.FirstName;
                    ticket.PaxId = pax.PaxId;
                    ticket.PaxLastName = pax.LastName;
                    ticket.PaxType = pax.Type;
                    ticket.Price = pax.Price;
                    ticket.PtcDetail = new List<SegmentPTCDetail>();
                    for (int j = 0; j < itinerary.Segments.Length; j++)
                    {
                        SegmentPTCDetail ptcDetail = new SegmentPTCDetail();
                        FlightInfo segment = itinerary.Segments[j];
                        ptcDetail.FareBasis = "";
                        //  if (!string.IsNullOrEmpty(pax.BaggageCode) && pax.Type != PassengerType.Infant)  commented by bangar
                        if (!string.IsNullOrEmpty(pax.BaggageCode) )
                        {
                            ptcDetail.Baggage = pax.BaggageCode;
                        }
                        else
                        {
                            ptcDetail.Baggage = "";

                        }
                        ptcDetail.FlightKey = segment.FlightKey;
                        ptcDetail.NVA = "";
                        ptcDetail.NVB = "";
                        ptcDetail.SegmentId = segment.SegmentId;
                        switch (pax.Type)
                        {
                            case PassengerType.Adult:
                                ptcDetail.PaxType = "ADT";
                                break;
                            case PassengerType.Child:
                                ptcDetail.PaxType = "CNN";
                                break;
                            case PassengerType.Infant:
                                ptcDetail.PaxType = "INF";
                                break;
                        }
                        ticket.PtcDetail.Add(ptcDetail);
                    }
                    ticket.Remarks = string.Empty;
                    ticket.Status = "OK";
                    ticket.StockType = string.Empty;
                    ticket.TaxBreakup = new List<KeyValuePair<string, decimal>>();
                    ticket.TicketAdvisory = string.Empty;
                    ticket.TicketDesignator = string.Empty;
                    ticket.TicketType = string.Empty;
                    ticket.Title = pax.Title;
                    ticket.TourCode = string.Empty;
                    ticket.ValidatingAriline = itinerary.ValidatingAirline;
                    ticket.TicketNumber = PaxTickets[ticketId];
                    tickets.Add(ticket);
                    ticketId++;
                }
                Audit.Add(EventType.Exception, Severity.Normal, 1, "(Amadeus)Ticketing Successful PNR " + itinerary.PNR + " for " + itinerary.Segments[0].Origin.CityCode + "-" + itinerary.Segments[0].Destination.CityCode, "");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to generate Ticket. Reason : " + ex.ToString(), "");
                ticketingResponse.Message = ex.Message;
                ticketingResponse.Status = TicketingResponseStatus.OtherError;
                throw ex;
            }
            return ticketingResponse;
        }

        #endregion

        #region 08: VOID PROCESS

        /// <summary>
        /// This service is available for both travel agencies and airline offices (ATO/CTO). It cancels documents before the sales report closure, when no monetary transfer has been executed.
        /// </summary>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public Ticket_CancelDocumentReplyTransactionResults[] TicketCancelDocument(string ticketNumber)
        {
            GenerateHeader();
            Ticket_CancelDocumentReplyTransactionResults[] cancelledResults = null;
            try
            {
                hSession.handleSession(SessionHandler.TransactionStatusCode.None);
                hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.None);
                //hAddressing.update();
                Session session = hSession.Session;
                TransactionFlowLinkType link = hLink.Link;

                Ticket_CancelDocument cancelDoc = new Ticket_CancelDocument();

                //documentNumberDetails
                cancelDoc.documentNumberDetails = new TicketNumberTypeI4();
                cancelDoc.documentNumberDetails.documentDetails = new TicketNumberDetailsTypeI4();
                cancelDoc.documentNumberDetails.documentDetails.number = ticketNumber.Replace("-", "");

                //stockProviderDetails
                cancelDoc.stockProviderDetails = new OfficeSettingsDetailsType();
                cancelDoc.stockProviderDetails.officeSettingsDetails = new DocumentInfoFromOfficeSettingType();
                cancelDoc.stockProviderDetails.officeSettingsDetails.marketIataCode = marketIataCode;

                //targetOfficeDetails
                cancelDoc.targetOfficeDetails = new AdditionalBusinessSourceInformationType1();
                cancelDoc.targetOfficeDetails.originatorDetails = new OriginatorIdentificationDetailsType1();
                cancelDoc.targetOfficeDetails.originatorDetails.inHouseIdentification2 = officeId;

                Ticket_CancelDocumentRequest cancelRequest = new Ticket_CancelDocumentRequest(session, link, hSecurity.getHostedUser(), cancelDoc);

                //Serialize Ticket_CancelDocumentRequest
                try
                {
                    string filepath = xmlPath + sessionId + "_" + appUserId + "_Ticket_CancelDocumentRequest_" + ticketNumber + "_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(cancelRequest.GetType());
                    StreamWriter sw = new StreamWriter(filepath);
                    xsr.Serialize(sw, cancelRequest);
                    sw.Close();
                }
                catch { }

                cancelledResults = client.Ticket_CancelDocument(ref session, ref link, hSecurity.getHostedUser(), cancelDoc);
                if (cancelledResults != null && cancelledResults.Length > 0)
                {


                    hSession.Session = session;
                    hLink.Link = link;
                    hSession.AfterReply(SessionHandler.TransactionStatusCode.None);
                    hLink.AfterReply(TransactionFlowLinkHandler.TransactionFlowLinkAction.None);
                    //hAddressing.update();
                    Ticket_CancelDocumentResponse cancelResponse = new Ticket_CancelDocumentResponse(session, link, cancelledResults);
                    //Serialize Ticket_CancelDocumentResponse
                    try
                    {
                        string filepath = xmlPath + sessionId + "_" + appUserId + "_Ticket_CancelDocumentResponse_" + ticketNumber + "_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                        XmlSerializer xsr = new System.Xml.Serialization.XmlSerializer(cancelResponse.GetType());
                        StreamWriter sw = new StreamWriter(filepath);
                        xsr.Serialize(sw, cancelResponse);
                        sw.Close();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to execute TicketCancelDocument method. Reason : " + ex.ToString(), "");
            }
            return cancelledResults;
        }


        public Dictionary<string, string> CancelBooking(FlightItinerary itinerary)
        {
            Dictionary<string, string> cancellationData = new Dictionary<string, string>();
            try
            {
                GenerateHeader();
                PNR_Reply pnrReply = RetrieveStatelessPNR(itinerary.PNR);
                if (pnrReply != null && ValidatePNRResponse(pnrReply))
                {
                    List<PNR_ReplyDataElementsMasterDataElementsIndiv> ElementsDiv = new List<PNR_ReplyDataElementsMasterDataElementsIndiv>();
                    ElementsDiv.AddRange(pnrReply.dataElementsMaster.dataElementsIndiv);
                    List<PNR_ReplyDataElementsMasterDataElementsIndiv> TicketElements = ElementsDiv.FindAll(delegate (PNR_ReplyDataElementsMasterDataElementsIndiv ed) { return ed.elementManagementData.segmentName == "FA" && ed.otherDataFreetext[0].freetextDetail.type == "P06"; });

                    if (TicketElements == null)
                    {
                        TicketElements = ElementsDiv;
                    }
                    int segmentCount = itinerary.Segments.Length;
                    Dictionary<int, string> PaxTickets = new Dictionary<int, string>();
                    string ticketNumber = string.Empty;
                    int ticketId = 0;

                    foreach (PNR_ReplyDataElementsMasterDataElementsIndiv dataElementsIndiv in TicketElements)
                    {
                        //Ticket Number Automated Tickets element
                        if (dataElementsIndiv.elementManagementData.segmentName == "FA")
                        {
                            ReferencingDetailsType_111975C rde = dataElementsIndiv.referenceForDataElement[segmentCount];
                            if (rde.qualifier == "PT")
                            {
                                string[] data = dataElementsIndiv.otherDataFreetext[0].longFreetext.Split('/');
                                ticketNumber = data[0].Replace("PAX", "").Replace("INF", "");//Read Ticket Number
                                ticketNumber = ticketNumber.Split('-')[0] + ticketNumber.Split('-')[1];
                                PaxTickets.Add(ticketId, ticketNumber);
                                ticketId++;
                            }
                        }
                    }

                    if (PaxTickets.Count > 0)
                    {
                        ticketId = 0;
                        Dictionary<int, bool> ticketsCancellationResponse = new Dictionary<int, bool>();

                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            Ticket_CancelDocumentReplyTransactionResults[] result = TicketCancelDocument(PaxTickets[ticketId].Trim());
                            bool ticketCancelledResponse = ValidateTicketCancelDocumentResponse(result);
                            ticketsCancellationResponse.Add(ticketId, ticketCancelledResponse);
                            ticketId++;
                        }
                        //After the TicketCancelDocument call the CancelItinerary method
                        if (!string.IsNullOrEmpty(itinerary.PNR))
                        {
                            string cancellationResponse = CancelItinerary(itinerary.PNR);
                            if (cancellationResponse == itinerary.PNR)
                            {
                                cancellationData.Add("Cancelled", "True");
                            }
                            else
                            {
                                cancellationData.Add("Cancelled", "False");
                            }
                        }
                        if (ticketsCancellationResponse.Count > 0)
                        {
                            int failedTicketsCount = 0;
                            for (int i = 0; i < ticketsCancellationResponse.Count; i++)
                            {
                                if (ticketsCancellationResponse[i] == false)
                                {
                                    failedTicketsCount++;
                                }
                            }
                            if (failedTicketsCount > 0)
                            {
                                cancellationData.Add("Cancelled", "False");
                            }
                            else
                            {
                                cancellationData.Add("Cancelled", "True");
                            }

                        }
                        else
                        {
                            cancellationData.Add("Cancelled", "False");
                        }
                    }
                    else
                    {
                        cancellationData.Add("Cancelled", "False");
                    }
                }
                else
                {
                    cancellationData.Add("Cancelled", "False");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to execute TicketCancel method. Reason : " + ex.ToString(), "");
            }
            return cancellationData;
        }

        #endregion

        #region Validate Response Methods

        public bool ValidateSeatCount(Air_SellFromRecommendationReply sellReply, int seatCount)
        {
            bool success = true;
            if (sellReply.itineraryDetails != null && sellReply.itineraryDetails.Length > 0)
            {
                for (int i = 0; i < sellReply.itineraryDetails.Length; i++)
                {
                    for (int p = 0; p < sellReply.itineraryDetails[i].segmentInformation.Length; p++)
                    {
                        if (sellReply.itineraryDetails[i].segmentInformation[p].actionDetails.statusCode[0] == "OK")//Need to change this..
                        {
                            if (Convert.ToInt32(sellReply.itineraryDetails[i].segmentInformation[p].actionDetails.quantity) != seatCount)
                            {
                                success = false;
                                break;
                            }
                        }
                        else
                        {
                            success = false;
                            break;
                        }
                    }
                }
            }
            return success;
        }

        public bool ValidatePNRResponse(PNR_Reply pnrReply)
        {
            bool validResponse = true;
            string errorMessage = string.Empty;
            if (pnrReply != null && pnrReply.generalErrorInfo != null && pnrReply.generalErrorInfo.Length > 0)
            {
                validResponse = false;
                for (int i = 0; i < pnrReply.generalErrorInfo.Length; i++)
                {
                    if (pnrReply.generalErrorInfo[i].errorOrWarningCodeDetails != null && pnrReply.generalErrorInfo[i].errorOrWarningCodeDetails.errorDetails != null && !string.IsNullOrEmpty(pnrReply.generalErrorInfo[i].errorOrWarningCodeDetails.errorDetails.errorCode))
                    {
                        errorMessage += "Error Code:" + pnrReply.generalErrorInfo[i].errorOrWarningCodeDetails.errorDetails.errorCode;
                    }
                    if (pnrReply.generalErrorInfo[i].errorOrWarningCodeDetails != null && pnrReply.generalErrorInfo[i].errorOrWarningCodeDetails.errorDetails != null && !string.IsNullOrEmpty(pnrReply.generalErrorInfo[i].errorOrWarningCodeDetails.errorDetails.errorCategory))
                    {
                        errorMessage += ";Error Category:" + pnrReply.generalErrorInfo[i].errorOrWarningCodeDetails.errorDetails.errorCategory;
                    }
                    if (pnrReply.generalErrorInfo[i].errorWarningDescription != null && pnrReply.generalErrorInfo[i].errorWarningDescription.freeText.Length > 0)
                    {
                        errorMessage += ";Error Description:";
                        for (int m = 0; m < pnrReply.generalErrorInfo[i].errorWarningDescription.freeText.Length; m++)
                        {
                            if (!string.IsNullOrEmpty(pnrReply.generalErrorInfo[i].errorWarningDescription.freeText[m]))
                            {
                                errorMessage += pnrReply.generalErrorInfo[i].errorWarningDescription.freeText[m];
                            }
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(errorMessage) && errorMessage.Length > 0)
            {
                validResponse = false;
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)did not receive valid PNR_Reply:Reason-" + errorMessage.ToString(), "");
                throw new Exception("Not a Valid PNR_Reply.Reason:" + errorMessage);
            }
            return validResponse;
        }

        public bool ValidatePNRWithBookingClassResponse(Fare_PricePNRWithBookingClassReply bookingClassReply)
        {
            bool validResponse = true;
            string errorMessage = string.Empty;
            if (bookingClassReply != null && bookingClassReply.applicationError != null)
            {
                validResponse = false;

                if (bookingClassReply.applicationError.errorOrWarningCodeDetails != null && bookingClassReply.applicationError.errorOrWarningCodeDetails.errorDetails != null && !string.IsNullOrEmpty(bookingClassReply.applicationError.errorOrWarningCodeDetails.errorDetails.errorCode))
                {
                    errorMessage += "Error Code:" + bookingClassReply.applicationError.errorOrWarningCodeDetails.errorDetails.errorCode;
                }
                if (bookingClassReply.applicationError.errorOrWarningCodeDetails != null && bookingClassReply.applicationError.errorOrWarningCodeDetails.errorDetails != null && !string.IsNullOrEmpty(bookingClassReply.applicationError.errorOrWarningCodeDetails.errorDetails.errorCategory))
                {
                    errorMessage += ";Error Category:" + bookingClassReply.applicationError.errorOrWarningCodeDetails.errorDetails.errorCategory;
                }
                if (bookingClassReply.applicationError.errorWarningDescription != null && bookingClassReply.applicationError.errorWarningDescription.freeText.Length > 0)
                {
                    errorMessage += ";Error Description:";
                    for (int m = 0; m < bookingClassReply.applicationError.errorWarningDescription.freeText.Length; m++)
                    {
                        if (!string.IsNullOrEmpty(bookingClassReply.applicationError.errorWarningDescription.freeText[m]))
                        {
                            errorMessage += bookingClassReply.applicationError.errorWarningDescription.freeText[m];
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(errorMessage) && errorMessage.Length > 0)
            {
                validResponse = false;
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)did not receive valid Fare_PricePNRWithBookingClassReply:Reason-" + errorMessage.ToString(), "");
                throw new Exception("Not a Valid Fare_PricePNRWithBookingClassReply.Reason:" + errorMessage);
            }
            return validResponse;
        }

        public bool ValidateTSTFromPricingResponse(Ticket_CreateTSTFromPricingReply tstReply)
        {
            bool validResponse = true;
            string errorMessage = string.Empty;
            if (tstReply != null && tstReply.applicationError != null)
            {
                validResponse = false;
                if (tstReply.applicationError.applicationErrorInfo != null && tstReply.applicationError.applicationErrorInfo.applicationErrorDetail != null && !string.IsNullOrEmpty(tstReply.applicationError.applicationErrorInfo.applicationErrorDetail.applicationErrorCode))
                {
                    errorMessage += "Error Code:" + tstReply.applicationError.applicationErrorInfo.applicationErrorDetail.applicationErrorCode;
                }
                if (tstReply.applicationError.errorText != null && !string.IsNullOrEmpty(tstReply.applicationError.errorText.errorFreeText))
                {
                    errorMessage += ";Error Description:" + tstReply.applicationError.errorText.errorFreeText;
                }
            }
            if (!string.IsNullOrEmpty(errorMessage) && errorMessage.Length > 0)
            {
                validResponse = false;
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)did not receive valid Ticket_CreateTSTFromPricingReply:Reason-" + errorMessage.ToString(), "");
                throw new Exception("Not a Valid Ticket_CreateTSTFromPricingReply.Reason:" + errorMessage);
            }
            return validResponse;
        }

        public bool ValidateSellResponse(Air_SellFromRecommendationReply sellResponse)
        {
            bool validResponse = true;
            string errorMessage = string.Empty;
            if (sellResponse != null && sellResponse.errorAtMessageLevel != null && sellResponse.errorAtMessageLevel.Length > 0)
            {
                validResponse = false;

                for (int i = 0; i < sellResponse.errorAtMessageLevel.Length; i++)
                {
                    if (sellResponse.errorAtMessageLevel[i].errorSegment != null && sellResponse.errorAtMessageLevel[i].errorSegment.errorDetails != null && !string.IsNullOrEmpty(sellResponse.errorAtMessageLevel[i].errorSegment.errorDetails.errorCode))
                    {
                        errorMessage += "Error Code:" + sellResponse.errorAtMessageLevel[i].errorSegment.errorDetails.errorCode;
                    }
                    if (sellResponse.errorAtMessageLevel[i].errorSegment != null && sellResponse.errorAtMessageLevel[i].errorSegment.errorDetails != null && !string.IsNullOrEmpty(sellResponse.errorAtMessageLevel[i].errorSegment.errorDetails.errorCategory))
                    {
                        errorMessage += ";Error Category:" + sellResponse.errorAtMessageLevel[i].errorSegment.errorDetails.errorCategory;
                    }
                    if (sellResponse.errorAtMessageLevel[i].informationText != null && sellResponse.errorAtMessageLevel[i].informationText.freeText.Length > 0)
                    {
                        errorMessage += ";Error Description:";
                        for (int m = 0; m < sellResponse.errorAtMessageLevel[i].informationText.freeText.Length; m++)
                        {
                            if (!string.IsNullOrEmpty(sellResponse.errorAtMessageLevel[i].informationText.freeText[m]))
                            {
                                errorMessage += sellResponse.errorAtMessageLevel[i].informationText.freeText[m];
                            }
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(errorMessage) && errorMessage.Length > 0)
            {
                validResponse = false;
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)did not receive valid Air_SellFromRecommendationReply:Reason-" + errorMessage.ToString(), "");

                throw new Exception("Not a Valid Air_SellFromRecommendationReply.Reason:" + errorMessage);
            }
            return validResponse;
        }

        public bool ValidateDocIssuanceIssueTicketReply(DocIssuance_IssueTicketReply docResponse)
        {
            bool validResponse = true;
            string errorMessage = string.Empty;
            if (docResponse.processingStatus != null && docResponse.processingStatus.statusCode.ToLower() == "o")//OK
            {
                validResponse = true;
            }
            else
            {
                if (docResponse != null && docResponse.errorGroup != null && docResponse.errorGroup.errorOrWarningCodeDetails != null && docResponse.errorGroup.errorOrWarningCodeDetails.errorDetails != null)
                {
                    validResponse = false;
                    if (!string.IsNullOrEmpty(docResponse.errorGroup.errorOrWarningCodeDetails.errorDetails.errorCode))
                    {
                        errorMessage += "Error Code:" + docResponse.errorGroup.errorOrWarningCodeDetails.errorDetails.errorCode;
                    }
                }
                if (docResponse != null && docResponse.errorGroup != null && docResponse.errorGroup.errorWarningDescription != null && !string.IsNullOrEmpty(docResponse.errorGroup.errorWarningDescription.freeText))
                {
                    errorMessage += ";Error Description:" + docResponse.errorGroup.errorWarningDescription.freeText;
                }

                if (!string.IsNullOrEmpty(errorMessage) && errorMessage.Length > 0)
                {
                    validResponse = false;
                    Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)did not receive valid DocIssuance_IssueTicketReply:Reason-" + errorMessage.ToString(), "");
                    throw new Exception("Not a Valid ValidateDocIssuanceIssueTicketReply.Reason:" + errorMessage);
                }
            }
            return validResponse;
        }

        public bool ValidateMiniRuleReponse(MiniRule_GetFromPricingReply response)
        {
            //MiniRule_GetFromPricingReply/responseDetails/statusCode
            //N:Recoverable error
            //O:OK processed. Further data in further segments follow

            bool validResponse = true;
            string errorMessage = string.Empty;
            if (response != null && response.errorWarningGroup != null && response.errorWarningGroup.errorOrWarningCodeDetails != null && response.errorWarningGroup.errorOrWarningCodeDetails.errorDetails != null)
            {
                validResponse = false;
                if (!string.IsNullOrEmpty(response.errorWarningGroup.errorOrWarningCodeDetails.errorDetails.errorCode))
                {
                    errorMessage += "Error Code:" + response.errorWarningGroup.errorOrWarningCodeDetails.errorDetails.errorCode;
                }
                if (!string.IsNullOrEmpty(response.errorWarningGroup.errorOrWarningCodeDetails.errorDetails.errorCategory))
                {
                    errorMessage += ";Error Category:" + response.errorWarningGroup.errorOrWarningCodeDetails.errorDetails.errorCategory;
                }
            }
            if (response != null && response.errorWarningGroup != null && response.errorWarningGroup.errorWarningDescription != null)
            {
                errorMessage += ";Error Description:";

                if (!string.IsNullOrEmpty(response.errorWarningGroup.errorWarningDescription.freeText))
                {
                    errorMessage += response.errorWarningGroup.errorWarningDescription.freeText;
                }
            }
            if (!string.IsNullOrEmpty(errorMessage) && errorMessage.Length > 0)
            {
                validResponse = false;
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)did not receive valid MiniRule_GetFromPricingReply:Reason-" + errorMessage.ToString(), "");
                //throw new Exception("Not a Valid MiniRule_GetFromPricingReply.Reason:" + errorMessage);
            }
            return validResponse;
        }

        public void SignOut()
        {
            try
            {
                hSession.handleSession(SessionHandler.TransactionStatusCode.End);
                hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.FollowUp);

                Security_SignOutRequest signoutRequest = new Security_SignOutRequest();
                signoutRequest.Security_SignOut = new Security_SignOut();
                signoutRequest.AMA_SecurityHostedUser = hSecurity.getHostedUser();
                signoutRequest.Session = hSession.Session;
                signoutRequest.TransactionFlowLink = hLink.Link;

                Session session = hSession.Session;
                TransactionFlowLinkType link = hLink.Link;

                try
                {
                    string filepath = xmlPath + sessionId + "_" + appUserId + "_Security_SignOutRequest_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    XmlSerializer xsr = new XmlSerializer(signoutRequest.GetType());
                    StreamWriter sw = new StreamWriter(filepath);
                    xsr.Serialize(sw, signoutRequest);
                    sw.Close();
                }
                catch { }

                Security_SignOutReply signOutReply = client.Security_SignOut(ref session, ref link, hSecurity.getHostedUser(), signoutRequest.Security_SignOut);
                Security_SignOutResponse signoutResponse = new Security_SignOutResponse(session, link, signOutReply);
                hSession.Session = session;
                hLink.Link = link;
                hSession.handleSession(SessionHandler.TransactionStatusCode.None);
                hLink.handleLinkAction(TransactionFlowLinkHandler.TransactionFlowLinkAction.None);

                try
                {
                    string filepath = xmlPath + sessionId + "_" + appUserId + "_Security_SignOutResponse_" + DateTime.Now.ToString("yyyyMMdd_Hmmss") + ".xml";
                    XmlSerializer xsr = new XmlSerializer(signoutResponse.GetType());
                    StreamWriter sw = new StreamWriter(filepath);
                    xsr.Serialize(sw, signoutResponse);
                    sw.Close();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to sign out. Error : " + ex.ToString(), "");
            }
        }


        public string GetDateQualifierDetail(string code)
        {
            string detail = string.Empty;
            switch (code)
            {
                case "ADV": detail = "Ticket validity date in case of reissue or refund after departure"; break;
                case "ANV": detail = "Ticket validity date in case of reissue or refund with no show after departure"; break;
                case "BDV": detail = "Ticket validity date in case of reissue or refund before departure"; break;
                case "BNV": detail = "Ticket validity date in case of reissue or refund with no show before departure"; break;
                case "ERD": detail = "Advance Reservation/Ticketing - Earliest reservation date – "; break;
                case "ETD": detail = "Advance Reservation/Ticketing - Earliest ticketing date – "; break;
                case "LRD": detail = "Advance Reservation/Ticketing - Latest reservation date – "; break;
                case "LTD": detail = "Advance Reservation/Ticketing - Latest ticketing date before departure – "; break;
                case "LTR": detail = "Advance Reservation/Ticketing - Latest ticketing date after reservation – "; break;
                case "MIS": detail = "Minimum stay date – "; break;
                case "MSC": detail = "Travel must commence before date - "; break;
                case "MSP": detail = "Travel must be completed before date - "; break;
            }
            return detail;
        }

        public string GetMonetoryDetailQualifierDetail(string code)
        {
            string detail = string.Empty;
            switch (code)
            {
                case "ADC": detail = "Maximum penalty amount for the ticket if revalidation in departure"; break;
                case "ADF": detail = "Minimum penalty amount if reissue or refund after departure in fare filing currency"; break;
                case "ADG": detail = "Maximum penalty amount if reissue or refund after departure in fare filing currency"; break;
                case "ADH": detail = "Minimum Penalty amount if revalidation after departure in fare filing currency"; break;
                case "ADI": detail = "Minimum Penalty amount if revalidation after departure in sale currency"; break;
                case "ADL": detail = "Maximum penalty amount if revalidation after departure in fare filing currency"; break;
                case "ADM": detail = "Minimum penalty amount if reissue or refund after departure in sale currency"; break;
                case "ADT": detail = "Maximum penalty amount for the ticket if reissue or refund after departure"; break;
                case "ADU": detail = "Maximum penalty amount if revalidation after departure in sale currency"; break;
                case "ADX": detail = "Maximum penalty amount if reissue or refund after departure in sale currency"; break;
                case "ANC": detail = "Maximum penalty amount for the ticket in case of revalidation after departure no show"; break;
                case "ANF": detail = "Minimum penalty amount in case of reissue or refund with no show after departure in fare filing currency"; break;
                case "ANG": detail = "Maximum penalty amount in case of reissue or refund with no show after departure in fare filing currency"; break;
                case "ANH": detail = "Minimum Penalty amount in case of revalidation after departure no show in fare filing currency"; break;
                case "ANI": detail = "Minimum Penalty amount in case of revalidation after departure no show in sale currency"; break;
                case "ANL": detail = "Maximum penalty amount in case of revalidation after departure no show in fare filing currency"; break;
                case "ANM": detail = "Minimum penalty amount in case of reissue or refund with no show after departure in sale currency"; break;
                case "ANT": detail = "Maximum penalty amount for the ticket in case reissue or refund with of no show after departure"; break;
                case "ANU": detail = "Maximum penalty amount in case of revalidation after departure no show in sale currency"; break;
                case "ANX": detail = "Maximum penalty amount in reissue or refund with no show after departure in sale currency"; break;
                case "BDC": detail = "Maximum penalty amount for the ticket if revalidation after departure"; break;
                case "BDF": detail = "Minimum penalty amount in case of reissue or refund before departure in fare filing currency"; break;
                case "BDG": detail = "Maximum penalty amount in case of reissue or refund before departure in fare filing currency"; break;
                case "BDH": detail = "Minimum Penalty amount if revalidation before departure in fare filing currency"; break;
                case "BDI": detail = "Minimum Penalty amount if revalidation before departure in sale currency"; break;
                case "BDL": detail = "Maximum penalty amount if revalidation before departure in fare filing currency"; break;
                case "BDM": detail = "Minimum penalty amount in case of reissue or refund before departure in sale currency"; break;
                case "BDT": detail = "Maximum penalty amount for the ticket in case of reissue or refund before departure"; break;
                case "BDU": detail = "Maximum penalty amount if revalidation before departure in sale currency"; break;
                case "BDX": detail = "Maximum penalty amount in case of reissue or refund before departure in sale currency"; break;
                case "BNC": detail = "Revalidation maximum penalty amount for the ticket in case of revalidation before departure no show"; break;
                case "BNF": detail = "Minimum penalty amount in case of reissue or refund with no show before departure in fare filing currency"; break;
                case "BNG": detail = "Maximum penalty amount in case of reissue or refund with no show before departure in fare filing currency"; break;
                case "BNH": detail = "Minimum Penalty amount in case of revalidation before departure no show in fare filing currency"; break;
                case "BNI": detail = "Minimum Penalty amount in case of revalidation before departure no show in sale currency"; break;
                case "BNL": detail = "Maximum penalty amount in case of revalidation before departure no show in fare filing currency"; break;
                case "BNM": detail = "Minimum penalty amount in case of reissue or refund with no show before departure in sale currency"; break;
                case "BNT": detail = "Maximum penalty amount for the ticket in case of reissue or refund with no show before departure"; break;
                case "BNU": detail = "Maximum penalty amount in case of revalidation before departure no show in sale currency"; break;
                case "BNX": detail = "Maximum penalty amount in case of reissue or refund with no show before departure in sale currency"; break;
            }
            return detail;
        }

        public string GetRestrictionQualifierDetail(string code)
        {
            string detail = string.Empty;

            switch (code)
            {
                case "ADA": detail = "Reissue or refund after departure allowance flag"; break;
                case "ADR": detail = "Revalidation allowance flag in case of reissue after departure"; break;
                case "ADW": detail = "Penalty waiver allowance flag in case of reissue after departure"; break;
                case "ANA": detail = "Reissue or refund after departure when no show allowance flag"; break;
                case "ANR": detail = "Revalidation allowance flag when Reissue after departure with no show"; break;
                case "ANW": detail = "Penalty waiver allowance flag when Reissue after departure with no show"; break;
                case "BDA": detail = "Reissue or refund before departure allowance flag"; break;
                case "BNA": detail = "Reissue or refund with no show before departure allowance flag"; break;
                case "BNR": detail = "Revalidation allowance flag in case of reissue before departure with no show"; break;
                case "BNW": detail = "Penalty waiver allowance flag when Reissue before departure with no show"; break;
                case "FFT": detail = "Part of the rule is Free form from Cat 16"; break;
                case "RVA": detail = "Revalidation allowance flag in case of reissue before departure"; break;
            }
            return detail;
        }

        public string GetRestrictionActonDetail(string code)
        {
            string detail = string.Empty;

            switch (code)
            {
                case "0": detail = "Not allowed"; break;
                case "1": detail = "Allowed/Allowed with restriction/Present"; break;
                case "A": detail = "Allowed"; break;
                case "BID": detail = "Both Illness and Death can waive penalties"; break;
                case "C": detail = "Travel must commence before"; break;
                case "DEA": detail = "Death can waive penalties"; break;
                case "ILL": detail = "Illness can waive penalties"; break;
                case "N": detail = "Not allowed"; break;
                case "P": detail = "Travel must be completed before"; break;
                case "R": detail = "Restricted"; break;
            }

            return detail;
        }

        public bool ValidateTicketCancelDocumentResponse(Ticket_CancelDocumentReplyTransactionResults[] result)
        {
            bool validResponse = true;
            try
            {
                string errorMessage = string.Empty;
                Ticket_CancelDocumentReplyTransactionResults pnrReply = result[0];

                if (pnrReply != null && pnrReply.responseDetails != null && pnrReply.responseDetails.statusCode == "O")//Recoverable error
                {
                    validResponse = true;
                }
                else
                {
                    validResponse = false;
                }

                if (pnrReply != null && pnrReply.errorGroup != null)
                {
                    ErrorGroupType7 err = pnrReply.errorGroup;
                    if (err.errorOrWarningCodeDetails != null && err.errorOrWarningCodeDetails.errorDetails != null && !string.IsNullOrEmpty(err.errorOrWarningCodeDetails.errorDetails.errorCode))
                    {
                        errorMessage = "Error Code:" + err.errorOrWarningCodeDetails.errorDetails.errorCode;
                    }
                    if (err.errorOrWarningCodeDetails != null && err.errorOrWarningCodeDetails.errorDetails != null && !string.IsNullOrEmpty(err.errorOrWarningCodeDetails.errorDetails.errorCategory))
                    {
                        errorMessage += ";Error Category:" + err.errorOrWarningCodeDetails.errorDetails.errorCategory;
                    }
                    if (err.errorWarningDescription != null && err.errorWarningDescription.freeText.Length > 0)
                    {
                        errorMessage += ";Error Description:";
                        for (int m = 0; m < err.errorWarningDescription.freeText.Length; m++)
                        {
                            if (!string.IsNullOrEmpty(err.errorWarningDescription.freeText[m]))
                            {
                                errorMessage += err.errorWarningDescription.freeText[m];
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(errorMessage) && errorMessage.Length > 0)
                {
                    validResponse = false;
                    Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)did not receive valid TicketCancelDocumentResponse:Reason-" + errorMessage.ToString(), "");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, appUserId, "(Amadeus)Failed to execute ValidateTicketCancelDocumentResponse method. Reason : " + ex.ToString(), "");
            }
            return validResponse;
        }


        #endregion

    }
}
