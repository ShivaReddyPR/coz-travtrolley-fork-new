﻿using System;
using System.ServiceModel;
using CT.BookingEngine.GDS.IBE;
namespace CT.BookingEngine.GDS
{
    [Serializable]
    public abstract class HeaderHandler
    {
        protected ClientBase<AmadeusWebServicesPT> mClient;
        public HeaderHandler()
        { }

        public HeaderHandler(ClientBase<AmadeusWebServicesPT> client)
        {
            mClient = client;
        }
    }

    
}
