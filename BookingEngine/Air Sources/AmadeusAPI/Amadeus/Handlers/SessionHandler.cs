﻿using System;
using System.Diagnostics;
using CT.BookingEngine.GDS.IBE;
namespace CT.BookingEngine.GDS
{
    
    [Serializable]
    public class SessionHandler : HeaderHandler
    {
        #region Attributes
        public Session Session {get; set;}
        private SecurityHandler hSecurity;
        #endregion

        public enum TransactionStatusCode { None, Start, Continue, End };

        #region Constructor
        public SessionHandler()
        {
        }
        public SessionHandler(AmadeusWebServicesPTClient client, SecurityHandler h)
            : base(client)
        {
            this.Session = null;
            hSecurity = h;
        }
        #endregion

        #region Members
        public void handleSession(TransactionStatusCode transactionStatusCode)
        {
            switch (transactionStatusCode)
            {
                case TransactionStatusCode.Start:
                    startSession();
                    break;
                case TransactionStatusCode.Continue:
                    continueSession();
                    break;
                case TransactionStatusCode.End:
                    endSession();
                    break;
                case TransactionStatusCode.None:
                    hSecurity.fill();
                    resetSession();
                    break;
                default:
                    break;
            }
        }

        public void AfterReply(TransactionStatusCode requestedStatusCode)
        {
            switch (requestedStatusCode)
            {
                case TransactionStatusCode.Start:
                    Trace.Assert(Session.TransactionStatusCode.Equals("InSeries"));
                    break;
                case TransactionStatusCode.Continue:
                    Trace.Assert(Session.TransactionStatusCode.Equals("InSeries"));
                    break;
                case TransactionStatusCode.End:
                    Trace.Assert(Session.TransactionStatusCode.Equals("End"));
                    break;
                case TransactionStatusCode.None:
                    Trace.Assert(Session.TransactionStatusCode.Equals("End"));
                    break;
                default:
                    break;
            }
        }

        private void startSession()
        {
            hSecurity.fill();
            this.Session = new Session();
            this.Session.TransactionStatusCode = "Start";
        }

        private void continueSession()
        {
            hSecurity.reset();
            int sequenceNumber = int.Parse(this.Session.SequenceNumber) + 1;
            this.Session.SequenceNumber = sequenceNumber.ToString();
        }

        private void endSession()
        {
            continueSession();
            this.Session.TransactionStatusCode = "End";
        }

        private void resetSession()
        {
            this.Session = null;
        }
        #endregion
    }
}
