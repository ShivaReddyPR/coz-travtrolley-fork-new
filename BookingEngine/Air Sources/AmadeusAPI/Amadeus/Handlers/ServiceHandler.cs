﻿using System;
using CT.BookingEngine.GDS.IBE;


namespace CT.BookingEngine.GDS
{
    [Serializable]
    class ServiceHandler
    {
        #region Attributes
        public AmadeusWebServicesPTClient client;
        public SessionHandler hSession;
        public TransactionFlowLinkHandler hLink;
        public SecurityHandler hSecurity;
        public AddressingHandler hAddressing;
        #endregion

        #region Constructor
        public ServiceHandler(String wsap, String endpointConfigurationName)
        {
            client = new AmadeusWebServicesPTClient(endpointConfigurationName);
            hSecurity = new SecurityHandler(client, "", "");
            hSession = new SessionHandler(client, hSecurity);
            hLink = new TransactionFlowLinkHandler(client);
            hAddressing = new AddressingHandler(client, wsap);
        }
        #endregion

        #region Members




        public Security_SignOutReply security_SignOut(SessionHandler.TransactionStatusCode transactionStatusCode, TransactionFlowLinkHandler.TransactionFlowLinkAction linkAction)
        {
            BeforeRequest(transactionStatusCode, linkAction);
            Session session = hSession.Session;
            TransactionFlowLinkType link = hLink.Link;
            Security_SignOutReply reply = client.Security_SignOut(ref session, ref link, hSecurity.getHostedUser(), MessageFactory.buildSignOutRequest());
            hSession.Session = session;
            hLink.Link = link;
            return reply;
        }

        private void BeforeRequest(SessionHandler.TransactionStatusCode transactionStatusCode, TransactionFlowLinkHandler.TransactionFlowLinkAction linkAction)
        {
            hSession.handleSession(transactionStatusCode);
            hLink.handleLinkAction(linkAction);
            hAddressing.update();
        }

        private void AfterReply(SessionHandler.TransactionStatusCode transactionStatusCode, TransactionFlowLinkHandler.TransactionFlowLinkAction linkAction)
        {
            hSession.AfterReply(transactionStatusCode);
            hLink.AfterReply(linkAction);
        }

        #endregion
    }
}
