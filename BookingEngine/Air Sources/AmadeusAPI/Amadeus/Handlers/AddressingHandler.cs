﻿using System;
using System.ServiceModel;
using CT.BookingEngine.GDS.IBE;

namespace CT.BookingEngine.GDS
{
    [Serializable]
    class AddressingHandler : HeaderHandler
    {
        #region Attributes
        private String mWSAP;
        #endregion

        #region Constructor
        public AddressingHandler()
        {
        }
        public AddressingHandler(AmadeusWebServicesPTClient client, String wsap)
            : base(client)
        {
            this.mWSAP = wsap;
        }
        #endregion

        #region Members
        public void setWSAP(String wsap)
        {
            mWSAP = wsap;
        }

        public String getWSAP()
        {
            return mWSAP;
        }

        public void update()
        {
            setWSAP();
        }

        private void setWSAP()
        {
            Uri uri = mClient.Endpoint.Address.Uri;
            String newUri = uri.Scheme + "://" + uri.Host + "/" + this.mWSAP;
            EndpointAddress address = new EndpointAddress(newUri);
            mClient.Endpoint.Address = address;
        }
        #endregion
    }
}
