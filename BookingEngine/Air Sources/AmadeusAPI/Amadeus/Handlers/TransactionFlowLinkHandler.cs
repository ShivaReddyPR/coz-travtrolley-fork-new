﻿using System;
using System.Diagnostics;
using CT.BookingEngine.GDS.IBE;

namespace CT.BookingEngine.GDS
{

    [Serializable]
    public class TransactionFlowLinkHandler : HeaderHandler
    {
        #region Attributes
        public TransactionFlowLinkType Link { get; set; }
        private String UniqueID { get; set; }
        private String ServerID { get; set; }

        #endregion

        public enum TransactionFlowLinkAction { None, New, FollowUp, Reset };

        #region Constructor
        public TransactionFlowLinkHandler()
        {
        }
        public TransactionFlowLinkHandler(AmadeusWebServicesPTClient client)
            : base(client)
        {
            this.Link = null;
        }
        #endregion

        #region Members
        public void handleLinkAction(TransactionFlowLinkAction linkAction)
        {
            switch (linkAction)
            {
                case TransactionFlowLinkAction.New:
                    newLink();
                    break;
                case TransactionFlowLinkAction.FollowUp:
                    followUpLink();
                    break;
                case TransactionFlowLinkAction.Reset:
                    resetLink();
                    break;
                case TransactionFlowLinkAction.None:
                    noLink();
                    break;
                default:
                    break;
            }

        }

        public void AfterReply(TransactionFlowLinkAction requestedAction)
        {
            switch (requestedAction)
            {
                case TransactionFlowLinkAction.New:
                    if (Link.Receiver != null && Link.Consumer != null)
                    {
                        Trace.Assert(Link.Receiver != null);
                        if (this != null && !string.IsNullOrEmpty(this.UniqueID) && !string.IsNullOrEmpty(Link.Consumer.UniqueID) && Link.Consumer.UniqueID.Equals(this.UniqueID))
                        {
                            Trace.Assert(Link.Consumer.UniqueID.Equals(this.UniqueID));
                        }
                        if (!string.IsNullOrEmpty(Link.Receiver.ServerID))
                        {
                            this.ServerID = Link.Receiver.ServerID;
                        }
                    }
                    break;
                case TransactionFlowLinkAction.FollowUp:
                    if (Link.Receiver != null && Link.Consumer != null)
                    {
                        if (this != null && !string.IsNullOrEmpty(this.ServerID) && !string.IsNullOrEmpty(Link.Receiver.ServerID) && Link.Receiver.ServerID.Equals(this.ServerID))
                        {
                            Trace.Assert(Link.Receiver.ServerID.Equals(this.ServerID));
                        }
                        if (this != null && !string.IsNullOrEmpty(this.UniqueID) && !string.IsNullOrEmpty(Link.Consumer.UniqueID) && Link.Consumer.UniqueID.Equals(this.UniqueID))
                        {
                            Trace.Assert(Link.Consumer.UniqueID.Equals(this.UniqueID));
                        }
                    }
                    break;
                case TransactionFlowLinkAction.Reset:
                    if (Link.Receiver != null && Link.Consumer != null)
                    {
                        Trace.Assert(Link.Receiver != null);
                        if (this != null && !string.IsNullOrEmpty(this.ServerID) && !string.IsNullOrEmpty(Link.Receiver.ServerID) && !Link.Receiver.ServerID.Equals(this.ServerID))
                        {
                            Trace.Assert(!Link.Receiver.ServerID.Equals(this.ServerID));
                        }
                        if (this != null && !string.IsNullOrEmpty(this.UniqueID) && !string.IsNullOrEmpty(Link.Consumer.UniqueID) && Link.Consumer.UniqueID.Equals(this.UniqueID))
                        {
                            Trace.Assert(Link.Consumer.UniqueID.Equals(this.UniqueID));
                        }
                        if (!string.IsNullOrEmpty(Link.Receiver.ServerID))
                        {
                            this.ServerID = Link.Receiver.ServerID;
                        }
                    }
                    break;
                case TransactionFlowLinkAction.None:
                    Trace.Assert(Link == null);
                    break;
                default:
                    break;
            }
        }

        private void newLink()
        {

            this.Link = new TransactionFlowLinkType();
            this.Link.Consumer = new ConsumerType();
            this.UniqueID = System.Guid.NewGuid().ToString();
            this.Link.Consumer.UniqueID = UniqueID;
        }

        private void followUpLink()
        {

        }

        private void resetLink()
        {

            this.Link.Receiver = null;
        }

        private void noLink()
        {

            this.Link = null;
        }
        #endregion
    }
}
