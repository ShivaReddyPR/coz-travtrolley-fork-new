﻿using System;
using CT.BookingEngine.GDS.IBE;
namespace CT.BookingEngine.GDS
{
    
    [Serializable]
    public class SecurityHandler : HeaderHandler
    {
        #region Attributes
        private AMA_SecurityHostedUser mHostedUser;
        private SecurityTokenInspector iSecurity;
        //private static readonly ILog log = LogManager.GetLogger(typeof(SecurityHandler));
        public string pcc, agentyDutyCode, posType, requestType;
        #endregion

        #region Constructor
        public SecurityHandler()
        {
        }
        public SecurityHandler(AmadeusWebServicesPTClient client, string username, string password)
            : base(client)
        {

            this.iSecurity = new SecurityTokenInspector(username, password);
            SecurityBehavior behavior = new SecurityBehavior();
            behavior.inspector = this.iSecurity;
            
            client.Endpoint.Behaviors.Insert(0, behavior);
        }
        #endregion

        #region Members
        public void fill()
        {
            addWSSecurity();
            setHostedUser();
        }

        public void reset()
        {
            mHostedUser = null;
            removeWSSecurity();
        }

        private void removeWSSecurity()
        {
            iSecurity.Activated = false;
        }

        private void addWSSecurity()
        {
            iSecurity.Activated = true;
        }

        public void setHostedUser()
        {
            mHostedUser = new AMA_SecurityHostedUser();
            mHostedUser.UserID = new AMA_SecurityHostedUserUserID();
            mHostedUser.UserID.AgentDutyCode = agentyDutyCode;
            mHostedUser.UserID.POS_Type = posType;
            mHostedUser.UserID.PseudoCityCode = pcc;
            mHostedUser.UserID.RequestorType = requestType;
            mHostedUser.UserID.RequestorID = new UniqueID_Type();
            mHostedUser.UserID.RequestorID.CompanyName = new CompanyNameType();
            mHostedUser.UserID.RequestorID.CompanyName.Value = "1A";
        }

        public AMA_SecurityHostedUser getHostedUser()
        {
            return mHostedUser;
        }
        #endregion
    }
}
