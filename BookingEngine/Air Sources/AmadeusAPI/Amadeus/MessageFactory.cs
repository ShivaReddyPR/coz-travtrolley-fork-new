﻿using CT.BookingEngine.GDS.IBE;

namespace CT.BookingEngine.GDS
{
    class MessageFactory
    {
        public static Security_SignOut buildSignOutRequest() {
            return new Security_SignOut();
        } 
    }
}
