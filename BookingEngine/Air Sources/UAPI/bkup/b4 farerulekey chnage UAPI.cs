﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Xml;
using System.Configuration;
using System.Net;
using System.Text.RegularExpressions;
using CT.BookingEngine;
using CT.BookingEngine.GDS;
using CT.Core;
using Air20;

namespace CT.BookingEngine.GDS
{

    public class UAPI
    {
        #region Credential
        static string userName = string.Empty;
        static string password = string.Empty;
        static string urlAir = string.Empty;
        static string urlUR = string.Empty;
        static string targetBranch = string.Empty;
        static string originalApplication = "UAPI";
        # endregion

        # region Constructor

        public UAPI()
        {

            Connection();
        }
        
        public static void Connection()
        {
            # region LiVe HC Credentials
            //userName = "Universal API/uAPI7280978342-0fbaffdc";
            //password = "6Hz}$9KeJi";
            //urlAir = "https://emea.universal-api.travelport.com/B2BGateway/connect/uAPI/AirService";
            //urlUR = "https://emea.universal-api.travelport.com/B2BGateway/connect/uAPI/UniversalRecordService";
            //targetBranch = "P1544552";
            //Audit.Add(EventType.Search, Severity.Normal, 0, " Live userName:" + userName, string.Empty);
            # endregion
            # region Live 
            userName = CT.Configuration.ConfigurationSystem.UAPIConfig["Userid"];
            password = CT.Configuration.ConfigurationSystem.UAPIConfig["Password"];
            urlAir = CT.Configuration.ConfigurationSystem.UAPIConfig["Url"] + "AirService";
            urlUR = CT.Configuration.ConfigurationSystem.UAPIConfig["Url"] + "UniversalRecordService";
            targetBranch = CT.Configuration.ConfigurationSystem.UAPIConfig["InternationalHAP"];
            # endregion
            # region Test Credentials
            //userName = "Universal API/uAPI7997403705-43d54fd1";
            //password = "x-5Y6P}c!4";
            //urlAir = "https://emea.copy-webservices.travelport.com/B2BGateway/connect/uAPI/AirService";
            //urlUR = "https://emea.copy-webservices.travelport.com/B2BGateway/connect/uAPI/UniversalRecordService";
            //targetBranch = "P7005785";
            # endregion

        }
        # endregion

        # region Search
        public static SearchResult[] Search(SearchRequest request, string sessionId)
        {
            
            Trace.TraceInformation("UAPI.Search() entered.");
            List<SearchResult> finalResult = new List<SearchResult>();
            SearchResult[] result = new SearchResult[0];
            SearchResult[] icSearchResult = new SearchResult[0];
            bool isDomestic = true;
            List<int> flightNotFound = new List<int>();
            //Dictionary<int, List<FlightInfo>> outboundflightList = new Dictionary<int, List<FlightInfo>>();
            //Dictionary<int, List<FlightInfo>> inboundflightList = new Dictionary<int, List<FlightInfo>>();

            
            isDomestic = false;
            //Connection connection = new Connection(isDomestic);
            result = LowFareSearch(request, sessionId );
            finalResult.AddRange(result);

            Trace.TraceInformation("UAPI.Search() exiting.");
            Basket.FlightBookingSession[sessionId].Log.Add("UAPI Search complete. Result count = " + finalResult.Count.ToString());
            return finalResult.ToArray();
        }
        private static SearchResult[] LowFareSearch(SearchRequest request, string sessionId)
        {
            //Dictionary<int, List<FlightInfo>> outboundflightList = new Dictionary<int, List<FlightInfo>>();
            //Dictionary<int, List<FlightInfo>> inboundflightList = new Dictionary<int, List<FlightInfo>>();
            //List<FlightInfo> outbound = new List<FlightInfo>();
            //List<FlightInfo> inbound = new List<FlightInfo>();

            Dictionary<int, FlightInfo> outbound = new Dictionary<int, FlightInfo>();
            Dictionary<int, FlightInfo> inbound = new Dictionary<int, FlightInfo>();

            SearchResult[] result = new SearchResult[0];
            //NetworkCredential nCredential=new NetworkCredential(this
            LowFareSearchRsp lfResponse = GenerateSearch(request);

            
            //Basket.FlightBookingSession[sessionId].XmlMessage.Add(searchXml);


            if (request != null)
            {
                ReadFlightInfoResponse(lfResponse, ref outbound, ref inbound);
                if (outbound.Count > 0)
                {
                    result = ReadFareResponse(lfResponse, request, outbound, inbound);
                }

            }
            //if (searchXml.Length > 0)
            //{
            //    string responseXml = connection.MultiSubmitXml(searchXml);
            //    //Basket.FlightBookingSession[sessionId].XmlMessage.Add(responseXml);
            //    Audit.Add(EventType.Search, Severity.Normal, 0, "Search XML 1G. req : " + searchXml + " resp : " + responseXml, string.Empty);
            //    ReadFlightInfoResponse(responseXml, request, ref outboundflightList, ref inboundflightList);
            //    if (outboundflightList.Count > 0)
            //    {
            //        result = ReadFareResponse(responseXml, request, outboundflightList, inboundflightList);
            //    }
            //}
            return result;
        }
        private static LowFareSearchRsp GenerateSearch(SearchRequest request)
        {
            
            Trace.TraceInformation("UAPI.GenerateSearchMessage entered");
            bool isDomestic = true;
            if ((request.AdultCount + request.ChildCount + request.SeniorCount + request.InfantCount) > 9)
            {
                throw new BookingEngineException("Total number of passengers should not exceed 9");
            }
            if (request.InfantCount > (request.AdultCount + request.SeniorCount))
            {
                throw new BookingEngineException("Number of infants should not exceed the number of Adult and Senior");
            }
            int paxCount = request.AdultCount + request.ChildCount + request.InfantCount+request.SeniorCount;

            Connection();
            LowFareSearchReq lfRequest = new LowFareSearchReq();
            lfRequest.TargetBranch = targetBranch;

            BillingPointOfSaleInfo pos = new BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            lfRequest.BillingPointOfSaleInfo = pos;

            AirSearchModifiers airModifiers = new AirSearchModifiers();
            lfRequest.AirSearchModifiers = airModifiers;
            SearchPassenger[] passAdult = new SearchPassenger[paxCount+1];
            int paxIncrement = 0;
            for (int i = 0; i < request.AdultCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "ADT";
                paxIncrement++;

            }
            for (int i = 0; i < request.ChildCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "CNN";
                passAdult[paxIncrement].Age = "5";
                paxIncrement++;

            }
            for (int i = 0; i < request.SeniorCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "SRC";
                passAdult[paxIncrement].Age = "65";
                paxIncrement++;

            }
            for (int i = 0; i < request.InfantCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "INF";
                paxIncrement++;

            }
            lfRequest.SearchPassenger = passAdult;
            //lfRequest.SearchPassenger = passAdult;

            //lfRequest.SearchAirLeg = new SearchAirLeg[2];
            //lfRequest.SearchAirLeg[0] = new SearchAirLeg();
            //SearchAirLeg leg = lfRequest.SearchAirLeg[0];

            //Air20.Airport origin = new Air20.Airport();
            //origin.Code = request.Segments[0].Origin;

            //Air20.Airport destination = new Air20.Airport();
            //destination.Code = request.Segments[0].Destination;

            //leg.SearchOrigin = new typeSearchLocation[1];
            //leg.SearchOrigin[0] = new typeSearchLocation();
            //leg.SearchOrigin[0].Item= origin;

            //leg.SearchDestination = new typeSearchLocation[1];
            //leg.SearchDestination[0] = new typeSearchLocation();
            //leg.SearchDestination[0].Item= destination;

            //leg.Items = new typeFlexibleTimeSpec[1];
            //leg.Items[0] = new typeFlexibleTimeSpec();
            //leg.Items[0].PreferredTime = request.Segments[0].PreferredDepartureTime.ToString("yyyy-MM-dd");


            // PReferred Cabin
            typeCabinClass cabinType = typeCabinClass.Economy;
            switch (request.Segments[0].flightCabinClass)
            {
                case CabinClass.Business:
                    cabinType = typeCabinClass.Business;
                    break;
                case CabinClass.Economy:
                    cabinType = typeCabinClass.Economy;
                    break;
                case CabinClass.First:
                    cabinType = typeCabinClass.First;
                    break;
                
                    
            }
            if (request.Type == SearchType.MultiWay)
            {
                lfRequest.SearchAirLeg = new SearchAirLeg[request.Segments.Length];
                int segIndex = 0;
                foreach (FlightSegment segment in request.Segments)
                {
                    lfRequest.SearchAirLeg[segIndex] = new SearchAirLeg();
                    SearchAirLeg leg = lfRequest.SearchAirLeg[segIndex];
                    // origin
                    Air20.Airport origin = new Air20.Airport();
                    origin.Code = request.Segments[segIndex].Origin;

                    leg.SearchOrigin = new typeSearchLocation[1];
                    leg.SearchOrigin[0] = new typeSearchLocation();
                    leg.SearchOrigin[0].Item = origin;
                    // destination
                    Air20.Airport destination = new Air20.Airport();
                    destination.Code = request.Segments[segIndex].Destination;

                    leg.SearchDestination = new typeSearchLocation[1];
                    leg.SearchDestination[0] = new typeSearchLocation();
                    leg.SearchDestination[0].Item = destination;


                    leg.Items = new typeFlexibleTimeSpec[1];
                    leg.Items[0] = new typeFlexibleTimeSpec();
                    leg.Items[0].PreferredTime = request.Segments[segIndex].PreferredDepartureTime.ToString("yyyy-MM-dd");
                    AirLegModifiers legModifiers = new AirLegModifiers();
                    if (request.Segments[0].flightCabinClass != CabinClass.All)
                    {
                        AirLegModifiersPreferredCabins prefCabin = new AirLegModifiersPreferredCabins();
                        Air20.CabinClass airCabin = new Air20.CabinClass();
                        airCabin.Type = cabinType;
                        prefCabin.CabinClass = airCabin;
                        legModifiers.PreferredCabins = new AirLegModifiersPreferredCabins();
                        legModifiers.PreferredCabins = prefCabin;
                    }

                    if (request.RestrictAirline)
                    {

                        if (request.Segments[segIndex].PreferredAirlines != null && request.Segments[segIndex].PreferredAirlines.Length > 0)
                        {
                            // For Preffered Airlines
                            Carrier[] tmpCarrierList = new Carrier[1];
                            Carrier tmpCarrier = new Carrier();
                            tmpCarrier.Code = request.Segments[segIndex].PreferredAirlines[segIndex].ToString();
                            tmpCarrierList[0] = new Carrier();
                            tmpCarrierList[0] = tmpCarrier;
                            legModifiers.PreferredCarriers = tmpCarrierList;
                        }
                    }




                    // Preferred Cabin for Outbound
                    leg.AirLegModifiers = legModifiers;

                    segIndex++;
                }
            }
            else  // one way and round trip
            {
               

                lfRequest.SearchAirLeg = new SearchAirLeg[2];
                lfRequest.SearchAirLeg[0] = new SearchAirLeg();
                SearchAirLeg leg = lfRequest.SearchAirLeg[0];

                Air20.Airport origin = new Air20.Airport();
                origin.Code = request.Segments[0].Origin;

                Air20.Airport destination = new Air20.Airport();
                destination.Code = request.Segments[0].Destination;

                leg.SearchOrigin = new typeSearchLocation[1];
                leg.SearchOrigin[0] = new typeSearchLocation();
                leg.SearchOrigin[0].Item = origin;

                leg.SearchDestination = new typeSearchLocation[1];
                leg.SearchDestination[0] = new typeSearchLocation();
                leg.SearchDestination[0].Item = destination;

                leg.Items = new typeFlexibleTimeSpec[1];
                leg.Items[0] = new typeFlexibleTimeSpec();
                leg.Items[0].PreferredTime = request.Segments[0].PreferredDepartureTime.ToString("yyyy-MM-dd");
                AirLegModifiers legModifiers = new AirLegModifiers();
                if (request.Segments[0].flightCabinClass != CabinClass.All)
                {
                    AirLegModifiersPreferredCabins prefCabin = new AirLegModifiersPreferredCabins();
                    Air20.CabinClass airCabin = new Air20.CabinClass();
                    airCabin.Type = cabinType;
                    prefCabin.CabinClass = airCabin;
                    legModifiers.PreferredCabins = new AirLegModifiersPreferredCabins();
                    legModifiers.PreferredCabins = prefCabin;
                }
                if (request.RestrictAirline)
                {

                    if (request.Segments[0].PreferredAirlines != null && request.Segments[0].PreferredAirlines.Length > 0)
                    {
                        // For Preffered Airlines
                        Carrier[] tmpCarrierList = new Carrier[1];
                        Carrier tmpCarrier = new Carrier();
                        tmpCarrier.Code = request.Segments[0].PreferredAirlines[0].ToString();
                        tmpCarrierList[0] = new Carrier();
                        tmpCarrierList[0] = tmpCarrier;
                        legModifiers.PreferredCarriers = tmpCarrierList;
                    }
                }




                // Preferred Cabin for Outbound
                leg.AirLegModifiers = legModifiers;
                //lfRequest.SearchAirLeg[0].AirLegModifiers = legModifiers;


                // Return Leg
                if (request.Type == SearchType.Return)
                {
                    lfRequest.SearchAirLeg[1] = new SearchAirLeg();
                    SearchAirLeg legReturn = lfRequest.SearchAirLeg[1];

                    Air20.Airport retOrigin = new Air20.Airport();
                    retOrigin.Code = request.Segments[0].Destination;

                    Air20.Airport retDestination = new Air20.Airport();
                    retDestination.Code = request.Segments[0].Origin;

                    legReturn.SearchOrigin = new typeSearchLocation[1];
                    legReturn.SearchOrigin[0] = new typeSearchLocation();
                    legReturn.SearchOrigin[0].Item = retOrigin;

                    legReturn.SearchDestination = new typeSearchLocation[1];
                    legReturn.SearchDestination[0] = new typeSearchLocation();
                    legReturn.SearchDestination[0].Item = retDestination;


                    legReturn.Items = new typeFlexibleTimeSpec[1];
                    legReturn.Items[0] = new typeFlexibleTimeSpec();
                    legReturn.Items[0].PreferredTime = request.Segments[1].PreferredDepartureTime.ToString("yyyy-MM-dd");


                    // Preferred Cabin for Inbound
                    legReturn.AirLegModifiers = legModifiers;

                }
            } 

           //// For No Of Stops
           if (!string.IsNullOrEmpty(request.MaxStops))
           {
               airModifiers.MaxStops = request.MaxStops;
           }
           // Fare Type (Refundable /Non Refundable) AirPricingModifiers 

           if (request.RefundableFares)
           {
               AirPricingModifiers priceModifier = new AirPricingModifiers();
               priceModifier.ProhibitNonRefundableFares = request.RefundableFares;// All Refundable will be shown
               lfRequest.AirPricingModifiers = priceModifier;
               //AirLowFareSearchAsynchBinding binding = new AirLowFareSearchAsynchBinding();
               
           }

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(lfRequest.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, lfRequest); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\LFSearchReq.xml");
            }
            AirLowFareSearchBinding binding = new AirLowFareSearchBinding();
            binding.Url = urlAir;
            binding.Credentials = new NetworkCredential(userName, password);

            LowFareSearchRsp lfResponse = null;
        
            lfResponse = binding.service(lfRequest);
            return lfResponse;
        }
        //private static void ReadFlightInfoResponse(LowFareSearchRsp lfResponse, ref List<FlightInfo> outboundflightList, ref List<FlightInfo> inboundflightList)
        private static void ReadFlightInfoResponse(LowFareSearchRsp lfResponse, ref Dictionary<int, FlightInfo> outboundflightList, ref Dictionary<int, FlightInfo> inboundflightList)
        {
            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(lfResponse.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, lfResponse); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\LFSearchRes.xml");

            }
            //List<FlightInfo> outBound = new List<FlightInfo>();
            //List<FlightInfo> inBound = new List<FlightInfo>();
            outboundflightList = new Dictionary<int, FlightInfo>() ;
            inboundflightList = new Dictionary<int, FlightInfo>() ;
            bool isError = false;
            //ResponseMessage[] lfMessage=lfResponse.ResponseMessage;
            //for (int x = 0; x < lfMessage.Length; x++)
            //{
            //    if (lfMessage[x].Type == ResponseMessageType.Error)
            //    {isError = true;break;}
            //}

            FlightDetails[] lfFlightDetails=lfResponse.FlightDetailsList;
            AirSegment[] lfAirSegment=lfResponse.AirSegmentList;

            if(!isError)
            {
                int segGroup=0;
                for (int x = 0; x < lfAirSegment.Length; x++)
                {
                    

                    segGroup=lfAirSegment[x].Group;
                    FlightInfo flight = new FlightInfo();
                    int segmentKey =keyGen(lfAirSegment[x].Key);
                    int flightKey =0;
                    flight.UapiSegmentRefKey= segmentKey;
                    

                    FlightDetails lfFlight = null;
                    
                    for (int i = 0; i < lfFlightDetails.Length; i++)
                    {
                        int tempflightKey = keyGen(lfFlightDetails[i].Key);
                        //FlightDetails[] lfFlightLit = lfFlightDetails[0].Key;
                        //for (int f = 0; f < lfFlightDetails; f++)
                        //{

                        //}
                        if (tempflightKey == keyGen(lfAirSegment[x].FlightDetailsRef[0].Key))
                        {
                          //  lfFlight=lfAirSegment[x].
                            flightKey = keyGen(lfAirSegment[x].FlightDetailsRef[0].Key);
                            lfFlight = lfFlightDetails[i];
                            break;

                        }
                        //FlightDetails lfFlight=lfFlightDetails[i].ke
                    }

                    flight.FlightId = flightKey; 

                    flight.Origin= new Airport(lfAirSegment[x].Origin);
                    flight.Destination= new Airport(lfAirSegment[x].Destination);
                    flight.Airline= lfAirSegment[x].Carrier;
                    flight.FlightNumber= lfAirSegment[x].FlightNumber;

                    //Local Time
                    string GMTdiff = lfAirSegment[x].DepartureTime.IndexOf("+") != -1 ? "+" : "-";
                    string depTime = lfAirSegment[x].DepartureTime.Substring(0, lfAirSegment[x].DepartureTime.LastIndexOf(GMTdiff));
                    GMTdiff = lfAirSegment[x].ArrivalTime.IndexOf("+") != -1 ? "+" : "-";
                    string arrTime = lfAirSegment[x].ArrivalTime.Substring(0, lfAirSegment[x].ArrivalTime.LastIndexOf(GMTdiff));


                    flight.DepartureTime = Convert.ToDateTime(depTime);
                    flight.ArrivalTime = Convert.ToDateTime(arrTime);

                    //GMt Time
                    //flight.DepartureTime = Convert.ToDateTime(lfAirSegment[x].DepartureTime);
                    //flight.ArrivalTime= Convert.ToDateTime(lfAirSegment[x].ArrivalTime);
                    // For UAPI REservation Request
                    flight.UapiDepartureTime= lfAirSegment[x].DepartureTime;
                    flight.UapiArrivalTime = lfAirSegment[x].ArrivalTime;


                    flight.Craft= lfAirSegment[x].Equipment;
                    int duration = Convert.ToInt32(lfAirSegment[x].FlightTime);
                    flight.Duration = new TimeSpan(0, duration, 0);
                    flight.DepTerminal= lfFlight.OriginTerminal;
                    flight.ArrTerminal = lfFlight.DestinationTerminal;
                    flight.ETicketEligible = lfAirSegment[x].ETicketabilitySpecified;
                    flight.Group = segGroup;
                    
                    if (segGroup == 0)
                    {
                        outboundflightList.Add(segmentKey,flight);
                    }
                    else
                    {
                        inboundflightList.Add(segmentKey,flight);
                    }

                }


            }

        }
        
        private static SearchResult[] ReadFareResponse(LowFareSearchRsp lfResponse, SearchRequest request, Dictionary<int, FlightInfo> outboundflightList, Dictionary<int, FlightInfo> inboundflightList)
        {

            List<SearchResult> resultarray = new List<SearchResult>();
            List<string> resultKey = new List<string>();
            //List<FlightInfo> outbound = outboundflightList;
            //List<FlightInfo> inbound = inboundflightList;

            Dictionary<int, FlightInfo> outbound = outboundflightList;
            Dictionary<int, FlightInfo> inbound = inboundflightList;

            Dictionary<int, Air20.FareInfo> fareBasisCodeRef = new Dictionary<int, Air20.FareInfo>();
            //Dictionary<int, Air20.FareInfo> fareInfoDic = new Dictionary<int, Air20.FareInfo>();
            Air20.FareInfo[] fareInfoList=lfResponse.FareInfoList;
            for (int f = 0; fareInfoList.Length > f; f++)
            {
                Air20.FareInfo fareInfo = fareInfoList[f];
                int fareInfoKey = keyGen(fareInfo.Key);
                fareBasisCodeRef.Add(fareInfoKey, fareInfo);
            }

          
            string currency = lfResponse.CurrencyType;
            Air20.AirPricingSolution[] pricingSolutionList = lfResponse.AirPricingSolution;
            Fare[] fare = new Fare[0];
            foreach (AirPricingSolution pricingSolution in pricingSolutionList) // AirPricingSolution
            {
                //AirPricingSolution RRpricingSolution = new AirPricingSolution();// TO Pass with Air Create Reservation Request
                //RRpricingSolution.airse

                bool nonRefundable = false;
                double totalResultFare = 0;
                double totalbaseFare = 0;
                string lastTktDate = string.Empty;
                //pricingSolution.Journey[0].
                int fareKey = 0;
                double baseFare = 0;
                double totalFare = 0;
                fare = new Fare[pricingSolution.AirPricingInfo.Length];

                Air20.BookingInfo[] bookingInfoList = pricingSolution.AirPricingInfo[0].BookingInfo;
                int infoIndex = 0;
                foreach (Air20.AirPricingInfo pricingInfo in pricingSolution.AirPricingInfo) //AirPricingInfo
                {
                    baseFare = getCurrAmount(pricingInfo.ApproximateBasePrice);
                    totalFare = getCurrAmount(pricingInfo.TotalPrice);
                    lastTktDate = pricingInfo.LatestTicketingTime;
                    nonRefundable = !pricingInfo.Refundable;
                    pricingInfo.FareInfo = new Air20.FareInfo[pricingInfo.FareInfoRef.Length];
                    int indexFareInfo = 0;
                    foreach (Air20.FareInfoRef tempFareInfo in pricingInfo.FareInfoRef)
                    {
                        pricingInfo.FareInfo[indexFareInfo] = fareBasisCodeRef[keyGen(tempFareInfo.Key)];
                        indexFareInfo++;

                    }



                    fare[infoIndex] = new Fare();

                    if (pricingInfo.PassengerType[0].Code == "ADT") // Adult
                    {
                        fare[infoIndex].PassengerType= PassengerType.Adult;
                        fare[infoIndex].PassengerCount = request.AdultCount;
                        fare[infoIndex].BaseFare = baseFare * request.AdultCount;
                        fare[infoIndex].TotalFare = totalFare * request.AdultCount;
                    }
                    else if (pricingInfo.PassengerType[0].Code == "CNN")// Children
                    {
                        fare[infoIndex].PassengerType = PassengerType.Child;
                        fare[infoIndex].PassengerCount = request.ChildCount;
                        fare[infoIndex].BaseFare = baseFare * request.ChildCount;
                        fare[infoIndex].TotalFare = totalFare * request.ChildCount;
                    }
                    else if (pricingInfo.PassengerType[0].Code == "INF")// Infant
                    {
                        fare[infoIndex].PassengerType = PassengerType.Infant;
                        fare[infoIndex].PassengerCount = request.InfantCount;
                        fare[infoIndex].BaseFare = baseFare * request.InfantCount;
                        fare[infoIndex].TotalFare = totalFare * request.InfantCount;
                    }
                    else if (pricingInfo.PassengerType[0].Code == "SRC")// Senior citizen
                    {
                        fare[infoIndex].PassengerType = PassengerType.Senior;
                        fare[infoIndex].PassengerCount = request.SeniorCount;
                        fare[infoIndex].BaseFare = baseFare * request.SeniorCount;
                        fare[infoIndex].TotalFare = totalFare * request.SeniorCount;
                    }
                    totalbaseFare += baseFare * fare[infoIndex].PassengerCount;
                    totalResultFare += totalFare * fare[infoIndex].PassengerCount;
                    infoIndex++;
                }// Air Pricing Info End



                // Adding segments in single result
                List<FlightInfo[]> outFlights = new List<FlightInfo[]>();
                List<FlightInfo[]> inFlights = new List<FlightInfo[]>();

                // Adding Connection Details
                Air20.Connection[] connectionList = pricingSolution.Connection;
                //Adding Jopurney Details
                Air20.Journey[] journeyList = pricingSolution.Journey;
                int jIndex = 0;
                int segmentIndex = 0;
                foreach(Air20.Journey tempJourney in journeyList) // Journey Array
                {
                    FlightInfo[] flights = new FlightInfo[0];
                    Air20.AirSegmentRef[] jSegRefList=tempJourney.AirSegmentRef;
                    int segRefIndex = 0;
                    foreach (Air20.AirSegmentRef jSegRef in jSegRefList)
                    {
                        if (segRefIndex == 0)
                        {
                            flights = new FlightInfo[jSegRefList.Length];
                        }

                        int jSegRefKey = keyGen(jSegRef.Key);
                        if (jIndex==0)// outbound
                        {
                            //outbound[jSegRefKey].Group = "0";
                            flights[segRefIndex] = outbound[jSegRefKey];
                            
                        }
                        else // return
                        {
                            //outbound[jSegRefKey].Group = "1";
                            flights[segRefIndex] = inbound[jSegRefKey];
                            
                        }
                        string bookingClass = string.Empty;
                        string cabinClass = string.Empty;
                        int fareInfoKey = 0;

                       // pricingSolution.AirSegment = new AirSegment[bookingInfoList.Length];// to assign airsegment in Create Reservation Pricing Info
                        int bkInfoIndex = 0;
                        foreach (Air20.BookingInfo bookingInfo in bookingInfoList)
                        {
                            if (keyGen(bookingInfo.SegmentRef) == jSegRefKey)
                            {
                                bookingClass = bookingInfo.BookingCode;
                                //typeCabinClass tempCabinClass = bookingInfo.CabinClass;
                                cabinClass = bookingInfo.CabinClass.ToString();
                                fareInfoKey = keyGen(bookingInfo.FareInfoRef);
                                break;
                                //Air20.FareInfo tempfareInfo = fareInfoDic[keyGen(bookingInfo.FareInfoRef)];
                                //fareBasisCodeRef.Add(keyGen(bookingInfo.SegmentRef), tempfareInfo);
                            }
                            
                            

                        }
                        flights[segRefIndex].BookingClass= bookingClass;
                        flights[segRefIndex].CabinClass = cabinClass;
                        flights[segRefIndex].FareInfoKey = fareInfoKey;
                        //Adding Connection Details
                        if (connectionList != null && connectionList.Length > 0)
                        {
                            foreach (Air20.Connection tempConnection in connectionList) // Connection Array
                            {
                                int conSegIndex = Convert.ToInt32(tempConnection.SegmentIndex);
                                if (conSegIndex == segmentIndex)
                                {
                                    flights[segRefIndex].StopOver = true; // Connection Flight
                                }
                                //else
                                //{
                                //    flights[segRefIndex].StopOver = false;
                                //}

                            
                            }

                        }
                        segRefIndex += 1;
                        if (segRefIndex== jSegRefList.Length )
                        {

                            if (jIndex == 0)// outbound
                            {
                                outFlights.Add(flights); 
                            }
                            else // return
                            {
                                inFlights.Add(flights);
                            }

                        }
                        segmentIndex++;
                    }
                    jIndex++;

                    
                }// Journey Array End


                if (inFlights.Count > 0) // return
                {

                    SearchResult result = new SearchResult();
                    result.UapiPricingSolution= pricingSolution;// for UAPI Create Reservatiobn Req.... to get Airfare with response
                    //result.UapiPricingSolution = new object();
                    result.FareBreakdown = fare;
                    result.TotalFare = totalResultFare;
                    result.BaseFare = totalbaseFare;
                    result.Tax = totalResultFare - totalbaseFare;
                    result.LastTicketDate = lastTktDate;
                    result.Currency = currency;
                    result.NonRefundable = nonRefundable;
                    //result.ResultBookingSource = BookingSource.Galileo;
                    result.ResultBookingSource = BookingSource.UAPI;
                    result.FareRules = new List<FareRule>();
                    result.Flights = new FlightInfo[2][];

                    result.Flights[0] = new FlightInfo[outFlights[0].Length];
                    for (int k = 0; k < outFlights[0].Length; k++)
                    {
                        result.Flights[0][k] = new FlightInfo();
                        outFlights[0][k].Stops = outFlights[0].Length - 1;
                        result.Flights[0][k] = FlightInfo.Copy(outFlights[0][k]);
                    }
                    // including mutli search as well

                    int inFlightLength = 0;
                    for (int f = 0; f < inFlights.Count; f++)
                    {
                        inFlightLength = inFlightLength + inFlights[f].Length;
                    }

                    result.Flights[1] = new FlightInfo[inFlightLength];
                    inFlightLength = 0;// reassigning values
                    for (int f = 0; f < inFlights.Count; f++)
                    {

                        for (int k = 0; k < inFlights[f].Length; k++)
                        {
                            result.Flights[1][inFlightLength] = new FlightInfo();
                            inFlights[f][k].Stops = inFlights[f].Length - 1;
                            result.Flights[1][inFlightLength] = FlightInfo.Copy(inFlights[f][k]);
                            inFlightLength++;
                        }
                    }
                    //result.Flights[1] = new FlightInfo[inFlights[0].Length];
                    //for (int k = 0; k < inFlights[0].Length; k++)
                    //{
                    //    result.Flights[1][k] = new FlightInfo();
                    //    inFlights[0][k].Stops = inFlights[0].Length - 1;
                    //    result.Flights[1][k] = FlightInfo.Copy(inFlights[0][k]);
                    //}

                    FlightInfo[] flightSegments = SearchResult.GetSegments(result);
                    for (int s = 0; s < flightSegments.Length; s++)
                    {
                        if (fareBasisCodeRef.ContainsKey(flightSegments[s].FareInfoKey))
                        {
                            FareRule fareRule = new FareRule();
                            fareRule.Airline = flightSegments[s].Airline;
                            fareRule.Destination = flightSegments[s].Destination.AirportCode;
                            fareRule.Origin = flightSegments[s].Origin.AirportCode;
                            
                            Air20.FareInfo tempFareInfo = fareBasisCodeRef[flightSegments[s].FareInfoKey];
                            fareRule.FareBasisCode = tempFareInfo.FareBasis;

                            Air20.FareRuleKey tempFareRuleKey= tempFareInfo.FareRuleKey;
                            fareRule.FareRuleKeyValue = tempFareRuleKey.Value;
                            fareRule.FareInfoRef= keyGen(tempFareRuleKey.FareInfoRef);

                            // fareRule.FareBasisCode = fareBasisCodeRef[s + 1]; todo ziya
                            result.FareRules.Add(fareRule);
                        }
                    }
                    string key = BuildResultKey(result);
                    if (!resultKey.Contains(key))
                    {
                        if (IsValidResult(result, request))
                        {
                            resultarray.Add(result);
                            resultKey.Add(key);
                        }
                    }

                }
                else // One Way
                {
                    SearchResult result = new SearchResult();
                    result.UapiPricingSolution = pricingSolution;// for UAPI Create Reservatiobn Req.... to get Airfare with response
                    //result.UapiPricingSolution = new object();
                    result.FareBreakdown = fare;
                    result.TotalFare = totalResultFare;
                    result.BaseFare = totalbaseFare;
                    result.Tax = totalResultFare - totalbaseFare;
                    result.LastTicketDate = lastTktDate;
                    result.Currency = currency;
                    result.NonRefundable = nonRefundable;
                    //result.ResultBookingSource = BookingSource.Galileo;
                    result.ResultBookingSource = BookingSource.UAPI;
                    result.FareRules = new List<FareRule>();
                    result.Flights = new FlightInfo[1][];
                    result.Flights[0] = new FlightInfo[outFlights[0].Length];
                    for (int j = 0; j < outFlights[0].Length; j++)
                    {
                        result.Flights[0][j] = new FlightInfo();
                        result.Flights[0][j] = FlightInfo.Copy(outFlights[0][j]);
                    }

                    FlightInfo[] flightSegments = SearchResult.GetSegments(result);
                    for (int s = 0; s < flightSegments.Length; s++)
                    {
                        if (fareBasisCodeRef.ContainsKey(flightSegments[s].FareInfoKey))
                        {
                            FareRule fareRule = new FareRule();
                            fareRule.Airline = flightSegments[s].Airline;
                            fareRule.Destination = flightSegments[s].Destination.AirportCode;
                            fareRule.Origin = flightSegments[s].Origin.AirportCode;

                            Air20.FareInfo tempFareInfo = fareBasisCodeRef[flightSegments[s].FareInfoKey];
                            fareRule.FareBasisCode = tempFareInfo.FareBasis;

                            Air20.FareRuleKey tempFareRuleKey = tempFareInfo.FareRuleKey;
                            fareRule.FareRuleKeyValue = tempFareRuleKey.Value;
                            fareRule.FareInfoRef = keyGen(tempFareRuleKey.FareInfoRef);

                            // fareRule.FareBasisCode = fareBasisCodeRef[s + 1]; todo ziya
                            result.FareRules.Add(fareRule);
                        }
                    }
                    string key = BuildResultKey(result);
                    if (!resultKey.Contains(key))
                    {
                        if (IsValidResult(result, request))
                        {
                            resultarray.Add(result);
                            resultKey.Add(key);
                        }
                    }
                }


            } // Air Pricing Solution End
            

           
            return resultarray.ToArray();
        }

        private static string BuildResultKey(SearchResult result)
        {
            int obLength = result.Flights[0].Length;
            StringBuilder key = new StringBuilder();
            key.Append(result.Flights[0][0].DepartureTime.DayOfYear.ToString());
            key.Append(result.Flights[0][0].DepartureTime.ToString("HHmm"));
            key.Append(result.Flights[0][obLength - 1].ArrivalTime.DayOfYear.ToString());
            key.Append(result.Flights[0][obLength - 1].ArrivalTime.ToString("HHmm"));
            if (result.Flights.Length > 1)
            {
                int ibLength = result.Flights[1].Length;
                key.Append(result.Flights[1][0].DepartureTime.DayOfYear.ToString());
                key.Append(result.Flights[1][0].DepartureTime.ToString("HHmm"));
                key.Append(result.Flights[1][ibLength - 1].ArrivalTime.DayOfYear.ToString());
                key.Append(result.Flights[1][ibLength - 1].ArrivalTime.ToString("HHmm"));
            }
            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    key.Append(result.Flights[i][j].FlightNumber);
                    key.Append(result.Flights[i][j].BookingClass);
                }
            }
            return key.ToString();
        }
        private static bool IsValidResult(SearchResult result, SearchRequest request)
        {
            bool isValid = false;
            for (int i = 0; i < result.Flights.Length; i++)
            {
                if (i == 0)
                {
                    if (((result.Flights[i][0].Origin.CityCode == request.Segments[0].Origin) || (result.Flights[i][0].Origin.AirportCode == request.Segments[0].Origin)) && ((result.Flights[i][result.Flights[i].Length - 1].Destination.CityCode == request.Segments[0].Destination) || (result.Flights[i][result.Flights[i].Length - 1].Destination.AirportCode == request.Segments[0].Destination)))
                    {
                        isValid = true;
                    }
                }
                else
                {
                    if (((result.Flights[i][0].Origin.CityCode == request.Segments[0].Destination) || (result.Flights[i][0].Origin.AirportCode == request.Segments[0].Destination)) && ((result.Flights[i][result.Flights[i].Length - 1].Destination.CityCode == request.Segments[0].Origin) || (result.Flights[i][result.Flights[i].Length - 1].Destination.AirportCode == request.Segments[0].Origin)))
                    {
                        isValid = true;
                    }
                }
            }
            return isValid;
        }
    # endregion

        # region Fare Rules

        public static List<FareRule> GetFareRuleList(List<FareRule> fareRuleList)
        {
            Connection();

            AirFareRulesReq fareRulereq = new AirFareRulesReq();
            fareRulereq.TargetBranch = targetBranch;
            fareRulereq.FareRuleType = typeFareRuleType.@long;

            BillingPointOfSaleInfo pos = new BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            fareRulereq.BillingPointOfSaleInfo = pos;

            fareRulereq.FareRuleKey=new FareRuleKey[fareRuleList.Count];
            for (int i = 0; i < fareRuleList.Count; i++)
            {
                


                FareRuleKey fRuleKey = new FareRuleKey();

                string tempFareBasisCode = fareRuleList[i].FareBasisCode;
                string tempFareInfoRef = string.Empty;
                string tempFareRuleKeyValue = string.Empty;
                if (tempFareBasisCode.IndexOf('-') != -1) // From UAPI - FareInfo Key & FareInfo Key Value
                {
                    string[] fareInfo = tempFareBasisCode.Split('-');

                    if (fareInfo.Length > 1)
                    {
                        //fRule.FareBasisCode = fareInfo[0];
                        tempFareInfoRef = fareInfo[1];
                        tempFareRuleKeyValue = fareInfo[2];
                    }
                }
                else
                {
                    tempFareInfoRef = Convert.ToString(fareRuleList[i].FareInfoRef);
                    tempFareRuleKeyValue = fareRuleList[i].FareRuleKeyValue; 

                    //fRule.FareBasisCode = tempFareBasisCode;
                }

                fRuleKey.FareInfoRef = tempFareInfoRef + "T";
                fRuleKey.ProviderCode = "1G"; // TODO-- add Dynamic Provider
                fRuleKey.Value = tempFareRuleKeyValue;

                fareRulereq.FareRuleKey[i] = new FareRuleKey();
                fareRulereq.FareRuleKey[i] = fRuleKey;





                //fRuleKey.FareInfoRef = Convert.ToString(fareRuleList[i].FareInfoRef)+"T";
                //fRuleKey.ProviderCode = "1G"; // TODO-- add Dynamic Provider
                //fRuleKey.Value = fareRuleList[i].FareRuleKeyValue;

                //fareRulereq.FareRuleKey[i] = new FareRuleKey();
                //fareRulereq.FareRuleKey[i] = fRuleKey;

            }

            // To remove
            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer serReq = new System.Xml.Serialization.XmlSerializer(fareRulereq.GetType());
                System.Text.StringBuilder sbReq = new System.Text.StringBuilder();
                System.IO.StringWriter writerReq = new System.IO.StringWriter(sbReq);
                serReq.Serialize(writerReq, fareRulereq); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docReq = new XmlDocument();
                docReq.LoadXml(sbReq.ToString());
                docReq.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirFareRulesReq .xml");
            }
            AirFareRulesBinding binding = new AirFareRulesBinding();
            binding.Url = urlAir;
            binding.Credentials = new NetworkCredential(userName, password);
            AirFareRulesRsp fareRuleResponse = binding.service(fareRulereq);

            // startTo Remove
            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(fareRuleResponse.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, fareRuleResponse); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());

                doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirFareRulesRsp.xml");
                // end To Remove
            }
            // Fare Rule Response
            Air20.FareRule[] fareRulesResultList = fareRuleResponse.FareRule;
            foreach (Air20.FareRule fareRulesResult in fareRulesResultList)
            {
                //string category = string.Empty;
                string fareruleValue = string.Empty;
                int farInfoRef = keyGen(fareRulesResult.FareInfoRef);
                FareRuleLong[] fareRuleslongList = fareRulesResult.FareRuleLong;
                if (fareRuleslongList!=null &&fareRuleslongList.Length > 0)
                {
                    foreach (FareRuleLong fareRuleLong in fareRuleslongList)
                    {
                        fareruleValue += "<br>" + fareRuleLong.Value;
                    }


                    for (int i = 0; i < fareRuleList.Count; i++)
                    {
                        if (fareRuleList[i].FareInfoRef == farInfoRef)
                        {
                            fareRuleList[i].FareRuleDetail = fareruleValue;
                        }
                    }
                }
            }

            return fareRuleList;
        }
        # endregion

        # region Book

        /// <summary>
        /// Method to book a selected Itinerary
        /// </summary>
        /// <param name="itinerary">An object of FlightItinerary</param>
        /// <returns>BookingResponse</returns>
        public static BookingResponse Book(CT.BookingEngine.FlightItinerary itinerary, string sessionId)
        {
            BookingResponse bookingResponse = new BookingResponse();
            bool isDomestic = itinerary.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
            AirCreateReservationRsp reservationRsp=new AirCreateReservationRsp();
            reservationRsp = CreateReservation(itinerary);


            // To Remove
            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(reservationRsp.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, reservationRsp); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirReservationRes .xml");
                //

            }
            Audit.Add(EventType.Book, Severity.Normal, 0, "UAPI Create Reservation Response. : ", string.Empty);
            bool booked=true;

            //Checking Error
            AirSegmentError[] sellFailureErr = reservationRsp.AirSegmentSellFailureInfo;
            if (sellFailureErr != null)
            {
                booked = false;
                
                Basket.FlightBookingSession[sessionId].Log.Add("UAPI Sell Failure Error.");
                bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "SEAT NOT AVAILABLE -" + sellFailureErr[0].ErrorMessage, "");
                Audit.Add(EventType.Book, Severity.High, 0, "Booking Failed UAPI. Sell Failure Error :" + sellFailureErr[0].ErrorMessage, string.Empty);
            }
            else if (reservationRsp.AirSolutionChangedInfo != null && reservationRsp.AirSolutionChangedInfo.Length>0) // Price/Schedule Changed - if yes, pnr will not generate
            {
                booked = false;

                Basket.FlightBookingSession[sessionId].Log.Add("UAPI Air Soulution Changed Info.ReasonCode:" + reservationRsp.AirSolutionChangedInfo[0].ReasonCode.ToString());
                if (reservationRsp.ResponseMessage != null)
                {
                    bookingResponse = new BookingResponse(BookingResponseStatus.Failed, reservationRsp.ResponseMessage[0].Value, string.Empty);
                    Audit.Add(EventType.Book, Severity.High, 0, "Booking Failed UAPI:(" + reservationRsp.AirSolutionChangedInfo[0].ReasonCode.ToString() + "):" + reservationRsp.ResponseMessage[0].Value, string.Empty);
                }
                else
                {
                    bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "Price/Schedule Changed", string.Empty);
                    Audit.Add(EventType.Book, Severity.High, 0, "Booking Failed UAPI:Price/Schedule Changed:" + reservationRsp.AirSolutionChangedInfo[0].ReasonCode.ToString(), string.Empty);
                }
                
                
            }
            else
            {
                UniversalRecord unRecord = reservationRsp.UniversalRecord;
                AirReservation[] airReservationList = unRecord.Items;
                if (airReservationList == null)
                    booked = false;
                foreach (AirReservation airReservation in airReservationList)
                {
                    booked = ReadBookingStatus(airReservation);

                }
                if (booked)
                {
                    bookingResponse = ReadBookResponse(reservationRsp,itinerary);
                    Audit.Add(EventType.Book, Severity.High, 0, "Book UAPI.booking Response from ReadBookResponse ", string.Empty); 

                }
                else
                {
                    
                    
                    Basket.FlightBookingSession[sessionId].Log.Add("UAPI Booking Failed.");
                    bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "SEAT NOT AVAILABLE", "");
                    Audit.Add(EventType.Book, Severity.High, 0, "Booking Failed UAPI. Booked Status : false", string.Empty);
                }

            }

            if (booked == false)
            {
                if (reservationRsp.UniversalRecord != null)
                {
                    if (!string.IsNullOrEmpty(reservationRsp.UniversalRecord.LocatorCode))
                    {
                        CancelItinerary(reservationRsp.UniversalRecord.LocatorCode, "UR");
                    }
                }
            }

            return bookingResponse;

        }

        private static AirCreateReservationRsp CreateReservation(FlightItinerary itinerary)
        {
            Connection();
            List<KeyValuePair<string, SSR>> ssrList = GenerateSSRPaxList(itinerary);
            AirCreateReservationRsp reservationRsp=new AirCreateReservationRsp();
            // for mutli city booking 
            AirCreateReservationReq createRequest = new AirCreateReservationReq();
            ContinuityCheckOverride continuityCheck = new ContinuityCheckOverride();
            continuityCheck.Key = "Yes";
            continuityCheck.Value = "Yes";
            createRequest.ContinuityCheckOverride = continuityCheck;


            createRequest.TargetBranch = targetBranch;
            //createRequest.RetainReservation = typeRetainReservation.Both;
            createRequest.RetainReservation = typeRetainReservation.None;// If schedule/Price Change.... PNR should not be created

            BillingPointOfSaleInfo pos = new BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            createRequest.BillingPointOfSaleInfo = pos;

            createRequest.BookingTraveler = new BookingTraveler[itinerary.Passenger.Length];
            int paxIndex = 0;
            foreach (FlightPassenger passenger in itinerary.Passenger)
            {
                string passengerType = string.Empty;
                // Asigning Pax Type
                if (passenger.Type == PassengerType.Adult)
                    passengerType = "ADT";
                else if (passenger.Type == PassengerType.Child)
                    passengerType = "CNN";
                else if (passenger.Type == PassengerType.Infant)
                    passengerType = "INF";
                else if (passenger.Type == PassengerType.Senior)
                    passengerType = "SRC";
                BookingTraveler tmpBkTraveler = new BookingTraveler();
                tmpBkTraveler.Key = paxIndex.ToString();
                tmpBkTraveler.TravelerType = passengerType;
                string tmpGender = "M";
                if (passenger.Gender == Gender.Male)
                    tmpGender = "M";
                else if (passenger.Gender == Gender.Female)
                    tmpGender = "F";
                tmpBkTraveler.Gender = tmpGender;
                tmpBkTraveler.DOb = passenger.DateOfBirth.ToString("yyyy-MM-dd");
                // adding SSR
                tmpBkTraveler.SSR = new Air20.SSR[ssrList.Count];
                if (passenger.Type != PassengerType.Child && passenger.Type != PassengerType.Infant)
                {
                    int ssrPaxIndex = 0;
                    for (int x = 0; x < ssrList.Count; x++)
                    {
                        KeyValuePair<string, SSR> ssrPax = ssrList[x];

                        //if (!string.IsNullOrEmpty(ssrList[paxIndex].Key))
                        if (ssrPax.Key == passenger.PaxKey)
                        {
                            //KeyValuePair<string, SSR> ssrPax = ssrPaxList[paxIndex];
                            SSR tmpSSR = ssrPax.Value;
                            Air20.SSR airSSR = new Air20.SSR();
                            airSSR.Type = tmpSSR.SsrCode;
                            airSSR.FreeText = tmpSSR.Detail;

                            tmpBkTraveler.SSR[ssrPaxIndex] = airSSR;
                            ssrPaxIndex++;
                        }

                        //List<SSR> ssrPaxList = new List<SSR>();
                    }
                }
                //ssrPaxList.Add(

                //Air20.SSR[1] airSSRList = new Air20.SSR();




                BookingTravelerName tmpTravelrName = new BookingTravelerName();
                tmpTravelrName.Prefix = passenger.Title;
                tmpTravelrName.First = passenger.FirstName;
                tmpTravelrName.Last = passenger.LastName;
                tmpBkTraveler.BookingTravelerName = tmpTravelrName;

                PhoneNumber tmpPhone = new PhoneNumber();
                tmpPhone.Type = PhoneNumberType.Mobile;
                //tmpPhone.Number = passenger.CellPhone;
                tmpPhone.Number = itinerary.Passenger[0].CellPhone;
                tmpBkTraveler.PhoneNumber = new PhoneNumber[1];
                tmpBkTraveler.PhoneNumber[0] = new PhoneNumber();
                tmpBkTraveler.PhoneNumber[0] = tmpPhone;

                //if (paxIndex == 0)
                //{
                // adding pax Addres 
                typeStructuredAddress tmpAdds = new typeStructuredAddress();
                tmpAdds.AddressName = passenger.AddressLine1;
                //if (!string.IsNullOrEmpty(passenger.AddressLine2))
                //{
                //    tmpAdds.Street = new string[1];
                //    tmpAdds.Street[0] = passenger.AddressLine2;
                //}
                //if (!string.IsNullOrEmpty(passenger.AddressLine2))
                //{
                //    tmpAdds.City = passenger.City;
                //}
                //if (!string.IsNullOrEmpty(passenger.AddressLine2))
                //{
                //    State tmpState = new State();
                //    tmpState.Value = passenger.City;
                //    tmpAdds.State = tmpState;
                //}  For timebeing assiginh addresline1

                if (!string.IsNullOrEmpty(passenger.AddressLine1))
                {
                    tmpAdds.Street = new string[1];
                    tmpAdds.Street[0] = passenger.AddressLine1;
                }
                if (!string.IsNullOrEmpty(passenger.AddressLine1) || !string.IsNullOrEmpty(passenger.City))
                {
                    if (!string.IsNullOrEmpty(passenger.City)) tmpAdds.City = passenger.City;
                    else tmpAdds.City = passenger.AddressLine1;
                }
                if (!string.IsNullOrEmpty(passenger.AddressLine1) || !string.IsNullOrEmpty(passenger.City))
                {
                    State tmpState = new State();
                    if (!string.IsNullOrEmpty(passenger.City)) tmpState.Value = passenger.City;
                    else tmpState.Value = passenger.AddressLine1;
                    tmpAdds.State = tmpState;
                }

                //tmpAdds.PostalCode = "0097160";// To DO ziyad-- validating UAPI based on Country
                tmpAdds.Country = passenger.Country.CountryCode;

                tmpBkTraveler.Address = new typeStructuredAddress[1];
                tmpBkTraveler.Address[0] = new typeStructuredAddress();
                tmpBkTraveler.Address[0] = tmpAdds;
                

                // adding pax Loyalty Card details
                if (!string.IsNullOrEmpty(passenger.FFAirline) && !string.IsNullOrEmpty(passenger.FFNumber))
                {
                    string[] tmpFFAirlines=passenger.FFAirline.Split(',');
                    string[] tmpFFNumbers = passenger.FFNumber.Split(',');
                    LoyaltyCard[] lyCardList = new LoyaltyCard[tmpFFAirlines.Length];
                    for (int x = 0; x < tmpFFAirlines.Length-1;x++ )
                    {
                        if (!string.IsNullOrEmpty(tmpFFNumbers[x]))
                        {
                            LoyaltyCard lyCard = new LoyaltyCard();
                            lyCard.SupplierCode = tmpFFAirlines[x];
                            lyCard.CardNumber = tmpFFNumbers[x];
                            lyCardList[x] = lyCard;
                        }
                        
                    }
                    tmpBkTraveler.LoyaltyCard = lyCardList;

                }

                //}
                createRequest.BookingTraveler[paxIndex] = new BookingTraveler();
                createRequest.BookingTraveler[paxIndex] = tmpBkTraveler;
                paxIndex++;
            }

           


            itinerary.UapiPricingSolution.Journey = null;
            itinerary.UapiPricingSolution.LegRef = null;
            int paxRef = 0;
            foreach (Air20.AirPricingInfo tempPriceinfo in itinerary.UapiPricingSolution.AirPricingInfo)
            {
                tempPriceinfo.FareInfoRef = null;
                foreach (Air20.FareInfo tempfareInfo in tempPriceinfo.FareInfo)
                {
                    tempfareInfo.FareSurcharge = null;
                    tempfareInfo.FareRuleKey = null;
                    tempfareInfo.BaggageAllowance = null;
                    
                }

                foreach (Air20.BookingInfo tempBookingInfo in tempPriceinfo.BookingInfo)
                {
                    tempBookingInfo.SegmentRef= null;

                }
                
                foreach (Air20.PassengerType tempPaxType in tempPriceinfo.PassengerType)
                {
                    tempPaxType.BookingTravelerRef = paxRef.ToString(); ;
                    paxRef++;

                }
            }

            itinerary.UapiPricingSolution.AirSegment = new AirSegment[itinerary.Segments.Length];
            int segIndex = 0;
            foreach (FlightInfo flight in itinerary.Segments)
            {
                string airSegKey = Convert.ToString(flight.UapiSegmentRefKey) + "T";

                AirSegment tmpAirSegment = new AirSegment();
                tmpAirSegment.Key = airSegKey;
                tmpAirSegment.Group = flight.Group;
                tmpAirSegment.Carrier = flight.Airline;
                tmpAirSegment.FlightNumber = flight.FlightNumber;
                tmpAirSegment.Origin = flight.Origin.AirportCode;
                tmpAirSegment.Destination = flight.Destination.AirportCode;
                //tmpAirSegment.DepartureTime = flight.DepartureTime.ToString("yyyy-MM-dd");  To get Schedule Error
                //tmpAirSegment.DepartureTime = flight.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss");
                //tmpAirSegment.ArrivalTime = flight.ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"); 
                tmpAirSegment.DepartureTime = flight.UapiDepartureTime;
                tmpAirSegment.ArrivalTime = flight.UapiArrivalTime;

                tmpAirSegment.ClassOfService = flight.BookingClass;

                itinerary.UapiPricingSolution.AirSegment[segIndex] = new AirSegment();
                itinerary.UapiPricingSolution.AirSegment[segIndex] = tmpAirSegment;
                segIndex++;
                if (flight.StopOver)
                {
                    tmpAirSegment.Connection = new Connection();
                }
            }



            createRequest.AirPricingSolution=itinerary.UapiPricingSolution;
            
            ActionStatus actionStatus = new ActionStatus();
            actionStatus.ProviderCode = "1G";// To do -- should be assigned dynamically
            actionStatus.Type = ActionStatusType.ACTIVE;

            createRequest.ActionStatus = new ActionStatus[1];
            createRequest.ActionStatus[0] = new ActionStatus();
            createRequest.ActionStatus[0] = actionStatus;

            // For Payment Information
            FormOfPayment paymentForm = new FormOfPayment();
            paymentForm.Key = "123";
            paymentForm.Type = "Cash";
            createRequest.FormOfPayment= new FormOfPayment[1];
            createRequest.FormOfPayment[0] = new FormOfPayment();
            createRequest.FormOfPayment[0] = paymentForm;


            // To Remove
            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(createRequest.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, createRequest); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirReservationReq .xml");
                //
            }
            AirCreateReservationBinding binding = new AirCreateReservationBinding();
            binding.Url = urlAir;
            binding.Credentials = new NetworkCredential(userName, password);

            reservationRsp = binding.service(createRequest);
            

            return reservationRsp;


        }


        private static bool ReadBookingStatus(AirReservation airReservation)
        { 
            bool booked = true;
            List<string> availabilityStatus = new List<string>();
            availabilityStatus.Add("HS");
            availabilityStatus.Add("SS");
            availabilityStatus.Add("HK");
            availabilityStatus.Add("KK");
            availabilityStatus.Add("TK");

            AirSegment[] airSegmentList = airReservation.AirSegment;
            if (airSegmentList != null)
            {
                foreach (AirSegment airSegment in airSegmentList)
                {
                    string status = airSegment.Status;
                    if (!availabilityStatus.Contains(status))
                    {
                        booked = false;
                    }

                }
            }
            else
                booked = false;

            return booked;
           
        }

        private static BookingResponse ReadBookResponse(AirCreateReservationRsp reservationRsp, FlightItinerary itinerary)
        {
            
            
            BookingResponse booking = new BookingResponse();
            UniversalRecord unRecord = reservationRsp.UniversalRecord;
            string ssrMessage = string.Empty;
            // IF SSR is required for Pax 
            if (unRecord != null)
            {


                Air20.BookingTraveler[] tmpBktravelerList=unRecord.BookingTraveler;
                if (tmpBktravelerList != null)
                {
                    foreach (Air20.BookingTraveler tmpBktraveler in tmpBktravelerList)
                    {
                        Air20.SSR[] tempSSRList = tmpBktraveler.SSR;

                        if (tempSSRList != null)
                        {
                            foreach (Air20.SSR tempSSR in tempSSRList)
                            {

                                ssrMessage += " (" + tmpBktraveler.BookingTravelerName.First + ") SSR type: " + tempSSR.Type + ",Status:" + tempSSR.Status + ", Free Text:" + tempSSR.FreeText;
                            }
                        }
                    }
                }
               
                // end SSR

                string URlocatorCode = unRecord.LocatorCode;// UR
                string providerLocatorCode = unRecord.ProviderReservationInfo[0].LocatorCode;// TO do with multiple Airline Test
                // PNR ( supplier)
                string reservationLocatorCode = string.Empty;


                AirReservation[] airReservationList = unRecord.Items;
                if (airReservationList != null)
                {
                    foreach (AirReservation airReservation in airReservationList)
                    {
                        reservationLocatorCode = airReservation.LocatorCode;// TO do with multiple Airline Test
                        AirSegment[] airSegmentList = airReservation.AirSegment;
                        int airSegIndex = 0;
                        foreach (AirSegment airSegment in airSegmentList)
                        {
                            string status = airSegment.Status;

                            if ((airSegIndex + 1) == Convert.ToInt32(airSegment.TravelOrder)) // To cross check the Segments/Flights
                            {
                                itinerary.Segments[airSegIndex].Status = status;
                                if ((itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'K') || (itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'S'))
                                {
                                    itinerary.Segments[airSegIndex].FlightStatus = FlightStatus.Confirmed;
                                    SupplierLocator[] supLocatorList = airReservation.SupplierLocator;
                                    if (supLocatorList != null)
                                    {
                                        foreach (SupplierLocator supLocator in supLocatorList)
                                        {

                                            if (airSegment.Carrier == supLocator.SupplierCode)
                                            {
                                                itinerary.Segments[airSegIndex].AirlinePNR = supLocator.SupplierLocatorCode;

                                            }
                                        }
                                    }

                                }
                            }
                            airSegIndex++;

                        }

                    }
                    //itinerary.UniversalRecord = URlocatorCode;// Assigning UR to Itinerary OLD
                    //itinerary.PNR = reservationLocatorCode;// REservation PNr is Actual Pnr
                    //itinerary.ProviderPNR = providerLocatorCode;//Provider PNR is Actual Pnr
                    //booking.PNR = reservationLocatorCode;//reservationLocatorCode;


                    
                    
                    
                    
                    itinerary.UniversalRecord = URlocatorCode;// Assigning UR to Itinerary
                    itinerary.PNR = providerLocatorCode;// //Provider PNR is Actual Pnr
                    itinerary.AirLocatorCode = reservationLocatorCode;//Reservation PNR is for issue the Ticket
                    booking.PNR = providerLocatorCode;//reservationLocatorCode;
                    itinerary.FareType = "PUB";
                    booking.Status = BookingResponseStatus.Successful;
                    // SSR Details




                }
                else
                {
                    booking.Status = BookingResponseStatus.Failed;
                    booking.Error = "NO VALID FARE FOR INPUT CRITERIA";
                }
            }
            else
            {
                booking.Status = BookingResponseStatus.Failed;
                booking.Error = "UniversalRecord is null";
            }
            if (ssrMessage.Length > 0)
            {
                booking.SSRMessage = ssrMessage;
                booking.SSRDenied = true;
            }

            return booking;
            
           
              
                
               // For Price Info Changed
                //for (int i = 0; i < itinerary.Passenger.Length; i++)
                //{
                //    if (paxRef.ContainsKey(itinerary.Passenger[i].Type))
                //    {
                //        int fareKey = paxRef[itinerary.Passenger[i].Type];

                //        if (priceRef.ContainsKey(fareKey) && (itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.Tax) != (priceRef[fareKey].PublishedFare + priceRef[fareKey].Tax))
                //        {
                //            itinerary.Passenger[i].Price.PublishedFare = priceRef[fareKey].PublishedFare;
                //            itinerary.Passenger[i].Price.Tax = priceRef[fareKey].Tax;
                //            booking.Status = BookingResponseStatus.BookedOther;
                //        }
                //    }
                //}
               
           
               
            
            
        }
        # endregion

        # region Cancel Itinerary
        public static new string CancelItinerary(string pnr,string pnrType)
        {
            Connection();
            //bool isCancel = false;
            string tempPNR = pnr;
            string cancelledPNR = string.Empty;
            string cancelledProvider = string.Empty;
            bool isCancelled = false;

            if (pnrType != "UR")
            {
                FlightItinerary itinerary = new FlightItinerary(FlightItinerary.GetFlightId(pnr));
                pnr = itinerary.UniversalRecord;

            }

            
            CT.Core.Audit.Add(EventType.CancellBooking, Severity.Normal, 0, "UAPI Cancell Itinerary : Request UR PNR = " + pnr, "0");

            UR18.UniversalRecordCancelReq urCancelRequest = new UR18.UniversalRecordCancelReq();
            urCancelRequest.TargetBranch = targetBranch;
            urCancelRequest.UniversalRecordLocatorCode = pnr;
            urCancelRequest.Version = "18";// TO DO
            urCancelRequest.AuthorizedBy = string.Empty ;

            UR18.BillingPointOfSaleInfo pos = new UR18.BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            urCancelRequest.BillingPointOfSaleInfo = pos;

            UR18.UniversalRecordCancelServiceBinding binding = new UR18.UniversalRecordCancelServiceBinding();
            
            binding.Url = urlUR;
            binding.Credentials = new NetworkCredential(userName, password);

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(urCancelRequest.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, urCancelRequest); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\UniversalRecordCancelReq.xml");
            }

            UR18.UniversalRecordCancelRsp urCancelResponse = null;

            try
            {
                urCancelResponse = binding.service(urCancelRequest);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.CancellBooking, Severity.High, 0, "UAP.CancelItinerary exiting : Cancellation of PNR " + pnr + " failed. Erro msg:"+ex.Message, string.Empty);
                cancelledPNR = ex.Message;
                
                return cancelledPNR;
            }


            // TO Remove
            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(urCancelResponse.GetType());
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, urCancelResponse); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docRes = new XmlDocument();
                docRes.LoadXml(sbRes.ToString());
                docRes.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\UniversalRecordCancelRsp.xml");
            }
            UR18.ProviderReservationStatus[] reservationStatusList = urCancelResponse.ProviderReservationStatus;
            if (reservationStatusList != null)
            {
                foreach (UR18.ProviderReservationStatus reservationStatus in reservationStatusList)
                {
                    isCancelled = reservationStatus.Cancelled;
                    cancelledPNR = reservationStatus.LocatorCode;
                    cancelledProvider= reservationStatus.ProviderCode;
                }

            }
            if (isCancelled)
            {
                if (pnrType != "UR")
                    cancelledPNR = tempPNR;



            }
            else
            {
                Trace.TraceError("UAP.CancelItinerary exiting : Cancellation of PNR " + pnr + " failed.");
                throw new BookingEngineException("Cancellation of PNR " + pnr + " failed.");
            }




            
              
            return cancelledPNR;
        }
        # endregion

        # region Retrieve Itinerary
        public static FlightItinerary RetrieveItinerary(string pnr)
        {
            FlightItinerary itinerary = new FlightItinerary();
            Ticket[] ticket;
            Trace.TraceInformation("UAPI.RetrieveItinerary entered WITH PNR Param : PNR= " + pnr);
            itinerary = RetrieveItinerary(pnr, out ticket);
            return itinerary;
        }
        public static FlightItinerary RetrieveItinerary(string pnr, out Ticket[] ticket)
        {
            FlightItinerary itinerary = new FlightItinerary();
            Trace.TraceInformation("UAPI.RetrieveItinerary entered : PNR = " + pnr);
            
           // UR18.UniversalRecordRetrieveRsp pnrResponse = GenerateRetrievePNRObject(pnr);
            UR18.UniversalRecordImportRsp URimportResponse = ImportUR(pnr);
            try
            {
                itinerary = MakeBookingObject(URimportResponse, out ticket);
            }
            catch (BookingEngineException ex)
            {
               // throw ex;
                string errorMess = ex.Message;
                string restrictedBf = "UNABLE TO RETRIEVE - RESTRICTED BF";
                if (errorMess.IndexOf(restrictedBf) >= 0)
                {
                    //connection = new Connection(true);

                    //pnrResponse = connection.SubmitXml(request);
                    try
                    {
                        itinerary = MakeBookingObject(URimportResponse, out ticket);
                    }
                    catch (BookingEngineException excep)
                    {
                        throw ex;
                        //errorMess = excep.Message;
                        //restrictedBf = "UNABLE TO RETRIEVE - RESTRICTED BF";
                        //if (errorMess.IndexOf(restrictedBf) >= 0)
                        //{
                        //    string pcc = ConfigurationSystem.GalileoConfig["DomesticPCC"];
                        //    itinerary.PNR = pnr;
                        //    GetTicketref itinerary, out ticket, pcc);
                        //}
                        //else
                        //{
                        //    throw ex;
                        //}
                    }
                }
                else
                {
                    throw ex;
                }
            }
            return itinerary;
        }
        public static FlightItinerary RetrieveItinerary(FlightItinerary itinerary, out Ticket[] ticket)

        {
            //FlightItinerary itinerary = new FlightItinerary();
            Trace.TraceInformation("UAPI.RetrieveItinerary entered : PNR = " + itinerary.PNR);


            UR18.UniversalRecordImportRsp URimportResponse = ImportUR(itinerary.PNR);
            try
            {
                itinerary = MakeBookingObject(URimportResponse, out ticket);
            }
            catch (BookingEngineException ex)
            {
                // throw ex;
                string errorMess = ex.Message;
                string restrictedBf = "UNABLE TO RETRIEVE - RESTRICTED BF";
                if (errorMess.IndexOf(restrictedBf) >= 0)
                {
                    //connection = new Connection(true);

                    //pnrResponse = connection.SubmitXml(request);
                    try
                    {
                        itinerary = MakeBookingObject(URimportResponse, out ticket);
                    }
                    catch (BookingEngineException excep)
                    {
                        throw ex;
                        //errorMess = excep.Message;
                        //restrictedBf = "UNABLE TO RETRIEVE - RESTRICTED BF";
                        //if (errorMess.IndexOf(restrictedBf) >= 0)
                        //{
                        //    string pcc = ConfigurationSystem.GalileoConfig["DomesticPCC"];
                        //    itinerary.PNR = pnr;
                        //    GetTicketref itinerary, out ticket, pcc);
                        //}
                        //else
                        //{
                        //    throw ex;
                        //}
                    }
                }
                else
                {
                    throw ex;
                }
            }
            return itinerary;
        }

        private static UR18.UniversalRecordRetrieveRsp GenerateRetrievePNRObject(string pnr)// Not using this method since we are using Import UR
        {
            Connection();
            Trace.TraceInformation("UApi.GenerateRetrievePNRObject entered");
            UR18.UniversalRecordRetrieveReq request = new UR18.UniversalRecordRetrieveReq();

            request.TargetBranch = targetBranch;
            request.UniversalRecordLocatorCode = pnr;

            UR18.BillingPointOfSaleInfo pos = new UR18.BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            request.BillingPointOfSaleInfo = pos;

            UR18.UniversalRecordRetrieveServiceBinding binding = new UR18.UniversalRecordRetrieveServiceBinding();
            binding.Url = urlUR;
            binding.Credentials = new NetworkCredential(userName, password);


            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(request.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, request); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\URRetrieveReq.xml");
            }

            UR18.UniversalRecordRetrieveRsp response =null;
            try
            {
                response= binding.service(request);

            }
            catch { throw; }
            //{
            //    throw ex;
            //}

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(response.GetType());
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, response); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docres = new XmlDocument();
                docres.LoadXml(sbRes.ToString());
                docres.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\URRetrieveRes.xml");
            }
            Trace.TraceInformation("UApi.GenerateRetrievePNRObject exiting");
            return response;
        }

       /* private static FlightItinerary MakeBookingObject(UR18.UniversalRecordRetrieveRsp response, out Ticket[] ticket)// this method is not using currently,its for retrive pnr.. refer overloaded method
        {
            ticket = new Ticket[0];
            FlightItinerary itinerary = new FlightItinerary();
            UR18.UniversalRecord uRecord=response.UniversalRecord;
            itinerary.UniversalRecord = uRecord.LocatorCode;

            itinerary.PNR = uRecord.ProviderReservationInfo[0].LocatorCode;// TO do with multiple Airline Test


            // Passenger Information 
            UR18.BookingTraveler[] bookingTravelerList = uRecord.BookingTraveler;

            Dictionary<long, FlightPassenger> passengerDict = new Dictionary<long, FlightPassenger>();// To Stor Passnger Details
            FlightPassenger[] tempPassengerList = new FlightPassenger[bookingTravelerList.Length];
            Dictionary<long, List<KeyValuePair<string, decimal>>> taxBreakUpPax = new Dictionary<long, List<KeyValuePair<string, decimal>>>(); // To Store Pax wise Tax Break Up
            int paxIndex = 0;
            foreach (UR18.BookingTraveler bookingTraveler in bookingTravelerList)
            {
                //bookingTraveler.TravelerType
                tempPassengerList[paxIndex] = new FlightPassenger();
                if (bookingTraveler.TravelerType == "ADT")
                    tempPassengerList[paxIndex].Type = PassengerType.Adult;
                if (bookingTraveler.TravelerType == "CNN")
                    tempPassengerList[paxIndex].Type = PassengerType.Child;
                if (bookingTraveler.TravelerType == "INF")
                    tempPassengerList[paxIndex].Type = PassengerType.Infant;
                if (bookingTraveler.TravelerType == "SRC")
                    tempPassengerList[paxIndex].Type = PassengerType.Senior;

                UR18.BookingTravelerName travelerName = new UR18.BookingTravelerName();
                travelerName = bookingTraveler.BookingTravelerName;
                tempPassengerList[paxIndex].FirstName = travelerName.First;
                tempPassengerList[paxIndex].LastName = travelerName.Last;
                tempPassengerList[paxIndex].Title = travelerName.Prefix;


                UR18.PhoneNumber[] phone = bookingTraveler.PhoneNumber;
                tempPassengerList[paxIndex].CellPhone = phone[0].Number;
                UR18.typeStructuredAddress[] adds = bookingTraveler.Address;
                tempPassengerList[paxIndex].AddressLine1 = adds[0].AddressName;
                if (adds[0].Street != null)
                {
                    string[] street = adds[0].Street;
                    tempPassengerList[paxIndex].AddressLine2 = street[0].ToString();
                }
                
                if(adds[0].City!=null)tempPassengerList[paxIndex].City = adds[0].City;

                Country tempCountry=new Country();
                tempCountry.CountryCode= adds[0].Country;
                tempPassengerList[paxIndex].Country=tempCountry;
                passengerDict.Add(Convert.ToInt64(bookingTraveler.Key), tempPassengerList[paxIndex]);
                paxIndex++;

                // Segment Information


            }
            // Air Reservation Details
            string reservationLocatorCode = string.Empty;
            UR18.AirReservation[] airReservationList = response.UniversalRecord.AirReservation;
            if (airReservationList != null)
            {

                foreach (UR18.AirReservation airReservation in airReservationList)
                {
                    reservationLocatorCode = airReservation.LocatorCode;// storing the PNR for issue the ticket for UAPI-TO do with multiple Airline Test
                    
                    itinerary.CreatedOn = Convert.ToDateTime(airReservation.CreateDate);
                    //itinerary.Ticketed = true; TO check after Ticket
                    itinerary.FareType = "PUB";
                    itinerary.FlightBookingSource = BookingSource.UAPI;
                    itinerary.Segments = new FlightInfo[airReservation.AirSegment.Length];

                    // Air Segment Details
                     int airSegIndex = 0;
                     foreach (UR18.AirSegment tempAirSegment in airReservation.AirSegment)
                    {

                        itinerary.Segments[airSegIndex] = new FlightInfo();
                        itinerary.Segments[airSegIndex].Status = tempAirSegment.Status;
                        if ((itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'K') || (itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'S'))
                            itinerary.Segments[airSegIndex].FlightStatus = FlightStatus.Confirmed;
                        itinerary.Segments[airSegIndex].Airline = tempAirSegment.Carrier;
                        itinerary.Segments[airSegIndex].Origin = new Airport(tempAirSegment.Origin);
                        //itinerary.Segments[airSegIndex].Origin.AirportCode= tempAirSegment.Origin;
                        itinerary.Segments[airSegIndex].Destination= new Airport(tempAirSegment.Destination);
                        //itinerary.Segments[airSegIndex].Destination.AirportCode = tempAirSegment.Destination;

                         // Local Time
                         string depTime = tempAirSegment.DepartureTime.Substring(0,tempAirSegment.DepartureTime.IndexOf("+"));
                         string arrTime = tempAirSegment.ArrivalTime.Substring(0, tempAirSegment.ArrivalTime.IndexOf("+"));


                        itinerary.Segments[airSegIndex].DepartureTime = Convert.ToDateTime(depTime);
                        itinerary.Segments[airSegIndex].ArrivalTime = Convert.ToDateTime(arrTime);
                         // GMT Time
                        //itinerary.Segments[airSegIndex].ArrivalTime= Convert.ToDateTime(tempAirSegment.ArrivalTime);
                        //itinerary.Segments[airSegIndex].DepartureTime= Convert.ToDateTime(tempAirSegment.DepartureTime);
                        itinerary.Segments[airSegIndex].BookingClass = tempAirSegment.ClassOfService;
                        itinerary.Segments[airSegIndex].CabinClass = tempAirSegment.ClassOfService;
                        itinerary.Segments[airSegIndex].FlightNumber= tempAirSegment.FlightNumber;
                        itinerary.Segments[airSegIndex].UapiSegmentRefKey= Convert.ToInt32(tempAirSegment.Key);
                        itinerary.Segments[airSegIndex].AirlinePNR = airReservation.LocatorCode;
                        UR18.SupplierLocator[] supLocatorList = airReservation.SupplierLocator;
                        if (supLocatorList != null)
                        {
                            foreach (UR18.SupplierLocator supLocator in supLocatorList)
                            {

                                if (tempAirSegment.Carrier == supLocator.SupplierCode)
                                {
                                    itinerary.Segments[airSegIndex].AirlinePNR = supLocator.SupplierLocatorCode;

                                }
                            }
                        }



                        //UR18.typeEticketability ticketability = new UR18.typeEticketability();
                        //ticketability = tempAirSegment.ETicketability;
                        if (tempAirSegment.ETicketability == UR18.typeEticketability.Yes)
                            itinerary.Segments[airSegIndex].ETicketEligible = true;
                        else itinerary.Segments[airSegIndex].ETicketEligible = false;


                        itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
                        airSegIndex++;

                    }


                    // Air Pricing Info Details
                     bool nonRefundable = false;
                     //double totalResultFare = 0;
                     //double totalbaseFare = 0;
                     string lastTktDate = string.Empty;
                     //pricingSolution.Journey[0].
                     //int fareKey = 0;
                     decimal baseFare = 0;
                     decimal totalFare = 0;
                     //fare = new Fare[pricingSolution.AirPricingInfo.Length];

                    // Air20.BookingInfo[] bookingInfoList = pricingSolution.AirPricingInfo[0].BookingInfo;
                     //int infoIndex = 0;
                    Dictionary<string, Dictionary<string, UR18.FareInfo>> fareInfoPaxtDict = new Dictionary<string, Dictionary<string, UR18.FareInfo>>();
                     foreach (UR18.AirPricingInfo pricingInfo in airReservation.AirPricingInfo) //AirPricingInfo
                     {
                         baseFare = (decimal)getCurrAmount(pricingInfo.BasePrice);
                         totalFare = (decimal)getCurrAmount(pricingInfo.TotalPrice);
                         lastTktDate = pricingInfo.LatestTicketingTime;
                         nonRefundable = !pricingInfo.Refundable;
                         //Fare Info Details
                         Dictionary<string, UR18.FareInfo> fareInfoDict = new Dictionary<string, UR18.FareInfo>();
                         Dictionary<string, UR18.FareInfo> fareInfoSegmentDict = new Dictionary<string, UR18.FareInfo>();
                         
                         foreach (UR18.FareInfo tempFareInfo in pricingInfo.FareInfo)
                         {
                             fareInfoDict.Add(tempFareInfo.Key, tempFareInfo);
                             
                         }

                         foreach(UR18.BookingInfo tempBookingInfo in pricingInfo.BookingInfo)
                         {
                             fareInfoSegmentDict.Add(tempBookingInfo.SegmentRef,fareInfoDict[tempBookingInfo.FareInfoRef]);
                         }


                         UR18.PassengerType[] tempPassTypeList = pricingInfo.PassengerType;
                         foreach (UR18.PassengerType tempPassType in tempPassTypeList)
                         {


                             fareInfoPaxtDict.Add(tempPassType.BookingTravelerRef, fareInfoSegmentDict);
                             // Price Details
                             
                             PriceAccounts price = new PriceAccounts();
                             price.PublishedFare = baseFare;
                             price.Tax = totalFare - baseFare;

                             FlightPassenger tempPassenger = passengerDict[Convert.ToInt64(tempPassType.BookingTravelerRef)];
                             tempPassenger.Price = price;
                             // For Tax Break Up -- not yet implementd( only after Ticket)
                             List<KeyValuePair<string, decimal>> taxBreakUp = new List<KeyValuePair<string, decimal>>();
                             UR18.typeTaxInfo[] tempTaxTypeList = pricingInfo.TaxInfo;
                             if (tempTaxTypeList != null && tempTaxTypeList.Length > 0)
                             {
                                 foreach (UR18.typeTaxInfo tempTaxType in tempTaxTypeList)
                                 {
                                     string taxType = tempTaxType.Category;
                                     decimal taxValue = (decimal)getCurrAmount(tempTaxType.Amount);
                                     taxBreakUp.Add(new KeyValuePair<string, decimal>(taxType, taxValue));
                                 }
                                 taxBreakUpPax.Add(Convert.ToInt64(tempPassType.BookingTravelerRef), taxBreakUp);
                             }


                         }
                        
                     }// Air Pricing Info End


                    
                    // Checking whether ticketed or not
                    List<FareRule> fareRuleList = new List<FareRule>();
                     if (airReservation.DocumentInfo != null)
                     {
                         UR18.DocumentInfo docInfo = airReservation.DocumentInfo;
                         
                         if (docInfo.TicketInfo != null && docInfo.TicketInfo.Length > 0)
                         {
                             
                             UR18.TicketInfo[] ticketInfoList= docInfo.TicketInfo;
                             // Assiging Ticket Count
                             ticket = new Ticket[ticketInfoList.Length];
                             List<Ticket> ticketArray = new List<Ticket>();
                             int ticketIndex=0;
                             foreach (UR18.TicketInfo ticketInfo in ticketInfoList)
                             {
                                 //if(UR18.typeTicketStatus
                                 if (!string.IsNullOrEmpty(ticketInfo.Number)) // TODO replace Tic,et Number to Status, once we get status Descritption
                                 {
                                     long travelerRef = Convert.ToInt64(ticketInfo.BookingTravelerRef);
                                     Ticket tempTicket = new Ticket();


                                     tempTicket.TicketNumber = ticketInfo.Number;
                                     //tempTicket.ValidatingAriline = ticketNumNode.InnerText.Substring(0, 3);


                                     itinerary.Ticketed = true;

                                     //Assigning Pax Details
                                     FlightPassenger tempPassenger = passengerDict[travelerRef];
                                     //tempPassenger.p
                                     tempTicket.PaxFirstName = tempPassenger.FirstName;
                                     tempTicket.PaxLastName = tempPassenger.LastName;
                                     tempTicket.PaxType = tempPassenger.Type;
                                     tempTicket.Title= tempPassenger.Title;
                                     tempTicket.ETicket = true;
                                     tempTicket.IssueDate = DateTime.UtcNow;
                                     tempTicket.LastModifiedOn = itinerary.LastModifiedOn;
                                     tempTicket.PtcDetail = new List<SegmentPTCDetail>();
                                     // Adding Tax Details for Pax
                                     
                                     if(taxBreakUpPax.ContainsKey(travelerRef))
                                     {
                                         tempTicket.TaxBreakup = new List<KeyValuePair<string, decimal>>();

                                         tempTicket.TaxBreakup = taxBreakUpPax[travelerRef];
                                     }

                                     // Adding PTCDetail 
                                     
                                     for (int k = 0; k < itinerary.Segments.Length; k++)
                                     {
                                         

                                         Dictionary<string, UR18.FareInfo> fareInfoSegmentTemp= fareInfoPaxtDict[ticketInfo.BookingTravelerRef];
                                         UR18.FareInfo tempFareInfo = fareInfoSegmentTemp[Convert.ToString(itinerary.Segments[k].UapiSegmentRefKey)];
                                         // Fare Rules details
                                         if (ticketIndex == 0)
                                         {
                                             FareRule fareRule = new FareRule();
                                             fareRule.FareBasisCode = tempFareInfo.FareBasis;
                                             fareRule.Origin = itinerary.Segments[k].Origin.AirportCode;
                                             fareRule.Destination = itinerary.Segments[k].Destination.AirportCode;
                                             fareRule.Airline = itinerary.Segments[k].Airline;
                                             fareRuleList.Add(fareRule);

                                         }
                                         // Baagege Details
                                         SegmentPTCDetail ptcDtl = new SegmentPTCDetail();
                                         ptcDtl.Baggage = tempFareInfo.BaggageAllowance.MaxWeight.Value;
                                         string flightKey = string.Empty;
                                         flightKey = itinerary.Segments[k].Airline;
                                         //if (itinerary.Segments[k].FlightNumber.Length == 3)
                                         //{
                                         //    flightKey += "0" + itinerary.Segments[k].FlightNumber;
                                         //}
                                         //else if (itinerary.Segments[k].FlightNumber.Length == 4)
                                         //{
                                         //    flightKey += itinerary.Segments[k].FlightNumber;
                                         //}
                                         flightKey += itinerary.Segments[k].FlightNumber.PadLeft(4, '0');
                                         flightKey += itinerary.Segments[k].DepartureTime.ToString("ddMMMyyyyHHmm").ToUpper();
                                         ptcDtl.FlightKey = flightKey;
                                         ptcDtl.NVA = string.Empty;
                                         ptcDtl.NVB = string.Empty;
                                         //ptcDtl.FareBasis = itinerary.FareRules[k].FareBasisCode;
                                         ptcDtl.FareBasis = tempFareInfo.FareBasis;
                                         tempTicket.PtcDetail.Add(ptcDtl);
                                         
                                         //ticket[paxIndex].PtcDetail.Add(ptcDtl);
                                     }
                                     
                                     //tempTicket.PtcDetail = new List<SegmentPTCDetail>();
                                     ticketArray.Add(tempTicket);
                                 }
                                 ticketIndex++;
                             }
                             ticket = ticketArray.ToArray();
                         }
                         else
                             ticket = new Ticket[0];
                     }


                     //if (airReservation.TicketingModifiers != null && airReservation.TicketingModifiers.Length > 0)
                     //{
                     //    UR18.TicketingModifiers[] ticketModifierList = airReservation.TicketingModifiers;
                     //    foreach (UR18.TicketingModifiers ticketModifier in ticketModifierList)
                     //    {
                     //        itinerary.Ticketed = ticketModifier.DocumentSelect.IssueElectronicTicket;
                     //    }
                     //}


                    if(fareRuleList.Count>0)itinerary.FareRules = fareRuleList;
                }

            }
            // Adding Temp FLight Passenger List To Itinerary
            itinerary.Passenger = new FlightPassenger[tempPassengerList.Length];
            
            itinerary.Passenger = tempPassengerList;
            itinerary.AirLocatorCode = reservationLocatorCode;// store the air reservation code to issue the ticket

            // TO DO
            
            return itinerary;
           
                  
        }*/
        private static FlightItinerary MakeBookingObject(UR18.UniversalRecordImportRsp response, out Ticket[] ticket)
        {
            ticket = new Ticket[0];
            FlightItinerary itinerary = new FlightItinerary();
            UR18.UniversalRecord uRecord = response.UniversalRecord;
            itinerary.UniversalRecord = uRecord.LocatorCode;
            itinerary.PNR = uRecord.ProviderReservationInfo[0].LocatorCode;// TO do with multiple Airline Test
            // Passenger Information 
            UR18.BookingTraveler[] bookingTravelerList = uRecord.BookingTraveler;

            Dictionary<long, FlightPassenger> passengerDict = new Dictionary<long, FlightPassenger>();// To Stor Passnger Details
            FlightPassenger[] tempPassengerList = new FlightPassenger[bookingTravelerList.Length];
            Dictionary<long, List<KeyValuePair<string, decimal>>> taxBreakUpPax = new Dictionary<long, List<KeyValuePair<string, decimal>>>(); // To Store Pax wise Tax Break Up
            int paxIndex = 0;
            foreach (UR18.BookingTraveler bookingTraveler in bookingTravelerList)
            {
                //bookingTraveler.TravelerType
                tempPassengerList[paxIndex] = new FlightPassenger();
                if (bookingTraveler.TravelerType == "ADT" || bookingTraveler.TravelerType==null)
                    tempPassengerList[paxIndex].Type = PassengerType.Adult;
                if (bookingTraveler.TravelerType == "CNN")
                    tempPassengerList[paxIndex].Type = PassengerType.Child;
                if (bookingTraveler.TravelerType == "INF")
                    tempPassengerList[paxIndex].Type = PassengerType.Infant;
                if (bookingTraveler.TravelerType == "SRC")
                    tempPassengerList[paxIndex].Type = PassengerType.Senior;

                UR18.BookingTravelerName travelerName = new UR18.BookingTravelerName();
                travelerName = bookingTraveler.BookingTravelerName;
                tempPassengerList[paxIndex].FirstName = travelerName.First;
                tempPassengerList[paxIndex].LastName = travelerName.Last;
                tempPassengerList[paxIndex].Title = travelerName.Prefix;


                UR18.PhoneNumber[] phone = bookingTraveler.PhoneNumber;
                tempPassengerList[paxIndex].CellPhone = phone[0].Number;
                UR18.typeStructuredAddress[] adds = bookingTraveler.Address;
                if (adds != null)
                {
                    tempPassengerList[paxIndex].AddressLine1 = adds[0].AddressName;
                    if (adds[0].Street != null)
                    {
                        string[] street = adds[0].Street;
                        tempPassengerList[paxIndex].AddressLine2 = street[0].ToString();
                    }

                    if (adds[0].City != null) tempPassengerList[paxIndex].City = adds[0].City;

                    Country tempCountry = new Country();
                    tempCountry.CountryCode = adds[0].Country;
                    tempPassengerList[paxIndex].Country = tempCountry;
                }
                passengerDict.Add(Convert.ToInt64(bookingTraveler.Key), tempPassengerList[paxIndex]);
                paxIndex++;

                // Segment Information


            }
            // Air Reservation Details
            string reservationLocatorCode = string.Empty;
            UR18.AirReservation[] airReservationList = response.UniversalRecord.AirReservation;
            if (airReservationList != null)
            {

                foreach (UR18.AirReservation airReservation in airReservationList)
                {
                    reservationLocatorCode = airReservation.LocatorCode;// storing the PNR for issue the ticket for UAPI-TO do with multiple Airline Test
                    itinerary.CreatedOn = Convert.ToDateTime(airReservation.CreateDate);
                    //itinerary.Ticketed = true; TO check after Ticket
                    itinerary.FareType = "PUB";
                    itinerary.FlightBookingSource = BookingSource.UAPI;
                    itinerary.Segments = new FlightInfo[airReservation.AirSegment.Length];

                    // Air Segment Details
                    int airSegIndex = 0;
                    foreach (UR18.AirSegment tempAirSegment in airReservation.AirSegment)
                    {

                        itinerary.Segments[airSegIndex] = new FlightInfo();
                        itinerary.Segments[airSegIndex].Status = tempAirSegment.Status;
                        if ((itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'K') || (itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'S'))
                            itinerary.Segments[airSegIndex].FlightStatus = FlightStatus.Confirmed;
                        itinerary.Segments[airSegIndex].Airline = tempAirSegment.Carrier;
                        itinerary.Segments[airSegIndex].Origin = new Airport(tempAirSegment.Origin);
                        //itinerary.Segments[airSegIndex].Origin.AirportCode= tempAirSegment.Origin;
                        itinerary.Segments[airSegIndex].Destination = new Airport(tempAirSegment.Destination);
                        //itinerary.Segments[airSegIndex].Destination.AirportCode = tempAirSegment.Destination;

                        // Local Time
                        string depTime = tempAirSegment.DepartureTime.Substring(0, tempAirSegment.DepartureTime.IndexOf("+"));
                        string arrTime = tempAirSegment.ArrivalTime.Substring(0, tempAirSegment.ArrivalTime.IndexOf("+"));


                        itinerary.Segments[airSegIndex].DepartureTime = Convert.ToDateTime(depTime);
                        itinerary.Segments[airSegIndex].ArrivalTime = Convert.ToDateTime(arrTime);
                        // GMT Time
                        //itinerary.Segments[airSegIndex].ArrivalTime= Convert.ToDateTime(tempAirSegment.ArrivalTime);
                        //itinerary.Segments[airSegIndex].DepartureTime= Convert.ToDateTime(tempAirSegment.DepartureTime);
                        itinerary.Segments[airSegIndex].BookingClass = tempAirSegment.ClassOfService;
                        itinerary.Segments[airSegIndex].CabinClass = tempAirSegment.ClassOfService;
                        itinerary.Segments[airSegIndex].FlightNumber = tempAirSegment.FlightNumber;
                        itinerary.Segments[airSegIndex].UapiSegmentRefKey = Convert.ToInt32(tempAirSegment.Key);
                        itinerary.Segments[airSegIndex].AirlinePNR = airReservation.LocatorCode;
                        UR18.SupplierLocator[] supLocatorList = airReservation.SupplierLocator;
                        if (supLocatorList != null)
                        {
                            foreach (UR18.SupplierLocator supLocator in supLocatorList)
                            {

                                if (tempAirSegment.Carrier == supLocator.SupplierCode)
                                {
                                    itinerary.Segments[airSegIndex].AirlinePNR = supLocator.SupplierLocatorCode;

                                }
                            }
                        }



                        //UR18.typeEticketability ticketability = new UR18.typeEticketability();
                        //ticketability = tempAirSegment.ETicketability;
                        if (tempAirSegment.ETicketability == UR18.typeEticketability.Yes)
                            itinerary.Segments[airSegIndex].ETicketEligible = true;
                        else itinerary.Segments[airSegIndex].ETicketEligible = false;


                        itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
                        airSegIndex++;

                    }


                    // Air Pricing Info Details
                    bool nonRefundable = false;
                    //double totalResultFare = 0;
                    //double totalbaseFare = 0;
                    string lastTktDate = string.Empty;
                    //pricingSolution.Journey[0].
                    //int fareKey = 0;
                    decimal baseFare = 0;
                    decimal totalFare = 0;
                    //fare = new Fare[pricingSolution.AirPricingInfo.Length];

                    // Air20.BookingInfo[] bookingInfoList = pricingSolution.AirPricingInfo[0].BookingInfo;
                    //int infoIndex = 0;
                    Dictionary<string, Dictionary<string, UR18.FareInfo>> fareInfoPaxtDict = new Dictionary<string, Dictionary<string, UR18.FareInfo>>();
                    if (airReservation.AirPricingInfo == null)
                    {
                        throw new Exception("Pricing Info is not assigned to this PNR !");
                    }
                    foreach (UR18.AirPricingInfo pricingInfo in airReservation.AirPricingInfo) //AirPricingInfo
                    {
                        baseFare = (decimal)getCurrAmount(pricingInfo.BasePrice);
                        totalFare = (decimal)getCurrAmount(pricingInfo.TotalPrice);
                        lastTktDate = pricingInfo.LatestTicketingTime;
                        nonRefundable = !pricingInfo.Refundable;
                        //Fare Info Details
                        Dictionary<string, UR18.FareInfo> fareInfoDict = new Dictionary<string, UR18.FareInfo>();
                        Dictionary<string, UR18.FareInfo> fareInfoSegmentDict = new Dictionary<string, UR18.FareInfo>();

                        foreach (UR18.FareInfo tempFareInfo in pricingInfo.FareInfo)
                        {
                            fareInfoDict.Add(tempFareInfo.Key, tempFareInfo);

                        }

                        foreach (UR18.BookingInfo tempBookingInfo in pricingInfo.BookingInfo)
                        {
                            fareInfoSegmentDict.Add(tempBookingInfo.SegmentRef, fareInfoDict[tempBookingInfo.FareInfoRef]);
                        }


                        UR18.PassengerType[] tempPassTypeList = pricingInfo.PassengerType;
                        foreach (UR18.PassengerType tempPassType in tempPassTypeList)
                        {


                            fareInfoPaxtDict.Add(tempPassType.BookingTravelerRef, fareInfoSegmentDict);
                            // Price Details

                            PriceAccounts price = new PriceAccounts();
                            price.PublishedFare = baseFare;
                            price.Tax = totalFare - baseFare;

                            FlightPassenger tempPassenger = passengerDict[Convert.ToInt64(tempPassType.BookingTravelerRef)];
                            tempPassenger.Price = price;
                            // For Tax Break Up -- not yet implementd( only after Ticket)
                            List<KeyValuePair<string, decimal>> taxBreakUp = new List<KeyValuePair<string, decimal>>();
                            UR18.typeTaxInfo[] tempTaxTypeList = pricingInfo.TaxInfo;
                            if (tempTaxTypeList != null && tempTaxTypeList.Length > 0)
                            {
                                foreach (UR18.typeTaxInfo tempTaxType in tempTaxTypeList)
                                {
                                    string taxType = tempTaxType.Category;
                                    decimal taxValue = (decimal)getCurrAmount(tempTaxType.Amount);
                                    taxBreakUp.Add(new KeyValuePair<string, decimal>(taxType, taxValue));
                                }
                                taxBreakUpPax.Add(Convert.ToInt64(tempPassType.BookingTravelerRef), taxBreakUp);
                            }


                        }

                    }// Air Pricing Info End



                    // Checking whether ticketed or not
                    List<FareRule> fareRuleList = new List<FareRule>();
                    if (airReservation.DocumentInfo != null)
                    {
                        UR18.DocumentInfo docInfo = airReservation.DocumentInfo;

                        if (docInfo.TicketInfo != null && docInfo.TicketInfo.Length > 0)
                        {

                            UR18.TicketInfo[] ticketInfoList = docInfo.TicketInfo;
                            // Assiging Ticket Count
                            ticket = new Ticket[ticketInfoList.Length];
                            List<Ticket> ticketArray = new List<Ticket>();
                            int ticketIndex = 0;
                            foreach (UR18.TicketInfo ticketInfo in ticketInfoList)
                            {
                                //if(UR18.typeTicketStatus
                                if (!string.IsNullOrEmpty(ticketInfo.Number)) // TODO replace Tic,et Number to Status, once we get status Descritption
                                {
                                    long travelerRef = Convert.ToInt64(ticketInfo.BookingTravelerRef);
                                    Ticket tempTicket = new Ticket();


                                    tempTicket.TicketNumber = ticketInfo.Number;
                                    //tempTicket.ValidatingAriline = ticketNumNode.InnerText.Substring(0, 3);


                                    itinerary.Ticketed = true;

                                    //Assigning Pax Details
                                    FlightPassenger tempPassenger = passengerDict[travelerRef];
                                    //tempPassenger.p
                                    tempTicket.PaxFirstName = tempPassenger.FirstName;
                                    tempTicket.PaxLastName = tempPassenger.LastName;
                                    tempTicket.PaxType = tempPassenger.Type;
                                    tempTicket.Title = tempPassenger.Title;
                                    tempTicket.ETicket = true;
                                    tempTicket.IssueDate = DateTime.UtcNow;
                                    tempTicket.LastModifiedOn = itinerary.LastModifiedOn;
                                    tempTicket.PtcDetail = new List<SegmentPTCDetail>();
                                    // Adding Tax Details for Pax

                                    if (taxBreakUpPax.ContainsKey(travelerRef))
                                    {
                                        tempTicket.TaxBreakup = new List<KeyValuePair<string, decimal>>();

                                        tempTicket.TaxBreakup = taxBreakUpPax[travelerRef];
                                    }

                                    // Adding PTCDetail 

                                    for (int k = 0; k < itinerary.Segments.Length; k++)
                                    {


                                        Dictionary<string, UR18.FareInfo> fareInfoSegmentTemp = fareInfoPaxtDict[ticketInfo.BookingTravelerRef];
                                        UR18.FareInfo tempFareInfo = fareInfoSegmentTemp[Convert.ToString(itinerary.Segments[k].UapiSegmentRefKey)];
                                        // Fare Rules details
                                        if (ticketIndex == 0)
                                        {
                                            FareRule fareRule = new FareRule();
                                            fareRule.FareBasisCode = tempFareInfo.FareBasis;
                                            fareRule.Origin = itinerary.Segments[k].Origin.AirportCode;
                                            fareRule.Destination = itinerary.Segments[k].Destination.AirportCode;
                                            fareRule.Airline = itinerary.Segments[k].Airline;
                                            fareRuleList.Add(fareRule);

                                        }
                                        // Baagege Details
                                        SegmentPTCDetail ptcDtl = new SegmentPTCDetail();
                                        ptcDtl.Baggage = tempFareInfo.BaggageAllowance.MaxWeight.Value;
                                        string flightKey = string.Empty;
                                        flightKey = itinerary.Segments[k].Airline;
                                        //if (itinerary.Segments[k].FlightNumber.Length == 3)
                                        //{
                                        //    flightKey += "0" + itinerary.Segments[k].FlightNumber;
                                        //}
                                        //else if (itinerary.Segments[k].FlightNumber.Length == 4)
                                        //{
                                        //    flightKey += itinerary.Segments[k].FlightNumber;
                                        //}
                                        flightKey += itinerary.Segments[k].FlightNumber.PadLeft(4, '0');
                                        flightKey += itinerary.Segments[k].DepartureTime.ToString("ddMMMyyyyHHmm").ToUpper();
                                        ptcDtl.FlightKey = flightKey;
                                        ptcDtl.NVA = string.Empty;
                                        ptcDtl.NVB = string.Empty;
                                        //ptcDtl.FareBasis = itinerary.FareRules[k].FareBasisCode;
                                        ptcDtl.FareBasis = tempFareInfo.FareBasis;
                                        tempTicket.PtcDetail.Add(ptcDtl);

                                        //ticket[paxIndex].PtcDetail.Add(ptcDtl);
                                    }

                                    //tempTicket.PtcDetail = new List<SegmentPTCDetail>();
                                    ticketArray.Add(tempTicket);
                                }
                                ticketIndex++;
                            }
                            ticket = ticketArray.ToArray();
                        }
                        else
                            ticket = new Ticket[0];
                    }


                    //if (airReservation.TicketingModifiers != null && airReservation.TicketingModifiers.Length > 0)
                    //{
                    //    UR18.TicketingModifiers[] ticketModifierList = airReservation.TicketingModifiers;
                    //    foreach (UR18.TicketingModifiers ticketModifier in ticketModifierList)
                    //    {
                    //        itinerary.Ticketed = ticketModifier.DocumentSelect.IssueElectronicTicket;
                    //    }
                    //}


                    if (fareRuleList.Count > 0) itinerary.FareRules = fareRuleList;
                }

            }
            // Adding Temp FLight Passenger List To Itinerary
            itinerary.Passenger = new FlightPassenger[tempPassengerList.Length];

            itinerary.Passenger = tempPassengerList;
            itinerary.AirLocatorCode = reservationLocatorCode;// store the air reservation code to issue the ticket
            // TO DO

            return itinerary;


        }
        # endregion

        # region Ticket
        public static TicketingResponse Ticket(FlightItinerary itineraryDB, int  member, Dictionary<string, string> ticketData, string ipAddr)
        {
            TicketingResponse response = new TicketingResponse();
            bool isDomestic = itineraryDB.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
            //bool isYatra = false;
            //bool isVia = false;
            //Connection con = new Connection();
            string hap = string.Empty;
            string username = string.Empty;
            string password = string.Empty;
            if (isDomestic)
            {
                hap = Configuration.ConfigurationSystem.GalileoConfig["DomesticHAP"].ToString();
            }
            else
            {
                hap = Configuration.ConfigurationSystem.GalileoConfig["InternationalHAP"].ToString();
            }
            //AirlineHAP airHap = new AirlineHAP();
            //airHap.Load(itineraryDB.ValidatingAirline, "1G");
            //if (isDomestic)
            //{
            //    //if (airHap.DomesticHAP != null && airHap.DomesticHAP.Length > 0)
            //    //{
            //    //    string supName = Supplier.GetSupplierNameById(airHap.SupplierId);
            //    //    if (supName.ToUpper().Contains("YATRA"))
            //    //    {
            //    //        isYatra = true;
            //    //    }
            //    //    else
            //    //    {
            //    //        if (supName.ToUpper().Contains("VIA"))
            //    //        {
            //    //            isVia = true;
            //    //            username = Configuration.ConfigurationSystem.GalileoConfig["ViaUsername"].ToString();
            //    //            password = Configuration.ConfigurationSystem.GalileoConfig["ViaPassword"].ToString();
            //    //        }
            //    //    }
            //    //    hap = airHap.DomesticHAP;
            //    //    string rtResponse = con.SubmitTerminalTransaction("*" + itineraryDB.PNR);
            //    //    rtResponse = con.SubmitTerminalTransaction("QEB/" + airHap.DomesticPCC + "/66+67");
            //    //    con.EndSession();
            //    //    Audit.Add(EventType.Ticketing, Severity.Normal, 0, "Queueing XML 1G. req : QEB/" + airHap.DomesticPCC + "/66+67" + rtResponse, string.Empty);
            //    //}
            //}
            //else
            //{
            //    if (airHap.InternationalHAP != null && airHap.InternationalHAP.Length > 0)
            //    {
            //        //string supName = Supplier.GetSupplierNameById(airHap.SupplierId);
            //        //if (supName.ToUpper().Contains("YATRA"))
            //        //{
            //        //    isYatra = true;
            //        //}
            //        //hap = airHap.InternationalHAP;
            //        //string rtResponse = con.SubmitTerminalTransaction("*" + itineraryDB.PNR);
            //        //rtResponse = con.SubmitTerminalTransaction("QEB/" + airHap.InternationalPCC + "/66+67");
            //        //con.EndSession();
            //        //Audit.Add(EventType.Ticketing, Severity.Normal, 0, "Queueing XML 1G. req : QEB/" + airHap.InternationalPCC + "/66+67" + rtResponse, string.Empty);
            //    }
            //}


            string endorsement = string.Empty;
            string tourCode = string.Empty;
            string corporateCode = string.Empty;
            if (ticketData != null && ticketData.ContainsKey("corporateCode") && ticketData["corporateCode"].Length > 0)
            {
                corporateCode = ticketData["corporateCode"];
            }
            if (itineraryDB.Endorsement != null && itineraryDB.Endorsement.Length > 0)
            {
                endorsement = itineraryDB.Endorsement;
            }
            else if (ticketData != null && ticketData.ContainsKey("endorsement") && ticketData["endorsement"].Length > 0)
            {
                endorsement = ticketData["endorsement"];
            }
            if (corporateCode != null && corporateCode.Trim().Length > 0)
            {
                endorsement += " " + corporateCode.Trim();
            }
            if (itineraryDB.TourCode != null && itineraryDB.TourCode.Length > 0)
            {
                tourCode = itineraryDB.TourCode;
            }
            else if (ticketData != null && ticketData.ContainsKey("tourCode") && ticketData["tourCode"].Length > 0)
            {
                tourCode = ticketData["tourCode"];
            }
            //try //RetrieveItinerary is  Already calling in Metasearch.TicketUAPI.
            //{
            //    Audit.Add(EventType.Ticketing, Severity.Normal, 0, "Retrieve Before Ticket XML UAPI. ", string.Empty);
            //    FlightItinerary retrievItinerary = RetrieveItinerary(itineraryDB.UniversalRecord);
            //}
            //catch
            //{
            //    Audit.Add(EventType.Ticketing, Severity.High, 0, "UAPI Retrieve PNR Failed before Ticketing . Execption. ", string.Empty);
            //    throw;
            //}


            Air20.ResponseMessage responseMessage = null;
            try
            {


                //Air20.AirTicketingRsp ticketingRsp = GenerateETicketMessage(itineraryDB.PNR);
                Air20.AirTicketingRsp ticketingRsp = GenerateETicketMessage(itineraryDB.AirLocatorCode);// in UAPI to issue the ticket, should pass the airreservationlocatorcode
                if (ticketingRsp.TicketFailureInfo != null && ticketingRsp.TicketFailureInfo.Length > 0)
                {
                    response.PNR = itineraryDB.PNR;
                    response.Status = TicketingResponseStatus.OtherError;
                    
                    string erroMessage = ticketingRsp.TicketFailureInfo[0].Message;
                    response.Message = erroMessage;
                    throw new Exception(erroMessage);
                    //Audit.Add(EventType.Ticketing, Severity.High, 0, "UAPI Ticketing Failed. Execption :- " + erroMessage, string.Empty);

                }

                responseMessage = ticketingRsp.ResponseMessage[0];
                //eticketResponse = connection.SubmitXmlOnSession(request);
                Audit.Add(EventType.Ticketing, Severity.Normal, 0, "UAPI Ticket Response Message: " + responseMessage.Value , string.Empty);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Ticketing, Severity.High, 0, "UAPI Ticketing Failed. Execption :- " + ex.Message, string.Empty);
            }
            string ticketSuccessPattern = "ELECTRONIC TKT GENERATED";
            //Match match = Regex.Match(responseMessage.Value, ticketSuccessPattern);
            Match match = Regex.Match((responseMessage != null && !string.IsNullOrEmpty(responseMessage.Value)) ? responseMessage.Value : "", ticketSuccessPattern);
            if (match.Success)
            {
                response.Status = TicketingResponseStatus.Successful;
                response.PNR = itineraryDB.PNR;
            }
            else
            {
                response.Status = TicketingResponseStatus.NotCreated;
                //response.Message = "Ticketing Failed";
            }
            return response;
        }

        private static Air20.AirTicketingRsp GenerateETicketMessage(string pnr)
        {
            Connection();
            Trace.TraceInformation("UApi.GenerateETicket entered");

            Air20.AirTicketingReq request = new Air20.AirTicketingReq();

            request.TargetBranch = targetBranch;
            request.ReturnInfoOnFail = true;
            request.BulkTicket = false;
            request.AuthorizedBy = "ONLINE";

            Air20.BillingPointOfSaleInfo pos = new Air20.BillingPointOfSaleInfo();
            pos.OriginApplication = "UAPI";
            request.BillingPointOfSaleInfo = pos;


            Air20.AirReservationLocatorCode locatorCode = new Air20.AirReservationLocatorCode();
            locatorCode.Value = pnr;

            request.AirReservationLocatorCode = locatorCode;

            Air20.AirTicketingBinding binding = new Air20.AirTicketingBinding();
            binding.Url = urlAir;
            binding.Credentials = new NetworkCredential(userName, password);

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(request.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, request); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirTicketingReq.xml");
            }
            Air20.AirTicketingRsp response = null;
            try
            {
                response = binding.service(request);

            }
            catch { throw; }
            //{
            //    throw ex;
            //}

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(response.GetType());
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, response); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docres = new XmlDocument();
                docres.LoadXml(sbRes.ToString());
                docres.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirTicketingRes.xml");
            }
            Trace.TraceInformation("UApi.GenerateRetrievePNRObject exiting");
            return response;
        }
        # endregion

        # region Import UR 
        private static UR18.UniversalRecordImportRsp ImportUR(string pnr)
        {
            Connection();
            Trace.TraceInformation("UApi.ImportUR entered");
            //UR18.UniversalRecordRetrieveReq request = new UR18.UniversalRecordRetrieveReq();

            //request.TargetBranch = targetBranch;
            //request.UniversalRecordLocatorCode = pnr;

            //UR18.BillingPointOfSaleInfo pos = new UR18.BillingPointOfSaleInfo();
            //pos.OriginApplication = originalApplication;
            //request.BillingPointOfSaleInfo = pos;

            //UR18.UniversalRecordRetrieveServiceBinding binding = new UR18.UniversalRecordRetrieveServiceBinding();
            //binding.Url = urlUR;
            //binding.Credentials = new NetworkCredential(userName, password);


            UR18.UniversalRecordImportReq request = new UR18.UniversalRecordImportReq();

            request.TargetBranch = targetBranch;
            //request.UniversalRecordLocatorCode = pnr;

            request.ProviderCode = "1G";// TODO-- add Dynamic Provider
            request.ProviderLocatorCode = pnr;


            UR18.BillingPointOfSaleInfo pos = new UR18.BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            request.BillingPointOfSaleInfo = pos;

            UR18.UniversalRecordImportServiceBinding binding = new UR18.UniversalRecordImportServiceBinding();
            binding.Url = urlUR;
            binding.Credentials = new NetworkCredential(userName, password);



            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(request.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, request); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\URImportReq.xml");
            }

            UR18.UniversalRecordImportRsp response = null;
            
            try
            {
                response = binding.service(request);

            }
            catch { throw; }
            //{
            //    throw ex;
            //}

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(response.GetType());
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, response); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docres = new XmlDocument();
                docres.LoadXml(sbRes.ToString());
                docres.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\URimportRes.xml");
            }
            Trace.TraceInformation("UApi.ImportUR exiting");
            return response;
        }
        # endregion

        # region common Methods

        private static int keyGen(string keyValue)
        {
            int key = 0;
            string newKey = keyValue.Substring(0,keyValue.Length-1 );
            key = Convert.ToInt32(newKey);
            return key;


            //int key=
        }
        private static double getCurrAmount(string keyValue)
        {
            double amount = 0;
            string newAmont = keyValue.Remove(0, 3);
            amount= Convert.ToDouble(newAmont);
            return amount;
        }

        public static List<KeyValuePair<string, SSR>> GenerateSSRPaxList(FlightItinerary itinerary)
        {
            Trace.TraceInformation("UAPI.GenerateSSR entered");
            List<KeyValuePair<string, SSR>> ssrPaxList = new List<KeyValuePair<string, SSR>>();
            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                //if (itinerary.Passenger[i].Meal.Code != null || itinerary.Passenger[i].Meal.Code == string.Empty)
                //{
                //    //ssrList.Add("3SAN" + (i + 1) + itinerary.Passenger[i].Meal.Code);
                //    SSR ssr = new SSR();
                //    ssr.PaxId = itinerary.Passenger[i].PaxId;
                //    ssr.SsrCode = "MEAL";
                //    ssr.Detail = itinerary.Passenger[i].Meal.Code;
                //    KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                //    ssrPaxList.Add(ssrPax);
                //}
                //if ((itinerary.Passenger[i].FFAirline != null && itinerary.Passenger[i].FFAirline.Length > 0) && (itinerary.Passenger[i].FFNumber != null && itinerary.Passenger[i].FFNumber.Length > 0))
                //{//3SSRFQTVUAHK/UA123456382-1
                //    //ssrList.Add("3SSRFQTV" + itinerary.Passenger[i].FFAirline.ToUpper() + "HK/" + itinerary.Passenger[i].FFAirline + itinerary.Passenger[i].FFNumber + "-" + (i + 1));
                //    SSR ssr = new SSR();
                //    ssr.PaxId = itinerary.Passenger[i].PaxId;
                //    ssr.SsrCode = "FQTV";
                //    ssr.Detail = itinerary.Passenger[i].FFAirline + itinerary.Passenger[i].FFNumber;
                //    KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                //    ssrPaxList.Add(ssrPax);
                //}
                if ((itinerary.Passenger[i].Type == PassengerType.Child || itinerary.Passenger[i].Type == PassengerType.Infant) && (itinerary.Passenger[i].DateOfBirth != null && itinerary.Passenger[i].DateOfBirth > new DateTime()))
                {
                    
                    SSR ssr = new SSR();
                    ssr.PaxId = itinerary.Passenger[i].PaxId;
                    if (itinerary.Passenger[i].Type == PassengerType.Child)
                    {
                        ssr.SsrCode = "CHLD";
                    }
                    else
                    {
                        ssr.SsrCode = "INFT";
                    }
                    ssr.Detail = itinerary.Passenger[i].LastName + "/" + itinerary.Passenger[i].FirstName +""+itinerary.Passenger[i].Title +" " + itinerary.Passenger[i].DateOfBirth.ToString("dd/MM/yyyy");
                    KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                    ssrPaxList.Add(ssrPax);
                }
                if ((itinerary.Passenger[i].PassportNo != null && itinerary.Passenger[i].PassportNo.Length > 0) && (itinerary.Passenger[i].Country.CountryCode != null && itinerary.Passenger[i].Country.CountryCode.Length > 0))
                {
                    
                    SSR ssr = new SSR();
                    ssr.PaxId = itinerary.Passenger[i].PaxId;
                    ssr.SsrCode = "DOCS";// passport info
                    //ssr.Detail = itinerary.Passenger[i].PassportNo + "-" + itinerary.Passenger[i].Country.CountryCode;
                    //string detail = string.Format("P/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}", itinerary.Passenger[i].Country.CountryCode, itinerary.Passenger[i].PassportNo, itinerary.Passenger[i].Country.CountryCode,
                    //itinerary.Passenger[i].DateOfBirth.ToString("ddMMMyy"),itinerary.Passenger[i].Gender==Gender.Male?"M":"F",itinerary.Passenger[i].PassportExpiry.ToString("ddMMMyy"),itinerary.Passenger[i].FirstName+" "+itinerary.Passenger[i].LastName,itinerary.Passenger[i].FirstName+" "+itinerary.Passenger[i].LastName);

                    string detail = string.Format("P/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}", itinerary.Passenger[i].Country.CountryCode, itinerary.Passenger[i].PassportNo, itinerary.Passenger[i].Country.CountryCode,
                   itinerary.Passenger[i].DateOfBirth.ToString("ddMMMyy"), itinerary.Passenger[i].Gender == Gender.Male ? "M" : "F", itinerary.Passenger[i].PassportExpiry.ToString("ddMMMyy"), itinerary.Passenger[i].FirstName , itinerary.Passenger[i].LastName);
                    ssr.Detail = detail;
                    KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                    ssrPaxList.Add(ssrPax);
                }

            }
            Trace.TraceInformation("UAPI.GenerateSSR exiting");
            return ssrPaxList;
        }
        # endregion


    }
}
