﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Xml;
using System.Configuration;
using System.Net;
using System.Text.RegularExpressions;
using Technology.BookingEngine;
using Technology.Configuration;
using CoreLogic;
using Air20;

namespace Technology.BookingEngine.GDS
{

    public class UAPI
    {
        #region Credential
        static string userName = string.Empty;
        static string password = string.Empty;
        static string urlAir = string.Empty;
        static string urlUR = string.Empty;
        static string targetBranch = string.Empty;
        static string originalApplication = "UAPI";
        # endregion

        # region Constructor

        public UAPI()
        {

            Connection();
        }
        
        public static void Connection()
        {
            userName = "Universal API/uAPI7997403705-43d54fd1";
            password = "x-5Y6P}c!4";
            urlAir = "https://emea.copy-webservices.travelport.com/B2BGateway/connect/uAPI/AirService";
            urlUR = "https://emea.copy-webservices.travelport.com/B2BGateway/connect/uAPI/UniversalRecordService";
            targetBranch = "P7005785";

        }
        # endregion

        # region Search
        public static SearchResult[] Search(SearchRequest request, string sessionId)
        {
            
            Trace.TraceInformation("UAPI.Search() entered.");
            List<SearchResult> finalResult = new List<SearchResult>();
            SearchResult[] result = new SearchResult[0];
            SearchResult[] icSearchResult = new SearchResult[0];
            bool isDomestic = true;
            List<int> flightNotFound = new List<int>();
            //Dictionary<int, List<FlightInfo>> outboundflightList = new Dictionary<int, List<FlightInfo>>();
            //Dictionary<int, List<FlightInfo>> inboundflightList = new Dictionary<int, List<FlightInfo>>();

            
            isDomestic = false;
            //Connection connection = new Connection(isDomestic);
            result = LowFareSearch(request, sessionId );
            finalResult.AddRange(result);

            Trace.TraceInformation("UAPI.Search() exiting.");
            Basket.FlightBookingSession[sessionId].Log.Add("UAPI Search complete. Result count = " + finalResult.Count.ToString());
            return finalResult.ToArray();
        }
        private static SearchResult[] LowFareSearch(SearchRequest request, string sessionId)
        {
            //Dictionary<int, List<FlightInfo>> outboundflightList = new Dictionary<int, List<FlightInfo>>();
            //Dictionary<int, List<FlightInfo>> inboundflightList = new Dictionary<int, List<FlightInfo>>();
            //List<FlightInfo> outbound = new List<FlightInfo>();
            //List<FlightInfo> inbound = new List<FlightInfo>();

            Dictionary<int, FlightInfo> outbound = new Dictionary<int, FlightInfo>();
            Dictionary<int, FlightInfo> inbound = new Dictionary<int, FlightInfo>();

            SearchResult[] result = new SearchResult[0];
            //NetworkCredential nCredential=new NetworkCredential(this
            LowFareSearchRsp lfResponse = GenerateSearch(request);

            
            //Basket.FlightBookingSession[sessionId].XmlMessage.Add(searchXml);


            if (request != null)
            {
                ReadFlightInfoResponse(lfResponse, ref outbound, ref inbound);
                if (outbound.Count > 0)
                {
                    Audit.Add(EventType.Search, Severity.Normal, 0, "Search XML UAPI. req : 'test' resp : 'testrsp' ", string.Empty);
                    result = ReadFareResponse(lfResponse, request, outbound, inbound);
                }

            }
            //if (searchXml.Length > 0)
            //{
            //    string responseXml = connection.MultiSubmitXml(searchXml);
            //    //Basket.FlightBookingSession[sessionId].XmlMessage.Add(responseXml);
            //    Audit.Add(EventType.Search, Severity.Normal, 0, "Search XML 1G. req : " + searchXml + " resp : " + responseXml, string.Empty);
            //    ReadFlightInfoResponse(responseXml, request, ref outboundflightList, ref inboundflightList);
            //    if (outboundflightList.Count > 0)
            //    {
            //        result = ReadFareResponse(responseXml, request, outboundflightList, inboundflightList);
            //    }
            //}
            return result;
        }
        private static LowFareSearchRsp GenerateSearch(SearchRequest request)
        {
            
            Trace.TraceInformation("UAPI.GenerateSearchMessage entered");
            bool isDomestic = true;
            if ((request.AdultCount + request.ChildCount + request.SeniorCount + request.InfantCount) > 9)
            {
                throw new BookingEngineException("Total number of passengers should not exceed 9");
            }
            if (request.InfantCount > (request.AdultCount + request.SeniorCount))
            {
                throw new BookingEngineException("Number of infants should not exceed the number of Adult and Senior");
            }
            int paxCount = request.AdultCount + request.ChildCount + request.SeniorCount;

            Connection();
            LowFareSearchReq lfRequest = new LowFareSearchReq();
            lfRequest.TargetBranch = targetBranch;

            BillingPointOfSaleInfo pos = new BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            lfRequest.BillingPointOfSaleInfo = pos;

            AirSearchModifiers airModifiers = new AirSearchModifiers();
            lfRequest.AirSearchModifiers = airModifiers;
            SearchPassenger[] passAdult = new SearchPassenger[paxCount+1];
            int paxIncrement = 0;
            for (int i = 0; i < request.AdultCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "ADT";
                paxIncrement++;

            }
            for (int i = 0; i < request.ChildCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "CNN";
                paxIncrement++;

            }
            for (int i = 0; i < request.SeniorCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "SRC";
                paxIncrement++;

            }
            for (int i = 0; i < request.InfantCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "INF";
                paxIncrement++;

            }

            lfRequest.SearchPassenger = passAdult;

            lfRequest.SearchAirLeg = new SearchAirLeg[2];
            lfRequest.SearchAirLeg[0] = new SearchAirLeg();
            SearchAirLeg leg = lfRequest.SearchAirLeg[0];

            Air20.Airport origin = new Air20.Airport();
            origin.Code = request.Segments[0].Origin;

            Air20.Airport destination = new Air20.Airport();
            destination.Code = request.Segments[0].Destination;

            leg.SearchOrigin = new typeSearchLocation[1];
            leg.SearchOrigin[0] = new typeSearchLocation();
            leg.SearchOrigin[0].Item= origin;

            leg.SearchDestination = new typeSearchLocation[1];
            leg.SearchDestination[0] = new typeSearchLocation();
            leg.SearchDestination[0].Item= destination;

            leg.Items = new typeFlexibleTimeSpec[1];
            leg.Items[0] = new typeFlexibleTimeSpec();
            leg.Items[0].PreferredTime = request.Segments[0].PreferredDepartureTime.ToString("yyyy-MM-dd");
            
            
       // Return Leg
           if (request.Type == SearchType.Return)
            {
                lfRequest.SearchAirLeg[1] = new SearchAirLeg();
                SearchAirLeg legReturn = lfRequest.SearchAirLeg[1];

                Air20.Airport retOrigin = new Air20.Airport();
                retOrigin.Code = request.Segments[0].Destination;

                Air20.Airport retDestination = new Air20.Airport();
                retDestination.Code = request.Segments[0].Origin;

                legReturn.SearchOrigin = new typeSearchLocation[1];
                legReturn.SearchOrigin[0] = new typeSearchLocation();
                legReturn.SearchOrigin[0].Item = retOrigin;

                legReturn.SearchDestination = new typeSearchLocation[1];
                legReturn.SearchDestination[0] = new typeSearchLocation();
                legReturn.SearchDestination[0].Item = retDestination;


                legReturn.Items = new typeFlexibleTimeSpec[1];
                legReturn.Items[0] = new typeFlexibleTimeSpec();
                legReturn.Items[0].PreferredTime = request.Segments[1].PreferredDepartureTime.ToString("yyyy-MM-dd");

            }

            typeCabinClass cabinType= typeCabinClass.Economy;
            switch (request.Segments[0].flightCabinClass)
            {
                case CabinClass.Business:
                    cabinType=typeCabinClass.Business;
                    break;
                case CabinClass.Economy:
                    cabinType=typeCabinClass.Economy;
                    break;
                case CabinClass.First:
                    cabinType=typeCabinClass.First;
                    break;
            }



            AirLegModifiers legModifiers = new AirLegModifiers();
            AirLegModifiersPreferredCabins prefCabin = new AirLegModifiersPreferredCabins();
            Air20.CabinClass airCabin = new Air20.CabinClass();
            airCabin.Type = cabinType;
            prefCabin.CabinClass = airCabin;
            legModifiers.PreferredCabins = new AirLegModifiersPreferredCabins();
            legModifiers.PreferredCabins = prefCabin;
            lfRequest.SearchAirLeg[0].AirLegModifiers = legModifiers;



            //AirLowFareSearchAsynchBinding binding = new AirLowFareSearchAsynchBinding();
            AirLowFareSearchBinding binding = new AirLowFareSearchBinding();


            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(lfRequest.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            ser.Serialize(writer, lfRequest); 	// Here Classes are converted to XML String. 
            // This can be viewed in SB or writer.
            // Above XML in SB can be loaded in XmlDocument object
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());
            doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\LFSearchReq.xml");

            binding.Url = urlAir;
            binding.Credentials = new NetworkCredential(userName, password);

            LowFareSearchRsp lfResponse = null;
        
            lfResponse = binding.service(lfRequest);
            return lfResponse;
        }
        //private static void ReadFlightInfoResponse(LowFareSearchRsp lfResponse, ref List<FlightInfo> outboundflightList, ref List<FlightInfo> inboundflightList)
        private static void ReadFlightInfoResponse(LowFareSearchRsp lfResponse, ref Dictionary<int, FlightInfo> outboundflightList, ref Dictionary<int, FlightInfo> inboundflightList)
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(lfResponse.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            ser.Serialize(writer, lfResponse); 	// Here Classes are converted to XML String. 
            // This can be viewed in SB or writer.
            // Above XML in SB can be loaded in XmlDocument object
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());
            doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\LFSearchRes.xml");


            //List<FlightInfo> outBound = new List<FlightInfo>();
            //List<FlightInfo> inBound = new List<FlightInfo>();
            outboundflightList = new Dictionary<int, FlightInfo>() ;
            inboundflightList = new Dictionary<int, FlightInfo>() ;
            bool isError = false;
            //ResponseMessage[] lfMessage=lfResponse.ResponseMessage;
            //for (int x = 0; x < lfMessage.Length; x++)
            //{
            //    if (lfMessage[x].Type == ResponseMessageType.Error)
            //    {isError = true;break;}
            //}

            FlightDetails[] lfFlightDetails=lfResponse.FlightDetailsList;
            AirSegment[] lfAirSegment=lfResponse.AirSegmentList;

            if(!isError)
            {
                int segGroup=0;
                for (int x = 0; x < lfAirSegment.Length; x++)
                {
                    

                    segGroup=lfAirSegment[x].Group;
                    FlightInfo flight = new FlightInfo();
                    int segmentKey =keyGen(lfAirSegment[x].Key);
                    int flightKey =0;
                    flight.UapiSegmentRefKey= segmentKey;
                    

                    FlightDetails lfFlight = null;
                    
                    for (int i = 0; i < lfFlightDetails.Length; i++)
                    {
                        int tempflightKey = keyGen(lfFlightDetails[i].Key);
                        //FlightDetails[] lfFlightLit = lfFlightDetails[0].Key;
                        //for (int f = 0; f < lfFlightDetails; f++)
                        //{

                        //}
                        if (tempflightKey == keyGen(lfAirSegment[x].FlightDetailsRef[0].Key))
                        {
                          //  lfFlight=lfAirSegment[x].
                            flightKey = keyGen(lfAirSegment[x].FlightDetailsRef[0].Key);
                            lfFlight = lfFlightDetails[i];
                            break;

                        }
                        //FlightDetails lfFlight=lfFlightDetails[i].ke
                    }

                    flight.FlightId = flightKey; 

                    flight.Origin= new Airport(lfAirSegment[x].Origin);
                    flight.Destination= new Airport(lfAirSegment[x].Destination);
                    flight.Airline= lfAirSegment[x].Carrier;
                    flight.FlightNumber= lfAirSegment[x].FlightNumber;
                    flight.DepartureTime = Convert.ToDateTime(lfAirSegment[x].DepartureTime);
                    flight.ArrivalTime= Convert.ToDateTime(lfAirSegment[x].ArrivalTime);
                    // For UAPI REservation Request
                    flight.UapiDepartureTime= lfAirSegment[x].DepartureTime;
                    flight.UapiArrivalTime = lfAirSegment[x].ArrivalTime;


                    flight.Craft= lfAirSegment[x].Equipment;
                    int duration = Convert.ToInt32(lfAirSegment[x].FlightTime);
                    flight.Duration = new TimeSpan(0, duration, 0);
                    flight.DepTerminal= lfFlight.OriginTerminal;
                    flight.ArrTerminal = lfFlight.DestinationTerminal;
                    flight.ETicketEligible = lfAirSegment[x].ETicketabilitySpecified;
                    flight.Group = segGroup;
                    
                    if (segGroup == 0)
                    {
                        outboundflightList.Add(segmentKey,flight);
                    }
                    else
                    {
                        inboundflightList.Add(segmentKey,flight);
                    }

                }


            }

        }
        
        private static SearchResult[] ReadFareResponse(LowFareSearchRsp lfResponse, SearchRequest request, Dictionary<int, FlightInfo> outboundflightList, Dictionary<int, FlightInfo> inboundflightList)
        {

            List<SearchResult> resultarray = new List<SearchResult>();
            List<string> resultKey = new List<string>();
            //List<FlightInfo> outbound = outboundflightList;
            //List<FlightInfo> inbound = inboundflightList;

            Dictionary<int, FlightInfo> outbound = outboundflightList;
            Dictionary<int, FlightInfo> inbound = inboundflightList;

            Dictionary<int, Air20.FareInfo> fareBasisCodeRef = new Dictionary<int, Air20.FareInfo>();
            //Dictionary<int, Air20.FareInfo> fareInfoDic = new Dictionary<int, Air20.FareInfo>();
            Air20.FareInfo[] fareInfoList=lfResponse.FareInfoList;
            for (int f = 0; fareInfoList.Length > f; f++)
            {
                Air20.FareInfo fareInfo = fareInfoList[f];
                int fareInfoKey = keyGen(fareInfo.Key);
                fareBasisCodeRef.Add(fareInfoKey, fareInfo);
            }

          
            string currency = lfResponse.CurrencyType;
            Air20.AirPricingSolution[] pricingSolutionList = lfResponse.AirPricingSolution;
            Fare[] fare = new Fare[0];
            foreach (AirPricingSolution pricingSolution in pricingSolutionList) // AirPricingSolution
            {
                //AirPricingSolution RRpricingSolution = new AirPricingSolution();// TO Pass with Air Create Reservation Request
                //RRpricingSolution.airse

                bool nonRefundable = false;
                double totalResultFare = 0;
                double totalbaseFare = 0;
                string lastTktDate = string.Empty;
                //pricingSolution.Journey[0].
                int fareKey = 0;
                double baseFare = 0;
                double totalFare = 0;
                fare = new Fare[pricingSolution.AirPricingInfo.Length];

                Air20.BookingInfo[] bookingInfoList = pricingSolution.AirPricingInfo[0].BookingInfo;
                int infoIndex = 0;
                foreach (Air20.AirPricingInfo pricingInfo in pricingSolution.AirPricingInfo) //AirPricingInfo
                {
                    baseFare = getCurrAmount(pricingInfo.BasePrice);
                    totalFare = getCurrAmount(pricingInfo.TotalPrice);
                    lastTktDate = pricingInfo.LatestTicketingTime;
                    nonRefundable = !pricingInfo.Refundable;
                    pricingInfo.FareInfo = new Air20.FareInfo[pricingInfo.FareInfoRef.Length];
                    int indexFareInfo = 0;
                    foreach (Air20.FareInfoRef tempFareInfo in pricingInfo.FareInfoRef)
                    {
                        pricingInfo.FareInfo[indexFareInfo] = fareBasisCodeRef[keyGen(tempFareInfo.Key)];
                        indexFareInfo++;

                    }



                    fare[infoIndex] = new Fare();

                    if (pricingInfo.PassengerType[0].Code == "ADT") // Adult
                    {
                        fare[infoIndex].PassengerType= PassengerType.Adult;
                        fare[infoIndex].PassengerCount = request.AdultCount;
                        fare[infoIndex].BaseFare = baseFare * request.AdultCount;
                        fare[infoIndex].TotalFare = totalFare * request.AdultCount;
                    }
                    else if (pricingInfo.PassengerType[0].Code == "CNN")// Children
                    {
                        fare[infoIndex].PassengerType = PassengerType.Child;
                        fare[infoIndex].PassengerCount = request.ChildCount;
                        fare[infoIndex].BaseFare = baseFare * request.ChildCount;
                        fare[infoIndex].TotalFare = totalFare * request.ChildCount;
                    }
                    else if (pricingInfo.PassengerType[0].Code == "INF")// Infant
                    {
                        fare[infoIndex].PassengerType = PassengerType.Infant;
                        fare[infoIndex].PassengerCount = request.InfantCount;
                        fare[infoIndex].BaseFare = baseFare * request.InfantCount;
                        fare[infoIndex].TotalFare = totalFare * request.InfantCount;
                    }
                    else if (pricingInfo.PassengerType[0].Code == "SRC")// Senior citizen
                    {
                        fare[infoIndex].PassengerType = PassengerType.Senior;
                        fare[infoIndex].PassengerCount = request.SeniorCount;
                        fare[infoIndex].BaseFare = baseFare * request.SeniorCount;
                        fare[infoIndex].TotalFare = totalFare * request.SeniorCount;
                    }
                    totalbaseFare += baseFare * fare[infoIndex].PassengerCount;
                    totalResultFare += totalFare * fare[infoIndex].PassengerCount;
                    infoIndex++;
                }// Air Pricing Info End

                // Adding segments in single result
                List<FlightInfo[]> outFlights = new List<FlightInfo[]>();
                List<FlightInfo[]> inFlights = new List<FlightInfo[]>();
                Air20.Journey[] journeyList = pricingSolution.Journey;
                int jIndex = 0;
                foreach(Air20.Journey tempJourney in journeyList) // Journey Array
                {
                    FlightInfo[] flights = new FlightInfo[0];
                    Air20.AirSegmentRef[] jSegRefList=tempJourney.AirSegmentRef;
                    int segRefIndex = 0;
                    foreach (Air20.AirSegmentRef jSegRef in jSegRefList)
                    {
                        
                        if (segRefIndex == 0)
                        {
                            flights = new FlightInfo[jSegRefList.Length];
                        }

                        int jSegRefKey = keyGen(jSegRef.Key);
                        if (jIndex==0)// outbound
                        {
                            //outbound[jSegRefKey].Group = "0";
                            flights[segRefIndex] = outbound[jSegRefKey];
                            
                        }
                        else // return
                        {
                            //outbound[jSegRefKey].Group = "1";
                            flights[segRefIndex] = inbound[jSegRefKey];
                            
                        }
                        string bookingClass = string.Empty;
                        int fareInfoKey = 0;

                       // pricingSolution.AirSegment = new AirSegment[bookingInfoList.Length];// to assign airsegment in Create Reservation Pricing Info
                        int bkInfoIndex = 0;
                        foreach (Air20.BookingInfo bookingInfo in bookingInfoList)
                        {
                            if (keyGen(bookingInfo.SegmentRef) == jSegRefKey)
                            {
                                bookingClass = bookingInfo.BookingCode;
                                fareInfoKey = keyGen(bookingInfo.FareInfoRef);
                                //Air20.FareInfo tempfareInfo = fareInfoDic[keyGen(bookingInfo.FareInfoRef)];
                                //fareBasisCodeRef.Add(keyGen(bookingInfo.SegmentRef), tempfareInfo);
                            }
                            
                            

                        }
                        flights[segRefIndex].BookingClass = bookingClass;
                        flights[segRefIndex].FareInfoKey = fareInfoKey;
                        segRefIndex += 1;
                        if (segRefIndex== jSegRefList.Length )
                        {

                            if (jIndex == 0)// outbound
                            {
                                outFlights.Add(flights); 
                            }
                            else // return
                            {
                                inFlights.Add(flights);
                            }

                        }
                       
                    }
                    jIndex++;

                    
                }// Journey Array End


                if (inFlights.Count > 0) // return
                {

                    SearchResult result = new SearchResult();
                    result.UapiPricingSolution= pricingSolution;// for UAPI Create Reservatiobn Req.... to get Airfare with response
                    //result.UapiPricingSolution = new object();
                    result.FareBreakdown = fare;
                    result.TotalFare = totalResultFare;
                    result.BaseFare = totalbaseFare;
                    result.Tax = totalResultFare - totalbaseFare;
                    result.LastTicketDate = lastTktDate;
                    result.Currency = currency;
                    result.NonRefundable = nonRefundable;
                    //result.ResultBookingSource = BookingSource.Galileo;
                    result.ResultBookingSource = BookingSource.UAPI;
                    result.FareRules = new List<FareRule>();
                    result.Flights = new FlightInfo[2][];

                    result.Flights[0] = new FlightInfo[outFlights[0].Length];
                    for (int k = 0; k < outFlights[0].Length; k++)
                    {
                        result.Flights[0][k] = new FlightInfo();
                        result.Flights[0][k] = FlightInfo.Copy(outFlights[0][k]);
                    }
                    result.Flights[1] = new FlightInfo[inFlights[0].Length];
                    for (int k = 0; k < inFlights[0].Length; k++)
                    {
                        result.Flights[1][k] = new FlightInfo();
                        result.Flights[1][k] = FlightInfo.Copy(inFlights[0][k]);
                    }

                    FlightInfo[] flightSegments = SearchResult.GetSegments(result);
                    for (int s = 0; s < flightSegments.Length; s++)
                    {
                        if (fareBasisCodeRef.ContainsKey(flightSegments[s].FareInfoKey))
                        {
                            FareRule fareRule = new FareRule();
                            fareRule.Airline = flightSegments[s].Airline;
                            fareRule.Destination = flightSegments[s].Destination.AirportCode;
                            fareRule.Origin = flightSegments[s].Origin.AirportCode;
                            
                            Air20.FareInfo tempFareInfo = fareBasisCodeRef[flightSegments[s].FareInfoKey];
                            fareRule.FareBasisCode = tempFareInfo.FareBasis;

                            Air20.FareRuleKey tempFareRuleKey= tempFareInfo.FareRuleKey;
                            fareRule.FareRuleKeyValue = tempFareRuleKey.Value;
                            fareRule.FareInfoRef= keyGen(tempFareRuleKey.FareInfoRef);

                            // fareRule.FareBasisCode = fareBasisCodeRef[s + 1]; todo ziya
                            result.FareRules.Add(fareRule);
                        }
                    }
                    string key = BuildResultKey(result);
                    if (!resultKey.Contains(key))
                    {
                        if (IsValidResult(result, request))
                        {
                            resultarray.Add(result);
                            resultKey.Add(key);
                        }
                    }

                }
                else // One Way
                {
                    SearchResult result = new SearchResult();
                    result.UapiPricingSolution = pricingSolution;// for UAPI Create Reservatiobn Req.... to get Airfare with response
                    //result.UapiPricingSolution = new object();
                    result.FareBreakdown = fare;
                    result.TotalFare = totalResultFare;
                    result.BaseFare = totalbaseFare;
                    result.Tax = totalResultFare - totalbaseFare;
                    result.LastTicketDate = lastTktDate;
                    result.Currency = currency;
                    result.NonRefundable = nonRefundable;
                    //result.ResultBookingSource = BookingSource.Galileo;
                    result.ResultBookingSource = BookingSource.UAPI;
                    result.FareRules = new List<FareRule>();
                    result.Flights = new FlightInfo[1][];
                    result.Flights[0] = new FlightInfo[outFlights[0].Length];
                    for (int j = 0; j < outFlights[0].Length; j++)
                    {
                        result.Flights[0][j] = new FlightInfo();
                        result.Flights[0][j] = FlightInfo.Copy(outFlights[0][j]);
                    }

                    FlightInfo[] flightSegments = SearchResult.GetSegments(result);
                    for (int s = 0; s < flightSegments.Length; s++)
                    {
                        if (fareBasisCodeRef.ContainsKey(flightSegments[s].FareInfoKey))
                        {
                            FareRule fareRule = new FareRule();
                            fareRule.Airline = flightSegments[s].Airline;
                            fareRule.Destination = flightSegments[s].Destination.AirportCode;
                            fareRule.Origin = flightSegments[s].Origin.AirportCode;

                            Air20.FareInfo tempFareInfo = fareBasisCodeRef[flightSegments[s].FareInfoKey];
                            fareRule.FareBasisCode = tempFareInfo.FareBasis;

                            Air20.FareRuleKey tempFareRuleKey = tempFareInfo.FareRuleKey;
                            fareRule.FareRuleKeyValue = tempFareRuleKey.Value;
                            fareRule.FareInfoRef = keyGen(tempFareRuleKey.FareInfoRef);

                            // fareRule.FareBasisCode = fareBasisCodeRef[s + 1]; todo ziya
                            result.FareRules.Add(fareRule);
                        }
                    }
                    string key = BuildResultKey(result);
                    if (!resultKey.Contains(key))
                    {
                        if (IsValidResult(result, request))
                        {
                            resultarray.Add(result);
                            resultKey.Add(key);
                        }
                    }
                }


            } // Air Pricing Solution End
            

           
            return resultarray.ToArray();
        }

        private static string BuildResultKey(SearchResult result)
        {
            int obLength = result.Flights[0].Length;
            StringBuilder key = new StringBuilder();
            key.Append(result.Flights[0][0].DepartureTime.DayOfYear.ToString());
            key.Append(result.Flights[0][0].DepartureTime.ToString("HHmm"));
            key.Append(result.Flights[0][obLength - 1].ArrivalTime.DayOfYear.ToString());
            key.Append(result.Flights[0][obLength - 1].ArrivalTime.ToString("HHmm"));
            if (result.Flights.Length > 1)
            {
                int ibLength = result.Flights[1].Length;
                key.Append(result.Flights[1][0].DepartureTime.DayOfYear.ToString());
                key.Append(result.Flights[1][0].DepartureTime.ToString("HHmm"));
                key.Append(result.Flights[1][ibLength - 1].ArrivalTime.DayOfYear.ToString());
                key.Append(result.Flights[1][ibLength - 1].ArrivalTime.ToString("HHmm"));
            }
            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    key.Append(result.Flights[i][j].FlightNumber);
                    key.Append(result.Flights[i][j].BookingClass);
                }
            }
            return key.ToString();
        }
        private static bool IsValidResult(SearchResult result, SearchRequest request)
        {
            bool isValid = false;
            for (int i = 0; i < result.Flights.Length; i++)
            {
                if (i == 0)
                {
                    if (((result.Flights[i][0].Origin.CityCode == request.Segments[0].Origin) || (result.Flights[i][0].Origin.AirportCode == request.Segments[0].Origin)) && ((result.Flights[i][result.Flights[i].Length - 1].Destination.CityCode == request.Segments[0].Destination) || (result.Flights[i][result.Flights[i].Length - 1].Destination.AirportCode == request.Segments[0].Destination)))
                    {
                        isValid = true;
                    }
                }
                else
                {
                    if (((result.Flights[i][0].Origin.CityCode == request.Segments[0].Destination) || (result.Flights[i][0].Origin.AirportCode == request.Segments[0].Destination)) && ((result.Flights[i][result.Flights[i].Length - 1].Destination.CityCode == request.Segments[0].Origin) || (result.Flights[i][result.Flights[i].Length - 1].Destination.AirportCode == request.Segments[0].Origin)))
                    {
                        isValid = true;
                    }
                }
            }
            return isValid;
        }
    # endregion

        # region Fare Rules

        public static List<FareRule> GetFareRuleList(List<FareRule> fareRuleList)
        {
            Connection();

            AirFareRulesReq fareRulereq = new AirFareRulesReq();
            fareRulereq.TargetBranch = targetBranch;
            fareRulereq.FareRuleType = typeFareRuleType.@long;

            BillingPointOfSaleInfo pos = new BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            fareRulereq.BillingPointOfSaleInfo = pos;

            fareRulereq.FareRuleKey=new FareRuleKey[fareRuleList.Count];
            for (int i = 0; i < fareRuleList.Count; i++)
            {
                


                FareRuleKey fRuleKey = new FareRuleKey();

                string tempFareBasisCode = fareRuleList[i].FareBasisCode;
                string tempFareInfoRef = string.Empty;
                string tempFareRuleKeyValue = string.Empty;
                if (tempFareBasisCode.IndexOf('-') != -1) // From UAPI - FareInfo Key & FareInfo Key Value
                {
                    string[] fareInfo = tempFareBasisCode.Split('-');

                    if (fareInfo.Length > 1)
                    {
                        //fRule.FareBasisCode = fareInfo[0];
                        tempFareInfoRef = fareInfo[1];
                        tempFareRuleKeyValue = fareInfo[2];
                    }
                }
                else
                {
                    tempFareInfoRef = Convert.ToString(fareRuleList[i].FareInfoRef);
                    tempFareRuleKeyValue = fareRuleList[i].FareRuleKeyValue; 

                    //fRule.FareBasisCode = tempFareBasisCode;
                }

                fRuleKey.FareInfoRef = tempFareInfoRef + "T";
                fRuleKey.ProviderCode = "1G"; // TODO-- add Dynamic Provider
                fRuleKey.Value = tempFareRuleKeyValue;

                fareRulereq.FareRuleKey[i] = new FareRuleKey();
                fareRulereq.FareRuleKey[i] = fRuleKey;





                //fRuleKey.FareInfoRef = Convert.ToString(fareRuleList[i].FareInfoRef)+"T";
                //fRuleKey.ProviderCode = "1G"; // TODO-- add Dynamic Provider
                //fRuleKey.Value = fareRuleList[i].FareRuleKeyValue;

                //fareRulereq.FareRuleKey[i] = new FareRuleKey();
                //fareRulereq.FareRuleKey[i] = fRuleKey;

            }

            AirFareRulesBinding binding = new AirFareRulesBinding();
            binding.Url = urlAir;
            binding.Credentials = new NetworkCredential(userName, password);
            AirFareRulesRsp fareRuleResponse = binding.service(fareRulereq);

            // startTo Remove
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(fareRuleResponse.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            ser.Serialize(writer, fareRuleResponse); 	// Here Classes are converted to XML String. 
            // This can be viewed in SB or writer.
            // Above XML in SB can be loaded in XmlDocument object
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());

            doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirFareRulesRsp.xml");
            // end To Remove

            // Fare Rule Response
            Air20.FareRule[] fareRulesResultList = fareRuleResponse.FareRule;
            foreach (Air20.FareRule fareRulesResult in fareRulesResultList)
            {
                //string category = string.Empty;
                string fareruleValue = string.Empty;
                int farInfoRef = keyGen(fareRulesResult.FareInfoRef);
                FareRuleLong[] fareRuleslongList = fareRulesResult.FareRuleLong;
                foreach (FareRuleLong fareRuleLong in fareRuleslongList)
                {
                    fareruleValue += "<br>" + fareRuleLong.Value;
                }


                for (int i = 0; i < fareRuleList.Count; i++)
                {
                    if (fareRuleList[i].FareInfoRef == farInfoRef)
                    {
                        fareRuleList[i].FareRuleDetail = fareruleValue;
                    }
                }
            }

            return fareRuleList;
        }
        # endregion

        # region Book

        /// <summary>
        /// Method to book a selected Itinerary
        /// </summary>
        /// <param name="itinerary">An object of FlightItinerary</param>
        /// <returns>BookingResponse</returns>
        public static BookingResponse Book(FlightItinerary itinerary, string sessionId)
        {
            BookingResponse bookingResponse = new BookingResponse();
            bool isDomestic = itinerary.CheckDomestic("" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
            AirCreateReservationRsp reservationRsp=new AirCreateReservationRsp();
            reservationRsp = CreateReservation(itinerary);


            // To Remove
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(reservationRsp.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            ser.Serialize(writer, reservationRsp); 	// Here Classes are converted to XML String. 
            // This can be viewed in SB or writer.
            // Above XML in SB can be loaded in XmlDocument object
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());
            doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirReservationRes .xml");
            //


            Audit.Add(EventType.Book, Severity.Normal, 0, "UAPI Create Reservation Response. : ", string.Empty);
            bool booked=true;

            //Checking Error
            AirSegmentError[] sellFailureErr = reservationRsp.AirSegmentSellFailureInfo;
            if (sellFailureErr != null)
            {
                booked = false;
                
                Basket.FlightBookingSession[sessionId].Log.Add("UAPI Sell Failure Error.");
                bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "SEAT NOT AVAILABLE -" + sellFailureErr[0].ErrorMessage, "");
                Audit.Add(EventType.Book, Severity.High, 0, "Booking Failed UAPI. Sell Failure Error :" + sellFailureErr[0].ErrorMessage, string.Empty);
            }
            else
            {
                UniversalRecord unRecord = reservationRsp.UniversalRecord;
                AirReservation[] airReservationList = unRecord.Items;
                if (airReservationList == null)
                    booked = false;
                foreach (AirReservation airReservation in airReservationList)
                {
                    booked = ReadBookingStatus(airReservation);

                }
                if (booked)
                {
                    bookingResponse = ReadBookResponse(reservationRsp,itinerary);
                    Audit.Add(EventType.Book, Severity.High, 0, "Book UAPI.booking Response from ReadBookResponse ", string.Empty); 

                }
                else
                {
                    
                    
                    Basket.FlightBookingSession[sessionId].Log.Add("UAPI Booking Failed.");
                    bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "SEAT NOT AVAILABLE", "");
                    Audit.Add(EventType.Book, Severity.High, 0, "Booking Failed UAPI. Booked Status : false", string.Empty);
                }

            }

            if (booked == false)
            {
                if (!string.IsNullOrEmpty(reservationRsp.UniversalRecord.LocatorCode))
                {
                    CancelItinerary(reservationRsp.UniversalRecord.LocatorCode, "UR");
                }
            }

            return bookingResponse;

        }

        private static AirCreateReservationRsp CreateReservation(FlightItinerary itinerary)
        {
            AirCreateReservationRsp reservationRsp=new AirCreateReservationRsp();

            AirCreateReservationReq createRequest = new AirCreateReservationReq();
            createRequest.TargetBranch = targetBranch;
            createRequest.RetainReservation = typeRetainReservation.Both;

            BillingPointOfSaleInfo pos = new BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            createRequest.BillingPointOfSaleInfo = pos;

            createRequest.BookingTraveler = new BookingTraveler[itinerary.Passenger.Length];
            int paxIndex = 0;
            foreach(FlightPassenger passenger in itinerary.Passenger)
            {
                string passengerType = string.Empty;
                // Asigning Pax Type
                if (passenger.Type == PassengerType.Adult)
                    passengerType = "ADT";
                else if (passenger.Type == PassengerType.Child)
                    passengerType = "CNN";
                else if (passenger.Type == PassengerType.Infant)
                    passengerType = "INF";
                else if (passenger.Type == PassengerType.Senior)
                    passengerType = "SRC";
                BookingTraveler tmpBkTraveler = new BookingTraveler();
                tmpBkTraveler.Key = paxIndex.ToString();
                tmpBkTraveler.TravelerType = passengerType;
                BookingTravelerName tmpTravelrName = new BookingTravelerName();
                tmpTravelrName.Prefix = passenger.Title;
                tmpTravelrName.First = passenger.FirstName;
                tmpTravelrName.Last = passenger.LastName; 
                tmpBkTraveler.BookingTravelerName = tmpTravelrName;

                PhoneNumber tmpPhone = new PhoneNumber();
                tmpPhone.Type = PhoneNumberType.Mobile;
                //tmpPhone.Number = passenger.CellPhone;
                tmpPhone.Number = itinerary.Passenger[0].CellPhone;
                tmpBkTraveler.PhoneNumber = new PhoneNumber[1];
                tmpBkTraveler.PhoneNumber[0] = new PhoneNumber();
                tmpBkTraveler.PhoneNumber[0] = tmpPhone;

                //if (paxIndex == 0)
                //{
                    typeStructuredAddress tmpAdds = new typeStructuredAddress();
                    tmpAdds.AddressName = passenger.AddressLine1;
                    //tmpAdds.Street = new string[0];
                    //tmpAdds.Street = new string();
                    if (!string.IsNullOrEmpty(passenger.AddressLine2))
                    {
                        tmpAdds.Street = new string[1];
                        tmpAdds.Street[0] = passenger.AddressLine2;
                    }
                    if (!string.IsNullOrEmpty(passenger.AddressLine2))
                    {
                        tmpAdds.City = passenger.City;
                    }
                    if (!string.IsNullOrEmpty(passenger.AddressLine2))
                    {
                        State tmpState = new State();
                        tmpState.Value = passenger.City;
                        tmpAdds.State = tmpState;
                    }
                    
                    tmpAdds.PostalCode = "0097160";
                    tmpAdds.Country = passenger.Country.CountryCode;

                    tmpBkTraveler.Address = new typeStructuredAddress[1];
                    tmpBkTraveler.Address[0] = new typeStructuredAddress();
                    tmpBkTraveler.Address[0] = tmpAdds;
                //}
                createRequest.BookingTraveler[paxIndex] = new BookingTraveler();
                createRequest.BookingTraveler[paxIndex] = tmpBkTraveler;
                paxIndex++;
            }

            AirPricingSolution pricingSolution = new AirPricingSolution();
            pricingSolution.Key = Convert.ToString(itinerary.UapiPricingSolutionKey) + "T";
            pricingSolution.AirSegment=new AirSegment[itinerary.Segments.Length];

            //AirSegment airSegmentList
            // Creating Air Segment 
            int segIndex = 0;
            foreach (FlightInfo flight in itinerary.Segments)
            {
                string airSegKey = Convert.ToString(flight.UapiSegmentRefKey) + "T";

                AirSegment tmpAirSegment = new AirSegment();
                tmpAirSegment.Key = airSegKey;
                tmpAirSegment.Group = flight.Group;
                tmpAirSegment.Carrier = flight.Airline;
                tmpAirSegment.FlightNumber= flight.FlightNumber;
                tmpAirSegment.Origin = flight.Origin.AirportCode;
                tmpAirSegment.Destination = flight.Destination.AirportCode;
                //tmpAirSegment.DepartureTime = flight.DepartureTime.ToString("yyyy-MM-dd");  To get Schedule Error
                //tmpAirSegment.DepartureTime = flight.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss");
                //tmpAirSegment.ArrivalTime = flight.ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"); 
                tmpAirSegment.DepartureTime = flight.UapiDepartureTime;
                tmpAirSegment.ArrivalTime = flight.UapiArrivalTime;
                
                tmpAirSegment.ClassOfService= flight.BookingClass;

                pricingSolution.AirSegment[segIndex] = new AirSegment();
                pricingSolution.AirSegment[segIndex] = tmpAirSegment;
                segIndex++;

            }


            createRequest.AirPricingSolution=pricingSolution;
            
            ActionStatus actionStatus = new ActionStatus();
            actionStatus.ProviderCode = "1G";// To do -- should be assigned dynamically
            actionStatus.Type = ActionStatusType.ACTIVE;

            createRequest.ActionStatus = new ActionStatus[1];
            createRequest.ActionStatus[0] = new ActionStatus();
            createRequest.ActionStatus[0] = actionStatus;

            // To Remove
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(createRequest.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            ser.Serialize(writer, createRequest); 	// Here Classes are converted to XML String. 
            // This can be viewed in SB or writer.
            // Above XML in SB can be loaded in XmlDocument object
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());
            doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirReservationReq .xml");
            //

            AirCreateReservationBinding binding = new AirCreateReservationBinding();
            binding.Url = urlAir;
            binding.Credentials = new NetworkCredential(userName, password);

            reservationRsp = binding.service(createRequest);
            

            return reservationRsp;


        }


        private static bool ReadBookingStatus(AirReservation airReservation)
        { 
            bool booked = true;
            List<string> availabilityStatus = new List<string>();
            availabilityStatus.Add("HS");
            availabilityStatus.Add("SS");
            availabilityStatus.Add("HK");
            availabilityStatus.Add("KK");
            availabilityStatus.Add("TK");

            AirSegment[] airSegmentList = airReservation.AirSegment;
            if (airSegmentList != null)
            {
                foreach (AirSegment airSegment in airSegmentList)
                {
                    string status = airSegment.Status;
                    if (!availabilityStatus.Contains(status))
                    {
                        booked = false;
                    }

                }
            }
            else
                booked = false;

            return booked;
           
        }

        private static BookingResponse ReadBookResponse(AirCreateReservationRsp reservationRsp, FlightItinerary itinerary)
        {
            
            
            BookingResponse booking = new BookingResponse();
            UniversalRecord unRecord = reservationRsp.UniversalRecord;
            string URlocatorCode = unRecord.LocatorCode;// UR
            string providerLocatorCode = unRecord.ProviderReservationInfo[0].LocatorCode;// TO do with multiple Airline Test
            // PNR ( supplier)
            string reservationLocatorCode =string.Empty;


            AirReservation[] airReservationList = unRecord.Items;
            if(airReservationList!=null)
            {
            foreach (AirReservation airReservation in airReservationList)
            {
                reservationLocatorCode=airReservation.LocatorCode;// TO do with multiple Airline Test
                AirSegment[] airSegmentList = airReservation.AirSegment;
                int airSegIndex=0;
                foreach (AirSegment airSegment in airSegmentList)
                {
                    string status = airSegment.Status;

                    if((airSegIndex+1)==Convert.ToInt32(airSegment.TravelOrder)) // To cross check the Segments/Flights
                    {
                     itinerary.Segments[airSegIndex].Status = status;
                       if ((itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'K') || (itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'S'))
                        {
                            itinerary.Segments[airSegIndex].FlightStatus = FlightStatus.Confirmed;
                            SupplierLocator[] supLocatorList = airReservation.SupplierLocator;
                            if (supLocatorList != null)
                            {
                                foreach(SupplierLocator supLocator in supLocatorList)
                                {

                                if(airSegment.Carrier==supLocator.SupplierCode)
                                {
                                    itinerary.Segments[airSegIndex].AirlinePNR = supLocator.SupplierLocatorCode;

                                }
                                }
                            }
                           
                        }
                    }
                    airSegIndex++;
                    
                }    

            }
            itinerary.UniversalRecord = URlocatorCode;// Assigning UR to Itinerary
            itinerary.PNR=reservationLocatorCode;// REservation PNr is Actual Pnr
            itinerary.ProviderPNR = providerLocatorCode;//Provider PNR is Actual Pnr
            booking.PNR = reservationLocatorCode;//reservationLocatorCode;
            itinerary.FareType = "PUB";
            booking.Status = BookingResponseStatus.Successful;
            }
            else 
            {
                booking.Status = BookingResponseStatus.Failed;
                booking.Error = "NO VALID FARE FOR INPUT CRITERIA";
            }


            return booking;
            
           
              
                
               // For Price Info Changed
                //for (int i = 0; i < itinerary.Passenger.Length; i++)
                //{
                //    if (paxRef.ContainsKey(itinerary.Passenger[i].Type))
                //    {
                //        int fareKey = paxRef[itinerary.Passenger[i].Type];

                //        if (priceRef.ContainsKey(fareKey) && (itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.Tax) != (priceRef[fareKey].PublishedFare + priceRef[fareKey].Tax))
                //        {
                //            itinerary.Passenger[i].Price.PublishedFare = priceRef[fareKey].PublishedFare;
                //            itinerary.Passenger[i].Price.Tax = priceRef[fareKey].Tax;
                //            booking.Status = BookingResponseStatus.BookedOther;
                //        }
                //    }
                //}
               
           
               
            
            
        }
        # endregion

        # region Cancel Itinerary
        public static new string CancelItinerary(string pnr,string pnrType)
        {
            Connection();
            //bool isCancel = false;
            string tempPNR = pnr;
            string cancelledPNR = string.Empty;
            string cancelledProvider = string.Empty;
            bool isCancelled = false;

            if (pnrType != "UR")
            {
                FlightItinerary itinerary = new FlightItinerary(FlightItinerary.GetFlightId(pnr));
                pnr = itinerary.UniversalRecord;

            }

            
            CoreLogic.Audit.Add(EventType.CancellBooking, Severity.Normal, 0, "UAPI Cancell Itinerary : Request UR PNR = " + pnr, "0");

            UR18.UniversalRecordCancelReq urCancelRequest = new UR18.UniversalRecordCancelReq();
            urCancelRequest.TargetBranch = targetBranch;
            urCancelRequest.UniversalRecordLocatorCode = pnr;
            urCancelRequest.Version = "18";// TO DO
            urCancelRequest.AuthorizedBy = string.Empty ;

            UR18.BillingPointOfSaleInfo pos = new UR18.BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            urCancelRequest.BillingPointOfSaleInfo = pos;

            UR18.UniversalRecordCancelServiceBinding binding = new UR18.UniversalRecordCancelServiceBinding();
            
            binding.Url = urlUR;
            binding.Credentials = new NetworkCredential(userName, password);


            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(urCancelRequest.GetType());
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter writer = new System.IO.StringWriter(sb);
            ser.Serialize(writer, urCancelRequest); 	// Here Classes are converted to XML String. 
            // This can be viewed in SB or writer.
            // Above XML in SB can be loaded in XmlDocument object
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sb.ToString());

            doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\UniversalRecordCancelReq.xml");
            UR18.UniversalRecordCancelRsp urCancelResponse = null;


            try
            {
                urCancelResponse = binding.service(urCancelRequest);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.CancellBooking, Severity.High, 0, "UAP.CancelItinerary exiting : Cancellation of PNR " + pnr + " failed. Erro msg:"+ex.Message, string.Empty);
                cancelledPNR = ex.Message;
                
                return cancelledPNR;
            }


            // TO Remove
            System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(urCancelResponse.GetType());
            System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
            System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
            serRes.Serialize(writerRes, urCancelResponse); 	// Here Classes are converted to XML String. 
            // This can be viewed in SB or writer.
            // Above XML in SB can be loaded in XmlDocument object
            XmlDocument docRes = new XmlDocument();
            docRes.LoadXml(sbRes.ToString());
            docRes.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\UniversalRecordCancelRsp.xml");

            UR18.ProviderReservationStatus[] reservationStatusList = urCancelResponse.ProviderReservationStatus;
            if (reservationStatusList != null)
            {
                foreach (UR18.ProviderReservationStatus reservationStatus in reservationStatusList)
                {
                    isCancelled = reservationStatus.Cancelled;
                    cancelledPNR = reservationStatus.LocatorCode;
                    cancelledProvider= reservationStatus.ProviderCode;
                }

            }
            if (isCancelled)
            {
                if (pnrType != "UR")
                    cancelledPNR = tempPNR;



            }
            else
            {
                Trace.TraceError("UAP.CancelItinerary exiting : Cancellation of PNR " + pnr + " failed.");
                throw new BookingEngineException("Cancellation of PNR " + pnr + " failed.");
            }




            
              
            return cancelledPNR;
        }
        # endregion

        # region common Methods

        private static int keyGen(string keyValue)
        {
            int key = 0;
            string newKey = keyValue.Substring(0,keyValue.Length-1 );
            key = Convert.ToInt32(newKey);
            return key;


            //int key=
        }
        private static double getCurrAmount(string keyValue)
        {
            double amount = 0;
            string newAmont = keyValue.Remove(0, 3);
            amount= Convert.ToDouble(newAmont);
            return amount;
        }
        # endregion


    }
}
