﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Xml;
using System.Configuration;
using System.Net;
using System.Text.RegularExpressions;
using CT.BookingEngine;
using CT.Core;
using UAPIdll.WebAir15;
using System.Data;
using System.Web.Services.Protocols;

namespace CT.BookingEngine.GDS
{
    public class UAPICredentials
    {
        public string UserName = string.Empty;
        public string Password = string.Empty;
        public string TargetBranch = string.Empty;
    }
    public class UAPI
    {

        #region Credential
        public static string UserName = string.Empty;
        public static string Password = string.Empty;
        static string urlAir = string.Empty;
        static string urlUR = string.Empty;
        public static string TargetBranch = string.Empty;
        static string originalApplication = "UAPI";
        public static string agentBaseCurrency;
        public static Dictionary<string, decimal> ExchangeRates;
        static double rateOfExchange;
        static string xmlPath = string.Empty;
        public static string AppUserId;
        public static string SessionId;
        # endregion

        public const string LINK_AVAILABILITY = "LinkAvailability";
        public const string AVAILABILITY_SOURCE = "AvailabilitySource";
        public const string PROVIDER_CODE = "ProviderCode";
        public const string SEGMENT_REF = "SegmentRef";
        public const string POLLED_AVAILABILITY_OPTION = "PolledAvailabilityOption";
        public const string PARENT_AGENT_LOCATION = "ParentAgentLocation";
        public const string PARENT_AGENT_PHONE = "ParentAgentPhone";
        static bool formOfPaymentIsShown = true;        
        # region Constructor

        public UAPI()
        {

            Connection();
        }
        
        public static void Connection()
        {
            # region LiVe HC Credentials
            //userName = "Universal API/uAPI7280978342-0fbaffdc";
            //password = "6Hz}$9KeJi";
            //urlAir = "https://emea.universal-api.travelport.com/B2BGateway/connect/uAPI/AirService";
            //urlUR = "https://emea.universal-api.travelport.com/B2BGateway/connect/uAPI/UniversalRecordService";
            //targetBranch = "P1544552";
            //Audit.Add(EventType.Search, Severity.Normal, 0, " Live userName:" + userName, string.Empty);
            # endregion
            # region Live 
            //userName = CT.Configuration.ConfigurationSystem.UAPIConfig["Userid"];
            //password = CT.Configuration.ConfigurationSystem.UAPIConfig["Password"];
            //password = "M}y2&6Tt7D";
            urlAir = CT.Configuration.ConfigurationSystem.UAPIConfig["Url"] + "AirService";
            urlUR = CT.Configuration.ConfigurationSystem.UAPIConfig["Url"] + "UniversalRecordService";
            //targetBranch = CT.Configuration.ConfigurationSystem.UAPIConfig["InternationalHAP"];
            # endregion
            # region Test Credentials
            //userName = "Universal API/uAPI7997403705-43d54fd1";
            //password = "x-5Y6P}c!4";
            //urlAir = "https://emea.copy-webservices.travelport.com/B2BGateway/connect/uAPI/AirService";
            //urlUR = "https://emea.copy-webservices.travelport.com/B2BGateway/connect/uAPI/UniversalRecordService";
            //targetBranch = "P7005785";
            # endregion
            xmlPath = CT.Configuration.ConfigurationSystem.UAPIConfig["XmlLogPath"];
            try
            {
                if (!System.IO.Directory.Exists(xmlPath))
                {
                    System.IO.Directory.CreateDirectory(xmlPath);
                }
            }
            catch { }
        }
        # endregion

        # region Search
        public static SearchResult[] Search(SearchRequest request, string sessionId)
        {
            
            Trace.TraceInformation("UAPI.Search() entered.");
            List<SearchResult> finalResult = new List<SearchResult>();
            SearchResult[] result = new SearchResult[0];
            //SearchResult[] icSearchResult = new SearchResult[0];
           // bool isDomestic = true;
            //List<int> flightNotFound = new List<int>();
            //Dictionary<int, List<FlightInfo>> outboundflightList = new Dictionary<int, List<FlightInfo>>();
            //Dictionary<int, List<FlightInfo>> inboundflightList = new Dictionary<int, List<FlightInfo>>();

            
            //isDomestic = false;
            //Connection connection = new Connection(isDomestic);
            result = LowFareSearch(request, sessionId );
            finalResult.AddRange(result);

            Trace.TraceInformation("UAPI.Search() exiting.");
            Basket.FlightBookingSession[sessionId].Log.Add("UAPI Search complete. Result count = " + finalResult.Count.ToString());
            return finalResult.ToArray();
        }
        private static SearchResult[] LowFareSearch(SearchRequest request, string sessionId)
        {
            //Dictionary<int, List<FlightInfo>> outboundflightList = new Dictionary<int, List<FlightInfo>>();
            //Dictionary<int, List<FlightInfo>> inboundflightList = new Dictionary<int, List<FlightInfo>>();
            //List<FlightInfo> outbound = new List<FlightInfo>();
            //List<FlightInfo> inbound = new List<FlightInfo>();

            Dictionary<string, FlightInfo> outbound = new Dictionary<string, FlightInfo>();
            Dictionary<string, FlightInfo> inbound = new Dictionary<string, FlightInfo>();

            SearchResult[] result = new SearchResult[0];
            //NetworkCredential nCredential=new NetworkCredential(this
            LowFareSearchRsp lfResponse = GenerateSearch(request);

            
            //Basket.FlightBookingSession[sessionId].XmlMessage.Add(searchXml);


            if (request != null)
            {
                ReadFlightInfoResponse(lfResponse, ref outbound, ref inbound);
                if (outbound.Count > 0)
                {
                    result = ReadFareResponse(lfResponse, request, outbound, inbound);
                }

            }
            //if (searchXml.Length > 0)
            //{
            //    string responseXml = connection.MultiSubmitXml(searchXml);
            //    //Basket.FlightBookingSession[sessionId].XmlMessage.Add(responseXml);
            //    Audit.Add(EventType.Search, Severity.Normal, 0, "Search XML 1G. req : " + searchXml + " resp : " + responseXml, string.Empty);
            //    ReadFlightInfoResponse(responseXml, request, ref outboundflightList, ref inboundflightList);
            //    if (outboundflightList.Count > 0)
            //    {
            //        result = ReadFareResponse(responseXml, request, outboundflightList, inboundflightList);
            //    }
            //}
            return result;
        }
        private static LowFareSearchRsp GenerateSearch(SearchRequest request)
        {

            Trace.TraceInformation("UAPI.GenerateSearchMessage entered");
            bool isDomestic = true;
            if ((request.AdultCount + request.ChildCount + request.SeniorCount + request.InfantCount) > 9)
            {
                throw new BookingEngineException("Total number of passengers should not exceed 9");
            }
            if (request.InfantCount > (request.AdultCount + request.SeniorCount))
            {
                throw new BookingEngineException("Number of infants should not exceed the number of Adult and Senior");
            }
            int paxCount = request.AdultCount + request.ChildCount + request.InfantCount + request.SeniorCount;

            Connection();
            LowFareSearchReq lfRequest = new LowFareSearchReq();
            lfRequest.TargetBranch = TargetBranch;

            BillingPointOfSaleInfo pos = new BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;            
            lfRequest.BillingPointOfSaleInfo = pos;

            AirSearchModifiers airModifiers = new AirSearchModifiers();
            lfRequest.AirSearchModifiers = airModifiers;
            SearchPassenger[] passAdult = new SearchPassenger[paxCount + 1];
            int paxIncrement = 0;
            for (int i = 0; i < request.AdultCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "ADT";
                paxIncrement++;

            }
            for (int i = 0; i < request.ChildCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "CNN";
                passAdult[paxIncrement].Age = "5";
                paxIncrement++;

            }
            for (int i = 0; i < request.SeniorCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "SRC";
                passAdult[paxIncrement].Age = "65";
                paxIncrement++;

            }
            for (int i = 0; i < request.InfantCount; i++)
            {
                passAdult[paxIncrement] = new SearchPassenger();
                passAdult[paxIncrement].Code = "INF";
                paxIncrement++;

            }
            lfRequest.SearchPassenger = passAdult;
            //lfRequest.SearchPassenger = passAdult;

            //lfRequest.SearchAirLeg = new SearchAirLeg[2];
            //lfRequest.SearchAirLeg[0] = new SearchAirLeg();
            //SearchAirLeg leg = lfRequest.SearchAirLeg[0];

            //Air20.Airport origin = new Air20.Airport();
            //origin.Code = request.Segments[0].Origin;

            //Air20.Airport destination = new Air20.Airport();
            //destination.Code = request.Segments[0].Destination;

            //leg.SearchOrigin = new typeSearchLocation[1];
            //leg.SearchOrigin[0] = new typeSearchLocation();
            //leg.SearchOrigin[0].Item= origin;

            //leg.SearchDestination = new typeSearchLocation[1];
            //leg.SearchDestination[0] = new typeSearchLocation();
            //leg.SearchDestination[0].Item= destination;

            //leg.Items = new typeFlexibleTimeSpec[1];
            //leg.Items[0] = new typeFlexibleTimeSpec();
            //leg.Items[0].PreferredTime = request.Segments[0].PreferredDepartureTime.ToString("yyyy-MM-dd");

            // PReferred Cabin
            
            CabinClass cabinType = CabinClass.Economy;
            switch (request.Segments[0].flightCabinClass)
            {
                case CabinClass.Business:
                    cabinType = CabinClass.Business;
                    break;
                case CabinClass.Economy:
                    cabinType = CabinClass.Economy;
                    break;
                case CabinClass.First:
                    cabinType = CabinClass.First;
                    break;
                    //Added by Shiva
                case CabinClass.PremiumEconomy:
                    cabinType = CabinClass.PremiumEconomy;
                    break;
                //case CabinClass.PremiumBusiness:
                //    cabinType = CabinClass.PremiumBusiness;
                //    break;  

            }
            if (request.Type == SearchType.MultiWay)
            {
                
                lfRequest.Items = new SearchAirLeg[request.Segments.Length];
                int segIndex = 0;
                foreach (FlightSegment segment in request.Segments)
                {
                    lfRequest.Items[segIndex] = new SearchAirLeg();
                    SearchAirLeg leg = lfRequest.Items[segIndex] as SearchAirLeg;
                    // origin
                    UAPIdll.WebAir15.Airport origin = new UAPIdll.WebAir15.Airport();
                    origin.Code = request.Segments[segIndex].Origin;

                    leg.SearchOrigin = new typeSearchLocation[1];
                    leg.SearchOrigin[0] = new typeSearchLocation();
                    leg.SearchOrigin[0].Item = origin;
                    // destination
                    UAPIdll.WebAir15.Airport destination = new UAPIdll.WebAir15.Airport();
                    destination.Code = request.Segments[segIndex].Destination;

                    leg.SearchDestination = new typeSearchLocation[1];
                    leg.SearchDestination[0] = new typeSearchLocation();
                    leg.SearchDestination[0].Item = destination;


                    leg.Items = new typeFlexibleTimeSpec[1];
                    leg.Items[0] = new typeFlexibleTimeSpec();
                    leg.Items[0].PreferredTime = request.Segments[segIndex].PreferredDepartureTime.ToString("yyyy-MM-dd");
                    AirLegModifiers legModifiers = new AirLegModifiers();
                    if (request.Segments[0].flightCabinClass != CabinClass.All)
                    {
                        PreferredCabins prefCabin = new PreferredCabins();
                        UAPIdll.WebAir15.CabinClass airCabin = new UAPIdll.WebAir15.CabinClass();
                        airCabin.Type = cabinType.ToString();
                        prefCabin.CabinClass = airCabin;
                        legModifiers.PreferredCabins = new PreferredCabins();
                        legModifiers.PreferredCabins = prefCabin;
                    }

                    if (request.RestrictAirline)
                    {

                        if (request.Segments[segIndex].PreferredAirlines != null && request.Segments[segIndex].PreferredAirlines.Length > 0)
                        {
                            // For Preffered Airlines
                            Carrier[] tmpCarrierList = new Carrier[1];
                            Carrier tmpCarrier = new Carrier();
                            tmpCarrier.Code = request.Segments[segIndex].PreferredAirlines[segIndex].ToString();
                            tmpCarrierList[0] = new Carrier();
                            tmpCarrierList[0] = tmpCarrier;
                            legModifiers.PreferredCarriers = tmpCarrierList;
                        }
                    }

                    // Preferred Cabin for Outbound
                    leg.AirLegModifiers = legModifiers;

                    segIndex++;
                }
            }
            else  // one way and round trip
            {


                lfRequest.Items = new SearchAirLeg[2];
                lfRequest.Items[0] = new SearchAirLeg();
                SearchAirLeg leg = lfRequest.Items[0] as SearchAirLeg;

                UAPIdll.WebAir15.Airport origin = new UAPIdll.WebAir15.Airport();
                origin.Code = request.Segments[0].Origin;

                UAPIdll.WebAir15.Airport destination = new UAPIdll.WebAir15.Airport();
                destination.Code = request.Segments[0].Destination;

                leg.SearchOrigin = new typeSearchLocation[1];
                leg.SearchOrigin[0] = new typeSearchLocation();
                leg.SearchOrigin[0].Item = origin;

                leg.SearchDestination = new typeSearchLocation[1];
                leg.SearchDestination[0] = new typeSearchLocation();
                leg.SearchDestination[0].Item = destination;

                leg.Items = new typeFlexibleTimeSpec[1];
                leg.Items[0] = new typeFlexibleTimeSpec();
                leg.Items[0].PreferredTime = request.Segments[0].PreferredDepartureTime.ToString("yyyy-MM-dd");
                AirLegModifiers legModifiers = new AirLegModifiers();
                if (request.Segments[0].flightCabinClass != CabinClass.All)
                {
                    PreferredCabins prefCabin = new PreferredCabins();
                    UAPIdll.WebAir15.CabinClass airCabin = new UAPIdll.WebAir15.CabinClass();
                    airCabin.Type = cabinType.ToString();
                    prefCabin.CabinClass = airCabin;
                    legModifiers.PreferredCabins = new PreferredCabins();
                    legModifiers.PreferredCabins = prefCabin;
                }
                if (request.RestrictAirline)
                {

                    if (request.Segments[0].PreferredAirlines != null && request.Segments[0].PreferredAirlines.Length > 0)
                    {
                        // For Preffered Airlines
                        //Carrier[] tmpCarrierList = new Carrier[1];
                        //Carrier tmpCarrier = new Carrier();
                        //tmpCarrier.Code = request.Segments[0].PreferredAirlines[0].ToString();
                        //tmpCarrierList[0] = new Carrier();
                        //tmpCarrierList[0] = tmpCarrier;
                        //legModifiers.PreferredCarriers = tmpCarrierList;

                        Carrier[] tmpCarrierList = new Carrier[request.Segments[0].PreferredAirlines.Length];

                        for (int i = 0; i < request.Segments[0].PreferredAirlines.Length; i++)
                        {
                            tmpCarrierList[i] = new Carrier();
                            tmpCarrierList[i].Code = request.Segments[0].PreferredAirlines[i];
                        }
                        legModifiers.PreferredCarriers = tmpCarrierList;
                    }
                }

                //If Non Stop filter applied then MaxStops will be 0 in B2C
                //If Direct filter applied then MaxStops will be 0 in B2B
                if (!string.IsNullOrEmpty(request.MaxStops))
                {
                    if (Convert.ToInt32(request.MaxStops) >= 0)
                    {
                        legModifiers.FlightType = new FlightType();
                        legModifiers.FlightType.MaxStops = request.MaxStops;
                    }
                    
                }


                // Preferred Cabin for Outbound
                leg.AirLegModifiers = legModifiers;
                //lfRequest.SearchAirLeg[0].AirLegModifiers = legModifiers;


                // Return Leg
                if (request.Type == SearchType.Return)
                {
                    lfRequest.Items[1] = new SearchAirLeg();
                    SearchAirLeg legReturn = lfRequest.Items[1] as SearchAirLeg;

                    UAPIdll.WebAir15.Airport retOrigin = new UAPIdll.WebAir15.Airport();
                    retOrigin.Code = request.Segments[0].Destination;

                    UAPIdll.WebAir15.Airport retDestination = new UAPIdll.WebAir15.Airport();
                    retDestination.Code = request.Segments[0].Origin;

                    legReturn.SearchOrigin = new typeSearchLocation[1];
                    legReturn.SearchOrigin[0] = new typeSearchLocation();
                    legReturn.SearchOrigin[0].Item = retOrigin;

                    legReturn.SearchDestination = new typeSearchLocation[1];
                    legReturn.SearchDestination[0] = new typeSearchLocation();
                    legReturn.SearchDestination[0].Item = retDestination;


                    legReturn.Items = new typeFlexibleTimeSpec[1];
                    legReturn.Items[0] = new typeFlexibleTimeSpec();
                    legReturn.Items[0].PreferredTime = request.Segments[1].PreferredDepartureTime.ToString("yyyy-MM-dd");


                    // Preferred Cabin for Inbound
                    legReturn.AirLegModifiers = legModifiers;

                }
            }

            //// For No Of Stops
            if (!string.IsNullOrEmpty(request.MaxStops))
            {
                if (Convert.ToInt32(request.MaxStops) > 0)
                {
                    airModifiers.PreferNonStop = false;
                }
                else
                {
                    airModifiers.PreferNonStop = true;
                }
                //airModifiers.MaxStops = request.MaxStops;
            }
            // Fare Type (Refundable /Non Refundable) AirPricingModifiers 

            if (request.RefundableFares)
            {
                AirPricingModifiers priceModifier = new AirPricingModifiers();
                priceModifier.ProhibitNonRefundableFares = request.RefundableFares;// All Refundable will be shown
                lfRequest.AirPricingModifiers = priceModifier;
                //AirLowFareSearchAsynchBinding binding = new AirLowFareSearchAsynchBinding();

            }
             //For AirlinePrivateFares Pass Account Codes 31 May 2016
            if(ConfigurationManager.AppSettings["UAPIAccountCodes"] != null)
            {
                string[] codes = ConfigurationManager.AppSettings["UAPIAccountCodes"].Split(',');
                AirPricingModifiers priceModifier = null;
                if (lfRequest.AirPricingModifiers != null)
                {
                    priceModifier = lfRequest.AirPricingModifiers;
                }
                else
                {
                    priceModifier = new AirPricingModifiers();
                }
                priceModifier.AccountCodeFaresOnly = false;
                priceModifier.AccountCodeFaresOnlySpecified = false;
                priceModifier.AccountCodes = new AccountCode[codes.Length];
                for (int i = 0; i < codes.Length; i++)
                {
                    priceModifier.AccountCodes[i] = new AccountCode();
                    priceModifier.AccountCodes[i].Code = codes[i];
                    priceModifier.AccountCodes[i].ProviderCode = "1G";
                    //priceModifier.AccountCodes[i].SupplierCode = "";
                    //priceModifier.AccountCodes[i].Type = "";
                }
                priceModifier.FaresIndicatorSpecified = true;
                //priceModifier.FaresIndicator = typeFaresIndicator.PublicAndPrivateFares;
                priceModifier.FaresIndicator = typeFaresIndicator.AllFares;

                lfRequest.AirPricingModifiers = priceModifier;
            }

             Airline.GetAllAirlinesList();

             Dictionary<string, System.Collections.ArrayList> AllAirlines = CT.Configuration.CacheData.AirlineList;
             List<string> RestrictedAirlines = new List<string>();

             foreach (KeyValuePair<string, System.Collections.ArrayList> airline in AllAirlines)
             {
                 if (!Convert.ToBoolean(airline.Value[22]))//UapiAllowed column value
                 { 
                     RestrictedAirlines.Add(airline.Key);
                 }
             }

             if (RestrictedAirlines.Count > 0)
             {
                 int restrictedCount = 0;
                 //Check whether FlyDubai is restricted by default
                 if (RestrictedAirlines.Contains("FZ"))
                 {                     
                     airModifiers.DisfavoredCarriers = new Carrier[RestrictedAirlines.Count];
                 }
                 else//If not restricted, add in the restriction list
                 {
                     restrictedCount = RestrictedAirlines.Count + 1;
                     airModifiers.DisfavoredCarriers = new Carrier[restrictedCount];
                 }

                 for (int i = 0; i < RestrictedAirlines.Count; i++)
                 {
                     airModifiers.DisfavoredCarriers[i] = new Carrier();
                     airModifiers.DisfavoredCarriers[i].Code = RestrictedAirlines[i];
                 }
                 if (airModifiers.DisfavoredCarriers.Length > RestrictedAirlines.Count)
                 {
                     airModifiers.DisfavoredCarriers[airModifiers.DisfavoredCarriers.Length - 1] = new Carrier();
                     airModifiers.DisfavoredCarriers[airModifiers.DisfavoredCarriers.Length - 1].Code = "FZ";
                 }
             }
             else
             {
                 airModifiers.DisfavoredCarriers = new Carrier[1];
                 airModifiers.DisfavoredCarriers[0] = new Carrier();
                 airModifiers.DisfavoredCarriers[0].Code = "FZ";
             }
            lfRequest.SolutionResult = true;            
            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(LowFareSearchReq));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, lfRequest); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object                
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\LFSearchReq.xml");
                string filePath = xmlPath + "FlightSearchRequest_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
            }

            
            
            //Air20.AirLowFareSearchBinding binding = new Air20.AirLowFareSearchBinding();
            //binding.Url = urlAir;
            //binding.Credentials = new NetworkCredential(UserName, Password);
            UAPIdll.WebAir15.AirLowFareSearchBinding binding = new UAPIdll.WebAir15.AirLowFareSearchBinding();
            binding.Url = urlAir;
            binding.Credentials = new NetworkCredential(UserName, Password);
            binding.PreAuthenticate = true;//to implement GZIP

            LowFareSearchRsp lfResponse = new LowFareSearchRsp();
            try
            {
                lfResponse = binding.service(lfRequest); //ziyad for local test                
            }
            catch (SoapException se)
            {
                throw new Exception(se.Message, se);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            ////----------------------Loading previous static xml-------------------------------
            //System.Xml.Serialization.XmlSerializer ser1 = new System.Xml.Serialization.XmlSerializer(typeof(LowFareSearchRsp));
            //StreamReader reader = new StreamReader(@"C:\Developments\Team\Check-Out\17-dec-2015\FlightSearchResponse_add2ea15-ada7-4858-a0e9-7cac7a3c7111_591_16122015_063311.xml");
            //LowFareSearchRsp lfResponse = ser1.Deserialize(reader) as LowFareSearchRsp;
            return lfResponse;
        }

        

       
        //private static void ReadFlightInfoResponse(LowFareSearchRsp lfResponse, ref List<FlightInfo> outboundflightList, ref List<FlightInfo> inboundflightList)
        private static void ReadFlightInfoResponse(LowFareSearchRsp lfResponse, ref Dictionary<string, FlightInfo> outboundflightList, ref Dictionary<string, FlightInfo> inboundflightList)
        {
            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(LowFareSearchRsp));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, lfResponse); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\LFSearchRes.xml");
                string filePath = xmlPath + "FlightSearchResponse_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
            }
            //List<FlightInfo> outBound = new List<FlightInfo>();
            //List<FlightInfo> inBound = new List<FlightInfo>();
            outboundflightList = new Dictionary<string, FlightInfo>() ;
            inboundflightList = new Dictionary<string, FlightInfo>() ;
            bool isError = false;
            //ResponseMessage[] lfMessage=lfResponse.ResponseMessage;
            //for (int x = 0; x < lfMessage.Length; x++)
            //{
            //    if (lfMessage[x].Type == ResponseMessageType.Error)
            //    {isError = true;break;}
            //}

            FlightDetails[] lfFlightDetails=lfResponse.FlightDetailsList;
            typeBaseAirSegment[] lfAirSegment=lfResponse.AirSegmentList;

            if(!isError)
            {
                int segGroup=0;
                for (int x = 0; x < lfAirSegment.Length; x++)
                {
                    //if (lfAirSegment[x].Carrier != "FZ")
                    {
                        segGroup = lfAirSegment[x].Group;
                        FlightInfo flight = new FlightInfo();
                        string segmentKey = lfAirSegment[x].Key;//keyGen(lfAirSegment[x].Key);
                        string flightKey = "";
                        flight.UapiSegmentRefKey = segmentKey;


                        FlightDetails lfFlight = null;

                        for (int i = 0; i < lfFlightDetails.Length; i++)
                        {
                            string tempflightKey = lfFlightDetails[i].Key;//keyGen(lfFlightDetails[i].Key);
                            //FlightDetails[] lfFlightLit = lfFlightDetails[0].Key;
                            //for (int f = 0; f < lfFlightDetails; f++)
                            //{

                            //}
                            if (tempflightKey == lfAirSegment[x].FlightDetailsRef[0].Key) //keyGen(lfAirSegment[x].FlightDetailsRef[0].Key))
                            {
                                //  lfFlight=lfAirSegment[x].
                                flightKey = lfAirSegment[x].FlightDetailsRef[0].Key;//keyGen(lfAirSegment[x].FlightDetailsRef[0].Key);
                                lfFlight = lfFlightDetails[i];
                                break;

                            }
                            //FlightDetails lfFlight=lfFlightDetails[i].ke
                        }

                        //flight.FlightId = flightKey;

                        flight.Origin = new Airport(lfAirSegment[x].Origin);
                        flight.Destination = new Airport(lfAirSegment[x].Destination);
                        flight.Airline = lfAirSegment[x].Carrier;
                        flight.FlightNumber = lfAirSegment[x].FlightNumber;

                        //Local Time
                        string GMTdiff = lfAirSegment[x].DepartureTime.IndexOf("+") != -1 ? "+" : "-";
                        string depTime = lfAirSegment[x].DepartureTime.Substring(0, lfAirSegment[x].DepartureTime.LastIndexOf(GMTdiff));
                        GMTdiff = lfAirSegment[x].ArrivalTime.IndexOf("+") != -1 ? "+" : "-";
                        string arrTime = lfAirSegment[x].ArrivalTime.Substring(0, lfAirSegment[x].ArrivalTime.LastIndexOf(GMTdiff));

                        //Assign CodeShareInfo
                        if (lfAirSegment[x].CodeshareInfo != null && lfAirSegment[x].CodeshareInfo.OperatingCarrier != null && lfAirSegment[x].CodeshareInfo.OperatingFlightNumber != null)
                        {
                            flight.OperatingCarrier = lfAirSegment[x].CodeshareInfo.OperatingCarrier + "-" + lfAirSegment[x].CodeshareInfo.OperatingFlightNumber + " " + lfAirSegment[x].CodeshareInfo.Value;
                        }

                        flight.DepartureTime = Convert.ToDateTime(depTime);
                        flight.ArrivalTime = Convert.ToDateTime(arrTime);

                        //GMt Time
                        //flight.DepartureTime = Convert.ToDateTime(lfAirSegment[x].DepartureTime);
                        //flight.ArrivalTime= Convert.ToDateTime(lfAirSegment[x].ArrivalTime);
                        // For UAPI REservation Request
                        flight.UapiDepartureTime = lfAirSegment[x].DepartureTime;
                        flight.UapiArrivalTime = lfAirSegment[x].ArrivalTime;


                        flight.Craft = lfAirSegment[x].Equipment;
                        int duration = Convert.ToInt32(lfAirSegment[x].FlightTime);
                        flight.Duration = new TimeSpan(0, duration, 0);
                        flight.DepTerminal = lfFlight.OriginTerminal;
                        flight.ArrTerminal = lfFlight.DestinationTerminal;
                        flight.ETicketEligible = lfAirSegment[x].ETicketabilitySpecified;
                        flight.Group = segGroup;
                        if (flight.UAPIReservationValues == null)
                        {
                            flight.UAPIReservationValues = new System.Collections.Hashtable();
                        }
                        flight.UAPIReservationValues[LINK_AVAILABILITY] = lfAirSegment[x].LinkAvailability;
                        if (lfAirSegment[x].AvailabilitySource != null)
                        {
                            flight.UAPIReservationValues[AVAILABILITY_SOURCE] = lfAirSegment[x].AvailabilitySource;
                        }
                        else
                        {
                            flight.UAPIReservationValues[AVAILABILITY_SOURCE] = "";
                        }
                        flight.UAPIReservationValues[POLLED_AVAILABILITY_OPTION] = lfAirSegment[x].PolledAvailabilityOption;
                        flight.UAPIReservationValues[PROVIDER_CODE] = lfAirSegment[x].AirAvailInfo[0].ProviderCode;
                        flight.UAPIReservationValues["AirSegment"] = lfAirSegment[x];//Storing the AirSegment for UAPI Repricing request


                        if (segGroup == 0)
                        {
                            outboundflightList.Add(segmentKey, flight);
                        }
                        else
                        {
                            inboundflightList.Add(segmentKey, flight);
                        }
                    }
                }
            }
        }

        private static SearchResult[] ReadFareResponse(LowFareSearchRsp lfResponse, SearchRequest request, Dictionary<string, FlightInfo> outboundflightList, Dictionary<string, FlightInfo> inboundflightList)
        {

            List<SearchResult> resultarray = new List<SearchResult>();
            List<string> resultKey = new List<string>();
            //List<FlightInfo> outbound = outboundflightList;
            //List<FlightInfo> inbound = inboundflightList;

            Dictionary<string, FlightInfo> outbound = outboundflightList;
            Dictionary<string, FlightInfo> inbound = inboundflightList;

            Dictionary<string, FareInfo> fareBasisCodeRef = new Dictionary<string, FareInfo>();
            Dictionary<string, string> fareBasisBaggage = new Dictionary<string, string>();
            //Dictionary<int, Air20.FareInfo> fareInfoDic = new Dictionary<int, Air20.FareInfo>();
            FareInfo[] fareInfoList = lfResponse.FareInfoList;
            for (int f = 0; fareInfoList.Length > f; f++)
            {
                FareInfo fareInfo = fareInfoList[f];
                string fareInfoKey = fareInfo.Key;//keyGen(fareInfo.Key);
                fareBasisCodeRef.Add(fareInfoKey, fareInfo);
                if (fareInfo.BaggageAllowance != null && fareInfo.BaggageAllowance.MaxWeight != null && fareInfo.BaggageAllowance.MaxWeight.Value != null)
                {
                    fareBasisBaggage.Add(fareInfoKey, fareInfo.BaggageAllowance.MaxWeight.Value);
                }
                else if (fareInfo.BaggageAllowance != null && fareInfo.BaggageAllowance.NumberOfPieces != null)
                {
                    fareBasisBaggage.Add(fareInfoKey, fareInfo.BaggageAllowance.NumberOfPieces + " piece");
                }
                else
                {
                    fareBasisBaggage.Add(fareInfoKey, "As per Airline");
                }
            }


            string currency = lfResponse.CurrencyType;

            if (ExchangeRates.ContainsKey(currency))
            {
                rateOfExchange = Convert.ToDouble(ExchangeRates[currency]);
            }
            else
            {
                rateOfExchange = 1;
            }

            //AirPricingSolution[] pricingSolutionList = (AirPricingSolution[])lfResponse.Items;
            Fare[] fare = new Fare[0];
            foreach (AirPricingSolution pricingSolution in lfResponse.Items) // AirPricingSolution
            {
                //AirPricingSolution RRpricingSolution = new AirPricingSolution();// TO Pass with Air Create Reservation Request
                //RRpricingSolution.airse

                bool nonRefundable = false;
                double totalResultFare = 0;
                double totalbaseFare = 0;
                double totalSupplierFare = 0;
                string lastTktDate = string.Empty;
                //pricingSolution.Journey[0].
                int fareKey = 0;
                double baseFare = 0;
                double totalFare = 0;
                double supplierFare = 0;

                fare = new Fare[pricingSolution.AirPricingInfo.Length];

                BookingInfo[] bookingInfoList = pricingSolution.AirPricingInfo[0].BookingInfo;
                int infoIndex = 0;
                List<FlightInfo> flightLists = new List<FlightInfo>();
                foreach (AirPricingInfo pricingInfo in pricingSolution.AirPricingInfo) //AirPricingInfo
                {
                    baseFare = getCurrAmount(pricingInfo.ApproximateBasePrice) * rateOfExchange;

                    totalFare = getCurrAmount(pricingInfo.TotalPrice) * rateOfExchange;
                    supplierFare = getCurrAmount(pricingInfo.TotalPrice);
                    //assigning local Time
                    string GMTdiff = pricingInfo.LatestTicketingTime.IndexOf("+") != -1 ? "+" : "-";
                    lastTktDate = pricingInfo.LatestTicketingTime.Substring(0, pricingInfo.LatestTicketingTime.LastIndexOf(GMTdiff));

                    //lastTktDate = pricingInfo.LatestTicketingTime;
                    nonRefundable = !pricingInfo.Refundable;
                    pricingInfo.FareInfo = new FareInfo[pricingInfo.FareInfoRef.Length];
                    int indexFareInfo = 0;
                    foreach (FareInfoRef tempFareInfo in pricingInfo.FareInfoRef)
                    {
                        pricingInfo.FareInfo[indexFareInfo] = fareBasisCodeRef[tempFareInfo.Key];
                        indexFareInfo++;

                    }

                    fare[infoIndex] = new Fare();

                    if (pricingInfo.PassengerType[0].Code == "ADT") // Adult
                    {
                        fare[infoIndex].PassengerType = PassengerType.Adult;
                        fare[infoIndex].PassengerCount = request.AdultCount;
                        fare[infoIndex].BaseFare = baseFare * request.AdultCount;
                        fare[infoIndex].TotalFare = totalFare * request.AdultCount;
                        fare[infoIndex].SupplierFare = (supplierFare) * request.AdultCount;
                    }
                    else if (pricingInfo.PassengerType[0].Code == "CNN")// Children
                    {
                        fare[infoIndex].PassengerType = PassengerType.Child;
                        fare[infoIndex].PassengerCount = request.ChildCount;
                        fare[infoIndex].BaseFare = baseFare * request.ChildCount;
                        fare[infoIndex].TotalFare = totalFare * request.ChildCount;
                        fare[infoIndex].SupplierFare = (supplierFare) * request.ChildCount;
                    }
                    else if (pricingInfo.PassengerType[0].Code == "INF")// Infant
                    {
                        fare[infoIndex].PassengerType = PassengerType.Infant;
                        fare[infoIndex].PassengerCount = request.InfantCount;
                        fare[infoIndex].BaseFare = baseFare * request.InfantCount;
                        fare[infoIndex].TotalFare = totalFare * request.InfantCount;
                        fare[infoIndex].SupplierFare = (supplierFare) * request.InfantCount;
                    }
                    else if (pricingInfo.PassengerType[0].Code == "SRC")// Senior citizen
                    {
                        fare[infoIndex].PassengerType = PassengerType.Senior;
                        fare[infoIndex].PassengerCount = request.SeniorCount;
                        fare[infoIndex].BaseFare = baseFare * request.SeniorCount;
                        fare[infoIndex].TotalFare = totalFare * request.SeniorCount;
                        fare[infoIndex].SupplierFare = (supplierFare) * request.SeniorCount;
                    }
                    totalbaseFare += baseFare * fare[infoIndex].PassengerCount;
                    totalResultFare += totalFare * fare[infoIndex].PassengerCount;
                    totalSupplierFare += (supplierFare) * fare[infoIndex].PassengerCount;
                    infoIndex++;
                }// Air Pricing Info End



                // Adding segments in single result
                List<FlightInfo[]> outFlights = new List<FlightInfo[]>();
                List<FlightInfo[]> inFlights = new List<FlightInfo[]>();

                // Adding Connection Details
                Connection[] connectionList = pricingSolution.Connection;
                //Adding Jopurney Details
                Journey[] journeyList = pricingSolution.Journey;
                int jIndex = 0;
                int segmentIndex = 0;
                foreach (Journey tempJourney in journeyList) // Journey Array
                {
                    FlightInfo[] flights = new FlightInfo[0];

                    AirSegmentRef[] jSegRefList = tempJourney.AirSegmentRef;
                    int segRefIndex = 0;
                    foreach (AirSegmentRef jSegRef in jSegRefList)
                    {
                        if (segRefIndex == 0)
                        {
                            flights = new FlightInfo[jSegRefList.Length];
                        }

                        string jSegRefKey = (jSegRef.Key);

                        if (!flightLists.Exists(delegate(FlightInfo fi) { return fi.UapiSegmentRefKey == jSegRefKey; }))
                        {
                            if (jIndex == 0)// outbound
                            {
                                //outbound[jSegRefKey].Group = "0";
                                try
                                {
                                    if (!flightLists.Exists(delegate(FlightInfo fi) { return fi.UapiSegmentRefKey == jSegRefKey; }))
                                    {
                                        flights[segRefIndex] = outbound[jSegRefKey];
                                        flightLists.Add(outbound[jSegRefKey]);
                                    }
                                }
                                catch { continue; }
                            }
                            else // return
                            {
                                //outbound[jSegRefKey].Group = "1";
                                try
                                {
                                    if (!flightLists.Exists(delegate(FlightInfo fi) { return fi.UapiSegmentRefKey == jSegRefKey; }))
                                    {
                                        flights[segRefIndex] = inbound[jSegRefKey];
                                        flightLists.Add(inbound[jSegRefKey]);
                                    }
                                }
                                catch { continue; }
                            }
                            string bookingClass = string.Empty;
                            string cabinClass = string.Empty;
                            string fareInfoKey = "";
                            string segmentRef = string.Empty;
                            // pricingSolution.AirSegment = new AirSegment[bookingInfoList.Length];// to assign airsegment in Create Reservation Pricing Info
                            int bkInfoIndex = 0;
                            foreach (BookingInfo bookingInfo in bookingInfoList)
                            {
                                if ((bookingInfo.SegmentRef) == jSegRefKey)
                                {
                                    bookingClass = bookingInfo.BookingCode;
                                    //typeCabinClass tempCabinClass = bookingInfo.CabinClass;
                                    cabinClass = bookingInfo.CabinClass.ToString();
                                    fareInfoKey = (bookingInfo.FareInfoRef);
                                    segmentRef = bookingInfo.SegmentRef;
                                    break;
                                    //Air20.FareInfo tempfareInfo = fareInfoDic[keyGen(bookingInfo.FareInfoRef)];
                                    //fareBasisCodeRef.Add(keyGen(bookingInfo.SegmentRef), tempfareInfo);
                                }
                            }
                            flights[segRefIndex].UAPIReservationValues[SEGMENT_REF] = segmentRef;
                            flights[segRefIndex].BookingClass = bookingClass;
                            flights[segRefIndex].CabinClass = cabinClass;
                            flights[segRefIndex].FareInfoKey = fareInfoKey;
                            //Adding Connection Details
                            if (connectionList != null && connectionList.Length > 0)
                            {
                                foreach (Connection tempConnection in connectionList) // Connection Array
                                {
                                    int conSegIndex = Convert.ToInt32(tempConnection.SegmentIndex);
                                    if (conSegIndex == segmentIndex)
                                    {
                                        flights[segRefIndex].StopOver = true; // Connection Flight
                                    }
                                    //else
                                    //{
                                    //    flights[segRefIndex].StopOver = false;
                                    //}


                                }

                            }

                            segRefIndex += 1;

                            if (segRefIndex == jSegRefList.Length)
                            {
                                if (jIndex == 0)// outbound
                                {
                                    outFlights.Add(flights);
                                }
                                else // return
                                {
                                    inFlights.Add(flights);
                                }

                            }
                            segmentIndex++;
                        }
                    }
                    jIndex++;


                }// Journey Array End


                if (inFlights.Count > 0) // return
                {

                    SearchResult result = new SearchResult();
                    result.UapiPricingSolution = pricingSolution;// for UAPI Create Reservatiobn Req.... to get Airfare with response
                    //result.UapiPricingSolution = new object();
                    result.FareBreakdown = fare;
                    result.TotalFare = totalResultFare;
                    result.BaseFare = totalbaseFare;
                    result.Price = new PriceAccounts();
                    result.Price.SupplierPrice = (decimal)totalSupplierFare;
                    result.Price.SupplierCurrency = currency;
                    result.Tax = totalResultFare - totalbaseFare;
                    result.LastTicketDate = lastTktDate;
                    result.Currency = agentBaseCurrency;
                    result.NonRefundable = nonRefundable;
                    //result.ResultBookingSource = BookingSource.Galileo;
                    result.ResultBookingSource = BookingSource.UAPI;
                    result.FareRules = new List<FareRule>();
                    result.Flights = new FlightInfo[2][];
                    //AirSegments need to be updated in UAPIPricingSolution for UAPI Repricing
                    List<typeBaseAirSegment> airSegments = new List<typeBaseAirSegment>();
                    result.Flights[0] = new FlightInfo[outFlights[0].Length];
                    for (int k = 0; k < outFlights[0].Length; k++)
                    {
                        result.Flights[0][k] = new FlightInfo();
                        outFlights[0][k].Stops = outFlights[0].Length - 1;
                        result.Flights[0][k] = FlightInfo.Copy(outFlights[0][k]);
                        //Adding outbound AirSegments to the list 
                        airSegments.Add((typeBaseAirSegment)result.Flights[0][k].UAPIReservationValues["AirSegment"]);
                    }
                    // including mutli search as well

                    int inFlightLength = 0;
                    for (int f = 0; f < inFlights.Count; f++)
                    {
                        inFlightLength = inFlightLength + inFlights[f].Length;
                    }

                    result.Flights[1] = new FlightInfo[inFlightLength];
                    inFlightLength = 0;// reassigning values
                    for (int f = 0; f < inFlights.Count; f++)
                    {
                        for (int k = 0; k < inFlights[f].Length; k++)
                        {
                            result.Flights[1][inFlightLength] = new FlightInfo();
                            inFlights[f][k].Stops = inFlights[f].Length - 1;
                            result.Flights[1][inFlightLength] = FlightInfo.Copy(inFlights[f][k]);
                            inFlightLength++;
                            airSegments.Add((typeBaseAirSegment)inFlights[f][k].UAPIReservationValues["AirSegment"]);
                        }
                    }
                    //Assigning all AirSegments to the UAPIPricingSolution 
                    result.UapiPricingSolution.AirSegment = airSegments.ToArray();
                    //result.Flights[1] = new FlightInfo[inFlights[0].Length];
                    //for (int k = 0; k < inFlights[0].Length; k++)
                    //{
                    //    result.Flights[1][k] = new FlightInfo();
                    //    inFlights[0][k].Stops = inFlights[0].Length - 1;
                    //    result.Flights[1][k] = FlightInfo.Copy(inFlights[0][k]);
                    //}

                    FlightInfo[] flightSegments = SearchResult.GetSegments(result);
                    for (int s = 0; s < flightSegments.Length; s++)
                    {
                        if (fareBasisCodeRef.ContainsKey(flightSegments[s].FareInfoKey))
                        {
                            flightSegments[s].TourCode = fareBasisCodeRef[flightSegments[s].FareInfoKey].TourCode;// Assiginf Tourcode for Corporate Negotiated Fare
                            FareRule fareRule = new FareRule();
                            fareRule.Airline = flightSegments[s].Airline;
                            fareRule.Destination = flightSegments[s].Destination.AirportCode;
                            fareRule.Origin = flightSegments[s].Origin.AirportCode;

                            FareInfo tempFareInfo = fareBasisCodeRef[flightSegments[s].FareInfoKey];
                            fareRule.FareBasisCode = tempFareInfo.FareBasis;

                            FareRuleKey tempFareRuleKey = tempFareInfo.FareRuleKey;
                            fareRule.FareRuleKeyValue = tempFareRuleKey.Value;
                            fareRule.FareInfoRef = (tempFareRuleKey.FareInfoRef);
                            if (result.BaggageIncludedInFare != null && result.BaggageIncludedInFare.Length > 0)
                            {
                                result.BaggageIncludedInFare += "," + fareBasisBaggage[flightSegments[s].FareInfoKey];
                            }
                            else
                            {
                                result.BaggageIncludedInFare = fareBasisBaggage[flightSegments[s].FareInfoKey];
                            }
                            // fareRule.FareBasisCode = fareBasisCodeRef[s + 1]; todo ziya
                            result.FareRules.Add(fareRule);
                        }
                    }
                    string key = BuildResultKey(result);
                    if (!resultKey.Contains(key))
                    {
                        if (IsValidResult(result, request))
                        {
                            resultarray.Add(result);
                            resultKey.Add(key);
                        }
                    }

                }
                else // One Way
                {
                    SearchResult result = new SearchResult();
                    result.UapiPricingSolution = pricingSolution;// for UAPI Create Reservatiobn Req.... to get Airfare with response
                    //result.UapiPricingSolution = new object();
                    result.FareBreakdown = fare;
                    result.TotalFare = totalResultFare;
                    result.BaseFare = totalbaseFare;
                    result.Price = new PriceAccounts();
                    result.Price.SupplierPrice = (decimal)totalSupplierFare;
                    result.Price.SupplierCurrency = currency;
                    result.Tax = totalResultFare - totalbaseFare;
                    result.LastTicketDate = lastTktDate;
                    result.Currency = agentBaseCurrency;
                    result.NonRefundable = nonRefundable;
                    //result.ResultBookingSource = BookingSource.Galileo;
                    result.ResultBookingSource = BookingSource.UAPI;                    
                    result.FareRules = new List<FareRule>();
                    result.Flights = new FlightInfo[1][];
                    result.Flights[0] = new FlightInfo[outFlights[0].Length];
                    result.UapiPricingSolution.AirSegment = new typeBaseAirSegment[outFlights[0].Length];
                    for (int j = 0; j < outFlights[0].Length; j++)
                    {
                        result.Flights[0][j] = new FlightInfo();
                        result.Flights[0][j] = FlightInfo.Copy(outFlights[0][j]);
                        //Adding outbound segments directly to UAPIPricingSolution for Repricing
                        result.UapiPricingSolution.AirSegment[j] = outFlights[0][j].UAPIReservationValues["AirSegment"] as typeBaseAirSegment;
                    }

                    FlightInfo[] flightSegments = SearchResult.GetSegments(result);
                    for (int s = 0; s < flightSegments.Length; s++)
                    {
                        if (fareBasisCodeRef.ContainsKey(flightSegments[s].FareInfoKey))
                        {
                            flightSegments[s].TourCode = fareBasisCodeRef[flightSegments[s].FareInfoKey].TourCode;
                            FareRule fareRule = new FareRule();
                            fareRule.Airline = flightSegments[s].Airline;
                            fareRule.Destination = flightSegments[s].Destination.AirportCode;
                            fareRule.Origin = flightSegments[s].Origin.AirportCode;

                            FareInfo tempFareInfo = fareBasisCodeRef[flightSegments[s].FareInfoKey];
                            fareRule.FareBasisCode = tempFareInfo.FareBasis;

                            FareRuleKey tempFareRuleKey = tempFareInfo.FareRuleKey;
                            fareRule.FareRuleKeyValue = tempFareRuleKey.Value;
                            fareRule.FareInfoRef = (tempFareRuleKey.FareInfoRef);

                            result.BaggageIncludedInFare = fareBasisBaggage[flightSegments[s].FareInfoKey];
                            // fareRule.FareBasisCode = fareBasisCodeRef[s + 1]; todo ziya
                            result.FareRules.Add(fareRule);
                        }
                    }
                    string key = BuildResultKey(result);
                    if (!resultKey.Contains(key))
                    {
                        if (IsValidResult(result, request))
                        {
                            resultarray.Add(result);
                            resultKey.Add(key);
                        }
                    }
                }

                flightLists.Clear();
            } // Air Pricing Solution End



            return resultarray.ToArray();
        }

        private static string BuildResultKey(SearchResult result)
        {
            int obLength = result.Flights[0].Length;
            StringBuilder key = new StringBuilder();
            key.Append(result.Flights[0][0].DepartureTime.DayOfYear.ToString());
            key.Append(result.Flights[0][0].DepartureTime.ToString("HHmm"));
            key.Append(result.Flights[0][obLength - 1].ArrivalTime.DayOfYear.ToString());
            key.Append(result.Flights[0][obLength - 1].ArrivalTime.ToString("HHmm"));
            if (result.Flights.Length > 1)
            {
                int ibLength = result.Flights[1].Length;
                key.Append(result.Flights[1][0].DepartureTime.DayOfYear.ToString());
                key.Append(result.Flights[1][0].DepartureTime.ToString("HHmm"));
                key.Append(result.Flights[1][ibLength - 1].ArrivalTime.DayOfYear.ToString());
                key.Append(result.Flights[1][ibLength - 1].ArrivalTime.ToString("HHmm"));
            }
            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                {
                    key.Append(result.Flights[i][j].FlightNumber);
                    key.Append(result.Flights[i][j].BookingClass);
                }
            }
            return key.ToString();
        }
        private static bool IsValidResult(SearchResult result, SearchRequest request)
        {
            bool isValid = false;
            for (int i = 0; i < result.Flights.Length; i++)
            {
                if (i == 0)
                {
                    if (((result.Flights[i][0].Origin.CityCode == request.Segments[0].Origin) || (result.Flights[i][0].Origin.AirportCode == request.Segments[0].Origin)) && ((result.Flights[i][result.Flights[i].Length - 1].Destination.CityCode == request.Segments[0].Destination) || (result.Flights[i][result.Flights[i].Length - 1].Destination.AirportCode == request.Segments[0].Destination)))
                    {
                        isValid = true;
                    }
                }
                else
                {
                    if (((result.Flights[i][0].Origin.CityCode == request.Segments[0].Destination) || (result.Flights[i][0].Origin.AirportCode == request.Segments[0].Destination)) && ((result.Flights[i][result.Flights[i].Length - 1].Destination.CityCode == request.Segments[0].Origin) || (result.Flights[i][result.Flights[i].Length - 1].Destination.AirportCode == request.Segments[0].Origin)))
                    {
                        isValid = true;
                    }
                }
            }
            return isValid;
        }
    # endregion

        # region Fare Rules

        public static List<FareRule> GetFareRuleList(List<FareRule> fareRuleList)
        {
            Connection();

            AirFareRulesReq fareRulereq = new AirFareRulesReq();
            fareRulereq.TargetBranch = TargetBranch;
            fareRulereq.FareRuleType = typeFareRuleType.@long;

            BillingPointOfSaleInfo pos = new BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            fareRulereq.BillingPointOfSaleInfo = pos;

            fareRulereq.Items=new FareRuleKey[fareRuleList.Count];
            for (int i = 0; i < fareRuleList.Count; i++)
            {
                


                FareRuleKey fRuleKey = new FareRuleKey();

                string tempFareBasisCode = fareRuleList[i].FareBasisCode;
                string tempFareInfoRef = string.Empty;
                string tempFareRuleKeyValue = string.Empty;
                if (tempFareBasisCode.IndexOf('-') != -1) // From UAPI - FareInfo Key & FareInfo Key Value
                {
                    string[] fareInfo = tempFareBasisCode.Split('-');

                    if (fareInfo.Length > 1)
                    {
                        //fRule.FareBasisCode = fareInfo[0];
                        tempFareInfoRef = fareInfo[1];
                        tempFareRuleKeyValue = fareInfo[2];
                    }
                }
                else
                {
                    tempFareInfoRef = Convert.ToString(fareRuleList[i].FareInfoRef);
                    tempFareRuleKeyValue = fareRuleList[i].FareRuleKeyValue; 

                    //fRule.FareBasisCode = tempFareBasisCode;
                }
                //Guid result;
                //using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
                //{
                //    byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(tempFareInfoRef + "T"));
                //    result = new Guid(hash);                    
                //}
                fRuleKey.FareInfoRef = tempFareInfoRef;//result.ToString(); //tempFareInfoRef + "T";
                fRuleKey.ProviderCode = "1G"; // TODO-- add Dynamic Provider
                fRuleKey.Value = tempFareRuleKeyValue;

                fareRulereq.Items[i] = new FareRuleKey();
                fareRulereq.Items[i] = fRuleKey;





                //fRuleKey.FareInfoRef = Convert.ToString(fareRuleList[i].FareInfoRef)+"T";
                //fRuleKey.ProviderCode = "1G"; // TODO-- add Dynamic Provider
                //fRuleKey.Value = fareRuleList[i].FareRuleKeyValue;

                //fareRulereq.FareRuleKey[i] = new FareRuleKey();
                //fareRulereq.FareRuleKey[i] = fRuleKey;

            }

            // To remove
            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer serReq = new System.Xml.Serialization.XmlSerializer(fareRulereq.GetType());
                System.Text.StringBuilder sbReq = new System.Text.StringBuilder();
                System.IO.StringWriter writerReq = new System.IO.StringWriter(sbReq);
                serReq.Serialize(writerReq, fareRulereq); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docReq = new XmlDocument();
                docReq.LoadXml(sbReq.ToString());
                //docReq.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirFareRulesReq .xml");
                string filePath = xmlPath + "FlightFareRuleRequest_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                docReq.Save(filePath);
            }
            
            AirFareRulesBinding binding = new AirFareRulesBinding();
            binding.Url = urlAir;
            binding.Credentials = new NetworkCredential(UserName, Password);
            AirFareRulesRsp fareRuleResponse = binding.service(fareRulereq);

            // startTo Remove
            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(fareRuleResponse.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, fareRuleResponse); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());

                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirFareRulesRsp.xml");
                string filePath = xmlPath + "FlightFareRuleResponse_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
                // end To Remove
            }
            // Fare Rule Response
            UAPIdll.WebAir15.FareRule[] fareRulesResultList = fareRuleResponse.FareRule;
            foreach (UAPIdll.WebAir15.FareRule fareRulesResult in fareRulesResultList)
            {
                //string category = string.Empty;
                string fareruleValue = string.Empty;
                //int farInfoRef = keyGen(fareRulesResult.FareInfoRef);
                FareRuleLong[] fareRuleslongList = fareRulesResult.FareRuleLong;
                if (fareRuleslongList!=null &&fareRuleslongList.Length > 0)
                {
                    //foreach (FareRuleLong fareRuleLong in fareRuleslongList)
                    //{
                    //    fareruleValue += "<br>" + fareRuleLong.Value;
                    //}
                    /****************************************************************************************
                     *    Loading Fare Rules based on Category. Default Penalties will be loaded first.
                     * **************************************************************************************/
                    try
                    {
                        List<FareRuleLong> fareRules = new List<FareRuleLong>();
                        fareRules.AddRange(fareRuleslongList);
                        FareRuleLong rule = fareRules.Find(delegate(FareRuleLong f) { return f.Category == "16"; });//Penalties

                        fareruleValue = "<p style='display:block;float:left;font-style:normal;	background:#18407B;	color:#fff;	padding:1px 2px 0 2px;margin-top:-1px;margin-left:0;margin-right:5px;border-radius:3px;'><b>" + rule.Category + "</b>&nbsp;<b>" + rule.Value.Substring(0, rule.Value.IndexOf("\n")) + "</b></p><br/>";
                        fareruleValue += "<p style='border:1px solid #18407B;margin-right:10px;'>" + rule.Value.Substring(rule.Value.IndexOf("\n") + 1) + "</p>";
                        fareRules = null;
                    }
                    catch { }

                    foreach (FareRuleLong fareRuleLong in fareRuleslongList)
                    {
                        if (fareRuleLong.Category != "16")//Avoid repeating
                        {
                            if (fareruleValue.Length > 0)
                            {
                                fareruleValue += "<p style='display:block;float:left;font-style:normal;	background:#18407B;	color:#fff;	padding:1px 2px 0 2px;margin-top:-1px;margin-left:0;margin-right:5px;border-radius:3px;'><b>" + fareRuleLong.Category + "</b>&nbsp;<b>" + fareRuleLong.Value.Substring(0, fareRuleLong.Value.IndexOf("\n")) + "</b></p><br/>";
                                fareruleValue += "<p style='border:1px solid #18407B;margin-right:10px;'>" + fareRuleLong.Value.Substring(fareRuleLong.Value.IndexOf("\n") + 1) + "</p>";
                            }
                            else
                            {
                                fareruleValue = "<p style='display:block;float:left;font-style:normal;	background:#18407B;	color:#fff;	padding:1px 2px 0 2px;margin-top:-1px;margin-left:0;width:100%;	text-align:center;	margin-right:5px;border-radius:3px;'><em>" + fareRuleLong.Category + "</em><b>" + fareRuleLong.Value.Substring(0, fareRuleLong.Value.IndexOf("\n")) + "</b></p>";
                                fareruleValue += "<p>" + fareRuleLong.Value.Substring(fareRuleLong.Value.IndexOf("\n") + 1) + "</p>";
                            }
                        }
                    }


                    for (int i = 0; i < fareRuleList.Count; i++)
                    {
                        //Guid result;
                        //System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
                        //byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(fareRuleList[i].FareInfoRef + "T"));
                        //result = new Guid(hash);

                        if (fareRuleList[i].FareInfoRef == fareRulesResult.FareInfoRef)
                        {
                            fareRuleList[i].FareRuleDetail = fareruleValue;
                        }
                    }
                }
            }

            return fareRuleList;
        }
        # endregion

        # region Book

        /// <summary>
        /// Method to book a selected Itinerary
        /// </summary>
        /// <param name="itinerary">An object of FlightItinerary</param>
        /// <returns>BookingResponse</returns>
        public static BookingResponse Book(CT.BookingEngine.FlightItinerary itinerary, string sessionId, out List<SegmentPTCDetail> ptcDetail ,UAPICredentials upaiCredentials)
        {
            BookingResponse bookingResponse = new BookingResponse();
            bool isDomestic = itinerary.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
            UAPIdll.WebUR15.AirCreateReservationRsp reservationRsp=new UAPIdll.WebUR15.AirCreateReservationRsp();
            reservationRsp = CreateReservation(itinerary, upaiCredentials);
            ptcDetail = new List<SegmentPTCDetail>();

            // To Remove
            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.WebUR15.AirCreateReservationRsp));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, reservationRsp); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirReservationRes .xml");
                string filePath = xmlPath+"FlightReservationResponse_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
                //

            }
            Audit.Add(EventType.Book, Severity.Normal, 0, "UAPI Create Reservation Response. : ", string.Empty);
            bool booked=true;

            

            //Checking Error
            UAPIdll.WebUR15.AirSegmentError[] sellFailureErr = reservationRsp.AirSegmentSellFailureInfo;
            if (sellFailureErr != null)
            {
                booked = false;
                
                Basket.FlightBookingSession[sessionId].Log.Add("UAPI Sell Failure Error.");
                bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "SEAT NOT AVAILABLE -" + sellFailureErr[0].ErrorMessage, "");
                Audit.Add(EventType.Book, Severity.High, 0, "Booking Failed UAPI. Sell Failure Error :" + sellFailureErr[0].ErrorMessage, string.Empty);
            }
            else if (reservationRsp.AirSolutionChangedInfo != null && reservationRsp.AirSolutionChangedInfo.Length>0) // Price/Schedule Changed - if yes, pnr will not generate
            {
                booked = false;

                Basket.FlightBookingSession[sessionId].Log.Add("UAPI Air Soulution Changed Info.ReasonCode:" + reservationRsp.AirSolutionChangedInfo[0].ReasonCode.ToString());
                if (reservationRsp.ResponseMessage != null)
                {
                    bookingResponse = new BookingResponse(BookingResponseStatus.Failed, reservationRsp.ResponseMessage[0].Value, string.Empty);
                    Audit.Add(EventType.Book, Severity.High, 0, "Booking Failed UAPI:(" + reservationRsp.AirSolutionChangedInfo[0].ReasonCode.ToString() + "):" + reservationRsp.ResponseMessage[0].Value, string.Empty);
                }
                else
                {
                    bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "Price/Schedule Changed", string.Empty);
                    Audit.Add(EventType.Book, Severity.High, 0, "Booking Failed UAPI:Price/Schedule Changed:" + reservationRsp.AirSolutionChangedInfo[0].ReasonCode.ToString(), string.Empty);
                }
                
                
            }
            else
            {
                
                UAPIdll.WebUR15.UniversalRecord unRecord = reservationRsp.UniversalRecord;
                UAPIdll.WebUR15.AirReservation[] airReservationList = unRecord.Items;
                if (airReservationList == null)
                    booked = false;
                foreach (UAPIdll.WebUR15.AirReservation airReservation in airReservationList)
                {
                    booked = ReadBookingStatus(airReservation);

                }
                if (booked)
                {
                    bookingResponse = ReadBookResponse(reservationRsp, itinerary, out ptcDetail);
                    Audit.Add(EventType.Book, Severity.High, 0, "Book UAPI.booking Response from ReadBookResponse ", string.Empty); 

                }
                else
                {
                    
                    
                    Basket.FlightBookingSession[sessionId].Log.Add("UAPI Booking Failed.");
                    bookingResponse = new BookingResponse(BookingResponseStatus.Failed, "SEAT NOT AVAILABLE", "");
                    Audit.Add(EventType.Book, Severity.High, 0, "Booking Failed UAPI. Booked Status : false", string.Empty);
                }

            }

            if (booked == false)
            {
                if (reservationRsp.UniversalRecord != null)
                {
                    if (!string.IsNullOrEmpty(reservationRsp.UniversalRecord.LocatorCode))
                    {
                        CancelItinerary(reservationRsp.UniversalRecord.LocatorCode, "UR");
                    }
                }
            }

            return bookingResponse;

        }

        private static UAPIdll.WebUR15.AirCreateReservationRsp CreateReservation(FlightItinerary itinerary,UAPICredentials _uapiCredentials)
        {
            UAPIdll.WebUR15.AirCreateReservationRsp reservationRsp = new UAPIdll.WebUR15.AirCreateReservationRsp();
            try
            {
                Connection();
                List<KeyValuePair<string, SSR>> ssrList = GenerateSSRPaxList(itinerary);
                
                // for mutli city booking 
                UAPIdll.WebUR15.AirCreateReservationReq createRequest = new UAPIdll.WebUR15.AirCreateReservationReq();
                UAPIdll.WebUR15.ContinuityCheckOverride continuityCheck = new UAPIdll.WebUR15.ContinuityCheckOverride();
                continuityCheck.Key = "Yes";
                continuityCheck.Value = "Yes";
                createRequest.ContinuityCheckOverride = continuityCheck;


                // createRequest.TargetBranch = TargetBranch;
                createRequest.TargetBranch = _uapiCredentials.TargetBranch;// MOD by ziyad for target branch clash
                createRequest.RetainReservation = UAPIdll.WebUR15.typeRetainReservation.None;// If schedule/Price Change.... PNR should not be created

                UAPIdll.WebUR15.BillingPointOfSaleInfo pos = new UAPIdll.WebUR15.BillingPointOfSaleInfo();
                pos.OriginApplication = originalApplication;
                createRequest.BillingPointOfSaleInfo = pos;

                createRequest.BookingTraveler = new UAPIdll.WebUR15.BookingTraveler[itinerary.Passenger.Length];
                int paxIndex = 0;
                foreach (FlightPassenger passenger in itinerary.Passenger)
                {
                    string passengerType = string.Empty;
                    // Asigning Pax Type
                    if (passenger.Type == PassengerType.Adult)
                        passengerType = "ADT";
                    else if (passenger.Type == PassengerType.Child)
                        passengerType = "CNN";
                    else if (passenger.Type == PassengerType.Infant)
                        passengerType = "INF";
                    else if (passenger.Type == PassengerType.Senior)
                        passengerType = "SRC";
                    UAPIdll.WebUR15.BookingTraveler tmpBkTraveler = new UAPIdll.WebUR15.BookingTraveler();
                    tmpBkTraveler.Key = paxIndex.ToString();
                    tmpBkTraveler.TravelerType = passengerType;

                    string tmpGender = "M";
                    if (passenger.Gender == Gender.Male)
                        tmpGender = "M";
                    else if (passenger.Gender == Gender.Female)
                        tmpGender = "F";
                    tmpBkTraveler.Gender = tmpGender;
                    tmpBkTraveler.DOB = passenger.DateOfBirth.ToString("yyyy-MM-dd");
                    tmpBkTraveler.DOBSpecified = true;
                    // adding SSR
                    tmpBkTraveler.SSR = new UAPIdll.WebUR15.SSR[ssrList.Count];
                    if (passenger.Type != PassengerType.Child && passenger.Type != PassengerType.Infant)
                    {
                        int ssrPaxIndex = 0;
                        for (int x = 0; x < ssrList.Count; x++)
                        {
                            KeyValuePair<string, SSR> ssrPax = ssrList[x];

                            //if (!string.IsNullOrEmpty(ssrList[paxIndex].Key))
                            if (ssrPax.Key == passenger.PaxKey)
                            {
                                //KeyValuePair<string, SSR> ssrPax = ssrPaxList[paxIndex];
                                SSR tmpSSR = ssrPax.Value;
                                UAPIdll.WebUR15.SSR airSSR = new UAPIdll.WebUR15.SSR();
                                airSSR.Type = tmpSSR.SsrCode;
                                airSSR.FreeText = tmpSSR.Detail;

                                tmpBkTraveler.SSR[ssrPaxIndex] = airSSR;
                                ssrPaxIndex++;
                            }

                            //List<SSR> ssrPaxList = new List<SSR>();
                        }
                    }
                    //ssrPaxList.Add(

                    //Air20.SSR[1] airSSRList = new Air20.SSR();




                    UAPIdll.WebUR15.BookingTravelerName tmpTravelrName = new UAPIdll.WebUR15.BookingTravelerName();
                    tmpTravelrName.Prefix = passenger.Title;
                    tmpTravelrName.First = passenger.FirstName;
                    tmpTravelrName.Last = passenger.LastName;
                    tmpBkTraveler.BookingTravelerName = tmpTravelrName;

                    UAPIdll.WebUR15.PhoneNumber tmpPhone = new UAPIdll.WebUR15.PhoneNumber();
                    tmpPhone.Type = UAPIdll.WebUR15.PhoneNumberType.Mobile;
                    tmpPhone.TypeSpecified = true;
                    //tmpPhone.Number = passenger.CellPhone;
                    tmpPhone.Number = itinerary.Passenger[0].CellPhone;
                    tmpBkTraveler.PhoneNumber = new UAPIdll.WebUR15.PhoneNumber[1];
                    tmpBkTraveler.PhoneNumber[0] = new UAPIdll.WebUR15.PhoneNumber();
                    tmpBkTraveler.PhoneNumber[0] = tmpPhone;


                    //New Node added on 17 Nov 2015 as informed by Ziyad
                    UAPIdll.WebUR15.AgencyContactInfo agencyContact = new UAPIdll.WebUR15.AgencyContactInfo();
                    agencyContact.PhoneNumber = new UAPIdll.WebUR15.PhoneNumber[1];
                    agencyContact.PhoneNumber[0] = new UAPIdll.WebUR15.PhoneNumber();
                    agencyContact.PhoneNumber[0].Number = itinerary.Segments[0].UAPIReservationValues[PARENT_AGENT_PHONE].ToString();
                    agencyContact.PhoneNumber[0].Type = UAPIdll.WebUR15.PhoneNumberType.Agency;
                    agencyContact.PhoneNumber[0].TypeSpecified = true;
                    agencyContact.PhoneNumber[0].Location = itinerary.Segments[0].UAPIReservationValues[PARENT_AGENT_LOCATION].ToString();
                    agencyContact.PhoneNumber[0].Text = "COZMO TRAVEL/pax/" + itinerary.Passenger[0].CellPhone;
                    agencyContact.PhoneNumber[0].Key = "125155";

                    createRequest.AgencyContactInfo = agencyContact;

                    if (!string.IsNullOrEmpty(itinerary.Passenger[0].Email))
                    {
                        UAPIdll.WebUR15.Email tmpEmail = new UAPIdll.WebUR15.Email();
                        tmpEmail.Type = "Home";
                        tmpEmail.EmailID = itinerary.Passenger[0].Email;
                        tmpBkTraveler.Email = new UAPIdll.WebUR15.Email[1];
                        tmpBkTraveler.Email[0] = new UAPIdll.WebUR15.Email();
                        tmpBkTraveler.Email[0] = tmpEmail;
                    }
                    //if (paxIndex == 0)
                    //{
                    // adding pax Addres 
                    UAPIdll.WebUR15.typeStructuredAddress tmpAdds = new UAPIdll.WebUR15.typeStructuredAddress();
                    tmpAdds.AddressName = passenger.AddressLine1;
                    //if (!string.IsNullOrEmpty(passenger.AddressLine2))
                    //{
                    //    tmpAdds.Street = new string[1];
                    //    tmpAdds.Street[0] = passenger.AddressLine2;
                    //}
                    //if (!string.IsNullOrEmpty(passenger.AddressLine2))
                    //{
                    //    tmpAdds.City = passenger.City;
                    //}
                    //if (!string.IsNullOrEmpty(passenger.AddressLine2))
                    //{
                    //    State tmpState = new State();
                    //    tmpState.Value = passenger.City;
                    //    tmpAdds.State = tmpState;
                    //}  For timebeing assiginh addresline1

                    if (!string.IsNullOrEmpty(passenger.AddressLine1))
                    {
                        tmpAdds.Street = new string[1];
                        tmpAdds.Street[0] = passenger.AddressLine1;
                    }
                    if (!string.IsNullOrEmpty(passenger.AddressLine1) || !string.IsNullOrEmpty(passenger.City))
                    {
                        if (!string.IsNullOrEmpty(passenger.City)) tmpAdds.City = passenger.City;
                        else tmpAdds.City = passenger.AddressLine1;
                    }
                    if (!string.IsNullOrEmpty(passenger.AddressLine1) || !string.IsNullOrEmpty(passenger.City))
                    {
                        UAPIdll.WebUR15.State tmpState = new UAPIdll.WebUR15.State();
                        if (!string.IsNullOrEmpty(passenger.City)) tmpState.Value = passenger.City;
                        else tmpState.Value = passenger.AddressLine1;
                        tmpAdds.State = tmpState;
                    }

                    //tmpAdds.PostalCode = "0097160";// To DO ziyad-- validating UAPI based on Country
                    tmpAdds.Country = passenger.Country.CountryCode;

                    tmpBkTraveler.Address = new UAPIdll.WebUR15.typeStructuredAddress[1];
                    tmpBkTraveler.Address[0] = new UAPIdll.WebUR15.typeStructuredAddress();
                    tmpBkTraveler.Address[0] = tmpAdds;


                    // adding pax Loyalty Card details
                    if (!string.IsNullOrEmpty(passenger.FFAirline) && !string.IsNullOrEmpty(passenger.FFNumber))
                    {
                        string[] tmpFFAirlines = passenger.FFAirline.Split(',');
                        string[] tmpFFNumbers = passenger.FFNumber.Split(',');
                        UAPIdll.WebUR15.LoyaltyCard[] lyCardList = new UAPIdll.WebUR15.LoyaltyCard[tmpFFAirlines.Length];
                        for (int x = 0; x < tmpFFAirlines.Length - 1; x++)
                        {
                            if (!string.IsNullOrEmpty(tmpFFNumbers[x]))
                            {
                                UAPIdll.WebUR15.LoyaltyCard lyCard = new UAPIdll.WebUR15.LoyaltyCard();
                                lyCard.SupplierCode = tmpFFAirlines[x];
                                lyCard.CardNumber = tmpFFNumbers[x];
                                lyCardList[x] = lyCard;
                            }

                        }
                        tmpBkTraveler.LoyaltyCard = lyCardList;

                    }

                    //}
                    createRequest.BookingTraveler[paxIndex] = new UAPIdll.WebUR15.BookingTraveler();
                    createRequest.BookingTraveler[paxIndex] = tmpBkTraveler;
                    paxIndex++;
                }




                itinerary.UapiPricingSolution.Journey = null;
                itinerary.UapiPricingSolution.LegRef = null;
                int paxRef = 0;
                foreach (UAPIdll.WebAir15.AirPricingInfo tempPriceinfo in itinerary.UapiPricingSolution.AirPricingInfo)
                {
                    tempPriceinfo.FareInfoRef = null;

                    foreach (UAPIdll.WebAir15.FareInfo tempfareInfo in tempPriceinfo.FareInfo)
                    {
                        tempfareInfo.FareSurcharge = null;
                        tempfareInfo.FareRuleKey = null;
                        tempfareInfo.BaggageAllowance = null;

                    }
                    //int count = 0;
                    foreach (UAPIdll.WebAir15.BookingInfo tempBookingInfo in tempPriceinfo.BookingInfo)
                    {
                        //tempBookingInfo.SegmentRef= itinerary.Segments[count].UAPIReservationValues[SEGMENT_REF].ToString();
                        //count++;
                    }

                    foreach (UAPIdll.WebAir15.PassengerType tempPaxType in tempPriceinfo.PassengerType)
                    {
                        tempPaxType.BookingTravelerRef = paxRef.ToString(); ;
                        paxRef++;

                    }
                }

                itinerary.UapiPricingSolution.AirSegment = new typeBaseAirSegment[itinerary.Segments.Length];
                int segIndex = 0;
                foreach (FlightInfo flight in itinerary.Segments)
                {
                    string airSegKey = Convert.ToString(flight.UapiSegmentRefKey);// + "T";

                    typeBaseAirSegment tmpAirSegment = new typeBaseAirSegment();
                    tmpAirSegment.Key = airSegKey;
                    tmpAirSegment.Group = flight.Group;
                    tmpAirSegment.Carrier = flight.Airline;
                    tmpAirSegment.FlightNumber = flight.FlightNumber;
                    tmpAirSegment.Origin = flight.Origin.AirportCode;
                    tmpAirSegment.Destination = flight.Destination.AirportCode;
                    //tmpAirSegment.DepartureTime = flight.DepartureTime.ToString("yyyy-MM-dd");  To get Schedule Error
                    //tmpAirSegment.DepartureTime = flight.DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss");
                    //tmpAirSegment.ArrivalTime = flight.ArrivalTime.ToString("yyyy-MM-dd'T'HH:mm:ss"); 
                    tmpAirSegment.DepartureTime = flight.UapiDepartureTime;
                    tmpAirSegment.ArrivalTime = flight.UapiArrivalTime;

                    //==============Set values based on the search response====================//
                    tmpAirSegment.LinkAvailability = Convert.ToBoolean(flight.UAPIReservationValues[LINK_AVAILABILITY]);
                    tmpAirSegment.LinkAvailabilitySpecified = true;
                    tmpAirSegment.PolledAvailabilityOption = flight.UAPIReservationValues[POLLED_AVAILABILITY_OPTION].ToString();
                    tmpAirSegment.AvailabilitySource = flight.UAPIReservationValues[AVAILABILITY_SOURCE].ToString();
                    //tmpAirSegment.AvailabilitySourceSpecified = true;
                    tmpAirSegment.PolledAvailabilityOption = "Polled avail exists";
                    tmpAirSegment.ProviderCode = flight.UAPIReservationValues[PROVIDER_CODE].ToString();
                    //=================================End=====================================//

                    tmpAirSegment.ClassOfService = flight.BookingClass;

                    itinerary.UapiPricingSolution.AirSegment[segIndex] = new typeBaseAirSegment();
                    itinerary.UapiPricingSolution.AirSegment[segIndex] = tmpAirSegment;
                    segIndex++;
                    if (flight.StopOver)
                    {
                        tmpAirSegment.Connection = new Connection();

                    }
                }
                itinerary.UapiPricingSolution.OptionalServices = null;
                itinerary.UapiPricingSolution.PricingDetails = null;
                itinerary.UapiPricingSolution.AvailableSSR = null;
                createRequest.AirPricingSolution = new UAPIdll.WebUR15.AirPricingSolution();

                //createRequest.AirPricingSolution = (UAPIdll.WebUR15.AirPricingSolution)UAPIdll.Converter.Convert(itinerary.UapiPricingSolution, typeof(UAPIdll.WebUR15.AirPricingSolution));
                PropertiesCopier.CopyProperties(itinerary.UapiPricingSolution, createRequest.AirPricingSolution);
                PropertiesCopier.CheckRecursiveDifferentTypes(itinerary.UapiPricingSolution, createRequest.AirPricingSolution);

                //Assign Master AccountCodes for AirlinePrivateFare booking if AccountCode is returned from Search Response
                UAPIdll.WebUR15.AirPricingInfo pricingInfo = createRequest.AirPricingSolution.AirPricingInfo[0];//Take always first Adult PricingInfo
                if (pricingInfo != null && pricingInfo.PricingMethod == UAPIdll.WebUR15.typePricingMethod.GuaranteedUsingAirlinePrivateFare && pricingInfo.FareInfo != null && pricingInfo.FareInfo.Length > 0 && pricingInfo.FareInfo[0].AccountCode != null && pricingInfo.FareInfo[0].AccountCode.Length > 0)
                {
                    pricingInfo.AirPricingModifiers = new UAPIdll.WebUR15.AirPricingModifiers();
                    //pricingInfo.AirPricingModifiers.AccountCodes = new UAPIdll.WebUR15.AccountCode[1];
                    //pricingInfo.AirPricingModifiers.AccountCodes[0] = new UAPIdll.WebUR15.AccountCode();
                    //pricingInfo.AirPricingModifiers.AccountCodes[0].Code = pricingInfo.FareInfo[0].AccountCode[0].Code;
                    //pricingInfo.AirPricingModifiers.AccountCodes[0].ProviderCode = pricingInfo.ProviderCode;
                    //if (pricingInfo.FareInfo[0].AccountCode[0].SupplierCode != null && pricingInfo.FareInfo[0].AccountCode[0].SupplierCode.Length > 0)
                    //{
                    //    pricingInfo.AirPricingModifiers.AccountCodes[0].SupplierCode = pricingInfo.FareInfo[0].AccountCode[0].SupplierCode;
                    //}
                    //if (pricingInfo.FareInfo[0].AccountCode[0].Type != null && pricingInfo.FareInfo[0].AccountCode[0].Type.Length > 0)
                    //{
                    //    pricingInfo.AirPricingModifiers.AccountCodes[0].Type = pricingInfo.FareInfo[0].AccountCode[0].Type;
                    //}

                    string[] codes = ConfigurationManager.AppSettings["UAPIAccountCodes"].Split(',');

                    pricingInfo.AirPricingModifiers.AccountCodeFaresOnly = false;
                    pricingInfo.AirPricingModifiers.AccountCodeFaresOnlySpecified = false;
                    pricingInfo.AirPricingModifiers.AccountCodes = new UAPIdll.WebUR15.AccountCode[codes.Length];
                    for (int i = 0; i < codes.Length; i++)
                    {
                        pricingInfo.AirPricingModifiers.AccountCodes[i] = new UAPIdll.WebUR15.AccountCode();
                        pricingInfo.AirPricingModifiers.AccountCodes[i].Code = codes[i];
                        pricingInfo.AirPricingModifiers.AccountCodes[i].ProviderCode = "1G";
                        //priceModifier.AccountCodes[i].SupplierCode = "";
                        //priceModifier.AccountCodes[i].Type = "";
                    }
                    pricingInfo.AirPricingModifiers.FaresIndicator = UAPIdll.WebUR15.typeFaresIndicator.PrivateFaresOnly;
                }

                createRequest.AirPricingSolution.AirSegmentRef = new UAPIdll.WebUR15.AirSegmentRef[0];//Clear AirSegmentRef if RePricing done

                UAPIdll.WebUR15.ActionStatus actionStatus = new UAPIdll.WebUR15.ActionStatus();
                actionStatus.ProviderCode = itinerary.UapiPricingSolution.AirSegment[0].ProviderCode;// "1G";// To do -- should be assigned dynamically
                actionStatus.Type = UAPIdll.WebUR15.ActionStatusType.ACTIVE;

                createRequest.ActionStatus = new UAPIdll.WebUR15.ActionStatus[1];
                createRequest.ActionStatus[0] = new UAPIdll.WebUR15.ActionStatus();
                createRequest.ActionStatus[0] = actionStatus;

                // For Payment Information
                UAPIdll.WebUR15.FormOfPayment paymentForm = new UAPIdll.WebUR15.FormOfPayment();
                paymentForm.Key = "123";
                paymentForm.Type = "Cash";
                createRequest.FormOfPayment = new UAPIdll.WebUR15.FormOfPayment[1];
                createRequest.FormOfPayment[0] = new UAPIdll.WebUR15.FormOfPayment();
                createRequest.FormOfPayment[0] = paymentForm;

                createRequest.AirPricingSolution.AvailableSSR = null;
                createRequest.AirPricingSolution.OptionalServices = null;
                createRequest.AirPricingSolution.PricingDetails = null;

                // To Remove
                if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.WebUR15.AirCreateReservationReq));
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                    ser.Serialize(writer, createRequest); 	// Here Classes are converted to XML String. 
                    // This can be viewed in SB or writer.
                    // Above XML in SB can be loaded in XmlDocument object
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(sb.ToString());
                    //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirReservationReq .xml");
                    string filePath = xmlPath + "FlightReservationRequest_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                    doc.Save(filePath);
                    //
                }


                UAPIdll.WebUR15.AirCreateReservationBinding binding = new UAPIdll.WebUR15.AirCreateReservationBinding();
                binding.Url = urlAir;
                //binding.Credentials = new NetworkCredential(UserName, Password);
                binding.Credentials = new NetworkCredential(_uapiCredentials.UserName, _uapiCredentials.Password);// MOD by ziyad as clashjing UAPI crdentails
                binding.PreAuthenticate = true;//to implement GZIP

                reservationRsp = binding.service(createRequest); //ziyad for local test 
                //----------------------Loading previous static xml-------------------------------
                //System.Xml.Serialization.XmlSerializer serLoc = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.WebUR15.AirCreateReservationRsp));
                //System.IO.StreamReader writerLoc = new StreamReader(@"C:\temp\ziyad\Cozmo Live\FlightReservationResponse.xml");
                //reservationRsp = serLoc.Deserialize(writerLoc) as UAPIdll.WebUR15.AirCreateReservationRsp;
                // ----------------------------- end test ----------------
            }
            catch (SoapException se)
            {
                throw new Exception(se.Message, se);
            }
            return reservationRsp;
        }


        private static bool ReadBookingStatus(UAPIdll.WebUR15.AirReservation airReservation)
        { 
            bool booked = true;
            List<string> availabilityStatus = new List<string>();
            availabilityStatus.Add("HS");
            availabilityStatus.Add("SS");
            availabilityStatus.Add("HK");
            availabilityStatus.Add("KK");
            availabilityStatus.Add("TK");

            UAPIdll.WebUR15.typeBaseAirSegment[] airSegmentList = airReservation.AirSegment;
            if (airSegmentList != null)
            {
                foreach (UAPIdll.WebUR15.typeBaseAirSegment airSegment in airSegmentList)
                {
                    string status = airSegment.Status;
                    if (!availabilityStatus.Contains(status))
                    {
                        booked = false;
                    }

                }
            }
            else
                booked = false;              
            

            return booked;           
        }

        private static BookingResponse ReadBookResponse(UAPIdll.WebUR15.AirCreateReservationRsp reservationRsp, FlightItinerary itinerary, out List<SegmentPTCDetail> ptcDetail)
        {

            ptcDetail = new List<SegmentPTCDetail>();
            BookingResponse booking = new BookingResponse();
            UAPIdll.WebUR15.UniversalRecord unRecord = reservationRsp.UniversalRecord;
            string ssrMessage = string.Empty;
            // IF SSR is required for Pax 
            if (unRecord != null)
            {
                UAPIdll.WebUR15.BookingTraveler[] tmpBktravelerList=unRecord.BookingTraveler;
                if (tmpBktravelerList != null)
                {
                    foreach (UAPIdll.WebUR15.BookingTraveler tmpBktraveler in tmpBktravelerList)
                    {
                        UAPIdll.WebUR15.SSR[] tempSSRList = tmpBktraveler.SSR;

                        if (tempSSRList != null)
                        {
                            foreach (UAPIdll.WebUR15.SSR tempSSR in tempSSRList)
                            {

                                ssrMessage += " (" + tmpBktraveler.BookingTravelerName.First + ") SSR type: " + tempSSR.Type + ",Status:" + tempSSR.Status + ", Free Text:" + tempSSR.FreeText;
                            }
                        }
                    }
                }
               
                // end SSR

                string URlocatorCode = unRecord.LocatorCode ;// UR
                string providerLocatorCode = unRecord.ProviderReservationInfo[0].LocatorCode;// TO do with multiple Airline Test
                // PNR ( supplier)
                string reservationLocatorCode = string.Empty;


                UAPIdll.WebUR15.AirReservation[] airReservationList = unRecord.Items;
                if (airReservationList != null)
                {

                    foreach (UAPIdll.WebUR15.AirReservation airReservation in airReservationList)
                    {

                        UAPIdll.WebUR15.AirPricingInfo[] airPricingList = airReservation.AirPricingInfo;
            
                        reservationLocatorCode = airReservation.LocatorCode;// TO do with multiple Airline Test
                        UAPIdll.WebUR15.typeBaseAirSegment[] airSegmentList = airReservation.AirSegment;
                        int airSegIndex = 0;
                        foreach (UAPIdll.WebUR15.typeBaseAirSegment airSegment in airSegmentList)
                        {
                            string status = airSegment.Status;

                            if ((airSegIndex + 1) == Convert.ToInt32(airSegment.TravelOrder)) // To cross check the Segments/Flights
                            {
                                itinerary.Segments[airSegIndex].Status = status;
                                if ((itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'K') || (itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'S'))
                                {
                                    itinerary.Segments[airSegIndex].FlightStatus = FlightStatus.Confirmed;
                                    UAPIdll.WebUR15.SupplierLocator[] supLocatorList = airReservation.SupplierLocator;
                                    if (supLocatorList != null)
                                    {
                                        foreach (UAPIdll.WebUR15.SupplierLocator supLocator in supLocatorList)
                                        {
                                            if (airSegment.Carrier == supLocator.SupplierCode)
                                            {
                                                itinerary.Segments[airSegIndex].AirlinePNR = supLocator.SupplierLocatorCode;

                                            }
                                        }
                                    }
                                    //******************************** Adding PTC Details ********************************//
                                    foreach (UAPIdll.WebUR15.AirPricingInfo price in airPricingList)
                                    {
                                         UAPIdll.WebUR15.FareInfo[] fares = price.FareInfo;

                                        foreach (UAPIdll.WebUR15.FareInfo fare in fares)
                                        {
                                            if (airSegment.FlightDetails[0].Origin == fare.Origin && airSegment.FlightDetails[0].Destination == fare.Destination)
                                            {
                                                SegmentPTCDetail ptc = new SegmentPTCDetail();
                                                ptc.SegmentId = airSegment.Group;
                                                if (fare.BaggageAllowance != null && fare.BaggageAllowance.MaxWeight != null && fare.BaggageAllowance.MaxWeight.Value != "0")
                                                {
                                                    ptc.Baggage = fare.BaggageAllowance.MaxWeight.Value + "Kg";
                                                }
                                                else if (fare.BaggageAllowance.NumberOfPieces != null && fare.BaggageAllowance.NumberOfPieces.Length > 0)
                                                {
                                                    ptc.Baggage = fare.BaggageAllowance.NumberOfPieces + " piece";
                                                }
                                                else
                                                {
                                                    ptc.Baggage = "As per Airline";
                                                }
                                                ptc.FlightKey = itinerary.Segments[airSegIndex].FlightKey;
                                                ptc.FareBasis = fare.FareBasis;
                                                ptc.PaxType = fare.PassengerTypeCode;
                                                ptc.NVA = "";
                                                ptc.NVB = "";
                                                ptcDetail.Add(ptc);
                                            }
                                        }
                                    }
                                }
                            }
                            airSegIndex++;

                        }

                    }
                    //itinerary.UniversalRecord = URlocatorCode;// Assigning UR to Itinerary OLD
                    //itinerary.PNR = reservationLocatorCode;// REservation PNr is Actual Pnr
                    //itinerary.ProviderPNR = providerLocatorCode;//Provider PNR is Actual Pnr
                    //booking.PNR = reservationLocatorCode;//reservationLocatorCode;

                    
                    itinerary.UniversalRecord = URlocatorCode;// Assigning UR to Itinerary
                    itinerary.PNR = providerLocatorCode;// //Provider PNR is Actual Pnr
                    itinerary.AirLocatorCode = reservationLocatorCode;//Reservation PNR is for issue the Ticket
                    booking.PNR = providerLocatorCode;//reservationLocatorCode;
                    itinerary.FareType = "PUB";
                    booking.Status = BookingResponseStatus.Successful;
                    // SSR Details




                }
                else
                {
                    booking.Status = BookingResponseStatus.Failed;
                    booking.Error = "NO VALID FARE FOR INPUT CRITERIA";
                }
            }
            else
            {
                booking.Status = BookingResponseStatus.Failed;
                booking.Error = "UniversalRecord is null";
            }
            if (ssrMessage.Length > 0)
            {
                booking.SSRMessage = ssrMessage;
                booking.SSRDenied = true;
            }

            return booking;
            
           
              
                
               // For Price Info Changed
                //for (int i = 0; i < itinerary.Passenger.Length; i++)
                //{
                //    if (paxRef.ContainsKey(itinerary.Passenger[i].Type))
                //    {
                //        int fareKey = paxRef[itinerary.Passenger[i].Type];

                //        if (priceRef.ContainsKey(fareKey) && (itinerary.Passenger[i].Price.PublishedFare + itinerary.Passenger[i].Price.Tax) != (priceRef[fareKey].PublishedFare + priceRef[fareKey].Tax))
                //        {
                //            itinerary.Passenger[i].Price.PublishedFare = priceRef[fareKey].PublishedFare;
                //            itinerary.Passenger[i].Price.Tax = priceRef[fareKey].Tax;
                //            booking.Status = BookingResponseStatus.BookedOther;
                //        }
                //    }
                //}
               
           
               
            
            
        }
        # endregion

        # region Cancel Itinerary
        public static string CancelItinerary(string pnr,string pnrType)
        {
            Connection();
            //bool isCancel = false;
            string tempPNR = pnr;
            string cancelledPNR = string.Empty;
            string cancelledProvider = string.Empty;
            bool isCancelled = false;

            if (pnrType != "UR")
            {
                FlightItinerary itinerary = new FlightItinerary(FlightItinerary.GetFlightId(pnr));
                pnr = itinerary.UniversalRecord;

            }

            
            CT.Core.Audit.Add(EventType.CancellBooking, Severity.Normal, 0, "UAPI Cancell Itinerary : Request UR PNR = " + pnr, "0");

            UAPIdll.WebUR15.UniversalRecordCancelReq urCancelRequest = new UAPIdll.WebUR15.UniversalRecordCancelReq();
            urCancelRequest.TargetBranch = TargetBranch;
            urCancelRequest.UniversalRecordLocatorCode = pnr;
            urCancelRequest.Version = "18";// TO DO
            urCancelRequest.AuthorizedBy = string.Empty ;

            UAPIdll.WebUR15.BillingPointOfSaleInfo pos = new UAPIdll.WebUR15.BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            urCancelRequest.BillingPointOfSaleInfo = pos;

            UAPIdll.WebUR15.UniversalRecordCancelServiceBinding binding = new UAPIdll.WebUR15.UniversalRecordCancelServiceBinding();
            
            binding.Url = urlUR;
            binding.Credentials = new NetworkCredential(UserName, Password);
            binding.PreAuthenticate = true;//to implement GZIP

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(urCancelRequest.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, urCancelRequest); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\UniversalRecordCancelReq.xml");
                string filePath = xmlPath+"FlightURCancelRequest_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
            }

            UAPIdll.WebUR15.UniversalRecordCancelRsp urCancelResponse = null;

            try
            {
                urCancelResponse = binding.service(urCancelRequest);
            }
            catch (SoapException se)
            {
                Audit.Add(EventType.CancellBooking, Severity.High, 0, "UAP.CancelItinerary exiting : Cancellation of PNR " + pnr + " failed. Erro msg:" + se.Message, string.Empty);
                cancelledPNR = se.Message;

                return cancelledPNR;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.CancellBooking, Severity.High, 0, "UAP.CancelItinerary exiting : Cancellation of PNR " + pnr + " failed. Erro msg:" + ex.Message, string.Empty);
                cancelledPNR = ex.Message;

                return cancelledPNR;
            }


            // TO Remove
            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(urCancelResponse.GetType());
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, urCancelResponse); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docRes = new XmlDocument();
                docRes.LoadXml(sbRes.ToString());
                //docRes.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\UniversalRecordCancelRsp.xml");
                string filePath = xmlPath + "FlightURCancelResponse_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                docRes.Save(filePath);
            }
            UAPIdll.WebUR15.ProviderReservationStatus[] reservationStatusList = urCancelResponse.ProviderReservationStatus;
            if (reservationStatusList != null)
            {
                foreach (UAPIdll.WebUR15.ProviderReservationStatus reservationStatus in reservationStatusList)
                {
                    isCancelled = reservationStatus.Cancelled;
                    cancelledPNR = reservationStatus.LocatorCode;
                    cancelledProvider= reservationStatus.ProviderCode;
                }

            }
            if (isCancelled)
            {
                if (pnrType != "UR")
                    cancelledPNR = tempPNR;



            }
            else
            {
                Trace.TraceError("UAP.CancelItinerary exiting : Cancellation of PNR " + pnr + " failed.");
                throw new BookingEngineException("Cancellation of PNR " + pnr + " failed.");
            }




            
              
            return cancelledPNR;
        }
        # endregion

        # region Retrieve Itinerary
        public static FlightItinerary RetrieveItinerary(string pnr, UAPICredentials _uapiCredentials)
        {
            FlightItinerary itinerary = new FlightItinerary();
            Ticket[] ticket;
            Trace.TraceInformation("UAPI.RetrieveItinerary entered WITH PNR Param : PNR= " + pnr);
            itinerary = RetrieveItinerary(pnr, out ticket,_uapiCredentials);
            return itinerary;
        }
        public static FlightItinerary RetrieveItinerary(string pnr, out Ticket[] ticket, UAPICredentials _uapiCredentials)
        {
            FlightItinerary itinerary = new FlightItinerary();
            Trace.TraceInformation("UAPI.RetrieveItinerary entered : PNR = " + pnr);
            try
            {
                // UAPIdll.WebUR15.UniversalRecordRetrieveRsp pnrResponse = GenerateRetrievePNRObject(pnr);
                UAPIdll.WebUR15.UniversalRecordImportRsp URimportResponse = ImportUR(pnr, _uapiCredentials);
                try
                {
                    itinerary = MakeBookingObject(URimportResponse, out ticket);
                }
                catch (BookingEngineException ex)
                {
                    // throw ex;
                    string errorMess = ex.Message;
                    string restrictedBf = "UNABLE TO RETRIEVE - RESTRICTED BF";
                    if (errorMess.IndexOf(restrictedBf) >= 0)
                    {
                        //connection = new Connection(true);

                        //pnrResponse = connection.SubmitXml(request);
                        try
                        {
                            itinerary = MakeBookingObject(URimportResponse, out ticket);
                        }
                        catch (BookingEngineException excep)
                        {
                            throw ex;
                            //errorMess = excep.Message;
                            //restrictedBf = "UNABLE TO RETRIEVE - RESTRICTED BF";
                            //if (errorMess.IndexOf(restrictedBf) >= 0)
                            //{
                            //    string pcc = ConfigurationSystem.GalileoConfig["DomesticPCC"];
                            //    itinerary.PNR = pnr;
                            //    GetTicketref itinerary, out ticket, pcc);
                            //}
                            //else
                            //{
                            //    throw ex;
                            //}
                        }
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            catch (SoapException se)
            {
                throw new Exception(se.Message, se);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return itinerary;
        }
        public static FlightItinerary RetrieveItinerary(FlightItinerary itinerary, out Ticket[] ticket, UAPICredentials _uapiCredentials)
        {
            //FlightItinerary itinerary = new FlightItinerary();
            Trace.TraceInformation("UAPI.RetrieveItinerary entered : PNR = " + itinerary.PNR);

            try
            {
                UAPIdll.WebUR15.UniversalRecordImportRsp URimportResponse = ImportUR(itinerary.PNR, _uapiCredentials);
                try
                {
                    itinerary = MakeBookingObject(URimportResponse, out ticket);
                }
                catch (BookingEngineException ex)
                {
                    // throw ex;
                    string errorMess = ex.Message;
                    string restrictedBf = "UNABLE TO RETRIEVE - RESTRICTED BF";
                    if (errorMess.IndexOf(restrictedBf) >= 0)
                    {
                        //connection = new Connection(true);

                        //pnrResponse = connection.SubmitXml(request);
                        try
                        {
                            itinerary = MakeBookingObject(URimportResponse, out ticket);
                        }
                        catch (BookingEngineException excep)
                        {
                            throw ex;
                            //errorMess = excep.Message;
                            //restrictedBf = "UNABLE TO RETRIEVE - RESTRICTED BF";
                            //if (errorMess.IndexOf(restrictedBf) >= 0)
                            //{
                            //    string pcc = ConfigurationSystem.GalileoConfig["DomesticPCC"];
                            //    itinerary.PNR = pnr;
                            //    GetTicketref itinerary, out ticket, pcc);
                            //}
                            //else
                            //{
                            //    throw ex;
                            //}
                        }
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            catch (SoapException se)
            {
                throw new Exception(se.Message, se);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return itinerary;
        }

        private static UAPIdll.WebUR15.UniversalRecordRetrieveRsp GenerateRetrievePNRObject(string pnr)// Not using this method since we are using Import UR
        {
            Connection();
            Trace.TraceInformation("UApi.GenerateRetrievePNRObject entered");
            UAPIdll.WebUR15.UniversalRecordRetrieveReq request = new UAPIdll.WebUR15.UniversalRecordRetrieveReq();

            request.TargetBranch = TargetBranch;
            request.Item = pnr;

            UAPIdll.WebUR15.BillingPointOfSaleInfo pos = new UAPIdll.WebUR15.BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            request.BillingPointOfSaleInfo = pos;

            UAPIdll.WebUR15.UniversalRecordRetrieveServiceBinding binding = new UAPIdll.WebUR15.UniversalRecordRetrieveServiceBinding();
            binding.Url = urlUR;
            binding.Credentials = new NetworkCredential(UserName, Password);
            binding.PreAuthenticate = true;//to implement GZIP

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(request.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, request); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\URRetrieveReq.xml");
                string filePath = xmlPath+"FlightURRetrieveRequest_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
            }

            UAPIdll.WebUR15.UniversalRecordRetrieveRsp response =null;
            try
            {
                response= binding.service(request);

            }
            catch { throw; }
            //{
            //    throw ex;
            //}

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(response.GetType());
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, response); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docres = new XmlDocument();
                docres.LoadXml(sbRes.ToString());
                docres.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\URRetrieveRes.xml");
            }
            Trace.TraceInformation("UApi.GenerateRetrievePNRObject exiting");
            return response;
        }

       /* private static FlightItinerary MakeBookingObject(UAPIdll.WebUR15.UniversalRecordRetrieveRsp response, out Ticket[] ticket)// this method is not using currently,its for retrive pnr.. refer overloaded method
        {
            ticket = new Ticket[0];
            FlightItinerary itinerary = new FlightItinerary();
            UAPIdll.WebUR15.UniversalRecord uRecord=response.UniversalRecord;
            itinerary.UniversalRecord = uRecord.LocatorCode;

            itinerary.PNR = uRecord.ProviderReservationInfo[0].LocatorCode;// TO do with multiple Airline Test


            // Passenger Information 
            UAPIdll.WebUR15.BookingTraveler[] bookingTravelerList = uRecord.BookingTraveler;

            Dictionary<long, FlightPassenger> passengerDict = new Dictionary<long, FlightPassenger>();// To Stor Passnger Details
            FlightPassenger[] tempPassengerList = new FlightPassenger[bookingTravelerList.Length];
            Dictionary<long, List<KeyValuePair<string, decimal>>> taxBreakUpPax = new Dictionary<long, List<KeyValuePair<string, decimal>>>(); // To Store Pax wise Tax Break Up
            int paxIndex = 0;
            foreach (UAPIdll.WebUR15.BookingTraveler bookingTraveler in bookingTravelerList)
            {
                //bookingTraveler.TravelerType
                tempPassengerList[paxIndex] = new FlightPassenger();
                if (bookingTraveler.TravelerType == "ADT")
                    tempPassengerList[paxIndex].Type = PassengerType.Adult;
                if (bookingTraveler.TravelerType == "CNN")
                    tempPassengerList[paxIndex].Type = PassengerType.Child;
                if (bookingTraveler.TravelerType == "INF")
                    tempPassengerList[paxIndex].Type = PassengerType.Infant;
                if (bookingTraveler.TravelerType == "SRC")
                    tempPassengerList[paxIndex].Type = PassengerType.Senior;

                UAPIdll.WebUR15.BookingTravelerName travelerName = new UAPIdll.WebUR15.BookingTravelerName();
                travelerName = bookingTraveler.BookingTravelerName;
                tempPassengerList[paxIndex].FirstName = travelerName.First;
                tempPassengerList[paxIndex].LastName = travelerName.Last;
                tempPassengerList[paxIndex].Title = travelerName.Prefix;


                UAPIdll.WebUR15.PhoneNumber[] phone = bookingTraveler.PhoneNumber;
                tempPassengerList[paxIndex].CellPhone = phone[0].Number;
                UAPIdll.WebUR15.typeStructuredAddress[] adds = bookingTraveler.Address;
                tempPassengerList[paxIndex].AddressLine1 = adds[0].AddressName;
                if (adds[0].Street != null)
                {
                    string[] street = adds[0].Street;
                    tempPassengerList[paxIndex].AddressLine2 = street[0].ToString();
                }
                
                if(adds[0].City!=null)tempPassengerList[paxIndex].City = adds[0].City;

                Country tempCountry=new Country();
                tempCountry.CountryCode= adds[0].Country;
                tempPassengerList[paxIndex].Country=tempCountry;
                passengerDict.Add(Convert.ToInt64(bookingTraveler.Key), tempPassengerList[paxIndex]);
                paxIndex++;

                // Segment Information


            }
            // Air Reservation Details
            string reservationLocatorCode = string.Empty;
            UAPIdll.WebUR15.AirReservation[] airReservationList = response.UniversalRecord.AirReservation;
            if (airReservationList != null)
            {

                foreach (UAPIdll.WebUR15.AirReservation airReservation in airReservationList)
                {
                    reservationLocatorCode = airReservation.LocatorCode;// storing the PNR for issue the ticket for UAPI-TO do with multiple Airline Test
                    
                    itinerary.CreatedOn = Convert.ToDateTime(airReservation.CreateDate);
                    //itinerary.Ticketed = true; TO check after Ticket
                    itinerary.FareType = "PUB";
                    itinerary.FlightBookingSource = BookingSource.UAPI;
                    itinerary.Segments = new FlightInfo[airReservation.AirSegment.Length];

                    // Air Segment Details
                     int airSegIndex = 0;
                     foreach (UAPIdll.WebUR15.AirSegment tempAirSegment in airReservation.AirSegment)
                    {

                        itinerary.Segments[airSegIndex] = new FlightInfo();
                        itinerary.Segments[airSegIndex].Status = tempAirSegment.Status;
                        if ((itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'K') || (itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'S'))
                            itinerary.Segments[airSegIndex].FlightStatus = FlightStatus.Confirmed;
                        itinerary.Segments[airSegIndex].Airline = tempAirSegment.Carrier;
                        itinerary.Segments[airSegIndex].Origin = new Airport(tempAirSegment.Origin);
                        //itinerary.Segments[airSegIndex].Origin.AirportCode= tempAirSegment.Origin;
                        itinerary.Segments[airSegIndex].Destination= new Airport(tempAirSegment.Destination);
                        //itinerary.Segments[airSegIndex].Destination.AirportCode = tempAirSegment.Destination;

                         // Local Time
                         string depTime = tempAirSegment.DepartureTime.Substring(0,tempAirSegment.DepartureTime.IndexOf("+"));
                         string arrTime = tempAirSegment.ArrivalTime.Substring(0, tempAirSegment.ArrivalTime.IndexOf("+"));


                        itinerary.Segments[airSegIndex].DepartureTime = Convert.ToDateTime(depTime);
                        itinerary.Segments[airSegIndex].ArrivalTime = Convert.ToDateTime(arrTime);
                         // GMT Time
                        //itinerary.Segments[airSegIndex].ArrivalTime= Convert.ToDateTime(tempAirSegment.ArrivalTime);
                        //itinerary.Segments[airSegIndex].DepartureTime= Convert.ToDateTime(tempAirSegment.DepartureTime);
                        itinerary.Segments[airSegIndex].BookingClass = tempAirSegment.ClassOfService;
                        itinerary.Segments[airSegIndex].CabinClass = tempAirSegment.ClassOfService;
                        itinerary.Segments[airSegIndex].FlightNumber= tempAirSegment.FlightNumber;
                        itinerary.Segments[airSegIndex].UapiSegmentRefKey= Convert.ToInt32(tempAirSegment.Key);
                        itinerary.Segments[airSegIndex].AirlinePNR = airReservation.LocatorCode;
                        UAPIdll.WebUR15.SupplierLocator[] supLocatorList = airReservation.SupplierLocator;
                        if (supLocatorList != null)
                        {
                            foreach (UAPIdll.WebUR15.SupplierLocator supLocator in supLocatorList)
                            {

                                if (tempAirSegment.Carrier == supLocator.SupplierCode)
                                {
                                    itinerary.Segments[airSegIndex].AirlinePNR = supLocator.SupplierLocatorCode;

                                }
                            }
                        }



                        //UAPIdll.WebUR15.typeEticketability ticketability = new UAPIdll.WebUR15.typeEticketability();
                        //ticketability = tempAirSegment.ETicketability;
                        if (tempAirSegment.ETicketability == UAPIdll.WebUR15.typeEticketability.Yes)
                            itinerary.Segments[airSegIndex].ETicketEligible = true;
                        else itinerary.Segments[airSegIndex].ETicketEligible = false;


                        itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
                        airSegIndex++;

                    }


                    // Air Pricing Info Details
                     bool nonRefundable = false;
                     //double totalResultFare = 0;
                     //double totalbaseFare = 0;
                     string lastTktDate = string.Empty;
                     //pricingSolution.Journey[0].
                     //int fareKey = 0;
                     decimal baseFare = 0;
                     decimal totalFare = 0;
                     //fare = new Fare[pricingSolution.AirPricingInfo.Length];

                    // Air20.BookingInfo[] bookingInfoList = pricingSolution.AirPricingInfo[0].BookingInfo;
                     //int infoIndex = 0;
                    Dictionary<string, Dictionary<string, UAPIdll.WebUR15.FareInfo>> fareInfoPaxtDict = new Dictionary<string, Dictionary<string, UAPIdll.WebUR15.FareInfo>>();
                     foreach (UAPIdll.WebUR15.AirPricingInfo pricingInfo in airReservation.AirPricingInfo) //AirPricingInfo
                     {
                         baseFare = (decimal)getCurrAmount(pricingInfo.BasePrice);
                         totalFare = (decimal)getCurrAmount(pricingInfo.TotalPrice);
                         lastTktDate = pricingInfo.LatestTicketingTime;
                         nonRefundable = !pricingInfo.Refundable;
                         //Fare Info Details
                         Dictionary<string, UAPIdll.WebUR15.FareInfo> fareInfoDict = new Dictionary<string, UAPIdll.WebUR15.FareInfo>();
                         Dictionary<string, UAPIdll.WebUR15.FareInfo> fareInfoSegmentDict = new Dictionary<string, UAPIdll.WebUR15.FareInfo>();
                         
                         foreach (UAPIdll.WebUR15.FareInfo tempFareInfo in pricingInfo.FareInfo)
                         {
                             fareInfoDict.Add(tempFareInfo.Key, tempFareInfo);
                             
                         }

                         foreach(UAPIdll.WebUR15.BookingInfo tempBookingInfo in pricingInfo.BookingInfo)
                         {
                             fareInfoSegmentDict.Add(tempBookingInfo.SegmentRef,fareInfoDict[tempBookingInfo.FareInfoRef]);
                         }


                         UAPIdll.WebUR15.PassengerType[] tempPassTypeList = pricingInfo.PassengerType;
                         foreach (UAPIdll.WebUR15.PassengerType tempPassType in tempPassTypeList)
                         {


                             fareInfoPaxtDict.Add(tempPassType.BookingTravelerRef, fareInfoSegmentDict);
                             // Price Details
                             
                             PriceAccounts price = new PriceAccounts();
                             price.PublishedFare = baseFare;
                             price.Tax = totalFare - baseFare;

                             FlightPassenger tempPassenger = passengerDict[Convert.ToInt64(tempPassType.BookingTravelerRef)];
                             tempPassenger.Price = price;
                             // For Tax Break Up -- not yet implementd( only after Ticket)
                             List<KeyValuePair<string, decimal>> taxBreakUp = new List<KeyValuePair<string, decimal>>();
                             UAPIdll.WebUR15.typeTaxInfo[] tempTaxTypeList = pricingInfo.TaxInfo;
                             if (tempTaxTypeList != null && tempTaxTypeList.Length > 0)
                             {
                                 foreach (UAPIdll.WebUR15.typeTaxInfo tempTaxType in tempTaxTypeList)
                                 {
                                     string taxType = tempTaxType.Category;
                                     decimal taxValue = (decimal)getCurrAmount(tempTaxType.Amount);
                                     taxBreakUp.Add(new KeyValuePair<string, decimal>(taxType, taxValue));
                                 }
                                 taxBreakUpPax.Add(Convert.ToInt64(tempPassType.BookingTravelerRef), taxBreakUp);
                             }


                         }
                        
                     }// Air Pricing Info End


                    
                    // Checking whether ticketed or not
                    List<FareRule> fareRuleList = new List<FareRule>();
                     if (airReservation.DocumentInfo != null)
                     {
                         UAPIdll.WebUR15.DocumentInfo docInfo = airReservation.DocumentInfo;
                         
                         if (docInfo.TicketInfo != null && docInfo.TicketInfo.Length > 0)
                         {
                             
                             UAPIdll.WebUR15.TicketInfo[] ticketInfoList= docInfo.TicketInfo;
                             // Assiging Ticket Count
                             ticket = new Ticket[ticketInfoList.Length];
                             List<Ticket> ticketArray = new List<Ticket>();
                             int ticketIndex=0;
                             foreach (UAPIdll.WebUR15.TicketInfo ticketInfo in ticketInfoList)
                             {
                                 //if(UAPIdll.WebUR15.typeTicketStatus
                                 if (!string.IsNullOrEmpty(ticketInfo.Number)) // TODO replace Tic,et Number to Status, once we get status Descritption
                                 {
                                     long travelerRef = Convert.ToInt64(ticketInfo.BookingTravelerRef);
                                     Ticket tempTicket = new Ticket();


                                     tempTicket.TicketNumber = ticketInfo.Number;
                                     //tempTicket.ValidatingAriline = ticketNumNode.InnerText.Substring(0, 3);


                                     itinerary.Ticketed = true;

                                     //Assigning Pax Details
                                     FlightPassenger tempPassenger = passengerDict[travelerRef];
                                     //tempPassenger.p
                                     tempTicket.PaxFirstName = tempPassenger.FirstName;
                                     tempTicket.PaxLastName = tempPassenger.LastName;
                                     tempTicket.PaxType = tempPassenger.Type;
                                     tempTicket.Title= tempPassenger.Title;
                                     tempTicket.ETicket = true;
                                     tempTicket.IssueDate = DateTime.UtcNow;
                                     tempTicket.LastModifiedOn = itinerary.LastModifiedOn;
                                     tempTicket.PtcDetail = new List<SegmentPTCDetail>();
                                     // Adding Tax Details for Pax
                                     
                                     if(taxBreakUpPax.ContainsKey(travelerRef))
                                     {
                                         tempTicket.TaxBreakup = new List<KeyValuePair<string, decimal>>();

                                         tempTicket.TaxBreakup = taxBreakUpPax[travelerRef];
                                     }

                                     // Adding PTCDetail 
                                     
                                     for (int k = 0; k < itinerary.Segments.Length; k++)
                                     {
                                         

                                         Dictionary<string, UAPIdll.WebUR15.FareInfo> fareInfoSegmentTemp= fareInfoPaxtDict[ticketInfo.BookingTravelerRef];
                                         UAPIdll.WebUR15.FareInfo tempFareInfo = fareInfoSegmentTemp[Convert.ToString(itinerary.Segments[k].UapiSegmentRefKey)];
                                         // Fare Rules details
                                         if (ticketIndex == 0)
                                         {
                                             FareRule fareRule = new FareRule();
                                             fareRule.FareBasisCode = tempFareInfo.FareBasis;
                                             fareRule.Origin = itinerary.Segments[k].Origin.AirportCode;
                                             fareRule.Destination = itinerary.Segments[k].Destination.AirportCode;
                                             fareRule.Airline = itinerary.Segments[k].Airline;
                                             fareRuleList.Add(fareRule);

                                         }
                                         // Baagege Details
                                         SegmentPTCDetail ptcDtl = new SegmentPTCDetail();
                                         ptcDtl.Baggage = tempFareInfo.BaggageAllowance.MaxWeight.Value;
                                         string flightKey = string.Empty;
                                         flightKey = itinerary.Segments[k].Airline;
                                         //if (itinerary.Segments[k].FlightNumber.Length == 3)
                                         //{
                                         //    flightKey += "0" + itinerary.Segments[k].FlightNumber;
                                         //}
                                         //else if (itinerary.Segments[k].FlightNumber.Length == 4)
                                         //{
                                         //    flightKey += itinerary.Segments[k].FlightNumber;
                                         //}
                                         flightKey += itinerary.Segments[k].FlightNumber.PadLeft(4, '0');
                                         flightKey += itinerary.Segments[k].DepartureTime.ToString("ddMMMyyyyHHmm").ToUpper();
                                         ptcDtl.FlightKey = flightKey;
                                         ptcDtl.NVA = string.Empty;
                                         ptcDtl.NVB = string.Empty;
                                         //ptcDtl.FareBasis = itinerary.FareRules[k].FareBasisCode;
                                         ptcDtl.FareBasis = tempFareInfo.FareBasis;
                                         tempTicket.PtcDetail.Add(ptcDtl);
                                         
                                         //ticket[paxIndex].PtcDetail.Add(ptcDtl);
                                     }
                                     
                                     //tempTicket.PtcDetail = new List<SegmentPTCDetail>();
                                     ticketArray.Add(tempTicket);
                                 }
                                 ticketIndex++;
                             }
                             ticket = ticketArray.ToArray();
                         }
                         else
                             ticket = new Ticket[0];
                     }


                     //if (airReservation.TicketingModifiers != null && airReservation.TicketingModifiers.Length > 0)
                     //{
                     //    UAPIdll.WebUR15.TicketingModifiers[] ticketModifierList = airReservation.TicketingModifiers;
                     //    foreach (UAPIdll.WebUR15.TicketingModifiers ticketModifier in ticketModifierList)
                     //    {
                     //        itinerary.Ticketed = ticketModifier.DocumentSelect.IssueElectronicTicket;
                     //    }
                     //}


                    if(fareRuleList.Count>0)itinerary.FareRules = fareRuleList;
                }

            }
            // Adding Temp FLight Passenger List To Itinerary
            itinerary.Passenger = new FlightPassenger[tempPassengerList.Length];
            
            itinerary.Passenger = tempPassengerList;
            itinerary.AirLocatorCode = reservationLocatorCode;// store the air reservation code to issue the ticket

            // TO DO
            
            return itinerary;
           
                  
        }*/
        private static FlightItinerary MakeBookingObject(UAPIdll.WebUR15.UniversalRecordImportRsp response, out Ticket[] ticket)
        {
            ticket = new Ticket[0];
            FlightItinerary itinerary = new FlightItinerary();
            UAPIdll.WebUR15.UniversalRecord uRecord = response.UniversalRecord;
            itinerary.UniversalRecord = uRecord.LocatorCode;
            itinerary.PNR = uRecord.ProviderReservationInfo[0].LocatorCode;// TO do with multiple Airline Test
            // Passenger Information 
            UAPIdll.WebUR15.BookingTraveler[] bookingTravelerList = uRecord.BookingTraveler;

            Dictionary<string, FlightPassenger> passengerDict = new Dictionary<string, FlightPassenger>();// To Stor Passnger Details
            FlightPassenger[] tempPassengerList = new FlightPassenger[bookingTravelerList.Length];
            Dictionary<string, List<KeyValuePair<string, decimal>>> taxBreakUpPax = new Dictionary<string, List<KeyValuePair<string, decimal>>>(); // To Store Pax wise Tax Break Up
            int paxIndex = 0;
            foreach (UAPIdll.WebUR15.BookingTraveler bookingTraveler in bookingTravelerList)
            {
                //bookingTraveler.TravelerType
                tempPassengerList[paxIndex] = new FlightPassenger();
                if (bookingTraveler.TravelerType == "ADT" || bookingTraveler.TravelerType==null)
                    tempPassengerList[paxIndex].Type = PassengerType.Adult;
                if (bookingTraveler.TravelerType == "CNN")
                    tempPassengerList[paxIndex].Type = PassengerType.Child;
                if (bookingTraveler.TravelerType == "INF")
                    tempPassengerList[paxIndex].Type = PassengerType.Infant;
                if (bookingTraveler.TravelerType == "SRC")
                    tempPassengerList[paxIndex].Type = PassengerType.Senior;

                UAPIdll.WebUR15.BookingTravelerName travelerName = new UAPIdll.WebUR15.BookingTravelerName();
                travelerName = bookingTraveler.BookingTravelerName;
                tempPassengerList[paxIndex].FirstName = travelerName.First;
                tempPassengerList[paxIndex].LastName = travelerName.Last;
                tempPassengerList[paxIndex].Title = travelerName.Prefix;
                tempPassengerList[paxIndex].DateOfBirth = Convert.ToDateTime(bookingTraveler.DOB);
                // for passport
                try
                {
                    UAPIdll.WebUR15.SSR[] SSRList = bookingTraveler.SSR;
                    if (SSRList != null)
                    {
                        string passport =string.Empty;
                        foreach (UAPIdll.WebUR15.SSR ssrPassport in SSRList)
                        {
                            if (ssrPassport.Type == "DOCS")
                            {
                                //string passport = "P/AE/00002345/AE/0000000/M/02FEB17/MHD/ZIYAD -1ZIYAD/MHDMR";
                                passport = ssrPassport.FreeText;
                                passport = passport.Substring(passport.IndexOf("/", 2) + 1, passport.Length - passport.IndexOf("/", 2) - 1);
                                passport = passport.Substring(0, passport.IndexOf("/", 1));
                                break;
                            }
                            
                        }
                        tempPassengerList[paxIndex].PassportNo = passport;

                    }
                }
                catch { }
                if (bookingTraveler.Gender == "M")
                    tempPassengerList[paxIndex].Gender=Gender.Male ;
                else if (bookingTraveler.Gender == "F")
                    tempPassengerList[paxIndex].Gender = Gender.Female;



                UAPIdll.WebUR15.PhoneNumber[] phone = bookingTraveler.PhoneNumber;
                if (phone != null)
                {
                    tempPassengerList[paxIndex].CellPhone = phone[0].Number;
                }
                UAPIdll.WebUR15.Email[] mail = bookingTraveler.Email;
                if (mail!= null)
                {
                    tempPassengerList[paxIndex].Email= mail[0].EmailID;
                }
                UAPIdll.WebUR15.typeStructuredAddress[] adds = bookingTraveler.Address;
                if (adds != null)
                {
                    tempPassengerList[paxIndex].AddressLine1 = adds[0].AddressName;
                    if (adds[0].Street != null)
                    {
                        string[] street = adds[0].Street;
                        tempPassengerList[paxIndex].AddressLine2 = street[0].ToString();
                    }

                    if (adds[0].City != null) tempPassengerList[paxIndex].City = adds[0].City;
                    if (adds[0].Country != null)
                    {
                        Country tempCountry = new Country();
                        tempCountry.CountryCode = adds[0].Country;
                        tempPassengerList[paxIndex].Country = tempCountry;
                    }
                }
                passengerDict.Add((bookingTraveler.Key), tempPassengerList[paxIndex]);
                paxIndex++;

                // Segment Information


            }
            // Air Reservation Details
            string reservationLocatorCode = string.Empty;
            UAPIdll.WebUR15.AirReservation[] airReservationList = response.UniversalRecord.Items;
            if (airReservationList != null)
            {

                foreach (UAPIdll.WebUR15.AirReservation airReservation in airReservationList)
                {
                    reservationLocatorCode = airReservation.LocatorCode;// storing the PNR for issue the ticket for UAPI-TO do with multiple Airline Test
                    itinerary.CreatedOn = Convert.ToDateTime(airReservation.CreateDate);
                    //itinerary.Ticketed = true; TO check after Ticket
                    itinerary.FareType = "PUB";
                    itinerary.FlightBookingSource = BookingSource.UAPI;
                    itinerary.Segments = new FlightInfo[airReservation.AirSegment.Length];

                    // Air Segment Details
                    int airSegIndex = 0;
                    foreach (UAPIdll.WebUR15.typeBaseAirSegment tempAirSegment in airReservation.AirSegment)
                    {

                        itinerary.Segments[airSegIndex] = new FlightInfo();
                        itinerary.Segments[airSegIndex].Status = tempAirSegment.Status;
                        if ((itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'K') || (itinerary.Segments[airSegIndex].Status[itinerary.Segments[airSegIndex].Status.Length - 1] == 'S'))
                            itinerary.Segments[airSegIndex].FlightStatus = FlightStatus.Confirmed;
                        itinerary.Segments[airSegIndex].Airline = tempAirSegment.Carrier;
                        itinerary.Segments[airSegIndex].Origin = new Airport(tempAirSegment.Origin);
                        //itinerary.Segments[airSegIndex].Origin.AirportCode= tempAirSegment.Origin;
                        itinerary.Segments[airSegIndex].Destination = new Airport(tempAirSegment.Destination);
                        //itinerary.Segments[airSegIndex].Destination.AirportCode = tempAirSegment.Destination;

                        // Local Time
                        string GMTdiff = tempAirSegment.DepartureTime.IndexOf("+") != -1 ? "+" : "-";
                        string depTime = tempAirSegment.DepartureTime.Substring(0, tempAirSegment.DepartureTime.LastIndexOf(GMTdiff));
                        GMTdiff = tempAirSegment.ArrivalTime.IndexOf("+") != -1 ? "+" : "-";
                        string arrTime = tempAirSegment.ArrivalTime.Substring(0, tempAirSegment.ArrivalTime.LastIndexOf(GMTdiff));


                        itinerary.Segments[airSegIndex].DepartureTime = Convert.ToDateTime(depTime);
                        itinerary.Segments[airSegIndex].ArrivalTime = Convert.ToDateTime(arrTime);
                        // GMT Time
                        //itinerary.Segments[airSegIndex].ArrivalTime= Convert.ToDateTime(tempAirSegment.ArrivalTime);
                        //itinerary.Segments[airSegIndex].DepartureTime= Convert.ToDateTime(tempAirSegment.DepartureTime);
                        itinerary.Segments[airSegIndex].BookingClass = tempAirSegment.ClassOfService;
                        itinerary.Segments[airSegIndex].CabinClass = tempAirSegment.ClassOfService;
                        itinerary.Segments[airSegIndex].FlightNumber = tempAirSegment.FlightNumber;
                        itinerary.Segments[airSegIndex].UapiSegmentRefKey = tempAirSegment.Key;//Convert.ToInt32(tempAirSegment.Key);
                        itinerary.Segments[airSegIndex].AirlinePNR = airReservation.LocatorCode;
                        itinerary.Segments[airSegIndex].Group = tempAirSegment.Group;
                        int duration = Convert.ToInt32(tempAirSegment.TravelTime);
                        itinerary.Segments[airSegIndex].Duration = new TimeSpan(0, duration, 0);
                        UAPIdll.WebUR15.SupplierLocator[] supLocatorList = airReservation.SupplierLocator;
                        if (supLocatorList != null)
                        {
                            foreach (UAPIdll.WebUR15.SupplierLocator supLocator in supLocatorList)
                            {

                                if (tempAirSegment.Carrier == supLocator.SupplierCode)
                                {
                                    itinerary.Segments[airSegIndex].AirlinePNR = supLocator.SupplierLocatorCode;

                                }
                            }
                        }



                        //UAPIdll.WebUR15.typeEticketability ticketability = new UAPIdll.WebUR15.typeEticketability();
                        //ticketability = tempAirSegment.ETicketability;
                        if (tempAirSegment.ETicketability == UAPIdll.WebUR15.typeEticketability.Yes)
                            itinerary.Segments[airSegIndex].ETicketEligible = true;
                        else itinerary.Segments[airSegIndex].ETicketEligible = false;


                        itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
                        airSegIndex++;

                    }


                    // Air Pricing Info Details
                    bool nonRefundable = false;
                    //double totalResultFare = 0;
                    //double totalbaseFare = 0;
                    string lastTktDate = string.Empty;
                    //pricingSolution.Journey[0].
                    //int fareKey = 0;
                    decimal baseFare = 0;
                    decimal totalFare = 0;

                    

                    
                    //fare = new Fare[pricingSolution.AirPricingInfo.Length];

                    // Air20.BookingInfo[] bookingInfoList = pricingSolution.AirPricingInfo[0].BookingInfo;
                    //int infoIndex = 0;
                    Dictionary<string, Dictionary<string, UAPIdll.WebUR15.FareInfo>> fareInfoPaxtDict = new Dictionary<string, Dictionary<string, UAPIdll.WebUR15.FareInfo>>();
                    if (airReservation.AirPricingInfo == null)
                    {
                        throw new Exception("Pricing Info is not assigned to this PNR !");
                    }
                    foreach (UAPIdll.WebUR15.AirPricingInfo pricingInfo in airReservation.AirPricingInfo) //AirPricingInfo
                    {
                        string currency = getCurrency(pricingInfo.TotalPrice);

                        if (ExchangeRates.ContainsKey(currency))
                        {
                            rateOfExchange = Convert.ToDouble(ExchangeRates[currency]);
                        }
                        else
                        {
                            rateOfExchange = 1;
                        }
                        baseFare = (decimal)getCurrAmount(pricingInfo.ApproximateBasePrice) * Convert.ToDecimal(rateOfExchange);
                        totalFare = (decimal)getCurrAmount(pricingInfo.TotalPrice) * Convert.ToDecimal(rateOfExchange);
                        lastTktDate = pricingInfo.LatestTicketingTime;
                        nonRefundable = !pricingInfo.Refundable;
                        //Fare Info Details
                        Dictionary<string, UAPIdll.WebUR15.FareInfo> fareInfoDict = new Dictionary<string, UAPIdll.WebUR15.FareInfo>();
                        Dictionary<string, UAPIdll.WebUR15.FareInfo> fareInfoSegmentDict = new Dictionary<string, UAPIdll.WebUR15.FareInfo>();

                        foreach (UAPIdll.WebUR15.FareInfo tempFareInfo in pricingInfo.FareInfo)
                        {
                            fareInfoDict.Add(tempFareInfo.Key, tempFareInfo);

                        }

                        foreach (UAPIdll.WebUR15.BookingInfo tempBookingInfo in pricingInfo.BookingInfo)
                        {
                            fareInfoSegmentDict.Add(tempBookingInfo.SegmentRef, fareInfoDict[tempBookingInfo.FareInfoRef]);
                        }


                        UAPIdll.WebUR15.PassengerType[] tempPassTypeList = pricingInfo.PassengerType;
                        foreach (UAPIdll.WebUR15.PassengerType tempPassType in tempPassTypeList)
                        {


                            fareInfoPaxtDict.Add(tempPassType.BookingTravelerRef, fareInfoSegmentDict);
                            // Price Details

                            PriceAccounts price = new PriceAccounts();
                            price.PublishedFare = baseFare;
                            price.Tax = totalFare - baseFare;

                            FlightPassenger tempPassenger = passengerDict[(tempPassType.BookingTravelerRef)];
                            tempPassenger.Price = price;
                            // For Tax Break Up -- not yet implementd( only after Ticket)
                            List<KeyValuePair<string, decimal>> taxBreakUp = new List<KeyValuePair<string, decimal>>();
                            UAPIdll.WebUR15.typeTaxInfo[] tempTaxTypeList = pricingInfo.TaxInfo;
                            if (tempTaxTypeList != null && tempTaxTypeList.Length > 0)
                            {
                                foreach (UAPIdll.WebUR15.typeTaxInfo tempTaxType in tempTaxTypeList)
                                {
                                    string taxType = tempTaxType.Category;
                                    decimal taxValue = (decimal)getCurrAmount(tempTaxType.Amount);
                                    taxBreakUp.Add(new KeyValuePair<string, decimal>(taxType, taxValue));
                                }
                                taxBreakUpPax.Add((tempPassType.BookingTravelerRef), taxBreakUp);
                            }


                        }

                    }// Air Pricing Info End



                    // Checking whether ticketed or not
                    List<FareRule> fareRuleList = new List<FareRule>();
                    if (airReservation.DocumentInfo != null)
                    {
                        UAPIdll.WebUR15.DocumentInfo docInfo = airReservation.DocumentInfo;

                        if (docInfo.TicketInfo != null && docInfo.TicketInfo.Length > 0)
                        {

                            UAPIdll.WebUR15.TicketInfo[] ticketInfoList = docInfo.TicketInfo;
                            // Assiging Ticket Count
                            ticket = new Ticket[ticketInfoList.Length];
                            List<Ticket> ticketArray = new List<Ticket>();
                            int ticketIndex = 0;
                            foreach (UAPIdll.WebUR15.TicketInfo ticketInfo in ticketInfoList)
                            {
                                //if(UAPIdll.WebUR15.typeTicketStatus
                                if (!string.IsNullOrEmpty(ticketInfo.Number)) // TODO replace Tic,et Number to Status, once we get status Descritption
                                {
                                    string travelerRef = (ticketInfo.BookingTravelerRef);
                                    Ticket tempTicket = new Ticket();


                                    tempTicket.TicketNumber = ticketInfo.Number;
                                    //tempTicket.ValidatingAriline = ticketNumNode.InnerText.Substring(0, 3);


                                    itinerary.Ticketed = true;

                                    //Assigning Pax Details
                                    FlightPassenger tempPassenger = passengerDict[travelerRef];
                                    //tempPassenger.p
                                    tempTicket.PaxFirstName = tempPassenger.FirstName;
                                    tempTicket.PaxLastName = tempPassenger.LastName;
                                    tempTicket.PaxType = tempPassenger.Type;
                                    tempTicket.Title = tempPassenger.Title;
                                    tempTicket.ETicket = true;
                                    tempTicket.IssueDate = DateTime.UtcNow;
                                    tempTicket.LastModifiedOn = itinerary.LastModifiedOn;
                                    //Assign TourCode, UAPI AccountCode and MasterAccountCode to Ticket for saving in DB
                                    if (airReservationList[0].AirPricingInfo != null && airReservationList[0].AirPricingInfo[0].PricingMethod == UAPIdll.WebUR15.typePricingMethod.GuaranteedUsingAirlinePrivateFare)
                                    {
                                        if (airReservationList[0].TicketingModifiers != null && airReservationList[0].TicketingModifiers.Length > 0 && airReservationList[0].TicketingModifiers[0].TourCode != null)
                                        {
                                            //Save Tour Code, TicketDesignator & AccountCode
                                            tempTicket.TourCode = airReservationList[0].TicketingModifiers[0].TourCode.Value;//Tour Code
                                        }
                                        else
                                        {
                                            tempTicket.TourCode = "1";// For Without Airline PrivateFare
                                        }
                                        List<UAPIdll.WebUR15.AirPricingInfo> pricingInfo = new List<UAPIdll.WebUR15.AirPricingInfo>();
                                        pricingInfo.AddRange(airReservationList[0].AirPricingInfo);
                                        UAPIdll.WebUR15.AirPricingInfo price = pricingInfo.Find(delegate(UAPIdll.WebUR15.AirPricingInfo p) { return p.Key == ticketInfo.AirPricingInfoRef; });

                                        if (price != null && price.AirPricingModifiers != null && price.AirPricingModifiers.AccountCodes != null && price.AirPricingModifiers.AccountCodes.Length > 0)
                                        {
                                            tempTicket.TicketDesignator = price.AirPricingModifiers.AccountCodes[0].Code;//Our Account Code                                            
                                        }
                                        else
                                        {
                                            tempTicket.TicketDesignator = "0";//For Without Airline PrivateFare
                                        }
                                        if (price != null && price.FareInfo != null && price.FareInfo.Length > 0 && price.FareInfo[0].AccountCode != null && price.FareInfo[0].AccountCode.Length > 0)
                                        {
                                            tempTicket.StockType = price.FareInfo[0].AccountCode[0].Code;//Segmentwise Account Code
                                        }
                                        else
                                        {
                                            tempTicket.StockType = "1";//For Without Airline PrivateFare
                                        }
                                    }
                                    tempTicket.PtcDetail = new List<SegmentPTCDetail>();
                                    // Adding Tax Details for Pax

                                    if (taxBreakUpPax.ContainsKey(travelerRef))
                                    {
                                        tempTicket.TaxBreakup = new List<KeyValuePair<string, decimal>>();

                                        tempTicket.TaxBreakup = taxBreakUpPax[travelerRef];
                                    }

                                    // Adding PTCDetail 

                                    for (int k = 0; k < itinerary.Segments.Length; k++)
                                    {


                                        Dictionary<string, UAPIdll.WebUR15.FareInfo> fareInfoSegmentTemp = fareInfoPaxtDict[ticketInfo.BookingTravelerRef];
                                        UAPIdll.WebUR15.FareInfo tempFareInfo = fareInfoSegmentTemp[Convert.ToString(itinerary.Segments[k].UapiSegmentRefKey)];
                                        // Fare Rules details
                                        if (ticketIndex == 0)
                                        {
                                            FareRule fareRule = new FareRule();
                                            fareRule.FareBasisCode = tempFareInfo.FareBasis;
                                            fareRule.Origin = itinerary.Segments[k].Origin.AirportCode;
                                            fareRule.Destination = itinerary.Segments[k].Destination.AirportCode;
                                            fareRule.Airline = itinerary.Segments[k].Airline;
                                            fareRuleList.Add(fareRule);

                                        }
                                        // Baagege Details
                                        SegmentPTCDetail ptcDtl = new SegmentPTCDetail();
                                        if (tempFareInfo.BaggageAllowance != null && tempFareInfo.BaggageAllowance.MaxWeight != null)
                                        {
                                            ptcDtl.Baggage = tempFareInfo.BaggageAllowance.MaxWeight.Value;
                                        }
                                        else if (tempFareInfo.BaggageAllowance.NumberOfPieces != null && tempFareInfo.BaggageAllowance.NumberOfPieces.Length > 0)
                                        {
                                            ptcDtl.Baggage = tempFareInfo.BaggageAllowance.NumberOfPieces + " piece";
                                        }
                                        else
                                        {
                                            ptcDtl.Baggage = "As per Airline";
                                        }
                                        string flightKey = string.Empty;
                                        flightKey = itinerary.Segments[k].Airline;
                                        //if (itinerary.Segments[k].FlightNumber.Length == 3)
                                        //{
                                        //    flightKey += "0" + itinerary.Segments[k].FlightNumber;
                                        //}
                                        //else if (itinerary.Segments[k].FlightNumber.Length == 4)
                                        //{
                                        //    flightKey += itinerary.Segments[k].FlightNumber;
                                        //}
                                        flightKey += itinerary.Segments[k].FlightNumber.PadLeft(4, '0');
                                        flightKey += itinerary.Segments[k].DepartureTime.ToString("ddMMMyyyyHHmm").ToUpper();
                                        ptcDtl.FlightKey = flightKey;
                                        ptcDtl.NVA = string.Empty;
                                        ptcDtl.NVB = string.Empty;
                                        //ptcDtl.FareBasis = itinerary.FareRules[k].FareBasisCode;
                                        ptcDtl.FareBasis = tempFareInfo.FareBasis;
                                        tempTicket.PtcDetail.Add(ptcDtl);

                                        //ticket[paxIndex].PtcDetail.Add(ptcDtl);
                                    }

                                    //tempTicket.PtcDetail = new List<SegmentPTCDetail>();
                                    ticketArray.Add(tempTicket);
                                }
                                ticketIndex++;
                            }
                            ticket = ticketArray.ToArray();
                        }
                        else
                            ticket = new Ticket[0];
                    }


                    //if (airReservation.TicketingModifiers != null && airReservation.TicketingModifiers.Length > 0)
                    //{
                    //    UAPIdll.WebUR15.TicketingModifiers[] ticketModifierList = airReservation.TicketingModifiers;
                    //    foreach (UAPIdll.WebUR15.TicketingModifiers ticketModifier in ticketModifierList)
                    //    {
                    //        itinerary.Ticketed = ticketModifier.DocumentSelect.IssueElectronicTicket;
                    //    }
                    //}


                    if (fareRuleList.Count > 0) itinerary.FareRules = fareRuleList;
                }

            }
            // Adding Temp FLight Passenger List To Itinerary
            itinerary.Passenger = new FlightPassenger[tempPassengerList.Length];

            itinerary.Passenger = tempPassengerList;
            itinerary.AirLocatorCode = reservationLocatorCode;// store the air reservation code to issue the ticket
            // TO DO

            //Added by shiva 21 Sep 2015
            //If FormOfPayment is nor returned or if returned with Type other than "Cash" then add this node in AirTicketingModifiers request of Ticket request.
            //So set the following variable to "false". The same variable will be checked in 'GenerateETicketMessage' method of line 3012
            if (response.UniversalRecord.FormOfPayment == null || response.UniversalRecord.FormOfPayment.Length == 0 || (response.UniversalRecord.FormOfPayment.Length > 0 && response.UniversalRecord.FormOfPayment[0].Type != "Cash"))
            {
                formOfPaymentIsShown = false;
            }
            return itinerary;


        }
        # endregion

        # region Ticket
        public static TicketingResponse Ticket(FlightItinerary itineraryDB, int member, Dictionary<string, string> ticketData, string ipAddr, UAPICredentials _uapiCredentials)
        {
            TicketingResponse response = new TicketingResponse();
            bool isDomestic = itineraryDB.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "");
            //bool isYatra = false;
            //bool isVia = false;
            //Connection con = new Connection();
            string hap = string.Empty;
            string username = string.Empty;
            string password = string.Empty;
            if (isDomestic)
            {
                hap = Configuration.ConfigurationSystem.GalileoConfig["DomesticHAP"].ToString();
            }
            else
            {
                hap = Configuration.ConfigurationSystem.GalileoConfig["InternationalHAP"].ToString();
            }
            //AirlineHAP airHap = new AirlineHAP();
            //airHap.Load(itineraryDB.ValidatingAirline, "1G");
            //if (isDomestic)
            //{
            //    //if (airHap.DomesticHAP != null && airHap.DomesticHAP.Length > 0)
            //    //{
            //    //    string supName = Supplier.GetSupplierNameById(airHap.SupplierId);
            //    //    if (supName.ToUpper().Contains("YATRA"))
            //    //    {
            //    //        isYatra = true;
            //    //    }
            //    //    else
            //    //    {
            //    //        if (supName.ToUpper().Contains("VIA"))
            //    //        {
            //    //            isVia = true;
            //    //            username = Configuration.ConfigurationSystem.GalileoConfig["ViaUsername"].ToString();
            //    //            password = Configuration.ConfigurationSystem.GalileoConfig["ViaPassword"].ToString();
            //    //        }
            //    //    }
            //    //    hap = airHap.DomesticHAP;
            //    //    string rtResponse = con.SubmitTerminalTransaction("*" + itineraryDB.PNR);
            //    //    rtResponse = con.SubmitTerminalTransaction("QEB/" + airHap.DomesticPCC + "/66+67");
            //    //    con.EndSession();
            //    //    Audit.Add(EventType.Ticketing, Severity.Normal, 0, "Queueing XML 1G. req : QEB/" + airHap.DomesticPCC + "/66+67" + rtResponse, string.Empty);
            //    //}
            //}
            //else
            //{
            //    if (airHap.InternationalHAP != null && airHap.InternationalHAP.Length > 0)
            //    {
            //        //string supName = Supplier.GetSupplierNameById(airHap.SupplierId);
            //        //if (supName.ToUpper().Contains("YATRA"))
            //        //{
            //        //    isYatra = true;
            //        //}
            //        //hap = airHap.InternationalHAP;
            //        //string rtResponse = con.SubmitTerminalTransaction("*" + itineraryDB.PNR);
            //        //rtResponse = con.SubmitTerminalTransaction("QEB/" + airHap.InternationalPCC + "/66+67");
            //        //con.EndSession();
            //        //Audit.Add(EventType.Ticketing, Severity.Normal, 0, "Queueing XML 1G. req : QEB/" + airHap.InternationalPCC + "/66+67" + rtResponse, string.Empty);
            //    }
            //}


            string endorsement = string.Empty;
            string tourCode = string.Empty;
            string corporateCode = string.Empty;
            if (ticketData != null && ticketData.ContainsKey("corporateCode") && ticketData["corporateCode"].Length > 0)
            {
                corporateCode = ticketData["corporateCode"];
            }
            if (itineraryDB.Endorsement != null && itineraryDB.Endorsement.Length > 0)
            {
                endorsement = itineraryDB.Endorsement;
            }
            else if (ticketData != null && ticketData.ContainsKey("endorsement") && ticketData["endorsement"].Length > 0)
            {
                endorsement = ticketData["endorsement"];
            }
            if (corporateCode != null && corporateCode.Trim().Length > 0)
            {
                endorsement += " " + corporateCode.Trim();
            }
            if (itineraryDB.TourCode != null && itineraryDB.TourCode.Length > 0)
            {
                tourCode = itineraryDB.TourCode;
            }
            else if (ticketData != null && ticketData.ContainsKey("tourCode") && ticketData["tourCode"].Length > 0)
            {
                tourCode = ticketData["tourCode"];
            }
            //try //RetrieveItinerary is  Already calling in Metasearch.TicketUAPI.
            //{
            //    Audit.Add(EventType.Ticketing, Severity.Normal, 0, "Retrieve Before Ticket XML UAPI. ", string.Empty);
            //    FlightItinerary retrievItinerary = RetrieveItinerary(itineraryDB.UniversalRecord);
            //}
            //catch
            //{
            //    Audit.Add(EventType.Ticketing, Severity.High, 0, "UAPI Retrieve PNR Failed before Ticketing . Execption. ", string.Empty);
            //    throw;
            //}


            UAPIdll.WebAir15.ResponseMessage responseMessage = null;
            string resMessageText=string.Empty;
            try
            {


                //Air20.AirTicketingRsp ticketingRsp = GenerateETicketMessage(itineraryDB.PNR);
                UAPIdll.WebAir15.AirTicketingRsp ticketingRsp = GenerateETicketMessage(itineraryDB.AirLocatorCode, _uapiCredentials);// in UAPI to issue the ticket, should pass the airreservationlocatorcode

                //If ResponseMessage count is 1 then Items will be TicketFailureInfo 
                //otherwise Items will be ETR. For TicketFailureInfo we need to check in ZERO index
                //as per schema v33.0 because only single TicketFailureIno node will be there in case of error.
                if (ticketingRsp.Items != null && ticketingRsp.Items.Length > 0 && ticketingRsp.ResponseMessage.Length <= 1)
                {
                    response.PNR = itineraryDB.PNR;
                    response.Status = TicketingResponseStatus.OtherError;

                    string erroMessage = (ticketingRsp.Items[0] as TicketFailureInfo).Message;
                    response.Message = erroMessage;
                    throw new Exception(erroMessage);
                    //Audit.Add(EventType.Ticketing, Severity.High, 0, "UAPI Ticketing Failed. Execption :- " + erroMessage, string.Empty);

                }
                if (ticketingRsp.ResponseMessage[0] != null)
                {
                    foreach (ResponseMessage msg in ticketingRsp.ResponseMessage)
                    {
                        resMessageText += msg.Type.ToString() + ":" + msg.Value + ",";
                    }
                }

                // responseMessage = ticketingRsp.ResponseMessage[0];
                //eticketResponse = connection.SubmitXmlOnSession(request);
                Audit.Add(EventType.Ticketing, Severity.Normal, 0, "UAPI Ticket Response Message: " + resMessageText, string.Empty);
            }
            catch (SoapException se)
            {
                Audit.Add(EventType.Ticketing, Severity.High, 0, "UAPI Ticketing Failed. Execption :- " + se.ToString(), string.Empty);
                throw new Exception(se.Message, se);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Ticketing, Severity.High, 0, "UAPI Ticketing Failed. Execption :- " + ex.ToString(), string.Empty);
                throw ex;//for writing in FailedBooking and sending email
            }
            string ticketSuccessPattern = "ELECTRONIC TKT GENERATED";
            //Match match = Regex.Match(responseMessage.Value, ticketSuccessPattern);
            Match match = Regex.Match((!string.IsNullOrEmpty(resMessageText)) ? resMessageText : string.Empty, ticketSuccessPattern);
            if (match.Success)
            {
                response.Status = TicketingResponseStatus.Successful;
                response.PNR = itineraryDB.PNR;
            }
            else
            {
                response.Status = TicketingResponseStatus.NotCreated;
                //response.Message = "Ticketing Failed";
            }
            return response;
        }

        private static UAPIdll.WebAir15.AirTicketingRsp GenerateETicketMessage(string pnr,UAPICredentials _uapiCredentials)
        {
            Connection();
            Trace.TraceInformation("UApi.GenerateETicket entered");

            UAPIdll.WebAir15.AirTicketingReq request = new UAPIdll.WebAir15.AirTicketingReq();

            //request.TargetBranch = TargetBranch;
            request.TargetBranch = _uapiCredentials.TargetBranch;// Mod By ziya for targetbranch clash
            
            request.ReturnInfoOnFail = true;
            request.BulkTicket = false;
            request.AuthorizedBy = "ONLINE";

            UAPIdll.WebAir15.BillingPointOfSaleInfo pos = new UAPIdll.WebAir15.BillingPointOfSaleInfo();
            pos.OriginApplication = "UAPI";
            request.BillingPointOfSaleInfo = pos;


            UAPIdll.WebAir15.AirReservationLocatorCode locatorCode = new UAPIdll.WebAir15.AirReservationLocatorCode();
            locatorCode.Value = pnr;

            request.AirReservationLocatorCode = locatorCode;

            //if (airReservation.TicketingModifiers != null && airReservation.TicketingModifiers.Length > 0)
            //{
            //    UAPIdll.WebUR15.TicketingModifiers[] ticketModifierList = airReservation.TicketingModifiers;
            //    foreach (UAPIdll.WebUR15.TicketingModifiers ticketModifier in ticketModifierList)
            //    {
            //        itinerary.Ticketed = ticketModifier.DocumentSelect.IssueElectronicTicket;
            //    }
            //}
            UAPIdll.WebAir15.AirTicketingModifiers[] ticketModifierList = new AirTicketingModifiers[1];
            ticketModifierList[0] = new AirTicketingModifiers();
            //If FormOfPayment node is missing from Import UR or FormOfPayment, Type is other than "Cash" 
            //then add FormOfPayment here in AirTicketingModifiers to get the Ticket. Added by shiva - 21 Sep 2015
            if (!formOfPaymentIsShown)
            {
                ticketModifierList[0].FormOfPayment = new FormOfPayment[1];
                ticketModifierList[0].FormOfPayment[0] = new FormOfPayment();
                ticketModifierList[0].FormOfPayment[0].Key = "123";
                ticketModifierList[0].FormOfPayment[0].Type = "Cash";
            }
            UAPIdll.WebAir15.DocumentModifiers docModifier= new DocumentModifiers();
            //docModifier=new DocumentModifiers();
            docModifier.GenerateAccountingInterface = true;
            ticketModifierList[0].DocumentModifiers = docModifier;
            request.AirTicketingModifiers = ticketModifierList;

            //ticketModifierList[0].
            //request.AirTicketingModifiers

            
            UAPIdll.WebAir15.AirTicketingBinding binding = new UAPIdll.WebAir15.AirTicketingBinding();
            binding.Url = urlAir;
            //binding.Credentials = new NetworkCredential(UserName, Password);
            binding.Credentials = new NetworkCredential(_uapiCredentials.UserName, _uapiCredentials.Password);// Mod by ziya for credentials clash
            binding.PreAuthenticate = true;//to implement GZIP

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(AirTicketingReq));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, request); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirTicketingReq.xml");
                string filePath = xmlPath+"FlightTicketingRequest_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
            }
            UAPIdll.WebAir15.AirTicketingRsp response = null;
            try
            {
                response = binding.service(request);

            }
            catch { throw; }
            //{
            //    throw ex;
            //}

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(AirTicketingRsp));
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, response); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docres = new XmlDocument();
                docres.LoadXml(sbRes.ToString());
                //docres.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\AirTicketingRes.xml");
                string filePath = xmlPath+"FlightTicketingResponse_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                docres.Save(filePath);
            }
            Trace.TraceInformation("UApi.GenerateRetrievePNRObject exiting");
            return response;
        }
        # endregion

        # region Import UR 
        private static UAPIdll.WebUR15.UniversalRecordImportRsp ImportUR(string pnr, UAPICredentials _uapiCredentials)
        {
            Connection();
            Trace.TraceInformation("UApi.ImportUR entered");
            //UAPIdll.WebUR15.UniversalRecordRetrieveReq request = new UAPIdll.WebUR15.UniversalRecordRetrieveReq();

            //request.TargetBranch = targetBranch;
            //request.UniversalRecordLocatorCode = pnr;

            //UAPIdll.WebUR15.BillingPointOfSaleInfo pos = new UAPIdll.WebUR15.BillingPointOfSaleInfo();
            //pos.OriginApplication = originalApplication;
            //request.BillingPointOfSaleInfo = pos;

            //UAPIdll.WebUR15.UniversalRecordRetrieveServiceBinding binding = new UAPIdll.WebUR15.UniversalRecordRetrieveServiceBinding();
            //binding.Url = urlUR;
            //binding.Credentials = new NetworkCredential(userName, password);


            UAPIdll.WebUR15.UniversalRecordImportReq request = new UAPIdll.WebUR15.UniversalRecordImportReq();

            //request.TargetBranch = TargetBranch;
            request.TargetBranch = _uapiCredentials.TargetBranch;// MOd by ziya uapi credntials clash
            
            //request.UniversalRecordLocatorCode = pnr;

            request.ProviderCode = "1G";// TODO-- add Dynamic Provider
            request.ProviderLocatorCode = pnr;


            UAPIdll.WebUR15.BillingPointOfSaleInfo pos = new UAPIdll.WebUR15.BillingPointOfSaleInfo();
            pos.OriginApplication = originalApplication;
            request.BillingPointOfSaleInfo = pos;

            UAPIdll.WebUR15.UniversalRecordImportServiceBinding binding = new UAPIdll.WebUR15.UniversalRecordImportServiceBinding();
            binding.Url = urlUR;
            //binding.Credentials = new NetworkCredential(UserName, Password);
            binding.Credentials = new NetworkCredential(_uapiCredentials.UserName, _uapiCredentials.Password);// MOd by ziya uapi credntials clash
            binding.PreAuthenticate = true;//to implement GZIP


            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.WebUR15.UniversalRecordImportReq));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, request); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sb.ToString());
                //doc.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\URImportReq.xml");
                string filePath = xmlPath+"FlightURImportRequest_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                doc.Save(filePath);
            }

            UAPIdll.WebUR15.UniversalRecordImportRsp response = null;

            try
            {
                response = binding.service(request);
            }
            catch (SoapException se)
            {
                throw se;
            }
            catch (Exception ex)
            { throw ex; }
            //{
            //    throw ex;
            //}

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.WebUR15.UniversalRecordImportRsp));
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, response); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docres = new XmlDocument();
                docres.LoadXml(sbRes.ToString());
                //docres.Save(@"C:\Developments\DotNet\uAPI\CozmoAppXml\URimportRes.xml");
                string filePath = xmlPath+"FlightURImportResponse_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                docres.Save(filePath);
            }
            Trace.TraceInformation("UApi.ImportUR exiting");
            return response;
        }
        # endregion

        #region Void Ticket

        public static void VoidTicket(string airLocatorCode, string PNR, string universalRecord)
        {
            FlightItinerary itinerary = new FlightItinerary(FlightItinerary.GetFlightId(PNR));
            
            AirVoidDocumentReq voidReq = new AirVoidDocumentReq();
            voidReq.AirReservationLocatorCode = new AirReservationLocatorCode();
            voidReq.AirReservationLocatorCode.Value = airLocatorCode;
            voidReq.BillingPointOfSaleInfo = new BillingPointOfSaleInfo();
            voidReq.BillingPointOfSaleInfo.OriginApplication = "UAPI";
            voidReq.ProviderCode = "1G";
            voidReq.ProviderLocatorCode = PNR;
            voidReq.ShowETR = true;
            voidReq.TargetBranch = TargetBranch;

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.WebAir15.AirVoidDocumentReq));
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, voidReq); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docres = new XmlDocument();
                docres.LoadXml(sbRes.ToString());                
                string filePath = xmlPath + "FlightVoidTicketRequest_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                docres.Save(filePath);
            }

            AirVoidDocumentBinding binding = new AirVoidDocumentBinding();
            binding.Url = urlAir;
            binding.Credentials = new NetworkCredential(UserName, Password);

            AirVoidDocumentRsp voidResp = binding.service(voidReq);

            if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
            {
                System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.WebAir15.AirVoidDocumentRsp));
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                serRes.Serialize(writerRes, voidResp); 	// Here Classes are converted to XML String. 
                // This can be viewed in SB or writer.
                // Above XML in SB can be loaded in XmlDocument object
                XmlDocument docres = new XmlDocument();
                docres.LoadXml(sbRes.ToString());                
                string filePath = xmlPath + "FlightVoidTicketResponse_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                docres.Save(filePath);
            }

            if (voidResp.VoidResultInfo[0].ResultType == "Success")
            {
                try
                {
                    UAPIdll.WebUR15.UniversalRecordCancelReq urCancelReq = new UAPIdll.WebUR15.UniversalRecordCancelReq();
                    urCancelReq.BillingPointOfSaleInfo = new UAPIdll.WebUR15.BillingPointOfSaleInfo();
                    urCancelReq.BillingPointOfSaleInfo.OriginApplication = "UAPI";
                    urCancelReq.RetrieveProviderReservationDetails = true;
                    urCancelReq.TargetBranch = TargetBranch;
                    urCancelReq.UniversalRecordLocatorCode = universalRecord;
                    urCancelReq.Version = "33";

                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.WebUR15.UniversalRecordCancelReq));
                        System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                        System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                        serRes.Serialize(writerRes, urCancelReq); 	// Here Classes are converted to XML String. 
                        // This can be viewed in SB or writer.
                        // Above XML in SB can be loaded in XmlDocument object
                        XmlDocument docres = new XmlDocument();
                        docres.LoadXml(sbRes.ToString());

                        string filePath = xmlPath + "FlightURCancelRequest_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        docres.Save(filePath);
                    }

                    UAPIdll.WebUR15.UniversalRecordCancelServiceBinding cancelBinding = new UAPIdll.WebUR15.UniversalRecordCancelServiceBinding();
                    cancelBinding.Url = urlUR;
                    cancelBinding.Credentials = new NetworkCredential(UserName, Password);

                    UAPIdll.WebUR15.UniversalRecordCancelRsp urCancelResp = cancelBinding.service(urCancelReq);

                    if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                    {
                        System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.WebUR15.UniversalRecordCancelRsp));
                        System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                        System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                        serRes.Serialize(writerRes, urCancelResp); 	// Here Classes are converted to XML String. 
                        // This can be viewed in SB or writer.
                        // Above XML in SB can be loaded in XmlDocument object
                        XmlDocument docres = new XmlDocument();
                        docres.LoadXml(sbRes.ToString());
                        string filePath = xmlPath + "FlightURCancelResponse_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        docres.Save(filePath);
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Cancel UR after Void. Reason: " + ex.ToString(), "");
                }
            }

        }
        #endregion

        # region common Methods

        private static int keyGen(string keyValue)
        {
            int key = 0;
            string newKey = keyValue.Substring(0,keyValue.Length-1 );
            key = Convert.ToInt32(newKey);
            return key;


            //int key=
        }
        private static double getCurrAmount(string keyValue)
        {
            double amount = 0;
            string newAmont = keyValue.Remove(0, 3);
            amount= Convert.ToDouble(newAmont);
            return amount;
        }

        private static string getCurrency(string keyValue)// to get Currency Code (retrieve PNR)
        {
            string currencyCode = "AED";
            currencyCode = keyValue.Substring(0, 3);
            return currencyCode;
        }

        public static List<KeyValuePair<string, SSR>> GenerateSSRPaxList(FlightItinerary itinerary)
        {
            Trace.TraceInformation("UAPI.GenerateSSR entered");
            List<KeyValuePair<string, SSR>> ssrPaxList = new List<KeyValuePair<string, SSR>>();
            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                //if (itinerary.Passenger[i].Meal.Code != null || itinerary.Passenger[i].Meal.Code == string.Empty)
                //{
                //    //ssrList.Add("3SAN" + (i + 1) + itinerary.Passenger[i].Meal.Code);
                //    SSR ssr = new SSR();
                //    ssr.PaxId = itinerary.Passenger[i].PaxId;
                //    ssr.SsrCode = "MEAL";
                //    ssr.Detail = itinerary.Passenger[i].Meal.Code;
                //    KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                //    ssrPaxList.Add(ssrPax);
                //}
                //if ((itinerary.Passenger[i].FFAirline != null && itinerary.Passenger[i].FFAirline.Length > 0) && (itinerary.Passenger[i].FFNumber != null && itinerary.Passenger[i].FFNumber.Length > 0))
                //{//3SSRFQTVUAHK/UA123456382-1
                //    //ssrList.Add("3SSRFQTV" + itinerary.Passenger[i].FFAirline.ToUpper() + "HK/" + itinerary.Passenger[i].FFAirline + itinerary.Passenger[i].FFNumber + "-" + (i + 1));
                //    SSR ssr = new SSR();
                //    ssr.PaxId = itinerary.Passenger[i].PaxId;
                //    ssr.SsrCode = "FQTV";
                //    ssr.Detail = itinerary.Passenger[i].FFAirline + itinerary.Passenger[i].FFNumber;
                //    KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                //    ssrPaxList.Add(ssrPax);
                //}
                if ((itinerary.Passenger[i].Type == PassengerType.Child || itinerary.Passenger[i].Type == PassengerType.Infant) && (itinerary.Passenger[i].DateOfBirth != null && itinerary.Passenger[i].DateOfBirth > new DateTime()))
                {

                    SSR ssr = new SSR();
                    ssr.PaxId = itinerary.Passenger[i].PaxId;
                    if (itinerary.Passenger[i].Type == PassengerType.Child)
                    {
                        ssr.SsrCode = "CHLD";
                    }
                    else
                    {
                        ssr.SsrCode = "INFT";
                    }
                    ssr.Detail = itinerary.Passenger[i].LastName + "/" + itinerary.Passenger[i].FirstName + "" + itinerary.Passenger[i].Title + " " + itinerary.Passenger[i].DateOfBirth.ToString("dd/MM/yyyy");
                    KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                    ssrPaxList.Add(ssrPax);
                }
                if ((itinerary.Passenger[i].PassportNo != null && itinerary.Passenger[i].PassportNo.Length > 0) && (itinerary.Passenger[i].Country.CountryCode != null && itinerary.Passenger[i].Country.CountryCode.Length > 0))
                {

                    SSR ssr = new SSR();
                    ssr.PaxId = itinerary.Passenger[i].PaxId;
                    ssr.SsrCode = "DOCS";// passport info
                    //ssr.Detail = itinerary.Passenger[i].PassportNo + "-" + itinerary.Passenger[i].Country.CountryCode;
                    //string detail = string.Format("P/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}", itinerary.Passenger[i].Country.CountryCode, itinerary.Passenger[i].PassportNo, itinerary.Passenger[i].Country.CountryCode,
                    //itinerary.Passenger[i].DateOfBirth.ToString("ddMMMyy"),itinerary.Passenger[i].Gender==Gender.Male?"M":"F",itinerary.Passenger[i].PassportExpiry.ToString("ddMMMyy"),itinerary.Passenger[i].FirstName+" "+itinerary.Passenger[i].LastName,itinerary.Passenger[i].FirstName+" "+itinerary.Passenger[i].LastName);

                    string detail = string.Format("P/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}", itinerary.Passenger[i].Country.CountryCode, itinerary.Passenger[i].PassportNo, itinerary.Passenger[i].Country.CountryCode,
                   itinerary.Passenger[i].DateOfBirth.ToString("ddMMMyy"), itinerary.Passenger[i].Gender == Gender.Male ? "M" : "F", itinerary.Passenger[i].PassportExpiry.ToString("ddMMMyy"), itinerary.Passenger[i].FirstName, itinerary.Passenger[i].LastName);
                    ssr.Detail = detail;
                    KeyValuePair<string, SSR> ssrPax = new KeyValuePair<string, SSR>(itinerary.Passenger[i].PaxKey, ssr);
                    ssrPaxList.Add(ssrPax);
                }

            }
            Trace.TraceInformation("UAPI.GenerateSSR exiting");
            return ssrPaxList;
        }
        # endregion


        /// <summary>
        /// This method will retrieve the Latest price and will be used for Repricing
        /// </summary>
        /// <param name="result"></param>
        /// <param name="credentials"></param>
        /// <param name="fares"></param>
        /// <returns></returns>
        public static AirPriceRsp RePrice(SearchResult result, SearchType type,  UAPICredentials credentials, ref Fare[] fares, ref string BookingValuesChanged)
        {
            AirPriceRsp priceResponse = new AirPriceRsp();
            BookingValuesChanged = string.Empty;
            try
            {
                Connection();
                AirPriceReq priceReq = new AirPriceReq();
                priceReq.AirItinerary = new AirItinerary();
                priceReq.AirItinerary.AirSegment = result.UapiPricingSolution.AirSegment;

                //Update missing Provider Code and remove AirAvailInfo and FlightDetailsRef
                for (int i = 0; i < priceReq.AirItinerary.AirSegment.Length; i++)
                {
                    typeBaseAirSegment segment = priceReq.AirItinerary.AirSegment[i];
                    segment.ProviderCode = result.UapiPricingSolution.AirPricingInfo[0].ProviderCode;
                    //segment.ClassOfService = result.UapiPricingSolution.AirPricingInfo[0].BookingInfo[i].BookingCode;
                    segment.AirAvailInfo = new AirAvailInfo[0];
                    segment.FlightDetailsRef = new FlightDetailsRef[0];
                }

                /*********************************************************************************************************                 
                 *  If a Connection/SegmentIndex is returned in a Low Fare Shopping or Air Availability response,        *
                 *  it indicates a connection between two or more air segments. The Connection indicator must be used    * 
                 *  when Pricing and Booking to ensure the flights are sold correctly and a sell failure does not occur. *     
                 *  Remember, if in the Low Fare Shopping response example, the SegmentIndex indicators were 0, 1, 3, 4. * 
                 *  That means in the Air Pricing request, AirSegment 1, 2, 4, and 5 require the Connection indicator.   *                  
                 *********************************************************************************************************/
                if (result.UapiPricingSolution.Connection != null && result.UapiPricingSolution.Connection.Length > 0)
                {
                    foreach (UAPIdll.WebAir15.Connection con in result.UapiPricingSolution.Connection)
                    {
                        for (int i = 0; i < result.UapiPricingSolution.AirSegment.Length; i++)
                        {
                            if ((con.SegmentIndex) == i)
                            {
                                priceReq.AirItinerary.AirSegment[i].Connection = new Connection();
                            }
                        }
                    }
                }

                priceReq.BillingPointOfSaleInfo = new BillingPointOfSaleInfo();
                priceReq.BillingPointOfSaleInfo.OriginApplication = "UAPI";
                priceReq.CheckFlightDetails = true;
                priceReq.FareRuleType = typeFareRuleType.@short;
                priceReq.CheckOBFees = true;

                //Get the total pax count
                int paxCount = 0, adults = 0, childs = 0, infants = 0;
                for (int i = 0; i < result.FareBreakdown.Length; i++)
                {
                    paxCount += result.FareBreakdown[i].PassengerCount;
                    switch (result.FareBreakdown[i].PassengerType)
                    {
                        case PassengerType.Adult:
                            adults = result.FareBreakdown[i].PassengerCount;
                            break;
                        case PassengerType.Child:
                            childs = result.FareBreakdown[i].PassengerCount;
                            break;
                        case PassengerType.Infant:
                            infants = result.FareBreakdown[i].PassengerCount;
                            break;
                    }
                }

                //Initialize Search passengers
                priceReq.SearchPassenger = new SearchPassenger[paxCount];

                for (int p = 0; p < adults; p++)
                {
                    priceReq.SearchPassenger[p] = new SearchPassenger();
                    priceReq.SearchPassenger[p].BookingTravelerRef = (p + 1).ToString();
                    priceReq.SearchPassenger[p].Code = "ADT";
                    priceReq.SearchPassenger[p].Age = "40";// as per documentation hard-coding
                }

                for (int p = adults; p < adults + childs; p++)
                {
                    priceReq.SearchPassenger[p] = new SearchPassenger();
                    priceReq.SearchPassenger[p].BookingTravelerRef = (p + 1).ToString();
                    priceReq.SearchPassenger[p].Code = "CNN";
                    priceReq.SearchPassenger[p].Age = "5";
                }

                for (int p = (adults + childs); p < paxCount; p++)
                {
                    priceReq.SearchPassenger[p] = new SearchPassenger();
                    priceReq.SearchPassenger[p].BookingTravelerRef = (p + 1).ToString();
                    priceReq.SearchPassenger[p].Code = "INF";
                    priceReq.SearchPassenger[p].Age = "1";
                }


                priceReq.AirPricingCommand = new AirPricingCommand[1];
                priceReq.AirPricingCommand[0] = new AirPricingCommand();
                priceReq.AirPricingCommand[0].AirPricingModifiers = new AirPricingModifiers();
                //priceReq.AirPricingCommand[0].AirSegmentPricingModifiers = new AirSegmentPricingModifiers[0];
                //priceReq.AirPricingCommand[0].AirPricingModifiers.PermittedCabins = new UAPIdll.WebAir15.CabinClass[1];

                //priceReq.AirPricingCommand[0].AirPricingModifiers.PermittedCabins[0] = new UAPIdll.WebAir15.CabinClass();
                //priceReq.AirPricingCommand[0].AirPricingModifiers.PermittedCabins[0].Type = result.Flights[0][0].CabinClass;

                //priceReq.AirPricingCommand[0].CabinClass = result.Flights[0][0].CabinClass;
                //priceReq.AirPricingCommand[0].AirSegmentPricingModifiers = new AirSegmentPricingModifiers[result.UapiPricingSolution.AirSegment.Length];

                //for (int i = 0; i < result.UapiPricingSolution.AirSegment.Length; i++)
                //{
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i] = new AirSegmentPricingModifiers();
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].AirSegmentRef = result.UapiPricingSolution.AirSegment[i].Key;
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].PermittedBookingCodes = new BookingCode[1];
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].PermittedBookingCodes[0] = new BookingCode();
                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].PermittedBookingCodes[0].Code = result.UapiPricingSolution.AirPricingInfo[0].BookingInfo[i].BookingCode;

                //    priceReq.AirPricingCommand[0].AirSegmentPricingModifiers[i].CabinClass = result.Flights[0][0].CabinClass;
                //}

                priceReq.AirPricingModifiers = new AirPricingModifiers();
                priceReq.AirPricingModifiers.PermittedCabins = new UAPIdll.WebAir15.CabinClass[1];
                priceReq.AirPricingModifiers.PermittedCabins[0] = new UAPIdll.WebAir15.CabinClass();
                priceReq.AirPricingModifiers.PermittedCabins[0].Type = result.Flights[0][0].CabinClass;
                priceReq.AirPricingModifiers.InventoryRequestType = typeInventoryRequest.DirectAccess;// Always checks availability with Provider

                //Assign Master AccountCodes for AirlinePrivateFare booking if AccountCode is returned from Search Response
                UAPIdll.WebAir15.AirPricingInfo pricingInfo = result.UapiPricingSolution.AirPricingInfo[0];//Take always first Adult PricingInfo
                if (pricingInfo != null && pricingInfo.PricingMethod == UAPIdll.WebAir15.typePricingMethod.GuaranteedUsingAirlinePrivateFare && pricingInfo.FareInfo != null && pricingInfo.FareInfo.Length > 0 && pricingInfo.FareInfo[0].AccountCode != null && pricingInfo.FareInfo[0].AccountCode.Length > 0)
                {
                    //priceReq.AirPricingModifiers.AccountCodes = new UAPIdll.WebAir15.AccountCode[1];
                    //priceReq.AirPricingModifiers.AccountCodes[0] = new UAPIdll.WebAir15.AccountCode();
                    //priceReq.AirPricingModifiers.AccountCodes[0].Code = pricingInfo.FareInfo[0].AccountCode[0].Code;
                    //priceReq.AirPricingModifiers.AccountCodes[0].ProviderCode = pricingInfo.ProviderCode;
                    //if (pricingInfo.FareInfo[0].AccountCode[0].SupplierCode != null && pricingInfo.FareInfo[0].AccountCode[0].Type.Length > 0)
                    //{
                    //    priceReq.AirPricingModifiers.AccountCodes[0].SupplierCode = pricingInfo.FareInfo[0].AccountCode[0].SupplierCode;
                    //}
                    //if (pricingInfo.FareInfo[0].AccountCode[0].Type != null && pricingInfo.FareInfo[0].AccountCode[0].Type.Length > 0)
                    //{
                    //    priceReq.AirPricingModifiers.AccountCodes[0].Type = pricingInfo.FareInfo[0].AccountCode[0].Type;
                    //}
                    string[] codes = ConfigurationManager.AppSettings["UAPIAccountCodes"].Split(',');

                    priceReq.AirPricingModifiers.AccountCodeFaresOnly = false;
                    priceReq.AirPricingModifiers.AccountCodeFaresOnlySpecified = false;
                    priceReq.AirPricingModifiers.AccountCodes = new AccountCode[codes.Length];
                    for (int i = 0; i < codes.Length; i++)
                    {
                        priceReq.AirPricingModifiers.AccountCodes[i] = new AccountCode();
                        priceReq.AirPricingModifiers.AccountCodes[i].Code = codes[i];
                        priceReq.AirPricingModifiers.AccountCodes[i].ProviderCode = "1G";
                        //priceModifier.AccountCodes[i].SupplierCode = "";
                        //priceModifier.AccountCodes[i].Type = "";
                    }
                    priceReq.AirPricingModifiers.FaresIndicator = UAPIdll.WebAir15.typeFaresIndicator.PrivateFaresOnly;
                }

                priceReq.TargetBranch = credentials.TargetBranch;



                if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    try
                    {
                        System.Xml.Serialization.XmlSerializer serReq = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.WebAir15.AirPriceReq));
                        System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                        System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                        serReq.Serialize(writerRes, priceReq); 	// Here Classes are converted to XML String. 
                        // This can be viewed in SB or writer.
                        // Above XML in SB can be loaded in XmlDocument object
                        XmlDocument docres = new XmlDocument();
                        docres.LoadXml(sbRes.ToString());
                        string filePath = xmlPath + "FlightRePriceRequest_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        docres.Save(filePath);
                    }
                    catch { }
                }

                AirPriceBinding binding = new AirPriceBinding();
                binding.Url = urlAir;
                binding.Credentials = new NetworkCredential(credentials.UserName, credentials.Password);
                binding.PreAuthenticate = true;//to implement GZIP

                priceResponse = binding.service(priceReq);

                //System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.WebAir15.AirPriceRsp));
                //StreamReader sr = new StreamReader(@"C:\Developments\DotNet\uAPI\CozmoAppXml\FlightRePriceResponse_5c432f80-1727-49f3-875f-40109691330d_1_12102016_074644.xml");
                //priceResponse = serRes.Deserialize(sr) as UAPIdll.WebAir15.AirPriceRsp;
                //sr.Close();
                if (CT.Configuration.ConfigurationSystem.UAPIConfig["GenerateWSXML"] == "True")
                {
                    try
                    {
                        System.Xml.Serialization.XmlSerializer serRes = new System.Xml.Serialization.XmlSerializer(typeof(UAPIdll.WebAir15.AirPriceRsp));
                        System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                        System.IO.StringWriter writerRes = new System.IO.StringWriter(sbRes);
                        serRes.Serialize(writerRes, priceResponse); 	// Here Classes are converted to XML String. 
                        // This can be viewed in SB or writer.
                        // Above XML in SB can be loaded in XmlDocument object
                        XmlDocument docres = new XmlDocument();
                        docres.LoadXml(sbRes.ToString());
                        string filePath = xmlPath + "FlightRePriceResponse_" + SessionId + "_" + AppUserId + "_" + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".xml";
                        docres.Save(filePath);
                    }
                    catch { }
                }

                string errorMessage = string.Empty;

                if (priceResponse != null && priceResponse.AirPriceResult != null && priceResponse.AirPriceResult.Length > 0 && priceResponse.AirPriceResult[0].AirPricingSolution != null && priceResponse.AirPriceResult[0].AirPricingSolution.Length > 0)
                {
                    foreach (ResponseMessage responseMsg in priceResponse.ResponseMessage)
                    {
                        if (responseMsg.Type == ResponseMessageType.Error)
                        {
                            errorMessage += responseMsg.Value;
                        }
                    }

                    if (errorMessage.Length > 0)
                    {
                        throw new Exception(errorMessage);
                    }
                    else
                    {
                        //Retrieve Agent Exchange rate from the xml currency
                        string currency = getCurrency(priceResponse.AirPriceResult[0].AirPricingSolution[0].TotalPrice);
                        rateOfExchange = (double)ExchangeRates[currency];

                        for (int i = 0; i < priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo.Length; i++)
                        {
                            AirPricingInfo priceInfo = priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[i];

                            //Update the FareBasisCode for all Passengers
                            for (int f = 0; f < priceInfo.FareInfo.Length; f++)
                            {
                                result.UapiPricingSolution.AirPricingInfo[i].FareInfo[f].FareBasis = priceInfo.FareInfo[f].FareBasis;
                            }

                            ////////////////////////////////////////////////////////////////////////////////
                            //  Updating the BaseFare, TotalFare & SupplierFare pax type wise
                            //  Updating the PublishedFare and Tax in the Price for B2C
                            ////////////////////////////////////////////////////////////////////////////////
                            switch (priceInfo.PassengerType[0].Code)
                            {
                                case "ADT":
                                    if (fares[i].PassengerType == PassengerType.Adult)
                                    {
                                        fares[i].BaseFare = ((Convert.ToDouble(priceInfo.ApproximateBasePrice.Remove(0, 3))) * rateOfExchange) * fares[i].PassengerCount;
                                        fares[i].TotalFare = ((Convert.ToDouble(priceInfo.TotalPrice.Remove(0, 3))) * rateOfExchange) * fares[i].PassengerCount;
                                        fares[i].SupplierFare = Convert.ToDouble(priceInfo.TotalPrice.Remove(0, 3)) * fares[i].PassengerCount;
                                        result.Price.PublishedFare += ((Convert.ToDecimal(priceInfo.ApproximateBasePrice.Remove(0, 3))) * (decimal)rateOfExchange);
                                        result.Price.Tax += ((Convert.ToDecimal(priceInfo.Taxes.Remove(0, 3))) * (decimal)rateOfExchange);
                                    }
                                    break;
                                case "CNN":
                                    if (fares[i].PassengerType == PassengerType.Child)
                                    {
                                        fares[i].BaseFare = ((Convert.ToDouble(priceInfo.ApproximateBasePrice.Remove(0, 3))) * rateOfExchange) * fares[i].PassengerCount;
                                        fares[i].TotalFare = ((Convert.ToDouble(priceInfo.TotalPrice.Remove(0, 3))) * rateOfExchange) * fares[i].PassengerCount;
                                        fares[i].SupplierFare = Convert.ToDouble(priceInfo.TotalPrice.Remove(0, 3)) * fares[i].PassengerCount;
                                        result.Price.PublishedFare += ((Convert.ToDecimal(priceInfo.ApproximateBasePrice.Remove(0, 3))) * (decimal)rateOfExchange);
                                        result.Price.Tax += ((Convert.ToDecimal(priceInfo.Taxes.Remove(0, 3))) * (decimal)rateOfExchange);
                                    }
                                    break;
                                case "INF":
                                    if (fares[i].PassengerType == PassengerType.Infant)
                                    {
                                        fares[i].BaseFare = ((Convert.ToDouble(priceInfo.ApproximateBasePrice.Remove(0, 3))) * rateOfExchange) * fares[i].PassengerCount;
                                        fares[i].TotalFare = ((Convert.ToDouble(priceInfo.TotalPrice.Remove(0, 3))) * rateOfExchange) * fares[i].PassengerCount;
                                        fares[i].SupplierFare = Convert.ToDouble(priceInfo.TotalPrice.Remove(0, 3)) * fares[i].PassengerCount;
                                        result.Price.PublishedFare += ((Convert.ToDecimal(priceInfo.ApproximateBasePrice.Remove(0, 3))) * (decimal)rateOfExchange);
                                        result.Price.Tax += ((Convert.ToDecimal(priceInfo.Taxes.Remove(0, 3))) * (decimal)rateOfExchange);
                                    }
                                    break;
                            }
                        }



                        //Get the Price details from Price Response AirPricingSolution
                        decimal newTotal = Convert.ToDecimal(priceResponse.AirPriceResult[0].AirPricingSolution[0].TotalPrice.Remove(0, 3));
                        decimal newTax = Convert.ToDecimal(priceResponse.AirPriceResult[0].AirPricingSolution[0].Taxes.Remove(0, 3));
                        decimal newBasePrice = Convert.ToDecimal(priceResponse.AirPriceResult[0].AirPricingSolution[0].ApproximateBasePrice.Remove(0, 3));

                        //Get the price details from existing AirPricingSolution
                        decimal basePrice = Convert.ToDecimal(result.UapiPricingSolution.ApproximateBasePrice.Remove(0, 3));
                        decimal Taxes = Convert.ToDecimal(result.UapiPricingSolution.Taxes.Remove(0, 3));
                        decimal Total = Convert.ToDecimal(result.UapiPricingSolution.TotalPrice.Remove(0, 3));

                        /******************************************************************************************
                         *      Check whether any of segments times have changed or not
                         * ****************************************************************************************/

                        //Sort the segments based on Departure Time
                        List<typeBaseAirSegment> airSegments = new List<typeBaseAirSegment>();

                        typeBaseAirSegment[] segments = priceResponse.AirItinerary.AirSegment;
                        Array.Sort(segments, delegate(typeBaseAirSegment s1, typeBaseAirSegment s2) { return Convert.ToDateTime(s1.DepartureTime).CompareTo(Convert.ToDateTime(s2.DepartureTime)); });
                        airSegments.AddRange(segments);

                        try
                        {
                            //Check whether any time difference or Booking class difference is there
                            for (int i = 0; i < airSegments.Count; i++)
                            {
                                typeBaseAirSegment aSeg = airSegments[i];
                                BookingInfo bkgInfo = result.UapiPricingSolution.AirPricingInfo[0].BookingInfo[i];
                                //Calculate departure time and arrival time excluding the time zone
                                string depTime = string.Empty;
                                if (aSeg.DepartureTime.Contains("+"))
                                {
                                    depTime = aSeg.DepartureTime.Split('+')[0];
                                }
                                else if (aSeg.DepartureTime.Split('-').Length > 3)
                                {
                                    depTime = aSeg.DepartureTime.Remove(aSeg.DepartureTime.LastIndexOf("-"));
                                }
                                else
                                {
                                    depTime = aSeg.DepartureTime;
                                }

                                string arrTime = string.Empty;
                                if (aSeg.ArrivalTime.Contains("+"))
                                {
                                    arrTime = aSeg.ArrivalTime.Split('+')[0];
                                }
                                else if (aSeg.ArrivalTime.Split('-').Length > 3)
                                {
                                    arrTime = aSeg.ArrivalTime.Remove(aSeg.ArrivalTime.LastIndexOf("-"));
                                }
                                else
                                {
                                    arrTime = aSeg.ArrivalTime;
                                }

                                /***************************************************************************************************
                                 * Whenever there is a change in the Segment details, i.e. Departure Time, Arrival Time, Booking 
                                 * Class or Cabin Class, we are updating the changed values to BookingValuesChanged string in the 
                                 * following format. ex: if searched for DXB-BKK then DXB-BOM|D|A|B|C,BOM-BKK|A|B|C
                                 * Changed values will be added segment wise seperated by comma (,) and each change value will be
                                 * seperated by pipe '|' 
                                 * ORG-DEST - Indicates the segment origin and destination
                                 * D - Indicates Departure Time is changed
                                 * A - Indicates Arrival Time is changed
                                 * B - Indicates Booking class is changed
                                 * C - Indicates Cabin class is changed
                                 * *************************************************************************************************/
                                //Check time difference for onward segments by matching the origin & destination airport codes
                                if (i < result.Flights[0].Length && aSeg.Group == result.Flights[0][i].Group && result.Flights[0][i].Origin.AirportCode == aSeg.Origin && result.Flights[0][i].Destination.AirportCode == aSeg.Destination)
                                {
                                    FlightInfo fSeg = result.Flights[0][i];

                                    if (fSeg.DepartureTime.CompareTo(Convert.ToDateTime(depTime)) != 0)
                                    {
                                        result.IsTimesChanged = true;
                                        if (BookingValuesChanged.Length > 0)
                                        {
                                            if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                            {
                                                BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|D";
                                            }
                                            else
                                            {
                                                BookingValuesChanged += "|D";
                                            }
                                        }
                                        else
                                        {
                                            BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|D";
                                        }
                                    }
                                    if (fSeg.ArrivalTime.CompareTo(Convert.ToDateTime(arrTime)) != 0)
                                    {
                                        result.IsTimesChanged = true;
                                        if (BookingValuesChanged.Length > 0)
                                        {
                                            if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                            {
                                                BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|A";
                                            }
                                            else
                                            {
                                                BookingValuesChanged += "|A";
                                            }
                                        }
                                        else
                                        {
                                            BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|A";
                                        }
                                    }
                                    if (fSeg.BookingClass != aSeg.ClassOfService)
                                    {
                                        result.IsBookingClassChanged = true;
                                        if (BookingValuesChanged.Length > 0)
                                        {
                                            if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                            {
                                                BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|B";
                                            }
                                            else
                                            {
                                                BookingValuesChanged += "|B";
                                            }
                                        }
                                        else
                                        {
                                            BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|B";
                                        }
                                    }
                                    if (fSeg.CabinClass != bkgInfo.CabinClass)
                                    {
                                        result.IsBookingClassChanged = true;
                                        if (BookingValuesChanged.Length > 0)
                                        {
                                            if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                            {
                                                BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|C";
                                            }
                                            else
                                            {
                                                BookingValuesChanged += "|C";
                                            }
                                        }
                                        else
                                        {
                                            BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|C";
                                        }
                                    }
                                }
                                else if (type == SearchType.MultiWay)//Check difference for Multiway segments
                                {
                                    int ret = result.Flights[0].Length;
                                    if (result.Flights[1][i - ret].Origin.AirportCode == aSeg.Origin && result.Flights[1][i - ret].Destination.AirportCode == aSeg.Destination)
                                    {
                                        FlightInfo fSeg = result.Flights[1][i - ret];
                                        if (fSeg.DepartureTime.CompareTo(Convert.ToDateTime(depTime)) != 0)
                                        {
                                            result.IsTimesChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|D";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|D";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|D";
                                            }
                                        }
                                        if (fSeg.ArrivalTime.CompareTo(Convert.ToDateTime(arrTime)) != 0)
                                        {
                                            result.IsTimesChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|A";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|A";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|A";
                                            }
                                        }
                                        if (fSeg.BookingClass != aSeg.ClassOfService)
                                        {
                                            result.IsBookingClassChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|B";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|B";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|B";
                                            }
                                        }
                                        if (fSeg.CabinClass != bkgInfo.CabinClass)
                                        {
                                            result.IsBookingClassChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|C";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|C";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|C";
                                            }
                                        }
                                    }
                                }
                                else if (type == SearchType.Return)//Check difference for Return segments
                                {
                                    if (result.Flights[1][i - result.Flights[0].Length].Origin.AirportCode == aSeg.Origin && result.Flights[1][i - result.Flights[0].Length].Destination.AirportCode == aSeg.Destination)
                                    {
                                        FlightInfo fSeg = result.Flights[1][i - result.Flights[0].Length];
                                        if (fSeg.DepartureTime.CompareTo(Convert.ToDateTime(depTime)) != 0)
                                        {
                                            result.IsTimesChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|D";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|D";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|D";
                                            }
                                        }
                                        if (fSeg.ArrivalTime.CompareTo(Convert.ToDateTime(arrTime)) != 0)
                                        {
                                            result.IsTimesChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|A";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|A";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|A";
                                            }
                                        }
                                        if (fSeg.BookingClass != aSeg.ClassOfService)
                                        {
                                            result.IsBookingClassChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|B";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|B";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|B";
                                            }
                                        }
                                        if (fSeg.CabinClass != bkgInfo.CabinClass)
                                        {
                                            result.IsBookingClassChanged = true;
                                            if (BookingValuesChanged.Length > 0)
                                            {
                                                if (!BookingValuesChanged.Contains(aSeg.Origin + "-" + aSeg.Destination))
                                                {
                                                    BookingValuesChanged += "," + aSeg.Origin + "-" + aSeg.Destination + "|C";
                                                }
                                                else
                                                {
                                                    BookingValuesChanged += "|C";
                                                }
                                            }
                                            else
                                            {
                                                BookingValuesChanged = aSeg.Origin + "-" + aSeg.Destination + "|C";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, "Failed to compare dates in UAPI Reprice. " + ex.ToString(), "");
                            result.RepriceErrorMessage = "There is a technical error while confirming your price. Please Search again";
                        }

                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        //
                        //  If any of the values are not same, i.e. Base Price, Taxes , Total, Departure Time, Arrival Time or Booking Class
                        //  update the segment details and AirPricingSolution
                        //
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        /********************************************************************************************************
                         *  Update the AirSegmentDetails returned from Price Response and update the result Flight Segments     *
                         *  along with UapiPricingSolution. These values are required to pass in the Reservation Request.       *
                         *  Values that are updated include                                                                     *
                         *  1.  Segment Key                                                                                      * 
                         *  2.  Departure Time
                         *  3.  Arrival Time
                         *  4.  Booking Class
                         *  5.  Cabin Class
                         *  5.  Duration
                         *  6.  Group
                         *  7.  Flight Number
                         *  8.  Link Availability
                         *  9.  Availability Source
                         *  10. Polled Availability Option
                         *  11. Provider Code
                         *  12. Segment Ref Key
                         *  13. Code Share Info                                                                                 *    
                        * *******************************************************************************************************/


                        if (basePrice != newBasePrice || Taxes != newTax || Total != newTotal || result.IsTimesChanged || result.IsBookingClassChanged)
                        {
                            //Assign the new UapiPricingSolution returned from the pricing response.
                            result.UapiPricingSolution = priceResponse.AirPriceResult[0].AirPricingSolution[0];
                            //Assign the segments returned from the price response bcoz segments will be empty in UapiPricingSolution
                            result.UapiPricingSolution.AirSegment = priceResponse.AirItinerary.AirSegment;

                            foreach (UAPIdll.WebAir15.AirPricingInfo price in result.UapiPricingSolution.AirPricingInfo)
                            {
                                price.BaggageAllowances = null;//Remove the baggage info 

                                foreach (UAPIdll.WebAir15.FareInfo fare in price.FareInfo)
                                {
                                    fare.Endorsement = new Endorsement[0];//Remove any endorsements                                    
                                }
                            }


                            //Update the result segments as per segment details
                            for (int i = 0; i < airSegments.Count; i++)
                            {
                                typeBaseAirSegment segment = airSegments[i];

                                //Calculate departure time and arrival time excluding the time zone
                                string depTime = string.Empty;
                                if (segment.DepartureTime.Contains("+"))
                                {
                                    depTime = segment.DepartureTime.Split('+')[0];
                                }
                                else if (segment.DepartureTime.Split('-').Length > 3)
                                {
                                    depTime = segment.DepartureTime.Remove(segment.DepartureTime.LastIndexOf("-"));
                                }
                                else
                                {
                                    depTime = segment.DepartureTime;
                                }

                                string arrTime = string.Empty;
                                if (segment.ArrivalTime.Contains("+"))
                                {
                                    arrTime = segment.ArrivalTime.Split('+')[0];
                                }
                                else if (segment.ArrivalTime.Split('-').Length > 3)
                                {
                                    arrTime = segment.ArrivalTime.Remove(segment.ArrivalTime.LastIndexOf("-"));
                                }
                                else
                                {
                                    arrTime = segment.ArrivalTime;
                                }

                                //Assign onward segment details 
                                if (i < result.Flights[0].Length && result.Flights[0][i].Origin.AirportCode == segment.Origin && result.Flights[0][i].Destination.AirportCode == segment.Destination)
                                {
                                    result.Flights[0][i].UapiSegmentRefKey = segment.Key;
                                    result.Flights[0][i].BookingClass = segment.ClassOfService;
                                    result.Flights[0][i].DepartureTime = Convert.ToDateTime(depTime);
                                    result.Flights[0][i].ArrivalTime = Convert.ToDateTime(arrTime);
                                    result.Flights[0][i].UapiArrivalTime = segment.ArrivalTime;
                                    result.Flights[0][i].UapiDepartureTime = segment.DepartureTime;
                                    result.Flights[0][i].Duration = new TimeSpan(0, Convert.ToInt32(segment.FlightTime), 0);
                                    result.Flights[0][i].Group = segment.Group;
                                    result.Flights[0][i].FlightNumber = segment.FlightNumber;
                                    result.Flights[0][i].CabinClass = priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[0].BookingInfo[0].CabinClass;

                                    result.Flights[0][i].UAPIReservationValues[UAPI.LINK_AVAILABILITY] = segment.LinkAvailability;
                                    result.Flights[0][i].UAPIReservationValues[UAPI.POLLED_AVAILABILITY_OPTION] = segment.PolledAvailabilityOption;
                                    result.Flights[0][i].UAPIReservationValues[UAPI.AVAILABILITY_SOURCE] = segment.AvailabilitySource;
                                    result.Flights[0][i].UAPIReservationValues[UAPI.PROVIDER_CODE] = segment.ProviderCode;
                                    result.Flights[0][i].UAPIReservationValues[UAPI.SEGMENT_REF] = segment.Key;
                                    if (segment.CodeshareInfo != null && segment.CodeshareInfo.OperatingCarrier != result.Flights[0][i].Airline)
                                    {
                                        result.Flights[0][i].OperatingCarrier = segment.CodeshareInfo.OperatingCarrier + "-" + segment.CodeshareInfo.OperatingFlightNumber + " " + segment.CodeshareInfo.Value;
                                    }
                                }
                                else if (type != SearchType.OneWay)//Assign Multiway & Return segment details
                                {
                                    int ret = result.Flights[0].Length;
                                    if (result.Flights[1][i - ret].Origin.AirportCode == segment.Origin && result.Flights[1][i - ret].Destination.AirportCode == segment.Destination)
                                    {
                                        result.Flights[1][i - ret].UapiSegmentRefKey = segment.Key;
                                        result.Flights[1][i - ret].BookingClass = segment.ClassOfService;
                                        result.Flights[1][i - ret].ArrivalTime = Convert.ToDateTime(arrTime);
                                        result.Flights[1][i - ret].DepartureTime = Convert.ToDateTime(depTime);
                                        result.Flights[1][i - ret].UapiArrivalTime = segment.ArrivalTime;
                                        result.Flights[1][i - ret].UapiDepartureTime = segment.DepartureTime;
                                        result.Flights[1][i - ret].Duration = new TimeSpan(0, Convert.ToInt32(segment.FlightTime), 0);
                                        result.Flights[1][i - ret].Group = segment.Group;
                                        result.Flights[1][i - ret].FlightNumber = segment.FlightNumber;
                                        result.Flights[1][i - ret].CabinClass = priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[0].BookingInfo[0].CabinClass;
                                        result.Flights[1][i - ret].UAPIReservationValues[UAPI.LINK_AVAILABILITY] = segment.LinkAvailability;
                                        result.Flights[1][i - ret].UAPIReservationValues[UAPI.POLLED_AVAILABILITY_OPTION] = segment.PolledAvailabilityOption;
                                        result.Flights[1][i - ret].UAPIReservationValues[UAPI.AVAILABILITY_SOURCE] = segment.AvailabilitySource;
                                        result.Flights[1][i - ret].UAPIReservationValues[UAPI.PROVIDER_CODE] = segment.ProviderCode;
                                        result.Flights[1][i - ret].UAPIReservationValues[UAPI.SEGMENT_REF] = segment.Key;
                                        if (segment.CodeshareInfo != null && segment.CodeshareInfo.OperatingCarrier != result.Flights[1][i - ret].Airline)
                                        {
                                            result.Flights[1][i - ret].OperatingCarrier = segment.CodeshareInfo.OperatingCarrier + "-" + segment.CodeshareInfo.OperatingFlightNumber + " " + segment.CodeshareInfo.Value;
                                        }
                                    }
                                }
                                //else if (type == SearchType.Return)
                                //{
                                //    if (result.Flights[1][i - result.Flights[1].Length].Origin.AirportCode == segment.Origin && result.Flights[1][i - result.Flights[1].Length].Destination.AirportCode == segment.Destination)
                                //    {
                                //        result.Flights[1][i - result.Flights[1].Length].UapiSegmentRefKey = segment.Key;
                                //        result.Flights[1][i - result.Flights[1].Length].BookingClass = segment.ClassOfService;
                                //        result.Flights[1][i - result.Flights[1].Length].ArrivalTime = Convert.ToDateTime(arrTime);
                                //        result.Flights[1][i - result.Flights[1].Length].DepartureTime = Convert.ToDateTime(depTime);
                                //        result.Flights[1][i - result.Flights[1].Length].UapiArrivalTime = segment.ArrivalTime;
                                //        result.Flights[1][i - result.Flights[1].Length].UapiDepartureTime = segment.DepartureTime;
                                //        result.Flights[1][i - result.Flights[1].Length].Duration = new TimeSpan(0, Convert.ToInt32(segment.FlightTime), 0);
                                //        result.Flights[1][i - result.Flights[1].Length].Group = segment.Group;
                                //        result.Flights[1][i - result.Flights[1].Length].FlightNumber = segment.FlightNumber;
                                //        result.Flights[1][i - result.Flights[1].Length].CabinClass = priceResponse.AirPriceResult[0].AirPricingSolution[0].AirPricingInfo[0].BookingInfo[0].CabinClass;
                                //        result.Flights[1][i - result.Flights[1].Length].UAPIReservationValues[UAPI.LINK_AVAILABILITY] = segment.LinkAvailability;
                                //        result.Flights[1][i - result.Flights[1].Length].UAPIReservationValues[UAPI.POLLED_AVAILABILITY_OPTION] = segment.PolledAvailabilityOption;
                                //        result.Flights[1][i - result.Flights[1].Length].UAPIReservationValues[UAPI.AVAILABILITY_SOURCE] = segment.AvailabilitySource;
                                //        result.Flights[1][i - result.Flights[1].Length].UAPIReservationValues[UAPI.PROVIDER_CODE] = segment.ProviderCode;
                                //        result.Flights[1][i - result.Flights[1].Length].UAPIReservationValues[UAPI.SEGMENT_REF] = segment.Key;
                                //        if (segment.CodeshareInfo != null && segment.CodeshareInfo.OperatingCarrier != result.Flights[1][i - result.Flights[1].Length].Airline)
                                //        {
                                //            result.Flights[1][i - result.Flights[1].Length].OperatingCarrier = segment.CodeshareInfo.OperatingCarrier + "-" + segment.CodeshareInfo.OperatingFlightNumber + " " + segment.CodeshareInfo.Value;
                                //        }
                                //    }
                                //}
                            }
                        }
                    }
                }
            }
            catch (SoapException se)
            {
                result.RepriceErrorMessage = se.Message;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(UAPI Reprice)" + ex.ToString(), "");
                result.RepriceErrorMessage = "There is a technical error while confirming your price. Please Search again";
            }

            return priceResponse;
        }
    }
}
