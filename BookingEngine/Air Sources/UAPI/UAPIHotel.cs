﻿using CT.Configuration;
using CT.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Web.Script.Serialization;
using UAPIdll.Hotel;

namespace CT.BookingEngine.GDS
{
    public class UAPIHotel
    {
        #region Variables
        /// <summary>
        ///  This variable is used store the log file path, which is read from api configuration file
        /// </summary>
        string xmlPath = string.Empty;
        /// <summary>
        /// This variable is used, to send the language(response will be returned in same language Ex:en-us) to api, which is read from api configuration file
        /// </summary>
        string language = string.Empty;
        /// <summary>
        /// This variable is used,to send the currency(response will be returned in same currency Ex:AED) to api, which is read from api configuration file
        /// </summary>
        string currency = string.Empty;
        /// <summary>
        /// This variable is used,to store rateOfExchange value 
        /// </summary>
        decimal rateOfExchange = 1;
        /// <summary>
        /// This variable is used,to get exchange rates based on the login agent, which is read from MetaserchEngine
        /// </summary>
        Dictionary<string, decimal> exchangeRates;
        /// <summary>
        /// This variable is used,to be rounded off HotelFare, which is read from MetaserchEngine
        /// </summary>
        int decimalPoint;
        /// <summary>
        /// This variable is used,to get agent currency based on the login agent, which is read from MetaserchEngine  
        /// </summary>
        string agentCurrency;
        /// <summary>
        /// This variable is used,to get apicountry code based on the api, which is read from MetaserchEngine  
        /// </summary>
        string sourceCountryCode;
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        string sessionId;
        /// <summary>
        /// This variable is used,to store the login userid,which is read from MetaserchEngine  
        /// </summary>
        long _UserId;
        /// <summary>
        /// UAPI accessprofile code
        /// </summary>
        string _TargetBranch;
        /// <summary>
        /// UAPI access token
        /// </summary>
        string _Token;
        /// <summary>
        /// UAPI rate plan codes
        /// </summary>
        string _RatePlanCodes;
        /// <summary>
        /// UAPI host username
        /// </summary>
        string _UserName;
        /// <summary>
        /// UAPI host password
        /// </summary>
        string _Password;
        /// <summary>
        /// UAPI host url
        /// </summary>
        string _sHostURL;
        /// <summary>
        /// UAPI more results url
        /// </summary>
        string _moreResultsURL;

        HotelRequest clsHotreq;
        List<HotelSearchResult> liHotresult = new List<HotelSearchResult>();
        JavaScriptSerializer js = new JavaScriptSerializer();
        
        #endregion

        #region Properties
        /// <summary>
        /// Exchange rates to be used for conversion in Agent currency from suplier currency
        /// </summary>
        public Dictionary<string, decimal> ExchangeRates { get => exchangeRates; set => exchangeRates = value; }
        /// <summary>
        /// Decimal value to be rounded off
        /// </summary>
        public int DecimalPoint { get => decimalPoint; set => decimalPoint = value; }
        /// <summary>
        /// Base currency used by the Agent
        /// </summary>
        public string AgentCurrency { get => agentCurrency; set => agentCurrency = value; }
        /// <summary>
        /// Api source countryCode
        /// </summary>
        public string SourceCountryCode { get => sourceCountryCode; set => sourceCountryCode = value; }
        /// <summary>
        /// this session id is unquie for every Request,which is read from MetaserchEngine
        /// </summary>
        public string SessionId { get => sessionId; set => sessionId = value; }
        /// <summary>
        /// this UserId is store user id
        /// </summary>
        public long UserId { get => _UserId; set => _UserId = value; }
        /// <summary>
        /// accessprofile code
        /// </summary>
        public string TargetBranch { get => _TargetBranch; set => _TargetBranch = value; }
        /// <summary>
        /// access token code
        /// </summary>
        public string Token { get => _Token; set => _Token = value; }
        /// <summary>
        /// Rate plan codes to get special offers for corporate
        /// </summary>
        public string RatePlanCodes { get => _RatePlanCodes; set => _RatePlanCodes = value; }
        /// <summary>
        /// to store UAPI hotel user name
        /// </summary>
        public string UserName { get => _UserName; set => _UserName = value; }
        /// <summary>
        /// to store UAPI hotel password
        /// </summary>
        public string Password { get => _Password; set => _Password = value; }
        /// <summary>
        /// Rate plan codes to get special offers for corporate
        /// </summary>
        public string sHostURL { get => _sHostURL; set => _sHostURL = value; }
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public UAPIHotel()
        {
            Initialize();
        }
        #endregion

        /// <summary>
        /// /* To initialize required properties and create the log path folder if it is not there */
        /// </summary>
        private void Initialize()
        {
            try {

                xmlPath = ConfigurationSystem.UAPIHotelConfig["XmlLogPath"] + DateTime.Now.ToString("dd-MM-yyy") + "\\";
                language = ConfigurationSystem.UAPIHotelConfig["Lang"];
                currency = ConfigurationSystem.UAPIHotelConfig["Currency"];
                sHostURL = ConfigurationSystem.UAPIHotelConfig["HostUrl"];
                TargetBranch = ConfigurationSystem.UAPIHotelConfig["TargetBranch"];
                Token = ConfigurationSystem.UAPIHotelConfig["Token"];

                if (!Directory.Exists(xmlPath))
                    Directory.CreateDirectory(xmlPath);
            }
            catch (Exception ex){
                Audit.Add(EventType.GimmonixSearch, Severity.Normal, 1, "Failed to initialize UAPI Hotel object properties Err :" + ex.ToString(), "0");
            }
        }

        /// <summary>
        /// Common method to make web request call for UAPI hotel bookings
        /// </summary>
        /// <param name="sUrl"></param>
        /// <param name="sToken"></param>
        /// <param name="sMethod"></param>
        /// <param name="liHeaders"></param>
        /// <param name="sReqName"></param>
        /// <returns></returns>
        private string WebReqCall(string sUrl, string sMethod, List<string> liHeaders, string sReqName, string sData)
        {
            string JSresponse = string.Empty;
            try
            {
                ServicePointManager.SecurityProtocol = !ServicePointManager.SecurityProtocol.HasFlag(SecurityProtocolType.Tls12) ?
                    ServicePointManager.SecurityProtocol | SecurityProtocolType.Tls12 : ServicePointManager.SecurityProtocol;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sUrl);
                request.Method = sMethod;
                request.ContentType = request.Accept = "application/json";
                request.Headers.Add(HttpRequestHeader.Authorization, Token);
                request.Headers.Add("accessProfile", TargetBranch);
                request.Headers.Add("e2etrackingid", TargetBranch + "_Cozmo");

                if (liHeaders != null && liHeaders.Count > 0)
                    liHeaders.ForEach(x => request.Headers.Add(x.Split('|')[0], x.Split('|')[1]));

                if (!string.IsNullOrEmpty(sData))
                {
                    GenericStatic.GenerateLogs(true, true, ref sData, xmlPath + sReqName + "ReqData" + "_" + SessionId);
                    byte[] bytes = Encoding.ASCII.GetBytes(sData);
                    request.ContentLength = bytes.Length;
                    Stream requestWriter = (request.GetRequestStream());
                    requestWriter.Write(bytes, 0, bytes.Length);
                    requestWriter.Close();
                }

                string sReq = SerializeJson(request);
                GenericStatic.GenerateLogs(true, true, ref sReq, xmlPath + sReqName + "WebReq" + "_" + SessionId);

                HttpWebResponse httpResponse = (HttpWebResponse)request.GetResponse();

                Stream dataStream = httpResponse.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                JSresponse = reader.ReadToEnd();

                GenericStatic.GenerateLogs(true, true, ref JSresponse, xmlPath + sReqName + "Rsp" + "_" + SessionId);
            }
            catch (WebException ex)
            {                
                string message = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                Audit.Add(EventType.GimmonixSearch, Severity.Normal, 1, "UAPI Hotel Search Request soap exception Err :" + message, "0");
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return JSresponse;
        }

        /// <summary>
        /// /* To search hotels and prepare hotel results object based on request given */
        /// </summary>
        /// <param name="request"></param>
        /// <param name="markup"></param>
        /// <param name="markupType"></param>
        /// <param name="discount"></param>
        /// <param name="discounttype"></param>
        /// <returns></returns>
        public HotelSearchResult[] GetSearchResults(HotelRequest request, decimal markup, string markupType, decimal discount, string discounttype)
        {
            try
            {
                clsHotreq = request;
                liHotresult = liHotresult == null ? new List<HotelSearchResult>() : liHotresult;

                int TotalPax = clsHotreq.RoomGuest.Sum(x => x.noOfAdults + x.noOfChild);

                /* Prepare property request URL and make a web request call */
                var liProplist = GetPropertyList(clsHotreq.StartDate.ToString("yyyyMMdd"), clsHotreq.EndDate.ToString("yyyyMMdd"),
                    TotalPax.ToString(), clsHotreq.Radius > 25 ? 25 : clsHotreq.Radius, clsHotreq.CityName, clsHotreq.CityName, clsHotreq.CountryCode, clsHotreq.Latitude, clsHotreq.Longtitude,
                    null, null, null);

                /* Bind hotel result object with the retrieved results */
                if (liProplist != null && liProplist.PropertyInfo != null && liProplist.PropertyInfo.Count > 0)
                    BindSearchResult(liProplist, request, ref liHotresult, null, markup, markupType, discount, discounttype);                
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GimmonixSearch, Severity.Normal, 1, "UAPI Hotel Search Request Err :" + ex.ToString(), "0");
                //throw ex;
            }

            return liHotresult.ToArray();
        }

        /// <summary>
        /// To get list of properties
        /// </summary>
        /// <param name="checkinDate"></param>
        /// <param name="checkoutDate"></param>
        /// <param name="numberOfGuests"></param>
        /// <param name="radius"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="country"></param>
        /// <param name="lat"></param>
        /// <param name="lon"></param>
        /// <param name="minRate"></param>
        /// <param name="maxRate"></param>
        /// <param name="getMoreKey"></param>
        /// <param name="sToken"></param>
        /// <returns></returns>
        public Properties GetPropertyList(string checkinDate, string checkoutDate, string numberOfGuests, int radius, string city, 
            string state, string country, double? lat, double? lon, object minRate, object maxRate, int? getMoreKey)
        {
            Properties clsProps = new Properties();            

            try
            {
                var urlBuilder_ = new StringBuilder();

                if (!string.IsNullOrEmpty(_moreResultsURL))
                    urlBuilder_.Append(_moreResultsURL);
                else
                {
                    /* Prepare property request URL based on available input values */
                    if (string.IsNullOrEmpty(checkinDate))
                        throw new ArgumentNullException("checkinDate");

                    if (string.IsNullOrEmpty(checkoutDate))
                        throw new ArgumentNullException("checkoutDate");

                    if (numberOfGuests == "0" || string.IsNullOrEmpty(numberOfGuests))
                        throw new ArgumentNullException("numberOfGuests");

                    if (radius == 0)
                        throw new ArgumentNullException("radius");

                    urlBuilder_.Append(sHostURL != null ? sHostURL.TrimEnd('/') : "").Append("/shop/v4/properties?");
                    urlBuilder_.Append(Uri.EscapeDataString("checkinDate") + "=").Append(Uri.EscapeDataString(ConvertToString(checkinDate, CultureInfo.InvariantCulture))).Append("&");
                    urlBuilder_.Append(Uri.EscapeDataString("checkoutDate") + "=").Append(Uri.EscapeDataString(ConvertToString(checkoutDate, CultureInfo.InvariantCulture))).Append("&");
                    urlBuilder_.Append(Uri.EscapeDataString("numberOfGuests") + "=").Append(Uri.EscapeDataString(ConvertToString(numberOfGuests, CultureInfo.InvariantCulture))).Append("&");
                    urlBuilder_.Append(Uri.EscapeDataString("radius") + "=").Append(Uri.EscapeDataString(ConvertToString(radius, CultureInfo.InvariantCulture))).Append("&");

                    if (!string.IsNullOrEmpty(city))
                        urlBuilder_.Append(Uri.EscapeDataString("city") + "=").Append(Uri.EscapeDataString(ConvertToString(city, CultureInfo.InvariantCulture))).Append("&");

                    if (!string.IsNullOrEmpty(state))
                        urlBuilder_.Append(Uri.EscapeDataString("state") + "=").Append(Uri.EscapeDataString(ConvertToString(state, CultureInfo.InvariantCulture))).Append("&");

                    if (!string.IsNullOrEmpty(country))
                        urlBuilder_.Append(Uri.EscapeDataString("country") + "=").Append(Uri.EscapeDataString(ConvertToString(country, CultureInfo.InvariantCulture))).Append("&");

                    if (lat != 0)
                        urlBuilder_.Append(Uri.EscapeDataString("lat") + "=").Append(Uri.EscapeDataString(ConvertToString(lat, CultureInfo.InvariantCulture))).Append("&");

                    if (lon != 0)
                        urlBuilder_.Append(Uri.EscapeDataString("lon") + "=").Append(Uri.EscapeDataString(ConvertToString(lon, CultureInfo.InvariantCulture))).Append("&");

                    if (minRate != null)
                        urlBuilder_.Append(Uri.EscapeDataString("minRate") + "=").Append(Uri.EscapeDataString(ConvertToString(minRate, CultureInfo.InvariantCulture))).Append("&");

                    if (maxRate != null)
                        urlBuilder_.Append(Uri.EscapeDataString("maxRate") + "=").Append(Uri.EscapeDataString(ConvertToString(maxRate, CultureInfo.InvariantCulture))).Append("&");

                    if (getMoreKey != null)
                        urlBuilder_.Append(Uri.EscapeDataString("getMoreKey") + "=").Append(Uri.EscapeDataString(ConvertToString(getMoreKey, CultureInfo.InvariantCulture))).Append("&");

                    urlBuilder_.Length--;
                }

                /* Prepare required header keys to make property request call */
                List<string> liHeaders = new List<string>();
                liHeaders.Add("hotelContent|yes"); liHeaders.Add("imageSize|M"); liHeaders.Add("sort|rating"); liHeaders.Add("maxProperties|25");

                /* Make web request call to retrieve the resutls */
                string response = WebReqCall(urlBuilder_.ToString(), "GET", liHeaders, "Search", string.Empty);

                /* De serilize the Json response to properties object */
                clsProps = js.Deserialize<Properties>(response.Replace("\"Properties\": {", "").TrimEnd('}'));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return clsProps;
        }

        /// <summary>
        /// /* To prepare hotel result object from search result response object */
        /// </summary>
        /// <param name="liProplist"></param>
        /// <param name="request"></param>
        /// <param name="hotelResults"></param>
        /// <param name="staticdata"></param>
        /// <param name="markup"></param>
        /// <param name="markupType"></param>
        /// <param name="discount"></param>
        /// <param name="discountType"></param>
        private void BindSearchResult(Properties liProplist, HotelRequest request, ref List<HotelSearchResult> hotelResults,
            List<HotelStaticData> staticdata, decimal markup, string markupType, decimal discount, string discountType)
        {
            try
            {
                liProplist.PropertyInfo.Where(x => x.Property != null && !string.IsNullOrEmpty(x.Property.Id) &&
                    x.Property.Image != null && x.Property.Image.Where(i => !string.IsNullOrEmpty(i.Value)).ToList().Count > 0 && 
                    x.LowestAvailableRate != null && !string.IsNullOrEmpty(x.LowestAvailableRate.Code) &&
                    x.LowestAvailableRate.Value != null && x.LowestAvailableRate.Value > 0 && x.Property.NextSteps != null && 
                    x.Property.NextSteps.NextStep != null && x.Property.NextSteps.NextStep.Count > 0).ToList().ForEach(y =>
                    {

                        HotelSearchResult clsHResult = new HotelSearchResult();
                        clsHResult.HotelCode = y.Id;
                        clsHResult.HotelName = y.Property.Name;

                        if (y.Property.Rating != null && y.Property.Rating.Count > 0 && y.Property.Rating.Where(r => !string.IsNullOrEmpty(r.Value)).Count() > 0)
                            clsHResult.Rating = (HotelRating)Convert.ToInt32(y.Property.Rating.Select(x => x.Value.Substring(0, 1)).FirstOrDefault());

                        clsHResult.HotelFacilities = null;
                        clsHResult.HotelMap = y.Property.GeoLocation != null ? Convert.ToString(y.Property.GeoLocation.Latitude) + "||" +
                            Convert.ToString(y.Property.GeoLocation.Longitude) : Convert.ToString(request.Latitude) + "||" + Convert.ToString(request.Longtitude);
                        
                        clsHResult.RoomGuest = request.RoomGuest;
                        clsHResult.StartDate = request.StartDate;
                        clsHResult.EndDate = request.EndDate;
                        clsHResult.Currency = agentCurrency;
                        clsHResult.BookingSource = HotelBookingSource.UAH;

                        /* if supplier currency is not there with our agent exchange rates info skip the binding and throw exception */
                        if (!exchangeRates.ContainsKey(y.LowestAvailableRate.Code))
                            throw new Exception("EXCHANGE RATES NOT AVAILABLE FOR CURRENCY CODE: (" + y.LowestAvailableRate.Code + 
                                ") WITH AGENT CURRENCY CODE: (" + agentCurrency + "). \r\n City Name: " + request.CityName + ", Country code/name: " + 
                                request.CountryCode + "/" + request.CountryName + ", User Id: " + UserId);

                        rateOfExchange = exchangeRates[y.LowestAvailableRate.Code];
                        clsHResult.Price = new PriceAccounts();
                        clsHResult.Price.SupplierCurrency = y.LowestAvailableRate.Code;
                        clsHResult.Price.RateOfExchange = rateOfExchange;

                        if (y.Property.Image != null && y.Property.Image.Where(i => !string.IsNullOrEmpty(i.Value)).ToList().Count > 0)
                        {
                            clsHResult.HotelPicture = y.Property.Image.Select(m => m.Value).FirstOrDefault().ToString();
                            clsHResult.HotelPicture = !string.IsNullOrEmpty(clsHResult.HotelPicture) && !clsHResult.HotelPicture.ToLower().Contains("https") ?
                                clsHResult.HotelPicture.Replace("http", "https") : clsHResult.HotelPicture;
                            clsHResult.HotelImages = new List<string>();
                            clsHResult.HotelImages = y.Property.Image.Where(im => !string.IsNullOrEmpty(im.Value)).
                                Select(simg => !simg.Value.ToLower().StartsWith("https") ? simg.Value.Replace("http", "https") : simg.Value).ToList();
                        }

                        decimal totalprice = Convert.ToDecimal(y.LowestAvailableRate.Value);
                        decimal hotelTotalPrice = 0m;
                        decimal vatAmount = 0m;

                        /* VAT Calculations */
                        PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                        hotelTotalPrice = Math.Round(totalprice * rateOfExchange, decimalPoint);
                        if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                            hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);

                        hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);

                        clsHResult.Price = new PriceAccounts();
                        clsHResult.Price.SupplierPrice = Math.Round(totalprice);
                        clsHResult.Price.RateOfExchange = rateOfExchange;
                        clsHResult.SellingFare = hotelTotalPrice;
                        clsHResult.TotalPrice = clsHResult.SellingFare + (markupType == "F" ? markup * request.NoOfRooms : clsHResult.SellingFare * (markup / 100));
                        clsHResult.Price.Discount = 0;
                        if (markup > 0 && discount > 0)
                        {
                            clsHResult.Price.Discount = discountType == "F" ? ((markupType == "F" ? (markup - discount) * request.NoOfRooms : (discount * request.NoOfRooms)))
                                                   : (markupType == "F" ? (((markup * request.NoOfRooms) * discount) / 100) : (clsHResult.TotalPrice - clsHResult.SellingFare) * (discount / 100));
                        }
                        clsHResult.Price.Discount = clsHResult.Price.Discount > 0 ? clsHResult.Price.Discount : 0;
                        clsHResult.Price.NetFare = clsHResult.TotalPrice;
                        clsHResult.Price.AccPriceType = PriceType.NetFare;
                        clsHResult.Price.RateOfExchange = rateOfExchange;
                        clsHResult.Price.Currency = agentCurrency;
                        clsHResult.Price.CurrencyCode = agentCurrency;

                        if (y.Property.NextSteps != null && y.Property.NextSteps.NextStep != null && y.Property.NextSteps.NextStep.Count > 0)
                            clsHResult.NextURL = y.Property.NextSteps.NextStep.Select(u => u.Value).FirstOrDefault();

                        liHotresult.Add(clsHResult);
                    });

                /* To save more results url to cache */
                if (liHotresult.Count > 0 && liProplist.NextSteps != null && liProplist.NextSteps.NextStep != null && liProplist.NextSteps.NextStep.Count > 0 &&
                    liProplist.NextSteps.NextStep[0].Description.ToLower() == "get more hotels" && !string.IsNullOrEmpty(liProplist.NextSteps.NextStep[0].Value))
                {
                    _moreResultsURL = liProplist.NextSteps.NextStep[0].Value;
                    GetSearchResults(request, markup, markupType, discount, discountType);
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        /// <summary>
        /// /* To get hotel details like images, address etc */
        /// </summary>
        /// <param name="clsHResult"></param>
        /// <param name="request"></param>
        public void GetHotelDetails(ref HotelSearchResult clsHResult, HotelRequest clsHotreq)
        {
            try
            {   
                string response = WebReqCall(clsHResult.NextURL, "GET", null, "HotelDetails", string.Empty);
                                
                PropertyDetail liPropdtlist = js.Deserialize<PropertyDetail>(response.Replace("\"PropertyDetail\": {", "").TrimEnd('}'));

                if (liPropdtlist != null && liPropdtlist.NextSteps != null && liPropdtlist.NextSteps.NextStep != null && liPropdtlist.NextSteps.NextStep.Count > 0)
                {
                    /* Add Hotel Images to result object */
                    clsHResult.HotelImages = liPropdtlist.Image != null && liPropdtlist.Image.Count > 0 ?
                        liPropdtlist.Image.Where(x => x != null && !string.IsNullOrEmpty(x.Value)).Select(simg => !simg.Value.ToLower().StartsWith("https") ? simg.Value.Replace("http", "https") : simg.Value).ToList() : clsHResult.HotelImages;

                    /* Add Hotel description to result object */
                    if (liPropdtlist.Description != null && liPropdtlist.Description.Where(x => !string.IsNullOrEmpty(x)).Count() > 0)
                        clsHResult.HotelDescription = String.Join(", ", liPropdtlist.Description.ToArray());

                    /* Add Hotel address to result object */
                    if (liPropdtlist.Address != null)
                    {
                        clsHResult.HotelAddress = liPropdtlist.Address.Address_Line != null ? 
                            String.Join(", ", liPropdtlist.Address.Address_Line.ToArray()) : string.Empty;
                        clsHResult.HotelAddress = string.IsNullOrEmpty(liPropdtlist.Address.Street) ? clsHResult.HotelAddress :
                            clsHResult.HotelAddress + ", " + liPropdtlist.Address.Street;
                        clsHResult.HotelAddress = string.IsNullOrEmpty(liPropdtlist.Address.City) ? clsHResult.HotelAddress :
                            clsHResult.HotelAddress + ", " + liPropdtlist.Address.City;
                        clsHResult.HotelAddress = liPropdtlist.Address.StateProv != null && !string.IsNullOrEmpty(liPropdtlist.Address.StateProv.Name) ?
                            clsHResult.HotelAddress + ", " + liPropdtlist.Address.StateProv.Name : clsHResult.HotelAddress;
                        clsHResult.HotelAddress = liPropdtlist.Address.Country != null && !string.IsNullOrEmpty(liPropdtlist.Address.Country.Name) ?
                            clsHResult.HotelAddress + ", " + liPropdtlist.Address.Country.Name : clsHResult.HotelAddress;
                        clsHResult.HotelAddress = string.IsNullOrEmpty(liPropdtlist.Address.PostalCode) ? clsHResult.HotelAddress :
                            clsHResult.HotelAddress + ", " + liPropdtlist.Address.PostalCode;
                    }

                    /* Add Hotel phone to result object */
                    clsHResult.HotelContactNo = liPropdtlist.Telephone != null && liPropdtlist.Telephone.Count() > 0
                        && !string.IsNullOrEmpty(liPropdtlist.Telephone[0]) ? liPropdtlist.Telephone[0] : clsHResult.HotelContactNo;

                    /* Add Hotel amenities to result object */
                    clsHResult.HotelFacilities = liPropdtlist.Amenity != null && liPropdtlist.Amenity.Count > 0 ?
                    liPropdtlist.Amenity.Where(x => x != null && !string.IsNullOrEmpty(x.Description)).Select(i => i.Description).ToList() : clsHResult.HotelFacilities;
                    
                    /* Add next web call url to result object */
                    clsHResult.NextURL = liPropdtlist.NextSteps.NextStep.Select(u => u.Value).LastOrDefault();
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, 1, "UAPI Hotel Details Request Err :" + ex.ToString(), "0");
            }
        }

        /// <summary>
        /// /* To get hotel rooms availability and property details */
        /// </summary>
        /// <param name="clsHResult"></param>
        /// <param name="request"></param>
        /// <param name="markup"></param>
        /// <param name="markupType"></param>
        /// <param name="sessionid"></param>
        /// <param name="discount"></param>
        /// <param name="discountType"></param>
        public void GetRoomsAvailability(ref HotelSearchResult clsHResult, HotelRequest request, decimal markup, string markupType, string sessionid, decimal discount, string discountType)
        {
            SessionId = sessionid;
            /* To get hotel images and other property info */
            if (clsHResult.HotelFacilities == null)
                GetHotelDetails(ref clsHResult, request);

            if (request.IsGetHotelDtls)
                return;

            try
            {
                /* To prepare hotel availability request */

                OfferQueryHotelRequest clsOHR = new OfferQueryHotelRequest();
                clsOHR.Currency = clsHResult.Price.SupplierCurrency;
                clsOHR.StayDates = new DateOrDateWindows { Start= request.StartDate, End= request.EndDate };
                clsOHR.HotelSearchCriterion = new HotelSearchCriterion();
                clsOHR.HotelSearchCriterion.PropertyRequest = new List<PropertyRequest>();
                clsOHR.HotelSearchCriterion.PropertyRequest.Add(new PropertyRequest { PropertyKey = new PropertyKey { PropertyCode = clsHResult.HotelCode.Split('-')[1], ChainCode = clsHResult.HotelCode.Split('-')[0] } });
                clsOHR.HotelSearchCriterion.RoomStayCandidates = new RoomStayCandidates();
                clsOHR.HotelSearchCriterion.RoomStayCandidates.RoomStayCandidate = new List<RoomStayCandidate>();

                clsOHR.HotelSearchCriterion.RatePlanCandidates = new RatePlanCandidatesDetail();
                clsOHR.HotelSearchCriterion.RatePlanCandidates.NumberOfRatePlans = 99;

                List<RatePlanCandidateDetail> liRPC = new List<RatePlanCandidateDetail>();

                if (!string.IsNullOrEmpty(RatePlanCodes))
                    RatePlanCodes.Split(',').ToList().ForEach(x => { liRPC.Add(new RatePlanCandidateDetail { RatePlanCode = x }); });

                clsOHR.HotelSearchCriterion.RatePlanCandidates.RatePlanCandidate = liRPC;

                List<GuestCount> ligGCnt = new List<GuestCount>();
                ligGCnt.Add(new GuestCount { Age=23, Count= request.RoomGuest.Sum(x => x.noOfAdults + x.noOfChild) });

                clsOHR.HotelSearchCriterion.RoomStayCandidates.RoomStayCandidate.Add(new RoomStayCandidate { GuestCounts = new GuestCounts { GuestCount = ligGCnt } });

                HotelOfferReq clsHOR = new HotelOfferReq();
                clsHOR.OfferQueryHotelRequest = clsOHR;

                /* Convert availability request object to Json and do a web request call */
                string response = WebReqCall(sHostURL.TrimEnd('/') + "/availability/v3/offers?", "POST", null, "HotelAvailability", SerializeJson(clsHOR));

                /* De serialize the response Json */
                Offers clsOffers = js.Deserialize<Offers>(response.Replace("\"Offers\": {", "").TrimEnd('}'));

                /* Read response object and update the room details to hotel result object */
                BindRoomDetails(clsOffers, clsHResult, request, markup, markupType, discount, discountType);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, 1, "UAPI Hotel Room availability request Err :" + ex.ToString(), "0");
                throw ex;
            }
        }

        /// <summary>
        /// /* To Bind the hotel result rooms object from supplier response object */
        /// </summary>
        /// <param name="liRooms"></param>
        /// <param name="clsHResult"></param>
        /// <param name="request"></param>
        /// <param name="markup"></param>
        /// <param name="markupType"></param>
        /// <param name="discount"></param>
        /// <param name="discountType"></param>
        private void BindRoomDetails(Offers clsOffers, HotelSearchResult clsHResult, HotelRequest request,
            decimal markup, string markupType, decimal discount, string discountType)
        {
            try
            {
                List<HotelRoomsDetails> rooms = new List<HotelRoomsDetails>();

                List<Offer> liRoomOffers = (List<Offer>)clsOffers.Offer; string sSupCurrency = string.Empty;
                int iSeq = 1; TimeSpan diffResult = clsHResult.EndDate.Subtract(clsHResult.StartDate);

                /* Loop each room received in the response */
                liRoomOffers.Where(x => x != null && x.Products != null && x.Products.Count > 0).ToList().ForEach(y =>
                {                    
                    HotelRoomsDetails roomDetail = new HotelRoomsDetails();
                    
                    decimal TotalPrice = Convert.ToDecimal(y.Price.TotalPrice.Where(r => r.Total != null && r.Total.Value != null).Select(t => t.Total.Value).FirstOrDefault());
                    
                    roomDetail.SequenceNo = iSeq.ToString();

                    var Prdcts = y.Products.Select(p => p).FirstOrDefault();
                    var PrdDtl = Prdcts.Product.Select(p => p).FirstOrDefault();

                    roomDetail.RoomTypeCode = roomDetail.SequenceNo + "||" + PrdDtl.BookingCode + "||" + " " + "||" + "Trip Services" + "||";

                    if (PrdDtl.RoomType != null && PrdDtl.RoomType.RoomAmenity != null && PrdDtl.RoomType.RoomAmenity.Count > 0)
                    {
                        var liCurrRoomAmenity = PrdDtl.RoomType.RoomAmenity.Where(r => !string.IsNullOrEmpty(r.Amenity)).ToList();
                        var liRoomAmenity = HAmenity.GetRoom();
                        roomDetail.Amenities = (from cr in liCurrRoomAmenity join ra in liRoomAmenity on cr.Amenity equals ra.Code orderby ra.Name
                                                    select new { ra.Name }).Select(x => x.Name).ToList();
                    }
                    
                    roomDetail.RoomTypeName = PrdDtl.RoomType != null && PrdDtl.RoomType.Description != null && !string.IsNullOrEmpty(PrdDtl.RoomType.Description.Value) ?
                        PrdDtl.RoomType.Description.Value : string.Empty;

                    var clsTAC = y.TermsAndConditions != null ? y.TermsAndConditions.Where(t => t != null).Select(c => c).FirstOrDefault() : null;
                    var IsRefund = string.Empty; var MealDesc = "Room only RO";
                    if (clsTAC != null)
                    {
                        IsRefund = clsTAC.CancelPenalty != null && clsTAC.CancelPenalty.Where(n => n.NonRefundableInd != null).Count() > 0 ? 
                            clsTAC.CancelPenalty.Where(n => n.NonRefundableInd != null).Select(m => m.NonRefundableInd).FirstOrDefault().ToString().ToLower() : IsRefund;

                        if (clsTAC.MealsIncluded != null)
                        {
                            MealDesc = clsTAC.MealsIncluded.BreakfastInd != null && clsTAC.MealsIncluded.BreakfastInd == true ? "Bed and Breakfast BB" : string.Empty;
                            MealDesc = clsTAC.MealsIncluded.LunchInd != null && clsTAC.MealsIncluded.LunchInd == true ? 
                                (string.IsNullOrEmpty(MealDesc) ? "Lunch Included" : MealDesc + ", Lunch Included") : MealDesc;
                            MealDesc = clsTAC.MealsIncluded.DinnerInd != null && clsTAC.MealsIncluded.DinnerInd == true ?
                                (string.IsNullOrEmpty(MealDesc) ? "Dinner Included" : MealDesc + ", Dinner Included") : MealDesc;
                            MealDesc = string.IsNullOrEmpty(MealDesc) ? "Room only RO" : MealDesc;
                        }

                        if (clsTAC.TextBlock != null && clsTAC.TextBlock.Count() > 0 &&
                            clsTAC.TextBlock[0].TextFormatted != null && clsTAC.TextBlock[0].TextFormatted.Count() > 0)
                        {
                            if (string.IsNullOrEmpty(roomDetail.RoomTypeName))
                            {
                                clsTAC.TextBlock[0].TextFormatted.Where(v => !string.IsNullOrEmpty(v.Value)).ToList().ForEach(rt => roomDetail.RoomTypeName += rt.Value + ", ");
                                roomDetail.RoomTypeName = roomDetail.RoomTypeName.TrimEnd(' ').TrimEnd(',');
                            }
                            else if (clsTAC.TextBlock[0].TextFormatted[0] != null && !string.IsNullOrEmpty(clsTAC.TextBlock[0].TextFormatted[0].Value) &&
                                    clsTAC.TextBlock[0].TextFormatted[0].Value.ToUpper().Contains("PETROFAC") && !roomDetail.RoomTypeName.ToUpper().Contains("PETROFAC"))
                            {
                                roomDetail.RoomTypeName = clsTAC.TextBlock[0].TextFormatted[0].Value + ", " + roomDetail.RoomTypeName;
                            }
                        }

                        if (clsTAC.Guarantee != null && clsTAC.Guarantee.Count() > 0 && !string.IsNullOrEmpty(clsTAC.Guarantee[0].Code) && 
                            (clsTAC.Guarantee[0].Code.ToUpper() == "PREPAYREQUIRED" || clsTAC.Guarantee[0].Code.ToUpper() == "DEPOSITREQUIRED" || 
                            clsTAC.Guarantee[0].Code.ToUpper() == "CREDIT CARD DEPOSIT"))
                            roomDetail.GuaranteeTypeCode = "DP";

                        if (clsTAC.Guarantee != null && clsTAC.Guarantee.Count() > 0 && clsTAC.Guarantee[0].Type != null &&
                            (clsTAC.Guarantee[0].Type == GuaranteeType_Enum.PrepayRequired || clsTAC.Guarantee[0].Type == GuaranteeType_Enum.DepositRequired))
                            roomDetail.GuaranteeTypeCode = "DP";
                    }

                    roomDetail.mealPlanDesc = MealDesc;
                    roomDetail.RoomTypeName += IsRefund == "true" ? "-Refundable" : "-Nonrefundable";                    

                    decimal hotelTotalPrice = 0m; decimal vatAmount = 0m;

                    sSupCurrency = y.Price.TotalPrice.Where(r => r.Total != null && !string.IsNullOrEmpty(r.Total.Code)).Select(t => t.Total.Code).FirstOrDefault();
                    rateOfExchange = !string.IsNullOrEmpty(sSupCurrency) && exchangeRates.ContainsKey(sSupCurrency) ? exchangeRates[sSupCurrency] : 1;

                    PriceTaxDetails priceTaxDet = PriceTaxDetails.GetVATCharges(request.CountryCode, request.LoginCountryCode, sourceCountryCode, (int)ProductType.Hotel, Module.Hotel.ToString(), DestinationType.International.ToString());
                    hotelTotalPrice = Math.Round(TotalPrice * rateOfExchange, decimalPoint);

                    if (priceTaxDet != null && priceTaxDet.InputVAT != null)
                        hotelTotalPrice = priceTaxDet.InputVAT.CalculateVatAmount(hotelTotalPrice, ref vatAmount, decimalPoint);

                    hotelTotalPrice = Math.Round(hotelTotalPrice, decimalPoint);
                    roomDetail.TotalPrice = hotelTotalPrice;
                    roomDetail.Markup = (markupType == "F") ? markup : (roomDetail.TotalPrice * (markup / 100m));
                    roomDetail.Discount = 0;

                    if (markup > 0 && discount > 0)
                        roomDetail.Discount = discountType == "F" ? roomDetail.Markup - discount : roomDetail.Markup * (discount / 100);

                    roomDetail.Discount = roomDetail.Discount > 0 ? roomDetail.Discount : 0;

                    roomDetail.MarkupType = markupType;
                    roomDetail.MarkupValue = markup;
                    roomDetail.SellingFare = roomDetail.TotalPrice;
                    roomDetail.SupplierName = HotelBookingSource.UAH.ToString();
                    roomDetail.SupplierId = Convert.ToString((int)HotelBookingSource.UAH);
                    roomDetail.supplierPrice = TotalPrice;

                    /* Price calculation for no of days */
                    RoomRates[] hRoomRates = new RoomRates[diffResult.Days];

                    for (int fareIndex = 0; fareIndex < diffResult.Days; fareIndex++)
                    {
                        hRoomRates[fareIndex].Amount = hRoomRates[fareIndex].BaseFare = hRoomRates[fareIndex].SellingFare =
                            hRoomRates[fareIndex].Totalfare = (hotelTotalPrice + roomDetail.Markup - roomDetail.Discount) / diffResult.Days; 
                        hRoomRates[fareIndex].RateType = RateType.Negotiated;
                        hRoomRates[fareIndex].Days = clsHResult.StartDate.AddDays(fareIndex);
                    }

                    roomDetail.Rates = hRoomRates;
                    roomDetail.TaxDetail = new PriceTaxDetails();
                    roomDetail.TaxDetail = priceTaxDet;
                    roomDetail.InputVATAmount = vatAmount;
                    roomDetail.RoomTypeCode = roomDetail.RoomTypeCode + roomDetail.TotalPrice;
                    roomDetail.TotalPrice = roomDetail.TotalPrice + roomDetail.Markup;
                    roomDetail.NextURL = y.NextSteps != null && y.NextSteps.NextStep != null && y.NextSteps.NextStep[0] != null && 
                        !string.IsNullOrEmpty(y.NextSteps.NextStep[0].Value) ? y.NextSteps.NextStep[0].Value : roomDetail.NextURL;
                    roomDetail.RatePlanCode = y.Price.RatePlanCode;
                    roomDetail.RatePlanId = y.Price.RatePlanId;

                    iSeq++;
                    rooms.Add(roomDetail);
                });

                clsHResult.RoomDetails = rooms.OrderBy(x => x.TotalPrice).ToArray();

                /* Update first room price details to result object by default */
                clsHResult.TotalPrice = clsHResult.RoomDetails[0].TotalPrice;
                clsHResult.Price.Discount = clsHResult.RoomDetails[0].Discount;
                clsHResult.Price.AccPriceType = PriceType.NetFare;
                clsHResult.Price.RateOfExchange = rateOfExchange;
                clsHResult.Price.Currency = clsHResult.Price.CurrencyCode = agentCurrency;
                clsHResult.Price.SupplierCurrency = string.IsNullOrEmpty(sSupCurrency) ? agentCurrency : sSupCurrency;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// /* To get hotel price rules */
        /// </summary>
        /// <param name="clsHResult"></param>
        /// <param name="clsHotreq"></param>
        /// <param name="sBookingCode"></param>
        public string GetHotelRules(ref HotelSearchResult clsHResult, string sBookingCode)
        {
            string sCanPol = string.Empty;
            //Cancel Policy Updates
            List<HotelCancelPolicy> cancelPolicyList = new List<HotelCancelPolicy>();
            try
            {
                /* Prepare hotel rules web request URL */
                int TotalPax = clsHResult.RoomGuest.Sum(x => x.noOfAdults + x.noOfChild);
                int iRoomIndx = clsHResult.RoomDetails.ToList().FindIndex(x => x.RoomTypeCode == sBookingCode);

                string sBookCode = sBookingCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[1];

                var urlBuilder_ = new StringBuilder();
                urlBuilder_.Append(sHostURL.TrimEnd('/')).Append("/rules/HotelRules/" + clsHResult.HotelCode.Split('-')[0] + "/" + clsHResult.HotelCode.Split('-')[1] + "?");
                urlBuilder_.Append(Uri.EscapeDataString("checkinDate") + "=").Append(Uri.EscapeDataString(ConvertToString(clsHResult.StartDate.ToString("yyyyMMdd"), CultureInfo.InvariantCulture))).Append("&");
                urlBuilder_.Append(Uri.EscapeDataString("checkoutDate") + "=").Append(Uri.EscapeDataString(ConvertToString(clsHResult.EndDate.ToString("yyyyMMdd"), CultureInfo.InvariantCulture))).Append("&");
                urlBuilder_.Append(Uri.EscapeDataString("bookingCode") + "=").Append(Uri.EscapeDataString(ConvertToString(sBookCode, CultureInfo.InvariantCulture))).Append("&");
                urlBuilder_.Append(Uri.EscapeDataString("numberOfGuests") + "=").Append(Uri.EscapeDataString(ConvertToString(TotalPax.ToString(), CultureInfo.InvariantCulture))).Append("&");
                urlBuilder_.Append(Uri.EscapeDataString("storedCurrency") + "=").Append(Uri.EscapeDataString(ConvertToString(clsHResult.Price.SupplierCurrency, CultureInfo.InvariantCulture))).Append("&");
                urlBuilder_.Length--;

                /* Call web request with the prepared URL */
                //string response = WebReqCall(clsHResult.RoomDetails[iRoomIndx].NextURL.Replace("/rules/", "/rules/v2/"), "GET", null, "HotelRules", string.Empty);
                //string response = WebReqCall(clsHResult.RoomDetails[iRoomIndx].NextURL, "GET", null, "HotelRules", string.Empty);
                clsHResult.RoomDetails[iRoomIndx].NextURL = clsHResult.RoomDetails[iRoomIndx].NextURL.Replace("apigateway.travelport.com", "api-adc.travelport.com");
                string response = WebReqCall(clsHResult.RoomDetails[iRoomIndx].NextURL, "GET", null, "HotelRules", string.Empty);

                /* De serialize Json response and update the hotel rules to result object */
                if (!string.IsNullOrEmpty(response))
                {                    
                    var clsHotelRules = js.Deserialize<HotelRules>(response.Replace("\"HotelRules\": {", "").TrimEnd('}'));
                                       
                    if (clsHotelRules.Rules != null && clsHotelRules.Rules.TextParagraph != null && 
                        clsHotelRules.Rules.TextParagraph.Where(r => r.FormattedText != null).Count() > 0)
                    {
                        string sOtherInfo = string.Empty; string sPolicyText = string.Empty;
                        sCanPol = js.Serialize(clsHotelRules.Rules.TextParagraph);

                        clsHotelRules.Rules.TextParagraph.Where(r => r.FormattedText != null).ToList().ForEach(c => {

                            if (!string.IsNullOrEmpty(c.name) && c.FormattedText != null && c.FormattedText.Count() > 0 && !string.IsNullOrEmpty(c.FormattedText[0].Value))
                            {
                                if (c.name.ToLower() == "cancellation")
                                    sPolicyText = c.FormattedText[0].Value;
                                else
                                    sOtherInfo = string.IsNullOrEmpty(sOtherInfo) ? c.FormattedText[0].Value : sOtherInfo + ", " + c.FormattedText[0].Value;
                            }
                        });

                        for (int i = 0; i < clsHResult.RoomDetails.Length; i++)
                        {
                            if (clsHResult.RoomDetails[i].RoomTypeCode == sBookingCode)
                            {
                                clsHResult.RoomDetails[i].CancellationPolicy = sPolicyText;
                                clsHResult.RoomDetails[i].EssentialInformation = sOtherInfo;

                                //Cancel Policy Updates
                                HotelCancelPolicy policy = new HotelCancelPolicy();
                                policy.Remarks = sPolicyText;
                                policy.ChargeType = null;
                                policy.Currency = null;
                                cancelPolicyList.Add(policy);
                                clsHResult.RoomDetails[i].CancelPolicyList = cancelPolicyList;
                            }
                        }
                    }

                    if (clsHotelRules.Rules != null && clsHotelRules.Rules.TextBlock != null &&
                        clsHotelRules.Rules.TextBlock.Where(r => r.TextFormatted != null).Count() > 0)
                    {
                        string sOtherInfo = string.Empty; string sPolicyText = string.Empty;
                        clsHotelRules.Rules.TextParagraph = new List<TextParagraph>();

                        clsHotelRules.Rules.TextBlock.Where(r => r.TextFormatted != null).ToList().ForEach(c => {

                            if (!string.IsNullOrEmpty(c.Title) && c.TextFormatted != null && c.TextFormatted.Count() > 0 && !string.IsNullOrEmpty(c.TextFormatted[0].Value))
                            {
                                clsHotelRules.Rules.TextParagraph.Add(new TextParagraph { name= c.Title, FormattedText = c.TextFormatted });
                                if (c.Title.ToLower() == "cancellation")
                                    sPolicyText = c.TextFormatted[0].Value;
                                else
                                    sOtherInfo = string.IsNullOrEmpty(sOtherInfo) ? c.Title + "|" + c.TextFormatted[0].Value : sOtherInfo + ", " + c.Title + "|" + c.TextFormatted[0].Value;
                            }
                        });

                        for (int i = 0; i < clsHResult.RoomDetails.Length; i++)
                        {
                            if (clsHResult.RoomDetails[i].RoomTypeCode == sBookingCode)
                            {
                                clsHResult.RoomDetails[i].CancellationPolicy = sPolicyText;
                                clsHResult.RoomDetails[i].EssentialInformation = sOtherInfo;

                                //Cancel Policy Updates
                                HotelCancelPolicy policy = new HotelCancelPolicy();
                                policy.Remarks = sPolicyText;
                                policy.ChargeType = null;
                                policy.Currency = null;
                                cancelPolicyList.Add(policy);
                                clsHResult.RoomDetails[i].CancelPolicyList = cancelPolicyList;
                            }
                        }

                        sCanPol = js.Serialize(clsHotelRules.Rules.TextParagraph);
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBedsAvailSearch, Severity.Normal, 1, "UAPI Hotel Details Request Err :" + ex.ToString(), "0");
            }
            return sCanPol;
        }

        /// <summary>
        /// To make reservation booking based on hotel itinerary object data
        /// </summary>
        /// <param name="clsHI"></param>
        /// <param name="clsCC"></param>
        /// <param name="clsUAPI"></param>
        /// <param name="clsUAC"></param>
        /// <returns></returns>
        public BookingResponse GetBooking(ref HotelItinerary clsHI, CreditCard clsCC, UAPI clsUAPI, UAPICredentials clsUAC)
        {
            BookingResponse clsBR = new BookingResponse();
            try
            {
                /* Prepare create reservation request and make a web call */

                string response = CreateReservation(clsHI, clsCC);

                /* De serialize Json resposne and update the booking confirmation number and booking status */
                bool IsSuccess = false;
                if (!string.IsNullOrEmpty(response))
                {
                    Reservation clsReserv = js.Deserialize<Reservation>(response.Replace("\"Reservation\": {", "").TrimEnd('}'));

                    /* Check erros in response and throw exception */
                    if (clsReserv.Result != null && clsReserv.Result.Error != null && clsReserv.Result.Error.Count > 0)
                    {
                        string sMsg = "Booking response :";
                        foreach (var err in clsReserv.Result.Error)
                        {
                            sMsg += err.StatusCode + ", " + err.Message + Environment.NewLine;
                        }
                        throw new BookingEngineException(sMsg);
                    }

                    /* Read booking response */
                    if (clsReserv.ReservationLocator != null && !string.IsNullOrEmpty(clsReserv.ReservationLocator.ProviderCode))
                    {
                        /* Read provider and supplier locator codes and assign to itinerary object */
                        string sSuppLocator = string.Empty;
                        if (clsReserv.Order != null && clsReserv.Order[0].Offer != null && clsReserv.Order[0].Offer[0].Products != null &&
                            clsReserv.Order[0].Offer[0].Products[0].Product != null && clsReserv.Order[0].Offer[0].Products[0].Product[0].SupplierLocator != null &&
                            clsReserv.Order[0].Offer[0].Products[0].Product[0].SupplierLocator.Value != null)
                        {
                            sSuppLocator = clsReserv.Order[0].Offer[0].Products[0].Product[0].SupplierLocator.Value;
                        }

                        clsHI.ConfirmationNo = clsReserv.ReservationLocator.ProviderCode;

                        /* Get UR Locator code for the confirmed reservation PNR and Add DI info to UR */                        
                        string URLCode = GetURLCode(clsReserv.ReservationLocator.ProviderCode, clsUAPI, clsUAC);
                        if (!string.IsNullOrEmpty(URLCode))
                        {
                            sSuppLocator = URLCode + "|" + sSuppLocator;
                            UpdateURData(clsHI, clsUAPI, clsUAC, URLCode, "0");
                        }                        

                        clsBR.ConfirmationNo = clsReserv.ReservationLocator.ProviderCode + "|" + sSuppLocator;
                        clsHI.Status = HotelBookingStatus.Confirmed;                        
                        clsHI.BookingRefNo = sSuppLocator;
                        IsSuccess = true;
                        clsBR.Status = BookingResponseStatus.Successful;
                        
                        /* Set reservation response comments */
                        if (clsReserv.Order != null && clsReserv.Order[0].Offer != null && clsReserv.Order[0].Offer[0].TermsAndConditions != null &&
                            clsReserv.Order[0].Offer[0].TermsAndConditions[0].Description != null)
                        {
                            string sComments = string.Empty;
                            clsReserv.Order[0].Offer[0].TermsAndConditions[0].Description.Where(x => !string.IsNullOrEmpty(x)).ToList().ForEach(y => {

                                sComments += y + " ";
                            });
                            clsHI.HotelCancelPolicy = string.IsNullOrEmpty(sComments) ? clsHI.HotelCancelPolicy : sComments;
                        }
                    }
                }

                if (!IsSuccess)
                {
                    clsBR.Status = BookingResponseStatus.Failed;
                    clsHI.Status = HotelBookingStatus.Failed;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBook, Severity.Normal, 1, "UAPI Hotel create reservation request Err :" + ex.ToString(), "0");
                clsBR.Status = BookingResponseStatus.Failed;
                throw ex;
            }
            return clsBR;
        }

        /// <summary>
        /// To prepare create reservation request and make a web call
        /// </summary>
        /// <param name="clsHI"></param>
        /// <returns></returns>
        private string CreateReservation(HotelItinerary clsHI, CreditCard clsCC)
        {
            try
            {
                /* Prepare hotel reservation request */
                List<Telephone> liTel = new List<Telephone>();
                if (!string.IsNullOrEmpty(clsHI.Roomtype[0].PassenegerInfo[0].Phoneno))
                    liTel.Add(new Telephone { PhoneNumber = clsHI.Roomtype[0].PassenegerInfo[0].Phoneno.Replace("-", string.Empty).Replace("+", string.Empty).Replace(" ", string.Empty) });

                Reservation clsRerv = new Reservation();
                clsRerv.Traveler = new List<Traveler>();
                clsRerv.Traveler.Add(new Traveler
                {
                    PersonName = new PersonName
                    {
                        Given = clsHI.Roomtype[0].PassenegerInfo[0].Firstname,
                        Surname = clsHI.Roomtype[0].PassenegerInfo[0].Lastname,
                        Title = clsHI.Roomtype[0].PassenegerInfo[0].Title
                    },
                    Telephone = liTel,

                });

                List<FormOfPaymentPaymentCard> liFOP = new List<FormOfPaymentPaymentCard>();

                var clsFOP = new FormOfPaymentPaymentCard();
                clsFOP.GuaranteeIndicatorInd = true;
                clsFOP.GuaranteeTypeCode = !string.IsNullOrEmpty(clsHI.Roomtype[0].GuaranteeTypeCode) ? clsHI.Roomtype[0].GuaranteeTypeCode : clsFOP.GuaranteeTypeCode;
                clsFOP.PaymentCard = new PaymentCard
                {
                    CardCode = clsCC.CardType,
                    CardType = PaymentCardType.Credit,
                    ExpireDate = clsCC.ExpDate,
                    CardHolderName = string.IsNullOrEmpty(clsCC.Name) ? clsHI.Roomtype[0].PassenegerInfo[0].Firstname : clsCC.Name,
                    CardNumber = new CardNumber { PlainText = clsCC.Number }
                };
                liFOP.Add(clsFOP);
                //LIVE CARD
                //clsFOP.PaymentCard = new PaymentCard
                //{
                //    CardCode = "CA",
                //    CardType = PaymentCardType.Credit,
                //    ExpireDate = "1121",
                //    CardHolderName = "Cozmo Travel",
                //    CardNumber = new CardNumber { PlainText = "5585920203727152" }
                //};

                // liFOP.Add(clsFOP);
                // Test Card
                //clsFOP = new FormOfPaymentPaymentCard();
                //clsFOP.GuaranteeIndicatorInd = true;
                //clsFOP.GuaranteeTypeCode = !string.IsNullOrEmpty(clsHI.Roomtype[0].GuaranteeTypeCode) ? clsHI.Roomtype[0].GuaranteeTypeCode : clsFOP.GuaranteeTypeCode;
                //clsFOP.PaymentCard = new PaymentCard
                //{
                //    CardCode = "VI",
                //    CardType = PaymentCardType.Credit,
                //    ExpireDate = "1120",
                //    CardHolderName = "FRANK SINATRA",
                //    CardNumber = new CardNumber { PlainText = "4444333322221111" }
                //};
                //liFOP.Add(clsFOP);

                List<Payment> liPmnt = new List<Payment>();
                liPmnt.Add(new Payment
                {
                    Amount = new CurrencyAmount { Value = Convert.ToDouble(clsHI.Roomtype[0].Price.SupplierPrice * clsHI.NoOfRooms), Code = clsHI.Roomtype[0].Price.SupplierCurrency },
                    FormOfPayment = liFOP
                });

                List<ProductHospitality> liProduct = new List<ProductHospitality>();

                string sBookCode = clsHI.Roomtype[0].RoomTypeCode.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries)[1];

                liProduct.Add(new ProductHospitality
                {
                    QuantityRequested = clsHI.NoOfRooms,
                    DateRange = new DateRange { Start = clsHI.StartDate, End = clsHI.EndDate },
                    PropertyKey = new PropertyKey { ChainCode = clsHI.HotelCode.Split('-')[0], PropertyCode = clsHI.HotelCode.Split('-')[1] },
                    BookingCode = sBookCode,
                    Guests = clsHI.Roomtype.Sum(x => x.AdultCount + x.ChildCount)
                });

                List<Products> liProducts = new List<Products>();
                liProducts.Add(new Products { Product = liProduct });

                List<Offer> liOffer = new List<Offer>();
                liOffer.Add(new Offer {
                    Products = liProducts,
                    Price = new PriceHospitality { RatePlanCode = clsHI.Roomtype[0].RatePlanCode, RatePlanId = clsHI.Roomtype[0].RatePlanId }
                });

                List<NameValuePair> liNVP = null; 
                if (clsHI.Roomtype[0].PassenegerInfo[0].FlexDetailsList != null && clsHI.Roomtype[0].PassenegerInfo[0].FlexDetailsList.Count() > 0)
                {
                    liNVP = new List<NameValuePair>();
                    clsHI.Roomtype[0].PassenegerInfo[0].FlexDetailsList =
                        clsHI.Roomtype[0].PassenegerInfo[0].FlexDetailsList.Where(x => !string.IsNullOrEmpty(x.FlexData)).ToList();
                    foreach (var flexinfo in clsHI.Roomtype[0].PassenegerInfo[0].FlexDetailsList)
                    {
                        liNVP.Add(new NameValuePair { Name = flexinfo.FlexGDSprefix, Value = flexinfo.FlexGDSprefix + flexinfo.FlexData });
                    }
                }

                clsRerv.Order = new List<OrderDetail>();
                clsRerv.Order.Add(new OrderDetail
                {
                    Payment = liPmnt,
                    Offer = liOffer,
                    BackOffice = liNVP != null && liNVP.Count() > 0 ? new BackOffice { NameValuePair = liNVP, DataType = "String Value" } : null
                });
                
                clsRerv.ReservationLocator = !string.IsNullOrEmpty(clsHI.URLocator) ? new ProviderLocator { ProviderCode = clsHI.URLocator } : clsRerv.ReservationLocator;

                clsRerv.ReservationComment = new List<ReservationComment>();

                List<UAPIdll.Hotel.Comment> liComment = new List<UAPIdll.Hotel.Comment>();

                liComment.Add(new UAPIdll.Hotel.Comment { Value = "Please do not charge booking amount from customer" });

                clsRerv.ReservationComment.Add(new ReservationComment { Comment = liComment });

                CreateReserv clsCR = new CreateReserv();
                clsCR.CreateReq = clsRerv;

                /* Convert reservation request object to Json and do a web request call */
                string response = WebReqCall(sHostURL.TrimEnd('/') + "/book/v2/reservations", "POST", null, "CreateReservation", SerializeJson(clsCR));

                return response;
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.HotelBook, Severity.Normal, 1, "UAPI Hotel create reservation request Err :" + ex.ToString(), "0");
                throw ex;
            }
        }

        /// <summary>
        /// To cancel reservation
        /// </summary>
        /// <param name="sPNR"></param>
        /// <param name="sConfirmNo"></param>
        public Dictionary<string, string> CancelReservation(string sProviderCode, string sSuppLocCode)
        {
            Dictionary<string, string> cancelRes = new Dictionary<string, string>();
            cancelRes.Add("Status", "FAILED");
            try
            {
                /* Append PNR and confirmation no to URL and make Web request call to cancel the reservation */
                string response = WebReqCall(sHostURL.TrimEnd('/') + "/book/v2/reservations/" + sProviderCode + "/confirmation/" + sSuppLocCode, 
                    "DELETE", null, "CancelReservation", string.Empty);

                /* De serialize Json resposne and update the cancellation id and cancellation status */
                
                if (!string.IsNullOrEmpty(response))
                {
                    Reservation clsReserv = js.Deserialize<Reservation>(response.Replace("\"Reservation\": {", "").TrimEnd('}'));
                    
                    if (clsReserv.Order != null && clsReserv.Order[0].Offer != null && clsReserv.Order[0].Offer[0].Products != null &&
                        clsReserv.Order[0].Offer[0].Products[0].Product != null && clsReserv.Order[0].Offer[0].Products[0].Product[0].Status != null &&
                        clsReserv.Order[0].Offer[0].Products[0].Product[0].CancellationID != null && 
                        clsReserv.Order[0].Offer[0].Products[0].Product[0].Status.TravelportStatus != null &&
                        !string.IsNullOrEmpty(clsReserv.Order[0].Offer[0].Products[0].Product[0].Status.TravelportStatus.Value) &&
                        !string.IsNullOrEmpty(clsReserv.Order[0].Offer[0].Products[0].Product[0].CancellationID.Value))
                    {
                        cancelRes = new Dictionary<string, string>();
                        cancelRes.Add("Status", "Cancelled");
                        cancelRes.Add("ID", clsReserv.Order[0].Offer[0].Products[0].Product[0].CancellationID.Value);
                        //cancelRes.Add("Amount", Convert.ToString(response.CancellationFee));
                        //cancelRes.Add("Currency", response.Currency);
                    }                    
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBook, Severity.Normal, 1, "UAPI Hotel cancel reservation request Err :" + ex.ToString(), "0");
                throw ex;
            }
            return cancelRes;
        }

        /// <summary>
        /// To get URLocator code of the created reservation using UAPI import PNR
        /// </summary>
        /// <param name="sPNR"></param>
        /// <param name="clsUAPI"></param>
        /// <param name="clsUAC"></param>
        /// <returns></returns>
        private string GetURLCode(string sPNR, UAPI clsUAPI, UAPICredentials clsUAC)
        {
            string sURLCode = string.Empty;
            try
            {
                UAPIdll.Universal46.UniversalRecordImportRsp clsURImpRsp = clsUAPI.ImportUR(sPNR, clsUAC);
                sURLCode = clsURImpRsp != null && clsURImpRsp.UniversalRecord != null ? clsURImpRsp.UniversalRecord.LocatorCode : sURLCode;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBook, Severity.Normal, 1, "Failed to GetURLCode for UAPI Hotel reservation PNR(" + sPNR + "), Err :" + ex.ToString(), "0");
            }
            return sURLCode;
        }

        /// <summary>
        /// To create UR modify request and call UAPI UR modify req
        /// </summary>
        /// <param name="clsHI"></param>
        /// <param name="clsUAPI"></param>
        /// <param name="clsUAC"></param>
        /// <param name="URLCode"></param>
        /// <param name="URVersion"></param>
        /// <returns></returns>
        private string UpdateURData(HotelItinerary clsHI, UAPI clsUAPI, UAPICredentials clsUAC, string URLCode, string URVersion)
        {
            string sURModRsp = string.Empty;
            try
            {
                UAPIdll.Universal46.UniversalRecordModifyReq clsURModReq = new UAPIdll.Universal46.UniversalRecordModifyReq();

                clsURModReq.Version = URVersion;
                clsURModReq.RecordIdentifier = new UAPIdll.Universal46.RecordIdentifier();
                clsURModReq.RecordIdentifier.ProviderLocatorCode = clsHI.ConfirmationNo;
                clsURModReq.RecordIdentifier.UniversalLocatorCode = URLCode;

                clsURModReq.UniversalModifyCmd = new UAPIdll.Universal46.UniversalModifyCmd[1];
                clsURModReq.UniversalModifyCmd[0] = new UAPIdll.Universal46.UniversalModifyCmd();
                clsURModReq.UniversalModifyCmd[0].Key = "0";

                List<UAPIdll.Universal46.AccountingRemark> liDIinfo = new List<UAPIdll.Universal46.AccountingRemark>();
                clsHI.Roomtype[0].PassenegerInfo[0].FlexDetailsList.Where(y => !string.IsNullOrEmpty(y.FlexGDSprefix) || !string.IsNullOrEmpty(y.FlexData)).ToList().ForEach(x => {

                    string sFlexData = string.IsNullOrEmpty(x.FlexData) ? string.Empty : GenericStatic.ReplaceNonASCII(x.FlexData.Replace("@", "//").Replace("_", "--"));
                    liDIinfo.Add(new UAPIdll.Universal46.AccountingRemark {
                                                
                        Category ="FT",
                        TypeInGds = "Other",
                        ProviderCode = "1G",
                        RemarkData = (x.FlexGDSprefix + sFlexData).Length < 84 ? x.FlexGDSprefix + sFlexData :
                                    (x.FlexGDSprefix + sFlexData).Substring(0, 84)
                    });
                });

                UAPIdll.Universal46.UniversalAdd clsUAdd = new UAPIdll.Universal46.UniversalAdd();
                clsUAdd.Items = liDIinfo.ToArray();
                clsURModReq.UniversalModifyCmd[0].Item = clsUAdd;

                UAPIdll.Universal46.UniversalRecordModifyRsp clsURModRsp = new UAPIdll.Universal46.UniversalRecordModifyRsp();
                clsUAPI.ModifyUR(clsURModReq, ref clsURModRsp, clsUAC);
                if (clsURModRsp != null && clsURModRsp.ResponseMessage != null && !string.IsNullOrEmpty(clsURModRsp.ResponseMessage[0].Value))
                    sURModRsp = clsURModRsp.ResponseMessage[0].Value;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBook, Severity.Normal, 1, "Failed to UpdateURData for UAPI Hotel reservation PNR(" + clsHI.ConfirmationNo + "), Err :" + ex.ToString(), "0");
            }
            return sURModRsp;
        }

        /// <summary>
        /// To convert string as per given culture info
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        private string ConvertToString(object value, CultureInfo cultureInfo)
        {
            if (value is Enum)
            {
                string name = Enum.GetName(value.GetType(), value);
                if (name != null)
                {
                    var field = IntrospectionExtensions.GetTypeInfo(value.GetType()).GetDeclaredField(name);
                    if (field != null)
                    {
                        var attribute = CustomAttributeExtensions.GetCustomAttribute(field, typeof(EnumMemberAttribute))
                            as EnumMemberAttribute;
                        if (attribute != null)
                        {
                            return attribute.Value != null ? attribute.Value : name;
                        }
                    }
                }
            }
            else if (value is bool)
            {
                return Convert.ToString(value, cultureInfo).ToLowerInvariant();
            }
            else if (value is byte[])
            {
                return Convert.ToBase64String((byte[])value);
            }
            else if (value != null && value.GetType().IsArray)
            {
                var array = Enumerable.OfType<object>((Array)value);
                return string.Join(",", Enumerable.Select(array, o => ConvertToString(o, cultureInfo)));
            }

            return Convert.ToString(value, cultureInfo);
        }

        /// <summary>
        /// To serialize given object into Json string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SerializeJson<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }
    }
}
